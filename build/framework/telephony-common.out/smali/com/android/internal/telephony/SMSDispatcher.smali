.class public abstract Lcom/android/internal/telephony/SMSDispatcher;
.super Landroid/os/Handler;
.source "SMSDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/SMSDispatcher$SegmentExpirationRunnable;,
        Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;,
        Lcom/android/internal/telephony/SMSDispatcher$Segment;,
        Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;,
        Lcom/android/internal/telephony/SMSDispatcher$ConfirmDialogListener;,
        Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;,
        Lcom/android/internal/telephony/SMSDispatcher$SettingsObserver;,
        Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;
    }
.end annotation


# static fields
.field protected static final ADDRESS_TRANSLATION_FAILURE:I = 0x1

.field protected static final CDMA_SMS_SERVICE_OPTION_14:I = 0xe

.field protected static final CDMA_SMS_SERVICE_OPTION_6:I = 0x6

.field private static final DEBUG:Z = false

.field private static final DEFAULT_SEND_DELAY:I = 0x12c

.field private static final DESTINATION_PORT_COLUMN:I = 0x2

.field private static final DUPLICATE_PROJECTION:[Ljava/lang/String; = null

.field public static final EMS_EXPIRATION_TIME:I = 0x493e0

.field private static final EVENT_CONFIRM_SEND_TO_POSSIBLE_PREMIUM_SHORT_CODE:I = 0x8

.field private static final EVENT_CONFIRM_SEND_TO_PREMIUM_SHORT_CODE:I = 0x9

.field protected static final EVENT_ICC_CHANGED:I = 0xe

.field protected static final EVENT_IMS_STATE_CHANGED:I = 0xb

.field protected static final EVENT_IMS_STATE_DONE:I = 0xc

.field protected static final EVENT_NEW_ICC_SMS:I = 0xd

.field protected static final EVENT_NEW_SMS:I = 0x1

.field protected static final EVENT_RADIO_ON:I = 0xa

.field protected static final EVENT_RETRY_ALERT_TIMEOUT:I = 0x10

.field static final EVENT_SEND_CONFIRMED_SMS:I = 0x5

.field private static final EVENT_SEND_LIMIT_REACHED_CONFIRMATION:I = 0x4

.field protected static final EVENT_SEND_MESSAGE:I = 0x15

.field protected static final EVENT_SEND_MORE_MESSAGE:I = 0x16

.field private static final EVENT_SEND_RETRY:I = 0x3

.field protected static final EVENT_SEND_RETRY_CONFIRMED_SMS:I = 0x11

.field protected static final EVENT_SEND_RETRY_WITHPOPUP:I = 0x13

.field protected static final EVENT_SEND_SMS_COMPLETE:I = 0x2

.field protected static final EVENT_SMS_ON_ICC:I = 0xf

.field protected static final EVENT_STOP_RETRY_SENDING:I = 0x12

.field static final EVENT_STOP_SENDING:I = 0x7

.field protected static final EVENT_UPDATE_ICC_MWI:I = 0x14

.field protected static final EVNET_DAN_SMS_COMPLETE:I = 0x15

.field protected static final GENERAL_PROBLEMS:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final MAX_SEND_RETRIES:I = 0x0

.field private static final MEM_THRESHOLD:I = 0x2000

#the value of this static final field might be set in the static constructor
.field private static final MO_MSG_QUEUE_LIMIT:I = 0x0

.field protected static final MSG_NOT_SENT:I = 0x5

.field protected static final MSG_TOO_LONG_FORNEWORK:I = 0x4

.field protected static final NETWORK_PROBLEMS:I = 0x1

.field protected static final NUMBER_OF_CAUSE_CODE:I = 0x33

.field private static final PDU_COLUMN:I = 0x0

.field private static final PDU_PROJECTION:[Ljava/lang/String; = null

.field private static final PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String; = null

.field private static final PREMIUM_RULE_USE_BOTH:I = 0x3

.field private static final PREMIUM_RULE_USE_NETWORK:I = 0x2

.field private static final PREMIUM_RULE_USE_SIM:I = 0x1

.field private static final PRIVACY_ENABLE:Z = false

.field static RECEIVE_DAN_SUCCESS:Z = false

.field public static final RECEIVE_EMERGENCY_BROADCAST_PERMISSION:Ljava/lang/String; = "android.permission.RECEIVE_EMERGENCY_BROADCAST"

.field public static final RECEIVE_SMS_PERMISSION:Ljava/lang/String; = "android.permission.RECEIVE_SMS"

.field private static final SEND_NEXT_MSG_EXTRA:Ljava/lang/String; = "SendNextMsg"

#the value of this static final field might be set in the static constructor
.field private static final SEND_RETRY_DELAY:I = 0x0

.field private static final SEND_SMS_NO_CONFIRMATION_PERMISSION:Ljava/lang/String; = "android.permission.SEND_SMS_NO_CONFIRMATION"

.field private static final SEQUENCE_COLUMN:I = 0x1

.field protected static final SERVICE_NOT_AVAILABLE:I = 0x2

.field protected static final SERVICE_NOT_SUPPORTED:I = 0x3

.field private static final SINGLE_PART_SMS:I = 0x1

.field private static final SMS_INBOX_CONSTRAINT:Ljava/lang/String; = "(type = 1)"

.field private static final SMS_INBOX_MAX_COUNT:I = 0x32

.field protected static final SMS_NETWORK_FAILURE:I = 0x3

.field protected static final SMS_NOT_SUPPORTED:I = 0x64

.field protected static final SMS_ORIGINATION_DENIED:I = 0x61

.field private static final SMS_RETRY_POPUP_DISP_TIMEOUT:I = 0x2710

.field static final TAG:Ljava/lang/String; = "SMS"

.field private static final VZW_PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String; = null

.field private static final WAKE_LOCK_TIMEOUT:I = 0x1388

.field protected static final deliveryPendingListForKddi:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;",
            ">;"
        }
    .end annotation
.end field

.field private static excutedSegmentExpirationAfterBootUp:Z

.field protected static final mRawUri:Landroid/net/Uri;

.field public static mSubmitIsRoaming:Z

.field public static mSubmitPriority:I

.field private static sConcatenatedRef:I

.field private static segmentExpirationLock:Ljava/util/concurrent/locks/ReentrantLock;

.field public static vp:I


# instance fields
.field private causeCodeArray:[J

.field protected final deliveryPendingList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;",
            ">;"
        }
    .end annotation
.end field

.field protected domainNotiMsgCsIms:I

.field protected domainNotiMsgCsOnly:I

.field protected domainNotiMsgImsOnly:I

.field private errorCodeArray:[J

.field protected final mCm:Lcom/android/internal/telephony/CommandsInterface;

.field protected final mContext:Landroid/content/Context;

.field protected mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

.field private mPendingMessagesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingTrackerCount:I

.field protected mPhone:Lcom/android/internal/telephony/PhoneBase;

.field private final mPremiumSmsRule:Ljava/util/concurrent/atomic/AtomicInteger;

.field protected mRemainingMessages:I

.field protected final mResolver:Landroid/content/ContentResolver;

.field private final mResultReceiver:Landroid/content/BroadcastReceiver;

.field private mRetryListener:Landroid/content/DialogInterface$OnClickListener;

.field private mSTrackersForRetry:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;",
            ">;"
        }
    .end annotation
.end field

.field protected final mSegmentUri:Landroid/net/Uri;

.field private final mSettingsObserver:Lcom/android/internal/telephony/SMSDispatcher$SettingsObserver;

.field protected mSmsCapable:Z

.field protected mSmsReceiveDisabled:Z

.field protected mSmsSendDisabled:Z

.field protected mStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

.field private mSyncronousSending:Z

.field protected final mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field protected final mWapPush:Lcom/android/internal/telephony/WapPushOverSms;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x4

    #@2
    const/4 v5, 0x3

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 147
    sput-boolean v3, Lcom/android/internal/telephony/SMSDispatcher;->RECEIVE_DAN_SUCCESS:Z

    #@7
    .line 164
    new-array v0, v4, [Ljava/lang/String;

    #@9
    const-string v1, "pdu"

    #@b
    aput-object v1, v0, v3

    #@d
    sput-object v0, Lcom/android/internal/telephony/SMSDispatcher;->PDU_PROJECTION:[Ljava/lang/String;

    #@f
    .line 169
    new-array v0, v6, [Ljava/lang/String;

    #@11
    const-string v1, "pdu"

    #@13
    aput-object v1, v0, v3

    #@15
    const-string v1, "sequence"

    #@17
    aput-object v1, v0, v4

    #@19
    const/4 v1, 0x2

    #@1a
    const-string v2, "destination_port"

    #@1c
    aput-object v2, v0, v1

    #@1e
    const-string v1, "icc_index"

    #@20
    aput-object v1, v0, v5

    #@22
    sput-object v0, Lcom/android/internal/telephony/SMSDispatcher;->PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String;

    #@24
    .line 180
    const/4 v0, 0x5

    #@25
    new-array v0, v0, [Ljava/lang/String;

    #@27
    const-string v1, "pdu"

    #@29
    aput-object v1, v0, v3

    #@2b
    const-string v1, "sequence"

    #@2d
    aput-object v1, v0, v4

    #@2f
    const/4 v1, 0x2

    #@30
    const-string v2, "destination_port"

    #@32
    aput-object v2, v0, v1

    #@34
    const-string v1, "icc_index"

    #@36
    aput-object v1, v0, v5

    #@38
    const-string v1, "time"

    #@3a
    aput-object v1, v0, v6

    #@3c
    sput-object v0, Lcom/android/internal/telephony/SMSDispatcher;->VZW_PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String;

    #@3e
    .line 194
    const/4 v0, 0x5

    #@3f
    new-array v0, v0, [Ljava/lang/String;

    #@41
    const-string v1, "_id"

    #@43
    aput-object v1, v0, v3

    #@45
    const-string v1, "address"

    #@47
    aput-object v1, v0, v4

    #@49
    const/4 v1, 0x2

    #@4a
    const-string v2, "date_sent"

    #@4c
    aput-object v2, v0, v1

    #@4e
    const-string v1, "person"

    #@50
    aput-object v1, v0, v5

    #@52
    const-string v1, "body"

    #@54
    aput-object v1, v0, v6

    #@56
    sput-object v0, Lcom/android/internal/telephony/SMSDispatcher;->DUPLICATE_PROJECTION:[Ljava/lang/String;

    #@58
    .line 334
    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@5a
    const-string v1, "raw"

    #@5c
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@5f
    move-result-object v0

    #@60
    sput-object v0, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@62
    .line 349
    const-string v0, "doNotUse_AP_retry"

    #@64
    invoke-static {v7, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@67
    move-result v0

    #@68
    if-ne v0, v4, :cond_a8

    #@6a
    .line 350
    sput v3, Lcom/android/internal/telephony/SMSDispatcher;->MAX_SEND_RETRIES:I

    #@6c
    .line 365
    :goto_6c
    const-string v0, "vzw_sms_retry_scheme"

    #@6e
    invoke-static {v7, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@71
    move-result v0

    #@72
    if-ne v0, v4, :cond_b6

    #@74
    .line 366
    const/16 v0, 0x7530

    #@76
    sput v0, Lcom/android/internal/telephony/SMSDispatcher;->SEND_RETRY_DELAY:I

    #@78
    .line 383
    :goto_78
    const-string v0, "increase_mo_msg_queue_limit_vzw"

    #@7a
    invoke-static {v7, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7d
    move-result v0

    #@7e
    if-ne v0, v4, :cond_bb

    #@80
    .line 384
    const/4 v0, 0x7

    #@81
    sput v0, Lcom/android/internal/telephony/SMSDispatcher;->MO_MSG_QUEUE_LIMIT:I

    #@83
    .line 401
    :goto_83
    new-instance v0, Ljava/util/Random;

    #@85
    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    #@88
    const/16 v1, 0x100

    #@8a
    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    #@8d
    move-result v0

    #@8e
    sput v0, Lcom/android/internal/telephony/SMSDispatcher;->sConcatenatedRef:I

    #@90
    .line 430
    const/4 v0, -0x1

    #@91
    sput v0, Lcom/android/internal/telephony/SMSDispatcher;->vp:I

    #@93
    .line 434
    sput v3, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitPriority:I

    #@95
    .line 462
    sput-boolean v3, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitIsRoaming:Z

    #@97
    .line 593
    new-instance v0, Ljava/util/ArrayList;

    #@99
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9c
    sput-object v0, Lcom/android/internal/telephony/SMSDispatcher;->deliveryPendingListForKddi:Ljava/util/ArrayList;

    #@9e
    .line 4604
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    #@a0
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@a3
    sput-object v0, Lcom/android/internal/telephony/SMSDispatcher;->segmentExpirationLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@a5
    .line 4636
    sput-boolean v3, Lcom/android/internal/telephony/SMSDispatcher;->excutedSegmentExpirationAfterBootUp:Z

    #@a7
    return-void

    #@a8
    .line 352
    :cond_a8
    const-string v0, "vzw_sms_retry_scheme"

    #@aa
    invoke-static {v7, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@ad
    move-result v0

    #@ae
    if-ne v0, v4, :cond_b3

    #@b0
    .line 353
    sput v6, Lcom/android/internal/telephony/SMSDispatcher;->MAX_SEND_RETRIES:I

    #@b2
    goto :goto_6c

    #@b3
    .line 356
    :cond_b3
    sput v5, Lcom/android/internal/telephony/SMSDispatcher;->MAX_SEND_RETRIES:I

    #@b5
    goto :goto_6c

    #@b6
    .line 368
    :cond_b6
    const/16 v0, 0x7d0

    #@b8
    sput v0, Lcom/android/internal/telephony/SMSDispatcher;->SEND_RETRY_DELAY:I

    #@ba
    goto :goto_78

    #@bb
    .line 388
    :cond_bb
    const/16 v0, 0x1e

    #@bd
    sput v0, Lcom/android/internal/telephony/SMSDispatcher;->MO_MSG_QUEUE_LIMIT:I

    #@bf
    goto :goto_83
.end method

.method protected constructor <init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;)V
    .registers 10
    .parameter "phone"
    .parameter "storageMonitor"
    .parameter "usageMonitor"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 483
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    .line 219
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    #@7
    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@a
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPremiumSmsRule:Ljava/util/concurrent/atomic/AtomicInteger;

    #@c
    .line 296
    const/4 v1, 0x4

    #@d
    new-array v1, v1, [J

    #@f
    fill-array-data v1, :array_14e

    #@12
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->errorCodeArray:[J

    #@14
    .line 298
    const/16 v1, 0x33

    #@16
    new-array v1, v1, [J

    #@18
    fill-array-data v1, :array_162

    #@1b
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->causeCodeArray:[J

    #@1d
    .line 337
    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@1f
    const-string v4, "segment"

    #@21
    invoke-static {v1, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@24
    move-result-object v1

    #@25
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSegmentUri:Landroid/net/Uri;

    #@27
    .line 410
    new-instance v1, Ljava/util/ArrayList;

    #@29
    sget v4, Lcom/android/internal/telephony/SMSDispatcher;->MO_MSG_QUEUE_LIMIT:I

    #@2b
    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    #@2e
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@30
    .line 424
    iput-boolean v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsCapable:Z

    #@32
    .line 428
    const/4 v1, -0x1

    #@33
    iput v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@35
    .line 466
    iput v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->domainNotiMsgCsOnly:I

    #@37
    .line 467
    iput v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->domainNotiMsgImsOnly:I

    #@39
    .line 468
    const/4 v1, 0x2

    #@3a
    iput v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->domainNotiMsgCsIms:I

    #@3c
    .line 591
    new-instance v1, Ljava/util/ArrayList;

    #@3e
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@41
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->deliveryPendingList:Ljava/util/ArrayList;

    #@43
    .line 3754
    new-instance v1, Lcom/android/internal/telephony/SMSDispatcher$1;

    #@45
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/SMSDispatcher$1;-><init>(Lcom/android/internal/telephony/SMSDispatcher;)V

    #@48
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResultReceiver:Landroid/content/BroadcastReceiver;

    #@4a
    .line 3862
    new-instance v1, Lcom/android/internal/telephony/SMSDispatcher$2;

    #@4c
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/SMSDispatcher$2;-><init>(Lcom/android/internal/telephony/SMSDispatcher;)V

    #@4f
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mRetryListener:Landroid/content/DialogInterface$OnClickListener;

    #@51
    .line 484
    iput-object p1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@53
    .line 485
    new-instance v1, Lcom/android/internal/telephony/WapPushOverSms;

    #@55
    invoke-direct {v1, p1, p0}, Lcom/android/internal/telephony/WapPushOverSms;-><init>(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/SMSDispatcher;)V

    #@58
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@5a
    .line 486
    invoke-virtual {p1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5d
    move-result-object v1

    #@5e
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@60
    .line 487
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@62
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@65
    move-result-object v1

    #@66
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@68
    .line 488
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@6a
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@6c
    .line 489
    iput-object p2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@6e
    .line 490
    iput-object p3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@70
    .line 491
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@72
    const-string v4, "phone"

    #@74
    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@77
    move-result-object v1

    #@78
    check-cast v1, Landroid/telephony/TelephonyManager;

    #@7a
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@7c
    .line 492
    new-instance v1, Lcom/android/internal/telephony/SMSDispatcher$SettingsObserver;

    #@7e
    iget-object v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPremiumSmsRule:Ljava/util/concurrent/atomic/AtomicInteger;

    #@80
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@82
    invoke-direct {v1, p0, v4, v5}, Lcom/android/internal/telephony/SMSDispatcher$SettingsObserver;-><init>(Landroid/os/Handler;Ljava/util/concurrent/atomic/AtomicInteger;Landroid/content/Context;)V

    #@85
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSettingsObserver:Lcom/android/internal/telephony/SMSDispatcher$SettingsObserver;

    #@87
    .line 493
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@89
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8c
    move-result-object v1

    #@8d
    const-string v4, "sms_short_code_rule"

    #@8f
    invoke-static {v4}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@92
    move-result-object v4

    #@93
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSettingsObserver:Lcom/android/internal/telephony/SMSDispatcher$SettingsObserver;

    #@95
    invoke-virtual {v1, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@98
    .line 497
    const/4 v1, 0x0

    #@99
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    #@9b
    .line 500
    invoke-direct {p0}, Lcom/android/internal/telephony/SMSDispatcher;->createWakelock()V

    #@9e
    .line 502
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@a0
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a3
    move-result-object v1

    #@a4
    const v4, 0x1110031

    #@a7
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@aa
    move-result v1

    #@ab
    iput-boolean v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsCapable:Z

    #@ad
    .line 504
    const-string v1, "telephony.sms.receive"

    #@af
    iget-boolean v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsCapable:Z

    #@b1
    invoke-static {v1, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@b4
    move-result v1

    #@b5
    if-nez v1, :cond_13b

    #@b7
    move v1, v2

    #@b8
    :goto_b8
    iput-boolean v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsReceiveDisabled:Z

    #@ba
    .line 506
    const-string v1, "telephony.sms.send"

    #@bc
    iget-boolean v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsCapable:Z

    #@be
    invoke-static {v1, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@c1
    move-result v1

    #@c2
    if-nez v1, :cond_13e

    #@c4
    move v1, v2

    #@c5
    :goto_c5
    iput-boolean v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsSendDisabled:Z

    #@c7
    .line 508
    const-string v1, "SMS"

    #@c9
    new-instance v4, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    const-string v5, "SMSDispatcher: ctor mSmsCapable="

    #@d0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v4

    #@d4
    iget-boolean v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsCapable:Z

    #@d6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v4

    #@da
    const-string v5, " format="

    #@dc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v4

    #@e0
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@e3
    move-result-object v5

    #@e4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v4

    #@e8
    const-string v5, " mSmsReceiveDisabled="

    #@ea
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v4

    #@ee
    iget-boolean v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsReceiveDisabled:Z

    #@f0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v4

    #@f4
    const-string v5, " mSmsSendDisabled="

    #@f6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v4

    #@fa
    iget-boolean v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsSendDisabled:Z

    #@fc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v4

    #@100
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v4

    #@104
    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@107
    .line 512
    new-instance v1, Ljava/util/ArrayList;

    #@109
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@10c
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@10e
    .line 513
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@110
    const-string v4, "sms_gcf_config"

    #@112
    invoke-static {v1, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@115
    move-result v1

    #@116
    if-ne v1, v2, :cond_140

    #@118
    .line 514
    const-string v1, "SMSDispatcher(), Creator KEY_SMS_GCF_CONFIG is Defined -> AsyncronousSending"

    #@11a
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@11d
    .line 515
    iput-boolean v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSyncronousSending:Z

    #@11f
    .line 525
    :goto_11f
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@121
    const-string v2, "sprint_reassembly_sms"

    #@123
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@126
    move-result v1

    #@127
    if-eqz v1, :cond_13a

    #@129
    .line 526
    new-instance v0, Landroid/content/IntentFilter;

    #@12b
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@12e
    .line 527
    .local v0, Segmentfilter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    #@130
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@133
    .line 528
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@135
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResultReceiver:Landroid/content/BroadcastReceiver;

    #@137
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@13a
    .line 531
    .end local v0           #Segmentfilter:Landroid/content/IntentFilter;
    :cond_13a
    return-void

    #@13b
    :cond_13b
    move v1, v3

    #@13c
    .line 504
    goto/16 :goto_b8

    #@13e
    :cond_13e
    move v1, v3

    #@13f
    .line 506
    goto :goto_c5

    #@140
    .line 517
    :cond_140
    const-string v1, "SMSDispatcher(), Creator KEY_SMS_GCF_CONFIG is NOT Defined -> SyncronousSending"

    #@142
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@145
    .line 518
    const-string v1, "persist.radio.sms_sync_sending"

    #@147
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@14a
    move-result v1

    #@14b
    iput-boolean v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSyncronousSending:Z

    #@14d
    goto :goto_11f

    #@14e
    .line 296
    :array_14e
    .array-data 0x8
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
    .end array-data

    #@162
    .line 298
    :array_162
    .array-data 0x8
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x1ft 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x21t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x22t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x23t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x24t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x25t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x26t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x27t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x28t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x2ft 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x30t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x3ft 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x40t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x41t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x42t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x43t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x5ft 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x60t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x61t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x62t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x63t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x64t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x65t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x66t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x67t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x68t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x69t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x6at 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x6bt 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x6ct 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0xfft 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x1t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x2t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x3t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x4t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x5t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x6t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x7t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x8t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x9t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0xat 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0xbt 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/SMSDispatcher;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 138
    invoke-direct {p0}, Lcom/android/internal/telephony/SMSDispatcher;->trySegmentExpirationAfterBootUp()V

    #@3
    return-void
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 138
    sget-object v0, Lcom/android/internal/telephony/SMSDispatcher;->VZW_PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->sendSavedPdusIndividually(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private checkAvailableToSend(Landroid/app/PendingIntent;)Z
    .registers 6
    .parameter "sentIntent"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 4799
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@3
    const-string v3, "sms_over_ims_in_lte_single_mode"

    #@5
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8
    move-result v2

    #@9
    if-ne v1, v2, :cond_81

    #@b
    .line 4800
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "checkAvailableToSend(), usim = "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    new-instance v3, Lcom/android/internal/telephony/uicc/UsimService;

    #@18
    invoke-direct {v3}, Lcom/android/internal/telephony/uicc/UsimService;-><init>()V

    #@1b
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/UsimService;->getUsimType()I

    #@1e
    move-result v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, " roaming = "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@30
    move-result v3

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    const-string v3, " servicestate = "

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3d
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I

    #@44
    move-result v3

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    const-string v3, " ims = "

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->isIms()Z

    #@52
    move-result v3

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5e
    .line 4804
    new-instance v2, Lcom/android/internal/telephony/uicc/UsimService;

    #@60
    invoke-direct {v2}, Lcom/android/internal/telephony/uicc/UsimService;-><init>()V

    #@63
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UsimService;->getUsimType()I

    #@66
    move-result v2

    #@67
    const/4 v3, 0x5

    #@68
    if-ne v2, v3, :cond_81

    #@6a
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6d
    move-result-object v2

    #@6e
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@71
    move-result v2

    #@72
    if-nez v2, :cond_81

    #@74
    .line 4806
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->isIms()Z

    #@77
    move-result v2

    #@78
    if-nez v2, :cond_81

    #@7a
    .line 4807
    if-eqz p1, :cond_80

    #@7c
    .line 4809
    const/4 v1, 0x1

    #@7d
    :try_start_7d
    invoke-virtual {p1, v1}, Landroid/app/PendingIntent;->send(I)V
    :try_end_80
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_7d .. :try_end_80} :catch_82

    #@80
    .line 4814
    :cond_80
    :goto_80
    const/4 v1, 0x0

    #@81
    .line 4818
    :cond_81
    return v1

    #@82
    .line 4810
    :catch_82
    move-exception v0

    #@83
    .line 4811
    .local v0, ex:Landroid/app/PendingIntent$CanceledException;
    const-string v1, "checkAvailableToSend(), failed to send error result"

    #@85
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@88
    goto :goto_80
.end method

.method private completeProcessMessageSegment(Lcom/android/internal/telephony/SmsMessageBase;)I
    .registers 26
    .parameter "lastSms"

    #@0
    .prologue
    .line 4667
    const/4 v8, 0x0

    #@1
    .line 4668
    .local v8, cursor:Landroid/database/Cursor;
    const/16 v18, 0x0

    #@3
    check-cast v18, [[B

    #@5
    .line 4669
    .local v18, pdus:[[B
    const/4 v13, 0x0

    #@6
    .line 4670
    .local v13, index:I
    const/16 v16, 0x0

    #@8
    .line 4672
    .local v16, msg:Landroid/telephony/SmsMessage;
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/SMSDispatcher;->isSegmentedPDU(Lcom/android/internal/telephony/SmsMessageBase;)Z

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_10

    #@e
    .line 4673
    const/4 v2, 0x2

    #@f
    .line 4748
    :cond_f
    :goto_f
    return v2

    #@10
    .line 4675
    :cond_10
    new-instance v23, Ljava/lang/StringBuilder;

    #@12
    const-string v2, "address ="

    #@14
    move-object/from16 v0, v23

    #@16
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@19
    .line 4677
    .local v23, where:Ljava/lang/StringBuilder;
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getMessageBody()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    move-object/from16 v0, p0

    #@1f
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/SMSDispatcher;->getPaginationIndicator(Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;

    #@22
    move-result-object v19

    #@23
    .line 4678
    .local v19, pi:Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;
    if-nez v19, :cond_27

    #@25
    .line 4679
    const/4 v2, 0x2

    #@26
    goto :goto_f

    #@27
    .line 4682
    :cond_27
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->getSequence()I

    #@2a
    move-result v20

    #@2b
    .line 4683
    .local v20, sequence:I
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->getTotalCount()I

    #@2e
    move-result v22

    #@2f
    .line 4685
    .local v22, totalCount:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "\'"

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    const-string v3, "\'"

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    move-object/from16 v0, v23

    #@4e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    .line 4686
    const-string v2, " AND ( totalCount ="

    #@53
    move-object/from16 v0, v23

    #@55
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    .line 4687
    new-instance v2, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    const-string v3, " )"

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    move-object/from16 v0, v23

    #@71
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    .line 4690
    :try_start_74
    move-object/from16 v0, p0

    #@76
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@78
    move-object/from16 v0, p0

    #@7a
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSegmentUri:Landroid/net/Uri;

    #@7c
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher$Segment;->PROJECTION:[Ljava/lang/String;

    #@7e
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v5

    #@82
    const/4 v6, 0x0

    #@83
    const-string v7, "sequence"

    #@85
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_88
    .catchall {:try_start_74 .. :try_end_88} :catchall_136
    .catch Landroid/database/SQLException; {:try_start_74 .. :try_end_88} :catch_17d

    #@88
    move-result-object v8

    #@89
    .line 4691
    if-nez v8, :cond_93

    #@8b
    .line 4692
    const/4 v2, 0x2

    #@8c
    .line 4723
    if-eqz v8, :cond_f

    #@8e
    .line 4724
    :goto_8e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@91
    goto/16 :goto_f

    #@93
    .line 4694
    :cond_93
    :try_start_93
    const-string v2, "pdu"

    #@95
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@98
    move-result v17

    #@99
    .line 4695
    .local v17, pduColumn:I
    const-string v2, "time"

    #@9b
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@9e
    move-result v21

    #@9f
    .line 4696
    .local v21, timeColumn:I
    const-wide/16 v10, -0x1

    #@a1
    .line 4698
    .local v10, firstTime:J
    move/from16 v0, v22

    #@a3
    new-array v0, v0, [[B

    #@a5
    move-object/from16 v18, v0
    :try_end_a7
    .catchall {:try_start_93 .. :try_end_a7} :catchall_136
    .catch Landroid/database/SQLException; {:try_start_93 .. :try_end_a7} :catch_17d

    #@a7
    .line 4700
    const/4 v2, 0x1

    #@a8
    move/from16 v0, v20

    #@aa
    if-ne v0, v2, :cond_180

    #@ac
    .line 4701
    add-int/lit8 v14, v13, 0x1

    #@ae
    .end local v13           #index:I
    .local v14, index:I
    :try_start_ae
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getPdu()[B

    #@b1
    move-result-object v2

    #@b2
    aput-object v2, v18, v13

    #@b4
    .line 4703
    :cond_b4
    :goto_b4
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@b7
    move-result v2

    #@b8
    if-eqz v2, :cond_f8

    #@ba
    .line 4704
    const-wide/16 v2, -0x1

    #@bc
    cmp-long v2, v10, v2

    #@be
    if-nez v2, :cond_c6

    #@c0
    .line 4705
    move/from16 v0, v21

    #@c2
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    #@c5
    move-result-wide v10

    #@c6
    .line 4707
    :cond_c6
    move/from16 v0, v21

    #@c8
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_cb
    .catchall {:try_start_ae .. :try_end_cb} :catchall_17a
    .catch Landroid/database/SQLException; {:try_start_ae .. :try_end_cb} :catch_ed

    #@cb
    move-result-wide v2

    #@cc
    cmp-long v2, v2, v10

    #@ce
    if-nez v2, :cond_b4

    #@d0
    .line 4708
    add-int/lit8 v13, v14, 0x1

    #@d2
    .end local v14           #index:I
    .restart local v13       #index:I
    :try_start_d2
    move/from16 v0, v17

    #@d4
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@d7
    move-result-object v2

    #@d8
    invoke-static {v2}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@db
    move-result-object v2

    #@dc
    aput-object v2, v18, v14
    :try_end_de
    .catchall {:try_start_d2 .. :try_end_de} :catchall_136
    .catch Landroid/database/SQLException; {:try_start_d2 .. :try_end_de} :catch_17d

    #@de
    .line 4709
    add-int/lit8 v2, v13, 0x1

    #@e0
    move/from16 v0, v20

    #@e2
    if-ne v0, v2, :cond_180

    #@e4
    .line 4710
    add-int/lit8 v14, v13, 0x1

    #@e6
    .end local v13           #index:I
    .restart local v14       #index:I
    :try_start_e6
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getPdu()[B

    #@e9
    move-result-object v2

    #@ea
    aput-object v2, v18, v13
    :try_end_ec
    .catchall {:try_start_e6 .. :try_end_ec} :catchall_17a
    .catch Landroid/database/SQLException; {:try_start_e6 .. :try_end_ec} :catch_ed

    #@ec
    goto :goto_b4

    #@ed
    .line 4719
    :catch_ed
    move-exception v9

    #@ee
    move v13, v14

    #@ef
    .line 4720
    .end local v10           #firstTime:J
    .end local v14           #index:I
    .end local v17           #pduColumn:I
    .end local v21           #timeColumn:I
    .local v9, e:Landroid/database/SQLException;
    .restart local v13       #index:I
    :goto_ef
    :try_start_ef
    const-string v2, "completeProcessMessageSegment(), query exception catch"

    #@f1
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_f4
    .catchall {:try_start_ef .. :try_end_f4} :catchall_136

    #@f4
    .line 4721
    const/4 v2, 0x2

    #@f5
    .line 4723
    if-eqz v8, :cond_f

    #@f7
    goto :goto_8e

    #@f8
    .line 4714
    .end local v9           #e:Landroid/database/SQLException;
    .end local v13           #index:I
    .restart local v10       #firstTime:J
    .restart local v14       #index:I
    .restart local v17       #pduColumn:I
    .restart local v21       #timeColumn:I
    :cond_f8
    :try_start_f8
    const-string v2, " AND "

    #@fa
    move-object/from16 v0, v23

    #@fc
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    .line 4715
    const-string v2, "time ="

    #@101
    move-object/from16 v0, v23

    #@103
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    .line 4716
    move-object/from16 v0, v23

    #@108
    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@10b
    .line 4718
    move-object/from16 v0, p0

    #@10d
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@10f
    move-object/from16 v0, p0

    #@111
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSegmentUri:Landroid/net/Uri;

    #@113
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v4

    #@117
    const/4 v5, 0x0

    #@118
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_11b
    .catchall {:try_start_f8 .. :try_end_11b} :catchall_17a
    .catch Landroid/database/SQLException; {:try_start_f8 .. :try_end_11b} :catch_ed

    #@11b
    .line 4723
    if-eqz v8, :cond_120

    #@11d
    .line 4724
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@120
    .line 4728
    :cond_120
    const/4 v12, 0x0

    #@121
    .local v12, i:I
    :goto_121
    move/from16 v0, v22

    #@123
    if-ge v12, v0, :cond_16a

    #@125
    .line 4729
    aget-object v2, v18, v12

    #@127
    invoke-static {v2}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    #@12a
    move-result-object v16

    #@12b
    .line 4730
    if-nez v16, :cond_13d

    #@12d
    .line 4731
    const-string v2, "completeProcessMessageSegment(), some PDU is broken."

    #@12f
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@132
    .line 4732
    const/4 v2, 0x2

    #@133
    move v13, v14

    #@134
    .end local v14           #index:I
    .restart local v13       #index:I
    goto/16 :goto_f

    #@136
    .line 4723
    .end local v10           #firstTime:J
    .end local v12           #i:I
    .end local v17           #pduColumn:I
    .end local v21           #timeColumn:I
    :catchall_136
    move-exception v2

    #@137
    :goto_137
    if-eqz v8, :cond_13c

    #@139
    .line 4724
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@13c
    .line 4723
    :cond_13c
    throw v2

    #@13d
    .line 4735
    .end local v13           #index:I
    .restart local v10       #firstTime:J
    .restart local v12       #i:I
    .restart local v14       #index:I
    .restart local v17       #pduColumn:I
    .restart local v21       #timeColumn:I
    :cond_13d
    invoke-virtual/range {v16 .. v16}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@140
    move-result-object v2

    #@141
    move-object/from16 v0, p0

    #@143
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/SMSDispatcher;->getPaginationIndicator(Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;

    #@146
    move-result-object v19

    #@147
    .line 4736
    if-nez v19, :cond_14d

    #@149
    .line 4737
    const/4 v2, 0x2

    #@14a
    move v13, v14

    #@14b
    .end local v14           #index:I
    .restart local v13       #index:I
    goto/16 :goto_f

    #@14d
    .line 4740
    .end local v13           #index:I
    .restart local v14       #index:I
    :cond_14d
    invoke-virtual/range {v16 .. v16}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@150
    move-result-object v2

    #@151
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->getLength()I

    #@154
    move-result v3

    #@155
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@158
    move-result-object v15

    #@159
    .line 4741
    .local v15, messageBody:Ljava/lang/String;
    move-object/from16 v0, v16

    #@15b
    invoke-virtual {v0, v15}, Landroid/telephony/SmsMessage;->replaceMessageBody(Ljava/lang/String;)Z

    #@15e
    move-result v2

    #@15f
    if-eqz v2, :cond_167

    #@161
    .line 4742
    invoke-virtual/range {v16 .. v16}, Landroid/telephony/SmsMessage;->getPdu()[B

    #@164
    move-result-object v2

    #@165
    aput-object v2, v18, v12

    #@167
    .line 4728
    :cond_167
    add-int/lit8 v12, v12, 0x1

    #@169
    goto :goto_121

    #@16a
    .line 4745
    .end local v15           #messageBody:Ljava/lang/String;
    :cond_16a
    move-object/from16 v0, p0

    #@16c
    move-object/from16 v1, v18

    #@16e
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[B)V

    #@171
    .line 4746
    const-string v2, "completeProcessMessageSegment(), All segments are reassembled"

    #@173
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@176
    .line 4748
    const/4 v2, -0x1

    #@177
    move v13, v14

    #@178
    .end local v14           #index:I
    .restart local v13       #index:I
    goto/16 :goto_f

    #@17a
    .line 4723
    .end local v12           #i:I
    .end local v13           #index:I
    .restart local v14       #index:I
    :catchall_17a
    move-exception v2

    #@17b
    move v13, v14

    #@17c
    .end local v14           #index:I
    .restart local v13       #index:I
    goto :goto_137

    #@17d
    .line 4719
    .end local v10           #firstTime:J
    .end local v17           #pduColumn:I
    .end local v21           #timeColumn:I
    :catch_17d
    move-exception v9

    #@17e
    goto/16 :goto_ef

    #@180
    .restart local v10       #firstTime:J
    .restart local v17       #pduColumn:I
    .restart local v21       #timeColumn:I
    :cond_180
    move v14, v13

    #@181
    .end local v13           #index:I
    .restart local v14       #index:I
    goto/16 :goto_b4
.end method

.method private createWakelock()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 854
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "power"

    #@5
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/os/PowerManager;

    #@b
    .line 855
    .local v0, pm:Landroid/os/PowerManager;
    const-string v1, "SMSDispatcher"

    #@d
    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@10
    move-result-object v1

    #@11
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@13
    .line 856
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@15
    invoke-virtual {v1, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@18
    .line 857
    return-void
.end method

.method private denyIfQueueLimitReached(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)Z
    .registers 6
    .parameter "tracker"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 3239
    iget v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingTrackerCount:I

    #@3
    sget v3, Lcom/android/internal/telephony/SMSDispatcher;->MO_MSG_QUEUE_LIMIT:I

    #@5
    if-lt v2, v3, :cond_25

    #@7
    .line 3242
    if-eqz p1, :cond_14

    #@9
    :try_start_9
    iget-object v2, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@b
    if-eqz v2, :cond_14

    #@d
    .line 3243
    iget-object v2, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@f
    const/4 v3, 0x5

    #@10
    invoke-virtual {v2, v3}, Landroid/app/PendingIntent;->send(I)V

    #@13
    .line 3254
    :goto_13
    return v1

    #@14
    .line 3245
    :cond_14
    const-string v2, "SMS"

    #@16
    const-string v3, "tracker is null or mSentIntent is null"

    #@18
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1b
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_9 .. :try_end_1b} :catch_1c

    #@1b
    goto :goto_13

    #@1c
    .line 3248
    :catch_1c
    move-exception v0

    #@1d
    .line 3249
    .local v0, ex:Landroid/app/PendingIntent$CanceledException;
    const-string v2, "SMS"

    #@1f
    const-string v3, "failed to send back RESULT_ERROR_LIMIT_EXCEEDED"

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_13

    #@25
    .line 3253
    .end local v0           #ex:Landroid/app/PendingIntent$CanceledException;
    :cond_25
    iget v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingTrackerCount:I

    #@27
    add-int/lit8 v1, v1, 0x1

    #@29
    iput v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingTrackerCount:I

    #@2b
    .line 3254
    const/4 v1, 0x0

    #@2c
    goto :goto_13
.end method

.method private dequeueFailedPendingMessage()V
    .registers 5

    #@0
    .prologue
    .line 4059
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 4060
    const/4 v0, 0x0

    #@4
    .line 4061
    .local v0, loopCount:I
    :goto_4
    :try_start_4
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v1

    #@a
    if-lez v1, :cond_39

    #@c
    .line 4062
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@e
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    #@13
    .line 4063
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "dequeueFailedPendingMessage(), Removed Failed message from pending queue. "

    #@1a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@23
    move-result v3

    #@24
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    const-string v3, " left"

    #@2a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@35
    goto :goto_4

    #@36
    .line 4065
    :catchall_36
    move-exception v1

    #@37
    monitor-exit v2
    :try_end_38
    .catchall {:try_start_4 .. :try_end_38} :catchall_36

    #@38
    throw v1

    #@39
    :cond_39
    :try_start_39
    monitor-exit v2
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_36

    #@3a
    .line 4066
    return-void
.end method

.method private enqueueMessageForSending(Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;)V
    .registers 5
    .parameter "message"

    #@0
    .prologue
    .line 4043
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 4044
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8
    .line 4045
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "enqueueMessageForSending(), Added message to the pending queue. Queue size is "

    #@f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v2

    #@19
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@24
    .line 4048
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@29
    move-result v0

    #@2a
    const/4 v2, 0x1

    #@2b
    if-ne v0, v2, :cond_38

    #@2d
    .line 4049
    iget-object v0, p1, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;->sendType:Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;

    #@2f
    sget-object v2, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;->NORMAL:Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;

    #@31
    if-ne v0, v2, :cond_3a

    #@33
    .line 4050
    iget-object v0, p1, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;->tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@35
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/SMSDispatcher;->sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@38
    .line 4055
    :cond_38
    :goto_38
    monitor-exit v1

    #@39
    .line 4056
    return-void

    #@3a
    .line 4052
    :cond_3a
    iget-object v0, p1, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;->tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@3c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/SMSDispatcher;->sendSmsMore(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@3f
    goto :goto_38

    #@40
    .line 4055
    :catchall_40
    move-exception v0

    #@41
    monitor-exit v1
    :try_end_42
    .catchall {:try_start_3 .. :try_end_42} :catchall_40

    #@42
    throw v0
.end method

.method private getAppLabel(Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 8
    .parameter "appPackage"

    #@0
    .prologue
    .line 3263
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v2

    #@6
    .line 3265
    .local v2, pm:Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    #@7
    :try_start_7
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@a
    move-result-object v0

    #@b
    .line 3266
    .local v0, appInfo:Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v0, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    :try_end_e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7 .. :try_end_e} :catch_10

    #@e
    move-result-object p1

    #@f
    .line 3269
    .end local v0           #appInfo:Landroid/content/pm/ApplicationInfo;
    .end local p1
    :goto_f
    return-object p1

    #@10
    .line 3267
    .restart local p1
    :catch_10
    move-exception v1

    #@11
    .line 3268
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "SMS"

    #@13
    new-instance v4, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v5, "PackageManager Name Not Found for package "

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v4

    #@26
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_f
.end method

.method protected static getNextConcatenatedRef()I
    .registers 1

    #@0
    .prologue
    .line 472
    sget v0, Lcom/android/internal/telephony/SMSDispatcher;->sConcatenatedRef:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    sput v0, Lcom/android/internal/telephony/SMSDispatcher;->sConcatenatedRef:I

    #@6
    .line 473
    sget v0, Lcom/android/internal/telephony/SMSDispatcher;->sConcatenatedRef:I

    #@8
    return v0
.end method

.method private getPaginationIndicator(Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;
    .registers 14
    .parameter "body"

    #@0
    .prologue
    const/16 v11, 0x9

    #@2
    const/4 v10, 0x4

    #@3
    const/16 v9, 0x14

    #@5
    const/16 v8, 0xb

    #@7
    const/16 v7, 0x13

    #@9
    .line 4215
    if-eqz p1, :cond_11

    #@b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@e
    move-result v5

    #@f
    if-nez v5, :cond_13

    #@11
    .line 4216
    :cond_11
    const/4 v5, 0x0

    #@12
    .line 4472
    :goto_12
    return-object v5

    #@13
    .line 4218
    :cond_13
    const/4 v0, 0x0

    #@14
    .line 4219
    .local v0, index:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@17
    move-result v1

    #@18
    .line 4220
    .local v1, length:I
    const/4 v3, 0x0

    #@19
    .line 4221
    .local v3, state:I
    new-instance v2, Ljava/lang/String;

    #@1b
    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    #@1e
    .line 4222
    .local v2, sequence:Ljava/lang/String;
    new-instance v4, Ljava/lang/String;

    #@20
    invoke-direct {v4}, Ljava/lang/String;-><init>()V

    #@23
    .line 4224
    .local v4, totalCount:Ljava/lang/String;
    const/16 v5, 0xc

    #@25
    if-le v1, v5, :cond_29

    #@27
    .line 4225
    const/16 v1, 0xc

    #@29
    .line 4228
    :cond_29
    :goto_29
    if-ge v0, v1, :cond_28e

    #@2b
    .line 4229
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@2e
    move-result v5

    #@2f
    sparse-switch v5, :sswitch_data_2b0

    #@32
    .line 4441
    if-ne v3, v8, :cond_27b

    #@34
    .line 4442
    const/16 v3, 0x15

    #@36
    .line 4452
    :cond_36
    :goto_36
    const/4 v5, -0x1

    #@37
    if-ne v3, v5, :cond_28a

    #@39
    .line 4453
    const/4 v5, 0x0

    #@3a
    goto :goto_12

    #@3b
    .line 4231
    :sswitch_3b
    if-nez v3, :cond_3f

    #@3d
    .line 4232
    const/4 v3, 0x1

    #@3e
    goto :goto_36

    #@3f
    .line 4233
    :cond_3f
    if-ne v3, v8, :cond_44

    #@41
    .line 4234
    const/16 v3, 0x15

    #@43
    goto :goto_36

    #@44
    .line 4235
    :cond_44
    if-ne v3, v7, :cond_49

    #@46
    .line 4236
    const/16 v3, 0x15

    #@48
    goto :goto_36

    #@49
    .line 4237
    :cond_49
    if-ne v3, v9, :cond_4e

    #@4b
    .line 4238
    const/16 v3, 0x15

    #@4d
    goto :goto_36

    #@4e
    .line 4240
    :cond_4e
    const/4 v3, -0x1

    #@4f
    .line 4241
    goto :goto_36

    #@50
    .line 4243
    :sswitch_50
    const/4 v5, 0x1

    #@51
    if-ne v3, v5, :cond_55

    #@53
    .line 4244
    const/4 v3, 0x2

    #@54
    goto :goto_36

    #@55
    .line 4245
    :cond_55
    const/4 v5, 0x2

    #@56
    if-ne v3, v5, :cond_5a

    #@58
    .line 4246
    const/4 v3, 0x2

    #@59
    goto :goto_36

    #@5a
    .line 4247
    :cond_5a
    if-ne v3, v10, :cond_5e

    #@5c
    .line 4248
    const/4 v3, 0x5

    #@5d
    goto :goto_36

    #@5e
    .line 4249
    :cond_5e
    const/4 v5, 0x5

    #@5f
    if-ne v3, v5, :cond_63

    #@61
    .line 4250
    const/4 v3, 0x5

    #@62
    goto :goto_36

    #@63
    .line 4251
    :cond_63
    const/4 v5, 0x6

    #@64
    if-ne v3, v5, :cond_68

    #@66
    .line 4252
    const/4 v3, 0x7

    #@67
    goto :goto_36

    #@68
    .line 4253
    :cond_68
    const/4 v5, 0x7

    #@69
    if-ne v3, v5, :cond_6d

    #@6b
    .line 4254
    const/4 v3, 0x7

    #@6c
    goto :goto_36

    #@6d
    .line 4255
    :cond_6d
    if-ne v3, v11, :cond_72

    #@6f
    .line 4256
    const/16 v3, 0xa

    #@71
    goto :goto_36

    #@72
    .line 4257
    :cond_72
    const/16 v5, 0xa

    #@74
    if-ne v3, v5, :cond_79

    #@76
    .line 4258
    const/16 v3, 0xa

    #@78
    goto :goto_36

    #@79
    .line 4260
    :cond_79
    const/16 v5, 0xd

    #@7b
    if-ne v3, v5, :cond_80

    #@7d
    .line 4261
    const/16 v3, 0xe

    #@7f
    goto :goto_36

    #@80
    .line 4262
    :cond_80
    const/16 v5, 0xe

    #@82
    if-ne v3, v5, :cond_87

    #@84
    .line 4263
    const/16 v3, 0xe

    #@86
    goto :goto_36

    #@87
    .line 4264
    :cond_87
    const/16 v5, 0x10

    #@89
    if-ne v3, v5, :cond_8e

    #@8b
    .line 4265
    const/16 v3, 0x11

    #@8d
    goto :goto_36

    #@8e
    .line 4266
    :cond_8e
    const/16 v5, 0x11

    #@90
    if-ne v3, v5, :cond_95

    #@92
    .line 4267
    const/16 v3, 0x11

    #@94
    goto :goto_36

    #@95
    .line 4269
    :cond_95
    if-ne v3, v8, :cond_9a

    #@97
    .line 4270
    const/16 v3, 0x14

    #@99
    goto :goto_36

    #@9a
    .line 4271
    :cond_9a
    if-ne v3, v7, :cond_9f

    #@9c
    .line 4272
    const/16 v3, 0x14

    #@9e
    goto :goto_36

    #@9f
    .line 4273
    :cond_9f
    if-ne v3, v9, :cond_a4

    #@a1
    .line 4274
    const/16 v3, 0x15

    #@a3
    goto :goto_36

    #@a4
    .line 4276
    :cond_a4
    const/16 v5, 0x16

    #@a6
    if-ne v3, v5, :cond_ab

    #@a8
    .line 4277
    const/16 v3, 0x11

    #@aa
    goto :goto_36

    #@ab
    .line 4279
    :cond_ab
    const/4 v3, -0x1

    #@ac
    .line 4280
    goto :goto_36

    #@ad
    .line 4282
    :sswitch_ad
    const/4 v5, 0x1

    #@ae
    if-ne v3, v5, :cond_e1

    #@b0
    .line 4283
    const/4 v3, 0x3

    #@b1
    .line 4324
    :goto_b1
    if-ne v3, v10, :cond_c8

    #@b3
    .line 4325
    new-instance v5, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v5

    #@bc
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@bf
    move-result v6

    #@c0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v5

    #@c4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v2

    #@c8
    .line 4326
    :cond_c8
    if-ne v3, v11, :cond_36

    #@ca
    .line 4327
    new-instance v5, Ljava/lang/StringBuilder;

    #@cc
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@cf
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v5

    #@d3
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@d6
    move-result v6

    #@d7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@da
    move-result-object v5

    #@db
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v4

    #@df
    goto/16 :goto_36

    #@e1
    .line 4284
    :cond_e1
    const/4 v5, 0x2

    #@e2
    if-ne v3, v5, :cond_e6

    #@e4
    .line 4285
    const/4 v3, 0x3

    #@e5
    goto :goto_b1

    #@e6
    .line 4286
    :cond_e6
    const/4 v5, 0x3

    #@e7
    if-ne v3, v5, :cond_eb

    #@e9
    .line 4287
    const/4 v3, 0x3

    #@ea
    goto :goto_b1

    #@eb
    .line 4288
    :cond_eb
    const/4 v5, 0x6

    #@ec
    if-ne v3, v5, :cond_f1

    #@ee
    .line 4289
    const/16 v3, 0x8

    #@f0
    goto :goto_b1

    #@f1
    .line 4290
    :cond_f1
    const/4 v5, 0x7

    #@f2
    if-ne v3, v5, :cond_f7

    #@f4
    .line 4291
    const/16 v3, 0x8

    #@f6
    goto :goto_b1

    #@f7
    .line 4292
    :cond_f7
    const/16 v5, 0x8

    #@f9
    if-ne v3, v5, :cond_fe

    #@fb
    .line 4293
    const/16 v3, 0x8

    #@fd
    goto :goto_b1

    #@fe
    .line 4295
    :cond_fe
    if-ne v3, v10, :cond_102

    #@100
    .line 4296
    const/4 v3, 0x4

    #@101
    goto :goto_b1

    #@102
    .line 4297
    :cond_102
    if-ne v3, v11, :cond_107

    #@104
    .line 4298
    const/16 v3, 0x9

    #@106
    goto :goto_b1

    #@107
    .line 4300
    :cond_107
    if-nez v3, :cond_10c

    #@109
    .line 4301
    const/16 v3, 0xc

    #@10b
    goto :goto_b1

    #@10c
    .line 4302
    :cond_10c
    const/16 v5, 0xc

    #@10e
    if-ne v3, v5, :cond_113

    #@110
    .line 4303
    const/16 v3, 0xc

    #@112
    goto :goto_b1

    #@113
    .line 4304
    :cond_113
    const/16 v5, 0x10

    #@115
    if-ne v3, v5, :cond_11a

    #@117
    .line 4305
    const/16 v3, 0x12

    #@119
    goto :goto_b1

    #@11a
    .line 4306
    :cond_11a
    const/16 v5, 0x11

    #@11c
    if-ne v3, v5, :cond_121

    #@11e
    .line 4307
    const/16 v3, 0x12

    #@120
    goto :goto_b1

    #@121
    .line 4308
    :cond_121
    const/16 v5, 0x12

    #@123
    if-ne v3, v5, :cond_128

    #@125
    .line 4309
    const/16 v3, 0x12

    #@127
    goto :goto_b1

    #@128
    .line 4311
    :cond_128
    if-ne v3, v8, :cond_12d

    #@12a
    .line 4312
    const/16 v3, 0x15

    #@12c
    goto :goto_b1

    #@12d
    .line 4313
    :cond_12d
    if-ne v3, v7, :cond_132

    #@12f
    .line 4314
    const/16 v3, 0x15

    #@131
    goto :goto_b1

    #@132
    .line 4315
    :cond_132
    if-ne v3, v9, :cond_138

    #@134
    .line 4316
    const/16 v3, 0x15

    #@136
    goto/16 :goto_b1

    #@138
    .line 4318
    :cond_138
    const/16 v5, 0x16

    #@13a
    if-ne v3, v5, :cond_140

    #@13c
    .line 4319
    const/16 v3, 0x12

    #@13e
    goto/16 :goto_b1

    #@140
    .line 4321
    :cond_140
    const/4 v3, -0x1

    #@141
    goto/16 :goto_b1

    #@143
    .line 4330
    :sswitch_143
    const/4 v5, 0x1

    #@144
    if-ne v3, v5, :cond_164

    #@146
    .line 4331
    const/4 v3, 0x4

    #@147
    .line 4373
    :goto_147
    if-eq v3, v10, :cond_14d

    #@149
    const/16 v5, 0xd

    #@14b
    if-ne v3, v5, :cond_1cb

    #@14d
    .line 4374
    :cond_14d
    new-instance v5, Ljava/lang/StringBuilder;

    #@14f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@152
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v5

    #@156
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@159
    move-result v6

    #@15a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v5

    #@15e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@161
    move-result-object v2

    #@162
    goto/16 :goto_36

    #@164
    .line 4332
    :cond_164
    const/4 v5, 0x2

    #@165
    if-ne v3, v5, :cond_169

    #@167
    .line 4333
    const/4 v3, 0x4

    #@168
    goto :goto_147

    #@169
    .line 4334
    :cond_169
    const/4 v5, 0x3

    #@16a
    if-ne v3, v5, :cond_16e

    #@16c
    .line 4335
    const/4 v3, 0x4

    #@16d
    goto :goto_147

    #@16e
    .line 4336
    :cond_16e
    if-ne v3, v10, :cond_172

    #@170
    .line 4337
    const/4 v3, 0x4

    #@171
    goto :goto_147

    #@172
    .line 4338
    :cond_172
    const/4 v5, 0x6

    #@173
    if-ne v3, v5, :cond_178

    #@175
    .line 4339
    const/16 v3, 0x9

    #@177
    goto :goto_147

    #@178
    .line 4340
    :cond_178
    const/4 v5, 0x7

    #@179
    if-ne v3, v5, :cond_17e

    #@17b
    .line 4341
    const/16 v3, 0x9

    #@17d
    goto :goto_147

    #@17e
    .line 4342
    :cond_17e
    const/16 v5, 0x8

    #@180
    if-ne v3, v5, :cond_185

    #@182
    .line 4343
    const/16 v3, 0x9

    #@184
    goto :goto_147

    #@185
    .line 4344
    :cond_185
    if-ne v3, v11, :cond_18a

    #@187
    .line 4345
    const/16 v3, 0x9

    #@189
    goto :goto_147

    #@18a
    .line 4347
    :cond_18a
    if-nez v3, :cond_18f

    #@18c
    .line 4348
    const/16 v3, 0xd

    #@18e
    goto :goto_147

    #@18f
    .line 4349
    :cond_18f
    const/16 v5, 0xc

    #@191
    if-ne v3, v5, :cond_196

    #@193
    .line 4350
    const/16 v3, 0xd

    #@195
    goto :goto_147

    #@196
    .line 4351
    :cond_196
    const/16 v5, 0xd

    #@198
    if-ne v3, v5, :cond_19d

    #@19a
    .line 4352
    const/16 v3, 0xd

    #@19c
    goto :goto_147

    #@19d
    .line 4353
    :cond_19d
    const/16 v5, 0x10

    #@19f
    if-ne v3, v5, :cond_1a4

    #@1a1
    .line 4354
    const/16 v3, 0x13

    #@1a3
    goto :goto_147

    #@1a4
    .line 4355
    :cond_1a4
    const/16 v5, 0x11

    #@1a6
    if-ne v3, v5, :cond_1ab

    #@1a8
    .line 4356
    const/16 v3, 0x13

    #@1aa
    goto :goto_147

    #@1ab
    .line 4357
    :cond_1ab
    const/16 v5, 0x12

    #@1ad
    if-ne v3, v5, :cond_1b2

    #@1af
    .line 4358
    const/16 v3, 0x13

    #@1b1
    goto :goto_147

    #@1b2
    .line 4359
    :cond_1b2
    if-ne v3, v7, :cond_1b7

    #@1b4
    .line 4360
    const/16 v3, 0x13

    #@1b6
    goto :goto_147

    #@1b7
    .line 4362
    :cond_1b7
    if-ne v3, v8, :cond_1bc

    #@1b9
    .line 4363
    const/16 v3, 0x15

    #@1bb
    goto :goto_147

    #@1bc
    .line 4364
    :cond_1bc
    if-ne v3, v9, :cond_1c1

    #@1be
    .line 4365
    const/16 v3, 0x15

    #@1c0
    goto :goto_147

    #@1c1
    .line 4367
    :cond_1c1
    const/16 v5, 0x16

    #@1c3
    if-ne v3, v5, :cond_1c8

    #@1c5
    .line 4368
    const/16 v3, 0x13

    #@1c7
    goto :goto_147

    #@1c8
    .line 4370
    :cond_1c8
    const/4 v3, -0x1

    #@1c9
    goto/16 :goto_147

    #@1cb
    .line 4375
    :cond_1cb
    if-eq v3, v11, :cond_1cf

    #@1cd
    if-ne v3, v7, :cond_36

    #@1cf
    .line 4376
    :cond_1cf
    new-instance v5, Ljava/lang/StringBuilder;

    #@1d1
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1d4
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v5

    #@1d8
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@1db
    move-result v6

    #@1dc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v5

    #@1e0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e3
    move-result-object v4

    #@1e4
    goto/16 :goto_36

    #@1e6
    .line 4379
    :sswitch_1e6
    if-ne v3, v10, :cond_1eb

    #@1e8
    .line 4380
    const/4 v3, 0x6

    #@1e9
    goto/16 :goto_36

    #@1eb
    .line 4381
    :cond_1eb
    const/4 v5, 0x5

    #@1ec
    if-ne v3, v5, :cond_1f1

    #@1ee
    .line 4382
    const/4 v3, 0x6

    #@1ef
    goto/16 :goto_36

    #@1f1
    .line 4383
    :cond_1f1
    if-ne v3, v8, :cond_1f7

    #@1f3
    .line 4384
    const/16 v3, 0x15

    #@1f5
    goto/16 :goto_36

    #@1f7
    .line 4385
    :cond_1f7
    if-ne v3, v7, :cond_1fd

    #@1f9
    .line 4386
    const/16 v3, 0x15

    #@1fb
    goto/16 :goto_36

    #@1fd
    .line 4387
    :cond_1fd
    if-ne v3, v9, :cond_203

    #@1ff
    .line 4388
    const/16 v3, 0x15

    #@201
    goto/16 :goto_36

    #@203
    .line 4390
    :cond_203
    const/16 v5, 0xd

    #@205
    if-ne v3, v5, :cond_20b

    #@207
    .line 4391
    const/16 v3, 0x16

    #@209
    goto/16 :goto_36

    #@20b
    .line 4393
    :cond_20b
    const/16 v5, 0xe

    #@20d
    if-ne v3, v5, :cond_213

    #@20f
    .line 4394
    const/16 v3, 0x16

    #@211
    goto/16 :goto_36

    #@213
    .line 4396
    :cond_213
    const/4 v3, -0x1

    #@214
    .line 4397
    goto/16 :goto_36

    #@216
    .line 4399
    :sswitch_216
    if-ne v3, v11, :cond_21c

    #@218
    .line 4400
    const/16 v3, 0xb

    #@21a
    goto/16 :goto_36

    #@21c
    .line 4401
    :cond_21c
    const/16 v5, 0xa

    #@21e
    if-ne v3, v5, :cond_224

    #@220
    .line 4402
    const/16 v3, 0xb

    #@222
    goto/16 :goto_36

    #@224
    .line 4403
    :cond_224
    if-ne v3, v8, :cond_22a

    #@226
    .line 4404
    const/16 v3, 0x15

    #@228
    goto/16 :goto_36

    #@22a
    .line 4405
    :cond_22a
    if-ne v3, v7, :cond_230

    #@22c
    .line 4406
    const/16 v3, 0x15

    #@22e
    goto/16 :goto_36

    #@230
    .line 4407
    :cond_230
    if-ne v3, v9, :cond_236

    #@232
    .line 4408
    const/16 v3, 0x15

    #@234
    goto/16 :goto_36

    #@236
    .line 4410
    :cond_236
    const/4 v3, -0x1

    #@237
    .line 4411
    goto/16 :goto_36

    #@239
    .line 4414
    :sswitch_239
    const/16 v5, 0xd

    #@23b
    if-ne v3, v5, :cond_241

    #@23d
    .line 4415
    const/16 v3, 0xf

    #@23f
    goto/16 :goto_36

    #@241
    .line 4416
    :cond_241
    const/16 v5, 0xe

    #@243
    if-ne v3, v5, :cond_249

    #@245
    .line 4417
    const/16 v3, 0xf

    #@247
    goto/16 :goto_36

    #@249
    .line 4418
    :cond_249
    if-ne v3, v8, :cond_24f

    #@24b
    .line 4419
    const/16 v3, 0x15

    #@24d
    goto/16 :goto_36

    #@24f
    .line 4420
    :cond_24f
    if-ne v3, v7, :cond_255

    #@251
    .line 4421
    const/16 v3, 0x15

    #@253
    goto/16 :goto_36

    #@255
    .line 4422
    :cond_255
    if-ne v3, v9, :cond_25b

    #@257
    .line 4423
    const/16 v3, 0x15

    #@259
    goto/16 :goto_36

    #@25b
    .line 4425
    :cond_25b
    const/4 v3, -0x1

    #@25c
    .line 4426
    goto/16 :goto_36

    #@25e
    .line 4429
    :sswitch_25e
    const/16 v5, 0xf

    #@260
    if-ne v3, v5, :cond_266

    #@262
    .line 4430
    const/16 v3, 0x10

    #@264
    goto/16 :goto_36

    #@266
    .line 4431
    :cond_266
    if-ne v3, v8, :cond_26c

    #@268
    .line 4432
    const/16 v3, 0x15

    #@26a
    goto/16 :goto_36

    #@26c
    .line 4433
    :cond_26c
    if-ne v3, v7, :cond_272

    #@26e
    .line 4434
    const/16 v3, 0x15

    #@270
    goto/16 :goto_36

    #@272
    .line 4435
    :cond_272
    if-ne v3, v9, :cond_278

    #@274
    .line 4436
    const/16 v3, 0x15

    #@276
    goto/16 :goto_36

    #@278
    .line 4438
    :cond_278
    const/4 v3, -0x1

    #@279
    .line 4439
    goto/16 :goto_36

    #@27b
    .line 4443
    :cond_27b
    if-ne v3, v7, :cond_281

    #@27d
    .line 4444
    const/16 v3, 0x15

    #@27f
    goto/16 :goto_36

    #@281
    .line 4445
    :cond_281
    if-ne v3, v9, :cond_287

    #@283
    .line 4446
    const/16 v3, 0x15

    #@285
    goto/16 :goto_36

    #@287
    .line 4448
    :cond_287
    const/4 v3, -0x1

    #@288
    goto/16 :goto_36

    #@28a
    .line 4456
    :cond_28a
    const/16 v5, 0x15

    #@28c
    if-ne v3, v5, :cond_2a9

    #@28e
    .line 4463
    :cond_28e
    if-eq v3, v8, :cond_294

    #@290
    if-eq v3, v7, :cond_294

    #@292
    if-ne v3, v9, :cond_296

    #@294
    .line 4464
    :cond_294
    const/16 v3, 0x15

    #@296
    .line 4467
    :cond_296
    const/16 v5, 0x15

    #@298
    if-ne v3, v5, :cond_2ad

    #@29a
    .line 4469
    new-instance v5, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;

    #@29c
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@29f
    move-result v6

    #@2a0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2a3
    move-result v7

    #@2a4
    invoke-direct {v5, v6, v7, v0}, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;-><init>(III)V

    #@2a7
    goto/16 :goto_12

    #@2a9
    .line 4460
    :cond_2a9
    add-int/lit8 v0, v0, 0x1

    #@2ab
    goto/16 :goto_29

    #@2ad
    .line 4472
    :cond_2ad
    const/4 v5, 0x0

    #@2ae
    goto/16 :goto_12

    #@2b0
    .line 4229
    :sswitch_data_2b0
    .sparse-switch
        0x20 -> :sswitch_50
        0x28 -> :sswitch_3b
        0x29 -> :sswitch_216
        0x2f -> :sswitch_1e6
        0x30 -> :sswitch_ad
        0x31 -> :sswitch_143
        0x32 -> :sswitch_143
        0x33 -> :sswitch_143
        0x34 -> :sswitch_143
        0x35 -> :sswitch_143
        0x36 -> :sswitch_143
        0x37 -> :sswitch_143
        0x38 -> :sswitch_143
        0x39 -> :sswitch_143
        0x46 -> :sswitch_25e
        0x4f -> :sswitch_239
        0x5b -> :sswitch_3b
        0x5d -> :sswitch_216
        0x66 -> :sswitch_25e
        0x6f -> :sswitch_239
        0x7b -> :sswitch_3b
        0x7d -> :sswitch_216
    .end sparse-switch
.end method

.method private getSMSInboxMessageCount()I
    .registers 10

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3510
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v1

    #@7
    .line 3511
    .local v1, resolver:Landroid/content/ContentResolver;
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@9
    sget-object v2, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@b
    const-string v4, "(type = 1)"

    #@d
    move-object v5, v3

    #@e
    move-object v6, v3

    #@f
    invoke-static/range {v0 .. v6}, Landroid/database/sqlite/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@12
    move-result-object v8

    #@13
    .line 3515
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_17

    #@15
    .line 3516
    const/4 v7, 0x0

    #@16
    .line 3521
    :goto_16
    return v7

    #@17
    .line 3519
    :cond_17
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    #@1a
    move-result v7

    #@1b
    .line 3520
    .local v7, count:I
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@1e
    goto :goto_16
.end method

.method public static getSmsIsRoaming()Z
    .registers 2

    #@0
    .prologue
    .line 4787
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "getSmsIsRoaming(), get value = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-boolean v1, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitIsRoaming:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@18
    .line 4789
    sget-boolean v0, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitIsRoaming:Z

    #@1a
    return v0
.end method

.method protected static handleNotInService(ILandroid/app/PendingIntent;)V
    .registers 3
    .parameter "ss"
    .parameter "sentIntent"

    #@0
    .prologue
    .line 1317
    if-eqz p1, :cond_9

    #@2
    .line 1319
    const/4 v0, 0x3

    #@3
    if-ne p0, v0, :cond_a

    #@5
    .line 1320
    const/4 v0, 0x2

    #@6
    :try_start_6
    invoke-virtual {p1, v0}, Landroid/app/PendingIntent;->send(I)V

    #@9
    .line 1326
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1322
    :cond_a
    const/4 v0, 0x4

    #@b
    invoke-virtual {p1, v0}, Landroid/app/PendingIntent;->send(I)V
    :try_end_e
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_6 .. :try_end_e} :catch_f

    #@e
    goto :goto_9

    #@f
    .line 1324
    :catch_f
    move-exception v0

    #@10
    goto :goto_9
.end method

.method private insertMessageSegment(Lcom/android/internal/telephony/SmsMessageBase;)J
    .registers 21
    .parameter "sms"

    #@0
    .prologue
    .line 4516
    new-instance v17, Landroid/content/ContentValues;

    #@2
    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 4517
    .local v17, values:Landroid/content/ContentValues;
    const/4 v7, 0x0

    #@6
    .line 4518
    .local v7, cursor:Landroid/database/Cursor;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9
    move-result-wide v9

    #@a
    .line 4520
    .local v9, firstTime:J
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getMessageBody()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    move-object/from16 v0, p0

    #@10
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->getPaginationIndicator(Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;

    #@13
    move-result-object v13

    #@14
    .line 4521
    .local v13, pi:Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;
    if-nez v13, :cond_18

    #@16
    move-wide v11, v9

    #@17
    .line 4567
    .end local v9           #firstTime:J
    .local v11, firstTime:J
    :goto_17
    return-wide v11

    #@18
    .line 4525
    .end local v11           #firstTime:J
    .restart local v9       #firstTime:J
    :cond_18
    invoke-virtual {v13}, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->getSequence()I

    #@1b
    move-result v14

    #@1c
    .line 4526
    .local v14, sequence:I
    invoke-virtual {v13}, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->getTotalCount()I

    #@1f
    move-result v16

    #@20
    .line 4528
    .local v16, totalCount:I
    new-instance v18, Ljava/lang/StringBuilder;

    #@22
    const-string v1, "address ="

    #@24
    move-object/from16 v0, v18

    #@26
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@29
    .line 4529
    .local v18, where:Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v2, "\'"

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    const-string v2, "\'"

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    move-object/from16 v0, v18

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    .line 4530
    const-string v1, " AND ( totalCount ="

    #@4d
    move-object/from16 v0, v18

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    .line 4531
    new-instance v1, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v1

    #@5f
    const-string v2, " )"

    #@61
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v1

    #@65
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    move-object/from16 v0, v18

    #@6b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    .line 4534
    :try_start_6e
    move-object/from16 v0, p0

    #@70
    iget-object v1, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@72
    move-object/from16 v0, p0

    #@74
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSegmentUri:Landroid/net/Uri;

    #@76
    sget-object v3, Lcom/android/internal/telephony/SMSDispatcher$Segment;->PROJECTION:[Ljava/lang/String;

    #@78
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    const/4 v5, 0x0

    #@7d
    const-string v6, "sequence"

    #@7f
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_82
    .catchall {:try_start_6e .. :try_end_82} :catchall_106
    .catch Landroid/database/SQLException; {:try_start_6e .. :try_end_82} :catch_f8

    #@82
    move-result-object v7

    #@83
    .line 4535
    if-nez v7, :cond_8c

    #@85
    .line 4548
    if-eqz v7, :cond_8a

    #@87
    .line 4549
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@8a
    :cond_8a
    move-wide v11, v9

    #@8b
    .line 4536
    .end local v9           #firstTime:J
    .restart local v11       #firstTime:J
    goto :goto_17

    #@8c
    .line 4538
    .end local v11           #firstTime:J
    .restart local v9       #firstTime:J
    :cond_8c
    :try_start_8c
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@8f
    move-result v1

    #@90
    if-lez v1, :cond_a2

    #@92
    .line 4539
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@95
    move-result v1

    #@96
    if-eqz v1, :cond_a2

    #@98
    .line 4540
    const-string v1, "time"

    #@9a
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@9d
    move-result v15

    #@9e
    .line 4541
    .local v15, timeColumn:I
    invoke-interface {v7, v15}, Landroid/database/Cursor;->getLong(I)J
    :try_end_a1
    .catchall {:try_start_8c .. :try_end_a1} :catchall_106
    .catch Landroid/database/SQLException; {:try_start_8c .. :try_end_a1} :catch_f8

    #@a1
    move-result-wide v9

    #@a2
    .line 4548
    .end local v15           #timeColumn:I
    :cond_a2
    if-eqz v7, :cond_a7

    #@a4
    .line 4549
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@a7
    .line 4553
    :cond_a7
    const-string v1, "time"

    #@a9
    new-instance v2, Ljava/lang/Long;

    #@ab
    invoke-direct {v2, v9, v10}, Ljava/lang/Long;-><init>(J)V

    #@ae
    move-object/from16 v0, v17

    #@b0
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@b3
    .line 4554
    const-string v1, "sequence"

    #@b5
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b8
    move-result-object v2

    #@b9
    move-object/from16 v0, v17

    #@bb
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@be
    .line 4555
    const-string v1, "totalCount"

    #@c0
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c3
    move-result-object v2

    #@c4
    move-object/from16 v0, v17

    #@c6
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@c9
    .line 4556
    const-string v1, "address"

    #@cb
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@ce
    move-result-object v2

    #@cf
    move-object/from16 v0, v17

    #@d1
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d4
    .line 4557
    const-string v1, "pdu"

    #@d6
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getPdu()[B

    #@d9
    move-result-object v2

    #@da
    invoke-static {v2}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@dd
    move-result-object v2

    #@de
    move-object/from16 v0, v17

    #@e0
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@e3
    .line 4560
    :try_start_e3
    move-object/from16 v0, p0

    #@e5
    iget-object v1, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@e7
    move-object/from16 v0, p0

    #@e9
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSegmentUri:Landroid/net/Uri;

    #@eb
    move-object/from16 v0, v17

    #@ed
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@f0
    .line 4561
    const-string v1, "insertMessageSegment(), trying to insert a segment"

    #@f2
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_f5
    .catchall {:try_start_e3 .. :try_end_f5} :catchall_114
    .catch Landroid/database/SQLException; {:try_start_e3 .. :try_end_f5} :catch_10d

    #@f5
    :goto_f5
    move-wide v11, v9

    #@f6
    .line 4567
    .end local v9           #firstTime:J
    .restart local v11       #firstTime:J
    goto/16 :goto_17

    #@f8
    .line 4544
    .end local v11           #firstTime:J
    .restart local v9       #firstTime:J
    :catch_f8
    move-exception v8

    #@f9
    .line 4545
    .local v8, e:Landroid/database/SQLException;
    :try_start_f9
    const-string v1, "insertMessageSegment(), query exception catch"

    #@fb
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_fe
    .catchall {:try_start_f9 .. :try_end_fe} :catchall_106

    #@fe
    .line 4548
    if-eqz v7, :cond_103

    #@100
    .line 4549
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@103
    :cond_103
    move-wide v11, v9

    #@104
    .line 4546
    .end local v9           #firstTime:J
    .restart local v11       #firstTime:J
    goto/16 :goto_17

    #@106
    .line 4548
    .end local v8           #e:Landroid/database/SQLException;
    .end local v11           #firstTime:J
    .restart local v9       #firstTime:J
    :catchall_106
    move-exception v1

    #@107
    if-eqz v7, :cond_10c

    #@109
    .line 4549
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@10c
    .line 4548
    :cond_10c
    throw v1

    #@10d
    .line 4562
    :catch_10d
    move-exception v8

    #@10e
    .line 4563
    .restart local v8       #e:Landroid/database/SQLException;
    :try_start_10e
    const-string v1, "insertMessageSegment(), query exception catch"

    #@110
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_113
    .catchall {:try_start_10e .. :try_end_113} :catchall_114

    #@113
    goto :goto_f5

    #@114
    .line 4564
    .end local v8           #e:Landroid/database/SQLException;
    :catchall_114
    move-exception v1

    #@115
    throw v1
.end method

.method private isLastSegmentedPDU(Lcom/android/internal/telephony/SmsMessageBase;)Z
    .registers 16
    .parameter "sms"

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    const/4 v12, 0x0

    #@2
    .line 4477
    const/4 v8, 0x0

    #@3
    .line 4478
    .local v8, nCount:I
    const/4 v6, 0x0

    #@4
    .line 4479
    .local v6, cursor:Landroid/database/Cursor;
    new-instance v11, Ljava/lang/StringBuilder;

    #@6
    const-string v0, "address ="

    #@8
    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@b
    .line 4481
    .local v11, where:Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v1, "\'"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {p1}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, "\'"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    .line 4483
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->isSegmentedPDU(Lcom/android/internal/telephony/SmsMessageBase;)Z

    #@2e
    move-result v0

    #@2f
    if-nez v0, :cond_33

    #@31
    move v0, v12

    #@32
    .line 4512
    :goto_32
    return v0

    #@33
    .line 4486
    :cond_33
    invoke-virtual {p1}, Lcom/android/internal/telephony/SmsMessageBase;->getMessageBody()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/SMSDispatcher;->getPaginationIndicator(Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;

    #@3a
    move-result-object v9

    #@3b
    .line 4487
    .local v9, pi:Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;
    if-nez v9, :cond_3f

    #@3d
    move v0, v12

    #@3e
    .line 4488
    goto :goto_32

    #@3f
    .line 4491
    :cond_3f
    invoke-virtual {v9}, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->getTotalCount()I

    #@42
    move-result v10

    #@43
    .line 4493
    .local v10, totalCount:I
    const-string v0, " AND ( totalCount ="

    #@45
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    .line 4494
    new-instance v0, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    const-string v1, " )"

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v0

    #@5f
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    .line 4497
    :try_start_62
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@64
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSegmentUri:Landroid/net/Uri;

    #@66
    sget-object v2, Lcom/android/internal/telephony/SMSDispatcher$Segment;->PROJECTION:[Ljava/lang/String;

    #@68
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    const/4 v4, 0x0

    #@6d
    const/4 v5, 0x0

    #@6e
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@71
    move-result-object v6

    #@72
    .line 4498
    if-eqz v6, :cond_78

    #@74
    .line 4499
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_77
    .catchall {:try_start_62 .. :try_end_77} :catchall_92
    .catch Landroid/database/SQLException; {:try_start_62 .. :try_end_77} :catch_85

    #@77
    move-result v8

    #@78
    .line 4505
    :cond_78
    if-eqz v6, :cond_7d

    #@7a
    .line 4506
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7d
    .line 4509
    :cond_7d
    if-le v10, v13, :cond_99

    #@7f
    add-int/lit8 v0, v10, -0x1

    #@81
    if-ne v8, v0, :cond_99

    #@83
    move v0, v13

    #@84
    .line 4510
    goto :goto_32

    #@85
    .line 4501
    :catch_85
    move-exception v7

    #@86
    .line 4502
    .local v7, e:Landroid/database/SQLException;
    :try_start_86
    const-string v0, "isLastSegmentedPDU(), query exception catch"

    #@88
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_8b
    .catchall {:try_start_86 .. :try_end_8b} :catchall_92

    #@8b
    .line 4505
    if-eqz v6, :cond_90

    #@8d
    .line 4506
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@90
    :cond_90
    move v0, v12

    #@91
    .line 4503
    goto :goto_32

    #@92
    .line 4505
    .end local v7           #e:Landroid/database/SQLException;
    :catchall_92
    move-exception v0

    #@93
    if-eqz v6, :cond_98

    #@95
    .line 4506
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@98
    .line 4505
    :cond_98
    throw v0

    #@99
    :cond_99
    move v0, v12

    #@9a
    .line 4512
    goto :goto_32
.end method

.method private isLowMemory(Landroid/database/sqlite/SQLiteException;)Z
    .registers 12
    .parameter "e"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 4928
    instance-of v6, p1, Landroid/database/sqlite/SQLiteFullException;

    #@3
    if-eqz v6, :cond_6

    #@5
    .line 4937
    :cond_5
    :goto_5
    return v5

    #@6
    .line 4930
    :cond_6
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    #@9
    move-result-object v6

    #@a
    const-string v7, "no transaction is active"

    #@c
    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@f
    move-result v6

    #@10
    if-eqz v6, :cond_2b

    #@12
    .line 4931
    new-instance v4, Landroid/os/StatFs;

    #@14
    const-string v6, "/data"

    #@16
    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    #@19
    .line 4932
    .local v4, stat:Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    #@1c
    move-result v6

    #@1d
    int-to-long v0, v6

    #@1e
    .line 4933
    .local v0, availBlocks:J
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    #@21
    move-result v6

    #@22
    int-to-long v2, v6

    #@23
    .line 4934
    .local v2, blockSize:J
    mul-long v6, v0, v2

    #@25
    const-wide/16 v8, 0x2000

    #@27
    cmp-long v6, v6, v8

    #@29
    if-ltz v6, :cond_5

    #@2b
    .line 4937
    .end local v0           #availBlocks:J
    .end local v2           #blockSize:J
    .end local v4           #stat:Landroid/os/StatFs;
    :cond_2b
    const/4 v5, 0x0

    #@2c
    goto :goto_5
.end method

.method private notifyAndAcknowledgeLastIncomingSms(ZILandroid/os/Message;)V
    .registers 8
    .parameter "success"
    .parameter "result"
    .parameter "response"

    #@0
    .prologue
    .line 3498
    if-nez p1, :cond_1c

    #@2
    .line 3500
    new-instance v0, Landroid/content/Intent;

    #@4
    const-string v1, "android.provider.Telephony.SMS_REJECTED"

    #@6
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9
    .line 3501
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "result"

    #@b
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@e
    .line 3502
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@10
    const-wide/16 v2, 0x1388

    #@12
    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@15
    .line 3503
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@17
    const-string v2, "android.permission.RECEIVE_SMS"

    #@19
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@1c
    .line 3505
    .end local v0           #intent:Landroid/content/Intent;
    :cond_1c
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/telephony/SMSDispatcher;->acknowledgeLastIncomingSms(ZILandroid/os/Message;)V

    #@1f
    .line 3506
    return-void
.end method

.method private processNextPendingMessage()V
    .registers 5

    #@0
    .prologue
    .line 4020
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 4022
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v1

    #@9
    if-lez v1, :cond_51

    #@b
    .line 4023
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@d
    const/4 v3, 0x0

    #@e
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@11
    .line 4024
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "processNextPendingMessage(), Removed message from pending queue. "

    #@18
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@21
    move-result v3

    #@22
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v3, " left"

    #@28
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@33
    .line 4030
    :goto_33
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@35
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@38
    move-result v1

    #@39
    if-lez v1, :cond_4f

    #@3b
    .line 4031
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingMessagesList:Ljava/util/ArrayList;

    #@3d
    const/4 v3, 0x0

    #@3e
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@41
    move-result-object v0

    #@42
    check-cast v0, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;

    #@44
    .line 4033
    .local v0, message:Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;
    iget-object v1, v0, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;->sendType:Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;

    #@46
    sget-object v3, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;->NORMAL:Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;

    #@48
    if-ne v1, v3, :cond_5a

    #@4a
    .line 4034
    iget-object v1, v0, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;->tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@4c
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@4f
    .line 4039
    .end local v0           #message:Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;
    :cond_4f
    :goto_4f
    monitor-exit v2

    #@50
    .line 4040
    return-void

    #@51
    .line 4026
    :cond_51
    const-string v1, "processNextPendingMessage(), Pending messages list consistency failure detected!"

    #@53
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@56
    goto :goto_33

    #@57
    .line 4039
    :catchall_57
    move-exception v1

    #@58
    monitor-exit v2
    :try_end_59
    .catchall {:try_start_3 .. :try_end_59} :catchall_57

    #@59
    throw v1

    #@5a
    .line 4036
    .restart local v0       #message:Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;
    :cond_5a
    :try_start_5a
    iget-object v1, v0, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;->tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@5c
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->sendSmsMore(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    :try_end_5f
    .catchall {:try_start_5a .. :try_end_5f} :catchall_57

    #@5f
    goto :goto_4f
.end method

.method private sendMultipartSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 14
    .parameter "tracker"

    #@0
    .prologue
    .line 3451
    iget-object v8, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mData:Ljava/util/HashMap;

    #@2
    .line 3453
    .local v8, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "destination"

    #@4
    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Ljava/lang/String;

    #@a
    .line 3454
    .local v1, destinationAddress:Ljava/lang/String;
    const-string v0, "scaddress"

    #@c
    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Ljava/lang/String;

    #@12
    .line 3456
    .local v2, scAddress:Ljava/lang/String;
    const-string v0, "parts"

    #@14
    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    check-cast v3, Ljava/util/ArrayList;

    #@1a
    .line 3457
    .local v3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, "sentIntents"

    #@1c
    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v4

    #@20
    check-cast v4, Ljava/util/ArrayList;

    #@22
    .line 3458
    .local v4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const-string v0, "deliveryIntents"

    #@24
    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    move-result-object v5

    #@28
    check-cast v5, Ljava/util/ArrayList;

    #@2a
    .line 3461
    .local v5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2c
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@2f
    move-result-object v0

    #@30
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    #@33
    move-result v10

    #@34
    .line 3465
    .local v10, ss:I
    const/4 v0, 0x1

    #@35
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->checkNotInService()Z

    #@38
    move-result v11

    #@39
    if-ne v0, v11, :cond_57

    #@3b
    .line 3467
    const/4 v7, 0x0

    #@3c
    .local v7, i:I
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@3f
    move-result v6

    #@40
    .local v6, count:I
    :goto_40
    if-ge v7, v6, :cond_5b

    #@42
    .line 3468
    const/4 v9, 0x0

    #@43
    .line 3469
    .local v9, sentIntent:Landroid/app/PendingIntent;
    if-eqz v4, :cond_51

    #@45
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@48
    move-result v0

    #@49
    if-le v0, v7, :cond_51

    #@4b
    .line 3470
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4e
    move-result-object v9

    #@4f
    .end local v9           #sentIntent:Landroid/app/PendingIntent;
    check-cast v9, Landroid/app/PendingIntent;

    #@51
    .line 3472
    .restart local v9       #sentIntent:Landroid/app/PendingIntent;
    :cond_51
    invoke-static {v10, v9}, Lcom/android/internal/telephony/SMSDispatcher;->handleNotInService(ILandroid/app/PendingIntent;)V

    #@54
    .line 3467
    add-int/lit8 v7, v7, 0x1

    #@56
    goto :goto_40

    #@57
    .end local v6           #count:I
    .end local v7           #i:I
    .end local v9           #sentIntent:Landroid/app/PendingIntent;
    :cond_57
    move-object v0, p0

    #@58
    .line 3477
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    #@5b
    .line 3478
    :cond_5b
    return-void
.end method

.method private sendSavedPdusIndividually(Ljava/lang/String;)V
    .registers 12
    .parameter "where"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 4571
    const/4 v6, 0x0

    #@2
    .line 4572
    .local v6, cursor:Landroid/database/Cursor;
    check-cast v9, [[B

    #@4
    .line 4574
    .local v9, pdus:[[B
    sget-object v0, Lcom/android/internal/telephony/SMSDispatcher;->segmentExpirationLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@6
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    #@9
    .line 4576
    new-instance v0, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v1, "sendSavedPdusIndividually(), where "

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1f
    .line 4579
    :try_start_1f
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@21
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSegmentUri:Landroid/net/Uri;

    #@23
    sget-object v2, Lcom/android/internal/telephony/SMSDispatcher$Segment;->PROJECTION:[Ljava/lang/String;

    #@25
    const/4 v4, 0x0

    #@26
    const/4 v5, 0x0

    #@27
    move-object v3, p1

    #@28
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2b
    move-result-object v6

    #@2c
    .line 4581
    if-eqz v6, :cond_34

    #@2e
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_31
    .catchall {:try_start_1f .. :try_end_31} :catchall_80
    .catch Landroid/database/SQLException; {:try_start_1f .. :try_end_31} :catch_62

    #@31
    move-result v0

    #@32
    if-nez v0, :cond_3f

    #@34
    .line 4597
    :cond_34
    if-eqz v6, :cond_39

    #@36
    .line 4598
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@39
    .line 4600
    :cond_39
    sget-object v0, Lcom/android/internal/telephony/SMSDispatcher;->segmentExpirationLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@3b
    :goto_3b
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@3e
    .line 4602
    return-void

    #@3f
    .line 4585
    :cond_3f
    :try_start_3f
    const-string v0, "pdu"

    #@41
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@44
    move-result v8

    #@45
    .line 4586
    .local v8, pduColumn:I
    :goto_45
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@48
    move-result v0

    #@49
    if-eqz v0, :cond_70

    #@4b
    .line 4587
    const/4 v0, 0x1

    #@4c
    new-array v9, v0, [[B

    #@4e
    .line 4588
    const/4 v0, 0x0

    #@4f
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-static {v1}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@56
    move-result-object v1

    #@57
    aput-object v1, v9, v0

    #@59
    .line 4589
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[B)V

    #@5c
    .line 4590
    const-string v0, "sendSavedPdusIndividually(), One segment is sent individually."

    #@5e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_61
    .catchall {:try_start_3f .. :try_end_61} :catchall_80
    .catch Landroid/database/SQLException; {:try_start_3f .. :try_end_61} :catch_62

    #@61
    goto :goto_45

    #@62
    .line 4594
    .end local v8           #pduColumn:I
    :catch_62
    move-exception v7

    #@63
    .line 4595
    .local v7, e:Landroid/database/SQLException;
    :try_start_63
    const-string v0, "sendSavedPdusIndividually(), query exception catch"

    #@65
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_68
    .catchall {:try_start_63 .. :try_end_68} :catchall_80

    #@68
    .line 4597
    if-eqz v6, :cond_6d

    #@6a
    .line 4598
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@6d
    .line 4600
    :cond_6d
    sget-object v0, Lcom/android/internal/telephony/SMSDispatcher;->segmentExpirationLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@6f
    goto :goto_3b

    #@70
    .line 4593
    .end local v7           #e:Landroid/database/SQLException;
    .restart local v8       #pduColumn:I
    :cond_70
    :try_start_70
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@72
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSegmentUri:Landroid/net/Uri;

    #@74
    const/4 v2, 0x0

    #@75
    invoke-virtual {v0, v1, p1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_78
    .catchall {:try_start_70 .. :try_end_78} :catchall_80
    .catch Landroid/database/SQLException; {:try_start_70 .. :try_end_78} :catch_62

    #@78
    .line 4597
    if-eqz v6, :cond_7d

    #@7a
    .line 4598
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7d
    .line 4600
    :cond_7d
    sget-object v0, Lcom/android/internal/telephony/SMSDispatcher;->segmentExpirationLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@7f
    goto :goto_3b

    #@80
    .line 4597
    .end local v8           #pduColumn:I
    :catchall_80
    move-exception v0

    #@81
    if-eqz v6, :cond_86

    #@83
    .line 4598
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@86
    .line 4600
    :cond_86
    sget-object v1, Lcom/android/internal/telephony/SMSDispatcher;->segmentExpirationLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@88
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@8b
    .line 4597
    throw v0
.end method

.method private trySegmentExpirationAfterBootUp()V
    .registers 3

    #@0
    .prologue
    .line 4639
    sget-boolean v0, Lcom/android/internal/telephony/SMSDispatcher;->excutedSegmentExpirationAfterBootUp:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 4663
    :goto_4
    return-void

    #@5
    .line 4642
    :cond_5
    const/4 v0, 0x1

    #@6
    sput-boolean v0, Lcom/android/internal/telephony/SMSDispatcher;->excutedSegmentExpirationAfterBootUp:Z

    #@8
    .line 4644
    new-instance v0, Ljava/lang/Thread;

    #@a
    new-instance v1, Lcom/android/internal/telephony/SMSDispatcher$3;

    #@c
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/SMSDispatcher$3;-><init>(Lcom/android/internal/telephony/SMSDispatcher;)V

    #@f
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@12
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@15
    goto :goto_4
.end method


# virtual methods
.method protected SendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V
    .registers 35
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .parameter "cbAddress"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 2671
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-static {}, Lcom/android/internal/telephony/SMSDispatcher;->getNextConcatenatedRef()I

    #@3
    move-result v2

    #@4
    and-int/lit16 v0, v2, 0xff

    #@6
    move/from16 v24, v0

    #@8
    .line 2672
    .local v24, refNumber:I
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v20

    #@c
    .line 2673
    .local v20, msgCount:I
    const/4 v7, 0x0

    #@d
    .line 2676
    .local v7, encoding:I
    const/16 v17, 0x0

    #@f
    .line 2677
    .local v17, hasEmailMultiSms:Z
    move/from16 v0, v20

    #@11
    new-array v0, v0, [Ljava/lang/String;

    #@13
    move-object/from16 v23, v0

    #@15
    .line 2680
    .local v23, reParts:[Ljava/lang/String;
    move/from16 v0, v20

    #@17
    move-object/from16 v1, p0

    #@19
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@1b
    .line 2682
    move/from16 v0, v20

    #@1d
    new-array v0, v0, [Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@1f
    move-object/from16 v16, v0

    #@21
    .line 2683
    .local v16, encodingForParts:[Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    const/16 v18, 0x0

    #@23
    .local v18, i:I
    :goto_23
    move/from16 v0, v18

    #@25
    move/from16 v1, v20

    #@27
    if-ge v0, v1, :cond_4a

    #@29
    .line 2684
    move-object/from16 v0, p3

    #@2b
    move/from16 v1, v18

    #@2d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@30
    move-result-object v2

    #@31
    check-cast v2, Ljava/lang/CharSequence;

    #@33
    const/4 v3, 0x0

    #@34
    move-object/from16 v0, p0

    #@36
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/telephony/SMSDispatcher;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@39
    move-result-object v14

    #@3a
    .line 2685
    .local v14, details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    iget v2, v14, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@3c
    if-eq v7, v2, :cond_45

    #@3e
    if-eqz v7, :cond_43

    #@40
    const/4 v2, 0x1

    #@41
    if-ne v7, v2, :cond_45

    #@43
    .line 2688
    :cond_43
    iget v7, v14, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@45
    .line 2690
    :cond_45
    aput-object v14, v16, v18

    #@47
    .line 2683
    add-int/lit8 v18, v18, 0x1

    #@49
    goto :goto_23

    #@4a
    .line 2694
    .end local v14           #details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_4a
    move-object/from16 v0, p0

    #@4c
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@4e
    const-string v3, "sprint_segment_sms"

    #@50
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@53
    move-result v2

    #@54
    if-eqz v2, :cond_f1

    #@56
    .line 2697
    invoke-static/range {p1 .. p1}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@59
    move-result v2

    #@5a
    if-eqz v2, :cond_f1

    #@5c
    .line 2698
    move-object/from16 v19, p3

    #@5e
    .line 2700
    .local v19, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v25, 0x0

    #@60
    .line 2702
    .local v25, segCount:I
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    #@63
    move-result v25

    #@64
    .line 2703
    new-instance v2, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v3, "SendMultipartText(), messages.size:"

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v2

    #@77
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v2

    #@7b
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@7e
    .line 2705
    const/16 v2, 0xa

    #@80
    move/from16 v0, v25

    #@82
    if-le v0, v2, :cond_a0

    #@84
    .line 2706
    const/16 v25, 0xa

    #@86
    .line 2707
    new-instance v2, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v3, "SendMultipartText(), (segment>10) messages.size:"

    #@8d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v2

    #@91
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@94
    move-result-object v3

    #@95
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v2

    #@99
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v2

    #@9d
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@a0
    .line 2710
    :cond_a0
    const/4 v2, 0x1

    #@a1
    move/from16 v0, v25

    #@a3
    if-le v0, v2, :cond_f1

    #@a5
    .line 2712
    const/16 v21, 0x1

    #@a7
    .line 2713
    .local v21, reCalculate:Z
    const/16 v26, 0x0

    #@a9
    .line 2714
    .local v26, segIdx:I
    new-instance v22, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    .line 2715
    .local v22, reMsgText:Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    move-object/from16 v0, p1

    #@b5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v2

    #@b9
    const-string v3, " "

    #@bb
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v2

    #@bf
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v15

    #@c3
    .line 2716
    .local v15, emailAddrStr:Ljava/lang/String;
    const/16 v17, 0x1

    #@c5
    .line 2718
    const/16 v27, 0x0

    #@c7
    .local v27, segIdxLoop:I
    :goto_c7
    move/from16 v0, v27

    #@c9
    move/from16 v1, v25

    #@cb
    if-ge v0, v1, :cond_ed

    #@cd
    .line 2720
    new-instance v2, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v3

    #@d6
    move-object/from16 v0, v19

    #@d8
    move/from16 v1, v27

    #@da
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@dd
    move-result-object v2

    #@de
    check-cast v2, Ljava/lang/String;

    #@e0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v2

    #@e4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e7
    move-result-object v2

    #@e8
    aput-object v2, v23, v27

    #@ea
    .line 2718
    add-int/lit8 v27, v27, 0x1

    #@ec
    goto :goto_c7

    #@ed
    .line 2722
    :cond_ed
    move/from16 v20, v25

    #@ef
    .line 2724
    const-string p1, "6245"

    #@f1
    .line 2730
    .end local v15           #emailAddrStr:Ljava/lang/String;
    .end local v19           #messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v21           #reCalculate:Z
    .end local v22           #reMsgText:Ljava/lang/StringBuilder;
    .end local v25           #segCount:I
    .end local v26           #segIdx:I
    .end local v27           #segIdxLoop:I
    :cond_f1
    const/16 v18, 0x0

    #@f3
    :goto_f3
    move/from16 v0, v18

    #@f5
    move/from16 v1, v20

    #@f7
    if-ge v0, v1, :cond_209

    #@f9
    .line 2731
    new-instance v13, Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@fb
    invoke-direct {v13}, Lcom/android/internal/telephony/SmsHeader$ConcatRef;-><init>()V

    #@fe
    .line 2732
    .local v13, concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    move/from16 v0, v24

    #@100
    iput v0, v13, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@102
    .line 2733
    add-int/lit8 v2, v18, 0x1

    #@104
    iput v2, v13, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@106
    .line 2734
    move/from16 v0, v20

    #@108
    iput v0, v13, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@10a
    .line 2743
    move-object/from16 v0, p0

    #@10c
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@10e
    const-string v3, "sprint_segment_sms"

    #@110
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@113
    move-result v2

    #@114
    if-eqz v2, :cond_18e

    #@116
    .line 2744
    const/4 v2, 0x0

    #@117
    iput-boolean v2, v13, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->isEightBits:Z

    #@119
    .line 2750
    :goto_119
    new-instance v6, Lcom/android/internal/telephony/SmsHeader;

    #@11b
    invoke-direct {v6}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@11e
    .line 2751
    .local v6, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    iput-object v13, v6, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@120
    .line 2754
    const/4 v2, 0x1

    #@121
    if-ne v7, v2, :cond_12f

    #@123
    .line 2755
    aget-object v2, v16, v18

    #@125
    iget v2, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@127
    iput v2, v6, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@129
    .line 2756
    aget-object v2, v16, v18

    #@12b
    iget v2, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@12d
    iput v2, v6, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@12f
    .line 2759
    :cond_12f
    const/4 v8, 0x0

    #@130
    .line 2760
    .local v8, sentIntent:Landroid/app/PendingIntent;
    if-eqz p4, :cond_144

    #@132
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    #@135
    move-result v2

    #@136
    move/from16 v0, v18

    #@138
    if-le v2, v0, :cond_144

    #@13a
    .line 2761
    move-object/from16 v0, p4

    #@13c
    move/from16 v1, v18

    #@13e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@141
    move-result-object v8

    #@142
    .end local v8           #sentIntent:Landroid/app/PendingIntent;
    check-cast v8, Landroid/app/PendingIntent;

    #@144
    .line 2764
    .restart local v8       #sentIntent:Landroid/app/PendingIntent;
    :cond_144
    const/4 v9, 0x0

    #@145
    .line 2765
    .local v9, deliveryIntent:Landroid/app/PendingIntent;
    if-eqz p5, :cond_159

    #@147
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    #@14a
    move-result v2

    #@14b
    move/from16 v0, v18

    #@14d
    if-le v2, v0, :cond_159

    #@14f
    .line 2766
    move-object/from16 v0, p5

    #@151
    move/from16 v1, v18

    #@153
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@156
    move-result-object v9

    #@157
    .end local v9           #deliveryIntent:Landroid/app/PendingIntent;
    check-cast v9, Landroid/app/PendingIntent;

    #@159
    .line 2770
    .restart local v9       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_159
    move-object/from16 v0, p0

    #@15b
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@15d
    const-string v3, "vzw_sms_retry_scheme"

    #@15f
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@162
    move-result v2

    #@163
    const/4 v3, 0x1

    #@164
    if-ne v2, v3, :cond_1b9

    #@166
    .line 2771
    if-eqz v20, :cond_16d

    #@168
    const/4 v2, 0x1

    #@169
    move/from16 v0, v20

    #@16b
    if-ne v0, v2, :cond_194

    #@16d
    .line 2772
    :cond_16d
    move-object/from16 v0, p3

    #@16f
    move/from16 v1, v18

    #@171
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@174
    move-result-object v5

    #@175
    check-cast v5, Ljava/lang/String;

    #@177
    add-int/lit8 v2, v20, -0x1

    #@179
    move/from16 v0, v18

    #@17b
    if-ne v0, v2, :cond_192

    #@17d
    const/4 v10, 0x1

    #@17e
    :goto_17e
    const/4 v12, 0x0

    #@17f
    move-object/from16 v2, p0

    #@181
    move-object/from16 v3, p1

    #@183
    move-object/from16 v4, p2

    #@185
    move-object/from16 v11, p6

    #@187
    invoke-virtual/range {v2 .. v12}, Lcom/android/internal/telephony/SMSDispatcher;->sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;Z)V

    #@18a
    .line 2730
    :cond_18a
    :goto_18a
    add-int/lit8 v18, v18, 0x1

    #@18c
    goto/16 :goto_f3

    #@18e
    .line 2746
    .end local v6           #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    .end local v8           #sentIntent:Landroid/app/PendingIntent;
    .end local v9           #deliveryIntent:Landroid/app/PendingIntent;
    :cond_18e
    const/4 v2, 0x1

    #@18f
    iput-boolean v2, v13, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->isEightBits:Z

    #@191
    goto :goto_119

    #@192
    .line 2772
    .restart local v6       #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    .restart local v8       #sentIntent:Landroid/app/PendingIntent;
    .restart local v9       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_192
    const/4 v10, 0x0

    #@193
    goto :goto_17e

    #@194
    .line 2774
    :cond_194
    const/4 v2, 0x1

    #@195
    move/from16 v0, v20

    #@197
    if-le v0, v2, :cond_18a

    #@199
    .line 2775
    move-object/from16 v0, p3

    #@19b
    move/from16 v1, v18

    #@19d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a0
    move-result-object v5

    #@1a1
    check-cast v5, Ljava/lang/String;

    #@1a3
    add-int/lit8 v2, v20, -0x1

    #@1a5
    move/from16 v0, v18

    #@1a7
    if-ne v0, v2, :cond_1b7

    #@1a9
    const/4 v10, 0x1

    #@1aa
    :goto_1aa
    const/4 v12, 0x1

    #@1ab
    move-object/from16 v2, p0

    #@1ad
    move-object/from16 v3, p1

    #@1af
    move-object/from16 v4, p2

    #@1b1
    move-object/from16 v11, p6

    #@1b3
    invoke-virtual/range {v2 .. v12}, Lcom/android/internal/telephony/SMSDispatcher;->sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;Z)V

    #@1b6
    goto :goto_18a

    #@1b7
    :cond_1b7
    const/4 v10, 0x0

    #@1b8
    goto :goto_1aa

    #@1b9
    .line 2779
    :cond_1b9
    move-object/from16 v0, p0

    #@1bb
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@1bd
    const-string v3, "sprint_segment_sms"

    #@1bf
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1c2
    move-result v2

    #@1c3
    if-eqz v2, :cond_1ea

    #@1c5
    .line 2780
    if-eqz v17, :cond_1dc

    #@1c7
    aget-object v5, v23, v18

    #@1c9
    :goto_1c9
    add-int/lit8 v2, v20, -0x1

    #@1cb
    move/from16 v0, v18

    #@1cd
    if-ne v0, v2, :cond_1e8

    #@1cf
    const/4 v10, 0x1

    #@1d0
    :goto_1d0
    move-object/from16 v2, p0

    #@1d2
    move-object/from16 v3, p1

    #@1d4
    move-object/from16 v4, p2

    #@1d6
    move-object/from16 v11, p6

    #@1d8
    invoke-virtual/range {v2 .. v11}, Lcom/android/internal/telephony/SMSDispatcher;->sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;)V

    #@1db
    goto :goto_18a

    #@1dc
    :cond_1dc
    move-object/from16 v0, p3

    #@1de
    move/from16 v1, v18

    #@1e0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1e3
    move-result-object v2

    #@1e4
    check-cast v2, Ljava/lang/String;

    #@1e6
    move-object v5, v2

    #@1e7
    goto :goto_1c9

    #@1e8
    :cond_1e8
    const/4 v10, 0x0

    #@1e9
    goto :goto_1d0

    #@1ea
    .line 2784
    :cond_1ea
    move-object/from16 v0, p3

    #@1ec
    move/from16 v1, v18

    #@1ee
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f1
    move-result-object v5

    #@1f2
    check-cast v5, Ljava/lang/String;

    #@1f4
    add-int/lit8 v2, v20, -0x1

    #@1f6
    move/from16 v0, v18

    #@1f8
    if-ne v0, v2, :cond_207

    #@1fa
    const/4 v10, 0x1

    #@1fb
    :goto_1fb
    move-object/from16 v2, p0

    #@1fd
    move-object/from16 v3, p1

    #@1ff
    move-object/from16 v4, p2

    #@201
    move-object/from16 v11, p6

    #@203
    invoke-virtual/range {v2 .. v11}, Lcom/android/internal/telephony/SMSDispatcher;->sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;)V

    #@206
    goto :goto_18a

    #@207
    :cond_207
    const/4 v10, 0x0

    #@208
    goto :goto_1fb

    #@209
    .line 2790
    .end local v6           #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    .end local v8           #sentIntent:Landroid/app/PendingIntent;
    .end local v9           #deliveryIntent:Landroid/app/PendingIntent;
    .end local v13           #concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    :cond_209
    return-void
.end method

.method protected abstract SendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
.end method

.method protected SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    .registers 15
    .parameter
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "format"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/app/PendingIntent;",
            "Landroid/app/PendingIntent;",
            "Ljava/lang/String;",
            ")",
            "Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;"
        }
    .end annotation

    #@0
    .prologue
    .line 3570
    .local p1, data:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v9

    #@6
    .line 3571
    .local v9, pm:Landroid/content/pm/PackageManager;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@9
    move-result v0

    #@a
    invoke-virtual {v9, v0}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@d
    move-result-object v8

    #@e
    .line 3574
    .local v8, packageNames:[Ljava/lang/String;
    const/4 v4, 0x0

    #@f
    .line 3575
    .local v4, appInfo:Landroid/content/pm/PackageInfo;
    if-eqz v8, :cond_1d

    #@11
    array-length v0, v8

    #@12
    if-lez v0, :cond_1d

    #@14
    .line 3578
    const/4 v0, 0x0

    #@15
    :try_start_15
    aget-object v0, v8, v0

    #@17
    const/16 v1, 0x40

    #@19
    invoke-virtual {v9, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_15 .. :try_end_1c} :catch_34

    #@1c
    move-result-object v4

    #@1d
    .line 3585
    :cond_1d
    :goto_1d
    const-string v0, "destAddr"

    #@1f
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Ljava/lang/String;

    #@25
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    .line 3586
    .local v5, destAddr:Ljava/lang/String;
    new-instance v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@2b
    const/4 v7, 0x0

    #@2c
    move-object v1, p1

    #@2d
    move-object v2, p2

    #@2e
    move-object v3, p3

    #@2f
    move-object v6, p4

    #@30
    invoke-direct/range {v0 .. v7}, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;-><init>(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher$1;)V

    #@33
    return-object v0

    #@34
    .line 3579
    .end local v5           #destAddr:Ljava/lang/String;
    :catch_34
    move-exception v0

    #@35
    goto :goto_1d
.end method

.method protected SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;I[BLcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;
    .registers 9
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "destPort"
    .parameter "data"
    .parameter "pdu"

    #@0
    .prologue
    .line 3645
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    .line 3646
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "destAddr"

    #@7
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 3647
    const-string v1, "scAddr"

    #@c
    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 3648
    const-string v1, "destPort"

    #@11
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 3649
    const-string v1, "data"

    #@1a
    invoke-virtual {v0, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    .line 3650
    const-string v1, "smsc"

    #@1f
    iget-object v2, p5, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@21
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 3651
    const-string v1, "pdu"

    #@26
    iget-object v2, p5, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@28
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    .line 3652
    return-object v0
.end method

.method protected SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;
    .registers 8
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "pdu"

    #@0
    .prologue
    .line 3591
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    .line 3592
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "destAddr"

    #@7
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 3593
    const-string v1, "scAddr"

    #@c
    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 3594
    const-string v1, "text"

    #@11
    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 3595
    const-string v1, "smsc"

    #@16
    iget-object v2, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@18
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 3596
    const-string v1, "pdu"

    #@1d
    iget-object v2, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@1f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 3597
    return-object v0
.end method

.method protected SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;Ljava/lang/String;)Ljava/util/HashMap;
    .registers 9
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "pdu"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 3617
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    .line 3618
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "destAddr"

    #@7
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 3619
    const-string v1, "scAddr"

    #@c
    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 3620
    const-string v1, "text"

    #@11
    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 3621
    const-string v1, "smsc"

    #@16
    iget-object v2, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@18
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 3622
    const-string v1, "pdu"

    #@1d
    iget-object v2, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@1f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 3623
    const-string v1, "cbAddress"

    #@24
    invoke-virtual {v0, v1, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    .line 3624
    return-object v0
.end method

.method protected SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;Ljava/lang/String;Z)Ljava/util/HashMap;
    .registers 10
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "pdu"
    .parameter "cbAddress"
    .parameter "isMultiPart"

    #@0
    .prologue
    .line 3631
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    .line 3632
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "destAddr"

    #@7
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 3633
    const-string v1, "scAddr"

    #@c
    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 3634
    const-string v1, "text"

    #@11
    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 3635
    const-string v1, "smsc"

    #@16
    iget-object v2, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@18
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 3636
    const-string v1, "pdu"

    #@1d
    iget-object v2, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@1f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 3637
    const-string v1, "cbAddress"

    #@24
    invoke-virtual {v0, v1, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    .line 3638
    const-string v1, "parts"

    #@29
    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@30
    .line 3639
    return-object v0
.end method

.method protected SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;Z)Ljava/util/HashMap;
    .registers 9
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "pdu"
    .parameter "isMultiPart"

    #@0
    .prologue
    .line 3603
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    .line 3604
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "destAddr"

    #@7
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 3605
    const-string v1, "scAddr"

    #@c
    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 3606
    const-string v1, "text"

    #@11
    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 3607
    const-string v1, "smsc"

    #@16
    iget-object v2, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@18
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 3608
    const-string v1, "pdu"

    #@1d
    iget-object v2, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@1f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 3609
    const-string v1, "parts"

    #@24
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    .line 3610
    return-object v0
.end method

.method protected abstract acknowledgeLastIncomingSms(ZILandroid/os/Message;)V
.end method

.method protected abstract calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
.end method

.method protected abstract calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
.end method

.method checkDestination(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)Z
    .registers 14
    .parameter "tracker"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v11, 0x3

    #@2
    const/4 v10, 0x2

    #@3
    const/4 v6, 0x1

    #@4
    .line 3074
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@6
    const-string v9, "android.permission.SEND_SMS_NO_CONFIRMATION"

    #@8
    invoke-virtual {v8, v9}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@b
    move-result v8

    #@c
    if-nez v8, :cond_f

    #@e
    .line 3133
    :cond_e
    :goto_e
    return v6

    #@f
    .line 3078
    :cond_f
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPremiumSmsRule:Ljava/util/concurrent/atomic/AtomicInteger;

    #@11
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@14
    move-result v3

    #@15
    .line 3079
    .local v3, rule:I
    const/4 v5, 0x0

    #@16
    .line 3080
    .local v5, smsCategory:I
    if-eq v3, v6, :cond_1a

    #@18
    if-ne v3, v11, :cond_3d

    #@1a
    .line 3081
    :cond_1a
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@1c
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    .line 3082
    .local v4, simCountryIso:Ljava/lang/String;
    if-eqz v4, :cond_28

    #@22
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@25
    move-result v8

    #@26
    if-eq v8, v10, :cond_35

    #@28
    .line 3083
    :cond_28
    const-string v8, "SMS"

    #@2a
    const-string v9, "Can\'t get SIM country Iso: trying network country Iso"

    #@2c
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 3084
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@31
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    .line 3087
    :cond_35
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@37
    iget-object v9, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mDestAddress:Ljava/lang/String;

    #@39
    invoke-virtual {v8, v9, v4}, Lcom/android/internal/telephony/SmsUsageMonitor;->checkDestination(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    move-result v5

    #@3d
    .line 3089
    .end local v4           #simCountryIso:Ljava/lang/String;
    :cond_3d
    if-eq v3, v10, :cond_41

    #@3f
    if-ne v3, v11, :cond_6a

    #@41
    .line 3090
    :cond_41
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@43
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    .line 3091
    .local v1, networkCountryIso:Ljava/lang/String;
    if-eqz v1, :cond_4f

    #@49
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@4c
    move-result v8

    #@4d
    if-eq v8, v10, :cond_5c

    #@4f
    .line 3092
    :cond_4f
    const-string v8, "SMS"

    #@51
    const-string v9, "Can\'t get Network country Iso: trying SIM country Iso"

    #@53
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 3093
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@58
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    #@5b
    move-result-object v1

    #@5c
    .line 3096
    :cond_5c
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@5e
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@60
    iget-object v9, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mDestAddress:Ljava/lang/String;

    #@62
    invoke-virtual {v8, v9, v1}, Lcom/android/internal/telephony/SmsUsageMonitor;->checkDestination(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    move-result v8

    #@66
    invoke-static {v5, v8}, Lcom/android/internal/telephony/SmsUsageMonitor;->mergeShortCodeCategories(II)I

    #@69
    move-result v5

    #@6a
    .line 3100
    .end local v1           #networkCountryIso:Ljava/lang/String;
    :cond_6a
    if-eqz v5, :cond_e

    #@6c
    if-eq v5, v6, :cond_e

    #@6e
    if-eq v5, v10, :cond_e

    #@70
    .line 3107
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@72
    iget-object v9, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mAppInfo:Landroid/content/pm/PackageInfo;

    #@74
    iget-object v9, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@76
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/SmsUsageMonitor;->getPremiumSmsPermission(Ljava/lang/String;)I

    #@79
    move-result v2

    #@7a
    .line 3109
    .local v2, premiumSmsPermission:I
    if-nez v2, :cond_7d

    #@7c
    .line 3111
    const/4 v2, 0x1

    #@7d
    .line 3114
    :cond_7d
    packed-switch v2, :pswitch_data_ac

    #@80
    .line 3127
    if-ne v5, v11, :cond_a8

    #@82
    .line 3128
    const/16 v0, 0x8

    #@84
    .line 3132
    .local v0, event:I
    :goto_84
    invoke-virtual {p0, v0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@87
    move-result-object v6

    #@88
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessage(Landroid/os/Message;)Z

    #@8b
    move v6, v7

    #@8c
    .line 3133
    goto :goto_e

    #@8d
    .line 3116
    .end local v0           #event:I
    :pswitch_8d
    const-string v7, "SMS"

    #@8f
    const-string v8, "User approved this app to send to premium SMS"

    #@91
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    goto/16 :goto_e

    #@96
    .line 3120
    :pswitch_96
    const-string v6, "SMS"

    #@98
    const-string v8, "User denied this app from sending to premium SMS"

    #@9a
    invoke-static {v6, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    .line 3121
    const/4 v6, 0x7

    #@9e
    invoke-virtual {p0, v6, p1}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a1
    move-result-object v6

    #@a2
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessage(Landroid/os/Message;)Z

    #@a5
    move v6, v7

    #@a6
    .line 3122
    goto/16 :goto_e

    #@a8
    .line 3130
    :cond_a8
    const/16 v0, 0x9

    #@aa
    .restart local v0       #event:I
    goto :goto_84

    #@ab
    .line 3114
    nop

    #@ac
    :pswitch_data_ac
    .packed-switch 0x2
        :pswitch_96
        :pswitch_8d
    .end packed-switch
.end method

.method public checkDuplicateKddiMessage(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 21
    .parameter "messageId"
    .parameter "sent_date"
    .parameter "addr"
    .parameter "body"

    #@0
    .prologue
    .line 4828
    const/4 v12, 0x0

    #@1
    .line 4830
    .local v12, isDuplicated:Z
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v2, "checkDuplicateKddiMessage(), [KDDI] check duplicate ? : originAddr: "

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    move-object/from16 v0, p3

    #@e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, "// timeStamp: "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    #@1b
    move-result-wide v2

    #@1c
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, " // Message ID : "

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    move/from16 v0, p1

    #@2c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@37
    .line 4834
    move-object/from16 v0, p0

    #@39
    iget-object v1, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@3b
    sget-object v2, Landroid/provider/Telephony$Sms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    #@3d
    sget-object v3, Lcom/android/internal/telephony/SMSDispatcher;->DUPLICATE_PROJECTION:[Ljava/lang/String;

    #@3f
    const-string v4, "address = ? AND date_sent = ? "

    #@41
    const/4 v5, 0x2

    #@42
    new-array v5, v5, [Ljava/lang/String;

    #@44
    const/4 v6, 0x0

    #@45
    aput-object p3, v5, v6

    #@47
    const/4 v6, 0x1

    #@48
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    #@4b
    move-result-wide v14

    #@4c
    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@4f
    move-result-object v14

    #@50
    aput-object v14, v5, v6

    #@52
    const/4 v6, 0x0

    #@53
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@56
    move-result-object v8

    #@57
    .line 4838
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_61

    #@59
    .line 4839
    const-string v1, "checkDuplicateKddiMessage(), [KDDI] Duplicate Error"

    #@5b
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5e
    .line 4840
    const/4 v12, 0x0

    #@5f
    move v1, v12

    #@60
    .line 4920
    :cond_60
    :goto_60
    return v1

    #@61
    .line 4844
    :cond_61
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    #@64
    move-result v9

    #@65
    .line 4845
    .local v9, cursorCount:I
    if-nez v9, :cond_d4

    #@67
    .line 4846
    const-string v1, "checkDuplicateKddiMessage(), [KDDI]  Not duplicated message "

    #@69
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@6c
    .line 4847
    const/4 v12, 0x0

    #@6d
    .line 4861
    :goto_6d
    if-eqz v8, :cond_72

    #@6f
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@72
    .line 4866
    :cond_72
    new-instance v13, Landroid/content/ContentValues;

    #@74
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    #@77
    .line 4867
    .local v13, vals:Landroid/content/ContentValues;
    const-string v1, "address"

    #@79
    move-object/from16 v0, p3

    #@7b
    invoke-virtual {v13, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7e
    .line 4868
    const-string v1, "date_sent"

    #@80
    new-instance v2, Ljava/lang/Long;

    #@82
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    #@85
    move-result-wide v3

    #@86
    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    #@89
    invoke-virtual {v13, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@8c
    .line 4869
    const-string v1, "body"

    #@8e
    move-object/from16 v0, p4

    #@90
    invoke-virtual {v13, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@93
    .line 4870
    const-string v1, "person"

    #@95
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@98
    move-result-object v2

    #@99
    invoke-virtual {v13, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@9c
    .line 4871
    const-string v1, "thread_id"

    #@9e
    const/4 v2, -0x1

    #@9f
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a2
    move-result-object v2

    #@a3
    invoke-virtual {v13, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@a6
    .line 4872
    const-string v1, "read"

    #@a8
    const/4 v2, 0x1

    #@a9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ac
    move-result-object v2

    #@ad
    invoke-virtual {v13, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@b0
    .line 4875
    move-object/from16 v0, p0

    #@b2
    iget-object v1, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@b4
    sget-object v2, Landroid/provider/Telephony$Sms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    #@b6
    const/4 v3, 0x0

    #@b7
    const-string v4, "thread_id = ? "

    #@b9
    const/4 v5, 0x1

    #@ba
    new-array v5, v5, [Ljava/lang/String;

    #@bc
    const/4 v6, 0x0

    #@bd
    const/4 v14, -0x1

    #@be
    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@c1
    move-result-object v14

    #@c2
    aput-object v14, v5, v6

    #@c4
    const-string v6, "date_sent asc"

    #@c6
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@c9
    move-result-object v8

    #@ca
    .line 4878
    if-nez v8, :cond_12b

    #@cc
    .line 4879
    const-string v1, "checkDuplicateKddiMessage(), [KDDI] Duplicate.. Delete Error"

    #@ce
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@d1
    .line 4880
    const/4 v12, 0x0

    #@d2
    move v1, v12

    #@d3
    .line 4881
    goto :goto_60

    #@d4
    .line 4850
    .end local v13           #vals:Landroid/content/ContentValues;
    :cond_d4
    :goto_d4
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@d7
    move-result v1

    #@d8
    if-eqz v1, :cond_123

    #@da
    .line 4852
    new-instance v1, Ljava/lang/StringBuilder;

    #@dc
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@df
    const-string v2, "checkDuplicateKddiMessage(), [KDDI] Stored message data: _id= "

    #@e1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v1

    #@e5
    const/4 v2, 0x3

    #@e6
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@e9
    move-result-object v2

    #@ea
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v1

    #@ee
    const-string v2, "  address ="

    #@f0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v1

    #@f4
    const/4 v2, 0x1

    #@f5
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@f8
    move-result-object v2

    #@f9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v1

    #@fd
    const-string v2, "  mcTimeStamp= "

    #@ff
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v1

    #@103
    const/4 v2, 0x2

    #@104
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@107
    move-result-object v2

    #@108
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v1

    #@10c
    const-string v2, "  body = "

    #@10e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v1

    #@112
    const/4 v2, 0x4

    #@113
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@116
    move-result-object v2

    #@117
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v1

    #@11b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11e
    move-result-object v1

    #@11f
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@122
    goto :goto_d4

    #@123
    .line 4857
    :cond_123
    const-string v1, "checkDuplicateKddiMessage(), [KDDI] Duplicated message "

    #@125
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@128
    .line 4858
    const/4 v12, 0x1

    #@129
    goto/16 :goto_6d

    #@12b
    .line 4884
    .restart local v13       #vals:Landroid/content/ContentValues;
    :cond_12b
    new-instance v1, Ljava/lang/StringBuilder;

    #@12d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@130
    const-string v2, "checkDuplicateKddiMessage(), [KDDI] getCount(), before delete the Message "

    #@132
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v1

    #@136
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    #@139
    move-result v2

    #@13a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v1

    #@13e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@141
    move-result-object v1

    #@142
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@145
    .line 4886
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    #@148
    move-result v1

    #@149
    const/16 v2, 0x13

    #@14b
    if-le v1, v2, :cond_196

    #@14d
    .line 4887
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@150
    .line 4888
    const/4 v1, 0x0

    #@151
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@154
    move-result-object v7

    #@155
    .line 4890
    .local v7, _id:Ljava/lang/String;
    :try_start_155
    new-instance v1, Ljava/lang/StringBuilder;

    #@157
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15a
    const-string v2, "content://sms/"

    #@15c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v1

    #@160
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v1

    #@164
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@167
    move-result-object v1

    #@168
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@16b
    move-result-object v10

    #@16c
    .line 4891
    .local v10, dup_delete_MessageUri:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@16e
    iget-object v1, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@170
    const-string v2, "_id =?"

    #@172
    const/4 v3, 0x1

    #@173
    new-array v3, v3, [Ljava/lang/String;

    #@175
    const/4 v4, 0x0

    #@176
    aput-object v7, v3, v4

    #@178
    invoke-virtual {v1, v10, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@17b
    .line 4892
    new-instance v1, Ljava/lang/StringBuilder;

    #@17d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@180
    const-string v2, "checkDuplicateKddiMessage(), [KDDI] delete old one in duplicate SMS database  _id: "

    #@182
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@185
    move-result-object v1

    #@186
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v1

    #@18a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18d
    move-result-object v1

    #@18e
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_191
    .catchall {:try_start_155 .. :try_end_191} :catchall_1d3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_155 .. :try_end_191} :catch_1bd

    #@191
    .line 4899
    if-eqz v8, :cond_196

    #@193
    .end local v10           #dup_delete_MessageUri:Landroid/net/Uri;
    :goto_193
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@196
    .line 4903
    .end local v7           #_id:Ljava/lang/String;
    :cond_196
    if-eqz v8, :cond_19b

    #@198
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@19b
    .line 4906
    :cond_19b
    :try_start_19b
    move-object/from16 v0, p0

    #@19d
    iget-object v1, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@19f
    sget-object v2, Landroid/provider/Telephony$Sms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    #@1a1
    invoke-virtual {v1, v2, v13}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1a4
    .catchall {:try_start_19b .. :try_end_1a4} :catchall_1f4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_19b .. :try_end_1a4} :catch_1da
    .catch Ljava/lang/IllegalArgumentException; {:try_start_19b .. :try_end_1a4} :catch_1eb

    #@1a4
    .line 4918
    :cond_1a4
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a9
    const-string v2, "checkDuplicateKddiMessage(), [KDDI] is duplicate Message? : "

    #@1ab
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ae
    move-result-object v1

    #@1af
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v1

    #@1b3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b6
    move-result-object v1

    #@1b7
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1ba
    move v1, v12

    #@1bb
    .line 4920
    goto/16 :goto_60

    #@1bd
    .line 4893
    .restart local v7       #_id:Ljava/lang/String;
    :catch_1bd
    move-exception v11

    #@1be
    .line 4894
    .local v11, e:Landroid/database/sqlite/SQLiteException;
    :try_start_1be
    move-object/from16 v0, p0

    #@1c0
    invoke-direct {v0, v11}, Lcom/android/internal/telephony/SMSDispatcher;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@1c3
    move-result v1

    #@1c4
    if-eqz v1, :cond_1f6

    #@1c6
    .line 4895
    const-string v1, "checkDuplicateKddiMessage(), [KDDI]  Can\'t access duplicate SMS database"

    #@1c8
    invoke-static {v1, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1cb
    .catchall {:try_start_1be .. :try_end_1cb} :catchall_1d3

    #@1cb
    .line 4896
    const/4 v1, 0x0

    #@1cc
    .line 4899
    if-eqz v8, :cond_60

    #@1ce
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@1d1
    goto/16 :goto_60

    #@1d3
    .end local v11           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_1d3
    move-exception v1

    #@1d4
    if-eqz v8, :cond_1d9

    #@1d6
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@1d9
    :cond_1d9
    throw v1

    #@1da
    .line 4907
    .end local v7           #_id:Ljava/lang/String;
    :catch_1da
    move-exception v11

    #@1db
    .line 4908
    .restart local v11       #e:Landroid/database/sqlite/SQLiteException;
    :try_start_1db
    move-object/from16 v0, p0

    #@1dd
    invoke-direct {v0, v11}, Lcom/android/internal/telephony/SMSDispatcher;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@1e0
    move-result v1

    #@1e1
    if-eqz v1, :cond_1a4

    #@1e3
    .line 4909
    const-string v1, "checkDuplicateKddiMessage(), [KDDI] Can\'t access duplicate SMS database"

    #@1e5
    invoke-static {v1, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e8
    .line 4910
    const/4 v1, 0x0

    #@1e9
    goto/16 :goto_60

    #@1eb
    .line 4912
    .end local v11           #e:Landroid/database/sqlite/SQLiteException;
    :catch_1eb
    move-exception v11

    #@1ec
    .line 4913
    .local v11, e:Ljava/lang/IllegalArgumentException;
    const-string v1, "checkDuplicateKddiMessage(), [KDDI] Fail to duplicate SMS"

    #@1ee
    invoke-static {v1, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1f1
    .catchall {:try_start_1db .. :try_end_1f1} :catchall_1f4

    #@1f1
    .line 4914
    const/4 v1, 0x0

    #@1f2
    goto/16 :goto_60

    #@1f4
    .line 4915
    .end local v11           #e:Ljava/lang/IllegalArgumentException;
    :catchall_1f4
    move-exception v1

    #@1f5
    throw v1

    #@1f6
    .line 4899
    .restart local v7       #_id:Ljava/lang/String;
    .local v11, e:Landroid/database/sqlite/SQLiteException;
    :cond_1f6
    if-eqz v8, :cond_196

    #@1f8
    goto :goto_193
.end method

.method public checkNotInService()Z
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 3982
    const/4 v1, 0x0

    #@2
    .line 3983
    .local v1, notInService:Z
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@7
    move-result-object v3

    #@8
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I

    #@b
    move-result v2

    #@c
    .line 3985
    .local v2, ss:I
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@e
    const-string v4, "kr_sms_3g_common"

    #@10
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@13
    move-result v3

    #@14
    if-ne v5, v3, :cond_5e

    #@16
    .line 3986
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@18
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getDataState()I

    #@1f
    move-result v0

    #@20
    .line 3987
    .local v0, data_ss:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "checkNotInService(), [KRSMS--] service state : "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v4, " data service state : "

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@40
    .line 3989
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@42
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@45
    move-result v3

    #@46
    if-ne v5, v3, :cond_54

    #@48
    .line 3990
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->isIms()Z

    #@4b
    move-result v3

    #@4c
    if-nez v3, :cond_53

    #@4e
    if-eqz v2, :cond_53

    #@50
    if-eqz v0, :cond_53

    #@52
    .line 3991
    const/4 v1, 0x1

    #@53
    .line 4014
    .end local v0           #data_ss:I
    :cond_53
    :goto_53
    return v1

    #@54
    .line 3995
    .restart local v0       #data_ss:I
    :cond_54
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->isIms()Z

    #@57
    move-result v3

    #@58
    if-nez v3, :cond_53

    #@5a
    if-eqz v2, :cond_53

    #@5c
    .line 3996
    const/4 v1, 0x1

    #@5d
    goto :goto_53

    #@5e
    .line 4000
    .end local v0           #data_ss:I
    :cond_5e
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@60
    const-string v4, "mo_sms_with_1xcsfb"

    #@62
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@65
    move-result v3

    #@66
    if-eqz v3, :cond_9e

    #@68
    .line 4001
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@6a
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getDataState()I

    #@71
    move-result v0

    #@72
    .line 4002
    .restart local v0       #data_ss:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v4, "checkNotInService(), service state : "

    #@79
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v3

    #@7d
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    const-string v4, ", data service state : "

    #@83
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v3

    #@8b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v3

    #@8f
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@92
    .line 4005
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->isIms()Z

    #@95
    move-result v3

    #@96
    if-nez v3, :cond_53

    #@98
    if-eqz v2, :cond_53

    #@9a
    if-eqz v0, :cond_53

    #@9c
    .line 4006
    const/4 v1, 0x1

    #@9d
    goto :goto_53

    #@9e
    .line 4010
    .end local v0           #data_ss:I
    :cond_9e
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->isIms()Z

    #@a1
    move-result v3

    #@a2
    if-nez v3, :cond_53

    #@a4
    if-eqz v2, :cond_53

    #@a6
    .line 4011
    const/4 v1, 0x1

    #@a7
    goto :goto_53
.end method

.method public dispatch(Landroid/content/Intent;Ljava/lang/String;)V
    .registers 11
    .parameter "intent"
    .parameter "permission"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 870
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3
    const-wide/16 v1, 0x1388

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@8
    .line 871
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@a
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResultReceiver:Landroid/content/BroadcastReceiver;

    #@c
    const/4 v5, -0x1

    #@d
    move-object v1, p1

    #@e
    move-object v2, p2

    #@f
    move-object v4, p0

    #@10
    move-object v7, v6

    #@11
    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@14
    .line 873
    return-void
.end method

.method public dispatch(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;)V
    .registers 12
    .parameter "intent"
    .parameter "permission"
    .parameter "resultReceiver"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 916
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3
    const-wide/16 v1, 0x1388

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@8
    .line 917
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@a
    const/4 v5, -0x1

    #@b
    move-object v1, p1

    #@c
    move-object v2, p2

    #@d
    move-object v3, p3

    #@e
    move-object v4, p0

    #@f
    move-object v7, v6

    #@10
    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@13
    .line 919
    return-void
.end method

.method protected dispatch(Landroid/content/Intent;Ljava/lang/String;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 15
    .parameter "intent"
    .parameter "permission"
    .parameter "scheduler"
    .parameter "initialCode"
    .parameter "initialData"
    .parameter "initialExtras"

    #@0
    .prologue
    .line 899
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    const-wide/16 v1, 0x1388

    #@4
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@7
    .line 900
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@9
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResultReceiver:Landroid/content/BroadcastReceiver;

    #@b
    move-object v1, p1

    #@c
    move-object v2, p2

    #@d
    move-object v4, p3

    #@e
    move v5, p4

    #@f
    move-object v6, p5

    #@10
    move-object v7, p6

    #@11
    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@14
    .line 902
    return-void
.end method

.method protected dispatchBroadcastCmasMessage(Landroid/telephony/SmsCbMessage;)V
    .registers 4
    .parameter "message"

    #@0
    .prologue
    .line 3809
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "com.lge.provider.Telephony.LGE_CMAS_RECEIVED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 3810
    .local v0, broadcastIntent:Landroid/content/Intent;
    const-string v1, "message"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@c
    .line 3811
    const-string v1, "dispatchBroadcastCmasMessage(), Dispatching LGE CMAS CB"

    #@e
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@11
    .line 3812
    const-string v1, "android.permission.RECEIVE_SMS"

    #@13
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@16
    .line 3813
    return-void
.end method

.method protected dispatchBroadcastMessage(Landroid/telephony/SmsCbMessage;)V
    .registers 5
    .parameter "message"

    #@0
    .prologue
    .line 3787
    invoke-virtual {p1}, Landroid/telephony/SmsCbMessage;->isEmergencyMessage()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_2b

    #@6
    .line 3789
    const-string v1, "US"

    #@8
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_12

    #@e
    .line 3790
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchBroadcastCmasMessage(Landroid/telephony/SmsCbMessage;)V

    #@11
    .line 3805
    :goto_11
    return-void

    #@12
    .line 3792
    :cond_12
    new-instance v0, Landroid/content/Intent;

    #@14
    const-string v1, "android.provider.Telephony.SMS_EMERGENCY_CB_RECEIVED"

    #@16
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@19
    .line 3793
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "message"

    #@1b
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@1e
    .line 3794
    const-string v1, "SMS"

    #@20
    const-string v2, "Dispatching emergency SMS CB"

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 3795
    const-string v1, "android.permission.RECEIVE_EMERGENCY_BROADCAST"

    #@27
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@2a
    goto :goto_11

    #@2b
    .line 3799
    .end local v0           #intent:Landroid/content/Intent;
    :cond_2b
    new-instance v0, Landroid/content/Intent;

    #@2d
    const-string v1, "android.provider.Telephony.SMS_CB_RECEIVED"

    #@2f
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@32
    .line 3800
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v1, "message"

    #@34
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@37
    .line 3801
    const-string v1, "SMS"

    #@39
    const-string v2, "Dispatching SMS CB"

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 3802
    const-string v1, "dispatchBroadcastMessage(), UI <-- SMSDispatcher"

    #@40
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@43
    .line 3803
    const-string v1, "android.permission.RECEIVE_SMS"

    #@45
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@48
    goto :goto_11
.end method

.method protected dispatchDirectedSms(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "packageName"
    .parameter "parameters"

    #@0
    .prologue
    .line 2334
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "verizon.intent.action.DIRECTED_SMS_RECEIVED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2335
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@a
    .line 2336
    const-string v1, "parameters"

    #@c
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@f
    .line 2337
    const-string v1, "android.permission.RECEIVE_SMS"

    #@11
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@14
    .line 2338
    return-void
.end method

.method public dispatchEx(Landroid/content/Intent;Ljava/lang/String;)V
    .registers 10
    .parameter "intent"
    .parameter "permission"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2310
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "dispatchEx(), ["

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    const-string v1, "] dispatchEx: "

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, "intent: "

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    const-string v1, ", permission: "

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@39
    .line 2312
    const-string v0, "format"

    #@3b
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@42
    .line 2313
    const/4 v4, -0x1

    #@43
    move-object v0, p0

    #@44
    move-object v1, p1

    #@45
    move-object v2, p2

    #@46
    move-object v3, p0

    #@47
    move-object v6, v5

    #@48
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/SMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@4b
    .line 2315
    return-void
.end method

.method protected dispatchImsInfo(I)V
    .registers 6
    .parameter "regi"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4996
    const-string v1, "kddi_domain_notification"

    #@3
    invoke-static {v3, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v1

    #@7
    const/4 v2, 0x1

    #@8
    if-ne v1, v2, :cond_42

    #@a
    .line 4997
    new-instance v0, Landroid/content/Intent;

    #@c
    const-string v1, "com.lge.kddi.intent.action.IMS_REGI_INFO"

    #@e
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@11
    .line 4998
    .local v0, mIntent:Landroid/content/Intent;
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "cdmaDanStatusReportReceiver, [KDDI][DAN] DAN Send Success, send Intent : "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, " regi status = "

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@35
    .line 4999
    const-string v1, "state"

    #@37
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3a
    .line 5000
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@3c
    invoke-virtual {v1, v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@3f
    .line 5001
    const/4 v1, 0x0

    #@40
    sput-boolean v1, Lcom/android/internal/telephony/SMSDispatcher;->RECEIVE_DAN_SUCCESS:Z

    #@42
    .line 5003
    .end local v0           #mIntent:Landroid/content/Intent;
    :cond_42
    return-void
.end method

.method protected abstract dispatchMessage(Lcom/android/internal/telephony/SmsMessageBase;)I
.end method

.method protected dispatchNormalMessage(Lcom/android/internal/telephony/SmsMessageBase;)I
    .registers 28
    .parameter "sms"

    #@0
    .prologue
    .line 1346
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@3
    move-result-object v24

    #@4
    .line 1349
    .local v24, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    if-eqz v24, :cond_c

    #@6
    move-object/from16 v0, v24

    #@8
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@a
    if-nez v3, :cond_212

    #@c
    .line 1351
    :cond_c
    const/4 v3, 0x1

    #@d
    new-array v0, v3, [[B

    #@f
    move-object/from16 v19, v0

    #@11
    .line 1352
    .local v19, pdus:[[B
    const/4 v3, 0x0

    #@12
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getPdu()[B

    #@15
    move-result-object v4

    #@16
    aput-object v4, v19, v3

    #@18
    .line 1354
    if-eqz v24, :cond_8e

    #@1a
    move-object/from16 v0, v24

    #@1c
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@1e
    if-eqz v3, :cond_8e

    #@20
    .line 1356
    move-object/from16 v0, p0

    #@22
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@24
    const-string v4, "OperatorMessage"

    #@26
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@29
    move-result v3

    #@2a
    const/4 v4, 0x1

    #@2b
    if-ne v3, v4, :cond_49

    #@2d
    .line 1357
    const/16 v21, 0x5

    #@2f
    .line 1359
    .local v21, result:I
    move-object/from16 v0, v24

    #@31
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@33
    if-nez v3, :cond_47

    #@35
    const/4 v3, 0x0

    #@36
    :goto_36
    const/4 v4, 0x0

    #@37
    move-object/from16 v0, p0

    #@39
    move-object/from16 v1, v19

    #@3b
    move-object/from16 v2, p1

    #@3d
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/SMSDispatcher;->processOperatorMessage([[BLcom/android/internal/telephony/SmsMessageBase;ZZ)I

    #@40
    move-result v21

    #@41
    .line 1361
    const/4 v3, 0x5

    #@42
    move/from16 v0, v21

    #@44
    if-eq v0, v3, :cond_49

    #@46
    .line 1487
    .end local v19           #pdus:[[B
    .end local v21           #result:I
    :cond_46
    :goto_46
    return v21

    #@47
    .line 1359
    .restart local v19       #pdus:[[B
    .restart local v21       #result:I
    :cond_47
    const/4 v3, 0x1

    #@48
    goto :goto_36

    #@49
    .line 1366
    .end local v21           #result:I
    :cond_49
    move-object/from16 v0, v24

    #@4b
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@4d
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@4f
    const/16 v4, 0xb84

    #@51
    if-ne v3, v4, :cond_79

    #@53
    .line 1369
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_WAPSERVICE:Z

    #@55
    if-eqz v3, :cond_6c

    #@57
    .line 1370
    move-object/from16 v0, p0

    #@59
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@5b
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getUserData()[B

    #@5e
    move-result-object v4

    #@5f
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getServiceCenterAddress()Ljava/lang/String;

    #@62
    move-result-object v5

    #@63
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@66
    move-result-object v6

    #@67
    invoke-virtual {v3, v4, v5, v6}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([BLjava/lang/String;Ljava/lang/String;)I

    #@6a
    move-result v21

    #@6b
    goto :goto_46

    #@6c
    .line 1375
    :cond_6c
    move-object/from16 v0, p0

    #@6e
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@70
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getUserData()[B

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([B)I

    #@77
    move-result v21

    #@78
    goto :goto_46

    #@79
    .line 1378
    :cond_79
    move-object/from16 v0, v24

    #@7b
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@7d
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@7f
    move-object/from16 v0, p0

    #@81
    move-object/from16 v1, v19

    #@83
    invoke-virtual {v0, v1, v3}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPortAddressedPdus([[BI)V

    #@86
    .line 1462
    :goto_86
    const-string v3, "dispatchNormalMessage(), UI <-- SMSDispatcher"

    #@88
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@8b
    .line 1463
    const/16 v21, -0x1

    #@8d
    goto :goto_46

    #@8e
    .line 1382
    :cond_8e
    move-object/from16 v0, p0

    #@90
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@92
    const-string v4, "OperatorMessage"

    #@94
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@97
    move-result v3

    #@98
    const/4 v4, 0x1

    #@99
    if-ne v3, v4, :cond_ae

    #@9b
    .line 1383
    const/16 v21, 0x5

    #@9d
    .line 1384
    .restart local v21       #result:I
    const/4 v3, 0x0

    #@9e
    const/4 v4, 0x0

    #@9f
    move-object/from16 v0, p0

    #@a1
    move-object/from16 v1, v19

    #@a3
    move-object/from16 v2, p1

    #@a5
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/SMSDispatcher;->processOperatorMessage([[BLcom/android/internal/telephony/SmsMessageBase;ZZ)I

    #@a8
    move-result v21

    #@a9
    .line 1385
    const/4 v3, 0x5

    #@aa
    move/from16 v0, v21

    #@ac
    if-ne v0, v3, :cond_46

    #@ae
    .line 1392
    .end local v21           #result:I
    :cond_ae
    move-object/from16 v0, p0

    #@b0
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@b2
    const-string v4, "sprint_reassembly_sms"

    #@b4
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@b7
    move-result v3

    #@b8
    if-eqz v3, :cond_11c

    #@ba
    .line 1393
    move-object/from16 v0, p0

    #@bc
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@be
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@c1
    move-result-object v3

    #@c2
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c5
    move-result-object v3

    #@c6
    const-string v4, "lg_sms_setting_reassembly"

    #@c8
    const/4 v5, 0x1

    #@c9
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@cc
    move-result v3

    #@cd
    if-nez v3, :cond_11a

    #@cf
    const/4 v15, 0x0

    #@d0
    .line 1396
    .local v15, bMessageReassembly:Z
    :goto_d0
    move-object/from16 v0, p0

    #@d2
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@d4
    const-string v4, "sprint_not_support_reassembly_sms_setting_menu"

    #@d6
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@d9
    move-result v3

    #@da
    if-eqz v3, :cond_dd

    #@dc
    .line 1397
    const/4 v15, 0x1

    #@dd
    .line 1401
    :cond_dd
    new-instance v3, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    const-string v4, "dispatchNormalMessage(), MessageReassembly setting = "

    #@e4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v3

    #@e8
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v3

    #@ec
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ef
    move-result-object v3

    #@f0
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@f3
    .line 1403
    if-eqz v15, :cond_11c

    #@f5
    .line 1406
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/SMSDispatcher;->processMessageSegment(Lcom/android/internal/telephony/SmsMessageBase;)I

    #@f8
    move-result v22

    #@f9
    .line 1407
    .local v22, segmentResult:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@fb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@fe
    const-string v4, "dispatchNormalMessage(), processMessageSegment result = "

    #@100
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v3

    #@104
    move/from16 v0, v22

    #@106
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@109
    move-result-object v3

    #@10a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10d
    move-result-object v3

    #@10e
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@111
    .line 1408
    const/4 v3, 0x2

    #@112
    move/from16 v0, v22

    #@114
    if-eq v0, v3, :cond_11c

    #@116
    move/from16 v21, v22

    #@118
    .line 1409
    goto/16 :goto_46

    #@11a
    .line 1393
    .end local v15           #bMessageReassembly:Z
    .end local v22           #segmentResult:I
    :cond_11a
    const/4 v15, 0x1

    #@11b
    goto :goto_d0

    #@11c
    .line 1415
    :cond_11c
    const/4 v3, 0x0

    #@11d
    const-string v4, "wifi_off_emergency_received"

    #@11f
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@122
    move-result v3

    #@123
    const/4 v4, 0x1

    #@124
    if-ne v3, v4, :cond_195

    #@126
    .line 1416
    move-object/from16 v0, p1

    #@128
    instance-of v3, v0, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@12a
    if-eqz v3, :cond_195

    #@12c
    .line 1418
    const-string v3, "dispatchNormalMessage(), [KDDI] WIFI OFF SmsMessage is cdma instance "

    #@12e
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@131
    move-object/from16 v25, p1

    #@133
    .line 1419
    check-cast v25, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@135
    .line 1420
    .local v25, wifi_off_sms:Lcom/android/internal/telephony/cdma/SmsMessage;
    invoke-virtual/range {v25 .. v25}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSmsEnvelope()Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@138
    move-result-object v3

    #@139
    iget v0, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@13b
    move/from16 v23, v0

    #@13d
    .line 1421
    .local v23, serviceCategory:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@13f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@142
    const-string v4, "dispatchNormalMessage(), [KDDI] Service category is : "

    #@144
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v3

    #@148
    move/from16 v0, v23

    #@14a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v3

    #@14e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@151
    move-result-object v3

    #@152
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@155
    .line 1422
    const/4 v3, 0x1

    #@156
    move/from16 v0, v23

    #@158
    if-eq v0, v3, :cond_166

    #@15a
    const/16 v3, 0x26

    #@15c
    move/from16 v0, v23

    #@15e
    if-eq v0, v3, :cond_166

    #@160
    const/16 v3, 0x28

    #@162
    move/from16 v0, v23

    #@164
    if-ne v0, v3, :cond_195

    #@166
    .line 1424
    :cond_166
    new-instance v3, Ljava/lang/StringBuilder;

    #@168
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16b
    const-string v4, "dispatchNormalMessage(), [KDDI] Service category  "

    #@16d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v3

    #@171
    move/from16 v0, v23

    #@173
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@176
    move-result-object v3

    #@177
    const-string v4, " broadcast to WIFI!! "

    #@179
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v3

    #@17d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@180
    move-result-object v3

    #@181
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@184
    .line 1425
    new-instance v18, Landroid/content/Intent;

    #@186
    const-string v3, "android.intent.action.SMS_WIFI_OFF"

    #@188
    move-object/from16 v0, v18

    #@18a
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@18d
    .line 1426
    .local v18, intent:Landroid/content/Intent;
    const/4 v3, 0x0

    #@18e
    move-object/from16 v0, p0

    #@190
    move-object/from16 v1, v18

    #@192
    invoke-virtual {v0, v1, v3}, Lcom/android/internal/telephony/SMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@195
    .line 1433
    .end local v18           #intent:Landroid/content/Intent;
    .end local v23           #serviceCategory:I
    .end local v25           #wifi_off_sms:Lcom/android/internal/telephony/cdma/SmsMessage;
    :cond_195
    move-object/from16 v0, p0

    #@197
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@199
    const-string v4, "seperate_processing_sms_uicc"

    #@19b
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@19e
    move-result v3

    #@19f
    if-eqz v3, :cond_1b2

    #@1a1
    .line 1434
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getIndexOnIcc()I

    #@1a4
    move-result v3

    #@1a5
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1a8
    move-result-object v3

    #@1a9
    move-object/from16 v0, p0

    #@1ab
    move-object/from16 v1, v19

    #@1ad
    invoke-virtual {v0, v1, v3}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[BLjava/lang/String;)V

    #@1b0
    goto/16 :goto_86

    #@1b2
    .line 1436
    :cond_1b2
    const/4 v3, 0x0

    #@1b3
    const-string v4, "kddi_message_duplicate_check"

    #@1b5
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1b8
    move-result v3

    #@1b9
    const/4 v4, 0x1

    #@1ba
    if-ne v3, v4, :cond_209

    #@1bc
    .line 1437
    move-object/from16 v0, p1

    #@1be
    instance-of v3, v0, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@1c0
    if-eqz v3, :cond_1db

    #@1c2
    move-object/from16 v3, p1

    #@1c4
    .line 1438
    check-cast v3, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@1c6
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSmsEnvelope()Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@1c9
    move-result-object v3

    #@1ca
    iget v3, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@1cc
    const/4 v4, 0x1

    #@1cd
    if-ne v3, v4, :cond_1db

    #@1cf
    .line 1441
    const-string v3, "dispatchNormalMessage(), [KDDI] Broadcast Message!!, No duplicate check"

    #@1d1
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1d4
    .line 1442
    move-object/from16 v0, p0

    #@1d6
    move-object/from16 v1, v19

    #@1d8
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[B)V

    #@1db
    .line 1447
    :cond_1db
    move-object/from16 v0, p1

    #@1dd
    iget v3, v0, Lcom/android/internal/telephony/SmsMessageBase;->messageRef:I

    #@1df
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getTimestampMillis()J

    #@1e2
    move-result-wide v4

    #@1e3
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1e6
    move-result-object v4

    #@1e7
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@1ea
    move-result-object v5

    #@1eb
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getMessageBody()Ljava/lang/String;

    #@1ee
    move-result-object v6

    #@1ef
    move-object/from16 v0, p0

    #@1f1
    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/android/internal/telephony/SMSDispatcher;->checkDuplicateKddiMessage(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Z

    #@1f4
    move-result v17

    #@1f5
    .line 1448
    .local v17, discard:Z
    if-eqz v17, :cond_200

    #@1f7
    .line 1450
    const-string v3, "dispatchNormalMessage(), [KDDI] discard duplicate Message "

    #@1f9
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1fc
    .line 1451
    const/16 v21, 0x1

    #@1fe
    goto/16 :goto_46

    #@200
    .line 1453
    :cond_200
    move-object/from16 v0, p0

    #@202
    move-object/from16 v1, v19

    #@204
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[B)V

    #@207
    goto/16 :goto_86

    #@209
    .line 1459
    .end local v17           #discard:Z
    :cond_209
    move-object/from16 v0, p0

    #@20b
    move-object/from16 v1, v19

    #@20d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[B)V

    #@210
    goto/16 :goto_86

    #@212
    .line 1466
    .end local v19           #pdus:[[B
    :cond_212
    move-object/from16 v0, v24

    #@214
    iget-object v0, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@216
    move-object/from16 v16, v0

    #@218
    .line 1467
    .local v16, concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    move-object/from16 v0, v24

    #@21a
    iget-object v0, v0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@21c
    move-object/from16 v20, v0

    #@21e
    .line 1474
    .local v20, portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    const/4 v14, -0x1

    #@21f
    .line 1475
    .local v14, indexOnIcc:I
    move-object/from16 v0, p0

    #@221
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@223
    const-string v4, "seperate_processing_sms_uicc"

    #@225
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@228
    move-result v3

    #@229
    if-eqz v3, :cond_22f

    #@22b
    .line 1476
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getIndexOnIcc()I

    #@22e
    move-result v14

    #@22f
    .line 1479
    :cond_22f
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_WAPSERVICE:Z

    #@231
    if-eqz v3, :cond_260

    #@233
    .line 1480
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getPdu()[B

    #@236
    move-result-object v4

    #@237
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@23a
    move-result-object v5

    #@23b
    move-object/from16 v0, v16

    #@23d
    iget v6, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@23f
    move-object/from16 v0, v16

    #@241
    iget v7, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@243
    move-object/from16 v0, v16

    #@245
    iget v8, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@247
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getTimestampMillis()J

    #@24a
    move-result-wide v9

    #@24b
    if-eqz v20, :cond_25e

    #@24d
    move-object/from16 v0, v20

    #@24f
    iget v11, v0, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@251
    :goto_251
    const/4 v12, 0x0

    #@252
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getServiceCenterAddress()Ljava/lang/String;

    #@255
    move-result-object v13

    #@256
    move-object/from16 v3, p0

    #@258
    invoke-virtual/range {v3 .. v14}, Lcom/android/internal/telephony/SMSDispatcher;->processMessagePart([BLjava/lang/String;IIIJIZLjava/lang/String;I)I

    #@25b
    move-result v21

    #@25c
    goto/16 :goto_46

    #@25e
    :cond_25e
    const/4 v11, -0x1

    #@25f
    goto :goto_251

    #@260
    .line 1487
    :cond_260
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getPdu()[B

    #@263
    move-result-object v4

    #@264
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@267
    move-result-object v5

    #@268
    move-object/from16 v0, v16

    #@26a
    iget v6, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@26c
    move-object/from16 v0, v16

    #@26e
    iget v7, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@270
    move-object/from16 v0, v16

    #@272
    iget v8, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@274
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getTimestampMillis()J

    #@277
    move-result-wide v9

    #@278
    if-eqz v20, :cond_288

    #@27a
    move-object/from16 v0, v20

    #@27c
    iget v11, v0, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@27e
    :goto_27e
    const/4 v12, 0x0

    #@27f
    const/4 v13, 0x0

    #@280
    move-object/from16 v3, p0

    #@282
    invoke-virtual/range {v3 .. v14}, Lcom/android/internal/telephony/SMSDispatcher;->processMessagePart([BLjava/lang/String;IIIJIZLjava/lang/String;I)I

    #@285
    move-result v21

    #@286
    goto/16 :goto_46

    #@288
    :cond_288
    const/4 v11, -0x1

    #@289
    goto :goto_27e
.end method

.method protected dispatchOperatorMessage(Lcom/android/internal/telephony/SmsMessageBase;Ljava/util/ArrayList;)I
    .registers 11
    .parameter "sms"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/SmsMessageBase;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/SmsOperatorBasicMessage;",
            ">;)I"
        }
    .end annotation

    #@0
    .prologue
    .local p2, operatorMessageList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/SmsOperatorBasicMessage;>;"
    const/4 v5, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    .line 4951
    if-nez p1, :cond_5

    #@4
    .line 4991
    :goto_4
    return v5

    #@5
    .line 4955
    :cond_5
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .local v1, i$:Ljava/util/Iterator;
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v7

    #@d
    if-eqz v7, :cond_31

    #@f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@15
    .line 4956
    .local v2, operatorMessage:Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    invoke-interface {v2}, Lcom/android/internal/telephony/SmsOperatorBasicMessage;->getInformation()Landroid/os/Bundle;

    #@18
    move-result-object v4

    #@19
    .line 4957
    .local v4, value:Landroid/os/Bundle;
    if-eqz v4, :cond_9

    #@1b
    .line 4960
    const-string v7, "valid"

    #@1d
    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@20
    move-result v7

    #@21
    if-ne v7, v6, :cond_2f

    #@23
    .line 4961
    invoke-interface {v2, p0}, Lcom/android/internal/telephony/SmsOperatorBasicMessage;->dispatch(Lcom/android/internal/telephony/SMSDispatcher;)I

    #@26
    move-result v0

    #@27
    .line 4962
    .local v0, dispatchResult:I
    if-eqz v0, :cond_9

    #@29
    .line 4964
    if-ne v0, v5, :cond_2d

    #@2b
    .line 4965
    const/4 v5, -0x1

    #@2c
    goto :goto_4

    #@2d
    :cond_2d
    move v5, v6

    #@2e
    .line 4967
    goto :goto_4

    #@2f
    .end local v0           #dispatchResult:I
    :cond_2f
    move v5, v6

    #@30
    .line 4970
    goto :goto_4

    #@31
    .line 4979
    .end local v2           #operatorMessage:Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    .end local v4           #value:Landroid/os/Bundle;
    :cond_31
    invoke-virtual {p1}, Lcom/android/internal/telephony/SmsMessageBase;->getProtocolIdentifier()I

    #@34
    move-result v5

    #@35
    and-int/lit16 v3, v5, 0xff

    #@37
    .line 4980
    .local v3, pid_byte:I
    const/16 v5, 0x49

    #@39
    if-lt v3, v5, :cond_3f

    #@3b
    const/16 v5, 0x5d

    #@3d
    if-le v3, v5, :cond_65

    #@3f
    :cond_3f
    const/16 v5, 0x60

    #@41
    if-lt v3, v5, :cond_47

    #@43
    const/16 v5, 0x7b

    #@45
    if-le v3, v5, :cond_65

    #@47
    :cond_47
    const/16 v5, 0x80

    #@49
    if-lt v3, v5, :cond_4f

    #@4b
    const/16 v5, 0xbf

    #@4d
    if-le v3, v5, :cond_65

    #@4f
    :cond_4f
    const/16 v5, 0x7d

    #@51
    if-ne v3, v5, :cond_7d

    #@53
    const-string v5, "KT"

    #@55
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@58
    move-result v5

    #@59
    if-ne v5, v6, :cond_7d

    #@5b
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@5d
    const-string v7, "KTFotaMessage"

    #@5f
    invoke-static {v5, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@62
    move-result v5

    #@63
    if-eq v5, v6, :cond_7d

    #@65
    .line 4987
    :cond_65
    new-instance v5, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v7, "dispatchOperatorMessage(), message discard : [reserved] "

    #@6c
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v5

    #@70
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v5

    #@74
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v5

    #@78
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7b
    move v5, v6

    #@7c
    .line 4988
    goto :goto_4

    #@7d
    .line 4991
    :cond_7d
    const/4 v5, 0x5

    #@7e
    goto :goto_4
.end method

.method protected dispatchPdus([[B)V
    .registers 5
    .parameter "pdus"

    #@0
    .prologue
    .line 2285
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2286
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "pdus"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@c
    .line 2287
    const-string v1, "format"

    #@e
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@15
    .line 2288
    const-string v1, "android.permission.RECEIVE_SMS"

    #@17
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@1a
    .line 2289
    return-void
.end method

.method protected dispatchPdus([[BLjava/lang/String;)V
    .registers 6
    .parameter "pdus"
    .parameter "indexOnIcc"

    #@0
    .prologue
    .line 2262
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "dispatchPdus(), indexOnIcc: "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 2263
    new-instance v0, Landroid/content/Intent;

    #@18
    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    #@1a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1d
    .line 2264
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "pdus"

    #@1f
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@22
    .line 2265
    const-string v1, "format"

    #@24
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2b
    .line 2266
    const-string v1, "indexOnIcc"

    #@2d
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@30
    .line 2267
    const-string v1, "android.permission.RECEIVE_SMS"

    #@32
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@35
    .line 2268
    return-void
.end method

.method protected dispatchPortAddressedPdus([[BI)V
    .registers 7
    .parameter "pdus"
    .parameter "port"

    #@0
    .prologue
    .line 2325
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "sms://localhost:"

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@16
    move-result-object v1

    #@17
    .line 2326
    .local v1, uri:Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    #@19
    const-string v2, "android.intent.action.DATA_SMS_RECEIVED"

    #@1b
    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@1e
    .line 2327
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "pdus"

    #@20
    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@23
    .line 2328
    const-string v2, "format"

    #@25
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2c
    .line 2329
    const-string v2, "android.permission.RECEIVE_SMS"

    #@2e
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/telephony/SMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@31
    .line 2330
    return-void
.end method

.method protected dispatchSncSms([[B)V
    .registers 4
    .parameter "pdus"

    #@0
    .prologue
    .line 2343
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "com.lge.snc.SMS_RECEIVED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2344
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "pdus"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@c
    .line 2345
    const-string v1, "android.permission.RECEIVE_SMS"

    #@e
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@11
    .line 2346
    return-void
.end method

.method public abstract dispose()V
.end method

.method protected enableAutoDCDisconnect(I)V
    .registers 2
    .parameter "timeOut"

    #@0
    .prologue
    .line 3968
    return-void
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 581
    const-string v0, "SMS"

    #@2
    const-string v1, "SMSDispatcher finalized"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 582
    return-void
.end method

.method protected getDisplayOfCauseCode(J)I
    .registers 8
    .parameter "causeCode"

    #@0
    .prologue
    .line 1277
    const/16 v1, 0x33

    #@2
    .line 1278
    .local v1, displayIndex:I
    const/4 v0, 0x0

    #@3
    .local v0, arrayIndex:I
    :goto_3
    const/16 v3, 0x33

    #@5
    if-ge v0, v3, :cond_2e

    #@7
    .line 1279
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->causeCodeArray:[J

    #@9
    aget-wide v3, v3, v0

    #@b
    cmp-long v3, p1, v3

    #@d
    if-nez v3, :cond_2b

    #@f
    .line 1280
    if-ltz v0, :cond_17

    #@11
    const/4 v3, 0x7

    #@12
    if-gt v0, v3, :cond_17

    #@14
    .line 1281
    const/4 v1, 0x1

    #@15
    :goto_15
    move v2, v1

    #@16
    .line 1294
    .end local v1           #displayIndex:I
    .local v2, displayIndex:I
    :goto_16
    return v2

    #@17
    .line 1282
    .end local v2           #displayIndex:I
    .restart local v1       #displayIndex:I
    :cond_17
    const/16 v3, 0x1a

    #@19
    if-ne v0, v3, :cond_1d

    #@1b
    .line 1283
    const/4 v1, 0x2

    #@1c
    goto :goto_15

    #@1d
    .line 1284
    :cond_1d
    const/16 v3, 0x1d

    #@1f
    if-ne v0, v3, :cond_23

    #@21
    .line 1285
    const/4 v1, 0x3

    #@22
    goto :goto_15

    #@23
    .line 1286
    :cond_23
    const/16 v3, 0x23

    #@25
    if-ne v0, v3, :cond_29

    #@27
    .line 1287
    const/4 v1, 0x4

    #@28
    goto :goto_15

    #@29
    .line 1289
    :cond_29
    const/4 v1, 0x0

    #@2a
    goto :goto_15

    #@2b
    .line 1278
    :cond_2b
    add-int/lit8 v0, v0, 0x1

    #@2d
    goto :goto_3

    #@2e
    :cond_2e
    move v2, v1

    #@2f
    .line 1294
    .end local v1           #displayIndex:I
    .restart local v2       #displayIndex:I
    goto :goto_16
.end method

.method protected abstract getFormat()Ljava/lang/String;
.end method

.method public abstract getImsSmsFormat()Ljava/lang/String;
.end method

.method public getLine1Number()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3963
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getLine1Number()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPremiumSmsPermission(Ljava/lang/String;)I
    .registers 3
    .parameter "packageName"

    #@0
    .prologue
    .line 3403
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SmsUsageMonitor;->getPremiumSmsPermission(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected handleConfirmShortCode(ZLcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 21
    .parameter "isPremium"
    .parameter "tracker"

    #@0
    .prologue
    .line 3341
    move-object/from16 v0, p0

    #@2
    move-object/from16 v1, p2

    #@4
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->denyIfQueueLimitReached(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)Z

    #@7
    move-result v14

    #@8
    if-eqz v14, :cond_b

    #@a
    .line 3390
    :goto_a
    return-void

    #@b
    .line 3346
    :cond_b
    if-eqz p1, :cond_d8

    #@d
    .line 3347
    const v4, 0x104044a

    #@10
    .line 3352
    .local v4, detailsId:I
    :goto_10
    move-object/from16 v0, p2

    #@12
    iget-object v14, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mAppInfo:Landroid/content/pm/PackageInfo;

    #@14
    iget-object v14, v14, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@16
    move-object/from16 v0, p0

    #@18
    invoke-direct {v0, v14}, Lcom/android/internal/telephony/SMSDispatcher;->getAppLabel(Ljava/lang/String;)Ljava/lang/CharSequence;

    #@1b
    move-result-object v2

    #@1c
    .line 3353
    .local v2, appLabel:Ljava/lang/CharSequence;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@1f
    move-result-object v12

    #@20
    .line 3354
    .local v12, r:Landroid/content/res/Resources;
    const v14, 0x1040448

    #@23
    const/4 v15, 0x2

    #@24
    new-array v15, v15, [Ljava/lang/Object;

    #@26
    const/16 v16, 0x0

    #@28
    aput-object v2, v15, v16

    #@2a
    const/16 v16, 0x1

    #@2c
    move-object/from16 v0, p2

    #@2e
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mDestAddress:Ljava/lang/String;

    #@30
    move-object/from16 v17, v0

    #@32
    aput-object v17, v15, v16

    #@34
    invoke-virtual {v12, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@37
    move-result-object v14

    #@38
    invoke-static {v14}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    #@3b
    move-result-object v10

    #@3c
    .line 3357
    .local v10, messageText:Landroid/text/Spanned;
    move-object/from16 v0, p0

    #@3e
    iget-object v14, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@40
    const-string v15, "layout_inflater"

    #@42
    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@45
    move-result-object v7

    #@46
    check-cast v7, Landroid/view/LayoutInflater;

    #@48
    .line 3359
    .local v7, inflater:Landroid/view/LayoutInflater;
    const v14, 0x10900ce

    #@4b
    const/4 v15, 0x0

    #@4c
    invoke-virtual {v7, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@4f
    move-result-object v8

    #@50
    .line 3361
    .local v8, layout:Landroid/view/View;
    new-instance v9, Lcom/android/internal/telephony/SMSDispatcher$ConfirmDialogListener;

    #@52
    const v14, 0x1020387

    #@55
    invoke-virtual {v8, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@58
    move-result-object v14

    #@59
    check-cast v14, Landroid/widget/TextView;

    #@5b
    move-object/from16 v0, p0

    #@5d
    move-object/from16 v1, p2

    #@5f
    invoke-direct {v9, v0, v1, v14}, Lcom/android/internal/telephony/SMSDispatcher$ConfirmDialogListener;-><init>(Lcom/android/internal/telephony/SMSDispatcher;Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;Landroid/widget/TextView;)V

    #@62
    .line 3365
    .local v9, listener:Lcom/android/internal/telephony/SMSDispatcher$ConfirmDialogListener;
    const v14, 0x1020381

    #@65
    invoke-virtual {v8, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@68
    move-result-object v11

    #@69
    check-cast v11, Landroid/widget/TextView;

    #@6b
    .line 3366
    .local v11, messageView:Landroid/widget/TextView;
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@6e
    .line 3368
    const v14, 0x1020382

    #@71
    invoke-virtual {v8, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@74
    move-result-object v5

    #@75
    check-cast v5, Landroid/view/ViewGroup;

    #@77
    .line 3370
    .local v5, detailsLayout:Landroid/view/ViewGroup;
    const v14, 0x1020384

    #@7a
    invoke-virtual {v5, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@7d
    move-result-object v6

    #@7e
    check-cast v6, Landroid/widget/TextView;

    #@80
    .line 3372
    .local v6, detailsView:Landroid/widget/TextView;
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(I)V

    #@83
    .line 3374
    const v14, 0x1020385

    #@86
    invoke-virtual {v8, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@89
    move-result-object v13

    #@8a
    check-cast v13, Landroid/widget/CheckBox;

    #@8c
    .line 3376
    .local v13, rememberChoice:Landroid/widget/CheckBox;
    invoke-virtual {v13, v9}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    #@8f
    .line 3378
    new-instance v14, Landroid/app/AlertDialog$Builder;

    #@91
    move-object/from16 v0, p0

    #@93
    iget-object v15, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@95
    invoke-direct {v14, v15}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@98
    invoke-virtual {v14, v8}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    #@9b
    move-result-object v14

    #@9c
    const v15, 0x104044b

    #@9f
    invoke-virtual {v12, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@a2
    move-result-object v15

    #@a3
    invoke-virtual {v14, v15, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@a6
    move-result-object v14

    #@a7
    const v15, 0x104044c

    #@aa
    invoke-virtual {v12, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ad
    move-result-object v15

    #@ae
    invoke-virtual {v14, v15, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@b1
    move-result-object v14

    #@b2
    invoke-virtual {v14, v9}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@b5
    move-result-object v14

    #@b6
    invoke-virtual {v14}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@b9
    move-result-object v3

    #@ba
    .line 3385
    .local v3, d:Landroid/app/AlertDialog;
    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@bd
    move-result-object v14

    #@be
    const/16 v15, 0x7d3

    #@c0
    invoke-virtual {v14, v15}, Landroid/view/Window;->setType(I)V

    #@c3
    .line 3386
    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    #@c6
    .line 3388
    const/4 v14, -0x1

    #@c7
    invoke-virtual {v3, v14}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    #@ca
    move-result-object v14

    #@cb
    invoke-virtual {v9, v14}, Lcom/android/internal/telephony/SMSDispatcher$ConfirmDialogListener;->setPositiveButton(Landroid/widget/Button;)V

    #@ce
    .line 3389
    const/4 v14, -0x2

    #@cf
    invoke-virtual {v3, v14}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    #@d2
    move-result-object v14

    #@d3
    invoke-virtual {v9, v14}, Lcom/android/internal/telephony/SMSDispatcher$ConfirmDialogListener;->setNegativeButton(Landroid/widget/Button;)V

    #@d6
    goto/16 :goto_a

    #@d8
    .line 3349
    .end local v2           #appLabel:Ljava/lang/CharSequence;
    .end local v3           #d:Landroid/app/AlertDialog;
    .end local v4           #detailsId:I
    .end local v5           #detailsLayout:Landroid/view/ViewGroup;
    .end local v6           #detailsView:Landroid/widget/TextView;
    .end local v7           #inflater:Landroid/view/LayoutInflater;
    .end local v8           #layout:Landroid/view/View;
    .end local v9           #listener:Lcom/android/internal/telephony/SMSDispatcher$ConfirmDialogListener;
    .end local v10           #messageText:Landroid/text/Spanned;
    .end local v11           #messageView:Landroid/widget/TextView;
    .end local v12           #r:Landroid/content/res/Resources;
    .end local v13           #rememberChoice:Landroid/widget/CheckBox;
    :cond_d8
    const v4, 0x1040449

    #@db
    .restart local v4       #detailsId:I
    goto/16 :goto_10
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 26
    .parameter "msg"

    #@0
    .prologue
    .line 605
    move-object/from16 v0, p1

    #@2
    iget v0, v0, Landroid/os/Message;->what:I

    #@4
    move/from16 v21, v0

    #@6
    packed-switch v21, :pswitch_data_52e

    #@9
    .line 851
    :cond_9
    :goto_9
    :pswitch_9
    return-void

    #@a
    .line 612
    :pswitch_a
    move-object/from16 v0, p0

    #@c
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@e
    move-object/from16 v21, v0

    #@10
    const-string v22, "sms_permission_tracking"

    #@12
    invoke-static/range {v21 .. v22}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@15
    move-result v21

    #@16
    if-eqz v21, :cond_6b

    #@18
    .line 613
    move-object/from16 v0, p0

    #@1a
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@1c
    move-object/from16 v21, v0

    #@1e
    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@21
    move-result-object v14

    #@22
    .line 614
    .local v14, pm:Landroid/content/pm/PackageManager;
    new-instance v21, Landroid/content/Intent;

    #@24
    const-string v22, "android.provider.Telephony.SMS_RECEIVED"

    #@26
    invoke-direct/range {v21 .. v22}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@29
    const/16 v22, 0x0

    #@2b
    move-object/from16 v0, v21

    #@2d
    move/from16 v1, v22

    #@2f
    invoke-virtual {v14, v0, v1}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    #@32
    move-result-object v13

    #@33
    .line 616
    .local v13, pkgList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    #@36
    move-result v21

    #@37
    if-nez v21, :cond_6b

    #@39
    .line 617
    const/4 v9, 0x0

    #@3a
    .local v9, i:I
    :goto_3a
    invoke-interface {v13}, Ljava/util/List;->size()I

    #@3d
    move-result v21

    #@3e
    move/from16 v0, v21

    #@40
    if-ge v9, v0, :cond_6b

    #@42
    .line 618
    new-instance v21, Ljava/lang/StringBuilder;

    #@44
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v22, "handleMessage(),[Permission Check] SMS_RECV Activity : "

    #@49
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v22

    #@4d
    invoke-interface {v13, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@50
    move-result-object v21

    #@51
    check-cast v21, Landroid/content/pm/ResolveInfo;

    #@53
    move-object/from16 v0, v21

    #@55
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@57
    move-object/from16 v21, v0

    #@59
    move-object/from16 v0, v22

    #@5b
    move-object/from16 v1, v21

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v21

    #@61
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v21

    #@65
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@68
    .line 617
    add-int/lit8 v9, v9, 0x1

    #@6a
    goto :goto_3a

    #@6b
    .line 625
    .end local v9           #i:I
    .end local v13           #pkgList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v14           #pm:Landroid/content/pm/PackageManager;
    :cond_6b
    move-object/from16 v0, p1

    #@6d
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6f
    check-cast v4, Landroid/os/AsyncResult;

    #@71
    .line 627
    .local v4, ar:Landroid/os/AsyncResult;
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@73
    move-object/from16 v21, v0

    #@75
    if-eqz v21, :cond_95

    #@77
    .line 628
    const-string v21, "SMS"

    #@79
    new-instance v22, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v23, "Exception processing incoming SMS. Exception:"

    #@80
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v22

    #@84
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@86
    move-object/from16 v23, v0

    #@88
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v22

    #@8c
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v22

    #@90
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    goto/16 :goto_9

    #@95
    .line 632
    :cond_95
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@97
    move-object/from16 v17, v0

    #@99
    check-cast v17, Landroid/telephony/SmsMessage;

    #@9b
    .line 634
    .local v17, sms:Landroid/telephony/SmsMessage;
    move-object/from16 v0, p0

    #@9d
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@9f
    move-object/from16 v21, v0

    #@a1
    const-string v22, "cdma_sms_cdg2"

    #@a3
    invoke-static/range {v21 .. v22}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a6
    move-result v21

    #@a7
    if-eqz v21, :cond_154

    #@a9
    .line 635
    new-instance v21, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    const-string v22, "handleMessage(), CDMA SMS CDG2 Test mode"

    #@b0
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v21

    #@b4
    move-object/from16 v0, p0

    #@b6
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@b8
    move-object/from16 v22, v0

    #@ba
    const-string v23, "cdma_sms_cdg2"

    #@bc
    invoke-static/range {v22 .. v23}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@bf
    move-result v22

    #@c0
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v21

    #@c4
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v21

    #@c8
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@cb
    .line 636
    invoke-static {}, Landroid/telephony/SmsMessage;->isCdmaVoice()Z

    #@ce
    move-result v21

    #@cf
    const/16 v22, 0x1

    #@d1
    move/from16 v0, v21

    #@d3
    move/from16 v1, v22

    #@d5
    if-ne v0, v1, :cond_154

    #@d7
    .line 637
    move-object/from16 v0, p0

    #@d9
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@db
    move-object/from16 v21, v0

    #@dd
    const-string v22, "sms_over_lgims"

    #@df
    invoke-static/range {v21 .. v22}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e2
    move-result v21

    #@e3
    if-eqz v21, :cond_1c7

    #@e5
    .line 638
    new-instance v21, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v22, "handleMessage(), KEY_SMS_OVER_LGIMS: "

    #@ec
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v21

    #@f0
    move-object/from16 v0, p0

    #@f2
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@f4
    move-object/from16 v22, v0

    #@f6
    const-string v23, "sms_over_lgims"

    #@f8
    invoke-static/range {v22 .. v23}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@fb
    move-result v22

    #@fc
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v21

    #@100
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v21

    #@104
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@107
    .line 639
    const-string v21, "persist.radio.sms_ims"

    #@109
    const-string v22, "false"

    #@10b
    invoke-static/range {v21 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@10e
    move-result-object v11

    #@10f
    .line 640
    .local v11, mImsRegi:Ljava/lang/String;
    const-string v21, "false"

    #@111
    move-object/from16 v0, v21

    #@113
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@116
    move-result v21

    #@117
    if-eqz v21, :cond_1c1

    #@119
    .line 641
    const-string v21, "handleMessage(), IMS is not registered!Execute SMS CDG2 Test mode"

    #@11b
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@11e
    .line 642
    move-object/from16 v0, p0

    #@120
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@122
    move-object/from16 v21, v0

    #@124
    const/16 v22, 0x1

    #@126
    invoke-virtual/range {v21 .. v22}, Lcom/android/internal/telephony/SmsStorageMonitor;->setStorageAvailableStatus(Z)V

    #@129
    .line 643
    move-object/from16 v0, v17

    #@12b
    iget-object v0, v0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@12d
    move-object/from16 v18, v0

    #@12f
    check-cast v18, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@131
    .line 644
    .local v18, smsCdma:Lcom/android/internal/telephony/cdma/SmsMessage;
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleService()I

    #@134
    move-result v19

    #@135
    .line 646
    .local v19, teleService:I
    const/16 v21, 0x1002

    #@137
    move/from16 v0, v19

    #@139
    move/from16 v1, v21

    #@13b
    if-ne v0, v1, :cond_154

    #@13d
    .line 647
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->getSMSInboxMessageCount()I

    #@140
    move-result v21

    #@141
    const/16 v22, 0x32

    #@143
    move/from16 v0, v21

    #@145
    move/from16 v1, v22

    #@147
    if-lt v0, v1, :cond_154

    #@149
    .line 648
    move-object/from16 v0, p0

    #@14b
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@14d
    move-object/from16 v21, v0

    #@14f
    const/16 v22, 0x0

    #@151
    invoke-virtual/range {v21 .. v22}, Lcom/android/internal/telephony/SmsStorageMonitor;->setStorageAvailableStatus(Z)V

    #@154
    .line 673
    .end local v11           #mImsRegi:Ljava/lang/String;
    .end local v18           #smsCdma:Lcom/android/internal/telephony/cdma/SmsMessage;
    .end local v19           #teleService:I
    :cond_154
    :goto_154
    const/4 v15, 0x1

    #@155
    .line 674
    .local v15, result:I
    const/4 v10, 0x0

    #@156
    .line 676
    .local v10, isDDMMessage:Z
    :try_start_156
    move-object/from16 v0, p0

    #@158
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    #@15a
    move-object/from16 v21, v0

    #@15c
    if-nez v21, :cond_169

    #@15e
    .line 677
    new-instance v21, Lcom/carrieriq/iqagent/client/IQClient;

    #@160
    invoke-direct/range {v21 .. v21}, Lcom/carrieriq/iqagent/client/IQClient;-><init>()V

    #@163
    move-object/from16 v0, v21

    #@165
    move-object/from16 v1, p0

    #@167
    iput-object v0, v1, Lcom/android/internal/telephony/SMSDispatcher;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    #@169
    .line 679
    :cond_169
    move-object/from16 v0, p0

    #@16b
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    #@16d
    move-object/from16 v21, v0

    #@16f
    invoke-virtual/range {v17 .. v17}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@172
    move-result-object v22

    #@173
    invoke-virtual/range {v21 .. v22}, Lcom/carrieriq/iqagent/client/IQClient;->checkSMS(Ljava/lang/String;)Z

    #@176
    move-result v21

    #@177
    if-eqz v21, :cond_17a

    #@179
    .line 680
    const/4 v10, 0x1

    #@17a
    .line 684
    :cond_17a
    if-nez v10, :cond_18a

    #@17c
    .line 685
    move-object/from16 v0, v17

    #@17e
    iget-object v0, v0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@180
    move-object/from16 v21, v0

    #@182
    move-object/from16 v0, p0

    #@184
    move-object/from16 v1, v21

    #@186
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchMessage(Lcom/android/internal/telephony/SmsMessageBase;)I

    #@189
    move-result v15

    #@18a
    .line 689
    :cond_18a
    const/16 v21, -0x1

    #@18c
    move/from16 v0, v21

    #@18e
    if-eq v15, v0, :cond_9

    #@190
    .line 692
    const/16 v21, 0x1

    #@192
    move/from16 v0, v21

    #@194
    if-ne v15, v0, :cond_221

    #@196
    const/4 v8, 0x1

    #@197
    .line 693
    .local v8, handled:Z
    :goto_197
    const/16 v21, 0x0

    #@199
    move-object/from16 v0, p0

    #@19b
    move-object/from16 v1, v21

    #@19d
    invoke-direct {v0, v8, v15, v1}, Lcom/android/internal/telephony/SMSDispatcher;->notifyAndAcknowledgeLastIncomingSms(ZILandroid/os/Message;)V
    :try_end_1a0
    .catch Ljava/lang/RuntimeException; {:try_start_156 .. :try_end_1a0} :catch_1a2

    #@1a0
    goto/16 :goto_9

    #@1a2
    .line 695
    .end local v8           #handled:Z
    :catch_1a2
    move-exception v7

    #@1a3
    .line 696
    .local v7, ex:Ljava/lang/RuntimeException;
    const-string v21, "SMS"

    #@1a5
    const-string v22, "Exception dispatching message"

    #@1a7
    move-object/from16 v0, v21

    #@1a9
    move-object/from16 v1, v22

    #@1ab
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1ae
    .line 697
    const/16 v21, 0x0

    #@1b0
    const/16 v22, 0x2

    #@1b2
    const/16 v23, 0x0

    #@1b4
    move-object/from16 v0, p0

    #@1b6
    move/from16 v1, v21

    #@1b8
    move/from16 v2, v22

    #@1ba
    move-object/from16 v3, v23

    #@1bc
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/SMSDispatcher;->notifyAndAcknowledgeLastIncomingSms(ZILandroid/os/Message;)V

    #@1bf
    goto/16 :goto_9

    #@1c1
    .line 652
    .end local v7           #ex:Ljava/lang/RuntimeException;
    .end local v10           #isDDMMessage:Z
    .end local v15           #result:I
    .restart local v11       #mImsRegi:Ljava/lang/String;
    :cond_1c1
    const-string v21, "handleMessage(), IMS is registered!Ignore SMS CDG2 Test mode"

    #@1c3
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1c6
    goto :goto_154

    #@1c7
    .line 655
    .end local v11           #mImsRegi:Ljava/lang/String;
    :cond_1c7
    new-instance v21, Ljava/lang/StringBuilder;

    #@1c9
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@1cc
    const-string v22, "handleMessage(), KEY_SMS_OVER_LGIMS: "

    #@1ce
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v21

    #@1d2
    move-object/from16 v0, p0

    #@1d4
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@1d6
    move-object/from16 v22, v0

    #@1d8
    const-string v23, "sms_over_lgims"

    #@1da
    invoke-static/range {v22 .. v23}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1dd
    move-result v22

    #@1de
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e1
    move-result-object v21

    #@1e2
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e5
    move-result-object v21

    #@1e6
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1e9
    .line 656
    move-object/from16 v0, p0

    #@1eb
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@1ed
    move-object/from16 v21, v0

    #@1ef
    const/16 v22, 0x1

    #@1f1
    invoke-virtual/range {v21 .. v22}, Lcom/android/internal/telephony/SmsStorageMonitor;->setStorageAvailableStatus(Z)V

    #@1f4
    .line 657
    move-object/from16 v0, v17

    #@1f6
    iget-object v0, v0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@1f8
    move-object/from16 v18, v0

    #@1fa
    check-cast v18, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@1fc
    .line 658
    .restart local v18       #smsCdma:Lcom/android/internal/telephony/cdma/SmsMessage;
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleService()I

    #@1ff
    move-result v19

    #@200
    .line 660
    .restart local v19       #teleService:I
    const/16 v21, 0x1002

    #@202
    move/from16 v0, v19

    #@204
    move/from16 v1, v21

    #@206
    if-ne v0, v1, :cond_154

    #@208
    .line 661
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->getSMSInboxMessageCount()I

    #@20b
    move-result v21

    #@20c
    const/16 v22, 0x32

    #@20e
    move/from16 v0, v21

    #@210
    move/from16 v1, v22

    #@212
    if-lt v0, v1, :cond_154

    #@214
    .line 662
    move-object/from16 v0, p0

    #@216
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@218
    move-object/from16 v21, v0

    #@21a
    const/16 v22, 0x0

    #@21c
    invoke-virtual/range {v21 .. v22}, Lcom/android/internal/telephony/SmsStorageMonitor;->setStorageAvailableStatus(Z)V

    #@21f
    goto/16 :goto_154

    #@221
    .line 692
    .end local v18           #smsCdma:Lcom/android/internal/telephony/cdma/SmsMessage;
    .end local v19           #teleService:I
    .restart local v10       #isDDMMessage:Z
    .restart local v15       #result:I
    :cond_221
    const/4 v8, 0x0

    #@222
    goto/16 :goto_197

    #@224
    .line 704
    .end local v4           #ar:Landroid/os/AsyncResult;
    .end local v10           #isDDMMessage:Z
    .end local v15           #result:I
    .end local v17           #sms:Landroid/telephony/SmsMessage;
    :pswitch_224
    move-object/from16 v0, p1

    #@226
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@228
    move-object/from16 v21, v0

    #@22a
    check-cast v21, Landroid/os/AsyncResult;

    #@22c
    move-object/from16 v0, p0

    #@22e
    move-object/from16 v1, v21

    #@230
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->handleSendComplete(Landroid/os/AsyncResult;)V

    #@233
    goto/16 :goto_9

    #@235
    .line 709
    :pswitch_235
    new-instance v12, Landroid/content/Intent;

    #@237
    const-string v21, "com.lge.kddi.intent.action.DAN_SENT_OK"

    #@239
    move-object/from16 v0, v21

    #@23b
    invoke-direct {v12, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@23e
    .line 710
    .local v12, mIntent:Landroid/content/Intent;
    new-instance v21, Ljava/lang/StringBuilder;

    #@240
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@243
    const-string v22, "cdmaDanStatusReportReceiver, [KDDI][DAN] DAN Send Success, send Intent : "

    #@245
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@248
    move-result-object v21

    #@249
    invoke-virtual {v12}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@24c
    move-result-object v22

    #@24d
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@250
    move-result-object v21

    #@251
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@254
    move-result-object v21

    #@255
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@258
    .line 711
    move-object/from16 v0, p0

    #@25a
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@25c
    move-object/from16 v21, v0

    #@25e
    const/16 v22, 0x0

    #@260
    move-object/from16 v0, v21

    #@262
    move-object/from16 v1, v22

    #@264
    invoke-virtual {v0, v12, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@267
    .line 712
    const/16 v21, 0x1

    #@269
    sput-boolean v21, Lcom/android/internal/telephony/SMSDispatcher;->RECEIVE_DAN_SUCCESS:Z

    #@26b
    goto/16 :goto_9

    #@26d
    .line 718
    .end local v12           #mIntent:Landroid/content/Intent;
    :pswitch_26d
    const-string v21, "handleMessage(), EVENT_SEND_RETRY_WITHPOPUP Received!!"

    #@26f
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@272
    .line 719
    move-object/from16 v0, p1

    #@274
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@276
    move-object/from16 v21, v0

    #@278
    check-cast v21, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@27a
    check-cast v21, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@27c
    move-object/from16 v0, p0

    #@27e
    move-object/from16 v1, v21

    #@280
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->handleRetryByOption(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@283
    goto/16 :goto_9

    #@285
    .line 724
    :pswitch_285
    const-string v21, "handleMessage(), SMS retry.."

    #@287
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@28a
    .line 725
    move-object/from16 v0, p1

    #@28c
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@28e
    move-object/from16 v21, v0

    #@290
    check-cast v21, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@292
    move-object/from16 v0, p0

    #@294
    move-object/from16 v1, v21

    #@296
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->sendRetrySms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@299
    goto/16 :goto_9

    #@29b
    .line 730
    :pswitch_29b
    const-string v21, "handleMessage(), EVENT_RETRY_ALERT_TIMEOUT Received!!"

    #@29d
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2a0
    .line 731
    move-object/from16 v0, p1

    #@2a2
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2a4
    move-object/from16 v21, v0

    #@2a6
    check-cast v21, Landroid/app/AlertDialog;

    #@2a8
    check-cast v21, Landroid/app/AlertDialog;

    #@2aa
    invoke-virtual/range {v21 .. v21}, Landroid/app/AlertDialog;->dismiss()V

    #@2ad
    .line 732
    const/16 v21, 0x0

    #@2af
    move-object/from16 v0, v21

    #@2b1
    move-object/from16 v1, p1

    #@2b3
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2b5
    .line 733
    move-object/from16 v0, p0

    #@2b7
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@2b9
    move-object/from16 v21, v0

    #@2bb
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->isEmpty()Z

    #@2be
    move-result v21

    #@2bf
    if-nez v21, :cond_309

    #@2c1
    .line 734
    move-object/from16 v0, p0

    #@2c3
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@2c5
    move-object/from16 v21, v0

    #@2c7
    const/16 v22, 0x0

    #@2c9
    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@2cc
    move-result-object v20

    #@2cd
    check-cast v20, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@2cf
    .line 735
    .local v20, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    sget v21, Lcom/android/internal/telephony/SMSDispatcher;->MAX_SEND_RETRIES:I

    #@2d1
    move/from16 v0, v21

    #@2d3
    move-object/from16 v1, v20

    #@2d5
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@2d7
    .line 737
    new-instance v5, Landroid/os/AsyncResult;

    #@2d9
    const/16 v21, 0x0

    #@2db
    const/16 v22, 0x0

    #@2dd
    move-object/from16 v0, v20

    #@2df
    move-object/from16 v1, v21

    #@2e1
    move-object/from16 v2, v22

    #@2e3
    invoke-direct {v5, v0, v1, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@2e6
    .line 738
    .local v5, asyncres:Landroid/os/AsyncResult;
    new-instance v6, Lcom/android/internal/telephony/CommandException;

    #@2e8
    sget-object v21, Lcom/android/internal/telephony/CommandException$Error;->SMS_FAIL_RETRY:Lcom/android/internal/telephony/CommandException$Error;

    #@2ea
    move-object/from16 v0, v21

    #@2ec
    invoke-direct {v6, v0}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@2ef
    .line 739
    .local v6, commandExcep:Lcom/android/internal/telephony/CommandException;
    iput-object v6, v5, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2f1
    .line 743
    const/16 v21, 0x2

    #@2f3
    move-object/from16 v0, p0

    #@2f5
    move/from16 v1, v21

    #@2f7
    invoke-virtual {v0, v1, v5}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2fa
    move-result-object v21

    #@2fb
    move-object/from16 v0, p0

    #@2fd
    move-object/from16 v1, v21

    #@2ff
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessage(Landroid/os/Message;)Z

    #@302
    .line 745
    const-string v21, "handleMessage(), [SMS.MO.RETRY] Case EVENT_RETRY_ALERT_TIMEOUT, ok!!"

    #@304
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@307
    goto/16 :goto_9

    #@309
    .line 748
    .end local v5           #asyncres:Landroid/os/AsyncResult;
    .end local v6           #commandExcep:Lcom/android/internal/telephony/CommandException;
    .end local v20           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :cond_309
    const-string v21, "handleMessage(), [SMS.MO.RETRY] Case EVENT_RETRY_ALERT_TIMEOUT, mSTrackersForRetry empty!!"

    #@30b
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@30e
    goto/16 :goto_9

    #@310
    .line 753
    :pswitch_310
    const-string v21, "handleMessage(), EVENT_SEND_RETRY_CONFIRMED_SMS Received!!"

    #@312
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@315
    .line 754
    move-object/from16 v0, p0

    #@317
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@319
    move-object/from16 v21, v0

    #@31b
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->isEmpty()Z

    #@31e
    move-result v21

    #@31f
    if-nez v21, :cond_9

    #@321
    .line 755
    new-instance v21, Ljava/lang/StringBuilder;

    #@323
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@326
    const-string v22, "handleMessage(), [SMS.MO.RETRY] mSTrackersForRetry.size()=["

    #@328
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32b
    move-result-object v21

    #@32c
    move-object/from16 v0, p0

    #@32e
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@330
    move-object/from16 v22, v0

    #@332
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    #@335
    move-result v22

    #@336
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@339
    move-result-object v21

    #@33a
    const-string v22, "] "

    #@33c
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33f
    move-result-object v21

    #@340
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@343
    move-result-object v21

    #@344
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@347
    .line 756
    move-object/from16 v0, p0

    #@349
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@34b
    move-object/from16 v21, v0

    #@34d
    move-object/from16 v0, p0

    #@34f
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@351
    move-object/from16 v22, v0

    #@353
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    #@356
    move-result v22

    #@357
    add-int/lit8 v22, v22, -0x1

    #@359
    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@35c
    move-result-object v16

    #@35d
    check-cast v16, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@35f
    .line 757
    .local v16, sTracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    move-object/from16 v0, v16

    #@361
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@363
    move/from16 v21, v0

    #@365
    add-int/lit8 v21, v21, 0x1

    #@367
    move/from16 v0, v21

    #@369
    move-object/from16 v1, v16

    #@36b
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@36d
    .line 758
    new-instance v21, Ljava/lang/StringBuilder;

    #@36f
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@372
    const-string v22, "handleMessage(), [SMS.MO.RETRY] sTracker.mRetryCount=["

    #@374
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@377
    move-result-object v21

    #@378
    move-object/from16 v0, v16

    #@37a
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@37c
    move/from16 v22, v0

    #@37e
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@381
    move-result-object v21

    #@382
    const-string v22, "] "

    #@384
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@387
    move-result-object v21

    #@388
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38b
    move-result-object v21

    #@38c
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@38f
    .line 759
    move-object/from16 v0, p0

    #@391
    move-object/from16 v1, v16

    #@393
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->sendRetrySms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@396
    .line 760
    const/16 v21, 0x10

    #@398
    move-object/from16 v0, p1

    #@39a
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@39c
    move-object/from16 v22, v0

    #@39e
    move-object/from16 v0, p0

    #@3a0
    move/from16 v1, v21

    #@3a2
    move-object/from16 v2, v22

    #@3a4
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->removeMessages(ILjava/lang/Object;)V

    #@3a7
    goto/16 :goto_9

    #@3a9
    .line 765
    .end local v16           #sTracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :pswitch_3a9
    const-string v21, "handleMessage(), EVENT_STOP_RETRY_SENDING Received!!"

    #@3ab
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@3ae
    .line 767
    move-object/from16 v0, p0

    #@3b0
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@3b2
    move-object/from16 v21, v0

    #@3b4
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->isEmpty()Z

    #@3b7
    move-result v21

    #@3b8
    if-nez v21, :cond_9

    #@3ba
    .line 768
    const-string v21, "handleMessage(), Case EVENT_STOP_RETRY_SENDING called..."

    #@3bc
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@3bf
    .line 769
    move-object/from16 v0, p0

    #@3c1
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@3c3
    move-object/from16 v21, v0

    #@3c5
    move-object/from16 v0, p0

    #@3c7
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@3c9
    move-object/from16 v22, v0

    #@3cb
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    #@3ce
    move-result v22

    #@3cf
    add-int/lit8 v22, v22, -0x1

    #@3d1
    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@3d4
    move-result-object v16

    #@3d5
    check-cast v16, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@3d7
    .line 770
    .restart local v16       #sTracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    sget v21, Lcom/android/internal/telephony/SMSDispatcher;->MAX_SEND_RETRIES:I

    #@3d9
    move/from16 v0, v21

    #@3db
    move-object/from16 v1, v16

    #@3dd
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@3df
    .line 772
    new-instance v5, Landroid/os/AsyncResult;

    #@3e1
    const/16 v21, 0x0

    #@3e3
    const/16 v22, 0x0

    #@3e5
    move-object/from16 v0, v16

    #@3e7
    move-object/from16 v1, v21

    #@3e9
    move-object/from16 v2, v22

    #@3eb
    invoke-direct {v5, v0, v1, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@3ee
    .line 773
    .restart local v5       #asyncres:Landroid/os/AsyncResult;
    new-instance v6, Lcom/android/internal/telephony/CommandException;

    #@3f0
    sget-object v21, Lcom/android/internal/telephony/CommandException$Error;->SMS_FAIL_RETRY:Lcom/android/internal/telephony/CommandException$Error;

    #@3f2
    move-object/from16 v0, v21

    #@3f4
    invoke-direct {v6, v0}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@3f7
    .line 774
    .restart local v6       #commandExcep:Lcom/android/internal/telephony/CommandException;
    iput-object v6, v5, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3f9
    .line 778
    const/16 v21, 0x2

    #@3fb
    move-object/from16 v0, p0

    #@3fd
    move/from16 v1, v21

    #@3ff
    invoke-virtual {v0, v1, v5}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@402
    move-result-object v21

    #@403
    move-object/from16 v0, p0

    #@405
    move-object/from16 v1, v21

    #@407
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessage(Landroid/os/Message;)Z

    #@40a
    .line 779
    const/16 v21, 0x10

    #@40c
    move-object/from16 v0, p1

    #@40e
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@410
    move-object/from16 v22, v0

    #@412
    move-object/from16 v0, p0

    #@414
    move/from16 v1, v21

    #@416
    move-object/from16 v2, v22

    #@418
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->removeMessages(ILjava/lang/Object;)V

    #@41b
    goto/16 :goto_9

    #@41d
    .line 784
    .end local v5           #asyncres:Landroid/os/AsyncResult;
    .end local v6           #commandExcep:Lcom/android/internal/telephony/CommandException;
    .end local v16           #sTracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :pswitch_41d
    move-object/from16 v0, p1

    #@41f
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@421
    move-object/from16 v21, v0

    #@423
    check-cast v21, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@425
    check-cast v21, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@427
    move-object/from16 v0, p0

    #@429
    move-object/from16 v1, v21

    #@42b
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->handleReachSentLimit(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@42e
    goto/16 :goto_9

    #@430
    .line 788
    :pswitch_430
    const/16 v22, 0x0

    #@432
    move-object/from16 v0, p1

    #@434
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@436
    move-object/from16 v21, v0

    #@438
    check-cast v21, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@43a
    check-cast v21, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@43c
    move-object/from16 v0, p0

    #@43e
    move/from16 v1, v22

    #@440
    move-object/from16 v2, v21

    #@442
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->handleConfirmShortCode(ZLcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@445
    goto/16 :goto_9

    #@447
    .line 792
    :pswitch_447
    const/16 v22, 0x1

    #@449
    move-object/from16 v0, p1

    #@44b
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@44d
    move-object/from16 v21, v0

    #@44f
    check-cast v21, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@451
    check-cast v21, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@453
    move-object/from16 v0, p0

    #@455
    move/from16 v1, v22

    #@457
    move-object/from16 v2, v21

    #@459
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->handleConfirmShortCode(ZLcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@45c
    goto/16 :goto_9

    #@45e
    .line 797
    :pswitch_45e
    move-object/from16 v0, p1

    #@460
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@462
    move-object/from16 v20, v0

    #@464
    check-cast v20, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@466
    .line 806
    .restart local v20       #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    move-object/from16 v0, p0

    #@468
    iget-boolean v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSyncronousSending:Z

    #@46a
    move/from16 v21, v0

    #@46c
    if-eqz v21, :cond_48e

    #@46e
    .line 807
    new-instance v21, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;

    #@470
    move-object/from16 v0, v21

    #@472
    move-object/from16 v1, v20

    #@474
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;-><init>(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@477
    move-object/from16 v0, p0

    #@479
    move-object/from16 v1, v21

    #@47b
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->enqueueMessageForSending(Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;)V

    #@47e
    .line 813
    :goto_47e
    move-object/from16 v0, p0

    #@480
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingTrackerCount:I

    #@482
    move/from16 v21, v0

    #@484
    add-int/lit8 v21, v21, -0x1

    #@486
    move/from16 v0, v21

    #@488
    move-object/from16 v1, p0

    #@48a
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher;->mPendingTrackerCount:I

    #@48c
    goto/16 :goto_9

    #@48e
    .line 809
    :cond_48e
    move-object/from16 v0, p0

    #@490
    move-object/from16 v1, v20

    #@492
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@495
    goto :goto_47e

    #@496
    .line 818
    .end local v20           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :pswitch_496
    move-object/from16 v0, p1

    #@498
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@49a
    check-cast v4, Landroid/os/AsyncResult;

    #@49c
    .line 819
    .restart local v4       #ar:Landroid/os/AsyncResult;
    if-eqz v4, :cond_9

    #@49e
    .line 821
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4a0
    move-object/from16 v21, v0

    #@4a2
    if-eqz v21, :cond_9

    #@4a4
    .line 822
    new-instance v21, Ljava/lang/StringBuilder;

    #@4a6
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@4a9
    const-string v22, "handleMessage(), MWI update on card failed "

    #@4ab
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ae
    move-result-object v21

    #@4af
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4b1
    move-object/from16 v22, v0

    #@4b3
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b6
    move-result-object v21

    #@4b7
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4ba
    move-result-object v21

    #@4bb
    invoke-static/range {v21 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@4be
    .line 823
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->storeVoiceMailCount()V

    #@4c1
    goto/16 :goto_9

    #@4c3
    .line 828
    .end local v4           #ar:Landroid/os/AsyncResult;
    :pswitch_4c3
    move-object/from16 v0, p1

    #@4c5
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4c7
    move-object/from16 v20, v0

    #@4c9
    check-cast v20, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@4cb
    .line 829
    .restart local v20       #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    move-object/from16 v0, v20

    #@4cd
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@4cf
    move-object/from16 v21, v0

    #@4d1
    if-eqz v21, :cond_4de

    #@4d3
    .line 831
    :try_start_4d3
    move-object/from16 v0, v20

    #@4d5
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@4d7
    move-object/from16 v21, v0

    #@4d9
    const/16 v22, 0x5

    #@4db
    invoke-virtual/range {v21 .. v22}, Landroid/app/PendingIntent;->send(I)V
    :try_end_4de
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_4d3 .. :try_end_4de} :catch_4ee

    #@4de
    .line 836
    :cond_4de
    :goto_4de
    move-object/from16 v0, p0

    #@4e0
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mPendingTrackerCount:I

    #@4e2
    move/from16 v21, v0

    #@4e4
    add-int/lit8 v21, v21, -0x1

    #@4e6
    move/from16 v0, v21

    #@4e8
    move-object/from16 v1, p0

    #@4ea
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher;->mPendingTrackerCount:I

    #@4ec
    goto/16 :goto_9

    #@4ee
    .line 832
    :catch_4ee
    move-exception v7

    #@4ef
    .line 833
    .local v7, ex:Landroid/app/PendingIntent$CanceledException;
    const-string v21, "SMS"

    #@4f1
    const-string v22, "failed to send RESULT_ERROR_LIMIT_EXCEEDED"

    #@4f3
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f6
    goto :goto_4de

    #@4f7
    .line 843
    .end local v7           #ex:Landroid/app/PendingIntent$CanceledException;
    .end local v20           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :pswitch_4f7
    move-object/from16 v0, p0

    #@4f9
    iget-boolean v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSyncronousSending:Z

    #@4fb
    move/from16 v21, v0

    #@4fd
    if-eqz v21, :cond_51d

    #@4ff
    .line 844
    new-instance v22, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;

    #@501
    move-object/from16 v0, p1

    #@503
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@505
    move-object/from16 v21, v0

    #@507
    check-cast v21, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@509
    sget-object v23, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;->MORE:Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;

    #@50b
    move-object/from16 v0, v22

    #@50d
    move-object/from16 v1, v21

    #@50f
    move-object/from16 v2, v23

    #@511
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;-><init>(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;)V

    #@514
    move-object/from16 v0, p0

    #@516
    move-object/from16 v1, v22

    #@518
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->enqueueMessageForSending(Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;)V

    #@51b
    goto/16 :goto_9

    #@51d
    .line 847
    :cond_51d
    move-object/from16 v0, p1

    #@51f
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@521
    move-object/from16 v21, v0

    #@523
    check-cast v21, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@525
    move-object/from16 v0, p0

    #@527
    move-object/from16 v1, v21

    #@529
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->sendSmsMore(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@52c
    goto/16 :goto_9

    #@52e
    .line 605
    :pswitch_data_52e
    .packed-switch 0x1
        :pswitch_a
        :pswitch_224
        :pswitch_285
        :pswitch_41d
        :pswitch_45e
        :pswitch_9
        :pswitch_4c3
        :pswitch_430
        :pswitch_447
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_29b
        :pswitch_310
        :pswitch_3a9
        :pswitch_26d
        :pswitch_496
        :pswitch_235
        :pswitch_4f7
    .end packed-switch
.end method

.method protected handleReachSentLimit(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 11
    .parameter "tracker"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 3278
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->denyIfQueueLimitReached(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)Z

    #@4
    move-result v5

    #@5
    if-eqz v5, :cond_8

    #@7
    .line 3302
    :goto_7
    return-void

    #@8
    .line 3282
    :cond_8
    iget-object v5, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mAppInfo:Landroid/content/pm/PackageInfo;

    #@a
    iget-object v5, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@c
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/SMSDispatcher;->getAppLabel(Ljava/lang/String;)Ljava/lang/CharSequence;

    #@f
    move-result-object v0

    #@10
    .line 3283
    .local v0, appLabel:Ljava/lang/CharSequence;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@13
    move-result-object v4

    #@14
    .line 3284
    .local v4, r:Landroid/content/res/Resources;
    const v5, 0x1040445

    #@17
    const/4 v6, 0x1

    #@18
    new-array v6, v6, [Ljava/lang/Object;

    #@1a
    aput-object v0, v6, v8

    #@1c
    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    #@23
    move-result-object v3

    #@24
    .line 3286
    .local v3, messageText:Landroid/text/Spanned;
    new-instance v2, Lcom/android/internal/telephony/SMSDispatcher$ConfirmDialogListener;

    #@26
    const/4 v5, 0x0

    #@27
    invoke-direct {v2, p0, p1, v5}, Lcom/android/internal/telephony/SMSDispatcher$ConfirmDialogListener;-><init>(Lcom/android/internal/telephony/SMSDispatcher;Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;Landroid/widget/TextView;)V

    #@2a
    .line 3288
    .local v2, listener:Lcom/android/internal/telephony/SMSDispatcher$ConfirmDialogListener;
    new-instance v5, Landroid/app/AlertDialog$Builder;

    #@2c
    iget-object v6, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@2e
    const/4 v7, 0x3

    #@2f
    invoke-direct {v5, v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@32
    const v6, 0x1040444

    #@35
    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@38
    move-result-object v5

    #@39
    const v6, 0x108008a

    #@3c
    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@43
    move-result-object v5

    #@44
    const v6, 0x1040446

    #@47
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v5, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@4e
    move-result-object v5

    #@4f
    const v6, 0x1040447

    #@52
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@55
    move-result-object v6

    #@56
    invoke-virtual {v5, v6, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    #@5d
    move-result-object v5

    #@5e
    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@65
    move-result-object v1

    #@66
    .line 3300
    .local v1, d:Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@69
    move-result-object v5

    #@6a
    const/16 v6, 0x7d3

    #@6c
    invoke-virtual {v5, v6}, Landroid/view/Window;->setType(I)V

    #@6f
    .line 3301
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@72
    goto :goto_7
.end method

.method protected handleRetryByOption(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 7
    .parameter "tracker"

    #@0
    .prologue
    .line 3306
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "handleRetryByOption(), [SMS.MO.RETRY] retry popup mSTrackersForRetry.size()=["

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v3

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "] "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@22
    .line 3307
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@27
    move-result v2

    #@28
    sget v3, Lcom/android/internal/telephony/SMSDispatcher;->MO_MSG_QUEUE_LIMIT:I

    #@2a
    if-lt v2, v3, :cond_36

    #@2c
    .line 3308
    const/16 v2, 0x12

    #@2e
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(I)Landroid/os/Message;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessage(Landroid/os/Message;)Z

    #@35
    .line 3332
    :goto_35
    return-void

    #@36
    .line 3311
    :cond_36
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@39
    move-result-object v1

    #@3a
    .line 3313
    .local v1, r:Landroid/content/res/Resources;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    #@3c
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@3e
    const/4 v4, 0x3

    #@3f
    invoke-direct {v2, v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@42
    const v3, 0x2090311

    #@45
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@4c
    move-result-object v2

    #@4d
    const v3, 0x2090312

    #@50
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@57
    move-result-object v2

    #@58
    const v3, 0x2090313

    #@5b
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    iget-object v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mRetryListener:Landroid/content/DialogInterface$OnClickListener;

    #@61
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@64
    move-result-object v2

    #@65
    const v3, 0x2090314

    #@68
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    iget-object v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mRetryListener:Landroid/content/DialogInterface$OnClickListener;

    #@6e
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@71
    move-result-object v2

    #@72
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@75
    move-result-object v0

    #@76
    .line 3324
    .local v0, d:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@79
    move-result-object v2

    #@7a
    const/16 v3, 0x7d3

    #@7c
    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    #@7f
    .line 3325
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@82
    .line 3327
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSTrackersForRetry:Ljava/util/ArrayList;

    #@84
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@87
    .line 3328
    const-string v2, "handleRetryByOption(), [SMS.MO.RETRY] handleRetryByOption: mSTrackersForRetry add..."

    #@89
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@8c
    .line 3330
    const/16 v2, 0x10

    #@8e
    invoke-virtual {p0, v2, v0}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@91
    move-result-object v2

    #@92
    const-wide/16 v3, 0x2710

    #@94
    invoke-virtual {p0, v2, v3, v4}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@97
    goto :goto_35
.end method

.method protected handleSendComplete(Landroid/os/AsyncResult;)V
    .registers 23
    .parameter "ar"

    #@0
    .prologue
    .line 931
    move-object/from16 v0, p1

    #@2
    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@4
    move-object/from16 v17, v0

    #@6
    check-cast v17, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@8
    .line 932
    .local v17, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    move-object/from16 v0, v17

    #@a
    iget-object v15, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@c
    .line 934
    .local v15, sentIntent:Landroid/app/PendingIntent;
    move-object/from16 v0, p1

    #@e
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@10
    move-object/from16 v18, v0

    #@12
    if-eqz v18, :cond_ac

    #@14
    .line 935
    move-object/from16 v0, p1

    #@16
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@18
    move-object/from16 v18, v0

    #@1a
    check-cast v18, Lcom/android/internal/telephony/SmsResponse;

    #@1c
    move-object/from16 v0, v18

    #@1e
    iget v0, v0, Lcom/android/internal/telephony/SmsResponse;->messageRef:I

    #@20
    move/from16 v18, v0

    #@22
    move/from16 v0, v18

    #@24
    move-object/from16 v1, v17

    #@26
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mMessageRef:I

    #@28
    .line 940
    :goto_28
    move-object/from16 v0, p1

    #@2a
    iget-object v0, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2c
    move-object/from16 v18, v0

    #@2e
    if-nez v18, :cond_cb

    #@30
    .line 946
    move-object/from16 v0, v17

    #@32
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mDeliveryIntent:Landroid/app/PendingIntent;

    #@34
    move-object/from16 v18, v0

    #@36
    if-eqz v18, :cond_56

    #@38
    .line 948
    const/16 v18, 0x0

    #@3a
    const-string v19, "kddi_receive_status_report_iwk"

    #@3c
    invoke-static/range {v18 .. v19}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3f
    move-result v18

    #@40
    const/16 v19, 0x1

    #@42
    move/from16 v0, v18

    #@44
    move/from16 v1, v19

    #@46
    if-ne v0, v1, :cond_b3

    #@48
    .line 949
    sget-object v18, Lcom/android/internal/telephony/SMSDispatcher;->deliveryPendingListForKddi:Ljava/util/ArrayList;

    #@4a
    move-object/from16 v0, v18

    #@4c
    move-object/from16 v1, v17

    #@4e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@51
    .line 950
    const-string v18, "handleSendComplete(),[KDDI] handleSendComplete()- tracker.mDeliveryIntent!=null)"

    #@53
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@56
    .line 959
    :cond_56
    :goto_56
    if-eqz v15, :cond_a0

    #@58
    .line 961
    :try_start_58
    move-object/from16 v0, p0

    #@5a
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@5c
    move/from16 v18, v0

    #@5e
    const/16 v19, -0x1

    #@60
    move/from16 v0, v18

    #@62
    move/from16 v1, v19

    #@64
    if-le v0, v1, :cond_74

    #@66
    .line 962
    move-object/from16 v0, p0

    #@68
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@6a
    move/from16 v18, v0

    #@6c
    add-int/lit8 v18, v18, -0x1

    #@6e
    move/from16 v0, v18

    #@70
    move-object/from16 v1, p0

    #@72
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@74
    .line 964
    :cond_74
    const-string v18, "handleSendComplete(), UI <-- SMSDispatcher"

    #@76
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@79
    .line 966
    move-object/from16 v0, p0

    #@7b
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@7d
    move/from16 v18, v0

    #@7f
    if-nez v18, :cond_c1

    #@81
    .line 967
    new-instance v14, Landroid/content/Intent;

    #@83
    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    #@86
    .line 968
    .local v14, sendNext:Landroid/content/Intent;
    const-string v18, "SendNextMsg"

    #@88
    const/16 v19, 0x1

    #@8a
    move-object/from16 v0, v18

    #@8c
    move/from16 v1, v19

    #@8e
    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@91
    .line 969
    move-object/from16 v0, p0

    #@93
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@95
    move-object/from16 v18, v0

    #@97
    const/16 v19, -0x1

    #@99
    move-object/from16 v0, v18

    #@9b
    move/from16 v1, v19

    #@9d
    invoke-virtual {v15, v0, v1, v14}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_a0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_58 .. :try_end_a0} :catch_c9

    #@a0
    .line 976
    .end local v14           #sendNext:Landroid/content/Intent;
    :cond_a0
    :goto_a0
    move-object/from16 v0, p0

    #@a2
    iget-boolean v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSyncronousSending:Z

    #@a4
    move/from16 v18, v0

    #@a6
    if-eqz v18, :cond_ab

    #@a8
    .line 977
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->processNextPendingMessage()V

    #@ab
    .line 1273
    :cond_ab
    :goto_ab
    return-void

    #@ac
    .line 937
    :cond_ac
    const-string v18, "handleSendComplete(), SmsResponse was null "

    #@ae
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@b1
    goto/16 :goto_28

    #@b3
    .line 954
    :cond_b3
    move-object/from16 v0, p0

    #@b5
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->deliveryPendingList:Ljava/util/ArrayList;

    #@b7
    move-object/from16 v18, v0

    #@b9
    move-object/from16 v0, v18

    #@bb
    move-object/from16 v1, v17

    #@bd
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c0
    goto :goto_56

    #@c1
    .line 971
    :cond_c1
    const/16 v18, -0x1

    #@c3
    :try_start_c3
    move/from16 v0, v18

    #@c5
    invoke-virtual {v15, v0}, Landroid/app/PendingIntent;->send(I)V
    :try_end_c8
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_c3 .. :try_end_c8} :catch_c9

    #@c8
    goto :goto_a0

    #@c9
    .line 973
    :catch_c9
    move-exception v18

    #@ca
    goto :goto_a0

    #@cb
    .line 985
    :cond_cb
    move-object/from16 v0, p0

    #@cd
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@cf
    move-object/from16 v18, v0

    #@d1
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@d4
    move-result-object v18

    #@d5
    invoke-virtual/range {v18 .. v18}, Landroid/telephony/ServiceState;->getState()I

    #@d8
    move-result v16

    #@d9
    .line 988
    .local v16, ss:I
    move-object/from16 v0, p0

    #@db
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@dd
    move-object/from16 v18, v0

    #@df
    const-string v19, "cdma_sms_cdg2"

    #@e1
    invoke-static/range {v18 .. v19}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e4
    move-result v18

    #@e5
    if-nez v18, :cond_50f

    #@e7
    .line 989
    move-object/from16 v0, v17

    #@e9
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@eb
    move/from16 v18, v0

    #@ed
    if-lez v18, :cond_15b

    #@ef
    if-eqz v16, :cond_15b

    #@f1
    .line 993
    sget v18, Lcom/android/internal/telephony/SMSDispatcher;->MAX_SEND_RETRIES:I

    #@f3
    move/from16 v0, v18

    #@f5
    move-object/from16 v1, v17

    #@f7
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@f9
    .line 995
    new-instance v18, Ljava/lang/StringBuilder;

    #@fb
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@fe
    const-string v19, "handleSendComplete(), Skipping retry:  isIms()="

    #@100
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v18

    #@104
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->isIms()Z

    #@107
    move-result v19

    #@108
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v18

    #@10c
    const-string v19, " mRetryCount="

    #@10e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v18

    #@112
    move-object/from16 v0, v17

    #@114
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@116
    move/from16 v19, v0

    #@118
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v18

    #@11c
    const-string v19, " mImsRetry="

    #@11e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v18

    #@122
    move-object/from16 v0, v17

    #@124
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@126
    move/from16 v19, v0

    #@128
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v18

    #@12c
    const-string v19, " mMessageRef="

    #@12e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v18

    #@132
    move-object/from16 v0, v17

    #@134
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mMessageRef:I

    #@136
    move/from16 v19, v0

    #@138
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v18

    #@13c
    const-string v19, " SS= "

    #@13e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v18

    #@142
    move-object/from16 v0, p0

    #@144
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@146
    move-object/from16 v19, v0

    #@148
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@14b
    move-result-object v19

    #@14c
    invoke-virtual/range {v19 .. v19}, Landroid/telephony/ServiceState;->getState()I

    #@14f
    move-result v19

    #@150
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@153
    move-result-object v18

    #@154
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@157
    move-result-object v18

    #@158
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@15b
    .line 1006
    :cond_15b
    const/16 v18, 0x1

    #@15d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->checkNotInService()Z

    #@160
    move-result v19

    #@161
    move/from16 v0, v18

    #@163
    move/from16 v1, v19

    #@165
    if-ne v0, v1, :cond_181

    #@167
    .line 1009
    move-object/from16 v0, p0

    #@169
    iget-boolean v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSyncronousSending:Z

    #@16b
    move/from16 v18, v0

    #@16d
    if-eqz v18, :cond_172

    #@16f
    .line 1010
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->dequeueFailedPendingMessage()V

    #@172
    .line 1013
    :cond_172
    move-object/from16 v0, v17

    #@174
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@176
    move-object/from16 v18, v0

    #@178
    move/from16 v0, v16

    #@17a
    move-object/from16 v1, v18

    #@17c
    invoke-static {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->handleNotInService(ILandroid/app/PendingIntent;)V

    #@17f
    goto/16 :goto_ab

    #@181
    .line 1014
    :cond_181
    move-object/from16 v0, p1

    #@183
    iget-object v0, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@185
    move-object/from16 v18, v0

    #@187
    check-cast v18, Lcom/android/internal/telephony/CommandException;

    #@189
    check-cast v18, Lcom/android/internal/telephony/CommandException;

    #@18b
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    #@18e
    move-result-object v18

    #@18f
    sget-object v19, Lcom/android/internal/telephony/CommandException$Error;->SMS_FAIL_RETRY:Lcom/android/internal/telephony/CommandException$Error;

    #@191
    move-object/from16 v0, v18

    #@193
    move-object/from16 v1, v19

    #@195
    if-ne v0, v1, :cond_24c

    #@197
    move-object/from16 v0, v17

    #@199
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@19b
    move/from16 v18, v0

    #@19d
    sget v19, Lcom/android/internal/telephony/SMSDispatcher;->MAX_SEND_RETRIES:I

    #@19f
    move/from16 v0, v18

    #@1a1
    move/from16 v1, v19

    #@1a3
    if-ge v0, v1, :cond_24c

    #@1a5
    .line 1026
    move-object/from16 v0, p0

    #@1a7
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@1a9
    move-object/from16 v18, v0

    #@1ab
    const-string v19, "vzw_sms_retry_scheme"

    #@1ad
    invoke-static/range {v18 .. v19}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1b0
    move-result v18

    #@1b1
    if-eqz v18, :cond_222

    #@1b3
    .line 1027
    if-eqz v17, :cond_206

    #@1b5
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->isMultipart()Z

    #@1b8
    move-result v18

    #@1b9
    if-eqz v18, :cond_206

    #@1bb
    .line 1029
    const-string v18, "handleSendComplete(), Send ReTry : This is Multipart Message"

    #@1bd
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1c0
    .line 1030
    move-object/from16 v0, v17

    #@1c2
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@1c4
    move/from16 v18, v0

    #@1c6
    add-int/lit8 v18, v18, 0x1

    #@1c8
    move/from16 v0, v18

    #@1ca
    move-object/from16 v1, v17

    #@1cc
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@1ce
    .line 1031
    const/16 v18, 0x3

    #@1d0
    move-object/from16 v0, p0

    #@1d2
    move/from16 v1, v18

    #@1d4
    move-object/from16 v2, v17

    #@1d6
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1d9
    move-result-object v12

    #@1da
    .line 1032
    .local v12, retryMsg:Landroid/os/Message;
    sget v18, Lcom/android/internal/telephony/SMSDispatcher;->SEND_RETRY_DELAY:I

    #@1dc
    move/from16 v0, v18

    #@1de
    int-to-long v0, v0

    #@1df
    move-wide/from16 v18, v0

    #@1e1
    move-object/from16 v0, p0

    #@1e3
    move-wide/from16 v1, v18

    #@1e5
    invoke-virtual {v0, v12, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1e8
    .line 1033
    new-instance v18, Ljava/lang/StringBuilder;

    #@1ea
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@1ed
    const-string v19, "handleSendComplete(), Send EVENT_SEND_RETRY mRetryCount: "

    #@1ef
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v18

    #@1f3
    move-object/from16 v0, v17

    #@1f5
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@1f7
    move/from16 v19, v0

    #@1f9
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v18

    #@1fd
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@200
    move-result-object v18

    #@201
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@204
    goto/16 :goto_ab

    #@206
    .line 1035
    .end local v12           #retryMsg:Landroid/os/Message;
    :cond_206
    const-string v18, "handleSendComplete(), Send ReTry : This is Not Multipart Message"

    #@208
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@20b
    .line 1036
    const/16 v18, 0x13

    #@20d
    move-object/from16 v0, p0

    #@20f
    move/from16 v1, v18

    #@211
    move-object/from16 v2, v17

    #@213
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@216
    move-result-object v12

    #@217
    .line 1037
    .restart local v12       #retryMsg:Landroid/os/Message;
    const-wide/16 v18, 0x64

    #@219
    move-object/from16 v0, p0

    #@21b
    move-wide/from16 v1, v18

    #@21d
    invoke-virtual {v0, v12, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@220
    goto/16 :goto_ab

    #@222
    .line 1040
    .end local v12           #retryMsg:Landroid/os/Message;
    :cond_222
    move-object/from16 v0, v17

    #@224
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@226
    move/from16 v18, v0

    #@228
    add-int/lit8 v18, v18, 0x1

    #@22a
    move/from16 v0, v18

    #@22c
    move-object/from16 v1, v17

    #@22e
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@230
    .line 1041
    const/16 v18, 0x3

    #@232
    move-object/from16 v0, p0

    #@234
    move/from16 v1, v18

    #@236
    move-object/from16 v2, v17

    #@238
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@23b
    move-result-object v12

    #@23c
    .line 1042
    .restart local v12       #retryMsg:Landroid/os/Message;
    sget v18, Lcom/android/internal/telephony/SMSDispatcher;->SEND_RETRY_DELAY:I

    #@23e
    move/from16 v0, v18

    #@240
    int-to-long v0, v0

    #@241
    move-wide/from16 v18, v0

    #@243
    move-object/from16 v0, p0

    #@245
    move-wide/from16 v1, v18

    #@247
    invoke-virtual {v0, v12, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@24a
    goto/16 :goto_ab

    #@24c
    .line 1045
    .end local v12           #retryMsg:Landroid/os/Message;
    :cond_24c
    move-object/from16 v0, v17

    #@24e
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@250
    move-object/from16 v18, v0

    #@252
    if-eqz v18, :cond_502

    #@254
    .line 1046
    const/4 v8, 0x1

    #@255
    .line 1047
    .local v8, error:I
    const-string v18, "handleSendComplete(), SMS send failed RESULT_ERROR_GENERIC_FAILURE"

    #@257
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@25a
    .line 1048
    move-object/from16 v0, p1

    #@25c
    iget-object v0, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@25e
    move-object/from16 v18, v0

    #@260
    check-cast v18, Lcom/android/internal/telephony/CommandException;

    #@262
    check-cast v18, Lcom/android/internal/telephony/CommandException;

    #@264
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    #@267
    move-result-object v18

    #@268
    sget-object v19, Lcom/android/internal/telephony/CommandException$Error;->FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@26a
    move-object/from16 v0, v18

    #@26c
    move-object/from16 v1, v19

    #@26e
    if-ne v0, v1, :cond_271

    #@270
    .line 1050
    const/4 v8, 0x6

    #@271
    .line 1054
    :cond_271
    :try_start_271
    new-instance v10, Landroid/content/Intent;

    #@273
    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    #@276
    .line 1055
    .local v10, fillIn:Landroid/content/Intent;
    move-object/from16 v0, p1

    #@278
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@27a
    move-object/from16 v18, v0

    #@27c
    if-eqz v18, :cond_373

    #@27e
    .line 1056
    const-string v19, "errorCode"

    #@280
    move-object/from16 v0, p1

    #@282
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@284
    move-object/from16 v18, v0

    #@286
    check-cast v18, Lcom/android/internal/telephony/SmsResponse;

    #@288
    move-object/from16 v0, v18

    #@28a
    iget v0, v0, Lcom/android/internal/telephony/SmsResponse;->errorCode:I

    #@28c
    move/from16 v18, v0

    #@28e
    move-object/from16 v0, v19

    #@290
    move/from16 v1, v18

    #@292
    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@295
    .line 1058
    move-object/from16 v0, p0

    #@297
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@299
    move-object/from16 v18, v0

    #@29b
    const-string v19, "cdma_cause_code_display"

    #@29d
    invoke-static/range {v18 .. v19}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@2a0
    move-result v18

    #@2a1
    const/16 v19, 0x1

    #@2a3
    move/from16 v0, v18

    #@2a5
    move/from16 v1, v19

    #@2a7
    if-ne v0, v1, :cond_373

    #@2a9
    .line 1059
    move-object/from16 v0, p1

    #@2ab
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2ad
    move-object/from16 v18, v0

    #@2af
    check-cast v18, Lcom/android/internal/telephony/SmsResponse;

    #@2b1
    move-object/from16 v0, v18

    #@2b3
    iget v0, v0, Lcom/android/internal/telephony/SmsResponse;->errorCode:I

    #@2b5
    move/from16 v18, v0

    #@2b7
    move/from16 v0, v18

    #@2b9
    int-to-long v3, v0

    #@2ba
    .line 1060
    .local v3, causeCode:J
    new-instance v18, Ljava/lang/StringBuilder;

    #@2bc
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@2bf
    const-string v19, "handleSendComplete(), [SMS.MO.ERROR] causeCode : "

    #@2c1
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c4
    move-result-object v18

    #@2c5
    move-object/from16 v0, v18

    #@2c7
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2ca
    move-result-object v18

    #@2cb
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ce
    move-result-object v18

    #@2cf
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2d2
    .line 1062
    const/4 v7, 0x0

    #@2d3
    .line 1063
    .local v7, displayCauseCode:I
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@2d6
    move-result-object v13

    #@2d7
    .line 1064
    .local v13, rsrc:Landroid/content/res/Resources;
    const-string v9, ""

    #@2d9
    .line 1065
    .local v9, failReasonStr:Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    #@2db
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@2de
    const-string v19, "("

    #@2e0
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e3
    move-result-object v18

    #@2e4
    const v19, 0x2090315

    #@2e7
    move/from16 v0, v19

    #@2e9
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2ec
    move-result-object v19

    #@2ed
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f0
    move-result-object v18

    #@2f1
    const-string v19, " : "

    #@2f3
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f6
    move-result-object v18

    #@2f7
    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@2fa
    move-result-object v19

    #@2fb
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fe
    move-result-object v18

    #@2ff
    const-string v19, ")"

    #@301
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@304
    move-result-object v18

    #@305
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@308
    move-result-object v6

    #@309
    .line 1067
    .local v6, causeCodeStr:Ljava/lang/String;
    const v18, 0x2080005

    #@30c
    move/from16 v0, v18

    #@30e
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@311
    move-result-object v5

    #@312
    .line 1069
    .local v5, causeCodeItems:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@314
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/SMSDispatcher;->getDisplayOfCauseCode(J)I

    #@317
    move-result v7

    #@318
    .line 1072
    move-object/from16 v0, p0

    #@31a
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/SMSDispatcher;->informTranslateCauseCode(J)I

    #@31d
    move-result v18

    #@31e
    const/16 v19, 0x33

    #@320
    move/from16 v0, v18

    #@322
    move/from16 v1, v19

    #@324
    if-eq v0, v1, :cond_358

    #@326
    .line 1073
    const-string v18, "[SMS.MO.ERROR] CAUSECODE"

    #@328
    new-instance v19, Ljava/lang/StringBuilder;

    #@32a
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@32d
    const-string v20, "Cause Code : "

    #@32f
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@332
    move-result-object v19

    #@333
    move-object/from16 v0, v19

    #@335
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@338
    move-result-object v19

    #@339
    const-string v20, "("

    #@33b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33e
    move-result-object v19

    #@33f
    move-object/from16 v0, p0

    #@341
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/SMSDispatcher;->informTranslateCauseCode(J)I

    #@344
    move-result v20

    #@345
    aget-object v20, v5, v20

    #@347
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34a
    move-result-object v19

    #@34b
    const-string v20, ")"

    #@34d
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@350
    move-result-object v19

    #@351
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@354
    move-result-object v19

    #@355
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@358
    .line 1076
    :cond_358
    packed-switch v7, :pswitch_data_988

    #@35b
    .line 1114
    new-instance v18, Ljava/lang/StringBuilder;

    #@35d
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@360
    const-string v19, "handleSendComplete(), [SMS.MO.ERROR] Invalid display CauseCode : "

    #@362
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@365
    move-result-object v18

    #@366
    move-object/from16 v0, v18

    #@368
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36b
    move-result-object v18

    #@36c
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36f
    move-result-object v18

    #@370
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@373
    .line 1121
    .end local v3           #causeCode:J
    .end local v5           #causeCodeItems:[Ljava/lang/String;
    .end local v6           #causeCodeStr:Ljava/lang/String;
    .end local v7           #displayCauseCode:I
    .end local v9           #failReasonStr:Ljava/lang/String;
    .end local v13           #rsrc:Landroid/content/res/Resources;
    :cond_373
    :goto_373
    move-object/from16 v0, p0

    #@375
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@377
    move/from16 v18, v0

    #@379
    const/16 v19, -0x1

    #@37b
    move/from16 v0, v18

    #@37d
    move/from16 v1, v19

    #@37f
    if-le v0, v1, :cond_38f

    #@381
    .line 1122
    move-object/from16 v0, p0

    #@383
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@385
    move/from16 v18, v0

    #@387
    add-int/lit8 v18, v18, -0x1

    #@389
    move/from16 v0, v18

    #@38b
    move-object/from16 v1, p0

    #@38d
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@38f
    .line 1125
    :cond_38f
    move-object/from16 v0, p0

    #@391
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@393
    move/from16 v18, v0

    #@395
    if-nez v18, :cond_3a2

    #@397
    .line 1126
    const-string v18, "SendNextMsg"

    #@399
    const/16 v19, 0x1

    #@39b
    move-object/from16 v0, v18

    #@39d
    move/from16 v1, v19

    #@39f
    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@3a2
    .line 1129
    :cond_3a2
    move-object/from16 v0, v17

    #@3a4
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@3a6
    move-object/from16 v18, v0

    #@3a8
    move-object/from16 v0, p0

    #@3aa
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@3ac
    move-object/from16 v19, v0

    #@3ae
    move-object/from16 v0, v18

    #@3b0
    move-object/from16 v1, v19

    #@3b2
    invoke-virtual {v0, v1, v8, v10}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_3b5
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_271 .. :try_end_3b5} :catch_3f7

    #@3b5
    .line 1132
    .end local v10           #fillIn:Landroid/content/Intent;
    :goto_3b5
    move-object/from16 v0, p0

    #@3b7
    iget-boolean v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSyncronousSending:Z

    #@3b9
    move/from16 v18, v0

    #@3bb
    if-eqz v18, :cond_ab

    #@3bd
    .line 1133
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->processNextPendingMessage()V

    #@3c0
    goto/16 :goto_ab

    #@3c2
    .line 1078
    .restart local v3       #causeCode:J
    .restart local v5       #causeCodeItems:[Ljava/lang/String;
    .restart local v6       #causeCodeStr:Ljava/lang/String;
    .restart local v7       #displayCauseCode:I
    .restart local v9       #failReasonStr:Ljava/lang/String;
    .restart local v10       #fillIn:Landroid/content/Intent;
    .restart local v13       #rsrc:Landroid/content/res/Resources;
    :pswitch_3c2
    const v18, 0x2090316

    #@3c5
    :try_start_3c5
    move/from16 v0, v18

    #@3c7
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3ca
    move-result-object v9

    #@3cb
    .line 1080
    move-object/from16 v0, p0

    #@3cd
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@3cf
    move-object/from16 v18, v0

    #@3d1
    new-instance v19, Ljava/lang/StringBuilder;

    #@3d3
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@3d6
    move-object/from16 v0, v19

    #@3d8
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3db
    move-result-object v19

    #@3dc
    const-string v20, "\n"

    #@3de
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e1
    move-result-object v19

    #@3e2
    move-object/from16 v0, v19

    #@3e4
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e7
    move-result-object v19

    #@3e8
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3eb
    move-result-object v19

    #@3ec
    const/16 v20, 0x1

    #@3ee
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@3f1
    move-result-object v18

    #@3f2
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@3f5
    goto/16 :goto_373

    #@3f7
    .line 1130
    .end local v3           #causeCode:J
    .end local v5           #causeCodeItems:[Ljava/lang/String;
    .end local v6           #causeCodeStr:Ljava/lang/String;
    .end local v7           #displayCauseCode:I
    .end local v9           #failReasonStr:Ljava/lang/String;
    .end local v10           #fillIn:Landroid/content/Intent;
    .end local v13           #rsrc:Landroid/content/res/Resources;
    :catch_3f7
    move-exception v18

    #@3f8
    goto :goto_3b5

    #@3f9
    .line 1084
    .restart local v3       #causeCode:J
    .restart local v5       #causeCodeItems:[Ljava/lang/String;
    .restart local v6       #causeCodeStr:Ljava/lang/String;
    .restart local v7       #displayCauseCode:I
    .restart local v9       #failReasonStr:Ljava/lang/String;
    .restart local v10       #fillIn:Landroid/content/Intent;
    .restart local v13       #rsrc:Landroid/content/res/Resources;
    :pswitch_3f9
    const v18, 0x2090317

    #@3fc
    move/from16 v0, v18

    #@3fe
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@401
    move-result-object v9

    #@402
    .line 1086
    move-object/from16 v0, p0

    #@404
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@406
    move-object/from16 v18, v0

    #@408
    new-instance v19, Ljava/lang/StringBuilder;

    #@40a
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@40d
    move-object/from16 v0, v19

    #@40f
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@412
    move-result-object v19

    #@413
    const-string v20, "\n"

    #@415
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@418
    move-result-object v19

    #@419
    move-object/from16 v0, v19

    #@41b
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41e
    move-result-object v19

    #@41f
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@422
    move-result-object v19

    #@423
    const/16 v20, 0x1

    #@425
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@428
    move-result-object v18

    #@429
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@42c
    goto/16 :goto_373

    #@42e
    .line 1090
    :pswitch_42e
    const v18, 0x2090318

    #@431
    move/from16 v0, v18

    #@433
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@436
    move-result-object v9

    #@437
    .line 1092
    move-object/from16 v0, p0

    #@439
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@43b
    move-object/from16 v18, v0

    #@43d
    new-instance v19, Ljava/lang/StringBuilder;

    #@43f
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@442
    move-object/from16 v0, v19

    #@444
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@447
    move-result-object v19

    #@448
    const-string v20, "\n"

    #@44a
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44d
    move-result-object v19

    #@44e
    move-object/from16 v0, v19

    #@450
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@453
    move-result-object v19

    #@454
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@457
    move-result-object v19

    #@458
    const/16 v20, 0x1

    #@45a
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@45d
    move-result-object v18

    #@45e
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@461
    goto/16 :goto_373

    #@463
    .line 1096
    :pswitch_463
    const v18, 0x2090319

    #@466
    move/from16 v0, v18

    #@468
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@46b
    move-result-object v9

    #@46c
    .line 1098
    move-object/from16 v0, p0

    #@46e
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@470
    move-object/from16 v18, v0

    #@472
    new-instance v19, Ljava/lang/StringBuilder;

    #@474
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@477
    move-object/from16 v0, v19

    #@479
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47c
    move-result-object v19

    #@47d
    const-string v20, "\n"

    #@47f
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@482
    move-result-object v19

    #@483
    move-object/from16 v0, v19

    #@485
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@488
    move-result-object v19

    #@489
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48c
    move-result-object v19

    #@48d
    const/16 v20, 0x1

    #@48f
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@492
    move-result-object v18

    #@493
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@496
    goto/16 :goto_373

    #@498
    .line 1102
    :pswitch_498
    const v18, 0x209031a

    #@49b
    move/from16 v0, v18

    #@49d
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4a0
    move-result-object v9

    #@4a1
    .line 1104
    move-object/from16 v0, p0

    #@4a3
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@4a5
    move-object/from16 v18, v0

    #@4a7
    new-instance v19, Ljava/lang/StringBuilder;

    #@4a9
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@4ac
    move-object/from16 v0, v19

    #@4ae
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b1
    move-result-object v19

    #@4b2
    const-string v20, "\n"

    #@4b4
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b7
    move-result-object v19

    #@4b8
    move-object/from16 v0, v19

    #@4ba
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4bd
    move-result-object v19

    #@4be
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c1
    move-result-object v19

    #@4c2
    const/16 v20, 0x1

    #@4c4
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@4c7
    move-result-object v18

    #@4c8
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@4cb
    goto/16 :goto_373

    #@4cd
    .line 1108
    :pswitch_4cd
    const v18, 0x209031b

    #@4d0
    move/from16 v0, v18

    #@4d2
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4d5
    move-result-object v9

    #@4d6
    .line 1110
    move-object/from16 v0, p0

    #@4d8
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@4da
    move-object/from16 v18, v0

    #@4dc
    new-instance v19, Ljava/lang/StringBuilder;

    #@4de
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@4e1
    move-object/from16 v0, v19

    #@4e3
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e6
    move-result-object v19

    #@4e7
    const-string v20, "\n"

    #@4e9
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ec
    move-result-object v19

    #@4ed
    move-object/from16 v0, v19

    #@4ef
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f2
    move-result-object v19

    #@4f3
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f6
    move-result-object v19

    #@4f7
    const/16 v20, 0x1

    #@4f9
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@4fc
    move-result-object v18

    #@4fd
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V
    :try_end_500
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_3c5 .. :try_end_500} :catch_3f7

    #@500
    goto/16 :goto_373

    #@502
    .line 1139
    .end local v3           #causeCode:J
    .end local v5           #causeCodeItems:[Ljava/lang/String;
    .end local v6           #causeCodeStr:Ljava/lang/String;
    .end local v7           #displayCauseCode:I
    .end local v8           #error:I
    .end local v9           #failReasonStr:Ljava/lang/String;
    .end local v10           #fillIn:Landroid/content/Intent;
    .end local v13           #rsrc:Landroid/content/res/Resources;
    :cond_502
    move-object/from16 v0, p0

    #@504
    iget-boolean v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSyncronousSending:Z

    #@506
    move/from16 v18, v0

    #@508
    if-eqz v18, :cond_ab

    #@50a
    .line 1140
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->processNextPendingMessage()V

    #@50d
    goto/16 :goto_ab

    #@50f
    .line 1144
    :cond_50f
    move-object/from16 v0, p0

    #@511
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@513
    move-object/from16 v18, v0

    #@515
    const-string v19, "cdma_sms_cdg2"

    #@517
    invoke-static/range {v18 .. v19}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@51a
    move-result v18

    #@51b
    const/16 v19, 0x1

    #@51d
    move/from16 v0, v18

    #@51f
    move/from16 v1, v19

    #@521
    if-ne v0, v1, :cond_ab

    #@523
    .line 1145
    move-object/from16 v0, v17

    #@525
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@527
    move-object/from16 v18, v0

    #@529
    if-eqz v18, :cond_ab

    #@52b
    .line 1146
    const/4 v8, 0x1

    #@52c
    .line 1148
    .restart local v8       #error:I
    move-object/from16 v0, p1

    #@52e
    iget-object v0, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@530
    move-object/from16 v18, v0

    #@532
    check-cast v18, Lcom/android/internal/telephony/CommandException;

    #@534
    check-cast v18, Lcom/android/internal/telephony/CommandException;

    #@536
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    #@539
    move-result-object v18

    #@53a
    sget-object v19, Lcom/android/internal/telephony/CommandException$Error;->FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@53c
    move-object/from16 v0, v18

    #@53e
    move-object/from16 v1, v19

    #@540
    if-ne v0, v1, :cond_543

    #@542
    .line 1150
    const/4 v8, 0x6

    #@543
    .line 1155
    :cond_543
    :try_start_543
    new-instance v10, Landroid/content/Intent;

    #@545
    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    #@548
    .line 1156
    .restart local v10       #fillIn:Landroid/content/Intent;
    move-object/from16 v0, p1

    #@54a
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@54c
    move-object/from16 v18, v0

    #@54e
    if-eqz v18, :cond_610

    #@550
    .line 1157
    const-string v19, "errorCode"

    #@552
    move-object/from16 v0, p1

    #@554
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@556
    move-object/from16 v18, v0

    #@558
    check-cast v18, Lcom/android/internal/telephony/SmsResponse;

    #@55a
    move-object/from16 v0, v18

    #@55c
    iget v0, v0, Lcom/android/internal/telephony/SmsResponse;->errorCode:I

    #@55e
    move/from16 v18, v0

    #@560
    move-object/from16 v0, v19

    #@562
    move/from16 v1, v18

    #@564
    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@567
    .line 1158
    new-instance v18, Ljava/lang/StringBuilder;

    #@569
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@56c
    const-string v19, "handleSendComplete(), [SMS.MO.CDG2] SMSDispatcher"

    #@56e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@571
    move-result-object v18

    #@572
    move-object/from16 v0, p0

    #@574
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@576
    move-object/from16 v19, v0

    #@578
    const-string v20, "cdma_sms_cdg2"

    #@57a
    invoke-static/range {v19 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@57d
    move-result v19

    #@57e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@581
    move-result-object v18

    #@582
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@585
    move-result-object v18

    #@586
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@589
    .line 1160
    move-object/from16 v0, p0

    #@58b
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@58d
    move-object/from16 v18, v0

    #@58f
    const-string v19, "cdma_sms_cdg2"

    #@591
    invoke-static/range {v18 .. v19}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@594
    move-result v18

    #@595
    const/16 v19, 0x1

    #@597
    move/from16 v0, v18

    #@599
    move/from16 v1, v19

    #@59b
    if-ne v0, v1, :cond_73f

    #@59d
    .line 1161
    new-instance v18, Ljava/lang/StringBuilder;

    #@59f
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@5a2
    const-string v19, "handleSendComplete(), [SMS.MO.CDG2] SMSDispatcher2"

    #@5a4
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a7
    move-result-object v18

    #@5a8
    move-object/from16 v0, p0

    #@5aa
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@5ac
    move-object/from16 v19, v0

    #@5ae
    const-string v20, "cdma_sms_cdg2"

    #@5b0
    invoke-static/range {v19 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@5b3
    move-result v19

    #@5b4
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5b7
    move-result-object v18

    #@5b8
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5bb
    move-result-object v18

    #@5bc
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5bf
    .line 1162
    move-object/from16 v0, p1

    #@5c1
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@5c3
    move-object/from16 v18, v0

    #@5c5
    check-cast v18, Lcom/android/internal/telephony/SmsResponse;

    #@5c7
    move-object/from16 v0, v18

    #@5c9
    iget v3, v0, Lcom/android/internal/telephony/SmsResponse;->errorCode:I

    #@5cb
    .line 1163
    .local v3, causeCode:I
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@5ce
    move-result-object v11

    #@5cf
    .line 1164
    .local v11, r:Landroid/content/res/Resources;
    const-string v9, ""

    #@5d1
    .line 1165
    .restart local v9       #failReasonStr:Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    #@5d3
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@5d6
    const v19, 0x2090315

    #@5d9
    move/from16 v0, v19

    #@5db
    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5de
    move-result-object v19

    #@5df
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e2
    move-result-object v18

    #@5e3
    const-string v19, " : "

    #@5e5
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e8
    move-result-object v18

    #@5e9
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5ec
    move-result-object v19

    #@5ed
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f0
    move-result-object v18

    #@5f1
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f4
    move-result-object v6

    #@5f5
    .line 1167
    .restart local v6       #causeCodeStr:Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    #@5f7
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@5fa
    const-string v19, "handleSendComplete(), [SMS.MO.CDG2] causeCode => "

    #@5fc
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5ff
    move-result-object v18

    #@600
    move-object/from16 v0, v18

    #@602
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@605
    move-result-object v18

    #@606
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@609
    move-result-object v18

    #@60a
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@60d
    .line 1169
    sparse-switch v3, :sswitch_data_998

    #@610
    .line 1252
    .end local v3           #causeCode:I
    .end local v6           #causeCodeStr:Ljava/lang/String;
    .end local v9           #failReasonStr:Ljava/lang/String;
    .end local v11           #r:Landroid/content/res/Resources;
    :cond_610
    :goto_610
    move-object/from16 v0, p0

    #@612
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@614
    move/from16 v18, v0

    #@616
    const/16 v19, -0x1

    #@618
    move/from16 v0, v18

    #@61a
    move/from16 v1, v19

    #@61c
    if-le v0, v1, :cond_62c

    #@61e
    .line 1253
    move-object/from16 v0, p0

    #@620
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@622
    move/from16 v18, v0

    #@624
    add-int/lit8 v18, v18, -0x1

    #@626
    move/from16 v0, v18

    #@628
    move-object/from16 v1, p0

    #@62a
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@62c
    .line 1256
    :cond_62c
    move-object/from16 v0, p0

    #@62e
    iget v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@630
    move/from16 v18, v0

    #@632
    if-nez v18, :cond_63f

    #@634
    .line 1257
    const-string v18, "SendNextMsg"

    #@636
    const/16 v19, 0x1

    #@638
    move-object/from16 v0, v18

    #@63a
    move/from16 v1, v19

    #@63c
    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@63f
    .line 1260
    :cond_63f
    new-instance v18, Ljava/lang/StringBuilder;

    #@641
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@644
    const-string v19, "handleSendComplete(), [SMS.MO.CDG2]  CDG final error"

    #@646
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@649
    move-result-object v18

    #@64a
    move-object/from16 v0, v18

    #@64c
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64f
    move-result-object v18

    #@650
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@653
    move-result-object v18

    #@654
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@657
    .line 1262
    move-object/from16 v0, v17

    #@659
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@65b
    move-object/from16 v18, v0

    #@65d
    move-object/from16 v0, p0

    #@65f
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@661
    move-object/from16 v19, v0

    #@663
    move-object/from16 v0, v18

    #@665
    move-object/from16 v1, v19

    #@667
    invoke-virtual {v0, v1, v8, v10}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_66a
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_543 .. :try_end_66a} :catch_6b9

    #@66a
    .line 1265
    .end local v10           #fillIn:Landroid/content/Intent;
    :goto_66a
    move-object/from16 v0, p0

    #@66c
    iget-boolean v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mSyncronousSending:Z

    #@66e
    move/from16 v18, v0

    #@670
    if-eqz v18, :cond_ab

    #@672
    .line 1266
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->processNextPendingMessage()V

    #@675
    goto/16 :goto_ab

    #@677
    .line 1171
    .restart local v3       #causeCode:I
    .restart local v6       #causeCodeStr:Ljava/lang/String;
    .restart local v9       #failReasonStr:Ljava/lang/String;
    .restart local v10       #fillIn:Landroid/content/Intent;
    .restart local v11       #r:Landroid/content/res/Resources;
    :sswitch_677
    :try_start_677
    new-instance v18, Ljava/lang/StringBuilder;

    #@679
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@67c
    const v19, 0x209031c

    #@67f
    move/from16 v0, v19

    #@681
    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@684
    move-result-object v19

    #@685
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@688
    move-result-object v18

    #@689
    const-string v19, "\n"

    #@68b
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68e
    move-result-object v18

    #@68f
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@692
    move-result-object v9

    #@693
    .line 1173
    move-object/from16 v0, p0

    #@695
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@697
    move-object/from16 v18, v0

    #@699
    new-instance v19, Ljava/lang/StringBuilder;

    #@69b
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@69e
    move-object/from16 v0, v19

    #@6a0
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a3
    move-result-object v19

    #@6a4
    move-object/from16 v0, v19

    #@6a6
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a9
    move-result-object v19

    #@6aa
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6ad
    move-result-object v19

    #@6ae
    const/16 v20, 0x1

    #@6b0
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@6b3
    move-result-object v18

    #@6b4
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@6b7
    goto/16 :goto_610

    #@6b9
    .line 1263
    .end local v3           #causeCode:I
    .end local v6           #causeCodeStr:Ljava/lang/String;
    .end local v9           #failReasonStr:Ljava/lang/String;
    .end local v10           #fillIn:Landroid/content/Intent;
    .end local v11           #r:Landroid/content/res/Resources;
    :catch_6b9
    move-exception v18

    #@6ba
    goto :goto_66a

    #@6bb
    .line 1176
    .restart local v3       #causeCode:I
    .restart local v6       #causeCodeStr:Ljava/lang/String;
    .restart local v9       #failReasonStr:Ljava/lang/String;
    .restart local v10       #fillIn:Landroid/content/Intent;
    .restart local v11       #r:Landroid/content/res/Resources;
    :sswitch_6bb
    new-instance v18, Ljava/lang/StringBuilder;

    #@6bd
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@6c0
    const v19, 0x209031d

    #@6c3
    move/from16 v0, v19

    #@6c5
    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6c8
    move-result-object v19

    #@6c9
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6cc
    move-result-object v18

    #@6cd
    const-string v19, "\n"

    #@6cf
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d2
    move-result-object v18

    #@6d3
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d6
    move-result-object v9

    #@6d7
    .line 1178
    move-object/from16 v0, p0

    #@6d9
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@6db
    move-object/from16 v18, v0

    #@6dd
    new-instance v19, Ljava/lang/StringBuilder;

    #@6df
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@6e2
    move-object/from16 v0, v19

    #@6e4
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e7
    move-result-object v19

    #@6e8
    move-object/from16 v0, v19

    #@6ea
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6ed
    move-result-object v19

    #@6ee
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f1
    move-result-object v19

    #@6f2
    const/16 v20, 0x1

    #@6f4
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@6f7
    move-result-object v18

    #@6f8
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@6fb
    goto/16 :goto_610

    #@6fd
    .line 1181
    :sswitch_6fd
    new-instance v18, Ljava/lang/StringBuilder;

    #@6ff
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@702
    const v19, 0x2090319

    #@705
    move/from16 v0, v19

    #@707
    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@70a
    move-result-object v19

    #@70b
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70e
    move-result-object v18

    #@70f
    const-string v19, "\n"

    #@711
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@714
    move-result-object v18

    #@715
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@718
    move-result-object v9

    #@719
    .line 1183
    move-object/from16 v0, p0

    #@71b
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@71d
    move-object/from16 v18, v0

    #@71f
    new-instance v19, Ljava/lang/StringBuilder;

    #@721
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@724
    move-object/from16 v0, v19

    #@726
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@729
    move-result-object v19

    #@72a
    move-object/from16 v0, v19

    #@72c
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72f
    move-result-object v19

    #@730
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@733
    move-result-object v19

    #@734
    const/16 v20, 0x1

    #@736
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@739
    move-result-object v18

    #@73a
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@73d
    goto/16 :goto_610

    #@73f
    .line 1191
    .end local v3           #causeCode:I
    .end local v6           #causeCodeStr:Ljava/lang/String;
    .end local v9           #failReasonStr:Ljava/lang/String;
    .end local v11           #r:Landroid/content/res/Resources;
    :cond_73f
    move-object/from16 v0, p1

    #@741
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@743
    move-object/from16 v18, v0

    #@745
    check-cast v18, Lcom/android/internal/telephony/SmsResponse;

    #@747
    move-object/from16 v0, v18

    #@749
    iget v0, v0, Lcom/android/internal/telephony/SmsResponse;->errorCode:I

    #@74b
    move/from16 v18, v0

    #@74d
    move/from16 v0, v18

    #@74f
    int-to-long v3, v0

    #@750
    .line 1192
    .local v3, causeCode:J
    new-instance v18, Ljava/lang/StringBuilder;

    #@752
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@755
    const-string v19, "handleSendComplete(), [SMS.MO.CDG2] causeCode : "

    #@757
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75a
    move-result-object v18

    #@75b
    move-object/from16 v0, v18

    #@75d
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@760
    move-result-object v18

    #@761
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@764
    move-result-object v18

    #@765
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@768
    .line 1193
    const/4 v7, 0x0

    #@769
    .line 1194
    .restart local v7       #displayCauseCode:I
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@76c
    move-result-object v13

    #@76d
    .line 1195
    .restart local v13       #rsrc:Landroid/content/res/Resources;
    const-string v9, ""

    #@76f
    .line 1196
    .restart local v9       #failReasonStr:Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    #@771
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@774
    const-string v19, "("

    #@776
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@779
    move-result-object v18

    #@77a
    const v19, 0x2090315

    #@77d
    move/from16 v0, v19

    #@77f
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@782
    move-result-object v19

    #@783
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@786
    move-result-object v18

    #@787
    const-string v19, " : "

    #@789
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78c
    move-result-object v18

    #@78d
    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@790
    move-result-object v19

    #@791
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@794
    move-result-object v18

    #@795
    const-string v19, ")"

    #@797
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79a
    move-result-object v18

    #@79b
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79e
    move-result-object v6

    #@79f
    .line 1198
    .restart local v6       #causeCodeStr:Ljava/lang/String;
    const v18, 0x2080005

    #@7a2
    move/from16 v0, v18

    #@7a4
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@7a7
    move-result-object v5

    #@7a8
    .line 1199
    .restart local v5       #causeCodeItems:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@7aa
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/SMSDispatcher;->getDisplayOfCauseCode(J)I

    #@7ad
    move-result v7

    #@7ae
    .line 1202
    move-object/from16 v0, p0

    #@7b0
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/SMSDispatcher;->informTranslateCauseCode(J)I

    #@7b3
    move-result v18

    #@7b4
    const/16 v19, 0x33

    #@7b6
    move/from16 v0, v18

    #@7b8
    move/from16 v1, v19

    #@7ba
    if-eq v0, v1, :cond_82d

    #@7bc
    .line 1203
    const-string v18, "[SMS.MO.CDG2]  CAUSECODE"

    #@7be
    new-instance v19, Ljava/lang/StringBuilder;

    #@7c0
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@7c3
    const v20, 0x2090315

    #@7c6
    move/from16 v0, v20

    #@7c8
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@7cb
    move-result-object v20

    #@7cc
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7cf
    move-result-object v19

    #@7d0
    const-string v20, " : "

    #@7d2
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d5
    move-result-object v19

    #@7d6
    move-object/from16 v0, v19

    #@7d8
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@7db
    move-result-object v19

    #@7dc
    const-string v20, "("

    #@7de
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e1
    move-result-object v19

    #@7e2
    move-object/from16 v0, p0

    #@7e4
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/SMSDispatcher;->informTranslateCauseCode(J)I

    #@7e7
    move-result v20

    #@7e8
    aget-object v20, v5, v20

    #@7ea
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7ed
    move-result-object v19

    #@7ee
    const-string v20, ")"

    #@7f0
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f3
    move-result-object v19

    #@7f4
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f7
    move-result-object v19

    #@7f8
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7fb
    .line 1204
    const-string v18, "[SMS.MO.CDG2]  CAUSECODE"

    #@7fd
    new-instance v19, Ljava/lang/StringBuilder;

    #@7ff
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@802
    const-string v20, "Cause Code : "

    #@804
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@807
    move-result-object v19

    #@808
    move-object/from16 v0, v19

    #@80a
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@80d
    move-result-object v19

    #@80e
    const-string v20, "("

    #@810
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@813
    move-result-object v19

    #@814
    move-object/from16 v0, p0

    #@816
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/SMSDispatcher;->informTranslateCauseCode(J)I

    #@819
    move-result v20

    #@81a
    aget-object v20, v5, v20

    #@81c
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81f
    move-result-object v19

    #@820
    const-string v20, ")"

    #@822
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@825
    move-result-object v19

    #@826
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@829
    move-result-object v19

    #@82a
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@82d
    .line 1207
    :cond_82d
    packed-switch v7, :pswitch_data_9a6

    #@830
    .line 1245
    new-instance v18, Ljava/lang/StringBuilder;

    #@832
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@835
    const-string v19, "handleSendComplete(), [SMS.MO.CDG2] Invalid displayCauseCode : "

    #@837
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83a
    move-result-object v18

    #@83b
    move-object/from16 v0, v18

    #@83d
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@840
    move-result-object v18

    #@841
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@844
    move-result-object v18

    #@845
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@848
    goto/16 :goto_610

    #@84a
    .line 1209
    :pswitch_84a
    const v18, 0x2090316

    #@84d
    move/from16 v0, v18

    #@84f
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@852
    move-result-object v9

    #@853
    .line 1211
    move-object/from16 v0, p0

    #@855
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@857
    move-object/from16 v18, v0

    #@859
    new-instance v19, Ljava/lang/StringBuilder;

    #@85b
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@85e
    move-object/from16 v0, v19

    #@860
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@863
    move-result-object v19

    #@864
    const-string v20, "\n"

    #@866
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@869
    move-result-object v19

    #@86a
    move-object/from16 v0, v19

    #@86c
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86f
    move-result-object v19

    #@870
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@873
    move-result-object v19

    #@874
    const/16 v20, 0x1

    #@876
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@879
    move-result-object v18

    #@87a
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@87d
    goto/16 :goto_610

    #@87f
    .line 1215
    :pswitch_87f
    const v18, 0x2090317

    #@882
    move/from16 v0, v18

    #@884
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@887
    move-result-object v9

    #@888
    .line 1217
    move-object/from16 v0, p0

    #@88a
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@88c
    move-object/from16 v18, v0

    #@88e
    new-instance v19, Ljava/lang/StringBuilder;

    #@890
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@893
    move-object/from16 v0, v19

    #@895
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@898
    move-result-object v19

    #@899
    const-string v20, "\n"

    #@89b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89e
    move-result-object v19

    #@89f
    move-object/from16 v0, v19

    #@8a1
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a4
    move-result-object v19

    #@8a5
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a8
    move-result-object v19

    #@8a9
    const/16 v20, 0x1

    #@8ab
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@8ae
    move-result-object v18

    #@8af
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@8b2
    goto/16 :goto_610

    #@8b4
    .line 1221
    :pswitch_8b4
    const v18, 0x2090318

    #@8b7
    move/from16 v0, v18

    #@8b9
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@8bc
    move-result-object v9

    #@8bd
    .line 1223
    move-object/from16 v0, p0

    #@8bf
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@8c1
    move-object/from16 v18, v0

    #@8c3
    new-instance v19, Ljava/lang/StringBuilder;

    #@8c5
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@8c8
    move-object/from16 v0, v19

    #@8ca
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8cd
    move-result-object v19

    #@8ce
    const-string v20, "\n"

    #@8d0
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d3
    move-result-object v19

    #@8d4
    move-object/from16 v0, v19

    #@8d6
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d9
    move-result-object v19

    #@8da
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8dd
    move-result-object v19

    #@8de
    const/16 v20, 0x1

    #@8e0
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@8e3
    move-result-object v18

    #@8e4
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@8e7
    goto/16 :goto_610

    #@8e9
    .line 1227
    :pswitch_8e9
    const v18, 0x2090319

    #@8ec
    move/from16 v0, v18

    #@8ee
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@8f1
    move-result-object v9

    #@8f2
    .line 1229
    move-object/from16 v0, p0

    #@8f4
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@8f6
    move-object/from16 v18, v0

    #@8f8
    new-instance v19, Ljava/lang/StringBuilder;

    #@8fa
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@8fd
    move-object/from16 v0, v19

    #@8ff
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@902
    move-result-object v19

    #@903
    const-string v20, "\n"

    #@905
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@908
    move-result-object v19

    #@909
    move-object/from16 v0, v19

    #@90b
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90e
    move-result-object v19

    #@90f
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@912
    move-result-object v19

    #@913
    const/16 v20, 0x1

    #@915
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@918
    move-result-object v18

    #@919
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@91c
    goto/16 :goto_610

    #@91e
    .line 1233
    :pswitch_91e
    const v18, 0x209031a

    #@921
    move/from16 v0, v18

    #@923
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@926
    move-result-object v9

    #@927
    .line 1235
    move-object/from16 v0, p0

    #@929
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@92b
    move-object/from16 v18, v0

    #@92d
    new-instance v19, Ljava/lang/StringBuilder;

    #@92f
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@932
    move-object/from16 v0, v19

    #@934
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@937
    move-result-object v19

    #@938
    const-string v20, "\n"

    #@93a
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93d
    move-result-object v19

    #@93e
    move-object/from16 v0, v19

    #@940
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@943
    move-result-object v19

    #@944
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@947
    move-result-object v19

    #@948
    const/16 v20, 0x1

    #@94a
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@94d
    move-result-object v18

    #@94e
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@951
    goto/16 :goto_610

    #@953
    .line 1239
    :pswitch_953
    const v18, 0x209031b

    #@956
    move/from16 v0, v18

    #@958
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@95b
    move-result-object v9

    #@95c
    .line 1241
    move-object/from16 v0, p0

    #@95e
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@960
    move-object/from16 v18, v0

    #@962
    new-instance v19, Ljava/lang/StringBuilder;

    #@964
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@967
    move-object/from16 v0, v19

    #@969
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96c
    move-result-object v19

    #@96d
    const-string v20, "\n"

    #@96f
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@972
    move-result-object v19

    #@973
    move-object/from16 v0, v19

    #@975
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@978
    move-result-object v19

    #@979
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97c
    move-result-object v19

    #@97d
    const/16 v20, 0x1

    #@97f
    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@982
    move-result-object v18

    #@983
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V
    :try_end_986
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_677 .. :try_end_986} :catch_6b9

    #@986
    goto/16 :goto_610

    #@988
    .line 1076
    :pswitch_data_988
    .packed-switch 0x0
        :pswitch_3c2
        :pswitch_3f9
        :pswitch_42e
        :pswitch_463
        :pswitch_498
        :pswitch_4cd
    .end packed-switch

    #@998
    .line 1169
    :sswitch_data_998
    .sparse-switch
        0x1 -> :sswitch_677
        0x61 -> :sswitch_6bb
        0x64 -> :sswitch_6fd
    .end sparse-switch

    #@9a6
    .line 1207
    :pswitch_data_9a6
    .packed-switch 0x0
        :pswitch_84a
        :pswitch_87f
        :pswitch_8b4
        :pswitch_8e9
        :pswitch_91e
        :pswitch_953
    .end packed-switch
.end method

.method protected informTranslateCauseCode(J)I
    .registers 7
    .parameter "causeCode"

    #@0
    .prologue
    const/16 v1, 0x33

    #@2
    .line 1298
    const/4 v0, 0x0

    #@3
    .local v0, arrayIndex:I
    :goto_3
    if-ge v0, v1, :cond_11

    #@5
    .line 1299
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->causeCodeArray:[J

    #@7
    aget-wide v2, v2, v0

    #@9
    cmp-long v2, p1, v2

    #@b
    if-nez v2, :cond_e

    #@d
    .line 1303
    .end local v0           #arrayIndex:I
    :goto_d
    return v0

    #@e
    .line 1298
    .restart local v0       #arrayIndex:I
    :cond_e
    add-int/lit8 v0, v0, 0x1

    #@10
    goto :goto_3

    #@11
    :cond_11
    move v0, v1

    #@12
    .line 1303
    goto :goto_d
.end method

.method protected isCdmaMo()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 3827
    const-string v3, "isCdmaMo(), start"

    #@4
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7
    .line 3829
    const/4 v3, 0x0

    #@8
    const-string v4, "check_radio_tech_for_msg_sending"

    #@a
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@d
    move-result v3

    #@e
    if-ne v3, v1, :cond_bc

    #@10
    .line 3830
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@12
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@19
    move-result v0

    #@1a
    .line 3831
    .local v0, radioTech:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->isIms()Z

    #@1d
    move-result v3

    #@1e
    if-nez v3, :cond_9b

    #@20
    .line 3833
    sget-boolean v3, Lcom/android/internal/telephony/SMSDispatcher;->RECEIVE_DAN_SUCCESS:Z

    #@22
    if-eqz v3, :cond_4f

    #@24
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@26
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@29
    invoke-static {v0}, Landroid/telephony/ServiceState;->isEhrpd(I)Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_4f

    #@2f
    .line 3834
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "isCdmaMo(), [KDDI] IMS is not registered / DAN Success / Phone Type is "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3c
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@3f
    invoke-static {v0}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4e
    .line 3857
    .end local v0           #radioTech:I
    :cond_4e
    :goto_4e
    return v1

    #@4f
    .line 3837
    .restart local v0       #radioTech:I
    :cond_4f
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@51
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@54
    invoke-static {v0}, Landroid/telephony/ServiceState;->isCdmaFormat(I)Z

    #@57
    move-result v3

    #@58
    if-eqz v3, :cond_7a

    #@5a
    .line 3838
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v3, "isCdmaMo(), [KDDI] IMS is not registered / Phone Type is "

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@67
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@6a
    invoke-static {v0}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v2

    #@72
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v2

    #@76
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@79
    goto :goto_4e

    #@7a
    .line 3841
    :cond_7a
    new-instance v1, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v3, "isCdmaMo(), [KDDI] IMS is not registered / Phone Type is "

    #@81
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v1

    #@85
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@87
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@8a
    invoke-static {v0}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@8d
    move-result-object v3

    #@8e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v1

    #@92
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v1

    #@96
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@99
    move v1, v2

    #@9a
    .line 3842
    goto :goto_4e

    #@9b
    .line 3845
    :cond_9b
    new-instance v1, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v3, "isCdmaMo(), [KDDI] IMS is registered / Phone Type is "

    #@a2
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v1

    #@a6
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a8
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@ab
    invoke-static {v0}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@ae
    move-result-object v3

    #@af
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v1

    #@b3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v1

    #@b7
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@ba
    move v1, v2

    #@bb
    .line 3846
    goto :goto_4e

    #@bc
    .line 3850
    .end local v0           #radioTech:I
    :cond_bc
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->isIms()Z

    #@bf
    move-result v3

    #@c0
    if-nez v3, :cond_d3

    #@c2
    .line 3851
    const-string v3, "isCdmaMo(), IMS is not registered, use Voice technology to determine SMS encoding"

    #@c4
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@c7
    .line 3853
    const/4 v3, 0x2

    #@c8
    iget-object v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ca
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@cd
    move-result v4

    #@ce
    if-eq v3, v4, :cond_4e

    #@d0
    move v1, v2

    #@d1
    goto/16 :goto_4e

    #@d3
    .line 3856
    :cond_d3
    new-instance v1, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v2, "isCdmaMo(), IMS is registered, use getImsSmsFormat() : "

    #@da
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v1

    #@de
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->getImsSmsFormat()Ljava/lang/String;

    #@e1
    move-result-object v2

    #@e2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v1

    #@e6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e9
    move-result-object v1

    #@ea
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@ed
    .line 3857
    const-string v1, "3gpp2"

    #@ef
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->getImsSmsFormat()Ljava/lang/String;

    #@f2
    move-result-object v2

    #@f3
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f6
    move-result v1

    #@f7
    goto/16 :goto_4e
.end method

.method public abstract isIms()Z
.end method

.method protected isSegmentedPDU(Lcom/android/internal/telephony/SmsMessageBase;)Z
    .registers 17
    .parameter "sms"

    #@0
    .prologue
    .line 4160
    const/4 v7, 0x0

    #@1
    .line 4161
    .local v7, cursor:Landroid/database/Cursor;
    const/4 v9, 0x0

    #@2
    .line 4163
    .local v9, isByPass:Z
    if-nez p1, :cond_6

    #@4
    .line 4164
    const/4 v0, 0x0

    #@5
    .line 4211
    :cond_5
    :goto_5
    return v0

    #@6
    .line 4166
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getMessageBody()Ljava/lang/String;

    #@9
    move-result-object v10

    #@a
    .line 4168
    .local v10, messageBody:Ljava/lang/String;
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/SMSDispatcher;->getPaginationIndicator(Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;

    #@d
    move-result-object v11

    #@e
    .line 4169
    .local v11, pi:Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;
    if-nez v11, :cond_12

    #@10
    .line 4170
    const/4 v0, 0x0

    #@11
    goto :goto_5

    #@12
    .line 4173
    :cond_12
    invoke-virtual {v11}, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->getSequence()I

    #@15
    move-result v12

    #@16
    .line 4174
    .local v12, sequence:I
    invoke-virtual {v11}, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->getTotalCount()I

    #@19
    move-result v13

    #@1a
    .line 4176
    .local v13, totalCount:I
    if-gt v12, v13, :cond_23

    #@1c
    const/16 v0, 0xa

    #@1e
    if-gt v13, v0, :cond_23

    #@20
    const/4 v0, 0x2

    #@21
    if-ge v13, v0, :cond_25

    #@23
    .line 4177
    :cond_23
    const/4 v0, 0x0

    #@24
    goto :goto_5

    #@25
    .line 4179
    :cond_25
    new-instance v14, Ljava/lang/StringBuilder;

    #@27
    const-string v0, "address ="

    #@29
    invoke-direct {v14, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@2c
    .line 4180
    .local v14, where:Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v1, "\'"

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, "\'"

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    .line 4181
    const-string v0, " AND ( sequence ="

    #@4e
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    .line 4182
    new-instance v0, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v0

    #@5e
    const-string v1, " OR "

    #@60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v0

    #@64
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v0

    #@68
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    .line 4183
    const-string v0, "totalCount !="

    #@6d
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    .line 4184
    new-instance v0, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@78
    move-result-object v1

    #@79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v0

    #@7d
    const-string v1, " )"

    #@7f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v0

    #@83
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v0

    #@87
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    .line 4187
    :try_start_8a
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@8c
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSegmentUri:Landroid/net/Uri;

    #@8e
    sget-object v2, Lcom/android/internal/telephony/SMSDispatcher$Segment;->PROJECTION:[Ljava/lang/String;

    #@90
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v3

    #@94
    const/4 v4, 0x0

    #@95
    const/4 v5, 0x0

    #@96
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_99
    .catchall {:try_start_8a .. :try_end_99} :catchall_cd
    .catch Landroid/database/SQLException; {:try_start_8a .. :try_end_99} :catch_c3

    #@99
    move-result-object v7

    #@9a
    .line 4189
    if-nez v7, :cond_a4

    #@9c
    .line 4190
    const/4 v0, 0x0

    #@9d
    .line 4207
    if-eqz v7, :cond_5

    #@9f
    .line 4208
    :goto_9f
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@a2
    goto/16 :goto_5

    #@a4
    .line 4192
    :cond_a4
    :try_start_a4
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@a7
    move-result v0

    #@a8
    if-eqz v0, :cond_b7

    #@aa
    .line 4193
    const-string v0, "totalCount"

    #@ac
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@af
    move-result v6

    #@b0
    .line 4194
    .local v6, TotalCountIndex:I
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getInt(I)I

    #@b3
    move-result v0

    #@b4
    if-eq v0, v13, :cond_a4

    #@b6
    .line 4195
    const/4 v9, 0x1

    #@b7
    .line 4200
    .end local v6           #TotalCountIndex:I
    :cond_b7
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_ba
    .catchall {:try_start_a4 .. :try_end_ba} :catchall_cd
    .catch Landroid/database/SQLException; {:try_start_a4 .. :try_end_ba} :catch_c3

    #@ba
    move-result v0

    #@bb
    if-lez v0, :cond_d4

    #@bd
    if-nez v9, :cond_d4

    #@bf
    .line 4201
    const/4 v0, 0x0

    #@c0
    .line 4207
    if-eqz v7, :cond_5

    #@c2
    goto :goto_9f

    #@c3
    .line 4203
    :catch_c3
    move-exception v8

    #@c4
    .line 4204
    .local v8, e:Landroid/database/SQLException;
    :try_start_c4
    const-string v0, "isSegmentedPDU(), query exception catch"

    #@c6
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_c9
    .catchall {:try_start_c4 .. :try_end_c9} :catchall_cd

    #@c9
    .line 4205
    const/4 v0, 0x0

    #@ca
    .line 4207
    if-eqz v7, :cond_5

    #@cc
    goto :goto_9f

    #@cd
    .end local v8           #e:Landroid/database/SQLException;
    :catchall_cd
    move-exception v0

    #@ce
    if-eqz v7, :cond_d3

    #@d0
    .line 4208
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@d3
    .line 4207
    :cond_d3
    throw v0

    #@d4
    :cond_d4
    if-eqz v7, :cond_d9

    #@d6
    .line 4208
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@d9
    .line 4211
    :cond_d9
    const/4 v0, 0x1

    #@da
    goto/16 :goto_5
.end method

.method protected processMessagePart([BLjava/lang/String;IIIJIZ)I
    .registers 21
    .parameter "pdu"
    .parameter "address"
    .parameter "referenceNumber"
    .parameter "sequenceNumber"
    .parameter "messageCount"
    .parameter "timestamp"
    .parameter "destPort"
    .parameter "isCdmaWapPush"

    #@0
    .prologue
    .line 2273
    const/4 v10, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move/from16 v5, p5

    #@8
    move-wide/from16 v6, p6

    #@a
    move/from16 v8, p8

    #@c
    move/from16 v9, p9

    #@e
    invoke-virtual/range {v0 .. v10}, Lcom/android/internal/telephony/SMSDispatcher;->processMessagePart([BLjava/lang/String;IIIJIZLjava/lang/String;)I

    #@11
    move-result v0

    #@12
    return v0
.end method

.method protected processMessagePart([BLjava/lang/String;IIIJIZLjava/lang/String;)I
    .registers 47
    .parameter "pdu"
    .parameter "address"
    .parameter "referenceNumber"
    .parameter "sequenceNumber"
    .parameter "messageCount"
    .parameter "timestamp"
    .parameter "destPort"
    .parameter "isCdmaWapPush"
    .parameter "serviceCenter"

    #@0
    .prologue
    .line 1535
    const/16 v28, 0x0

    #@2
    check-cast v28, [[B

    #@4
    .line 1536
    .local v28, pdus:[[B
    const/16 v16, 0x0

    #@6
    .line 1538
    .local v16, cursor:Landroid/database/Cursor;
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "processMessagePart(), address: "

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    move-object/from16 v0, p2

    #@13
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1e
    .line 1539
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "processMessagePart(), referenceNumber = "

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    move/from16 v0, p3

    #@2b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@36
    .line 1540
    new-instance v3, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v4, "processMessagePart(), sequenceNumber = "

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    move/from16 v0, p4

    #@43
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4e
    .line 1541
    new-instance v3, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v4, "processMessagePart(), messageCount = "

    #@55
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    move/from16 v0, p5

    #@5b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@66
    .line 1542
    new-instance v3, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v4, "processMessagePart(), destPort = "

    #@6d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    move/from16 v0, p8

    #@73
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v3

    #@7b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7e
    .line 1543
    new-instance v3, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v4, "processMessagePart(), isCdmaWapPush = "

    #@85
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v3

    #@89
    move/from16 v0, p9

    #@8b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v3

    #@8f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v3

    #@93
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@96
    .line 1546
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@99
    move-result-wide v13

    #@9a
    .line 1551
    .local v13, firstTime:J
    :try_start_9a
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@9d
    move-result-object v29

    #@9e
    .line 1552
    .local v29, refNumber:Ljava/lang/String;
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@a1
    move-result-object v31

    #@a2
    .line 1555
    .local v31, seqNumber:Ljava/lang/String;
    move-object/from16 v0, p0

    #@a4
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@a6
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@a8
    sget-object v5, Lcom/android/internal/telephony/SMSDispatcher;->PDU_PROJECTION:[Ljava/lang/String;

    #@aa
    const-string v6, "address=? AND reference_number=? AND sequence=?"

    #@ac
    const/4 v8, 0x3

    #@ad
    new-array v7, v8, [Ljava/lang/String;

    #@af
    const/4 v8, 0x0

    #@b0
    aput-object p2, v7, v8

    #@b2
    const/4 v8, 0x1

    #@b3
    aput-object v29, v7, v8

    #@b5
    const/4 v8, 0x2

    #@b6
    aput-object v31, v7, v8

    #@b8
    const/4 v8, 0x0

    #@b9
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@bc
    move-result-object v16

    #@bd
    .line 1560
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    #@c0
    move-result v3

    #@c1
    if-eqz v3, :cond_13a

    #@c3
    .line 1561
    const-string v3, "SMS"

    #@c5
    new-instance v4, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v5, "Discarding duplicate message segment from address="

    #@cc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v4

    #@d0
    move-object/from16 v0, p2

    #@d2
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v4

    #@d6
    const-string v5, " refNumber="

    #@d8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v4

    #@dc
    move-object/from16 v0, v29

    #@de
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v4

    #@e2
    const-string v5, " seqNumber="

    #@e4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v4

    #@e8
    move-object/from16 v0, v31

    #@ea
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v4

    #@ee
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v4

    #@f2
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f5
    .line 1563
    const/4 v3, 0x0

    #@f6
    move-object/from16 v0, v16

    #@f8
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@fb
    move-result-object v26

    #@fc
    .line 1564
    .local v26, oldPduString:Ljava/lang/String;
    invoke-static/range {v26 .. v26}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@ff
    move-result-object v25

    #@100
    .line 1565
    .local v25, oldPdu:[B
    move-object/from16 v0, v25

    #@102
    move-object/from16 v1, p1

    #@104
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    #@107
    move-result v3

    #@108
    if-nez v3, :cond_132

    #@10a
    .line 1566
    const-string v3, "SMS"

    #@10c
    new-instance v4, Ljava/lang/StringBuilder;

    #@10e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@111
    const-string v5, "Warning: dup message segment PDU of length "

    #@113
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v4

    #@117
    move-object/from16 v0, p1

    #@119
    array-length v5, v0

    #@11a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v4

    #@11e
    const-string v5, " is different from existing PDU of length "

    #@120
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v4

    #@124
    move-object/from16 v0, v25

    #@126
    array-length v5, v0

    #@127
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v4

    #@12b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12e
    move-result-object v4

    #@12f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_132
    .catchall {:try_start_9a .. :try_end_132} :catchall_295
    .catch Landroid/database/SQLException; {:try_start_9a .. :try_end_132} :catch_27d

    #@132
    .line 1569
    :cond_132
    const/16 v30, 0x1

    #@134
    .line 1671
    if-eqz v16, :cond_139

    #@136
    .end local v25           #oldPdu:[B
    .end local v26           #oldPduString:Ljava/lang/String;
    .end local v29           #refNumber:Ljava/lang/String;
    .end local v31           #seqNumber:Ljava/lang/String;
    :goto_136
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    #@139
    .line 1741
    :cond_139
    :goto_139
    return v30

    #@13a
    .line 1571
    .restart local v29       #refNumber:Ljava/lang/String;
    .restart local v31       #seqNumber:Ljava/lang/String;
    :cond_13a
    :try_start_13a
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    #@13d
    .line 1575
    move-object/from16 v0, p0

    #@13f
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@141
    const-string v4, "ConcatMTCheckTimestamp"

    #@143
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@146
    move-result v3

    #@147
    if-eqz v3, :cond_18a

    #@149
    .line 1576
    const-wide/32 v3, 0x927c0

    #@14c
    sub-long v3, p6, v3

    #@14e
    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@151
    move-result-object v15

    #@152
    .line 1577
    .local v15, comparingTime:Ljava/lang/String;
    const-string v35, "address=? AND reference_number=? AND date<?"

    #@154
    .line 1578
    .local v35, whereEx:Ljava/lang/String;
    const/4 v3, 0x3

    #@155
    new-array v0, v3, [Ljava/lang/String;

    #@157
    move-object/from16 v34, v0

    #@159
    const/4 v3, 0x0

    #@15a
    aput-object p2, v34, v3

    #@15c
    const/4 v3, 0x1

    #@15d
    aput-object v29, v34, v3

    #@15f
    const/4 v3, 0x2

    #@160
    aput-object v15, v34, v3
    :try_end_162
    .catchall {:try_start_13a .. :try_end_162} :catchall_295
    .catch Landroid/database/SQLException; {:try_start_13a .. :try_end_162} :catch_27d

    #@162
    .line 1582
    .local v34, whereArgsEx:[Ljava/lang/String;
    :try_start_162
    move-object/from16 v0, p0

    #@164
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@166
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@168
    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@16b
    move-result-object v5

    #@16c
    move-object/from16 v0, v34

    #@16e
    invoke-virtual {v3, v4, v5, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@171
    move-result v21

    #@172
    .line 1583
    .local v21, deleteCnt:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@174
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@177
    const-string v4, "processMessagePart(), [KRSMS] duplicated reference number & more than 10 minutes earlier timestamp"

    #@179
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v3

    #@17d
    move/from16 v0, v21

    #@17f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@182
    move-result-object v3

    #@183
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@186
    move-result-object v3

    #@187
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_18a
    .catchall {:try_start_162 .. :try_end_18a} :catchall_295
    .catch Landroid/database/SQLException; {:try_start_162 .. :try_end_18a} :catch_273
    .catch Ljava/lang/RuntimeException; {:try_start_162 .. :try_end_18a} :catch_28d

    #@18a
    .line 1593
    .end local v15           #comparingTime:Ljava/lang/String;
    .end local v21           #deleteCnt:I
    .end local v34           #whereArgsEx:[Ljava/lang/String;
    .end local v35           #whereEx:Ljava/lang/String;
    :cond_18a
    :goto_18a
    :try_start_18a
    const-string v6, "address=? AND reference_number=?"

    #@18c
    .line 1594
    .local v6, where:Ljava/lang/String;
    const/4 v3, 0x2

    #@18d
    new-array v7, v3, [Ljava/lang/String;

    #@18f
    const/4 v3, 0x0

    #@190
    aput-object p2, v7, v3

    #@192
    const/4 v3, 0x1

    #@193
    aput-object v29, v7, v3

    #@195
    .line 1597
    .local v7, whereArgs:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@197
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@199
    const-string v4, "vzw_ems_segment_timer"

    #@19b
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@19e
    move-result v3

    #@19f
    if-eqz v3, :cond_29c

    #@1a1
    .line 1598
    move-object/from16 v0, p0

    #@1a3
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@1a5
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@1a7
    sget-object v5, Lcom/android/internal/telephony/SMSDispatcher;->VZW_PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String;

    #@1a9
    const/4 v8, 0x0

    #@1aa
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1ad
    move-result-object v16

    #@1ae
    .line 1604
    :goto_1ae
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    #@1b1
    move-result v17

    #@1b2
    .line 1605
    .local v17, cursorCount:I
    add-int/lit8 v3, p5, -0x1

    #@1b4
    move/from16 v0, v17

    #@1b6
    if-eq v0, v3, :cond_2ab

    #@1b8
    .line 1607
    new-instance v33, Landroid/content/ContentValues;

    #@1ba
    invoke-direct/range {v33 .. v33}, Landroid/content/ContentValues;-><init>()V

    #@1bd
    .line 1608
    .local v33, values:Landroid/content/ContentValues;
    const-string v3, "date"

    #@1bf
    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1c2
    move-result-object v4

    #@1c3
    move-object/from16 v0, v33

    #@1c5
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@1c8
    .line 1609
    const-string v3, "pdu"

    #@1ca
    invoke-static/range {p1 .. p1}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@1cd
    move-result-object v4

    #@1ce
    move-object/from16 v0, v33

    #@1d0
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1d3
    .line 1610
    const-string v3, "address"

    #@1d5
    move-object/from16 v0, v33

    #@1d7
    move-object/from16 v1, p2

    #@1d9
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1dc
    .line 1611
    const-string v3, "reference_number"

    #@1de
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e1
    move-result-object v4

    #@1e2
    move-object/from16 v0, v33

    #@1e4
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1e7
    .line 1612
    const-string v3, "count"

    #@1e9
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1ec
    move-result-object v4

    #@1ed
    move-object/from16 v0, v33

    #@1ef
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1f2
    .line 1613
    const-string v3, "sequence"

    #@1f4
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f7
    move-result-object v4

    #@1f8
    move-object/from16 v0, v33

    #@1fa
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1fd
    .line 1614
    const/4 v3, -0x1

    #@1fe
    move/from16 v0, p8

    #@200
    if-eq v0, v3, :cond_20d

    #@202
    .line 1615
    const-string v3, "destination_port"

    #@204
    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@207
    move-result-object v4

    #@208
    move-object/from16 v0, v33

    #@20a
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@20d
    .line 1619
    :cond_20d
    move-object/from16 v0, p0

    #@20f
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@211
    const-string v4, "vzw_ems_segment_timer"

    #@213
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@216
    move-result v3

    #@217
    if-eqz v3, :cond_241

    #@219
    .line 1620
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    #@21c
    move-result v3

    #@21d
    if-lez v3, :cond_235

    #@21f
    .line 1621
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    #@222
    move-result v3

    #@223
    if-eqz v3, :cond_235

    #@225
    .line 1622
    const-string v3, "time"

    #@227
    move-object/from16 v0, v16

    #@229
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@22c
    move-result v32

    #@22d
    .line 1623
    .local v32, timeColumn:I
    move-object/from16 v0, v16

    #@22f
    move/from16 v1, v32

    #@231
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    #@234
    move-result-wide v13

    #@235
    .line 1626
    .end local v32           #timeColumn:I
    :cond_235
    const-string v3, "time"

    #@237
    new-instance v4, Ljava/lang/Long;

    #@239
    invoke-direct {v4, v13, v14}, Ljava/lang/Long;-><init>(J)V

    #@23c
    move-object/from16 v0, v33

    #@23e
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@241
    .line 1630
    :cond_241
    move-object/from16 v0, p0

    #@243
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@245
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@247
    move-object/from16 v0, v33

    #@249
    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@24c
    .line 1632
    move-object/from16 v0, p0

    #@24e
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@250
    const-string v4, "vzw_ems_segment_timer"

    #@252
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@255
    move-result v3

    #@256
    if-eqz v3, :cond_26d

    #@258
    .line 1633
    new-instance v3, Ljava/lang/Thread;

    #@25a
    new-instance v8, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;

    #@25c
    move-object/from16 v9, p0

    #@25e
    move-object/from16 v10, p2

    #@260
    move/from16 v11, p3

    #@262
    move/from16 v12, p5

    #@264
    invoke-direct/range {v8 .. v14}, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;-><init>(Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;IIJ)V

    #@267
    invoke-direct {v3, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@26a
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    #@26d
    .line 1636
    :cond_26d
    const/16 v30, 0x1

    #@26f
    .line 1671
    if-eqz v16, :cond_139

    #@271
    goto/16 :goto_136

    #@273
    .line 1584
    .end local v6           #where:Ljava/lang/String;
    .end local v7           #whereArgs:[Ljava/lang/String;
    .end local v17           #cursorCount:I
    .end local v33           #values:Landroid/content/ContentValues;
    .restart local v15       #comparingTime:Ljava/lang/String;
    .restart local v34       #whereArgsEx:[Ljava/lang/String;
    .restart local v35       #whereEx:Ljava/lang/String;
    :catch_273
    move-exception v22

    #@274
    .line 1585
    .local v22, e:Landroid/database/SQLException;
    const-string v3, "processMessagePart(), Can\'t access multipart SMS database"

    #@276
    move-object/from16 v0, v22

    #@278
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_27b
    .catchall {:try_start_18a .. :try_end_27b} :catchall_295
    .catch Landroid/database/SQLException; {:try_start_18a .. :try_end_27b} :catch_27d

    #@27b
    goto/16 :goto_18a

    #@27d
    .line 1667
    .end local v15           #comparingTime:Ljava/lang/String;
    .end local v22           #e:Landroid/database/SQLException;
    .end local v29           #refNumber:Ljava/lang/String;
    .end local v31           #seqNumber:Ljava/lang/String;
    .end local v34           #whereArgsEx:[Ljava/lang/String;
    .end local v35           #whereEx:Ljava/lang/String;
    :catch_27d
    move-exception v22

    #@27e
    .line 1668
    .restart local v22       #e:Landroid/database/SQLException;
    :try_start_27e
    const-string v3, "SMS"

    #@280
    const-string v4, "Can\'t access multipart SMS database"

    #@282
    move-object/from16 v0, v22

    #@284
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_287
    .catchall {:try_start_27e .. :try_end_287} :catchall_295

    #@287
    .line 1669
    const/16 v30, 0x2

    #@289
    .line 1671
    if-eqz v16, :cond_139

    #@28b
    goto/16 :goto_136

    #@28d
    .line 1586
    .end local v22           #e:Landroid/database/SQLException;
    .restart local v15       #comparingTime:Ljava/lang/String;
    .restart local v29       #refNumber:Ljava/lang/String;
    .restart local v31       #seqNumber:Ljava/lang/String;
    .restart local v34       #whereArgsEx:[Ljava/lang/String;
    .restart local v35       #whereEx:Ljava/lang/String;
    :catch_28d
    move-exception v22

    #@28e
    .line 1587
    .local v22, e:Ljava/lang/RuntimeException;
    :try_start_28e
    const-string v3, "processMessagePart(), Runtime Exception: maybe concat reference is mixed"

    #@290
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_293
    .catchall {:try_start_28e .. :try_end_293} :catchall_295
    .catch Landroid/database/SQLException; {:try_start_28e .. :try_end_293} :catch_27d

    #@293
    goto/16 :goto_18a

    #@295
    .line 1671
    .end local v15           #comparingTime:Ljava/lang/String;
    .end local v22           #e:Ljava/lang/RuntimeException;
    .end local v29           #refNumber:Ljava/lang/String;
    .end local v31           #seqNumber:Ljava/lang/String;
    .end local v34           #whereArgsEx:[Ljava/lang/String;
    .end local v35           #whereEx:Ljava/lang/String;
    :catchall_295
    move-exception v3

    #@296
    if-eqz v16, :cond_29b

    #@298
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    #@29b
    :cond_29b
    throw v3

    #@29c
    .line 1600
    .restart local v6       #where:Ljava/lang/String;
    .restart local v7       #whereArgs:[Ljava/lang/String;
    .restart local v29       #refNumber:Ljava/lang/String;
    .restart local v31       #seqNumber:Ljava/lang/String;
    :cond_29c
    :try_start_29c
    move-object/from16 v0, p0

    #@29e
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@2a0
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@2a2
    sget-object v5, Lcom/android/internal/telephony/SMSDispatcher;->PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String;

    #@2a4
    const/4 v8, 0x0

    #@2a5
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2a8
    move-result-object v16

    #@2a9
    goto/16 :goto_1ae

    #@2ab
    .line 1640
    .restart local v17       #cursorCount:I
    :cond_2ab
    move/from16 v0, p5

    #@2ad
    new-array v0, v0, [[B

    #@2af
    move-object/from16 v28, v0

    #@2b1
    .line 1641
    const/16 v23, 0x0

    #@2b3
    .local v23, i:I
    :goto_2b3
    move/from16 v0, v23

    #@2b5
    move/from16 v1, v17

    #@2b7
    if-ge v0, v1, :cond_2e9

    #@2b9
    .line 1642
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    #@2bc
    .line 1643
    const/4 v3, 0x1

    #@2bd
    move-object/from16 v0, v16

    #@2bf
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    #@2c2
    move-result v18

    #@2c3
    .line 1645
    .local v18, cursorSequence:I
    if-nez p9, :cond_2c7

    #@2c5
    .line 1646
    add-int/lit8 v18, v18, -0x1

    #@2c7
    .line 1648
    :cond_2c7
    const/4 v3, 0x0

    #@2c8
    move-object/from16 v0, v16

    #@2ca
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2cd
    move-result-object v3

    #@2ce
    invoke-static {v3}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@2d1
    move-result-object v3

    #@2d2
    aput-object v3, v28, v18

    #@2d4
    .line 1653
    if-nez v18, :cond_2e6

    #@2d6
    const/4 v3, 0x2

    #@2d7
    move-object/from16 v0, v16

    #@2d9
    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    #@2dc
    move-result v3

    #@2dd
    if-nez v3, :cond_2e6

    #@2df
    .line 1654
    const/4 v3, 0x2

    #@2e0
    move-object/from16 v0, v16

    #@2e2
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    #@2e5
    move-result p8

    #@2e6
    .line 1641
    :cond_2e6
    add-int/lit8 v23, v23, 0x1

    #@2e8
    goto :goto_2b3

    #@2e9
    .line 1659
    .end local v18           #cursorSequence:I
    :cond_2e9
    if-eqz p9, :cond_318

    #@2eb
    .line 1660
    aput-object p1, v28, p4

    #@2ed
    .line 1666
    :goto_2ed
    move-object/from16 v0, p0

    #@2ef
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@2f1
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@2f3
    invoke-virtual {v3, v4, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2f6
    .catchall {:try_start_29c .. :try_end_2f6} :catchall_295
    .catch Landroid/database/SQLException; {:try_start_29c .. :try_end_2f6} :catch_27d

    #@2f6
    .line 1671
    if-eqz v16, :cond_2fb

    #@2f8
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    #@2fb
    .line 1675
    :cond_2fb
    if-eqz p9, :cond_348

    #@2fd
    .line 1677
    new-instance v27, Ljava/io/ByteArrayOutputStream;

    #@2ff
    invoke-direct/range {v27 .. v27}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@302
    .line 1678
    .local v27, output:Ljava/io/ByteArrayOutputStream;
    const/16 v23, 0x0

    #@304
    :goto_304
    move/from16 v0, v23

    #@306
    move/from16 v1, p5

    #@308
    if-ge v0, v1, :cond_31d

    #@30a
    .line 1680
    aget-object v3, v28, v23

    #@30c
    const/4 v4, 0x0

    #@30d
    aget-object v5, v28, v23

    #@30f
    array-length v5, v5

    #@310
    move-object/from16 v0, v27

    #@312
    invoke-virtual {v0, v3, v4, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@315
    .line 1678
    add-int/lit8 v23, v23, 0x1

    #@317
    goto :goto_304

    #@318
    .line 1662
    .end local v27           #output:Ljava/io/ByteArrayOutputStream;
    :cond_318
    add-int/lit8 v3, p4, -0x1

    #@31a
    :try_start_31a
    aput-object p1, v28, v3
    :try_end_31c
    .catchall {:try_start_31a .. :try_end_31c} :catchall_295
    .catch Landroid/database/SQLException; {:try_start_31a .. :try_end_31c} :catch_27d

    #@31c
    goto :goto_2ed

    #@31d
    .line 1682
    .restart local v27       #output:Ljava/io/ByteArrayOutputStream;
    :cond_31d
    invoke-virtual/range {v27 .. v27}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@320
    move-result-object v20

    #@321
    .line 1685
    .local v20, datagram:[B
    const/16 v3, 0xb84

    #@323
    move/from16 v0, p8

    #@325
    if-ne v0, v3, :cond_333

    #@327
    .line 1687
    move-object/from16 v0, p0

    #@329
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@32b
    move-object/from16 v0, v20

    #@32d
    invoke-virtual {v3, v0}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([B)I

    #@330
    move-result v30

    #@331
    goto/16 :goto_139

    #@333
    .line 1689
    :cond_333
    const/4 v3, 0x1

    #@334
    new-array v0, v3, [[B

    #@336
    move-object/from16 v28, v0

    #@338
    .line 1690
    const/4 v3, 0x0

    #@339
    aput-object v20, v28, v3

    #@33b
    .line 1692
    move-object/from16 v0, p0

    #@33d
    move-object/from16 v1, v28

    #@33f
    move/from16 v2, p8

    #@341
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPortAddressedPdus([[BI)V

    #@344
    .line 1693
    const/16 v30, -0x1

    #@346
    goto/16 :goto_139

    #@348
    .line 1698
    .end local v20           #datagram:[B
    .end local v27           #output:Ljava/io/ByteArrayOutputStream;
    :cond_348
    const/4 v3, -0x1

    #@349
    move/from16 v0, p8

    #@34b
    if-eq v0, v3, :cond_3cc

    #@34d
    .line 1700
    move-object/from16 v0, p0

    #@34f
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@351
    const-string v4, "OperatorMessage"

    #@353
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@356
    move-result v3

    #@357
    const/4 v4, 0x1

    #@358
    if-ne v3, v4, :cond_36c

    #@35a
    .line 1701
    const/16 v30, 0x5

    #@35c
    .line 1702
    .local v30, result:I
    const/4 v3, 0x0

    #@35d
    const/4 v4, 0x1

    #@35e
    const/4 v5, 0x1

    #@35f
    move-object/from16 v0, p0

    #@361
    move-object/from16 v1, v28

    #@363
    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/android/internal/telephony/SMSDispatcher;->processOperatorMessage([[BLcom/android/internal/telephony/SmsMessageBase;ZZ)I

    #@366
    move-result v30

    #@367
    .line 1703
    const/4 v3, 0x5

    #@368
    move/from16 v0, v30

    #@36a
    if-ne v0, v3, :cond_139

    #@36c
    .line 1708
    .end local v30           #result:I
    :cond_36c
    const/16 v3, 0xb84

    #@36e
    move/from16 v0, p8

    #@370
    if-ne v0, v3, :cond_3bf

    #@372
    .line 1710
    new-instance v27, Ljava/io/ByteArrayOutputStream;

    #@374
    invoke-direct/range {v27 .. v27}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@377
    .line 1711
    .restart local v27       #output:Ljava/io/ByteArrayOutputStream;
    const/16 v23, 0x0

    #@379
    :goto_379
    move/from16 v0, v23

    #@37b
    move/from16 v1, p5

    #@37d
    if-ge v0, v1, :cond_39b

    #@37f
    .line 1712
    aget-object v3, v28, v23

    #@381
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@384
    move-result-object v4

    #@385
    invoke-static {v3, v4}, Landroid/telephony/SmsMessage;->createFromPdu([BLjava/lang/String;)Landroid/telephony/SmsMessage;

    #@388
    move-result-object v24

    #@389
    .line 1713
    .local v24, msg:Landroid/telephony/SmsMessage;
    invoke-virtual/range {v24 .. v24}, Landroid/telephony/SmsMessage;->getUserData()[B

    #@38c
    move-result-object v19

    #@38d
    .line 1714
    .local v19, data:[B
    const/4 v3, 0x0

    #@38e
    move-object/from16 v0, v19

    #@390
    array-length v4, v0

    #@391
    move-object/from16 v0, v27

    #@393
    move-object/from16 v1, v19

    #@395
    invoke-virtual {v0, v1, v3, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@398
    .line 1711
    add-int/lit8 v23, v23, 0x1

    #@39a
    goto :goto_379

    #@39b
    .line 1718
    .end local v19           #data:[B
    .end local v24           #msg:Landroid/telephony/SmsMessage;
    :cond_39b
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_WAPSERVICE:Z

    #@39d
    if-eqz v3, :cond_3b1

    #@39f
    .line 1719
    move-object/from16 v0, p0

    #@3a1
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@3a3
    invoke-virtual/range {v27 .. v27}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@3a6
    move-result-object v4

    #@3a7
    move-object/from16 v0, p10

    #@3a9
    move-object/from16 v1, p2

    #@3ab
    invoke-virtual {v3, v4, v0, v1}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([BLjava/lang/String;Ljava/lang/String;)I

    #@3ae
    move-result v30

    #@3af
    goto/16 :goto_139

    #@3b1
    .line 1723
    :cond_3b1
    move-object/from16 v0, p0

    #@3b3
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@3b5
    invoke-virtual/range {v27 .. v27}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@3b8
    move-result-object v4

    #@3b9
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([B)I

    #@3bc
    move-result v30

    #@3bd
    goto/16 :goto_139

    #@3bf
    .line 1726
    .end local v27           #output:Ljava/io/ByteArrayOutputStream;
    :cond_3bf
    move-object/from16 v0, p0

    #@3c1
    move-object/from16 v1, v28

    #@3c3
    move/from16 v2, p8

    #@3c5
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPortAddressedPdus([[BI)V

    #@3c8
    .line 1741
    :goto_3c8
    const/16 v30, -0x1

    #@3ca
    goto/16 :goto_139

    #@3cc
    .line 1730
    :cond_3cc
    move-object/from16 v0, p0

    #@3ce
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@3d0
    const-string v4, "OperatorMessage"

    #@3d2
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3d5
    move-result v3

    #@3d6
    const/4 v4, 0x1

    #@3d7
    if-ne v3, v4, :cond_3eb

    #@3d9
    .line 1731
    const/16 v30, 0x5

    #@3db
    .line 1732
    .restart local v30       #result:I
    const/4 v3, 0x0

    #@3dc
    const/4 v4, 0x0

    #@3dd
    const/4 v5, 0x1

    #@3de
    move-object/from16 v0, p0

    #@3e0
    move-object/from16 v1, v28

    #@3e2
    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/android/internal/telephony/SMSDispatcher;->processOperatorMessage([[BLcom/android/internal/telephony/SmsMessageBase;ZZ)I

    #@3e5
    move-result v30

    #@3e6
    .line 1733
    const/4 v3, 0x5

    #@3e7
    move/from16 v0, v30

    #@3e9
    if-ne v0, v3, :cond_139

    #@3eb
    .line 1739
    .end local v30           #result:I
    :cond_3eb
    move-object/from16 v0, p0

    #@3ed
    move-object/from16 v1, v28

    #@3ef
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[B)V

    #@3f2
    goto :goto_3c8
.end method

.method protected processMessagePart([BLjava/lang/String;IIIJIZLjava/lang/String;I)I
    .registers 49
    .parameter "pdu"
    .parameter "address"
    .parameter "referenceNumber"
    .parameter "sequenceNumber"
    .parameter "messageCount"
    .parameter "timestamp"
    .parameter "destPort"
    .parameter "isCdmaWapPush"
    .parameter "serviceCenter"
    .parameter "iccIndex"

    #@0
    .prologue
    .line 1748
    const/16 v30, 0x0

    #@2
    check-cast v30, [[B

    #@4
    .line 1749
    .local v30, pdus:[[B
    const/4 v10, 0x0

    #@5
    .line 1751
    .local v10, cursor:Landroid/database/Cursor;
    const/16 v20, 0x0

    #@7
    .line 1752
    .local v20, iccSring:Ljava/lang/String;
    const/16 v22, 0x0

    #@9
    .line 1754
    .local v22, indexOnIcc:I
    const-string v3, "SMS"

    #@b
    const-string v4, "processMessagePart"

    #@d
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1755
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "processMessagePart(), referenceNumber = "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    move/from16 v0, p3

    #@1d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@28
    .line 1756
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v4, "processMessagePart(), sequenceNumber = "

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    move/from16 v0, p4

    #@35
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@40
    .line 1757
    new-instance v3, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v4, "processMessagePart(), messageCount = "

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    move/from16 v0, p5

    #@4d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@58
    .line 1758
    new-instance v3, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v4, "processMessagePart(), destPort = "

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    move/from16 v0, p8

    #@65
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@70
    .line 1759
    new-instance v3, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v4, "processMessagePart(), isCdmaWapPush = "

    #@77
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v3

    #@7b
    move/from16 v0, p9

    #@7d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v3

    #@85
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@88
    .line 1760
    new-instance v3, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v4, "processMessagePart(), serviceCenter = "

    #@8f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v3

    #@93
    move-object/from16 v0, p10

    #@95
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v3

    #@99
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v3

    #@9d
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@a0
    .line 1761
    new-instance v3, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v4, "processMessagePart(), iccIndex = "

    #@a7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v3

    #@ab
    move/from16 v0, p11

    #@ad
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v3

    #@b1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v3

    #@b5
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@b8
    .line 1763
    move-object/from16 v0, p0

    #@ba
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@bc
    const-string v4, "seperate_processing_sms_uicc"

    #@be
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c1
    move-result v3

    #@c2
    if-eqz v3, :cond_c6

    #@c4
    .line 1764
    move/from16 v22, p11

    #@c6
    .line 1769
    :cond_c6
    :try_start_c6
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@c9
    move-result-object v31

    #@ca
    .line 1770
    .local v31, refNumber:Ljava/lang/String;
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@cd
    move-result-object v33

    #@ce
    .line 1774
    .local v33, seqNumber:Ljava/lang/String;
    move-object/from16 v0, p0

    #@d0
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@d2
    const-string v4, "ConcatMTCheckTimestamp_kddi"

    #@d4
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@d7
    move-result v3

    #@d8
    if-eqz v3, :cond_116

    #@da
    .line 1775
    const-wide/32 v3, 0x3e800

    #@dd
    sub-long v3, p6, v3

    #@df
    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@e2
    move-result-object v9

    #@e3
    .line 1776
    .local v9, comparingTime:Ljava/lang/String;
    const-string v36, "reference_number=? AND date<?"

    #@e5
    .line 1777
    .local v36, whereEx:Ljava/lang/String;
    const/4 v3, 0x2

    #@e6
    new-array v0, v3, [Ljava/lang/String;

    #@e8
    move-object/from16 v35, v0

    #@ea
    const/4 v3, 0x0

    #@eb
    aput-object v31, v35, v3

    #@ed
    const/4 v3, 0x1

    #@ee
    aput-object v9, v35, v3
    :try_end_f0
    .catchall {:try_start_c6 .. :try_end_f0} :catchall_1c9
    .catch Landroid/database/SQLException; {:try_start_c6 .. :try_end_f0} :catch_1b4

    #@f0
    .line 1781
    .local v35, whereArgsEx:[Ljava/lang/String;
    :try_start_f0
    move-object/from16 v0, p0

    #@f2
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@f4
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@f6
    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@f9
    move-result-object v5

    #@fa
    move-object/from16 v0, v35

    #@fc
    invoke-virtual {v3, v4, v5, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@ff
    move-result v15

    #@100
    .line 1782
    .local v15, deleteCnt:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@102
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@105
    const-string v4, "processMessagePart(), [KDDI] duplicated reference number & more than 256 secs earlier timestamp : "

    #@107
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v3

    #@10b
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v3

    #@10f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@112
    move-result-object v3

    #@113
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_116
    .catchall {:try_start_f0 .. :try_end_116} :catchall_1c9
    .catch Landroid/database/SQLException; {:try_start_f0 .. :try_end_116} :catch_1aa
    .catch Ljava/lang/RuntimeException; {:try_start_f0 .. :try_end_116} :catch_1c1

    #@116
    .line 1792
    .end local v9           #comparingTime:Ljava/lang/String;
    .end local v15           #deleteCnt:I
    .end local v35           #whereArgsEx:[Ljava/lang/String;
    .end local v36           #whereEx:Ljava/lang/String;
    :cond_116
    :goto_116
    :try_start_116
    move-object/from16 v0, p0

    #@118
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@11a
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@11c
    sget-object v5, Lcom/android/internal/telephony/SMSDispatcher;->PDU_PROJECTION:[Ljava/lang/String;

    #@11e
    const-string v6, "address=? AND reference_number=? AND sequence=?"

    #@120
    const/4 v8, 0x3

    #@121
    new-array v7, v8, [Ljava/lang/String;

    #@123
    const/4 v8, 0x0

    #@124
    aput-object p2, v7, v8

    #@126
    const/4 v8, 0x1

    #@127
    aput-object v31, v7, v8

    #@129
    const/4 v8, 0x2

    #@12a
    aput-object v33, v7, v8

    #@12c
    const/4 v8, 0x0

    #@12d
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@130
    move-result-object v10

    #@131
    .line 1796
    if-eqz v10, :cond_359

    #@133
    .line 1798
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@136
    move-result v3

    #@137
    if-eqz v3, :cond_1d0

    #@139
    .line 1799
    new-instance v3, Ljava/lang/StringBuilder;

    #@13b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13e
    const-string v4, "processMessagePart(), Discarding duplicate message segment from address="

    #@140
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v3

    #@144
    move-object/from16 v0, p2

    #@146
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v3

    #@14a
    const-string v4, " refNumber="

    #@14c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v3

    #@150
    move-object/from16 v0, v31

    #@152
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v3

    #@156
    const-string v4, " seqNumber="

    #@158
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v3

    #@15c
    move-object/from16 v0, v33

    #@15e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v3

    #@162
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@165
    move-result-object v3

    #@166
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@169
    .line 1801
    const/4 v3, 0x0

    #@16a
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@16d
    move-result-object v26

    #@16e
    .line 1802
    .local v26, oldPduString:Ljava/lang/String;
    invoke-static/range {v26 .. v26}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@171
    move-result-object v25

    #@172
    .line 1803
    .local v25, oldPdu:[B
    move-object/from16 v0, v25

    #@174
    move-object/from16 v1, p1

    #@176
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    #@179
    move-result v3

    #@17a
    if-nez v3, :cond_1a2

    #@17c
    .line 1804
    new-instance v3, Ljava/lang/StringBuilder;

    #@17e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@181
    const-string v4, "processMessagePart(), Warning: dup message segment PDU of length "

    #@183
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@186
    move-result-object v3

    #@187
    move-object/from16 v0, p1

    #@189
    array-length v4, v0

    #@18a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v3

    #@18e
    const-string v4, " is different from existing PDU of length "

    #@190
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@193
    move-result-object v3

    #@194
    move-object/from16 v0, v25

    #@196
    array-length v4, v0

    #@197
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v3

    #@19b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19e
    move-result-object v3

    #@19f
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_1a2
    .catchall {:try_start_116 .. :try_end_1a2} :catchall_1c9
    .catch Landroid/database/SQLException; {:try_start_116 .. :try_end_1a2} :catch_1b4

    #@1a2
    .line 1807
    :cond_1a2
    const/16 v32, 0x1

    #@1a4
    .line 1932
    if-eqz v10, :cond_1a9

    #@1a6
    .end local v25           #oldPdu:[B
    .end local v26           #oldPduString:Ljava/lang/String;
    .end local v31           #refNumber:Ljava/lang/String;
    .end local v33           #seqNumber:Ljava/lang/String;
    :goto_1a6
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@1a9
    .line 2049
    :cond_1a9
    :goto_1a9
    return v32

    #@1aa
    .line 1783
    .restart local v9       #comparingTime:Ljava/lang/String;
    .restart local v31       #refNumber:Ljava/lang/String;
    .restart local v33       #seqNumber:Ljava/lang/String;
    .restart local v35       #whereArgsEx:[Ljava/lang/String;
    .restart local v36       #whereEx:Ljava/lang/String;
    :catch_1aa
    move-exception v17

    #@1ab
    .line 1784
    .local v17, e:Landroid/database/SQLException;
    :try_start_1ab
    const-string v3, "processMessagePart(), Can\'t access multipart SMS database"

    #@1ad
    move-object/from16 v0, v17

    #@1af
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1b2
    .catchall {:try_start_1ab .. :try_end_1b2} :catchall_1c9
    .catch Landroid/database/SQLException; {:try_start_1ab .. :try_end_1b2} :catch_1b4

    #@1b2
    goto/16 :goto_116

    #@1b4
    .line 1928
    .end local v9           #comparingTime:Ljava/lang/String;
    .end local v17           #e:Landroid/database/SQLException;
    .end local v31           #refNumber:Ljava/lang/String;
    .end local v33           #seqNumber:Ljava/lang/String;
    .end local v35           #whereArgsEx:[Ljava/lang/String;
    .end local v36           #whereEx:Ljava/lang/String;
    :catch_1b4
    move-exception v17

    #@1b5
    .line 1929
    .restart local v17       #e:Landroid/database/SQLException;
    :goto_1b5
    :try_start_1b5
    const-string v3, "processMessagePart(), Can\'t access multipart SMS database"

    #@1b7
    move-object/from16 v0, v17

    #@1b9
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1bc
    .catchall {:try_start_1b5 .. :try_end_1bc} :catchall_1c9

    #@1bc
    .line 1930
    const/16 v32, 0x2

    #@1be
    .line 1932
    if-eqz v10, :cond_1a9

    #@1c0
    goto :goto_1a6

    #@1c1
    .line 1785
    .end local v17           #e:Landroid/database/SQLException;
    .restart local v9       #comparingTime:Ljava/lang/String;
    .restart local v31       #refNumber:Ljava/lang/String;
    .restart local v33       #seqNumber:Ljava/lang/String;
    .restart local v35       #whereArgsEx:[Ljava/lang/String;
    .restart local v36       #whereEx:Ljava/lang/String;
    :catch_1c1
    move-exception v17

    #@1c2
    .line 1786
    .local v17, e:Ljava/lang/RuntimeException;
    :try_start_1c2
    const-string v3, "processMessagePart(), Runtime Exception: maybe concat reference is mixed"

    #@1c4
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_1c7
    .catchall {:try_start_1c2 .. :try_end_1c7} :catchall_1c9
    .catch Landroid/database/SQLException; {:try_start_1c2 .. :try_end_1c7} :catch_1b4

    #@1c7
    goto/16 :goto_116

    #@1c9
    .line 1932
    .end local v9           #comparingTime:Ljava/lang/String;
    .end local v17           #e:Ljava/lang/RuntimeException;
    .end local v31           #refNumber:Ljava/lang/String;
    .end local v33           #seqNumber:Ljava/lang/String;
    .end local v35           #whereArgsEx:[Ljava/lang/String;
    .end local v36           #whereEx:Ljava/lang/String;
    :catchall_1c9
    move-exception v3

    #@1ca
    :goto_1ca
    if-eqz v10, :cond_1cf

    #@1cc
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@1cf
    :cond_1cf
    throw v3

    #@1d0
    .line 1809
    .restart local v31       #refNumber:Ljava/lang/String;
    .restart local v33       #seqNumber:Ljava/lang/String;
    :cond_1d0
    :try_start_1d0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@1d3
    .line 1816
    :goto_1d3
    move-object/from16 v0, p0

    #@1d5
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@1d7
    const-string v4, "ConcatMTCheckTimestamp"

    #@1d9
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1dc
    move-result v3

    #@1dd
    if-eqz v3, :cond_21e

    #@1df
    .line 1817
    const-wide/32 v3, 0x927c0

    #@1e2
    sub-long v3, p6, v3

    #@1e4
    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@1e7
    move-result-object v9

    #@1e8
    .line 1818
    .restart local v9       #comparingTime:Ljava/lang/String;
    const-string v36, "address=? AND reference_number=? AND date<?"

    #@1ea
    .line 1819
    .restart local v36       #whereEx:Ljava/lang/String;
    const/4 v3, 0x3

    #@1eb
    new-array v0, v3, [Ljava/lang/String;

    #@1ed
    move-object/from16 v35, v0

    #@1ef
    const/4 v3, 0x0

    #@1f0
    aput-object p2, v35, v3

    #@1f2
    const/4 v3, 0x1

    #@1f3
    aput-object v31, v35, v3

    #@1f5
    const/4 v3, 0x2

    #@1f6
    aput-object v9, v35, v3
    :try_end_1f8
    .catchall {:try_start_1d0 .. :try_end_1f8} :catchall_1c9
    .catch Landroid/database/SQLException; {:try_start_1d0 .. :try_end_1f8} :catch_1b4

    #@1f8
    .line 1823
    .restart local v35       #whereArgsEx:[Ljava/lang/String;
    :try_start_1f8
    move-object/from16 v0, p0

    #@1fa
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@1fc
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@1fe
    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@201
    move-result-object v5

    #@202
    move-object/from16 v0, v35

    #@204
    invoke-virtual {v3, v4, v5, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@207
    move-result v15

    #@208
    .line 1824
    .restart local v15       #deleteCnt:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@20a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20d
    const-string v4, "processMessagePart(), [KRSMS] duplicated reference number & more than 10 minutes earlier timestamp"

    #@20f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v3

    #@213
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@216
    move-result-object v3

    #@217
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21a
    move-result-object v3

    #@21b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_21e
    .catchall {:try_start_1f8 .. :try_end_21e} :catchall_1c9
    .catch Landroid/database/SQLException; {:try_start_1f8 .. :try_end_21e} :catch_360
    .catch Ljava/lang/RuntimeException; {:try_start_1f8 .. :try_end_21e} :catch_36a

    #@21e
    .line 1834
    .end local v9           #comparingTime:Ljava/lang/String;
    .end local v15           #deleteCnt:I
    .end local v35           #whereArgsEx:[Ljava/lang/String;
    .end local v36           #whereEx:Ljava/lang/String;
    :cond_21e
    :goto_21e
    :try_start_21e
    const-string v6, "address=? AND reference_number=?"

    #@220
    .line 1835
    .local v6, where:Ljava/lang/String;
    const/4 v3, 0x2

    #@221
    new-array v7, v3, [Ljava/lang/String;

    #@223
    const/4 v3, 0x0

    #@224
    aput-object p2, v7, v3

    #@226
    const/4 v3, 0x1

    #@227
    aput-object v31, v7, v3

    #@229
    .line 1837
    .local v7, whereArgs:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@22b
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@22d
    const-string v4, "vzw_ems_segment_timer"

    #@22f
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@232
    move-result v3

    #@233
    if-eqz v3, :cond_372

    #@235
    .line 1838
    move-object/from16 v0, p0

    #@237
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@239
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@23b
    sget-object v5, Lcom/android/internal/telephony/SMSDispatcher;->VZW_PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String;

    #@23d
    const/4 v8, 0x0

    #@23e
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@241
    move-result-object v10

    #@242
    .line 1844
    :goto_242
    if-eqz v10, :cond_443

    #@244
    .line 1845
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    #@247
    move-result v11

    #@248
    .line 1846
    .local v11, cursorCount:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@24a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24d
    const-string v4, "processMessagePart(), cursorCount = "

    #@24f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@252
    move-result-object v3

    #@253
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@256
    move-result-object v3

    #@257
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25a
    move-result-object v3

    #@25b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@25e
    .line 1847
    add-int/lit8 v3, p5, -0x1

    #@260
    if-eq v11, v3, :cond_381

    #@262
    .line 1849
    new-instance v34, Landroid/content/ContentValues;

    #@264
    invoke-direct/range {v34 .. v34}, Landroid/content/ContentValues;-><init>()V

    #@267
    .line 1850
    .local v34, values:Landroid/content/ContentValues;
    const-string v3, "date"

    #@269
    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@26c
    move-result-object v4

    #@26d
    move-object/from16 v0, v34

    #@26f
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@272
    .line 1851
    const-string v3, "pdu"

    #@274
    invoke-static/range {p1 .. p1}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@277
    move-result-object v4

    #@278
    move-object/from16 v0, v34

    #@27a
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@27d
    .line 1852
    const-string v3, "address"

    #@27f
    move-object/from16 v0, v34

    #@281
    move-object/from16 v1, p2

    #@283
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@286
    .line 1853
    const-string v3, "reference_number"

    #@288
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@28b
    move-result-object v4

    #@28c
    move-object/from16 v0, v34

    #@28e
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@291
    .line 1854
    const-string v3, "count"

    #@293
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@296
    move-result-object v4

    #@297
    move-object/from16 v0, v34

    #@299
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@29c
    .line 1855
    const-string v3, "sequence"

    #@29e
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a1
    move-result-object v4

    #@2a2
    move-object/from16 v0, v34

    #@2a4
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2a7
    .line 1856
    const/4 v3, -0x1

    #@2a8
    move/from16 v0, p8

    #@2aa
    if-eq v0, v3, :cond_2b7

    #@2ac
    .line 1857
    const-string v3, "destination_port"

    #@2ae
    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b1
    move-result-object v4

    #@2b2
    move-object/from16 v0, v34

    #@2b4
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2b7
    .line 1860
    :cond_2b7
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2bc
    const-string v4, "processMessagePart(), timestamp = "

    #@2be
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v3

    #@2c2
    move-wide/from16 v0, p6

    #@2c4
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2c7
    move-result-object v3

    #@2c8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2cb
    move-result-object v3

    #@2cc
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2cf
    .line 1861
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d4
    const-string v4, "processMessagePart(), address = "

    #@2d6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d9
    move-result-object v3

    #@2da
    move-object/from16 v0, p2

    #@2dc
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2df
    move-result-object v3

    #@2e0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e3
    move-result-object v3

    #@2e4
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2e7
    .line 1862
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2ec
    const-string v4, "processMessagePart(), referenceNumber = "

    #@2ee
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f1
    move-result-object v3

    #@2f2
    move/from16 v0, p3

    #@2f4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f7
    move-result-object v3

    #@2f8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2fb
    move-result-object v3

    #@2fc
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2ff
    .line 1863
    new-instance v3, Ljava/lang/StringBuilder;

    #@301
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@304
    const-string v4, "processMessagePart(), messageCount = "

    #@306
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@309
    move-result-object v3

    #@30a
    move/from16 v0, p5

    #@30c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30f
    move-result-object v3

    #@310
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@313
    move-result-object v3

    #@314
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@317
    .line 1864
    new-instance v3, Ljava/lang/StringBuilder;

    #@319
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31c
    const-string v4, "processMessagePart(), sequenceNumber = "

    #@31e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@321
    move-result-object v3

    #@322
    move/from16 v0, p4

    #@324
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@327
    move-result-object v3

    #@328
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32b
    move-result-object v3

    #@32c
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@32f
    .line 1866
    move-object/from16 v0, p0

    #@331
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@333
    const-string v4, "seperate_processing_sms_uicc"

    #@335
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@338
    move-result v3

    #@339
    if-eqz v3, :cond_348

    #@33b
    .line 1867
    if-lez v22, :cond_348

    #@33d
    .line 1868
    const-string v3, "icc_index"

    #@33f
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@342
    move-result-object v4

    #@343
    move-object/from16 v0, v34

    #@345
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@348
    .line 1871
    :cond_348
    move-object/from16 v0, p0

    #@34a
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@34c
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@34e
    move-object/from16 v0, v34

    #@350
    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@353
    .line 1872
    const/16 v32, 0x1

    #@355
    .line 1932
    if-eqz v10, :cond_1a9

    #@357
    goto/16 :goto_1a6

    #@359
    .line 1811
    .end local v6           #where:Ljava/lang/String;
    .end local v7           #whereArgs:[Ljava/lang/String;
    .end local v11           #cursorCount:I
    .end local v34           #values:Landroid/content/ContentValues;
    :cond_359
    const-string v3, "processMessagePart(), mResolver.query() returned null"

    #@35b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@35e
    goto/16 :goto_1d3

    #@360
    .line 1825
    .restart local v9       #comparingTime:Ljava/lang/String;
    .restart local v35       #whereArgsEx:[Ljava/lang/String;
    .restart local v36       #whereEx:Ljava/lang/String;
    :catch_360
    move-exception v17

    #@361
    .line 1826
    .local v17, e:Landroid/database/SQLException;
    const-string v3, "processMessagePart(), Can\'t access multipart SMS database"

    #@363
    move-object/from16 v0, v17

    #@365
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@368
    goto/16 :goto_21e

    #@36a
    .line 1827
    .end local v17           #e:Landroid/database/SQLException;
    :catch_36a
    move-exception v17

    #@36b
    .line 1828
    .local v17, e:Ljava/lang/RuntimeException;
    const-string v3, "processMessagePart(), Runtime Exception: maybe concat reference is mixed"

    #@36d
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@370
    goto/16 :goto_21e

    #@372
    .line 1840
    .end local v9           #comparingTime:Ljava/lang/String;
    .end local v17           #e:Ljava/lang/RuntimeException;
    .end local v35           #whereArgsEx:[Ljava/lang/String;
    .end local v36           #whereEx:Ljava/lang/String;
    .restart local v6       #where:Ljava/lang/String;
    .restart local v7       #whereArgs:[Ljava/lang/String;
    :cond_372
    move-object/from16 v0, p0

    #@374
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@376
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@378
    sget-object v5, Lcom/android/internal/telephony/SMSDispatcher;->PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String;

    #@37a
    const/4 v8, 0x0

    #@37b
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@37e
    move-result-object v10

    #@37f
    goto/16 :goto_242

    #@381
    .line 1875
    .restart local v11       #cursorCount:I
    :cond_381
    move-object/from16 v0, p0

    #@383
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@385
    const-string v4, "seperate_processing_sms_uicc"

    #@387
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@38a
    move-result v3

    #@38b
    if-eqz v3, :cond_3f2

    #@38d
    .line 1876
    const-string v3, "icc_index"

    #@38f
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@392
    move-result v19

    #@393
    .line 1877
    .local v19, iccColumn:I
    const-string v3, "pdu"

    #@395
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@398
    move-result v28

    #@399
    .line 1878
    .local v28, pduColumn:I
    new-instance v21, Ljava/lang/String;

    #@39b
    invoke-direct/range {v21 .. v21}, Ljava/lang/String;-><init>()V
    :try_end_39e
    .catchall {:try_start_21e .. :try_end_39e} :catchall_1c9
    .catch Landroid/database/SQLException; {:try_start_21e .. :try_end_39e} :catch_1b4

    #@39e
    .line 1879
    .end local v20           #iccSring:Ljava/lang/String;
    .local v21, iccSring:Ljava/lang/String;
    :try_start_39e
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_3a1
    .catchall {:try_start_39e .. :try_end_3a1} :catchall_5e7
    .catch Landroid/database/SQLException; {:try_start_39e .. :try_end_3a1} :catch_5ec

    #@3a1
    move-result v3

    #@3a2
    if-eqz v3, :cond_5f1

    #@3a4
    move-object/from16 v20, v21

    #@3a6
    .line 1881
    .end local v21           #iccSring:Ljava/lang/String;
    .restart local v20       #iccSring:Ljava/lang/String;
    :cond_3a6
    :try_start_3a6
    invoke-static/range {p1 .. p1}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@3a9
    move-result-object v3

    #@3aa
    move/from16 v0, v28

    #@3ac
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@3af
    move-result-object v4

    #@3b0
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b3
    move-result v3

    #@3b4
    if-eqz v3, :cond_3bc

    #@3b6
    .line 1882
    const/16 v32, 0x1

    #@3b8
    .line 1932
    if-eqz v10, :cond_1a9

    #@3ba
    goto/16 :goto_1a6

    #@3bc
    .line 1884
    :cond_3bc
    move/from16 v0, v19

    #@3be
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    #@3c1
    move-result v3

    #@3c2
    if-lez v3, :cond_3dc

    #@3c4
    .line 1885
    move/from16 v0, v19

    #@3c6
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    #@3c9
    move-result v3

    #@3ca
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3cd
    move-result-object v3

    #@3ce
    move-object/from16 v0, v20

    #@3d0
    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@3d3
    move-result-object v20

    #@3d4
    .line 1886
    const-string v3, ","

    #@3d6
    move-object/from16 v0, v20

    #@3d8
    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@3db
    move-result-object v20

    #@3dc
    .line 1888
    :cond_3dc
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@3df
    move-result v3

    #@3e0
    if-nez v3, :cond_3a6

    #@3e2
    .line 1890
    :goto_3e2
    const/4 v3, -0x1

    #@3e3
    invoke-interface {v10, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@3e6
    .line 1891
    if-lez v22, :cond_3f2

    #@3e8
    .line 1892
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3eb
    move-result-object v3

    #@3ec
    move-object/from16 v0, v20

    #@3ee
    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@3f1
    move-result-object v20

    #@3f2
    .line 1897
    .end local v19           #iccColumn:I
    .end local v28           #pduColumn:I
    :cond_3f2
    move/from16 v0, p5

    #@3f4
    new-array v0, v0, [[B

    #@3f6
    move-object/from16 v30, v0

    #@3f8
    .line 1898
    const/16 v18, 0x0

    #@3fa
    .local v18, i:I
    :goto_3fa
    move/from16 v0, v18

    #@3fc
    if-ge v0, v11, :cond_426

    #@3fe
    .line 1899
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@401
    .line 1900
    const/4 v3, 0x1

    #@402
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    #@405
    move-result v12

    #@406
    .line 1902
    .local v12, cursorSequence:I
    if-nez p9, :cond_40a

    #@408
    .line 1903
    add-int/lit8 v12, v12, -0x1

    #@40a
    .line 1905
    :cond_40a
    const/4 v3, 0x0

    #@40b
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@40e
    move-result-object v3

    #@40f
    invoke-static {v3}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@412
    move-result-object v3

    #@413
    aput-object v3, v30, v12

    #@415
    .line 1910
    if-nez v12, :cond_423

    #@417
    const/4 v3, 0x2

    #@418
    invoke-interface {v10, v3}, Landroid/database/Cursor;->isNull(I)Z

    #@41b
    move-result v3

    #@41c
    if-nez v3, :cond_423

    #@41e
    .line 1911
    const/4 v3, 0x2

    #@41f
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    #@422
    move-result p8

    #@423
    .line 1898
    :cond_423
    add-int/lit8 v18, v18, 0x1

    #@425
    goto :goto_3fa

    #@426
    .line 1916
    .end local v12           #cursorSequence:I
    :cond_426
    if-eqz p9, :cond_43e

    #@428
    .line 1917
    aput-object p1, v30, p4

    #@42a
    .line 1923
    :goto_42a
    move-object/from16 v0, p0

    #@42c
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@42e
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@430
    invoke-virtual {v3, v4, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_433
    .catchall {:try_start_3a6 .. :try_end_433} :catchall_1c9
    .catch Landroid/database/SQLException; {:try_start_3a6 .. :try_end_433} :catch_1b4

    #@433
    .line 1932
    .end local v11           #cursorCount:I
    .end local v18           #i:I
    :goto_433
    if-eqz v10, :cond_438

    #@435
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@438
    .line 1935
    :cond_438
    if-nez v30, :cond_449

    #@43a
    .line 1936
    const/16 v32, -0x1

    #@43c
    goto/16 :goto_1a9

    #@43e
    .line 1919
    .restart local v11       #cursorCount:I
    .restart local v18       #i:I
    :cond_43e
    add-int/lit8 v3, p4, -0x1

    #@440
    :try_start_440
    aput-object p1, v30, v3

    #@442
    goto :goto_42a

    #@443
    .line 1926
    .end local v11           #cursorCount:I
    .end local v18           #i:I
    :cond_443
    const-string v3, "processMessagePart(), mResolver.query() returned null"

    #@445
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_448
    .catchall {:try_start_440 .. :try_end_448} :catchall_1c9
    .catch Landroid/database/SQLException; {:try_start_440 .. :try_end_448} :catch_1b4

    #@448
    goto :goto_433

    #@449
    .line 1940
    :cond_449
    if-eqz p9, :cond_48f

    #@44b
    .line 1942
    new-instance v27, Ljava/io/ByteArrayOutputStream;

    #@44d
    invoke-direct/range {v27 .. v27}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@450
    .line 1943
    .local v27, output:Ljava/io/ByteArrayOutputStream;
    const/16 v18, 0x0

    #@452
    .restart local v18       #i:I
    :goto_452
    move/from16 v0, v18

    #@454
    move/from16 v1, p5

    #@456
    if-ge v0, v1, :cond_466

    #@458
    .line 1945
    aget-object v3, v30, v18

    #@45a
    const/4 v4, 0x0

    #@45b
    aget-object v5, v30, v18

    #@45d
    array-length v5, v5

    #@45e
    move-object/from16 v0, v27

    #@460
    invoke-virtual {v0, v3, v4, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@463
    .line 1943
    add-int/lit8 v18, v18, 0x1

    #@465
    goto :goto_452

    #@466
    .line 1947
    :cond_466
    invoke-virtual/range {v27 .. v27}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@469
    move-result-object v14

    #@46a
    .line 1950
    .local v14, datagram:[B
    const/16 v3, 0xb84

    #@46c
    move/from16 v0, p8

    #@46e
    if-ne v0, v3, :cond_47a

    #@470
    .line 1952
    move-object/from16 v0, p0

    #@472
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@474
    invoke-virtual {v3, v14}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([B)I

    #@477
    move-result v32

    #@478
    goto/16 :goto_1a9

    #@47a
    .line 1954
    :cond_47a
    const/4 v3, 0x1

    #@47b
    new-array v0, v3, [[B

    #@47d
    move-object/from16 v30, v0

    #@47f
    .line 1955
    const/4 v3, 0x0

    #@480
    aput-object v14, v30, v3

    #@482
    .line 1957
    move-object/from16 v0, p0

    #@484
    move-object/from16 v1, v30

    #@486
    move/from16 v2, p8

    #@488
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPortAddressedPdus([[BI)V

    #@48b
    .line 1958
    const/16 v32, -0x1

    #@48d
    goto/16 :goto_1a9

    #@48f
    .line 1963
    .end local v14           #datagram:[B
    .end local v18           #i:I
    .end local v27           #output:Ljava/io/ByteArrayOutputStream;
    :cond_48f
    const/4 v3, -0x1

    #@490
    move/from16 v0, p8

    #@492
    if-eq v0, v3, :cond_51e

    #@494
    .line 1965
    move-object/from16 v0, p0

    #@496
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@498
    const-string v4, "OperatorMessage"

    #@49a
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@49d
    move-result v3

    #@49e
    const/4 v4, 0x1

    #@49f
    if-ne v3, v4, :cond_4b3

    #@4a1
    .line 1966
    const/16 v32, 0x5

    #@4a3
    .line 1967
    .local v32, result:I
    const/4 v3, 0x0

    #@4a4
    const/4 v4, 0x1

    #@4a5
    const/4 v5, 0x1

    #@4a6
    move-object/from16 v0, p0

    #@4a8
    move-object/from16 v1, v30

    #@4aa
    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/android/internal/telephony/SMSDispatcher;->processOperatorMessage([[BLcom/android/internal/telephony/SmsMessageBase;ZZ)I

    #@4ad
    move-result v32

    #@4ae
    .line 1968
    const/4 v3, 0x5

    #@4af
    move/from16 v0, v32

    #@4b1
    if-ne v0, v3, :cond_1a9

    #@4b3
    .line 1973
    .end local v32           #result:I
    :cond_4b3
    const/16 v3, 0xb84

    #@4b5
    move/from16 v0, p8

    #@4b7
    if-ne v0, v3, :cond_50c

    #@4b9
    .line 1975
    new-instance v27, Ljava/io/ByteArrayOutputStream;

    #@4bb
    invoke-direct/range {v27 .. v27}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@4be
    .line 1976
    .restart local v27       #output:Ljava/io/ByteArrayOutputStream;
    const/16 v18, 0x0

    #@4c0
    .restart local v18       #i:I
    :goto_4c0
    move/from16 v0, v18

    #@4c2
    move/from16 v1, p5

    #@4c4
    if-ge v0, v1, :cond_4e8

    #@4c6
    .line 1977
    aget-object v3, v30, v18

    #@4c8
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@4cb
    move-result-object v4

    #@4cc
    invoke-static {v3, v4}, Landroid/telephony/SmsMessage;->createFromPdu([BLjava/lang/String;)Landroid/telephony/SmsMessage;

    #@4cf
    move-result-object v23

    #@4d0
    .line 1978
    .local v23, msg:Landroid/telephony/SmsMessage;
    if-eqz v23, :cond_4e0

    #@4d2
    .line 1979
    invoke-virtual/range {v23 .. v23}, Landroid/telephony/SmsMessage;->getUserData()[B

    #@4d5
    move-result-object v13

    #@4d6
    .line 1980
    .local v13, data:[B
    const/4 v3, 0x0

    #@4d7
    array-length v4, v13

    #@4d8
    move-object/from16 v0, v27

    #@4da
    invoke-virtual {v0, v13, v3, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@4dd
    .line 1976
    .end local v13           #data:[B
    :goto_4dd
    add-int/lit8 v18, v18, 0x1

    #@4df
    goto :goto_4c0

    #@4e0
    .line 1983
    :cond_4e0
    const-string v3, "SMS"

    #@4e2
    const-string v4, "SMSDispatcher.processMessagePart(): SmsMessage.createFromPdu returned null"

    #@4e4
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4e7
    goto :goto_4dd

    #@4e8
    .line 1988
    .end local v23           #msg:Landroid/telephony/SmsMessage;
    :cond_4e8
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_WAPSERVICE:Z

    #@4ea
    if-eqz v3, :cond_4fe

    #@4ec
    .line 1989
    move-object/from16 v0, p0

    #@4ee
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@4f0
    invoke-virtual/range {v27 .. v27}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@4f3
    move-result-object v4

    #@4f4
    move-object/from16 v0, p10

    #@4f6
    move-object/from16 v1, p2

    #@4f8
    invoke-virtual {v3, v4, v0, v1}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([BLjava/lang/String;Ljava/lang/String;)I

    #@4fb
    move-result v32

    #@4fc
    goto/16 :goto_1a9

    #@4fe
    .line 1993
    :cond_4fe
    move-object/from16 v0, p0

    #@500
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@502
    invoke-virtual/range {v27 .. v27}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@505
    move-result-object v4

    #@506
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([B)I

    #@509
    move-result v32

    #@50a
    goto/16 :goto_1a9

    #@50c
    .line 1996
    .end local v18           #i:I
    .end local v27           #output:Ljava/io/ByteArrayOutputStream;
    :cond_50c
    move-object/from16 v0, p0

    #@50e
    move-object/from16 v1, v30

    #@510
    move/from16 v2, p8

    #@512
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPortAddressedPdus([[BI)V

    #@515
    .line 2047
    :goto_515
    const-string v3, "processMessagePart(), SMSDispatcher --> UI"

    #@517
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@51a
    .line 2049
    const/16 v32, -0x1

    #@51c
    goto/16 :goto_1a9

    #@51e
    .line 2000
    :cond_51e
    move-object/from16 v0, p0

    #@520
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@522
    const-string v4, "OperatorMessage"

    #@524
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@527
    move-result v3

    #@528
    const/4 v4, 0x1

    #@529
    if-ne v3, v4, :cond_53d

    #@52b
    .line 2001
    const/16 v32, 0x5

    #@52d
    .line 2002
    .restart local v32       #result:I
    const/4 v3, 0x0

    #@52e
    const/4 v4, 0x0

    #@52f
    const/4 v5, 0x1

    #@530
    move-object/from16 v0, p0

    #@532
    move-object/from16 v1, v30

    #@534
    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/android/internal/telephony/SMSDispatcher;->processOperatorMessage([[BLcom/android/internal/telephony/SmsMessageBase;ZZ)I

    #@537
    move-result v32

    #@538
    .line 2003
    const/4 v3, 0x5

    #@539
    move/from16 v0, v32

    #@53b
    if-ne v0, v3, :cond_1a9

    #@53d
    .line 2009
    .end local v32           #result:I
    :cond_53d
    move-object/from16 v0, p0

    #@53f
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@541
    const-string v4, "seperate_processing_sms_uicc"

    #@543
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@546
    move-result v3

    #@547
    if-eqz v3, :cond_553

    #@549
    .line 2010
    move-object/from16 v0, p0

    #@54b
    move-object/from16 v1, v30

    #@54d
    move-object/from16 v2, v20

    #@54f
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[BLjava/lang/String;)V

    #@552
    goto :goto_515

    #@553
    .line 2013
    :cond_553
    const/4 v3, 0x0

    #@554
    const-string v4, "kddi_message_duplicate_check"

    #@556
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@559
    move-result v3

    #@55a
    const/4 v4, 0x1

    #@55b
    if-ne v3, v4, :cond_5de

    #@55d
    .line 2015
    move-object/from16 v0, v30

    #@55f
    array-length v0, v0

    #@560
    move/from16 v29, v0

    #@562
    .line 2016
    .local v29, pduCount:I
    move/from16 v0, v29

    #@564
    new-array v0, v0, [Landroid/telephony/SmsMessage;

    #@566
    move-object/from16 v24, v0

    #@568
    .line 2017
    .local v24, msgs:[Landroid/telephony/SmsMessage;
    const/16 v18, 0x0

    #@56a
    .restart local v18       #i:I
    :goto_56a
    move/from16 v0, v18

    #@56c
    move/from16 v1, v29

    #@56e
    if-ge v0, v1, :cond_57b

    #@570
    .line 2018
    aget-object v3, v30, v18

    #@572
    invoke-static {v3}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    #@575
    move-result-object v3

    #@576
    aput-object v3, v24, v18

    #@578
    .line 2017
    add-int/lit8 v18, v18, 0x1

    #@57a
    goto :goto_56a

    #@57b
    .line 2021
    :cond_57b
    const/4 v3, 0x0

    #@57c
    aget-object v3, v24, v3

    #@57e
    iget-object v3, v3, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@580
    instance-of v3, v3, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@582
    if-eqz v3, :cond_5a4

    #@584
    .line 2022
    const/4 v3, 0x0

    #@585
    aget-object v3, v24, v3

    #@587
    iget-object v3, v3, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@589
    check-cast v3, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@58b
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSmsEnvelope()Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@58e
    move-result-object v3

    #@58f
    iget v3, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@591
    const/4 v4, 0x1

    #@592
    if-ne v3, v4, :cond_5a4

    #@594
    .line 2024
    const-string v3, "processMessagePart(), [KDDI] Broadcast Message!!, No duplicate check"

    #@596
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@599
    .line 2025
    move-object/from16 v0, p0

    #@59b
    move-object/from16 v1, v30

    #@59d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[B)V

    #@5a0
    .line 2026
    const/16 v32, -0x1

    #@5a2
    goto/16 :goto_1a9

    #@5a4
    .line 2031
    :cond_5a4
    const/4 v3, 0x0

    #@5a5
    aget-object v3, v24, v3

    #@5a7
    iget-object v3, v3, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@5a9
    iget v3, v3, Lcom/android/internal/telephony/SmsMessageBase;->messageRef:I

    #@5ab
    const/4 v4, 0x0

    #@5ac
    aget-object v4, v24, v4

    #@5ae
    invoke-virtual {v4}, Landroid/telephony/SmsMessage;->getTimestampMillis()J

    #@5b1
    move-result-wide v4

    #@5b2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5b5
    move-result-object v4

    #@5b6
    const/4 v5, 0x0

    #@5b7
    aget-object v5, v24, v5

    #@5b9
    invoke-virtual {v5}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@5bc
    move-result-object v5

    #@5bd
    const/4 v8, 0x0

    #@5be
    aget-object v8, v24, v8

    #@5c0
    invoke-virtual {v8}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@5c3
    move-result-object v8

    #@5c4
    move-object/from16 v0, p0

    #@5c6
    invoke-virtual {v0, v3, v4, v5, v8}, Lcom/android/internal/telephony/SMSDispatcher;->checkDuplicateKddiMessage(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Z

    #@5c9
    move-result v16

    #@5ca
    .line 2033
    .local v16, discard:Z
    if-eqz v16, :cond_5d5

    #@5cc
    .line 2035
    const-string v3, "processMessagePart(), [KDDI] discard duplicate Message "

    #@5ce
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5d1
    .line 2036
    const/16 v32, 0x1

    #@5d3
    goto/16 :goto_1a9

    #@5d5
    .line 2038
    :cond_5d5
    move-object/from16 v0, p0

    #@5d7
    move-object/from16 v1, v30

    #@5d9
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[B)V

    #@5dc
    goto/16 :goto_515

    #@5de
    .line 2043
    .end local v16           #discard:Z
    .end local v18           #i:I
    .end local v24           #msgs:[Landroid/telephony/SmsMessage;
    .end local v29           #pduCount:I
    :cond_5de
    move-object/from16 v0, p0

    #@5e0
    move-object/from16 v1, v30

    #@5e2
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[B)V

    #@5e5
    goto/16 :goto_515

    #@5e7
    .line 1932
    .end local v20           #iccSring:Ljava/lang/String;
    .restart local v11       #cursorCount:I
    .restart local v19       #iccColumn:I
    .restart local v21       #iccSring:Ljava/lang/String;
    .restart local v28       #pduColumn:I
    :catchall_5e7
    move-exception v3

    #@5e8
    move-object/from16 v20, v21

    #@5ea
    .end local v21           #iccSring:Ljava/lang/String;
    .restart local v20       #iccSring:Ljava/lang/String;
    goto/16 :goto_1ca

    #@5ec
    .line 1928
    .end local v20           #iccSring:Ljava/lang/String;
    .restart local v21       #iccSring:Ljava/lang/String;
    :catch_5ec
    move-exception v17

    #@5ed
    move-object/from16 v20, v21

    #@5ef
    .end local v21           #iccSring:Ljava/lang/String;
    .restart local v20       #iccSring:Ljava/lang/String;
    goto/16 :goto_1b5

    #@5f1
    .end local v20           #iccSring:Ljava/lang/String;
    .restart local v21       #iccSring:Ljava/lang/String;
    :cond_5f1
    move-object/from16 v20, v21

    #@5f3
    .end local v21           #iccSring:Ljava/lang/String;
    .restart local v20       #iccSring:Ljava/lang/String;
    goto/16 :goto_3e2
.end method

.method protected processMessagePartKRTestBed([BLjava/lang/String;IIIJIZLjava/lang/String;)I
    .registers 43
    .parameter "pdu"
    .parameter "address"
    .parameter "referenceNumber"
    .parameter "sequenceNumber"
    .parameter "messageCount"
    .parameter "timestamp"
    .parameter "destPort"
    .parameter "isCdmaWapPush"
    .parameter "serviceCenter"

    #@0
    .prologue
    .line 2057
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v12

    #@4
    .line 2060
    .local v12, firstTime:J
    const/16 v25, 0x0

    #@6
    check-cast v25, [[B

    #@8
    .line 2061
    .local v25, pdus:[[B
    const/4 v14, 0x0

    #@9
    .line 2064
    .local v14, cursor:Landroid/database/Cursor;
    :try_start_9
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@c
    move-result-object v26

    #@d
    .line 2065
    .local v26, refNumber:Ljava/lang/String;
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@10
    move-result-object v27

    #@11
    .line 2068
    .local v27, seqNumber:Ljava/lang/String;
    move-object/from16 v0, p0

    #@13
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@15
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@17
    sget-object v5, Lcom/android/internal/telephony/SMSDispatcher;->PDU_PROJECTION:[Ljava/lang/String;

    #@19
    const-string v6, "address=? AND reference_number=? AND sequence=?"

    #@1b
    const/4 v7, 0x3

    #@1c
    new-array v7, v7, [Ljava/lang/String;

    #@1e
    const/4 v8, 0x0

    #@1f
    aput-object p2, v7, v8

    #@21
    const/4 v8, 0x1

    #@22
    aput-object v26, v7, v8

    #@24
    const/4 v8, 0x2

    #@25
    aput-object v27, v7, v8

    #@27
    const/4 v8, 0x0

    #@28
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2b
    move-result-object v14

    #@2c
    .line 2072
    if-eqz v14, :cond_1bd

    #@2e
    .line 2074
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_a8

    #@34
    .line 2075
    const-string v3, "SMS"

    #@36
    new-instance v4, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v5, "Discarding duplicate message segment from address="

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    move-object/from16 v0, p2

    #@43
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v4

    #@47
    const-string v5, " refNumber="

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    move-object/from16 v0, v26

    #@4f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    const-string v5, " seqNumber="

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    move-object/from16 v0, v27

    #@5b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 2077
    const/4 v3, 0x0

    #@67
    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@6a
    move-result-object v23

    #@6b
    .line 2078
    .local v23, oldPduString:Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@6e
    move-result-object v22

    #@6f
    .line 2079
    .local v22, oldPdu:[B
    move-object/from16 v0, v22

    #@71
    move-object/from16 v1, p1

    #@73
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    #@76
    move-result v3

    #@77
    if-nez v3, :cond_a1

    #@79
    .line 2080
    const-string v3, "SMS"

    #@7b
    new-instance v4, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v5, "Warning: dup message segment PDU of length "

    #@82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    move-object/from16 v0, p1

    #@88
    array-length v5, v0

    #@89
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v4

    #@8d
    const-string v5, " is different from existing PDU of length "

    #@8f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v4

    #@93
    move-object/from16 v0, v22

    #@95
    array-length v5, v0

    #@96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@99
    move-result-object v4

    #@9a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v4

    #@9e
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a1
    .catchall {:try_start_9 .. :try_end_a1} :catchall_266
    .catch Landroid/database/SQLException; {:try_start_9 .. :try_end_a1} :catch_1c6

    #@a1
    .line 2083
    :cond_a1
    const/4 v3, 0x1

    #@a2
    .line 2189
    if-eqz v14, :cond_a7

    #@a4
    .end local v22           #oldPdu:[B
    .end local v23           #oldPduString:Ljava/lang/String;
    .end local v26           #refNumber:Ljava/lang/String;
    .end local v27           #seqNumber:Ljava/lang/String;
    :goto_a4
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@a7
    .line 2252
    :cond_a7
    :goto_a7
    return v3

    #@a8
    .line 2085
    .restart local v26       #refNumber:Ljava/lang/String;
    .restart local v27       #seqNumber:Ljava/lang/String;
    :cond_a8
    :try_start_a8
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@ab
    .line 2092
    :goto_ab
    const-string v6, ""

    #@ad
    .line 2093
    .local v6, where:Ljava/lang/String;
    move-object/from16 v0, p0

    #@af
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@b1
    const-string v4, "cdma_kr_testbed_mms_receive"

    #@b3
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@b6
    move-result v3

    #@b7
    if-eqz v3, :cond_1d5

    #@b9
    if-eqz p9, :cond_1d5

    #@bb
    .line 2094
    new-instance v31, Ljava/lang/StringBuilder;

    #@bd
    const-string v3, "reference_number ="

    #@bf
    move-object/from16 v0, v31

    #@c1
    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@c4
    .line 2095
    .local v31, whereTemp:Ljava/lang/StringBuilder;
    move-object/from16 v0, v31

    #@c6
    move-object/from16 v1, v26

    #@c8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    .line 2096
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v6

    #@cf
    .line 2101
    .end local v31           #whereTemp:Ljava/lang/StringBuilder;
    :goto_cf
    const/4 v3, 0x2

    #@d0
    new-array v0, v3, [Ljava/lang/String;

    #@d2
    move-object/from16 v30, v0

    #@d4
    const/4 v3, 0x0

    #@d5
    aput-object p2, v30, v3

    #@d7
    const/4 v3, 0x1

    #@d8
    aput-object v26, v30, v3

    #@da
    .line 2102
    .local v30, whereArgs:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@dc
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@de
    const-string v4, "cdma_kr_testbed_mms_receive"

    #@e0
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e3
    move-result v3

    #@e4
    if-eqz v3, :cond_1d9

    #@e6
    if-eqz p9, :cond_1d9

    #@e8
    .line 2103
    const-string v3, "SMS"

    #@ea
    const-string v4, "processMessagePart() - KEY_CDMA_MMS_RECEIVE and CDMA WAP PUSH - RawUri query"

    #@ec
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    .line 2104
    move-object/from16 v0, p0

    #@f1
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@f3
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@f5
    sget-object v5, Lcom/android/internal/telephony/SMSDispatcher;->VZW_PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String;

    #@f7
    const/4 v7, 0x0

    #@f8
    const/4 v8, 0x0

    #@f9
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@fc
    move-result-object v14

    #@fd
    .line 2115
    :goto_fd
    if-eqz v14, :cond_279

    #@ff
    .line 2116
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    #@102
    move-result v15

    #@103
    .line 2117
    .local v15, cursorCount:I
    add-int/lit8 v3, p5, -0x1

    #@105
    if-eq v15, v3, :cond_207

    #@107
    .line 2119
    new-instance v29, Landroid/content/ContentValues;

    #@109
    invoke-direct/range {v29 .. v29}, Landroid/content/ContentValues;-><init>()V

    #@10c
    .line 2120
    .local v29, values:Landroid/content/ContentValues;
    const-string v3, "date"

    #@10e
    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@111
    move-result-object v4

    #@112
    move-object/from16 v0, v29

    #@114
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@117
    .line 2121
    const-string v3, "pdu"

    #@119
    invoke-static/range {p1 .. p1}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@11c
    move-result-object v4

    #@11d
    move-object/from16 v0, v29

    #@11f
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@122
    .line 2122
    const-string v3, "address"

    #@124
    move-object/from16 v0, v29

    #@126
    move-object/from16 v1, p2

    #@128
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@12b
    .line 2123
    const-string v3, "reference_number"

    #@12d
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@130
    move-result-object v4

    #@131
    move-object/from16 v0, v29

    #@133
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@136
    .line 2124
    const-string v3, "count"

    #@138
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13b
    move-result-object v4

    #@13c
    move-object/from16 v0, v29

    #@13e
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@141
    .line 2125
    const-string v3, "sequence"

    #@143
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@146
    move-result-object v4

    #@147
    move-object/from16 v0, v29

    #@149
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@14c
    .line 2126
    const/4 v3, -0x1

    #@14d
    move/from16 v0, p8

    #@14f
    if-eq v0, v3, :cond_15c

    #@151
    .line 2127
    const-string v3, "destination_port"

    #@153
    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@156
    move-result-object v4

    #@157
    move-object/from16 v0, v29

    #@159
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@15c
    .line 2130
    :cond_15c
    move-object/from16 v0, p0

    #@15e
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@160
    const-string v4, "vzw_ems_segment_timer"

    #@162
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@165
    move-result v3

    #@166
    if-eqz v3, :cond_18c

    #@168
    .line 2131
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    #@16b
    move-result v3

    #@16c
    if-lez v3, :cond_180

    #@16e
    .line 2132
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    #@171
    move-result v3

    #@172
    if-eqz v3, :cond_180

    #@174
    .line 2133
    const-string v3, "time"

    #@176
    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@179
    move-result v28

    #@17a
    .line 2134
    .local v28, timeColumn:I
    move/from16 v0, v28

    #@17c
    invoke-interface {v14, v0}, Landroid/database/Cursor;->getLong(I)J

    #@17f
    move-result-wide v12

    #@180
    .line 2137
    .end local v28           #timeColumn:I
    :cond_180
    const-string v3, "time"

    #@182
    new-instance v4, Ljava/lang/Long;

    #@184
    invoke-direct {v4, v12, v13}, Ljava/lang/Long;-><init>(J)V

    #@187
    move-object/from16 v0, v29

    #@189
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@18c
    .line 2140
    :cond_18c
    move-object/from16 v0, p0

    #@18e
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@190
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@192
    move-object/from16 v0, v29

    #@194
    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@197
    .line 2142
    move-object/from16 v0, p0

    #@199
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@19b
    const-string v4, "vzw_ems_segment_timer"

    #@19d
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1a0
    move-result v3

    #@1a1
    if-eqz v3, :cond_1b8

    #@1a3
    .line 2143
    new-instance v3, Ljava/lang/Thread;

    #@1a5
    new-instance v7, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;

    #@1a7
    move-object/from16 v8, p0

    #@1a9
    move-object/from16 v9, p2

    #@1ab
    move/from16 v10, p3

    #@1ad
    move/from16 v11, p5

    #@1af
    invoke-direct/range {v7 .. v13}, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;-><init>(Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;IIJ)V

    #@1b2
    invoke-direct {v3, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@1b5
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    #@1b8
    .line 2146
    :cond_1b8
    const/4 v3, 0x1

    #@1b9
    .line 2189
    if-eqz v14, :cond_a7

    #@1bb
    goto/16 :goto_a4

    #@1bd
    .line 2087
    .end local v6           #where:Ljava/lang/String;
    .end local v15           #cursorCount:I
    .end local v29           #values:Landroid/content/ContentValues;
    .end local v30           #whereArgs:[Ljava/lang/String;
    :cond_1bd
    const-string v3, "SMS"

    #@1bf
    const-string v4, "SMSDispatcher.processMessagePartKRTestBed(): mResolver.query() returned null"

    #@1c1
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1c4
    .catchall {:try_start_a8 .. :try_end_1c4} :catchall_266
    .catch Landroid/database/SQLException; {:try_start_a8 .. :try_end_1c4} :catch_1c6

    #@1c4
    goto/16 :goto_ab

    #@1c6
    .line 2185
    .end local v26           #refNumber:Ljava/lang/String;
    .end local v27           #seqNumber:Ljava/lang/String;
    :catch_1c6
    move-exception v19

    #@1c7
    .line 2186
    .local v19, e:Landroid/database/SQLException;
    :try_start_1c7
    const-string v3, "SMS"

    #@1c9
    const-string v4, "Can\'t access multipart SMS database"

    #@1cb
    move-object/from16 v0, v19

    #@1cd
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1d0
    .catchall {:try_start_1c7 .. :try_end_1d0} :catchall_266

    #@1d0
    .line 2187
    const/4 v3, 0x2

    #@1d1
    .line 2189
    if-eqz v14, :cond_a7

    #@1d3
    goto/16 :goto_a4

    #@1d5
    .line 2098
    .end local v19           #e:Landroid/database/SQLException;
    .restart local v6       #where:Ljava/lang/String;
    .restart local v26       #refNumber:Ljava/lang/String;
    .restart local v27       #seqNumber:Ljava/lang/String;
    :cond_1d5
    :try_start_1d5
    const-string v6, "address=? AND reference_number=?"

    #@1d7
    goto/16 :goto_cf

    #@1d9
    .line 2107
    .restart local v30       #whereArgs:[Ljava/lang/String;
    :cond_1d9
    move-object/from16 v0, p0

    #@1db
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@1dd
    const-string v4, "vzw_ems_segment_timer"

    #@1df
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1e2
    move-result v3

    #@1e3
    if-eqz v3, :cond_1f6

    #@1e5
    .line 2108
    move-object/from16 v0, p0

    #@1e7
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@1e9
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@1eb
    sget-object v5, Lcom/android/internal/telephony/SMSDispatcher;->VZW_PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String;

    #@1ed
    const/4 v8, 0x0

    #@1ee
    move-object/from16 v7, v30

    #@1f0
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1f3
    move-result-object v14

    #@1f4
    goto/16 :goto_fd

    #@1f6
    .line 2110
    :cond_1f6
    move-object/from16 v0, p0

    #@1f8
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@1fa
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@1fc
    sget-object v5, Lcom/android/internal/telephony/SMSDispatcher;->PDU_SEQUENCE_PORT_PROJECTION:[Ljava/lang/String;

    #@1fe
    const/4 v8, 0x0

    #@1ff
    move-object/from16 v7, v30

    #@201
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@204
    move-result-object v14

    #@205
    goto/16 :goto_fd

    #@207
    .line 2150
    .restart local v15       #cursorCount:I
    :cond_207
    move/from16 v0, p5

    #@209
    new-array v0, v0, [[B

    #@20b
    move-object/from16 v25, v0

    #@20d
    .line 2151
    const/16 v20, 0x0

    #@20f
    .local v20, i:I
    :goto_20f
    move/from16 v0, v20

    #@211
    if-ge v0, v15, :cond_23b

    #@213
    .line 2152
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    #@216
    .line 2153
    const/4 v3, 0x1

    #@217
    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I

    #@21a
    move-result v16

    #@21b
    .line 2155
    .local v16, cursorSequence:I
    if-nez p9, :cond_21f

    #@21d
    .line 2156
    add-int/lit8 v16, v16, -0x1

    #@21f
    .line 2158
    :cond_21f
    const/4 v3, 0x0

    #@220
    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@223
    move-result-object v3

    #@224
    invoke-static {v3}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@227
    move-result-object v3

    #@228
    aput-object v3, v25, v16

    #@22a
    .line 2163
    if-nez v16, :cond_238

    #@22c
    const/4 v3, 0x2

    #@22d
    invoke-interface {v14, v3}, Landroid/database/Cursor;->isNull(I)Z

    #@230
    move-result v3

    #@231
    if-nez v3, :cond_238

    #@233
    .line 2164
    const/4 v3, 0x2

    #@234
    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I

    #@237
    move-result p8

    #@238
    .line 2151
    :cond_238
    add-int/lit8 v20, v20, 0x1

    #@23a
    goto :goto_20f

    #@23b
    .line 2169
    .end local v16           #cursorSequence:I
    :cond_23b
    if-eqz p9, :cond_261

    #@23d
    .line 2170
    aput-object p1, v25, p4

    #@23f
    .line 2176
    :goto_23f
    move-object/from16 v0, p0

    #@241
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@243
    const-string v4, "cdma_kr_testbed_mms_receive"

    #@245
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@248
    move-result v3

    #@249
    if-eqz v3, :cond_26d

    #@24b
    if-eqz p9, :cond_26d

    #@24d
    .line 2177
    move-object/from16 v0, p0

    #@24f
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@251
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@253
    const/4 v5, 0x0

    #@254
    invoke-virtual {v3, v4, v6, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_257
    .catchall {:try_start_1d5 .. :try_end_257} :catchall_266
    .catch Landroid/database/SQLException; {:try_start_1d5 .. :try_end_257} :catch_1c6

    #@257
    .line 2189
    .end local v15           #cursorCount:I
    .end local v20           #i:I
    :goto_257
    if-eqz v14, :cond_25c

    #@259
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@25c
    .line 2193
    :cond_25c
    if-nez v25, :cond_281

    #@25e
    .line 2194
    const/4 v3, -0x1

    #@25f
    goto/16 :goto_a7

    #@261
    .line 2172
    .restart local v15       #cursorCount:I
    .restart local v20       #i:I
    :cond_261
    add-int/lit8 v3, p4, -0x1

    #@263
    :try_start_263
    aput-object p1, v25, v3
    :try_end_265
    .catchall {:try_start_263 .. :try_end_265} :catchall_266
    .catch Landroid/database/SQLException; {:try_start_263 .. :try_end_265} :catch_1c6

    #@265
    goto :goto_23f

    #@266
    .line 2189
    .end local v6           #where:Ljava/lang/String;
    .end local v15           #cursorCount:I
    .end local v20           #i:I
    .end local v26           #refNumber:Ljava/lang/String;
    .end local v27           #seqNumber:Ljava/lang/String;
    .end local v30           #whereArgs:[Ljava/lang/String;
    :catchall_266
    move-exception v3

    #@267
    if-eqz v14, :cond_26c

    #@269
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@26c
    :cond_26c
    throw v3

    #@26d
    .line 2179
    .restart local v6       #where:Ljava/lang/String;
    .restart local v15       #cursorCount:I
    .restart local v20       #i:I
    .restart local v26       #refNumber:Ljava/lang/String;
    .restart local v27       #seqNumber:Ljava/lang/String;
    .restart local v30       #whereArgs:[Ljava/lang/String;
    :cond_26d
    :try_start_26d
    move-object/from16 v0, p0

    #@26f
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@271
    sget-object v4, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@273
    move-object/from16 v0, v30

    #@275
    invoke-virtual {v3, v4, v6, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@278
    goto :goto_257

    #@279
    .line 2182
    .end local v15           #cursorCount:I
    .end local v20           #i:I
    :cond_279
    const-string v3, "SMS"

    #@27b
    const-string v4, "SMSDispatcher.processMessagePartKRTestBed(): mResolver.query() returned null"

    #@27d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_280
    .catchall {:try_start_26d .. :try_end_280} :catchall_266
    .catch Landroid/database/SQLException; {:try_start_26d .. :try_end_280} :catch_1c6

    #@280
    goto :goto_257

    #@281
    .line 2198
    :cond_281
    if-eqz p9, :cond_2c8

    #@283
    .line 2200
    new-instance v24, Ljava/io/ByteArrayOutputStream;

    #@285
    invoke-direct/range {v24 .. v24}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@288
    .line 2201
    .local v24, output:Ljava/io/ByteArrayOutputStream;
    const/16 v20, 0x0

    #@28a
    .restart local v20       #i:I
    :goto_28a
    move/from16 v0, v20

    #@28c
    move/from16 v1, p5

    #@28e
    if-ge v0, v1, :cond_29e

    #@290
    .line 2203
    aget-object v3, v25, v20

    #@292
    const/4 v4, 0x0

    #@293
    aget-object v5, v25, v20

    #@295
    array-length v5, v5

    #@296
    move-object/from16 v0, v24

    #@298
    invoke-virtual {v0, v3, v4, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@29b
    .line 2201
    add-int/lit8 v20, v20, 0x1

    #@29d
    goto :goto_28a

    #@29e
    .line 2205
    :cond_29e
    invoke-virtual/range {v24 .. v24}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@2a1
    move-result-object v18

    #@2a2
    .line 2208
    .local v18, datagram:[B
    const/16 v3, 0xb84

    #@2a4
    move/from16 v0, p8

    #@2a6
    if-ne v0, v3, :cond_2b4

    #@2a8
    .line 2210
    move-object/from16 v0, p0

    #@2aa
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@2ac
    move-object/from16 v0, v18

    #@2ae
    invoke-virtual {v3, v0}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([B)I

    #@2b1
    move-result v3

    #@2b2
    goto/16 :goto_a7

    #@2b4
    .line 2212
    :cond_2b4
    const/4 v3, 0x1

    #@2b5
    new-array v0, v3, [[B

    #@2b7
    move-object/from16 v25, v0

    #@2b9
    .line 2213
    const/4 v3, 0x0

    #@2ba
    aput-object v18, v25, v3

    #@2bc
    .line 2215
    move-object/from16 v0, p0

    #@2be
    move-object/from16 v1, v25

    #@2c0
    move/from16 v2, p8

    #@2c2
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPortAddressedPdus([[BI)V

    #@2c5
    .line 2216
    const/4 v3, -0x1

    #@2c6
    goto/16 :goto_a7

    #@2c8
    .line 2221
    .end local v18           #datagram:[B
    .end local v20           #i:I
    .end local v24           #output:Ljava/io/ByteArrayOutputStream;
    :cond_2c8
    const/4 v3, -0x1

    #@2c9
    move/from16 v0, p8

    #@2cb
    if-eq v0, v3, :cond_32e

    #@2cd
    .line 2222
    const/16 v3, 0xb84

    #@2cf
    move/from16 v0, p8

    #@2d1
    if-ne v0, v3, :cond_322

    #@2d3
    .line 2224
    new-instance v24, Ljava/io/ByteArrayOutputStream;

    #@2d5
    invoke-direct/range {v24 .. v24}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@2d8
    .line 2225
    .restart local v24       #output:Ljava/io/ByteArrayOutputStream;
    const/16 v20, 0x0

    #@2da
    .restart local v20       #i:I
    :goto_2da
    move/from16 v0, v20

    #@2dc
    move/from16 v1, p5

    #@2de
    if-ge v0, v1, :cond_2fe

    #@2e0
    .line 2226
    aget-object v3, v25, v20

    #@2e2
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@2e5
    move-result-object v4

    #@2e6
    invoke-static {v3, v4}, Landroid/telephony/SmsMessage;->createFromPdu([BLjava/lang/String;)Landroid/telephony/SmsMessage;

    #@2e9
    move-result-object v21

    #@2ea
    .line 2228
    .local v21, msg:Landroid/telephony/SmsMessage;
    if-eqz v21, :cond_2fb

    #@2ec
    .line 2229
    invoke-virtual/range {v21 .. v21}, Landroid/telephony/SmsMessage;->getUserData()[B

    #@2ef
    move-result-object v17

    #@2f0
    .line 2230
    .local v17, data:[B
    const/4 v3, 0x0

    #@2f1
    move-object/from16 v0, v17

    #@2f3
    array-length v4, v0

    #@2f4
    move-object/from16 v0, v24

    #@2f6
    move-object/from16 v1, v17

    #@2f8
    invoke-virtual {v0, v1, v3, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@2fb
    .line 2225
    .end local v17           #data:[B
    :cond_2fb
    add-int/lit8 v20, v20, 0x1

    #@2fd
    goto :goto_2da

    #@2fe
    .line 2236
    .end local v21           #msg:Landroid/telephony/SmsMessage;
    :cond_2fe
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_WAPSERVICE:Z

    #@300
    if-eqz v3, :cond_314

    #@302
    .line 2237
    move-object/from16 v0, p0

    #@304
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@306
    invoke-virtual/range {v24 .. v24}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@309
    move-result-object v4

    #@30a
    move-object/from16 v0, p10

    #@30c
    move-object/from16 v1, p2

    #@30e
    invoke-virtual {v3, v4, v0, v1}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([BLjava/lang/String;Ljava/lang/String;)I

    #@311
    move-result v3

    #@312
    goto/16 :goto_a7

    #@314
    .line 2242
    :cond_314
    move-object/from16 v0, p0

    #@316
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@318
    invoke-virtual/range {v24 .. v24}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@31b
    move-result-object v4

    #@31c
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([B)I

    #@31f
    move-result v3

    #@320
    goto/16 :goto_a7

    #@322
    .line 2245
    .end local v20           #i:I
    .end local v24           #output:Ljava/io/ByteArrayOutputStream;
    :cond_322
    move-object/from16 v0, p0

    #@324
    move-object/from16 v1, v25

    #@326
    move/from16 v2, p8

    #@328
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPortAddressedPdus([[BI)V

    #@32b
    .line 2252
    :goto_32b
    const/4 v3, -0x1

    #@32c
    goto/16 :goto_a7

    #@32e
    .line 2250
    :cond_32e
    move-object/from16 v0, p0

    #@330
    move-object/from16 v1, v25

    #@332
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatchPdus([[B)V

    #@335
    goto :goto_32b
.end method

.method protected processMessageSegment(Lcom/android/internal/telephony/SmsMessageBase;)I
    .registers 6
    .parameter "sms"

    #@0
    .prologue
    .line 4752
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->isSegmentedPDU(Lcom/android/internal/telephony/SmsMessageBase;)Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_8

    #@6
    .line 4753
    const/4 v2, 0x2

    #@7
    .line 4765
    :goto_7
    return v2

    #@8
    .line 4755
    :cond_8
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->isLastSegmentedPDU(Lcom/android/internal/telephony/SmsMessageBase;)Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_13

    #@e
    .line 4756
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->completeProcessMessageSegment(Lcom/android/internal/telephony/SmsMessageBase;)I

    #@11
    move-result v2

    #@12
    goto :goto_7

    #@13
    .line 4761
    :cond_13
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->insertMessageSegment(Lcom/android/internal/telephony/SmsMessageBase;)J

    #@16
    move-result-wide v0

    #@17
    .line 4762
    .local v0, firstTime:J
    new-instance v2, Ljava/lang/Thread;

    #@19
    new-instance v3, Lcom/android/internal/telephony/SMSDispatcher$SegmentExpirationRunnable;

    #@1b
    invoke-direct {v3, p0, p1, v0, v1}, Lcom/android/internal/telephony/SMSDispatcher$SegmentExpirationRunnable;-><init>(Lcom/android/internal/telephony/SMSDispatcher;Lcom/android/internal/telephony/SmsMessageBase;J)V

    #@1e
    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@21
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    #@24
    .line 4765
    const/4 v2, 0x1

    #@25
    goto :goto_7
.end method

.method protected abstract processOperatorMessage([[BLcom/android/internal/telephony/SmsMessageBase;ZZ)I
.end method

.method protected abstract sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V
.end method

.method protected sendEmailoverMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 22
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2798
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-static {}, Lcom/android/internal/telephony/SMSDispatcher;->getNextConcatenatedRef()I

    #@3
    move-result v1

    #@4
    and-int/lit16 v15, v1, 0xff

    #@6
    .line 2799
    .local v15, refNumber:I
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v14

    #@a
    .line 2800
    .local v14, msgCount:I
    const/4 v6, 0x0

    #@b
    .line 2802
    .local v6, encoding:I
    move-object/from16 v0, p0

    #@d
    iput v14, v0, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@f
    .line 2804
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "sendEmailoverMultipartText(), msgCount = "

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@25
    .line 2805
    new-array v12, v14, [Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@27
    .line 2806
    .local v12, encodingForParts:[Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    const/4 v13, 0x0

    #@28
    .local v13, i:I
    :goto_28
    if-ge v13, v14, :cond_6d

    #@2a
    .line 2807
    move-object/from16 v0, p3

    #@2c
    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2f
    move-result-object v1

    #@30
    check-cast v1, Ljava/lang/CharSequence;

    #@32
    const/4 v2, 0x0

    #@33
    move-object/from16 v0, p0

    #@35
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/SMSDispatcher;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@38
    move-result-object v11

    #@39
    .line 2808
    .local v11, details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v2, "sendEmailoverMultipartText(), i = "

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    const-string v2, "details = "

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v11}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v1

    #@5a
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5d
    .line 2809
    iget v1, v11, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@5f
    if-eq v6, v1, :cond_68

    #@61
    if-eqz v6, :cond_66

    #@63
    const/4 v1, 0x1

    #@64
    if-ne v6, v1, :cond_68

    #@66
    .line 2812
    :cond_66
    iget v6, v11, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@68
    .line 2814
    :cond_68
    aput-object v11, v12, v13

    #@6a
    .line 2806
    add-int/lit8 v13, v13, 0x1

    #@6c
    goto :goto_28

    #@6d
    .line 2817
    .end local v11           #details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_6d
    const/4 v13, 0x0

    #@6e
    :goto_6e
    if-ge v13, v14, :cond_d3

    #@70
    .line 2818
    new-instance v10, Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@72
    invoke-direct {v10}, Lcom/android/internal/telephony/SmsHeader$ConcatRef;-><init>()V

    #@75
    .line 2819
    .local v10, concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    iput v15, v10, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@77
    .line 2820
    add-int/lit8 v1, v13, 0x1

    #@79
    iput v1, v10, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@7b
    .line 2821
    iput v14, v10, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@7d
    .line 2828
    const/4 v1, 0x1

    #@7e
    iput-boolean v1, v10, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->isEightBits:Z

    #@80
    .line 2829
    new-instance v5, Lcom/android/internal/telephony/SmsHeader;

    #@82
    invoke-direct {v5}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@85
    .line 2830
    .local v5, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    iput-object v10, v5, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@87
    .line 2833
    const/4 v1, 0x1

    #@88
    if-ne v6, v1, :cond_96

    #@8a
    .line 2834
    aget-object v1, v12, v13

    #@8c
    iget v1, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@8e
    iput v1, v5, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@90
    .line 2835
    aget-object v1, v12, v13

    #@92
    iget v1, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@94
    iput v1, v5, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@96
    .line 2838
    :cond_96
    const/4 v7, 0x0

    #@97
    .line 2839
    .local v7, sentIntent:Landroid/app/PendingIntent;
    if-eqz p4, :cond_a7

    #@99
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    #@9c
    move-result v1

    #@9d
    if-le v1, v13, :cond_a7

    #@9f
    .line 2840
    move-object/from16 v0, p4

    #@a1
    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a4
    move-result-object v7

    #@a5
    .end local v7           #sentIntent:Landroid/app/PendingIntent;
    check-cast v7, Landroid/app/PendingIntent;

    #@a7
    .line 2843
    .restart local v7       #sentIntent:Landroid/app/PendingIntent;
    :cond_a7
    const/4 v8, 0x0

    #@a8
    .line 2844
    .local v8, deliveryIntent:Landroid/app/PendingIntent;
    if-eqz p5, :cond_b8

    #@aa
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    #@ad
    move-result v1

    #@ae
    if-le v1, v13, :cond_b8

    #@b0
    .line 2845
    move-object/from16 v0, p5

    #@b2
    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b5
    move-result-object v8

    #@b6
    .end local v8           #deliveryIntent:Landroid/app/PendingIntent;
    check-cast v8, Landroid/app/PendingIntent;

    #@b8
    .line 2848
    .restart local v8       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_b8
    move-object/from16 v0, p3

    #@ba
    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@bd
    move-result-object v4

    #@be
    check-cast v4, Ljava/lang/String;

    #@c0
    add-int/lit8 v1, v14, -0x1

    #@c2
    if-ne v13, v1, :cond_d1

    #@c4
    const/4 v9, 0x1

    #@c5
    :goto_c5
    move-object/from16 v1, p0

    #@c7
    move-object/from16 v2, p1

    #@c9
    move-object/from16 v3, p2

    #@cb
    invoke-virtual/range {v1 .. v9}, Lcom/android/internal/telephony/SMSDispatcher;->sendNewSubmitPduforEmailoverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V

    #@ce
    .line 2817
    add-int/lit8 v13, v13, 0x1

    #@d0
    goto :goto_6e

    #@d1
    .line 2848
    :cond_d1
    const/4 v9, 0x0

    #@d2
    goto :goto_c5

    #@d3
    .line 2852
    .end local v5           #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    .end local v7           #sentIntent:Landroid/app/PendingIntent;
    .end local v8           #deliveryIntent:Landroid/app/PendingIntent;
    .end local v10           #concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    :cond_d3
    return-void
.end method

.method protected abstract sendEmailoverText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
.end method

.method protected sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 38
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2472
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-static {}, Lcom/android/internal/telephony/SMSDispatcher;->getNextConcatenatedRef()I

    #@3
    move-result v2

    #@4
    and-int/lit16 v0, v2, 0xff

    #@6
    move/from16 v26, v0

    #@8
    .line 2473
    .local v26, refNumber:I
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v22

    #@c
    .line 2474
    .local v22, msgCount:I
    const/4 v7, 0x0

    #@d
    .line 2476
    .local v7, encoding:I
    const-string v2, "persist.gsm.sms.forcegsm7"

    #@f
    const-string v3, "1"

    #@11
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v17

    #@15
    .line 2477
    .local v17, encodingType:Ljava/lang/String;
    const-string v2, "0"

    #@17
    move-object/from16 v0, v17

    #@19
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v20

    #@1d
    .line 2481
    .local v20, isConvertToGsmAlphabet:Z
    const/16 v18, 0x0

    #@1f
    .line 2482
    .local v18, hasEmailMultiSms:Z
    move/from16 v0, v22

    #@21
    new-array v0, v0, [Ljava/lang/String;

    #@23
    move-object/from16 v25, v0

    #@25
    .line 2485
    .local v25, reParts:[Ljava/lang/String;
    move/from16 v0, v22

    #@27
    move-object/from16 v1, p0

    #@29
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@2b
    .line 2487
    move/from16 v0, v22

    #@2d
    new-array v0, v0, [Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@2f
    move-object/from16 v16, v0

    #@31
    .line 2488
    .local v16, encodingForParts:[Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    const/16 v19, 0x0

    #@33
    .local v19, i:I
    :goto_33
    move/from16 v0, v19

    #@35
    move/from16 v1, v22

    #@37
    if-ge v0, v1, :cond_79

    #@39
    .line 2495
    move-object/from16 v0, p0

    #@3b
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@3d
    const-string v3, "KREncodingScheme"

    #@3f
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@42
    move-result v2

    #@43
    const/4 v3, 0x1

    #@44
    if-ne v2, v3, :cond_66

    #@46
    .line 2496
    move-object/from16 v0, p3

    #@48
    move/from16 v1, v19

    #@4a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4d
    move-result-object v2

    #@4e
    check-cast v2, Ljava/lang/CharSequence;

    #@50
    const/4 v3, 0x0

    #@51
    const/4 v4, 0x0

    #@52
    invoke-static {v2, v3, v4}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;ZZ)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@55
    move-result-object v14

    #@56
    .line 2506
    .local v14, details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :goto_56
    iget v2, v14, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@58
    if-eq v7, v2, :cond_61

    #@5a
    if-eqz v7, :cond_5f

    #@5c
    const/4 v2, 0x1

    #@5d
    if-ne v7, v2, :cond_61

    #@5f
    .line 2509
    :cond_5f
    iget v7, v14, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@61
    .line 2511
    :cond_61
    aput-object v14, v16, v19

    #@63
    .line 2488
    add-int/lit8 v19, v19, 0x1

    #@65
    goto :goto_33

    #@66
    .line 2499
    .end local v14           #details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_66
    move-object/from16 v0, p3

    #@68
    move/from16 v1, v19

    #@6a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6d
    move-result-object v2

    #@6e
    check-cast v2, Ljava/lang/CharSequence;

    #@70
    move-object/from16 v0, p0

    #@72
    move/from16 v1, v20

    #@74
    invoke-virtual {v0, v2, v1}, Lcom/android/internal/telephony/SMSDispatcher;->calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@77
    move-result-object v14

    #@78
    .restart local v14       #details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    goto :goto_56

    #@79
    .line 2515
    .end local v14           #details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_79
    move-object/from16 v0, p0

    #@7b
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@7d
    const-string v3, "sprint_segment_sms"

    #@7f
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@82
    move-result v2

    #@83
    if-eqz v2, :cond_120

    #@85
    .line 2518
    invoke-static/range {p1 .. p1}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@88
    move-result v2

    #@89
    if-eqz v2, :cond_120

    #@8b
    .line 2519
    move-object/from16 v21, p3

    #@8d
    .line 2521
    .local v21, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v29, 0x0

    #@8f
    .line 2523
    .local v29, segCount:I
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    #@92
    move-result v29

    #@93
    .line 2524
    new-instance v2, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v3, "sendMultipartText(), messages.size:"

    #@9a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v2

    #@9e
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@a1
    move-result-object v3

    #@a2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v2

    #@a6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v2

    #@aa
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@ad
    .line 2526
    const/16 v2, 0xa

    #@af
    move/from16 v0, v29

    #@b1
    if-le v0, v2, :cond_cf

    #@b3
    .line 2527
    const/16 v29, 0xa

    #@b5
    .line 2528
    new-instance v2, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v3, "sendMultipartText(), (segment>10) messages.size:"

    #@bc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v2

    #@c0
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@c3
    move-result-object v3

    #@c4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v2

    #@c8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v2

    #@cc
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@cf
    .line 2531
    :cond_cf
    const/4 v2, 0x1

    #@d0
    move/from16 v0, v29

    #@d2
    if-le v0, v2, :cond_120

    #@d4
    .line 2533
    const/16 v23, 0x1

    #@d6
    .line 2534
    .local v23, reCalculate:Z
    const/16 v30, 0x0

    #@d8
    .line 2535
    .local v30, segIdx:I
    new-instance v24, Ljava/lang/StringBuilder;

    #@da
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    .line 2536
    .local v24, reMsgText:Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    move-object/from16 v0, p1

    #@e4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v2

    #@e8
    const-string v3, " "

    #@ea
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v2

    #@ee
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v15

    #@f2
    .line 2537
    .local v15, emailAddrStr:Ljava/lang/String;
    const/16 v18, 0x1

    #@f4
    .line 2539
    const/16 v31, 0x0

    #@f6
    .local v31, segIdxLoop:I
    :goto_f6
    move/from16 v0, v31

    #@f8
    move/from16 v1, v29

    #@fa
    if-ge v0, v1, :cond_11c

    #@fc
    .line 2541
    new-instance v2, Ljava/lang/StringBuilder;

    #@fe
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@101
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v3

    #@105
    move-object/from16 v0, v21

    #@107
    move/from16 v1, v31

    #@109
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10c
    move-result-object v2

    #@10d
    check-cast v2, Ljava/lang/String;

    #@10f
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v2

    #@113
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v2

    #@117
    aput-object v2, v25, v31

    #@119
    .line 2539
    add-int/lit8 v31, v31, 0x1

    #@11b
    goto :goto_f6

    #@11c
    .line 2543
    :cond_11c
    move/from16 v22, v29

    #@11e
    .line 2545
    const-string p1, "6245"

    #@120
    .line 2551
    .end local v15           #emailAddrStr:Ljava/lang/String;
    .end local v21           #messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v23           #reCalculate:Z
    .end local v24           #reMsgText:Ljava/lang/StringBuilder;
    .end local v29           #segCount:I
    .end local v30           #segIdx:I
    .end local v31           #segIdxLoop:I
    :cond_120
    const/16 v19, 0x0

    #@122
    :goto_122
    move/from16 v0, v19

    #@124
    move/from16 v1, v22

    #@126
    if-ge v0, v1, :cond_286

    #@128
    .line 2552
    new-instance v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@12a
    invoke-direct {v12}, Lcom/android/internal/telephony/SmsHeader$ConcatRef;-><init>()V

    #@12d
    .line 2553
    .local v12, concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    move/from16 v0, v26

    #@12f
    iput v0, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@131
    .line 2554
    add-int/lit8 v2, v19, 0x1

    #@133
    iput v2, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@135
    .line 2555
    move/from16 v0, v22

    #@137
    iput v0, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@139
    .line 2564
    move-object/from16 v0, p0

    #@13b
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@13d
    const-string v3, "sprint_segment_sms"

    #@13f
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@142
    move-result v2

    #@143
    if-eqz v2, :cond_20e

    #@145
    .line 2565
    const/4 v2, 0x0

    #@146
    iput-boolean v2, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->isEightBits:Z

    #@148
    .line 2570
    :goto_148
    new-instance v6, Lcom/android/internal/telephony/SmsHeader;

    #@14a
    invoke-direct {v6}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@14d
    .line 2571
    .local v6, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    iput-object v12, v6, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@14f
    .line 2574
    const/4 v2, 0x1

    #@150
    if-ne v7, v2, :cond_15e

    #@152
    .line 2575
    aget-object v2, v16, v19

    #@154
    iget v2, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@156
    iput v2, v6, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@158
    .line 2576
    aget-object v2, v16, v19

    #@15a
    iget v2, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@15c
    iput v2, v6, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@15e
    .line 2579
    :cond_15e
    if-nez v19, :cond_1a8

    #@160
    .line 2580
    move-object/from16 v0, p0

    #@162
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@164
    const-string v3, "replyAddress"

    #@166
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@169
    move-result v2

    #@16a
    const/4 v3, 0x1

    #@16b
    if-ne v2, v3, :cond_1a8

    #@16d
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isReleaseOperator()Z

    #@170
    move-result v2

    #@171
    const/4 v3, 0x1

    #@172
    if-ne v2, v3, :cond_1a8

    #@174
    .line 2582
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/SMSDispatcher;->getLine1Number()Ljava/lang/String;

    #@177
    move-result-object v28

    #@178
    .line 2583
    .local v28, replyAddr:Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@17b
    move-result v2

    #@17c
    if-nez v2, :cond_1a8

    #@17e
    .line 2585
    invoke-static/range {v28 .. v28}, Landroid/telephony/PhoneNumberUtils;->KRsmsnetworkPortionToCalledPartyBCD(Ljava/lang/String;)[B

    #@181
    move-result-object v13

    #@182
    .line 2586
    .local v13, daBytes:[B
    new-instance v27, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@184
    invoke-direct/range {v27 .. v27}, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;-><init>()V

    #@187
    .line 2587
    .local v27, replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    if-eqz v13, :cond_1a4

    #@189
    .line 2589
    move-object/from16 v0, v27

    #@18b
    iput-object v13, v0, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->replyData:[B

    #@18d
    .line 2590
    array-length v2, v13

    #@18e
    add-int/lit8 v2, v2, -0x1

    #@190
    mul-int/lit8 v3, v2, 0x2

    #@192
    array-length v2, v13

    #@193
    add-int/lit8 v2, v2, -0x1

    #@195
    aget-byte v2, v13, v2

    #@197
    and-int/lit16 v2, v2, 0xf0

    #@199
    const/16 v4, 0xf0

    #@19b
    if-ne v2, v4, :cond_213

    #@19d
    const/4 v2, 0x1

    #@19e
    :goto_19e
    sub-int v2, v3, v2

    #@1a0
    move-object/from16 v0, v27

    #@1a2
    iput v2, v0, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->addressLength:I

    #@1a4
    .line 2595
    :cond_1a4
    move-object/from16 v0, v27

    #@1a6
    iput-object v0, v6, Lcom/android/internal/telephony/SmsHeader;->replyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@1a8
    .line 2601
    .end local v13           #daBytes:[B
    .end local v27           #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    .end local v28           #replyAddr:Ljava/lang/String;
    :cond_1a8
    const/4 v8, 0x0

    #@1a9
    .line 2602
    .local v8, sentIntent:Landroid/app/PendingIntent;
    if-eqz p4, :cond_1bd

    #@1ab
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    #@1ae
    move-result v2

    #@1af
    move/from16 v0, v19

    #@1b1
    if-le v2, v0, :cond_1bd

    #@1b3
    .line 2603
    move-object/from16 v0, p4

    #@1b5
    move/from16 v1, v19

    #@1b7
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1ba
    move-result-object v8

    #@1bb
    .end local v8           #sentIntent:Landroid/app/PendingIntent;
    check-cast v8, Landroid/app/PendingIntent;

    #@1bd
    .line 2606
    .restart local v8       #sentIntent:Landroid/app/PendingIntent;
    :cond_1bd
    const/4 v9, 0x0

    #@1be
    .line 2607
    .local v9, deliveryIntent:Landroid/app/PendingIntent;
    if-eqz p5, :cond_1d2

    #@1c0
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    #@1c3
    move-result v2

    #@1c4
    move/from16 v0, v19

    #@1c6
    if-le v2, v0, :cond_1d2

    #@1c8
    .line 2608
    move-object/from16 v0, p5

    #@1ca
    move/from16 v1, v19

    #@1cc
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1cf
    move-result-object v9

    #@1d0
    .end local v9           #deliveryIntent:Landroid/app/PendingIntent;
    check-cast v9, Landroid/app/PendingIntent;

    #@1d2
    .line 2611
    .restart local v9       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_1d2
    sget v2, Lcom/android/internal/telephony/SMSDispatcher;->vp:I

    #@1d4
    if-lez v2, :cond_1db

    #@1d6
    .line 2612
    sget v2, Lcom/android/internal/telephony/SMSDispatcher;->vp:I

    #@1d8
    invoke-static {v2}, Landroid/telephony/SmsMessage;->setValidityPeriod(I)V

    #@1db
    .line 2617
    :cond_1db
    move-object/from16 v0, p0

    #@1dd
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@1df
    const-string v3, "vzw_sms_retry_scheme"

    #@1e1
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1e4
    move-result v2

    #@1e5
    const/4 v3, 0x1

    #@1e6
    if-ne v2, v3, :cond_23a

    #@1e8
    .line 2618
    if-eqz v22, :cond_1ef

    #@1ea
    const/4 v2, 0x1

    #@1eb
    move/from16 v0, v22

    #@1ed
    if-ne v0, v2, :cond_217

    #@1ef
    .line 2619
    :cond_1ef
    move-object/from16 v0, p3

    #@1f1
    move/from16 v1, v19

    #@1f3
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f6
    move-result-object v5

    #@1f7
    check-cast v5, Ljava/lang/String;

    #@1f9
    add-int/lit8 v2, v22, -0x1

    #@1fb
    move/from16 v0, v19

    #@1fd
    if-ne v0, v2, :cond_215

    #@1ff
    const/4 v10, 0x1

    #@200
    :goto_200
    const/4 v11, 0x0

    #@201
    move-object/from16 v2, p0

    #@203
    move-object/from16 v3, p1

    #@205
    move-object/from16 v4, p2

    #@207
    invoke-virtual/range {v2 .. v11}, Lcom/android/internal/telephony/SMSDispatcher;->sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZZ)V

    #@20a
    .line 2551
    :cond_20a
    :goto_20a
    add-int/lit8 v19, v19, 0x1

    #@20c
    goto/16 :goto_122

    #@20e
    .line 2567
    .end local v6           #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    .end local v8           #sentIntent:Landroid/app/PendingIntent;
    .end local v9           #deliveryIntent:Landroid/app/PendingIntent;
    :cond_20e
    const/4 v2, 0x1

    #@20f
    iput-boolean v2, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->isEightBits:Z

    #@211
    goto/16 :goto_148

    #@213
    .line 2590
    .restart local v6       #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    .restart local v13       #daBytes:[B
    .restart local v27       #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    .restart local v28       #replyAddr:Ljava/lang/String;
    :cond_213
    const/4 v2, 0x0

    #@214
    goto :goto_19e

    #@215
    .line 2619
    .end local v13           #daBytes:[B
    .end local v27           #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    .end local v28           #replyAddr:Ljava/lang/String;
    .restart local v8       #sentIntent:Landroid/app/PendingIntent;
    .restart local v9       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_215
    const/4 v10, 0x0

    #@216
    goto :goto_200

    #@217
    .line 2621
    :cond_217
    const/4 v2, 0x1

    #@218
    move/from16 v0, v22

    #@21a
    if-le v0, v2, :cond_20a

    #@21c
    .line 2622
    move-object/from16 v0, p3

    #@21e
    move/from16 v1, v19

    #@220
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@223
    move-result-object v5

    #@224
    check-cast v5, Ljava/lang/String;

    #@226
    add-int/lit8 v2, v22, -0x1

    #@228
    move/from16 v0, v19

    #@22a
    if-ne v0, v2, :cond_238

    #@22c
    const/4 v10, 0x1

    #@22d
    :goto_22d
    const/4 v11, 0x1

    #@22e
    move-object/from16 v2, p0

    #@230
    move-object/from16 v3, p1

    #@232
    move-object/from16 v4, p2

    #@234
    invoke-virtual/range {v2 .. v11}, Lcom/android/internal/telephony/SMSDispatcher;->sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZZ)V

    #@237
    goto :goto_20a

    #@238
    :cond_238
    const/4 v10, 0x0

    #@239
    goto :goto_22d

    #@23a
    .line 2626
    :cond_23a
    move-object/from16 v0, p0

    #@23c
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@23e
    const-string v3, "sprint_segment_sms"

    #@240
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@243
    move-result v2

    #@244
    if-eqz v2, :cond_269

    #@246
    .line 2627
    if-eqz v18, :cond_25b

    #@248
    aget-object v5, v25, v19

    #@24a
    :goto_24a
    add-int/lit8 v2, v22, -0x1

    #@24c
    move/from16 v0, v19

    #@24e
    if-ne v0, v2, :cond_267

    #@250
    const/4 v10, 0x1

    #@251
    :goto_251
    move-object/from16 v2, p0

    #@253
    move-object/from16 v3, p1

    #@255
    move-object/from16 v4, p2

    #@257
    invoke-virtual/range {v2 .. v10}, Lcom/android/internal/telephony/SMSDispatcher;->sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V

    #@25a
    goto :goto_20a

    #@25b
    :cond_25b
    move-object/from16 v0, p3

    #@25d
    move/from16 v1, v19

    #@25f
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@262
    move-result-object v2

    #@263
    check-cast v2, Ljava/lang/String;

    #@265
    move-object v5, v2

    #@266
    goto :goto_24a

    #@267
    :cond_267
    const/4 v10, 0x0

    #@268
    goto :goto_251

    #@269
    .line 2631
    :cond_269
    move-object/from16 v0, p3

    #@26b
    move/from16 v1, v19

    #@26d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@270
    move-result-object v5

    #@271
    check-cast v5, Ljava/lang/String;

    #@273
    add-int/lit8 v2, v22, -0x1

    #@275
    move/from16 v0, v19

    #@277
    if-ne v0, v2, :cond_284

    #@279
    const/4 v10, 0x1

    #@27a
    :goto_27a
    move-object/from16 v2, p0

    #@27c
    move-object/from16 v3, p1

    #@27e
    move-object/from16 v4, p2

    #@280
    invoke-virtual/range {v2 .. v10}, Lcom/android/internal/telephony/SMSDispatcher;->sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V

    #@283
    goto :goto_20a

    #@284
    :cond_284
    const/4 v10, 0x0

    #@285
    goto :goto_27a

    #@286
    .line 2637
    .end local v6           #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    .end local v8           #sentIntent:Landroid/app/PendingIntent;
    .end local v9           #deliveryIntent:Landroid/app/PendingIntent;
    .end local v12           #concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    :cond_286
    return-void
.end method

.method protected sendMultipartTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V
    .registers 10
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2881
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    return-void
.end method

.method protected sendMultipartTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V
    .registers 10
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2887
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    return-void
.end method

.method protected abstract sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V
.end method

.method protected abstract sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;)V
.end method

.method protected abstract sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;Z)V
.end method

.method protected abstract sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZZ)V
.end method

.method protected abstract sendNewSubmitPduforEmailoverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V
.end method

.method protected sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 15
    .parameter "tracker"

    #@0
    .prologue
    const/4 v12, 0x4

    #@1
    const/4 v11, 0x1

    #@2
    .line 2972
    iget-object v3, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mData:Ljava/util/HashMap;

    #@4
    .line 2973
    .local v3, map:Ljava/util/HashMap;
    const-string v9, "pdu"

    #@6
    invoke-virtual {v3, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v9

    #@a
    check-cast v9, [B

    #@c
    move-object v5, v9

    #@d
    check-cast v5, [B

    #@f
    .line 2975
    .local v5, pdu:[B
    iget-object v7, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@11
    .line 2976
    .local v7, sentIntent:Landroid/app/PendingIntent;
    iget-boolean v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsSendDisabled:Z

    #@13
    if-eqz v9, :cond_23

    #@15
    .line 2977
    if-eqz v7, :cond_1b

    #@17
    .line 2979
    const/4 v9, 0x4

    #@18
    :try_start_18
    invoke-virtual {v7, v9}, Landroid/app/PendingIntent;->send(I)V
    :try_end_1b
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_18 .. :try_end_1b} :catch_c2

    #@1b
    .line 2982
    :cond_1b
    :goto_1b
    const-string v9, "SMS"

    #@1d
    const-string v10, "Device does not support sending sms."

    #@1f
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 3064
    :cond_22
    :goto_22
    return-void

    #@23
    .line 2986
    :cond_23
    if-nez v5, :cond_2e

    #@25
    .line 2987
    if-eqz v7, :cond_22

    #@27
    .line 2989
    const/4 v9, 0x3

    #@28
    :try_start_28
    invoke-virtual {v7, v9}, Landroid/app/PendingIntent;->send(I)V
    :try_end_2b
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_28 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_22

    #@2c
    .line 2990
    :catch_2c
    move-exception v9

    #@2d
    goto :goto_22

    #@2e
    .line 2996
    :cond_2e
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@30
    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@33
    move-result-object v6

    #@34
    .line 2997
    .local v6, pm:Landroid/content/pm/PackageManager;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@37
    move-result v9

    #@38
    invoke-virtual {v6, v9}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    .line 2999
    .local v4, packageNames:[Ljava/lang/String;
    if-eqz v4, :cond_41

    #@3e
    array-length v9, v4

    #@3f
    if-nez v9, :cond_58

    #@41
    .line 3001
    :cond_41
    const-string v9, "SMS"

    #@43
    const-string v10, "Can\'t get calling app package name: refusing to send SMS"

    #@45
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 3002
    if-eqz v7, :cond_22

    #@4a
    .line 3004
    const/4 v9, 0x1

    #@4b
    :try_start_4b
    invoke-virtual {v7, v9}, Landroid/app/PendingIntent;->send(I)V
    :try_end_4e
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_4b .. :try_end_4e} :catch_4f

    #@4e
    goto :goto_22

    #@4f
    .line 3005
    :catch_4f
    move-exception v2

    #@50
    .line 3006
    .local v2, ex:Landroid/app/PendingIntent$CanceledException;
    const-string v9, "SMS"

    #@52
    const-string v10, "failed to send error result"

    #@54
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    goto :goto_22

    #@58
    .line 3013
    .end local v2           #ex:Landroid/app/PendingIntent$CanceledException;
    :cond_58
    const/4 v0, 0x0

    #@59
    .line 3016
    .local v0, appInfo:Landroid/content/pm/PackageInfo;
    const/4 v9, 0x0

    #@5a
    :try_start_5a
    aget-object v9, v4, v9

    #@5c
    const/16 v10, 0x40

    #@5e
    invoke-virtual {v6, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_61
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5a .. :try_end_61} :catch_80

    #@61
    move-result-object v0

    #@62
    .line 3030
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/SMSDispatcher;->checkAvailableToSend(Landroid/app/PendingIntent;)Z

    #@65
    move-result v9

    #@66
    if-eqz v9, :cond_22

    #@68
    .line 3038
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->checkDestination(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)Z

    #@6b
    move-result v9

    #@6c
    if-eqz v9, :cond_22

    #@6e
    .line 3040
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@70
    iget-object v10, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@72
    invoke-virtual {v9, v10, v11}, Lcom/android/internal/telephony/SmsUsageMonitor;->check(Ljava/lang/String;I)Z

    #@75
    move-result v9

    #@76
    if-nez v9, :cond_98

    #@78
    .line 3041
    invoke-virtual {p0, v12, p1}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@7b
    move-result-object v9

    #@7c
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessage(Landroid/os/Message;)Z

    #@7f
    goto :goto_22

    #@80
    .line 3017
    :catch_80
    move-exception v1

    #@81
    .line 3018
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v9, "SMS"

    #@83
    const-string v10, "Can\'t get calling app package info: refusing to send SMS"

    #@85
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 3019
    if-eqz v7, :cond_22

    #@8a
    .line 3021
    const/4 v9, 0x1

    #@8b
    :try_start_8b
    invoke-virtual {v7, v9}, Landroid/app/PendingIntent;->send(I)V
    :try_end_8e
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_8b .. :try_end_8e} :catch_8f

    #@8e
    goto :goto_22

    #@8f
    .line 3022
    :catch_8f
    move-exception v2

    #@90
    .line 3023
    .restart local v2       #ex:Landroid/app/PendingIntent$CanceledException;
    const-string v9, "SMS"

    #@92
    const-string v10, "failed to send error result"

    #@94
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    goto :goto_22

    #@98
    .line 3045
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v2           #ex:Landroid/app/PendingIntent$CanceledException;
    :cond_98
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@9a
    invoke-virtual {v9}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@9d
    move-result-object v9

    #@9e
    invoke-virtual {v9}, Landroid/telephony/ServiceState;->getState()I

    #@a1
    move-result v8

    #@a2
    .line 3050
    .local v8, ss:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->checkNotInService()Z

    #@a5
    move-result v9

    #@a6
    if-ne v11, v9, :cond_af

    #@a8
    .line 3052
    iget-object v9, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@aa
    invoke-static {v8, v9}, Lcom/android/internal/telephony/SMSDispatcher;->handleNotInService(ILandroid/app/PendingIntent;)V

    #@ad
    goto/16 :goto_22

    #@af
    .line 3055
    :cond_af
    iget-boolean v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSyncronousSending:Z

    #@b1
    if-eqz v9, :cond_bd

    #@b3
    .line 3056
    new-instance v9, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;

    #@b5
    invoke-direct {v9, p1}, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;-><init>(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@b8
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/SMSDispatcher;->enqueueMessageForSending(Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;)V

    #@bb
    goto/16 :goto_22

    #@bd
    .line 3060
    :cond_bd
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@c0
    goto/16 :goto_22

    #@c2
    .line 2980
    .end local v0           #appInfo:Landroid/content/pm/PackageInfo;
    .end local v4           #packageNames:[Ljava/lang/String;
    .end local v6           #pm:Landroid/content/pm/PackageManager;
    .end local v8           #ss:I
    :catch_c2
    move-exception v9

    #@c3
    goto/16 :goto_1b
.end method

.method protected sendRawPduMore(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 16
    .parameter "tracker"

    #@0
    .prologue
    const/4 v13, 0x4

    #@1
    const/4 v12, 0x1

    #@2
    .line 3140
    iget-object v3, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mData:Ljava/util/HashMap;

    #@4
    .line 3141
    .local v3, map:Ljava/util/HashMap;
    const-string v10, "pdu"

    #@6
    invoke-virtual {v3, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v10

    #@a
    check-cast v10, [B

    #@c
    move-object v6, v10

    #@d
    check-cast v6, [B

    #@f
    .line 3142
    .local v6, pdu:[B
    iget-object v8, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@11
    .line 3144
    .local v8, sentIntent:Landroid/app/PendingIntent;
    iget-boolean v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsSendDisabled:Z

    #@13
    if-eqz v10, :cond_21

    #@15
    .line 3145
    if-eqz v8, :cond_1b

    #@17
    .line 3147
    const/4 v10, 0x4

    #@18
    :try_start_18
    invoke-virtual {v8, v10}, Landroid/app/PendingIntent;->send(I)V
    :try_end_1b
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_18 .. :try_end_1b} :catch_ca

    #@1b
    .line 3150
    :cond_1b
    :goto_1b
    const-string v10, "sendRawPduMore(), Device does not support sending sms."

    #@1d
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@20
    .line 3229
    :cond_20
    :goto_20
    return-void

    #@21
    .line 3154
    :cond_21
    if-nez v6, :cond_2c

    #@23
    .line 3155
    if-eqz v8, :cond_20

    #@25
    .line 3157
    const/4 v10, 0x3

    #@26
    :try_start_26
    invoke-virtual {v8, v10}, Landroid/app/PendingIntent;->send(I)V
    :try_end_29
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_26 .. :try_end_29} :catch_2a

    #@29
    goto :goto_20

    #@2a
    .line 3158
    :catch_2a
    move-exception v10

    #@2b
    goto :goto_20

    #@2c
    .line 3164
    :cond_2c
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@2e
    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@31
    move-result-object v7

    #@32
    .line 3165
    .local v7, pm:Landroid/content/pm/PackageManager;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@35
    move-result v10

    #@36
    invoke-virtual {v7, v10}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@39
    move-result-object v5

    #@3a
    .line 3167
    .local v5, packageNames:[Ljava/lang/String;
    if-eqz v5, :cond_3f

    #@3c
    array-length v10, v5

    #@3d
    if-nez v10, :cond_52

    #@3f
    .line 3169
    :cond_3f
    const-string v10, "sendRawPduMore(), Can\'t get calling app package name: refusing to send SMS"

    #@41
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@44
    .line 3170
    if-eqz v8, :cond_20

    #@46
    .line 3172
    const/4 v10, 0x1

    #@47
    :try_start_47
    invoke-virtual {v8, v10}, Landroid/app/PendingIntent;->send(I)V
    :try_end_4a
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_47 .. :try_end_4a} :catch_4b

    #@4a
    goto :goto_20

    #@4b
    .line 3173
    :catch_4b
    move-exception v2

    #@4c
    .line 3174
    .local v2, ex:Landroid/app/PendingIntent$CanceledException;
    const-string v10, "sendRawPduMore(), failed to send error result"

    #@4e
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@51
    goto :goto_20

    #@52
    .line 3181
    .end local v2           #ex:Landroid/app/PendingIntent$CanceledException;
    :cond_52
    const/4 v0, 0x0

    #@53
    .line 3184
    .local v0, appInfo:Landroid/content/pm/PackageInfo;
    const/4 v10, 0x0

    #@54
    :try_start_54
    aget-object v10, v5, v10

    #@56
    const/16 v11, 0x40

    #@58
    invoke-virtual {v7, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_5b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_54 .. :try_end_5b} :catch_74

    #@5b
    move-result-object v0

    #@5c
    .line 3200
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->checkDestination(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)Z

    #@5f
    move-result v10

    #@60
    if-eqz v10, :cond_20

    #@62
    .line 3202
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@64
    iget-object v11, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@66
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/telephony/SmsUsageMonitor;->check(Ljava/lang/String;I)Z

    #@69
    move-result v10

    #@6a
    if-nez v10, :cond_88

    #@6c
    .line 3203
    invoke-virtual {p0, v13, p1}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@6f
    move-result-object v10

    #@70
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessage(Landroid/os/Message;)Z

    #@73
    goto :goto_20

    #@74
    .line 3185
    :catch_74
    move-exception v1

    #@75
    .line 3186
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v10, "sendRawPduMore(), Can\'t get calling app package info: refusing to send SMS"

    #@77
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@7a
    .line 3187
    if-eqz v8, :cond_20

    #@7c
    .line 3189
    const/4 v10, 0x1

    #@7d
    :try_start_7d
    invoke-virtual {v8, v10}, Landroid/app/PendingIntent;->send(I)V
    :try_end_80
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_7d .. :try_end_80} :catch_81

    #@80
    goto :goto_20

    #@81
    .line 3190
    :catch_81
    move-exception v2

    #@82
    .line 3191
    .restart local v2       #ex:Landroid/app/PendingIntent$CanceledException;
    const-string v10, "sendRawPduMore(), failed to send error result"

    #@84
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@87
    goto :goto_20

    #@88
    .line 3207
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v2           #ex:Landroid/app/PendingIntent$CanceledException;
    :cond_88
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8a
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@8d
    move-result-object v10

    #@8e
    invoke-virtual {v10}, Landroid/telephony/ServiceState;->getState()I

    #@91
    move-result v9

    #@92
    .line 3212
    .local v9, ss:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher;->checkNotInService()Z

    #@95
    move-result v10

    #@96
    if-ne v12, v10, :cond_9e

    #@98
    .line 3214
    iget-object v10, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@9a
    invoke-static {v9, v10}, Lcom/android/internal/telephony/SMSDispatcher;->handleNotInService(ILandroid/app/PendingIntent;)V

    #@9d
    goto :goto_20

    #@9e
    .line 3216
    :cond_9e
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@a0
    const-string v11, "sendMoreDelay"

    #@a2
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a5
    move-result v10

    #@a6
    if-eqz v10, :cond_b5

    #@a8
    .line 3217
    const/16 v10, 0x16

    #@aa
    invoke-virtual {p0, v10, p1}, Lcom/android/internal/telephony/SMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@ad
    move-result-object v4

    #@ae
    .line 3218
    .local v4, moreMsg:Landroid/os/Message;
    const-wide/16 v10, 0x12c

    #@b0
    invoke-virtual {p0, v4, v10, v11}, Lcom/android/internal/telephony/SMSDispatcher;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@b3
    goto/16 :goto_20

    #@b5
    .line 3221
    .end local v4           #moreMsg:Landroid/os/Message;
    :cond_b5
    iget-boolean v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSyncronousSending:Z

    #@b7
    if-eqz v10, :cond_c5

    #@b9
    .line 3222
    new-instance v10, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;

    #@bb
    sget-object v11, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;->MORE:Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;

    #@bd
    invoke-direct {v10, p1, v11}, Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;-><init>(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;Lcom/android/internal/telephony/SMSDispatcher$PendingMessage$SEND_TYPE;)V

    #@c0
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/SMSDispatcher;->enqueueMessageForSending(Lcom/android/internal/telephony/SMSDispatcher$PendingMessage;)V

    #@c3
    goto/16 :goto_20

    #@c5
    .line 3225
    :cond_c5
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->sendSmsMore(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@c8
    goto/16 :goto_20

    #@ca
    .line 3148
    .end local v0           #appInfo:Landroid/content/pm/PackageInfo;
    .end local v5           #packageNames:[Ljava/lang/String;
    .end local v7           #pm:Landroid/content/pm/PackageManager;
    .end local v9           #ss:I
    :catch_ca
    move-exception v10

    #@cb
    goto/16 :goto_1b
.end method

.method public abstract sendRetrySms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
.end method

.method protected abstract sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
.end method

.method protected sendSmsMore(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 2
    .parameter "tracker"

    #@0
    .prologue
    .line 3431
    return-void
.end method

.method protected abstract sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
.end method

.method protected sendTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V
    .registers 10
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"

    #@0
    .prologue
    .line 2877
    return-void
.end method

.method protected sendTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V
    .registers 10
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"

    #@0
    .prologue
    .line 2890
    return-void
.end method

.method protected setMultipartTextValidityPeriod(I)V
    .registers 2
    .parameter "validityperiod"

    #@0
    .prologue
    .line 2945
    sput p1, Lcom/android/internal/telephony/SMSDispatcher;->vp:I

    #@2
    .line 2946
    return-void
.end method

.method public setPremiumSmsPermission(Ljava/lang/String;I)V
    .registers 4
    .parameter "packageName"
    .parameter "permission"

    #@0
    .prologue
    .line 3415
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/telephony/SmsUsageMonitor;->setPremiumSmsPermission(Ljava/lang/String;I)V

    #@5
    .line 3416
    return-void
.end method

.method public setSmsIsRoaming(Z)V
    .registers 4
    .parameter "isRoaming"

    #@0
    .prologue
    .line 4776
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "setSmsIsRoaming(), isRoaming = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 4778
    sput-boolean p1, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitIsRoaming:Z

    #@18
    .line 4779
    return-void
.end method

.method public setSmsPriority(I)V
    .registers 2
    .parameter "priority"

    #@0
    .prologue
    .line 3782
    sput p1, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitPriority:I

    #@2
    .line 3783
    return-void
.end method

.method protected startTestCase(Ljava/lang/String;I)V
    .registers 5
    .parameter "pdu"
    .parameter "num"

    #@0
    .prologue
    .line 4944
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "startTestCase(), [KDDI] startTestCase pdu: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "  num: "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@20
    .line 4945
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@22
    check-cast v0, Lcom/android/internal/telephony/RIL;

    #@24
    const/4 v1, 0x0

    #@25
    invoke-virtual {v0, p1, p2, v1}, Lcom/android/internal/telephony/RIL;->startTestCase(Ljava/lang/String;ILandroid/os/Message;)V

    #@28
    .line 4946
    return-void
.end method

.method protected storeVoiceMailCount()V
    .registers 6

    #@0
    .prologue
    .line 1497
    iget-object v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getSubscriberId()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 1498
    .local v1, imsi:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getVoiceMessageCount()I

    #@b
    move-result v2

    #@c
    .line 1504
    .local v2, mwi:I
    iget-object v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@e
    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@11
    move-result-object v3

    #@12
    .line 1505
    .local v3, sp:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@15
    move-result-object v0

    #@16
    .line 1506
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    iget-object v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@18
    const-string v4, "vm_count_key"

    #@1a
    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    #@1d
    .line 1507
    iget-object v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1f
    const-string v4, "vm_id_key"

    #@21
    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@24
    .line 1508
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@27
    .line 1510
    return-void
.end method

.method protected updatePhoneObject(Lcom/android/internal/telephony/PhoneBase;)V
    .registers 4
    .parameter "phone"

    #@0
    .prologue
    .line 554
    iput-object p1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    .line 555
    iget-object v0, p1, Lcom/android/internal/telephony/PhoneBase;->mSmsStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@4
    iput-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@6
    .line 556
    iget-object v0, p1, Lcom/android/internal/telephony/PhoneBase;->mSmsUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@a
    .line 557
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "updatePhoneObject(), Active phone changed to "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@17
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getPhoneName()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@26
    .line 558
    return-void
.end method
