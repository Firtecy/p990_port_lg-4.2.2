.class public Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;
.super Ljava/lang/Thread;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGDataRecovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThreadPingToDnsServer"
.end annotation


# static fields
.field private static final DNS_DEFAULT_SERVER1:Ljava/lang/String; = "8.8.8.8"

.field private static final DNS_DEFAULT_SERVER2:Ljava/lang/String; = "8.8.4.4"


# instance fields
.field private mIface:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/internal/telephony/LGDataRecovery;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter "iface"

    #@0
    .prologue
    .line 2019
    iput-object p1, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 2016
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;->mIface:Ljava/lang/String;

    #@8
    .line 2020
    iput-object p2, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;->mIface:Ljava/lang/String;

    #@a
    .line 2021
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    .line 2026
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v4, "ThreadPingToDnsServer for iface : "

    #@7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v3

    #@b
    iget-object v4, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;->mIface:Ljava/lang/String;

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@18
    .line 2027
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;->mIface:Ljava/lang/String;

    #@1a
    if-nez v3, :cond_1d

    #@1c
    .line 2046
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 2030
    :cond_1d
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@1f
    iget-object v4, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;->mIface:Ljava/lang/String;

    #@21
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->access$800(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;)Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_1c

    #@27
    .line 2032
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v4, "ping -c 1 -I "

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    iget-object v4, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;->mIface:Ljava/lang/String;

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    const-string v4, " "

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    .line 2036
    .local v2, pingCmd:Ljava/lang/String;
    :try_start_42
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@44
    new-instance v4, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    const-string v5, "8.8.8.8"

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->access$600(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;)Ljava/util/ArrayList;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5e
    move-result-object v0

    #@5f
    .local v0, i$:Ljava/util/Iterator;
    :goto_5f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@62
    move-result v3

    #@63
    if-eqz v3, :cond_71

    #@65
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@68
    move-result-object v1

    #@69
    check-cast v1, Ljava/lang/String;

    #@6b
    .line 2037
    .local v1, line:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@6e
    goto :goto_5f

    #@6f
    .line 2044
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #line:Ljava/lang/String;
    :catch_6f
    move-exception v3

    #@70
    goto :goto_1c

    #@71
    .line 2040
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_71
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@73
    new-instance v4, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v4

    #@7c
    const-string v5, "8.8.4.4"

    #@7e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v4

    #@86
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->access$600(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;)Ljava/util/ArrayList;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@8d
    move-result-object v0

    #@8e
    :goto_8e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@91
    move-result v3

    #@92
    if-eqz v3, :cond_1c

    #@94
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@97
    move-result-object v1

    #@98
    check-cast v1, Ljava/lang/String;

    #@9a
    .line 2041
    .restart local v1       #line:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V
    :try_end_9d
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_9d} :catch_6f

    #@9d
    goto :goto_8e
.end method
