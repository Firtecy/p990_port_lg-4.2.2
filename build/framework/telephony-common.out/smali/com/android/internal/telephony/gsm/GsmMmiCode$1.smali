.class synthetic Lcom/android/internal/telephony/gsm/GsmMmiCode$1;
.super Ljava/lang/Object;
.source "GsmMmiCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gsm/GsmMmiCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$android$internal$telephony$CommandException$Error:[I

.field static final synthetic $SwitchMap$com$android$internal$telephony$gsm$SsData$RequestType:[I

.field static final synthetic $SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 1393
    invoke-static {}, Lcom/android/internal/telephony/CommandException$Error;->values()[Lcom/android/internal/telephony/CommandException$Error;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    new-array v0, v0, [I

    #@7
    sput-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@9
    :try_start_9
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@b
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->OP_NOT_ALLOWED_BEFORE_REG_NW:Lcom/android/internal/telephony/CommandException$Error;

    #@d
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x1

    #@12
    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_1dd

    #@14
    :goto_14
    :try_start_14
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@16
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->REQUEST_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@18
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@1b
    move-result v1

    #@1c
    const/4 v2, 0x2

    #@1d
    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_1da

    #@1f
    :goto_1f
    :try_start_1f
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@21
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@23
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x3

    #@28
    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_1d7

    #@2a
    :goto_2a
    :try_start_2a
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@2c
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    #@2e
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@31
    move-result v1

    #@32
    const/4 v2, 0x4

    #@33
    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_35} :catch_1d4

    #@35
    :goto_35
    :try_start_35
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@37
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->GENERIC_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@39
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@3c
    move-result v1

    #@3d
    const/4 v2, 0x5

    #@3e
    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_40} :catch_1d1

    #@40
    :goto_40
    :try_start_40
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@42
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->INVALID_RESPONSE:Lcom/android/internal/telephony/CommandException$Error;

    #@44
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@47
    move-result v1

    #@48
    const/4 v2, 0x6

    #@49
    aput v2, v0, v1
    :try_end_4b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_4b} :catch_1ce

    #@4b
    :goto_4b
    :try_start_4b
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@4d
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->OP_NOT_ALLOWED_DURING_VOICE_CALL:Lcom/android/internal/telephony/CommandException$Error;

    #@4f
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@52
    move-result v1

    #@53
    const/4 v2, 0x7

    #@54
    aput v2, v0, v1
    :try_end_56
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4b .. :try_end_56} :catch_1cb

    #@56
    :goto_56
    :try_start_56
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@58
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SIM_PIN2:Lcom/android/internal/telephony/CommandException$Error;

    #@5a
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@5d
    move-result v1

    #@5e
    const/16 v2, 0x8

    #@60
    aput v2, v0, v1
    :try_end_62
    .catch Ljava/lang/NoSuchFieldError; {:try_start_56 .. :try_end_62} :catch_1c8

    #@62
    :goto_62
    :try_start_62
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@64
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SIM_PUK2:Lcom/android/internal/telephony/CommandException$Error;

    #@66
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@69
    move-result v1

    #@6a
    const/16 v2, 0x9

    #@6c
    aput v2, v0, v1
    :try_end_6e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_62 .. :try_end_6e} :catch_1c5

    #@6e
    :goto_6e
    :try_start_6e
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@70
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SMS_FAIL_RETRY:Lcom/android/internal/telephony/CommandException$Error;

    #@72
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@75
    move-result v1

    #@76
    const/16 v2, 0xa

    #@78
    aput v2, v0, v1
    :try_end_7a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6e .. :try_end_7a} :catch_1c2

    #@7a
    .line 478
    :goto_7a
    invoke-static {}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->values()[Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@7d
    move-result-object v0

    #@7e
    array-length v0, v0

    #@7f
    new-array v0, v0, [I

    #@81
    sput-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@83
    :try_start_83
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@85
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CFU:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@87
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@8a
    move-result v1

    #@8b
    const/4 v2, 0x1

    #@8c
    aput v2, v0, v1
    :try_end_8e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_83 .. :try_end_8e} :catch_1bf

    #@8e
    :goto_8e
    :try_start_8e
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@90
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CF_BUSY:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@92
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@95
    move-result v1

    #@96
    const/4 v2, 0x2

    #@97
    aput v2, v0, v1
    :try_end_99
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8e .. :try_end_99} :catch_1bc

    #@99
    :goto_99
    :try_start_99
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@9b
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CF_NO_REPLY:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@9d
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@a0
    move-result v1

    #@a1
    const/4 v2, 0x3

    #@a2
    aput v2, v0, v1
    :try_end_a4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_99 .. :try_end_a4} :catch_1b9

    #@a4
    :goto_a4
    :try_start_a4
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@a6
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CF_NOT_REACHABLE:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@a8
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@ab
    move-result v1

    #@ac
    const/4 v2, 0x4

    #@ad
    aput v2, v0, v1
    :try_end_af
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a4 .. :try_end_af} :catch_1b6

    #@af
    :goto_af
    :try_start_af
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@b1
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CF_ALL:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@b3
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@b6
    move-result v1

    #@b7
    const/4 v2, 0x5

    #@b8
    aput v2, v0, v1
    :try_end_ba
    .catch Ljava/lang/NoSuchFieldError; {:try_start_af .. :try_end_ba} :catch_1b3

    #@ba
    :goto_ba
    :try_start_ba
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@bc
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CF_ALL_CONDITIONAL:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@be
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@c1
    move-result v1

    #@c2
    const/4 v2, 0x6

    #@c3
    aput v2, v0, v1
    :try_end_c5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ba .. :try_end_c5} :catch_1b0

    #@c5
    :goto_c5
    :try_start_c5
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@c7
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CLIP:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@c9
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@cc
    move-result v1

    #@cd
    const/4 v2, 0x7

    #@ce
    aput v2, v0, v1
    :try_end_d0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c5 .. :try_end_d0} :catch_1ad

    #@d0
    :goto_d0
    :try_start_d0
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@d2
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CLIR:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@d4
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@d7
    move-result v1

    #@d8
    const/16 v2, 0x8

    #@da
    aput v2, v0, v1
    :try_end_dc
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d0 .. :try_end_dc} :catch_1aa

    #@dc
    :goto_dc
    :try_start_dc
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@de
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_WAIT:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@e0
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@e3
    move-result v1

    #@e4
    const/16 v2, 0x9

    #@e6
    aput v2, v0, v1
    :try_end_e8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_dc .. :try_end_e8} :catch_1a7

    #@e8
    :goto_e8
    :try_start_e8
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@ea
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_BAOC:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@ec
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@ef
    move-result v1

    #@f0
    const/16 v2, 0xa

    #@f2
    aput v2, v0, v1
    :try_end_f4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e8 .. :try_end_f4} :catch_1a4

    #@f4
    :goto_f4
    :try_start_f4
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@f6
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_BAOIC:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@f8
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@fb
    move-result v1

    #@fc
    const/16 v2, 0xb

    #@fe
    aput v2, v0, v1
    :try_end_100
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f4 .. :try_end_100} :catch_1a1

    #@100
    :goto_100
    :try_start_100
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@102
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_BAOIC_EXC_HOME:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@104
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@107
    move-result v1

    #@108
    const/16 v2, 0xc

    #@10a
    aput v2, v0, v1
    :try_end_10c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_100 .. :try_end_10c} :catch_19e

    #@10c
    :goto_10c
    :try_start_10c
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@10e
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_BAIC:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@110
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@113
    move-result v1

    #@114
    const/16 v2, 0xd

    #@116
    aput v2, v0, v1
    :try_end_118
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10c .. :try_end_118} :catch_19b

    #@118
    :goto_118
    :try_start_118
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@11a
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_BAIC_ROAMING:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@11c
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@11f
    move-result v1

    #@120
    const/16 v2, 0xe

    #@122
    aput v2, v0, v1
    :try_end_124
    .catch Ljava/lang/NoSuchFieldError; {:try_start_118 .. :try_end_124} :catch_199

    #@124
    :goto_124
    :try_start_124
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@126
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_ALL_BARRING:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@128
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@12b
    move-result v1

    #@12c
    const/16 v2, 0xf

    #@12e
    aput v2, v0, v1
    :try_end_130
    .catch Ljava/lang/NoSuchFieldError; {:try_start_124 .. :try_end_130} :catch_197

    #@130
    :goto_130
    :try_start_130
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@132
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_OUTGOING_BARRING:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@134
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@137
    move-result v1

    #@138
    const/16 v2, 0x10

    #@13a
    aput v2, v0, v1
    :try_end_13c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_130 .. :try_end_13c} :catch_195

    #@13c
    :goto_13c
    :try_start_13c
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@13e
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_INCOMING_BARRING:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@140
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@143
    move-result v1

    #@144
    const/16 v2, 0x11

    #@146
    aput v2, v0, v1
    :try_end_148
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13c .. :try_end_148} :catch_193

    #@148
    .line 434
    :goto_148
    invoke-static {}, Lcom/android/internal/telephony/gsm/SsData$RequestType;->values()[Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@14b
    move-result-object v0

    #@14c
    array-length v0, v0

    #@14d
    new-array v0, v0, [I

    #@14f
    sput-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$RequestType:[I

    #@151
    :try_start_151
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$RequestType:[I

    #@153
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_ACTIVATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@155
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$RequestType;->ordinal()I

    #@158
    move-result v1

    #@159
    const/4 v2, 0x1

    #@15a
    aput v2, v0, v1
    :try_end_15c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_151 .. :try_end_15c} :catch_191

    #@15c
    :goto_15c
    :try_start_15c
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$RequestType:[I

    #@15e
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_DEACTIVATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@160
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$RequestType;->ordinal()I

    #@163
    move-result v1

    #@164
    const/4 v2, 0x2

    #@165
    aput v2, v0, v1
    :try_end_167
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15c .. :try_end_167} :catch_18f

    #@167
    :goto_167
    :try_start_167
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$RequestType:[I

    #@169
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_REGISTRATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@16b
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$RequestType;->ordinal()I

    #@16e
    move-result v1

    #@16f
    const/4 v2, 0x3

    #@170
    aput v2, v0, v1
    :try_end_172
    .catch Ljava/lang/NoSuchFieldError; {:try_start_167 .. :try_end_172} :catch_18d

    #@172
    :goto_172
    :try_start_172
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$RequestType:[I

    #@174
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_ERASURE:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@176
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$RequestType;->ordinal()I

    #@179
    move-result v1

    #@17a
    const/4 v2, 0x4

    #@17b
    aput v2, v0, v1
    :try_end_17d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_172 .. :try_end_17d} :catch_18b

    #@17d
    :goto_17d
    :try_start_17d
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$RequestType:[I

    #@17f
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_INTERROGATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@181
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$RequestType;->ordinal()I

    #@184
    move-result v1

    #@185
    const/4 v2, 0x5

    #@186
    aput v2, v0, v1
    :try_end_188
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17d .. :try_end_188} :catch_189

    #@188
    :goto_188
    return-void

    #@189
    :catch_189
    move-exception v0

    #@18a
    goto :goto_188

    #@18b
    :catch_18b
    move-exception v0

    #@18c
    goto :goto_17d

    #@18d
    :catch_18d
    move-exception v0

    #@18e
    goto :goto_172

    #@18f
    :catch_18f
    move-exception v0

    #@190
    goto :goto_167

    #@191
    :catch_191
    move-exception v0

    #@192
    goto :goto_15c

    #@193
    .line 478
    :catch_193
    move-exception v0

    #@194
    goto :goto_148

    #@195
    :catch_195
    move-exception v0

    #@196
    goto :goto_13c

    #@197
    :catch_197
    move-exception v0

    #@198
    goto :goto_130

    #@199
    :catch_199
    move-exception v0

    #@19a
    goto :goto_124

    #@19b
    :catch_19b
    move-exception v0

    #@19c
    goto/16 :goto_118

    #@19e
    :catch_19e
    move-exception v0

    #@19f
    goto/16 :goto_10c

    #@1a1
    :catch_1a1
    move-exception v0

    #@1a2
    goto/16 :goto_100

    #@1a4
    :catch_1a4
    move-exception v0

    #@1a5
    goto/16 :goto_f4

    #@1a7
    :catch_1a7
    move-exception v0

    #@1a8
    goto/16 :goto_e8

    #@1aa
    :catch_1aa
    move-exception v0

    #@1ab
    goto/16 :goto_dc

    #@1ad
    :catch_1ad
    move-exception v0

    #@1ae
    goto/16 :goto_d0

    #@1b0
    :catch_1b0
    move-exception v0

    #@1b1
    goto/16 :goto_c5

    #@1b3
    :catch_1b3
    move-exception v0

    #@1b4
    goto/16 :goto_ba

    #@1b6
    :catch_1b6
    move-exception v0

    #@1b7
    goto/16 :goto_af

    #@1b9
    :catch_1b9
    move-exception v0

    #@1ba
    goto/16 :goto_a4

    #@1bc
    :catch_1bc
    move-exception v0

    #@1bd
    goto/16 :goto_99

    #@1bf
    :catch_1bf
    move-exception v0

    #@1c0
    goto/16 :goto_8e

    #@1c2
    .line 1393
    :catch_1c2
    move-exception v0

    #@1c3
    goto/16 :goto_7a

    #@1c5
    :catch_1c5
    move-exception v0

    #@1c6
    goto/16 :goto_6e

    #@1c8
    :catch_1c8
    move-exception v0

    #@1c9
    goto/16 :goto_62

    #@1cb
    :catch_1cb
    move-exception v0

    #@1cc
    goto/16 :goto_56

    #@1ce
    :catch_1ce
    move-exception v0

    #@1cf
    goto/16 :goto_4b

    #@1d1
    :catch_1d1
    move-exception v0

    #@1d2
    goto/16 :goto_40

    #@1d4
    :catch_1d4
    move-exception v0

    #@1d5
    goto/16 :goto_35

    #@1d7
    :catch_1d7
    move-exception v0

    #@1d8
    goto/16 :goto_2a

    #@1da
    :catch_1da
    move-exception v0

    #@1db
    goto/16 :goto_1f

    #@1dd
    :catch_1dd
    move-exception v0

    #@1de
    goto/16 :goto_14
.end method
