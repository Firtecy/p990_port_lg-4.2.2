.class public Lcom/android/internal/telephony/DriverCall;
.super Ljava/lang/Object;
.source "DriverCall.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/DriverCall$State;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field private static final DBG:Z = false

.field static final LOG_TAG:Ljava/lang/String; = "RILB"


# instance fields
.field public TOA:I

.field public als:I

.field public cdnipNumber:Ljava/lang/String;

.field public index:I

.field public isMT:Z

.field public isMpty:Z

.field public isVoice:Z

.field public isVoicePrivacy:Z

.field public name:Ljava/lang/String;

.field public nameDCS:I

.field public namePresentation:I

.field public number:Ljava/lang/String;

.field public numberPresentation:I

.field public redirectNumber:Ljava/lang/String;

.field public signal:I

.field public state:Lcom/android/internal/telephony/DriverCall$State;

.field public uusInfo:Lcom/android/internal/telephony/UUSInfo;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 32
    const-string v2, "ro.debuggable"

    #@4
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v2

    #@8
    if-ne v2, v0, :cond_d

    #@a
    :goto_a
    sput-boolean v0, Lcom/android/internal/telephony/DriverCall;->DBG:Z

    #@c
    return-void

    #@d
    :cond_d
    move v0, v1

    #@e
    goto :goto_a
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 122
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 123
    return-void
.end method

.method static fromCLCCLine(Ljava/lang/String;)Lcom/android/internal/telephony/DriverCall;
    .registers 8
    .parameter "line"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 78
    new-instance v2, Lcom/android/internal/telephony/DriverCall;

    #@3
    invoke-direct {v2}, Lcom/android/internal/telephony/DriverCall;-><init>()V

    #@6
    .line 82
    .local v2, ret:Lcom/android/internal/telephony/DriverCall;
    new-instance v1, Lcom/android/internal/telephony/ATResponseParser;

    #@8
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/ATResponseParser;-><init>(Ljava/lang/String;)V

    #@b
    .line 85
    .local v1, p:Lcom/android/internal/telephony/ATResponseParser;
    :try_start_b
    invoke-virtual {v1}, Lcom/android/internal/telephony/ATResponseParser;->nextInt()I

    #@e
    move-result v4

    #@f
    iput v4, v2, Lcom/android/internal/telephony/DriverCall;->index:I

    #@11
    .line 86
    invoke-virtual {v1}, Lcom/android/internal/telephony/ATResponseParser;->nextBoolean()Z

    #@14
    move-result v4

    #@15
    iput-boolean v4, v2, Lcom/android/internal/telephony/DriverCall;->isMT:Z

    #@17
    .line 87
    invoke-virtual {v1}, Lcom/android/internal/telephony/ATResponseParser;->nextInt()I

    #@1a
    move-result v4

    #@1b
    invoke-static {v4}, Lcom/android/internal/telephony/DriverCall;->stateFromCLCC(I)Lcom/android/internal/telephony/DriverCall$State;

    #@1e
    move-result-object v4

    #@1f
    iput-object v4, v2, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@21
    .line 89
    invoke-virtual {v1}, Lcom/android/internal/telephony/ATResponseParser;->nextInt()I

    #@24
    move-result v4

    #@25
    if-nez v4, :cond_60

    #@27
    const/4 v4, 0x1

    #@28
    :goto_28
    iput-boolean v4, v2, Lcom/android/internal/telephony/DriverCall;->isVoice:Z

    #@2a
    .line 90
    invoke-virtual {v1}, Lcom/android/internal/telephony/ATResponseParser;->nextBoolean()Z

    #@2d
    move-result v4

    #@2e
    iput-boolean v4, v2, Lcom/android/internal/telephony/DriverCall;->isMpty:Z

    #@30
    .line 93
    sget v4, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@32
    iput v4, v2, Lcom/android/internal/telephony/DriverCall;->numberPresentation:I

    #@34
    .line 95
    invoke-virtual {v1}, Lcom/android/internal/telephony/ATResponseParser;->hasMore()Z

    #@37
    move-result v4

    #@38
    if-eqz v4, :cond_5f

    #@3a
    .line 98
    invoke-virtual {v1}, Lcom/android/internal/telephony/ATResponseParser;->nextString()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    iput-object v4, v2, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@44
    .line 100
    iget-object v4, v2, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@46
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@49
    move-result v4

    #@4a
    if-nez v4, :cond_4f

    #@4c
    .line 101
    const/4 v4, 0x0

    #@4d
    iput-object v4, v2, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@4f
    .line 104
    :cond_4f
    invoke-virtual {v1}, Lcom/android/internal/telephony/ATResponseParser;->nextInt()I

    #@52
    move-result v4

    #@53
    iput v4, v2, Lcom/android/internal/telephony/DriverCall;->TOA:I

    #@55
    .line 109
    iget-object v4, v2, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@57
    iget v5, v2, Lcom/android/internal/telephony/DriverCall;->TOA:I

    #@59
    invoke-static {v4, v5}, Landroid/telephony/PhoneNumberUtils;->stringFromStringAndTOA(Ljava/lang/String;I)Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    iput-object v4, v2, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;
    :try_end_5f
    .catch Lcom/android/internal/telephony/ATParseEx; {:try_start_b .. :try_end_5f} :catch_62

    #@5f
    .line 118
    .end local v2           #ret:Lcom/android/internal/telephony/DriverCall;
    :cond_5f
    :goto_5f
    return-object v2

    #@60
    .line 89
    .restart local v2       #ret:Lcom/android/internal/telephony/DriverCall;
    :cond_60
    const/4 v4, 0x0

    #@61
    goto :goto_28

    #@62
    .line 113
    :catch_62
    move-exception v0

    #@63
    .line 114
    .local v0, ex:Lcom/android/internal/telephony/ATParseEx;
    const-string v4, "RILB"

    #@65
    new-instance v5, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v6, "Invalid CLCC line: \'"

    #@6c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v5

    #@70
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v5

    #@74
    const-string v6, "\'"

    #@76
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v5

    #@7e
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    move-object v2, v3

    #@82
    .line 115
    goto :goto_5f
.end method

.method public static presentationFromCLIP(I)I
    .registers 4
    .parameter "cli"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/ATParseEx;
        }
    .end annotation

    #@0
    .prologue
    .line 161
    packed-switch p0, :pswitch_data_28

    #@3
    .line 167
    new-instance v0, Lcom/android/internal/telephony/ATParseEx;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "illegal presentation "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/ATParseEx;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 162
    :pswitch_1c
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@1e
    .line 165
    :goto_1e
    return v0

    #@1f
    .line 163
    :pswitch_1f
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_RESTRICTED:I

    #@21
    goto :goto_1e

    #@22
    .line 164
    :pswitch_22
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_UNKNOWN:I

    #@24
    goto :goto_1e

    #@25
    .line 165
    :pswitch_25
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_PAYPHONE:I

    #@27
    goto :goto_1e

    #@28
    .line 161
    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
    .end packed-switch
.end method

.method public static stateFromCLCC(I)Lcom/android/internal/telephony/DriverCall$State;
    .registers 4
    .parameter "state"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/ATParseEx;
        }
    .end annotation

    #@0
    .prologue
    .line 146
    packed-switch p0, :pswitch_data_2e

    #@3
    .line 154
    new-instance v0, Lcom/android/internal/telephony/ATParseEx;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "illegal call state "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/ATParseEx;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 147
    :pswitch_1c
    sget-object v0, Lcom/android/internal/telephony/DriverCall$State;->ACTIVE:Lcom/android/internal/telephony/DriverCall$State;

    #@1e
    .line 152
    :goto_1e
    return-object v0

    #@1f
    .line 148
    :pswitch_1f
    sget-object v0, Lcom/android/internal/telephony/DriverCall$State;->HOLDING:Lcom/android/internal/telephony/DriverCall$State;

    #@21
    goto :goto_1e

    #@22
    .line 149
    :pswitch_22
    sget-object v0, Lcom/android/internal/telephony/DriverCall$State;->DIALING:Lcom/android/internal/telephony/DriverCall$State;

    #@24
    goto :goto_1e

    #@25
    .line 150
    :pswitch_25
    sget-object v0, Lcom/android/internal/telephony/DriverCall$State;->ALERTING:Lcom/android/internal/telephony/DriverCall$State;

    #@27
    goto :goto_1e

    #@28
    .line 151
    :pswitch_28
    sget-object v0, Lcom/android/internal/telephony/DriverCall$State;->INCOMING:Lcom/android/internal/telephony/DriverCall$State;

    #@2a
    goto :goto_1e

    #@2b
    .line 152
    :pswitch_2b
    sget-object v0, Lcom/android/internal/telephony/DriverCall$State;->WAITING:Lcom/android/internal/telephony/DriverCall$State;

    #@2d
    goto :goto_1e

    #@2e
    .line 146
    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_2b
    .end packed-switch
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .registers 5
    .parameter "o"

    #@0
    .prologue
    .line 178
    move-object v0, p1

    #@1
    check-cast v0, Lcom/android/internal/telephony/DriverCall;

    #@3
    .line 180
    .local v0, dc:Lcom/android/internal/telephony/DriverCall;
    iget v1, p0, Lcom/android/internal/telephony/DriverCall;->index:I

    #@5
    iget v2, v0, Lcom/android/internal/telephony/DriverCall;->index:I

    #@7
    if-ge v1, v2, :cond_b

    #@9
    .line 181
    const/4 v1, -0x1

    #@a
    .line 185
    :goto_a
    return v1

    #@b
    .line 182
    :cond_b
    iget v1, p0, Lcom/android/internal/telephony/DriverCall;->index:I

    #@d
    iget v2, v0, Lcom/android/internal/telephony/DriverCall;->index:I

    #@f
    if-ne v1, v2, :cond_13

    #@11
    .line 183
    const/4 v1, 0x0

    #@12
    goto :goto_a

    #@13
    .line 185
    :cond_13
    const/4 v1, 0x1

    #@14
    goto :goto_a
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "id="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/internal/telephony/DriverCall;->index:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ","

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ","

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string v1, "toa="

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    iget v1, p0, Lcom/android/internal/telephony/DriverCall;->TOA:I

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, ","

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    iget-boolean v0, p0, Lcom/android/internal/telephony/DriverCall;->isMpty:Z

    #@37
    if-eqz v0, :cond_c4

    #@39
    const-string v0, "conf"

    #@3b
    :goto_3b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, ","

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    iget-boolean v0, p0, Lcom/android/internal/telephony/DriverCall;->isMT:Z

    #@47
    if-eqz v0, :cond_c8

    #@49
    const-string v0, "mt"

    #@4b
    :goto_4b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    const-string v1, ","

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    iget v1, p0, Lcom/android/internal/telephony/DriverCall;->als:I

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    const-string v1, ","

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    iget-boolean v0, p0, Lcom/android/internal/telephony/DriverCall;->isVoice:Z

    #@63
    if-eqz v0, :cond_cb

    #@65
    const-string v0, "voc"

    #@67
    :goto_67
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v0

    #@6b
    const-string v1, ","

    #@6d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    iget-boolean v0, p0, Lcom/android/internal/telephony/DriverCall;->isVoicePrivacy:Z

    #@73
    if-eqz v0, :cond_ce

    #@75
    const-string v0, "evp"

    #@77
    :goto_77
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v0

    #@7b
    const-string v1, ","

    #@7d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v0

    #@81
    const-string v1, "number="

    #@83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    sget-boolean v0, Lcom/android/internal/telephony/DriverCall;->DBG:Z

    #@89
    if-eqz v0, :cond_d1

    #@8b
    iget-object v0, p0, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@8d
    :goto_8d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    const-string v1, ",cli="

    #@93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v0

    #@97
    iget v1, p0, Lcom/android/internal/telephony/DriverCall;->numberPresentation:I

    #@99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v0

    #@9d
    const-string v1, ","

    #@9f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v0

    #@a3
    const-string v1, "name="

    #@a5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v1

    #@a9
    sget-boolean v0, Lcom/android/internal/telephony/DriverCall;->DBG:Z

    #@ab
    if-eqz v0, :cond_d4

    #@ad
    iget-object v0, p0, Lcom/android/internal/telephony/DriverCall;->name:Ljava/lang/String;

    #@af
    :goto_af
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v0

    #@b3
    const-string v1, ","

    #@b5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v0

    #@b9
    iget v1, p0, Lcom/android/internal/telephony/DriverCall;->namePresentation:I

    #@bb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@be
    move-result-object v0

    #@bf
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v0

    #@c3
    return-object v0

    #@c4
    :cond_c4
    const-string v0, "norm"

    #@c6
    goto/16 :goto_3b

    #@c8
    :cond_c8
    const-string v0, "mo"

    #@ca
    goto :goto_4b

    #@cb
    :cond_cb
    const-string v0, "nonvoc"

    #@cd
    goto :goto_67

    #@ce
    :cond_ce
    const-string v0, "noevp"

    #@d0
    goto :goto_77

    #@d1
    :cond_d1
    const-string v0, "***"

    #@d3
    goto :goto_8d

    #@d4
    :cond_d4
    const-string v0, "***"

    #@d6
    goto :goto_af
.end method
