.class Lcom/android/internal/telephony/cdma/CDMAPhone$2;
.super Landroid/content/BroadcastReceiver;
.source "CDMAPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cdma/CDMAPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 361
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 364
    invoke-static {p1, p2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->updateProfile(Landroid/content/Context;Landroid/content/Intent;)V

    #@5
    .line 365
    const-string v2, "ss"

    #@7
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    .line 366
    .local v1, stateExtra:Ljava/lang/String;
    const-string v2, "CDMA"

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v4, "[CDMAPhone] SimStateReceiver - stateExtra: "

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 367
    const-string v2, "LOADED"

    #@25
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v2

    #@29
    if-eqz v2, :cond_32

    #@2b
    .line 369
    const-string v2, "CDMA"

    #@2d
    const-string v3, "[CDMAPhone] SimStateReceiver ICC LOADED"

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 373
    :cond_32
    const-string v2, "READY"

    #@34
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v2

    #@38
    if-eqz v2, :cond_8f

    #@3a
    .line 375
    const-string v2, "CDMA"

    #@3c
    const-string v3, "[CDMAPhone] SimStateReceiver - ICC READY"

    #@3e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 377
    const-string v2, "seperate_processing_sms_uicc"

    #@43
    invoke-static {v5, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@46
    move-result v2

    #@47
    if-eqz v2, :cond_8f

    #@49
    .line 379
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4b
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$000(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@4e
    move-result-object v2

    #@4f
    const-string v3, "uicc_csim"

    #@51
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@54
    move-result v2

    #@55
    if-nez v2, :cond_8f

    #@57
    .line 381
    :try_start_57
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@5e
    move-result v2

    #@5f
    if-ne v2, v7, :cond_e5

    #@61
    .line 382
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@63
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$100(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@66
    move-result-object v2

    #@67
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@69
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$200(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@70
    move-result-object v3

    #@71
    sget-object v4, Lcom/android/internal/telephony/uicc/SIMRecords;->ALL_ICC_DS_URI:Landroid/net/Uri;

    #@73
    const/4 v5, 0x0

    #@74
    const/4 v6, 0x0

    #@75
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@78
    .line 387
    :goto_78
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@7a
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$500(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@7d
    move-result-object v2

    #@7e
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@80
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$600(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@83
    move-result-object v3

    #@84
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@87
    move-result-object v3

    #@88
    sget-object v4, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@8a
    const/4 v5, 0x0

    #@8b
    const/4 v6, 0x0

    #@8c
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_8f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_57 .. :try_end_8f} :catch_105

    #@8f
    .line 397
    :cond_8f
    :goto_8f
    const-string v2, "ABSENT"

    #@91
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@94
    move-result v2

    #@95
    if-eqz v2, :cond_e4

    #@97
    .line 399
    const-string v2, "CDMA"

    #@99
    const-string v3, "[CDMAPhone] SimStateReceiver - ICC ABSENT"

    #@9b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    .line 401
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@a0
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$700(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@a3
    move-result-object v2

    #@a4
    const-string v3, "seperate_processing_sms_uicc"

    #@a6
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a9
    move-result v2

    #@aa
    if-eqz v2, :cond_e4

    #@ac
    .line 403
    :try_start_ac
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@af
    move-result-object v2

    #@b0
    invoke-virtual {v2}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@b3
    move-result v2

    #@b4
    if-ne v2, v7, :cond_120

    #@b6
    .line 404
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@b8
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$800(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@bb
    move-result-object v2

    #@bc
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@be
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$900(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@c1
    move-result-object v3

    #@c2
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c5
    move-result-object v3

    #@c6
    sget-object v4, Lcom/android/internal/telephony/uicc/SIMRecords;->ALL_ICC_DS_URI:Landroid/net/Uri;

    #@c8
    const/4 v5, 0x0

    #@c9
    const/4 v6, 0x0

    #@ca
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@cd
    .line 409
    :goto_cd
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@cf
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$1200(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@d2
    move-result-object v2

    #@d3
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@d5
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$1300(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@d8
    move-result-object v3

    #@d9
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@dc
    move-result-object v3

    #@dd
    sget-object v4, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@df
    const/4 v5, 0x0

    #@e0
    const/4 v6, 0x0

    #@e1
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_e4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_ac .. :try_end_e4} :catch_13f

    #@e4
    .line 416
    :cond_e4
    :goto_e4
    return-void

    #@e5
    .line 384
    :cond_e5
    :try_start_e5
    const-string v2, "CDMA"

    #@e7
    const-string v3, "[CDMAPhone] SimStateReceiver - Delete RuimRecords.ICC_URI"

    #@e9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ec
    .line 385
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@ee
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$300(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@f1
    move-result-object v2

    #@f2
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@f4
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$400(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@f7
    move-result-object v3

    #@f8
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@fb
    move-result-object v3

    #@fc
    sget-object v4, Lcom/android/internal/telephony/uicc/RuimRecords;->ICC_URI:Landroid/net/Uri;

    #@fe
    const/4 v5, 0x0

    #@ff
    const/4 v6, 0x0

    #@100
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_103
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_e5 .. :try_end_103} :catch_105

    #@103
    goto/16 :goto_78

    #@105
    .line 389
    :catch_105
    move-exception v0

    #@106
    .line 390
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    const-string v2, "CDMA"

    #@108
    new-instance v3, Ljava/lang/StringBuilder;

    #@10a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10d
    const-string v4, "[TEL-SMS] sql exception -"

    #@10f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v3

    #@113
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v3

    #@117
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11a
    move-result-object v3

    #@11b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11e
    goto/16 :goto_8f

    #@120
    .line 406
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :cond_120
    :try_start_120
    const-string v2, "CDMA"

    #@122
    const-string v3, "[CDMAPhone] SimStateReceiver - Delete RuimRecords.ICC_URI"

    #@124
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@127
    .line 407
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@129
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$1000(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@12c
    move-result-object v2

    #@12d
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;->this$0:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@12f
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->access$1100(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;

    #@132
    move-result-object v3

    #@133
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@136
    move-result-object v3

    #@137
    sget-object v4, Lcom/android/internal/telephony/uicc/RuimRecords;->ICC_URI:Landroid/net/Uri;

    #@139
    const/4 v5, 0x0

    #@13a
    const/4 v6, 0x0

    #@13b
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_13e
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_120 .. :try_end_13e} :catch_13f

    #@13e
    goto :goto_cd

    #@13f
    .line 411
    :catch_13f
    move-exception v0

    #@140
    .line 412
    .restart local v0       #e:Landroid/database/sqlite/SQLiteException;
    const-string v2, "CDMA"

    #@142
    new-instance v3, Ljava/lang/StringBuilder;

    #@144
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@147
    const-string v4, "[TEL-SMS] sql exception -"

    #@149
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v3

    #@14d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v3

    #@151
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@154
    move-result-object v3

    #@155
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@158
    goto :goto_e4
.end method
