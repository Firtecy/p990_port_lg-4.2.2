.class public abstract Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;
.super Landroid/os/Binder;
.source "IUsimInfo.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IUsimInfo;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/IUsimInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/uicc/IUsimInfo$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.uicc.IUsimInfo"

.field static final TRANSACTION_PBMDeleteRecord:I = 0x5

.field static final TRANSACTION_PBMGetInfo:I = 0x6

.field static final TRANSACTION_PBMReadRecord:I = 0x3

.field static final TRANSACTION_PBMWriteRecord:I = 0x4

.field static final TRANSACTION_ReadFromSIM:I = 0x8

.field static final TRANSACTION_WriteToSIM:I = 0x9

.field static final TRANSACTION_getATR:I = 0x16

.field static final TRANSACTION_getEfRecordsSize:I = 0xc

.field static final TRANSACTION_getIMSI_M:I = 0x14

.field static final TRANSACTION_getSCAddressFromIcc:I = 0xa

.field static final TRANSACTION_getSCAddressTemp:I = 0xd

.field static final TRANSACTION_getUsimIsEmpty:I = 0x7

.field static final TRANSACTION_getUsimIsSponIMSI:I = 0xf

.field static final TRANSACTION_registerCallback:I = 0x1

.field static final TRANSACTION_sendAPDUToIcc:I = 0x15

.field static final TRANSACTION_sendChangeToHomeIMSI:I = 0x11

.field static final TRANSACTION_sendChangeToSponIMSI:I = 0x10

.field static final TRANSACTION_sendUpdatePLMN:I = 0x12

.field static final TRANSACTION_setSCAddressTemp:I = 0xe

.field static final TRANSACTION_setSCAddressToIcc:I = 0xb

.field static final TRANSACTION_startOtaService:I = 0x13

.field static final TRANSACTION_unregisterCallback:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/uicc/IUsimInfo;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.android.internal.telephony.uicc.IUsimInfo"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 16
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 38
    sparse-switch p1, :sswitch_data_1ec

    #@3
    .line 270
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v0

    #@7
    :goto_7
    return v0

    #@8
    .line 42
    :sswitch_8
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 43
    const/4 v0, 0x1

    #@e
    goto :goto_7

    #@f
    .line 47
    :sswitch_f
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@17
    move-result-wide v8

    #@18
    .line 51
    .local v8, _arg0:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v0

    #@1c
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/uicc/IUsimInfoCallback;

    #@1f
    move-result-object v2

    #@20
    .line 52
    .local v2, _arg1:Lcom/android/internal/telephony/uicc/IUsimInfoCallback;
    invoke-virtual {p0, v8, v9, v2}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->registerCallback(JLcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@23
    .line 53
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@26
    .line 54
    const/4 v0, 0x1

    #@27
    goto :goto_7

    #@28
    .line 58
    .end local v2           #_arg1:Lcom/android/internal/telephony/uicc/IUsimInfoCallback;
    .end local v8           #_arg0:J
    :sswitch_28
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@2a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d
    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@30
    move-result-wide v8

    #@31
    .line 62
    .restart local v8       #_arg0:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@34
    move-result-object v0

    #@35
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/uicc/IUsimInfoCallback;

    #@38
    move-result-object v2

    #@39
    .line 63
    .restart local v2       #_arg1:Lcom/android/internal/telephony/uicc/IUsimInfoCallback;
    invoke-virtual {p0, v8, v9, v2}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->unregisterCallback(JLcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@3c
    .line 64
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f
    .line 65
    const/4 v0, 0x1

    #@40
    goto :goto_7

    #@41
    .line 69
    .end local v2           #_arg1:Lcom/android/internal/telephony/uicc/IUsimInfoCallback;
    .end local v8           #_arg0:J
    :sswitch_41
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@43
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@46
    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@49
    move-result-wide v8

    #@4a
    .line 73
    .restart local v8       #_arg0:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v2

    #@4e
    .line 75
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@51
    move-result v3

    #@52
    .line 76
    .local v3, _arg2:I
    invoke-virtual {p0, v8, v9, v2, v3}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->PBMReadRecord(JII)V

    #@55
    .line 77
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@58
    .line 78
    const/4 v0, 0x1

    #@59
    goto :goto_7

    #@5a
    .line 82
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v8           #_arg0:J
    :sswitch_5a
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@5c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5f
    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@62
    move-result-wide v8

    #@63
    .line 86
    .restart local v8       #_arg0:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@66
    move-result v0

    #@67
    if-eqz v0, :cond_79

    #@69
    .line 87
    sget-object v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6b
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6e
    move-result-object v2

    #@6f
    check-cast v2, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@71
    .line 92
    .local v2, _arg1:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    :goto_71
    invoke-virtual {p0, v8, v9, v2}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->PBMWriteRecord(JLcom/android/internal/telephony/uicc/LGE_PBM_Records;)V

    #@74
    .line 93
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@77
    .line 94
    const/4 v0, 0x1

    #@78
    goto :goto_7

    #@79
    .line 90
    .end local v2           #_arg1:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    :cond_79
    const/4 v2, 0x0

    #@7a
    .restart local v2       #_arg1:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    goto :goto_71

    #@7b
    .line 98
    .end local v2           #_arg1:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    .end local v8           #_arg0:J
    :sswitch_7b
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@7d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@80
    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@83
    move-result-wide v8

    #@84
    .line 102
    .restart local v8       #_arg0:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@87
    move-result v2

    #@88
    .line 104
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@8b
    move-result v3

    #@8c
    .line 105
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v8, v9, v2, v3}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->PBMDeleteRecord(JII)V

    #@8f
    .line 106
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@92
    .line 107
    const/4 v0, 0x1

    #@93
    goto/16 :goto_7

    #@95
    .line 111
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v8           #_arg0:J
    :sswitch_95
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@97
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9a
    .line 113
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@9d
    move-result-wide v8

    #@9e
    .line 115
    .restart local v8       #_arg0:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a1
    move-result v2

    #@a2
    .line 116
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v8, v9, v2}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->PBMGetInfo(JI)V

    #@a5
    .line 117
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a8
    .line 118
    const/4 v0, 0x1

    #@a9
    goto/16 :goto_7

    #@ab
    .line 122
    .end local v2           #_arg1:I
    .end local v8           #_arg0:J
    :sswitch_ab
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@ad
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b0
    .line 123
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->getUsimIsEmpty()I

    #@b3
    move-result v10

    #@b4
    .line 124
    .local v10, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b7
    .line 125
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@ba
    .line 126
    const/4 v0, 0x1

    #@bb
    goto/16 :goto_7

    #@bd
    .line 130
    .end local v10           #_result:I
    :sswitch_bd
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@bf
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c2
    .line 132
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c5
    move-result v1

    #@c6
    .line 133
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->ReadFromSIM(I)[B

    #@c9
    move-result-object v10

    #@ca
    .line 134
    .local v10, _result:[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@cd
    .line 135
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeByteArray([B)V

    #@d0
    .line 136
    const/4 v0, 0x1

    #@d1
    goto/16 :goto_7

    #@d3
    .line 140
    .end local v1           #_arg0:I
    .end local v10           #_result:[B
    :sswitch_d3
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@d5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d8
    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@db
    move-result v1

    #@dc
    .line 144
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@df
    move-result-object v2

    #@e0
    .line 145
    .local v2, _arg1:[B
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->WriteToSIM(I[B)[B

    #@e3
    move-result-object v10

    #@e4
    .line 146
    .restart local v10       #_result:[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e7
    .line 147
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeByteArray([B)V

    #@ea
    .line 148
    const/4 v0, 0x1

    #@eb
    goto/16 :goto_7

    #@ed
    .line 152
    .end local v1           #_arg0:I
    .end local v2           #_arg1:[B
    .end local v10           #_result:[B
    :sswitch_ed
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@ef
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f2
    .line 153
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->getSCAddressFromIcc()Ljava/lang/String;

    #@f5
    move-result-object v10

    #@f6
    .line 154
    .local v10, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f9
    .line 155
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@fc
    .line 156
    const/4 v0, 0x1

    #@fd
    goto/16 :goto_7

    #@ff
    .line 160
    .end local v10           #_result:Ljava/lang/String;
    :sswitch_ff
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@101
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@104
    .line 162
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@107
    move-result-object v1

    #@108
    .line 163
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->setSCAddressToIcc(Ljava/lang/String;)V

    #@10b
    .line 164
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@10e
    .line 165
    const/4 v0, 0x1

    #@10f
    goto/16 :goto_7

    #@111
    .line 169
    .end local v1           #_arg0:Ljava/lang/String;
    :sswitch_111
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@113
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@116
    .line 171
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@119
    move-result v1

    #@11a
    .line 172
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->getEfRecordsSize(I)I

    #@11d
    move-result v10

    #@11e
    .line 173
    .local v10, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@121
    .line 174
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@124
    .line 175
    const/4 v0, 0x1

    #@125
    goto/16 :goto_7

    #@127
    .line 179
    .end local v1           #_arg0:I
    .end local v10           #_result:I
    :sswitch_127
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@129
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12c
    .line 180
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->getSCAddressTemp()Ljava/lang/String;

    #@12f
    move-result-object v10

    #@130
    .line 181
    .local v10, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@133
    .line 182
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@136
    .line 183
    const/4 v0, 0x1

    #@137
    goto/16 :goto_7

    #@139
    .line 187
    .end local v10           #_result:Ljava/lang/String;
    :sswitch_139
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@13b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13e
    .line 189
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@141
    move-result-object v1

    #@142
    .line 190
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->setSCAddressTemp(Ljava/lang/String;)V

    #@145
    .line 191
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@148
    .line 192
    const/4 v0, 0x1

    #@149
    goto/16 :goto_7

    #@14b
    .line 196
    .end local v1           #_arg0:Ljava/lang/String;
    :sswitch_14b
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@14d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@150
    .line 197
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->getUsimIsSponIMSI()I

    #@153
    move-result v10

    #@154
    .line 198
    .local v10, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@157
    .line 199
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@15a
    .line 200
    const/4 v0, 0x1

    #@15b
    goto/16 :goto_7

    #@15d
    .line 204
    .end local v10           #_result:I
    :sswitch_15d
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@15f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@162
    .line 205
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->sendChangeToSponIMSI()V

    #@165
    .line 206
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@168
    .line 207
    const/4 v0, 0x1

    #@169
    goto/16 :goto_7

    #@16b
    .line 211
    :sswitch_16b
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@16d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@170
    .line 212
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->sendChangeToHomeIMSI()V

    #@173
    .line 213
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@176
    .line 214
    const/4 v0, 0x1

    #@177
    goto/16 :goto_7

    #@179
    .line 218
    :sswitch_179
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@17b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@17e
    .line 219
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->sendUpdatePLMN()V

    #@181
    .line 220
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@184
    .line 221
    const/4 v0, 0x1

    #@185
    goto/16 :goto_7

    #@187
    .line 225
    :sswitch_187
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@189
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18c
    .line 226
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->startOtaService()I

    #@18f
    move-result v10

    #@190
    .line 227
    .restart local v10       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@193
    .line 228
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@196
    .line 229
    const/4 v0, 0x1

    #@197
    goto/16 :goto_7

    #@199
    .line 233
    .end local v10           #_result:I
    :sswitch_199
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@19b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19e
    .line 234
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->getIMSI_M()Ljava/lang/String;

    #@1a1
    move-result-object v10

    #@1a2
    .line 235
    .local v10, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a5
    .line 236
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a8
    .line 237
    const/4 v0, 0x1

    #@1a9
    goto/16 :goto_7

    #@1ab
    .line 241
    .end local v10           #_result:Ljava/lang/String;
    :sswitch_1ab
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@1ad
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b0
    .line 243
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b3
    move-result v1

    #@1b4
    .line 245
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b7
    move-result v2

    #@1b8
    .line 247
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1bb
    move-result v3

    #@1bc
    .line 249
    .restart local v3       #_arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1bf
    move-result v4

    #@1c0
    .line 251
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c3
    move-result v5

    #@1c4
    .line 253
    .local v5, _arg4:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c7
    move-result v6

    #@1c8
    .line 255
    .local v6, _arg5:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1cb
    move-result-object v7

    #@1cc
    .local v7, _arg6:Ljava/lang/String;
    move-object v0, p0

    #@1cd
    .line 256
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->sendAPDUToIcc(IIIIIILjava/lang/String;)Ljava/lang/String;

    #@1d0
    move-result-object v10

    #@1d1
    .line 257
    .restart local v10       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d4
    .line 258
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d7
    .line 259
    const/4 v0, 0x1

    #@1d8
    goto/16 :goto_7

    #@1da
    .line 263
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:I
    .end local v6           #_arg5:I
    .end local v7           #_arg6:Ljava/lang/String;
    .end local v10           #_result:Ljava/lang/String;
    :sswitch_1da
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfo"

    #@1dc
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1df
    .line 264
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->getATR()[B

    #@1e2
    move-result-object v10

    #@1e3
    .line 265
    .local v10, _result:[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e6
    .line 266
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeByteArray([B)V

    #@1e9
    .line 267
    const/4 v0, 0x1

    #@1ea
    goto/16 :goto_7

    #@1ec
    .line 38
    :sswitch_data_1ec
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_28
        0x3 -> :sswitch_41
        0x4 -> :sswitch_5a
        0x5 -> :sswitch_7b
        0x6 -> :sswitch_95
        0x7 -> :sswitch_ab
        0x8 -> :sswitch_bd
        0x9 -> :sswitch_d3
        0xa -> :sswitch_ed
        0xb -> :sswitch_ff
        0xc -> :sswitch_111
        0xd -> :sswitch_127
        0xe -> :sswitch_139
        0xf -> :sswitch_14b
        0x10 -> :sswitch_15d
        0x11 -> :sswitch_16b
        0x12 -> :sswitch_179
        0x13 -> :sswitch_187
        0x14 -> :sswitch_199
        0x15 -> :sswitch_1ab
        0x16 -> :sswitch_1da
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
