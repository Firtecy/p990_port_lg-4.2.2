.class public final enum Lcom/android/internal/telephony/gsm/SsData$RequestType;
.super Ljava/lang/Enum;
.source "SsData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gsm/SsData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RequestType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/gsm/SsData$RequestType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/gsm/SsData$RequestType;

.field public static final enum SS_ACTIVATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

.field public static final enum SS_DEACTIVATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

.field public static final enum SS_ERASURE:Lcom/android/internal/telephony/gsm/SsData$RequestType;

.field public static final enum SS_INTERROGATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

.field public static final enum SS_REGISTRATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 82
    new-instance v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@7
    const-string v1, "SS_ACTIVATION"

    #@9
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gsm/SsData$RequestType;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_ACTIVATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@e
    .line 83
    new-instance v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@10
    const-string v1, "SS_DEACTIVATION"

    #@12
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/gsm/SsData$RequestType;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_DEACTIVATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@17
    .line 84
    new-instance v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@19
    const-string v1, "SS_INTERROGATION"

    #@1b
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/gsm/SsData$RequestType;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_INTERROGATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@20
    .line 85
    new-instance v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@22
    const-string v1, "SS_REGISTRATION"

    #@24
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/gsm/SsData$RequestType;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_REGISTRATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@29
    .line 86
    new-instance v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@2b
    const-string v1, "SS_ERASURE"

    #@2d
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/gsm/SsData$RequestType;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_ERASURE:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@32
    .line 81
    const/4 v0, 0x5

    #@33
    new-array v0, v0, [Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@35
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_ACTIVATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@37
    aput-object v1, v0, v2

    #@39
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_DEACTIVATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@3b
    aput-object v1, v0, v3

    #@3d
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_INTERROGATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@3f
    aput-object v1, v0, v4

    #@41
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_REGISTRATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@43
    aput-object v1, v0, v5

    #@45
    sget-object v1, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_ERASURE:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@47
    aput-object v1, v0, v6

    #@49
    sput-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->$VALUES:[Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@4b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/gsm/SsData$RequestType;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 81
    const-class v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/gsm/SsData$RequestType;
    .registers 1

    #@0
    .prologue
    .line 81
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->$VALUES:[Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/gsm/SsData$RequestType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@8
    return-object v0
.end method


# virtual methods
.method public isTypeInterrogation()Z
    .registers 2

    #@0
    .prologue
    .line 89
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_INTERROGATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@2
    if-ne p0, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method
