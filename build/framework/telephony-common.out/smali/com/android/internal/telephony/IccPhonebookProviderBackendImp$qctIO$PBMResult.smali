.class Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;
.super Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$Result;
.source "IccPhonebookProviderBackendImp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PBMResult"
.end annotation


# instance fields
.field mNativeRecord:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

.field mReturnCode:I

.field final synthetic this$1:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;II)V
    .registers 5
    .parameter
    .parameter "result"
    .parameter "index"

    #@0
    .prologue
    .line 183
    iput-object p1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->this$1:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@2
    .line 184
    invoke-direct {p0, p1, p3}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$Result;-><init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;I)V

    #@5
    .line 181
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->mNativeRecord:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@8
    .line 185
    iput p2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->mReturnCode:I

    #@a
    .line 186
    return-void
.end method

.method constructor <init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)V
    .registers 4
    .parameter
    .parameter "record"

    #@0
    .prologue
    .line 188
    iput-object p1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->this$1:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@2
    .line 189
    iget v0, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@4
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$Result;-><init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;I)V

    #@7
    .line 181
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->mNativeRecord:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@a
    .line 190
    iput-object p2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->mNativeRecord:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@c
    .line 191
    return-void
.end method


# virtual methods
.method getNativeRecord()Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    .registers 2

    #@0
    .prologue
    .line 197
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->mNativeRecord:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@2
    return-object v0
.end method

.method public isSuccess()Z
    .registers 2

    #@0
    .prologue
    .line 194
    iget v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->mReturnCode:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method
