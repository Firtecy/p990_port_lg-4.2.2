.class Lcom/android/internal/telephony/cdma/EriManager$EriFile;
.super Ljava/lang/Object;
.source "EriManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cdma/EriManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EriFile"
.end annotation


# instance fields
.field public mCallPrmptTable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/internal/telephony/cdma/EriManager$EriPrmpt;",
            ">;"
        }
    .end annotation
.end field

.field public mCallPromptId:[Ljava/lang/String;

.field public mEriFileCrc:I

.field public mEriFileType:I

.field public mIconImageType:I

.field public mIconImgTable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/internal/telephony/cdma/EriManager$EriImg;",
            ">;"
        }
    .end annotation
.end field

.field public mNumberOfEriEntries:I

.field public mNumberOfIconImages:I

.field public mReservedPadBits:I

.field public mRoamIndTable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/internal/telephony/cdma/EriInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mVersionNumber:I

.field final synthetic this$0:Lcom/android/internal/telephony/cdma/EriManager;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/cdma/EriManager;)V
    .registers 5
    .parameter

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v0, -0x1

    #@2
    .line 78
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->this$0:Lcom/android/internal/telephony/cdma/EriManager;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 79
    iput v0, p0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mVersionNumber:I

    #@9
    .line 80
    iput v2, p0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mNumberOfEriEntries:I

    #@b
    .line 81
    iput v0, p0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mEriFileType:I

    #@d
    .line 82
    const/4 v0, 0x3

    #@e
    new-array v0, v0, [Ljava/lang/String;

    #@10
    const-string v1, ""

    #@12
    aput-object v1, v0, v2

    #@14
    const/4 v1, 0x1

    #@15
    const-string v2, ""

    #@17
    aput-object v2, v0, v1

    #@19
    const/4 v1, 0x2

    #@1a
    const-string v2, ""

    #@1c
    aput-object v2, v0, v1

    #@1e
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mCallPromptId:[Ljava/lang/String;

    #@20
    .line 83
    new-instance v0, Ljava/util/HashMap;

    #@22
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@25
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mRoamIndTable:Ljava/util/HashMap;

    #@27
    .line 85
    const/4 v0, 0x0

    #@28
    const-string v1, "vzw_eri"

    #@2a
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_3e

    #@30
    .line 86
    new-instance v0, Ljava/util/HashMap;

    #@32
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@35
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mIconImgTable:Ljava/util/HashMap;

    #@37
    .line 87
    new-instance v0, Ljava/util/HashMap;

    #@39
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@3c
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mCallPrmptTable:Ljava/util/HashMap;

    #@3e
    .line 90
    :cond_3e
    return-void
.end method
