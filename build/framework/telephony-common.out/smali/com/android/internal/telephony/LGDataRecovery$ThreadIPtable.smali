.class public Lcom/android/internal/telephony/LGDataRecovery$ThreadIPtable;
.super Ljava/lang/Thread;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGDataRecovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThreadIPtable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LGDataRecovery;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/LGDataRecovery;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1979
    iput-object p1, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadIPtable;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 1984
    const-string v2, "juhwan. thread 2"

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@5
    .line 1986
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadIPtable;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->access$200(Lcom/android/internal/telephony/LGDataRecovery;)Landroid/os/INetworkManagementService;

    #@a
    move-result-object v2

    #@b
    if-nez v2, :cond_1c

    #@d
    .line 1987
    const-string v2, "network_management"

    #@f
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@12
    move-result-object v0

    #@13
    .line 1989
    .local v0, b:Landroid/os/IBinder;
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadIPtable;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@15
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@18
    move-result-object v3

    #@19
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->access$202(Lcom/android/internal/telephony/LGDataRecovery;Landroid/os/INetworkManagementService;)Landroid/os/INetworkManagementService;

    #@1c
    .line 1993
    .end local v0           #b:Landroid/os/IBinder;
    :cond_1c
    :try_start_1c
    const-string v2, "********** DUMP IPTABLE INFO **********"

    #@1e
    const-string v3, "iptablesinfo.log"

    #@20
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->access$400(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 1994
    const-string v2, "<----- iptables -L ----->"

    #@25
    const-string v3, "iptablesinfo.log"

    #@27
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->access$400(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    .line 1995
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadIPtable;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@2c
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->access$200(Lcom/android/internal/telephony/LGDataRecovery;)Landroid/os/INetworkManagementService;

    #@2f
    move-result-object v2

    #@30
    const-string v3, "iptables -L -n"

    #@32
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->runShellCommand(Ljava/lang/String;)V

    #@35
    .line 1996
    const-string v2, "<----- iptables -t nat -L ----->"

    #@37
    const-string v3, "iptablesinfo.log"

    #@39
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->access$400(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 1997
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadIPtable;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@3e
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->access$200(Lcom/android/internal/telephony/LGDataRecovery;)Landroid/os/INetworkManagementService;

    #@41
    move-result-object v2

    #@42
    const-string v3, "iptables -t nat -L -n"

    #@44
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->runShellCommand(Ljava/lang/String;)V

    #@47
    .line 1998
    const-string v2, "<----- iptables -t mangle -L ----->"

    #@49
    const-string v3, "iptablesinfo.log"

    #@4b
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->access$400(Ljava/lang/String;Ljava/lang/String;)V

    #@4e
    .line 1999
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadIPtable;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@50
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->access$200(Lcom/android/internal/telephony/LGDataRecovery;)Landroid/os/INetworkManagementService;

    #@53
    move-result-object v2

    #@54
    const-string v3, "iptables -t mangle -L -n"

    #@56
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->runShellCommand(Ljava/lang/String;)V

    #@59
    .line 2000
    const-string v2, "<----- iptables -t raw -L ----->"

    #@5b
    const-string v3, "iptablesinfo.log"

    #@5d
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->access$400(Ljava/lang/String;Ljava/lang/String;)V

    #@60
    .line 2001
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadIPtable;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@62
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->access$200(Lcom/android/internal/telephony/LGDataRecovery;)Landroid/os/INetworkManagementService;

    #@65
    move-result-object v2

    #@66
    const-string v3, "iptables -t raw -L -n"

    #@68
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->runShellCommand(Ljava/lang/String;)V
    :try_end_6b
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_6b} :catch_6c

    #@6b
    .line 2007
    :goto_6b
    return-void

    #@6c
    .line 2003
    :catch_6c
    move-exception v1

    #@6d
    .line 2004
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "failed for logging iptables"

    #@6f
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@72
    goto :goto_6b
.end method
