.class Lcom/android/internal/telephony/IMSPhone$MyHandler;
.super Landroid/os/Handler;
.source "IMSPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IMSPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/IMSPhone;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/IMSPhone;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 220
    iput-object p1, p0, Lcom/android/internal/telephony/IMSPhone$MyHandler;->this$0:Lcom/android/internal/telephony/IMSPhone;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/IMSPhone;Lcom/android/internal/telephony/IMSPhone$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 220
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IMSPhone$MyHandler;-><init>(Lcom/android/internal/telephony/IMSPhone;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 14
    .parameter "msg"

    #@0
    .prologue
    .line 224
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v8, Landroid/os/AsyncResult;

    #@4
    move-object v0, v8

    #@5
    check-cast v0, Landroid/os/AsyncResult;

    #@7
    .line 226
    .local v0, ar:Landroid/os/AsyncResult;
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@a
    move-result v8

    #@b
    if-eqz v8, :cond_2d

    #@d
    .line 227
    const-string v8, "LGIMS"

    #@f
    new-instance v9, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v10, "handleMessage :: event ("

    #@16
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v9

    #@1a
    iget v10, p1, Landroid/os/Message;->what:I

    #@1c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v9

    #@20
    const-string v10, ")"

    #@22
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v9

    #@26
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v9

    #@2a
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 230
    :cond_2d
    if-nez v0, :cond_3d

    #@2f
    .line 231
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@32
    move-result v8

    #@33
    if-eqz v8, :cond_3c

    #@35
    .line 232
    const-string v8, "LGIMS"

    #@37
    const-string v9, "ar is null"

    #@39
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 402
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .line 237
    :cond_3d
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->getSysMonitor()Lcom/android/internal/telephony/ISysMonitor;

    #@40
    move-result-object v6

    #@41
    .line 239
    .local v6, sysMonitor:Lcom/android/internal/telephony/ISysMonitor;
    if-nez v6, :cond_51

    #@43
    .line 240
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@46
    move-result v8

    #@47
    if-eqz v8, :cond_3c

    #@49
    .line 241
    const-string v8, "LGIMS"

    #@4b
    const-string v9, "SysMonitor is null"

    #@4d
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    goto :goto_3c

    #@51
    .line 246
    :cond_51
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@53
    if-eqz v8, :cond_92

    #@55
    .line 247
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@58
    move-result v8

    #@59
    if-eqz v8, :cond_62

    #@5b
    .line 248
    const-string v8, "LGIMS"

    #@5d
    const-string v9, "ar.exception is not null"

    #@5f
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 251
    :cond_62
    iget v8, p1, Landroid/os/Message;->what:I

    #@64
    const/16 v9, 0x65

    #@66
    if-eq v8, v9, :cond_6e

    #@68
    iget v8, p1, Landroid/os/Message;->what:I

    #@6a
    const/16 v9, 0x66

    #@6c
    if-ne v8, v9, :cond_3c

    #@6e
    .line 253
    :cond_6e
    iget-object v4, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@70
    check-cast v4, Ljava/lang/Integer;

    #@72
    .line 256
    .local v4, fileId:Ljava/lang/Integer;
    const/4 v8, 0x3

    #@73
    :try_start_73
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@76
    move-result v9

    #@77
    const/4 v10, 0x0

    #@78
    const-string v11, ""

    #@7a
    invoke-interface {v6, v8, v9, v10, v11}, Lcom/android/internal/telephony/ISysMonitor;->onPhoneStateChanged(IIILjava/lang/String;)V
    :try_end_7d
    .catch Landroid/os/RemoteException; {:try_start_73 .. :try_end_7d} :catch_7e

    #@7d
    goto :goto_3c

    #@7e
    .line 257
    :catch_7e
    move-exception v2

    #@7f
    .line 258
    .local v2, e:Landroid/os/RemoteException;
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@82
    move-result v8

    #@83
    if-eqz v8, :cond_8c

    #@85
    .line 259
    const-string v8, "LGIMS"

    #@87
    const-string v9, "Unexpected remote exception"

    #@89
    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8c
    .line 261
    :cond_8c
    iget-object v8, p0, Lcom/android/internal/telephony/IMSPhone$MyHandler;->this$0:Lcom/android/internal/telephony/IMSPhone;

    #@8e
    invoke-virtual {v8}, Lcom/android/internal/telephony/IMSPhone;->handleRemoteException()V

    #@91
    goto :goto_3c

    #@92
    .line 267
    .end local v2           #e:Landroid/os/RemoteException;
    .end local v4           #fileId:Ljava/lang/Integer;
    :cond_92
    iget v8, p1, Landroid/os/Message;->what:I

    #@94
    packed-switch v8, :pswitch_data_1e0

    #@97
    .line 397
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@9a
    move-result v8

    #@9b
    if-eqz v8, :cond_3c

    #@9d
    .line 398
    const-string v8, "LGIMS"

    #@9f
    new-instance v9, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v10, "Unknown event ("

    #@a6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v9

    #@aa
    iget v10, p1, Landroid/os/Message;->what:I

    #@ac
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@af
    move-result-object v9

    #@b0
    const-string v10, ")"

    #@b2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v9

    #@b6
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v9

    #@ba
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bd
    goto/16 :goto_3c

    #@bf
    .line 300
    :pswitch_bf
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@c2
    move-result v8

    #@c3
    if-eqz v8, :cond_3c

    #@c5
    .line 301
    const-string v8, "LGIMS"

    #@c7
    const-string v9, "RIL_RESPONSE received"

    #@c9
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cc
    goto/16 :goto_3c

    #@ce
    .line 309
    :pswitch_ce
    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@d0
    if-nez v8, :cond_105

    #@d2
    .line 310
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@d5
    move-result v8

    #@d6
    if-eqz v8, :cond_df

    #@d8
    .line 311
    const-string v8, "LGIMS"

    #@da
    const-string v9, "ar.result is null"

    #@dc
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    .line 314
    :cond_df
    iget-object v4, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@e1
    check-cast v4, Ljava/lang/Integer;

    #@e3
    .line 317
    .restart local v4       #fileId:Ljava/lang/Integer;
    const/4 v8, 0x3

    #@e4
    :try_start_e4
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@e7
    move-result v9

    #@e8
    const/4 v10, 0x0

    #@e9
    const-string v11, ""

    #@eb
    invoke-interface {v6, v8, v9, v10, v11}, Lcom/android/internal/telephony/ISysMonitor;->onPhoneStateChanged(IIILjava/lang/String;)V
    :try_end_ee
    .catch Landroid/os/RemoteException; {:try_start_e4 .. :try_end_ee} :catch_f0

    #@ee
    goto/16 :goto_3c

    #@f0
    .line 318
    :catch_f0
    move-exception v2

    #@f1
    .line 319
    .restart local v2       #e:Landroid/os/RemoteException;
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@f4
    move-result v8

    #@f5
    if-eqz v8, :cond_fe

    #@f7
    .line 320
    const-string v8, "LGIMS"

    #@f9
    const-string v9, "Unexpected remote exception"

    #@fb
    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@fe
    .line 322
    :cond_fe
    iget-object v8, p0, Lcom/android/internal/telephony/IMSPhone$MyHandler;->this$0:Lcom/android/internal/telephony/IMSPhone;

    #@100
    invoke-virtual {v8}, Lcom/android/internal/telephony/IMSPhone;->handleRemoteException()V

    #@103
    goto/16 :goto_3c

    #@105
    .line 327
    .end local v2           #e:Landroid/os/RemoteException;
    .end local v4           #fileId:Ljava/lang/Integer;
    :cond_105
    iget-object v4, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@107
    check-cast v4, Ljava/lang/Integer;

    #@109
    .line 329
    .restart local v4       #fileId:Ljava/lang/Integer;
    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@10b
    check-cast v8, Ljava/util/ArrayList;

    #@10d
    move-object v3, v8

    #@10e
    check-cast v3, Ljava/util/ArrayList;

    #@110
    .line 330
    .local v3, efRecords:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    const-string v1, ""

    #@112
    .line 332
    .local v1, data:Ljava/lang/String;
    const/4 v5, 0x0

    #@113
    .local v5, i:I
    :goto_113
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@116
    move-result v8

    #@117
    if-ge v5, v8, :cond_14c

    #@119
    .line 333
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11c
    move-result-object v8

    #@11d
    check-cast v8, [B

    #@11f
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@122
    move-result-object v7

    #@123
    .line 335
    .local v7, tmp:Ljava/lang/String;
    if-eqz v7, :cond_136

    #@125
    .line 336
    new-instance v8, Ljava/lang/StringBuilder;

    #@127
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@12a
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v8

    #@12e
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v8

    #@132
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@135
    move-result-object v1

    #@136
    .line 339
    :cond_136
    new-instance v8, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v8

    #@13f
    const-string v9, "Z"

    #@141
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v8

    #@145
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@148
    move-result-object v1

    #@149
    .line 332
    add-int/lit8 v5, v5, 0x1

    #@14b
    goto :goto_113

    #@14c
    .line 343
    .end local v7           #tmp:Ljava/lang/String;
    :cond_14c
    const/4 v8, 0x3

    #@14d
    :try_start_14d
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@150
    move-result v9

    #@151
    const/4 v10, 0x0

    #@152
    invoke-interface {v6, v8, v9, v10, v1}, Lcom/android/internal/telephony/ISysMonitor;->onPhoneStateChanged(IIILjava/lang/String;)V
    :try_end_155
    .catch Landroid/os/RemoteException; {:try_start_14d .. :try_end_155} :catch_157

    #@155
    goto/16 :goto_3c

    #@157
    .line 344
    :catch_157
    move-exception v2

    #@158
    .line 345
    .restart local v2       #e:Landroid/os/RemoteException;
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@15b
    move-result v8

    #@15c
    if-eqz v8, :cond_165

    #@15e
    .line 346
    const-string v8, "LGIMS"

    #@160
    const-string v9, "Unexpected remote exception"

    #@162
    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@165
    .line 348
    :cond_165
    iget-object v8, p0, Lcom/android/internal/telephony/IMSPhone$MyHandler;->this$0:Lcom/android/internal/telephony/IMSPhone;

    #@167
    invoke-virtual {v8}, Lcom/android/internal/telephony/IMSPhone;->handleRemoteException()V

    #@16a
    goto/16 :goto_3c

    #@16c
    .line 355
    .end local v1           #data:Ljava/lang/String;
    .end local v2           #e:Landroid/os/RemoteException;
    .end local v3           #efRecords:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    .end local v4           #fileId:Ljava/lang/Integer;
    .end local v5           #i:I
    :pswitch_16c
    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@16e
    if-nez v8, :cond_1a3

    #@170
    .line 356
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@173
    move-result v8

    #@174
    if-eqz v8, :cond_17d

    #@176
    .line 357
    const-string v8, "LGIMS"

    #@178
    const-string v9, "ar.result is null"

    #@17a
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17d
    .line 360
    :cond_17d
    iget-object v4, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@17f
    check-cast v4, Ljava/lang/Integer;

    #@181
    .line 363
    .restart local v4       #fileId:Ljava/lang/Integer;
    const/4 v8, 0x3

    #@182
    :try_start_182
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@185
    move-result v9

    #@186
    const/4 v10, 0x0

    #@187
    const-string v11, ""

    #@189
    invoke-interface {v6, v8, v9, v10, v11}, Lcom/android/internal/telephony/ISysMonitor;->onPhoneStateChanged(IIILjava/lang/String;)V
    :try_end_18c
    .catch Landroid/os/RemoteException; {:try_start_182 .. :try_end_18c} :catch_18e

    #@18c
    goto/16 :goto_3c

    #@18e
    .line 364
    :catch_18e
    move-exception v2

    #@18f
    .line 365
    .restart local v2       #e:Landroid/os/RemoteException;
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@192
    move-result v8

    #@193
    if-eqz v8, :cond_19c

    #@195
    .line 366
    const-string v8, "LGIMS"

    #@197
    const-string v9, "Unexpected remote exception"

    #@199
    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@19c
    .line 368
    :cond_19c
    iget-object v8, p0, Lcom/android/internal/telephony/IMSPhone$MyHandler;->this$0:Lcom/android/internal/telephony/IMSPhone;

    #@19e
    invoke-virtual {v8}, Lcom/android/internal/telephony/IMSPhone;->handleRemoteException()V

    #@1a1
    goto/16 :goto_3c

    #@1a3
    .line 373
    .end local v2           #e:Landroid/os/RemoteException;
    .end local v4           #fileId:Ljava/lang/Integer;
    :cond_1a3
    iget-object v4, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@1a5
    check-cast v4, Ljava/lang/Integer;

    #@1a7
    .line 374
    .restart local v4       #fileId:Ljava/lang/Integer;
    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1a9
    check-cast v8, [B

    #@1ab
    check-cast v8, [B

    #@1ad
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@1b0
    move-result-object v1

    #@1b1
    .line 377
    .restart local v1       #data:Ljava/lang/String;
    const/4 v8, 0x3

    #@1b2
    :try_start_1b2
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@1b5
    move-result v9

    #@1b6
    const/4 v10, 0x0

    #@1b7
    invoke-interface {v6, v8, v9, v10, v1}, Lcom/android/internal/telephony/ISysMonitor;->onPhoneStateChanged(IIILjava/lang/String;)V
    :try_end_1ba
    .catch Landroid/os/RemoteException; {:try_start_1b2 .. :try_end_1ba} :catch_1bc

    #@1ba
    goto/16 :goto_3c

    #@1bc
    .line 378
    :catch_1bc
    move-exception v2

    #@1bd
    .line 379
    .restart local v2       #e:Landroid/os/RemoteException;
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@1c0
    move-result v8

    #@1c1
    if-eqz v8, :cond_1ca

    #@1c3
    .line 380
    const-string v8, "LGIMS"

    #@1c5
    const-string v9, "Unexpected remote exception"

    #@1c7
    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1ca
    .line 382
    :cond_1ca
    iget-object v8, p0, Lcom/android/internal/telephony/IMSPhone$MyHandler;->this$0:Lcom/android/internal/telephony/IMSPhone;

    #@1cc
    invoke-virtual {v8}, Lcom/android/internal/telephony/IMSPhone;->handleRemoteException()V

    #@1cf
    goto/16 :goto_3c

    #@1d1
    .line 390
    .end local v1           #data:Ljava/lang/String;
    .end local v2           #e:Landroid/os/RemoteException;
    .end local v4           #fileId:Ljava/lang/Integer;
    :pswitch_1d1
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->access$100()Z

    #@1d4
    move-result v8

    #@1d5
    if-eqz v8, :cond_3c

    #@1d7
    .line 391
    const-string v8, "LGIMS"

    #@1d9
    const-string v9, "RIL_IMS_STAUS_SET_DONE_FOR_DAN received"

    #@1db
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1de
    goto/16 :goto_3c

    #@1e0
    .line 267
    :pswitch_data_1e0
    .packed-switch 0x64
        :pswitch_bf
        :pswitch_ce
        :pswitch_16c
        :pswitch_1d1
    .end packed-switch
.end method
