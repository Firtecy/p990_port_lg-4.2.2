.class public interface abstract Lcom/android/internal/telephony/IccPhonebookProvider$Contract;
.super Ljava/lang/Object;
.source "IccPhonebookProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccPhonebookProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Contract"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Group;,
        Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry;,
        Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice;,
        Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal;
    }
.end annotation


# static fields
.field public static final ANR_SEPARATOR:Ljava/lang/String; = "~"

.field public static final AUTHORITY:Ljava/lang/String; = "icc-phonebook"

.field public static final EMAIL_SEPARATOR:Ljava/lang/String; = "~"

.field public static final ENTRY_PROJECTION:[Ljava/lang/String; = null

.field public static final FEATURE_DEFAULT:I = 0x0

.field public static final FEATURE_EMPTY:I = 0x0

.field public static final FEATURE_MASK_RESERVE_RESERVE_FIRST_FREE_SIM_INDEX:I = 0x100

.field public static final FEATURE_MASK_SLOT_COUNT:I = 0xff

.field public static final GROUP_PROJECTION:[Ljava/lang/String; = null

.field public static final INFO_PROJECTION:[Ljava/lang/String; = null

.field public static final VALUE_ANR_TYPE_1:I = 0x1

.field public static final VALUE_ANR_TYPE_2:I = 0x2

.field public static final VALUE_ANR_TYPE_3:I = 0x3

.field public static final VALUE_ANR_TYPE_UNKNOWN:I = 0x0

.field public static final VALUE_EMAIL_TYPE_1:I = 0x1

.field public static final VALUE_EMAIL_TYPE_2:I = 0x2

.field public static final VALUE_EMAIL_TYPE_3:I = 0x3

.field public static final VALUE_EMAIL_TYPE_UNKNOWN:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 77
    const/16 v0, 0x10

    #@7
    new-array v0, v0, [Ljava/lang/String;

    #@9
    const-string v1, "info1"

    #@b
    aput-object v1, v0, v3

    #@d
    const-string v1, "info2"

    #@f
    aput-object v1, v0, v4

    #@11
    const-string v1, "info"

    #@13
    aput-object v1, v0, v5

    #@15
    const-string v1, "info4"

    #@17
    aput-object v1, v0, v6

    #@19
    const-string v1, "info5"

    #@1b
    aput-object v1, v0, v7

    #@1d
    const/4 v1, 0x5

    #@1e
    const-string v2, "info6"

    #@20
    aput-object v2, v0, v1

    #@22
    const/4 v1, 0x6

    #@23
    const-string v2, "info7"

    #@25
    aput-object v2, v0, v1

    #@27
    const/4 v1, 0x7

    #@28
    const-string v2, "info8"

    #@2a
    aput-object v2, v0, v1

    #@2c
    const/16 v1, 0x8

    #@2e
    const-string v2, "info9"

    #@30
    aput-object v2, v0, v1

    #@32
    const/16 v1, 0x9

    #@34
    const-string v2, "info10"

    #@36
    aput-object v2, v0, v1

    #@38
    const/16 v1, 0xa

    #@3a
    const-string v2, "info11"

    #@3c
    aput-object v2, v0, v1

    #@3e
    const/16 v1, 0xb

    #@40
    const-string v2, "info12"

    #@42
    aput-object v2, v0, v1

    #@44
    const/16 v1, 0xc

    #@46
    const-string v2, "info13"

    #@48
    aput-object v2, v0, v1

    #@4a
    const/16 v1, 0xd

    #@4c
    const-string v2, "info14"

    #@4e
    aput-object v2, v0, v1

    #@50
    const/16 v1, 0xe

    #@52
    const-string v2, "info15"

    #@54
    aput-object v2, v0, v1

    #@56
    const/16 v1, 0xf

    #@58
    const-string v2, "info16"

    #@5a
    aput-object v2, v0, v1

    #@5c
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@5e
    .line 163
    const/4 v0, 0x6

    #@5f
    new-array v0, v0, [Ljava/lang/String;

    #@61
    const-string v1, "index"

    #@63
    aput-object v1, v0, v3

    #@65
    const-string v1, "tag"

    #@67
    aput-object v1, v0, v4

    #@69
    const-string v1, "number"

    #@6b
    aput-object v1, v0, v5

    #@6d
    const-string v1, "anrs"

    #@6f
    aput-object v1, v0, v6

    #@71
    const-string v1, "emails"

    #@73
    aput-object v1, v0, v7

    #@75
    const/4 v1, 0x5

    #@76
    const-string v2, "groupIndex"

    #@78
    aput-object v2, v0, v1

    #@7a
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@7c
    .line 185
    new-array v0, v5, [Ljava/lang/String;

    #@7e
    const-string v1, "index"

    #@80
    aput-object v1, v0, v3

    #@82
    const-string v1, "name"

    #@84
    aput-object v1, v0, v4

    #@86
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->GROUP_PROJECTION:[Ljava/lang/String;

    #@88
    return-void
.end method
