.class public abstract Lcom/android/internal/telephony/BaseCommands;
.super Ljava/lang/Object;
.source "BaseCommands.java"

# interfaces
.implements Lcom/android/internal/telephony/CommandsInterface;


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "RILB"


# instance fields
.field private OldSendingProfile:[Ljava/lang/String;

.field protected lgeCipheringIndRegistrants:Landroid/os/RegistrantList;

.field protected lgeunsolRegistrants:Landroid/os/RegistrantList;

.field protected mAvailRegistrants:Landroid/os/RegistrantList;

.field protected mCallStateRegistrants:Landroid/os/RegistrantList;

.field protected mCallWaitingInfoRegistrants:Landroid/os/RegistrantList;

.field protected mCatCallSetUpRegistrant:Landroid/os/Registrant;

.field protected mCatCcAlphaRegistrant:Landroid/os/Registrant;

.field protected mCatEventRegistrant:Landroid/os/Registrant;

.field protected mCatProCmdRegistrant:Landroid/os/Registrant;

.field protected mCatSessionEndRegistrant:Landroid/os/Registrant;

.field protected mCdmaFwdBurstDtmfRegistrants:Landroid/os/RegistrantList;

.field protected mCdmaFwdContDtmfStartRegistrants:Landroid/os/RegistrantList;

.field protected mCdmaFwdContDtmfStopRegistrants:Landroid/os/RegistrantList;

.field protected mCdmaInfoRecRegistrantsKddi:Landroid/os/RegistrantList;

.field protected mCdmaPrlChangedRegistrants:Landroid/os/RegistrantList;

.field protected mCdmaSidChangedRegistrants:Landroid/os/RegistrantList;

.field protected mCdmaSmsRegistrant:Landroid/os/Registrant;

.field protected mCdmaSubscription:I

.field protected mCdmaSubscriptionChangedRegistrants:Landroid/os/RegistrantList;

.field protected mContext:Landroid/content/Context;

.field protected mDataNetworkStateRegistrants:Landroid/os/RegistrantList;

.field protected mDataQosIndicationRegistrants:Landroid/os/RegistrantList;

.field protected mDisplayInfoRegistrants:Landroid/os/RegistrantList;

.field protected mEmergencyCallbackModeRegistrant:Landroid/os/Registrant;

.field protected mEndQueryAvailableNetworkRegistrants:Landroid/os/RegistrantList;

.field protected mExitEmergencyCallbackModeRegistrants:Landroid/os/RegistrantList;

.field protected mGsmBroadcastSmsRegistrant:Landroid/os/Registrant;

.field protected mGsmSmsRegistrant:Landroid/os/Registrant;

.field protected mGstkSetupOtaCallRegistrant:Landroid/os/Registrant;

.field protected mHDRRoamingIndicatorRegistrants:Landroid/os/RegistrantList;

.field protected mHdrLockRegistrants:Landroid/os/RegistrantList;

.field public mIMSEnable:Z

.field protected mIccAppRefreshRegistrants:Landroid/os/RegistrantList;

.field protected mIccRefreshRegistrants:Landroid/os/RegistrantList;

.field protected mIccSmsFullRegistrant:Landroid/os/Registrant;

.field protected mIccStatusChangedRegistrants:Landroid/os/RegistrantList;

.field protected mImsNetworkStateChangedRegistrants:Landroid/os/RegistrantList;

.field protected mLGfeature:Lcom/android/internal/telephony/LGfeature;

.field protected mLineControlInfoRegistrants:Landroid/os/RegistrantList;

.field protected mLockStateRegistrants:Landroid/os/RegistrantList;

.field protected mLteEhrpdForcedChangedRegistrants:Landroid/os/RegistrantList;

.field protected mLteEmmRejectRegistrants:Landroid/os/RegistrantList;

.field protected mLteLockRegistrants:Landroid/os/RegistrantList;

.field protected mLteMODataBarredRegistrants:Landroid/os/RegistrantList;

.field protected mNITZTimeRegistrant:Landroid/os/Registrant;

.field protected mNetworkErrorDispRegistrants:Landroid/os/RegistrantList;

.field protected mNotAvailRegistrants:Landroid/os/RegistrantList;

.field protected mNumberInfoRegistrants:Landroid/os/RegistrantList;

.field protected mOffOrNotAvailRegistrants:Landroid/os/RegistrantList;

.field protected mOnRegistrants:Landroid/os/RegistrantList;

.field protected mOtaProvisionRegistrants:Landroid/os/RegistrantList;

.field protected mOtaSessionFailRegistrants:Landroid/os/RegistrantList;

.field protected mOtaSessionSuccessRegistrants:Landroid/os/RegistrantList;

.field protected mPacketPagingRegistrant:Landroid/os/RegistrantList;

.field protected mPbReady:Z

.field protected mPbReadyRegistrant:Landroid/os/Registrant;

.field protected mPhoneType:I

.field protected mPreferredNetworkType:I

.field protected mRadioStateChangedRegistrants:Landroid/os/RegistrantList;

.field protected mRedirNumInfoRegistrants:Landroid/os/RegistrantList;

.field protected mResendIncallMuteRegistrants:Landroid/os/RegistrantList;

.field protected mRestrictedStateRegistrant:Landroid/os/Registrant;

.field protected mRilConnectedRegistrants:Landroid/os/RegistrantList;

.field protected mRilVersion:I

.field protected mRingRegistrant:Landroid/os/Registrant;

.field protected mRingbackToneRegistrants:Landroid/os/RegistrantList;

.field protected mSSRegistrant:Landroid/os/Registrant;

.field protected mSignalInfoRegistrants:Landroid/os/RegistrantList;

.field protected mSignalStrengthRegistrant:Landroid/os/Registrant;

.field protected mSmsDanRegistrant:Landroid/os/Registrant;

.field protected mSmsOnSimRegistrant:Landroid/os/Registrant;

.field protected mSmsStatusRegistrant:Landroid/os/Registrant;

.field protected mSsnRegistrant:Landroid/os/Registrant;

.field protected mStartQueryAvailableNetworkRegistrants:Landroid/os/RegistrantList;

.field protected mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

.field protected mStateMonitor:Ljava/lang/Object;

.field protected mSubscriptionStatusRegistrants:Landroid/os/RegistrantList;

.field protected mT53AudCntrlInfoRegistrants:Landroid/os/RegistrantList;

.field protected mT53ClirInfoRegistrants:Landroid/os/RegistrantList;

.field protected mT53ReleaseInfoRegistrantsKddi:Landroid/os/RegistrantList;

.field protected mTetheredModeStateRegistrants:Landroid/os/RegistrantList;

.field protected mUSSDRegistrant:Landroid/os/Registrant;

.field protected mUnsolOemHookRawRegistrant:Landroid/os/Registrant;

.field protected mVoiceNetworkStateRegistrants:Landroid/os/RegistrantList;

.field protected mVoicePrivacyOffRegistrants:Landroid/os/RegistrantList;

.field protected mVoicePrivacyOnRegistrants:Landroid/os/RegistrantList;

.field protected mVoiceRadioTechChangedRegistrants:Landroid/os/RegistrantList;

.field protected mWcdmaAcceptReceivedRegistrants:Landroid/os/RegistrantList;

.field protected mWcdmaNetChangedRegistrants:Landroid/os/RegistrantList;

.field protected mWcdmaNetToKoreaChangedRegistrants:Landroid/os/RegistrantList;

.field protected mWcdmaRejectReceivedRegistrants:Landroid/os/RegistrantList;

.field protected mWmsReadyRegistrants:Landroid/os/RegistrantList;

.field protected mWpbxStateChangedRegistrants:Landroid/os/RegistrantList;

.field public myDebugger:Lcom/android/internal/telephony/MMdebuger;

.field public pdnBlockInfo:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x6

    #@1
    const/4 v2, 0x0

    #@2
    .line 267
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 61
    sget-object v0, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_UNAVAILABLE:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@7
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@9
    .line 62
    new-instance v0, Ljava/lang/Object;

    #@b
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@e
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@10
    .line 64
    new-instance v0, Landroid/os/RegistrantList;

    #@12
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@15
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRadioStateChangedRegistrants:Landroid/os/RegistrantList;

    #@17
    .line 65
    new-instance v0, Landroid/os/RegistrantList;

    #@19
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1c
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mOnRegistrants:Landroid/os/RegistrantList;

    #@1e
    .line 66
    new-instance v0, Landroid/os/RegistrantList;

    #@20
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@23
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mAvailRegistrants:Landroid/os/RegistrantList;

    #@25
    .line 67
    new-instance v0, Landroid/os/RegistrantList;

    #@27
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@2a
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mOffOrNotAvailRegistrants:Landroid/os/RegistrantList;

    #@2c
    .line 68
    new-instance v0, Landroid/os/RegistrantList;

    #@2e
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@31
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mNotAvailRegistrants:Landroid/os/RegistrantList;

    #@33
    .line 69
    new-instance v0, Landroid/os/RegistrantList;

    #@35
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@38
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCallStateRegistrants:Landroid/os/RegistrantList;

    #@3a
    .line 70
    new-instance v0, Landroid/os/RegistrantList;

    #@3c
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@3f
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mVoiceNetworkStateRegistrants:Landroid/os/RegistrantList;

    #@41
    .line 71
    new-instance v0, Landroid/os/RegistrantList;

    #@43
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@46
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mDataNetworkStateRegistrants:Landroid/os/RegistrantList;

    #@48
    .line 72
    new-instance v0, Landroid/os/RegistrantList;

    #@4a
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@4d
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mVoiceRadioTechChangedRegistrants:Landroid/os/RegistrantList;

    #@4f
    .line 73
    new-instance v0, Landroid/os/RegistrantList;

    #@51
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@54
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mImsNetworkStateChangedRegistrants:Landroid/os/RegistrantList;

    #@56
    .line 74
    new-instance v0, Landroid/os/RegistrantList;

    #@58
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@5b
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mIccStatusChangedRegistrants:Landroid/os/RegistrantList;

    #@5d
    .line 75
    new-instance v0, Landroid/os/RegistrantList;

    #@5f
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@62
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mVoicePrivacyOnRegistrants:Landroid/os/RegistrantList;

    #@64
    .line 76
    new-instance v0, Landroid/os/RegistrantList;

    #@66
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@69
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mVoicePrivacyOffRegistrants:Landroid/os/RegistrantList;

    #@6b
    .line 78
    new-instance v0, Landroid/os/RegistrantList;

    #@6d
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@70
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mOtaProvisionRegistrants:Landroid/os/RegistrantList;

    #@72
    .line 79
    new-instance v0, Landroid/os/RegistrantList;

    #@74
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@77
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCallWaitingInfoRegistrants:Landroid/os/RegistrantList;

    #@79
    .line 80
    new-instance v0, Landroid/os/RegistrantList;

    #@7b
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@7e
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mDisplayInfoRegistrants:Landroid/os/RegistrantList;

    #@80
    .line 81
    new-instance v0, Landroid/os/RegistrantList;

    #@82
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@85
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSignalInfoRegistrants:Landroid/os/RegistrantList;

    #@87
    .line 82
    new-instance v0, Landroid/os/RegistrantList;

    #@89
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@8c
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mNumberInfoRegistrants:Landroid/os/RegistrantList;

    #@8e
    .line 83
    new-instance v0, Landroid/os/RegistrantList;

    #@90
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@93
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRedirNumInfoRegistrants:Landroid/os/RegistrantList;

    #@95
    .line 84
    new-instance v0, Landroid/os/RegistrantList;

    #@97
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@9a
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLineControlInfoRegistrants:Landroid/os/RegistrantList;

    #@9c
    .line 85
    new-instance v0, Landroid/os/RegistrantList;

    #@9e
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@a1
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mT53ClirInfoRegistrants:Landroid/os/RegistrantList;

    #@a3
    .line 86
    new-instance v0, Landroid/os/RegistrantList;

    #@a5
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@a8
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mT53AudCntrlInfoRegistrants:Landroid/os/RegistrantList;

    #@aa
    .line 87
    new-instance v0, Landroid/os/RegistrantList;

    #@ac
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@af
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRingbackToneRegistrants:Landroid/os/RegistrantList;

    #@b1
    .line 88
    new-instance v0, Landroid/os/RegistrantList;

    #@b3
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@b6
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mResendIncallMuteRegistrants:Landroid/os/RegistrantList;

    #@b8
    .line 89
    new-instance v0, Landroid/os/RegistrantList;

    #@ba
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@bd
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaSubscriptionChangedRegistrants:Landroid/os/RegistrantList;

    #@bf
    .line 90
    new-instance v0, Landroid/os/RegistrantList;

    #@c1
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@c4
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaPrlChangedRegistrants:Landroid/os/RegistrantList;

    #@c6
    .line 91
    new-instance v0, Landroid/os/RegistrantList;

    #@c8
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@cb
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mExitEmergencyCallbackModeRegistrants:Landroid/os/RegistrantList;

    #@cd
    .line 92
    new-instance v0, Landroid/os/RegistrantList;

    #@cf
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@d2
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRilConnectedRegistrants:Landroid/os/RegistrantList;

    #@d4
    .line 93
    new-instance v0, Landroid/os/RegistrantList;

    #@d6
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@d9
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mIccRefreshRegistrants:Landroid/os/RegistrantList;

    #@db
    .line 95
    new-instance v0, Landroid/os/RegistrantList;

    #@dd
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@e0
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mIccAppRefreshRegistrants:Landroid/os/RegistrantList;

    #@e2
    .line 97
    new-instance v0, Landroid/os/RegistrantList;

    #@e4
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@e7
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mTetheredModeStateRegistrants:Landroid/os/RegistrantList;

    #@e9
    .line 99
    new-instance v0, Landroid/os/RegistrantList;

    #@eb
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@ee
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->lgeunsolRegistrants:Landroid/os/RegistrantList;

    #@f0
    .line 101
    new-instance v0, Landroid/os/RegistrantList;

    #@f2
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@f5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSubscriptionStatusRegistrants:Landroid/os/RegistrantList;

    #@f7
    .line 102
    new-instance v0, Landroid/os/RegistrantList;

    #@f9
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@fc
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaFwdBurstDtmfRegistrants:Landroid/os/RegistrantList;

    #@fe
    .line 103
    new-instance v0, Landroid/os/RegistrantList;

    #@100
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@103
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaFwdContDtmfStartRegistrants:Landroid/os/RegistrantList;

    #@105
    .line 104
    new-instance v0, Landroid/os/RegistrantList;

    #@107
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@10a
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaFwdContDtmfStopRegistrants:Landroid/os/RegistrantList;

    #@10c
    .line 105
    new-instance v0, Landroid/os/RegistrantList;

    #@10e
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@111
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWmsReadyRegistrants:Landroid/os/RegistrantList;

    #@113
    .line 108
    new-instance v0, Landroid/os/RegistrantList;

    #@115
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@118
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mOtaSessionSuccessRegistrants:Landroid/os/RegistrantList;

    #@11a
    .line 109
    new-instance v0, Landroid/os/RegistrantList;

    #@11c
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@11f
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mOtaSessionFailRegistrants:Landroid/os/RegistrantList;

    #@121
    .line 110
    new-instance v0, Landroid/os/RegistrantList;

    #@123
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@126
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaSidChangedRegistrants:Landroid/os/RegistrantList;

    #@128
    .line 111
    new-instance v0, Landroid/os/RegistrantList;

    #@12a
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@12d
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWpbxStateChangedRegistrants:Landroid/os/RegistrantList;

    #@12f
    .line 112
    new-instance v0, Landroid/os/RegistrantList;

    #@131
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@134
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLockStateRegistrants:Landroid/os/RegistrantList;

    #@136
    .line 113
    new-instance v0, Landroid/os/RegistrantList;

    #@138
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@13b
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mNetworkErrorDispRegistrants:Landroid/os/RegistrantList;

    #@13d
    .line 117
    new-instance v0, Landroid/os/RegistrantList;

    #@13f
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@142
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mHdrLockRegistrants:Landroid/os/RegistrantList;

    #@144
    .line 118
    new-instance v0, Landroid/os/RegistrantList;

    #@146
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@149
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLteLockRegistrants:Landroid/os/RegistrantList;

    #@14b
    .line 119
    new-instance v0, Landroid/os/RegistrantList;

    #@14d
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@150
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLteEmmRejectRegistrants:Landroid/os/RegistrantList;

    #@152
    .line 120
    new-instance v0, Landroid/os/RegistrantList;

    #@154
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@157
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLteMODataBarredRegistrants:Landroid/os/RegistrantList;

    #@159
    .line 125
    new-instance v0, Landroid/os/RegistrantList;

    #@15b
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@15e
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaNetChangedRegistrants:Landroid/os/RegistrantList;

    #@160
    .line 126
    new-instance v0, Landroid/os/RegistrantList;

    #@162
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@165
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaNetToKoreaChangedRegistrants:Landroid/os/RegistrantList;

    #@167
    .line 131
    new-instance v0, Landroid/os/RegistrantList;

    #@169
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@16c
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaRejectReceivedRegistrants:Landroid/os/RegistrantList;

    #@16e
    .line 132
    new-instance v0, Landroid/os/RegistrantList;

    #@170
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@173
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaAcceptReceivedRegistrants:Landroid/os/RegistrantList;

    #@175
    .line 136
    new-instance v0, Landroid/os/RegistrantList;

    #@177
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@17a
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mDataQosIndicationRegistrants:Landroid/os/RegistrantList;

    #@17c
    .line 141
    new-instance v0, Landroid/os/RegistrantList;

    #@17e
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@181
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mStartQueryAvailableNetworkRegistrants:Landroid/os/RegistrantList;

    #@183
    .line 142
    new-instance v0, Landroid/os/RegistrantList;

    #@185
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@188
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mEndQueryAvailableNetworkRegistrants:Landroid/os/RegistrantList;

    #@18a
    .line 146
    new-instance v0, Landroid/os/RegistrantList;

    #@18c
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@18f
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaInfoRecRegistrantsKddi:Landroid/os/RegistrantList;

    #@191
    .line 149
    new-instance v0, Landroid/os/RegistrantList;

    #@193
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@196
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mT53ReleaseInfoRegistrantsKddi:Landroid/os/RegistrantList;

    #@198
    .line 185
    new-instance v0, Landroid/os/RegistrantList;

    #@19a
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@19d
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLteEhrpdForcedChangedRegistrants:Landroid/os/RegistrantList;

    #@19f
    .line 189
    new-instance v0, Landroid/os/RegistrantList;

    #@1a1
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1a4
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mHDRRoamingIndicatorRegistrants:Landroid/os/RegistrantList;

    #@1a6
    .line 193
    iput-boolean v2, p0, Lcom/android/internal/telephony/BaseCommands;->mPbReady:Z

    #@1a8
    .line 197
    new-instance v0, Landroid/os/RegistrantList;

    #@1aa
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1ad
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mPacketPagingRegistrant:Landroid/os/RegistrantList;

    #@1af
    .line 209
    const/4 v0, -0x1

    #@1b0
    iput v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRilVersion:I

    #@1b2
    .line 216
    iput-boolean v2, p0, Lcom/android/internal/telephony/BaseCommands;->mIMSEnable:Z

    #@1b4
    .line 221
    new-array v0, v3, [I

    #@1b6
    fill-array-data v0, :array_1ec

    #@1b9
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->pdnBlockInfo:[I

    #@1bb
    .line 230
    const/4 v0, 0x7

    #@1bc
    new-array v0, v0, [Ljava/lang/String;

    #@1be
    const-string v1, "never"

    #@1c0
    aput-object v1, v0, v2

    #@1c2
    const/4 v1, 0x1

    #@1c3
    const-string v2, "never"

    #@1c5
    aput-object v2, v0, v1

    #@1c7
    const/4 v1, 0x2

    #@1c8
    const-string v2, "never"

    #@1ca
    aput-object v2, v0, v1

    #@1cc
    const/4 v1, 0x3

    #@1cd
    const-string v2, "never"

    #@1cf
    aput-object v2, v0, v1

    #@1d1
    const/4 v1, 0x4

    #@1d2
    const-string v2, "never"

    #@1d4
    aput-object v2, v0, v1

    #@1d6
    const/4 v1, 0x5

    #@1d7
    const-string v2, "never"

    #@1d9
    aput-object v2, v0, v1

    #@1db
    const-string v1, "never"

    #@1dd
    aput-object v1, v0, v3

    #@1df
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->OldSendingProfile:[Ljava/lang/String;

    #@1e1
    .line 1184
    new-instance v0, Landroid/os/RegistrantList;

    #@1e3
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1e6
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->lgeCipheringIndRegistrants:Landroid/os/RegistrantList;

    #@1e8
    .line 268
    iput-object p1, p0, Lcom/android/internal/telephony/BaseCommands;->mContext:Landroid/content/Context;

    #@1ea
    .line 269
    return-void

    #@1eb
    .line 221
    nop

    #@1ec
    :array_1ec
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method


# virtual methods
.method public DrxDisableTest(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1294
    return-void
.end method

.method public PBMDeleteRecord(IILandroid/os/Message;)V
    .registers 4
    .parameter "EFdevice"
    .parameter "rec_index"
    .parameter "response"

    #@0
    .prologue
    .line 1221
    return-void
.end method

.method public PBMGetInfo(ILandroid/os/Message;)V
    .registers 3
    .parameter "EFdevice"
    .parameter "response"

    #@0
    .prologue
    .line 1224
    return-void
.end method

.method public PBMGetInitState(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1227
    return-void
.end method

.method public PBMReadRecord(IILandroid/os/Message;)V
    .registers 4
    .parameter "EFdevice"
    .parameter "rec_index"
    .parameter "response"

    #@0
    .prologue
    .line 1215
    return-void
.end method

.method public PBMWriteRecord(Lcom/android/internal/telephony/uicc/LGE_PBM_Records;Landroid/os/Message;)V
    .registers 3
    .parameter "RecordData"
    .parameter "response"

    #@0
    .prologue
    .line 1218
    return-void
.end method

.method public PrxDisableTest(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1293
    return-void
.end method

.method public PrxDrxEnableTest(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1295
    return-void
.end method

.method public dial(Ljava/lang/String;IZLandroid/os/Message;)V
    .registers 5
    .parameter "address"
    .parameter "clirMode"
    .parameter "subaddress"
    .parameter "result"

    #@0
    .prologue
    .line 1299
    return-void
.end method

.method public enableAutoDCDisconnect(I)V
    .registers 2
    .parameter "timeOut"

    #@0
    .prologue
    .line 1202
    return-void
.end method

.method public getApnBlockInfo()[I
    .registers 2

    #@0
    .prologue
    .line 263
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->pdnBlockInfo:[I

    #@2
    return-object v0
.end method

.method public getEmodeInfoPage(I)Ljava/lang/String;
    .registers 3
    .parameter "EngIndex"

    #@0
    .prologue
    .line 1280
    const-string v0, "Emode Info"

    #@2
    return-object v0
.end method

.method public getEngineeringModeInfo(ILandroid/os/Message;)I
    .registers 4
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 1197
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getLGfeatures()Lcom/android/internal/telephony/LGfeature;
    .registers 2

    #@0
    .prologue
    .line 282
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@2
    return-object v0
.end method

.method public getLteEmmErrorCode(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1289
    return-void
.end method

.method public getLteOnCdmaMode()I
    .registers 2

    #@0
    .prologue
    .line 1165
    invoke-static {}, Landroid/telephony/TelephonyManager;->getLteOnCdmaModeStatic()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getMipErrorCode(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1285
    return-void
.end method

.method public getModemIntegerItem(ILandroid/os/Message;)V
    .registers 3
    .parameter "item_index"
    .parameter "response"

    #@0
    .prologue
    .line 1262
    return-void
.end method

.method public getModemStringItem(ILandroid/os/Message;)V
    .registers 3
    .parameter "item_index"
    .parameter "response"

    #@0
    .prologue
    .line 1264
    return-void
.end method

.method public getMyDebugger()Lcom/android/internal/telephony/MMdebuger;
    .registers 2

    #@0
    .prologue
    .line 1273
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->myDebugger:Lcom/android/internal/telephony/MMdebuger;

    #@2
    return-object v0
.end method

.method public getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "isset"
    .parameter "pdn_id"
    .parameter "setvalue"

    #@0
    .prologue
    .line 247
    const-string v0, "RILB"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getOrSetIMSEnable : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 248
    if-eqz p1, :cond_2d

    #@1e
    .line 249
    if-nez p3, :cond_28

    #@20
    .line 250
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->OldSendingProfile:[Ljava/lang/String;

    #@22
    const-string v1, "none"

    #@24
    aput-object v1, v0, p2

    #@26
    .line 254
    :goto_26
    const/4 v0, 0x0

    #@27
    .line 256
    :goto_27
    return-object v0

    #@28
    .line 252
    :cond_28
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->OldSendingProfile:[Ljava/lang/String;

    #@2a
    aput-object p3, v0, p2

    #@2c
    goto :goto_26

    #@2d
    .line 256
    :cond_2d
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->OldSendingProfile:[Ljava/lang/String;

    #@2f
    aget-object v0, v0, p2

    #@31
    goto :goto_27
.end method

.method public getOrSetIMSEnable(ZZ)Z
    .registers 6
    .parameter "isset"
    .parameter "setvalue"

    #@0
    .prologue
    .line 234
    const-string v0, "RILB"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getOrSetIMSEnable : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 235
    if-eqz p1, :cond_22

    #@1e
    .line 236
    iput-boolean p2, p0, Lcom/android/internal/telephony/BaseCommands;->mIMSEnable:Z

    #@20
    .line 237
    const/4 v0, 0x1

    #@21
    .line 239
    :goto_21
    return v0

    #@22
    :cond_22
    iget-boolean v0, p0, Lcom/android/internal/telephony/BaseCommands;->mIMSEnable:Z

    #@24
    goto :goto_21
.end method

.method public getPendinginfo()Z
    .registers 2

    #@0
    .prologue
    .line 288
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;
    .registers 2

    #@0
    .prologue
    .line 274
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@2
    return-object v0
.end method

.method public getSearchStatus(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 1180
    return-void
.end method

.method public getUsimAuthentication(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILandroid/os/Message;)V
    .registers 7
    .parameter "aid"
    .parameter "rand_s"
    .parameter "rand_length"
    .parameter "autn_s"
    .parameter "autn_length"
    .parameter "response"

    #@0
    .prologue
    .line 1230
    return-void
.end method

.method protected onRadioAvailable()V
    .registers 1

    #@0
    .prologue
    .line 1158
    return-void
.end method

.method public registerFoT53ClirlInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 737
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 738
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mT53ClirInfoRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 739
    return-void
.end method

.method public registerForAvailable(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 10
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 335
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 337
    .local v0, r:Landroid/os/Registrant;
    iget-object v2, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@7
    monitor-enter v2

    #@8
    .line 338
    :try_start_8
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mAvailRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 340
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@f
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isAvailable()Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_20

    #@15
    .line 341
    new-instance v1, Landroid/os/AsyncResult;

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x0

    #@19
    const/4 v5, 0x0

    #@1a
    invoke-direct {v1, v3, v4, v5}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@1d
    invoke-virtual {v0, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@20
    .line 343
    :cond_20
    monitor-exit v2

    #@21
    .line 344
    return-void

    #@22
    .line 343
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public registerForCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 388
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 390
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mCallStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 391
    return-void
.end method

.method public registerForCallWaitingInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 675
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 676
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mCallWaitingInfoRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 677
    return-void
.end method

.method public registerForCdmaFwdBurstDtmf(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 858
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaFwdBurstDtmfRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 859
    return-void
.end method

.method public registerForCdmaFwdContDtmfStart(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 866
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaFwdContDtmfStartRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 867
    return-void
.end method

.method public registerForCdmaFwdContDtmfStop(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 874
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaFwdContDtmfStopRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 875
    return-void
.end method

.method public registerForCdmaInfoRecReceivedKddi(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1082
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1083
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaInfoRecRegistrantsKddi:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 1084
    return-void
.end method

.method public registerForCdmaOtaProvision(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 701
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 702
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mOtaProvisionRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 703
    return-void
.end method

.method public registerForCdmaPrlChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 795
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 796
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaPrlChangedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 797
    return-void
.end method

.method public registerForCdmaSidChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 920
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 921
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaSidChangedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 922
    return-void
.end method

.method public registerForCdmaSubscriptionChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 784
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 785
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaSubscriptionChangedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 786
    return-void
.end method

.method public registerForDataNetworkStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 408
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 410
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mDataNetworkStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 411
    return-void
.end method

.method public registerForDataQosChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 8
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 837
    const-string v1, "RILB"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "registerForDataQosChanged h="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, " w="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 838
    new-instance v0, Landroid/os/Registrant;

    #@24
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@27
    .line 839
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mDataQosIndicationRegistrants:Landroid/os/RegistrantList;

    #@29
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@2c
    .line 840
    return-void
.end method

.method public registerForDisplayInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 666
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 667
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mDisplayInfoRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 668
    return-void
.end method

.method public registerForEndQueryAvailableNetwork(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1073
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mEndQueryAvailableNetworkRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1074
    return-void
.end method

.method public registerForExitEmergencyCallbackMode(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 806
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 807
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mExitEmergencyCallbackModeRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 808
    return-void
.end method

.method public registerForHDRRoamingIndicator(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1054
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1055
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mHDRRoamingIndicatorRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 1056
    return-void
.end method

.method public registerForHdrLock(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 958
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 959
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mHdrLockRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 960
    return-void
.end method

.method public registerForIccAppRefresh(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 612
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 613
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mIccAppRefreshRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 614
    return-void
.end method

.method public registerForIccRefresh(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 568
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 569
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mIccRefreshRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 570
    return-void
.end method

.method public registerForIccStatusChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 427
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 428
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mIccStatusChangedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 429
    return-void
.end method

.method public registerForImsNetworkStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 308
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 309
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mImsNetworkStateChangedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 310
    return-void
.end method

.method public registerForInCallVoicePrivacyOff(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 649
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 650
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mVoicePrivacyOffRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 651
    return-void
.end method

.method public registerForInCallVoicePrivacyOn(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 640
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 641
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mVoicePrivacyOnRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 642
    return-void
.end method

.method public registerForLineControlInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 728
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 729
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mLineControlInfoRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 730
    return-void
.end method

.method public registerForLockStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 938
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 939
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mLockStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 940
    return-void
.end method

.method public registerForLteEhrpdForcedChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1041
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1042
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mLteEhrpdForcedChangedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 1043
    return-void
.end method

.method public registerForLteEmmReject(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 976
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 977
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mLteEmmRejectRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 978
    return-void
.end method

.method public registerForLteLock(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 967
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 968
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mLteLockRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 969
    return-void
.end method

.method public registerForNetworkErrorDisp(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 947
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 948
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mNetworkErrorDispRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 949
    return-void
.end method

.method public registerForNotAvailable(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 10
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 353
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 355
    .local v0, r:Landroid/os/Registrant;
    iget-object v2, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@7
    monitor-enter v2

    #@8
    .line 356
    :try_start_8
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mNotAvailRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 358
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@f
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isAvailable()Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_20

    #@15
    .line 359
    new-instance v1, Landroid/os/AsyncResult;

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x0

    #@19
    const/4 v5, 0x0

    #@1a
    invoke-direct {v1, v3, v4, v5}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@1d
    invoke-virtual {v0, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@20
    .line 361
    :cond_20
    monitor-exit v2

    #@21
    .line 362
    return-void

    #@22
    .line 361
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public registerForNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 710
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 711
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mNumberInfoRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 712
    return-void
.end method

.method public registerForOffOrNotAvailable(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 10
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 371
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 373
    .local v0, r:Landroid/os/Registrant;
    iget-object v2, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@7
    monitor-enter v2

    #@8
    .line 374
    :try_start_8
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mOffOrNotAvailRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 376
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@f
    sget-object v3, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_OFF:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@11
    if-eq v1, v3, :cond_1b

    #@13
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@15
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isAvailable()Z

    #@18
    move-result v1

    #@19
    if-nez v1, :cond_26

    #@1b
    .line 377
    :cond_1b
    new-instance v1, Landroid/os/AsyncResult;

    #@1d
    const/4 v3, 0x0

    #@1e
    const/4 v4, 0x0

    #@1f
    const/4 v5, 0x0

    #@20
    invoke-direct {v1, v3, v4, v5}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@23
    invoke-virtual {v0, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@26
    .line 379
    :cond_26
    monitor-exit v2

    #@27
    .line 380
    return-void

    #@28
    .line 379
    :catchall_28
    move-exception v1

    #@29
    monitor-exit v2
    :try_end_2a
    .catchall {:try_start_8 .. :try_end_2a} :catchall_28

    #@2a
    throw v1
.end method

.method public registerForOn(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 10
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 317
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 319
    .local v0, r:Landroid/os/Registrant;
    iget-object v2, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@7
    monitor-enter v2

    #@8
    .line 320
    :try_start_8
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mOnRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 322
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@f
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_20

    #@15
    .line 323
    new-instance v1, Landroid/os/AsyncResult;

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x0

    #@19
    const/4 v5, 0x0

    #@1a
    invoke-direct {v1, v3, v4, v5}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@1d
    invoke-virtual {v0, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@20
    .line 325
    :cond_20
    monitor-exit v2

    #@21
    .line 326
    return-void

    #@22
    .line 325
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public registerForOtaSessionFail(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 911
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 912
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mOtaSessionFailRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 913
    return-void
.end method

.method public registerForOtaSessionSuccess(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 902
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 903
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mOtaSessionSuccessRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 904
    return-void
.end method

.method public registerForPbReady(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 8
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1027
    new-instance v0, Landroid/os/Registrant;

    #@3
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mPbReadyRegistrant:Landroid/os/Registrant;

    #@8
    .line 1028
    iget-boolean v0, p0, Lcom/android/internal/telephony/BaseCommands;->mPbReady:Z

    #@a
    if-eqz v0, :cond_30

    #@c
    .line 1029
    const-string v0, "RILB"

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "mPbReady : "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget-boolean v2, p0, Lcom/android/internal/telephony/BaseCommands;->mPbReady:Z

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 1030
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mPbReadyRegistrant:Landroid/os/Registrant;

    #@28
    new-instance v1, Landroid/os/AsyncResult;

    #@2a
    invoke-direct {v1, v3, v3, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@2d
    invoke-virtual {v0, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@30
    .line 1032
    :cond_30
    return-void
.end method

.method public registerForRadioStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 7
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 292
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 294
    .local v0, r:Landroid/os/Registrant;
    iget-object v2, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@7
    monitor-enter v2

    #@8
    .line 295
    :try_start_8
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mRadioStateChangedRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 296
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@10
    .line 297
    monitor-exit v2

    #@11
    .line 298
    return-void

    #@12
    .line 297
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_8 .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method public registerForRedirectedNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 719
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 720
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mRedirNumInfoRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 721
    return-void
.end method

.method public registerForResendIncallMute(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 774
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 775
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mResendIncallMuteRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 776
    return-void
.end method

.method public registerForRilConnected(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 9
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 820
    const-string v1, "RILB"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "registerForRilConnected h="

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    const-string v3, " w="

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 821
    new-instance v0, Landroid/os/Registrant;

    #@25
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@28
    .line 822
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mRilConnectedRegistrants:Landroid/os/RegistrantList;

    #@2a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@2d
    .line 823
    iget v1, p0, Lcom/android/internal/telephony/BaseCommands;->mRilVersion:I

    #@2f
    const/4 v2, -0x1

    #@30
    if-eq v1, v2, :cond_5b

    #@32
    .line 824
    const-string v1, "RILB"

    #@34
    new-instance v2, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v3, "Notifying: ril connected mRilVersion="

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    iget v3, p0, Lcom/android/internal/telephony/BaseCommands;->mRilVersion:I

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 825
    new-instance v1, Landroid/os/AsyncResult;

    #@4e
    new-instance v2, Ljava/lang/Integer;

    #@50
    iget v3, p0, Lcom/android/internal/telephony/BaseCommands;->mRilVersion:I

    #@52
    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    #@55
    invoke-direct {v1, v4, v2, v4}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@58
    invoke-virtual {v0, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@5b
    .line 827
    :cond_5b
    return-void
.end method

.method public registerForRingbackTone(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 755
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 756
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mRingbackToneRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 757
    return-void
.end method

.method public registerForSignalInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 684
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 685
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mSignalInfoRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 686
    return-void
.end method

.method public registerForStartQueryAvailableNetwork(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1067
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mStartQueryAvailableNetworkRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1068
    return-void
.end method

.method public registerForSubscriptionStatusChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 849
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 850
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mSubscriptionStatusRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 851
    return-void
.end method

.method public registerForT53AudioControlInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 746
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 747
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mT53AudCntrlInfoRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 748
    return-void
.end method

.method public registerForT53ReleaseInfoKddi(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1093
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1094
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mT53ReleaseInfoRegistrantsKddi:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 1095
    return-void
.end method

.method public registerForTetheredModeStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 765
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 766
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mTetheredModeStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 767
    return-void
.end method

.method public registerForVoiceNetworkStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 398
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 400
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mVoiceNetworkStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 401
    return-void
.end method

.method public registerForVoiceRadioTechChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 418
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 419
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mVoiceRadioTechChangedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 420
    return-void
.end method

.method public registerForWcdmaAcceptReceived(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1016
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1017
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaAcceptReceivedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 1018
    return-void
.end method

.method public registerForWcdmaNetChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 988
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 989
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaNetChangedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 990
    return-void
.end method

.method public registerForWcdmaNetToKoreaChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 996
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 997
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaNetToKoreaChangedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 998
    return-void
.end method

.method public registerForWcdmaRejectReceived(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1007
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1008
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaRejectReceivedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 1009
    return-void
.end method

.method public registerForWmsReadyEvent(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 882
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWmsReadyRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 883
    return-void
.end method

.method public registerForWpbxStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 929
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 930
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mWpbxStateChangedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 931
    return-void
.end method

.method public registerLGECipheringInd(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1186
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1187
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->lgeCipheringIndRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 1188
    return-void
.end method

.method public registerLGEUnsol(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1252
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1253
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->lgeunsolRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 1254
    return-void
.end method

.method public registorForPacketPaging(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 440
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 441
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mPacketPagingRegistrant:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 442
    return-void
.end method

.method public sendNfcRebootDone(ILandroid/os/Message;)V
    .registers 3
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1239
    return-void
.end method

.method public sendSmsMore(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "smscPDU"
    .parameter "pdu"
    .parameter "response"

    #@0
    .prologue
    .line 897
    return-void
.end method

.method public setCurrentPreferredNetworkType()V
    .registers 1

    #@0
    .prologue
    .line 894
    return-void
.end method

.method public setEmergencyCallbackMode(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 576
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mEmergencyCallbackModeRegistrant:Landroid/os/Registrant;

    #@7
    .line 577
    return-void
.end method

.method public setImsStatusForDan(ILandroid/os/Message;)V
    .registers 3
    .parameter "ims_status"
    .parameter "result"

    #@0
    .prologue
    .line 1303
    return-void
.end method

.method public setModemIntegerItem(IILandroid/os/Message;)V
    .registers 4
    .parameter "item_index"
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1261
    return-void
.end method

.method public setModemStringItem(ILjava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "item_index"
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1263
    return-void
.end method

.method public setNetworkSelectionModeManual(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "operatorNumeric"
    .parameter "operatorRat"
    .parameter "response"

    #@0
    .prologue
    .line 1172
    return-void
.end method

.method public setOnCallRing(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 587
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRingRegistrant:Landroid/os/Registrant;

    #@7
    .line 588
    return-void
.end method

.method public setOnCatCallSetUp(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 552
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatCallSetUpRegistrant:Landroid/os/Registrant;

    #@7
    .line 553
    return-void
.end method

.method public setOnCatCcAlphaNotify(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 603
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatCcAlphaRegistrant:Landroid/os/Registrant;

    #@7
    .line 604
    return-void
.end method

.method public setOnCatEvent(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 544
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatEventRegistrant:Landroid/os/Registrant;

    #@7
    .line 545
    return-void
.end method

.method public setOnCatProactiveCmd(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 536
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatProCmdRegistrant:Landroid/os/Registrant;

    #@7
    .line 537
    return-void
.end method

.method public setOnCatSessionEnd(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 528
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatSessionEndRegistrant:Landroid/os/Registrant;

    #@7
    .line 529
    return-void
.end method

.method public setOnDanStatus(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 487
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSmsDanRegistrant:Landroid/os/Registrant;

    #@7
    .line 488
    return-void
.end method

.method public setOnGstkSetupOtaspCall(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1206
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mGstkSetupOtaCallRegistrant:Landroid/os/Registrant;

    #@7
    .line 1207
    return-void
.end method

.method public setOnIccAppRefresh(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 4
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 627
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/telephony/BaseCommands;->registerForIccAppRefresh(Landroid/os/Handler;ILjava/lang/Object;)V

    #@3
    .line 628
    return-void
.end method

.method public setOnIccRefresh(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 4
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 572
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/telephony/BaseCommands;->registerForIccRefresh(Landroid/os/Handler;ILjava/lang/Object;)V

    #@3
    .line 573
    return-void
.end method

.method public setOnIccSmsFull(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 560
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mIccSmsFullRegistrant:Landroid/os/Registrant;

    #@7
    .line 561
    return-void
.end method

.method public setOnNITZTime(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 504
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mNITZTimeRegistrant:Landroid/os/Registrant;

    #@7
    .line 505
    return-void
.end method

.method public setOnNewCdmaSms(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 454
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaSmsRegistrant:Landroid/os/Registrant;

    #@7
    .line 455
    return-void
.end method

.method public setOnNewGsmBroadcastSms(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 462
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mGsmBroadcastSmsRegistrant:Landroid/os/Registrant;

    #@7
    .line 463
    return-void
.end method

.method public setOnNewGsmSms(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 436
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mGsmSmsRegistrant:Landroid/os/Registrant;

    #@7
    .line 437
    return-void
.end method

.method public setOnRestrictedStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 658
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRestrictedStateRegistrant:Landroid/os/Registrant;

    #@7
    .line 659
    return-void
.end method

.method public setOnSS(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 595
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSSRegistrant:Landroid/os/Registrant;

    #@7
    .line 596
    return-void
.end method

.method public setOnSignalStrengthUpdate(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 496
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSignalStrengthRegistrant:Landroid/os/Registrant;

    #@7
    .line 497
    return-void
.end method

.method public setOnSmsOnSim(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 470
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSmsOnSimRegistrant:Landroid/os/Registrant;

    #@7
    .line 471
    return-void
.end method

.method public setOnSmsStatus(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 478
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSmsStatusRegistrant:Landroid/os/Registrant;

    #@7
    .line 479
    return-void
.end method

.method public setOnSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 520
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSsnRegistrant:Landroid/os/Registrant;

    #@7
    .line 521
    return-void
.end method

.method public setOnUSSD(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 512
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mUSSDRegistrant:Landroid/os/Registrant;

    #@7
    .line 513
    return-void
.end method

.method public setOnUnsolOemHookRaw(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 689
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mUnsolOemHookRawRegistrant:Landroid/os/Registrant;

    #@7
    .line 690
    return-void
.end method

.method public setPreviousNetworkSelectionModeManual(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "operatorNumeric"
    .parameter "operatorRat"
    .parameter "result"

    #@0
    .prologue
    .line 1176
    return-void
.end method

.method protected setRadioState(Lcom/android/internal/telephony/CommandsInterface$RadioState;)V
    .registers 6
    .parameter "newState"

    #@0
    .prologue
    .line 1116
    iget-object v2, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1122
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@5
    .line 1123
    .local v0, oldState:Lcom/android/internal/telephony/CommandsInterface$RadioState;
    iput-object p1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@7
    .line 1125
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@9
    if-ne v0, v1, :cond_d

    #@b
    .line 1127
    monitor-exit v2

    #@c
    .line 1155
    :goto_c
    return-void

    #@d
    .line 1130
    :cond_d
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mRadioStateChangedRegistrants:Landroid/os/RegistrantList;

    #@f
    invoke-virtual {v1}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@12
    .line 1132
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@14
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isAvailable()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_2f

    #@1a
    invoke-virtual {v0}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isAvailable()Z

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_2f

    #@20
    .line 1133
    const-string v1, "RILB"

    #@22
    const-string v3, "Notifying: radio available"

    #@24
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 1134
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mAvailRegistrants:Landroid/os/RegistrantList;

    #@29
    invoke-virtual {v1}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@2c
    .line 1135
    invoke-virtual {p0}, Lcom/android/internal/telephony/BaseCommands;->onRadioAvailable()V

    #@2f
    .line 1138
    :cond_2f
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@31
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isAvailable()Z

    #@34
    move-result v1

    #@35
    if-nez v1, :cond_49

    #@37
    invoke-virtual {v0}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isAvailable()Z

    #@3a
    move-result v1

    #@3b
    if-eqz v1, :cond_49

    #@3d
    .line 1139
    const-string v1, "RILB"

    #@3f
    const-string v3, "Notifying: radio not available"

    #@41
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 1140
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mNotAvailRegistrants:Landroid/os/RegistrantList;

    #@46
    invoke-virtual {v1}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@49
    .line 1143
    :cond_49
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@4b
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@4e
    move-result v1

    #@4f
    if-eqz v1, :cond_63

    #@51
    invoke-virtual {v0}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@54
    move-result v1

    #@55
    if-nez v1, :cond_63

    #@57
    .line 1144
    const-string v1, "RILB"

    #@59
    const-string v3, "Notifying: Radio On"

    #@5b
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 1145
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mOnRegistrants:Landroid/os/RegistrantList;

    #@60
    invoke-virtual {v1}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@63
    .line 1148
    :cond_63
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@65
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@68
    move-result v1

    #@69
    if-eqz v1, :cond_73

    #@6b
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@6d
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isAvailable()Z

    #@70
    move-result v1

    #@71
    if-nez v1, :cond_8b

    #@73
    :cond_73
    invoke-virtual {v0}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@76
    move-result v1

    #@77
    if-eqz v1, :cond_8b

    #@79
    invoke-virtual {v0}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isAvailable()Z

    #@7c
    move-result v1

    #@7d
    if-eqz v1, :cond_8b

    #@7f
    .line 1151
    const-string v1, "RILB"

    #@81
    const-string v3, "Notifying: radio off or not available"

    #@83
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 1152
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mOffOrNotAvailRegistrants:Landroid/os/RegistrantList;

    #@88
    invoke-virtual {v1}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@8b
    .line 1154
    :cond_8b
    monitor-exit v2

    #@8c
    goto :goto_c

    #@8d
    .end local v0           #oldState:Lcom/android/internal/telephony/CommandsInterface$RadioState;
    :catchall_8d
    move-exception v1

    #@8e
    monitor-exit v2
    :try_end_8f
    .catchall {:try_start_3 .. :try_end_8f} :catchall_8d

    #@8f
    throw v1
.end method

.method public smartCardGetATR(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 1236
    return-void
.end method

.method public smartCardTransmit([BLandroid/os/Message;)V
    .registers 3
    .parameter "command"
    .parameter "response"

    #@0
    .prologue
    .line 1233
    return-void
.end method

.method public testingEmergencyCall()V
    .registers 1

    #@0
    .prologue
    .line 1169
    return-void
.end method

.method public uknightEventSet([BLandroid/os/Message;)V
    .registers 3
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1243
    return-void
.end method

.method public uknightGetData(ILandroid/os/Message;)V
    .registers 3
    .parameter "buf_num"
    .parameter "response"

    #@0
    .prologue
    .line 1246
    return-void
.end method

.method public uknightLogSet([BLandroid/os/Message;)V
    .registers 3
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1242
    return-void
.end method

.method public uknightMemCheck(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 1247
    return-void
.end method

.method public uknightMemSet(ILandroid/os/Message;)V
    .registers 3
    .parameter "percent"
    .parameter "response"

    #@0
    .prologue
    .line 1245
    return-void
.end method

.method public uknightStateChangeSet(ILandroid/os/Message;)V
    .registers 3
    .parameter "event"
    .parameter "response"

    #@0
    .prologue
    .line 1244
    return-void
.end method

.method public unSetOnCallRing(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 591
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRingRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 592
    return-void
.end method

.method public unSetOnCatCallSetUp(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 556
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatCallSetUpRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 557
    return-void
.end method

.method public unSetOnCatCcAlphaNotify(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 607
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatCcAlphaRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 608
    return-void
.end method

.method public unSetOnCatEvent(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 548
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatEventRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 549
    return-void
.end method

.method public unSetOnCatProactiveCmd(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 540
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatProCmdRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 541
    return-void
.end method

.method public unSetOnCatSessionEnd(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 532
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatSessionEndRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 533
    return-void
.end method

.method public unSetOnDanStatus(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 491
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSmsDanRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 492
    return-void
.end method

.method public unSetOnGstkSetupOtaspCall(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1209
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mGstkSetupOtaCallRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 1210
    return-void
.end method

.method public unSetOnIccSmsFull(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 564
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mIccSmsFullRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 565
    return-void
.end method

.method public unSetOnNITZTime(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 508
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mNITZTimeRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 509
    return-void
.end method

.method public unSetOnNewCdmaSms(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 458
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaSmsRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 459
    return-void
.end method

.method public unSetOnNewGsmBroadcastSms(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 466
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mGsmBroadcastSmsRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 467
    return-void
.end method

.method public unSetOnNewGsmSms(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 450
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mGsmSmsRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 451
    return-void
.end method

.method public unSetOnRestrictedStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 662
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRestrictedStateRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 663
    return-void
.end method

.method public unSetOnSS(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 599
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSSRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 600
    return-void
.end method

.method public unSetOnSignalStrengthUpdate(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 500
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSignalStrengthRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 501
    return-void
.end method

.method public unSetOnSmsOnSim(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 474
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSmsOnSimRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 475
    return-void
.end method

.method public unSetOnSmsStatus(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 482
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSmsStatusRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 483
    return-void
.end method

.method public unSetOnSuppServiceNotification(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSsnRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 525
    return-void
.end method

.method public unSetOnUSSD(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 516
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mUSSDRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 517
    return-void
.end method

.method public unSetOnUnsolOemHookRaw(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 693
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mUnsolOemHookRawRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 694
    return-void
.end method

.method public unregisterForAvailable(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 347
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 348
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mAvailRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 349
    monitor-exit v1

    #@9
    .line 350
    return-void

    #@a
    .line 349
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unregisterForCallStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 394
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCallStateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 395
    return-void
.end method

.method public unregisterForCallWaitingInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 680
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCallWaitingInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 681
    return-void
.end method

.method public unregisterForCdmaFwdBurstDtmf(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 862
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaFwdBurstDtmfRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 863
    return-void
.end method

.method public unregisterForCdmaFwdContDtmfStart(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 870
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaFwdContDtmfStartRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 871
    return-void
.end method

.method public unregisterForCdmaFwdContDtmfStop(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 878
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaFwdContDtmfStopRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 879
    return-void
.end method

.method public unregisterForCdmaInfoRecReceivedKddi(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1087
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaInfoRecRegistrantsKddi:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1088
    return-void
.end method

.method public unregisterForCdmaOtaProvision(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 706
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mOtaProvisionRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 707
    return-void
.end method

.method public unregisterForCdmaPrlChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 801
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaPrlChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 802
    return-void
.end method

.method public unregisterForCdmaSidChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 925
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaSidChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 926
    return-void
.end method

.method public unregisterForCdmaSubscriptionChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 790
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCdmaSubscriptionChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 791
    return-void
.end method

.method public unregisterForDataNetworkStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 414
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mDataNetworkStateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 415
    return-void
.end method

.method public unregisterForDataQosChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 844
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mDataQosIndicationRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 845
    return-void
.end method

.method public unregisterForDisplayInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 671
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mDisplayInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 672
    return-void
.end method

.method public unregisterForEndQueryAvailableNetwork(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1076
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mEndQueryAvailableNetworkRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1077
    return-void
.end method

.method public unregisterForExitEmergencyCallbackMode(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 812
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mExitEmergencyCallbackModeRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 813
    return-void
.end method

.method public unregisterForHDRRoamingIndicator(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1060
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mHDRRoamingIndicatorRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1061
    return-void
.end method

.method public unregisterForHdrLock(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 963
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mHdrLockRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 964
    return-void
.end method

.method public unregisterForIccAppRefresh(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 617
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mIccAppRefreshRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 618
    return-void
.end method

.method public unregisterForIccRefresh(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 580
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mIccRefreshRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 581
    return-void
.end method

.method public unregisterForIccStatusChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 432
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mIccStatusChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 433
    return-void
.end method

.method public unregisterForImsNetworkStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 313
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mImsNetworkStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 314
    return-void
.end method

.method public unregisterForInCallVoicePrivacyOff(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 654
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mVoicePrivacyOffRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 655
    return-void
.end method

.method public unregisterForInCallVoicePrivacyOn(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 645
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mVoicePrivacyOnRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 646
    return-void
.end method

.method public unregisterForLineControlInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 733
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLineControlInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 734
    return-void
.end method

.method public unregisterForLockStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 943
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLockStateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 944
    return-void
.end method

.method public unregisterForLteEhrpdForcedChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1047
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLteEhrpdForcedChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1048
    return-void
.end method

.method public unregisterForLteEmmReject(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 981
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLteEmmRejectRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 982
    return-void
.end method

.method public unregisterForLteLock(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 972
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mLteLockRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 973
    return-void
.end method

.method public unregisterForNetworkErrorDisp(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 952
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mNetworkErrorDispRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 953
    return-void
.end method

.method public unregisterForNotAvailable(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 365
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 366
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mNotAvailRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 367
    monitor-exit v1

    #@9
    .line 368
    return-void

    #@a
    .line 367
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unregisterForNumberInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 715
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mNumberInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 716
    return-void
.end method

.method public unregisterForOffOrNotAvailable(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 382
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 383
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mOffOrNotAvailRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 384
    monitor-exit v1

    #@9
    .line 385
    return-void

    #@a
    .line 384
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unregisterForOn(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 328
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 329
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mOnRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 330
    monitor-exit v1

    #@9
    .line 331
    return-void

    #@a
    .line 330
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unregisterForOtaSessionFail(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 916
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mOtaSessionFailRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 917
    return-void
.end method

.method public unregisterForOtaSessionSuccess(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 907
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mOtaSessionSuccessRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 908
    return-void
.end method

.method public unregisterForPbReady(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1035
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mPbReadyRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 1036
    return-void
.end method

.method public unregisterForRadioStateChanged(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 301
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mStateMonitor:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 302
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRadioStateChangedRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 303
    monitor-exit v1

    #@9
    .line 304
    return-void

    #@a
    .line 303
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unregisterForRedirectedNumberInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 724
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRedirNumInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 725
    return-void
.end method

.method public unregisterForResendIncallMute(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 779
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mResendIncallMuteRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 780
    return-void
.end method

.method public unregisterForRilConnected(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 831
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRilConnectedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 832
    return-void
.end method

.method public unregisterForRingbackTone(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 760
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mRingbackToneRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 761
    return-void
.end method

.method public unregisterForSignalInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 697
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSignalInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 698
    return-void
.end method

.method public unregisterForStartQueryAvailableNetwork(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1070
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mStartQueryAvailableNetworkRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1071
    return-void
.end method

.method public unregisterForSubscriptionStatusChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 854
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mSubscriptionStatusRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 855
    return-void
.end method

.method public unregisterForT53AudioControlInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 751
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mT53AudCntrlInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 752
    return-void
.end method

.method public unregisterForT53ClirInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 742
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mT53ClirInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 743
    return-void
.end method

.method public unregisterForT53ReleaseInfoKddi(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1098
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mT53ReleaseInfoRegistrantsKddi:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1099
    return-void
.end method

.method public unregisterForTetheredModeStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 770
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mTetheredModeStateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 771
    return-void
.end method

.method public unregisterForVoiceNetworkStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 404
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mVoiceNetworkStateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 405
    return-void
.end method

.method public unregisterForVoiceRadioTechChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mVoiceRadioTechChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 424
    return-void
.end method

.method public unregisterForWcdmaAcceptReceived(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1021
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaAcceptReceivedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1022
    return-void
.end method

.method public unregisterForWcdmaNetChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 992
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaNetChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 993
    return-void
.end method

.method public unregisterForWcdmaNetToKoreaChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1000
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaNetToKoreaChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1001
    return-void
.end method

.method public unregisterForWcdmaRejectReceived(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1012
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWcdmaRejectReceivedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1013
    return-void
.end method

.method public unregisterForWmsReadyEvent(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 886
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWmsReadyRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 887
    return-void
.end method

.method public unregisterForWpbxStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 934
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mWpbxStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 935
    return-void
.end method

.method public unregisterLGECipheringInd(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1191
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->lgeCipheringIndRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1192
    return-void
.end method

.method public unregisterLGEUnsol(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1257
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->lgeunsolRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1258
    return-void
.end method

.method public unregistorForPacketPaging(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 445
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mPacketPagingRegistrant:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 446
    return-void
.end method

.method public unsetOnIccAppRefresh(Landroid/os/Handler;)V
    .registers 2
    .parameter "h"

    #@0
    .prologue
    .line 634
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/BaseCommands;->unregisterForIccAppRefresh(Landroid/os/Handler;)V

    #@3
    .line 635
    return-void
.end method

.method public unsetOnIccRefresh(Landroid/os/Handler;)V
    .registers 2
    .parameter "h"

    #@0
    .prologue
    .line 583
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/BaseCommands;->unregisterForIccRefresh(Landroid/os/Handler;)V

    #@3
    .line 584
    return-void
.end method
