.class Lcom/android/internal/telephony/cat/DTTZResponseData;
.super Lcom/android/internal/telephony/cat/ResponseData;
.source "ResponseData.java"


# instance fields
.field private calendar:Ljava/util/Calendar;


# direct methods
.method public constructor <init>(Ljava/util/Calendar;)V
    .registers 2
    .parameter "cal"

    #@0
    .prologue
    .line 206
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/ResponseData;-><init>()V

    #@3
    .line 207
    iput-object p1, p0, Lcom/android/internal/telephony/cat/DTTZResponseData;->calendar:Ljava/util/Calendar;

    #@5
    .line 208
    return-void
.end method

.method private byteToBCD(I)B
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 260
    if-gez p1, :cond_24

    #@2
    const/16 v0, 0x63

    #@4
    if-le p1, v0, :cond_24

    #@6
    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v1, "Err: byteToBCD conversion Value is "

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, " Value has to be between 0 and 99"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@22
    .line 263
    const/4 v0, 0x0

    #@23
    .line 266
    :goto_23
    return v0

    #@24
    :cond_24
    div-int/lit8 v0, p1, 0xa

    #@26
    rem-int/lit8 v1, p1, 0xa

    #@28
    shl-int/lit8 v1, v1, 0x4

    #@2a
    or-int/2addr v0, v1

    #@2b
    int-to-byte v0, v0

    #@2c
    goto :goto_23
.end method

.method private getTZOffSetByte(J)B
    .registers 11
    .parameter "offSetVal"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 270
    const-wide/16 v6, 0x0

    #@3
    cmp-long v6, p1, v6

    #@5
    if-gez v6, :cond_1e

    #@7
    move v2, v5

    #@8
    .line 278
    .local v2, isNegative:Z
    :goto_8
    const-wide/32 v6, 0xdbba0

    #@b
    div-long v3, p1, v6

    #@d
    .line 279
    .local v3, tzOffset:J
    if-eqz v2, :cond_10

    #@f
    const/4 v5, -0x1

    #@10
    :cond_10
    int-to-long v5, v5

    #@11
    mul-long/2addr v3, v5

    #@12
    .line 280
    long-to-int v5, v3

    #@13
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/cat/DTTZResponseData;->byteToBCD(I)B

    #@16
    move-result v0

    #@17
    .line 282
    .local v0, bcdVal:B
    if-eqz v2, :cond_20

    #@19
    or-int/lit8 v5, v0, 0x8

    #@1b
    int-to-byte v0, v5

    #@1c
    move v1, v0

    #@1d
    .end local v0           #bcdVal:B
    .local v1, bcdVal:B
    :goto_1d
    return v0

    #@1e
    .line 270
    .end local v1           #bcdVal:B
    .end local v2           #isNegative:Z
    .end local v3           #tzOffset:J
    :cond_1e
    const/4 v2, 0x0

    #@1f
    goto :goto_8

    #@20
    .restart local v0       #bcdVal:B
    .restart local v2       #isNegative:Z
    .restart local v3       #tzOffset:J
    :cond_20
    move v1, v0

    #@21
    .line 282
    .end local v0           #bcdVal:B
    .restart local v1       #bcdVal:B
    goto :goto_1d
.end method


# virtual methods
.method public format(Ljava/io/ByteArrayOutputStream;)V
    .registers 16
    .parameter "buf"

    #@0
    .prologue
    const/4 v13, 0x5

    #@1
    const/4 v11, 0x2

    #@2
    const/4 v10, 0x1

    #@3
    const/4 v12, 0x7

    #@4
    .line 212
    if-nez p1, :cond_7

    #@6
    .line 257
    :cond_6
    return-void

    #@7
    .line 217
    :cond_7
    sget-object v9, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->PROVIDE_LOCAL_INFORMATION:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@9
    invoke-virtual {v9}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->value()I

    #@c
    move-result v9

    #@d
    or-int/lit16 v5, v9, 0x80

    #@f
    .line 218
    .local v5, tag:I
    invoke-virtual {p1, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@12
    .line 220
    const/16 v9, 0x8

    #@14
    new-array v2, v9, [B

    #@16
    .line 222
    .local v2, data:[B
    const/4 v9, 0x0

    #@17
    aput-byte v12, v2, v9

    #@19
    .line 224
    iget-object v9, p0, Lcom/android/internal/telephony/cat/DTTZResponseData;->calendar:Ljava/util/Calendar;

    #@1b
    if-nez v9, :cond_23

    #@1d
    .line 225
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@20
    move-result-object v9

    #@21
    iput-object v9, p0, Lcom/android/internal/telephony/cat/DTTZResponseData;->calendar:Ljava/util/Calendar;

    #@23
    .line 228
    :cond_23
    iget-object v9, p0, Lcom/android/internal/telephony/cat/DTTZResponseData;->calendar:Ljava/util/Calendar;

    #@25
    invoke-virtual {v9, v10}, Ljava/util/Calendar;->get(I)I

    #@28
    move-result v9

    #@29
    rem-int/lit8 v9, v9, 0x64

    #@2b
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/cat/DTTZResponseData;->byteToBCD(I)B

    #@2e
    move-result v9

    #@2f
    aput-byte v9, v2, v10

    #@31
    .line 231
    iget-object v9, p0, Lcom/android/internal/telephony/cat/DTTZResponseData;->calendar:Ljava/util/Calendar;

    #@33
    invoke-virtual {v9, v11}, Ljava/util/Calendar;->get(I)I

    #@36
    move-result v9

    #@37
    add-int/lit8 v9, v9, 0x1

    #@39
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/cat/DTTZResponseData;->byteToBCD(I)B

    #@3c
    move-result v9

    #@3d
    aput-byte v9, v2, v11

    #@3f
    .line 234
    const/4 v9, 0x3

    #@40
    iget-object v10, p0, Lcom/android/internal/telephony/cat/DTTZResponseData;->calendar:Ljava/util/Calendar;

    #@42
    invoke-virtual {v10, v13}, Ljava/util/Calendar;->get(I)I

    #@45
    move-result v10

    #@46
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/cat/DTTZResponseData;->byteToBCD(I)B

    #@49
    move-result v10

    #@4a
    aput-byte v10, v2, v9

    #@4c
    .line 237
    const/4 v9, 0x4

    #@4d
    iget-object v10, p0, Lcom/android/internal/telephony/cat/DTTZResponseData;->calendar:Ljava/util/Calendar;

    #@4f
    const/16 v11, 0xb

    #@51
    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    #@54
    move-result v10

    #@55
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/cat/DTTZResponseData;->byteToBCD(I)B

    #@58
    move-result v10

    #@59
    aput-byte v10, v2, v9

    #@5b
    .line 240
    iget-object v9, p0, Lcom/android/internal/telephony/cat/DTTZResponseData;->calendar:Ljava/util/Calendar;

    #@5d
    const/16 v10, 0xc

    #@5f
    invoke-virtual {v9, v10}, Ljava/util/Calendar;->get(I)I

    #@62
    move-result v9

    #@63
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/cat/DTTZResponseData;->byteToBCD(I)B

    #@66
    move-result v9

    #@67
    aput-byte v9, v2, v13

    #@69
    .line 243
    const/4 v9, 0x6

    #@6a
    iget-object v10, p0, Lcom/android/internal/telephony/cat/DTTZResponseData;->calendar:Ljava/util/Calendar;

    #@6c
    const/16 v11, 0xd

    #@6e
    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    #@71
    move-result v10

    #@72
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/cat/DTTZResponseData;->byteToBCD(I)B

    #@75
    move-result v10

    #@76
    aput-byte v10, v2, v9

    #@78
    .line 245
    const-string v9, "persist.sys.timezone"

    #@7a
    const-string v10, ""

    #@7c
    invoke-static {v9, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7f
    move-result-object v6

    #@80
    .line 246
    .local v6, tz:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@83
    move-result v9

    #@84
    if-eqz v9, :cond_96

    #@86
    .line 247
    const/4 v9, -0x1

    #@87
    aput-byte v9, v2, v12

    #@89
    .line 254
    :goto_89
    move-object v0, v2

    #@8a
    .local v0, arr$:[B
    array-length v4, v0

    #@8b
    .local v4, len$:I
    const/4 v3, 0x0

    #@8c
    .local v3, i$:I
    :goto_8c
    if-ge v3, v4, :cond_6

    #@8e
    aget-byte v1, v0, v3

    #@90
    .line 255
    .local v1, b:B
    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@93
    .line 254
    add-int/lit8 v3, v3, 0x1

    #@95
    goto :goto_8c

    #@96
    .line 249
    .end local v0           #arr$:[B
    .end local v1           #b:B
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_96
    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@99
    move-result-object v7

    #@9a
    .line 250
    .local v7, zone:Ljava/util/TimeZone;
    invoke-virtual {v7}, Ljava/util/TimeZone;->getRawOffset()I

    #@9d
    move-result v9

    #@9e
    invoke-virtual {v7}, Ljava/util/TimeZone;->getDSTSavings()I

    #@a1
    move-result v10

    #@a2
    add-int v8, v9, v10

    #@a4
    .line 251
    .local v8, zoneOffset:I
    int-to-long v9, v8

    #@a5
    invoke-direct {p0, v9, v10}, Lcom/android/internal/telephony/cat/DTTZResponseData;->getTZOffSetByte(J)B

    #@a8
    move-result v9

    #@a9
    aput-byte v9, v2, v12

    #@ab
    goto :goto_89
.end method
