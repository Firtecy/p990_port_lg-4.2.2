.class public Lcom/android/internal/telephony/MMdebuger;
.super Ljava/lang/Object;
.source "MMdebuger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/MMdebuger$conHistory;,
        Lcom/android/internal/telephony/MMdebuger$PDNLostHistory;,
        Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;
    }
.end annotation


# static fields
.field protected static final DEACTIVE_REQ:I = 0x3

.field protected static final DEACTIVE_RSP:I = 0x4

.field protected static final SETUP_REQ:I = 0x1

.field protected static final SETUP_RSP:I = 0x2


# instance fields
.field public LastConRadioTech:I

.field public LastPDNIPv:I

.field public LastPDNType:I

.field LteEmmErrorcode:I

.field c:Ljava/util/Calendar;

.field public currentAPNId:I

.field public currentRadioTech:I

.field lastfailreasion:[I

.field lastfailreasionOfInternetPND:[I

.field lastfailreasionOnEHRPD:[I

.field lastfailreasionOnLTE:[I

.field private mConHistory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/MMdebuger$conHistory;",
            ">;"
        }
    .end annotation
.end field

.field public mLcurDay:I

.field public mLcurHour:I

.field public mLcurMinute:I

.field public mLcurMonth:I

.field public mLcurSecond:I

.field public mLcurYear:I

.field public mMaxSize:I

.field private mPDNFailHistory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;",
            ">;"
        }
    .end annotation
.end field

.field private mPDNFailHistoryOnLTE:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;",
            ">;"
        }
    .end annotation
.end field

.field private mPDNFailHistoryonEHRPD:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;",
            ">;"
        }
    .end annotation
.end field

.field public mcurDay:I

.field public mcurHour:I

.field public mcurMinute:I

.field public mcurMonth:I

.field public mcurSecond:I

.field public mcurYear:I


# direct methods
.method protected constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x4

    #@1
    const/4 v2, 0x0

    #@2
    .line 148
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 41
    const/16 v1, 0x46

    #@7
    iput v1, p0, Lcom/android/internal/telephony/MMdebuger;->mMaxSize:I

    #@9
    .line 150
    new-instance v1, Ljava/util/ArrayList;

    #@b
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@e
    iput-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistory:Ljava/util/ArrayList;

    #@10
    .line 154
    new-instance v1, Ljava/util/ArrayList;

    #@12
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@15
    iput-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistoryOnLTE:Ljava/util/ArrayList;

    #@17
    .line 155
    new-instance v1, Ljava/util/ArrayList;

    #@19
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@1c
    iput-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistoryonEHRPD:Ljava/util/ArrayList;

    #@1e
    .line 158
    new-instance v1, Ljava/util/ArrayList;

    #@20
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@23
    iput-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@25
    .line 160
    new-array v1, v3, [I

    #@27
    iput-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasion:[I

    #@29
    .line 164
    new-array v1, v3, [I

    #@2b
    iput-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOfInternetPND:[I

    #@2d
    .line 165
    new-array v1, v3, [I

    #@2f
    iput-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnLTE:[I

    #@31
    .line 166
    new-array v1, v3, [I

    #@33
    iput-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnEHRPD:[I

    #@35
    .line 168
    iput v2, p0, Lcom/android/internal/telephony/MMdebuger;->LteEmmErrorcode:I

    #@37
    .line 170
    const/4 v0, 0x0

    #@38
    .local v0, i:I
    :goto_38
    if-ge v0, v3, :cond_4d

    #@3a
    .line 172
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasion:[I

    #@3c
    aput v2, v1, v0

    #@3e
    .line 175
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOfInternetPND:[I

    #@40
    aput v2, v1, v0

    #@42
    .line 176
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnLTE:[I

    #@44
    aput v2, v1, v0

    #@46
    .line 177
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnEHRPD:[I

    #@48
    aput v2, v1, v0

    #@4a
    .line 170
    add-int/lit8 v0, v0, 0x1

    #@4c
    goto :goto_38

    #@4d
    .line 180
    :cond_4d
    return-void
.end method


# virtual methods
.method public CleanLastfailReasion()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 508
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOfInternetPND:[I

    #@3
    aput v2, v0, v2

    #@5
    .line 509
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOfInternetPND:[I

    #@7
    const/4 v1, 0x1

    #@8
    aput v2, v0, v1

    #@a
    .line 510
    iput v2, p0, Lcom/android/internal/telephony/MMdebuger;->LteEmmErrorcode:I

    #@c
    .line 511
    return-void
.end method

.method public SetLteEmmErrorCode(I)V
    .registers 2
    .parameter "ErrorCode"

    #@0
    .prologue
    .line 378
    iput p1, p0, Lcom/android/internal/telephony/MMdebuger;->LteEmmErrorcode:I

    #@2
    .line 379
    return-void
.end method

.method public UpdateCurrentTime()V
    .registers 3

    #@0
    .prologue
    .line 213
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@6
    .line 215
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@8
    const/4 v1, 0x1

    #@9
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@c
    move-result v0

    #@d
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mcurYear:I

    #@f
    .line 216
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@11
    const/4 v1, 0x2

    #@12
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@15
    move-result v0

    #@16
    add-int/lit8 v0, v0, 0x1

    #@18
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mcurMonth:I

    #@1a
    .line 217
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@1c
    const/4 v1, 0x5

    #@1d
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@20
    move-result v0

    #@21
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mcurDay:I

    #@23
    .line 218
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@25
    const/16 v1, 0xb

    #@27
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mcurHour:I

    #@2d
    .line 219
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@2f
    const/16 v1, 0xc

    #@31
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@34
    move-result v0

    #@35
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mcurMinute:I

    #@37
    .line 220
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@39
    const/16 v1, 0xd

    #@3b
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@3e
    move-result v0

    #@3f
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mcurSecond:I

    #@41
    .line 222
    return-void
.end method

.method public UpdateLastCurrentTime()V
    .registers 3

    #@0
    .prologue
    .line 198
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@6
    .line 200
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@8
    const/4 v1, 0x1

    #@9
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@c
    move-result v0

    #@d
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mLcurYear:I

    #@f
    .line 201
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@11
    const/4 v1, 0x2

    #@12
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@15
    move-result v0

    #@16
    add-int/lit8 v0, v0, 0x1

    #@18
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mLcurMonth:I

    #@1a
    .line 202
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@1c
    const/4 v1, 0x5

    #@1d
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@20
    move-result v0

    #@21
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mLcurDay:I

    #@23
    .line 203
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@25
    const/16 v1, 0xb

    #@27
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mLcurHour:I

    #@2d
    .line 204
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@2f
    const/16 v1, 0xc

    #@31
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@34
    move-result v0

    #@35
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mLcurMinute:I

    #@37
    .line 205
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->c:Ljava/util/Calendar;

    #@39
    const/16 v1, 0xd

    #@3b
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@3e
    move-result v0

    #@3f
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mLcurSecond:I

    #@41
    .line 207
    return-void
.end method

.method public dstoint(Ljava/lang/String;)I
    .registers 4
    .parameter "ds"

    #@0
    .prologue
    const/4 v0, 0x2

    #@1
    .line 517
    const-string v1, "fota"

    #@3
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_b

    #@9
    .line 519
    const/4 v0, 0x1

    #@a
    .line 531
    :cond_a
    :goto_a
    return v0

    #@b
    .line 521
    :cond_b
    const-string v1, "default"

    #@d
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v1

    #@11
    if-nez v1, :cond_a

    #@13
    .line 525
    const-string v1, "dun"

    #@15
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_a

    #@1b
    .line 527
    const/4 v0, 0x3

    #@1c
    goto :goto_a
.end method

.method public getConnHistory(I)[I
    .registers 6
    .parameter "itemnum"

    #@0
    .prologue
    .line 325
    const/16 v2, 0xb

    #@2
    new-array v1, v2, [I

    #@4
    .line 327
    .local v1, returnvalue:[I
    iget-object v2, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v2

    #@a
    if-gt v2, p1, :cond_e

    #@c
    .line 328
    const/4 v1, 0x0

    #@d
    .line 347
    .end local v1           #returnvalue:[I
    :goto_d
    return-object v1

    #@e
    .line 331
    .restart local v1       #returnvalue:[I
    :cond_e
    iget-object v2, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Lcom/android/internal/telephony/MMdebuger$conHistory;

    #@16
    .line 333
    .local v0, binfo:Lcom/android/internal/telephony/MMdebuger$conHistory;
    const/4 v2, 0x0

    #@17
    iget v3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->curYear:I

    #@19
    aput v3, v1, v2

    #@1b
    .line 334
    const/4 v2, 0x1

    #@1c
    iget v3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->curMonth:I

    #@1e
    aput v3, v1, v2

    #@20
    .line 335
    const/4 v2, 0x2

    #@21
    iget v3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->curDay:I

    #@23
    aput v3, v1, v2

    #@25
    .line 336
    const/4 v2, 0x3

    #@26
    iget v3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->curHour:I

    #@28
    aput v3, v1, v2

    #@2a
    .line 337
    const/4 v2, 0x4

    #@2b
    iget v3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->curMinute:I

    #@2d
    aput v3, v1, v2

    #@2f
    .line 338
    const/4 v2, 0x5

    #@30
    iget v3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->curSecond:I

    #@32
    aput v3, v1, v2

    #@34
    .line 340
    const/4 v2, 0x6

    #@35
    iget v3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->cmdtype:I

    #@37
    aput v3, v1, v2

    #@39
    .line 341
    const/4 v2, 0x7

    #@3a
    iget v3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->cid:I

    #@3c
    aput v3, v1, v2

    #@3e
    .line 342
    const/16 v2, 0x8

    #@40
    iget v3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->serialnum:I

    #@42
    aput v3, v1, v2

    #@44
    .line 343
    const/16 v2, 0x9

    #@46
    iget v3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->currRadioTech:I

    #@48
    aput v3, v1, v2

    #@4a
    .line 344
    const/16 v2, 0xa

    #@4c
    iget v3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->APNID:I

    #@4e
    aput v3, v1, v2

    #@50
    goto :goto_d
.end method

.method public getLastFailreaon()[I
    .registers 2

    #@0
    .prologue
    .line 367
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasion:[I

    #@2
    return-object v0
.end method

.method public getLastFailreaonAtInternetPND()[I
    .registers 2

    #@0
    .prologue
    .line 373
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOfInternetPND:[I

    #@2
    return-object v0
.end method

.method public getLastFailreaonOnEHRPD()[I
    .registers 2

    #@0
    .prologue
    .line 392
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnEHRPD:[I

    #@2
    return-object v0
.end method

.method public getLastFailreaonOnLTE()[I
    .registers 2

    #@0
    .prologue
    .line 388
    iget-object v0, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnLTE:[I

    #@2
    return-object v0
.end method

.method public getLteEmmErrorcode()I
    .registers 2

    #@0
    .prologue
    .line 383
    iget v0, p0, Lcom/android/internal/telephony/MMdebuger;->LteEmmErrorcode:I

    #@2
    return v0
.end method

.method public getTimeoutHistory(I)[I
    .registers 3
    .parameter "itemnum"

    #@0
    .prologue
    .line 361
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public saveAPNType(I)V
    .registers 2
    .parameter "apnId"

    #@0
    .prologue
    .line 232
    iput p1, p0, Lcom/android/internal/telephony/MMdebuger;->currentAPNId:I

    #@2
    .line 233
    return-void
.end method

.method public saveRspHistory(III)V
    .registers 12
    .parameter "cmdtype"
    .parameter "Serial"
    .parameter "cid"

    #@0
    .prologue
    .line 288
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    iget v2, p0, Lcom/android/internal/telephony/MMdebuger;->mMaxSize:I

    #@8
    if-lt v1, v2, :cond_10

    #@a
    .line 289
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@c
    const/4 v2, 0x0

    #@d
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@10
    .line 291
    :cond_10
    invoke-virtual {p0}, Lcom/android/internal/telephony/MMdebuger;->UpdateCurrentTime()V

    #@13
    .line 293
    new-instance v0, Lcom/android/internal/telephony/MMdebuger$conHistory;

    #@15
    iget v2, p0, Lcom/android/internal/telephony/MMdebuger;->mcurYear:I

    #@17
    iget v3, p0, Lcom/android/internal/telephony/MMdebuger;->mcurMonth:I

    #@19
    iget v4, p0, Lcom/android/internal/telephony/MMdebuger;->mcurDay:I

    #@1b
    iget v5, p0, Lcom/android/internal/telephony/MMdebuger;->mcurHour:I

    #@1d
    iget v6, p0, Lcom/android/internal/telephony/MMdebuger;->mcurMinute:I

    #@1f
    iget v7, p0, Lcom/android/internal/telephony/MMdebuger;->mcurSecond:I

    #@21
    move-object v1, p0

    #@22
    invoke-direct/range {v0 .. v7}, Lcom/android/internal/telephony/MMdebuger$conHistory;-><init>(Lcom/android/internal/telephony/MMdebuger;IIIIII)V

    #@25
    .line 298
    .local v0, binfo:Lcom/android/internal/telephony/MMdebuger$conHistory;
    const/16 v1, 0x1b

    #@27
    if-ne p1, v1, :cond_3e

    #@29
    .line 300
    const/4 v1, 0x2

    #@2a
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->cmdtype:I

    #@2c
    .line 312
    :goto_2c
    iput p3, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->cid:I

    #@2e
    .line 313
    iput p2, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->serialnum:I

    #@30
    .line 314
    iget v1, p0, Lcom/android/internal/telephony/MMdebuger;->currentAPNId:I

    #@32
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->APNID:I

    #@34
    .line 315
    iget v1, p0, Lcom/android/internal/telephony/MMdebuger;->currentRadioTech:I

    #@36
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->currRadioTech:I

    #@38
    .line 318
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@3a
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3d
    .line 320
    return-void

    #@3e
    .line 302
    :cond_3e
    const/16 v1, 0x29

    #@40
    if-ne p1, v1, :cond_46

    #@42
    .line 304
    const/4 v1, 0x4

    #@43
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->cmdtype:I

    #@45
    goto :goto_2c

    #@46
    .line 309
    :cond_46
    const/16 v1, 0x63

    #@48
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->cmdtype:I

    #@4a
    goto :goto_2c
.end method

.method public saveUpHistory(I)V
    .registers 10
    .parameter "Serial"

    #@0
    .prologue
    .line 241
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    iget v2, p0, Lcom/android/internal/telephony/MMdebuger;->mMaxSize:I

    #@8
    if-lt v1, v2, :cond_10

    #@a
    .line 242
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@c
    const/4 v2, 0x0

    #@d
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@10
    .line 244
    :cond_10
    invoke-virtual {p0}, Lcom/android/internal/telephony/MMdebuger;->UpdateCurrentTime()V

    #@13
    .line 246
    new-instance v0, Lcom/android/internal/telephony/MMdebuger$conHistory;

    #@15
    iget v2, p0, Lcom/android/internal/telephony/MMdebuger;->mcurYear:I

    #@17
    iget v3, p0, Lcom/android/internal/telephony/MMdebuger;->mcurMonth:I

    #@19
    iget v4, p0, Lcom/android/internal/telephony/MMdebuger;->mcurDay:I

    #@1b
    iget v5, p0, Lcom/android/internal/telephony/MMdebuger;->mcurHour:I

    #@1d
    iget v6, p0, Lcom/android/internal/telephony/MMdebuger;->mcurMinute:I

    #@1f
    iget v7, p0, Lcom/android/internal/telephony/MMdebuger;->mcurSecond:I

    #@21
    move-object v1, p0

    #@22
    invoke-direct/range {v0 .. v7}, Lcom/android/internal/telephony/MMdebuger$conHistory;-><init>(Lcom/android/internal/telephony/MMdebuger;IIIIII)V

    #@25
    .line 248
    .local v0, binfo:Lcom/android/internal/telephony/MMdebuger$conHistory;
    const/4 v1, 0x1

    #@26
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->cmdtype:I

    #@28
    .line 249
    const/4 v1, -0x1

    #@29
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->cid:I

    #@2b
    .line 250
    iput p1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->serialnum:I

    #@2d
    .line 251
    iget v1, p0, Lcom/android/internal/telephony/MMdebuger;->currentAPNId:I

    #@2f
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->APNID:I

    #@31
    .line 252
    iget v1, p0, Lcom/android/internal/telephony/MMdebuger;->currentRadioTech:I

    #@33
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->currRadioTech:I

    #@35
    .line 255
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3a
    .line 257
    return-void
.end method

.method public savecurrenttech(I)V
    .registers 2
    .parameter "newNetworkType"

    #@0
    .prologue
    .line 227
    iput p1, p0, Lcom/android/internal/telephony/MMdebuger;->currentRadioTech:I

    #@2
    .line 228
    return-void
.end method

.method public savedownHistory(II)V
    .registers 11
    .parameter "Serial"
    .parameter "cid"

    #@0
    .prologue
    .line 264
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    iget v2, p0, Lcom/android/internal/telephony/MMdebuger;->mMaxSize:I

    #@8
    if-lt v1, v2, :cond_10

    #@a
    .line 265
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@c
    const/4 v2, 0x0

    #@d
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@10
    .line 267
    :cond_10
    invoke-virtual {p0}, Lcom/android/internal/telephony/MMdebuger;->UpdateCurrentTime()V

    #@13
    .line 269
    new-instance v0, Lcom/android/internal/telephony/MMdebuger$conHistory;

    #@15
    iget v2, p0, Lcom/android/internal/telephony/MMdebuger;->mcurYear:I

    #@17
    iget v3, p0, Lcom/android/internal/telephony/MMdebuger;->mcurMonth:I

    #@19
    iget v4, p0, Lcom/android/internal/telephony/MMdebuger;->mcurDay:I

    #@1b
    iget v5, p0, Lcom/android/internal/telephony/MMdebuger;->mcurHour:I

    #@1d
    iget v6, p0, Lcom/android/internal/telephony/MMdebuger;->mcurMinute:I

    #@1f
    iget v7, p0, Lcom/android/internal/telephony/MMdebuger;->mcurSecond:I

    #@21
    move-object v1, p0

    #@22
    invoke-direct/range {v0 .. v7}, Lcom/android/internal/telephony/MMdebuger$conHistory;-><init>(Lcom/android/internal/telephony/MMdebuger;IIIIII)V

    #@25
    .line 271
    .local v0, binfo:Lcom/android/internal/telephony/MMdebuger$conHistory;
    const/4 v1, 0x3

    #@26
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->cmdtype:I

    #@28
    .line 272
    iput p2, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->cid:I

    #@2a
    .line 273
    iput p1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->serialnum:I

    #@2c
    .line 274
    iget v1, p0, Lcom/android/internal/telephony/MMdebuger;->currentAPNId:I

    #@2e
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->APNID:I

    #@30
    .line 275
    iget v1, p0, Lcom/android/internal/telephony/MMdebuger;->currentRadioTech:I

    #@32
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$conHistory;->currRadioTech:I

    #@34
    .line 278
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mConHistory:Ljava/util/ArrayList;

    #@36
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@39
    .line 280
    return-void
.end method

.method public setFailHistory(Ljava/lang/String;Ljava/lang/String;III)V
    .registers 14
    .parameter "ds"
    .parameter "ipv"
    .parameter "currRadioTech"
    .parameter "reason"
    .parameter "dy"

    #@0
    .prologue
    .line 406
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    iget v2, p0, Lcom/android/internal/telephony/MMdebuger;->mMaxSize:I

    #@8
    if-lt v1, v2, :cond_10

    #@a
    .line 408
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistory:Ljava/util/ArrayList;

    #@c
    const/4 v2, 0x0

    #@d
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@10
    .line 411
    :cond_10
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistoryOnLTE:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@15
    move-result v1

    #@16
    iget v2, p0, Lcom/android/internal/telephony/MMdebuger;->mMaxSize:I

    #@18
    if-lt v1, v2, :cond_20

    #@1a
    .line 413
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistoryOnLTE:Ljava/util/ArrayList;

    #@1c
    const/4 v2, 0x0

    #@1d
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@20
    .line 416
    :cond_20
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistoryonEHRPD:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@25
    move-result v1

    #@26
    iget v2, p0, Lcom/android/internal/telephony/MMdebuger;->mMaxSize:I

    #@28
    if-lt v1, v2, :cond_30

    #@2a
    .line 418
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistoryonEHRPD:Ljava/util/ArrayList;

    #@2c
    const/4 v2, 0x0

    #@2d
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@30
    .line 421
    :cond_30
    invoke-virtual {p0}, Lcom/android/internal/telephony/MMdebuger;->UpdateCurrentTime()V

    #@33
    .line 423
    new-instance v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;

    #@35
    iget v2, p0, Lcom/android/internal/telephony/MMdebuger;->mcurYear:I

    #@37
    iget v3, p0, Lcom/android/internal/telephony/MMdebuger;->mcurMonth:I

    #@39
    iget v4, p0, Lcom/android/internal/telephony/MMdebuger;->mcurDay:I

    #@3b
    iget v5, p0, Lcom/android/internal/telephony/MMdebuger;->mcurHour:I

    #@3d
    iget v6, p0, Lcom/android/internal/telephony/MMdebuger;->mcurMinute:I

    #@3f
    iget v7, p0, Lcom/android/internal/telephony/MMdebuger;->mcurSecond:I

    #@41
    move-object v1, p0

    #@42
    invoke-direct/range {v0 .. v7}, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;-><init>(Lcom/android/internal/telephony/MMdebuger;IIIIII)V

    #@45
    .line 426
    .local v0, binfo:Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;
    if-nez p5, :cond_77

    #@47
    .line 427
    const/16 v1, 0x63

    #@49
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;->LastFailPDNType:I

    #@4b
    .line 431
    :goto_4b
    iget v1, v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;->LastFailPDNType:I

    #@4d
    packed-switch v1, :pswitch_data_e6

    #@50
    .line 447
    :goto_50
    const-string v1, "IP"

    #@52
    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v1

    #@56
    if-eqz v1, :cond_90

    #@58
    .line 448
    const/4 v1, 0x0

    #@59
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;->LastFailPDNIPv:I

    #@5b
    .line 455
    :cond_5b
    :goto_5b
    iget v1, p0, Lcom/android/internal/telephony/MMdebuger;->LastConRadioTech:I

    #@5d
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;->RequestRadioTech:I

    #@5f
    .line 456
    iput p3, v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;->ResponseRadioTech:I

    #@61
    .line 457
    iput p4, v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;->reason:I

    #@63
    .line 459
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistory:Ljava/util/ArrayList;

    #@65
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@68
    .line 462
    const/16 v1, 0xe

    #@6a
    if-ne p3, v1, :cond_bf

    #@6c
    .line 464
    iget v1, v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;->LastFailPDNType:I

    #@6e
    packed-switch v1, :pswitch_data_f0

    #@71
    .line 479
    :goto_71
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistoryOnLTE:Ljava/util/ArrayList;

    #@73
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@76
    .line 503
    :cond_76
    :goto_76
    return-void

    #@77
    .line 429
    :cond_77
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/MMdebuger;->dstoint(Ljava/lang/String;)I

    #@7a
    move-result v1

    #@7b
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;->LastFailPDNType:I

    #@7d
    goto :goto_4b

    #@7e
    .line 434
    :pswitch_7e
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasion:[I

    #@80
    const/4 v2, 0x0

    #@81
    aput p4, v1, v2

    #@83
    goto :goto_50

    #@84
    .line 437
    :pswitch_84
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasion:[I

    #@86
    const/4 v2, 0x1

    #@87
    aput p4, v1, v2

    #@89
    goto :goto_50

    #@8a
    .line 440
    :pswitch_8a
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasion:[I

    #@8c
    const/4 v2, 0x2

    #@8d
    aput p4, v1, v2

    #@8f
    goto :goto_50

    #@90
    .line 449
    :cond_90
    const-string v1, "IPV6"

    #@92
    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@95
    move-result v1

    #@96
    if-eqz v1, :cond_9c

    #@98
    .line 450
    const/4 v1, 0x1

    #@99
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;->LastFailPDNIPv:I

    #@9b
    goto :goto_5b

    #@9c
    .line 451
    :cond_9c
    const-string v1, "IPV4V6"

    #@9e
    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a1
    move-result v1

    #@a2
    if-eqz v1, :cond_5b

    #@a4
    .line 452
    const/4 v1, 0x2

    #@a5
    iput v1, v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;->LastFailPDNIPv:I

    #@a7
    goto :goto_5b

    #@a8
    .line 467
    :pswitch_a8
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnLTE:[I

    #@aa
    const/4 v2, 0x0

    #@ab
    aput p4, v1, v2

    #@ad
    goto :goto_71

    #@ae
    .line 470
    :pswitch_ae
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnLTE:[I

    #@b0
    const/4 v2, 0x1

    #@b1
    aput p4, v1, v2

    #@b3
    .line 471
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOfInternetPND:[I

    #@b5
    const/4 v2, 0x0

    #@b6
    aput p4, v1, v2

    #@b8
    goto :goto_71

    #@b9
    .line 474
    :pswitch_b9
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnLTE:[I

    #@bb
    const/4 v2, 0x2

    #@bc
    aput p4, v1, v2

    #@be
    goto :goto_71

    #@bf
    .line 482
    :cond_bf
    const/16 v1, 0xd

    #@c1
    if-ne p3, v1, :cond_76

    #@c3
    .line 484
    iget v1, v0, Lcom/android/internal/telephony/MMdebuger$PDNFailHistory;->LastFailPDNType:I

    #@c5
    packed-switch v1, :pswitch_data_fa

    #@c8
    .line 499
    :goto_c8
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->mPDNFailHistoryonEHRPD:Ljava/util/ArrayList;

    #@ca
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@cd
    goto :goto_76

    #@ce
    .line 487
    :pswitch_ce
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnEHRPD:[I

    #@d0
    const/4 v2, 0x0

    #@d1
    aput p4, v1, v2

    #@d3
    goto :goto_c8

    #@d4
    .line 490
    :pswitch_d4
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnEHRPD:[I

    #@d6
    const/4 v2, 0x1

    #@d7
    aput p4, v1, v2

    #@d9
    .line 491
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOfInternetPND:[I

    #@db
    const/4 v2, 0x1

    #@dc
    aput p4, v1, v2

    #@de
    goto :goto_c8

    #@df
    .line 494
    :pswitch_df
    iget-object v1, p0, Lcom/android/internal/telephony/MMdebuger;->lastfailreasionOnEHRPD:[I

    #@e1
    const/4 v2, 0x2

    #@e2
    aput p4, v1, v2

    #@e4
    goto :goto_c8

    #@e5
    .line 431
    nop

    #@e6
    :pswitch_data_e6
    .packed-switch 0x1
        :pswitch_7e
        :pswitch_84
        :pswitch_8a
    .end packed-switch

    #@f0
    .line 464
    :pswitch_data_f0
    .packed-switch 0x1
        :pswitch_a8
        :pswitch_ae
        :pswitch_b9
    .end packed-switch

    #@fa
    .line 484
    :pswitch_data_fa
    .packed-switch 0x1
        :pswitch_ce
        :pswitch_d4
        :pswitch_df
    .end packed-switch
.end method

.method public setMaxLogSize(I)V
    .registers 4
    .parameter "maxnum"

    #@0
    .prologue
    const/16 v1, 0x1f4

    #@2
    const/16 v0, 0xa

    #@4
    .line 185
    iput p1, p0, Lcom/android/internal/telephony/MMdebuger;->mMaxSize:I

    #@6
    .line 187
    if-le p1, v1, :cond_b

    #@8
    .line 188
    iput v1, p0, Lcom/android/internal/telephony/MMdebuger;->mMaxSize:I

    #@a
    .line 192
    :cond_a
    :goto_a
    return-void

    #@b
    .line 189
    :cond_b
    if-ge p1, v0, :cond_a

    #@d
    .line 190
    iput v0, p0, Lcom/android/internal/telephony/MMdebuger;->mMaxSize:I

    #@f
    goto :goto_a
.end method

.method public setTimeoutHistory(IIIII)V
    .registers 6
    .parameter "currRadioTech"
    .parameter "reqRadioTech"
    .parameter "reqSrvState"
    .parameter "CurrSrvState"
    .parameter "apnid"

    #@0
    .prologue
    .line 355
    return-void
.end method
