.class public Lcom/android/internal/telephony/ApnSettingHelper;
.super Ljava/lang/Object;
.source "ApnSettingHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;
    }
.end annotation


# static fields
.field private static final APN_TABLE_URI:Landroid/net/Uri; = null

.field private static final NUMBER_OF_APN_AT_CMD_PARAM:I = 0x7

.field private static final VZW_MCCMNC:Ljava/lang/String; = "311480"

.field private static mContext:Landroid/content/Context;


# instance fields
.field private apnMcc:Ljava/lang/String;

.field private apnMnc:Ljava/lang/String;

.field private apnNumeric:Ljava/lang/String;

.field private apnb:Ljava/lang/String;

.field private apncl:Ljava/lang/String;

.field private apnclName:Ljava/lang/String;

.field private apned:Ljava/lang/String;

.field private apnni:Ljava/lang/String;

.field private apntime:Ljava/lang/String;

.field private apntypeipv4v6:Ljava/lang/String;

.field private isSimRead:I

.field private isVZW:I

.field private isValidate:I

.field private wapn:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 76
    const-string v0, "content://telephony/carriers"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/internal/telephony/ApnSettingHelper;->APN_TABLE_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 106
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 107
    sput-object p1, Lcom/android/internal/telephony/ApnSettingHelper;->mContext:Landroid/content/Context;

    #@6
    .line 109
    iput v0, p0, Lcom/android/internal/telephony/ApnSettingHelper;->isSimRead:I

    #@8
    .line 110
    iput v0, p0, Lcom/android/internal/telephony/ApnSettingHelper;->isValidate:I

    #@a
    .line 111
    const/4 v0, 0x1

    #@b
    iput v0, p0, Lcom/android/internal/telephony/ApnSettingHelper;->isVZW:I

    #@d
    .line 113
    invoke-direct {p0}, Lcom/android/internal/telephony/ApnSettingHelper;->setMccMnc()I

    #@10
    .line 118
    return-void
.end method

.method private setMccMnc()I
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v3, 0x3

    #@2
    const/4 v1, 0x0

    #@3
    .line 295
    const-string v2, "gsm.sim.operator.numeric"

    #@5
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    iput-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@b
    .line 298
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@d
    if-eqz v2, :cond_17

    #@f
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@11
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@14
    move-result v2

    #@15
    if-gt v2, v3, :cond_35

    #@17
    .line 299
    :cond_17
    const-string v0, "VZWAPN"

    #@19
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v3, "[ApnSettingHelper::setMccMnc] couldn\'t get the numeric from system. "

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    iget-object v3, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 302
    iput v1, p0, Lcom/android/internal/telephony/ApnSettingHelper;->isSimRead:I

    #@33
    move v0, v1

    #@34
    .line 315
    :goto_34
    return v0

    #@35
    .line 307
    :cond_35
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@37
    invoke-virtual {v2, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    iput-object v1, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnMcc:Ljava/lang/String;

    #@3d
    .line 308
    iget-object v1, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@3f
    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    iput-object v1, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnMnc:Ljava/lang/String;

    #@45
    .line 310
    const-string v1, "VZWAPN"

    #@47
    new-instance v2, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v3, "[ApnSettingHelper::setMccMnc] apnNumeric : "

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    iget-object v3, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@54
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    const-string v3, ", apnMcc:"

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    iget-object v3, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnMcc:Ljava/lang/String;

    #@60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v2

    #@64
    const-string v3, ", apnMnc:"

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    iget-object v3, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnMnc:Ljava/lang/String;

    #@6c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v2

    #@70
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v2

    #@74
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 313
    iput v0, p0, Lcom/android/internal/telephony/ApnSettingHelper;->isSimRead:I

    #@79
    goto :goto_34
.end method


# virtual methods
.method public apnQuery()Landroid/database/Cursor;
    .registers 10

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, " (numeric = \'"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    iget-object v1, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, "\') "

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    .line 127
    .local v3, where:Ljava/lang/String;
    sget-object v0, Lcom/android/internal/telephony/ApnSettingHelper;->mContext:Landroid/content/Context;

    #@1e
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@21
    move-result-object v0

    #@22
    sget-object v1, Lcom/android/internal/telephony/ApnSettingHelper;->APN_TABLE_URI:Landroid/net/Uri;

    #@24
    move-object v4, v2

    #@25
    move-object v5, v2

    #@26
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@29
    move-result-object v6

    #@2a
    .line 130
    .local v6, c:Landroid/database/Cursor;
    if-eqz v6, :cond_86

    #@2c
    .line 131
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@2f
    move-result v8

    #@30
    .line 132
    .local v8, numberOfApns:I
    const-string v0, "VZWAPN"

    #@32
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v2, "Number of APNs is "

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 134
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@4b
    move-result v0

    #@4c
    if-eqz v0, :cond_86

    #@4e
    .line 136
    :cond_4e
    const/4 v7, 0x0

    #@4f
    .local v7, i:I
    :goto_4f
    invoke-interface {v6}, Landroid/database/Cursor;->getColumnCount()I

    #@52
    move-result v0

    #@53
    if-ge v7, v0, :cond_80

    #@55
    .line 138
    const-string v0, "VZWAPN"

    #@57
    new-instance v1, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v1

    #@64
    const-string v2, "|"

    #@66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v1

    #@6a
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v1

    #@6e
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@71
    move-result-object v2

    #@72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v1

    #@76
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v1

    #@7a
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 136
    add-int/lit8 v7, v7, 0x1

    #@7f
    goto :goto_4f

    #@80
    .line 140
    :cond_80
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@83
    move-result v0

    #@84
    if-nez v0, :cond_4e

    #@86
    .line 144
    .end local v7           #i:I
    .end local v8           #numberOfApns:I
    :cond_86
    return-object v6
.end method

.method protected checkValidation([Ljava/lang/String;)I
    .registers 15
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    const/4 v12, 0x2

    #@1
    const/4 v11, 0x0

    #@2
    const/4 v10, 0x1

    #@3
    .line 157
    const/4 v7, 0x0

    #@4
    .line 158
    .local v7, tempWapn:I
    const/4 v5, 0x0

    #@5
    .line 174
    .local v5, tempApncl:I
    iget-object v8, p0, Lcom/android/internal/telephony/ApnSettingHelper;->wapn:Ljava/lang/String;

    #@7
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@a
    move-result v7

    #@b
    .line 175
    if-nez v7, :cond_15

    #@d
    new-instance v8, Ljava/lang/Exception;

    #@f
    const-string v9, "WAPN is zero. No action for this AT command."

    #@11
    invoke-direct {v8, v9}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@14
    throw v8

    #@15
    .line 178
    :cond_15
    iget-object v8, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apncl:Ljava/lang/String;

    #@17
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1a
    move-result v5

    #@1b
    .line 179
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/ApnSettingHelper;->getApnclName(I)Ljava/lang/String;

    #@1e
    move-result-object v8

    #@1f
    iput-object v8, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnclName:Ljava/lang/String;

    #@21
    .line 211
    const/4 v8, 0x4

    #@22
    new-array v1, v8, [Ljava/lang/String;

    #@24
    const-string v8, "IP"

    #@26
    aput-object v8, v1, v11

    #@28
    const-string v8, "IPv4"

    #@2a
    aput-object v8, v1, v10

    #@2c
    const-string v8, "IPv6"

    #@2e
    aput-object v8, v1, v12

    #@30
    const/4 v8, 0x3

    #@31
    const-string v9, "IPv4v6"

    #@33
    aput-object v9, v1, v8

    #@35
    .line 212
    .local v1, availableApntype:[Ljava/lang/String;
    const/4 v3, 0x0

    #@36
    .line 214
    .local v3, isValidApnType:I
    const/4 v2, 0x0

    #@37
    .local v2, i:I
    :goto_37
    array-length v8, v1

    #@38
    if-ge v2, v8, :cond_48

    #@3a
    .line 215
    iget-object v8, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apntypeipv4v6:Ljava/lang/String;

    #@3c
    aget-object v9, v1, v2

    #@3e
    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@41
    move-result v8

    #@42
    if-nez v8, :cond_45

    #@44
    const/4 v3, 0x1

    #@45
    .line 214
    :cond_45
    add-int/lit8 v2, v2, 0x1

    #@47
    goto :goto_37

    #@48
    .line 218
    :cond_48
    if-nez v3, :cond_6b

    #@4a
    new-instance v8, Ljava/lang/Exception;

    #@4c
    new-instance v9, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v10, "APNTYPE "

    #@53
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v9

    #@57
    iget-object v10, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apntypeipv4v6:Ljava/lang/String;

    #@59
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v9

    #@5d
    const-string v10, " is not right"

    #@5f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v9

    #@63
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v9

    #@67
    invoke-direct {v8, v9}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@6a
    throw v8

    #@6b
    .line 219
    :cond_6b
    iget-object v8, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apntypeipv4v6:Ljava/lang/String;

    #@6d
    const-string v9, "IPv4"

    #@6f
    invoke-virtual {v8, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@72
    move-result v8

    #@73
    if-nez v8, :cond_79

    #@75
    const-string v8, "IP"

    #@77
    iput-object v8, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apntypeipv4v6:Ljava/lang/String;

    #@79
    .line 220
    :cond_79
    iget-object v8, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apntypeipv4v6:Ljava/lang/String;

    #@7b
    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@7e
    move-result-object v8

    #@7f
    iput-object v8, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apntypeipv4v6:Ljava/lang/String;

    #@81
    .line 236
    new-array v0, v12, [Ljava/lang/String;

    #@83
    const-string v8, "Enabled"

    #@85
    aput-object v8, v0, v11

    #@87
    const-string v8, "Disabled"

    #@89
    aput-object v8, v0, v10

    #@8b
    .line 237
    .local v0, availableApned:[Ljava/lang/String;
    const/4 v4, 0x0

    #@8c
    .line 239
    .local v4, isValidApned:I
    const/4 v2, 0x0

    #@8d
    :goto_8d
    array-length v8, v0

    #@8e
    if-ge v2, v8, :cond_9e

    #@90
    .line 240
    iget-object v8, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apned:Ljava/lang/String;

    #@92
    aget-object v9, v0, v2

    #@94
    invoke-virtual {v8, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@97
    move-result v8

    #@98
    if-nez v8, :cond_9b

    #@9a
    const/4 v4, 0x1

    #@9b
    .line 239
    :cond_9b
    add-int/lit8 v2, v2, 0x1

    #@9d
    goto :goto_8d

    #@9e
    .line 243
    :cond_9e
    if-nez v4, :cond_c1

    #@a0
    new-instance v8, Ljava/lang/Exception;

    #@a2
    new-instance v9, Ljava/lang/StringBuilder;

    #@a4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a7
    const-string v10, "APNED "

    #@a9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v9

    #@ad
    iget-object v10, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apned:Ljava/lang/String;

    #@af
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v9

    #@b3
    const-string v10, " is not right"

    #@b5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v9

    #@b9
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v9

    #@bd
    invoke-direct {v8, v9}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@c0
    throw v8

    #@c1
    .line 246
    :cond_c1
    iget-object v8, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apntime:Ljava/lang/String;

    #@c3
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@c6
    move-result v6

    #@c7
    .line 248
    .local v6, tempApntime:I
    return v10
.end method

.method public get()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 505
    const-string v0, "TEST"

    #@2
    .line 506
    .local v0, abc:Ljava/lang/String;
    return-object v0
.end method

.method public getApnClass(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "apnType"

    #@0
    .prologue
    .line 375
    const/4 v3, 0x5

    #@1
    new-array v2, v3, [Ljava/lang/String;

    #@3
    const/4 v3, 0x0

    #@4
    const-string v4, "ims"

    #@6
    aput-object v4, v2, v3

    #@8
    const/4 v3, 0x1

    #@9
    const-string v4, "admin"

    #@b
    aput-object v4, v2, v3

    #@d
    const/4 v3, 0x2

    #@e
    const-string v4, "default"

    #@10
    aput-object v4, v2, v3

    #@12
    const/4 v3, 0x3

    #@13
    const-string v4, "vzwapp,mms,cbs"

    #@15
    aput-object v4, v2, v3

    #@17
    const/4 v3, 0x4

    #@18
    const-string v4, "vzw800"

    #@1a
    aput-object v4, v2, v3

    #@1c
    .line 378
    .local v2, type:[Ljava/lang/String;
    const/4 v1, 0x0

    #@1d
    .line 380
    .local v1, nApnType:I
    const/4 v0, 0x0

    #@1e
    .local v0, i:I
    :goto_1e
    array-length v3, v2

    #@1f
    if-ge v0, v3, :cond_2e

    #@21
    .line 381
    aget-object v3, v2, v0

    #@23
    invoke-virtual {v3, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@26
    move-result v3

    #@27
    if-nez v3, :cond_2b

    #@29
    add-int/lit8 v1, v0, 0x1

    #@2b
    .line 380
    :cond_2b
    add-int/lit8 v0, v0, 0x1

    #@2d
    goto :goto_1e

    #@2e
    .line 383
    :cond_2e
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    return-object v3
.end method

.method public getApnb(I)Ljava/lang/String;
    .registers 3
    .parameter "apnB"

    #@0
    .prologue
    .line 404
    packed-switch p1, :pswitch_data_c

    #@3
    .line 413
    const-string v0, "Unspecified"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 407
    :pswitch_6
    const-string v0, "LTE"

    #@8
    goto :goto_5

    #@9
    .line 410
    :pswitch_9
    const-string v0, "EHRPD"

    #@b
    goto :goto_5

    #@c
    .line 404
    :pswitch_data_c
    .packed-switch 0xd
        :pswitch_9
        :pswitch_6
    .end packed-switch
.end method

.method public getApnclName(I)Ljava/lang/String;
    .registers 7
    .parameter "apnclass"

    #@0
    .prologue
    .line 351
    const/4 v2, 0x5

    #@1
    new-array v1, v2, [Ljava/lang/String;

    #@3
    const/4 v2, 0x0

    #@4
    const-string v3, "ims"

    #@6
    aput-object v3, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    const-string v3, "admin"

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x2

    #@e
    const-string v3, "default"

    #@10
    aput-object v3, v1, v2

    #@12
    const/4 v2, 0x3

    #@13
    const-string v3, "vzwapp,mms,cbs"

    #@15
    aput-object v3, v1, v2

    #@17
    const/4 v2, 0x4

    #@18
    const-string v3, "vzw800"

    #@1a
    aput-object v3, v1, v2

    #@1c
    .line 354
    .local v1, type:[Ljava/lang/String;
    array-length v2, v1

    #@1d
    if-ge v2, p1, :cond_42

    #@1f
    if-gtz p1, :cond_42

    #@21
    .line 356
    const-string v2, "VZWAPN"

    #@23
    new-instance v3, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v4, "Coulnd\'t convert "

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    const-string v4, " to Integer."

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 357
    const-string v0, "NoneAPN"

    #@41
    .line 364
    .local v0, returnValue:Ljava/lang/String;
    :goto_41
    return-object v0

    #@42
    .line 361
    .end local v0           #returnValue:Ljava/lang/String;
    :cond_42
    add-int/lit8 v2, p1, -0x1

    #@44
    aget-object v0, v1, v2

    #@46
    .restart local v0       #returnValue:Ljava/lang/String;
    goto :goto_41
.end method

.method public getApnclName(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "apnclass"

    #@0
    .prologue
    .line 330
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3
    move-result v1

    #@4
    .line 332
    .local v1, nApnClass:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ApnSettingHelper;->getApnclName(I)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v2

    #@8
    .line 340
    .end local v1           #nApnClass:I
    .local v2, type:Ljava/lang/String;
    :goto_8
    return-object v2

    #@9
    .line 334
    .end local v2           #type:Ljava/lang/String;
    :catch_9
    move-exception v0

    #@a
    .line 336
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "VZWAPN"

    #@c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "Coulnd\'t convert "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, " to Integer."

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 337
    const-string v2, "none"

    #@2a
    .restart local v2       #type:Ljava/lang/String;
    goto :goto_8
.end method

.method public getApned(I)Ljava/lang/String;
    .registers 3
    .parameter "apnEd"

    #@0
    .prologue
    .line 393
    const/4 v0, 0x1

    #@1
    if-ne p1, v0, :cond_6

    #@3
    const-string v0, "Enabled"

    #@5
    .line 394
    :goto_5
    return-object v0

    #@6
    :cond_6
    const-string v0, "Disabled"

    #@8
    goto :goto_5
.end method

.method public set(Ljava/lang/String;)V
    .registers 8
    .parameter "atCmdRequest"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 259
    const-string v2, "VZWAPN"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "string args = "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 262
    const-string v2, "(\\r|\\n)"

    #@1b
    const-string v3, ""

    #@1d
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object p1

    #@21
    .line 263
    const-string v2, ","

    #@23
    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 266
    .local v0, args:[Ljava/lang/String;
    array-length v2, v0

    #@28
    const/4 v3, 0x7

    #@29
    if-eq v2, v3, :cond_47

    #@2b
    .line 267
    const-string v2, "VZWAPN"

    #@2d
    new-instance v3, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v4, "AT command Param Number is "

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    array-length v4, v0

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 268
    iput v5, p0, Lcom/android/internal/telephony/ApnSettingHelper;->isValidate:I

    #@46
    .line 289
    :goto_46
    return-void

    #@47
    .line 274
    :cond_47
    aget-object v2, v0, v5

    #@49
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    iput-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->wapn:Ljava/lang/String;

    #@4f
    .line 275
    const/4 v2, 0x1

    #@50
    aget-object v2, v0, v2

    #@52
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    iput-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apncl:Ljava/lang/String;

    #@58
    .line 276
    const/4 v2, 0x2

    #@59
    aget-object v2, v0, v2

    #@5b
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    iput-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnni:Ljava/lang/String;

    #@61
    .line 277
    const/4 v2, 0x3

    #@62
    aget-object v2, v0, v2

    #@64
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@67
    move-result-object v2

    #@68
    iput-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apntypeipv4v6:Ljava/lang/String;

    #@6a
    .line 278
    const/4 v2, 0x4

    #@6b
    aget-object v2, v0, v2

    #@6d
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@70
    move-result-object v2

    #@71
    iput-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnb:Ljava/lang/String;

    #@73
    .line 279
    const/4 v2, 0x5

    #@74
    aget-object v2, v0, v2

    #@76
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@79
    move-result-object v2

    #@7a
    iput-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apned:Ljava/lang/String;

    #@7c
    .line 280
    const/4 v2, 0x6

    #@7d
    aget-object v2, v0, v2

    #@7f
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    iput-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apntime:Ljava/lang/String;

    #@85
    .line 284
    :try_start_85
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ApnSettingHelper;->checkValidation([Ljava/lang/String;)I

    #@88
    move-result v2

    #@89
    iput v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->isValidate:I
    :try_end_8b
    .catch Ljava/lang/Exception; {:try_start_85 .. :try_end_8b} :catch_8c

    #@8b
    goto :goto_46

    #@8c
    .line 285
    :catch_8c
    move-exception v1

    #@8d
    .line 286
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "VZWAPN"

    #@8f
    new-instance v3, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string v4, "APN AT CMD PARAM IS NOT RIGHT. "

    #@96
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v3

    #@9a
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@9d
    move-result-object v4

    #@9e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v3

    #@a2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v3

    #@a6
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 287
    iput v5, p0, Lcom/android/internal/telephony/ApnSettingHelper;->isValidate:I

    #@ab
    goto :goto_46
.end method

.method public toString()Ljava/lang/String;
    .registers 18

    #@0
    .prologue
    .line 512
    new-instance v14, Ljava/lang/String;

    #@2
    invoke-direct {v14}, Ljava/lang/String;-><init>()V

    #@5
    .line 513
    .local v14, apns:Ljava/lang/String;
    const-string v7, ", "

    #@7
    .line 515
    .local v7, SEPERATOR:Ljava/lang/String;
    move-object/from16 v0, p0

    #@9
    iget v1, v0, Lcom/android/internal/telephony/ApnSettingHelper;->isSimRead:I

    #@b
    const/4 v2, 0x1

    #@c
    if-eq v1, v2, :cond_2d

    #@e
    .line 516
    const-string v1, "VZWAPN"

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "[ApnSettingHelper::toString] sim information is not valid. "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    move-object/from16 v0, p0

    #@1d
    iget-object v3, v0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 518
    const-string v1, "SIM is not initialized"

    #@2c
    .line 556
    :goto_2c
    return-object v1

    #@2d
    .line 521
    :cond_2d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/ApnSettingHelper;->apnQuery()Landroid/database/Cursor;

    #@30
    .line 523
    new-instance v1, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v2, " (numeric = \'"

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    move-object/from16 v0, p0

    #@3d
    iget-object v2, v0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    const-string v2, "\') "

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    .line 524
    .local v4, where:Ljava/lang/String;
    sget-object v1, Lcom/android/internal/telephony/ApnSettingHelper;->mContext:Landroid/content/Context;

    #@4f
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@52
    move-result-object v1

    #@53
    sget-object v2, Lcom/android/internal/telephony/ApnSettingHelper;->APN_TABLE_URI:Landroid/net/Uri;

    #@55
    const/4 v3, 0x0

    #@56
    const/4 v5, 0x0

    #@57
    const/4 v6, 0x0

    #@58
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@5b
    move-result-object v15

    #@5c
    .line 527
    .local v15, c:Landroid/database/Cursor;
    if-eqz v15, :cond_146

    #@5e
    .line 528
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    #@61
    move-result v16

    #@62
    .line 529
    .local v16, numberOfApns:I
    const-string v1, "VZWAPN"

    #@64
    new-instance v2, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v3, "Number of APNs is "

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    move/from16 v0, v16

    #@71
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@74
    move-result-object v2

    #@75
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v2

    #@79
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 531
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    #@7f
    move-result v1

    #@80
    if-eqz v1, :cond_146

    #@82
    .line 532
    new-instance v1, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v1

    #@8b
    const-string v2, "\r\n"

    #@8d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v1

    #@91
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v14

    #@95
    .line 536
    :cond_95
    const/4 v1, 0x5

    #@96
    invoke-interface {v15, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@99
    move-result-object v10

    #@9a
    .line 537
    .local v10, apnName:Ljava/lang/String;
    const/16 v1, 0xf

    #@9c
    invoke-interface {v15, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9f
    move-result-object v11

    #@a0
    .line 538
    .local v11, apnType:Ljava/lang/String;
    move-object/from16 v0, p0

    #@a2
    invoke-virtual {v0, v11}, Lcom/android/internal/telephony/ApnSettingHelper;->getApnClass(Ljava/lang/String;)Ljava/lang/String;

    #@a5
    move-result-object v13

    #@a6
    .line 539
    .local v13, apncl:Ljava/lang/String;
    const/16 v1, 0x14

    #@a8
    invoke-interface {v15, v1}, Landroid/database/Cursor;->getInt(I)I

    #@ab
    move-result v1

    #@ac
    move-object/from16 v0, p0

    #@ae
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/ApnSettingHelper;->getApned(I)Ljava/lang/String;

    #@b1
    move-result-object v9

    #@b2
    .line 540
    .local v9, apnEd:Ljava/lang/String;
    const/16 v1, 0x18

    #@b4
    invoke-interface {v15, v1}, Landroid/database/Cursor;->getInt(I)I

    #@b7
    move-result v1

    #@b8
    move-object/from16 v0, p0

    #@ba
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/ApnSettingHelper;->getApnb(I)Ljava/lang/String;

    #@bd
    move-result-object v8

    #@be
    .line 541
    .local v8, apnB:Ljava/lang/String;
    const/16 v1, 0x11

    #@c0
    invoke-interface {v15, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@c3
    move-result-object v12

    #@c4
    .line 543
    .local v12, apnTypeIpv4v6:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v1

    #@cd
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v1

    #@d1
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v1

    #@d5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v14

    #@d9
    .line 544
    new-instance v1, Ljava/lang/StringBuilder;

    #@db
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@de
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v1

    #@e2
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v1

    #@e6
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v1

    #@ea
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ed
    move-result-object v14

    #@ee
    .line 545
    new-instance v1, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v1

    #@f7
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v1

    #@fb
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v1

    #@ff
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@102
    move-result-object v14

    #@103
    .line 546
    new-instance v1, Ljava/lang/StringBuilder;

    #@105
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@108
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v1

    #@10c
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v1

    #@110
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v1

    #@114
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@117
    move-result-object v14

    #@118
    .line 547
    new-instance v1, Ljava/lang/StringBuilder;

    #@11a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11d
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v1

    #@121
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v1

    #@125
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v1

    #@129
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12c
    move-result-object v14

    #@12d
    .line 548
    new-instance v1, Ljava/lang/StringBuilder;

    #@12f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@132
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v1

    #@136
    const-string v2, "0\r\n"

    #@138
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v1

    #@13c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13f
    move-result-object v14

    #@140
    .line 550
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    #@143
    move-result v1

    #@144
    if-nez v1, :cond_95

    #@146
    .line 554
    .end local v8           #apnB:Ljava/lang/String;
    .end local v9           #apnEd:Ljava/lang/String;
    .end local v10           #apnName:Ljava/lang/String;
    .end local v11           #apnType:Ljava/lang/String;
    .end local v12           #apnTypeIpv4v6:Ljava/lang/String;
    .end local v13           #apncl:Ljava/lang/String;
    .end local v16           #numberOfApns:I
    :cond_146
    const-string v1, "VZWAPN"

    #@148
    invoke-static {v1, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@14b
    move-object v1, v14

    #@14c
    .line 556
    goto/16 :goto_2c
.end method

.method public update()I
    .registers 14

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v12, 0x0

    #@2
    const/4 v0, 0x0

    #@3
    .line 424
    const/4 v10, 0x0

    #@4
    .line 425
    .local v10, result:I
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, " (numeric = \'"

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, "\' and type =\'"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnclName:Ljava/lang/String;

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, "\') "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    .line 428
    .local v3, where:Ljava/lang/String;
    iget v1, p0, Lcom/android/internal/telephony/ApnSettingHelper;->isSimRead:I

    #@2d
    if-eq v1, v4, :cond_4a

    #@2f
    .line 429
    const-string v1, "VZWAPN"

    #@31
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v4, "[ApnSettingHelper::update] sim information is not valid. "

    #@38
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    iget-object v4, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@3e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 499
    :cond_49
    :goto_49
    return v0

    #@4a
    .line 434
    :cond_4a
    iget v1, p0, Lcom/android/internal/telephony/ApnSettingHelper;->isValidate:I

    #@4c
    if-eqz v1, :cond_49

    #@4e
    .line 436
    new-instance v11, Landroid/content/ContentValues;

    #@50
    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    #@53
    .line 437
    .local v11, values:Landroid/content/ContentValues;
    const-string v1, "type"

    #@55
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnclName:Ljava/lang/String;

    #@57
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    .line 440
    iget-object v1, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnb:Ljava/lang/String;

    #@5c
    const-string v2, "LTE"

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@61
    move-result v1

    #@62
    if-nez v1, :cond_126

    #@64
    .line 441
    const-string v1, "bearer"

    #@66
    const/16 v2, 0xe

    #@68
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b
    move-result-object v2

    #@6c
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@6f
    .line 448
    :goto_6f
    const-string v1, "mcc"

    #@71
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnMcc:Ljava/lang/String;

    #@73
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@76
    .line 449
    const-string v1, "mnc"

    #@78
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnMnc:Ljava/lang/String;

    #@7a
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7d
    .line 450
    const-string v1, "numeric"

    #@7f
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnNumeric:Ljava/lang/String;

    #@81
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@84
    .line 451
    const-string v1, "current"

    #@86
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@89
    move-result-object v2

    #@8a
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@8d
    .line 452
    const-string v1, "authtype"

    #@8f
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@92
    move-result-object v2

    #@93
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@96
    .line 453
    const-string v1, "roaming_protocol"

    #@98
    const-string v2, "IPV4V6"

    #@9a
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9d
    .line 454
    const-string v1, "name"

    #@9f
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnni:Ljava/lang/String;

    #@a1
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a4
    .line 455
    const-string v1, "apn"

    #@a6
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnni:Ljava/lang/String;

    #@a8
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@ab
    .line 456
    const-string v1, "protocol"

    #@ad
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apntypeipv4v6:Ljava/lang/String;

    #@af
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b2
    .line 458
    iget-object v1, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apned:Ljava/lang/String;

    #@b4
    const-string v2, "Enabled"

    #@b6
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@b9
    move-result v1

    #@ba
    if-nez v1, :cond_148

    #@bc
    .line 459
    const-string v0, "carrier_enabled"

    #@be
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c1
    move-result-object v1

    #@c2
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@c5
    .line 464
    :goto_c5
    const/4 v6, 0x0

    #@c6
    .line 465
    .local v6, c:Landroid/database/Cursor;
    const/4 v9, 0x0

    #@c7
    .line 468
    .local v9, numOfResult:I
    :try_start_c7
    sget-object v0, Lcom/android/internal/telephony/ApnSettingHelper;->mContext:Landroid/content/Context;

    #@c9
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@cc
    move-result-object v0

    #@cd
    sget-object v1, Lcom/android/internal/telephony/ApnSettingHelper;->APN_TABLE_URI:Landroid/net/Uri;

    #@cf
    const/4 v2, 0x0

    #@d0
    const/4 v4, 0x0

    #@d1
    const/4 v5, 0x0

    #@d2
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_d5
    .catch Ljava/lang/Exception; {:try_start_c7 .. :try_end_d5} :catch_153

    #@d5
    move-result-object v6

    #@d6
    .line 473
    :goto_d6
    if-eqz v6, :cond_dc

    #@d8
    .line 474
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@db
    move-result v9

    #@dc
    .line 478
    :cond_dc
    const-string v0, "VZWAPN"

    #@de
    new-instance v1, Ljava/lang/StringBuilder;

    #@e0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e3
    const-string v2, "Found : "

    #@e5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v1

    #@e9
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v1

    #@ed
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f0
    move-result-object v1

    #@f1
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@f4
    .line 480
    if-eqz v6, :cond_172

    #@f6
    if-eqz v9, :cond_172

    #@f8
    .line 482
    sget-object v0, Lcom/android/internal/telephony/ApnSettingHelper;->mContext:Landroid/content/Context;

    #@fa
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@fd
    move-result-object v0

    #@fe
    sget-object v1, Lcom/android/internal/telephony/ApnSettingHelper;->APN_TABLE_URI:Landroid/net/Uri;

    #@100
    invoke-virtual {v0, v1, v11, v3, v12}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@103
    .line 483
    const-string v0, "VZWAPN"

    #@105
    new-instance v1, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    const-string v2, "Updated APN is "

    #@10c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v1

    #@110
    invoke-virtual {v11}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    #@113
    move-result-object v2

    #@114
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v1

    #@118
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11b
    move-result-object v1

    #@11c
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11f
    .line 495
    :cond_11f
    :goto_11f
    invoke-virtual {p0}, Lcom/android/internal/telephony/ApnSettingHelper;->apnQuery()Landroid/database/Cursor;

    #@122
    .line 497
    const/4 v10, 0x1

    #@123
    move v0, v10

    #@124
    .line 499
    goto/16 :goto_49

    #@126
    .line 442
    .end local v6           #c:Landroid/database/Cursor;
    .end local v9           #numOfResult:I
    :cond_126
    iget-object v1, p0, Lcom/android/internal/telephony/ApnSettingHelper;->apnb:Ljava/lang/String;

    #@128
    const-string v2, "EHRPD"

    #@12a
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@12d
    move-result v1

    #@12e
    if-nez v1, :cond_13d

    #@130
    .line 443
    const-string v1, "bearer"

    #@132
    const/16 v2, 0xd

    #@134
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@137
    move-result-object v2

    #@138
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@13b
    goto/16 :goto_6f

    #@13d
    .line 445
    :cond_13d
    const-string v1, "bearer"

    #@13f
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@142
    move-result-object v2

    #@143
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@146
    goto/16 :goto_6f

    #@148
    .line 461
    :cond_148
    const-string v1, "carrier_enabled"

    #@14a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14d
    move-result-object v0

    #@14e
    invoke-virtual {v11, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@151
    goto/16 :goto_c5

    #@153
    .line 469
    .restart local v6       #c:Landroid/database/Cursor;
    .restart local v9       #numOfResult:I
    :catch_153
    move-exception v7

    #@154
    .line 470
    .local v7, e:Ljava/lang/Exception;
    const-string v0, "VZWAPN"

    #@156
    new-instance v1, Ljava/lang/StringBuilder;

    #@158
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15b
    const-string v2, "Exception.SQL + "

    #@15d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v1

    #@161
    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@164
    move-result-object v2

    #@165
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v1

    #@169
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16c
    move-result-object v1

    #@16d
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@170
    goto/16 :goto_d6

    #@172
    .line 487
    .end local v7           #e:Ljava/lang/Exception;
    :cond_172
    sget-object v0, Lcom/android/internal/telephony/ApnSettingHelper;->mContext:Landroid/content/Context;

    #@174
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@177
    move-result-object v0

    #@178
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@17a
    invoke-virtual {v0, v1, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@17d
    move-result-object v8

    #@17e
    .line 489
    .local v8, newUrl:Landroid/net/Uri;
    if-eqz v8, :cond_11f

    #@180
    .line 490
    const-string v0, "VZWAPN"

    #@182
    new-instance v1, Ljava/lang/StringBuilder;

    #@184
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@187
    const-string v2, "New Added APN is "

    #@189
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18c
    move-result-object v1

    #@18d
    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@190
    move-result-object v2

    #@191
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v1

    #@195
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v1

    #@199
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19c
    goto :goto_11f
.end method
