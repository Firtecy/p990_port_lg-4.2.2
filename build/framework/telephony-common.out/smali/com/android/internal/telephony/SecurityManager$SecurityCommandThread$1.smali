.class Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;
.super Landroid/os/Handler;
.source "SecurityManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 148
    iput-object p1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 153
    iget v1, p1, Landroid/os/Message;->what:I

    #@3
    sparse-switch v1, :sswitch_data_180

    #@6
    .line 224
    const-string v1, "SecurityManager"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "handleMessage: unexpected message code: "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    iget v3, p1, Landroid/os/Message;->what:I

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 227
    :goto_20
    return-void

    #@21
    .line 155
    :sswitch_21
    const-string v1, "SecurityManager"

    #@23
    const-string v2, "handleMessage : SECURITY_PROCESS_COMMAND_PERSONALIZATION_INFORMATION"

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 156
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2a
    check-cast v0, Landroid/os/AsyncResult;

    #@2c
    .line 157
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2e
    if-nez v1, :cond_4f

    #@30
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@32
    if-eqz v1, :cond_4f

    #@34
    .line 158
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@36
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@38
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3a
    iput-object v2, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@3c
    .line 164
    :goto_3c
    iget-object v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@3e
    monitor-enter v2

    #@3f
    .line 166
    :try_start_3f
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@41
    const/4 v3, 0x1

    #@42
    invoke-static {v1, v3}, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->access$102(Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;Z)Z

    #@45
    .line 167
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@47
    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    #@4a
    .line 168
    monitor-exit v2

    #@4b
    goto :goto_20

    #@4c
    :catchall_4c
    move-exception v1

    #@4d
    monitor-exit v2
    :try_end_4e
    .catchall {:try_start_3f .. :try_end_4e} :catchall_4c

    #@4e
    throw v1

    #@4f
    .line 160
    :cond_4f
    const-string v1, "SecurityManager"

    #@51
    const-string v2, "handleMessage: Faile"

    #@53
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 161
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@58
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@5a
    iput-object v3, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@5c
    goto :goto_3c

    #@5d
    .line 171
    .end local v0           #ar:Landroid/os/AsyncResult;
    :sswitch_5d
    const-string v1, "SecurityManager"

    #@5f
    const-string v2, "handleMessage : SECURITY_PROCESS_COMMAND_DEPERSONALIZATION"

    #@61
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 172
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@66
    check-cast v0, Landroid/os/AsyncResult;

    #@68
    .line 173
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@6a
    if-nez v1, :cond_b0

    #@6c
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@6e
    if-eqz v1, :cond_b0

    #@70
    .line 174
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@72
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@74
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@76
    iput-object v2, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@78
    .line 175
    const-string v1, "SecurityManager"

    #@7a
    new-instance v2, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v3, "handleMessage : SECURITY_PROCESS_COMMAND_DEPERSONALIZATION("

    #@81
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v2

    #@85
    iget-object v3, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@87
    iget-object v3, v3, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@89
    iget-object v3, v3, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@8b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v2

    #@8f
    const-string v3, ")"

    #@91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v2

    #@95
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v2

    #@99
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 181
    :goto_9c
    iget-object v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@9e
    monitor-enter v2

    #@9f
    .line 183
    :try_start_9f
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@a1
    const/4 v3, 0x1

    #@a2
    invoke-static {v1, v3}, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->access$102(Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;Z)Z

    #@a5
    .line 184
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@a7
    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    #@aa
    .line 185
    monitor-exit v2

    #@ab
    goto/16 :goto_20

    #@ad
    :catchall_ad
    move-exception v1

    #@ae
    monitor-exit v2
    :try_end_af
    .catchall {:try_start_9f .. :try_end_af} :catchall_ad

    #@af
    throw v1

    #@b0
    .line 177
    :cond_b0
    const-string v1, "SecurityManager"

    #@b2
    const-string v2, "handleMessage: Faile"

    #@b4
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    .line 178
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@b9
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@bb
    iput-object v3, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@bd
    goto :goto_9c

    #@be
    .line 188
    .end local v0           #ar:Landroid/os/AsyncResult;
    :sswitch_be
    const-string v1, "SecurityManager"

    #@c0
    const-string v2, "handleMessage : SECURITY_PROCESS_COMMAND_PERSONALIZATION"

    #@c2
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    .line 189
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c7
    check-cast v0, Landroid/os/AsyncResult;

    #@c9
    .line 190
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@cb
    if-nez v1, :cond_111

    #@cd
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@cf
    if-eqz v1, :cond_111

    #@d1
    .line 191
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@d3
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@d5
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@d7
    iput-object v2, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@d9
    .line 192
    const-string v1, "SecurityManager"

    #@db
    new-instance v2, Ljava/lang/StringBuilder;

    #@dd
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e0
    const-string v3, "handleMessage : SECURITY_PROCESS_COMMAND_PERSONALIZATION("

    #@e2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v2

    #@e6
    iget-object v3, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@e8
    iget-object v3, v3, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@ea
    iget-object v3, v3, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@ec
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v2

    #@f0
    const-string v3, ")"

    #@f2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v2

    #@f6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v2

    #@fa
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@fd
    .line 198
    :goto_fd
    iget-object v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@ff
    monitor-enter v2

    #@100
    .line 200
    :try_start_100
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@102
    const/4 v3, 0x1

    #@103
    invoke-static {v1, v3}, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->access$102(Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;Z)Z

    #@106
    .line 201
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@108
    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    #@10b
    .line 202
    monitor-exit v2

    #@10c
    goto/16 :goto_20

    #@10e
    :catchall_10e
    move-exception v1

    #@10f
    monitor-exit v2
    :try_end_110
    .catchall {:try_start_100 .. :try_end_110} :catchall_10e

    #@110
    throw v1

    #@111
    .line 194
    :cond_111
    const-string v1, "SecurityManager"

    #@113
    const-string v2, "handleMessage: Faile"

    #@115
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@118
    .line 195
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@11a
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@11c
    iput-object v3, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@11e
    goto :goto_fd

    #@11f
    .line 206
    .end local v0           #ar:Landroid/os/AsyncResult;
    :sswitch_11f
    const-string v1, "SecurityManager"

    #@121
    const-string v2, "handleMessage : SECURITY_PROCESS_COMMAND_DEBUG_ENABLE"

    #@123
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@126
    .line 207
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@128
    check-cast v0, Landroid/os/AsyncResult;

    #@12a
    .line 208
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@12c
    if-nez v1, :cond_172

    #@12e
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@130
    if-eqz v1, :cond_172

    #@132
    .line 209
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@134
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@136
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@138
    iput-object v2, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@13a
    .line 210
    const-string v1, "SecurityManager"

    #@13c
    new-instance v2, Ljava/lang/StringBuilder;

    #@13e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@141
    const-string v3, "handleMessage : SECURITY_PROCESS_COMMAND_DEBUG_ENABLE("

    #@143
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v2

    #@147
    iget-object v3, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@149
    iget-object v3, v3, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@14b
    iget-object v3, v3, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@14d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v2

    #@151
    const-string v3, ")"

    #@153
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v2

    #@157
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v2

    #@15b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15e
    .line 216
    :goto_15e
    iget-object v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@160
    monitor-enter v2

    #@161
    .line 218
    :try_start_161
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@163
    const/4 v3, 0x1

    #@164
    invoke-static {v1, v3}, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->access$102(Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;Z)Z

    #@167
    .line 219
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@169
    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    #@16c
    .line 220
    monitor-exit v2

    #@16d
    goto/16 :goto_20

    #@16f
    :catchall_16f
    move-exception v1

    #@170
    monitor-exit v2
    :try_end_171
    .catchall {:try_start_161 .. :try_end_171} :catchall_16f

    #@171
    throw v1

    #@172
    .line 212
    :cond_172
    const-string v1, "SecurityManager"

    #@174
    const-string v2, " handleMessage: Faile"

    #@176
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@179
    .line 213
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;->this$0:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@17b
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@17d
    iput-object v3, v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@17f
    goto :goto_15e

    #@180
    .line 153
    :sswitch_data_180
    .sparse-switch
        0x1 -> :sswitch_21
        0x2 -> :sswitch_5d
        0x3 -> :sswitch_be
        0xe -> :sswitch_11f
    .end sparse-switch
.end method
