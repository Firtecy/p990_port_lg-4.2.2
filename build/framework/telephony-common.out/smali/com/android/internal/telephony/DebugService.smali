.class public Lcom/android/internal/telephony/DebugService;
.super Ljava/lang/Object;
.source "DebugService.java"


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 32
    const-string v0, "DebugService"

    #@2
    sput-object v0, Lcom/android/internal/telephony/DebugService;->TAG:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 36
    const-string v0, "DebugService:"

    #@5
    invoke-static {v0}, Lcom/android/internal/telephony/DebugService;->log(Ljava/lang/String;)V

    #@8
    .line 37
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 106
    sget-object v0, Lcom/android/internal/telephony/DebugService;->TAG:Ljava/lang/String;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "DebugService "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 107
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 10
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 43
    const-string v4, "dump: +"

    #@2
    invoke-static {v4}, Lcom/android/internal/telephony/DebugService;->log(Ljava/lang/String;)V

    #@5
    .line 44
    const/4 v3, 0x0

    #@6
    .line 45
    .local v3, phoneProxy:Lcom/android/internal/telephony/PhoneProxy;
    const/4 v2, 0x0

    #@7
    .line 48
    .local v2, phoneBase:Lcom/android/internal/telephony/PhoneBase;
    :try_start_7
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    #@a
    move-result-object v4

    #@b
    move-object v0, v4

    #@c
    check-cast v0, Lcom/android/internal/telephony/PhoneProxy;

    #@e
    move-object v3, v0
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_f} :catch_6d

    #@f
    .line 54
    :try_start_f
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    #@12
    move-result-object v4

    #@13
    move-object v0, v4

    #@14
    check-cast v0, Lcom/android/internal/telephony/PhoneBase;

    #@16
    move-object v2, v0
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_17} :catch_85

    #@17
    .line 64
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@1a
    .line 65
    const-string v4, "++++++++++++++++++++++++++++++++"

    #@1c
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1f
    .line 66
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@22
    .line 68
    :try_start_22
    invoke-virtual {v2, p1, p2, p3}, Lcom/android/internal/telephony/PhoneBase;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_25} :catch_9d

    #@25
    .line 72
    :goto_25
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@28
    .line 73
    const-string v4, "++++++++++++++++++++++++++++++++"

    #@2a
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2d
    .line 75
    :try_start_2d
    iget-object v4, v2, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2f
    invoke-virtual {v4, p1, p2, p3}, Lcom/android/internal/telephony/DataConnectionTracker;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_32} :catch_a2

    #@32
    .line 79
    :goto_32
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@35
    .line 80
    const-string v4, "++++++++++++++++++++++++++++++++"

    #@37
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3a
    .line 82
    :try_start_3a
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, p1, p2, p3}, Lcom/android/internal/telephony/ServiceStateTracker;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_3a .. :try_end_41} :catch_a7

    #@41
    .line 86
    :goto_41
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@44
    .line 87
    const-string v4, "++++++++++++++++++++++++++++++++"

    #@46
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@49
    .line 89
    :try_start_49
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4, p1, p2, p3}, Lcom/android/internal/telephony/CallTracker;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_50
    .catch Ljava/lang/Exception; {:try_start_49 .. :try_end_50} :catch_ac

    #@50
    .line 93
    :goto_50
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@53
    .line 94
    const-string v4, "++++++++++++++++++++++++++++++++"

    #@55
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@58
    .line 96
    :try_start_58
    iget-object v4, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5a
    check-cast v4, Lcom/android/internal/telephony/RIL;

    #@5c
    invoke-virtual {v4, p1, p2, p3}, Lcom/android/internal/telephony/RIL;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_5f
    .catch Ljava/lang/Exception; {:try_start_58 .. :try_end_5f} :catch_b1

    #@5f
    .line 100
    :goto_5f
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@62
    .line 101
    const-string v4, "++++++++++++++++++++++++++++++++"

    #@64
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@67
    .line 102
    const-string v4, "dump: -"

    #@69
    invoke-static {v4}, Lcom/android/internal/telephony/DebugService;->log(Ljava/lang/String;)V

    #@6c
    .line 103
    :goto_6c
    return-void

    #@6d
    .line 49
    :catch_6d
    move-exception v1

    #@6e
    .line 50
    .local v1, e:Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v5, "Telephony DebugService: Could not getDefaultPhone e="

    #@75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v4

    #@79
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v4

    #@81
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@84
    goto :goto_6c

    #@85
    .line 55
    .end local v1           #e:Ljava/lang/Exception;
    :catch_85
    move-exception v1

    #@86
    .line 56
    .restart local v1       #e:Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v5, "Telephony DebugService: Could not PhoneBase e="

    #@8d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v4

    #@99
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9c
    goto :goto_6c

    #@9d
    .line 69
    .end local v1           #e:Ljava/lang/Exception;
    :catch_9d
    move-exception v1

    #@9e
    .line 70
    .restart local v1       #e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@a1
    goto :goto_25

    #@a2
    .line 76
    .end local v1           #e:Ljava/lang/Exception;
    :catch_a2
    move-exception v1

    #@a3
    .line 77
    .restart local v1       #e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@a6
    goto :goto_32

    #@a7
    .line 83
    .end local v1           #e:Ljava/lang/Exception;
    :catch_a7
    move-exception v1

    #@a8
    .line 84
    .restart local v1       #e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@ab
    goto :goto_41

    #@ac
    .line 90
    .end local v1           #e:Ljava/lang/Exception;
    :catch_ac
    move-exception v1

    #@ad
    .line 91
    .restart local v1       #e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@b0
    goto :goto_50

    #@b1
    .line 97
    .end local v1           #e:Ljava/lang/Exception;
    :catch_b1
    move-exception v1

    #@b2
    .line 98
    .restart local v1       #e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@b5
    goto :goto_5f
.end method
