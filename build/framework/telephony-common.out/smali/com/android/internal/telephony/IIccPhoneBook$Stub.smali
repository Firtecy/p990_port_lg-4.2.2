.class public abstract Lcom/android/internal/telephony/IIccPhoneBook$Stub;
.super Landroid/os/Binder;
.source "IIccPhoneBook.java"

# interfaces
.implements Lcom/android/internal/telephony/IIccPhoneBook;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IIccPhoneBook;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/IIccPhoneBook$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.IIccPhoneBook"

.field static final TRANSACTION_getAdnRecordsInEf:I = 0x1

.field static final TRANSACTION_getAdnRecordsSize:I = 0x4

.field static final TRANSACTION_updateAdnRecordsInEfByIndex:I = 0x3

.field static final TRANSACTION_updateAdnRecordsInEfBySearch:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 27
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 28
    const-string v0, "com.android.internal.telephony.IIccPhoneBook"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 29
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IIccPhoneBook;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 36
    if-nez p0, :cond_4

    #@2
    .line 37
    const/4 v0, 0x0

    #@3
    .line 43
    :goto_3
    return-object v0

    #@4
    .line 39
    :cond_4
    const-string v1, "com.android.internal.telephony.IIccPhoneBook"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 40
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/IIccPhoneBook;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 41
    check-cast v0, Lcom/android/internal/telephony/IIccPhoneBook;

    #@12
    goto :goto_3

    #@13
    .line 43
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/IIccPhoneBook$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/IIccPhoneBook$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 47
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 16
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 51
    sparse-switch p1, :sswitch_data_90

    #@5
    .line 117
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v9

    #@9
    :goto_9
    return v9

    #@a
    .line 55
    :sswitch_a
    const-string v0, "com.android.internal.telephony.IIccPhoneBook"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 60
    :sswitch_10
    const-string v0, "com.android.internal.telephony.IIccPhoneBook"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v1

    #@19
    .line 63
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->getAdnRecordsInEf(I)Ljava/util/List;

    #@1c
    move-result-object v8

    #@1d
    .line 64
    .local v8, _result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20
    .line 65
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@23
    goto :goto_9

    #@24
    .line 70
    .end local v1           #_arg0:I
    .end local v8           #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    :sswitch_24
    const-string v0, "com.android.internal.telephony.IIccPhoneBook"

    #@26
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29
    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v1

    #@2d
    .line 74
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    .line 76
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    .line 78
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    .line 80
    .local v4, _arg3:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    .line 82
    .local v5, _arg4:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@40
    move-result-object v6

    #@41
    .local v6, _arg5:Ljava/lang/String;
    move-object v0, p0

    #@42
    .line 83
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->updateAdnRecordsInEfBySearch(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@45
    move-result v7

    #@46
    .line 84
    .local v7, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@49
    .line 85
    if-eqz v7, :cond_50

    #@4b
    move v0, v9

    #@4c
    :goto_4c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4f
    goto :goto_9

    #@50
    :cond_50
    move v0, v10

    #@51
    goto :goto_4c

    #@52
    .line 90
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:Ljava/lang/String;
    .end local v6           #_arg5:Ljava/lang/String;
    .end local v7           #_result:Z
    :sswitch_52
    const-string v0, "com.android.internal.telephony.IIccPhoneBook"

    #@54
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@57
    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5a
    move-result v1

    #@5b
    .line 94
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    .line 96
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    .line 98
    .restart local v3       #_arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@66
    move-result v4

    #@67
    .line 100
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6a
    move-result-object v5

    #@6b
    .restart local v5       #_arg4:Ljava/lang/String;
    move-object v0, p0

    #@6c
    .line 101
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->updateAdnRecordsInEfByIndex(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z

    #@6f
    move-result v7

    #@70
    .line 102
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@73
    .line 103
    if-eqz v7, :cond_76

    #@75
    move v10, v9

    #@76
    :cond_76
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@79
    goto :goto_9

    #@7a
    .line 108
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:I
    .end local v5           #_arg4:Ljava/lang/String;
    .end local v7           #_result:Z
    :sswitch_7a
    const-string v0, "com.android.internal.telephony.IIccPhoneBook"

    #@7c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7f
    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@82
    move-result v1

    #@83
    .line 111
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->getAdnRecordsSize(I)[I

    #@86
    move-result-object v7

    #@87
    .line 112
    .local v7, _result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8a
    .line 113
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeIntArray([I)V

    #@8d
    goto/16 :goto_9

    #@8f
    .line 51
    nop

    #@90
    :sswitch_data_90
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_24
        0x3 -> :sswitch_52
        0x4 -> :sswitch_7a
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
