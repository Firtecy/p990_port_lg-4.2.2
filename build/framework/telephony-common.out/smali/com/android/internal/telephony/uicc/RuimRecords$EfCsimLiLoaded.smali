.class Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;
.super Ljava/lang/Object;
.source "RuimRecords.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/RuimRecords;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EfCsimLiLoaded"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/uicc/RuimRecords;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/uicc/RuimRecords;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 333
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/uicc/RuimRecords;Lcom/android/internal/telephony/uicc/RuimRecords$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 333
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;)V

    #@3
    return-void
.end method


# virtual methods
.method public getEfName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 335
    const-string v0, "EF_CSIM_LI"

    #@2
    return-object v0
.end method

.method public onRecordLoaded(Landroid/os/AsyncResult;)V
    .registers 9
    .parameter "ar"

    #@0
    .prologue
    const/16 v6, 0x68

    #@2
    const/16 v5, 0x20

    #@4
    const/16 v4, 0x65

    #@6
    .line 339
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@8
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@a
    check-cast v1, [B

    #@c
    check-cast v1, [B

    #@e
    invoke-static {v2, v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$102(Lcom/android/internal/telephony/uicc/RuimRecords;[B)[B

    #@11
    .line 341
    const/4 v0, 0x0

    #@12
    .local v0, i:I
    :goto_12
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@14
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@17
    move-result-object v1

    #@18
    array-length v1, v1

    #@19
    if-ge v0, v1, :cond_d6

    #@1b
    .line 342
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@1d
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@20
    move-result-object v1

    #@21
    add-int/lit8 v2, v0, 0x1

    #@23
    aget-byte v1, v1, v2

    #@25
    packed-switch v1, :pswitch_data_fa

    #@28
    .line 350
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@2a
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@2d
    move-result-object v1

    #@2e
    aput-byte v5, v1, v0

    #@30
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@32
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@35
    move-result-object v1

    #@36
    add-int/lit8 v2, v0, 0x1

    #@38
    aput-byte v5, v1, v2

    #@3a
    .line 341
    :goto_3a
    add-int/lit8 v0, v0, 0x2

    #@3c
    goto :goto_12

    #@3d
    .line 343
    :pswitch_3d
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@3f
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@42
    move-result-object v1

    #@43
    aput-byte v4, v1, v0

    #@45
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@47
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@4a
    move-result-object v1

    #@4b
    add-int/lit8 v2, v0, 0x1

    #@4d
    const/16 v3, 0x6e

    #@4f
    aput-byte v3, v1, v2

    #@51
    goto :goto_3a

    #@52
    .line 344
    :pswitch_52
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@54
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@57
    move-result-object v1

    #@58
    const/16 v2, 0x66

    #@5a
    aput-byte v2, v1, v0

    #@5c
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@5e
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@61
    move-result-object v1

    #@62
    add-int/lit8 v2, v0, 0x1

    #@64
    const/16 v3, 0x72

    #@66
    aput-byte v3, v1, v2

    #@68
    goto :goto_3a

    #@69
    .line 345
    :pswitch_69
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@6b
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@6e
    move-result-object v1

    #@6f
    aput-byte v4, v1, v0

    #@71
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@73
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@76
    move-result-object v1

    #@77
    add-int/lit8 v2, v0, 0x1

    #@79
    const/16 v3, 0x73

    #@7b
    aput-byte v3, v1, v2

    #@7d
    goto :goto_3a

    #@7e
    .line 346
    :pswitch_7e
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@80
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@83
    move-result-object v1

    #@84
    const/16 v2, 0x6a

    #@86
    aput-byte v2, v1, v0

    #@88
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@8a
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@8d
    move-result-object v1

    #@8e
    add-int/lit8 v2, v0, 0x1

    #@90
    const/16 v3, 0x61

    #@92
    aput-byte v3, v1, v2

    #@94
    goto :goto_3a

    #@95
    .line 347
    :pswitch_95
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@97
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@9a
    move-result-object v1

    #@9b
    const/16 v2, 0x6b

    #@9d
    aput-byte v2, v1, v0

    #@9f
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@a1
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@a4
    move-result-object v1

    #@a5
    add-int/lit8 v2, v0, 0x1

    #@a7
    const/16 v3, 0x6f

    #@a9
    aput-byte v3, v1, v2

    #@ab
    goto :goto_3a

    #@ac
    .line 348
    :pswitch_ac
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@ae
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@b1
    move-result-object v1

    #@b2
    const/16 v2, 0x7a

    #@b4
    aput-byte v2, v1, v0

    #@b6
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@b8
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@bb
    move-result-object v1

    #@bc
    add-int/lit8 v2, v0, 0x1

    #@be
    aput-byte v6, v1, v2

    #@c0
    goto/16 :goto_3a

    #@c2
    .line 349
    :pswitch_c2
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@c4
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@c7
    move-result-object v1

    #@c8
    aput-byte v6, v1, v0

    #@ca
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@cc
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@cf
    move-result-object v1

    #@d0
    add-int/lit8 v2, v0, 0x1

    #@d2
    aput-byte v4, v1, v2

    #@d4
    goto/16 :goto_3a

    #@d6
    .line 354
    :cond_d6
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@d8
    new-instance v2, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v3, "EF_LI="

    #@df
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v2

    #@e3
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@e5
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B

    #@e8
    move-result-object v3

    #@e9
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@ec
    move-result-object v3

    #@ed
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v2

    #@f1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v2

    #@f5
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@f8
    .line 355
    return-void

    #@f9
    .line 342
    nop

    #@fa
    :pswitch_data_fa
    .packed-switch 0x1
        :pswitch_3d
        :pswitch_52
        :pswitch_69
        :pswitch_7e
        :pswitch_95
        :pswitch_ac
        :pswitch_c2
    .end packed-switch
.end method
