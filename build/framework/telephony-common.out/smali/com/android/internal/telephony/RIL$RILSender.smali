.class Lcom/android/internal/telephony/RIL$RILSender;
.super Landroid/os/Handler;
.source "RIL.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/RIL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RILSender"
.end annotation


# instance fields
.field dataLength:[B

.field final synthetic this$0:Lcom/android/internal/telephony/RIL;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/RIL;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 422
    iput-object p1, p0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@2
    .line 423
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 427
    const/4 v0, 0x4

    #@6
    new-array v0, v0, [B

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/RIL$RILSender;->dataLength:[B

    #@a
    .line 424
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 18
    .parameter "msg"

    #@0
    .prologue
    .line 439
    move-object/from16 v0, p1

    #@2
    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4
    check-cast v10, Lcom/android/internal/telephony/RILRequest;

    #@6
    move-object v8, v10

    #@7
    check-cast v8, Lcom/android/internal/telephony/RILRequest;

    #@9
    .line 440
    .local v8, rr:Lcom/android/internal/telephony/RILRequest;
    const/4 v7, 0x0

    #@a
    .line 442
    .local v7, req:Lcom/android/internal/telephony/RILRequest;
    move-object/from16 v0, p1

    #@c
    iget v10, v0, Landroid/os/Message;->what:I

    #@e
    packed-switch v10, :pswitch_data_278

    #@11
    .line 601
    :cond_11
    :goto_11
    return-void

    #@12
    .line 449
    :pswitch_12
    const/4 v1, 0x0

    #@13
    .line 453
    .local v1, alreadySubtracted:Z
    :try_start_13
    move-object/from16 v0, p0

    #@15
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@17
    iget-object v9, v10, Lcom/android/internal/telephony/RIL;->mSocket:Landroid/net/LocalSocket;

    #@19
    .line 455
    .local v9, s:Landroid/net/LocalSocket;
    if-nez v9, :cond_3e

    #@1b
    .line 456
    const/4 v10, 0x1

    #@1c
    const/4 v11, 0x0

    #@1d
    invoke-virtual {v8, v10, v11}, Lcom/android/internal/telephony/RILRequest;->onError(ILjava/lang/Object;)V

    #@20
    .line 457
    invoke-virtual {v8}, Lcom/android/internal/telephony/RILRequest;->release()V

    #@23
    .line 458
    move-object/from16 v0, p0

    #@25
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@27
    iget v10, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I

    #@29
    if-lez v10, :cond_35

    #@2b
    .line 459
    move-object/from16 v0, p0

    #@2d
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@2f
    iget v11, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I

    #@31
    add-int/lit8 v11, v11, -0x1

    #@33
    iput v11, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I
    :try_end_35
    .catchall {:try_start_13 .. :try_end_35} :catchall_138
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_35} :catch_99
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_35} :catch_d7

    #@35
    .line 460
    :cond_35
    const/4 v1, 0x1

    #@36
    .line 516
    move-object/from16 v0, p0

    #@38
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@3a
    invoke-static {v10}, Lcom/android/internal/telephony/RIL;->access$300(Lcom/android/internal/telephony/RIL;)V

    #@3d
    goto :goto_11

    #@3e
    .line 464
    :cond_3e
    :try_start_3e
    move-object/from16 v0, p0

    #@40
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@42
    iget-object v11, v10, Lcom/android/internal/telephony/RIL;->mRequestsList:Ljava/util/ArrayList;

    #@44
    monitor-enter v11
    :try_end_45
    .catchall {:try_start_3e .. :try_end_45} :catchall_138
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_45} :catch_99
    .catch Ljava/lang/RuntimeException; {:try_start_3e .. :try_end_45} :catch_d7

    #@45
    .line 465
    :try_start_45
    move-object/from16 v0, p0

    #@47
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@49
    iget-object v10, v10, Lcom/android/internal/telephony/RIL;->mRequestsList:Ljava/util/ArrayList;

    #@4b
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4e
    .line 466
    move-object/from16 v0, p0

    #@50
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@52
    iget v12, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesWaiting:I

    #@54
    add-int/lit8 v12, v12, 0x1

    #@56
    iput v12, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesWaiting:I

    #@58
    .line 467
    monitor-exit v11
    :try_end_59
    .catchall {:try_start_45 .. :try_end_59} :catchall_d4

    #@59
    .line 469
    :try_start_59
    move-object/from16 v0, p0

    #@5b
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@5d
    iget v10, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I

    #@5f
    if-lez v10, :cond_6b

    #@61
    .line 470
    move-object/from16 v0, p0

    #@63
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@65
    iget v11, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I

    #@67
    add-int/lit8 v11, v11, -0x1

    #@69
    iput v11, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I

    #@6b
    .line 471
    :cond_6b
    const/4 v1, 0x1

    #@6c
    .line 475
    iget-object v10, v8, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@6e
    invoke-virtual {v10}, Landroid/os/Parcel;->marshall()[B

    #@71
    move-result-object v3

    #@72
    .line 476
    .local v3, data:[B
    iget-object v10, v8, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@74
    invoke-virtual {v10}, Landroid/os/Parcel;->recycle()V

    #@77
    .line 477
    const/4 v10, 0x0

    #@78
    iput-object v10, v8, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@7a
    .line 479
    array-length v10, v3

    #@7b
    const/16 v11, 0x2000

    #@7d
    if-le v10, v11, :cond_fa

    #@7f
    .line 480
    new-instance v10, Ljava/lang/RuntimeException;

    #@81
    new-instance v11, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v12, "Parcel larger than max bytes allowed! "

    #@88
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v11

    #@8c
    array-length v12, v3

    #@8d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v11

    #@91
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v11

    #@95
    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@98
    throw v10
    :try_end_99
    .catchall {:try_start_59 .. :try_end_99} :catchall_138
    .catch Ljava/io/IOException; {:try_start_59 .. :try_end_99} :catch_99
    .catch Ljava/lang/RuntimeException; {:try_start_59 .. :try_end_99} :catch_d7

    #@99
    .line 494
    .end local v3           #data:[B
    .end local v9           #s:Landroid/net/LocalSocket;
    :catch_99
    move-exception v4

    #@9a
    .line 495
    .local v4, ex:Ljava/io/IOException;
    :try_start_9a
    const-string v10, "RILJ"

    #@9c
    const-string v11, "IOException"

    #@9e
    invoke-static {v10, v11, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a1
    .line 496
    move-object/from16 v0, p0

    #@a3
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@a5
    iget v11, v8, Lcom/android/internal/telephony/RILRequest;->mSerial:I

    #@a7
    invoke-static {v10, v11}, Lcom/android/internal/telephony/RIL;->access$200(Lcom/android/internal/telephony/RIL;I)Lcom/android/internal/telephony/RILRequest;

    #@aa
    move-result-object v7

    #@ab
    .line 499
    if-nez v7, :cond_af

    #@ad
    if-nez v1, :cond_b7

    #@af
    .line 500
    :cond_af
    const/4 v10, 0x1

    #@b0
    const/4 v11, 0x0

    #@b1
    invoke-virtual {v8, v10, v11}, Lcom/android/internal/telephony/RILRequest;->onError(ILjava/lang/Object;)V

    #@b4
    .line 501
    invoke-virtual {v8}, Lcom/android/internal/telephony/RILRequest;->release()V
    :try_end_b7
    .catchall {:try_start_9a .. :try_end_b7} :catchall_138

    #@b7
    .line 516
    :cond_b7
    move-object/from16 v0, p0

    #@b9
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@bb
    .end local v4           #ex:Ljava/io/IOException;
    :goto_bb
    invoke-static {v10}, Lcom/android/internal/telephony/RIL;->access$300(Lcom/android/internal/telephony/RIL;)V

    #@be
    .line 519
    if-nez v1, :cond_11

    #@c0
    move-object/from16 v0, p0

    #@c2
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@c4
    iget v10, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I

    #@c6
    if-lez v10, :cond_11

    #@c8
    .line 520
    move-object/from16 v0, p0

    #@ca
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@cc
    iget v11, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I

    #@ce
    add-int/lit8 v11, v11, -0x1

    #@d0
    iput v11, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I

    #@d2
    goto/16 :goto_11

    #@d4
    .line 467
    .restart local v9       #s:Landroid/net/LocalSocket;
    :catchall_d4
    move-exception v10

    #@d5
    :try_start_d5
    monitor-exit v11
    :try_end_d6
    .catchall {:try_start_d5 .. :try_end_d6} :catchall_d4

    #@d6
    :try_start_d6
    throw v10
    :try_end_d7
    .catchall {:try_start_d6 .. :try_end_d7} :catchall_138
    .catch Ljava/io/IOException; {:try_start_d6 .. :try_end_d7} :catch_99
    .catch Ljava/lang/RuntimeException; {:try_start_d6 .. :try_end_d7} :catch_d7

    #@d7
    .line 503
    .end local v9           #s:Landroid/net/LocalSocket;
    :catch_d7
    move-exception v5

    #@d8
    .line 504
    .local v5, exc:Ljava/lang/RuntimeException;
    :try_start_d8
    const-string v10, "RILJ"

    #@da
    const-string v11, "Uncaught exception "

    #@dc
    invoke-static {v10, v11, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@df
    .line 505
    move-object/from16 v0, p0

    #@e1
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@e3
    iget v11, v8, Lcom/android/internal/telephony/RILRequest;->mSerial:I

    #@e5
    invoke-static {v10, v11}, Lcom/android/internal/telephony/RIL;->access$200(Lcom/android/internal/telephony/RIL;I)Lcom/android/internal/telephony/RILRequest;

    #@e8
    move-result-object v7

    #@e9
    .line 508
    if-nez v7, :cond_ed

    #@eb
    if-nez v1, :cond_f5

    #@ed
    .line 509
    :cond_ed
    const/4 v10, 0x2

    #@ee
    const/4 v11, 0x0

    #@ef
    invoke-virtual {v8, v10, v11}, Lcom/android/internal/telephony/RILRequest;->onError(ILjava/lang/Object;)V

    #@f2
    .line 510
    invoke-virtual {v8}, Lcom/android/internal/telephony/RILRequest;->release()V
    :try_end_f5
    .catchall {:try_start_d8 .. :try_end_f5} :catchall_138

    #@f5
    .line 516
    :cond_f5
    move-object/from16 v0, p0

    #@f7
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@f9
    goto :goto_bb

    #@fa
    .line 486
    .end local v5           #exc:Ljava/lang/RuntimeException;
    .restart local v3       #data:[B
    .restart local v9       #s:Landroid/net/LocalSocket;
    :cond_fa
    :try_start_fa
    move-object/from16 v0, p0

    #@fc
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->dataLength:[B

    #@fe
    const/4 v11, 0x0

    #@ff
    move-object/from16 v0, p0

    #@101
    iget-object v12, v0, Lcom/android/internal/telephony/RIL$RILSender;->dataLength:[B

    #@103
    const/4 v13, 0x1

    #@104
    const/4 v14, 0x0

    #@105
    aput-byte v14, v12, v13

    #@107
    aput-byte v14, v10, v11

    #@109
    .line 487
    move-object/from16 v0, p0

    #@10b
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->dataLength:[B

    #@10d
    const/4 v11, 0x2

    #@10e
    array-length v12, v3

    #@10f
    shr-int/lit8 v12, v12, 0x8

    #@111
    and-int/lit16 v12, v12, 0xff

    #@113
    int-to-byte v12, v12

    #@114
    aput-byte v12, v10, v11

    #@116
    .line 488
    move-object/from16 v0, p0

    #@118
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->dataLength:[B

    #@11a
    const/4 v11, 0x3

    #@11b
    array-length v12, v3

    #@11c
    and-int/lit16 v12, v12, 0xff

    #@11e
    int-to-byte v12, v12

    #@11f
    aput-byte v12, v10, v11

    #@121
    .line 492
    invoke-virtual {v9}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@124
    move-result-object v10

    #@125
    move-object/from16 v0, p0

    #@127
    iget-object v11, v0, Lcom/android/internal/telephony/RIL$RILSender;->dataLength:[B

    #@129
    invoke-virtual {v10, v11}, Ljava/io/OutputStream;->write([B)V

    #@12c
    .line 493
    invoke-virtual {v9}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@12f
    move-result-object v10

    #@130
    invoke-virtual {v10, v3}, Ljava/io/OutputStream;->write([B)V
    :try_end_133
    .catchall {:try_start_fa .. :try_end_133} :catchall_138
    .catch Ljava/io/IOException; {:try_start_fa .. :try_end_133} :catch_99
    .catch Ljava/lang/RuntimeException; {:try_start_fa .. :try_end_133} :catch_d7

    #@133
    .line 516
    move-object/from16 v0, p0

    #@135
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@137
    goto :goto_bb

    #@138
    .end local v3           #data:[B
    .end local v9           #s:Landroid/net/LocalSocket;
    :catchall_138
    move-exception v10

    #@139
    move-object/from16 v0, p0

    #@13b
    iget-object v11, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@13d
    invoke-static {v11}, Lcom/android/internal/telephony/RIL;->access$300(Lcom/android/internal/telephony/RIL;)V

    #@140
    throw v10

    #@141
    .line 528
    .end local v1           #alreadySubtracted:Z
    :pswitch_141
    move-object/from16 v0, p0

    #@143
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@145
    iget-object v11, v10, Lcom/android/internal/telephony/RIL;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@147
    monitor-enter v11

    #@148
    .line 529
    :try_start_148
    move-object/from16 v0, p0

    #@14a
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@14c
    iget-object v10, v10, Lcom/android/internal/telephony/RIL;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@14e
    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@151
    move-result v10

    #@152
    if-eqz v10, :cond_26a

    #@154
    .line 539
    move-object/from16 v0, p0

    #@156
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@158
    iget v10, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesWaiting:I

    #@15a
    if-eqz v10, :cond_22e

    #@15c
    .line 540
    const-string v10, "RILJ"

    #@15e
    new-instance v12, Ljava/lang/StringBuilder;

    #@160
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@163
    const-string v13, "NOTE: mReqWaiting is NOT 0 but"

    #@165
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v12

    #@169
    move-object/from16 v0, p0

    #@16b
    iget-object v13, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@16d
    iget v13, v13, Lcom/android/internal/telephony/RIL;->mRequestMessagesWaiting:I

    #@16f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@172
    move-result-object v12

    #@173
    const-string v13, " at TIMEOUT, reset!"

    #@175
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@178
    move-result-object v12

    #@179
    const-string v13, " There still msg waitng for response"

    #@17b
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v12

    #@17f
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@182
    move-result-object v12

    #@183
    invoke-static {v10, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@186
    .line 544
    move-object/from16 v0, p0

    #@188
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@18a
    const/4 v12, 0x0

    #@18b
    iput v12, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesWaiting:I

    #@18d
    .line 547
    move-object/from16 v0, p0

    #@18f
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@191
    iget-object v12, v10, Lcom/android/internal/telephony/RIL;->mRequestsList:Ljava/util/ArrayList;

    #@193
    monitor-enter v12
    :try_end_194
    .catchall {:try_start_148 .. :try_end_194} :catchall_26d

    #@194
    .line 548
    :try_start_194
    move-object/from16 v0, p0

    #@196
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@198
    iget-object v10, v10, Lcom/android/internal/telephony/RIL;->mRequestsList:Ljava/util/ArrayList;

    #@19a
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@19d
    move-result v2

    #@19e
    .line 549
    .local v2, count:I
    const-string v10, "RILJ"

    #@1a0
    new-instance v13, Ljava/lang/StringBuilder;

    #@1a2
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1a5
    const-string v14, "WAKE_LOCK_TIMEOUT  mRequestList="

    #@1a7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v13

    #@1ab
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ae
    move-result-object v13

    #@1af
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b2
    move-result-object v13

    #@1b3
    invoke-static {v10, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b6
    .line 552
    const/4 v6, 0x0

    #@1b7
    .local v6, i:I
    :goto_1b7
    if-ge v6, v2, :cond_22d

    #@1b9
    .line 553
    move-object/from16 v0, p0

    #@1bb
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@1bd
    iget-object v10, v10, Lcom/android/internal/telephony/RIL;->mRequestsList:Ljava/util/ArrayList;

    #@1bf
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c2
    move-result-object v10

    #@1c3
    move-object v0, v10

    #@1c4
    check-cast v0, Lcom/android/internal/telephony/RILRequest;

    #@1c6
    move-object v8, v0

    #@1c7
    .line 554
    const-string v10, "RILJ"

    #@1c9
    new-instance v13, Ljava/lang/StringBuilder;

    #@1cb
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1ce
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v13

    #@1d2
    const-string v14, ": ["

    #@1d4
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v13

    #@1d8
    iget v14, v8, Lcom/android/internal/telephony/RILRequest;->mSerial:I

    #@1da
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1dd
    move-result-object v13

    #@1de
    const-string v14, "] "

    #@1e0
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e3
    move-result-object v13

    #@1e4
    iget v14, v8, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@1e6
    invoke-static {v14}, Lcom/android/internal/telephony/RIL;->requestToString(I)Ljava/lang/String;

    #@1e9
    move-result-object v14

    #@1ea
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ed
    move-result-object v13

    #@1ee
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f1
    move-result-object v13

    #@1f2
    invoke-static {v10, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f5
    .line 560
    move-object/from16 v0, p0

    #@1f7
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@1f9
    iget-object v10, v10, Lcom/android/internal/telephony/BaseCommands;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@1fb
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_DEACTIVATE_DATA_CALL_PENDING_RECOVERY:Z

    #@1fd
    const/4 v13, 0x1

    #@1fe
    if-ne v10, v13, :cond_270

    #@200
    .line 561
    iget v10, v8, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@202
    const/16 v13, 0x29

    #@204
    if-ne v10, v13, :cond_270

    #@206
    .line 565
    move-object/from16 v0, p0

    #@208
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@20a
    iget-object v10, v10, Lcom/android/internal/telephony/BaseCommands;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@20c
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@20e
    if-eqz v10, :cond_21c

    #@210
    .line 566
    move-object/from16 v0, p0

    #@212
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@214
    iget v13, v8, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@216
    iget v14, v8, Lcom/android/internal/telephony/RILRequest;->mSerial:I

    #@218
    const/4 v15, 0x0

    #@219
    invoke-static {v10, v13, v14, v15}, Lcom/android/internal/telephony/RIL;->access$400(Lcom/android/internal/telephony/RIL;IILjava/lang/Object;)V

    #@21c
    .line 570
    :cond_21c
    const/4 v10, 0x2

    #@21d
    const/4 v13, 0x0

    #@21e
    invoke-virtual {v8, v10, v13}, Lcom/android/internal/telephony/RILRequest;->onError(ILjava/lang/Object;)V

    #@221
    .line 571
    invoke-virtual {v8}, Lcom/android/internal/telephony/RILRequest;->release()V

    #@224
    .line 572
    move-object/from16 v0, p0

    #@226
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@228
    iget-object v10, v10, Lcom/android/internal/telephony/RIL;->mRequestsList:Ljava/util/ArrayList;

    #@22a
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@22d
    .line 580
    :cond_22d
    monitor-exit v12
    :try_end_22e
    .catchall {:try_start_194 .. :try_end_22e} :catchall_274

    #@22e
    .line 590
    .end local v2           #count:I
    .end local v6           #i:I
    :cond_22e
    :try_start_22e
    move-object/from16 v0, p0

    #@230
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@232
    iget v10, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I

    #@234
    if-eqz v10, :cond_261

    #@236
    .line 591
    const-string v10, "RILJ"

    #@238
    new-instance v12, Ljava/lang/StringBuilder;

    #@23a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@23d
    const-string v13, "ERROR: mReqPending is NOT 0 but"

    #@23f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@242
    move-result-object v12

    #@243
    move-object/from16 v0, p0

    #@245
    iget-object v13, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@247
    iget v13, v13, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I

    #@249
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24c
    move-result-object v12

    #@24d
    const-string v13, " at TIMEOUT, reset!"

    #@24f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@252
    move-result-object v12

    #@253
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@256
    move-result-object v12

    #@257
    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25a
    .line 593
    move-object/from16 v0, p0

    #@25c
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@25e
    const/4 v12, 0x0

    #@25f
    iput v12, v10, Lcom/android/internal/telephony/RIL;->mRequestMessagesPending:I

    #@261
    .line 596
    :cond_261
    move-object/from16 v0, p0

    #@263
    iget-object v10, v0, Lcom/android/internal/telephony/RIL$RILSender;->this$0:Lcom/android/internal/telephony/RIL;

    #@265
    iget-object v10, v10, Lcom/android/internal/telephony/RIL;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@267
    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->release()V

    #@26a
    .line 598
    :cond_26a
    monitor-exit v11

    #@26b
    goto/16 :goto_11

    #@26d
    :catchall_26d
    move-exception v10

    #@26e
    monitor-exit v11
    :try_end_26f
    .catchall {:try_start_22e .. :try_end_26f} :catchall_26d

    #@26f
    throw v10

    #@270
    .line 552
    .restart local v2       #count:I
    .restart local v6       #i:I
    :cond_270
    add-int/lit8 v6, v6, 0x1

    #@272
    goto/16 :goto_1b7

    #@274
    .line 580
    .end local v2           #count:I
    .end local v6           #i:I
    :catchall_274
    move-exception v10

    #@275
    :try_start_275
    monitor-exit v12
    :try_end_276
    .catchall {:try_start_275 .. :try_end_276} :catchall_274

    #@276
    :try_start_276
    throw v10
    :try_end_277
    .catchall {:try_start_276 .. :try_end_277} :catchall_26d

    #@277
    .line 442
    nop

    #@278
    :pswitch_data_278
    .packed-switch 0x1
        :pswitch_12
        :pswitch_141
    .end packed-switch
.end method

.method public run()V
    .registers 1

    #@0
    .prologue
    .line 433
    return-void
.end method
