.class public final Lcom/android/internal/telephony/PhoneStateIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PhoneStateIntentReceiver.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "PHONE"

.field private static final NOTIF_MAX:I = 0x20

.field private static final NOTIF_PHONE:I = 0x1

.field private static final NOTIF_SERVICE:I = 0x2

.field private static final NOTIF_SIGNAL:I = 0x4


# instance fields
.field private mAsuEventWhat:I

.field private mContext:Landroid/content/Context;

.field private mFilter:Landroid/content/IntentFilter;

.field private mLocationEventWhat:I

.field mPhoneState:Lcom/android/internal/telephony/PhoneConstants$State;

.field private mPhoneStateEventWhat:I

.field mServiceState:Landroid/telephony/ServiceState;

.field private mServiceStateEventWhat:I

.field mSignalStrength:Landroid/telephony/SignalStrength;

.field private mTarget:Landroid/os/Handler;

.field private mWants:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    .line 49
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mPhoneState:Lcom/android/internal/telephony/PhoneConstants$State;

    #@7
    .line 50
    new-instance v0, Landroid/telephony/ServiceState;

    #@9
    invoke-direct {v0}, Landroid/telephony/ServiceState;-><init>()V

    #@c
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mServiceState:Landroid/telephony/ServiceState;

    #@e
    .line 51
    new-instance v0, Landroid/telephony/SignalStrength;

    #@10
    invoke-direct {v0}, Landroid/telephony/SignalStrength;-><init>()V

    #@13
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@15
    .line 64
    new-instance v0, Landroid/content/IntentFilter;

    #@17
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@1a
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mFilter:Landroid/content/IntentFilter;

    #@1c
    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 3
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 68
    invoke-direct {p0}, Lcom/android/internal/telephony/PhoneStateIntentReceiver;-><init>()V

    #@3
    .line 69
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->setContext(Landroid/content/Context;)V

    #@6
    .line 70
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->setTarget(Landroid/os/Handler;)V

    #@9
    .line 71
    return-void
.end method


# virtual methods
.method public getNotifyPhoneCallState()Z
    .registers 2

    #@0
    .prologue
    .line 133
    iget v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getNotifyServiceState()Z
    .registers 2

    #@0
    .prologue
    .line 143
    iget v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getNotifySignalStrength()Z
    .registers 2

    #@0
    .prologue
    .line 153
    iget v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getPhoneState()Lcom/android/internal/telephony/PhoneConstants$State;
    .registers 3

    #@0
    .prologue
    .line 82
    iget v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-nez v0, :cond_e

    #@6
    .line 83
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "client must call notifyPhoneCallState(int)"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 86
    :cond_e
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mPhoneState:Lcom/android/internal/telephony/PhoneConstants$State;

    #@10
    return-object v0
.end method

.method public getServiceState()Landroid/telephony/ServiceState;
    .registers 3

    #@0
    .prologue
    .line 90
    iget v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-nez v0, :cond_e

    #@6
    .line 91
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "client must call notifyServiceState(int)"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 94
    :cond_e
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mServiceState:Landroid/telephony/ServiceState;

    #@10
    return-object v0
.end method

.method public getSignalStrengthDbm()I
    .registers 3

    #@0
    .prologue
    .line 119
    iget v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-nez v0, :cond_e

    #@6
    .line 120
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "client must call notifySignalStrength(int)"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 123
    :cond_e
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@10
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getDbm()I

    #@13
    move-result v0

    #@14
    return v0
.end method

.method public getSignalStrengthLevelAsu()I
    .registers 3

    #@0
    .prologue
    .line 104
    iget v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-nez v0, :cond_e

    #@6
    .line 105
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "client must call notifySignalStrength(int)"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 108
    :cond_e
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@10
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getAsuLevel()I

    #@13
    move-result v0

    #@14
    return v0
.end method

.method public notifyPhoneCallState(I)V
    .registers 4
    .parameter "eventWhat"

    #@0
    .prologue
    .line 127
    iget v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@2
    or-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@6
    .line 128
    iput p1, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mPhoneStateEventWhat:I

    #@8
    .line 129
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mFilter:Landroid/content/IntentFilter;

    #@a
    const-string v1, "android.intent.action.PHONE_STATE"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 130
    return-void
.end method

.method public notifyServiceState(I)V
    .registers 4
    .parameter "eventWhat"

    #@0
    .prologue
    .line 137
    iget v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@2
    or-int/lit8 v0, v0, 0x2

    #@4
    iput v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@6
    .line 138
    iput p1, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mServiceStateEventWhat:I

    #@8
    .line 139
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mFilter:Landroid/content/IntentFilter;

    #@a
    const-string v1, "android.intent.action.SERVICE_STATE"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 140
    return-void
.end method

.method public notifySignalStrength(I)V
    .registers 4
    .parameter "eventWhat"

    #@0
    .prologue
    .line 147
    iget v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@2
    or-int/lit8 v0, v0, 0x4

    #@4
    iput v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mWants:I

    #@6
    .line 148
    iput p1, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mAsuEventWhat:I

    #@8
    .line 149
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mFilter:Landroid/content/IntentFilter;

    #@a
    const-string v1, "android.intent.action.SIG_STR"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 150
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 166
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 169
    .local v0, action:Ljava/lang/String;
    :try_start_4
    const-string v4, "android.intent.action.SIG_STR"

    #@6
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_2e

    #@c
    .line 170
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@f
    move-result-object v4

    #@10
    invoke-static {v4}, Landroid/telephony/SignalStrength;->newFromBundle(Landroid/os/Bundle;)Landroid/telephony/SignalStrength;

    #@13
    move-result-object v4

    #@14
    iput-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@16
    .line 172
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mTarget:Landroid/os/Handler;

    #@18
    if-eqz v4, :cond_2d

    #@1a
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->getNotifySignalStrength()Z

    #@1d
    move-result v4

    #@1e
    if-eqz v4, :cond_2d

    #@20
    .line 173
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mTarget:Landroid/os/Handler;

    #@22
    iget v5, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mAsuEventWhat:I

    #@24
    invoke-static {v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@27
    move-result-object v2

    #@28
    .line 174
    .local v2, message:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mTarget:Landroid/os/Handler;

    #@2a
    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@2d
    .line 201
    .end local v2           #message:Landroid/os/Message;
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 176
    :cond_2e
    const-string v4, "android.intent.action.PHONE_STATE"

    #@30
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_7b

    #@36
    .line 179
    const-string v4, "state"

    #@38
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    .line 180
    .local v3, phoneState:Ljava/lang/String;
    const-class v4, Lcom/android/internal/telephony/PhoneConstants$State;

    #@3e
    invoke-static {v4, v3}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@41
    move-result-object v4

    #@42
    check-cast v4, Lcom/android/internal/telephony/PhoneConstants$State;

    #@44
    iput-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mPhoneState:Lcom/android/internal/telephony/PhoneConstants$State;

    #@46
    .line 183
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mTarget:Landroid/os/Handler;

    #@48
    if-eqz v4, :cond_2d

    #@4a
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->getNotifyPhoneCallState()Z

    #@4d
    move-result v4

    #@4e
    if-eqz v4, :cond_2d

    #@50
    .line 184
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mTarget:Landroid/os/Handler;

    #@52
    iget v5, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mPhoneStateEventWhat:I

    #@54
    invoke-static {v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@57
    move-result-object v2

    #@58
    .line 186
    .restart local v2       #message:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mTarget:Landroid/os/Handler;

    #@5a
    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_5d
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_5d} :catch_5e

    #@5d
    goto :goto_2d

    #@5e
    .line 197
    .end local v2           #message:Landroid/os/Message;
    .end local v3           #phoneState:Ljava/lang/String;
    :catch_5e
    move-exception v1

    #@5f
    .line 198
    .local v1, ex:Ljava/lang/Exception;
    const-string v4, "PHONE"

    #@61
    new-instance v5, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v6, "[PhoneStateIntentRecv] caught "

    #@68
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v5

    #@70
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v5

    #@74
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 199
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@7a
    goto :goto_2d

    #@7b
    .line 188
    .end local v1           #ex:Ljava/lang/Exception;
    :cond_7b
    :try_start_7b
    const-string v4, "android.intent.action.SERVICE_STATE"

    #@7d
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@80
    move-result v4

    #@81
    if-eqz v4, :cond_2d

    #@83
    .line 189
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@86
    move-result-object v4

    #@87
    invoke-static {v4}, Landroid/telephony/ServiceState;->newFromBundle(Landroid/os/Bundle;)Landroid/telephony/ServiceState;

    #@8a
    move-result-object v4

    #@8b
    iput-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mServiceState:Landroid/telephony/ServiceState;

    #@8d
    .line 191
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mTarget:Landroid/os/Handler;

    #@8f
    if-eqz v4, :cond_2d

    #@91
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->getNotifyServiceState()Z

    #@94
    move-result v4

    #@95
    if-eqz v4, :cond_2d

    #@97
    .line 192
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mTarget:Landroid/os/Handler;

    #@99
    iget v5, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mServiceStateEventWhat:I

    #@9b
    invoke-static {v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@9e
    move-result-object v2

    #@9f
    .line 194
    .restart local v2       #message:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mTarget:Landroid/os/Handler;

    #@a1
    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_a4
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_a4} :catch_5e

    #@a4
    goto :goto_2d
.end method

.method public registerIntent()V
    .registers 3

    #@0
    .prologue
    .line 157
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mContext:Landroid/content/Context;

    #@2
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mFilter:Landroid/content/IntentFilter;

    #@4
    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@7
    .line 158
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 74
    iput-object p1, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mContext:Landroid/content/Context;

    #@2
    .line 75
    return-void
.end method

.method public setTarget(Landroid/os/Handler;)V
    .registers 2
    .parameter "h"

    #@0
    .prologue
    .line 78
    iput-object p1, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mTarget:Landroid/os/Handler;

    #@2
    .line 79
    return-void
.end method

.method public unregisterIntent()V
    .registers 2

    #@0
    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneStateIntentReceiver;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@5
    .line 162
    return-void
.end method
