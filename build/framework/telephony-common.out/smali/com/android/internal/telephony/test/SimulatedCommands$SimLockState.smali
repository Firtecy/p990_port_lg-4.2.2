.class final enum Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;
.super Ljava/lang/Enum;
.source "SimulatedCommands.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/test/SimulatedCommands;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SimLockState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

.field public static final enum NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

.field public static final enum REQUIRE_PIN:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

.field public static final enum REQUIRE_PUK:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

.field public static final enum SIM_PERM_LOCKED:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 47
    new-instance v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@6
    const-string v1, "NONE"

    #@8
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;-><init>(Ljava/lang/String;I)V

    #@b
    sput-object v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@d
    .line 48
    new-instance v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@f
    const-string v1, "REQUIRE_PIN"

    #@11
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;-><init>(Ljava/lang/String;I)V

    #@14
    sput-object v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->REQUIRE_PIN:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@16
    .line 49
    new-instance v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@18
    const-string v1, "REQUIRE_PUK"

    #@1a
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;-><init>(Ljava/lang/String;I)V

    #@1d
    sput-object v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->REQUIRE_PUK:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@1f
    .line 50
    new-instance v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@21
    const-string v1, "SIM_PERM_LOCKED"

    #@23
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;-><init>(Ljava/lang/String;I)V

    #@26
    sput-object v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->SIM_PERM_LOCKED:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@28
    .line 46
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@2b
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->REQUIRE_PIN:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->REQUIRE_PUK:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->SIM_PERM_LOCKED:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->$VALUES:[Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 46
    const-class v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;
    .registers 1

    #@0
    .prologue
    .line 46
    sget-object v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->$VALUES:[Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@8
    return-object v0
.end method
