.class public final enum Lcom/android/internal/telephony/cat/AppInterface$CommandType;
.super Ljava/lang/Enum;
.source "AppInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cat/AppInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CommandType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/cat/AppInterface$CommandType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum ACTIVATE:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum CLOSE_CHANNEL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum DISPLAY_TEXT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum GET_CHANNEL_STATUS:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum GET_INKEY:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum GET_INPUT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum LAUNCH_BROWSER:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum OPEN_CHANNEL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum PLAY_TONE:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum PROVIDE_LOCAL_INFORMATION:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum RECEIVE_DATA:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum REFRESH:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum SELECT_ITEM:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum SEND_DATA:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum SEND_DTMF:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum SEND_SMS:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum SEND_SS:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum SEND_USSD:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum SET_UP_CALL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum SET_UP_EVENT_LIST:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum SET_UP_IDLE_MODE_TEXT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

.field public static final enum SET_UP_MENU:Lcom/android/internal/telephony/cat/AppInterface$CommandType;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/16 v8, 0x12

    #@2
    const/16 v7, 0x11

    #@4
    const/16 v6, 0x10

    #@6
    const/4 v5, 0x5

    #@7
    const/4 v4, 0x1

    #@8
    .line 74
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@a
    const-string v1, "DISPLAY_TEXT"

    #@c
    const/4 v2, 0x0

    #@d
    const/16 v3, 0x21

    #@f
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@12
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->DISPLAY_TEXT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@14
    .line 75
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@16
    const-string v1, "GET_INKEY"

    #@18
    const/16 v2, 0x22

    #@1a
    invoke-direct {v0, v1, v4, v2}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@1d
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->GET_INKEY:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@1f
    .line 76
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@21
    const-string v1, "GET_INPUT"

    #@23
    const/4 v2, 0x2

    #@24
    const/16 v3, 0x23

    #@26
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@29
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->GET_INPUT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@2b
    .line 77
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@2d
    const-string v1, "LAUNCH_BROWSER"

    #@2f
    const/4 v2, 0x3

    #@30
    const/16 v3, 0x15

    #@32
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@35
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->LAUNCH_BROWSER:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@37
    .line 78
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@39
    const-string v1, "PLAY_TONE"

    #@3b
    const/4 v2, 0x4

    #@3c
    const/16 v3, 0x20

    #@3e
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@41
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->PLAY_TONE:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@43
    .line 79
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@45
    const-string v1, "REFRESH"

    #@47
    invoke-direct {v0, v1, v5, v4}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@4a
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->REFRESH:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@4c
    .line 80
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@4e
    const-string v1, "SELECT_ITEM"

    #@50
    const/4 v2, 0x6

    #@51
    const/16 v3, 0x24

    #@53
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@56
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SELECT_ITEM:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@58
    .line 81
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@5a
    const-string v1, "SEND_SS"

    #@5c
    const/4 v2, 0x7

    #@5d
    invoke-direct {v0, v1, v2, v7}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@60
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SEND_SS:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@62
    .line 82
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@64
    const-string v1, "SEND_USSD"

    #@66
    const/16 v2, 0x8

    #@68
    invoke-direct {v0, v1, v2, v8}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@6b
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SEND_USSD:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@6d
    .line 83
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@6f
    const-string v1, "SEND_SMS"

    #@71
    const/16 v2, 0x9

    #@73
    const/16 v3, 0x13

    #@75
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@78
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SEND_SMS:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@7a
    .line 84
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@7c
    const-string v1, "SEND_DTMF"

    #@7e
    const/16 v2, 0xa

    #@80
    const/16 v3, 0x14

    #@82
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@85
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SEND_DTMF:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@87
    .line 85
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@89
    const-string v1, "SET_UP_EVENT_LIST"

    #@8b
    const/16 v2, 0xb

    #@8d
    invoke-direct {v0, v1, v2, v5}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@90
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_EVENT_LIST:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@92
    .line 86
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@94
    const-string v1, "SET_UP_IDLE_MODE_TEXT"

    #@96
    const/16 v2, 0xc

    #@98
    const/16 v3, 0x28

    #@9a
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@9d
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_IDLE_MODE_TEXT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@9f
    .line 87
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@a1
    const-string v1, "SET_UP_MENU"

    #@a3
    const/16 v2, 0xd

    #@a5
    const/16 v3, 0x25

    #@a7
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@aa
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_MENU:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@ac
    .line 88
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@ae
    const-string v1, "SET_UP_CALL"

    #@b0
    const/16 v2, 0xe

    #@b2
    invoke-direct {v0, v1, v2, v6}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@b5
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_CALL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@b7
    .line 89
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@b9
    const-string v1, "PROVIDE_LOCAL_INFORMATION"

    #@bb
    const/16 v2, 0xf

    #@bd
    const/16 v3, 0x26

    #@bf
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@c2
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->PROVIDE_LOCAL_INFORMATION:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@c4
    .line 90
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@c6
    const-string v1, "OPEN_CHANNEL"

    #@c8
    const/16 v2, 0x40

    #@ca
    invoke-direct {v0, v1, v6, v2}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@cd
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->OPEN_CHANNEL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@cf
    .line 91
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@d1
    const-string v1, "CLOSE_CHANNEL"

    #@d3
    const/16 v2, 0x41

    #@d5
    invoke-direct {v0, v1, v7, v2}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@d8
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->CLOSE_CHANNEL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@da
    .line 92
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@dc
    const-string v1, "RECEIVE_DATA"

    #@de
    const/16 v2, 0x42

    #@e0
    invoke-direct {v0, v1, v8, v2}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@e3
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->RECEIVE_DATA:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@e5
    .line 93
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@e7
    const-string v1, "SEND_DATA"

    #@e9
    const/16 v2, 0x13

    #@eb
    const/16 v3, 0x43

    #@ed
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@f0
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SEND_DATA:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@f2
    .line 94
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@f4
    const-string v1, "GET_CHANNEL_STATUS"

    #@f6
    const/16 v2, 0x14

    #@f8
    const/16 v3, 0x44

    #@fa
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@fd
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->GET_CHANNEL_STATUS:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@ff
    .line 96
    new-instance v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@101
    const-string v1, "ACTIVATE"

    #@103
    const/16 v2, 0x15

    #@105
    const/16 v3, 0x70

    #@107
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;-><init>(Ljava/lang/String;II)V

    #@10a
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ACTIVATE:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@10c
    .line 73
    const/16 v0, 0x16

    #@10e
    new-array v0, v0, [Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@110
    const/4 v1, 0x0

    #@111
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->DISPLAY_TEXT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@113
    aput-object v2, v0, v1

    #@115
    sget-object v1, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->GET_INKEY:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@117
    aput-object v1, v0, v4

    #@119
    const/4 v1, 0x2

    #@11a
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->GET_INPUT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@11c
    aput-object v2, v0, v1

    #@11e
    const/4 v1, 0x3

    #@11f
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->LAUNCH_BROWSER:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@121
    aput-object v2, v0, v1

    #@123
    const/4 v1, 0x4

    #@124
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->PLAY_TONE:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@126
    aput-object v2, v0, v1

    #@128
    sget-object v1, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->REFRESH:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@12a
    aput-object v1, v0, v5

    #@12c
    const/4 v1, 0x6

    #@12d
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SELECT_ITEM:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@12f
    aput-object v2, v0, v1

    #@131
    const/4 v1, 0x7

    #@132
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SEND_SS:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@134
    aput-object v2, v0, v1

    #@136
    const/16 v1, 0x8

    #@138
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SEND_USSD:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@13a
    aput-object v2, v0, v1

    #@13c
    const/16 v1, 0x9

    #@13e
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SEND_SMS:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@140
    aput-object v2, v0, v1

    #@142
    const/16 v1, 0xa

    #@144
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SEND_DTMF:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@146
    aput-object v2, v0, v1

    #@148
    const/16 v1, 0xb

    #@14a
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_EVENT_LIST:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@14c
    aput-object v2, v0, v1

    #@14e
    const/16 v1, 0xc

    #@150
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_IDLE_MODE_TEXT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@152
    aput-object v2, v0, v1

    #@154
    const/16 v1, 0xd

    #@156
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_MENU:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@158
    aput-object v2, v0, v1

    #@15a
    const/16 v1, 0xe

    #@15c
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_CALL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@15e
    aput-object v2, v0, v1

    #@160
    const/16 v1, 0xf

    #@162
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->PROVIDE_LOCAL_INFORMATION:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@164
    aput-object v2, v0, v1

    #@166
    sget-object v1, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->OPEN_CHANNEL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@168
    aput-object v1, v0, v6

    #@16a
    sget-object v1, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->CLOSE_CHANNEL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@16c
    aput-object v1, v0, v7

    #@16e
    sget-object v1, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->RECEIVE_DATA:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@170
    aput-object v1, v0, v8

    #@172
    const/16 v1, 0x13

    #@174
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SEND_DATA:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@176
    aput-object v2, v0, v1

    #@178
    const/16 v1, 0x14

    #@17a
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->GET_CHANNEL_STATUS:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@17c
    aput-object v2, v0, v1

    #@17e
    const/16 v1, 0x15

    #@180
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ACTIVATE:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@182
    aput-object v2, v0, v1

    #@184
    sput-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->$VALUES:[Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@186
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 102
    iput p3, p0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->mValue:I

    #@5
    .line 103
    return-void
.end method

.method public static fromInt(I)Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 118
    invoke-static {}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->values()[Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@3
    move-result-object v0

    #@4
    .local v0, arr$:[Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    array-length v3, v0

    #@5
    .local v3, len$:I
    const/4 v2, 0x0

    #@6
    .local v2, i$:I
    :goto_6
    if-ge v2, v3, :cond_12

    #@8
    aget-object v1, v0, v2

    #@a
    .line 119
    .local v1, e:Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    iget v4, v1, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->mValue:I

    #@c
    if-ne v4, p0, :cond_f

    #@e
    .line 123
    .end local v1           #e:Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    :goto_e
    return-object v1

    #@f
    .line 118
    .restart local v1       #e:Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    :cond_f
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_6

    #@12
    .line 123
    .end local v1           #e:Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_e
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 73
    const-class v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    .registers 1

    #@0
    .prologue
    .line 73
    sget-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->$VALUES:[Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/cat/AppInterface$CommandType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@8
    return-object v0
.end method


# virtual methods
.method public value()I
    .registers 2

    #@0
    .prologue
    .line 106
    iget v0, p0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->mValue:I

    #@2
    return v0
.end method
