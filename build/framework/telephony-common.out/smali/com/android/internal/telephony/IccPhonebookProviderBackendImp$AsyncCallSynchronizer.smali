.class Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;
.super Ljava/lang/Object;
.source "IccPhonebookProviderBackendImp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncCallSynchronizer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mLock:Ljava/lang/Object;

.field private mNotified:Z

.field protected mResultHolder:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TRESU",
            "LT;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 895
    .local p0, this:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;,"Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer<TRESULT;>;"
    iput-object p1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->this$0:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 888
    new-instance v0, Ljava/lang/Object;

    #@7
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mLock:Ljava/lang/Object;

    #@c
    .line 889
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mNotified:Z

    #@f
    .line 896
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 885
    .local p0, this:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;,"Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer<TRESULT;>;"
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;-><init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 885
    invoke-direct {p0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->reset()V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 885
    invoke-direct {p0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->waitResult()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 885
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->notifyResult(Ljava/lang/Object;)V

    #@3
    return-void
.end method

.method private notifyResult(Ljava/lang/Object;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRESU",
            "LT;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 936
    .local p0, this:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;,"Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer<TRESULT;>;"
    .local p1, result:Ljava/lang/Object;,"TRESULT;"
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 938
    const/4 v0, 0x1

    #@4
    :try_start_4
    iput-boolean v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mNotified:Z

    #@6
    .line 939
    iput-object p1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mResultHolder:Ljava/lang/Object;

    #@8
    .line 940
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mLock:Ljava/lang/Object;

    #@a
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@d
    .line 942
    monitor-exit v1

    #@e
    .line 943
    return-void

    #@f
    .line 942
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method private reset()V
    .registers 2

    #@0
    .prologue
    .line 902
    .local p0, this:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;,"Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer<TRESULT;>;"
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mNotified:Z

    #@3
    .line 903
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mResultHolder:Ljava/lang/Object;

    #@6
    .line 904
    return-void
.end method

.method private waitResult()Ljava/lang/Object;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRESU",
            "LT;"
        }
    .end annotation

    #@0
    .prologue
    .line 909
    .local p0, this:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;,"Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer<TRESULT;>;"
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 911
    :try_start_3
    iget-boolean v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mNotified:Z

    #@5
    if-nez v1, :cond_f

    #@7
    .line 912
    const/4 v1, 0x0

    #@8
    iput-boolean v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mNotified:Z
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_1c

    #@a
    .line 916
    :try_start_a
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mLock:Ljava/lang/Object;

    #@c
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_f
    .catchall {:try_start_a .. :try_end_f} :catchall_1c
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_f} :catch_13

    #@f
    .line 922
    :cond_f
    :goto_f
    :try_start_f
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->mResultHolder:Ljava/lang/Object;

    #@11
    monitor-exit v2

    #@12
    return-object v1

    #@13
    .line 917
    :catch_13
    move-exception v0

    #@14
    .line 918
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v1, "IccPhonebookProvider"

    #@16
    const-string v3, "Interrupted while wait"

    #@18
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    goto :goto_f

    #@1c
    .line 923
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_1c
    move-exception v1

    #@1d
    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_f .. :try_end_1e} :catchall_1c

    #@1e
    throw v1
.end method
