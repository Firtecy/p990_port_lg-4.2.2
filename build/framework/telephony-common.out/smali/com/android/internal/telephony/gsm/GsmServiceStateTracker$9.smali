.class Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$9;
.super Ljava/util/TimerTask;
.source "GsmServiceStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->faCHGReboot()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field pm:Landroid/os/PowerManager;

.field final synthetic this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 5418
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$9;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    #@5
    .line 5419
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$9;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@7
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@c
    move-result-object v0

    #@d
    const-string v1, "power"

    #@f
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/os/PowerManager;

    #@15
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$9;->pm:Landroid/os/PowerManager;

    #@17
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 5422
    :try_start_0
    const-string v1, "GSM"

    #@2
    const-string v2, "EVENT_SKT_FA_CHG_DONE Reboot :: try"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 5423
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$9;->pm:Landroid/os/PowerManager;

    #@9
    const-string v2, "boot"

    #@b
    invoke-virtual {v1, v2}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_e} :catch_f

    #@e
    .line 5429
    :goto_e
    return-void

    #@f
    .line 5425
    :catch_f
    move-exception v0

    #@10
    .line 5426
    .local v0, e:Ljava/lang/SecurityException;
    const-string v1, "GSM"

    #@12
    const-string v2, "EVENT_SKT_FA_CHG_DONE Reboot :: catch"

    #@14
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    goto :goto_e
.end method
