.class public Lcom/android/internal/telephony/cat/IconLoader;
.super Landroid/os/Handler;
.source "IconLoader.java"


# static fields
.field private static final CLUT_ENTRY_SIZE:I = 0x3

.field private static final CLUT_LOCATION_OFFSET:I = 0x4

.field private static final EVENT_READ_CLUT_DONE:I = 0x3

.field private static final EVENT_READ_EF_IMG_RECOED_DONE:I = 0x1

.field private static final EVENT_READ_ICON_DONE:I = 0x2

.field private static final STATE_MULTI_ICONS:I = 0x2

.field private static final STATE_SINGLE_ICON:I = 0x1

.field private static sLoader:Lcom/android/internal/telephony/cat/IconLoader;


# instance fields
.field private mCurrentIcon:Landroid/graphics/Bitmap;

.field private mCurrentRecordIndex:I

.field private mEndMsg:Landroid/os/Message;

.field private mIconData:[B

.field private mIcons:[Landroid/graphics/Bitmap;

.field protected mIconsCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mId:Lcom/android/internal/telephony/cat/ImageDescriptor;

.field private mRecordNumber:I

.field private mRecordNumbers:[I

.field protected mSimFH:Lcom/android/internal/telephony/uicc/IccFileHandler;

.field private mState:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 53
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/android/internal/telephony/cat/IconLoader;->sLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@3
    return-void
.end method

.method protected constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 80
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@4
    .line 40
    const/4 v0, 0x1

    #@5
    iput v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mState:I

    #@7
    .line 41
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mId:Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@9
    .line 42
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentIcon:Landroid/graphics/Bitmap;

    #@b
    .line 44
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mSimFH:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@d
    .line 45
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mEndMsg:Landroid/os/Message;

    #@f
    .line 46
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconData:[B

    #@11
    .line 48
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mRecordNumbers:[I

    #@13
    .line 49
    const/4 v0, 0x0

    #@14
    iput v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentRecordIndex:I

    #@16
    .line 50
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIcons:[Landroid/graphics/Bitmap;

    #@18
    .line 51
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconsCache:Ljava/util/HashMap;

    #@1a
    .line 81
    return-void
.end method

.method private constructor <init>(Landroid/os/Looper;Lcom/android/internal/telephony/uicc/IccFileHandler;)V
    .registers 5
    .parameter "looper"
    .parameter "fh"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 73
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@4
    .line 40
    const/4 v0, 0x1

    #@5
    iput v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mState:I

    #@7
    .line 41
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mId:Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@9
    .line 42
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentIcon:Landroid/graphics/Bitmap;

    #@b
    .line 44
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mSimFH:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@d
    .line 45
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mEndMsg:Landroid/os/Message;

    #@f
    .line 46
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconData:[B

    #@11
    .line 48
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mRecordNumbers:[I

    #@13
    .line 49
    const/4 v0, 0x0

    #@14
    iput v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentRecordIndex:I

    #@16
    .line 50
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIcons:[Landroid/graphics/Bitmap;

    #@18
    .line 51
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconsCache:Ljava/util/HashMap;

    #@1a
    .line 74
    iput-object p2, p0, Lcom/android/internal/telephony/cat/IconLoader;->mSimFH:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1c
    .line 76
    new-instance v0, Ljava/util/HashMap;

    #@1e
    const/16 v1, 0x32

    #@20
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    #@23
    iput-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconsCache:Ljava/util/HashMap;

    #@25
    .line 77
    return-void
.end method

.method private static bitToBnW(I)I
    .registers 2
    .parameter "bit"

    #@0
    .prologue
    .line 278
    const/4 v0, 0x1

    #@1
    if-ne p0, v0, :cond_5

    #@3
    .line 279
    const/4 v0, -0x1

    #@4
    .line 281
    :goto_4
    return v0

    #@5
    :cond_5
    const/high16 v0, -0x100

    #@7
    goto :goto_4
.end method

.method static getInstance(Landroid/os/Handler;Lcom/android/internal/telephony/uicc/IccFileHandler;)Lcom/android/internal/telephony/cat/IconLoader;
    .registers 5
    .parameter "caller"
    .parameter "fh"

    #@0
    .prologue
    .line 84
    sget-object v1, Lcom/android/internal/telephony/cat/IconLoader;->sLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 85
    sget-object v1, Lcom/android/internal/telephony/cat/IconLoader;->sLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@6
    .line 92
    :goto_6
    return-object v1

    #@7
    .line 87
    :cond_7
    if-eqz p1, :cond_1d

    #@9
    .line 88
    new-instance v0, Landroid/os/HandlerThread;

    #@b
    const-string v1, "Cat Icon Loader"

    #@d
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@10
    .line 89
    .local v0, thread:Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@13
    .line 90
    new-instance v1, Lcom/android/internal/telephony/cat/IconLoader;

    #@15
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@18
    move-result-object v2

    #@19
    invoke-direct {v1, v2, p1}, Lcom/android/internal/telephony/cat/IconLoader;-><init>(Landroid/os/Looper;Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@1c
    goto :goto_6

    #@1d
    .line 92
    .end local v0           #thread:Landroid/os/HandlerThread;
    :cond_1d
    const/4 v1, 0x0

    #@1e
    goto :goto_6
.end method

.method private static getMask(I)I
    .registers 2
    .parameter "numOfBits"

    #@0
    .prologue
    .line 340
    const/4 v0, 0x0

    #@1
    .line 342
    .local v0, mask:I
    packed-switch p0, :pswitch_data_1a

    #@4
    .line 368
    :goto_4
    return v0

    #@5
    .line 344
    :pswitch_5
    const/4 v0, 0x1

    #@6
    .line 345
    goto :goto_4

    #@7
    .line 347
    :pswitch_7
    const/4 v0, 0x3

    #@8
    .line 348
    goto :goto_4

    #@9
    .line 350
    :pswitch_9
    const/4 v0, 0x7

    #@a
    .line 351
    goto :goto_4

    #@b
    .line 353
    :pswitch_b
    const/16 v0, 0xf

    #@d
    .line 354
    goto :goto_4

    #@e
    .line 356
    :pswitch_e
    const/16 v0, 0x1f

    #@10
    .line 357
    goto :goto_4

    #@11
    .line 359
    :pswitch_11
    const/16 v0, 0x3f

    #@13
    .line 360
    goto :goto_4

    #@14
    .line 362
    :pswitch_14
    const/16 v0, 0x7f

    #@16
    .line 363
    goto :goto_4

    #@17
    .line 365
    :pswitch_17
    const/16 v0, 0xff

    #@19
    goto :goto_4

    #@1a
    .line 342
    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_5
        :pswitch_7
        :pswitch_9
        :pswitch_b
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
    .end packed-switch
.end method

.method private handleImageDescriptor([B)Z
    .registers 4
    .parameter "rawData"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 188
    invoke-static {p1, v0}, Lcom/android/internal/telephony/cat/ImageDescriptor;->parse([BI)Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@4
    move-result-object v1

    #@5
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mId:Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@7
    .line 189
    iget-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mId:Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@9
    if-nez v1, :cond_c

    #@b
    .line 190
    const/4 v0, 0x0

    #@c
    .line 192
    :cond_c
    return v0
.end method

.method public static parseToBnW([BI)Landroid/graphics/Bitmap;
    .registers 15
    .parameter "data"
    .parameter "length"

    #@0
    .prologue
    .line 245
    const/4 v8, 0x0

    #@1
    .line 246
    .local v8, valueIndex:I
    add-int/lit8 v9, v8, 0x1

    #@3
    .end local v8           #valueIndex:I
    .local v9, valueIndex:I
    aget-byte v11, p0, v8

    #@5
    and-int/lit16 v10, v11, 0xff

    #@7
    .line 247
    .local v10, width:I
    add-int/lit8 v8, v9, 0x1

    #@9
    .end local v9           #valueIndex:I
    .restart local v8       #valueIndex:I
    aget-byte v11, p0, v9

    #@b
    and-int/lit16 v3, v11, 0xff

    #@d
    .line 248
    .local v3, height:I
    mul-int v4, v10, v3

    #@f
    .line 250
    .local v4, numOfPixels:I
    new-array v7, v4, [I

    #@11
    .line 252
    .local v7, pixels:[I
    const/4 v5, 0x0

    #@12
    .line 253
    .local v5, pixelIndex:I
    const/4 v0, 0x7

    #@13
    .line 254
    .local v0, bitIndex:I
    const/4 v2, 0x0

    #@14
    .local v2, currentByte:B
    move v6, v5

    #@15
    .end local v5           #pixelIndex:I
    .local v6, pixelIndex:I
    move v9, v8

    #@16
    .line 255
    .end local v8           #valueIndex:I
    .restart local v9       #valueIndex:I
    :goto_16
    if-ge v6, v4, :cond_33

    #@18
    .line 257
    rem-int/lit8 v11, v6, 0x8

    #@1a
    if-nez v11, :cond_43

    #@1c
    .line 258
    add-int/lit8 v8, v9, 0x1

    #@1e
    .end local v9           #valueIndex:I
    .restart local v8       #valueIndex:I
    aget-byte v2, p0, v9

    #@20
    .line 259
    const/4 v0, 0x7

    #@21
    .line 261
    :goto_21
    add-int/lit8 v5, v6, 0x1

    #@23
    .end local v6           #pixelIndex:I
    .restart local v5       #pixelIndex:I
    add-int/lit8 v1, v0, -0x1

    #@25
    .end local v0           #bitIndex:I
    .local v1, bitIndex:I
    shr-int v11, v2, v0

    #@27
    and-int/lit8 v11, v11, 0x1

    #@29
    invoke-static {v11}, Lcom/android/internal/telephony/cat/IconLoader;->bitToBnW(I)I

    #@2c
    move-result v11

    #@2d
    aput v11, v7, v6

    #@2f
    move v0, v1

    #@30
    .end local v1           #bitIndex:I
    .restart local v0       #bitIndex:I
    move v6, v5

    #@31
    .end local v5           #pixelIndex:I
    .restart local v6       #pixelIndex:I
    move v9, v8

    #@32
    .end local v8           #valueIndex:I
    .restart local v9       #valueIndex:I
    goto :goto_16

    #@33
    .line 264
    :cond_33
    if-eq v6, v4, :cond_3c

    #@35
    .line 265
    const-string v11, "IconLoader"

    #@37
    const-string v12, "parseToBnW; size error"

    #@39
    invoke-static {v11, v12}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 267
    :cond_3c
    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@3e
    invoke-static {v7, v10, v3, v11}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@41
    move-result-object v11

    #@42
    return-object v11

    #@43
    :cond_43
    move v8, v9

    #@44
    .end local v9           #valueIndex:I
    .restart local v8       #valueIndex:I
    goto :goto_21
.end method

.method public static parseToRGB([BIZ[B)Landroid/graphics/Bitmap;
    .registers 26
    .parameter "data"
    .parameter "length"
    .parameter "transparency"
    .parameter "clut"

    #@0
    .prologue
    .line 296
    const/16 v16, 0x0

    #@2
    .line 297
    .local v16, valueIndex:I
    add-int/lit8 v17, v16, 0x1

    #@4
    .end local v16           #valueIndex:I
    .local v17, valueIndex:I
    aget-byte v19, p0, v16

    #@6
    move/from16 v0, v19

    #@8
    and-int/lit16 v0, v0, 0xff

    #@a
    move/from16 v18, v0

    #@c
    .line 298
    .local v18, width:I
    add-int/lit8 v16, v17, 0x1

    #@e
    .end local v17           #valueIndex:I
    .restart local v16       #valueIndex:I
    aget-byte v19, p0, v17

    #@10
    move/from16 v0, v19

    #@12
    and-int/lit16 v9, v0, 0xff

    #@14
    .line 299
    .local v9, height:I
    add-int/lit8 v17, v16, 0x1

    #@16
    .end local v16           #valueIndex:I
    .restart local v17       #valueIndex:I
    aget-byte v19, p0, v16

    #@18
    move/from16 v0, v19

    #@1a
    and-int/lit16 v4, v0, 0xff

    #@1c
    .line 300
    .local v4, bitsPerImg:I
    add-int/lit8 v16, v17, 0x1

    #@1e
    .end local v17           #valueIndex:I
    .restart local v16       #valueIndex:I
    aget-byte v19, p0, v17

    #@20
    move/from16 v0, v19

    #@22
    and-int/lit16 v11, v0, 0xff

    #@24
    .line 302
    .local v11, numOfClutEntries:I
    const/16 v19, 0x1

    #@26
    move/from16 v0, v19

    #@28
    move/from16 v1, p2

    #@2a
    if-ne v0, v1, :cond_32

    #@2c
    .line 303
    add-int/lit8 v19, v11, -0x1

    #@2e
    const/16 v20, 0x0

    #@30
    aput-byte v20, p3, v19

    #@32
    .line 306
    :cond_32
    mul-int v12, v18, v9

    #@34
    .line 307
    .local v12, numOfPixels:I
    new-array v15, v12, [I

    #@36
    .line 309
    .local v15, pixels:[I
    const/16 v16, 0x6

    #@38
    .line 310
    const/4 v13, 0x0

    #@39
    .line 311
    .local v13, pixelIndex:I
    rsub-int/lit8 v5, v4, 0x8

    #@3b
    .line 312
    .local v5, bitsStartOffset:I
    move v2, v5

    #@3c
    .line 313
    .local v2, bitIndex:I
    add-int/lit8 v17, v16, 0x1

    #@3e
    .end local v16           #valueIndex:I
    .restart local v17       #valueIndex:I
    aget-byte v8, p0, v16

    #@40
    .line 314
    .local v8, currentByte:B
    invoke-static {v4}, Lcom/android/internal/telephony/cat/IconLoader;->getMask(I)I

    #@43
    move-result v10

    #@44
    .line 315
    .local v10, mask:I
    const/16 v19, 0x8

    #@46
    rem-int v19, v19, v4

    #@48
    if-nez v19, :cond_74

    #@4a
    const/4 v3, 0x1

    #@4b
    .local v3, bitsOverlaps:Z
    :goto_4b
    move v14, v13

    #@4c
    .line 316
    .end local v13           #pixelIndex:I
    .local v14, pixelIndex:I
    :goto_4c
    if-ge v14, v12, :cond_79

    #@4e
    .line 318
    if-gez v2, :cond_84

    #@50
    .line 319
    add-int/lit8 v16, v17, 0x1

    #@52
    .end local v17           #valueIndex:I
    .restart local v16       #valueIndex:I
    aget-byte v8, p0, v17

    #@54
    .line 320
    if-eqz v3, :cond_76

    #@56
    move v2, v5

    #@57
    .line 322
    :goto_57
    shr-int v19, v8, v2

    #@59
    and-int v6, v19, v10

    #@5b
    .line 323
    .local v6, clutEntry:I
    mul-int/lit8 v7, v6, 0x3

    #@5d
    .line 324
    .local v7, clutIndex:I
    add-int/lit8 v13, v14, 0x1

    #@5f
    .end local v14           #pixelIndex:I
    .restart local v13       #pixelIndex:I
    aget-byte v19, p3, v7

    #@61
    add-int/lit8 v20, v7, 0x1

    #@63
    aget-byte v20, p3, v20

    #@65
    add-int/lit8 v21, v7, 0x2

    #@67
    aget-byte v21, p3, v21

    #@69
    invoke-static/range {v19 .. v21}, Landroid/graphics/Color;->rgb(III)I

    #@6c
    move-result v19

    #@6d
    aput v19, v15, v14

    #@6f
    .line 326
    sub-int/2addr v2, v4

    #@70
    move v14, v13

    #@71
    .end local v13           #pixelIndex:I
    .restart local v14       #pixelIndex:I
    move/from16 v17, v16

    #@73
    .line 327
    .end local v16           #valueIndex:I
    .restart local v17       #valueIndex:I
    goto :goto_4c

    #@74
    .line 315
    .end local v3           #bitsOverlaps:Z
    .end local v6           #clutEntry:I
    .end local v7           #clutIndex:I
    .end local v14           #pixelIndex:I
    .restart local v13       #pixelIndex:I
    :cond_74
    const/4 v3, 0x0

    #@75
    goto :goto_4b

    #@76
    .line 320
    .end local v13           #pixelIndex:I
    .end local v17           #valueIndex:I
    .restart local v3       #bitsOverlaps:Z
    .restart local v14       #pixelIndex:I
    .restart local v16       #valueIndex:I
    :cond_76
    mul-int/lit8 v2, v2, -0x1

    #@78
    goto :goto_57

    #@79
    .line 329
    .end local v16           #valueIndex:I
    .restart local v17       #valueIndex:I
    :cond_79
    sget-object v19, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@7b
    move/from16 v0, v18

    #@7d
    move-object/from16 v1, v19

    #@7f
    invoke-static {v15, v0, v9, v1}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@82
    move-result-object v19

    #@83
    return-object v19

    #@84
    :cond_84
    move/from16 v16, v17

    #@86
    .end local v17           #valueIndex:I
    .restart local v16       #valueIndex:I
    goto :goto_57
.end method

.method private postIcon()V
    .registers 4

    #@0
    .prologue
    .line 223
    iget v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_11

    #@5
    .line 224
    iget-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mEndMsg:Landroid/os/Message;

    #@7
    iget-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentIcon:Landroid/graphics/Bitmap;

    #@9
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b
    .line 225
    iget-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mEndMsg:Landroid/os/Message;

    #@d
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@10
    .line 236
    :cond_10
    :goto_10
    return-void

    #@11
    .line 226
    :cond_11
    iget v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mState:I

    #@13
    const/4 v1, 0x2

    #@14
    if-ne v0, v1, :cond_10

    #@16
    .line 227
    iget-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIcons:[Landroid/graphics/Bitmap;

    #@18
    iget v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentRecordIndex:I

    #@1a
    add-int/lit8 v2, v1, 0x1

    #@1c
    iput v2, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentRecordIndex:I

    #@1e
    iget-object v2, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentIcon:Landroid/graphics/Bitmap;

    #@20
    aput-object v2, v0, v1

    #@22
    .line 229
    iget v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentRecordIndex:I

    #@24
    iget-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mRecordNumbers:[I

    #@26
    array-length v1, v1

    #@27
    if-ge v0, v1, :cond_33

    #@29
    .line 230
    iget-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mRecordNumbers:[I

    #@2b
    iget v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentRecordIndex:I

    #@2d
    aget v0, v0, v1

    #@2f
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cat/IconLoader;->startLoadingIcon(I)V

    #@32
    goto :goto_10

    #@33
    .line 232
    :cond_33
    iget-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mEndMsg:Landroid/os/Message;

    #@35
    iget-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIcons:[Landroid/graphics/Bitmap;

    #@37
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@39
    .line 233
    iget-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mEndMsg:Landroid/os/Message;

    #@3b
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@3e
    goto :goto_10
.end method

.method private readClut()V
    .registers 8

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 197
    iget-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconData:[B

    #@3
    aget-byte v0, v0, v1

    #@5
    mul-int/lit8 v4, v0, 0x3

    #@7
    .line 198
    .local v4, length:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cat/IconLoader;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v5

    #@b
    .line 199
    .local v5, msg:Landroid/os/Message;
    iget-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mSimFH:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@d
    iget-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mId:Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@f
    iget v1, v1, Lcom/android/internal/telephony/cat/ImageDescriptor;->imageId:I

    #@11
    iget-object v2, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconData:[B

    #@13
    const/4 v3, 0x4

    #@14
    aget-byte v2, v2, v3

    #@16
    iget-object v3, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconData:[B

    #@18
    const/4 v6, 0x5

    #@19
    aget-byte v3, v3, v6

    #@1b
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFImgTransparent(IIIILandroid/os/Message;)V

    #@1e
    .line 202
    return-void
.end method

.method private readIconData()V
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 217
    const/4 v0, 0x2

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cat/IconLoader;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v5

    #@6
    .line 218
    .local v5, msg:Landroid/os/Message;
    iget-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mSimFH:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@8
    iget-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mId:Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@a
    iget v1, v1, Lcom/android/internal/telephony/cat/ImageDescriptor;->imageId:I

    #@c
    iget-object v3, p0, Lcom/android/internal/telephony/cat/IconLoader;->mId:Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@e
    iget v4, v3, Lcom/android/internal/telephony/cat/ImageDescriptor;->length:I

    #@10
    move v3, v2

    #@11
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFImgTransparent(IIIILandroid/os/Message;)V

    #@14
    .line 219
    return-void
.end method

.method private readId()V
    .registers 4

    #@0
    .prologue
    .line 206
    iget v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mRecordNumber:I

    #@2
    if-gez v1, :cond_b

    #@4
    .line 207
    const/4 v1, 0x0

    #@5
    iput-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentIcon:Landroid/graphics/Bitmap;

    #@7
    .line 208
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/IconLoader;->postIcon()V

    #@a
    .line 213
    :goto_a
    return-void

    #@b
    .line 211
    :cond_b
    const/4 v1, 0x1

    #@c
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cat/IconLoader;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    .line 212
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mSimFH:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@12
    iget v2, p0, Lcom/android/internal/telephony/cat/IconLoader;->mRecordNumber:I

    #@14
    invoke-virtual {v1, v2, v0}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFImgLinearFixed(ILandroid/os/Message;)V

    #@17
    goto :goto_a
.end method

.method private startLoadingIcon(I)V
    .registers 4
    .parameter "recordNumber"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 119
    iput-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mId:Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@3
    .line 120
    iput-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconData:[B

    #@5
    .line 121
    iput-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentIcon:Landroid/graphics/Bitmap;

    #@7
    .line 122
    iput p1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mRecordNumber:I

    #@9
    .line 125
    iget-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconsCache:Ljava/util/HashMap;

    #@b
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_27

    #@15
    .line 126
    iget-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconsCache:Ljava/util/HashMap;

    #@17
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/graphics/Bitmap;

    #@21
    iput-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentIcon:Landroid/graphics/Bitmap;

    #@23
    .line 127
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/IconLoader;->postIcon()V

    #@26
    .line 133
    :goto_26
    return-void

    #@27
    .line 132
    :cond_27
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/IconLoader;->readId()V

    #@2a
    goto :goto_26
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    .line 140
    :try_start_0
    iget v5, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v5, :pswitch_data_94

    #@5
    .line 178
    :goto_5
    return-void

    #@6
    .line 142
    :pswitch_6
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v1, Landroid/os/AsyncResult;

    #@a
    .line 143
    .local v1, ar:Landroid/os/AsyncResult;
    iget-object v5, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c
    check-cast v5, [B

    #@e
    check-cast v5, [B

    #@10
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/cat/IconLoader;->handleImageDescriptor([B)Z

    #@13
    move-result v5

    #@14
    if-eqz v5, :cond_24

    #@16
    .line 144
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/IconLoader;->readIconData()V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_19} :catch_1a

    #@19
    goto :goto_5

    #@1a
    .line 173
    .end local v1           #ar:Landroid/os/AsyncResult;
    :catch_1a
    move-exception v3

    #@1b
    .line 174
    .local v3, e:Ljava/lang/Exception;
    const-string v5, "Icon load failed"

    #@1d
    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@20
    .line 176
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/IconLoader;->postIcon()V

    #@23
    goto :goto_5

    #@24
    .line 146
    .end local v3           #e:Ljava/lang/Exception;
    .restart local v1       #ar:Landroid/os/AsyncResult;
    :cond_24
    :try_start_24
    new-instance v5, Ljava/lang/Exception;

    #@26
    const-string v6, "Unable to parse image descriptor"

    #@28
    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@2b
    throw v5

    #@2c
    .line 150
    .end local v1           #ar:Landroid/os/AsyncResult;
    :pswitch_2c
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2e
    check-cast v1, Landroid/os/AsyncResult;

    #@30
    .line 151
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v5, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@32
    check-cast v5, [B

    #@34
    move-object v0, v5

    #@35
    check-cast v0, [B

    #@37
    move-object v4, v0

    #@38
    .line 152
    .local v4, rawData:[B
    iget-object v5, p0, Lcom/android/internal/telephony/cat/IconLoader;->mId:Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@3a
    iget v5, v5, Lcom/android/internal/telephony/cat/ImageDescriptor;->codingScheme:I

    #@3c
    const/16 v6, 0x11

    #@3e
    if-ne v5, v6, :cond_58

    #@40
    .line 153
    array-length v5, v4

    #@41
    invoke-static {v4, v5}, Lcom/android/internal/telephony/cat/IconLoader;->parseToBnW([BI)Landroid/graphics/Bitmap;

    #@44
    move-result-object v5

    #@45
    iput-object v5, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentIcon:Landroid/graphics/Bitmap;

    #@47
    .line 154
    iget-object v5, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconsCache:Ljava/util/HashMap;

    #@49
    iget v6, p0, Lcom/android/internal/telephony/cat/IconLoader;->mRecordNumber:I

    #@4b
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e
    move-result-object v6

    #@4f
    iget-object v7, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentIcon:Landroid/graphics/Bitmap;

    #@51
    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@54
    .line 155
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/IconLoader;->postIcon()V

    #@57
    goto :goto_5

    #@58
    .line 156
    :cond_58
    iget-object v5, p0, Lcom/android/internal/telephony/cat/IconLoader;->mId:Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@5a
    iget v5, v5, Lcom/android/internal/telephony/cat/ImageDescriptor;->codingScheme:I

    #@5c
    const/16 v6, 0x21

    #@5e
    if-ne v5, v6, :cond_66

    #@60
    .line 157
    iput-object v4, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconData:[B

    #@62
    .line 158
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/IconLoader;->readClut()V

    #@65
    goto :goto_5

    #@66
    .line 161
    :cond_66
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/IconLoader;->postIcon()V

    #@69
    goto :goto_5

    #@6a
    .line 165
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v4           #rawData:[B
    :pswitch_6a
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6c
    check-cast v1, Landroid/os/AsyncResult;

    #@6e
    .line 166
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v5, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@70
    check-cast v5, [B

    #@72
    move-object v0, v5

    #@73
    check-cast v0, [B

    #@75
    move-object v2, v0

    #@76
    .line 167
    .local v2, clut:[B
    iget-object v5, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconData:[B

    #@78
    iget-object v6, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconData:[B

    #@7a
    array-length v6, v6

    #@7b
    const/4 v7, 0x0

    #@7c
    invoke-static {v5, v6, v7, v2}, Lcom/android/internal/telephony/cat/IconLoader;->parseToRGB([BIZ[B)Landroid/graphics/Bitmap;

    #@7f
    move-result-object v5

    #@80
    iput-object v5, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentIcon:Landroid/graphics/Bitmap;

    #@82
    .line 169
    iget-object v5, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIconsCache:Ljava/util/HashMap;

    #@84
    iget v6, p0, Lcom/android/internal/telephony/cat/IconLoader;->mRecordNumber:I

    #@86
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@89
    move-result-object v6

    #@8a
    iget-object v7, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentIcon:Landroid/graphics/Bitmap;

    #@8c
    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8f
    .line 170
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/IconLoader;->postIcon()V
    :try_end_92
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_92} :catch_1a

    #@92
    goto/16 :goto_5

    #@94
    .line 140
    :pswitch_data_94
    .packed-switch 0x1
        :pswitch_6
        :pswitch_2c
        :pswitch_6a
    .end packed-switch
.end method

.method loadIcon(ILandroid/os/Message;)V
    .registers 4
    .parameter "recordNumber"
    .parameter "msg"

    #@0
    .prologue
    .line 109
    if-nez p2, :cond_3

    #@2
    .line 115
    :goto_2
    return-void

    #@3
    .line 112
    :cond_3
    iput-object p2, p0, Lcom/android/internal/telephony/cat/IconLoader;->mEndMsg:Landroid/os/Message;

    #@5
    .line 113
    const/4 v0, 0x1

    #@6
    iput v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mState:I

    #@8
    .line 114
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cat/IconLoader;->startLoadingIcon(I)V

    #@b
    goto :goto_2
.end method

.method loadIcons([ILandroid/os/Message;)V
    .registers 5
    .parameter "recordNumbers"
    .parameter "msg"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 96
    if-eqz p1, :cond_8

    #@3
    array-length v0, p1

    #@4
    if-eqz v0, :cond_8

    #@6
    if-nez p2, :cond_9

    #@8
    .line 106
    :cond_8
    :goto_8
    return-void

    #@9
    .line 99
    :cond_9
    iput-object p2, p0, Lcom/android/internal/telephony/cat/IconLoader;->mEndMsg:Landroid/os/Message;

    #@b
    .line 101
    array-length v0, p1

    #@c
    new-array v0, v0, [Landroid/graphics/Bitmap;

    #@e
    iput-object v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mIcons:[Landroid/graphics/Bitmap;

    #@10
    .line 102
    iput-object p1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mRecordNumbers:[I

    #@12
    .line 103
    iput v1, p0, Lcom/android/internal/telephony/cat/IconLoader;->mCurrentRecordIndex:I

    #@14
    .line 104
    const/4 v0, 0x2

    #@15
    iput v0, p0, Lcom/android/internal/telephony/cat/IconLoader;->mState:I

    #@17
    .line 105
    aget v0, p1, v1

    #@19
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cat/IconLoader;->startLoadingIcon(I)V

    #@1c
    goto :goto_8
.end method
