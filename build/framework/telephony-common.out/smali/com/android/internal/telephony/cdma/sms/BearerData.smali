.class public final Lcom/android/internal/telephony/cdma/sms/BearerData;
.super Ljava/lang/Object;
.source "BearerData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/sms/BearerData$1;,
        Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;,
        Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;,
        Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;
    }
.end annotation


# static fields
.field public static final ALERT_DEFAULT:I = 0x0

.field public static final ALERT_HIGH_PRIO:I = 0x3

.field public static final ALERT_LOW_PRIO:I = 0x1

.field public static final ALERT_MEDIUM_PRIO:I = 0x2

.field public static final DISPLAY_MODE_DEFAULT:I = 0x1

.field public static final DISPLAY_MODE_IMMEDIATE:I = 0x0

.field public static final DISPLAY_MODE_USER:I = 0x2

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_PERMANENT:I = 0x3

.field public static final ERROR_TEMPORARY:I = 0x2

.field public static final ERROR_UNDEFINED:I = 0xff

.field public static final LANGUAGE_CHINESE:I = 0x6

.field public static final LANGUAGE_ENGLISH:I = 0x1

.field public static final LANGUAGE_FRENCH:I = 0x2

.field public static final LANGUAGE_HEBREW:I = 0x7

.field public static final LANGUAGE_JAPANESE:I = 0x4

.field public static final LANGUAGE_KOREAN:I = 0x5

.field public static final LANGUAGE_KOREAN_LGT:I = 0x40

.field public static final LANGUAGE_SPANISH:I = 0x3

.field public static final LANGUAGE_UNKNOWN:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "SMS"

.field public static final MESSAGE_TYPE_CANCELLATION:I = 0x3

.field public static final MESSAGE_TYPE_DELIVER:I = 0x1

.field public static final MESSAGE_TYPE_DELIVERY_ACK:I = 0x4

.field public static final MESSAGE_TYPE_DELIVER_REPORT:I = 0x7

.field public static final MESSAGE_TYPE_READ_ACK:I = 0x6

.field public static final MESSAGE_TYPE_SUBMIT:I = 0x2

.field public static final MESSAGE_TYPE_SUBMIT_REPORT:I = 0x8

.field public static final MESSAGE_TYPE_USER_ACK:I = 0x5

.field public static final PRIORITY_EMERGENCY:I = 0x3

.field public static final PRIORITY_INTERACTIVE:I = 0x1

.field public static final PRIORITY_NORMAL:I = 0x0

.field public static final PRIORITY_URGENT:I = 0x2

.field public static final PRIVACY_CONFIDENTIAL:I = 0x2

.field public static final PRIVACY_NOT_RESTRICTED:I = 0x0

.field public static final PRIVACY_RESTRICTED:I = 0x1

.field public static final PRIVACY_SECRET:I = 0x3

.field public static final RELATIVE_TIME_DAYS_LIMIT:I = 0xc4

.field public static final RELATIVE_TIME_HOURS_LIMIT:I = 0xa7

.field public static final RELATIVE_TIME_INDEFINITE:I = 0xf5

.field public static final RELATIVE_TIME_MINS_LIMIT:I = 0x8f

.field public static final RELATIVE_TIME_MOBILE_INACTIVE:I = 0xf7

.field public static final RELATIVE_TIME_NOW:I = 0xf6

.field public static final RELATIVE_TIME_RESERVED:I = 0xf8

.field public static final RELATIVE_TIME_WEEKS_LIMIT:I = 0xf4

.field public static final STATUS_ACCEPTED:I = 0x0

.field public static final STATUS_BLOCKED_DESTINATION:I = 0x7

.field public static final STATUS_CANCELLED:I = 0x3

.field public static final STATUS_CANCEL_FAILED:I = 0x6

.field public static final STATUS_DELIVERED:I = 0x2

.field public static final STATUS_DEPOSITED_TO_INTERNET:I = 0x1

.field public static final STATUS_DUPLICATE_MESSAGE:I = 0x9

.field public static final STATUS_INVALID_DESTINATION:I = 0xa

.field public static final STATUS_MESSAGE_EXPIRED:I = 0xd

.field public static final STATUS_NETWORK_CONGESTION:I = 0x4

.field public static final STATUS_NETWORK_ERROR:I = 0x5

.field public static final STATUS_TEXT_TOO_LONG:I = 0x8

.field public static final STATUS_UNDEFINED:I = 0xff

.field public static final STATUS_UNKNOWN_ERROR:I = 0x1f

.field private static final SUBPARAM_ALERT_ON_MESSAGE_DELIVERY:B = 0xct

.field private static final SUBPARAM_CALLBACK_NUMBER:B = 0xet

.field private static final SUBPARAM_DEFERRED_DELIVERY_TIME_ABSOLUTE:B = 0x6t

.field private static final SUBPARAM_DEFERRED_DELIVERY_TIME_RELATIVE:B = 0x7t

.field private static final SUBPARAM_ID_LAST_DEFINED:B = 0x17t

.field private static final SUBPARAM_LANGUAGE_INDICATOR:B = 0xdt

.field private static final SUBPARAM_MESSAGE_CENTER_TIME_STAMP:B = 0x3t

.field private static final SUBPARAM_MESSAGE_DEPOSIT_INDEX:B = 0x11t

.field private static final SUBPARAM_MESSAGE_DISPLAY_MODE:B = 0xft

.field private static final SUBPARAM_MESSAGE_IDENTIFIER:B = 0x0t

.field private static final SUBPARAM_MESSAGE_STATUS:B = 0x14t

.field private static final SUBPARAM_NUMBER_OF_MESSAGES:B = 0xbt

.field private static final SUBPARAM_PRIORITY_INDICATOR:B = 0x8t

.field private static final SUBPARAM_PRIVACY_INDICATOR:B = 0x9t

.field private static final SUBPARAM_REPLY_OPTION:B = 0xat

.field private static final SUBPARAM_RESPONSE_TYPE:I = 0x80

.field private static final SUBPARAM_SERVICE_CATEGORY_PROGRAM_DATA:B = 0x12t

.field private static final SUBPARAM_SERVICE_CATEGORY_PROGRAM_RESULTS:B = 0x13t

.field private static final SUBPARAM_SESSION_INFO:I = 0x81

.field private static final SUBPARAM_USER_DATA:B = 0x1t

.field private static final SUBPARAM_USER_RESPONSE_CODE:B = 0x2t

.field private static final SUBPARAM_VALIDITY_PERIOD_ABSOLUTE:B = 0x4t

.field private static final SUBPARAM_VALIDITY_PERIOD_RELATIVE:B = 0x5t


# instance fields
.field public alert:I

.field public alertIndicatorSet:Z

.field public bgImageCategory:I

.field public bgImageNum:I

.field public callbackNumber:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

.field public cmasWarningInfo:Landroid/telephony/SmsCbCmasInfo;

.field public deferredDeliveryTimeAbsolute:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

.field public deferredDeliveryTimeRelative:I

.field public deferredDeliveryTimeRelativeSet:Z

.field public deliveryAckReq:Z

.field public depositIndex:I

.field public displayMode:I

.field public displayModeSet:Z

.field public endOfSession:I

.field public errorClass:I

.field public hasUserDataHeader:Z

.field public language:I

.field public languageIndicatorSet:Z

.field public messageId:I

.field public messageStatus:I

.field public messageStatusSet:Z

.field public messageType:I

.field public msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

.field public numberOfMessages:I

.field public priority:I

.field public priorityIndicatorSet:Z

.field public privacy:I

.field public privacyIndicatorSet:Z

.field public readAckReq:Z

.field public reportReq:Z

.field public responseType:I

.field public serviceCategoryProgramData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/telephony/cdma/CdmaSmsCbProgramData;",
            ">;"
        }
    .end annotation
.end field

.field public serviceCategoryProgramResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/telephony/cdma/CdmaSmsCbProgramResults;",
            ">;"
        }
    .end annotation
.end field

.field public sessioinId:I

.field public sessionSeq:I

.field public userAckReq:Z

.field public userData:Lcom/android/internal/telephony/cdma/sms/UserData;

.field public userResponseCode:I

.field public userResponseCodeSet:Z

.field public validityPeriodAbsolute:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

.field public validityPeriodRelative:I

.field public validityPeriodRelativeSet:Z


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/16 v2, 0xff

    #@2
    const/4 v1, 0x0

    #@3
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 130
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priorityIndicatorSet:Z

    #@8
    .line 131
    iput v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@a
    .line 142
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->privacyIndicatorSet:Z

    #@c
    .line 143
    iput v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->privacy:I

    #@e
    .line 154
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->alertIndicatorSet:Z

    #@10
    .line 155
    iput v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->alert:I

    #@12
    .line 167
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->displayModeSet:Z

    #@14
    .line 168
    const/4 v0, 0x1

    #@15
    iput v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->displayMode:I

    #@17
    .line 189
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->languageIndicatorSet:Z

    #@19
    .line 190
    iput v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->language:I

    #@1b
    .line 222
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageStatusSet:Z

    #@1d
    .line 223
    iput v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->errorClass:I

    #@1f
    .line 224
    iput v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageStatus:I

    #@21
    .line 250
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userResponseCodeSet:Z

    #@23
    .line 617
    return-void
.end method

.method public static calcTextEncodingDetails(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 7
    .parameter "msg"
    .parameter "force7BitEncoding"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 539
    invoke-static {p0, p1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->countAsciiSeptets(Ljava/lang/CharSequence;Z)I

    #@4
    move-result v1

    #@5
    .line 540
    .local v1, septets:I
    const/4 v3, -0x1

    #@6
    if-eq v1, v3, :cond_1c

    #@8
    const/16 v3, 0xa0

    #@a
    if-gt v1, v3, :cond_1c

    #@c
    .line 541
    new-instance v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@e
    invoke-direct {v2}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@11
    .line 542
    .local v2, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    iput v4, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@13
    .line 543
    iput v1, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@15
    .line 544
    rsub-int v3, v1, 0xa0

    #@17
    iput v3, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@19
    .line 545
    iput v4, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@1b
    .line 566
    :cond_1b
    :goto_1b
    return-object v2

    #@1c
    .line 547
    .end local v2           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_1c
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@1f
    move-result-object v2

    #@20
    .line 549
    .restart local v2       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    iget v3, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@22
    if-ne v3, v4, :cond_1b

    #@24
    iget v3, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@26
    if-ne v3, v4, :cond_1b

    #@28
    .line 552
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@2b
    move-result v3

    #@2c
    iput v3, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@2e
    .line 553
    iget v3, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@30
    mul-int/lit8 v0, v3, 0x2

    #@32
    .line 554
    .local v0, octets:I
    const/16 v3, 0x8c

    #@34
    if-le v0, v3, :cond_49

    #@36
    .line 555
    add-int/lit16 v3, v0, 0x85

    #@38
    div-int/lit16 v3, v3, 0x86

    #@3a
    iput v3, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@3c
    .line 557
    iget v3, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@3e
    mul-int/lit16 v3, v3, 0x86

    #@40
    sub-int/2addr v3, v0

    #@41
    div-int/lit8 v3, v3, 0x2

    #@43
    iput v3, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@45
    .line 563
    :goto_45
    const/4 v3, 0x3

    #@46
    iput v3, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@48
    goto :goto_1b

    #@49
    .line 560
    :cond_49
    iput v4, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@4b
    .line 561
    rsub-int v3, v0, 0x8c

    #@4d
    div-int/lit8 v3, v3, 0x2

    #@4f
    iput v3, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@51
    goto :goto_45
.end method

.method public static calcTextEncodingDetailsEx(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 8
    .parameter "msg"
    .parameter "force7BitEncoding"

    #@0
    .prologue
    .line 2515
    new-instance v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@2
    invoke-direct {v2}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@5
    .line 2518
    .local v2, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8
    move-result-object v4

    #@9
    const-string v5, "ksc5601"

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@e
    move-result-object v3

    #@f
    .line 2519
    .local v3, textPart:[B
    array-length v0, v3

    #@10
    .line 2521
    .local v0, byteCount:I
    iput v0, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@12
    .line 2522
    const/16 v4, 0x50

    #@14
    if-le v0, v4, :cond_22

    #@16
    .line 2523
    const/4 v4, 0x2

    #@17
    iput v4, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@19
    .line 2524
    sget v4, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@1b
    sub-int/2addr v4, v0

    #@1c
    iput v4, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@1e
    .line 2529
    :goto_1e
    const/4 v4, 0x2

    #@1f
    iput v4, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@21
    .line 2535
    .end local v0           #byteCount:I
    .end local v3           #textPart:[B
    :goto_21
    return-object v2

    #@22
    .line 2526
    .restart local v0       #byteCount:I
    .restart local v3       #textPart:[B
    :cond_22
    const/4 v4, 0x1

    #@23
    iput v4, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@25
    .line 2527
    sget v4, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@27
    sub-int/2addr v4, v0

    #@28
    iput v4, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I
    :try_end_2a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_2a} :catch_2b
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_2a} :catch_30

    #@2a
    goto :goto_1e

    #@2b
    .line 2530
    .end local v0           #byteCount:I
    .end local v3           #textPart:[B
    :catch_2b
    move-exception v1

    #@2c
    .line 2531
    .local v1, ex:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    #@2f
    goto :goto_21

    #@30
    .line 2532
    .end local v1           #ex:Ljava/io/UnsupportedEncodingException;
    :catch_30
    move-exception v1

    #@31
    .line 2533
    .local v1, ex:Ljava/lang/RuntimeException;
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    #@34
    goto :goto_21
.end method

.method private static countAsciiSeptets(Ljava/lang/CharSequence;Z)I
    .registers 7
    .parameter "msg"
    .parameter "force"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 519
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v1

    #@5
    .line 520
    .local v1, msgLen:I
    if-eqz p1, :cond_8

    #@7
    .line 526
    .end local v1           #msgLen:I
    :cond_7
    :goto_7
    return v1

    #@8
    .line 521
    .restart local v1       #msgLen:I
    :cond_8
    const/4 v0, 0x0

    #@9
    .local v0, i:I
    :goto_9
    if-ge v0, v1, :cond_7

    #@b
    .line 522
    sget-object v3, Lcom/android/internal/telephony/cdma/sms/UserData;->charToAscii:Landroid/util/SparseIntArray;

    #@d
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@10
    move-result v4

    #@11
    invoke-virtual {v3, v4, v2}, Landroid/util/SparseIntArray;->get(II)I

    #@14
    move-result v3

    #@15
    if-ne v3, v2, :cond_19

    #@17
    move v1, v2

    #@18
    .line 523
    goto :goto_7

    #@19
    .line 521
    :cond_19
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_9
.end method

.method public static decode([B)Lcom/android/internal/telephony/cdma/sms/BearerData;
    .registers 2
    .parameter "smsData"

    #@0
    .prologue
    .line 2363
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decode([BI)Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static decode([BI)Lcom/android/internal/telephony/cdma/sms/BearerData;
    .registers 14
    .parameter "smsData"
    .parameter "serviceCategory"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/16 v11, 0x17

    #@3
    const/4 v10, 0x1

    #@4
    .line 2381
    :try_start_4
    new-instance v4, Lcom/android/internal/util/BitwiseInputStream;

    #@6
    invoke-direct {v4, p0}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@9
    .line 2382
    .local v4, inStream:Lcom/android/internal/util/BitwiseInputStream;
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@b
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/BearerData;-><init>()V

    #@e
    .line 2383
    .local v0, bData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    const/4 v3, 0x0

    #@f
    .line 2384
    .local v3, foundSubparamMask:I
    :cond_f
    :goto_f
    invoke-virtual {v4}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@12
    move-result v8

    #@13
    if-lez v8, :cond_f3

    #@15
    .line 2385
    const/16 v8, 0x8

    #@17
    invoke-virtual {v4, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1a
    move-result v5

    #@1b
    .line 2386
    .local v5, subparamId:I
    shl-int v6, v10, v5

    #@1d
    .line 2388
    .local v6, subparamIdBit:I
    const/4 v8, 0x0

    #@1e
    const-string v9, "lgu_cbs"

    #@20
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@23
    move-result v8

    #@24
    if-ne v8, v10, :cond_3e

    #@26
    const/16 v8, 0x80

    #@28
    if-eq v5, v8, :cond_2e

    #@2a
    const/16 v8, 0x81

    #@2c
    if-ne v5, v8, :cond_3e

    #@2e
    .line 2390
    :cond_2e
    const/4 v6, 0x0

    #@2f
    .line 2409
    :cond_2f
    sparse-switch v5, :sswitch_data_162

    #@32
    .line 2476
    invoke-static {v0, v4, v5}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeReserved(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;I)Z

    #@35
    move-result v1

    #@36
    .line 2478
    .local v1, decodeSuccess:Z
    :goto_36
    if-eqz v1, :cond_f

    #@38
    if-ltz v5, :cond_f

    #@3a
    if-gt v5, v11, :cond_f

    #@3c
    .line 2481
    or-int/2addr v3, v6

    #@3d
    goto :goto_f

    #@3e
    .line 2399
    .end local v1           #decodeSuccess:Z
    :cond_3e
    and-int v8, v3, v6

    #@40
    if-eqz v8, :cond_2f

    #@42
    if-ltz v5, :cond_2f

    #@44
    if-gt v5, v11, :cond_2f

    #@46
    .line 2402
    new-instance v8, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@48
    new-instance v9, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v10, "illegal duplicate subparameter ("

    #@4f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v9

    #@53
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v9

    #@57
    const-string v10, ")"

    #@59
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v9

    #@5d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v9

    #@61
    invoke-direct {v8, v9}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@64
    throw v8
    :try_end_65
    .catch Lcom/android/internal/util/BitwiseInputStream$AccessException; {:try_start_4 .. :try_end_65} :catch_65
    .catch Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException; {:try_start_4 .. :try_end_65} :catch_ff

    #@65
    .line 2504
    .end local v0           #bData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    .end local v3           #foundSubparamMask:I
    .end local v4           #inStream:Lcom/android/internal/util/BitwiseInputStream;
    .end local v5           #subparamId:I
    .end local v6           #subparamIdBit:I
    :catch_65
    move-exception v2

    #@66
    .line 2505
    .local v2, ex:Lcom/android/internal/util/BitwiseInputStream$AccessException;
    const-string v8, "SMS"

    #@68
    new-instance v9, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v10, "BearerData decode failed: "

    #@6f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v9

    #@73
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v9

    #@77
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v9

    #@7b
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .end local v2           #ex:Lcom/android/internal/util/BitwiseInputStream$AccessException;
    :goto_7e
    move-object v0, v7

    #@7f
    .line 2509
    :cond_7f
    :goto_7f
    return-object v0

    #@80
    .line 2411
    .restart local v0       #bData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    .restart local v3       #foundSubparamMask:I
    .restart local v4       #inStream:Lcom/android/internal/util/BitwiseInputStream;
    .restart local v5       #subparamId:I
    .restart local v6       #subparamIdBit:I
    :sswitch_80
    :try_start_80
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeMessageId(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@83
    move-result v1

    #@84
    .line 2412
    .restart local v1       #decodeSuccess:Z
    goto :goto_36

    #@85
    .line 2414
    .end local v1           #decodeSuccess:Z
    :sswitch_85
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeUserData(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@88
    move-result v1

    #@89
    .line 2415
    .restart local v1       #decodeSuccess:Z
    goto :goto_36

    #@8a
    .line 2417
    .end local v1           #decodeSuccess:Z
    :sswitch_8a
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeUserResponseCode(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@8d
    move-result v1

    #@8e
    .line 2418
    .restart local v1       #decodeSuccess:Z
    goto :goto_36

    #@8f
    .line 2420
    .end local v1           #decodeSuccess:Z
    :sswitch_8f
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeReplyOption(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@92
    move-result v1

    #@93
    .line 2421
    .restart local v1       #decodeSuccess:Z
    goto :goto_36

    #@94
    .line 2423
    .end local v1           #decodeSuccess:Z
    :sswitch_94
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeMsgCount(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@97
    move-result v1

    #@98
    .line 2424
    .restart local v1       #decodeSuccess:Z
    goto :goto_36

    #@99
    .line 2426
    .end local v1           #decodeSuccess:Z
    :sswitch_99
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeCallbackNumber(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@9c
    move-result v1

    #@9d
    .line 2427
    .restart local v1       #decodeSuccess:Z
    goto :goto_36

    #@9e
    .line 2429
    .end local v1           #decodeSuccess:Z
    :sswitch_9e
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeMsgStatus(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@a1
    move-result v1

    #@a2
    .line 2430
    .restart local v1       #decodeSuccess:Z
    goto :goto_36

    #@a3
    .line 2432
    .end local v1           #decodeSuccess:Z
    :sswitch_a3
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeMsgCenterTimeStamp(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@a6
    move-result v1

    #@a7
    .line 2433
    .restart local v1       #decodeSuccess:Z
    goto :goto_36

    #@a8
    .line 2435
    .end local v1           #decodeSuccess:Z
    :sswitch_a8
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeValidityAbs(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@ab
    move-result v1

    #@ac
    .line 2436
    .restart local v1       #decodeSuccess:Z
    goto :goto_36

    #@ad
    .line 2438
    .end local v1           #decodeSuccess:Z
    :sswitch_ad
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeValidityRel(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@b0
    move-result v1

    #@b1
    .line 2439
    .restart local v1       #decodeSuccess:Z
    goto :goto_36

    #@b2
    .line 2441
    .end local v1           #decodeSuccess:Z
    :sswitch_b2
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeDeferredDeliveryAbs(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@b5
    move-result v1

    #@b6
    .line 2442
    .restart local v1       #decodeSuccess:Z
    goto :goto_36

    #@b7
    .line 2444
    .end local v1           #decodeSuccess:Z
    :sswitch_b7
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeDeferredDeliveryRel(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@ba
    move-result v1

    #@bb
    .line 2445
    .restart local v1       #decodeSuccess:Z
    goto/16 :goto_36

    #@bd
    .line 2447
    .end local v1           #decodeSuccess:Z
    :sswitch_bd
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodePrivacyIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@c0
    move-result v1

    #@c1
    .line 2448
    .restart local v1       #decodeSuccess:Z
    goto/16 :goto_36

    #@c3
    .line 2450
    .end local v1           #decodeSuccess:Z
    :sswitch_c3
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeLanguageIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@c6
    move-result v1

    #@c7
    .line 2451
    .restart local v1       #decodeSuccess:Z
    goto/16 :goto_36

    #@c9
    .line 2453
    .end local v1           #decodeSuccess:Z
    :sswitch_c9
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeDisplayMode(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@cc
    move-result v1

    #@cd
    .line 2454
    .restart local v1       #decodeSuccess:Z
    goto/16 :goto_36

    #@cf
    .line 2456
    .end local v1           #decodeSuccess:Z
    :sswitch_cf
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodePriorityIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@d2
    move-result v1

    #@d3
    .line 2457
    .restart local v1       #decodeSuccess:Z
    goto/16 :goto_36

    #@d5
    .line 2459
    .end local v1           #decodeSuccess:Z
    :sswitch_d5
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeMsgDeliveryAlert(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@d8
    move-result v1

    #@d9
    .line 2460
    .restart local v1       #decodeSuccess:Z
    goto/16 :goto_36

    #@db
    .line 2462
    .end local v1           #decodeSuccess:Z
    :sswitch_db
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeDepositIndex(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@de
    move-result v1

    #@df
    .line 2463
    .restart local v1       #decodeSuccess:Z
    goto/16 :goto_36

    #@e1
    .line 2465
    .end local v1           #decodeSuccess:Z
    :sswitch_e1
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeServiceCategoryProgramData(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@e4
    move-result v1

    #@e5
    .line 2466
    .restart local v1       #decodeSuccess:Z
    goto/16 :goto_36

    #@e7
    .line 2469
    .end local v1           #decodeSuccess:Z
    :sswitch_e7
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeSessionInfo(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@ea
    move-result v1

    #@eb
    .line 2470
    .restart local v1       #decodeSuccess:Z
    goto/16 :goto_36

    #@ed
    .line 2472
    .end local v1           #decodeSuccess:Z
    :sswitch_ed
    invoke-static {v0, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeResponseType(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z

    #@f0
    move-result v1

    #@f1
    .line 2473
    .restart local v1       #decodeSuccess:Z
    goto/16 :goto_36

    #@f3
    .line 2484
    .end local v1           #decodeSuccess:Z
    .end local v5           #subparamId:I
    .end local v6           #subparamIdBit:I
    :cond_f3
    and-int/lit8 v8, v3, 0x1

    #@f5
    if-nez v8, :cond_11a

    #@f7
    .line 2485
    new-instance v8, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@f9
    const-string v9, "missing MESSAGE_IDENTIFIER subparam"

    #@fb
    invoke-direct {v8, v9}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@fe
    throw v8
    :try_end_ff
    .catch Lcom/android/internal/util/BitwiseInputStream$AccessException; {:try_start_80 .. :try_end_ff} :catch_65
    .catch Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException; {:try_start_80 .. :try_end_ff} :catch_ff

    #@ff
    .line 2506
    .end local v0           #bData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    .end local v3           #foundSubparamMask:I
    .end local v4           #inStream:Lcom/android/internal/util/BitwiseInputStream;
    :catch_ff
    move-exception v2

    #@100
    .line 2507
    .local v2, ex:Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
    const-string v8, "SMS"

    #@102
    new-instance v9, Ljava/lang/StringBuilder;

    #@104
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@107
    const-string v10, "BearerData decode failed: "

    #@109
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v9

    #@10d
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v9

    #@111
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@114
    move-result-object v9

    #@115
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@118
    goto/16 :goto_7e

    #@11a
    .line 2487
    .end local v2           #ex:Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
    .restart local v0       #bData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    .restart local v3       #foundSubparamMask:I
    .restart local v4       #inStream:Lcom/android/internal/util/BitwiseInputStream;
    :cond_11a
    :try_start_11a
    iget-object v8, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@11c
    if-eqz v8, :cond_7f

    #@11e
    .line 2488
    invoke-static {p1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->isCmasAlertCategory(I)Z

    #@121
    move-result v8

    #@122
    if-eqz v8, :cond_129

    #@124
    .line 2489
    invoke-static {v0, p1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeCmasUserData(Lcom/android/internal/telephony/cdma/sms/BearerData;I)V

    #@127
    goto/16 :goto_7f

    #@129
    .line 2490
    :cond_129
    iget-object v8, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@12b
    iget v8, v8, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@12d
    if-ne v8, v10, :cond_158

    #@12f
    .line 2491
    xor-int/lit8 v8, v3, 0x1

    #@131
    xor-int/lit8 v8, v8, 0x2

    #@133
    if-eqz v8, :cond_153

    #@135
    .line 2495
    const-string v8, "SMS"

    #@137
    new-instance v9, Ljava/lang/StringBuilder;

    #@139
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@13c
    const-string v10, "IS-91 must occur without extra subparams ("

    #@13e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v9

    #@142
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@145
    move-result-object v9

    #@146
    const-string v10, ")"

    #@148
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v9

    #@14c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14f
    move-result-object v9

    #@150
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@153
    .line 2498
    :cond_153
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeIs91(Lcom/android/internal/telephony/cdma/sms/BearerData;)V

    #@156
    goto/16 :goto_7f

    #@158
    .line 2500
    :cond_158
    iget-object v8, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@15a
    iget-boolean v9, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@15c
    invoke-static {v8, v9}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeUserDataPayload(Lcom/android/internal/telephony/cdma/sms/UserData;Z)V
    :try_end_15f
    .catch Lcom/android/internal/util/BitwiseInputStream$AccessException; {:try_start_11a .. :try_end_15f} :catch_65
    .catch Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException; {:try_start_11a .. :try_end_15f} :catch_ff

    #@15f
    goto/16 :goto_7f

    #@161
    .line 2409
    nop

    #@162
    :sswitch_data_162
    .sparse-switch
        0x0 -> :sswitch_80
        0x1 -> :sswitch_85
        0x2 -> :sswitch_8a
        0x3 -> :sswitch_a3
        0x4 -> :sswitch_a8
        0x5 -> :sswitch_ad
        0x6 -> :sswitch_b2
        0x7 -> :sswitch_b7
        0x8 -> :sswitch_cf
        0x9 -> :sswitch_bd
        0xa -> :sswitch_8f
        0xb -> :sswitch_94
        0xc -> :sswitch_d5
        0xd -> :sswitch_c3
        0xe -> :sswitch_99
        0xf -> :sswitch_c9
        0x11 -> :sswitch_db
        0x12 -> :sswitch_e1
        0x14 -> :sswitch_9e
        0x80 -> :sswitch_ed
        0x81 -> :sswitch_e7
    .end sparse-switch
.end method

.method private static decode7bitAscii([BII)Ljava/lang/String;
    .registers 14
    .parameter "data"
    .parameter "offset"
    .parameter "numFields"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v10, 0x20

    #@2
    const/16 v9, 0xd

    #@4
    const/16 v8, 0xa

    #@6
    .line 1184
    mul-int/lit8 p1, p1, 0x8

    #@8
    .line 1185
    :try_start_8
    new-instance v4, Ljava/lang/StringBuffer;

    #@a
    invoke-direct {v4, p2}, Ljava/lang/StringBuffer;-><init>(I)V

    #@d
    .line 1186
    .local v4, strBuf:Ljava/lang/StringBuffer;
    new-instance v3, Lcom/android/internal/util/BitwiseInputStream;

    #@f
    invoke-direct {v3, p0}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@12
    .line 1187
    .local v3, inStream:Lcom/android/internal/util/BitwiseInputStream;
    mul-int/lit8 v6, p1, 0x8

    #@14
    mul-int/lit8 v7, p2, 0x7

    #@16
    add-int v5, v6, v7

    #@18
    .line 1188
    .local v5, wantedBits:I
    invoke-virtual {v3}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@1b
    move-result v6

    #@1c
    if-ge v6, v5, :cond_65

    #@1e
    .line 1189
    new-instance v6, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@20
    new-instance v7, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v8, "insufficient data (wanted "

    #@27
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v7

    #@2f
    const-string v8, " bits, but only have "

    #@31
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v7

    #@35
    invoke-virtual {v3}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@38
    move-result v8

    #@39
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v7

    #@3d
    const-string v8, ")"

    #@3f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v7

    #@47
    invoke-direct {v6, v7}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v6
    :try_end_4b
    .catch Lcom/android/internal/util/BitwiseInputStream$AccessException; {:try_start_8 .. :try_end_4b} :catch_4b

    #@4b
    .line 1208
    .end local v3           #inStream:Lcom/android/internal/util/BitwiseInputStream;
    .end local v4           #strBuf:Ljava/lang/StringBuffer;
    .end local v5           #wantedBits:I
    :catch_4b
    move-exception v1

    #@4c
    .line 1209
    .local v1, ex:Lcom/android/internal/util/BitwiseInputStream$AccessException;
    new-instance v6, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@4e
    new-instance v7, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v8, "7bit ASCII decode failed: "

    #@55
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v7

    #@59
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v7

    #@5d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v7

    #@61
    invoke-direct {v6, v7}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@64
    throw v6

    #@65
    .line 1192
    .end local v1           #ex:Lcom/android/internal/util/BitwiseInputStream$AccessException;
    .restart local v3       #inStream:Lcom/android/internal/util/BitwiseInputStream;
    .restart local v4       #strBuf:Ljava/lang/StringBuffer;
    .restart local v5       #wantedBits:I
    :cond_65
    :try_start_65
    invoke-virtual {v3, p1}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@68
    .line 1193
    const/4 v2, 0x0

    #@69
    .local v2, i:I
    :goto_69
    if-ge v2, p2, :cond_98

    #@6b
    .line 1194
    const/4 v6, 0x7

    #@6c
    invoke-virtual {v3, v6}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@6f
    move-result v0

    #@70
    .line 1195
    .local v0, charCode:I
    if-lt v0, v10, :cond_82

    #@72
    sget v6, Lcom/android/internal/telephony/cdma/sms/UserData;->ASCII_MAP_MAX_INDEX:I

    #@74
    if-gt v0, v6, :cond_82

    #@76
    .line 1197
    sget-object v6, Lcom/android/internal/telephony/cdma/sms/UserData;->ASCII_MAP:[C

    #@78
    add-int/lit8 v7, v0, -0x20

    #@7a
    aget-char v6, v6, v7

    #@7c
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@7f
    .line 1193
    :goto_7f
    add-int/lit8 v2, v2, 0x1

    #@81
    goto :goto_69

    #@82
    .line 1198
    :cond_82
    if-ne v0, v8, :cond_8a

    #@84
    .line 1199
    const/16 v6, 0xa

    #@86
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@89
    goto :goto_7f

    #@8a
    .line 1200
    :cond_8a
    if-ne v0, v9, :cond_92

    #@8c
    .line 1201
    const/16 v6, 0xd

    #@8e
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@91
    goto :goto_7f

    #@92
    .line 1204
    :cond_92
    const/16 v6, 0x20

    #@94
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@97
    goto :goto_7f

    #@98
    .line 1207
    .end local v0           #charCode:I
    :cond_98
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_9b
    .catch Lcom/android/internal/util/BitwiseInputStream$AccessException; {:try_start_65 .. :try_end_9b} :catch_4b

    #@9b
    move-result-object v6

    #@9c
    return-object v6
.end method

.method private static decode7bitGsm([BII)Ljava/lang/String;
    .registers 12
    .parameter "data"
    .parameter "offset"
    .parameter "numFields"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1244
    mul-int/lit8 v6, p1, 0x8

    #@3
    .line 1245
    .local v6, offsetBits:I
    add-int/lit8 v0, v6, 0x6

    #@5
    div-int/lit8 v7, v0, 0x7

    #@7
    .line 1246
    .local v7, offsetSeptets:I
    sub-int/2addr p2, v7

    #@8
    .line 1247
    mul-int/lit8 v0, v7, 0x7

    #@a
    sub-int v3, v0, v6

    #@c
    .local v3, paddingBits:I
    move-object v0, p0

    #@d
    move v1, p1

    #@e
    move v2, p2

    #@f
    move v5, v4

    #@10
    .line 1248
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BIIIII)Ljava/lang/String;

    #@13
    move-result-object v8

    #@14
    .line 1250
    .local v8, result:Ljava/lang/String;
    if-nez v8, :cond_1e

    #@16
    .line 1251
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@18
    const-string v1, "7bit GSM decoding failed"

    #@1a
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 1253
    :cond_1e
    return-object v8
.end method

.method private static decodeCallbackNumber(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 13
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/16 v10, 0x8

    #@3
    .line 1783
    const/16 v0, 0x8

    #@5
    .line 1784
    .local v0, EXPECTED_PARAM_SIZE:I
    invoke-virtual {p1, v10}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v9

    #@9
    mul-int/lit8 v6, v9, 0x8

    #@b
    .line 1785
    .local v6, paramBits:I
    if-ge v6, v10, :cond_12

    #@d
    .line 1786
    invoke-virtual {p1, v6}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@10
    .line 1787
    const/4 v8, 0x0

    #@11
    .line 1813
    :goto_11
    return v8

    #@12
    .line 1789
    :cond_12
    new-instance v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@14
    invoke-direct {v1}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;-><init>()V

    #@17
    .line 1790
    .local v1, addr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    invoke-virtual {p1, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1a
    move-result v9

    #@1b
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@1d
    .line 1791
    const/4 v4, 0x4

    #@1e
    .line 1792
    .local v4, fieldBits:B
    const/4 v2, 0x1

    #@1f
    .line 1793
    .local v2, consumedBits:B
    iget v9, v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@21
    if-ne v9, v8, :cond_34

    #@23
    .line 1794
    const/4 v9, 0x3

    #@24
    invoke-virtual {p1, v9}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@27
    move-result v9

    #@28
    iput v9, v1, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@2a
    .line 1795
    const/4 v9, 0x4

    #@2b
    invoke-virtual {p1, v9}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@2e
    move-result v9

    #@2f
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@31
    .line 1796
    const/16 v4, 0x8

    #@33
    .line 1797
    int-to-byte v2, v10

    #@34
    .line 1799
    :cond_34
    invoke-virtual {p1, v10}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@37
    move-result v9

    #@38
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@3a
    .line 1800
    add-int/lit8 v9, v2, 0x8

    #@3c
    int-to-byte v2, v9

    #@3d
    .line 1801
    sub-int v7, v6, v2

    #@3f
    .line 1802
    .local v7, remainingBits:I
    iget v9, v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@41
    mul-int v3, v9, v4

    #@43
    .line 1803
    .local v3, dataBits:I
    sub-int v5, v7, v3

    #@45
    .line 1804
    .local v5, paddingBits:I
    if-ge v7, v3, :cond_7a

    #@47
    .line 1805
    new-instance v8, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@49
    new-instance v9, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v10, "CALLBACK_NUMBER subparam encoding size error (remainingBits + "

    #@50
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v9

    #@54
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v9

    #@58
    const-string v10, ", dataBits + "

    #@5a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v9

    #@5e
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v9

    #@62
    const-string v10, ", paddingBits + "

    #@64
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v9

    #@68
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v9

    #@6c
    const-string v10, ")"

    #@6e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v9

    #@72
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v9

    #@76
    invoke-direct {v8, v9}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@79
    throw v8

    #@7a
    .line 1809
    :cond_7a
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->readByteArray(I)[B

    #@7d
    move-result-object v9

    #@7e
    iput-object v9, v1, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@80
    .line 1810
    invoke-virtual {p1, v5}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@83
    .line 1811
    invoke-static {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeSmsAddress(Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;)V

    #@86
    .line 1812
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->callbackNumber:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@88
    goto :goto_11
.end method

.method private static decodeCharset([BIIILjava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "data"
    .parameter "offset"
    .parameter "numFields"
    .parameter "width"
    .parameter "charset"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1161
    if-ltz p2, :cond_8

    #@2
    mul-int v3, p2, p3

    #@4
    add-int/2addr v3, p1

    #@5
    array-length v4, p0

    #@6
    if-le v3, v4, :cond_66

    #@8
    .line 1163
    :cond_8
    rem-int v2, p1, p3

    #@a
    .line 1164
    .local v2, padding:I
    array-length v3, p0

    #@b
    sub-int/2addr v3, p1

    #@c
    sub-int/2addr v3, v2

    #@d
    div-int v1, v3, p3

    #@f
    .line 1165
    .local v1, maxNumFields:I
    if-gez v1, :cond_2a

    #@11
    .line 1166
    new-instance v3, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@13
    new-instance v4, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    const-string v5, " decode failed: offset out of range"

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v4

    #@26
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@29
    throw v3

    #@2a
    .line 1168
    :cond_2a
    const-string v3, "SMS"

    #@2c
    new-instance v4, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string v5, " decode error: offset = "

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    const-string v5, " numFields = "

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    const-string v5, " data.length = "

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    array-length v5, p0

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    const-string v5, " maxNumFields = "

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v4

    #@62
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 1171
    move p2, v1

    #@66
    .line 1174
    .end local v1           #maxNumFields:I
    .end local v2           #padding:I
    :cond_66
    :try_start_66
    new-instance v3, Ljava/lang/String;

    #@68
    mul-int v4, p2, p3

    #@6a
    invoke-direct {v3, p0, p1, v4, p4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_6d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_66 .. :try_end_6d} :catch_6e

    #@6d
    return-object v3

    #@6e
    .line 1175
    :catch_6e
    move-exception v0

    #@6f
    .line 1176
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    new-instance v3, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@71
    new-instance v4, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    const-string v5, " decode failed: "

    #@7c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v4

    #@84
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v4

    #@88
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@8b
    throw v3
.end method

.method private static decodeCmasExpires(IIIIII)J
    .registers 16
    .parameter "year"
    .parameter "month"
    .parameter "day"
    .parameter "hours"
    .parameter "minutes"
    .parameter "seconds"

    #@0
    .prologue
    const/16 v5, 0x3b

    #@2
    const/4 v4, 0x1

    #@3
    const-wide/16 v1, -0x1

    #@5
    .line 2335
    const-wide/16 v8, -0x1

    #@7
    .line 2337
    .local v8, OUT_OF_RANGE:J
    if-ltz p0, :cond_d

    #@9
    const/16 v3, 0x63

    #@b
    if-le p0, v3, :cond_e

    #@d
    .line 2350
    :cond_d
    :goto_d
    return-wide v1

    #@e
    .line 2338
    :cond_e
    if-lt p1, v4, :cond_d

    #@10
    const/16 v3, 0xc

    #@12
    if-gt p1, v3, :cond_d

    #@14
    .line 2339
    if-lt p2, v4, :cond_d

    #@16
    const/16 v3, 0x1f

    #@18
    if-gt p2, v3, :cond_d

    #@1a
    .line 2340
    if-ltz p3, :cond_d

    #@1c
    const/16 v3, 0x17

    #@1e
    if-gt p3, v3, :cond_d

    #@20
    .line 2341
    if-ltz p4, :cond_d

    #@22
    if-gt p4, v5, :cond_d

    #@24
    .line 2342
    if-ltz p5, :cond_d

    #@26
    if-gt p5, v5, :cond_d

    #@28
    .line 2344
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@2b
    move-result-object v0

    #@2c
    .line 2345
    .local v0, expires:Ljava/util/Calendar;
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    #@2f
    move-result v7

    #@30
    .line 2347
    .local v7, CURRENT_YEAR:I
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    #@33
    .line 2348
    div-int/lit8 v1, v7, 0x64

    #@35
    mul-int/lit8 v1, v1, 0x64

    #@37
    add-int/2addr v1, p0

    #@38
    add-int/lit8 v2, p1, -0x1

    #@3a
    move v3, p2

    #@3b
    move v4, p3

    #@3c
    move v5, p4

    #@3d
    move v6, p5

    #@3e
    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    #@41
    .line 2350
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@44
    move-result-wide v1

    #@45
    goto :goto_d
.end method

.method private static decodeCmasUserData(Lcom/android/internal/telephony/cdma/sms/BearerData;I)V
    .registers 27
    .parameter "bData"
    .parameter "serviceCategory"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 2195
    new-instance v18, Lcom/android/internal/util/BitwiseInputStream;

    #@2
    move-object/from16 v0, p0

    #@4
    iget-object v8, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@6
    iget-object v8, v8, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@8
    move-object/from16 v0, v18

    #@a
    invoke-direct {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@d
    .line 2196
    .local v18, inStream:Lcom/android/internal/util/BitwiseInputStream;
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@10
    move-result v8

    #@11
    const/16 v23, 0x8

    #@13
    move/from16 v0, v23

    #@15
    if-ge v8, v0, :cond_21

    #@17
    .line 2197
    new-instance v8, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@19
    const-string v23, "emergency CB with no CMAE_protocol_version"

    #@1b
    move-object/from16 v0, v23

    #@1d
    invoke-direct {v8, v0}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@20
    throw v8

    #@21
    .line 2199
    :cond_21
    const/16 v8, 0x8

    #@23
    move-object/from16 v0, v18

    #@25
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@28
    move-result v20

    #@29
    .line 2200
    .local v20, protocolVersion:I
    if-eqz v20, :cond_4a

    #@2b
    .line 2201
    new-instance v8, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@2d
    new-instance v23, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v24, "unsupported CMAE_protocol_version "

    #@34
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v23

    #@38
    move-object/from16 v0, v23

    #@3a
    move/from16 v1, v20

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v23

    #@40
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v23

    #@44
    move-object/from16 v0, v23

    #@46
    invoke-direct {v8, v0}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@49
    throw v8

    #@4a
    .line 2204
    :cond_4a
    invoke-static/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->serviceCategoryToCmasMessageClass(I)I

    #@4d
    move-result v9

    #@4e
    .line 2205
    .local v9, messageClass:I
    const/4 v10, -0x1

    #@4f
    .line 2206
    .local v10, category:I
    const/4 v11, -0x1

    #@50
    .line 2207
    .local v11, responseType:I
    const/4 v12, -0x1

    #@51
    .line 2208
    .local v12, severity:I
    const/4 v13, -0x1

    #@52
    .line 2209
    .local v13, urgency:I
    const/4 v14, -0x1

    #@53
    .line 2213
    .local v14, certainty:I
    const/4 v2, 0x0

    #@54
    .line 2214
    .local v2, year:I
    const/4 v3, 0x1

    #@55
    .line 2215
    .local v3, month:I
    const/4 v4, 0x1

    #@56
    .line 2216
    .local v4, day:I
    const/4 v5, 0x0

    #@57
    .line 2217
    .local v5, hours:I
    const/4 v6, 0x0

    #@58
    .line 2218
    .local v6, minutes:I
    const/4 v7, 0x0

    #@59
    .line 2219
    .local v7, seconds:I
    const-wide/16 v15, -0x1

    #@5b
    .line 2223
    .local v15, expires:J
    :goto_5b
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@5e
    move-result v8

    #@5f
    const/16 v23, 0x10

    #@61
    move/from16 v0, v23

    #@63
    if-lt v8, v0, :cond_193

    #@65
    .line 2224
    const/16 v8, 0x8

    #@67
    move-object/from16 v0, v18

    #@69
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@6c
    move-result v22

    #@6d
    .line 2225
    .local v22, recordType:I
    const/16 v8, 0x8

    #@6f
    move-object/from16 v0, v18

    #@71
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@74
    move-result v21

    #@75
    .line 2226
    .local v21, recordLen:I
    packed-switch v22, :pswitch_data_1ae

    #@78
    .line 2294
    const-string v8, "SMS"

    #@7a
    new-instance v23, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v24, "skipping unsupported CMAS record type "

    #@81
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v23

    #@85
    move-object/from16 v0, v23

    #@87
    move/from16 v1, v22

    #@89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v23

    #@8d
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v23

    #@91
    move-object/from16 v0, v23

    #@93
    invoke-static {v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    .line 2295
    mul-int/lit8 v8, v21, 0x8

    #@98
    move-object/from16 v0, v18

    #@9a
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@9d
    goto :goto_5b

    #@9e
    .line 2228
    :pswitch_9e
    new-instance v17, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@a0
    invoke-direct/range {v17 .. v17}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@a3
    .line 2229
    .local v17, alertUserData:Lcom/android/internal/telephony/cdma/sms/UserData;
    const/4 v8, 0x5

    #@a4
    move-object/from16 v0, v18

    #@a6
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@a9
    move-result v8

    #@aa
    move-object/from16 v0, v17

    #@ac
    iput v8, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@ae
    .line 2230
    const/4 v8, 0x1

    #@af
    move-object/from16 v0, v17

    #@b1
    iput-boolean v8, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@b3
    .line 2231
    const/4 v8, 0x0

    #@b4
    move-object/from16 v0, v17

    #@b6
    iput v8, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgType:I

    #@b8
    .line 2234
    move-object/from16 v0, v17

    #@ba
    iget v8, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@bc
    packed-switch v8, :pswitch_data_1b8

    #@bf
    .line 2251
    :pswitch_bf
    const/16 v19, 0x0

    #@c1
    .line 2254
    .local v19, numFields:I
    :goto_c1
    const-string v8, "VZW"

    #@c3
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@c6
    move-result v8

    #@c7
    if-eqz v8, :cond_fb

    #@c9
    .line 2255
    if-nez v19, :cond_fb

    #@cb
    .line 2256
    new-instance v8, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@cd
    new-instance v23, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v24, "unsupported CMAE character set.  "

    #@d4
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v23

    #@d8
    move-object/from16 v0, v17

    #@da
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@dc
    move/from16 v24, v0

    #@de
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v23

    #@e2
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v23

    #@e6
    move-object/from16 v0, v23

    #@e8
    invoke-direct {v8, v0}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@eb
    throw v8

    #@ec
    .line 2237
    .end local v19           #numFields:I
    :pswitch_ec
    add-int/lit8 v19, v21, -0x1

    #@ee
    .line 2238
    .restart local v19       #numFields:I
    goto :goto_c1

    #@ef
    .line 2243
    .end local v19           #numFields:I
    :pswitch_ef
    mul-int/lit8 v8, v21, 0x8

    #@f1
    add-int/lit8 v8, v8, -0x5

    #@f3
    div-int/lit8 v19, v8, 0x7

    #@f5
    .line 2244
    .restart local v19       #numFields:I
    goto :goto_c1

    #@f6
    .line 2247
    .end local v19           #numFields:I
    :pswitch_f6
    add-int/lit8 v8, v21, -0x1

    #@f8
    div-int/lit8 v19, v8, 0x2

    #@fa
    .line 2248
    .restart local v19       #numFields:I
    goto :goto_c1

    #@fb
    .line 2261
    :cond_fb
    move/from16 v0, v19

    #@fd
    move-object/from16 v1, v17

    #@ff
    iput v0, v1, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@101
    .line 2262
    mul-int/lit8 v8, v21, 0x8

    #@103
    add-int/lit8 v8, v8, -0x5

    #@105
    move-object/from16 v0, v18

    #@107
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->readByteArray(I)[B

    #@10a
    move-result-object v8

    #@10b
    move-object/from16 v0, v17

    #@10d
    iput-object v8, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@10f
    .line 2263
    const/4 v8, 0x0

    #@110
    move-object/from16 v0, v17

    #@112
    invoke-static {v0, v8}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeUserDataPayload(Lcom/android/internal/telephony/cdma/sms/UserData;Z)V

    #@115
    .line 2264
    move-object/from16 v0, v17

    #@117
    move-object/from16 v1, p0

    #@119
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@11b
    goto/16 :goto_5b

    #@11d
    .line 2268
    .end local v17           #alertUserData:Lcom/android/internal/telephony/cdma/sms/UserData;
    .end local v19           #numFields:I
    :pswitch_11d
    const/16 v8, 0x8

    #@11f
    move-object/from16 v0, v18

    #@121
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@124
    move-result v10

    #@125
    .line 2269
    const/16 v8, 0x8

    #@127
    move-object/from16 v0, v18

    #@129
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@12c
    move-result v11

    #@12d
    .line 2270
    const/4 v8, 0x4

    #@12e
    move-object/from16 v0, v18

    #@130
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@133
    move-result v12

    #@134
    .line 2271
    const/4 v8, 0x4

    #@135
    move-object/from16 v0, v18

    #@137
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@13a
    move-result v13

    #@13b
    .line 2272
    const/4 v8, 0x4

    #@13c
    move-object/from16 v0, v18

    #@13e
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@141
    move-result v14

    #@142
    .line 2273
    mul-int/lit8 v8, v21, 0x8

    #@144
    add-int/lit8 v8, v8, -0x1c

    #@146
    move-object/from16 v0, v18

    #@148
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@14b
    goto/16 :goto_5b

    #@14d
    .line 2279
    :pswitch_14d
    const/16 v8, 0x18

    #@14f
    move-object/from16 v0, v18

    #@151
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@154
    .line 2280
    const/16 v8, 0x8

    #@156
    move-object/from16 v0, v18

    #@158
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@15b
    move-result v2

    #@15c
    .line 2281
    const/16 v8, 0x8

    #@15e
    move-object/from16 v0, v18

    #@160
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@163
    move-result v3

    #@164
    .line 2282
    const/16 v8, 0x8

    #@166
    move-object/from16 v0, v18

    #@168
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@16b
    move-result v4

    #@16c
    .line 2283
    const/16 v8, 0x8

    #@16e
    move-object/from16 v0, v18

    #@170
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@173
    move-result v5

    #@174
    .line 2284
    const/16 v8, 0x8

    #@176
    move-object/from16 v0, v18

    #@178
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@17b
    move-result v6

    #@17c
    .line 2285
    const/16 v8, 0x8

    #@17e
    move-object/from16 v0, v18

    #@180
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@183
    move-result v7

    #@184
    .line 2286
    mul-int/lit8 v8, v21, 0x8

    #@186
    add-int/lit8 v8, v8, -0x48

    #@188
    move-object/from16 v0, v18

    #@18a
    invoke-virtual {v0, v8}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@18d
    .line 2288
    invoke-static/range {v2 .. v7}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeCmasExpires(IIIIII)J

    #@190
    move-result-wide v15

    #@191
    .line 2289
    goto/16 :goto_5b

    #@193
    .line 2306
    .end local v21           #recordLen:I
    .end local v22           #recordType:I
    :cond_193
    const-wide/16 v23, -0x1

    #@195
    cmp-long v8, v15, v23

    #@197
    if-nez v8, :cond_1a3

    #@199
    .line 2307
    new-instance v8, Landroid/telephony/SmsCbCmasInfo;

    #@19b
    invoke-direct/range {v8 .. v14}, Landroid/telephony/SmsCbCmasInfo;-><init>(IIIIII)V

    #@19e
    move-object/from16 v0, p0

    #@1a0
    iput-object v8, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->cmasWarningInfo:Landroid/telephony/SmsCbCmasInfo;

    #@1a2
    .line 2315
    :goto_1a2
    return-void

    #@1a3
    .line 2310
    :cond_1a3
    new-instance v8, Landroid/telephony/SmsCbCmasInfo;

    #@1a5
    invoke-direct/range {v8 .. v16}, Landroid/telephony/SmsCbCmasInfo;-><init>(IIIIIIJ)V

    #@1a8
    move-object/from16 v0, p0

    #@1aa
    iput-object v8, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->cmasWarningInfo:Landroid/telephony/SmsCbCmasInfo;

    #@1ac
    goto :goto_1a2

    #@1ad
    .line 2226
    nop

    #@1ae
    :pswitch_data_1ae
    .packed-switch 0x0
        :pswitch_9e
        :pswitch_11d
        :pswitch_14d
    .end packed-switch

    #@1b8
    .line 2234
    :pswitch_data_1b8
    .packed-switch 0x0
        :pswitch_ec
        :pswitch_bf
        :pswitch_ef
        :pswitch_ef
        :pswitch_f6
        :pswitch_bf
        :pswitch_bf
        :pswitch_bf
        :pswitch_ec
        :pswitch_ef
    .end packed-switch
.end method

.method private static decodeDeferredDeliveryAbs(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x30

    #@2
    .line 1881
    const/16 v0, 0x30

    #@4
    .line 1882
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1883
    .local v1, decodeSuccess:Z
    const/16 v3, 0x8

    #@7
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@a
    move-result v3

    #@b
    mul-int/lit8 v2, v3, 0x8

    #@d
    .line 1884
    .local v2, paramBits:I
    if-lt v2, v4, :cond_1c

    #@f
    .line 1885
    add-int/lit8 v2, v2, -0x30

    #@11
    .line 1886
    const/4 v1, 0x1

    #@12
    .line 1887
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->readByteArray(I)[B

    #@15
    move-result-object v3

    #@16
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;->fromByteArray([B)Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@19
    move-result-object v3

    #@1a
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->deferredDeliveryTimeAbsolute:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@1c
    .line 1890
    :cond_1c
    if-eqz v1, :cond_20

    #@1e
    if-lez v2, :cond_4c

    #@20
    .line 1891
    :cond_20
    const-string v4, "SMS"

    #@22
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v5, "DEFERRED_DELIVERY_TIME_ABSOLUTE decode "

    #@29
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    if-eqz v1, :cond_50

    #@2f
    const-string v3, "succeeded"

    #@31
    :goto_31
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    const-string v5, " (extra bits = "

    #@37
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    const-string v5, ")"

    #@41
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 1895
    :cond_4c
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@4f
    .line 1896
    return v1

    #@50
    .line 1891
    :cond_50
    const-string v3, "failed"

    #@52
    goto :goto_31
.end method

.method private static decodeDeferredDeliveryRel(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 1923
    const/16 v0, 0x8

    #@4
    .line 1924
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1925
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 1926
    .local v2, paramBits:I
    if-lt v2, v4, :cond_16

    #@d
    .line 1927
    add-int/lit8 v2, v2, -0x8

    #@f
    .line 1928
    const/4 v1, 0x1

    #@10
    .line 1929
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@13
    move-result v3

    #@14
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->validityPeriodRelative:I

    #@16
    .line 1931
    :cond_16
    if-eqz v1, :cond_1a

    #@18
    if-lez v2, :cond_46

    #@1a
    .line 1932
    :cond_1a
    const-string v4, "SMS"

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "DEFERRED_DELIVERY_TIME_RELATIVE decode "

    #@23
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    if-eqz v1, :cond_4c

    #@29
    const-string v3, "succeeded"

    #@2b
    :goto_2b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v5, " (extra bits = "

    #@31
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    const-string v5, ")"

    #@3b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 1936
    :cond_46
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@49
    .line 1937
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->validityPeriodRelativeSet:Z

    #@4b
    .line 1938
    return v1

    #@4c
    .line 1932
    :cond_4c
    const-string v3, "failed"

    #@4e
    goto :goto_2b
.end method

.method private static decodeDepositIndex(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 1669
    const/16 v0, 0x10

    #@4
    .line 1670
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1671
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 1672
    .local v2, paramBits:I
    const/16 v3, 0x10

    #@d
    if-lt v2, v3, :cond_1f

    #@f
    .line 1673
    add-int/lit8 v2, v2, -0x10

    #@11
    .line 1674
    const/4 v1, 0x1

    #@12
    .line 1675
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@15
    move-result v3

    #@16
    shl-int/lit8 v3, v3, 0x8

    #@18
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1b
    move-result v4

    #@1c
    or-int/2addr v3, v4

    #@1d
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->depositIndex:I

    #@1f
    .line 1677
    :cond_1f
    if-eqz v1, :cond_23

    #@21
    if-lez v2, :cond_4f

    #@23
    .line 1678
    :cond_23
    const-string v4, "SMS"

    #@25
    new-instance v3, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "MESSAGE_DEPOSIT_INDEX decode "

    #@2c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    if-eqz v1, :cond_53

    #@32
    const-string v3, "succeeded"

    #@34
    :goto_34
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    const-string v5, " (extra bits = "

    #@3a
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    const-string v5, ")"

    #@44
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 1682
    :cond_4f
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@52
    .line 1683
    return v1

    #@53
    .line 1678
    :cond_53
    const-string v3, "failed"

    #@55
    goto :goto_34
.end method

.method private static decodeDisplayMode(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 1987
    const/16 v0, 0x8

    #@4
    .line 1988
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1989
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 1990
    .local v2, paramBits:I
    if-lt v2, v4, :cond_1b

    #@d
    .line 1991
    add-int/lit8 v2, v2, -0x8

    #@f
    .line 1992
    const/4 v1, 0x1

    #@10
    .line 1993
    const/4 v3, 0x2

    #@11
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@14
    move-result v3

    #@15
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->displayMode:I

    #@17
    .line 1994
    const/4 v3, 0x6

    #@18
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@1b
    .line 1996
    :cond_1b
    if-eqz v1, :cond_1f

    #@1d
    if-lez v2, :cond_4b

    #@1f
    .line 1997
    :cond_1f
    const-string v4, "SMS"

    #@21
    new-instance v3, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "DISPLAY_MODE decode "

    #@28
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    if-eqz v1, :cond_51

    #@2e
    const-string v3, "succeeded"

    #@30
    :goto_30
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    const-string v5, " (extra bits = "

    #@36
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    const-string v5, ")"

    #@40
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 2001
    :cond_4b
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@4e
    .line 2002
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->displayModeSet:Z

    #@50
    .line 2003
    return v1

    #@51
    .line 1997
    :cond_51
    const-string v3, "failed"

    #@53
    goto :goto_30
.end method

.method private static decodeDtmfSmsAddress([BI)Ljava/lang/String;
    .registers 10
    .parameter "rawData"
    .parameter "numFields"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v7, 0x30

    #@2
    const/16 v6, 0xa

    #@4
    const/4 v5, 0x1

    #@5
    .line 1745
    new-instance v1, Ljava/lang/StringBuffer;

    #@7
    invoke-direct {v1, p1}, Ljava/lang/StringBuffer;-><init>(I)V

    #@a
    .line 1746
    .local v1, strBuf:Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, p1, :cond_91

    #@d
    .line 1747
    div-int/lit8 v3, v0, 0x2

    #@f
    aget-byte v3, p0, v3

    #@11
    rem-int/lit8 v4, v0, 0x2

    #@13
    mul-int/lit8 v4, v4, 0x4

    #@15
    rsub-int/lit8 v4, v4, 0x4

    #@17
    ushr-int/2addr v3, v4

    #@18
    and-int/lit8 v2, v3, 0xf

    #@1a
    .line 1748
    .local v2, val:I
    if-lt v2, v5, :cond_2a

    #@1c
    const/16 v3, 0x9

    #@1e
    if-gt v2, v3, :cond_2a

    #@20
    invoke-static {v2, v6}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@27
    .line 1746
    :goto_27
    add-int/lit8 v0, v0, 0x1

    #@29
    goto :goto_b

    #@2a
    .line 1749
    :cond_2a
    if-ne v2, v6, :cond_30

    #@2c
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@2f
    goto :goto_27

    #@30
    .line 1750
    :cond_30
    const/16 v3, 0xb

    #@32
    if-ne v2, v3, :cond_3a

    #@34
    const/16 v3, 0x2a

    #@36
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@39
    goto :goto_27

    #@3a
    .line 1751
    :cond_3a
    const/16 v3, 0xc

    #@3c
    if-ne v2, v3, :cond_44

    #@3e
    const/16 v3, 0x23

    #@40
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@43
    goto :goto_27

    #@44
    .line 1753
    :cond_44
    if-nez v2, :cond_72

    #@46
    .line 1754
    const/4 v3, 0x0

    #@47
    const-string v4, "dtmf_error_handle"

    #@49
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@4c
    move-result v3

    #@4d
    if-ne v3, v5, :cond_53

    #@4f
    .line 1755
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@52
    goto :goto_27

    #@53
    .line 1757
    :cond_53
    new-instance v3, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "invalid SMS address DTMF code ("

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    const-string v5, ")"

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v4

    #@6e
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@71
    throw v3

    #@72
    .line 1761
    :cond_72
    new-instance v3, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@74
    new-instance v4, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v5, "invalid SMS address DTMF code ("

    #@7b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v4

    #@7f
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v4

    #@83
    const-string v5, ")"

    #@85
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v4

    #@89
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v4

    #@8d
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@90
    throw v3

    #@91
    .line 1763
    .end local v2           #val:I
    :cond_91
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@94
    move-result-object v3

    #@95
    return-object v3
.end method

.method private static decodeGsmDCSInCDMA([BIIIZLcom/android/internal/telephony/SmsHeader;)Ljava/lang/String;
    .registers 23
    .parameter "data"
    .parameter "offset"
    .parameter "datalen"
    .parameter "dataCodingScheme"
    .parameter "hasUserDataHeader"
    .parameter "userDataHeader"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1363
    const/4 v12, 0x0

    #@1
    .line 1364
    .local v12, hasMessageClass:Z
    const/16 v16, 0x0

    #@3
    .line 1365
    .local v16, userDataCompressed:Z
    const/4 v8, 0x0

    #@4
    .line 1367
    .local v8, automaticDeletion:Z
    const/4 v10, 0x0

    #@5
    .line 1368
    .local v10, encodingType:I
    const/4 v15, 0x0

    #@6
    .line 1370
    .local v15, ret:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "decodeGsmDCSInCDMA(),[KDDI] offset: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    move/from16 v0, p1

    #@13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, " // datalen: "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    move/from16 v0, p2

    #@1f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, " // dataCodingScheme: "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    move/from16 v0, p3

    #@2b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, " //hasUserDataHeader:(T/F) "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    move/from16 v0, p4

    #@37
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    const-string v3, " // data: "

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    new-instance v3, Ljava/lang/String;

    #@43
    move-object/from16 v0, p0

    #@45
    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@53
    .line 1375
    move/from16 v0, p3

    #@55
    and-int/lit16 v2, v0, 0x80

    #@57
    if-nez v2, :cond_135

    #@59
    .line 1377
    and-int/lit8 v2, p3, 0x40

    #@5b
    if-eqz v2, :cond_ff

    #@5d
    const/4 v8, 0x1

    #@5e
    .line 1378
    :goto_5e
    and-int/lit8 v2, p3, 0x20

    #@60
    if-eqz v2, :cond_102

    #@62
    const/16 v16, 0x1

    #@64
    .line 1379
    :goto_64
    and-int/lit8 v2, p3, 0x10

    #@66
    if-eqz v2, :cond_106

    #@68
    const/4 v12, 0x1

    #@69
    .line 1381
    :goto_69
    if-eqz v16, :cond_109

    #@6b
    .line 1382
    new-instance v2, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v3, "decodeGsmDCSInCDMA(), [KDDI] 4 - Unsupported SMS data coding scheme (compression) "

    #@72
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v2

    #@76
    move/from16 v0, p3

    #@78
    and-int/lit16 v3, v0, 0xff

    #@7a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v2

    #@82
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@85
    .line 1439
    :goto_85
    const/4 v14, 0x0

    #@86
    .line 1440
    .local v14, headerSeptets:I
    const/4 v5, 0x0

    #@87
    .line 1441
    .local v5, mUserDataSeptetPadding:I
    sub-int v9, p2, p1

    #@89
    .line 1442
    .local v9, count:I
    if-gez v9, :cond_8c

    #@8b
    .line 1443
    const/4 v9, 0x0

    #@8c
    .line 1446
    :cond_8c
    if-eqz p4, :cond_e5

    #@8e
    .line 1447
    const-string v2, "decodeGsmDCSInCDMA(), [KDDI] GSM_DCS has UserData header"

    #@90
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@93
    .line 1449
    mul-int/lit8 v13, p1, 0x8

    #@95
    .line 1450
    .local v13, headerBits:I
    div-int/lit8 v14, v13, 0x7

    #@97
    .line 1451
    rem-int/lit8 v2, v13, 0x7

    #@99
    if-lez v2, :cond_18e

    #@9b
    const/4 v2, 0x1

    #@9c
    :goto_9c
    add-int/2addr v14, v2

    #@9d
    .line 1452
    mul-int/lit8 v2, v14, 0x7

    #@9f
    sub-int v5, v2, v13

    #@a1
    .line 1453
    new-instance v2, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v3, "decodeGsmDCSInCDMA(), [KDDI] GSM_DCS has userData Header offset: "

    #@a8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v2

    #@ac
    move/from16 v0, p1

    #@ae
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v2

    #@b2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v2

    #@b6
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@b9
    .line 1454
    new-instance v2, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v3, "decodeGsmDCSInCDMA(), [KDDI] GSM_DCS has mUserDataSeptetPadding: "

    #@c0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v2

    #@c4
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v2

    #@c8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v2

    #@cc
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@cf
    .line 1455
    new-instance v2, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v3, "decodeGsmDCSInCDMA(), [KDDI] GSM_DCS has headerSeptets: "

    #@d6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v2

    #@da
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v2

    #@de
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v2

    #@e2
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@e5
    .line 1458
    .end local v13           #headerBits:I
    :cond_e5
    packed-switch v10, :pswitch_data_21a

    #@e8
    .line 1491
    new-instance v2, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string v3, "decodeGsmDCSInCDMA(), [KDDI] not Support DCS Type: "

    #@ef
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v2

    #@f3
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v2

    #@f7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fa
    move-result-object v2

    #@fb
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@fe
    .line 1493
    :goto_fe
    return-object v15

    #@ff
    .line 1377
    .end local v5           #mUserDataSeptetPadding:I
    .end local v9           #count:I
    .end local v14           #headerSeptets:I
    :cond_ff
    const/4 v8, 0x0

    #@100
    goto/16 :goto_5e

    #@102
    .line 1378
    :cond_102
    const/16 v16, 0x0

    #@104
    goto/16 :goto_64

    #@106
    .line 1379
    :cond_106
    const/4 v12, 0x0

    #@107
    goto/16 :goto_69

    #@109
    .line 1385
    :cond_109
    shr-int/lit8 v2, p3, 0x2

    #@10b
    and-int/lit8 v2, v2, 0x3

    #@10d
    packed-switch v2, :pswitch_data_226

    #@110
    goto/16 :goto_85

    #@112
    .line 1387
    :pswitch_112
    const/4 v10, 0x1

    #@113
    .line 1388
    goto/16 :goto_85

    #@115
    .line 1391
    :pswitch_115
    const/4 v10, 0x3

    #@116
    .line 1392
    goto/16 :goto_85

    #@118
    .line 1396
    :pswitch_118
    new-instance v2, Ljava/lang/StringBuilder;

    #@11a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11d
    const-string v3, "decodeGsmDCSInCDMA(), [KDDI] 1 - Unsupported SMS data coding scheme "

    #@11f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v2

    #@123
    move/from16 v0, p3

    #@125
    and-int/lit16 v3, v0, 0xff

    #@127
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v2

    #@12b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12e
    move-result-object v2

    #@12f
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@132
    .line 1398
    const/4 v10, 0x2

    #@133
    goto/16 :goto_85

    #@135
    .line 1402
    :cond_135
    move/from16 v0, p3

    #@137
    and-int/lit16 v2, v0, 0xf0

    #@139
    const/16 v3, 0xf0

    #@13b
    if-ne v2, v3, :cond_14b

    #@13d
    .line 1403
    const/4 v8, 0x0

    #@13e
    .line 1404
    const/4 v12, 0x1

    #@13f
    .line 1405
    const/16 v16, 0x0

    #@141
    .line 1407
    and-int/lit8 v2, p3, 0x4

    #@143
    if-nez v2, :cond_148

    #@145
    .line 1409
    const/4 v10, 0x1

    #@146
    goto/16 :goto_85

    #@148
    .line 1412
    :cond_148
    const/4 v10, 0x2

    #@149
    goto/16 :goto_85

    #@14b
    .line 1414
    :cond_14b
    move/from16 v0, p3

    #@14d
    and-int/lit16 v2, v0, 0xf0

    #@14f
    const/16 v3, 0xc0

    #@151
    if-eq v2, v3, :cond_163

    #@153
    move/from16 v0, p3

    #@155
    and-int/lit16 v2, v0, 0xf0

    #@157
    const/16 v3, 0xd0

    #@159
    if-eq v2, v3, :cond_163

    #@15b
    move/from16 v0, p3

    #@15d
    and-int/lit16 v2, v0, 0xf0

    #@15f
    const/16 v3, 0xe0

    #@161
    if-ne v2, v3, :cond_172

    #@163
    .line 1423
    :cond_163
    move/from16 v0, p3

    #@165
    and-int/lit16 v2, v0, 0xf0

    #@167
    const/16 v3, 0xe0

    #@169
    if-ne v2, v3, :cond_170

    #@16b
    .line 1424
    const/4 v10, 0x3

    #@16c
    .line 1429
    :goto_16c
    const/16 v16, 0x0

    #@16e
    goto/16 :goto_85

    #@170
    .line 1426
    :cond_170
    const/4 v10, 0x1

    #@171
    goto :goto_16c

    #@172
    .line 1435
    :cond_172
    new-instance v2, Ljava/lang/StringBuilder;

    #@174
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@177
    const-string v3, "decodeGsmDCSInCDMA(), [KDDI] 3 - Unsupported SMS data coding scheme "

    #@179
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v2

    #@17d
    move/from16 v0, p3

    #@17f
    and-int/lit16 v3, v0, 0xff

    #@181
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@184
    move-result-object v2

    #@185
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@188
    move-result-object v2

    #@189
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@18c
    goto/16 :goto_85

    #@18e
    .line 1451
    .restart local v5       #mUserDataSeptetPadding:I
    .restart local v9       #count:I
    .restart local v13       #headerBits:I
    .restart local v14       #headerSeptets:I
    :cond_18e
    const/4 v2, 0x0

    #@18f
    goto/16 :goto_9c

    #@191
    .line 1462
    .end local v13           #headerBits:I
    :pswitch_191
    const/4 v2, 0x0

    #@192
    const-string v3, "handle8bit"

    #@194
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@197
    move-result v2

    #@198
    if-eqz v2, :cond_1ae

    #@19a
    .line 1463
    const-string v2, "decodeGsmDCSInCDMA(), [KDDI] ENCODING_8BIT : messageBody is other binary data format"

    #@19c
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@19f
    .line 1464
    if-gez v9, :cond_1ac

    #@1a1
    const/4 v2, 0x0

    #@1a2
    :goto_1a2
    move-object/from16 v0, p0

    #@1a4
    move/from16 v1, p1

    #@1a6
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/GsmAlphabet;->gsm8BitUnpackedToString([BII)Ljava/lang/String;

    #@1a9
    move-result-object v15

    #@1aa
    goto/16 :goto_fe

    #@1ac
    :cond_1ac
    move v2, v9

    #@1ad
    goto :goto_1a2

    #@1ae
    .line 1467
    :cond_1ae
    const/4 v15, 0x0

    #@1af
    .line 1469
    goto/16 :goto_fe

    #@1b1
    .line 1471
    :pswitch_1b1
    sub-int v9, p2, v14

    #@1b3
    .line 1472
    if-gez v9, :cond_1e6

    #@1b5
    const/4 v4, 0x0

    #@1b6
    :goto_1b6
    if-eqz p4, :cond_1e8

    #@1b8
    if-eqz p5, :cond_1e8

    #@1ba
    move-object/from16 v0, p5

    #@1bc
    iget v6, v0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@1be
    :goto_1be
    if-eqz p4, :cond_1ea

    #@1c0
    if-eqz p5, :cond_1ea

    #@1c2
    move-object/from16 v0, p5

    #@1c4
    iget v7, v0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@1c6
    :goto_1c6
    move-object/from16 v2, p0

    #@1c8
    move/from16 v3, p1

    #@1ca
    invoke-static/range {v2 .. v7}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BIIIII)Ljava/lang/String;

    #@1cd
    move-result-object v15

    #@1ce
    .line 1477
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d3
    const-string v3, "decodeGsmDCSInCDMA(), [KDDI] GSM_DCS 7bit userData Length: "

    #@1d5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v2

    #@1d9
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v2

    #@1dd
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e0
    move-result-object v2

    #@1e1
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1e4
    goto/16 :goto_fe

    #@1e6
    :cond_1e6
    move v4, v9

    #@1e7
    .line 1472
    goto :goto_1b6

    #@1e8
    :cond_1e8
    const/4 v6, 0x0

    #@1e9
    goto :goto_1be

    #@1ea
    :cond_1ea
    const/4 v7, 0x0

    #@1eb
    goto :goto_1c6

    #@1ec
    .line 1482
    :pswitch_1ec
    :try_start_1ec
    new-instance v2, Ljava/lang/StringBuilder;

    #@1ee
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f1
    const-string v3, "decodeGsmDCSInCDMA(), [KDDI] GSM_DCS 16bit bin cnt: "

    #@1f3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v2

    #@1f7
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v2

    #@1fb
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fe
    move-result-object v2

    #@1ff
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@202
    .line 1483
    new-instance v15, Ljava/lang/String;

    #@204
    .end local v15           #ret:Ljava/lang/String;
    const-string v2, "utf-16"

    #@206
    move-object/from16 v0, p0

    #@208
    move/from16 v1, p1

    #@20a
    invoke-direct {v15, v0, v1, v9, v2}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_20d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1ec .. :try_end_20d} :catch_20f

    #@20d
    .restart local v15       #ret:Ljava/lang/String;
    goto/16 :goto_fe

    #@20f
    .line 1485
    .end local v15           #ret:Ljava/lang/String;
    :catch_20f
    move-exception v11

    #@210
    .line 1486
    .local v11, ex:Ljava/io/UnsupportedEncodingException;
    const-string v15, ""

    #@212
    .line 1487
    .restart local v15       #ret:Ljava/lang/String;
    const-string v2, "decodeGsmDCSInCDMA(),[KDDI] implausible UnsupportedEncodingException"

    #@214
    invoke-static {v2, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@217
    goto/16 :goto_fe

    #@219
    .line 1458
    nop

    #@21a
    :pswitch_data_21a
    .packed-switch 0x0
        :pswitch_191
        :pswitch_1b1
        :pswitch_191
        :pswitch_1ec
    .end packed-switch

    #@226
    .line 1385
    :pswitch_data_226
    .packed-switch 0x0
        :pswitch_112
        :pswitch_118
        :pswitch_115
        :pswitch_118
    .end packed-switch
.end method

.method private static decodeIs91(Lcom/android/internal/telephony/cdma/sms/BearerData;)V
    .registers 4
    .parameter "bData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1597
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@2
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgType:I

    #@4
    packed-switch v0, :pswitch_data_36

    #@7
    .line 1609
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "unsupported IS-91 message type ("

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@16
    iget v2, v2, Lcom/android/internal/telephony/cdma/sms/UserData;->msgType:I

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, ")"

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@29
    throw v0

    #@2a
    .line 1599
    :pswitch_2a
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeIs91VoicemailStatus(Lcom/android/internal/telephony/cdma/sms/BearerData;)V

    #@2d
    .line 1612
    :goto_2d
    return-void

    #@2e
    .line 1602
    :pswitch_2e
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeIs91Cli(Lcom/android/internal/telephony/cdma/sms/BearerData;)V

    #@31
    goto :goto_2d

    #@32
    .line 1606
    :pswitch_32
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeIs91ShortMessage(Lcom/android/internal/telephony/cdma/sms/BearerData;)V

    #@35
    goto :goto_2d

    #@36
    .line 1597
    :pswitch_data_36
    .packed-switch 0x82
        :pswitch_2a
        :pswitch_32
        :pswitch_2e
        :pswitch_32
    .end packed-switch
.end method

.method private static decodeIs91Cli(Lcom/android/internal/telephony/cdma/sms/BearerData;)V
    .registers 7
    .parameter "bData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1580
    new-instance v2, Lcom/android/internal/util/BitwiseInputStream;

    #@2
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@4
    iget-object v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@6
    invoke-direct {v2, v4}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@9
    .line 1581
    .local v2, inStream:Lcom/android/internal/util/BitwiseInputStream;
    invoke-virtual {v2}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@c
    move-result v4

    #@d
    div-int/lit8 v1, v4, 0x4

    #@f
    .line 1582
    .local v1, dataLen:I
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@11
    iget v3, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@13
    .line 1583
    .local v3, numFields:I
    const/16 v4, 0xe

    #@15
    if-gt v1, v4, :cond_1c

    #@17
    const/4 v4, 0x3

    #@18
    if-lt v1, v4, :cond_1c

    #@1a
    if-ge v1, v3, :cond_24

    #@1c
    .line 1584
    :cond_1c
    new-instance v4, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@1e
    const-string v5, "IS-91 voicemail status decoding failed"

    #@20
    invoke-direct {v4, v5}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@23
    throw v4

    #@24
    .line 1586
    :cond_24
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@26
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;-><init>()V

    #@29
    .line 1587
    .local v0, addr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    const/4 v4, 0x0

    #@2a
    iput v4, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@2c
    .line 1588
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@2e
    iget-object v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@30
    iput-object v4, v0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@32
    .line 1589
    int-to-byte v4, v3

    #@33
    iput v4, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@35
    .line 1590
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeSmsAddress(Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;)V

    #@38
    .line 1591
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->callbackNumber:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@3a
    .line 1592
    return-void
.end method

.method private static decodeIs91ShortMessage(Lcom/android/internal/telephony/cdma/sms/BearerData;)V
    .registers 8
    .parameter "bData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1557
    new-instance v2, Lcom/android/internal/util/BitwiseInputStream;

    #@2
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@4
    iget-object v5, v5, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@6
    invoke-direct {v2, v5}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@9
    .line 1558
    .local v2, inStream:Lcom/android/internal/util/BitwiseInputStream;
    invoke-virtual {v2}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@c
    move-result v5

    #@d
    div-int/lit8 v0, v5, 0x6

    #@f
    .line 1559
    .local v0, dataLen:I
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@11
    iget v3, v5, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@13
    .line 1561
    .local v3, numFields:I
    const/16 v5, 0xe

    #@15
    if-gt v3, v5, :cond_19

    #@17
    if-ge v0, v3, :cond_21

    #@19
    .line 1562
    :cond_19
    new-instance v5, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@1b
    const-string v6, "IS-91 short message decoding failed"

    #@1d
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@20
    throw v5

    #@21
    .line 1564
    :cond_21
    new-instance v4, Ljava/lang/StringBuffer;

    #@23
    invoke-direct {v4, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    #@26
    .line 1565
    .local v4, strbuf:Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    #@27
    .local v1, i:I
    :goto_27
    if-ge v1, v3, :cond_38

    #@29
    .line 1566
    sget-object v5, Lcom/android/internal/telephony/cdma/sms/UserData;->ASCII_MAP:[C

    #@2b
    const/4 v6, 0x6

    #@2c
    invoke-virtual {v2, v6}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@2f
    move-result v6

    #@30
    aget-char v5, v5, v6

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@35
    .line 1565
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_27

    #@38
    .line 1568
    :cond_38
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@3a
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@3d
    move-result-object v6

    #@3e
    iput-object v6, v5, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@40
    .line 1569
    return-void
.end method

.method private static decodeIs91VoicemailStatus(Lcom/android/internal/telephony/cdma/sms/BearerData;)V
    .registers 11
    .parameter "bData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x6

    #@1
    const/4 v8, 0x3

    #@2
    .line 1514
    new-instance v3, Lcom/android/internal/util/BitwiseInputStream;

    #@4
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@6
    iget-object v7, v7, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@8
    invoke-direct {v3, v7}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@b
    .line 1515
    .local v3, inStream:Lcom/android/internal/util/BitwiseInputStream;
    invoke-virtual {v3}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@e
    move-result v7

    #@f
    div-int/lit8 v1, v7, 0x6

    #@11
    .line 1516
    .local v1, dataLen:I
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@13
    iget v4, v7, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@15
    .line 1517
    .local v4, numFields:I
    const/16 v7, 0xe

    #@17
    if-gt v1, v7, :cond_1d

    #@19
    if-lt v1, v8, :cond_1d

    #@1b
    if-ge v1, v4, :cond_25

    #@1d
    .line 1518
    :cond_1d
    new-instance v7, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@1f
    const-string v8, "IS-91 voicemail status decoding failed"

    #@21
    invoke-direct {v7, v8}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@24
    throw v7

    #@25
    .line 1521
    :cond_25
    :try_start_25
    new-instance v6, Ljava/lang/StringBuffer;

    #@27
    invoke-direct {v6, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    #@2a
    .line 1522
    .local v6, strbuf:Ljava/lang/StringBuffer;
    :goto_2a
    invoke-virtual {v3}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@2d
    move-result v7

    #@2e
    if-lt v7, v9, :cond_57

    #@30
    .line 1523
    sget-object v7, Lcom/android/internal/telephony/cdma/sms/UserData;->ASCII_MAP:[C

    #@32
    const/4 v8, 0x6

    #@33
    invoke-virtual {v3, v8}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@36
    move-result v8

    #@37
    aget-char v7, v7, v8

    #@39
    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_3c
    .catch Ljava/lang/NumberFormatException; {:try_start_25 .. :try_end_3c} :catch_3d
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_25 .. :try_end_3c} :catch_8a

    #@3c
    goto :goto_2a

    #@3d
    .line 1538
    .end local v6           #strbuf:Ljava/lang/StringBuffer;
    :catch_3d
    move-exception v2

    #@3e
    .line 1539
    .local v2, ex:Ljava/lang/NumberFormatException;
    new-instance v7, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@40
    new-instance v8, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v9, "IS-91 voicemail status decoding failed: "

    #@47
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v8

    #@4b
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v8

    #@4f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v8

    #@53
    invoke-direct {v7, v8}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@56
    throw v7

    #@57
    .line 1525
    .end local v2           #ex:Ljava/lang/NumberFormatException;
    .restart local v6       #strbuf:Ljava/lang/StringBuffer;
    :cond_57
    :try_start_57
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    .line 1526
    .local v0, data:Ljava/lang/String;
    const/4 v7, 0x0

    #@5c
    const/4 v8, 0x2

    #@5d
    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@60
    move-result-object v7

    #@61
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@64
    move-result v7

    #@65
    iput v7, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->numberOfMessages:I

    #@67
    .line 1527
    const/4 v7, 0x2

    #@68
    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    #@6b
    move-result v5

    #@6c
    .line 1528
    .local v5, prioCode:C
    const/16 v7, 0x20

    #@6e
    if-ne v5, v7, :cond_82

    #@70
    .line 1529
    const/4 v7, 0x0

    #@71
    iput v7, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@73
    .line 1536
    :goto_73
    const/4 v7, 0x1

    #@74
    iput-boolean v7, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priorityIndicatorSet:Z

    #@76
    .line 1537
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@78
    const/4 v8, 0x3

    #@79
    add-int/lit8 v9, v4, -0x3

    #@7b
    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7e
    move-result-object v8

    #@7f
    iput-object v8, v7, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@81
    .line 1543
    return-void

    #@82
    .line 1530
    :cond_82
    const/16 v7, 0x21

    #@84
    if-ne v5, v7, :cond_a4

    #@86
    .line 1531
    const/4 v7, 0x2

    #@87
    iput v7, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I
    :try_end_89
    .catch Ljava/lang/NumberFormatException; {:try_start_57 .. :try_end_89} :catch_3d
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_57 .. :try_end_89} :catch_8a

    #@89
    goto :goto_73

    #@8a
    .line 1540
    .end local v0           #data:Ljava/lang/String;
    .end local v5           #prioCode:C
    .end local v6           #strbuf:Ljava/lang/StringBuffer;
    :catch_8a
    move-exception v2

    #@8b
    .line 1541
    .local v2, ex:Ljava/lang/IndexOutOfBoundsException;
    new-instance v7, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@8d
    new-instance v8, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v9, "IS-91 voicemail status decoding failed: "

    #@94
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v8

    #@98
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v8

    #@9c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v8

    #@a0
    invoke-direct {v7, v8}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@a3
    throw v7

    #@a4
    .line 1533
    .end local v2           #ex:Ljava/lang/IndexOutOfBoundsException;
    .restart local v0       #data:Ljava/lang/String;
    .restart local v5       #prioCode:C
    .restart local v6       #strbuf:Ljava/lang/StringBuffer;
    :cond_a4
    :try_start_a4
    new-instance v7, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@a6
    new-instance v8, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v9, "IS-91 voicemail status decoding failed: illegal priority setting ("

    #@ad
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v8

    #@b1
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v8

    #@b5
    const-string v9, ")"

    #@b7
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v8

    #@bb
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v8

    #@bf
    invoke-direct {v7, v8}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@c2
    throw v7
    :try_end_c3
    .catch Ljava/lang/NumberFormatException; {:try_start_a4 .. :try_end_c3} :catch_3d
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_a4 .. :try_end_c3} :catch_8a
.end method

.method private static decodeKsc5601([BII)Ljava/lang/String;
    .registers 11
    .parameter "data"
    .parameter "offset"
    .parameter "numFields"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1218
    mul-int/lit8 p1, p1, 0x8

    #@2
    .line 1219
    :try_start_2
    new-instance v3, Lcom/android/internal/util/BitwiseInputStream;

    #@4
    invoke-direct {v3, p0}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@7
    .line 1220
    .local v3, inStream:Lcom/android/internal/util/BitwiseInputStream;
    mul-int/lit8 v5, p2, 0x8

    #@9
    add-int v4, p1, v5

    #@b
    .line 1221
    .local v4, wantedBits:I
    invoke-virtual {v3}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@e
    move-result v5

    #@f
    if-ge v5, v4, :cond_58

    #@11
    .line 1222
    new-instance v5, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@13
    new-instance v6, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v7, "insufficient data (wanted "

    #@1a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v6

    #@1e
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v6

    #@22
    const-string v7, " bits, but only have "

    #@24
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v3}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@2b
    move-result v7

    #@2c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v6

    #@30
    const-string v7, ")"

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v6

    #@3a
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v5
    :try_end_3e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_3e} :catch_3e
    .catch Lcom/android/internal/util/BitwiseInputStream$AccessException; {:try_start_2 .. :try_end_3e} :catch_8f

    #@3e
    .line 1232
    .end local v3           #inStream:Lcom/android/internal/util/BitwiseInputStream;
    .end local v4           #wantedBits:I
    :catch_3e
    move-exception v0

    #@3f
    .line 1233
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    new-instance v5, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@41
    new-instance v6, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v7, "KSC5601 decode failed: "

    #@48
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v6

    #@4c
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v6

    #@50
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v6

    #@54
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@57
    throw v5

    #@58
    .line 1225
    .end local v0           #ex:Ljava/io/UnsupportedEncodingException;
    .restart local v3       #inStream:Lcom/android/internal/util/BitwiseInputStream;
    .restart local v4       #wantedBits:I
    :cond_58
    :try_start_58
    invoke-virtual {v3, p1}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@5b
    .line 1226
    new-array v1, p2, [B

    #@5d
    .line 1227
    .local v1, expandedData:[B
    const/4 v2, 0x0

    #@5e
    .local v2, i:I
    :goto_5e
    if-ge v2, p2, :cond_86

    #@60
    .line 1228
    const/16 v5, 0x8

    #@62
    invoke-virtual {v3, v5}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@65
    move-result v5

    #@66
    int-to-byte v5, v5

    #@67
    aput-byte v5, v1, v2

    #@69
    .line 1229
    const-string v5, "SMS"

    #@6b
    new-instance v6, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v7, "expandedData[i] = "

    #@72
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v6

    #@76
    aget-byte v7, v1, v2

    #@78
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v6

    #@80
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 1227
    add-int/lit8 v2, v2, 0x1

    #@85
    goto :goto_5e

    #@86
    .line 1231
    :cond_86
    new-instance v5, Ljava/lang/String;

    #@88
    const/4 v6, 0x0

    #@89
    const-string v7, "KSC5601"

    #@8b
    invoke-direct {v5, v1, v6, p2, v7}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_8e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_58 .. :try_end_8e} :catch_3e
    .catch Lcom/android/internal/util/BitwiseInputStream$AccessException; {:try_start_58 .. :try_end_8e} :catch_8f

    #@8e
    return-object v5

    #@8f
    .line 1234
    .end local v1           #expandedData:[B
    .end local v2           #i:I
    .end local v3           #inStream:Lcom/android/internal/util/BitwiseInputStream;
    .end local v4           #wantedBits:I
    :catch_8f
    move-exception v0

    #@90
    .line 1235
    .local v0, ex:Lcom/android/internal/util/BitwiseInputStream$AccessException;
    new-instance v5, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@92
    new-instance v6, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    const-string v7, "KSC5601 decode failed: "

    #@99
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v6

    #@9d
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v6

    #@a1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v6

    #@a5
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@a8
    throw v5
.end method

.method private static decodeLanguageIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 1966
    const/16 v0, 0x8

    #@4
    .line 1967
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1968
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 1969
    .local v2, paramBits:I
    if-lt v2, v4, :cond_16

    #@d
    .line 1970
    add-int/lit8 v2, v2, -0x8

    #@f
    .line 1971
    const/4 v1, 0x1

    #@10
    .line 1972
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@13
    move-result v3

    #@14
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->language:I

    #@16
    .line 1974
    :cond_16
    if-eqz v1, :cond_1a

    #@18
    if-lez v2, :cond_46

    #@1a
    .line 1975
    :cond_1a
    const-string v4, "SMS"

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "LANGUAGE_INDICATOR decode "

    #@23
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    if-eqz v1, :cond_4c

    #@29
    const-string v3, "succeeded"

    #@2b
    :goto_2b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v5, " (extra bits = "

    #@31
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    const-string v5, ")"

    #@3b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 1979
    :cond_46
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@49
    .line 1980
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->languageIndicatorSet:Z

    #@4b
    .line 1981
    return v1

    #@4c
    .line 1975
    :cond_4c
    const-string v3, "failed"

    #@4e
    goto :goto_2b
.end method

.method private static decodeLatin([BII)Ljava/lang/String;
    .registers 5
    .parameter "data"
    .parameter "offset"
    .parameter "numFields"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1259
    const/4 v0, 0x1

    #@1
    const-string v1, "ISO-8859-1"

    #@3
    invoke-static {p0, p1, p2, v0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeCharset([BIIILjava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method private static decodeMessageId(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v5, 0x8

    #@2
    const/4 v3, 0x1

    #@3
    .line 1070
    const/16 v0, 0x18

    #@5
    .line 1071
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@6
    .line 1072
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v5}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@9
    move-result v4

    #@a
    mul-int/lit8 v2, v4, 0x8

    #@c
    .line 1073
    .local v2, paramBits:I
    const/16 v4, 0x18

    #@e
    if-lt v2, v4, :cond_37

    #@10
    .line 1074
    add-int/lit8 v2, v2, -0x18

    #@12
    .line 1075
    const/4 v1, 0x1

    #@13
    .line 1076
    const/4 v4, 0x4

    #@14
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@17
    move-result v4

    #@18
    iput v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    #@1a
    .line 1077
    invoke-virtual {p1, v5}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1d
    move-result v4

    #@1e
    shl-int/lit8 v4, v4, 0x8

    #@20
    iput v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@22
    .line 1078
    iget v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@24
    invoke-virtual {p1, v5}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@27
    move-result v5

    #@28
    or-int/2addr v4, v5

    #@29
    iput v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@2b
    .line 1079
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@2e
    move-result v4

    #@2f
    if-ne v4, v3, :cond_6b

    #@31
    :goto_31
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@33
    .line 1080
    const/4 v3, 0x3

    #@34
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@37
    .line 1082
    :cond_37
    if-eqz v1, :cond_3b

    #@39
    if-lez v2, :cond_67

    #@3b
    .line 1083
    :cond_3b
    const-string v4, "SMS"

    #@3d
    new-instance v3, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v5, "MESSAGE_IDENTIFIER decode "

    #@44
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    if-eqz v1, :cond_6d

    #@4a
    const-string v3, "succeeded"

    #@4c
    :goto_4c
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v3

    #@50
    const-string v5, " (extra bits = "

    #@52
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    const-string v5, ")"

    #@5c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 1087
    :cond_67
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@6a
    .line 1088
    return v1

    #@6b
    .line 1079
    :cond_6b
    const/4 v3, 0x0

    #@6c
    goto :goto_31

    #@6d
    .line 1083
    :cond_6d
    const-string v3, "failed"

    #@6f
    goto :goto_4c
.end method

.method private static decodeMsgCenterTimeStamp(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x30

    #@2
    .line 1841
    const/16 v0, 0x30

    #@4
    .line 1842
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1843
    .local v1, decodeSuccess:Z
    const/16 v3, 0x8

    #@7
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@a
    move-result v3

    #@b
    mul-int/lit8 v2, v3, 0x8

    #@d
    .line 1844
    .local v2, paramBits:I
    if-lt v2, v4, :cond_1c

    #@f
    .line 1845
    add-int/lit8 v2, v2, -0x30

    #@11
    .line 1846
    const/4 v1, 0x1

    #@12
    .line 1847
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->readByteArray(I)[B

    #@15
    move-result-object v3

    #@16
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;->fromByteArray([B)Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@19
    move-result-object v3

    #@1a
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@1c
    .line 1849
    :cond_1c
    if-eqz v1, :cond_20

    #@1e
    if-lez v2, :cond_4c

    #@20
    .line 1850
    :cond_20
    const-string v4, "SMS"

    #@22
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v5, "MESSAGE_CENTER_TIME_STAMP decode "

    #@29
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    if-eqz v1, :cond_50

    #@2f
    const-string v3, "succeeded"

    #@31
    :goto_31
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    const-string v5, " (extra bits = "

    #@37
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    const-string v5, ")"

    #@41
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 1854
    :cond_4c
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@4f
    .line 1855
    return v1

    #@50
    .line 1850
    :cond_50
    const-string v3, "failed"

    #@52
    goto :goto_31
.end method

.method private static decodeMsgCount(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 9
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    .line 1641
    const/16 v0, 0x8

    #@4
    .line 1642
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1643
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v6}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 1644
    .local v2, paramBits:I
    if-lt v2, v6, :cond_21

    #@d
    .line 1645
    add-int/lit8 v2, v2, -0x8

    #@f
    .line 1646
    const/4 v1, 0x1

    #@10
    .line 1648
    const/4 v3, 0x1

    #@11
    const/4 v4, 0x0

    #@12
    const-string v5, "vmn_count_hex_conversion"

    #@14
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@17
    move-result v4

    #@18
    if-ne v3, v4, :cond_55

    #@1a
    .line 1649
    invoke-virtual {p1, v6}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1d
    move-result v3

    #@1e
    int-to-byte v3, v3

    #@1f
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->numberOfMessages:I

    #@21
    .line 1657
    :cond_21
    :goto_21
    if-eqz v1, :cond_25

    #@23
    if-lez v2, :cond_51

    #@25
    .line 1658
    :cond_25
    const-string v4, "SMS"

    #@27
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v5, "NUMBER_OF_MESSAGES decode "

    #@2e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    if-eqz v1, :cond_61

    #@34
    const-string v3, "succeeded"

    #@36
    :goto_36
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    const-string v5, " (extra bits = "

    #@3c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    const-string v5, ")"

    #@46
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v3

    #@4e
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 1662
    :cond_51
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@54
    .line 1663
    return v1

    #@55
    .line 1651
    :cond_55
    invoke-virtual {p1, v6}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@58
    move-result v3

    #@59
    int-to-byte v3, v3

    #@5a
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@5d
    move-result v3

    #@5e
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->numberOfMessages:I

    #@60
    goto :goto_21

    #@61
    .line 1658
    :cond_61
    const-string v3, "failed"

    #@63
    goto :goto_36
.end method

.method private static decodeMsgDeliveryAlert(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 2031
    const/16 v0, 0x8

    #@4
    .line 2032
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 2033
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 2034
    .local v2, paramBits:I
    if-lt v2, v4, :cond_1b

    #@d
    .line 2035
    add-int/lit8 v2, v2, -0x8

    #@f
    .line 2036
    const/4 v1, 0x1

    #@10
    .line 2037
    const/4 v3, 0x2

    #@11
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@14
    move-result v3

    #@15
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->alert:I

    #@17
    .line 2038
    const/4 v3, 0x6

    #@18
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@1b
    .line 2040
    :cond_1b
    if-eqz v1, :cond_1f

    #@1d
    if-lez v2, :cond_4b

    #@1f
    .line 2041
    :cond_1f
    const-string v4, "SMS"

    #@21
    new-instance v3, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "ALERT_ON_MESSAGE_DELIVERY decode "

    #@28
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    if-eqz v1, :cond_51

    #@2e
    const-string v3, "succeeded"

    #@30
    :goto_30
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    const-string v5, " (extra bits = "

    #@36
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    const-string v5, ")"

    #@40
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 2045
    :cond_4b
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@4e
    .line 2046
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->alertIndicatorSet:Z

    #@50
    .line 2047
    return v1

    #@51
    .line 2041
    :cond_51
    const-string v3, "failed"

    #@53
    goto :goto_30
.end method

.method private static decodeMsgStatus(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 1819
    const/16 v0, 0x8

    #@4
    .line 1820
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1821
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 1822
    .local v2, paramBits:I
    if-lt v2, v4, :cond_1e

    #@d
    .line 1823
    add-int/lit8 v2, v2, -0x8

    #@f
    .line 1824
    const/4 v1, 0x1

    #@10
    .line 1825
    const/4 v3, 0x2

    #@11
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@14
    move-result v3

    #@15
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->errorClass:I

    #@17
    .line 1826
    const/4 v3, 0x6

    #@18
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1b
    move-result v3

    #@1c
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageStatus:I

    #@1e
    .line 1828
    :cond_1e
    if-eqz v1, :cond_22

    #@20
    if-lez v2, :cond_4e

    #@22
    .line 1829
    :cond_22
    const-string v4, "SMS"

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v5, "MESSAGE_STATUS decode "

    #@2b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    if-eqz v1, :cond_54

    #@31
    const-string v3, "succeeded"

    #@33
    :goto_33
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    const-string v5, " (extra bits = "

    #@39
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    const-string v5, ")"

    #@43
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 1833
    :cond_4e
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@51
    .line 1834
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageStatusSet:Z

    #@53
    .line 1835
    return v1

    #@54
    .line 1829
    :cond_54
    const-string v3, "failed"

    #@56
    goto :goto_33
.end method

.method private static decodePriorityIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 2009
    const/16 v0, 0x8

    #@4
    .line 2010
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 2011
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 2012
    .local v2, paramBits:I
    if-lt v2, v4, :cond_1b

    #@d
    .line 2013
    add-int/lit8 v2, v2, -0x8

    #@f
    .line 2014
    const/4 v1, 0x1

    #@10
    .line 2015
    const/4 v3, 0x2

    #@11
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@14
    move-result v3

    #@15
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@17
    .line 2016
    const/4 v3, 0x6

    #@18
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@1b
    .line 2018
    :cond_1b
    if-eqz v1, :cond_1f

    #@1d
    if-lez v2, :cond_4b

    #@1f
    .line 2019
    :cond_1f
    const-string v4, "SMS"

    #@21
    new-instance v3, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "PRIORITY_INDICATOR decode "

    #@28
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    if-eqz v1, :cond_51

    #@2e
    const-string v3, "succeeded"

    #@30
    :goto_30
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    const-string v5, " (extra bits = "

    #@36
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    const-string v5, ")"

    #@40
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 2023
    :cond_4b
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@4e
    .line 2024
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priorityIndicatorSet:Z

    #@50
    .line 2025
    return v1

    #@51
    .line 2019
    :cond_51
    const-string v3, "failed"

    #@53
    goto :goto_30
.end method

.method private static decodePrivacyIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 1944
    const/16 v0, 0x8

    #@4
    .line 1945
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1946
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 1947
    .local v2, paramBits:I
    if-lt v2, v4, :cond_1b

    #@d
    .line 1948
    add-int/lit8 v2, v2, -0x8

    #@f
    .line 1949
    const/4 v1, 0x1

    #@10
    .line 1950
    const/4 v3, 0x2

    #@11
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@14
    move-result v3

    #@15
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->privacy:I

    #@17
    .line 1951
    const/4 v3, 0x6

    #@18
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@1b
    .line 1953
    :cond_1b
    if-eqz v1, :cond_1f

    #@1d
    if-lez v2, :cond_4b

    #@1f
    .line 1954
    :cond_1f
    const-string v4, "SMS"

    #@21
    new-instance v3, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "PRIVACY_INDICATOR decode "

    #@28
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    if-eqz v1, :cond_51

    #@2e
    const-string v3, "succeeded"

    #@30
    :goto_30
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    const-string v5, " (extra bits = "

    #@36
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    const-string v5, ")"

    #@40
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 1958
    :cond_4b
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@4e
    .line 1959
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->privacyIndicatorSet:Z

    #@50
    .line 1960
    return v1

    #@51
    .line 1954
    :cond_51
    const-string v3, "failed"

    #@53
    goto :goto_30
.end method

.method private static decodeReplyOption(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 9
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    .line 1617
    const/16 v0, 0x8

    #@6
    .line 1618
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@7
    .line 1619
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v6}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@a
    move-result v3

    #@b
    mul-int/lit8 v2, v3, 0x8

    #@d
    .line 1620
    .local v2, paramBits:I
    if-lt v2, v6, :cond_39

    #@f
    .line 1621
    add-int/lit8 v2, v2, -0x8

    #@11
    .line 1622
    const/4 v1, 0x1

    #@12
    .line 1623
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@15
    move-result v3

    #@16
    if-ne v3, v4, :cond_6d

    #@18
    move v3, v4

    #@19
    :goto_19
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userAckReq:Z

    #@1b
    .line 1624
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1e
    move-result v3

    #@1f
    if-ne v3, v4, :cond_6f

    #@21
    move v3, v4

    #@22
    :goto_22
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->deliveryAckReq:Z

    #@24
    .line 1625
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@27
    move-result v3

    #@28
    if-ne v3, v4, :cond_71

    #@2a
    move v3, v4

    #@2b
    :goto_2b
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->readAckReq:Z

    #@2d
    .line 1626
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@30
    move-result v3

    #@31
    if-ne v3, v4, :cond_73

    #@33
    :goto_33
    iput-boolean v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->reportReq:Z

    #@35
    .line 1627
    const/4 v3, 0x4

    #@36
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@39
    .line 1629
    :cond_39
    if-eqz v1, :cond_3d

    #@3b
    if-lez v2, :cond_69

    #@3d
    .line 1630
    :cond_3d
    const-string v4, "SMS"

    #@3f
    new-instance v3, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v5, "REPLY_OPTION decode "

    #@46
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    if-eqz v1, :cond_75

    #@4c
    const-string v3, "succeeded"

    #@4e
    :goto_4e
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    const-string v5, " (extra bits = "

    #@54
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v3

    #@5c
    const-string v5, ")"

    #@5e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v3

    #@66
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 1634
    :cond_69
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@6c
    .line 1635
    return v1

    #@6d
    :cond_6d
    move v3, v5

    #@6e
    .line 1623
    goto :goto_19

    #@6f
    :cond_6f
    move v3, v5

    #@70
    .line 1624
    goto :goto_22

    #@71
    :cond_71
    move v3, v5

    #@72
    .line 1625
    goto :goto_2b

    #@73
    :cond_73
    move v4, v5

    #@74
    .line 1626
    goto :goto_33

    #@75
    .line 1630
    :cond_75
    const-string v3, "failed"

    #@77
    goto :goto_4e
.end method

.method private static decodeReserved(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;I)Z
    .registers 9
    .parameter "bData"
    .parameter "inStream"
    .parameter "subparamId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1095
    const/4 v0, 0x0

    #@1
    .line 1096
    .local v0, decodeSuccess:Z
    const/16 v3, 0x8

    #@3
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@6
    move-result v2

    #@7
    .line 1097
    .local v2, subparamLen:I
    mul-int/lit8 v1, v2, 0x8

    #@9
    .line 1098
    .local v1, paramBits:I
    invoke-virtual {p1}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@c
    move-result v3

    #@d
    if-gt v1, v3, :cond_13

    #@f
    .line 1099
    const/4 v0, 0x1

    #@10
    .line 1100
    invoke-virtual {p1, v1}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@13
    .line 1102
    :cond_13
    const-string v4, "SMS"

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v5, "RESERVED bearer data subparameter "

    #@1c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    const-string v5, " decode "

    #@26
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    if-eqz v0, :cond_6e

    #@2c
    const-string v3, "succeeded"

    #@2e
    :goto_2e
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    const-string v5, " (param bits = "

    #@34
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    const-string v5, ")"

    #@3e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 1104
    if-nez v0, :cond_71

    #@4b
    .line 1105
    new-instance v3, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@4d
    new-instance v4, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v5, "RESERVED bearer data subparameter "

    #@54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    const-string v5, " had invalid SUBPARAM_LEN "

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v4

    #@6a
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@6d
    throw v3

    #@6e
    .line 1102
    :cond_6e
    const-string v3, "failed"

    #@70
    goto :goto_2e

    #@71
    .line 1109
    :cond_71
    return v0
.end method

.method private static decodeResponseType(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 1714
    const/16 v0, 0x8

    #@4
    .line 1715
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1717
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 1719
    .local v2, paramBits:I
    if-lt v2, v4, :cond_20

    #@d
    .line 1720
    const/4 v1, 0x1

    #@e
    .line 1721
    const/16 v3, 0x10

    #@10
    if-lt v2, v3, :cond_51

    #@12
    .line 1722
    add-int/lit8 v2, v2, -0x10

    #@14
    .line 1723
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@17
    move-result v3

    #@18
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->bgImageCategory:I

    #@1a
    .line 1724
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1d
    move-result v3

    #@1e
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->bgImageNum:I

    #@20
    .line 1731
    :cond_20
    :goto_20
    if-eqz v1, :cond_24

    #@22
    if-lez v2, :cond_50

    #@24
    .line 1732
    :cond_24
    const-string v4, "SMS"

    #@26
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v5, "RESPONSE TYPE decode "

    #@2d
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    if-eqz v1, :cond_5a

    #@33
    const-string v3, "succeeded"

    #@35
    :goto_35
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    const-string v5, " (extra bits = "

    #@3b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    const-string v5, ")"

    #@45
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 1736
    :cond_50
    return v1

    #@51
    .line 1726
    :cond_51
    add-int/lit8 v2, v2, -0x8

    #@53
    .line 1727
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@56
    move-result v3

    #@57
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->responseType:I

    #@59
    goto :goto_20

    #@5a
    .line 1732
    :cond_5a
    const-string v3, "failed"

    #@5c
    goto :goto_35
.end method

.method private static decodeServiceCategoryProgramData(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 22
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 2074
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@3
    move-result v17

    #@4
    const/16 v18, 0xd

    #@6
    move/from16 v0, v17

    #@8
    move/from16 v1, v18

    #@a
    if-ge v0, v1, :cond_2f

    #@c
    .line 2075
    new-instance v17, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@e
    new-instance v18, Ljava/lang/StringBuilder;

    #@10
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v19, "SERVICE_CATEGORY_PROGRAM_DATA decode failed: only "

    #@15
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v18

    #@19
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@1c
    move-result v19

    #@1d
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v18

    #@21
    const-string v19, " bits available"

    #@23
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v18

    #@27
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v18

    #@2b
    invoke-direct/range {v17 .. v18}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v17

    #@2f
    .line 2079
    :cond_2f
    const/16 v17, 0x8

    #@31
    move-object/from16 v0, p1

    #@33
    move/from16 v1, v17

    #@35
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@38
    move-result v17

    #@39
    mul-int/lit8 v13, v17, 0x8

    #@3b
    .line 2080
    .local v13, paramBits:I
    const/16 v17, 0x5

    #@3d
    move-object/from16 v0, p1

    #@3f
    move/from16 v1, v17

    #@41
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@44
    move-result v11

    #@45
    .line 2081
    .local v11, msgEncoding:I
    add-int/lit8 v13, v13, -0x5

    #@47
    .line 2083
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@4a
    move-result v17

    #@4b
    move/from16 v0, v17

    #@4d
    if-ge v0, v13, :cond_7e

    #@4f
    .line 2084
    new-instance v17, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@51
    new-instance v18, Ljava/lang/StringBuilder;

    #@53
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v19, "SERVICE_CATEGORY_PROGRAM_DATA decode failed: only "

    #@58
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v18

    #@5c
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/util/BitwiseInputStream;->available()I

    #@5f
    move-result v19

    #@60
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v18

    #@64
    const-string v19, " bits available ("

    #@66
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v18

    #@6a
    move-object/from16 v0, v18

    #@6c
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v18

    #@70
    const-string v19, " bits expected)"

    #@72
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v18

    #@76
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v18

    #@7a
    invoke-direct/range {v17 .. v18}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@7d
    throw v17

    #@7e
    .line 2088
    :cond_7e
    new-instance v14, Ljava/util/ArrayList;

    #@80
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    #@83
    .line 2090
    .local v14, programDataList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/cdma/CdmaSmsCbProgramData;>;"
    const/16 v9, 0x30

    #@85
    .line 2091
    .local v9, CATEGORY_FIELD_MIN_SIZE:I
    const/4 v10, 0x0

    #@86
    .line 2092
    .local v10, decodeSuccess:Z
    :goto_86
    const/16 v17, 0x30

    #@88
    move/from16 v0, v17

    #@8a
    if-lt v13, v0, :cond_147

    #@8c
    .line 2093
    const/16 v17, 0x4

    #@8e
    move-object/from16 v0, p1

    #@90
    move/from16 v1, v17

    #@92
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@95
    move-result v3

    #@96
    .line 2094
    .local v3, operation:I
    const/16 v17, 0x8

    #@98
    move-object/from16 v0, p1

    #@9a
    move/from16 v1, v17

    #@9c
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@9f
    move-result v17

    #@a0
    shl-int/lit8 v17, v17, 0x8

    #@a2
    const/16 v18, 0x8

    #@a4
    move-object/from16 v0, p1

    #@a6
    move/from16 v1, v18

    #@a8
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@ab
    move-result v18

    #@ac
    or-int v4, v17, v18

    #@ae
    .line 2095
    .local v4, category:I
    const/16 v17, 0x8

    #@b0
    move-object/from16 v0, p1

    #@b2
    move/from16 v1, v17

    #@b4
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@b7
    move-result v5

    #@b8
    .line 2096
    .local v5, language:I
    const/16 v17, 0x8

    #@ba
    move-object/from16 v0, p1

    #@bc
    move/from16 v1, v17

    #@be
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@c1
    move-result v6

    #@c2
    .line 2097
    .local v6, maxMessages:I
    const/16 v17, 0x4

    #@c4
    move-object/from16 v0, p1

    #@c6
    move/from16 v1, v17

    #@c8
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@cb
    move-result v7

    #@cc
    .line 2098
    .local v7, alertOption:I
    const/16 v17, 0x8

    #@ce
    move-object/from16 v0, p1

    #@d0
    move/from16 v1, v17

    #@d2
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@d5
    move-result v12

    #@d6
    .line 2099
    .local v12, numFields:I
    add-int/lit8 v13, v13, -0x30

    #@d8
    .line 2101
    invoke-static {v11, v12}, Lcom/android/internal/telephony/cdma/sms/BearerData;->getBitsForNumFields(II)I

    #@db
    move-result v15

    #@dc
    .line 2102
    .local v15, textBits:I
    if-ge v13, v15, :cond_111

    #@de
    .line 2103
    new-instance v17, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@e0
    new-instance v18, Ljava/lang/StringBuilder;

    #@e2
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@e5
    const-string v19, "category name is "

    #@e7
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v18

    #@eb
    move-object/from16 v0, v18

    #@ed
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v18

    #@f1
    const-string v19, " bits in length,"

    #@f3
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v18

    #@f7
    const-string v19, " but there are only "

    #@f9
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v18

    #@fd
    move-object/from16 v0, v18

    #@ff
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@102
    move-result-object v18

    #@103
    const-string v19, " bits available"

    #@105
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v18

    #@109
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v18

    #@10d
    invoke-direct/range {v17 .. v18}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@110
    throw v17

    #@111
    .line 2107
    :cond_111
    new-instance v16, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@113
    invoke-direct/range {v16 .. v16}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@116
    .line 2108
    .local v16, userData:Lcom/android/internal/telephony/cdma/sms/UserData;
    move-object/from16 v0, v16

    #@118
    iput v11, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@11a
    .line 2109
    const/16 v17, 0x1

    #@11c
    move/from16 v0, v17

    #@11e
    move-object/from16 v1, v16

    #@120
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@122
    .line 2110
    move-object/from16 v0, v16

    #@124
    iput v12, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@126
    .line 2111
    move-object/from16 v0, p1

    #@128
    invoke-virtual {v0, v15}, Lcom/android/internal/util/BitwiseInputStream;->readByteArray(I)[B

    #@12b
    move-result-object v17

    #@12c
    move-object/from16 v0, v17

    #@12e
    move-object/from16 v1, v16

    #@130
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@132
    .line 2112
    sub-int/2addr v13, v15

    #@133
    .line 2114
    const/16 v17, 0x0

    #@135
    invoke-static/range {v16 .. v17}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeUserDataPayload(Lcom/android/internal/telephony/cdma/sms/UserData;Z)V

    #@138
    .line 2115
    move-object/from16 v0, v16

    #@13a
    iget-object v8, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@13c
    .line 2116
    .local v8, categoryName:Ljava/lang/String;
    new-instance v2, Landroid/telephony/cdma/CdmaSmsCbProgramData;

    #@13e
    invoke-direct/range {v2 .. v8}, Landroid/telephony/cdma/CdmaSmsCbProgramData;-><init>(IIIIILjava/lang/String;)V

    #@141
    .line 2118
    .local v2, programData:Landroid/telephony/cdma/CdmaSmsCbProgramData;
    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@144
    .line 2120
    const/4 v10, 0x1

    #@145
    .line 2121
    goto/16 :goto_86

    #@147
    .line 2123
    .end local v2           #programData:Landroid/telephony/cdma/CdmaSmsCbProgramData;
    .end local v3           #operation:I
    .end local v4           #category:I
    .end local v5           #language:I
    .end local v6           #maxMessages:I
    .end local v7           #alertOption:I
    .end local v8           #categoryName:Ljava/lang/String;
    .end local v12           #numFields:I
    .end local v15           #textBits:I
    .end local v16           #userData:Lcom/android/internal/telephony/cdma/sms/UserData;
    :cond_147
    if-eqz v10, :cond_14b

    #@149
    if-lez v13, :cond_18d

    #@14b
    .line 2124
    :cond_14b
    const-string v18, "SMS"

    #@14d
    new-instance v17, Ljava/lang/StringBuilder;

    #@14f
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@152
    const-string v19, "SERVICE_CATEGORY_PROGRAM_DATA decode "

    #@154
    move-object/from16 v0, v17

    #@156
    move-object/from16 v1, v19

    #@158
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v19

    #@15c
    if-eqz v10, :cond_197

    #@15e
    const-string v17, "succeeded"

    #@160
    :goto_160
    move-object/from16 v0, v19

    #@162
    move-object/from16 v1, v17

    #@164
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v17

    #@168
    const-string v19, " (extra bits = "

    #@16a
    move-object/from16 v0, v17

    #@16c
    move-object/from16 v1, v19

    #@16e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@171
    move-result-object v17

    #@172
    move-object/from16 v0, v17

    #@174
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@177
    move-result-object v17

    #@178
    const/16 v19, 0x29

    #@17a
    move-object/from16 v0, v17

    #@17c
    move/from16 v1, v19

    #@17e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@181
    move-result-object v17

    #@182
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@185
    move-result-object v17

    #@186
    move-object/from16 v0, v18

    #@188
    move-object/from16 v1, v17

    #@18a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18d
    .line 2129
    :cond_18d
    move-object/from16 v0, p1

    #@18f
    invoke-virtual {v0, v13}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@192
    .line 2130
    move-object/from16 v0, p0

    #@194
    iput-object v14, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->serviceCategoryProgramData:Ljava/util/ArrayList;

    #@196
    .line 2131
    return v10

    #@197
    .line 2124
    :cond_197
    const-string v17, "failed"

    #@199
    goto :goto_160
.end method

.method private static decodeSessionInfo(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 1690
    const/16 v0, 0x10

    #@4
    .line 1691
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1693
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 1694
    .local v2, paramBits:I
    const/16 v3, 0x10

    #@d
    if-lt v2, v3, :cond_26

    #@f
    .line 1695
    add-int/lit8 v2, v2, -0x10

    #@11
    .line 1696
    const/4 v1, 0x1

    #@12
    .line 1697
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@15
    move-result v3

    #@16
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessioinId:I

    #@18
    .line 1698
    const/4 v3, 0x7

    #@19
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1c
    move-result v3

    #@1d
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessionSeq:I

    #@1f
    .line 1699
    const/4 v3, 0x1

    #@20
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@23
    move-result v3

    #@24
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->endOfSession:I

    #@26
    .line 1702
    :cond_26
    if-eqz v1, :cond_2a

    #@28
    if-lez v2, :cond_56

    #@2a
    .line 1703
    :cond_2a
    const-string v4, "SMS"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v5, "SESSION INFO decode "

    #@33
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    if-eqz v1, :cond_57

    #@39
    const-string v3, "succeeded"

    #@3b
    :goto_3b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    const-string v5, " (extra bits = "

    #@41
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    const-string v5, ")"

    #@4b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 1707
    :cond_56
    return v1

    #@57
    .line 1703
    :cond_57
    const-string v3, "failed"

    #@59
    goto :goto_3b
.end method

.method private static decodeShiftJis([BII)Ljava/lang/String;
    .registers 7
    .parameter "data"
    .parameter "offset"
    .parameter "numFields"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1266
    :try_start_0
    new-instance v1, Ljava/lang/String;

    #@2
    sub-int v2, p2, p1

    #@4
    const-string v3, "Shift_JIS"

    #@6
    invoke-direct {v1, p0, p1, v2, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    return-object v1

    #@a
    .line 1267
    :catch_a
    move-exception v0

    #@b
    .line 1268
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    new-instance v1, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Shift_JIS decode failed: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@23
    throw v1
.end method

.method private static decodeSmsAddress(Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;)V
    .registers 7
    .parameter "addr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1767
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@2
    const/4 v2, 0x1

    #@3
    if-ne v1, v2, :cond_1e

    #@5
    .line 1771
    :try_start_5
    new-instance v1, Ljava/lang/String;

    #@7
    iget-object v2, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@9
    const/4 v3, 0x0

    #@a
    iget-object v4, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@c
    array-length v4, v4

    #@d
    const-string v5, "US-ASCII"

    #@f
    invoke-direct {v1, v2, v3, v4, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@12
    iput-object v1, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;
    :try_end_14
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_14} :catch_15

    #@14
    .line 1778
    :goto_14
    return-void

    #@15
    .line 1772
    :catch_15
    move-exception v0

    #@16
    .line 1773
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    new-instance v1, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@18
    const-string v2, "invalid SMS address ASCII code"

    #@1a
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v1

    #@1e
    .line 1776
    .end local v0           #ex:Ljava/io/UnsupportedEncodingException;
    :cond_1e
    iget-object v1, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@20
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@22
    invoke-static {v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeDtmfSmsAddress([BI)Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    iput-object v1, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@28
    goto :goto_14
.end method

.method private static decodeUserData(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 10
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 1118
    const/16 v6, 0x8

    #@4
    :try_start_4
    invoke-virtual {p1, v6}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@7
    move-result v6

    #@8
    mul-int/lit8 v3, v6, 0x8

    #@a
    .line 1119
    .local v3, paramBits:I
    new-instance v6, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@c
    invoke-direct {v6}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@f
    iput-object v6, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@11
    .line 1120
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@13
    const/4 v7, 0x5

    #@14
    invoke-virtual {p1, v7}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@17
    move-result v7

    #@18
    iput v7, v6, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@1a
    .line 1121
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@1c
    const/4 v7, 0x1

    #@1d
    iput-boolean v7, v6, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@1f
    .line 1122
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@21
    const/4 v7, 0x0

    #@22
    iput v7, v6, Lcom/android/internal/telephony/cdma/sms/UserData;->msgType:I

    #@24
    .line 1123
    const/4 v0, 0x5

    #@25
    .line 1124
    .local v0, consumedBits:I
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@27
    iget v6, v6, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@29
    if-eq v6, v4, :cond_33

    #@2b
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@2d
    iget v6, v6, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@2f
    const/16 v7, 0xa

    #@31
    if-ne v6, v7, :cond_3f

    #@33
    .line 1126
    :cond_33
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@35
    const/16 v7, 0x8

    #@37
    invoke-virtual {p1, v7}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@3a
    move-result v7

    #@3b
    iput v7, v6, Lcom/android/internal/telephony/cdma/sms/UserData;->msgType:I

    #@3d
    .line 1127
    add-int/lit8 v0, v0, 0x8

    #@3f
    .line 1129
    :cond_3f
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@41
    const/16 v7, 0x8

    #@43
    invoke-virtual {p1, v7}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@46
    move-result v7

    #@47
    iput v7, v6, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@49
    .line 1130
    add-int/lit8 v0, v0, 0x8

    #@4b
    .line 1131
    sub-int v1, v3, v0

    #@4d
    .line 1132
    .local v1, dataBits:I
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@4f
    invoke-virtual {p1, v1}, Lcom/android/internal/util/BitwiseInputStream;->readByteArray(I)[B

    #@52
    move-result-object v7

    #@53
    iput-object v7, v6, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B
    :try_end_55
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_55} :catch_56

    #@55
    .line 1138
    .end local v0           #consumedBits:I
    .end local v1           #dataBits:I
    .end local v3           #paramBits:I
    :goto_55
    return v4

    #@56
    .line 1135
    :catch_56
    move-exception v2

    #@57
    .line 1136
    .local v2, e:Ljava/lang/OutOfMemoryError;
    const-string v4, "SMS"

    #@59
    const-string v6, "sometimes occurs for logging"

    #@5b
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 1137
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    #@61
    move v4, v5

    #@62
    .line 1138
    goto :goto_55
.end method

.method private static decodeUserDataPayload(Lcom/android/internal/telephony/cdma/sms/UserData;Z)V
    .registers 13
    .parameter "userData"
    .parameter "hasUserDataHeader"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1275
    const/4 v1, 0x0

    #@3
    .line 1276
    .local v1, offset:I
    if-eqz p1, :cond_1b

    #@5
    .line 1277
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@7
    aget-byte v0, v0, v3

    #@9
    and-int/lit16 v10, v0, 0xff

    #@b
    .line 1278
    .local v10, udhLen:I
    add-int/lit8 v0, v10, 0x1

    #@d
    add-int/2addr v1, v0

    #@e
    .line 1279
    new-array v8, v10, [B

    #@10
    .line 1280
    .local v8, headerData:[B
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@12
    invoke-static {v0, v4, v8, v3, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@15
    .line 1281
    invoke-static {v8}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@1b
    .line 1283
    .end local v8           #headerData:[B
    .end local v10           #udhLen:I
    :cond_1b
    iget v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@1d
    packed-switch v0, :pswitch_data_100

    #@20
    .line 1354
    :pswitch_20
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@22
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v3, "unsupported user data encoding ("

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    iget v3, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    const-string v3, ")"

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@40
    throw v0

    #@41
    .line 1288
    :pswitch_41
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@44
    move-result-object v0

    #@45
    const v2, 0x1110038

    #@48
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@4b
    move-result v7

    #@4c
    .line 1294
    .local v7, decodingtypeUTF8:Z
    iget v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@4e
    new-array v9, v0, [B

    #@50
    .line 1295
    .local v9, payload:[B
    iget v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@52
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@54
    array-length v2, v2

    #@55
    if-ge v0, v2, :cond_6d

    #@57
    iget v6, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@59
    .line 1298
    .local v6, copyLen:I
    :goto_59
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@5b
    invoke-static {v0, v3, v9, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@5e
    .line 1299
    iput-object v9, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@60
    .line 1301
    if-nez v7, :cond_71

    #@62
    .line 1304
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@64
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@66
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeLatin([BII)Ljava/lang/String;

    #@69
    move-result-object v0

    #@6a
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@6c
    .line 1357
    .end local v6           #copyLen:I
    .end local v7           #decodingtypeUTF8:Z
    .end local v9           #payload:[B
    :cond_6c
    :goto_6c
    return-void

    #@6d
    .line 1295
    .restart local v7       #decodingtypeUTF8:Z
    .restart local v9       #payload:[B
    :cond_6d
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@6f
    array-length v6, v0

    #@70
    goto :goto_59

    #@71
    .line 1306
    .restart local v6       #copyLen:I
    :cond_71
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@73
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@75
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeUtf8([BII)Ljava/lang/String;

    #@78
    move-result-object v0

    #@79
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@7b
    goto :goto_6c

    #@7c
    .line 1312
    .end local v6           #copyLen:I
    .end local v7           #decodingtypeUTF8:Z
    .end local v9           #payload:[B
    :pswitch_7c
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@7e
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@80
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decode7bitAscii([BII)Ljava/lang/String;

    #@83
    move-result-object v0

    #@84
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@86
    goto :goto_6c

    #@87
    .line 1327
    :pswitch_87
    const-string v0, "SMS"

    #@89
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/sms/UserData;->toString()Ljava/lang/String;

    #@8c
    move-result-object v2

    #@8d
    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    .line 1328
    const-string v0, "SMS"

    #@92
    const-string v2, "UserData.ENCODING_KSC5601"

    #@94
    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    .line 1329
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@99
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@9b
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeKsc5601([BII)Ljava/lang/String;

    #@9e
    move-result-object v0

    #@9f
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@a1
    goto :goto_6c

    #@a2
    .line 1334
    :pswitch_a2
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@a4
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@a6
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeUtf16([BII)Ljava/lang/String;

    #@a9
    move-result-object v0

    #@aa
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@ac
    goto :goto_6c

    #@ad
    .line 1337
    :pswitch_ad
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@af
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@b1
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decode7bitGsm([BII)Ljava/lang/String;

    #@b4
    move-result-object v0

    #@b5
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@b7
    goto :goto_6c

    #@b8
    .line 1340
    :pswitch_b8
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@ba
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@bc
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeLatin([BII)Ljava/lang/String;

    #@bf
    move-result-object v0

    #@c0
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@c2
    goto :goto_6c

    #@c3
    .line 1343
    :pswitch_c3
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@c5
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@c7
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeShiftJis([BII)Ljava/lang/String;

    #@ca
    move-result-object v0

    #@cb
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@cd
    goto :goto_6c

    #@ce
    .line 1347
    :pswitch_ce
    const/4 v0, 0x0

    #@cf
    const-string v2, "gsm_dcs_over_cdma_for_kddi"

    #@d1
    invoke-static {v0, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@d4
    move-result v0

    #@d5
    if-ne v0, v4, :cond_6c

    #@d7
    .line 1348
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@d9
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@db
    iget v3, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgType:I

    #@dd
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@df
    move v4, p1

    #@e0
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeGsmDCSInCDMA([BIIIZLcom/android/internal/telephony/SmsHeader;)Ljava/lang/String;

    #@e3
    move-result-object v0

    #@e4
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@e6
    .line 1349
    new-instance v0, Ljava/lang/StringBuilder;

    #@e8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@eb
    const-string v2, "decodeUserDataPayload(), [KDDI] bearerData payload Str(GSM DCS):"

    #@ed
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v0

    #@f1
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@f3
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v0

    #@f7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fa
    move-result-object v0

    #@fb
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@fe
    goto/16 :goto_6c

    #@100
    .line 1283
    :pswitch_data_100
    .packed-switch 0x0
        :pswitch_41
        :pswitch_20
        :pswitch_7c
        :pswitch_7c
        :pswitch_a2
        :pswitch_c3
        :pswitch_20
        :pswitch_20
        :pswitch_b8
        :pswitch_ad
        :pswitch_ce
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_87
    .end packed-switch
.end method

.method private static decodeUserResponseCode(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 2053
    const/16 v0, 0x8

    #@4
    .line 2054
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 2055
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 2056
    .local v2, paramBits:I
    if-lt v2, v4, :cond_16

    #@d
    .line 2057
    add-int/lit8 v2, v2, -0x8

    #@f
    .line 2058
    const/4 v1, 0x1

    #@10
    .line 2059
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@13
    move-result v3

    #@14
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userResponseCode:I

    #@16
    .line 2061
    :cond_16
    if-eqz v1, :cond_1a

    #@18
    if-lez v2, :cond_46

    #@1a
    .line 2062
    :cond_1a
    const-string v4, "SMS"

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "USER_RESPONSE_CODE decode "

    #@23
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    if-eqz v1, :cond_4c

    #@29
    const-string v3, "succeeded"

    #@2b
    :goto_2b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v5, " (extra bits = "

    #@31
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    const-string v5, ")"

    #@3b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 2066
    :cond_46
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@49
    .line 2067
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userResponseCodeSet:Z

    #@4b
    .line 2068
    return v1

    #@4c
    .line 2062
    :cond_4c
    const-string v3, "failed"

    #@4e
    goto :goto_2b
.end method

.method private static decodeUtf16([BII)Ljava/lang/String;
    .registers 6
    .parameter "data"
    .parameter "offset"
    .parameter "numFields"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1153
    rem-int/lit8 v0, p1, 0x2

    #@2
    .line 1154
    .local v0, padding:I
    add-int v1, p1, v0

    #@4
    div-int/lit8 v1, v1, 0x2

    #@6
    sub-int/2addr p2, v1

    #@7
    .line 1155
    const/4 v1, 0x2

    #@8
    const-string v2, "utf-16be"

    #@a
    invoke-static {p0, p1, p2, v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeCharset([BIIILjava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    return-object v1
.end method

.method private static decodeUtf8([BII)Ljava/lang/String;
    .registers 5
    .parameter "data"
    .parameter "offset"
    .parameter "numFields"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1146
    const/4 v0, 0x1

    #@1
    const-string v1, "UTF-8"

    #@3
    invoke-static {p0, p1, p2, v0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decodeCharset([BIIILjava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method private static decodeValidityAbs(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x30

    #@2
    .line 1861
    const/16 v0, 0x30

    #@4
    .line 1862
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1863
    .local v1, decodeSuccess:Z
    const/16 v3, 0x8

    #@7
    invoke-virtual {p1, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@a
    move-result v3

    #@b
    mul-int/lit8 v2, v3, 0x8

    #@d
    .line 1864
    .local v2, paramBits:I
    if-lt v2, v4, :cond_1c

    #@f
    .line 1865
    add-int/lit8 v2, v2, -0x30

    #@11
    .line 1866
    const/4 v1, 0x1

    #@12
    .line 1867
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->readByteArray(I)[B

    #@15
    move-result-object v3

    #@16
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;->fromByteArray([B)Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@19
    move-result-object v3

    #@1a
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->validityPeriodAbsolute:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@1c
    .line 1869
    :cond_1c
    if-eqz v1, :cond_20

    #@1e
    if-lez v2, :cond_4c

    #@20
    .line 1870
    :cond_20
    const-string v4, "SMS"

    #@22
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v5, "VALIDITY_PERIOD_ABSOLUTE decode "

    #@29
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    if-eqz v1, :cond_50

    #@2f
    const-string v3, "succeeded"

    #@31
    :goto_31
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    const-string v5, " (extra bits = "

    #@37
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    const-string v5, ")"

    #@41
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 1874
    :cond_4c
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@4f
    .line 1875
    return v1

    #@50
    .line 1870
    :cond_50
    const-string v3, "failed"

    #@52
    goto :goto_31
.end method

.method private static decodeValidityRel(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseInputStream;)Z
    .registers 8
    .parameter "bData"
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 1902
    const/16 v0, 0x8

    #@4
    .line 1903
    .local v0, EXPECTED_PARAM_SIZE:I
    const/4 v1, 0x0

    #@5
    .line 1904
    .local v1, decodeSuccess:Z
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@8
    move-result v3

    #@9
    mul-int/lit8 v2, v3, 0x8

    #@b
    .line 1905
    .local v2, paramBits:I
    if-lt v2, v4, :cond_16

    #@d
    .line 1906
    add-int/lit8 v2, v2, -0x8

    #@f
    .line 1907
    const/4 v1, 0x1

    #@10
    .line 1908
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@13
    move-result v3

    #@14
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->deferredDeliveryTimeRelative:I

    #@16
    .line 1910
    :cond_16
    if-eqz v1, :cond_1a

    #@18
    if-lez v2, :cond_46

    #@1a
    .line 1911
    :cond_1a
    const-string v4, "SMS"

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "VALIDITY_PERIOD_RELATIVE decode "

    #@23
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    if-eqz v1, :cond_4c

    #@29
    const-string v3, "succeeded"

    #@2b
    :goto_2b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v5, " (extra bits = "

    #@31
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    const-string v5, ")"

    #@3b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 1915
    :cond_46
    invoke-virtual {p1, v2}, Lcom/android/internal/util/BitwiseInputStream;->skip(I)V

    #@49
    .line 1916
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->deferredDeliveryTimeRelativeSet:Z

    #@4b
    .line 1917
    return v1

    #@4c
    .line 1911
    :cond_4c
    const-string v3, "failed"

    #@4e
    goto :goto_2b
.end method

.method public static encode(Lcom/android/internal/telephony/cdma/sms/BearerData;)[B
    .registers 6
    .parameter "bData"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 997
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@4
    if-eqz v4, :cond_e2

    #@6
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@8
    iget-object v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@a
    if-eqz v4, :cond_e2

    #@c
    :goto_c
    iput-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@e
    .line 1000
    :try_start_e
    new-instance v1, Lcom/android/internal/util/BitwiseOutputStream;

    #@10
    const/16 v2, 0xc8

    #@12
    invoke-direct {v1, v2}, Lcom/android/internal/util/BitwiseOutputStream;-><init>(I)V

    #@15
    .line 1001
    .local v1, outStream:Lcom/android/internal/util/BitwiseOutputStream;
    const/16 v2, 0x8

    #@17
    const/4 v3, 0x0

    #@18
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@1b
    .line 1002
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeMessageId(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@1e
    .line 1003
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@20
    if-eqz v2, :cond_2b

    #@22
    .line 1004
    const/16 v2, 0x8

    #@24
    const/4 v3, 0x1

    #@25
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@28
    .line 1005
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeUserData(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@2b
    .line 1007
    :cond_2b
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->callbackNumber:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@2d
    if-eqz v2, :cond_39

    #@2f
    .line 1008
    const/16 v2, 0x8

    #@31
    const/16 v3, 0xe

    #@33
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@36
    .line 1009
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeCallbackNumber(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@39
    .line 1011
    :cond_39
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userAckReq:Z

    #@3b
    if-nez v2, :cond_49

    #@3d
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->deliveryAckReq:Z

    #@3f
    if-nez v2, :cond_49

    #@41
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->readAckReq:Z

    #@43
    if-nez v2, :cond_49

    #@45
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->reportReq:Z

    #@47
    if-eqz v2, :cond_53

    #@49
    .line 1012
    :cond_49
    const/16 v2, 0x8

    #@4b
    const/16 v3, 0xa

    #@4d
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@50
    .line 1013
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeReplyOption(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@53
    .line 1015
    :cond_53
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->numberOfMessages:I

    #@55
    if-eqz v2, :cond_61

    #@57
    .line 1016
    const/16 v2, 0x8

    #@59
    const/16 v3, 0xb

    #@5b
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@5e
    .line 1017
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeMsgCount(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@61
    .line 1019
    :cond_61
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->validityPeriodRelativeSet:Z

    #@63
    if-eqz v2, :cond_6e

    #@65
    .line 1020
    const/16 v2, 0x8

    #@67
    const/4 v3, 0x5

    #@68
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@6b
    .line 1021
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeValidityPeriodRel(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@6e
    .line 1023
    :cond_6e
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->privacyIndicatorSet:Z

    #@70
    if-eqz v2, :cond_7c

    #@72
    .line 1024
    const/16 v2, 0x8

    #@74
    const/16 v3, 0x9

    #@76
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@79
    .line 1025
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodePrivacyIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@7c
    .line 1027
    :cond_7c
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->languageIndicatorSet:Z

    #@7e
    if-eqz v2, :cond_8a

    #@80
    .line 1028
    const/16 v2, 0x8

    #@82
    const/16 v3, 0xd

    #@84
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@87
    .line 1029
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeLanguageIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@8a
    .line 1031
    :cond_8a
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->displayModeSet:Z

    #@8c
    if-eqz v2, :cond_98

    #@8e
    .line 1032
    const/16 v2, 0x8

    #@90
    const/16 v3, 0xf

    #@92
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@95
    .line 1033
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeDisplayMode(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@98
    .line 1035
    :cond_98
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priorityIndicatorSet:Z

    #@9a
    if-eqz v2, :cond_a6

    #@9c
    .line 1036
    const/16 v2, 0x8

    #@9e
    const/16 v3, 0x8

    #@a0
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@a3
    .line 1037
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodePriorityIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@a6
    .line 1039
    :cond_a6
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->alertIndicatorSet:Z

    #@a8
    if-eqz v2, :cond_b4

    #@aa
    .line 1040
    const/16 v2, 0x8

    #@ac
    const/16 v3, 0xc

    #@ae
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@b1
    .line 1041
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeMsgDeliveryAlert(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@b4
    .line 1043
    :cond_b4
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageStatusSet:Z

    #@b6
    if-eqz v2, :cond_c2

    #@b8
    .line 1044
    const/16 v2, 0x8

    #@ba
    const/16 v3, 0x14

    #@bc
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@bf
    .line 1045
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeMsgStatus(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@c2
    .line 1047
    :cond_c2
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->serviceCategoryProgramResults:Ljava/util/ArrayList;

    #@c4
    if-eqz v2, :cond_d0

    #@c6
    .line 1048
    const/16 v2, 0x8

    #@c8
    const/16 v3, 0x13

    #@ca
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@cd
    .line 1049
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeScpResults(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@d0
    .line 1052
    :cond_d0
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@d2
    if-eqz v2, :cond_dd

    #@d4
    .line 1053
    const/16 v2, 0x8

    #@d6
    const/4 v3, 0x3

    #@d7
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@da
    .line 1054
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeMsgCenterTimeStamp(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V

    #@dd
    .line 1058
    :cond_dd
    invoke-virtual {v1}, Lcom/android/internal/util/BitwiseOutputStream;->toByteArray()[B
    :try_end_e0
    .catch Lcom/android/internal/util/BitwiseOutputStream$AccessException; {:try_start_e .. :try_end_e0} :catch_e5
    .catch Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException; {:try_start_e .. :try_end_e0} :catch_100

    #@e0
    move-result-object v2

    #@e1
    .line 1064
    .end local v1           #outStream:Lcom/android/internal/util/BitwiseOutputStream;
    :goto_e1
    return-object v2

    #@e2
    :cond_e2
    move v2, v3

    #@e3
    .line 997
    goto/16 :goto_c

    #@e5
    .line 1059
    :catch_e5
    move-exception v0

    #@e6
    .line 1060
    .local v0, ex:Lcom/android/internal/util/BitwiseOutputStream$AccessException;
    const-string v2, "SMS"

    #@e8
    new-instance v3, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string v4, "BearerData encode failed: "

    #@ef
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v3

    #@f3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v3

    #@f7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fa
    move-result-object v3

    #@fb
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@fe
    .line 1064
    .end local v0           #ex:Lcom/android/internal/util/BitwiseOutputStream$AccessException;
    :goto_fe
    const/4 v2, 0x0

    #@ff
    goto :goto_e1

    #@100
    .line 1061
    :catch_100
    move-exception v0

    #@101
    .line 1062
    .local v0, ex:Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
    const-string v2, "SMS"

    #@103
    new-instance v3, Ljava/lang/StringBuilder;

    #@105
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@108
    const-string v4, "BearerData encode failed: "

    #@10a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v3

    #@10e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v3

    #@112
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@115
    move-result-object v3

    #@116
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@119
    goto :goto_fe
.end method

.method private static encode16bitEms(Lcom/android/internal/telephony/cdma/sms/UserData;[B)V
    .registers 10
    .parameter "uData"
    .parameter "udhData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 670
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@4
    invoke-static {v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeUtf16(Ljava/lang/String;)[B

    #@7
    move-result-object v0

    #@8
    .line 671
    .local v0, payload:[B
    array-length v4, p1

    #@9
    add-int/lit8 v2, v4, 0x1

    #@b
    .line 672
    .local v2, udhBytes:I
    add-int/lit8 v4, v2, 0x1

    #@d
    div-int/lit8 v3, v4, 0x2

    #@f
    .line 673
    .local v3, udhCodeUnits:I
    array-length v4, v0

    #@10
    div-int/lit8 v1, v4, 0x2

    #@12
    .line 674
    .local v1, payloadCodeUnits:I
    const/4 v4, 0x4

    #@13
    iput v4, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@15
    .line 675
    iput-boolean v7, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@17
    .line 676
    add-int v4, v3, v1

    #@19
    iput v4, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@1b
    .line 677
    iget v4, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@1d
    mul-int/lit8 v4, v4, 0x2

    #@1f
    new-array v4, v4, [B

    #@21
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@23
    .line 678
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@25
    array-length v5, p1

    #@26
    int-to-byte v5, v5

    #@27
    aput-byte v5, v4, v6

    #@29
    .line 679
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@2b
    array-length v5, p1

    #@2c
    invoke-static {p1, v6, v4, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2f
    .line 680
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@31
    array-length v5, v0

    #@32
    invoke-static {v0, v6, v4, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@35
    .line 681
    return-void
.end method

.method private static encode7bitAscii(Ljava/lang/String;Z)[B
    .registers 11
    .parameter "msg"
    .parameter "force"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    .line 573
    :try_start_1
    new-instance v4, Lcom/android/internal/util/BitwiseOutputStream;

    #@3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v5

    #@7
    invoke-direct {v4, v5}, Lcom/android/internal/util/BitwiseOutputStream;-><init>(I)V

    #@a
    .line 574
    .local v4, outStream:Lcom/android/internal/util/BitwiseOutputStream;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@d
    move-result v3

    #@e
    .line 575
    .local v3, msgLen:I
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v3, :cond_6b

    #@11
    .line 576
    sget-object v5, Lcom/android/internal/telephony/cdma/sms/UserData;->charToAscii:Landroid/util/SparseIntArray;

    #@13
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@16
    move-result v6

    #@17
    const/4 v7, -0x1

    #@18
    invoke-virtual {v5, v6, v7}, Landroid/util/SparseIntArray;->get(II)I

    #@1b
    move-result v0

    #@1c
    .line 577
    .local v0, charCode:I
    if-ne v0, v8, :cond_66

    #@1e
    .line 578
    if-eqz p1, :cond_29

    #@20
    .line 579
    const/4 v5, 0x7

    #@21
    const/16 v6, 0x20

    #@23
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@26
    .line 575
    :goto_26
    add-int/lit8 v2, v2, 0x1

    #@28
    goto :goto_f

    #@29
    .line 581
    :cond_29
    new-instance v5, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@2b
    new-instance v6, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v7, "cannot ASCII encode ("

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@39
    move-result v7

    #@3a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    const-string v7, ")"

    #@40
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v6

    #@48
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v5
    :try_end_4c
    .catch Lcom/android/internal/util/BitwiseOutputStream$AccessException; {:try_start_1 .. :try_end_4c} :catch_4c

    #@4c
    .line 588
    .end local v0           #charCode:I
    .end local v2           #i:I
    .end local v3           #msgLen:I
    .end local v4           #outStream:Lcom/android/internal/util/BitwiseOutputStream;
    :catch_4c
    move-exception v1

    #@4d
    .line 589
    .local v1, ex:Lcom/android/internal/util/BitwiseOutputStream$AccessException;
    new-instance v5, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@4f
    new-instance v6, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v7, "7bit ASCII encode failed: "

    #@56
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v6

    #@5a
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v6

    #@5e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v6

    #@62
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@65
    throw v5

    #@66
    .line 584
    .end local v1           #ex:Lcom/android/internal/util/BitwiseOutputStream$AccessException;
    .restart local v0       #charCode:I
    .restart local v2       #i:I
    .restart local v3       #msgLen:I
    .restart local v4       #outStream:Lcom/android/internal/util/BitwiseOutputStream;
    :cond_66
    const/4 v5, 0x7

    #@67
    :try_start_67
    invoke-virtual {v4, v5, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@6a
    goto :goto_26

    #@6b
    .line 587
    .end local v0           #charCode:I
    :cond_6b
    invoke-virtual {v4}, Lcom/android/internal/util/BitwiseOutputStream;->toByteArray()[B
    :try_end_6e
    .catch Lcom/android/internal/util/BitwiseOutputStream$AccessException; {:try_start_67 .. :try_end_6e} :catch_4c

    #@6e
    move-result-object v5

    #@6f
    return-object v5
.end method

.method private static encode7bitEms(Lcom/android/internal/telephony/cdma/sms/UserData;[BZ)V
    .registers 10
    .parameter "uData"
    .parameter "udhData"
    .parameter "force"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 656
    array-length v3, p1

    #@3
    add-int/lit8 v1, v3, 0x1

    #@5
    .line 657
    .local v1, udhBytes:I
    mul-int/lit8 v3, v1, 0x8

    #@7
    add-int/lit8 v3, v3, 0x6

    #@9
    div-int/lit8 v2, v3, 0x7

    #@b
    .line 658
    .local v2, udhSeptets:I
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@d
    invoke-static {v3, v2, p2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode7bitGsm(Ljava/lang/String;IZ)Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;

    #@10
    move-result-object v0

    #@11
    .line 659
    .local v0, gcr:Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;
    const/16 v3, 0x9

    #@13
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@15
    .line 660
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@17
    .line 661
    iget v3, v0, Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;->septets:I

    #@19
    iput v3, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@1b
    .line 662
    iget-object v3, v0, Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;->data:[B

    #@1d
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@1f
    .line 663
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@21
    array-length v4, p1

    #@22
    int-to-byte v4, v4

    #@23
    aput-byte v4, v3, v5

    #@25
    .line 664
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@27
    array-length v4, p1

    #@28
    invoke-static {p1, v5, v3, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2b
    .line 665
    return-void
.end method

.method private static encode7bitGsm(Ljava/lang/String;IZ)Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;
    .registers 10
    .parameter "msg"
    .parameter "septetOffset"
    .parameter "force"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 642
    if-nez p2, :cond_29

    #@4
    :goto_4
    const/4 v4, 0x0

    #@5
    const/4 v5, 0x0

    #@6
    :try_start_6
    invoke-static {p0, p1, v3, v4, v5}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPacked(Ljava/lang/String;IZII)[B

    #@9
    move-result-object v1

    #@a
    .line 643
    .local v1, fullData:[B
    new-instance v2, Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;

    #@c
    const/4 v3, 0x0

    #@d
    invoke-direct {v2, v3}, Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;-><init>(Lcom/android/internal/telephony/cdma/sms/BearerData$1;)V

    #@10
    .line 644
    .local v2, result:Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;
    array-length v3, v1

    #@11
    add-int/lit8 v3, v3, -0x1

    #@13
    new-array v3, v3, [B

    #@15
    iput-object v3, v2, Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;->data:[B

    #@17
    .line 645
    const/4 v3, 0x1

    #@18
    iget-object v4, v2, Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;->data:[B

    #@1a
    const/4 v5, 0x0

    #@1b
    array-length v6, v1

    #@1c
    add-int/lit8 v6, v6, -0x1

    #@1e
    invoke-static {v1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@21
    .line 646
    const/4 v3, 0x0

    #@22
    aget-byte v3, v1, v3

    #@24
    and-int/lit16 v3, v3, 0xff

    #@26
    iput v3, v2, Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;->septets:I
    :try_end_28
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_6 .. :try_end_28} :catch_2b

    #@28
    .line 647
    return-object v2

    #@29
    .end local v1           #fullData:[B
    .end local v2           #result:Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;
    :cond_29
    move v3, v4

    #@2a
    .line 642
    goto :goto_4

    #@2b
    .line 648
    :catch_2b
    move-exception v0

    #@2c
    .line 649
    .local v0, ex:Lcom/android/internal/telephony/EncodeException;
    new-instance v3, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@2e
    new-instance v4, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v5, "7bit GSM encode failed: "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@44
    throw v3
.end method

.method private static encodeCallbackNumber(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 12
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v9, 0x8

    #@2
    const/4 v7, 0x0

    #@3
    const/4 v6, 0x1

    #@4
    .line 875
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->callbackNumber:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@6
    .line 876
    .local v0, addr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeCdmaSmsAddress(Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;)V

    #@9
    .line 877
    const/16 v3, 0x9

    #@b
    .line 878
    .local v3, paramBits:I
    const/4 v1, 0x0

    #@c
    .line 879
    .local v1, dataBits:I
    iget v5, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@e
    if-ne v5, v6, :cond_4c

    #@10
    .line 880
    add-int/lit8 v3, v3, 0x7

    #@12
    .line 881
    iget v5, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@14
    mul-int/lit8 v1, v5, 0x8

    #@16
    .line 885
    :goto_16
    add-int/2addr v3, v1

    #@17
    .line 886
    div-int/lit8 v8, v3, 0x8

    #@19
    rem-int/lit8 v5, v3, 0x8

    #@1b
    if-lez v5, :cond_51

    #@1d
    move v5, v6

    #@1e
    :goto_1e
    add-int v4, v8, v5

    #@20
    .line 887
    .local v4, paramBytes:I
    mul-int/lit8 v5, v4, 0x8

    #@22
    sub-int v2, v5, v3

    #@24
    .line 888
    .local v2, paddingBits:I
    invoke-virtual {p1, v9, v4}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@27
    .line 889
    iget v5, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@29
    invoke-virtual {p1, v6, v5}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@2c
    .line 890
    iget v5, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@2e
    if-ne v5, v6, :cond_3c

    #@30
    .line 891
    const/4 v5, 0x3

    #@31
    iget v6, v0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@33
    invoke-virtual {p1, v5, v6}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@36
    .line 892
    const/4 v5, 0x4

    #@37
    iget v6, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@39
    invoke-virtual {p1, v5, v6}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@3c
    .line 894
    :cond_3c
    iget v5, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@3e
    invoke-virtual {p1, v9, v5}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@41
    .line 895
    iget-object v5, v0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@43
    invoke-virtual {p1, v1, v5}, Lcom/android/internal/util/BitwiseOutputStream;->writeByteArray(I[B)V

    #@46
    .line 896
    if-lez v2, :cond_4b

    #@48
    invoke-virtual {p1, v2, v7}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@4b
    .line 897
    :cond_4b
    return-void

    #@4c
    .line 883
    .end local v2           #paddingBits:I
    .end local v4           #paramBytes:I
    :cond_4c
    iget v5, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@4e
    mul-int/lit8 v1, v5, 0x4

    #@50
    goto :goto_16

    #@51
    :cond_51
    move v5, v7

    #@52
    .line 886
    goto :goto_1e
.end method

.method private static encodeCdmaSmsAddress(Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;)V
    .registers 4
    .parameter "addr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 861
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@2
    const/4 v2, 0x1

    #@3
    if-ne v1, v2, :cond_19

    #@5
    .line 863
    :try_start_5
    iget-object v1, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@7
    const-string v2, "US-ASCII"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@c
    move-result-object v1

    #@d
    iput-object v1, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B
    :try_end_f
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_f} :catch_10

    #@f
    .line 870
    :goto_f
    return-void

    #@10
    .line 864
    :catch_10
    move-exception v0

    #@11
    .line 865
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    new-instance v1, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@13
    const-string v2, "invalid SMS address, cannot convert to ASCII"

    #@15
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@18
    throw v1

    #@19
    .line 868
    .end local v0           #ex:Ljava/io/UnsupportedEncodingException;
    :cond_19
    iget-object v1, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@1b
    invoke-static {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeDtmfSmsAddress(Ljava/lang/String;)[B

    #@1e
    move-result-object v1

    #@1f
    iput-object v1, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@21
    goto :goto_f
.end method

.method private static encodeDisplayMode(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 4
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    .line 939
    const/16 v0, 0x8

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@6
    .line 940
    const/4 v0, 0x2

    #@7
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->displayMode:I

    #@9
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@c
    .line 941
    const/4 v0, 0x6

    #@d
    invoke-virtual {p1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->skip(I)V

    #@10
    .line 942
    return-void
.end method

.method private static encodeDtmfSmsAddress(Ljava/lang/String;)[B
    .registers 11
    .parameter "address"

    #@0
    .prologue
    .line 836
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v3

    #@4
    .line 837
    .local v3, digits:I
    mul-int/lit8 v1, v3, 0x4

    #@6
    .line 838
    .local v1, dataBits:I
    div-int/lit8 v2, v1, 0x8

    #@8
    .line 839
    .local v2, dataBytes:I
    rem-int/lit8 v7, v1, 0x8

    #@a
    if-lez v7, :cond_35

    #@c
    const/4 v7, 0x1

    #@d
    :goto_d
    add-int/2addr v2, v7

    #@e
    .line 840
    new-array v5, v2, [B

    #@10
    .line 841
    .local v5, rawData:[B
    const/4 v4, 0x0

    #@11
    .local v4, i:I
    :goto_11
    if-ge v4, v3, :cond_4d

    #@13
    .line 842
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    #@16
    move-result v0

    #@17
    .line 843
    .local v0, c:C
    const/4 v6, 0x0

    #@18
    .line 844
    .local v6, val:I
    const/16 v7, 0x31

    #@1a
    if-lt v0, v7, :cond_37

    #@1c
    const/16 v7, 0x39

    #@1e
    if-gt v0, v7, :cond_37

    #@20
    add-int/lit8 v6, v0, -0x30

    #@22
    .line 849
    :goto_22
    div-int/lit8 v7, v4, 0x2

    #@24
    aget-byte v8, v5, v7

    #@26
    rem-int/lit8 v9, v4, 0x2

    #@28
    mul-int/lit8 v9, v9, 0x4

    #@2a
    rsub-int/lit8 v9, v9, 0x4

    #@2c
    shl-int v9, v6, v9

    #@2e
    or-int/2addr v8, v9

    #@2f
    int-to-byte v8, v8

    #@30
    aput-byte v8, v5, v7

    #@32
    .line 841
    add-int/lit8 v4, v4, 0x1

    #@34
    goto :goto_11

    #@35
    .line 839
    .end local v0           #c:C
    .end local v4           #i:I
    .end local v5           #rawData:[B
    .end local v6           #val:I
    :cond_35
    const/4 v7, 0x0

    #@36
    goto :goto_d

    #@37
    .line 845
    .restart local v0       #c:C
    .restart local v4       #i:I
    .restart local v5       #rawData:[B
    .restart local v6       #val:I
    :cond_37
    const/16 v7, 0x30

    #@39
    if-ne v0, v7, :cond_3e

    #@3b
    const/16 v6, 0xa

    #@3d
    goto :goto_22

    #@3e
    .line 846
    :cond_3e
    const/16 v7, 0x2a

    #@40
    if-ne v0, v7, :cond_45

    #@42
    const/16 v6, 0xb

    #@44
    goto :goto_22

    #@45
    .line 847
    :cond_45
    const/16 v7, 0x23

    #@47
    if-ne v0, v7, :cond_4c

    #@49
    const/16 v6, 0xc

    #@4b
    goto :goto_22

    #@4c
    .line 848
    :cond_4c
    const/4 v5, 0x0

    #@4d
    .line 851
    .end local v0           #c:C
    .end local v5           #rawData:[B
    .end local v6           #val:I
    :cond_4d
    return-object v5
.end method

.method private static encodeEmsUserDataPayload(Lcom/android/internal/telephony/cdma/sms/UserData;)V
    .registers 6
    .parameter "uData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 686
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@5
    move-result-object v1

    #@6
    .line 687
    .local v1, headerData:[B
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@8
    if-eqz v2, :cond_3f

    #@a
    .line 688
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@c
    const/16 v3, 0x9

    #@e
    if-ne v2, v3, :cond_15

    #@10
    .line 689
    const/4 v2, 0x1

    #@11
    invoke-static {p0, v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode7bitEms(Lcom/android/internal/telephony/cdma/sms/UserData;[BZ)V

    #@14
    .line 703
    :goto_14
    return-void

    #@15
    .line 690
    :cond_15
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@17
    const/4 v3, 0x4

    #@18
    if-ne v2, v3, :cond_1e

    #@1a
    .line 691
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode16bitEms(Lcom/android/internal/telephony/cdma/sms/UserData;[B)V

    #@1d
    goto :goto_14

    #@1e
    .line 693
    :cond_1e
    new-instance v2, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "unsupported EMS user data encoding ("

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    iget v4, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, ")"

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-direct {v2, v3}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@3e
    throw v2

    #@3f
    .line 698
    :cond_3f
    const/4 v2, 0x0

    #@40
    :try_start_40
    invoke-static {p0, v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode7bitEms(Lcom/android/internal/telephony/cdma/sms/UserData;[BZ)V
    :try_end_43
    .catch Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException; {:try_start_40 .. :try_end_43} :catch_44

    #@43
    goto :goto_14

    #@44
    .line 699
    :catch_44
    move-exception v0

    #@45
    .line 700
    .local v0, ex:Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode16bitEms(Lcom/android/internal/telephony/cdma/sms/UserData;[B)V

    #@48
    goto :goto_14
.end method

.method private static encodeKsc5601(Ljava/lang/String;)[B
    .registers 5
    .parameter "msg"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 608
    :try_start_0
    const-string v1, "ksc5601"

    #@2
    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    return-object v1

    #@7
    .line 609
    :catch_7
    move-exception v0

    #@8
    .line 610
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    new-instance v1, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "KSC5601 encode failed: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@20
    throw v1
.end method

.method private static encodeLanguageIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 4
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    .line 932
    const/4 v0, 0x1

    #@3
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@6
    .line 933
    iget v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->language:I

    #@8
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@b
    .line 934
    return-void
.end method

.method private static encodeMessageId(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 7
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v1, 0x1

    #@2
    const/16 v3, 0x8

    #@4
    .line 510
    invoke-virtual {p1, v3, v4}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@7
    .line 511
    const/4 v0, 0x4

    #@8
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    #@a
    invoke-virtual {p1, v0, v2}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@d
    .line 512
    iget v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@f
    shr-int/lit8 v0, v0, 0x8

    #@11
    invoke-virtual {p1, v3, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@14
    .line 513
    iget v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@16
    invoke-virtual {p1, v3, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@19
    .line 514
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@1b
    if-eqz v0, :cond_25

    #@1d
    move v0, v1

    #@1e
    :goto_1e
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@21
    .line 515
    invoke-virtual {p1, v4}, Lcom/android/internal/util/BitwiseOutputStream;->skip(I)V

    #@24
    .line 516
    return-void

    #@25
    .line 514
    :cond_25
    const/4 v0, 0x0

    #@26
    goto :goto_1e
.end method

.method private static encodeMsgCenterTimeStamp(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 4
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    .line 978
    const/4 v0, 0x6

    #@3
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@6
    .line 979
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@8
    iget v0, v0, Landroid/text/format/Time;->year:I

    #@a
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@d
    move-result v0

    #@e
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@11
    .line 980
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@13
    iget v0, v0, Landroid/text/format/Time;->month:I

    #@15
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@18
    move-result v0

    #@19
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@1c
    .line 981
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@1e
    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    #@20
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@23
    move-result v0

    #@24
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@27
    .line 982
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@29
    iget v0, v0, Landroid/text/format/Time;->hour:I

    #@2b
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@2e
    move-result v0

    #@2f
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@32
    .line 983
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@34
    iget v0, v0, Landroid/text/format/Time;->minute:I

    #@36
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@39
    move-result v0

    #@3a
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@3d
    .line 984
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@3f
    iget v0, v0, Landroid/text/format/Time;->second:I

    #@41
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@44
    move-result v0

    #@45
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@48
    .line 985
    return-void
.end method

.method private static encodeMsgCount(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 4
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    .line 910
    const/4 v0, 0x1

    #@3
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@6
    .line 911
    iget v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->numberOfMessages:I

    #@8
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@b
    .line 912
    return-void
.end method

.method private static encodeMsgDeliveryAlert(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 4
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    .line 955
    const/16 v0, 0x8

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@6
    .line 956
    const/4 v0, 0x2

    #@7
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->alert:I

    #@9
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@c
    .line 957
    const/4 v0, 0x6

    #@d
    invoke-virtual {p1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->skip(I)V

    #@10
    .line 958
    return-void
.end method

.method private static encodeMsgStatus(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 4
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    .line 902
    const/16 v0, 0x8

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@6
    .line 903
    const/4 v0, 0x2

    #@7
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->errorClass:I

    #@9
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@c
    .line 904
    const/4 v0, 0x6

    #@d
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageStatus:I

    #@f
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@12
    .line 905
    return-void
.end method

.method private static encodePriorityIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 4
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    .line 947
    const/16 v0, 0x8

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@6
    .line 948
    const/4 v0, 0x2

    #@7
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@9
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@c
    .line 949
    const/4 v0, 0x6

    #@d
    invoke-virtual {p1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->skip(I)V

    #@10
    .line 950
    return-void
.end method

.method private static encodePrivacyIndicator(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 4
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    .line 924
    const/16 v0, 0x8

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@6
    .line 925
    const/4 v0, 0x2

    #@7
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->privacy:I

    #@9
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@c
    .line 926
    const/4 v0, 0x6

    #@d
    invoke-virtual {p1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->skip(I)V

    #@10
    .line 927
    return-void
.end method

.method private static encodeReplyOption(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 5
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 827
    const/16 v0, 0x8

    #@4
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@7
    .line 828
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userAckReq:Z

    #@9
    if-eqz v0, :cond_2c

    #@b
    move v0, v1

    #@c
    :goto_c
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@f
    .line 829
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->deliveryAckReq:Z

    #@11
    if-eqz v0, :cond_2e

    #@13
    move v0, v1

    #@14
    :goto_14
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@17
    .line 830
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->readAckReq:Z

    #@19
    if-eqz v0, :cond_30

    #@1b
    move v0, v1

    #@1c
    :goto_1c
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@1f
    .line 831
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->reportReq:Z

    #@21
    if-eqz v0, :cond_32

    #@23
    move v0, v1

    #@24
    :goto_24
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@27
    .line 832
    const/4 v0, 0x4

    #@28
    invoke-virtual {p1, v0, v2}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@2b
    .line 833
    return-void

    #@2c
    :cond_2c
    move v0, v2

    #@2d
    .line 828
    goto :goto_c

    #@2e
    :cond_2e
    move v0, v2

    #@2f
    .line 829
    goto :goto_14

    #@30
    :cond_30
    move v0, v2

    #@31
    .line 830
    goto :goto_1c

    #@32
    :cond_32
    move v0, v2

    #@33
    .line 831
    goto :goto_24
.end method

.method private static encodeScpResults(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 9
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/16 v5, 0x8

    #@3
    .line 963
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->serviceCategoryProgramResults:Ljava/util/ArrayList;

    #@5
    .line 964
    .local v3, results:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/cdma/CdmaSmsCbProgramResults;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v4

    #@9
    mul-int/lit8 v4, v4, 0x4

    #@b
    invoke-virtual {p1, v5, v4}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@e
    .line 965
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v1

    #@12
    .local v1, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v4

    #@16
    if-eqz v4, :cond_3c

    #@18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Landroid/telephony/cdma/CdmaSmsCbProgramResults;

    #@1e
    .line 966
    .local v2, result:Landroid/telephony/cdma/CdmaSmsCbProgramResults;
    invoke-virtual {v2}, Landroid/telephony/cdma/CdmaSmsCbProgramResults;->getCategory()I

    #@21
    move-result v0

    #@22
    .line 967
    .local v0, category:I
    shr-int/lit8 v4, v0, 0x8

    #@24
    invoke-virtual {p1, v5, v4}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@27
    .line 968
    invoke-virtual {p1, v5, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@2a
    .line 969
    invoke-virtual {v2}, Landroid/telephony/cdma/CdmaSmsCbProgramResults;->getLanguage()I

    #@2d
    move-result v4

    #@2e
    invoke-virtual {p1, v5, v4}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@31
    .line 970
    invoke-virtual {v2}, Landroid/telephony/cdma/CdmaSmsCbProgramResults;->getCategoryResult()I

    #@34
    move-result v4

    #@35
    invoke-virtual {p1, v6, v4}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@38
    .line 971
    invoke-virtual {p1, v6}, Lcom/android/internal/util/BitwiseOutputStream;->skip(I)V

    #@3b
    goto :goto_12

    #@3c
    .line 973
    .end local v0           #category:I
    .end local v2           #result:Landroid/telephony/cdma/CdmaSmsCbProgramResults;
    :cond_3c
    return-void
.end method

.method private static encodeShiftJis(Ljava/lang/String;)[B
    .registers 5
    .parameter "msg"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 707
    :try_start_0
    const-string v1, "Shift_JIS"

    #@2
    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    return-object v1

    #@7
    .line 708
    :catch_7
    move-exception v0

    #@8
    .line 709
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    new-instance v1, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Shift-JIS encode failed: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@20
    throw v1
.end method

.method private static encodeUserData(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 12
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;,
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v7, 0x8c

    #@2
    const/16 v9, 0xa

    #@4
    const/16 v8, 0x8

    #@6
    const/4 v6, 0x0

    #@7
    const/4 v5, 0x1

    #@8
    .line 789
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@a
    invoke-static {v4}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeUserDataPayload(Lcom/android/internal/telephony/cdma/sms/UserData;)V

    #@d
    .line 790
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@f
    iget-object v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@11
    if-eqz v4, :cond_4b

    #@13
    move v4, v5

    #@14
    :goto_14
    iput-boolean v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@16
    .line 792
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@18
    iget-object v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@1a
    array-length v4, v4

    #@1b
    if-le v4, v7, :cond_4d

    #@1d
    .line 793
    new-instance v4, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@1f
    new-instance v5, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v6, "encoded user data too large ("

    #@26
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@2c
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@2e
    array-length v6, v6

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    const-string v6, " > "

    #@35
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    const-string v6, " bytes)"

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    invoke-direct {v4, v5}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v4

    #@4b
    :cond_4b
    move v4, v6

    #@4c
    .line 790
    goto :goto_14

    #@4d
    .line 805
    :cond_4d
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@4f
    iget-object v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@51
    array-length v4, v4

    #@52
    mul-int/lit8 v4, v4, 0x8

    #@54
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@56
    iget v7, v7, Lcom/android/internal/telephony/cdma/sms/UserData;->paddingBits:I

    #@58
    sub-int v0, v4, v7

    #@5a
    .line 806
    .local v0, dataBits:I
    add-int/lit8 v2, v0, 0xd

    #@5c
    .line 807
    .local v2, paramBits:I
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@5e
    iget v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@60
    if-eq v4, v5, :cond_68

    #@62
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@64
    iget v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@66
    if-ne v4, v9, :cond_6a

    #@68
    .line 809
    :cond_68
    add-int/lit8 v2, v2, 0x8

    #@6a
    .line 811
    :cond_6a
    div-int/lit8 v7, v2, 0x8

    #@6c
    rem-int/lit8 v4, v2, 0x8

    #@6e
    if-lez v4, :cond_a9

    #@70
    move v4, v5

    #@71
    :goto_71
    add-int v3, v7, v4

    #@73
    .line 812
    .local v3, paramBytes:I
    mul-int/lit8 v4, v3, 0x8

    #@75
    sub-int v1, v4, v2

    #@77
    .line 813
    .local v1, paddingBits:I
    invoke-virtual {p1, v8, v3}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@7a
    .line 814
    const/4 v4, 0x5

    #@7b
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@7d
    iget v7, v7, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@7f
    invoke-virtual {p1, v4, v7}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@82
    .line 815
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@84
    iget v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@86
    if-eq v4, v5, :cond_8e

    #@88
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@8a
    iget v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@8c
    if-ne v4, v9, :cond_95

    #@8e
    .line 817
    :cond_8e
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@90
    iget v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->msgType:I

    #@92
    invoke-virtual {p1, v8, v4}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@95
    .line 819
    :cond_95
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@97
    iget v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@99
    invoke-virtual {p1, v8, v4}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@9c
    .line 820
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@9e
    iget-object v4, v4, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@a0
    invoke-virtual {p1, v0, v4}, Lcom/android/internal/util/BitwiseOutputStream;->writeByteArray(I[B)V

    #@a3
    .line 821
    if-lez v1, :cond_a8

    #@a5
    invoke-virtual {p1, v1, v6}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@a8
    .line 822
    :cond_a8
    return-void

    #@a9
    .end local v1           #paddingBits:I
    .end local v3           #paramBytes:I
    :cond_a9
    move v4, v6

    #@aa
    .line 811
    goto :goto_71
.end method

.method private static encodeUserDataPayload(Lcom/android/internal/telephony/cdma/sms/UserData;)V
    .registers 9
    .parameter "uData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 716
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@6
    if-nez v2, :cond_17

    #@8
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@a
    if-eqz v2, :cond_17

    #@c
    .line 717
    const-string v2, "SMS"

    #@e
    const-string v3, "user data with null payloadStr"

    #@10
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 718
    const-string v2, ""

    #@15
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@17
    .line 721
    :cond_17
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@19
    if-eqz v2, :cond_1f

    #@1b
    .line 722
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeEmsUserDataPayload(Lcom/android/internal/telephony/cdma/sms/UserData;)V

    #@1e
    .line 779
    :goto_1e
    return-void

    #@1f
    .line 726
    :cond_1f
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@21
    if-eqz v2, :cond_d7

    #@23
    .line 727
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@25
    if-nez v2, :cond_3f

    #@27
    .line 728
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@29
    if-nez v2, :cond_39

    #@2b
    .line 729
    const-string v2, "SMS"

    #@2d
    const-string v3, "user data with octet encoding but null payload"

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 730
    new-array v2, v4, [B

    #@34
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@36
    .line 731
    iput v4, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@38
    goto :goto_1e

    #@39
    .line 733
    :cond_39
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@3b
    array-length v2, v2

    #@3c
    iput v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@3e
    goto :goto_1e

    #@3f
    .line 736
    :cond_3f
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@41
    if-nez v2, :cond_4e

    #@43
    .line 737
    const-string v2, "SMS"

    #@45
    const-string v3, "non-octet user data with null payloadStr"

    #@47
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 738
    const-string v2, ""

    #@4c
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@4e
    .line 740
    :cond_4e
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@50
    const/16 v3, 0x9

    #@52
    if-ne v2, v3, :cond_63

    #@54
    .line 741
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@56
    invoke-static {v2, v4, v5}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode7bitGsm(Ljava/lang/String;IZ)Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;

    #@59
    move-result-object v1

    #@5a
    .line 742
    .local v1, gcr:Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;
    iget-object v2, v1, Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;->data:[B

    #@5c
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@5e
    .line 743
    iget v2, v1, Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;->septets:I

    #@60
    iput v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@62
    goto :goto_1e

    #@63
    .line 744
    .end local v1           #gcr:Lcom/android/internal/telephony/cdma/sms/BearerData$Gsm7bitCodingResult;
    :cond_63
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@65
    if-ne v2, v6, :cond_78

    #@67
    .line 745
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@69
    invoke-static {v2, v5}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode7bitAscii(Ljava/lang/String;Z)[B

    #@6c
    move-result-object v2

    #@6d
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@6f
    .line 746
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@71
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@74
    move-result v2

    #@75
    iput v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@77
    goto :goto_1e

    #@78
    .line 747
    :cond_78
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@7a
    if-ne v2, v7, :cond_8d

    #@7c
    .line 748
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@7e
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeUtf16(Ljava/lang/String;)[B

    #@81
    move-result-object v2

    #@82
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@84
    .line 749
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@86
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@89
    move-result v2

    #@8a
    iput v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@8c
    goto :goto_1e

    #@8d
    .line 751
    :cond_8d
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@8f
    const/16 v3, 0x10

    #@91
    if-ne v2, v3, :cond_a2

    #@93
    .line 752
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@95
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeKsc5601(Ljava/lang/String;)[B

    #@98
    move-result-object v2

    #@99
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@9b
    .line 753
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@9d
    array-length v2, v2

    #@9e
    iput v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@a0
    goto/16 :goto_1e

    #@a2
    .line 760
    :cond_a2
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@a4
    const/4 v3, 0x5

    #@a5
    if-ne v2, v3, :cond_b6

    #@a7
    .line 761
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@a9
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeShiftJis(Ljava/lang/String;)[B

    #@ac
    move-result-object v2

    #@ad
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@af
    .line 762
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@b1
    array-length v2, v2

    #@b2
    iput v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@b4
    goto/16 :goto_1e

    #@b6
    .line 764
    :cond_b6
    new-instance v2, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@b8
    new-instance v3, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v4, "unsupported user data encoding ("

    #@bf
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v3

    #@c3
    iget v4, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@c5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v3

    #@c9
    const-string v4, ")"

    #@cb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v3

    #@cf
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v3

    #@d3
    invoke-direct {v2, v3}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@d6
    throw v2

    #@d7
    .line 770
    :cond_d7
    :try_start_d7
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@d9
    const/4 v3, 0x0

    #@da
    invoke-static {v2, v3}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode7bitAscii(Ljava/lang/String;Z)[B

    #@dd
    move-result-object v2

    #@de
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@e0
    .line 771
    const/4 v2, 0x2

    #@e1
    iput v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I
    :try_end_e3
    .catch Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException; {:try_start_d7 .. :try_end_e3} :catch_ef

    #@e3
    .line 776
    :goto_e3
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@e5
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@e8
    move-result v2

    #@e9
    iput v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@eb
    .line 777
    iput-boolean v5, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@ed
    goto/16 :goto_1e

    #@ef
    .line 772
    :catch_ef
    move-exception v0

    #@f0
    .line 773
    .local v0, ex:Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@f2
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encodeUtf16(Ljava/lang/String;)[B

    #@f5
    move-result-object v2

    #@f6
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@f8
    .line 774
    iput v7, p0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@fa
    goto :goto_e3
.end method

.method private static encodeUtf16(Ljava/lang/String;)[B
    .registers 5
    .parameter "msg"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 597
    :try_start_0
    const-string v1, "utf-16be"

    #@2
    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    return-object v1

    #@7
    .line 598
    :catch_7
    move-exception v0

    #@8
    .line 599
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    new-instance v1, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "UTF-16 encode failed: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@20
    throw v1
.end method

.method private static encodeValidityPeriodRel(Lcom/android/internal/telephony/cdma/sms/BearerData;Lcom/android/internal/util/BitwiseOutputStream;)V
    .registers 4
    .parameter "bData"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    .line 917
    const/4 v0, 0x1

    #@3
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@6
    .line 918
    iget v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->validityPeriodRelative:I

    #@8
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@b
    .line 919
    return-void
.end method

.method private static getBitsForNumFields(II)I
    .registers 5
    .parameter "msgEncoding"
    .parameter "numFields"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 2166
    packed-switch p0, :pswitch_data_2c

    #@3
    .line 2183
    :pswitch_3
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "unsupported message encoding ("

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const/16 v2, 0x29

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData$CodingException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 2172
    :pswitch_22
    mul-int/lit8 v0, p1, 0x8

    #@24
    .line 2180
    :goto_24
    return v0

    #@25
    .line 2177
    :pswitch_25
    mul-int/lit8 v0, p1, 0x7

    #@27
    goto :goto_24

    #@28
    .line 2180
    :pswitch_28
    mul-int/lit8 v0, p1, 0x10

    #@2a
    goto :goto_24

    #@2b
    .line 2166
    nop

    #@2c
    :pswitch_data_2c
    .packed-switch 0x0
        :pswitch_22
        :pswitch_3
        :pswitch_25
        :pswitch_25
        :pswitch_28
        :pswitch_22
        :pswitch_22
        :pswitch_22
        :pswitch_22
        :pswitch_25
    .end packed-switch
.end method

.method private static getLanguageCodeForValue(I)Ljava/lang/String;
    .registers 2
    .parameter "languageValue"

    #@0
    .prologue
    .line 444
    packed-switch p0, :pswitch_data_1a

    #@3
    .line 467
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 446
    :pswitch_5
    const-string v0, "en"

    #@7
    goto :goto_4

    #@8
    .line 449
    :pswitch_8
    const-string v0, "fr"

    #@a
    goto :goto_4

    #@b
    .line 452
    :pswitch_b
    const-string v0, "es"

    #@d
    goto :goto_4

    #@e
    .line 455
    :pswitch_e
    const-string v0, "ja"

    #@10
    goto :goto_4

    #@11
    .line 458
    :pswitch_11
    const-string v0, "ko"

    #@13
    goto :goto_4

    #@14
    .line 461
    :pswitch_14
    const-string v0, "zh"

    #@16
    goto :goto_4

    #@17
    .line 464
    :pswitch_17
    const-string v0, "he"

    #@19
    goto :goto_4

    #@1a
    .line 444
    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_5
        :pswitch_8
        :pswitch_b
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
    .end packed-switch
.end method

.method private static isCmasAlertCategory(I)Z
    .registers 2
    .parameter "category"

    #@0
    .prologue
    .line 2367
    const/16 v0, 0x1000

    #@2
    if-lt p0, v0, :cond_a

    #@4
    const/16 v0, 0x10ff

    #@6
    if-gt p0, v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private static lastByteIsMultiChar([BII)Z
    .registers 6
    .parameter "text"
    .parameter "pos"
    .parameter "nLength"

    #@0
    .prologue
    .line 2539
    move v1, p1

    #@1
    .line 2540
    .local v1, nIndex:I
    const/4 v0, 0x0

    #@2
    .line 2542
    .local v0, bIsMultiChar:Z
    :goto_2
    add-int v2, p2, p1

    #@4
    if-ge v1, v2, :cond_13

    #@6
    .line 2544
    aget-byte v2, p0, v1

    #@8
    if-gez v2, :cond_11

    #@a
    .line 2545
    if-eqz v0, :cond_d

    #@c
    .line 2546
    const/4 v0, 0x0

    #@d
    .line 2548
    :cond_d
    const/4 v0, 0x1

    #@e
    .line 2552
    :goto_e
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_2

    #@11
    .line 2550
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_e

    #@13
    .line 2554
    :cond_13
    return v0
.end method

.method private static serviceCategoryToCmasMessageClass(I)I
    .registers 2
    .parameter "serviceCategory"

    #@0
    .prologue
    .line 2135
    packed-switch p0, :pswitch_data_10

    #@3
    .line 2152
    const/4 v0, -0x1

    #@4
    :goto_4
    return v0

    #@5
    .line 2137
    :pswitch_5
    const/4 v0, 0x0

    #@6
    goto :goto_4

    #@7
    .line 2140
    :pswitch_7
    const/4 v0, 0x1

    #@8
    goto :goto_4

    #@9
    .line 2143
    :pswitch_9
    const/4 v0, 0x2

    #@a
    goto :goto_4

    #@b
    .line 2146
    :pswitch_b
    const/4 v0, 0x3

    #@c
    goto :goto_4

    #@d
    .line 2149
    :pswitch_d
    const/4 v0, 0x4

    #@e
    goto :goto_4

    #@f
    .line 2135
    nop

    #@10
    :pswitch_data_10
    .packed-switch 0x1000
        :pswitch_5
        :pswitch_7
        :pswitch_9
        :pswitch_b
        :pswitch_d
    .end packed-switch
.end method


# virtual methods
.method public getLanguage()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 435
    iget v0, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->language:I

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/sms/BearerData;->getLanguageCodeForValue(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 473
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 474
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "BearerData "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 475
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "{ messageType="

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 476
    new-instance v1, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v2, ", messageId="

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    .line 477
    new-instance v1, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v2, ", priority="

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priorityIndicatorSet:Z

    #@47
    if-eqz v1, :cond_290

    #@49
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@4b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e
    move-result-object v1

    #@4f
    :goto_4f
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    .line 478
    new-instance v1, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v2, ", privacy="

    #@61
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->privacyIndicatorSet:Z

    #@67
    if-eqz v1, :cond_294

    #@69
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->privacy:I

    #@6b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6e
    move-result-object v1

    #@6f
    :goto_6f
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v1

    #@77
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    .line 479
    new-instance v1, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v2, ", alert="

    #@81
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v2

    #@85
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->alertIndicatorSet:Z

    #@87
    if-eqz v1, :cond_298

    #@89
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->alert:I

    #@8b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8e
    move-result-object v1

    #@8f
    :goto_8f
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v1

    #@93
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v1

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    .line 480
    new-instance v1, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v2, ", displayMode="

    #@a1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v2

    #@a5
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->displayModeSet:Z

    #@a7
    if-eqz v1, :cond_29c

    #@a9
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->displayMode:I

    #@ab
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ae
    move-result-object v1

    #@af
    :goto_af
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v1

    #@b3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v1

    #@b7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    .line 481
    new-instance v1, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v2, ", language="

    #@c1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v2

    #@c5
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->languageIndicatorSet:Z

    #@c7
    if-eqz v1, :cond_2a0

    #@c9
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->language:I

    #@cb
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ce
    move-result-object v1

    #@cf
    :goto_cf
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v1

    #@d3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v1

    #@d7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    .line 482
    new-instance v1, Ljava/lang/StringBuilder;

    #@dc
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@df
    const-string v2, ", errorClass="

    #@e1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v2

    #@e5
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageStatusSet:Z

    #@e7
    if-eqz v1, :cond_2a4

    #@e9
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->errorClass:I

    #@eb
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ee
    move-result-object v1

    #@ef
    :goto_ef
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v1

    #@f3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f6
    move-result-object v1

    #@f7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    .line 483
    new-instance v1, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    const-string v2, ", msgStatus="

    #@101
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v2

    #@105
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageStatusSet:Z

    #@107
    if-eqz v1, :cond_2a8

    #@109
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageStatus:I

    #@10b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10e
    move-result-object v1

    #@10f
    :goto_10f
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v1

    #@113
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v1

    #@117
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    .line 484
    new-instance v1, Ljava/lang/StringBuilder;

    #@11c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11f
    const-string v2, ", msgCenterTimeStamp="

    #@121
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v2

    #@125
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@127
    if-eqz v1, :cond_2ac

    #@129
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@12b
    :goto_12b
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v1

    #@12f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@132
    move-result-object v1

    #@133
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    .line 486
    new-instance v1, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v2, ", validityPeriodAbsolute="

    #@13d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v2

    #@141
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->validityPeriodAbsolute:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@143
    if-eqz v1, :cond_2b0

    #@145
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->validityPeriodAbsolute:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@147
    :goto_147
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v1

    #@14b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14e
    move-result-object v1

    #@14f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    .line 488
    new-instance v1, Ljava/lang/StringBuilder;

    #@154
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@157
    const-string v2, ", validityPeriodRelative="

    #@159
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v2

    #@15d
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->validityPeriodRelativeSet:Z

    #@15f
    if-eqz v1, :cond_2b4

    #@161
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->validityPeriodRelative:I

    #@163
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@166
    move-result-object v1

    #@167
    :goto_167
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v1

    #@16b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16e
    move-result-object v1

    #@16f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    .line 490
    new-instance v1, Ljava/lang/StringBuilder;

    #@174
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@177
    const-string v2, ", deferredDeliveryTimeAbsolute="

    #@179
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v2

    #@17d
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->deferredDeliveryTimeAbsolute:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@17f
    if-eqz v1, :cond_2b8

    #@181
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->deferredDeliveryTimeAbsolute:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@183
    :goto_183
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@186
    move-result-object v1

    #@187
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18a
    move-result-object v1

    #@18b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    .line 492
    new-instance v1, Ljava/lang/StringBuilder;

    #@190
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@193
    const-string v2, ", deferredDeliveryTimeRelative="

    #@195
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198
    move-result-object v2

    #@199
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->deferredDeliveryTimeRelativeSet:Z

    #@19b
    if-eqz v1, :cond_2bc

    #@19d
    iget v1, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->deferredDeliveryTimeRelative:I

    #@19f
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a2
    move-result-object v1

    #@1a3
    :goto_1a3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v1

    #@1a7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1aa
    move-result-object v1

    #@1ab
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ae
    .line 494
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b3
    const-string v2, ", userAckReq="

    #@1b5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v1

    #@1b9
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userAckReq:Z

    #@1bb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1be
    move-result-object v1

    #@1bf
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c2
    move-result-object v1

    #@1c3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c6
    .line 495
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1cb
    const-string v2, ", deliveryAckReq="

    #@1cd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d0
    move-result-object v1

    #@1d1
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->deliveryAckReq:Z

    #@1d3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v1

    #@1d7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1da
    move-result-object v1

    #@1db
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1de
    .line 496
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e3
    const-string v2, ", readAckReq="

    #@1e5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v1

    #@1e9
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->readAckReq:Z

    #@1eb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v1

    #@1ef
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f2
    move-result-object v1

    #@1f3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    .line 497
    new-instance v1, Ljava/lang/StringBuilder;

    #@1f8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1fb
    const-string v2, ", reportReq="

    #@1fd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@200
    move-result-object v1

    #@201
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->reportReq:Z

    #@203
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@206
    move-result-object v1

    #@207
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20a
    move-result-object v1

    #@20b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20e
    .line 498
    new-instance v1, Ljava/lang/StringBuilder;

    #@210
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@213
    const-string v2, ", numberOfMessages="

    #@215
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@218
    move-result-object v1

    #@219
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->numberOfMessages:I

    #@21b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21e
    move-result-object v1

    #@21f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@222
    move-result-object v1

    #@223
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    .line 499
    new-instance v1, Ljava/lang/StringBuilder;

    #@228
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@22b
    const-string v2, ", callbackNumber="

    #@22d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@230
    move-result-object v1

    #@231
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->callbackNumber:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@233
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@236
    move-result-object v1

    #@237
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23a
    move-result-object v1

    #@23b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23e
    .line 500
    new-instance v1, Ljava/lang/StringBuilder;

    #@240
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@243
    const-string v2, ", depositIndex="

    #@245
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@248
    move-result-object v1

    #@249
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->depositIndex:I

    #@24b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24e
    move-result-object v1

    #@24f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@252
    move-result-object v1

    #@253
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@256
    .line 501
    new-instance v1, Ljava/lang/StringBuilder;

    #@258
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25b
    const-string v2, ", hasUserDataHeader="

    #@25d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@260
    move-result-object v1

    #@261
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@263
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@266
    move-result-object v1

    #@267
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26a
    move-result-object v1

    #@26b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26e
    .line 502
    new-instance v1, Ljava/lang/StringBuilder;

    #@270
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@273
    const-string v2, ", userData="

    #@275
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@278
    move-result-object v1

    #@279
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@27b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27e
    move-result-object v1

    #@27f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@282
    move-result-object v1

    #@283
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@286
    .line 503
    const-string v1, " }"

    #@288
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28b
    .line 504
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28e
    move-result-object v1

    #@28f
    return-object v1

    #@290
    .line 477
    :cond_290
    const-string v1, "unset"

    #@292
    goto/16 :goto_4f

    #@294
    .line 478
    :cond_294
    const-string v1, "unset"

    #@296
    goto/16 :goto_6f

    #@298
    .line 479
    :cond_298
    const-string v1, "unset"

    #@29a
    goto/16 :goto_8f

    #@29c
    .line 480
    :cond_29c
    const-string v1, "unset"

    #@29e
    goto/16 :goto_af

    #@2a0
    .line 481
    :cond_2a0
    const-string v1, "unset"

    #@2a2
    goto/16 :goto_cf

    #@2a4
    .line 482
    :cond_2a4
    const-string v1, "unset"

    #@2a6
    goto/16 :goto_ef

    #@2a8
    .line 483
    :cond_2a8
    const-string v1, "unset"

    #@2aa
    goto/16 :goto_10f

    #@2ac
    .line 484
    :cond_2ac
    const-string v1, "unset"

    #@2ae
    goto/16 :goto_12b

    #@2b0
    .line 486
    :cond_2b0
    const-string v1, "unset"

    #@2b2
    goto/16 :goto_147

    #@2b4
    .line 488
    :cond_2b4
    const-string v1, "unset"

    #@2b6
    goto/16 :goto_167

    #@2b8
    .line 490
    :cond_2b8
    const-string v1, "unset"

    #@2ba
    goto/16 :goto_183

    #@2bc
    .line 492
    :cond_2bc
    const-string v1, "unset"

    #@2be
    goto/16 :goto_1a3
.end method
