.class public abstract Lcom/android/internal/telephony/SmsAddress;
.super Ljava/lang/Object;
.source "SmsAddress.java"


# static fields
.field public static final IS_41_EMAIL_NETWORK_ADDRESS:Ljava/lang/String; = "6245"

.field public static final NUMBER_PLAN_DATA:I = 0x3

.field public static final NUMBER_PLAN_RESERVED_2:I = 0x2

.field public static final NUMBER_PLAN_TELEPHONY:I = 0x1

.field public static final NUMBER_PLAN_TELEX:I = 0x4

.field public static final NUMBER_PLAN_UNKNOWN:I = 0x0

.field public static final TON_ABBREVIATED:I = 0x6

.field public static final TON_ALPHANUMERIC:I = 0x5

.field public static final TON_INTERNATIONAL:I = 0x1

.field public static final TON_NATIONAL:I = 0x2

.field public static final TON_NETWORK:I = 0x3

.field public static final TON_SUBSCRIBER:I = 0x4

.field public static final TON_UNKNOWN:I


# instance fields
.field public address:Ljava/lang/String;

.field public origBytes:[B

.field public ton:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public couldBeEmailGateway()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x4

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v0, 0x1

    #@3
    .line 79
    const/4 v2, 0x0

    #@4
    const-string v3, "vzw_sms_fromvtext"

    #@6
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9
    move-result v2

    #@a
    if-ne v2, v0, :cond_21

    #@c
    .line 80
    iget-object v2, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@e
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@11
    move-result v2

    #@12
    if-ne v2, v4, :cond_1f

    #@14
    iget-object v2, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@16
    const-string v3, "6245"

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_1f

    #@1e
    .line 86
    :cond_1e
    :goto_1e
    return v0

    #@1f
    :cond_1f
    move v0, v1

    #@20
    .line 83
    goto :goto_1e

    #@21
    .line 86
    :cond_21
    iget-object v2, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@23
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@26
    move-result v2

    #@27
    if-le v2, v4, :cond_1e

    #@29
    move v0, v1

    #@2a
    goto :goto_1e
.end method

.method public getAddressString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public isAlphanumeric()Z
    .registers 3

    #@0
    .prologue
    .line 61
    iget v0, p0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@2
    const/4 v1, 0x5

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public isNetworkSpecific()Z
    .registers 3

    #@0
    .prologue
    .line 68
    iget v0, p0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@2
    const/4 v1, 0x3

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method
