.class public Lcom/android/internal/telephony/gfit/GfitUtils;
.super Landroid/os/Handler;
.source "GfitUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;
    }
.end annotation


# static fields
.field static final DBG:Z = true

.field public static final EVENT_COUNT_IN_NO_SERVICE:I = 0xdc

.field public static final EVENT_END_QUERY_AVAILABLE_NETWORKS:I = 0xde

.field public static final EVENT_GFIT_HANDLE_NETWORK_MODE_AFTER_DELAY:I = 0xc9

.field public static final EVENT_GFIT_ICC_CHANGED:I = 0xd4

.field public static final EVENT_GFIT_NO_SERVICE_CHANGED:I = 0x64

.field public static final EVENT_GFIT_POPUP_SWTICH_TO_GLOBAL_MODE:I = 0xc8

.field public static final EVENT_GFIT_QUERY_AVAILABLE_NETWORKS:I = 0xd0

.field public static final EVENT_GFIT_QUERY_NETWORK_SELECTION_MODE:I = 0xcc

.field public static final EVENT_GFIT_QUERY_NETWORK_SELECTION_MODE_AFTER_BOOT:I = 0xcd

.field public static final EVENT_GFIT_QUERY_PREFERRED_NETWORK_TYPE:I = 0xca

.field public static final EVENT_GFIT_REGISTERED_TO_NETWORK:I = 0x65

.field public static final EVENT_GFIT_RETRY_QUERY_AVAILABLE_NETWORKS:I = 0xd3

.field public static final EVENT_GFIT_RETRY_SET_PREFERRED_NETWORK_TYPE:I = 0xd2

.field public static final EVENT_GFIT_SET_NETWORK_SELECTION_AUTOMATIC:I = 0xce

.field public static final EVENT_GFIT_SET_NETWORK_SELECTION_MANUAL:I = 0xcf

.field public static final EVENT_GFIT_SET_PREFERRED_NETWORK_TYPE:I = 0xcb

.field public static final EVENT_GFIT_SWITCH_TO_NETWORK_SELECTION_MODE_AUTOMATIC:I = 0xd1

.field public static final EVENT_GFIT_TRIGGER_NO_SERVICE_CHANGED:I = 0x66

.field public static final EVENT_SET_PREFERRED_NETWORK_TYPE:I = 0xdf

.field public static final EVENT_START_QUERY_AVAILABLE_NETWORKS:I = 0xdd

.field private static final G1_FACTORY_PROPERTY:Ljava/lang/String; = "ro.factorytest"

.field private static final LGE_FTM_OFF:I = 0x2

.field private static final LGE_FTM_ON:I = 0x1

.field static LOG_TAG:Ljava/lang/String; = null

.field public static final NETWORK_SELECTION_KEY:Ljava/lang/String; = "network_selection_key"

.field public static final NETWORK_SELECTION_NAME_KEY:Ljava/lang/String; = "network_selection_name_key"

.field private static final NT_MODE_CDMA_ONLY:I = 0x2

.field private static final NT_MODE_GLOBAL:I = 0x0

.field private static final NT_MODE_LTE_CDMA:I = 0x3

.field private static final NT_MODE_LTE_GSM_UMTS:I = 0x1

.field private static final NT_SYSTEM_SELECTION_AUTOMATIC:I = 0x0

.field private static final NT_SYSTEM_SELECTION_MANUAL:I = 0x1

.field private static final PLMN_MAX:I = 0x14

.field private static final PROPERTY_AIRPLANE_MODE_ON:Ljava/lang/String; = "persist.radio.airplane_mode_on"

.field public static final RETRY_TO_QUERY_AVAILABLE_NETWORKS:I = 0xa

.field public static final RETRY_TO_SET_PREFFERED_NETWORK_TYPE:I = 0x5

.field private static final TIMEOUT_DECTECT_SIM_STATE:I = 0xbb8

.field private static final TIMEOUT_HANDLING_SIM_STATE_AFTER_DELAY:I = 0xbb8

.field private static final TIMEOUT_NO_DELAY:I = 0x0

.field private static final TIMEOUT_NO_SERVICE:I = 0x7530

.field private static final TIMEOUT_REMOVE_SWITCH_TO_AUTOMATIC_MODE:I = 0x1388

.field private static final TIMEOUT_RETRY_QUERY_AVAILABLE_NETWORKS:I = 0x2710

.field private static final TIMEOUT_RETRY_SET_PREFERRED_NETWORK_TYPE:I = 0x1388

.field static final TOAST_DBG:Z = false

.field private static final VZW_GFIT_ICC_ABSENT:I = 0x0

.field private static final VZW_GFIT_ICC_READY:I = 0x1

.field private static checkSIMState:Z = false

.field private static isFirstDisplay:Z = false

.field static isNoGlobalPopupNeeded:Z = false

.field private static final preferredNetworkMode:I = 0xa


# instance fields
.field private DISPLAY_INSERT_SIM_CARD:Ljava/lang/String;

.field private DISPLAY_NO_SIM:Ljava/lang/String;

.field private DISPLAY_POPUP:Ljava/lang/String;

.field private DISPLAY_SIM_DETECTED:Ljava/lang/String;

.field private cm:Lcom/android/internal/telephony/CommandsInterface;

.field private isManualMode:Z

.field private isManualSearching:Z

.field private isNetworkModeDisplayed:Z

.field private isToastDisplayed:Z

.field protected mContext:Landroid/content/Context;

.field private mFTMFlag:I

.field private mGfitIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mNetworkMode:I

.field private mNewServiceState:I

.field mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

.field private mProcessingNoService:Z

.field private mServiceState:I

.field mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

.field private nRetryPrefferedNetworkType:I

.field private nRetryQuertyAvailablenetworks:I

.field onClickCancelButton:Landroid/content/DialogInterface$OnClickListener;

.field onClickPlmnList:Landroid/content/DialogInterface$OnClickListener;

.field private phone:Lcom/android/internal/telephony/PhoneBase;

.field plmnListDialog:Landroid/app/AlertDialog;

.field private sst:Lcom/android/internal/telephony/ServiceStateTracker;

.field timeout:I

.field toastForCount:Landroid/widget/Toast;

.field private useActionSIMStateChanged:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 59
    const-string v0, "GSM"

    #@3
    sput-object v0, Lcom/android/internal/telephony/gfit/GfitUtils;->LOG_TAG:Ljava/lang/String;

    #@5
    .line 150
    sput-boolean v1, Lcom/android/internal/telephony/gfit/GfitUtils;->isNoGlobalPopupNeeded:Z

    #@7
    .line 152
    const/4 v0, 0x1

    #@8
    sput-boolean v0, Lcom/android/internal/telephony/gfit/GfitUtils;->isFirstDisplay:Z

    #@a
    .line 160
    sput-boolean v1, Lcom/android/internal/telephony/gfit/GfitUtils;->checkSIMState:Z

    #@c
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/ServiceStateTracker;Lcom/android/internal/telephony/PhoneBase;)V
    .registers 9
    .parameter "sst"
    .parameter "phone"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    .line 568
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@7
    .line 72
    const-string v1, "No SIM card. Switching to CDMA mode."

    #@9
    iput-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->DISPLAY_NO_SIM:Ljava/lang/String;

    #@b
    .line 73
    const-string v1, "SIM card detected. Switching to global mode."

    #@d
    iput-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->DISPLAY_SIM_DETECTED:Ljava/lang/String;

    #@f
    .line 74
    const-string v1, "A SIM Card is needed to operate this phone. Please turn off your phone and insert your SIM card."

    #@11
    iput-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->DISPLAY_INSERT_SIM_CARD:Ljava/lang/String;

    #@13
    .line 75
    const-string v1, "The network is not avialble. You can try global mode to see if alternative networks are availabe. Do you want to set network to global mode?"

    #@15
    iput-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->DISPLAY_POPUP:Ljava/lang/String;

    #@17
    .line 135
    iput v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryQuertyAvailablenetworks:I

    #@19
    .line 136
    iput v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryPrefferedNetworkType:I

    #@1b
    .line 139
    iput-boolean v4, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isNetworkModeDisplayed:Z

    #@1d
    .line 140
    iput-boolean v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@1f
    .line 141
    iput-boolean v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualMode:Z

    #@21
    .line 145
    iput-boolean v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualSearching:Z

    #@23
    .line 147
    iput-boolean v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mProcessingNoService:Z

    #@25
    .line 158
    iput-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@27
    .line 159
    iput-boolean v4, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->useActionSIMStateChanged:Z

    #@29
    .line 161
    iput v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->timeout:I

    #@2b
    .line 167
    iput v5, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mServiceState:I

    #@2d
    .line 168
    iput v5, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mNewServiceState:I

    #@2f
    .line 175
    iput v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mFTMFlag:I

    #@31
    .line 241
    const/16 v1, 0x14

    #@33
    new-array v1, v1, [Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@35
    iput-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@37
    .line 243
    new-instance v1, Lcom/android/internal/telephony/gfit/GfitUtils$1;

    #@39
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/gfit/GfitUtils$1;-><init>(Lcom/android/internal/telephony/gfit/GfitUtils;)V

    #@3c
    iput-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mGfitIntentReceiver:Landroid/content/BroadcastReceiver;

    #@3e
    .line 1184
    new-instance v1, Lcom/android/internal/telephony/gfit/GfitUtils$5;

    #@40
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/gfit/GfitUtils$5;-><init>(Lcom/android/internal/telephony/gfit/GfitUtils;)V

    #@43
    iput-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->onClickCancelButton:Landroid/content/DialogInterface$OnClickListener;

    #@45
    .line 1204
    new-instance v1, Lcom/android/internal/telephony/gfit/GfitUtils$6;

    #@47
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/gfit/GfitUtils$6;-><init>(Lcom/android/internal/telephony/gfit/GfitUtils;)V

    #@4a
    iput-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->onClickPlmnList:Landroid/content/DialogInterface$OnClickListener;

    #@4c
    .line 569
    iput-object p2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@4e
    .line 570
    iget-object v1, p2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@50
    iput-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@52
    .line 571
    iput-object p1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->sst:Lcom/android/internal/telephony/ServiceStateTracker;

    #@54
    .line 573
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@57
    move-result-object v1

    #@58
    iput-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@5a
    .line 575
    const/16 v1, 0x64

    #@5c
    invoke-virtual {p1, p0, v1, v3}, Lcom/android/internal/telephony/ServiceStateTracker;->registerForNoServiceChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5f
    .line 576
    const/16 v1, 0x65

    #@61
    invoke-virtual {p1, p0, v1, v3}, Lcom/android/internal/telephony/ServiceStateTracker;->registerForNetworkAttached(Landroid/os/Handler;ILjava/lang/Object;)V

    #@64
    .line 578
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@66
    if-eqz v1, :cond_6f

    #@68
    .line 579
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@6a
    const/16 v2, 0xd4

    #@6c
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/telephony/uicc/UiccController;->registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6f
    .line 582
    :cond_6f
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@71
    const/16 v2, 0xdd

    #@73
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForStartQueryAvailableNetwork(Landroid/os/Handler;ILjava/lang/Object;)V

    #@76
    .line 583
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@78
    const/16 v2, 0xde

    #@7a
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForEndQueryAvailableNetwork(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7d
    .line 584
    const/16 v1, 0xdf

    #@7f
    invoke-virtual {p2, p0, v1, v3}, Lcom/android/internal/telephony/PhoneBase;->registerForSetPreferredNetworkType(Landroid/os/Handler;ILjava/lang/Object;)V

    #@82
    .line 588
    invoke-virtual {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isUsedActionSIMStateChanged()Z

    #@85
    move-result v1

    #@86
    if-eqz v1, :cond_9b

    #@88
    .line 589
    new-instance v0, Landroid/content/IntentFilter;

    #@8a
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@8d
    .line 590
    .local v0, mGfitFilter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    #@8f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@92
    .line 591
    invoke-virtual {p2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@95
    move-result-object v1

    #@96
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mGfitIntentReceiver:Landroid/content/BroadcastReceiver;

    #@98
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@9b
    .line 594
    .end local v0           #mGfitFilter:Landroid/content/IntentFilter;
    :cond_9b
    const-string v1, "create GfitUtils..."

    #@9d
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@a0
    .line 595
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/gfit/GfitUtils;IIII)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/internal/telephony/gfit/GfitUtils;->triggerEventAfterTimeout(IIII)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/gfit/GfitUtils;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendQuerySystemModeAfterBoot()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private closeSystemDialogs()V
    .registers 3

    #@0
    .prologue
    .line 1290
    const-string v1, "close system dialogs"

    #@2
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@5
    .line 1291
    new-instance v0, Landroid/content/Intent;

    #@7
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    #@9
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c
    .line 1292
    .local v0, closeDialogs:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@e
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@15
    .line 1293
    return-void
.end method

.method private count()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 629
    iget v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->timeout:I

    #@3
    add-int/lit8 v0, v0, -0x1

    #@5
    iput v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->timeout:I

    #@7
    .line 630
    iget v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->timeout:I

    #@9
    if-ltz v0, :cond_61

    #@b
    .line 631
    iget-boolean v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mProcessingNoService:Z

    #@d
    if-eqz v0, :cond_39

    #@f
    .line 632
    iget-object v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->toastForCount:Landroid/widget/Toast;

    #@11
    if-eqz v0, :cond_3a

    #@13
    .line 633
    iget-object v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->toastForCount:Landroid/widget/Toast;

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "timer expiry : "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    iget v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->timeout:I

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    #@2d
    .line 634
    iget-object v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->toastForCount:Landroid/widget/Toast;

    #@2f
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@32
    .line 639
    :goto_32
    const/16 v0, 0xdc

    #@34
    const-wide/16 v1, 0x3e8

    #@36
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendEmptyMessageDelayed(IJ)Z

    #@39
    .line 644
    :cond_39
    :goto_39
    return-void

    #@3a
    .line 636
    :cond_3a
    iget-object v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@3c
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@3f
    move-result-object v0

    #@40
    new-instance v1, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v2, "timer expiry : "

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    iget v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->timeout:I

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@58
    move-result-object v0

    #@59
    iput-object v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->toastForCount:Landroid/widget/Toast;

    #@5b
    .line 637
    iget-object v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->toastForCount:Landroid/widget/Toast;

    #@5d
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@60
    goto :goto_32

    #@61
    .line 642
    :cond_61
    iput v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->timeout:I

    #@63
    goto :goto_39
.end method

.method private createNotePopup(I)V
    .registers 8
    .parameter "item"

    #@0
    .prologue
    .line 1064
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isPopupDisabled()Z

    #@3
    move-result v3

    #@4
    if-eqz v3, :cond_c

    #@6
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isSetupWizard()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_35

    #@c
    .line 1065
    :cond_c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Don\'t display user pop-up : isPopupDisabled = "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isPopupDisabled()Z

    #@1a
    move-result v4

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, " isSetupWizard = "

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isSetupWizard()Z

    #@28
    move-result v4

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@34
    .line 1097
    :goto_34
    return-void

    #@35
    .line 1068
    :cond_35
    const-string v1, ""

    #@37
    .line 1069
    .local v1, popup:Ljava/lang/String;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3a
    move-result-object v2

    #@3b
    .line 1070
    .local v2, r:Landroid/content/res/Resources;
    packed-switch p1, :pswitch_data_94

    #@3e
    .line 1078
    :goto_3e
    new-instance v3, Landroid/app/AlertDialog$Builder;

    #@40
    iget-object v4, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@42
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@45
    move-result-object v4

    #@46
    const v5, 0x20a01cb

    #@49
    invoke-direct {v3, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@4c
    const v4, 0x209025c

    #@4f
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@5a
    move-result-object v3

    #@5b
    const v4, 0x2090254

    #@5e
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@61
    move-result-object v4

    #@62
    new-instance v5, Lcom/android/internal/telephony/gfit/GfitUtils$2;

    #@64
    invoke-direct {v5, p0}, Lcom/android/internal/telephony/gfit/GfitUtils$2;-><init>(Lcom/android/internal/telephony/gfit/GfitUtils;)V

    #@67
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@6e
    move-result-object v0

    #@6f
    .line 1095
    .local v0, notePopup:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@72
    move-result-object v3

    #@73
    const/16 v4, 0x7d3

    #@75
    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    #@78
    .line 1096
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@7b
    goto :goto_34

    #@7c
    .line 1071
    .end local v0           #notePopup:Landroid/app/AlertDialog;
    :pswitch_7c
    const v3, 0x2090259

    #@7f
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@82
    move-result-object v1

    #@83
    goto :goto_3e

    #@84
    .line 1072
    :pswitch_84
    const v3, 0x2090257

    #@87
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@8a
    move-result-object v1

    #@8b
    goto :goto_3e

    #@8c
    .line 1073
    :pswitch_8c
    const v3, 0x2090258

    #@8f
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@92
    move-result-object v1

    #@93
    goto :goto_3e

    #@94
    .line 1070
    :pswitch_data_94
    .packed-switch 0x1
        :pswitch_7c
        :pswitch_84
        :pswitch_8c
    .end packed-switch
.end method

.method private createNotePopupGlobal()V
    .registers 7

    #@0
    .prologue
    .line 1100
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isPopupDisabled()Z

    #@3
    move-result v3

    #@4
    if-eqz v3, :cond_12

    #@6
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualSearchingInSystemSelect()Z

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_12

    #@c
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->getIsNoGlobalPopupNeeded()Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_18

    #@12
    .line 1103
    :cond_12
    const-string v3, "Don\'t display popup"

    #@14
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@17
    .line 1154
    :goto_17
    return-void

    #@18
    .line 1106
    :cond_18
    const-string v3, "Display Global popup"

    #@1a
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@1d
    .line 1108
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->closeSystemDialogs()V

    #@20
    .line 1111
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@23
    move-result-object v2

    #@24
    .line 1112
    .local v2, r:Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@26
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@29
    move-result-object v3

    #@2a
    const/high16 v4, 0x203

    #@2c
    const/4 v5, 0x0

    #@2d
    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    #@30
    move-result-object v0

    #@31
    check-cast v0, Landroid/widget/LinearLayout;

    #@33
    .line 1113
    .local v0, linear:Landroid/widget/LinearLayout;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    #@35
    iget-object v4, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@37
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@3a
    move-result-object v4

    #@3b
    const v5, 0x20a01cb

    #@3e
    invoke-direct {v3, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@41
    const v4, 0x209025c

    #@44
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    #@4f
    move-result-object v3

    #@50
    const v4, 0x2090254

    #@53
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    new-instance v5, Lcom/android/internal/telephony/gfit/GfitUtils$4;

    #@59
    invoke-direct {v5, p0, v0}, Lcom/android/internal/telephony/gfit/GfitUtils$4;-><init>(Lcom/android/internal/telephony/gfit/GfitUtils;Landroid/widget/LinearLayout;)V

    #@5c
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@5f
    move-result-object v3

    #@60
    const v4, 0x2090255

    #@63
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@66
    move-result-object v4

    #@67
    new-instance v5, Lcom/android/internal/telephony/gfit/GfitUtils$3;

    #@69
    invoke-direct {v5, p0}, Lcom/android/internal/telephony/gfit/GfitUtils$3;-><init>(Lcom/android/internal/telephony/gfit/GfitUtils;)V

    #@6c
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@73
    move-result-object v1

    #@74
    .line 1151
    .local v1, notePopup:Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@77
    move-result-object v3

    #@78
    const/16 v4, 0x7d3

    #@7a
    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    #@7d
    .line 1152
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@80
    goto :goto_17
.end method

.method private createPlmnListDialog([Ljava/lang/CharSequence;)V
    .registers 7
    .parameter "plmnList"

    #@0
    .prologue
    .line 1157
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isPopupDisabled()Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_c

    #@6
    .line 1158
    const-string v2, "Don\'t display popup"

    #@8
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@b
    .line 1182
    :goto_b
    return-void

    #@c
    .line 1162
    :cond_c
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->getSystemSelection()I

    #@f
    move-result v1

    #@10
    .line 1164
    .local v1, systemSelection:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "createPlmnListDialog(): systemSelection = "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@26
    .line 1165
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@29
    move-result-object v0

    #@2a
    .line 1166
    .local v0, r:Landroid/content/res/Resources;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    #@2c
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@2e
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@31
    move-result-object v3

    #@32
    const v4, 0x20a01cb

    #@35
    invoke-direct {v2, v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@38
    const v3, 0x209025d

    #@3b
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@42
    move-result-object v2

    #@43
    const/4 v3, -0x1

    #@44
    iget-object v4, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->onClickPlmnList:Landroid/content/DialogInterface$OnClickListener;

    #@46
    invoke-virtual {v2, p1, v3, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@49
    move-result-object v2

    #@4a
    const v3, 0x2090255

    #@4d
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@50
    move-result-object v3

    #@51
    iget-object v4, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->onClickCancelButton:Landroid/content/DialogInterface$OnClickListener;

    #@53
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@5a
    move-result-object v2

    #@5b
    iput-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->plmnListDialog:Landroid/app/AlertDialog;

    #@5d
    .line 1179
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->plmnListDialog:Landroid/app/AlertDialog;

    #@5f
    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@62
    move-result-object v2

    #@63
    const/16 v3, 0x7d3

    #@65
    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    #@68
    .line 1180
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->plmnListDialog:Landroid/app/AlertDialog;

    #@6a
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    #@6d
    .line 1181
    const/16 v2, 0x1388

    #@6f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendSwitchToNetworkSelectionModeAutomaticAfterTimeout(I)V

    #@72
    goto :goto_b
.end method

.method private eventToString(I)Ljava/lang/String;
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1224
    sparse-switch p1, :sswitch_data_42

    #@3
    .line 1245
    const-string v0, "Not supported Event"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 1225
    :sswitch_6
    const-string v0, "EVENT_GFIT_NO_SERVICE_CHANGED"

    #@8
    goto :goto_5

    #@9
    .line 1226
    :sswitch_9
    const-string v0, "EVENT_GFIT_REGISTERED_TO_NETWORK"

    #@b
    goto :goto_5

    #@c
    .line 1227
    :sswitch_c
    const-string v0, "EVENT_GFIT_TRIGGER_NO_SERVICE_CHANGED"

    #@e
    goto :goto_5

    #@f
    .line 1228
    :sswitch_f
    const-string v0, "EVENT_GFIT_POPUP_SWTICH_TO_GLOBAL_MODE"

    #@11
    goto :goto_5

    #@12
    .line 1229
    :sswitch_12
    const-string v0, "EVENT_GFIT_HANDLE_NETWORK_MODE_AFTER_DELAY"

    #@14
    goto :goto_5

    #@15
    .line 1230
    :sswitch_15
    const-string v0, "EVENT_GFIT_QUERY_PREFERRED_NETWORK_TYPE"

    #@17
    goto :goto_5

    #@18
    .line 1231
    :sswitch_18
    const-string v0, "EVENT_GFIT_SET_PREFERRED_NETWORK_TYPE"

    #@1a
    goto :goto_5

    #@1b
    .line 1232
    :sswitch_1b
    const-string v0, "EVENT_GFIT_QUERY_NETWORK_SELECTION_MODE"

    #@1d
    goto :goto_5

    #@1e
    .line 1233
    :sswitch_1e
    const-string v0, "EVENT_GFIT_QUERY_NETWORK_SELECTION_MODE_AFTER_BOOT"

    #@20
    goto :goto_5

    #@21
    .line 1234
    :sswitch_21
    const-string v0, "EVENT_GFIT_SET_NETWORK_SELECTION_AUTOMATIC"

    #@23
    goto :goto_5

    #@24
    .line 1235
    :sswitch_24
    const-string v0, "EVENT_GFIT_SET_NETWORK_SELECTION_MANUAL"

    #@26
    goto :goto_5

    #@27
    .line 1236
    :sswitch_27
    const-string v0, "EVENT_GFIT_QUERY_AVAILABLE_NETWORKS"

    #@29
    goto :goto_5

    #@2a
    .line 1237
    :sswitch_2a
    const-string v0, "EVENT_GFIT_SWITCH_TO_NETWORK_SELECTION_MODE_AUTOMATIC"

    #@2c
    goto :goto_5

    #@2d
    .line 1238
    :sswitch_2d
    const-string v0, "EVENT_GFIT_RETRY_SET_PREFERRED_NETWORK_TYPE"

    #@2f
    goto :goto_5

    #@30
    .line 1239
    :sswitch_30
    const-string v0, "EVENT_GFIT_RETRY_QUERY_AVAILABLE_NETWORKS"

    #@32
    goto :goto_5

    #@33
    .line 1240
    :sswitch_33
    const-string v0, "EVENT_GFIT_ICC_CHANGED"

    #@35
    goto :goto_5

    #@36
    .line 1241
    :sswitch_36
    const-string v0, "EVENT_COUNT_IN_NO_SERVICE"

    #@38
    goto :goto_5

    #@39
    .line 1242
    :sswitch_39
    const-string v0, "EVENT_START_QUERY_AVAILABLE_NETWORKS"

    #@3b
    goto :goto_5

    #@3c
    .line 1243
    :sswitch_3c
    const-string v0, "EVENT_END_QUERY_AVAILABLE_NETWORKS"

    #@3e
    goto :goto_5

    #@3f
    .line 1244
    :sswitch_3f
    const-string v0, "EVENT_SET_PREFERRED_NETWORK_TYPE"

    #@41
    goto :goto_5

    #@42
    .line 1224
    :sswitch_data_42
    .sparse-switch
        0x64 -> :sswitch_6
        0x65 -> :sswitch_9
        0x66 -> :sswitch_c
        0xc8 -> :sswitch_f
        0xc9 -> :sswitch_12
        0xca -> :sswitch_15
        0xcb -> :sswitch_18
        0xcc -> :sswitch_1b
        0xcd -> :sswitch_1e
        0xce -> :sswitch_21
        0xcf -> :sswitch_24
        0xd0 -> :sswitch_27
        0xd1 -> :sswitch_2a
        0xd2 -> :sswitch_2d
        0xd3 -> :sswitch_30
        0xd4 -> :sswitch_33
        0xdc -> :sswitch_36
        0xdd -> :sswitch_39
        0xde -> :sswitch_3c
        0xdf -> :sswitch_3f
    .end sparse-switch
.end method

.method private getIsNoGlobalPopupNeeded()Z
    .registers 3

    #@0
    .prologue
    .line 1297
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "isNoGlobalPopupNeeded = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-boolean v1, Lcom/android/internal/telephony/gfit/GfitUtils;->isNoGlobalPopupNeeded:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@18
    .line 1298
    sget-boolean v0, Lcom/android/internal/telephony/gfit/GfitUtils;->isNoGlobalPopupNeeded:Z

    #@1a
    return v0
.end method

.method private getServiceState()I
    .registers 4

    #@0
    .prologue
    .line 1003
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->sst:Lcom/android/internal/telephony/ServiceStateTracker;

    #@2
    iget-object v1, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@4
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getState()I

    #@7
    move-result v0

    #@8
    .line 1004
    .local v0, state:I
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "service state = "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@1e
    .line 1005
    return v0
.end method

.method private getSystemSelection()I
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 666
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@3
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v2

    #@b
    const-string v3, "preferred_network_mode"

    #@d
    const/16 v4, 0xa

    #@f
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@12
    move-result v0

    #@13
    .line 674
    .local v0, networkMode:I
    iput v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mNetworkMode:I

    #@15
    .line 676
    new-instance v2, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v3, "getSystemSelection() : networkMode = "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@2b
    .line 677
    packed-switch v0, :pswitch_data_3a

    #@2e
    .line 691
    :pswitch_2e
    const-string v2, "Not Supported system selection mode"

    #@30
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->loge(Ljava/lang/String;)V

    #@33
    .line 692
    :goto_33
    :pswitch_33
    return v1

    #@34
    .line 685
    :pswitch_34
    const/4 v1, 0x1

    #@35
    goto :goto_33

    #@36
    .line 687
    :pswitch_36
    const/4 v1, 0x2

    #@37
    goto :goto_33

    #@38
    .line 689
    :pswitch_38
    const/4 v1, 0x3

    #@39
    goto :goto_33

    #@3a
    .line 677
    :pswitch_data_3a
    .packed-switch 0x1
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_36
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_38
        :pswitch_34
        :pswitch_33
        :pswitch_34
    .end packed-switch
.end method

.method private handleCdmaOnlyMode(ZZ)V
    .registers 6
    .parameter "oldUiccState"
    .parameter "newUiccState"

    #@0
    .prologue
    const v2, 0x2090258

    #@3
    .line 839
    const-string v1, "handleNetworkMode() : CDMA Only"

    #@5
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@8
    .line 840
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@b
    move-result-object v0

    #@c
    .line 841
    .local v0, r:Landroid/content/res/Resources;
    if-eqz p1, :cond_26

    #@e
    .line 842
    if-eqz p2, :cond_1a

    #@10
    .line 844
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isNetworkModeDisplayed:Z

    #@12
    if-eqz v1, :cond_19

    #@14
    const-string v1, "Keep CDMA Only Mode"

    #@16
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@19
    .line 867
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 847
    :cond_1a
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@1c
    if-eqz v1, :cond_19

    #@1e
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@25
    goto :goto_19

    #@26
    .line 852
    :cond_26
    if-eqz p2, :cond_3e

    #@28
    .line 854
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@2a
    if-eqz v1, :cond_36

    #@2c
    const v1, 0x2090259

    #@2f
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@36
    .line 856
    :cond_36
    const/4 v1, 0x1

    #@37
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->createNotePopup(I)V

    #@3a
    .line 865
    :cond_3a
    :goto_3a
    invoke-virtual {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->switchToGlobalMode()V

    #@3d
    goto :goto_19

    #@3e
    .line 859
    :cond_3e
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@40
    if-eqz v1, :cond_3a

    #@42
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@49
    goto :goto_3a
.end method

.method private handleGlobalMode(ZZ)V
    .registers 6
    .parameter "oldUiccState"
    .parameter "newUiccState"

    #@0
    .prologue
    const v2, 0x2090258

    #@3
    .line 775
    const-string v1, "handleNetworkMode() : Global Mode"

    #@5
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@8
    .line 776
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@b
    move-result-object v0

    #@c
    .line 777
    .local v0, r:Landroid/content/res/Resources;
    if-eqz p1, :cond_26

    #@e
    .line 778
    if-eqz p2, :cond_1a

    #@10
    .line 780
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isNetworkModeDisplayed:Z

    #@12
    if-eqz v1, :cond_19

    #@14
    const-string v1, "Keep Global Mode"

    #@16
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@19
    .line 800
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 783
    :cond_1a
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@1c
    if-eqz v1, :cond_19

    #@1e
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@25
    goto :goto_19

    #@26
    .line 788
    :cond_26
    if-eqz p2, :cond_3b

    #@28
    .line 790
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@2a
    if-eqz v1, :cond_36

    #@2c
    const v1, 0x2090259

    #@2f
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@36
    .line 792
    :cond_36
    const/4 v1, 0x1

    #@37
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->createNotePopup(I)V

    #@3a
    goto :goto_19

    #@3b
    .line 795
    :cond_3b
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@3d
    if-eqz v1, :cond_19

    #@3f
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@46
    goto :goto_19
.end method

.method private handleLteCdmaMode(ZZ)V
    .registers 6
    .parameter "oldUiccState"
    .parameter "newUiccState"

    #@0
    .prologue
    const v2, 0x2090258

    #@3
    .line 870
    const-string v1, "handleNetworkMode() : LTE/CDMA"

    #@5
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@8
    .line 871
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@b
    move-result-object v0

    #@c
    .line 872
    .local v0, r:Landroid/content/res/Resources;
    if-eqz p1, :cond_26

    #@e
    .line 873
    if-eqz p2, :cond_1a

    #@10
    .line 874
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isNetworkModeDisplayed:Z

    #@12
    if-eqz v1, :cond_19

    #@14
    const-string v1, "Keep LTE/CDMA Mode"

    #@16
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@19
    .line 897
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 877
    :cond_1a
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@1c
    if-eqz v1, :cond_19

    #@1e
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@25
    goto :goto_19

    #@26
    .line 882
    :cond_26
    if-eqz p2, :cond_3e

    #@28
    .line 884
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@2a
    if-eqz v1, :cond_36

    #@2c
    const v1, 0x2090259

    #@2f
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@36
    .line 886
    :cond_36
    const/4 v1, 0x1

    #@37
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->createNotePopup(I)V

    #@3a
    .line 895
    :cond_3a
    :goto_3a
    invoke-virtual {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->switchToGlobalMode()V

    #@3d
    goto :goto_19

    #@3e
    .line 889
    :cond_3e
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@40
    if-eqz v1, :cond_3a

    #@42
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@49
    goto :goto_3a
.end method

.method private handleLteGsmUmtsMode(ZZ)V
    .registers 6
    .parameter "oldUiccState"
    .parameter "newUiccState"

    #@0
    .prologue
    const v2, 0x2090258

    #@3
    .line 803
    const-string v1, "handleNetworkMode() : LTE/GSM/UMTS"

    #@5
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@8
    .line 804
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@b
    move-result-object v0

    #@c
    .line 805
    .local v0, r:Landroid/content/res/Resources;
    if-eqz p1, :cond_38

    #@e
    .line 806
    if-eqz p2, :cond_2c

    #@10
    .line 808
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isNetworkModeDisplayed:Z

    #@12
    if-eqz v1, :cond_19

    #@14
    const-string v1, "Keep LTE/GSM/UMTS"

    #@16
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@19
    .line 810
    :cond_19
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualMode:Z

    #@1b
    if-nez v1, :cond_23

    #@1d
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualSelection()Z

    #@20
    move-result v1

    #@21
    if-eqz v1, :cond_2b

    #@23
    .line 811
    :cond_23
    const-string v1, "Query PLMN List"

    #@25
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@28
    .line 812
    invoke-virtual {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendQueryAvailableNetworks()V

    #@2b
    .line 836
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 816
    :cond_2c
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@2e
    if-eqz v1, :cond_2b

    #@30
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@37
    goto :goto_2b

    #@38
    .line 821
    :cond_38
    if-eqz p2, :cond_50

    #@3a
    .line 823
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@3c
    if-eqz v1, :cond_48

    #@3e
    const v1, 0x2090259

    #@41
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@48
    .line 825
    :cond_48
    const/4 v1, 0x1

    #@49
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->createNotePopup(I)V

    #@4c
    .line 834
    :cond_4c
    :goto_4c
    invoke-virtual {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->switchToGlobalMode()V

    #@4f
    goto :goto_2b

    #@50
    .line 828
    :cond_50
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isToastDisplayed:Z

    #@52
    if-eqz v1, :cond_4c

    #@54
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@57
    move-result-object v1

    #@58
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@5b
    goto :goto_4c
.end method

.method private handleNetworkMode(Z)V
    .registers 9
    .parameter "newUiccState"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 737
    iget-object v5, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@4
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@7
    move-result-object v5

    #@8
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v5

    #@c
    const-string v6, "old_uicc_state"

    #@e
    invoke-static {v5, v6, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11
    move-result v0

    #@12
    .line 741
    .local v0, hasUicc:I
    if-ne v0, v3, :cond_53

    #@14
    move v1, v3

    #@15
    .line 742
    .local v1, oldUiccState:Z
    :goto_15
    new-instance v5, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v6, "handleNetworkMode() : oldUiccState = "

    #@1c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v5

    #@20
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    const-string v6, " newUiccState = "

    #@26
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@35
    .line 743
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->getSystemSelection()I

    #@38
    move-result v2

    #@39
    .line 745
    .local v2, systemSelection:I
    packed-switch v2, :pswitch_data_68

    #@3c
    .line 763
    const-string v5, "handelNetworkmode() : Not supported"

    #@3e
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/gfit/GfitUtils;->loge(Ljava/lang/String;)V

    #@41
    .line 767
    :goto_41
    iget-object v5, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@43
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4a
    move-result-object v5

    #@4b
    const-string v6, "old_uicc_state"

    #@4d
    if-nez p1, :cond_65

    #@4f
    :goto_4f
    invoke-static {v5, v6, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@52
    .line 772
    return-void

    #@53
    .end local v1           #oldUiccState:Z
    .end local v2           #systemSelection:I
    :cond_53
    move v1, v4

    #@54
    .line 741
    goto :goto_15

    #@55
    .line 747
    .restart local v1       #oldUiccState:Z
    .restart local v2       #systemSelection:I
    :pswitch_55
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/gfit/GfitUtils;->handleGlobalMode(ZZ)V

    #@58
    goto :goto_41

    #@59
    .line 751
    :pswitch_59
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/gfit/GfitUtils;->handleLteGsmUmtsMode(ZZ)V

    #@5c
    goto :goto_41

    #@5d
    .line 755
    :pswitch_5d
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/gfit/GfitUtils;->handleCdmaOnlyMode(ZZ)V

    #@60
    goto :goto_41

    #@61
    .line 759
    :pswitch_61
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/gfit/GfitUtils;->handleLteCdmaMode(ZZ)V

    #@64
    goto :goto_41

    #@65
    :cond_65
    move v4, v3

    #@66
    .line 767
    goto :goto_4f

    #@67
    .line 745
    nop

    #@68
    :pswitch_data_68
    .packed-switch 0x0
        :pswitch_55
        :pswitch_59
        :pswitch_5d
        :pswitch_61
    .end packed-switch
.end method

.method private handleUiccStateChanged()V
    .registers 9

    #@0
    .prologue
    const/16 v7, 0xbb8

    #@2
    const/16 v6, 0xc9

    #@4
    const/4 v5, 0x1

    #@5
    const/4 v4, 0x0

    #@6
    .line 697
    sget-boolean v2, Lcom/android/internal/telephony/gfit/GfitUtils;->checkSIMState:Z

    #@8
    if-eqz v2, :cond_10

    #@a
    .line 698
    const-string v2, "skip uicc check"

    #@c
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@f
    .line 734
    :cond_f
    :goto_f
    return-void

    #@10
    .line 701
    :cond_10
    const-string v2, "check uicc check"

    #@12
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@15
    .line 704
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@17
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCard()Lcom/android/internal/telephony/uicc/UiccCard;

    #@1a
    move-result-object v0

    #@1b
    .line 705
    .local v0, mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@1d
    .line 707
    .local v1, state:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;
    if-eqz v0, :cond_f

    #@1f
    .line 708
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCard;->getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@22
    move-result-object v1

    #@23
    .line 709
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "Card State = "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@39
    .line 713
    invoke-virtual {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isUsedActionSIMStateChanged()Z

    #@3c
    move-result v2

    #@3d
    if-nez v2, :cond_f

    #@3f
    .line 714
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@41
    if-ne v1, v2, :cond_51

    #@43
    .line 715
    sput-boolean v5, Lcom/android/internal/telephony/gfit/GfitUtils;->checkSIMState:Z

    #@45
    .line 716
    const-string v2, "UICC is absent.. Display SIM state after 3000 mSec"

    #@47
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@4a
    .line 717
    invoke-direct {p0, v6, v4, v4, v7}, Lcom/android/internal/telephony/gfit/GfitUtils;->triggerEventAfterTimeout(IIII)V

    #@4d
    .line 721
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendQuerySystemModeAfterBoot()V

    #@50
    goto :goto_f

    #@51
    .line 723
    :cond_51
    sput-boolean v5, Lcom/android/internal/telephony/gfit/GfitUtils;->checkSIMState:Z

    #@53
    .line 724
    const-string v2, "UICC is ready.. Display SIM state after 3000 mSec"

    #@55
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@58
    .line 725
    invoke-direct {p0, v6, v5, v4, v7}, Lcom/android/internal/telephony/gfit/GfitUtils;->triggerEventAfterTimeout(IIII)V

    #@5b
    .line 729
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendQuerySystemModeAfterBoot()V

    #@5e
    goto :goto_f
.end method

.method private isManualSearchingInSystemSelect()Z
    .registers 3

    #@0
    .prologue
    .line 1284
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "isManualSearching = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-boolean v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualSearching:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@18
    .line 1285
    iget-boolean v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualSearching:Z

    #@1a
    return v0
.end method

.method private isManualSelection()Z
    .registers 4

    #@0
    .prologue
    .line 997
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->sst:Lcom/android/internal/telephony/ServiceStateTracker;

    #@2
    iget-object v1, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@4
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getIsManualSelection()Z

    #@7
    move-result v0

    #@8
    .line 998
    .local v0, isManual:Z
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "isManualSelection = "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@1e
    .line 999
    return v0
.end method

.method private isPopupDisabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 1251
    iget v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mFTMFlag:I

    #@4
    if-nez v3, :cond_1e

    #@6
    .line 1252
    const-string v3, "ro.factorytest"

    #@8
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 1253
    .local v0, factoryTestStr:Ljava/lang/String;
    if-eqz v0, :cond_19

    #@e
    const-string v3, "2"

    #@10
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_19

    #@16
    .line 1254
    iput v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mFTMFlag:I

    #@18
    .line 1262
    .end local v0           #factoryTestStr:Ljava/lang/String;
    :goto_18
    return v1

    #@19
    .line 1257
    .restart local v0       #factoryTestStr:Ljava/lang/String;
    :cond_19
    const/4 v1, 0x2

    #@1a
    iput v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mFTMFlag:I

    #@1c
    .end local v0           #factoryTestStr:Ljava/lang/String;
    :cond_1c
    move v1, v2

    #@1d
    .line 1262
    goto :goto_18

    #@1e
    .line 1259
    :cond_1e
    iget v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mFTMFlag:I

    #@20
    if-ne v3, v2, :cond_1c

    #@22
    goto :goto_18
.end method

.method private isSetupWizard()Z
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1267
    iget-object v4, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@3
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@6
    move-result-object v4

    #@7
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@a
    move-result-object v1

    #@b
    .line 1268
    .local v1, pm:Landroid/content/pm/PackageManager;
    const-string v2, "com.android.LGSetupWizard"

    #@d
    .line 1271
    .local v2, setupWizard:Ljava/lang/String;
    :try_start_d
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_10} :catch_16

    #@10
    move-result v4

    #@11
    const/4 v5, 0x2

    #@12
    if-eq v4, v5, :cond_15

    #@14
    .line 1272
    const/4 v3, 0x1

    #@15
    .line 1279
    :cond_15
    :goto_15
    return v3

    #@16
    .line 1276
    :catch_16
    move-exception v0

    #@17
    .line 1277
    .local v0, e:Ljava/lang/Exception;
    sget-object v4, Lcom/android/internal/telephony/gfit/GfitUtils;->LOG_TAG:Ljava/lang/String;

    #@19
    const-string v5, "isSetupwizard"

    #@1b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    goto :goto_15
.end method

.method private log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 1306
    const-string v0, "GSMCDMA"

    #@2
    .line 1307
    .local v0, TAG:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getPhoneName()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    const-string v2, "GSM"

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_29

    #@10
    .line 1308
    const-string v0, "GSM"

    #@12
    .line 1312
    :cond_12
    :goto_12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v2, "[GFIT] "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1313
    return-void

    #@29
    .line 1309
    :cond_29
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@2b
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getPhoneName()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    const-string v2, "CDMA"

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_12

    #@37
    .line 1310
    const-string v0, "CDMA"

    #@39
    goto :goto_12
.end method

.method private loge(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 1302
    sget-object v0, Lcom/android/internal/telephony/gfit/GfitUtils;->LOG_TAG:Ljava/lang/String;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[GFIT] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1303
    return-void
.end method

.method private selectPlmnDialog(Ljava/util/ArrayList;)V
    .registers 14
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/OperatorInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/OperatorInfo;>;"
    const/4 v11, 0x3

    #@1
    .line 1009
    const/4 v3, 0x0

    #@2
    .line 1010
    .local v3, nPlmnListNum:I
    const/4 v2, 0x0

    #@3
    .line 1011
    .local v2, nInfoIndex:I
    const/4 v4, 0x0

    #@4
    .line 1013
    .local v4, nRatNum:I
    if-nez p1, :cond_c

    #@6
    .line 1014
    const-string v8, "Fail to display PLMN Dialog"

    #@8
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@b
    .line 1060
    :goto_b
    return-void

    #@c
    .line 1017
    :cond_c
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v8

    #@10
    new-array v7, v8, [Ljava/lang/CharSequence;

    #@12
    .line 1019
    .local v7, plmnDialog:[Ljava/lang/CharSequence;
    new-instance v8, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v9, "selectPlmnDialog : result = "

    #@19
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v8

    #@1d
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v8

    #@21
    const-string v9, " size = "

    #@23
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v8

    #@27
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@2a
    move-result v9

    #@2b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v8

    #@2f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v8

    #@33
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@36
    .line 1020
    if-eqz p1, :cond_186

    #@38
    .line 1021
    const/4 v0, 0x0

    #@39
    .local v0, i:I
    :goto_39
    const/16 v8, 0x14

    #@3b
    if-ge v0, v8, :cond_49

    #@3d
    .line 1022
    iget-object v8, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@3f
    new-instance v9, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@41
    invoke-direct {v9, p0}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;-><init>(Lcom/android/internal/telephony/gfit/GfitUtils;)V

    #@44
    aput-object v9, v8, v0

    #@46
    .line 1021
    add-int/lit8 v0, v0, 0x1

    #@48
    goto :goto_39

    #@49
    .line 1025
    :cond_49
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@4c
    move-result-object v1

    #@4d
    .local v1, i$:Ljava/util/Iterator;
    :goto_4d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@50
    move-result v8

    #@51
    if-eqz v8, :cond_18b

    #@53
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@56
    move-result-object v5

    #@57
    check-cast v5, Lcom/android/internal/telephony/OperatorInfo;

    #@59
    .line 1027
    .local v5, ni:Lcom/android/internal/telephony/OperatorInfo;
    iget-object v8, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@5b
    aget-object v8, v8, v3

    #@5d
    invoke-virtual {v5}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    #@60
    move-result-object v9

    #@61
    const/4 v10, 0x0

    #@62
    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@65
    move-result-object v9

    #@66
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@69
    move-result v9

    #@6a
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->setMCC(I)V

    #@6d
    .line 1028
    iget-object v8, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@6f
    aget-object v8, v8, v3

    #@71
    invoke-virtual {v5}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    #@74
    move-result-object v9

    #@75
    invoke-virtual {v5}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    #@78
    move-result-object v10

    #@79
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@7c
    move-result v10

    #@7d
    invoke-virtual {v9, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@80
    move-result-object v9

    #@81
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@84
    move-result v9

    #@85
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->setMNC(I)V

    #@88
    .line 1029
    iget-object v8, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@8a
    aget-object v8, v8, v3

    #@8c
    invoke-virtual {v5}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorRAT()Ljava/lang/String;

    #@8f
    move-result-object v9

    #@90
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->setRAT(Ljava/lang/String;)V

    #@93
    .line 1030
    iget-object v8, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@95
    aget-object v8, v8, v3

    #@97
    invoke-virtual {v5}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    #@9a
    move-result-object v9

    #@9b
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->setOperatorNumeric(Ljava/lang/String;)V

    #@9e
    .line 1031
    iget-object v8, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@a0
    aget-object v8, v8, v3

    #@a2
    invoke-virtual {v5}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaLong()Ljava/lang/String;

    #@a5
    move-result-object v9

    #@a6
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->setOperatorAlphaLong(Ljava/lang/String;)V

    #@a9
    .line 1033
    new-instance v8, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    const-string v9, "["

    #@b0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v8

    #@b4
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v8

    #@b8
    const-string v9, "] "

    #@ba
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v8

    #@be
    const-string v9, "MCC = "

    #@c0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v8

    #@c4
    iget-object v9, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@c6
    aget-object v9, v9, v3

    #@c8
    invoke-virtual {v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->getMCC()I

    #@cb
    move-result v9

    #@cc
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v8

    #@d0
    const-string v9, " MNC = "

    #@d2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v8

    #@d6
    iget-object v9, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@d8
    aget-object v9, v9, v3

    #@da
    invoke-virtual {v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->getMNC()I

    #@dd
    move-result v9

    #@de
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v8

    #@e2
    const-string v9, " RAT = "

    #@e4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v8

    #@e8
    iget-object v9, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@ea
    aget-object v9, v9, v3

    #@ec
    invoke-virtual {v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->getRAT()Ljava/lang/String;

    #@ef
    move-result-object v9

    #@f0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v8

    #@f4
    const-string v9, " OperatorNemeric = "

    #@f6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v8

    #@fa
    iget-object v9, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@fc
    aget-object v9, v9, v3

    #@fe
    invoke-virtual {v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->getOperatorNumeric()Ljava/lang/String;

    #@101
    move-result-object v9

    #@102
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v8

    #@106
    const-string v9, " OperatorAlphaLong = "

    #@108
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v8

    #@10c
    iget-object v9, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@10e
    aget-object v9, v9, v3

    #@110
    invoke-virtual {v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->getOperatorAlphaLong()Ljava/lang/String;

    #@113
    move-result-object v9

    #@114
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v8

    #@118
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11b
    move-result-object v6

    #@11c
    .line 1040
    .local v6, plmn:Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@11f
    .line 1042
    invoke-virtual {v5}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaLong()Ljava/lang/String;

    #@122
    move-result-object v8

    #@123
    const-string v9, ""

    #@125
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@128
    move-result v8

    #@129
    if-eqz v8, :cond_15a

    #@12b
    .line 1045
    new-instance v8, Ljava/lang/StringBuilder;

    #@12d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@130
    invoke-virtual {v5}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    #@133
    move-result-object v9

    #@134
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v8

    #@138
    const-string v9, " ("

    #@13a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v8

    #@13e
    iget-object v9, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@140
    aget-object v9, v9, v3

    #@142
    invoke-virtual {v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->getRAT()Ljava/lang/String;

    #@145
    move-result-object v9

    #@146
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v8

    #@14a
    const-string v9, ")"

    #@14c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v8

    #@150
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@153
    move-result-object v8

    #@154
    aput-object v8, v7, v3

    #@156
    .line 1051
    :goto_156
    add-int/lit8 v3, v3, 0x1

    #@158
    .line 1052
    goto/16 :goto_4d

    #@15a
    .line 1048
    :cond_15a
    new-instance v8, Ljava/lang/StringBuilder;

    #@15c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@15f
    invoke-virtual {v5}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaLong()Ljava/lang/String;

    #@162
    move-result-object v9

    #@163
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v8

    #@167
    const-string v9, " ("

    #@169
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v8

    #@16d
    iget-object v9, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@16f
    aget-object v9, v9, v3

    #@171
    invoke-virtual {v9}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->getRAT()Ljava/lang/String;

    #@174
    move-result-object v9

    #@175
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@178
    move-result-object v8

    #@179
    const-string v9, ")"

    #@17b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v8

    #@17f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@182
    move-result-object v8

    #@183
    aput-object v8, v7, v3

    #@185
    goto :goto_156

    #@186
    .line 1055
    .end local v0           #i:I
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v5           #ni:Lcom/android/internal/telephony/OperatorInfo;
    .end local v6           #plmn:Ljava/lang/String;
    :cond_186
    const-string v8, "selectPlmnDialg() : no Plmn list"

    #@188
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gfit/GfitUtils;->loge(Ljava/lang/String;)V

    #@18b
    .line 1059
    :cond_18b
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/gfit/GfitUtils;->createPlmnListDialog([Ljava/lang/CharSequence;)V

    #@18e
    goto/16 :goto_b
.end method

.method private sendPreferredNetworkType(I)V
    .registers 5
    .parameter "networkType"

    #@0
    .prologue
    .line 949
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "sendPreferredNetworkType() : networkType = "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@16
    .line 950
    const/16 v1, 0xcb

    #@18
    const/4 v2, 0x0

    #@19
    invoke-virtual {p0, v1, p1, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->obtainMessage(III)Landroid/os/Message;

    #@1c
    move-result-object v0

    #@1d
    .line 951
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1f
    invoke-interface {v1, p1, v0}, Lcom/android/internal/telephony/CommandsInterface;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@22
    .line 952
    return-void
.end method

.method private sendQuerySystemModeAfterBoot()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 924
    const-string v1, "sendQuerySystemModeAfterBoot()"

    #@3
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@6
    .line 925
    const/16 v1, 0xcd

    #@8
    invoke-virtual {p0, v1, v2, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->obtainMessage(III)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    .line 926
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    invoke-interface {v1, v0}, Lcom/android/internal/telephony/CommandsInterface;->getNetworkSelectionMode(Landroid/os/Message;)V

    #@11
    .line 927
    return-void
.end method

.method private sendTriggerNoServiceChanged()V
    .registers 2

    #@0
    .prologue
    .line 992
    const-string v0, "sendTriggerNoServiceChanged()"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@5
    .line 993
    const/16 v0, 0x66

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendEmptyMessage(I)Z

    #@a
    .line 994
    return-void
.end method

.method private showToast(Ljava/lang/String;)V
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 663
    return-void
.end method

.method private triggerEventAfterTimeout(IIII)V
    .registers 8
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "timeout"

    #@0
    .prologue
    .line 651
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/telephony/gfit/GfitUtils;->obtainMessage(III)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 652
    .local v0, msg:Landroid/os/Message;
    int-to-long v1, p4

    #@5
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@8
    .line 653
    return-void
.end method


# virtual methods
.method public dispose()V
    .registers 6

    #@0
    .prologue
    const/16 v4, 0xc9

    #@2
    const/16 v3, 0xc8

    #@4
    .line 598
    const-string v1, "dispose GfitUtils..."

    #@6
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@9
    .line 599
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->sst:Lcom/android/internal/telephony/ServiceStateTracker;

    #@b
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/ServiceStateTracker;->unregisterForNoServiceChanged(Landroid/os/Handler;)V

    #@e
    .line 600
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->sst:Lcom/android/internal/telephony/ServiceStateTracker;

    #@10
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/ServiceStateTracker;->unregisterForNetworkAttached(Landroid/os/Handler;)V

    #@13
    .line 601
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@15
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/UiccController;->unregisterForIccChanged(Landroid/os/Handler;)V

    #@18
    .line 602
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1a
    invoke-interface {v1, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForStartQueryAvailableNetwork(Landroid/os/Handler;)V

    #@1d
    .line 603
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1f
    invoke-interface {v1, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForEndQueryAvailableNetwork(Landroid/os/Handler;)V

    #@22
    .line 604
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@24
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/PhoneBase;->unregisterForSetPreferredNetworkType(Landroid/os/Handler;)V

    #@27
    .line 605
    invoke-virtual {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isUsedActionSIMStateChanged()Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_3c

    #@2d
    .line 606
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mGfitIntentReceiver:Landroid/content/BroadcastReceiver;

    #@2f
    if-eqz v1, :cond_3c

    #@31
    .line 608
    :try_start_31
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@33
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@36
    move-result-object v1

    #@37
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->mGfitIntentReceiver:Landroid/content/BroadcastReceiver;

    #@39
    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_3c} :catch_59

    #@3c
    .line 617
    :cond_3c
    :goto_3c
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->hasMessages(I)Z

    #@3f
    move-result v1

    #@40
    if-eqz v1, :cond_4a

    #@42
    .line 618
    const-string v1, "dispose - remove EVENT_GFIT_POPUP_SWTICH_TO_GLOBAL_MODE msg"

    #@44
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@47
    .line 619
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->removeMessages(I)V

    #@4a
    .line 622
    :cond_4a
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/gfit/GfitUtils;->hasMessages(I)Z

    #@4d
    move-result v1

    #@4e
    if-eqz v1, :cond_58

    #@50
    .line 623
    const-string v1, "dispose - remove EVENT_GFIT_HANDLE_NETWORK_MODE_AFTER_DELAY msg"

    #@52
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@55
    .line 624
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/gfit/GfitUtils;->removeMessages(I)V

    #@58
    .line 626
    :cond_58
    return-void

    #@59
    .line 609
    :catch_59
    move-exception v0

    #@5a
    .line 610
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v2, "mGfitIntentReceiver unregisterReceiver - Exception Msg: "

    #@61
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v1

    #@65
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@68
    move-result-object v2

    #@69
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v1

    #@6d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v1

    #@71
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@74
    goto :goto_3c
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 23
    .parameter "msg"

    #@0
    .prologue
    .line 283
    new-instance v17, Ljava/lang/StringBuilder;

    #@2
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v18, "receive "

    #@7
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v17

    #@b
    move-object/from16 v0, p1

    #@d
    iget v0, v0, Landroid/os/Message;->what:I

    #@f
    move/from16 v18, v0

    #@11
    move-object/from16 v0, p0

    #@13
    move/from16 v1, v18

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->eventToString(I)Ljava/lang/String;

    #@18
    move-result-object v18

    #@19
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v17

    #@1d
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v17

    #@21
    move-object/from16 v0, p0

    #@23
    move-object/from16 v1, v17

    #@25
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@28
    .line 284
    move-object/from16 v0, p1

    #@2a
    iget v0, v0, Landroid/os/Message;->what:I

    #@2c
    move/from16 v17, v0

    #@2e
    sparse-switch v17, :sswitch_data_5f6

    #@31
    .line 563
    const-string v17, "Not supported"

    #@33
    move-object/from16 v0, p0

    #@35
    move-object/from16 v1, v17

    #@37
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->loge(Ljava/lang/String;)V

    #@3a
    .line 566
    :cond_3a
    :goto_3a
    return-void

    #@3b
    .line 287
    :sswitch_3b
    const-string v17, "No service"

    #@3d
    move-object/from16 v0, p0

    #@3f
    move-object/from16 v1, v17

    #@41
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@44
    .line 288
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->getServiceState()I

    #@47
    move-result v17

    #@48
    move/from16 v0, v17

    #@4a
    move-object/from16 v1, p0

    #@4c
    iput v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->mNewServiceState:I

    #@4e
    .line 289
    move-object/from16 v0, p0

    #@50
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mServiceState:I

    #@52
    move/from16 v17, v0

    #@54
    move-object/from16 v0, p0

    #@56
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mNewServiceState:I

    #@58
    move/from16 v18, v0

    #@5a
    move/from16 v0, v17

    #@5c
    move/from16 v1, v18

    #@5e
    if-eq v0, v1, :cond_fa

    #@60
    const/4 v9, 0x1

    #@61
    .line 291
    .local v9, hasChanged:Z
    :goto_61
    new-instance v17, Ljava/lang/StringBuilder;

    #@63
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v18, "hasChanged = "

    #@68
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v17

    #@6c
    move-object/from16 v0, v17

    #@6e
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@71
    move-result-object v17

    #@72
    const-string v18, " mServiceState = "

    #@74
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v17

    #@78
    move-object/from16 v0, p0

    #@7a
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mServiceState:I

    #@7c
    move/from16 v18, v0

    #@7e
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81
    move-result-object v17

    #@82
    const-string v18, " mNewServiceState = "

    #@84
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v17

    #@88
    move-object/from16 v0, p0

    #@8a
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mNewServiceState:I

    #@8c
    move/from16 v18, v0

    #@8e
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@91
    move-result-object v17

    #@92
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v17

    #@96
    move-object/from16 v0, p0

    #@98
    move-object/from16 v1, v17

    #@9a
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@9d
    .line 294
    move-object/from16 v0, p0

    #@9f
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mNewServiceState:I

    #@a1
    move/from16 v17, v0

    #@a3
    move/from16 v0, v17

    #@a5
    move-object/from16 v1, p0

    #@a7
    iput v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->mServiceState:I

    #@a9
    .line 296
    if-eqz v9, :cond_3a

    #@ab
    .line 297
    move-object/from16 v0, p0

    #@ad
    iget-boolean v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mProcessingNoService:Z

    #@af
    move/from16 v17, v0

    #@b1
    if-nez v17, :cond_3a

    #@b3
    .line 298
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->getSystemSelection()I

    #@b6
    move-result v16

    #@b7
    .line 302
    .local v16, systemSelection:I
    if-eqz v16, :cond_3a

    #@b9
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualSelection()Z

    #@bc
    move-result v17

    #@bd
    if-nez v17, :cond_3a

    #@bf
    const-string v17, "persist.radio.airplane_mode_on"

    #@c1
    const/16 v18, 0x0

    #@c3
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@c6
    move-result v17

    #@c7
    if-nez v17, :cond_3a

    #@c9
    .line 304
    const-string v17, "Switch to Global Mode after 30 secs"

    #@cb
    move-object/from16 v0, p0

    #@cd
    move-object/from16 v1, v17

    #@cf
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@d2
    .line 305
    const-string v17, "Switch to Global Mode after 30 secs"

    #@d4
    move-object/from16 v0, p0

    #@d6
    move-object/from16 v1, v17

    #@d8
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@db
    .line 306
    const/16 v17, 0x1

    #@dd
    move/from16 v0, v17

    #@df
    move-object/from16 v1, p0

    #@e1
    iput-boolean v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->mProcessingNoService:Z

    #@e3
    .line 313
    const/16 v17, 0xc8

    #@e5
    const/16 v18, 0x0

    #@e7
    const/16 v19, 0x0

    #@e9
    const/16 v20, 0x7530

    #@eb
    move-object/from16 v0, p0

    #@ed
    move/from16 v1, v17

    #@ef
    move/from16 v2, v18

    #@f1
    move/from16 v3, v19

    #@f3
    move/from16 v4, v20

    #@f5
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/gfit/GfitUtils;->triggerEventAfterTimeout(IIII)V

    #@f8
    goto/16 :goto_3a

    #@fa
    .line 289
    .end local v9           #hasChanged:Z
    .end local v16           #systemSelection:I
    :cond_fa
    const/4 v9, 0x0

    #@fb
    goto/16 :goto_61

    #@fd
    .line 320
    :sswitch_fd
    const-string v17, "register to network"

    #@ff
    move-object/from16 v0, p0

    #@101
    move-object/from16 v1, v17

    #@103
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@106
    .line 321
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->getServiceState()I

    #@109
    move-result v17

    #@10a
    move/from16 v0, v17

    #@10c
    move-object/from16 v1, p0

    #@10e
    iput v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->mNewServiceState:I

    #@110
    .line 322
    move-object/from16 v0, p0

    #@112
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mServiceState:I

    #@114
    move/from16 v17, v0

    #@116
    move-object/from16 v0, p0

    #@118
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mNewServiceState:I

    #@11a
    move/from16 v18, v0

    #@11c
    move/from16 v0, v17

    #@11e
    move/from16 v1, v18

    #@120
    if-eq v0, v1, :cond_19e

    #@122
    const/4 v9, 0x1

    #@123
    .line 323
    .restart local v9       #hasChanged:Z
    :goto_123
    new-instance v17, Ljava/lang/StringBuilder;

    #@125
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v18, "hasChanged = "

    #@12a
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v17

    #@12e
    move-object/from16 v0, v17

    #@130
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@133
    move-result-object v17

    #@134
    const-string v18, " mServiceState = "

    #@136
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v17

    #@13a
    move-object/from16 v0, p0

    #@13c
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mServiceState:I

    #@13e
    move/from16 v18, v0

    #@140
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@143
    move-result-object v17

    #@144
    const-string v18, " mNewServiceState = "

    #@146
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v17

    #@14a
    move-object/from16 v0, p0

    #@14c
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mNewServiceState:I

    #@14e
    move/from16 v18, v0

    #@150
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@153
    move-result-object v17

    #@154
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@157
    move-result-object v17

    #@158
    move-object/from16 v0, p0

    #@15a
    move-object/from16 v1, v17

    #@15c
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@15f
    .line 326
    move-object/from16 v0, p0

    #@161
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mNewServiceState:I

    #@163
    move/from16 v17, v0

    #@165
    move/from16 v0, v17

    #@167
    move-object/from16 v1, p0

    #@169
    iput v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->mServiceState:I

    #@16b
    .line 328
    if-eqz v9, :cond_3a

    #@16d
    .line 329
    const/16 v17, 0xc8

    #@16f
    move-object/from16 v0, p0

    #@171
    move/from16 v1, v17

    #@173
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->hasMessages(I)Z

    #@176
    move-result v17

    #@177
    if-eqz v17, :cond_3a

    #@179
    .line 330
    const-string v17, "remove EVENT_GFIT_POPUP_SWTICH_TO_GLOBAL_MODE from MSG queue"

    #@17b
    move-object/from16 v0, p0

    #@17d
    move-object/from16 v1, v17

    #@17f
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@182
    .line 331
    const-string v17, "register to network.. cancel to set global mode"

    #@184
    move-object/from16 v0, p0

    #@186
    move-object/from16 v1, v17

    #@188
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@18b
    .line 332
    const/16 v17, 0xc8

    #@18d
    move-object/from16 v0, p0

    #@18f
    move/from16 v1, v17

    #@191
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->removeMessages(I)V

    #@194
    .line 333
    const/16 v17, 0x0

    #@196
    move/from16 v0, v17

    #@198
    move-object/from16 v1, p0

    #@19a
    iput-boolean v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->mProcessingNoService:Z

    #@19c
    goto/16 :goto_3a

    #@19e
    .line 322
    .end local v9           #hasChanged:Z
    :cond_19e
    const/4 v9, 0x0

    #@19f
    goto :goto_123

    #@1a0
    .line 342
    :sswitch_1a0
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->getServiceState()I

    #@1a3
    move-result v17

    #@1a4
    if-eqz v17, :cond_1c1

    #@1a6
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualSelection()Z

    #@1a9
    move-result v17

    #@1aa
    if-nez v17, :cond_1c1

    #@1ac
    .line 343
    move-object/from16 v0, p0

    #@1ae
    iget-boolean v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mProcessingNoService:Z

    #@1b0
    move/from16 v17, v0

    #@1b2
    if-eqz v17, :cond_1b7

    #@1b4
    .line 344
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->createNotePopupGlobal()V

    #@1b7
    .line 349
    :cond_1b7
    :goto_1b7
    const/16 v17, 0x0

    #@1b9
    move/from16 v0, v17

    #@1bb
    move-object/from16 v1, p0

    #@1bd
    iput-boolean v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->mProcessingNoService:Z

    #@1bf
    goto/16 :goto_3a

    #@1c1
    .line 347
    :cond_1c1
    const-string v17, "ignore EVENT_GFIT_POPUP_SWTICH_TO_GLOBAL_MODE"

    #@1c3
    move-object/from16 v0, p0

    #@1c5
    move-object/from16 v1, v17

    #@1c7
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@1ca
    goto :goto_1b7

    #@1cb
    .line 353
    :sswitch_1cb
    move-object/from16 v0, p1

    #@1cd
    iget v15, v0, Landroid/os/Message;->arg1:I

    #@1cf
    .line 357
    .local v15, simState:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isUsedActionSIMStateChanged()Z

    #@1d2
    move-result v17

    #@1d3
    if-eqz v17, :cond_1f0

    #@1d5
    .line 358
    move-object/from16 v0, p0

    #@1d7
    iget-object v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mGfitIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1d9
    move-object/from16 v17, v0

    #@1db
    if-eqz v17, :cond_1f0

    #@1dd
    .line 360
    :try_start_1dd
    move-object/from16 v0, p0

    #@1df
    iget-object v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@1e1
    move-object/from16 v17, v0

    #@1e3
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1e6
    move-result-object v17

    #@1e7
    move-object/from16 v0, p0

    #@1e9
    iget-object v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mGfitIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1eb
    move-object/from16 v18, v0

    #@1ed
    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1f0
    .catch Ljava/lang/Exception; {:try_start_1dd .. :try_end_1f0} :catch_212

    #@1f0
    .line 367
    :cond_1f0
    :goto_1f0
    if-nez v15, :cond_232

    #@1f2
    .line 368
    const-string v17, "receive EVENT_GFIT_HANDLE_NETWORK_MODE_AFTER_DELAY : simState = VZW_GFIT_ICC_ABSENT"

    #@1f4
    move-object/from16 v0, p0

    #@1f6
    move-object/from16 v1, v17

    #@1f8
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@1fb
    .line 369
    move-object/from16 v0, p0

    #@1fd
    iget-object v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@1ff
    move-object/from16 v17, v0

    #@201
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@204
    move-result-object v17

    #@205
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@208
    move-result-object v17

    #@209
    const-string v18, "old_uicc_state"

    #@20b
    const/16 v19, 0x0

    #@20d
    invoke-static/range {v17 .. v19}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@210
    goto/16 :goto_3a

    #@212
    .line 361
    :catch_212
    move-exception v8

    #@213
    .line 362
    .local v8, e:Ljava/lang/Exception;
    new-instance v17, Ljava/lang/StringBuilder;

    #@215
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@218
    const-string v18, "mGfitIntentReceiver unregisterReceiver - Exception Msg: "

    #@21a
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21d
    move-result-object v17

    #@21e
    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@221
    move-result-object v18

    #@222
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@225
    move-result-object v17

    #@226
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@229
    move-result-object v17

    #@22a
    move-object/from16 v0, p0

    #@22c
    move-object/from16 v1, v17

    #@22e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@231
    goto :goto_1f0

    #@232
    .line 373
    .end local v8           #e:Ljava/lang/Exception;
    :cond_232
    const/16 v17, 0x1

    #@234
    move/from16 v0, v17

    #@236
    if-ne v15, v0, :cond_3a

    #@238
    .line 374
    sget-boolean v17, Lcom/android/internal/telephony/gfit/GfitUtils;->isFirstDisplay:Z

    #@23a
    if-eqz v17, :cond_249

    #@23c
    .line 375
    const/16 v17, 0x1

    #@23e
    move-object/from16 v0, p0

    #@240
    move/from16 v1, v17

    #@242
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->handleNetworkMode(Z)V

    #@245
    .line 376
    const/16 v17, 0x0

    #@247
    sput-boolean v17, Lcom/android/internal/telephony/gfit/GfitUtils;->isFirstDisplay:Z

    #@249
    .line 380
    :cond_249
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->getServiceState()I

    #@24c
    move-result v17

    #@24d
    if-eqz v17, :cond_3a

    #@24f
    .line 382
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendTriggerNoServiceChanged()V

    #@252
    goto/16 :goto_3a

    #@254
    .line 388
    .end local v15           #simState:I
    :sswitch_254
    move-object/from16 v0, p1

    #@256
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@258
    check-cast v6, Landroid/os/AsyncResult;

    #@25a
    .line 389
    .local v6, ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@25c
    move-object/from16 v17, v0

    #@25e
    if-nez v17, :cond_273

    #@260
    .line 390
    const-string v17, "success to set preferred network type"

    #@262
    move-object/from16 v0, p0

    #@264
    move-object/from16 v1, v17

    #@266
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@269
    .line 391
    const/16 v17, 0x0

    #@26b
    move/from16 v0, v17

    #@26d
    move-object/from16 v1, p0

    #@26f
    iput v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryPrefferedNetworkType:I

    #@271
    goto/16 :goto_3a

    #@273
    .line 393
    :cond_273
    move-object/from16 v0, p1

    #@275
    iget v12, v0, Landroid/os/Message;->arg1:I

    #@277
    .line 394
    .local v12, preferredNetworkType:I
    move-object/from16 v0, p0

    #@279
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryPrefferedNetworkType:I

    #@27b
    move/from16 v17, v0

    #@27d
    add-int/lit8 v17, v17, 0x1

    #@27f
    move/from16 v0, v17

    #@281
    move-object/from16 v1, p0

    #@283
    iput v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryPrefferedNetworkType:I

    #@285
    .line 396
    new-instance v17, Ljava/lang/StringBuilder;

    #@287
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@28a
    const-string v18, "fail to set preferred network type.. retry to set preffered network type ("

    #@28c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28f
    move-result-object v17

    #@290
    move-object/from16 v0, p0

    #@292
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryPrefferedNetworkType:I

    #@294
    move/from16 v18, v0

    #@296
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@299
    move-result-object v17

    #@29a
    const-string v18, ")"

    #@29c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29f
    move-result-object v17

    #@2a0
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a3
    move-result-object v17

    #@2a4
    move-object/from16 v0, p0

    #@2a6
    move-object/from16 v1, v17

    #@2a8
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@2ab
    .line 397
    move-object/from16 v0, p0

    #@2ad
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryPrefferedNetworkType:I

    #@2af
    move/from16 v17, v0

    #@2b1
    const/16 v18, 0x5

    #@2b3
    move/from16 v0, v17

    #@2b5
    move/from16 v1, v18

    #@2b7
    if-ge v0, v1, :cond_2d1

    #@2b9
    .line 398
    move-object/from16 v0, p0

    #@2bb
    invoke-direct {v0, v12}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendPreferredNetworkType(I)V

    #@2be
    .line 399
    const/16 v17, 0xd2

    #@2c0
    const/16 v18, 0x0

    #@2c2
    const/16 v19, 0x1388

    #@2c4
    move-object/from16 v0, p0

    #@2c6
    move/from16 v1, v17

    #@2c8
    move/from16 v2, v18

    #@2ca
    move/from16 v3, v19

    #@2cc
    invoke-direct {v0, v1, v12, v2, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->triggerEventAfterTimeout(IIII)V

    #@2cf
    goto/16 :goto_3a

    #@2d1
    .line 404
    :cond_2d1
    const/16 v17, 0x0

    #@2d3
    move/from16 v0, v17

    #@2d5
    move-object/from16 v1, p0

    #@2d7
    iput v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryPrefferedNetworkType:I

    #@2d9
    goto/16 :goto_3a

    #@2db
    .line 410
    .end local v6           #ar:Landroid/os/AsyncResult;
    .end local v12           #preferredNetworkType:I
    :sswitch_2db
    move-object/from16 v0, p1

    #@2dd
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2df
    check-cast v6, Landroid/os/AsyncResult;

    #@2e1
    .line 411
    .restart local v6       #ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2e3
    move-object/from16 v17, v0

    #@2e5
    if-nez v17, :cond_364

    #@2e7
    .line 412
    iget-object v0, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2e9
    move-object/from16 v17, v0

    #@2eb
    check-cast v17, [I

    #@2ed
    move-object/from16 v10, v17

    #@2ef
    check-cast v10, [I

    #@2f1
    .line 413
    .local v10, ints:[I
    const/16 v17, 0x0

    #@2f3
    aget v17, v10, v17

    #@2f5
    const/16 v18, 0x1

    #@2f7
    move/from16 v0, v17

    #@2f9
    move/from16 v1, v18

    #@2fb
    if-ne v0, v1, :cond_35f

    #@2fd
    const/4 v7, 0x1

    #@2fe
    .line 414
    .local v7, currentNetworkSelectinMode:I
    :goto_2fe
    const/16 v17, 0x0

    #@300
    aget v17, v10, v17

    #@302
    const/16 v18, 0x1

    #@304
    move/from16 v0, v17

    #@306
    move/from16 v1, v18

    #@308
    if-ne v0, v1, :cond_361

    #@30a
    const/16 v17, 0x1

    #@30c
    :goto_30c
    move/from16 v0, v17

    #@30e
    move-object/from16 v1, p0

    #@310
    iput-boolean v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualMode:Z

    #@312
    .line 415
    move-object/from16 v0, p1

    #@314
    iget v14, v0, Landroid/os/Message;->arg1:I

    #@316
    .line 417
    .local v14, selectionMode:I
    new-instance v17, Ljava/lang/StringBuilder;

    #@318
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@31b
    const-string v18, "selectionMode = "

    #@31d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@320
    move-result-object v17

    #@321
    move-object/from16 v0, v17

    #@323
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@326
    move-result-object v17

    #@327
    const-string v18, ", currentNetworkSelectinMode = "

    #@329
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32c
    move-result-object v17

    #@32d
    move-object/from16 v0, v17

    #@32f
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@332
    move-result-object v17

    #@333
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@336
    move-result-object v17

    #@337
    move-object/from16 v0, p0

    #@339
    move-object/from16 v1, v17

    #@33b
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@33e
    .line 418
    if-eq v7, v14, :cond_3a

    #@340
    .line 419
    if-nez v7, :cond_34e

    #@342
    .line 420
    const-string v17, "setNetworkSelectionModeAutomatic()"

    #@344
    move-object/from16 v0, p0

    #@346
    move-object/from16 v1, v17

    #@348
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@34b
    .line 421
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendNetworkSelectionModeAutomatic()V

    #@34e
    .line 423
    :cond_34e
    const/16 v17, 0x1

    #@350
    move/from16 v0, v17

    #@352
    if-ne v7, v0, :cond_3a

    #@354
    .line 424
    const-string v17, "sendNetworkSelectionModeManual()"

    #@356
    move-object/from16 v0, p0

    #@358
    move-object/from16 v1, v17

    #@35a
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@35d
    goto/16 :goto_3a

    #@35f
    .line 413
    .end local v7           #currentNetworkSelectinMode:I
    .end local v14           #selectionMode:I
    :cond_35f
    const/4 v7, 0x0

    #@360
    goto :goto_2fe

    #@361
    .line 414
    .restart local v7       #currentNetworkSelectinMode:I
    :cond_361
    const/16 v17, 0x0

    #@363
    goto :goto_30c

    #@364
    .line 429
    .end local v7           #currentNetworkSelectinMode:I
    .end local v10           #ints:[I
    :cond_364
    const-string v17, "Fail to query network selection mode : EVENT_GFIT_QUERY_NETWORK_SELECTION_MODE"

    #@366
    move-object/from16 v0, p0

    #@368
    move-object/from16 v1, v17

    #@36a
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@36d
    goto/16 :goto_3a

    #@36f
    .line 434
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_36f
    move-object/from16 v0, p1

    #@371
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@373
    check-cast v6, Landroid/os/AsyncResult;

    #@375
    .line 435
    .restart local v6       #ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@377
    move-object/from16 v17, v0

    #@379
    if-nez v17, :cond_3be

    #@37b
    .line 436
    iget-object v0, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@37d
    move-object/from16 v17, v0

    #@37f
    check-cast v17, [I

    #@381
    move-object/from16 v10, v17

    #@383
    check-cast v10, [I

    #@385
    .line 437
    .restart local v10       #ints:[I
    const/16 v17, 0x0

    #@387
    aget v17, v10, v17

    #@389
    const/16 v18, 0x1

    #@38b
    move/from16 v0, v17

    #@38d
    move/from16 v1, v18

    #@38f
    if-ne v0, v1, :cond_3bb

    #@391
    const/16 v17, 0x1

    #@393
    :goto_393
    move/from16 v0, v17

    #@395
    move-object/from16 v1, p0

    #@397
    iput-boolean v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualMode:Z

    #@399
    .line 438
    new-instance v17, Ljava/lang/StringBuilder;

    #@39b
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@39e
    const-string v18, "isManualMode = "

    #@3a0
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a3
    move-result-object v17

    #@3a4
    move-object/from16 v0, p0

    #@3a6
    iget-boolean v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualMode:Z

    #@3a8
    move/from16 v18, v0

    #@3aa
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3ad
    move-result-object v17

    #@3ae
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b1
    move-result-object v17

    #@3b2
    move-object/from16 v0, p0

    #@3b4
    move-object/from16 v1, v17

    #@3b6
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@3b9
    goto/16 :goto_3a

    #@3bb
    .line 437
    :cond_3bb
    const/16 v17, 0x0

    #@3bd
    goto :goto_393

    #@3be
    .line 440
    .end local v10           #ints:[I
    :cond_3be
    const-string v17, "Fail to query network selection mode after boot : EVENT_GFIT_QUERY_NETWORK_SELECTION_MODE_AFTER_BOOT"

    #@3c0
    move-object/from16 v0, p0

    #@3c2
    move-object/from16 v1, v17

    #@3c4
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@3c7
    goto/16 :goto_3a

    #@3c9
    .line 445
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_3c9
    move-object/from16 v0, p1

    #@3cb
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3cd
    check-cast v6, Landroid/os/AsyncResult;

    #@3cf
    .line 446
    .restart local v6       #ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3d1
    move-object/from16 v17, v0

    #@3d3
    if-nez v17, :cond_3e0

    #@3d5
    .line 447
    const-string v17, "success to set network selection automatic"

    #@3d7
    move-object/from16 v0, p0

    #@3d9
    move-object/from16 v1, v17

    #@3db
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@3de
    goto/16 :goto_3a

    #@3e0
    .line 449
    :cond_3e0
    const-string v17, "Fail to set network selection automatic"

    #@3e2
    move-object/from16 v0, p0

    #@3e4
    move-object/from16 v1, v17

    #@3e6
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@3e9
    goto/16 :goto_3a

    #@3eb
    .line 454
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_3eb
    move-object/from16 v0, p1

    #@3ed
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3ef
    check-cast v6, Landroid/os/AsyncResult;

    #@3f1
    .line 455
    .restart local v6       #ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3f3
    move-object/from16 v17, v0

    #@3f5
    if-nez v17, :cond_402

    #@3f7
    .line 456
    const-string v17, "success to set network selection manual"

    #@3f9
    move-object/from16 v0, p0

    #@3fb
    move-object/from16 v1, v17

    #@3fd
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@400
    goto/16 :goto_3a

    #@402
    .line 458
    :cond_402
    const-string v17, "Fail to set network selection manual"

    #@404
    move-object/from16 v0, p0

    #@406
    move-object/from16 v1, v17

    #@408
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@40b
    goto/16 :goto_3a

    #@40d
    .line 463
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_40d
    move-object/from16 v0, p1

    #@40f
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@411
    check-cast v6, Landroid/os/AsyncResult;

    #@413
    .line 465
    .restart local v6       #ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@415
    move-object/from16 v17, v0

    #@417
    if-nez v17, :cond_42c

    #@419
    .line 466
    iget-object v13, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@41b
    check-cast v13, Ljava/util/ArrayList;

    #@41d
    .line 467
    .local v13, ret:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/OperatorInfo;>;"
    move-object/from16 v0, p0

    #@41f
    invoke-direct {v0, v13}, Lcom/android/internal/telephony/gfit/GfitUtils;->selectPlmnDialog(Ljava/util/ArrayList;)V

    #@422
    .line 468
    const/16 v17, 0x0

    #@424
    move/from16 v0, v17

    #@426
    move-object/from16 v1, p0

    #@428
    iput v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryQuertyAvailablenetworks:I

    #@42a
    goto/16 :goto_3a

    #@42c
    .line 470
    .end local v13           #ret:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/OperatorInfo;>;"
    :cond_42c
    move-object/from16 v0, p0

    #@42e
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryQuertyAvailablenetworks:I

    #@430
    move/from16 v17, v0

    #@432
    add-int/lit8 v17, v17, 0x1

    #@434
    move/from16 v0, v17

    #@436
    move-object/from16 v1, p0

    #@438
    iput v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryQuertyAvailablenetworks:I

    #@43a
    .line 471
    new-instance v17, Ljava/lang/StringBuilder;

    #@43c
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@43f
    const-string v18, "Fail to query available networks.. retry to query avaialbe networks ("

    #@441
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@444
    move-result-object v17

    #@445
    move-object/from16 v0, p0

    #@447
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryQuertyAvailablenetworks:I

    #@449
    move/from16 v18, v0

    #@44b
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44e
    move-result-object v17

    #@44f
    const-string v18, ")"

    #@451
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@454
    move-result-object v17

    #@455
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@458
    move-result-object v17

    #@459
    move-object/from16 v0, p0

    #@45b
    move-object/from16 v1, v17

    #@45d
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@460
    .line 472
    move-object/from16 v0, p0

    #@462
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryQuertyAvailablenetworks:I

    #@464
    move/from16 v17, v0

    #@466
    const/16 v18, 0xa

    #@468
    move/from16 v0, v17

    #@46a
    move/from16 v1, v18

    #@46c
    if-ge v0, v1, :cond_485

    #@46e
    .line 473
    const/16 v17, 0xd3

    #@470
    const/16 v18, 0x0

    #@472
    const/16 v19, 0x0

    #@474
    const/16 v20, 0x2710

    #@476
    move-object/from16 v0, p0

    #@478
    move/from16 v1, v17

    #@47a
    move/from16 v2, v18

    #@47c
    move/from16 v3, v19

    #@47e
    move/from16 v4, v20

    #@480
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/gfit/GfitUtils;->triggerEventAfterTimeout(IIII)V

    #@483
    goto/16 :goto_3a

    #@485
    .line 478
    :cond_485
    const/16 v17, 0x0

    #@487
    move/from16 v0, v17

    #@489
    move-object/from16 v1, p0

    #@48b
    iput v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->nRetryQuertyAvailablenetworks:I

    #@48d
    goto/16 :goto_3a

    #@48f
    .line 484
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_48f
    move-object/from16 v0, p0

    #@491
    iget-object v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->plmnListDialog:Landroid/app/AlertDialog;

    #@493
    move-object/from16 v17, v0

    #@495
    if-eqz v17, :cond_3a

    #@497
    .line 485
    const-string v17, "set automatic mode"

    #@499
    move-object/from16 v0, p0

    #@49b
    move-object/from16 v1, v17

    #@49d
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@4a0
    .line 486
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendNetworkSelectionModeAutomatic()V

    #@4a3
    .line 487
    move-object/from16 v0, p0

    #@4a5
    iget-object v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->plmnListDialog:Landroid/app/AlertDialog;

    #@4a7
    move-object/from16 v17, v0

    #@4a9
    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog;->dismiss()V

    #@4ac
    .line 488
    const-string v17, "Set Automatic Mode now"

    #@4ae
    move-object/from16 v0, p0

    #@4b0
    move-object/from16 v1, v17

    #@4b2
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@4b5
    goto/16 :goto_3a

    #@4b7
    .line 493
    :sswitch_4b7
    move-object/from16 v0, p1

    #@4b9
    iget v12, v0, Landroid/os/Message;->arg1:I

    #@4bb
    .line 494
    .restart local v12       #preferredNetworkType:I
    move-object/from16 v0, p0

    #@4bd
    invoke-direct {v0, v12}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendPreferredNetworkType(I)V

    #@4c0
    goto/16 :goto_3a

    #@4c2
    .line 498
    .end local v12           #preferredNetworkType:I
    :sswitch_4c2
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendQueryAvailableNetworks()V

    #@4c5
    goto/16 :goto_3a

    #@4c7
    .line 502
    :sswitch_4c7
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->handleUiccStateChanged()V

    #@4ca
    goto/16 :goto_3a

    #@4cc
    .line 506
    :sswitch_4cc
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->count()V

    #@4cf
    goto/16 :goto_3a

    #@4d1
    .line 510
    :sswitch_4d1
    const/16 v17, 0x1

    #@4d3
    move/from16 v0, v17

    #@4d5
    move-object/from16 v1, p0

    #@4d7
    iput-boolean v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualSearching:Z

    #@4d9
    .line 511
    const/16 v17, 0xc8

    #@4db
    move-object/from16 v0, p0

    #@4dd
    move/from16 v1, v17

    #@4df
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->hasMessages(I)Z

    #@4e2
    move-result v17

    #@4e3
    if-eqz v17, :cond_3a

    #@4e5
    .line 512
    const-string v17, "Start querying Available networks... cancel popup to switch to global mode"

    #@4e7
    move-object/from16 v0, p0

    #@4e9
    move-object/from16 v1, v17

    #@4eb
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@4ee
    .line 513
    const/16 v17, 0xc8

    #@4f0
    move-object/from16 v0, p0

    #@4f2
    move/from16 v1, v17

    #@4f4
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->removeMessages(I)V

    #@4f7
    goto/16 :goto_3a

    #@4f9
    .line 518
    :sswitch_4f9
    const/16 v17, 0x0

    #@4fb
    move/from16 v0, v17

    #@4fd
    move-object/from16 v1, p0

    #@4ff
    iput-boolean v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualSearching:Z

    #@501
    goto/16 :goto_3a

    #@503
    .line 522
    :sswitch_503
    const-string v17, "Preferred network type is changed"

    #@505
    move-object/from16 v0, p0

    #@507
    move-object/from16 v1, v17

    #@509
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@50c
    .line 523
    move-object/from16 v0, p0

    #@50e
    iget-object v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@510
    move-object/from16 v17, v0

    #@512
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@515
    move-result-object v17

    #@516
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@519
    move-result-object v17

    #@51a
    const-string v18, "preferred_network_mode"

    #@51c
    const/16 v19, 0xa

    #@51e
    invoke-static/range {v17 .. v19}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@521
    move-result v5

    #@522
    .line 528
    .local v5, NetworkModeChange:I
    move-object/from16 v0, p0

    #@524
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mNetworkMode:I

    #@526
    move/from16 v17, v0

    #@528
    move/from16 v0, v17

    #@52a
    if-eq v0, v5, :cond_5cc

    #@52c
    const/4 v11, 0x1

    #@52d
    .line 529
    .local v11, isNetworkModeChanged:Z
    :goto_52d
    new-instance v17, Ljava/lang/StringBuilder;

    #@52f
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@532
    const-string v18, "isNetworkModeChanged = "

    #@534
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@537
    move-result-object v17

    #@538
    move-object/from16 v0, v17

    #@53a
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@53d
    move-result-object v17

    #@53e
    const-string v18, " NetworkModeChange = "

    #@540
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@543
    move-result-object v17

    #@544
    move-object/from16 v0, v17

    #@546
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@549
    move-result-object v17

    #@54a
    const-string v18, " mNetworkMode = "

    #@54c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54f
    move-result-object v17

    #@550
    move-object/from16 v0, p0

    #@552
    iget v0, v0, Lcom/android/internal/telephony/gfit/GfitUtils;->mNetworkMode:I

    #@554
    move/from16 v18, v0

    #@556
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@559
    move-result-object v17

    #@55a
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55d
    move-result-object v17

    #@55e
    move-object/from16 v0, p0

    #@560
    move-object/from16 v1, v17

    #@562
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@565
    .line 534
    if-eqz v11, :cond_3a

    #@567
    .line 535
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->getSystemSelection()I

    #@56a
    move-result v17

    #@56b
    if-eqz v17, :cond_5cf

    #@56d
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualSelection()Z

    #@570
    move-result v17

    #@571
    if-nez v17, :cond_5cf

    #@573
    const-string v17, "persist.radio.airplane_mode_on"

    #@575
    const/16 v18, 0x0

    #@577
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@57a
    move-result v17

    #@57b
    if-nez v17, :cond_5cf

    #@57d
    .line 538
    const-string v17, "Switch to Global Mode after 30 secs"

    #@57f
    move-object/from16 v0, p0

    #@581
    move-object/from16 v1, v17

    #@583
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@586
    .line 539
    const-string v17, "Switch to Global Mode after 30 secs"

    #@588
    move-object/from16 v0, p0

    #@58a
    move-object/from16 v1, v17

    #@58c
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->showToast(Ljava/lang/String;)V

    #@58f
    .line 540
    const/16 v17, 0x1

    #@591
    move/from16 v0, v17

    #@593
    move-object/from16 v1, p0

    #@595
    iput-boolean v0, v1, Lcom/android/internal/telephony/gfit/GfitUtils;->mProcessingNoService:Z

    #@597
    .line 542
    const/16 v17, 0xc8

    #@599
    move-object/from16 v0, p0

    #@59b
    move/from16 v1, v17

    #@59d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->hasMessages(I)Z

    #@5a0
    move-result v17

    #@5a1
    if-eqz v17, :cond_5b5

    #@5a3
    .line 543
    const-string v17, "Reset time expiry... restart to display Global mode after 30 sec"

    #@5a5
    move-object/from16 v0, p0

    #@5a7
    move-object/from16 v1, v17

    #@5a9
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@5ac
    .line 544
    const/16 v17, 0xc8

    #@5ae
    move-object/from16 v0, p0

    #@5b0
    move/from16 v1, v17

    #@5b2
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->removeMessages(I)V

    #@5b5
    .line 552
    :cond_5b5
    const/16 v17, 0xc8

    #@5b7
    const/16 v18, 0x0

    #@5b9
    const/16 v19, 0x0

    #@5bb
    const/16 v20, 0x7530

    #@5bd
    move-object/from16 v0, p0

    #@5bf
    move/from16 v1, v17

    #@5c1
    move/from16 v2, v18

    #@5c3
    move/from16 v3, v19

    #@5c5
    move/from16 v4, v20

    #@5c7
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/gfit/GfitUtils;->triggerEventAfterTimeout(IIII)V

    #@5ca
    goto/16 :goto_3a

    #@5cc
    .line 528
    .end local v11           #isNetworkModeChanged:Z
    :cond_5cc
    const/4 v11, 0x0

    #@5cd
    goto/16 :goto_52d

    #@5cf
    .line 553
    .restart local v11       #isNetworkModeChanged:Z
    :cond_5cf
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->getSystemSelection()I

    #@5d2
    move-result v17

    #@5d3
    if-nez v17, :cond_3a

    #@5d5
    .line 554
    const/16 v17, 0xc8

    #@5d7
    move-object/from16 v0, p0

    #@5d9
    move/from16 v1, v17

    #@5db
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->hasMessages(I)Z

    #@5de
    move-result v17

    #@5df
    if-eqz v17, :cond_3a

    #@5e1
    .line 555
    const-string v17, "Network mode changed to Global mode, remove msg for Global switching"

    #@5e3
    move-object/from16 v0, p0

    #@5e5
    move-object/from16 v1, v17

    #@5e7
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@5ea
    .line 556
    const/16 v17, 0xc8

    #@5ec
    move-object/from16 v0, p0

    #@5ee
    move/from16 v1, v17

    #@5f0
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->removeMessages(I)V

    #@5f3
    goto/16 :goto_3a

    #@5f5
    .line 284
    nop

    #@5f6
    :sswitch_data_5f6
    .sparse-switch
        0x64 -> :sswitch_3b
        0x65 -> :sswitch_fd
        0x66 -> :sswitch_3b
        0xc8 -> :sswitch_1a0
        0xc9 -> :sswitch_1cb
        0xcb -> :sswitch_254
        0xcc -> :sswitch_2db
        0xcd -> :sswitch_36f
        0xce -> :sswitch_3c9
        0xcf -> :sswitch_3eb
        0xd0 -> :sswitch_40d
        0xd1 -> :sswitch_48f
        0xd2 -> :sswitch_4b7
        0xd3 -> :sswitch_4c2
        0xd4 -> :sswitch_4c7
        0xdc -> :sswitch_4cc
        0xdd -> :sswitch_4d1
        0xde -> :sswitch_4f9
        0xdf -> :sswitch_503
    .end sparse-switch
.end method

.method public isUsedActionSIMStateChanged()Z
    .registers 2

    #@0
    .prologue
    .line 647
    iget-boolean v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->useActionSIMStateChanged:Z

    #@2
    return v0
.end method

.method public sendNetworkSelectionModeAutomatic()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 973
    const-string v3, "sendNetworkSelectionModeAutomatic()"

    #@3
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@6
    .line 975
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@8
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@b
    move-result-object v3

    #@c
    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@f
    move-result-object v2

    #@10
    .line 976
    .local v2, sp:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@13
    move-result-object v0

    #@14
    .line 977
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v3, "network_selection_key"

    #@16
    const-string v4, ""

    #@18
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@1b
    .line 978
    const-string v3, "network_selection_name_key"

    #@1d
    const-string v4, ""

    #@1f
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@22
    .line 979
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@25
    .line 981
    const/16 v3, 0xce

    #@27
    invoke-virtual {p0, v3, v5, v5}, Lcom/android/internal/telephony/gfit/GfitUtils;->obtainMessage(III)Landroid/os/Message;

    #@2a
    move-result-object v1

    #@2b
    .line 982
    .local v1, msg:Landroid/os/Message;
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2d
    invoke-interface {v3, v1}, Lcom/android/internal/telephony/CommandsInterface;->setNetworkSelectionModeAutomatic(Landroid/os/Message;)V

    #@30
    .line 983
    return-void
.end method

.method public sendNetworkSelectionModeManual(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "operatorNumeric"
    .parameter "operatorRat"
    .parameter "OperatorAlphaLong"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 956
    new-instance v3, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v4, "sendNetworkSelectionModeManual() : operatorNumeric = "

    #@8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    const-string v4, " operatorRat = "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    const-string v4, " operatorAlphaLong = "

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@2b
    .line 958
    const/16 v3, 0xcf

    #@2d
    invoke-virtual {p0, v3, v5, v5}, Lcom/android/internal/telephony/gfit/GfitUtils;->obtainMessage(III)Landroid/os/Message;

    #@30
    move-result-object v1

    #@31
    .line 962
    .local v1, msg:Landroid/os/Message;
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@33
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@36
    move-result-object v3

    #@37
    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@3a
    move-result-object v2

    #@3b
    .line 963
    .local v2, sp:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@3e
    move-result-object v0

    #@3f
    .line 965
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v3, "network_selection_key"

    #@41
    invoke-interface {v0, v3, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@44
    .line 966
    const-string v3, "network_selection_name_key"

    #@46
    invoke-interface {v0, v3, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@49
    .line 967
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@4c
    .line 969
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@4e
    invoke-interface {v3, p1, p2, v1}, Lcom/android/internal/telephony/CommandsInterface;->setNetworkSelectionModeManual(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@51
    .line 970
    return-void
.end method

.method public sendQueryAvailableNetworks()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 912
    const-string v1, "sendQueryAvailableNetworks()"

    #@3
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@6
    .line 913
    const/16 v1, 0xd0

    #@8
    invoke-virtual {p0, v1, v2, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->obtainMessage(III)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    .line 914
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    invoke-interface {v1, v0}, Lcom/android/internal/telephony/CommandsInterface;->getAvailableNetworks(Landroid/os/Message;)V

    #@11
    .line 915
    return-void
.end method

.method public sendQuerySystemMode(I)V
    .registers 5
    .parameter "systemSelectionMode"

    #@0
    .prologue
    .line 918
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "sendQuerySystemMode(systemSelectionMode = "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, " )"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@1c
    .line 919
    const/16 v1, 0xcc

    #@1e
    const/4 v2, 0x0

    #@1f
    invoke-virtual {p0, v1, p1, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->obtainMessage(III)Landroid/os/Message;

    #@22
    move-result-object v0

    #@23
    .line 920
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@25
    invoke-interface {v1, v0}, Lcom/android/internal/telephony/CommandsInterface;->getNetworkSelectionMode(Landroid/os/Message;)V

    #@28
    .line 921
    return-void
.end method

.method public sendSwitchToNetworkSelectionModeAutomaticAfterTimeout(I)V
    .registers 5
    .parameter "timeout"

    #@0
    .prologue
    .line 986
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "sendSwitchToNetworkSelectionModeAutomaticAfterTimeout(timeout = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, ")"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@1c
    .line 987
    const/16 v0, 0xd1

    #@1e
    int-to-long v1, p1

    #@1f
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendEmptyMessageDelayed(IJ)Z

    #@22
    .line 988
    return-void
.end method

.method public setPreferredNetworkMode(I)V
    .registers 6
    .parameter "preferrdNetworkMode"

    #@0
    .prologue
    .line 930
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v1

    #@a
    const-string v2, "preferred_network_mode"

    #@c
    const/16 v3, 0xa

    #@e
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11
    move-result v0

    #@12
    .line 935
    .local v0, networkMode:I
    if-eq v0, p1, :cond_3d

    #@14
    .line 936
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v2, "setPreferredNetworkMode(): preferrdNetworkMode= "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@2a
    .line 937
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@2c
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@33
    move-result-object v1

    #@34
    const-string v2, "preferred_network_mode"

    #@36
    invoke-static {v1, v2, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@39
    .line 942
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendPreferredNetworkType(I)V

    #@3c
    .line 946
    :goto_3c
    return-void

    #@3d
    .line 944
    :cond_3d
    const-string v1, "setPreferredNetowkrMode : Fail to set Preferred Network. "

    #@3f
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@42
    goto :goto_3c
.end method

.method public switchToGlobalMode()V
    .registers 2

    #@0
    .prologue
    .line 900
    const-string v0, "switchToGlobalMode()"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gfit/GfitUtils;->log(Ljava/lang/String;)V

    #@5
    .line 903
    invoke-direct {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->isManualSelection()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_e

    #@b
    .line 904
    invoke-virtual {p0}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendNetworkSelectionModeAutomatic()V

    #@e
    .line 908
    :cond_e
    const/16 v0, 0xa

    #@10
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gfit/GfitUtils;->setPreferredNetworkMode(I)V

    #@13
    .line 909
    return-void
.end method
