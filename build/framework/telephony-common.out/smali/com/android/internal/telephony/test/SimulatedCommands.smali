.class public final Lcom/android/internal/telephony/test/SimulatedCommands;
.super Lcom/android/internal/telephony/BaseCommands;
.source "SimulatedCommands.java"

# interfaces
.implements Lcom/android/internal/telephony/CommandsInterface;
.implements Lcom/android/internal/telephony/test/SimulatedRadioControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;,
        Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;
    }
.end annotation


# static fields
.field private static final DEFAULT_SIM_PIN2_CODE:Ljava/lang/String; = "5678"

.field private static final DEFAULT_SIM_PIN_CODE:Ljava/lang/String; = "1234"

.field private static final INITIAL_FDN_STATE:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState; = null

.field private static final INITIAL_LOCK_STATE:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState; = null

.field private static final LOG_TAG:Ljava/lang/String; = "SIM"

.field private static final SIM_PUK2_CODE:Ljava/lang/String; = "87654321"

.field private static final SIM_PUK_CODE:Ljava/lang/String; = "12345678"


# instance fields
.field mHandlerThread:Landroid/os/HandlerThread;

.field mNetworkType:I

.field mPin2Code:Ljava/lang/String;

.field mPin2UnlockAttempts:I

.field mPinCode:Ljava/lang/String;

.field mPinUnlockAttempts:I

.field mPuk2UnlockAttempts:I

.field mPukUnlockAttempts:I

.field mSimFdnEnabled:Z

.field mSimFdnEnabledState:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

.field mSimLockEnabled:Z

.field mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

.field mSsnNotifyOn:Z

.field nextCallFailCause:I

.field pausedResponseCount:I

.field pausedResponses:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 60
    sget-object v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@2
    sput-object v0, Lcom/android/internal/telephony/test/SimulatedCommands;->INITIAL_LOCK_STATE:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@4
    .line 63
    sget-object v0, Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;->NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@6
    sput-object v0, Lcom/android/internal/telephony/test/SimulatedCommands;->INITIAL_FDN_STATE:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 93
    const/4 v1, 0x0

    #@3
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/BaseCommands;-><init>(Landroid/content/Context;)V

    #@6
    .line 82
    iput-boolean v3, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSsnNotifyOn:Z

    #@8
    .line 85
    new-instance v1, Ljava/util/ArrayList;

    #@a
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponses:Ljava/util/ArrayList;

    #@f
    .line 87
    const/16 v1, 0x10

    #@11
    iput v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->nextCallFailCause:I

    #@13
    .line 94
    new-instance v1, Landroid/os/HandlerThread;

    #@15
    const-string v4, "SimulatedCommands"

    #@17
    invoke-direct {v1, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@1a
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mHandlerThread:Landroid/os/HandlerThread;

    #@1c
    .line 95
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mHandlerThread:Landroid/os/HandlerThread;

    #@1e
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@21
    .line 96
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mHandlerThread:Landroid/os/HandlerThread;

    #@23
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@26
    move-result-object v0

    #@27
    .line 98
    .local v0, looper:Landroid/os/Looper;
    new-instance v1, Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@29
    invoke-direct {v1, v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;-><init>(Landroid/os/Looper;)V

    #@2c
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2e
    .line 100
    sget-object v1, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_OFF:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@30
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->setRadioState(Lcom/android/internal/telephony/CommandsInterface$RadioState;)V

    #@33
    .line 101
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands;->INITIAL_LOCK_STATE:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@35
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@37
    .line 102
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@39
    sget-object v4, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@3b
    if-eq v1, v4, :cond_55

    #@3d
    move v1, v2

    #@3e
    :goto_3e
    iput-boolean v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockEnabled:Z

    #@40
    .line 103
    const-string v1, "1234"

    #@42
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPinCode:Ljava/lang/String;

    #@44
    .line 104
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands;->INITIAL_FDN_STATE:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@46
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabledState:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@48
    .line 105
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabledState:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@4a
    sget-object v4, Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;->NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@4c
    if-eq v1, v4, :cond_57

    #@4e
    :goto_4e
    iput-boolean v2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabled:Z

    #@50
    .line 106
    const-string v1, "5678"

    #@52
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPin2Code:Ljava/lang/String;

    #@54
    .line 107
    return-void

    #@55
    :cond_55
    move v1, v3

    #@56
    .line 102
    goto :goto_3e

    #@57
    :cond_57
    move v2, v3

    #@58
    .line 105
    goto :goto_4e
.end method

.method private isSimLocked()Z
    .registers 3

    #@0
    .prologue
    .line 1078
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@2
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@4
    if-eq v0, v1, :cond_8

    #@6
    .line 1079
    const/4 v0, 0x1

    #@7
    .line 1081
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private resultFail(Landroid/os/Message;Ljava/lang/Throwable;)V
    .registers 4
    .parameter "result"
    .parameter "tr"

    #@0
    .prologue
    .line 1431
    if-eqz p1, :cond_11

    #@2
    .line 1432
    invoke-static {p1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@5
    move-result-object v0

    #@6
    iput-object p2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@8
    .line 1433
    iget v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponseCount:I

    #@a
    if-lez v0, :cond_12

    #@c
    .line 1434
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponses:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11
    .line 1439
    :cond_11
    :goto_11
    return-void

    #@12
    .line 1436
    :cond_12
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@15
    goto :goto_11
.end method

.method private resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V
    .registers 4
    .parameter "result"
    .parameter "ret"

    #@0
    .prologue
    .line 1420
    if-eqz p1, :cond_11

    #@2
    .line 1421
    invoke-static {p1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@5
    move-result-object v0

    #@6
    iput-object p2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@8
    .line 1422
    iget v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponseCount:I

    #@a
    if-lez v0, :cond_12

    #@c
    .line 1423
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponses:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11
    .line 1428
    :cond_11
    :goto_11
    return-void

    #@12
    .line 1425
    :cond_12
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@15
    goto :goto_11
.end method

.method private unimplemented(Landroid/os/Message;)V
    .registers 5
    .parameter "result"

    #@0
    .prologue
    .line 1407
    if-eqz p1, :cond_18

    #@2
    .line 1408
    invoke-static {p1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@5
    move-result-object v0

    #@6
    new-instance v1, Ljava/lang/RuntimeException;

    #@8
    const-string v2, "Unimplemented"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    iput-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@f
    .line 1411
    iget v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponseCount:I

    #@11
    if-lez v0, :cond_19

    #@13
    .line 1412
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponses:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@18
    .line 1417
    :cond_18
    :goto_18
    return-void

    #@19
    .line 1414
    :cond_19
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@1c
    goto :goto_18
.end method


# virtual methods
.method public SAPConncetionrequest(ILandroid/os/Message;)V
    .registers 3
    .parameter "req"
    .parameter "result"

    #@0
    .prologue
    .line 1736
    return-void
.end method

.method public SAPrequest(ILjava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "op_type"
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 1734
    return-void
.end method

.method public acceptCall(Landroid/os/Message;)V
    .registers 5
    .parameter "result"

    #@0
    .prologue
    .line 728
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onAnswer()Z

    #@5
    move-result v0

    #@6
    .line 730
    .local v0, success:Z
    if-nez v0, :cond_13

    #@8
    .line 731
    new-instance v1, Ljava/lang/RuntimeException;

    #@a
    const-string v2, "Hangup Error"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@f
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultFail(Landroid/os/Message;Ljava/lang/Throwable;)V

    #@12
    .line 735
    :goto_12
    return-void

    #@13
    .line 733
    :cond_13
    const/4 v1, 0x0

    #@14
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@17
    goto :goto_12
.end method

.method public acknowledgeIncomingGsmSmsWithPdu(ZLjava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "success"
    .parameter "ackPdu"
    .parameter "result"

    #@0
    .prologue
    .line 1103
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1104
    return-void
.end method

.method public acknowledgeLastIncomingCdmaSms(ZILandroid/os/Message;)V
    .registers 4
    .parameter "success"
    .parameter "cause"
    .parameter "result"

    #@0
    .prologue
    .line 1098
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1099
    return-void
.end method

.method public acknowledgeLastIncomingGsmSms(ZILandroid/os/Message;)V
    .registers 4
    .parameter "success"
    .parameter "cause"
    .parameter "result"

    #@0
    .prologue
    .line 1094
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1095
    return-void
.end method

.method public cancelManualSearchingRequest(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1446
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public cancelPendingUssd(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1278
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 1279
    return-void
.end method

.method public changeBarringPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "facility"
    .parameter "oldPwd"
    .parameter "newPwd"
    .parameter "result"

    #@0
    .prologue
    .line 325
    invoke-direct {p0, p4}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 326
    return-void
.end method

.method public changeIccPin(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 8
    .parameter "oldPin"
    .parameter "newPin"
    .parameter "result"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 282
    if-eqz p1, :cond_16

    #@3
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPinCode:Ljava/lang/String;

    #@5
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_16

    #@b
    .line 283
    iput-object p2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPinCode:Ljava/lang/String;

    #@d
    .line 284
    if-eqz p3, :cond_15

    #@f
    .line 285
    invoke-static {p3, v3, v3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@12
    .line 286
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@15
    .line 300
    :cond_15
    :goto_15
    return-void

    #@16
    .line 292
    :cond_16
    if-eqz p3, :cond_15

    #@18
    .line 293
    const-string v1, "SIM"

    #@1a
    const-string v2, "[SimCmd] changeIccPin: pin failed!"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 295
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@21
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@23
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@26
    .line 297
    .local v0, ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p3, v3, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@29
    .line 298
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@2c
    goto :goto_15
.end method

.method public changeIccPin2(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 8
    .parameter "oldPin2"
    .parameter "newPin2"
    .parameter "result"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 303
    if-eqz p1, :cond_16

    #@3
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPin2Code:Ljava/lang/String;

    #@5
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_16

    #@b
    .line 304
    iput-object p2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPin2Code:Ljava/lang/String;

    #@d
    .line 305
    if-eqz p3, :cond_15

    #@f
    .line 306
    invoke-static {p3, v3, v3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@12
    .line 307
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@15
    .line 321
    :cond_15
    :goto_15
    return-void

    #@16
    .line 313
    :cond_16
    if-eqz p3, :cond_15

    #@18
    .line 314
    const-string v1, "SIM"

    #@1a
    const-string v2, "[SimCmd] changeIccPin2: pin2 failed!"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 316
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@21
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@23
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@26
    .line 318
    .local v0, ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p3, v3, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@29
    .line 319
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@2c
    goto :goto_15
.end method

.method public changeIccPin2ForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "oldPin2"
    .parameter "newPin2"
    .parameter "aidPtr"
    .parameter "response"

    #@0
    .prologue
    .line 1608
    invoke-direct {p0, p4}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1609
    return-void
.end method

.method public changeIccPinForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "oldPin"
    .parameter "newPin"
    .parameter "aidPtr"
    .parameter "response"

    #@0
    .prologue
    .line 1602
    invoke-direct {p0, p4}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1603
    return-void
.end method

.method public closeImsPdn(ILandroid/os/Message;)V
    .registers 3
    .parameter "reason"
    .parameter "result"

    #@0
    .prologue
    .line 1678
    return-void
.end method

.method public conference(Landroid/os/Message;)V
    .registers 6
    .parameter "result"

    #@0
    .prologue
    .line 672
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    const/16 v2, 0x33

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onChld(CC)Z

    #@8
    move-result v0

    #@9
    .line 674
    .local v0, success:Z
    if-nez v0, :cond_16

    #@b
    .line 675
    new-instance v1, Ljava/lang/RuntimeException;

    #@d
    const-string v2, "Hangup Error"

    #@f
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultFail(Landroid/os/Message;Ljava/lang/Throwable;)V

    #@15
    .line 679
    :goto_15
    return-void

    #@16
    .line 677
    :cond_16
    const/4 v1, 0x0

    #@17
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@1a
    goto :goto_15
.end method

.method public deactivateDataCall(IILandroid/os/Message;)V
    .registers 4
    .parameter "cid"
    .parameter "reason"
    .parameter "result"

    #@0
    .prologue
    .line 1023
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public deleteSmsOnRuim(ILandroid/os/Message;)V
    .registers 6
    .parameter "index"
    .parameter "response"

    #@0
    .prologue
    .line 996
    const-string v0, "SIM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Delete RUIM message at index "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 997
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@1b
    .line 998
    return-void
.end method

.method public deleteSmsOnSim(ILandroid/os/Message;)V
    .registers 6
    .parameter "index"
    .parameter "response"

    #@0
    .prologue
    .line 991
    const-string v0, "SIM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Delete message at index "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 992
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@1b
    .line 993
    return-void
.end method

.method public dial(Ljava/lang/String;ILandroid/os/Message;)V
    .registers 5
    .parameter "address"
    .parameter "clirMode"
    .parameter "result"

    #@0
    .prologue
    .line 490
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onDial(Ljava/lang/String;)Z

    #@5
    .line 492
    const/4 v0, 0x0

    #@6
    invoke-direct {p0, p3, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@9
    .line 493
    return-void
.end method

.method public dial(Ljava/lang/String;ILcom/android/internal/telephony/UUSInfo;Landroid/os/Message;)V
    .registers 6
    .parameter "address"
    .parameter "clirMode"
    .parameter "uusInfo"
    .parameter "result"

    #@0
    .prologue
    .line 507
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onDial(Ljava/lang/String;)Z

    #@5
    .line 509
    const/4 v0, 0x0

    #@6
    invoke-direct {p0, p4, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@9
    .line 510
    return-void
.end method

.method public doNVControl(IILjava/lang/String;IILandroid/os/Message;)V
    .registers 7
    .parameter "nv_control_command"
    .parameter "nv_item_number"
    .parameter "nv_data"
    .parameter "nv_length"
    .parameter "mode"
    .parameter "result"

    #@0
    .prologue
    .line 1744
    return-void
.end method

.method public emulNetworkState(I)V
    .registers 2
    .parameter "cmdID"

    #@0
    .prologue
    .line 1616
    return-void
.end method

.method public exitEmergencyCallbackMode(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1199
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public explicitCallTransfer(Landroid/os/Message;)V
    .registers 6
    .parameter "result"

    #@0
    .prologue
    .line 692
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    const/16 v2, 0x34

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onChld(CC)Z

    #@8
    move-result v0

    #@9
    .line 694
    .local v0, success:Z
    if-nez v0, :cond_16

    #@b
    .line 695
    new-instance v1, Ljava/lang/RuntimeException;

    #@d
    const-string v2, "Hangup Error"

    #@f
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultFail(Landroid/os/Message;Ljava/lang/Throwable;)V

    #@15
    .line 699
    :goto_15
    return-void

    #@16
    .line 697
    :cond_16
    const/4 v1, 0x0

    #@17
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@1a
    goto :goto_15
.end method

.method public forceDataDormancy(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 1558
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1559
    return-void
.end method

.method public getAvailableNetworks(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1227
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public getBasebandVersion(Landroid/os/Message;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 1230
    const-string v0, "SimulatedCommands"

    #@2
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@5
    .line 1231
    return-void
.end method

.method public getCDMASubscription(Landroid/os/Message;)V
    .registers 4
    .parameter "response"

    #@0
    .prologue
    .line 1458
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1459
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@a
    .line 1460
    return-void
.end method

.method public getCLIR(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1146
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public getCdmaBroadcastConfig(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 1544
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1546
    return-void
.end method

.method public getCdmaSubscriptionSource(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1074
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1075
    return-void
.end method

.method public getCurrentCalls(Landroid/os/Message;)V
    .registers 4
    .parameter "result"

    #@0
    .prologue
    .line 449
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@2
    sget-object v1, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_ON:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@4
    if-ne v0, v1, :cond_16

    #@6
    invoke-direct {p0}, Lcom/android/internal/telephony/test/SimulatedCommands;->isSimLocked()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_16

    #@c
    .line 451
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->getDriverCalls()Ljava/util/List;

    #@11
    move-result-object v0

    #@12
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@15
    .line 458
    :goto_15
    return-void

    #@16
    .line 454
    :cond_16
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@18
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    #@1a
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@1d
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultFail(Landroid/os/Message;Ljava/lang/Throwable;)V

    #@20
    goto :goto_15
.end method

.method public getDataCallList(Landroid/os/Message;)V
    .registers 4
    .parameter "result"

    #@0
    .prologue
    .line 475
    new-instance v0, Ljava/util/ArrayList;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@6
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@9
    .line 476
    return-void
.end method

.method public getDataCallProfile(ILandroid/os/Message;)V
    .registers 3
    .parameter "appType"
    .parameter "response"

    #@0
    .prologue
    .line 1645
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1646
    return-void
.end method

.method public getDataRegistrationState(Landroid/os/Message;)V
    .registers 6
    .parameter "result"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 920
    const/4 v1, 0x4

    #@2
    new-array v0, v1, [Ljava/lang/String;

    #@4
    .line 922
    .local v0, ret:[Ljava/lang/String;
    const/4 v1, 0x0

    #@5
    const-string v2, "5"

    #@7
    aput-object v2, v0, v1

    #@9
    .line 923
    const/4 v1, 0x1

    #@a
    aput-object v3, v0, v1

    #@c
    .line 924
    const/4 v1, 0x2

    #@d
    aput-object v3, v0, v1

    #@f
    .line 925
    const/4 v1, 0x3

    #@10
    const-string v2, "2"

    #@12
    aput-object v2, v0, v1

    #@14
    .line 927
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@17
    .line 928
    return-void
.end method

.method public getDebugInfo(II)[I
    .registers 4
    .parameter "type"
    .parameter "num"

    #@0
    .prologue
    .line 1694
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getDeviceIdentity(Landroid/os/Message;)V
    .registers 4
    .parameter "response"

    #@0
    .prologue
    .line 1452
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1453
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@a
    .line 1454
    return-void
.end method

.method public getEhrpdInfoForIms(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1680
    return-void
.end method

.method public getEmodeInfoPage(I)Ljava/lang/String;
    .registers 5
    .parameter "EngIndex"

    #@0
    .prologue
    .line 556
    const-string v0, "SIM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[Hidden] GSMPhone.java : getEmodeInfo = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 558
    const-string v0, "Emode Info"

    #@1a
    return-object v0
.end method

.method public getEngineeringModeInfo(ILandroid/os/Message;)I
    .registers 6
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 550
    const-string v0, "SIM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[Hidden] EngMode.java : getEngineeringModeInfo : request page is "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 551
    const-string v0, "OK"

    #@1a
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@1d
    .line 552
    const/4 v0, 0x0

    #@1e
    return v0
.end method

.method public getGsmBroadcastConfig(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 1572
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1573
    return-void
.end method

.method public getIMEI(Landroid/os/Message;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 534
    const-string v0, "012345678901234"

    #@2
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@5
    .line 535
    return-void
.end method

.method public getIMEISV(Landroid/os/Message;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 545
    const-string v0, "99"

    #@2
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@5
    .line 546
    return-void
.end method

.method public getIMSI(Landroid/os/Message;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 513
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->getIMSIForApp(Ljava/lang/String;Landroid/os/Message;)V

    #@4
    .line 514
    return-void
.end method

.method public getIMSIForApp(Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "aid"
    .parameter "result"

    #@0
    .prologue
    .line 523
    const-string v0, "012345678901234"

    #@2
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@5
    .line 524
    return-void
.end method

.method public getIccCardStatus(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 113
    return-void
.end method

.method public getImsRegistrationState(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 1624
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1625
    return-void
.end method

.method public getImsTest(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 805
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public getLastCallFailCause(Landroid/os/Message;)V
    .registers 5
    .parameter "result"

    #@0
    .prologue
    .line 764
    const/4 v1, 0x1

    #@1
    new-array v0, v1, [I

    #@3
    .line 766
    .local v0, ret:[I
    const/4 v1, 0x0

    #@4
    iget v2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->nextCallFailCause:I

    #@6
    aput v2, v0, v1

    #@8
    .line 767
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@b
    .line 768
    return-void
.end method

.method public getLastDataCallFailCause(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 779
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 780
    return-void
.end method

.method public getLastPdpFailCause(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 774
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 775
    return-void
.end method

.method public getLteInfoForIms(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1679
    return-void
.end method

.method public getMipErrorCode(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1668
    return-void
.end method

.method public getMute(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 784
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public getNeighboringCids(Landroid/os/Message;)V
    .registers 7
    .parameter "result"

    #@0
    .prologue
    const/4 v4, 0x7

    #@1
    .line 1043
    new-array v1, v4, [I

    #@3
    .line 1045
    .local v1, ret:[I
    const/4 v2, 0x0

    #@4
    const/4 v3, 0x6

    #@5
    aput v3, v1, v2

    #@7
    .line 1046
    const/4 v0, 0x1

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v4, :cond_f

    #@a
    .line 1047
    aput v0, v1, v0

    #@c
    .line 1046
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_8

    #@f
    .line 1049
    :cond_f
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@12
    .line 1050
    return-void
.end method

.method public getNetworkSelectionMode(Landroid/os/Message;)V
    .registers 5
    .parameter "result"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1216
    const/4 v1, 0x1

    #@2
    new-array v0, v1, [I

    #@4
    .line 1218
    .local v0, ret:[I
    aput v2, v0, v2

    #@6
    .line 1219
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@9
    .line 1220
    return-void
.end method

.method public getNvTest(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 804
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public getOperator(Landroid/os/Message;)V
    .registers 5
    .parameter "result"

    #@0
    .prologue
    .line 937
    const/4 v1, 0x3

    #@1
    new-array v0, v1, [Ljava/lang/String;

    #@3
    .line 939
    .local v0, ret:[Ljava/lang/String;
    const/4 v1, 0x0

    #@4
    const-string v2, "El Telco Loco"

    #@6
    aput-object v2, v0, v1

    #@8
    .line 940
    const/4 v1, 0x1

    #@9
    const-string v2, "Telco Loco"

    #@b
    aput-object v2, v0, v1

    #@d
    .line 941
    const/4 v1, 0x2

    #@e
    const-string v2, "001001"

    #@10
    aput-object v2, v0, v1

    #@12
    .line 943
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@15
    .line 944
    return-void
.end method

.method public getPDPContextList(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 464
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->getDataCallList(Landroid/os/Message;)V

    #@3
    .line 465
    return-void
.end method

.method public getPcscfAddress(ILjava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "cid"
    .parameter "ipv"
    .parameter "result"

    #@0
    .prologue
    .line 1683
    return-void
.end method

.method public getPreferredNetworkType(Landroid/os/Message;)V
    .registers 5
    .parameter "result"

    #@0
    .prologue
    .line 1036
    const/4 v1, 0x1

    #@1
    new-array v0, v1, [I

    #@3
    .line 1038
    .local v0, ret:[I
    const/4 v1, 0x0

    #@4
    iget v2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mNetworkType:I

    #@6
    aput v2, v0, v1

    #@8
    .line 1039
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@b
    .line 1040
    return-void
.end method

.method public getPreferredVoicePrivacy(Landroid/os/Message;)V
    .registers 4
    .parameter "result"

    #@0
    .prologue
    .line 1484
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1485
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@a
    .line 1486
    return-void
.end method

.method public getRssiTest(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 814
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public getSearchStatus(ILandroid/os/Message;)V
    .registers 4
    .parameter "ReqType"
    .parameter "response"

    #@0
    .prologue
    .line 565
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 566
    return-void
.end method

.method public getSearchStatus(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 570
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 571
    return-void
.end method

.method public getSignalStrength(Landroid/os/Message;)V
    .registers 5
    .parameter "result"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 794
    const/4 v1, 0x2

    #@2
    new-array v0, v1, [I

    #@4
    .line 796
    .local v0, ret:[I
    const/16 v1, 0x17

    #@6
    aput v1, v0, v2

    #@8
    .line 797
    const/4 v1, 0x1

    #@9
    aput v2, v0, v1

    #@b
    .line 799
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@e
    .line 800
    return-void
.end method

.method public getSmscAddress(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1057
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1058
    return-void
.end method

.method public getVoiceRadioTechnology(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 1620
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1621
    return-void
.end method

.method public getVoiceRegistrationState(Landroid/os/Message;)V
    .registers 6
    .parameter "result"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 882
    const/16 v1, 0xe

    #@3
    new-array v0, v1, [Ljava/lang/String;

    #@5
    .line 884
    .local v0, ret:[Ljava/lang/String;
    const/4 v1, 0x0

    #@6
    const-string v2, "5"

    #@8
    aput-object v2, v0, v1

    #@a
    .line 885
    const/4 v1, 0x1

    #@b
    aput-object v3, v0, v1

    #@d
    .line 886
    const/4 v1, 0x2

    #@e
    aput-object v3, v0, v1

    #@10
    .line 887
    const/4 v1, 0x3

    #@11
    aput-object v3, v0, v1

    #@13
    .line 888
    const/4 v1, 0x4

    #@14
    aput-object v3, v0, v1

    #@16
    .line 889
    const/4 v1, 0x5

    #@17
    aput-object v3, v0, v1

    #@19
    .line 890
    const/4 v1, 0x6

    #@1a
    aput-object v3, v0, v1

    #@1c
    .line 891
    const/4 v1, 0x7

    #@1d
    aput-object v3, v0, v1

    #@1f
    .line 892
    const/16 v1, 0x8

    #@21
    aput-object v3, v0, v1

    #@23
    .line 893
    const/16 v1, 0x9

    #@25
    aput-object v3, v0, v1

    #@27
    .line 894
    const/16 v1, 0xa

    #@29
    aput-object v3, v0, v1

    #@2b
    .line 895
    const/16 v1, 0xb

    #@2d
    aput-object v3, v0, v1

    #@2f
    .line 896
    const/16 v1, 0xc

    #@31
    aput-object v3, v0, v1

    #@33
    .line 897
    const/16 v1, 0xd

    #@35
    aput-object v3, v0, v1

    #@37
    .line 899
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@3a
    .line 900
    return-void
.end method

.method public getVssTest(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 803
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public handleCallSetupRequestFromSim(ZLandroid/os/Message;)V
    .registers 4
    .parameter "accept"
    .parameter "response"

    #@0
    .prologue
    .line 871
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 872
    return-void
.end method

.method public hangupConnection(ILandroid/os/Message;)V
    .registers 7
    .parameter "gsmIndex"
    .parameter "result"

    #@0
    .prologue
    .line 588
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    const/16 v2, 0x31

    #@4
    add-int/lit8 v3, p1, 0x30

    #@6
    int-to-char v3, v3

    #@7
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onChld(CC)Z

    #@a
    move-result v0

    #@b
    .line 590
    .local v0, success:Z
    if-nez v0, :cond_1f

    #@d
    .line 591
    const-string v1, "GSM"

    #@f
    const-string v2, "[SimCmd] hangupConnection: resultFail"

    #@11
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 592
    new-instance v1, Ljava/lang/RuntimeException;

    #@16
    const-string v2, "Hangup Error"

    #@18
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1b
    invoke-direct {p0, p2, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultFail(Landroid/os/Message;Ljava/lang/Throwable;)V

    #@1e
    .line 597
    :goto_1e
    return-void

    #@1f
    .line 594
    :cond_1f
    const-string v1, "GSM"

    #@21
    const-string v2, "[SimCmd] hangupConnection: resultSuccess"

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 595
    const/4 v1, 0x0

    #@27
    invoke-direct {p0, p2, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@2a
    goto :goto_1e
.end method

.method public hangupForegroundResumeBackground(Landroid/os/Message;)V
    .registers 6
    .parameter "result"

    #@0
    .prologue
    .line 631
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    const/16 v2, 0x31

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onChld(CC)Z

    #@8
    move-result v0

    #@9
    .line 633
    .local v0, success:Z
    if-nez v0, :cond_16

    #@b
    .line 634
    new-instance v1, Ljava/lang/RuntimeException;

    #@d
    const-string v2, "Hangup Error"

    #@f
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultFail(Landroid/os/Message;Ljava/lang/Throwable;)V

    #@15
    .line 638
    :goto_15
    return-void

    #@16
    .line 636
    :cond_16
    const/4 v1, 0x0

    #@17
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@1a
    goto :goto_15
.end method

.method public hangupWaitingOrBackground(Landroid/os/Message;)V
    .registers 6
    .parameter "result"

    #@0
    .prologue
    .line 610
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    const/16 v2, 0x30

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onChld(CC)Z

    #@8
    move-result v0

    #@9
    .line 612
    .local v0, success:Z
    if-nez v0, :cond_16

    #@b
    .line 613
    new-instance v1, Ljava/lang/RuntimeException;

    #@d
    const-string v2, "Hangup Error"

    #@f
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultFail(Landroid/os/Message;Ljava/lang/Throwable;)V

    #@15
    .line 617
    :goto_15
    return-void

    #@16
    .line 615
    :cond_16
    const/4 v1, 0x0

    #@17
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@1a
    goto :goto_15
.end method

.method public iccIO(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 21
    .parameter "command"
    .parameter "fileid"
    .parameter "path"
    .parameter "p1"
    .parameter "p2"
    .parameter "p3"
    .parameter "data"
    .parameter "pin2"
    .parameter "response"

    #@0
    .prologue
    .line 1108
    const/4 v9, 0x0

    #@1
    move-object v0, p0

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move/from16 v5, p5

    #@8
    move/from16 v6, p6

    #@a
    move-object/from16 v7, p7

    #@c
    move-object/from16 v8, p8

    #@e
    move-object/from16 v10, p9

    #@10
    invoke-virtual/range {v0 .. v10}, Lcom/android/internal/telephony/test/SimulatedCommands;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@13
    .line 1109
    return-void
.end method

.method public iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 11
    .parameter "command"
    .parameter "fileid"
    .parameter "path"
    .parameter "p1"
    .parameter "p2"
    .parameter "p3"
    .parameter "data"
    .parameter "pin2"
    .parameter "aid"
    .parameter "result"

    #@0
    .prologue
    .line 1118
    invoke-direct {p0, p10}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1119
    return-void
.end method

.method public invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .registers 4
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1288
    if-eqz p2, :cond_b

    #@2
    .line 1289
    invoke-static {p2}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@5
    move-result-object v0

    #@6
    iput-object p1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@8
    .line 1290
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@b
    .line 1292
    :cond_b
    return-void
.end method

.method public invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "strings"
    .parameter "response"

    #@0
    .prologue
    .line 1296
    if-eqz p2, :cond_b

    #@2
    .line 1297
    invoke-static {p2}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@5
    move-result-object v0

    #@6
    iput-object p1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@8
    .line 1298
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@b
    .line 1300
    :cond_b
    return-void
.end method

.method public pauseResponses()V
    .registers 2

    #@0
    .prologue
    .line 1387
    iget v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponseCount:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponseCount:I

    #@6
    .line 1388
    return-void
.end method

.method public progressConnectingCallState()V
    .registers 2

    #@0
    .prologue
    .line 1314
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->progressConnectingCallState()V

    #@5
    .line 1315
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCallStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@a
    .line 1316
    return-void
.end method

.method public progressConnectingToActive()V
    .registers 2

    #@0
    .prologue
    .line 1321
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->progressConnectingToActive()V

    #@5
    .line 1322
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCallStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@a
    .line 1323
    return-void
.end method

.method public queryAvailableBandMode(Landroid/os/Message;)V
    .registers 7
    .parameter "result"

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v3, 0x2

    #@2
    const/4 v2, 0x4

    #@3
    .line 835
    new-array v0, v2, [I

    #@5
    .line 837
    .local v0, ret:[I
    const/4 v1, 0x0

    #@6
    aput v2, v0, v1

    #@8
    .line 838
    const/4 v1, 0x1

    #@9
    aput v3, v0, v1

    #@b
    .line 839
    aput v4, v0, v3

    #@d
    .line 840
    aput v2, v0, v4

    #@f
    .line 842
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@12
    .line 843
    return-void
.end method

.method public queryCLIP(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 1127
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public queryCallForwardStatus(IILjava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "cfReason"
    .parameter "serviceClass"
    .parameter "number"
    .parameter "result"

    #@0
    .prologue
    .line 1196
    invoke-direct {p0, p4}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public queryCallWaiting(ILandroid/os/Message;)V
    .registers 3
    .parameter "serviceClass"
    .parameter "response"

    #@0
    .prologue
    .line 1165
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1166
    return-void
.end method

.method public queryCdmaRoamingPreference(Landroid/os/Message;)V
    .registers 4
    .parameter "response"

    #@0
    .prologue
    .line 1469
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1470
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@a
    .line 1471
    return-void
.end method

.method public queryFacilityLock(Ljava/lang/String;Ljava/lang/String;ILandroid/os/Message;)V
    .registers 11
    .parameter "facility"
    .parameter "pin"
    .parameter "serviceClass"
    .parameter "result"

    #@0
    .prologue
    .line 342
    const/4 v4, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move-object v5, p4

    #@6
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/test/SimulatedCommands;->queryFacilityLockForApp(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/os/Message;)V

    #@9
    .line 343
    return-void
.end method

.method public queryFacilityLockForApp(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/os/Message;)V
    .registers 12
    .parameter "facility"
    .parameter "pin"
    .parameter "serviceClass"
    .parameter "appId"
    .parameter "result"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 348
    if-eqz p1, :cond_41

    #@5
    const-string v3, "SC"

    #@7
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_41

    #@d
    .line 349
    if-eqz p5, :cond_3b

    #@f
    .line 350
    new-array v0, v1, [I

    #@11
    .line 351
    .local v0, r:[I
    iget-boolean v3, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockEnabled:Z

    #@13
    if-eqz v3, :cond_3c

    #@15
    :goto_15
    aput v1, v0, v2

    #@17
    .line 352
    const-string v3, "SIM"

    #@19
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v4, "[SimCmd] queryFacilityLock: SIM is "

    #@20
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    aget v1, v0, v2

    #@26
    if-nez v1, :cond_3e

    #@28
    const-string v1, "unlocked"

    #@2a
    :goto_2a
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 354
    invoke-static {p5, v0, v5}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@38
    .line 355
    invoke-virtual {p5}, Landroid/os/Message;->sendToTarget()V

    #@3b
    .line 371
    .end local v0           #r:[I
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .restart local v0       #r:[I
    :cond_3c
    move v1, v2

    #@3d
    .line 351
    goto :goto_15

    #@3e
    .line 352
    :cond_3e
    const-string v1, "locked"

    #@40
    goto :goto_2a

    #@41
    .line 358
    .end local v0           #r:[I
    :cond_41
    if-eqz p1, :cond_7f

    #@43
    const-string v3, "FD"

    #@45
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v3

    #@49
    if-eqz v3, :cond_7f

    #@4b
    .line 359
    if-eqz p5, :cond_3b

    #@4d
    .line 360
    new-array v0, v1, [I

    #@4f
    .line 361
    .restart local v0       #r:[I
    iget-boolean v3, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabled:Z

    #@51
    if-eqz v3, :cond_7a

    #@53
    :goto_53
    aput v1, v0, v2

    #@55
    .line 362
    const-string v3, "SIM"

    #@57
    new-instance v1, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v4, "[SimCmd] queryFacilityLock: FDN is "

    #@5e
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    aget v1, v0, v2

    #@64
    if-nez v1, :cond_7c

    #@66
    const-string v1, "disabled"

    #@68
    :goto_68
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 364
    invoke-static {p5, v0, v5}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@76
    .line 365
    invoke-virtual {p5}, Landroid/os/Message;->sendToTarget()V

    #@79
    goto :goto_3b

    #@7a
    :cond_7a
    move v1, v2

    #@7b
    .line 361
    goto :goto_53

    #@7c
    .line 362
    :cond_7c
    const-string v1, "enabled"

    #@7e
    goto :goto_68

    #@7f
    .line 370
    .end local v0           #r:[I
    :cond_7f
    invoke-direct {p0, p5}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@82
    goto :goto_3b
.end method

.method public queryTTYMode(Landroid/os/Message;)V
    .registers 4
    .parameter "response"

    #@0
    .prologue
    .line 1519
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1520
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@a
    .line 1521
    return-void
.end method

.method public rejectCall(Landroid/os/Message;)V
    .registers 6
    .parameter "result"

    #@0
    .prologue
    .line 746
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    const/16 v2, 0x30

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onChld(CC)Z

    #@8
    move-result v0

    #@9
    .line 748
    .local v0, success:Z
    if-nez v0, :cond_16

    #@b
    .line 749
    new-instance v1, Ljava/lang/RuntimeException;

    #@d
    const-string v2, "Hangup Error"

    #@f
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultFail(Landroid/os/Message;Ljava/lang/Throwable;)V

    #@15
    .line 753
    :goto_15
    return-void

    #@16
    .line 751
    :cond_16
    const/4 v1, 0x0

    #@17
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@1a
    goto :goto_15
.end method

.method public reportSmsMemoryStatus(ZLandroid/os/Message;)V
    .registers 3
    .parameter "available"
    .parameter "result"

    #@0
    .prologue
    .line 1065
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1066
    return-void
.end method

.method public reportStkServiceIsRunning(Landroid/os/Message;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 1069
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 1070
    return-void
.end method

.method public requestIsimAuthentication(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "nonce"
    .parameter "response"

    #@0
    .prologue
    .line 1612
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1613
    return-void
.end method

.method public resetRadio(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1283
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1284
    return-void
.end method

.method public resumeResponses()V
    .registers 5

    #@0
    .prologue
    .line 1392
    iget v2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponseCount:I

    #@2
    add-int/lit8 v2, v2, -0x1

    #@4
    iput v2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponseCount:I

    #@6
    .line 1394
    iget v2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponseCount:I

    #@8
    if-nez v2, :cond_27

    #@a
    .line 1395
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponses:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v1

    #@11
    .local v1, s:I
    :goto_11
    if-ge v0, v1, :cond_21

    #@13
    .line 1396
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponses:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/os/Message;

    #@1b
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    #@1e
    .line 1395
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_11

    #@21
    .line 1398
    :cond_21
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->pausedResponses:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@26
    .line 1402
    .end local v0           #i:I
    .end local v1           #s:I
    :goto_26
    return-void

    #@27
    .line 1400
    :cond_27
    const-string v2, "GSM"

    #@29
    const-string v3, "SimulatedCommands.resumeResponses < 0"

    #@2b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    goto :goto_26
.end method

.method public sendBurstDtmf(Ljava/lang/String;IILandroid/os/Message;)V
    .registers 6
    .parameter "dtmfString"
    .parameter "on"
    .parameter "off"
    .parameter "result"

    #@0
    .prologue
    .line 979
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p4, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 980
    return-void
.end method

.method public sendCDMAFeatureCode(Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "FeatureCode"
    .parameter "response"

    #@0
    .prologue
    .line 1527
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1528
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@a
    .line 1529
    return-void
.end method

.method public sendCdmaSms([BLandroid/os/Message;)V
    .registers 5
    .parameter "pdu"
    .parameter "response"

    #@0
    .prologue
    .line 1535
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1536
    return-void
.end method

.method public sendDefaultAttachProfile(ILandroid/os/Message;)V
    .registers 3
    .parameter "profileId"
    .parameter "result"

    #@0
    .prologue
    .line 1686
    return-void
.end method

.method public sendDefaultPdnTable(IILjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 12
    .parameter "pdnId"
    .parameter "apnLength"
    .parameter "apn"
    .parameter "ipType"
    .parameter "inactivityTime"
    .parameter "enable"
    .parameter "authType"
    .parameter "esminfo"
    .parameter "username"
    .parameter "password"
    .parameter "result"

    #@0
    .prologue
    .line 1673
    return-void
.end method

.method public sendDtmf(CLandroid/os/Message;)V
    .registers 4
    .parameter "c"
    .parameter "result"

    #@0
    .prologue
    .line 952
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 953
    return-void
.end method

.method public sendEnvelope(Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "contents"
    .parameter "response"

    #@0
    .prologue
    .line 856
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 857
    return-void
.end method

.method public sendEnvelopeWithStatus(Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "contents"
    .parameter "response"

    #@0
    .prologue
    .line 863
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 864
    return-void
.end method

.method public sendImsCdmaSms([BIILandroid/os/Message;)V
    .registers 5
    .parameter "pdu"
    .parameter "retry"
    .parameter "messageRef"
    .parameter "response"

    #@0
    .prologue
    .line 1629
    invoke-direct {p0, p4}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1630
    return-void
.end method

.method public sendImsGsmSms(Ljava/lang/String;Ljava/lang/String;IILandroid/os/Message;)V
    .registers 6
    .parameter "smscPDU"
    .parameter "pdu"
    .parameter "retry"
    .parameter "messageRef"
    .parameter "response"

    #@0
    .prologue
    .line 1634
    invoke-direct {p0, p5}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1635
    return-void
.end method

.method public sendMPDNTable(Landroid/os/Message;Z)V
    .registers 3
    .parameter "result"
    .parameter "isotaattach"

    #@0
    .prologue
    .line 1663
    return-void
.end method

.method public sendPdnTable(IILjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 12
    .parameter "pdnId"
    .parameter "apnLength"
    .parameter "apn"
    .parameter "ipType"
    .parameter "inactivityTime"
    .parameter "enable"
    .parameter "authType"
    .parameter "esminfo"
    .parameter "username"
    .parameter "password"
    .parameter "result"

    #@0
    .prologue
    .line 1662
    return-void
.end method

.method public sendSMS(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "smscPDU"
    .parameter "pdu"
    .parameter "result"

    #@0
    .prologue
    .line 988
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public sendStkCcAplha(Ljava/lang/String;)V
    .registers 2
    .parameter "alphaString"

    #@0
    .prologue
    .line 1244
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->triggerIncomingStkCcAlpha(Ljava/lang/String;)V

    #@3
    .line 1245
    return-void
.end method

.method public sendTerminalResponse(Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "contents"
    .parameter "response"

    #@0
    .prologue
    .line 849
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 850
    return-void
.end method

.method public sendUSSD(Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "ussdString"
    .parameter "result"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1264
    const-string v0, "#646#"

    #@3
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_14

    #@9
    .line 1265
    invoke-direct {p0, p2, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@c
    .line 1268
    const-string v0, "0"

    #@e
    const-string v1, "You have NNN minutes remaining."

    #@10
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->triggerIncomingUssd(Ljava/lang/String;Ljava/lang/String;)V

    #@13
    .line 1274
    :goto_13
    return-void

    #@14
    .line 1270
    :cond_14
    invoke-direct {p0, p2, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@17
    .line 1272
    const-string v0, "0"

    #@19
    const-string v1, "All Done"

    #@1b
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->triggerIncomingUssd(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    goto :goto_13
.end method

.method public separateConnection(ILandroid/os/Message;)V
    .registers 7
    .parameter "gsmIndex"
    .parameter "result"

    #@0
    .prologue
    .line 709
    add-int/lit8 v2, p1, 0x30

    #@2
    int-to-char v0, v2

    #@3
    .line 710
    .local v0, ch:C
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@5
    const/16 v3, 0x32

    #@7
    invoke-virtual {v2, v3, v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onChld(CC)Z

    #@a
    move-result v1

    #@b
    .line 712
    .local v1, success:Z
    if-nez v1, :cond_18

    #@d
    .line 713
    new-instance v2, Ljava/lang/RuntimeException;

    #@f
    const-string v3, "Hangup Error"

    #@11
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@14
    invoke-direct {p0, p2, v2}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultFail(Landroid/os/Message;Ljava/lang/Throwable;)V

    #@17
    .line 717
    :goto_17
    return-void

    #@18
    .line 715
    :cond_18
    const/4 v2, 0x0

    #@19
    invoke-direct {p0, p2, v2}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@1c
    goto :goto_17
.end method

.method public setAutoProgressConnectingCall(Z)V
    .registers 3
    .parameter "b"

    #@0
    .prologue
    .line 1330
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->setAutoProgressConnectingCall(Z)V

    #@5
    .line 1331
    return-void
.end method

.method public setBandMode(ILandroid/os/Message;)V
    .registers 4
    .parameter "bandMode"
    .parameter "result"

    #@0
    .prologue
    .line 824
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 825
    return-void
.end method

.method public setCLIR(ILandroid/os/Message;)V
    .registers 3
    .parameter "clirMode"
    .parameter "result"

    #@0
    .prologue
    .line 1154
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public setCSGSelectionManual(ILandroid/os/Message;)V
    .registers 4
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 1032
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 1033
    return-void
.end method

.method public setCallForward(IIILjava/lang/String;ILandroid/os/Message;)V
    .registers 7
    .parameter "action"
    .parameter "cfReason"
    .parameter "serviceClass"
    .parameter "number"
    .parameter "timeSeconds"
    .parameter "result"

    #@0
    .prologue
    .line 1185
    invoke-direct {p0, p6}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public setCallWaiting(ZILandroid/os/Message;)V
    .registers 4
    .parameter "enable"
    .parameter "serviceClass"
    .parameter "response"

    #@0
    .prologue
    .line 1176
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1177
    return-void
.end method

.method public setCdmaBroadcastActivation(ZLandroid/os/Message;)V
    .registers 3
    .parameter "activate"
    .parameter "response"

    #@0
    .prologue
    .line 1539
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1541
    return-void
.end method

.method public setCdmaBroadcastConfig([ILandroid/os/Message;)V
    .registers 3
    .parameter "configValuesArray"
    .parameter "response"

    #@0
    .prologue
    .line 1549
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1551
    return-void
.end method

.method public setCdmaBroadcastConfig([Lcom/android/internal/telephony/cdma/CdmaSmsBroadcastConfigInfo;Landroid/os/Message;)V
    .registers 3
    .parameter "configs"
    .parameter "response"

    #@0
    .prologue
    .line 1554
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1555
    return-void
.end method

.method public setCdmaEriVersion(ILandroid/os/Message;)V
    .registers 3
    .parameter "value"
    .parameter "result"

    #@0
    .prologue
    .line 1577
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1578
    return-void
.end method

.method public setCdmaFactoryReset(Landroid/os/Message;)V
    .registers 4
    .parameter "result"

    #@0
    .prologue
    .line 1639
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1640
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@a
    .line 1641
    return-void
.end method

.method public setCdmaRoamingPreference(ILandroid/os/Message;)V
    .registers 5
    .parameter "cdmaRoamingType"
    .parameter "response"

    #@0
    .prologue
    .line 1474
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1475
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@a
    .line 1476
    return-void
.end method

.method public setCdmaSubscriptionSource(ILandroid/os/Message;)V
    .registers 5
    .parameter "cdmaSubscriptionType"
    .parameter "response"

    #@0
    .prologue
    .line 1464
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1465
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@a
    .line 1466
    return-void
.end method

.method public setDataSubscription(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 1654
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1655
    return-void
.end method

.method public setEhrpdIpv6ControlSetting(ILandroid/os/Message;)V
    .registers 3
    .parameter "enable"
    .parameter "result"

    #@0
    .prologue
    .line 1690
    return-void
.end method

.method public setFacilityLock(Ljava/lang/String;ZLjava/lang/String;ILandroid/os/Message;)V
    .registers 13
    .parameter "facility"
    .parameter "lockEnabled"
    .parameter "pin"
    .parameter "serviceClass"
    .parameter "result"

    #@0
    .prologue
    .line 376
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move-object v6, p5

    #@7
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/test/SimulatedCommands;->setFacilityLockForApp(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;Landroid/os/Message;)V

    #@a
    .line 377
    return-void
.end method

.method public setFacilityLockForApp(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;Landroid/os/Message;)V
    .registers 11
    .parameter "facility"
    .parameter "lockEnabled"
    .parameter "pin"
    .parameter "serviceClass"
    .parameter "appId"
    .parameter "result"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 383
    if-eqz p1, :cond_3e

    #@3
    const-string v1, "SC"

    #@5
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_3e

    #@b
    .line 385
    if-eqz p3, :cond_27

    #@d
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPinCode:Ljava/lang/String;

    #@f
    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_27

    #@15
    .line 386
    const-string v1, "SIM"

    #@17
    const-string v2, "[SimCmd] setFacilityLock: pin is valid"

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 387
    iput-boolean p2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockEnabled:Z

    #@1e
    .line 389
    if-eqz p6, :cond_26

    #@20
    .line 390
    invoke-static {p6, v3, v3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@23
    .line 391
    invoke-virtual {p6}, Landroid/os/Message;->sendToTarget()V

    #@26
    .line 434
    :cond_26
    :goto_26
    return-void

    #@27
    .line 397
    :cond_27
    if-eqz p6, :cond_26

    #@29
    .line 398
    const-string v1, "SIM"

    #@2b
    const-string v2, "[SimCmd] setFacilityLock: pin failed!"

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 400
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@32
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->GENERIC_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@34
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@37
    .line 402
    .local v0, ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p6, v3, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@3a
    .line 403
    invoke-virtual {p6}, Landroid/os/Message;->sendToTarget()V

    #@3d
    goto :goto_26

    #@3e
    .line 407
    .end local v0           #ex:Lcom/android/internal/telephony/CommandException;
    :cond_3e
    if-eqz p1, :cond_7b

    #@40
    const-string v1, "FD"

    #@42
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v1

    #@46
    if-eqz v1, :cond_7b

    #@48
    .line 409
    if-eqz p3, :cond_64

    #@4a
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPin2Code:Ljava/lang/String;

    #@4c
    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v1

    #@50
    if-eqz v1, :cond_64

    #@52
    .line 410
    const-string v1, "SIM"

    #@54
    const-string v2, "[SimCmd] setFacilityLock: pin2 is valid"

    #@56
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 411
    iput-boolean p2, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabled:Z

    #@5b
    .line 413
    if-eqz p6, :cond_26

    #@5d
    .line 414
    invoke-static {p6, v3, v3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@60
    .line 415
    invoke-virtual {p6}, Landroid/os/Message;->sendToTarget()V

    #@63
    goto :goto_26

    #@64
    .line 421
    :cond_64
    if-eqz p6, :cond_26

    #@66
    .line 422
    const-string v1, "SIM"

    #@68
    const-string v2, "[SimCmd] setFacilityLock: pin2 failed!"

    #@6a
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 424
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@6f
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->GENERIC_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@71
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@74
    .line 426
    .restart local v0       #ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p6, v3, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@77
    .line 427
    invoke-virtual {p6}, Landroid/os/Message;->sendToTarget()V

    #@7a
    goto :goto_26

    #@7b
    .line 433
    .end local v0           #ex:Lcom/android/internal/telephony/CommandException;
    :cond_7b
    invoke-direct {p0, p6}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@7e
    goto :goto_26
.end method

.method public setGsmBroadcastActivation(ZLandroid/os/Message;)V
    .registers 3
    .parameter "activate"
    .parameter "response"

    #@0
    .prologue
    .line 1563
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1564
    return-void
.end method

.method public setGsmBroadcastConfig([Lcom/android/internal/telephony/gsm/SmsBroadcastConfigInfo;Landroid/os/Message;)V
    .registers 3
    .parameter "config"
    .parameter "response"

    #@0
    .prologue
    .line 1568
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1569
    return-void
.end method

.method public setLocationUpdates(ZLandroid/os/Message;)V
    .registers 3
    .parameter "enable"
    .parameter "response"

    #@0
    .prologue
    .line 1053
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1054
    return-void
.end method

.method public setModemReset(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 809
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public setMute(ZLandroid/os/Message;)V
    .registers 3
    .parameter "enableMute"
    .parameter "result"

    #@0
    .prologue
    .line 782
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public setNetworkSelectionModeAutomatic(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 1198
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public setNetworkSelectionModeManual(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "operatorNumeric"
    .parameter "result"

    #@0
    .prologue
    .line 1201
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public setNetworkSelectionModeManual(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "operatorNumeric"
    .parameter "operatorRat"
    .parameter "result"

    #@0
    .prologue
    .line 1204
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public setNextCallFailCause(I)V
    .registers 2
    .parameter "gsmCause"

    #@0
    .prologue
    .line 1340
    iput p1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->nextCallFailCause:I

    #@2
    .line 1341
    return-void
.end method

.method public setNextDialFailImmediately(Z)V
    .registers 3
    .parameter "b"

    #@0
    .prologue
    .line 1335
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->setNextDialFailImmediately(Z)V

    #@5
    .line 1336
    return-void
.end method

.method public setPhoneType(I)V
    .registers 4
    .parameter "phoneType"

    #@0
    .prologue
    .line 1480
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1481
    return-void
.end method

.method public setPreferredNetworkType(ILandroid/os/Message;)V
    .registers 4
    .parameter "networkType"
    .parameter "result"

    #@0
    .prologue
    .line 1026
    iput p1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mNetworkType:I

    #@2
    .line 1027
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@6
    .line 1028
    return-void
.end method

.method public setPreferredVoicePrivacy(ZLandroid/os/Message;)V
    .registers 5
    .parameter "enable"
    .parameter "result"

    #@0
    .prologue
    .line 1489
    const-string v0, "SIM"

    #@2
    const-string v1, "CDMA not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1490
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@a
    .line 1491
    return-void
.end method

.method public setPreviousNetworkSelectionModeManual(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "operatorNumeric"
    .parameter "operatorRat"
    .parameter "result"

    #@0
    .prologue
    .line 1442
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public setQcril(I)V
    .registers 2
    .parameter "set"

    #@0
    .prologue
    .line 1740
    return-void
.end method

.method public setRadioPower(ZLandroid/os/Message;)V
    .registers 4
    .parameter "on"
    .parameter "result"

    #@0
    .prologue
    .line 1085
    if-eqz p1, :cond_8

    #@2
    .line 1086
    sget-object v0, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_ON:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@4
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->setRadioState(Lcom/android/internal/telephony/CommandsInterface$RadioState;)V

    #@7
    .line 1090
    :goto_7
    return-void

    #@8
    .line 1088
    :cond_8
    sget-object v0, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_OFF:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->setRadioState(Lcom/android/internal/telephony/CommandsInterface$RadioState;)V

    #@d
    goto :goto_7
.end method

.method public setRmnetAutoconnect(ILandroid/os/Message;)V
    .registers 4
    .parameter "param"
    .parameter "result"

    #@0
    .prologue
    .line 1699
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 1700
    return-void
.end method

.method public setRssiTestAntConf(ILandroid/os/Message;)V
    .registers 3
    .parameter "AntType"
    .parameter "result"

    #@0
    .prologue
    .line 813
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method public setSmscAddress(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "address"
    .parameter "result"

    #@0
    .prologue
    .line 1061
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1062
    return-void
.end method

.method public setSubscriptionMode(ILandroid/os/Message;)V
    .registers 3
    .parameter "subscriptionMode"
    .parameter "response"

    #@0
    .prologue
    .line 1658
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1659
    return-void
.end method

.method public setSuppServiceNotifications(ZLandroid/os/Message;)V
    .registers 5
    .parameter "enable"
    .parameter "result"

    #@0
    .prologue
    .line 330
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 332
    if-eqz p1, :cond_11

    #@6
    iget-boolean v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSsnNotifyOn:Z

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 333
    const-string v0, "SIM"

    #@c
    const-string v1, "Supp Service Notifications already enabled!"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 336
    :cond_11
    iput-boolean p1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSsnNotifyOn:Z

    #@13
    .line 337
    return-void
.end method

.method public setTTYMode(ILandroid/os/Message;)V
    .registers 5
    .parameter "ttyMode"
    .parameter "response"

    #@0
    .prologue
    .line 1504
    const-string v0, "SIM"

    #@2
    const-string v1, "Not implemented in SimulatedCommands"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1505
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@a
    .line 1506
    return-void
.end method

.method public setTestMode(I)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 1615
    return-void
.end method

.method public setUiccSubscription(IIIILandroid/os/Message;)V
    .registers 6
    .parameter "slotId"
    .parameter "appIndex"
    .parameter "subId"
    .parameter "subStatus"
    .parameter "response"

    #@0
    .prologue
    .line 1650
    invoke-direct {p0, p5}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1651
    return-void
.end method

.method public setupDataCall(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 9
    .parameter "radioTechnology"
    .parameter "profile"
    .parameter "apn"
    .parameter "user"
    .parameter "password"
    .parameter "authType"
    .parameter "protocol"
    .parameter "result"

    #@0
    .prologue
    .line 1020
    invoke-direct {p0, p8}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1021
    return-void
.end method

.method public shutdown()V
    .registers 3

    #@0
    .prologue
    .line 1365
    sget-object v1, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_UNAVAILABLE:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@2
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->setRadioState(Lcom/android/internal/telephony/CommandsInterface$RadioState;)V

    #@5
    .line 1366
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mHandlerThread:Landroid/os/HandlerThread;

    #@7
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@a
    move-result-object v0

    #@b
    .line 1367
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_10

    #@d
    .line 1368
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    #@10
    .line 1370
    :cond_10
    return-void
.end method

.method public startDtmf(CLandroid/os/Message;)V
    .registers 4
    .parameter "c"
    .parameter "result"

    #@0
    .prologue
    .line 961
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p2, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 962
    return-void
.end method

.method public stopDtmf(Landroid/os/Message;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 970
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@4
    .line 971
    return-void
.end method

.method public supplyDepersonalization(Ljava/lang/String;ILandroid/os/Message;)V
    .registers 4
    .parameter "netpin"
    .parameter "type"
    .parameter "result"

    #@0
    .prologue
    .line 437
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 438
    return-void
.end method

.method public supplyIccPin(Ljava/lang/String;Landroid/os/Message;)V
    .registers 8
    .parameter "pin"
    .parameter "result"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 116
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@3
    sget-object v2, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->REQUIRE_PIN:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@5
    if-eq v1, v2, :cond_2f

    #@7
    .line 117
    const-string v1, "SIM"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "[SimCmd] supplyIccPin: wrong state, state="

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 119
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@23
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@25
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@28
    .line 121
    .local v0, ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p2, v4, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@2b
    .line 122
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@2e
    .line 155
    .end local v0           #ex:Lcom/android/internal/telephony/CommandException;
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 126
    :cond_2f
    if-eqz p1, :cond_55

    #@31
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPinCode:Ljava/lang/String;

    #@33
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_55

    #@39
    .line 127
    const-string v1, "SIM"

    #@3b
    const-string v2, "[SimCmd] supplyIccPin: success!"

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 128
    const/4 v1, 0x0

    #@41
    iput v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPinUnlockAttempts:I

    #@43
    .line 129
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@45
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@47
    .line 130
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mIccStatusChangedRegistrants:Landroid/os/RegistrantList;

    #@49
    invoke-virtual {v1}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@4c
    .line 132
    if-eqz p2, :cond_2e

    #@4e
    .line 133
    invoke-static {p2, v4, v4}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@51
    .line 134
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@54
    goto :goto_2e

    #@55
    .line 140
    :cond_55
    if-eqz p2, :cond_2e

    #@57
    .line 141
    iget v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPinUnlockAttempts:I

    #@59
    add-int/lit8 v1, v1, 0x1

    #@5b
    iput v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPinUnlockAttempts:I

    #@5d
    .line 143
    const-string v1, "SIM"

    #@5f
    new-instance v2, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v3, "[SimCmd] supplyIccPin: failed! attempt="

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    iget v3, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPinUnlockAttempts:I

    #@6c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v2

    #@70
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v2

    #@74
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 145
    iget v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPinUnlockAttempts:I

    #@79
    const/4 v2, 0x3

    #@7a
    if-lt v1, v2, :cond_87

    #@7c
    .line 146
    const-string v1, "SIM"

    #@7e
    const-string v2, "[SimCmd] supplyIccPin: set state to REQUIRE_PUK"

    #@80
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 147
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->REQUIRE_PUK:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@85
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@87
    .line 150
    :cond_87
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@89
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@8b
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@8e
    .line 152
    .restart local v0       #ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p2, v4, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@91
    .line 153
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@94
    goto :goto_2e
.end method

.method public supplyIccPin2(Ljava/lang/String;Landroid/os/Message;)V
    .registers 8
    .parameter "pin2"
    .parameter "result"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 200
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabledState:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@3
    sget-object v2, Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;->REQUIRE_PIN2:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@5
    if-eq v1, v2, :cond_2f

    #@7
    .line 201
    const-string v1, "SIM"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "[SimCmd] supplyIccPin2: wrong state, state="

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabledState:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 203
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@23
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@25
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@28
    .line 205
    .local v0, ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p2, v4, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@2b
    .line 206
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@2e
    .line 238
    .end local v0           #ex:Lcom/android/internal/telephony/CommandException;
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 210
    :cond_2f
    if-eqz p1, :cond_50

    #@31
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPin2Code:Ljava/lang/String;

    #@33
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_50

    #@39
    .line 211
    const-string v1, "SIM"

    #@3b
    const-string v2, "[SimCmd] supplyIccPin2: success!"

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 212
    const/4 v1, 0x0

    #@41
    iput v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPin2UnlockAttempts:I

    #@43
    .line 213
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;->NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@45
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabledState:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@47
    .line 215
    if-eqz p2, :cond_2e

    #@49
    .line 216
    invoke-static {p2, v4, v4}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@4c
    .line 217
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@4f
    goto :goto_2e

    #@50
    .line 223
    :cond_50
    if-eqz p2, :cond_2e

    #@52
    .line 224
    iget v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPin2UnlockAttempts:I

    #@54
    add-int/lit8 v1, v1, 0x1

    #@56
    iput v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPin2UnlockAttempts:I

    #@58
    .line 226
    const-string v1, "SIM"

    #@5a
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v3, "[SimCmd] supplyIccPin2: failed! attempt="

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    iget v3, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPin2UnlockAttempts:I

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    .line 228
    iget v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPin2UnlockAttempts:I

    #@74
    const/4 v2, 0x3

    #@75
    if-lt v1, v2, :cond_82

    #@77
    .line 229
    const-string v1, "SIM"

    #@79
    const-string v2, "[SimCmd] supplyIccPin2: set state to REQUIRE_PUK2"

    #@7b
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 230
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;->REQUIRE_PUK2:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@80
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabledState:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@82
    .line 233
    :cond_82
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@84
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@86
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@89
    .line 235
    .restart local v0       #ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p2, v4, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@8c
    .line 236
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@8f
    goto :goto_2e
.end method

.method public supplyIccPin2ForApp(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "pin2"
    .parameter "aid"
    .parameter "response"

    #@0
    .prologue
    .line 1592
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1593
    return-void
.end method

.method public supplyIccPinForApp(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "pin"
    .parameter "aid"
    .parameter "response"

    #@0
    .prologue
    .line 1582
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1583
    return-void
.end method

.method public supplyIccPuk(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 9
    .parameter "puk"
    .parameter "newPin"
    .parameter "result"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 158
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@3
    sget-object v2, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->REQUIRE_PUK:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@5
    if-eq v1, v2, :cond_2f

    #@7
    .line 159
    const-string v1, "SIM"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "[SimCmd] supplyIccPuk: wrong state, state="

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 161
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@23
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@25
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@28
    .line 163
    .local v0, ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p3, v4, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@2b
    .line 164
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@2e
    .line 197
    .end local v0           #ex:Lcom/android/internal/telephony/CommandException;
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 168
    :cond_2f
    if-eqz p1, :cond_55

    #@31
    const-string v1, "12345678"

    #@33
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_55

    #@39
    .line 169
    const-string v1, "SIM"

    #@3b
    const-string v2, "[SimCmd] supplyIccPuk: success!"

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 170
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@42
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@44
    .line 171
    const/4 v1, 0x0

    #@45
    iput v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPukUnlockAttempts:I

    #@47
    .line 172
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mIccStatusChangedRegistrants:Landroid/os/RegistrantList;

    #@49
    invoke-virtual {v1}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@4c
    .line 174
    if-eqz p3, :cond_2e

    #@4e
    .line 175
    invoke-static {p3, v4, v4}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@51
    .line 176
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@54
    goto :goto_2e

    #@55
    .line 182
    :cond_55
    if-eqz p3, :cond_2e

    #@57
    .line 183
    iget v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPukUnlockAttempts:I

    #@59
    add-int/lit8 v1, v1, 0x1

    #@5b
    iput v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPukUnlockAttempts:I

    #@5d
    .line 185
    const-string v1, "SIM"

    #@5f
    new-instance v2, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v3, "[SimCmd] supplyIccPuk: failed! attempt="

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    iget v3, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPukUnlockAttempts:I

    #@6c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v2

    #@70
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v2

    #@74
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 187
    iget v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPukUnlockAttempts:I

    #@79
    const/16 v2, 0xa

    #@7b
    if-lt v1, v2, :cond_88

    #@7d
    .line 188
    const-string v1, "SIM"

    #@7f
    const-string v2, "[SimCmd] supplyIccPuk: set state to SIM_PERM_LOCKED"

    #@81
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 189
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;->SIM_PERM_LOCKED:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@86
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@88
    .line 192
    :cond_88
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@8a
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@8c
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@8f
    .line 194
    .restart local v0       #ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p3, v4, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@92
    .line 195
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@95
    goto :goto_2e
.end method

.method public supplyIccPuk2(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 9
    .parameter "puk2"
    .parameter "newPin2"
    .parameter "result"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 241
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabledState:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@3
    sget-object v2, Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;->REQUIRE_PUK2:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@5
    if-eq v1, v2, :cond_2f

    #@7
    .line 242
    const-string v1, "SIM"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "[SimCmd] supplyIccPuk2: wrong state, state="

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimLockedState:Lcom/android/internal/telephony/test/SimulatedCommands$SimLockState;

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 244
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@23
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@25
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@28
    .line 246
    .local v0, ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p3, v4, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@2b
    .line 247
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@2e
    .line 279
    .end local v0           #ex:Lcom/android/internal/telephony/CommandException;
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 251
    :cond_2f
    if-eqz p1, :cond_50

    #@31
    const-string v1, "87654321"

    #@33
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_50

    #@39
    .line 252
    const-string v1, "SIM"

    #@3b
    const-string v2, "[SimCmd] supplyIccPuk2: success!"

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 253
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;->NONE:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@42
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabledState:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@44
    .line 254
    const/4 v1, 0x0

    #@45
    iput v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPuk2UnlockAttempts:I

    #@47
    .line 256
    if-eqz p3, :cond_2e

    #@49
    .line 257
    invoke-static {p3, v4, v4}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@4c
    .line 258
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@4f
    goto :goto_2e

    #@50
    .line 264
    :cond_50
    if-eqz p3, :cond_2e

    #@52
    .line 265
    iget v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPuk2UnlockAttempts:I

    #@54
    add-int/lit8 v1, v1, 0x1

    #@56
    iput v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPuk2UnlockAttempts:I

    #@58
    .line 267
    const-string v1, "SIM"

    #@5a
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v3, "[SimCmd] supplyIccPuk2: failed! attempt="

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    iget v3, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPuk2UnlockAttempts:I

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    .line 269
    iget v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mPuk2UnlockAttempts:I

    #@74
    const/16 v2, 0xa

    #@76
    if-lt v1, v2, :cond_83

    #@78
    .line 270
    const-string v1, "SIM"

    #@7a
    const-string v2, "[SimCmd] supplyIccPuk2: set state to SIM_PERM_LOCKED"

    #@7c
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 271
    sget-object v1, Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;->SIM_PERM_LOCKED:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@81
    iput-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->mSimFdnEnabledState:Lcom/android/internal/telephony/test/SimulatedCommands$SimFdnState;

    #@83
    .line 274
    :cond_83
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@85
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@87
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@8a
    .line 276
    .restart local v0       #ex:Lcom/android/internal/telephony/CommandException;
    invoke-static {p3, v4, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@8d
    .line 277
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@90
    goto :goto_2e
.end method

.method public supplyIccPuk2ForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "puk2"
    .parameter "newPin2"
    .parameter "aid"
    .parameter "response"

    #@0
    .prologue
    .line 1597
    invoke-direct {p0, p4}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1598
    return-void
.end method

.method public supplyIccPukForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "puk"
    .parameter "newPin"
    .parameter "aid"
    .parameter "response"

    #@0
    .prologue
    .line 1587
    invoke-direct {p0, p4}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1588
    return-void
.end method

.method public switchWaitingOrHoldingAndActive(Landroid/os/Message;)V
    .registers 6
    .parameter "result"

    #@0
    .prologue
    .line 652
    iget-object v1, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    const/16 v2, 0x32

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onChld(CC)Z

    #@8
    move-result v0

    #@9
    .line 654
    .local v0, success:Z
    if-nez v0, :cond_16

    #@b
    .line 655
    new-instance v1, Ljava/lang/RuntimeException;

    #@d
    const-string v2, "Hangup Error"

    #@f
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultFail(Landroid/os/Message;Ljava/lang/Throwable;)V

    #@15
    .line 659
    :goto_15
    return-void

    #@16
    .line 657
    :cond_16
    const/4 v1, 0x0

    #@17
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/test/SimulatedCommands;->resultSuccess(Landroid/os/Message;Ljava/lang/Object;)V

    #@1a
    goto :goto_15
.end method

.method public triggerHangupAll()V
    .registers 2

    #@0
    .prologue
    .line 1376
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->triggerHangupAll()Z

    #@5
    .line 1377
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCallStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@a
    .line 1378
    return-void
.end method

.method public triggerHangupBackground()V
    .registers 2

    #@0
    .prologue
    .line 1352
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->triggerHangupBackground()Z

    #@5
    .line 1353
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCallStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@a
    .line 1354
    return-void
.end method

.method public triggerHangupForeground()V
    .registers 2

    #@0
    .prologue
    .line 1345
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->triggerHangupForeground()Z

    #@5
    .line 1346
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCallStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@a
    .line 1347
    return-void
.end method

.method public triggerIncomingSMS(Ljava/lang/String;)V
    .registers 2
    .parameter "message"

    #@0
    .prologue
    .line 1383
    return-void
.end method

.method public triggerIncomingStkCcAlpha(Ljava/lang/String;)V
    .registers 3
    .parameter "alphaString"

    #@0
    .prologue
    .line 1238
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatCcAlphaRegistrant:Landroid/os/Registrant;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1239
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCatCcAlphaRegistrant:Landroid/os/Registrant;

    #@6
    invoke-virtual {v0, p1}, Landroid/os/Registrant;->notifyResult(Ljava/lang/Object;)V

    #@9
    .line 1241
    :cond_9
    return-void
.end method

.method public triggerIncomingUssd(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "statusCode"
    .parameter "message"

    #@0
    .prologue
    .line 1254
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mUSSDRegistrant:Landroid/os/Registrant;

    #@2
    if-eqz v1, :cond_12

    #@4
    .line 1255
    const/4 v1, 0x2

    #@5
    new-array v0, v1, [Ljava/lang/String;

    #@7
    const/4 v1, 0x0

    #@8
    aput-object p1, v0, v1

    #@a
    const/4 v1, 0x1

    #@b
    aput-object p2, v0, v1

    #@d
    .line 1256
    .local v0, result:[Ljava/lang/String;
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mUSSDRegistrant:Landroid/os/Registrant;

    #@f
    invoke-virtual {v1, v0}, Landroid/os/Registrant;->notifyResult(Ljava/lang/Object;)V

    #@12
    .line 1258
    .end local v0           #result:[Ljava/lang/String;
    :cond_12
    return-void
.end method

.method public triggerRing(Ljava/lang/String;)V
    .registers 3
    .parameter "number"

    #@0
    .prologue
    .line 1308
    iget-object v0, p0, Lcom/android/internal/telephony/test/SimulatedCommands;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->triggerRing(Ljava/lang/String;)Z

    #@5
    .line 1309
    iget-object v0, p0, Lcom/android/internal/telephony/BaseCommands;->mCallStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@a
    .line 1310
    return-void
.end method

.method public triggerSsn(II)V
    .registers 7
    .parameter "type"
    .parameter "code"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1357
    new-instance v0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    #@3
    invoke-direct {v0}, Lcom/android/internal/telephony/gsm/SuppServiceNotification;-><init>()V

    #@6
    .line 1358
    .local v0, not:Lcom/android/internal/telephony/gsm/SuppServiceNotification;
    iput p1, v0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->notificationType:I

    #@8
    .line 1359
    iput p2, v0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->code:I

    #@a
    .line 1360
    iget-object v1, p0, Lcom/android/internal/telephony/BaseCommands;->mSsnRegistrant:Landroid/os/Registrant;

    #@c
    new-instance v2, Landroid/os/AsyncResult;

    #@e
    invoke-direct {v2, v3, v0, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@14
    .line 1361
    return-void
.end method

.method public uiccAkaAuthenticate(I[B[BLandroid/os/Message;)V
    .registers 5
    .parameter "sessionId"
    .parameter "rand"
    .parameter "autn"
    .parameter "response"

    #@0
    .prologue
    .line 1714
    invoke-direct {p0, p4}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1715
    return-void
.end method

.method public uiccApplicationIO(IIILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 11
    .parameter "sessionId"
    .parameter "command"
    .parameter "fileid"
    .parameter "path"
    .parameter "p1"
    .parameter "p2"
    .parameter "p3"
    .parameter "data"
    .parameter "pin2"
    .parameter "response"

    #@0
    .prologue
    .line 1719
    invoke-direct {p0, p10}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1720
    return-void
.end method

.method public uiccDeactivateApplication(ILandroid/os/Message;)V
    .registers 3
    .parameter "sessionId"
    .parameter "response"

    #@0
    .prologue
    .line 1710
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1711
    return-void
.end method

.method public uiccGbaAuthenticateBootstrap(I[B[BLandroid/os/Message;)V
    .registers 5
    .parameter "sessionId"
    .parameter "rand"
    .parameter "autn"
    .parameter "response"

    #@0
    .prologue
    .line 1724
    invoke-direct {p0, p4}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1725
    return-void
.end method

.method public uiccGbaAuthenticateNaf(I[BLandroid/os/Message;)V
    .registers 4
    .parameter "sessionId"
    .parameter "nafId"
    .parameter "response"

    #@0
    .prologue
    .line 1728
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1729
    return-void
.end method

.method public uiccSelectApplication(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "aid"
    .parameter "response"

    #@0
    .prologue
    .line 1706
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@3
    .line 1707
    return-void
.end method

.method public writeSmsToCsim(I[BLandroid/os/Message;)V
    .registers 7
    .parameter "status"
    .parameter "pdu"
    .parameter "response"

    #@0
    .prologue
    .line 1012
    const-string v0, "SIM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Write SMS to RUIM2 with status "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1013
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@1b
    .line 1014
    return-void
.end method

.method public writeSmsToRuim(ILjava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "status"
    .parameter "pdu"
    .parameter "response"

    #@0
    .prologue
    .line 1006
    const-string v0, "SIM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Write SMS to RUIM with status "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1007
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@1b
    .line 1008
    return-void
.end method

.method public writeSmsToSim(ILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 8
    .parameter "status"
    .parameter "smsc"
    .parameter "pdu"
    .parameter "response"

    #@0
    .prologue
    .line 1001
    const-string v0, "SIM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Write SMS to SIM with status "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1002
    invoke-direct {p0, p4}, Lcom/android/internal/telephony/test/SimulatedCommands;->unimplemented(Landroid/os/Message;)V

    #@1b
    .line 1003
    return-void
.end method
