.class Lcom/android/internal/telephony/PhoneProxy$1;
.super Landroid/content/BroadcastReceiver;
.source "PhoneProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/PhoneProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/PhoneProxy;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/PhoneProxy;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1705
    iput-object p1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v7, 0x7

    #@1
    const/4 v6, 0x4

    #@2
    const/4 v3, 0x3

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 1707
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 1708
    .local v0, action:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "mPreferredNetworkBroadcastReceiver  mIntentDuplicate "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@16
    iget-boolean v2, v2, Lcom/android/internal/telephony/PhoneProxy;->mIntentDuplicate:Z

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v1}, Lcom/android/internal/telephony/PhoneProxy;->access$000(Ljava/lang/String;)V

    #@23
    .line 1709
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@25
    iget-boolean v1, v1, Lcom/android/internal/telephony/PhoneProxy;->mIntentDuplicate:Z

    #@27
    if-eqz v1, :cond_116

    #@29
    const-string v1, "SetNetworkMode_KDDI_LTE"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_116

    #@31
    .line 1710
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@33
    const-string v2, "NetworkType"

    #@35
    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@38
    move-result v2

    #@39
    iput v2, v1, Lcom/android/internal/telephony/PhoneProxy;->setPreferredNetworkType:I

    #@3b
    .line 1711
    new-instance v1, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v2, "mPreferredNetworkBroadcastReceiver  ACTION_PREFERRED_NETWORK_SET setPreferredNetworkType : "

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@48
    iget v2, v2, Lcom/android/internal/telephony/PhoneProxy;->setPreferredNetworkType:I

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-static {v1}, Lcom/android/internal/telephony/PhoneProxy;->access$000(Ljava/lang/String;)V

    #@55
    .line 1712
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@57
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->setPreferredNetworkType:I

    #@59
    packed-switch v1, :pswitch_data_132

    #@5c
    .line 1743
    :cond_5c
    :goto_5c
    new-instance v1, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v2, "mPreferredNetworkBroadcastReceiver  settingsNetworkMode "

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@69
    iget v2, v2, Lcom/android/internal/telephony/PhoneProxy;->setNetworkType:I

    #@6b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    invoke-static {v1}, Lcom/android/internal/telephony/PhoneProxy;->access$000(Ljava/lang/String;)V

    #@76
    .line 1744
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@78
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@7a
    iget v2, v2, Lcom/android/internal/telephony/PhoneProxy;->setNetworkType:I

    #@7c
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@7e
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneProxy;->mHandler:Lcom/android/internal/telephony/PhoneProxy$MyHandler;

    #@80
    invoke-virtual {v3, v5}, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->obtainMessage(I)Landroid/os/Message;

    #@83
    move-result-object v3

    #@84
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/PhoneProxy;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@87
    .line 1745
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@89
    iput-boolean v4, v1, Lcom/android/internal/telephony/PhoneProxy;->mIntentDuplicate:Z

    #@8b
    .line 1751
    :cond_8b
    :goto_8b
    return-void

    #@8c
    .line 1714
    :pswitch_8c
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@8e
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@90
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneProxy;->getPreferredNetworkMode()I

    #@93
    move-result v2

    #@94
    iput v2, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@96
    .line 1716
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@98
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@9a
    if-ne v1, v7, :cond_a3

    #@9c
    .line 1717
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@9e
    const/16 v2, 0xa

    #@a0
    iput v2, v1, Lcom/android/internal/telephony/PhoneProxy;->setNetworkType:I

    #@a2
    goto :goto_5c

    #@a3
    .line 1718
    :cond_a3
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@a5
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@a7
    if-eq v1, v6, :cond_b7

    #@a9
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@ab
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@ad
    const/4 v2, 0x5

    #@ae
    if-eq v1, v2, :cond_b7

    #@b0
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@b2
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@b4
    const/4 v2, 0x6

    #@b5
    if-ne v1, v2, :cond_be

    #@b7
    .line 1721
    :cond_b7
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@b9
    const/16 v2, 0x8

    #@bb
    iput v2, v1, Lcom/android/internal/telephony/PhoneProxy;->setNetworkType:I

    #@bd
    goto :goto_5c

    #@be
    .line 1722
    :cond_be
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@c0
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@c2
    if-eq v1, v3, :cond_d1

    #@c4
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@c6
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@c8
    if-eq v1, v5, :cond_d1

    #@ca
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@cc
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@ce
    const/4 v2, 0x2

    #@cf
    if-ne v1, v2, :cond_5c

    #@d1
    .line 1725
    :cond_d1
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@d3
    const/16 v2, 0x9

    #@d5
    iput v2, v1, Lcom/android/internal/telephony/PhoneProxy;->setNetworkType:I

    #@d7
    goto :goto_5c

    #@d8
    .line 1730
    :pswitch_d8
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@da
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@dc
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneProxy;->getPreferredNetworkMode()I

    #@df
    move-result v2

    #@e0
    iput v2, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@e2
    .line 1732
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@e4
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@e6
    const/16 v2, 0xa

    #@e8
    if-ne v1, v2, :cond_f0

    #@ea
    .line 1733
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@ec
    iput v7, v1, Lcom/android/internal/telephony/PhoneProxy;->setNetworkType:I

    #@ee
    goto/16 :goto_5c

    #@f0
    .line 1734
    :cond_f0
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@f2
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@f4
    const/16 v2, 0x8

    #@f6
    if-ne v1, v2, :cond_fe

    #@f8
    .line 1735
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@fa
    iput v6, v1, Lcom/android/internal/telephony/PhoneProxy;->setNetworkType:I

    #@fc
    goto/16 :goto_5c

    #@fe
    .line 1736
    :cond_fe
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@100
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@102
    const/16 v2, 0x9

    #@104
    if-ne v1, v2, :cond_10c

    #@106
    .line 1737
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@108
    iput v3, v1, Lcom/android/internal/telephony/PhoneProxy;->setNetworkType:I

    #@10a
    goto/16 :goto_5c

    #@10c
    .line 1738
    :cond_10c
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@10e
    iget v1, v1, Lcom/android/internal/telephony/PhoneProxy;->settingsNetworkMode:I

    #@110
    const/16 v2, 0xb

    #@112
    if-ne v1, v2, :cond_5c

    #@114
    goto/16 :goto_8b

    #@116
    .line 1747
    :cond_116
    const-string v1, "GetNetworkMode_KDDI_LTE"

    #@118
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11b
    move-result v1

    #@11c
    if-eqz v1, :cond_8b

    #@11e
    .line 1748
    const-string v1, "mPreferredNetworkBroadcastReceiver  ACTION_PREFERRED_NETWORK_GET "

    #@120
    invoke-static {v1}, Lcom/android/internal/telephony/PhoneProxy;->access$000(Ljava/lang/String;)V

    #@123
    .line 1749
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@125
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy$1;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@127
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneProxy;->mHandler:Lcom/android/internal/telephony/PhoneProxy$MyHandler;

    #@129
    invoke-virtual {v2, v4}, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->obtainMessage(I)Landroid/os/Message;

    #@12c
    move-result-object v2

    #@12d
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/PhoneProxy;->getPreferredNetworkType(Landroid/os/Message;)V

    #@130
    goto/16 :goto_8b

    #@132
    .line 1712
    :pswitch_data_132
    .packed-switch 0x0
        :pswitch_8c
        :pswitch_d8
    .end packed-switch
.end method
