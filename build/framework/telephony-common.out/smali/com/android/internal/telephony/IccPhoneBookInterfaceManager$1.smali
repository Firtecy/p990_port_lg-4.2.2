.class Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;
.super Landroid/os/Handler;
.source "IccPhoneBookInterfaceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method private notifyPending(Landroid/os/AsyncResult;)V
    .registers 4
    .parameter "ar"

    #@0
    .prologue
    .line 111
    iget-object v1, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 117
    :goto_4
    return-void

    #@5
    .line 114
    :cond_5
    iget-object v0, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@7
    check-cast v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@9
    .line 115
    .local v0, status:Ljava/util/concurrent/atomic/AtomicBoolean;
    const/4 v1, 0x1

    #@a
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@d
    .line 116
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@f
    iget-object v1, v1, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->mLock:Ljava/lang/Object;

    #@11
    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    #@14
    goto :goto_4
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 70
    iget v3, p1, Landroid/os/Message;->what:I

    #@4
    packed-switch v3, :pswitch_data_b6

    #@7
    .line 108
    :goto_7
    return-void

    #@8
    .line 72
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    check-cast v0, Landroid/os/AsyncResult;

    #@c
    .line 73
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@e
    iget-object v2, v1, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->mLock:Ljava/lang/Object;

    #@10
    monitor-enter v2

    #@11
    .line 74
    :try_start_11
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@13
    if-nez v1, :cond_60

    #@15
    .line 75
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@17
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@19
    check-cast v1, [I

    #@1b
    check-cast v1, [I

    #@1d
    iput-object v1, v3, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->recordSize:[I

    #@1f
    .line 79
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@21
    new-instance v3, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v4, "GET_RECORD_SIZE Size "

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@2e
    iget-object v4, v4, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->recordSize:[I

    #@30
    const/4 v5, 0x0

    #@31
    aget v4, v4, v5

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    const-string v4, " total "

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@3f
    iget-object v4, v4, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->recordSize:[I

    #@41
    const/4 v5, 0x1

    #@42
    aget v4, v4, v5

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    const-string v4, " #record "

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@50
    iget-object v4, v4, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->recordSize:[I

    #@52
    const/4 v5, 0x2

    #@53
    aget v4, v4, v5

    #@55
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v1, v3}, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->logd(Ljava/lang/String;)V

    #@60
    .line 83
    :cond_60
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->notifyPending(Landroid/os/AsyncResult;)V

    #@63
    .line 84
    monitor-exit v2

    #@64
    goto :goto_7

    #@65
    :catchall_65
    move-exception v1

    #@66
    monitor-exit v2
    :try_end_67
    .catchall {:try_start_11 .. :try_end_67} :catchall_65

    #@67
    throw v1

    #@68
    .line 87
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_68
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6a
    check-cast v0, Landroid/os/AsyncResult;

    #@6c
    .line 88
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@6e
    iget-object v3, v3, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->mLock:Ljava/lang/Object;

    #@70
    monitor-enter v3

    #@71
    .line 89
    :try_start_71
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@73
    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@75
    if-nez v5, :cond_81

    #@77
    :goto_77
    iput-boolean v1, v4, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->success:Z

    #@79
    .line 90
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->notifyPending(Landroid/os/AsyncResult;)V

    #@7c
    .line 91
    monitor-exit v3

    #@7d
    goto :goto_7

    #@7e
    :catchall_7e
    move-exception v1

    #@7f
    monitor-exit v3
    :try_end_80
    .catchall {:try_start_71 .. :try_end_80} :catchall_7e

    #@80
    throw v1

    #@81
    :cond_81
    move v1, v2

    #@82
    .line 89
    goto :goto_77

    #@83
    .line 94
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_83
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@85
    check-cast v0, Landroid/os/AsyncResult;

    #@87
    .line 95
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@89
    iget-object v2, v1, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->mLock:Ljava/lang/Object;

    #@8b
    monitor-enter v2

    #@8c
    .line 96
    :try_start_8c
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@8e
    if-nez v1, :cond_a1

    #@90
    .line 97
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@92
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@94
    check-cast v1, Ljava/util/List;

    #@96
    iput-object v1, v3, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->records:Ljava/util/List;

    #@98
    .line 104
    :cond_98
    :goto_98
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->notifyPending(Landroid/os/AsyncResult;)V

    #@9b
    .line 105
    monitor-exit v2

    #@9c
    goto/16 :goto_7

    #@9e
    :catchall_9e
    move-exception v1

    #@9f
    monitor-exit v2
    :try_end_a0
    .catchall {:try_start_8c .. :try_end_a0} :catchall_9e

    #@a0
    throw v1

    #@a1
    .line 99
    :cond_a1
    :try_start_a1
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@a3
    const-string v3, "Cannot load ADN records"

    #@a5
    invoke-virtual {v1, v3}, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->logd(Ljava/lang/String;)V

    #@a8
    .line 100
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@aa
    iget-object v1, v1, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->records:Ljava/util/List;

    #@ac
    if-eqz v1, :cond_98

    #@ae
    .line 101
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@b0
    iget-object v1, v1, Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;->records:Ljava/util/List;

    #@b2
    invoke-interface {v1}, Ljava/util/List;->clear()V
    :try_end_b5
    .catchall {:try_start_a1 .. :try_end_b5} :catchall_9e

    #@b5
    goto :goto_98

    #@b6
    .line 70
    :pswitch_data_b6
    .packed-switch 0x1
        :pswitch_8
        :pswitch_83
        :pswitch_68
    .end packed-switch
.end method
