.class abstract Lcom/android/internal/telephony/sip/SipPhoneBase;
.super Lcom/android/internal/telephony/PhoneBase;
.source "SipPhoneBase.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "SipPhone"


# instance fields
.field private mRingbackRegistrants:Landroid/os/RegistrantList;

.field private state:Lcom/android/internal/telephony/PhoneConstants$State;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/PhoneNotifier;)V
    .registers 5
    .parameter "context"
    .parameter "notifier"

    #@0
    .prologue
    .line 61
    new-instance v0, Lcom/android/internal/telephony/sip/SipCommandInterface;

    #@2
    invoke-direct {v0, p1}, Lcom/android/internal/telephony/sip/SipCommandInterface;-><init>(Landroid/content/Context;)V

    #@5
    const/4 v1, 0x0

    #@6
    invoke-direct {p0, p2, p1, v0, v1}, Lcom/android/internal/telephony/PhoneBase;-><init>(Lcom/android/internal/telephony/PhoneNotifier;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Z)V

    #@9
    .line 57
    new-instance v0, Landroid/os/RegistrantList;

    #@b
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@e
    iput-object v0, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->mRingbackRegistrants:Landroid/os/RegistrantList;

    #@10
    .line 58
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@12
    iput-object v0, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@14
    .line 62
    return-void
.end method

.method static migrate(Landroid/os/RegistrantList;Landroid/os/RegistrantList;)V
    .registers 5
    .parameter "to"
    .parameter "from"

    #@0
    .prologue
    .line 90
    invoke-virtual {p1}, Landroid/os/RegistrantList;->removeCleared()V

    #@3
    .line 91
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    invoke-virtual {p1}, Landroid/os/RegistrantList;->size()I

    #@7
    move-result v1

    #@8
    .local v1, n:I
    :goto_8
    if-ge v0, v1, :cond_16

    #@a
    .line 92
    invoke-virtual {p1, v0}, Landroid/os/RegistrantList;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v2

    #@e
    check-cast v2, Landroid/os/Registrant;

    #@10
    invoke-virtual {p0, v2}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@13
    .line 91
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_8

    #@16
    .line 94
    :cond_16
    return-void
.end method


# virtual methods
.method public activateCellBroadcastSms(ILandroid/os/Message;)V
    .registers 5
    .parameter "activate"
    .parameter "response"

    #@0
    .prologue
    .line 462
    const-string v0, "SipPhone"

    #@2
    const-string v1, "Error! This functionality is not implemented for SIP."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 463
    return-void
.end method

.method public canDial()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 213
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@4
    move-result-object v3

    #@5
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I

    #@8
    move-result v1

    #@9
    .line 214
    .local v1, serviceState:I
    const-string v3, "SipPhone"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "canDial(): serviceState = "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 215
    const/4 v3, 0x3

    #@22
    if-ne v1, v3, :cond_25

    #@24
    .line 225
    :cond_24
    :goto_24
    return v2

    #@25
    .line 217
    :cond_25
    const-string v3, "ro.telephony.disable-call"

    #@27
    const-string v4, "false"

    #@29
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 219
    .local v0, disableCall:Ljava/lang/String;
    const-string v3, "SipPhone"

    #@2f
    new-instance v4, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v5, "canDial(): disableCall = "

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 220
    const-string v3, "true"

    #@47
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v3

    #@4b
    if-nez v3, :cond_24

    #@4d
    .line 222
    const-string v3, "SipPhone"

    #@4f
    new-instance v4, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v5, "canDial(): ringingCall: "

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@5d
    move-result-object v5

    #@5e
    invoke-virtual {v5}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v4

    #@6a
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 223
    const-string v3, "SipPhone"

    #@6f
    new-instance v4, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v5, "canDial(): foregndCall: "

    #@76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@7d
    move-result-object v5

    #@7e
    invoke-virtual {v5}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@81
    move-result-object v5

    #@82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    .line 224
    const-string v3, "SipPhone"

    #@8f
    new-instance v4, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string v5, "canDial(): backgndCall: "

    #@96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v4

    #@9a
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@9d
    move-result-object v5

    #@9e
    invoke-virtual {v5}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@a1
    move-result-object v5

    #@a2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v4

    #@a6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v4

    #@aa
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    .line 225
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@b0
    move-result-object v3

    #@b1
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->isRinging()Z

    #@b4
    move-result v3

    #@b5
    if-nez v3, :cond_24

    #@b7
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@ba
    move-result-object v3

    #@bb
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@be
    move-result-object v3

    #@bf
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@c2
    move-result v3

    #@c3
    if-eqz v3, :cond_d3

    #@c5
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@c8
    move-result-object v3

    #@c9
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@cc
    move-result-object v3

    #@cd
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@d0
    move-result v3

    #@d1
    if-nez v3, :cond_24

    #@d3
    :cond_d3
    const/4 v2, 0x1

    #@d4
    goto/16 :goto_24
.end method

.method public cancelManualSearchingRequest()V
    .registers 1

    #@0
    .prologue
    .line 384
    return-void
.end method

.method public dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;
    .registers 4
    .parameter "dialString"
    .parameter "uusInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/sip/SipPhoneBase;->dial(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public disableDataConnectivity()Z
    .registers 2

    #@0
    .prologue
    .line 431
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public disableLocationUpdates()V
    .registers 1

    #@0
    .prologue
    .line 417
    return-void
.end method

.method public enableDataConnectivity()Z
    .registers 2

    #@0
    .prologue
    .line 427
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public enableLocationUpdates()V
    .registers 1

    #@0
    .prologue
    .line 414
    return-void
.end method

.method public getAllCellInfo()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/ServiceStateTracker;->getAllCellInfo()Ljava/util/List;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getAvailableNetworks(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 372
    return-void
.end method

.method public abstract getBackgroundCall()Lcom/android/internal/telephony/Call;
.end method

.method public getCallForwardingIndicator()Z
    .registers 2

    #@0
    .prologue
    .line 153
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getCallForwardingOption(ILandroid/os/Message;)V
    .registers 3
    .parameter "commandInterfaceCFReason"
    .parameter "onComplete"

    #@0
    .prologue
    .line 327
    return-void
.end method

.method public getCallWaiting(Landroid/os/Message;)V
    .registers 3
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 355
    invoke-static {p1, v0, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@4
    .line 356
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@7
    .line 357
    return-void
.end method

.method public getCellBroadcastSmsConfig(Landroid/os/Message;)V
    .registers 4
    .parameter "response"

    #@0
    .prologue
    .line 466
    const-string v0, "SipPhone"

    #@2
    const-string v1, "Error! This functionality is not implemented for SIP."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 467
    return-void
.end method

.method public getCellLocation()Landroid/telephony/CellLocation;
    .registers 2

    #@0
    .prologue
    .line 133
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getCurrentDataConnectionList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/DataConnection;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 407
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getDataActivityState()Lcom/android/internal/telephony/Phone$DataActivityState;
    .registers 2

    #@0
    .prologue
    .line 169
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->NONE:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@2
    return-object v0
.end method

.method public getDataCallList(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 404
    return-void
.end method

.method public getDataConnectionState()Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 2

    #@0
    .prologue
    .line 161
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2
    return-object v0
.end method

.method public getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 165
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2
    return-object v0
.end method

.method public getDataRoamingEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 420
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 276
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getDeviceSvn()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 280
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getEmodeInfoPage(I)Ljava/lang/String;
    .registers 3
    .parameter "EngIndex"

    #@0
    .prologue
    .line 508
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getEngineeringModeInfo(ILandroid/os/Message;)I
    .registers 4
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 507
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getEsn()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 288
    const-string v0, "SipPhone"

    #@2
    const-string v1, "[SipPhone] getEsn() is a CDMA method"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 289
    const-string v0, "0"

    #@9
    return-object v0
.end method

.method public abstract getForegroundCall()Lcom/android/internal/telephony/Call;
.end method

.method public getIPPhoneState()Z
    .registers 2

    #@0
    .prologue
    .line 520
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getIccCard()Lcom/android/internal/telephony/IccCard;
    .registers 2

    #@0
    .prologue
    .line 368
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;
    .registers 2

    #@0
    .prologue
    .line 458
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getIccPhoneBookInterfaceManager()Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
    .registers 2

    #@0
    .prologue
    .line 454
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getIccRecordsLoaded()Z
    .registers 2

    #@0
    .prologue
    .line 364
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getIccSerialNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 302
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getIccSmsInterfaceManager()Lcom/android/internal/telephony/IccSmsInterfaceManager;
    .registers 2

    #@0
    .prologue
    .line 450
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getImei()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 284
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getInternationalMdnVoiceMailNumberForVZW()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 267
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getLine1AlphaTag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 310
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getLine1Number()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 306
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getLinkProperties(Ljava/lang/String;)Landroid/net/LinkProperties;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 482
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getMeid()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 293
    const-string v0, "SipPhone"

    #@2
    const-string v1, "[SipPhone] getMeid() is a CDMA method"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 294
    const-string v0, "0"

    #@9
    return-object v0
.end method

.method public getMessageWaitingIndicator()Z
    .registers 2

    #@0
    .prologue
    .line 149
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getNeighboringCids(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 398
    return-void
.end method

.method public getOutgoingCallerIdDisplay(Landroid/os/Message;)V
    .registers 3
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 343
    invoke-static {p1, v0, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@4
    .line 344
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@7
    .line 345
    return-void
.end method

.method public getPendingMmiCodes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/android/internal/telephony/MmiCode;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 157
    new-instance v0, Ljava/util/ArrayList;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@6
    return-object v0
.end method

.method public getPhoneSubInfo()Lcom/android/internal/telephony/PhoneSubInfo;
    .registers 2

    #@0
    .prologue
    .line 446
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getPhoneType()I
    .registers 2

    #@0
    .prologue
    .line 141
    const/4 v0, 0x3

    #@1
    return v0
.end method

.method public abstract getRingingCall()Lcom/android/internal/telephony/Call;
.end method

.method public getSearchStatus(ILandroid/os/Message;)V
    .registers 3
    .parameter "ReqType"
    .parameter "response"

    #@0
    .prologue
    .line 514
    return-void
.end method

.method public getSearchStatus(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 394
    return-void
.end method

.method public getServiceState()Landroid/telephony/ServiceState;
    .registers 3

    #@0
    .prologue
    .line 119
    new-instance v0, Landroid/telephony/ServiceState;

    #@2
    invoke-direct {v0}, Landroid/telephony/ServiceState;-><init>()V

    #@5
    .line 120
    .local v0, s:Landroid/telephony/ServiceState;
    const/4 v1, 0x0

    #@6
    invoke-virtual {v0, v1}, Landroid/telephony/ServiceState;->setState(I)V

    #@9
    .line 121
    return-object v0
.end method

.method public getSignalStrength()Landroid/telephony/SignalStrength;
    .registers 2

    #@0
    .prologue
    .line 145
    new-instance v0, Landroid/telephony/SignalStrength;

    #@2
    invoke-direct {v0}, Landroid/telephony/SignalStrength;-><init>()V

    #@5
    return-object v0
.end method

.method public getState()Lcom/android/internal/telephony/PhoneConstants$State;
    .registers 2

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@2
    return-object v0
.end method

.method public getSubscriberId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 298
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getVoiceMailAlphaTag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 272
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getVoiceMailNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 262
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public handleInCallMmiCommands(Ljava/lang/String;)Z
    .registers 3
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 232
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public handlePinMmi(Ljava/lang/String;)Z
    .registers 3
    .parameter "dialString"

    #@0
    .prologue
    .line 245
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isDataConnectivityPossible()Z
    .registers 2

    #@0
    .prologue
    .line 435
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method isInCall()Z
    .registers 5

    #@0
    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@7
    move-result-object v1

    #@8
    .line 237
    .local v1, foregroundCallState:Lcom/android/internal/telephony/Call$State;
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@f
    move-result-object v0

    #@10
    .line 238
    .local v0, backgroundCallState:Lcom/android/internal/telephony/Call$State;
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@17
    move-result-object v2

    #@18
    .line 240
    .local v2, ringingCallState:Lcom/android/internal/telephony/Call$State;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@1b
    move-result v3

    #@1c
    if-nez v3, :cond_2a

    #@1e
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_2a

    #@24
    invoke-virtual {v2}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2c

    #@2a
    :cond_2a
    const/4 v3, 0x1

    #@2b
    :goto_2b
    return v3

    #@2c
    :cond_2c
    const/4 v3, 0x0

    #@2d
    goto :goto_2b
.end method

.method migrateFrom(Lcom/android/internal/telephony/sip/SipPhoneBase;)V
    .registers 4
    .parameter "from"

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->mRingbackRegistrants:Landroid/os/RegistrantList;

    #@2
    iget-object v1, p1, Lcom/android/internal/telephony/sip/SipPhoneBase;->mRingbackRegistrants:Landroid/os/RegistrantList;

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/sip/SipPhoneBase;->migrate(Landroid/os/RegistrantList;Landroid/os/RegistrantList;)V

    #@7
    .line 78
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

    #@9
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

    #@b
    invoke-static {v0, v1}, Lcom/android/internal/telephony/sip/SipPhoneBase;->migrate(Landroid/os/RegistrantList;Landroid/os/RegistrantList;)V

    #@e
    .line 79
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

    #@10
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

    #@12
    invoke-static {v0, v1}, Lcom/android/internal/telephony/sip/SipPhoneBase;->migrate(Landroid/os/RegistrantList;Landroid/os/RegistrantList;)V

    #@15
    .line 80
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mIncomingRingRegistrants:Landroid/os/RegistrantList;

    #@17
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mIncomingRingRegistrants:Landroid/os/RegistrantList;

    #@19
    invoke-static {v0, v1}, Lcom/android/internal/telephony/sip/SipPhoneBase;->migrate(Landroid/os/RegistrantList;Landroid/os/RegistrantList;)V

    #@1c
    .line 81
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@1e
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@20
    invoke-static {v0, v1}, Lcom/android/internal/telephony/sip/SipPhoneBase;->migrate(Landroid/os/RegistrantList;Landroid/os/RegistrantList;)V

    #@23
    .line 82
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mServiceStateRegistrants:Landroid/os/RegistrantList;

    #@25
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mServiceStateRegistrants:Landroid/os/RegistrantList;

    #@27
    invoke-static {v0, v1}, Lcom/android/internal/telephony/sip/SipPhoneBase;->migrate(Landroid/os/RegistrantList;Landroid/os/RegistrantList;)V

    #@2a
    .line 83
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@2c
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@2e
    invoke-static {v0, v1}, Lcom/android/internal/telephony/sip/SipPhoneBase;->migrate(Landroid/os/RegistrantList;Landroid/os/RegistrantList;)V

    #@31
    .line 84
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@33
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@35
    invoke-static {v0, v1}, Lcom/android/internal/telephony/sip/SipPhoneBase;->migrate(Landroid/os/RegistrantList;Landroid/os/RegistrantList;)V

    #@38
    .line 85
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@3a
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@3c
    invoke-static {v0, v1}, Lcom/android/internal/telephony/sip/SipPhoneBase;->migrate(Landroid/os/RegistrantList;Landroid/os/RegistrantList;)V

    #@3f
    .line 86
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

    #@41
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

    #@43
    invoke-static {v0, v1}, Lcom/android/internal/telephony/sip/SipPhoneBase;->migrate(Landroid/os/RegistrantList;Landroid/os/RegistrantList;)V

    #@46
    .line 87
    return-void
.end method

.method public needsOtaServiceProvisioning()Z
    .registers 2

    #@0
    .prologue
    .line 476
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public notifyCallForwardingIndicator()V
    .registers 2

    #@0
    .prologue
    .line 209
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/PhoneNotifier;->notifyCallForwardingChanged(Lcom/android/internal/telephony/Phone;)V

    #@5
    .line 210
    return-void
.end method

.method notifyDisconnect(Lcom/android/internal/telephony/Connection;)V
    .registers 3
    .parameter "cn"

    #@0
    .prologue
    .line 193
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@5
    .line 194
    return-void
.end method

.method notifyNewRingingConnection(Lcom/android/internal/telephony/Connection;)V
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 189
    invoke-super {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->notifyNewRingingConnectionP(Lcom/android/internal/telephony/Connection;)V

    #@3
    .line 190
    return-void
.end method

.method notifyPhoneStateChanged()V
    .registers 2

    #@0
    .prologue
    .line 176
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/PhoneNotifier;->notifyPhoneState(Lcom/android/internal/telephony/Phone;)V

    #@5
    .line 177
    return-void
.end method

.method notifyPreciseCallStateChanged()V
    .registers 1

    #@0
    .prologue
    .line 185
    invoke-super {p0}, Lcom/android/internal/telephony/PhoneBase;->notifyPreciseCallStateChangedP()V

    #@3
    .line 186
    return-void
.end method

.method notifyServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 2
    .parameter "ss"

    #@0
    .prologue
    .line 205
    invoke-super {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->notifyServiceStateChangedP(Landroid/telephony/ServiceState;)V

    #@3
    .line 206
    return-void
.end method

.method notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V
    .registers 3
    .parameter "code"

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@5
    .line 202
    return-void
.end method

.method notifyUnknownConnection()V
    .registers 2

    #@0
    .prologue
    .line 197
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p0}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@5
    .line 198
    return-void
.end method

.method protected onUpdateIccAvailability()V
    .registers 1

    #@0
    .prologue
    .line 505
    return-void
.end method

.method public registerForRingbackTone(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->mRingbackRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 99
    return-void
.end method

.method public registerForSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 4
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 253
    return-void
.end method

.method public saveClirSetting(I)V
    .registers 2
    .parameter "commandInterfaceCLIRMode"

    #@0
    .prologue
    .line 443
    return-void
.end method

.method public selectNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;)V
    .registers 3
    .parameter "network"
    .parameter "response"

    #@0
    .prologue
    .line 380
    return-void
.end method

.method public selectPreviousNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;)V
    .registers 3
    .parameter "network"
    .parameter "response"

    #@0
    .prologue
    .line 389
    return-void
.end method

.method public sendUssdResponse(Ljava/lang/String;)V
    .registers 2
    .parameter "ussdMessge"

    #@0
    .prologue
    .line 249
    return-void
.end method

.method public setCallForwardingOption(IILjava/lang/String;IILandroid/os/Message;)V
    .registers 7
    .parameter "commandInterfaceCFAction"
    .parameter "commandInterfaceCFReason"
    .parameter "dialingNumber"
    .parameter "serviceClass"
    .parameter "timerSeconds"
    .parameter "onComplete"

    #@0
    .prologue
    .line 338
    return-void
.end method

.method public setCallForwardingOption(IILjava/lang/String;ILandroid/os/Message;)V
    .registers 6
    .parameter "commandInterfaceCFAction"
    .parameter "commandInterfaceCFReason"
    .parameter "dialingNumber"
    .parameter "timerSeconds"
    .parameter "onComplete"

    #@0
    .prologue
    .line 332
    return-void
.end method

.method public setCallWaiting(ZLandroid/os/Message;)V
    .registers 5
    .parameter "enable"
    .parameter "onComplete"

    #@0
    .prologue
    .line 360
    const-string v0, "SipPhone"

    #@2
    const-string v1, "call waiting not supported"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 361
    return-void
.end method

.method public setCellBroadcastSmsConfig([ILandroid/os/Message;)V
    .registers 5
    .parameter "configValuesArray"
    .parameter "response"

    #@0
    .prologue
    .line 470
    const-string v0, "SipPhone"

    #@2
    const-string v1, "Error! This functionality is not implemented for SIP."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 471
    return-void
.end method

.method public setDataRoamingEnabled(Z)V
    .registers 2
    .parameter "enable"

    #@0
    .prologue
    .line 424
    return-void
.end method

.method public setLine1Number(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "alphaTag"
    .parameter "number"
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 315
    invoke-static {p3, v0, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@4
    .line 316
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@7
    .line 317
    return-void
.end method

.method public setNetworkSelectionModeAutomatic(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 375
    return-void
.end method

.method public setOnPostDialCharacter(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 4
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 401
    return-void
.end method

.method public setOutgoingCallerIdDisplay(ILandroid/os/Message;)V
    .registers 4
    .parameter "commandInterfaceCLIRMode"
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 350
    invoke-static {p2, v0, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@4
    .line 351
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@7
    .line 352
    return-void
.end method

.method public setRadioPower(Z)V
    .registers 2
    .parameter "power"

    #@0
    .prologue
    .line 259
    return-void
.end method

.method public setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "alphaTag"
    .parameter "voiceMailNumber"
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 322
    invoke-static {p3, v0, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@4
    .line 323
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@7
    .line 324
    return-void
.end method

.method protected startRingbackTone()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 107
    new-instance v0, Landroid/os/AsyncResult;

    #@3
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    #@5
    invoke-direct {v0, v2, v1, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@8
    .line 108
    .local v0, result:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->mRingbackRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@d
    .line 109
    return-void
.end method

.method protected stopRingbackTone()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 112
    new-instance v0, Landroid/os/AsyncResult;

    #@3
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    #@5
    invoke-direct {v0, v2, v1, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@8
    .line 113
    .local v0, result:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->mRingbackRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@d
    .line 114
    return-void
.end method

.method public unregisterForRingbackTone(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->mRingbackRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 104
    return-void
.end method

.method public unregisterForSuppServiceNotification(Landroid/os/Handler;)V
    .registers 2
    .parameter "h"

    #@0
    .prologue
    .line 256
    return-void
.end method

.method updateCurrentCarrierInProvider()Z
    .registers 2

    #@0
    .prologue
    .line 439
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method updatePhoneState()V
    .registers 5

    #@0
    .prologue
    .line 486
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@2
    .line 488
    .local v0, oldState:Lcom/android/internal/telephony/PhoneConstants$State;
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->isRinging()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_32

    #@c
    .line 489
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->RINGING:Lcom/android/internal/telephony/PhoneConstants$State;

    #@e
    iput-object v1, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@10
    .line 497
    :goto_10
    iget-object v1, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@12
    if-eq v1, v0, :cond_31

    #@14
    .line 498
    const-string v1, "SipPhone"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, " ^^^ new phone state: "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    iget-object v3, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 499
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->notifyPhoneStateChanged()V

    #@31
    .line 501
    :cond_31
    return-void

    #@32
    .line 490
    :cond_32
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->isIdle()Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_4b

    #@3c
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipPhoneBase;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->isIdle()Z

    #@43
    move-result v1

    #@44
    if-eqz v1, :cond_4b

    #@46
    .line 492
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@48
    iput-object v1, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@4a
    goto :goto_10

    #@4b
    .line 494
    :cond_4b
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@4d
    iput-object v1, p0, Lcom/android/internal/telephony/sip/SipPhoneBase;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@4f
    goto :goto_10
.end method

.method public updateServiceLocation()V
    .registers 1

    #@0
    .prologue
    .line 411
    return-void
.end method
