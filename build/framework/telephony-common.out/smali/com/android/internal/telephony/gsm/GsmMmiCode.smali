.class public final Lcom/android/internal/telephony/gsm/GsmMmiCode;
.super Landroid/os/Handler;
.source "GsmMmiCode.java"

# interfaces
.implements Lcom/android/internal/telephony/MmiCode;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gsm/GsmMmiCode$1;
    }
.end annotation


# static fields
.field static final ACTION_ACTIVATE:Ljava/lang/String; = "*"

.field static final ACTION_DEACTIVATE:Ljava/lang/String; = "#"

.field static final ACTION_ERASURE:Ljava/lang/String; = "##"

.field static final ACTION_INTERROGATE:Ljava/lang/String; = "*#"

.field static final ACTION_REGISTER:Ljava/lang/String; = "**"

.field private static final DBG:Z = true

.field private static ENABLE_PRIVACY_LOG_CALL:Z = false

.field static final END_OF_USSD_COMMAND:C = '#'

.field static final EVENT_GET_CLIR_COMPLETE:I = 0x2

.field static final EVENT_QUERY_CF_COMPLETE:I = 0x3

.field static final EVENT_QUERY_COMPLETE:I = 0x5

.field static final EVENT_SET_CFF_COMPLETE:I = 0x6

.field static final EVENT_SET_COMPLETE:I = 0x1

.field static final EVENT_USSD_CANCEL_COMPLETE:I = 0x7

.field static final EVENT_USSD_COMPLETE:I = 0x4

.field static final GLOBALDEV_CS:Ljava/lang/String; = "+19085594899"

.field static final KT_CF_STATUS_00:I = 0x0

.field static final KT_CF_STATUS_04:I = 0x4

.field static final KT_CF_STATUS_06:I = 0x6

.field static final KT_CF_STATUS_07:I = 0x7

.field static final LOG_TAG:Ljava/lang/String; = "GSM"

.field static final MATCH_GROUP_ACTION:I = 0x2

.field static final MATCH_GROUP_DIALING_NUMBER:I = 0xc

.field static final MATCH_GROUP_GLOBALDEV_DIALNUM:I = 0x5

.field static final MATCH_GROUP_GLOBALDEV_DIALPREFIX:I = 0x4

.field static final MATCH_GROUP_POUND_STRING:I = 0x1

.field static final MATCH_GROUP_PWD_CONFIRM:I = 0xb

.field static final MATCH_GROUP_SERVICE_CODE:I = 0x3

.field static final MATCH_GROUP_SIA:I = 0x5

.field static final MATCH_GROUP_SIB:I = 0x7

.field static final MATCH_GROUP_SIC:I = 0x9

.field static final MAX_LENGTH_SHORT_CODE:I = 0x2

.field static final SC_BAIC:Ljava/lang/String; = "35"

.field static final SC_BAICr:Ljava/lang/String; = "351"

.field static final SC_BAOC:Ljava/lang/String; = "33"

.field static final SC_BAOIC:Ljava/lang/String; = "331"

.field static final SC_BAOICxH:Ljava/lang/String; = "332"

.field static final SC_BA_ALL:Ljava/lang/String; = "330"

.field static final SC_BA_MO:Ljava/lang/String; = "333"

.field static final SC_BA_MT:Ljava/lang/String; = "353"

.field static final SC_CFB:Ljava/lang/String; = "67"

.field static final SC_CFNR:Ljava/lang/String; = "62"

.field static final SC_CFNRy:Ljava/lang/String; = "61"

.field static final SC_CFU:Ljava/lang/String; = "21"

.field static final SC_CF_All:Ljava/lang/String; = "002"

.field static final SC_CF_All_Conditional:Ljava/lang/String; = "004"

.field static final SC_CLIP:Ljava/lang/String; = "30"

.field static final SC_CLIR:Ljava/lang/String; = "31"

.field static final SC_CLIRO_KOREA:Ljava/lang/String; = "230"

.field static final SC_CLIR_KOREA:Ljava/lang/String; = "23"

.field static final SC_CNAP:Ljava/lang/String; = "300"

.field static final SC_COLP:Ljava/lang/String; = "76"

.field static final SC_COLR:Ljava/lang/String; = "77"

.field static final SC_GLOBALDEV_CLIR_INVK:Ljava/lang/String; = "67"

.field static final SC_GLOBALDEV_CLIR_SUPP:Ljava/lang/String; = "82"

.field static final SC_GLOBALDEV_CS:Ljava/lang/String; = "611"

.field static final SC_GLOBALDEV_VM:Ljava/lang/String; = "86"

.field static final SC_KOREA_TEMPCODE1:Ljava/lang/String; = "61"

.field static final SC_KOREA_TEMPCODE2:Ljava/lang/String; = "62"

.field static final SC_KOREA_TEMPCODE3:Ljava/lang/String; = "63"

.field static final SC_KOREA_TEMPCODE4:Ljava/lang/String; = "65"

.field static final SC_KOREA_TEMPCODE5:Ljava/lang/String; = "22"

.field static final SC_PIN:Ljava/lang/String; = "04"

.field static final SC_PIN2:Ljava/lang/String; = "042"

.field static final SC_PUK:Ljava/lang/String; = "05"

.field static final SC_PUK2:Ljava/lang/String; = "052"

.field static final SC_PWD:Ljava/lang/String; = "03"

.field static final SC_WAIT:Ljava/lang/String; = "43"

.field static sPatternSuppService:Ljava/util/regex/Pattern;

.field static sPatternSuppServiceGlobalDev:Ljava/util/regex/Pattern;

.field private static sTwoDigitNumberPattern:[Ljava/lang/String;


# instance fields
.field action:Ljava/lang/String;

.field context:Landroid/content/Context;

.field private dialString:Ljava/lang/String;

.field dialingNumber:Ljava/lang/String;

.field private isCallFwdRegister:Z

.field private isPendingUSSD:Z

.field private isSsInfo:Z

.field private isUssdRequest:Z

.field mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

.field mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

.field message:Ljava/lang/CharSequence;

.field phone:Lcom/android/internal/telephony/gsm/GSMPhone;

.field poundString:Ljava/lang/String;

.field pwd:Ljava/lang/String;

.field sc:Ljava/lang/String;

.field sia:Ljava/lang/String;

.field sib:Ljava/lang/String;

.field sic:Ljava/lang/String;

.field state:Lcom/android/internal/telephony/MmiCode$State;

.field private ussdCode:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 67
    const-string v1, "persist.service.privacy.enable"

    #@2
    const-string v0, "TMO"

    #@4
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_2a

    #@a
    const-string v0, "US"

    #@c
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_2a

    #@12
    const/4 v0, 0x0

    #@13
    :goto_13
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@16
    move-result v0

    #@17
    sput-boolean v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ENABLE_PRIVACY_LOG_CALL:Z

    #@19
    .line 200
    const-string v0, "((\\*|#|\\*#|\\*\\*|##)(\\d{2,3})(\\*([^*#]*)(\\*([^*#]*)(\\*([^*#]*)(\\*([^*#]*))?)?)?)?#)(.*)"

    #@1b
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@1e
    move-result-object v0

    #@1f
    sput-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sPatternSuppService:Ljava/util/regex/Pattern;

    #@21
    .line 213
    const-string v0, "((\\*)(\\d{2})(\\+{0,1})(\\d{0,}))"

    #@23
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@26
    move-result-object v0

    #@27
    sput-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sPatternSuppServiceGlobalDev:Ljava/util/regex/Pattern;

    #@29
    return-void

    #@2a
    .line 67
    :cond_2a
    const/4 v0, 0x1

    #@2b
    goto :goto_13
.end method

.method constructor <init>(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)V
    .registers 6
    .parameter "phone"
    .parameter "app"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 693
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getHandler()Landroid/os/Handler;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@9
    move-result-object v0

    #@a
    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@d
    .line 179
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isSsInfo:Z

    #@f
    .line 183
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isCallFwdRegister:Z

    #@11
    .line 185
    sget-object v0, Lcom/android/internal/telephony/MmiCode$State;->PENDING:Lcom/android/internal/telephony/MmiCode$State;

    #@13
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@15
    .line 190
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@17
    .line 191
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialString:Ljava/lang/String;

    #@19
    .line 694
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1b
    .line 695
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@21
    .line 696
    iput-object p2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@23
    .line 697
    if-eqz p2, :cond_2b

    #@25
    .line 698
    invoke-virtual {p2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@28
    move-result-object v0

    #@29
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@2b
    .line 700
    :cond_2b
    return-void
.end method

.method private createQueryCallBarringResultMessage(I)Ljava/lang/CharSequence;
    .registers 6
    .parameter "serviceClass"

    #@0
    .prologue
    .line 2128
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@4
    const v3, 0x1040095

    #@7
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@a
    move-result-object v2

    #@b
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@e
    .line 2130
    .local v1, sb:Ljava/lang/StringBuilder;
    const/4 v0, 0x1

    #@f
    .line 2131
    .local v0, classMask:I
    :goto_f
    const/16 v2, 0x100

    #@11
    if-gt v0, v2, :cond_28

    #@13
    .line 2134
    and-int v2, v0, p1

    #@15
    if-eqz v2, :cond_25

    #@17
    .line 2135
    const-string v2, "\n"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 2136
    and-int v2, v0, p1

    #@1e
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->serviceClassToCFString(I)Ljava/lang/CharSequence;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@25
    .line 2132
    :cond_25
    shl-int/lit8 v0, v0, 0x1

    #@27
    goto :goto_f

    #@28
    .line 2139
    :cond_28
    return-object v1
.end method

.method private createQueryCallWaitingResultMessage(I)Ljava/lang/CharSequence;
    .registers 6
    .parameter "serviceClass"

    #@0
    .prologue
    .line 2111
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@4
    const v3, 0x1040095

    #@7
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@a
    move-result-object v2

    #@b
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@e
    .line 2114
    .local v1, sb:Ljava/lang/StringBuilder;
    const/4 v0, 0x1

    #@f
    .line 2115
    .local v0, classMask:I
    :goto_f
    const/16 v2, 0x100

    #@11
    if-gt v0, v2, :cond_28

    #@13
    .line 2118
    and-int v2, v0, p1

    #@15
    if-eqz v2, :cond_25

    #@17
    .line 2119
    const-string v2, "\n"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 2120
    and-int v2, v0, p1

    #@1e
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->serviceClassToCFString(I)Ljava/lang/CharSequence;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@25
    .line 2116
    :cond_25
    shl-int/lit8 v0, v0, 0x1

    #@27
    goto :goto_f

    #@28
    .line 2123
    :cond_28
    return-object v1
.end method

.method private getActionStringFromReqType(Lcom/android/internal/telephony/gsm/SsData$RequestType;)Ljava/lang/String;
    .registers 4
    .parameter "rType"

    #@0
    .prologue
    .line 519
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$RequestType:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SsData$RequestType;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_1e

    #@b
    .line 532
    const-string v0, ""

    #@d
    :goto_d
    return-object v0

    #@e
    .line 521
    :pswitch_e
    const-string v0, "*"

    #@10
    goto :goto_d

    #@11
    .line 523
    :pswitch_11
    const-string v0, "#"

    #@13
    goto :goto_d

    #@14
    .line 525
    :pswitch_14
    const-string v0, "*#"

    #@16
    goto :goto_d

    #@17
    .line 527
    :pswitch_17
    const-string v0, "**"

    #@19
    goto :goto_d

    #@1a
    .line 529
    :pswitch_1a
    const-string v0, "##"

    #@1c
    goto :goto_d

    #@1d
    .line 519
    nop

    #@1e
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_e
        :pswitch_11
        :pswitch_17
        :pswitch_1a
        :pswitch_14
    .end packed-switch
.end method

.method private getErrorCode(Landroid/os/AsyncResult;)Ljava/lang/CharSequence;
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    .line 1386
    const-string v1, ""

    #@2
    .line 1388
    .local v1, errCode:Ljava/lang/CharSequence;
    const-string v2, "GSM"

    #@4
    const-string v3, "getErrorCode()"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 1391
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@b
    instance-of v2, v2, Lcom/android/internal/telephony/CommandException;

    #@d
    if-eqz v2, :cond_26

    #@f
    .line 1392
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@11
    check-cast v2, Lcom/android/internal/telephony/CommandException;

    #@13
    check-cast v2, Lcom/android/internal/telephony/CommandException;

    #@15
    invoke-virtual {v2}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    #@18
    move-result-object v0

    #@19
    .line 1393
    .local v0, err:Lcom/android/internal/telephony/CommandException$Error;
    sget-object v2, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$CommandException$Error:[I

    #@1b
    invoke-virtual {v0}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@1e
    move-result v3

    #@1f
    aget v2, v2, v3

    #@21
    packed-switch v2, :pswitch_data_60

    #@24
    .line 1423
    const-string v1, "0"

    #@26
    .line 1429
    .end local v0           #err:Lcom/android/internal/telephony/CommandException$Error;
    :cond_26
    :goto_26
    const-string v2, "GSM"

    #@28
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v4, "getErrorCode( return "

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    const-string v4, " )"

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 1431
    return-object v1

    #@45
    .line 1396
    .restart local v0       #err:Lcom/android/internal/telephony/CommandException$Error;
    :pswitch_45
    const-string v1, "17"

    #@47
    .line 1397
    goto :goto_26

    #@48
    .line 1399
    :pswitch_48
    const-string v1, "38"

    #@4a
    .line 1400
    goto :goto_26

    #@4b
    .line 1402
    :pswitch_4b
    const-string v1, "13"

    #@4d
    .line 1403
    goto :goto_26

    #@4e
    .line 1405
    :pswitch_4e
    const-string v1, "6"

    #@50
    .line 1406
    goto :goto_26

    #@51
    .line 1408
    :pswitch_51
    const-string v1, "5"

    #@53
    .line 1409
    goto :goto_26

    #@54
    .line 1411
    :pswitch_54
    const-string v1, "4"

    #@56
    .line 1412
    goto :goto_26

    #@57
    .line 1414
    :pswitch_57
    const-string v1, "3"

    #@59
    .line 1415
    goto :goto_26

    #@5a
    .line 1417
    :pswitch_5a
    const-string v1, "2"

    #@5c
    .line 1418
    goto :goto_26

    #@5d
    .line 1420
    :pswitch_5d
    const-string v1, "1"

    #@5f
    .line 1421
    goto :goto_26

    #@60
    .line 1393
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_45
        :pswitch_45
        :pswitch_48
        :pswitch_4b
        :pswitch_4e
        :pswitch_51
        :pswitch_54
        :pswitch_57
        :pswitch_5a
        :pswitch_5d
    .end packed-switch
.end method

.method private getErrorMessage(Landroid/os/AsyncResult;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "ar"

    #@0
    .prologue
    .line 1354
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2
    instance-of v1, v1, Lcom/android/internal/telephony/CommandException;

    #@4
    if-eqz v1, :cond_a3

    #@6
    .line 1355
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@8
    check-cast v1, Lcom/android/internal/telephony/CommandException;

    #@a
    check-cast v1, Lcom/android/internal/telephony/CommandException;

    #@c
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    #@f
    move-result-object v0

    #@10
    .line 1356
    .local v0, err:Lcom/android/internal/telephony/CommandException$Error;
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@12
    if-ne v0, v1, :cond_25

    #@14
    .line 1357
    const-string v1, "GSM"

    #@16
    const-string v2, "FDN_CHECK_FAILURE"

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 1358
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@1d
    const v2, 0x1040093

    #@20
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@23
    move-result-object v1

    #@24
    .line 1380
    .end local v0           #err:Lcom/android/internal/telephony/CommandException$Error;
    :goto_24
    return-object v1

    #@25
    .line 1359
    .restart local v0       #err:Lcom/android/internal/telephony/CommandException$Error;
    :cond_25
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@27
    if-ne v0, v1, :cond_3a

    #@29
    .line 1360
    const-string v1, "GSM"

    #@2b
    const-string v2, "USSD_MODIFIED_TO_DIAL"

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 1361
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@32
    const v2, 0x1040575

    #@35
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@38
    move-result-object v1

    #@39
    goto :goto_24

    #@3a
    .line 1362
    :cond_3a
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

    #@3c
    if-ne v0, v1, :cond_4f

    #@3e
    .line 1363
    const-string v1, "GSM"

    #@40
    const-string v2, "USSD_MODIFIED_TO_SS"

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 1364
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@47
    const v2, 0x1040576

    #@4a
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@4d
    move-result-object v1

    #@4e
    goto :goto_24

    #@4f
    .line 1365
    :cond_4f
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

    #@51
    if-ne v0, v1, :cond_64

    #@53
    .line 1366
    const-string v1, "GSM"

    #@55
    const-string v2, "USSD_MODIFIED_TO_USSD"

    #@57
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 1367
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@5c
    const v2, 0x1040577

    #@5f
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@62
    move-result-object v1

    #@63
    goto :goto_24

    #@64
    .line 1368
    :cond_64
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@66
    if-ne v0, v1, :cond_79

    #@68
    .line 1369
    const-string v1, "GSM"

    #@6a
    const-string v2, "SS_MODIFIED_TO_DIAL"

    #@6c
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 1370
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@71
    const v2, 0x1040578

    #@74
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@77
    move-result-object v1

    #@78
    goto :goto_24

    #@79
    .line 1371
    :cond_79
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

    #@7b
    if-ne v0, v1, :cond_8e

    #@7d
    .line 1372
    const-string v1, "GSM"

    #@7f
    const-string v2, "SS_MODIFIED_TO_USSD"

    #@81
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 1373
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@86
    const v2, 0x1040579

    #@89
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@8c
    move-result-object v1

    #@8d
    goto :goto_24

    #@8e
    .line 1374
    :cond_8e
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

    #@90
    if-ne v0, v1, :cond_a3

    #@92
    .line 1375
    const-string v1, "GSM"

    #@94
    const-string v2, "SS_MODIFIED_TO_SS"

    #@96
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@99
    .line 1376
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@9b
    const v2, 0x104057a

    #@9e
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@a1
    move-result-object v1

    #@a2
    goto :goto_24

    #@a3
    .line 1380
    .end local v0           #err:Lcom/android/internal/telephony/CommandException$Error;
    :cond_a3
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@a5
    const v2, 0x1040091

    #@a8
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@ab
    move-result-object v1

    #@ac
    goto/16 :goto_24
.end method

.method private getScString()Ljava/lang/CharSequence;
    .registers 3

    #@0
    .prologue
    .line 1437
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_88

    #@4
    .line 1438
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@6
    invoke-static {v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isServiceCodeCallBarring(Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_16

    #@c
    .line 1439
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@e
    const v1, 0x10400a9

    #@11
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@14
    move-result-object v0

    #@15
    .line 1455
    :goto_15
    return-object v0

    #@16
    .line 1440
    :cond_16
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@18
    invoke-static {v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isServiceCodeCallForwarding(Ljava/lang/String;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_28

    #@1e
    .line 1441
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@20
    const v1, 0x10400a7

    #@23
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@26
    move-result-object v0

    #@27
    goto :goto_15

    #@28
    .line 1442
    :cond_28
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2a
    const-string v1, "30"

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_3c

    #@32
    .line 1443
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@34
    const v1, 0x10400a5

    #@37
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@3a
    move-result-object v0

    #@3b
    goto :goto_15

    #@3c
    .line 1444
    :cond_3c
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@3e
    const-string v1, "31"

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v0

    #@44
    if-eqz v0, :cond_50

    #@46
    .line 1445
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@48
    const v1, 0x10400a6

    #@4b
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@4e
    move-result-object v0

    #@4f
    goto :goto_15

    #@50
    .line 1446
    :cond_50
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@52
    const-string v1, "03"

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v0

    #@58
    if-eqz v0, :cond_64

    #@5a
    .line 1447
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@5c
    const v1, 0x10400aa

    #@5f
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@62
    move-result-object v0

    #@63
    goto :goto_15

    #@64
    .line 1448
    :cond_64
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@66
    const-string v1, "43"

    #@68
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v0

    #@6c
    if-eqz v0, :cond_78

    #@6e
    .line 1449
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@70
    const v1, 0x10400a8

    #@73
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@76
    move-result-object v0

    #@77
    goto :goto_15

    #@78
    .line 1450
    :cond_78
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPinCommand()Z

    #@7b
    move-result v0

    #@7c
    if-eqz v0, :cond_88

    #@7e
    .line 1451
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@80
    const v1, 0x10400ab

    #@83
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@86
    move-result-object v0

    #@87
    goto :goto_15

    #@88
    .line 1455
    :cond_88
    const-string v0, ""

    #@8a
    goto :goto_15
.end method

.method private getScStringFromScType(Lcom/android/internal/telephony/gsm/SsData$ServiceType;)Ljava/lang/String;
    .registers 4
    .parameter "sType"

    #@0
    .prologue
    .line 478
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$ServiceType:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_42

    #@b
    .line 515
    const-string v0, ""

    #@d
    :goto_d
    return-object v0

    #@e
    .line 480
    :pswitch_e
    const-string v0, "21"

    #@10
    goto :goto_d

    #@11
    .line 482
    :pswitch_11
    const-string v0, "67"

    #@13
    goto :goto_d

    #@14
    .line 484
    :pswitch_14
    const-string v0, "61"

    #@16
    goto :goto_d

    #@17
    .line 486
    :pswitch_17
    const-string v0, "62"

    #@19
    goto :goto_d

    #@1a
    .line 488
    :pswitch_1a
    const-string v0, "002"

    #@1c
    goto :goto_d

    #@1d
    .line 490
    :pswitch_1d
    const-string v0, "004"

    #@1f
    goto :goto_d

    #@20
    .line 492
    :pswitch_20
    const-string v0, "30"

    #@22
    goto :goto_d

    #@23
    .line 494
    :pswitch_23
    const-string v0, "31"

    #@25
    goto :goto_d

    #@26
    .line 496
    :pswitch_26
    const-string v0, "43"

    #@28
    goto :goto_d

    #@29
    .line 498
    :pswitch_29
    const-string v0, "33"

    #@2b
    goto :goto_d

    #@2c
    .line 500
    :pswitch_2c
    const-string v0, "331"

    #@2e
    goto :goto_d

    #@2f
    .line 502
    :pswitch_2f
    const-string v0, "332"

    #@31
    goto :goto_d

    #@32
    .line 504
    :pswitch_32
    const-string v0, "35"

    #@34
    goto :goto_d

    #@35
    .line 506
    :pswitch_35
    const-string v0, "351"

    #@37
    goto :goto_d

    #@38
    .line 508
    :pswitch_38
    const-string v0, "330"

    #@3a
    goto :goto_d

    #@3b
    .line 510
    :pswitch_3b
    const-string v0, "333"

    #@3d
    goto :goto_d

    #@3e
    .line 512
    :pswitch_3e
    const-string v0, "353"

    #@40
    goto :goto_d

    #@41
    .line 478
    nop

    #@42
    :pswitch_data_42
    .packed-switch 0x1
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
        :pswitch_20
        :pswitch_23
        :pswitch_26
        :pswitch_29
        :pswitch_2c
        :pswitch_2f
        :pswitch_32
        :pswitch_35
        :pswitch_38
        :pswitch_3b
        :pswitch_3e
    .end packed-switch
.end method

.method private handlePasswordError(I)V
    .registers 4
    .parameter "res"

    #@0
    .prologue
    .line 1212
    sget-object v1, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@2
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@4
    .line 1213
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getScString()Ljava/lang/CharSequence;

    #@9
    move-result-object v1

    #@a
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@d
    .line 1214
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "\n"

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    .line 1215
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@14
    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@1b
    .line 1216
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@1d
    .line 1217
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1f
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@22
    .line 1218
    return-void
.end method

.method private static isEmptyOrNull(Ljava/lang/CharSequence;)Z
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 555
    if-eqz p0, :cond_8

    #@2
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private isServiceClassVoiceorNone(I)Z
    .registers 3
    .parameter "serviceClass"

    #@0
    .prologue
    .line 536
    and-int/lit8 v0, p1, 0x1

    #@2
    if-nez v0, :cond_6

    #@4
    if-nez p1, :cond_8

    #@6
    :cond_6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method static isServiceCodeCallBarring(Ljava/lang/String;)Z
    .registers 2
    .parameter "sc"

    #@0
    .prologue
    .line 642
    if-eqz p0, :cond_44

    #@2
    const-string v0, "33"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_42

    #@a
    const-string v0, "331"

    #@c
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_42

    #@12
    const-string v0, "332"

    #@14
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_42

    #@1a
    const-string v0, "35"

    #@1c
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_42

    #@22
    const-string v0, "351"

    #@24
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v0

    #@28
    if-nez v0, :cond_42

    #@2a
    const-string v0, "330"

    #@2c
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v0

    #@30
    if-nez v0, :cond_42

    #@32
    const-string v0, "333"

    #@34
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v0

    #@38
    if-nez v0, :cond_42

    #@3a
    const-string v0, "353"

    #@3c
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v0

    #@40
    if-eqz v0, :cond_44

    #@42
    :cond_42
    const/4 v0, 0x1

    #@43
    :goto_43
    return v0

    #@44
    :cond_44
    const/4 v0, 0x0

    #@45
    goto :goto_43
.end method

.method static isServiceCodeCallForwarding(Ljava/lang/String;)Z
    .registers 2
    .parameter "sc"

    #@0
    .prologue
    .line 633
    if-eqz p0, :cond_34

    #@2
    const-string v0, "21"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_32

    #@a
    const-string v0, "67"

    #@c
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_32

    #@12
    const-string v0, "61"

    #@14
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_32

    #@1a
    const-string v0, "62"

    #@1c
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_32

    #@22
    const-string v0, "002"

    #@24
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v0

    #@28
    if-nez v0, :cond_32

    #@2a
    const-string v0, "004"

    #@2c
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_34

    #@32
    :cond_32
    const/4 v0, 0x1

    #@33
    :goto_33
    return v0

    #@34
    :cond_34
    const/4 v0, 0x0

    #@35
    goto :goto_33
.end method

.method static isServiceCodeUnsupported(Ljava/lang/String;)Z
    .registers 2
    .parameter "sc"

    #@0
    .prologue
    .line 655
    if-eqz p0, :cond_1c

    #@2
    const-string v0, "76"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_1a

    #@a
    const-string v0, "77"

    #@c
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_1a

    #@12
    const-string v0, "300"

    #@14
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1c

    #@1a
    :cond_1a
    const/4 v0, 0x1

    #@1b
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method private static isShortCode(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;)Z
    .registers 4
    .parameter "dialString"
    .parameter "phone"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 800
    if-nez p0, :cond_4

    #@3
    .line 815
    :cond_3
    :goto_3
    return v0

    #@4
    .line 808
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_3

    #@a
    .line 812
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@d
    move-result-object v1

    #@e
    invoke-static {p0, v1}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_3

    #@14
    .line 815
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isShortCodeUSSD(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;)Z

    #@17
    move-result v0

    #@18
    goto :goto_3
.end method

.method private static isShortCodeUSSD(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;)Z
    .registers 7
    .parameter "dialString"
    .parameter "phone"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v0, 0x0

    #@3
    .line 842
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2, p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->checkShortCodeCall(Landroid/content/Context;Ljava/lang/String;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_e

    #@d
    .line 867
    :cond_d
    :goto_d
    return v0

    #@e
    .line 847
    :cond_e
    if-eqz p0, :cond_d

    #@10
    .line 848
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->isInCall()Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_1e

    #@16
    .line 850
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@19
    move-result v2

    #@1a
    if-gt v2, v4, :cond_1e

    #@1c
    move v0, v1

    #@1d
    .line 851
    goto :goto_d

    #@1e
    .line 856
    :cond_1e
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@21
    move-result v2

    #@22
    if-gt v2, v4, :cond_34

    #@24
    .line 857
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@27
    move-result v2

    #@28
    add-int/lit8 v2, v2, -0x1

    #@2a
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@2d
    move-result v2

    #@2e
    const/16 v3, 0x23

    #@30
    if-ne v2, v3, :cond_34

    #@32
    move v0, v1

    #@33
    .line 858
    goto :goto_d

    #@34
    .line 862
    :cond_34
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@37
    move-result v2

    #@38
    if-gt v2, v4, :cond_d

    #@3a
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@3d
    move-result v2

    #@3e
    const/16 v3, 0x31

    #@40
    if-eq v2, v3, :cond_d

    #@42
    move v0, v1

    #@43
    .line 864
    goto :goto_d
.end method

.method private static isTwoDigitShortCode(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 10
    .parameter "context"
    .parameter "dialString"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 773
    const-string v5, "GSM"

    #@3
    const-string v6, "isTwoDigitShortCode"

    #@5
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 775
    if-eqz p1, :cond_11

    #@a
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@d
    move-result v5

    #@e
    const/4 v6, 0x2

    #@f
    if-eq v5, v6, :cond_12

    #@11
    .line 790
    :cond_11
    :goto_11
    return v4

    #@12
    .line 777
    :cond_12
    sget-object v5, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sTwoDigitNumberPattern:[Ljava/lang/String;

    #@14
    if-nez v5, :cond_23

    #@16
    .line 778
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@19
    move-result-object v5

    #@1a
    const v6, 0x1070036

    #@1d
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    sput-object v5, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sTwoDigitNumberPattern:[Ljava/lang/String;

    #@23
    .line 782
    :cond_23
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sTwoDigitNumberPattern:[Ljava/lang/String;

    #@25
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@26
    .local v3, len$:I
    const/4 v2, 0x0

    #@27
    .local v2, i$:I
    :goto_27
    if-ge v2, v3, :cond_55

    #@29
    aget-object v1, v0, v2

    #@2b
    .line 783
    .local v1, dialnumber:Ljava/lang/String;
    const-string v5, "GSM"

    #@2d
    new-instance v6, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v7, "Two Digit Number Pattern "

    #@34
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v6

    #@38
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v6

    #@40
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 784
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v5

    #@47
    if-eqz v5, :cond_52

    #@49
    .line 785
    const-string v4, "GSM"

    #@4b
    const-string v5, "Two Digit Number Pattern -true"

    #@4d
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 786
    const/4 v4, 0x1

    #@51
    goto :goto_11

    #@52
    .line 782
    :cond_52
    add-int/lit8 v2, v2, 0x1

    #@54
    goto :goto_27

    #@55
    .line 789
    .end local v1           #dialnumber:Ljava/lang/String;
    :cond_55
    const-string v5, "GSM"

    #@57
    const-string v6, "Two Digit Number Pattern -false"

    #@59
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    goto :goto_11
.end method

.method private makeCFQueryResultMessage(Lcom/android/internal/telephony/CallForwardInfo;I)Ljava/lang/CharSequence;
    .registers 15
    .parameter "info"
    .parameter "serviceClassMask"

    #@0
    .prologue
    const/4 v11, 0x7

    #@1
    const/4 v8, 0x3

    #@2
    const/4 v10, 0x2

    #@3
    const/4 v6, 0x0

    #@4
    const/4 v5, 0x1

    #@5
    .line 1796
    new-array v3, v8, [Ljava/lang/String;

    #@7
    const-string v7, "{0}"

    #@9
    aput-object v7, v3, v6

    #@b
    const-string v7, "{1}"

    #@d
    aput-object v7, v3, v5

    #@f
    const-string v7, "{2}"

    #@11
    aput-object v7, v3, v10

    #@13
    .line 1797
    .local v3, sources:[Ljava/lang/String;
    new-array v1, v8, [Ljava/lang/CharSequence;

    #@15
    .line 1803
    .local v1, destinations:[Ljava/lang/CharSequence;
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    #@17
    if-ne v7, v10, :cond_cb

    #@19
    move v2, v5

    #@1a
    .line 1807
    .local v2, needTimeTemplate:Z
    :goto_1a
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@1c
    const-string v8, "KT_CFU_FROM_JB"

    #@1e
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@21
    move-result v7

    #@22
    if-eqz v7, :cond_111

    #@24
    .line 1809
    const-string v7, "GSM"

    #@26
    new-instance v8, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v9, "KT Call Fowarding info.status="

    #@2d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v8

    #@31
    iget v9, p1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@33
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v8

    #@37
    const-string v9, " ,info = "

    #@39
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v8

    #@45
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 1811
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@4a
    if-ne v7, v11, :cond_d8

    #@4c
    .line 1813
    if-eqz v2, :cond_ce

    #@4e
    .line 1814
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@50
    const v8, 0x10400d9

    #@53
    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@56
    move-result-object v4

    #@57
    .line 1875
    .local v4, template:Ljava/lang/CharSequence;
    :goto_57
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->serviceClass:I

    #@59
    and-int/2addr v7, p2

    #@5a
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->serviceClassToCFString(I)Ljava/lang/CharSequence;

    #@5d
    move-result-object v7

    #@5e
    aput-object v7, v1, v6

    #@60
    .line 1877
    iget-object v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    #@62
    if-nez v7, :cond_6f

    #@64
    .line 1879
    const-string v7, ""

    #@66
    iput-object v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    #@68
    .line 1880
    const-string v7, "GSM"

    #@6a
    const-string v8, "makeCFQueryResultMessage() : info.number is null"

    #@6c
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 1884
    :cond_6f
    iget-object v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    #@71
    iget v8, p1, Lcom/android/internal/telephony/CallForwardInfo;->toa:I

    #@73
    invoke-static {v7, v8}, Landroid/telephony/PhoneNumberUtils;->stringFromStringAndTOA(Ljava/lang/String;I)Ljava/lang/String;

    #@76
    move-result-object v7

    #@77
    aput-object v7, v1, v5

    #@79
    .line 1885
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->timeSeconds:I

    #@7b
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@7e
    move-result-object v7

    #@7f
    aput-object v7, v1, v10

    #@81
    .line 1887
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    #@83
    if-nez v7, :cond_c6

    #@85
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->serviceClass:I

    #@87
    and-int/2addr v7, p2

    #@88
    if-ne v7, v5, :cond_c6

    #@8a
    .line 1890
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@8c
    if-ne v7, v5, :cond_15c

    #@8e
    move v0, v5

    #@8f
    .line 1892
    .local v0, cffEnabled:Z
    :goto_8f
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@91
    const-string v8, "KT_CFU_FROM_JB"

    #@93
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@96
    move-result v7

    #@97
    if-eqz v7, :cond_b6

    #@99
    .line 1893
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@9b
    if-ne v7, v11, :cond_15f

    #@9d
    move v0, v5

    #@9e
    .line 1894
    :goto_9e
    const-string v6, "GSM"

    #@a0
    new-instance v7, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v8, "KT Call Fowarding cffEnableds="

    #@a7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v7

    #@ab
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v7

    #@af
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v7

    #@b3
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    .line 1897
    :cond_b6
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@b8
    if-eqz v6, :cond_c6

    #@ba
    .line 1898
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@bc
    iget-object v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    #@be
    invoke-virtual {v6, v5, v0, v7}, Lcom/android/internal/telephony/uicc/IccRecords;->setVoiceCallForwardingFlag(IZLjava/lang/String;)V

    #@c1
    .line 1899
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@c3
    invoke-virtual {v5, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->setCallForwardingPreference(Z)V

    #@c6
    .line 1903
    .end local v0           #cffEnabled:Z
    :cond_c6
    invoke-static {v4, v3, v1}, Landroid/text/TextUtils;->replace(Ljava/lang/CharSequence;[Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@c9
    move-result-object v5

    #@ca
    return-object v5

    #@cb
    .end local v2           #needTimeTemplate:Z
    .end local v4           #template:Ljava/lang/CharSequence;
    :cond_cb
    move v2, v6

    #@cc
    .line 1803
    goto/16 :goto_1a

    #@ce
    .line 1817
    .restart local v2       #needTimeTemplate:Z
    :cond_ce
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@d0
    const v8, 0x10400d8

    #@d3
    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@d6
    move-result-object v4

    #@d7
    .restart local v4       #template:Ljava/lang/CharSequence;
    goto :goto_57

    #@d8
    .line 1821
    .end local v4           #template:Ljava/lang/CharSequence;
    :cond_d8
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@da
    if-eqz v7, :cond_e6

    #@dc
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@de
    const/4 v8, 0x4

    #@df
    if-eq v7, v8, :cond_e6

    #@e1
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@e3
    const/4 v8, 0x6

    #@e4
    if-ne v7, v8, :cond_f9

    #@e6
    :cond_e6
    iget-object v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    #@e8
    invoke-static {v7}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isEmptyOrNull(Ljava/lang/CharSequence;)Z

    #@eb
    move-result v7

    #@ec
    if-eqz v7, :cond_f9

    #@ee
    .line 1825
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@f0
    const v8, 0x10400d7

    #@f3
    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@f6
    move-result-object v4

    #@f7
    .restart local v4       #template:Ljava/lang/CharSequence;
    goto/16 :goto_57

    #@f9
    .line 1835
    .end local v4           #template:Ljava/lang/CharSequence;
    :cond_f9
    if-eqz v2, :cond_106

    #@fb
    .line 1836
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@fd
    const v8, 0x10400db

    #@100
    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@103
    move-result-object v4

    #@104
    .restart local v4       #template:Ljava/lang/CharSequence;
    goto/16 :goto_57

    #@106
    .line 1839
    .end local v4           #template:Ljava/lang/CharSequence;
    :cond_106
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@108
    const v8, 0x10400da

    #@10b
    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@10e
    move-result-object v4

    #@10f
    .restart local v4       #template:Ljava/lang/CharSequence;
    goto/16 :goto_57

    #@111
    .line 1846
    .end local v4           #template:Ljava/lang/CharSequence;
    :cond_111
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@113
    if-ne v7, v5, :cond_12d

    #@115
    .line 1847
    if-eqz v2, :cond_122

    #@117
    .line 1848
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@119
    const v8, 0x10400d9

    #@11c
    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@11f
    move-result-object v4

    #@120
    .restart local v4       #template:Ljava/lang/CharSequence;
    goto/16 :goto_57

    #@122
    .line 1851
    .end local v4           #template:Ljava/lang/CharSequence;
    :cond_122
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@124
    const v8, 0x10400d8

    #@127
    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@12a
    move-result-object v4

    #@12b
    .restart local v4       #template:Ljava/lang/CharSequence;
    goto/16 :goto_57

    #@12d
    .line 1854
    .end local v4           #template:Ljava/lang/CharSequence;
    :cond_12d
    iget v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@12f
    if-nez v7, :cond_144

    #@131
    iget-object v7, p1, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    #@133
    invoke-static {v7}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isEmptyOrNull(Ljava/lang/CharSequence;)Z

    #@136
    move-result v7

    #@137
    if-eqz v7, :cond_144

    #@139
    .line 1855
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@13b
    const v8, 0x10400d7

    #@13e
    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@141
    move-result-object v4

    #@142
    .restart local v4       #template:Ljava/lang/CharSequence;
    goto/16 :goto_57

    #@144
    .line 1861
    .end local v4           #template:Ljava/lang/CharSequence;
    :cond_144
    if-eqz v2, :cond_151

    #@146
    .line 1862
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@148
    const v8, 0x10400db

    #@14b
    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@14e
    move-result-object v4

    #@14f
    .restart local v4       #template:Ljava/lang/CharSequence;
    goto/16 :goto_57

    #@151
    .line 1865
    .end local v4           #template:Ljava/lang/CharSequence;
    :cond_151
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@153
    const v8, 0x10400da

    #@156
    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@159
    move-result-object v4

    #@15a
    .restart local v4       #template:Ljava/lang/CharSequence;
    goto/16 :goto_57

    #@15c
    :cond_15c
    move v0, v6

    #@15d
    .line 1890
    goto/16 :goto_8f

    #@15f
    .restart local v0       #cffEnabled:Z
    :cond_15f
    move v0, v6

    #@160
    .line 1893
    goto/16 :goto_9e
.end method

.method private static makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 547
    if-eqz p0, :cond_9

    #@2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_9

    #@8
    const/4 p0, 0x0

    #@9
    .line 549
    .end local p0
    :cond_9
    return-object p0
.end method

.method static newFromDialString(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;
    .registers 14
    .parameter "dialString"
    .parameter "phone"
    .parameter "app"

    #@0
    .prologue
    const/4 v10, 0x5

    #@1
    const/4 v9, 0x3

    #@2
    const/4 v8, 0x2

    #@3
    .line 271
    const/4 v4, 0x0

    #@4
    .line 274
    .local v4, ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    const/4 v6, 0x0

    #@5
    const-string v7, "vzw_gfit"

    #@7
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a
    move-result v6

    #@b
    if-eqz v6, :cond_ce

    #@d
    .line 275
    sget-object v6, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sPatternSuppServiceGlobalDev:Ljava/util/regex/Pattern;

    #@f
    invoke-virtual {v6, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@12
    move-result-object v3

    #@13
    .line 276
    .local v3, m:Ljava/util/regex/Matcher;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    #@16
    move-result v6

    #@17
    if-eqz v6, :cond_ce

    #@19
    .line 277
    new-instance v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@1b
    .end local v4           #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    invoke-direct {v4, p1, p2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)V

    #@1e
    .line 278
    .restart local v4       #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    invoke-virtual {v3, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@21
    move-result-object v6

    #@22
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v6

    #@26
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@28
    .line 279
    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@2b
    move-result-object v6

    #@2c
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    .line 280
    .local v0, DialCode:Ljava/lang/String;
    const/4 v6, 0x4

    #@31
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    .line 281
    .local v2, DialPrefix:Ljava/lang/String;
    invoke-virtual {v3, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@38
    move-result-object v6

    #@39
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    .line 282
    .local v1, DialNumber:Ljava/lang/String;
    const-string v6, "86"

    #@3f
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@42
    move-result v6

    #@43
    if-eqz v6, :cond_82

    #@45
    .line 283
    const-string v6, "86"

    #@47
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@49
    .line 285
    new-instance v6, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v7, "+1"

    #@50
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v6

    #@54
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getLine1Number()Ljava/lang/String;

    #@57
    move-result-object v7

    #@58
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v6

    #@60
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@62
    .line 287
    sget-boolean v6, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ENABLE_PRIVACY_LOG_CALL:Z

    #@64
    if-eqz v6, :cond_80

    #@66
    .line 288
    const-string v6, "GSM"

    #@68
    new-instance v7, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v8, "newFromDialString().. ret.dialingNumber = "

    #@6f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v7

    #@73
    iget-object v8, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@75
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v7

    #@79
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v7

    #@7d
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    :cond_80
    move-object v5, v4

    #@81
    .line 374
    .end local v0           #DialCode:Ljava/lang/String;
    .end local v1           #DialNumber:Ljava/lang/String;
    .end local v2           #DialPrefix:Ljava/lang/String;
    .end local v4           #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    .local v5, ret:Ljava/lang/Object;
    :goto_81
    return-object v5

    #@82
    .line 292
    .end local v5           #ret:Ljava/lang/Object;
    .restart local v0       #DialCode:Ljava/lang/String;
    .restart local v1       #DialNumber:Ljava/lang/String;
    .restart local v2       #DialPrefix:Ljava/lang/String;
    .restart local v4       #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    :cond_82
    const-string v6, "611"

    #@84
    const/4 v7, 0x0

    #@85
    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@88
    move-result-object v6

    #@89
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v6

    #@8d
    if-eqz v6, :cond_ad

    #@8f
    const-string v6, "+"

    #@91
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@94
    move-result v6

    #@95
    if-nez v6, :cond_ad

    #@97
    const-string v6, "611"

    #@99
    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@9c
    move-result-object v6

    #@9d
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a0
    move-result v6

    #@a1
    if-eqz v6, :cond_ad

    #@a3
    .line 295
    const-string v6, "611"

    #@a5
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@a7
    .line 296
    const-string v6, "+19085594899"

    #@a9
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@ab
    move-object v5, v4

    #@ac
    .line 297
    .restart local v5       #ret:Ljava/lang/Object;
    goto :goto_81

    #@ad
    .line 298
    .end local v5           #ret:Ljava/lang/Object;
    :cond_ad
    const-string v6, "67"

    #@af
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b2
    move-result v6

    #@b3
    if-eqz v6, :cond_175

    #@b5
    if-eqz v1, :cond_175

    #@b7
    .line 300
    new-instance v6, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v7, "#31#"

    #@be
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v6

    #@c2
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v6

    #@c6
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v6

    #@ca
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object p0

    #@ce
    .line 309
    .end local v0           #DialCode:Ljava/lang/String;
    .end local v1           #DialNumber:Ljava/lang/String;
    .end local v2           #DialPrefix:Ljava/lang/String;
    .end local v3           #m:Ljava/util/regex/Matcher;
    :cond_ce
    :goto_ce
    sget-object v6, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sPatternSuppService:Ljava/util/regex/Pattern;

    #@d0
    invoke-virtual {v6, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@d3
    move-result-object v3

    #@d4
    .line 312
    .restart local v3       #m:Ljava/util/regex/Matcher;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    #@d7
    move-result v6

    #@d8
    if-eqz v6, :cond_198

    #@da
    .line 313
    new-instance v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@dc
    .end local v4           #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    invoke-direct {v4, p1, p2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)V

    #@df
    .line 314
    .restart local v4       #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    const/4 v6, 0x1

    #@e0
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@e3
    move-result-object v6

    #@e4
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;

    #@e7
    move-result-object v6

    #@e8
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->poundString:Ljava/lang/String;

    #@ea
    .line 315
    invoke-virtual {v3, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@ed
    move-result-object v6

    #@ee
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;

    #@f1
    move-result-object v6

    #@f2
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@f4
    .line 316
    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@f7
    move-result-object v6

    #@f8
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;

    #@fb
    move-result-object v6

    #@fc
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@fe
    .line 317
    invoke-virtual {v3, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@101
    move-result-object v6

    #@102
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;

    #@105
    move-result-object v6

    #@106
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sia:Ljava/lang/String;

    #@108
    .line 318
    const/4 v6, 0x7

    #@109
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@10c
    move-result-object v6

    #@10d
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;

    #@110
    move-result-object v6

    #@111
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sib:Ljava/lang/String;

    #@113
    .line 319
    const/16 v6, 0x9

    #@115
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@118
    move-result-object v6

    #@119
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;

    #@11c
    move-result-object v6

    #@11d
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sic:Ljava/lang/String;

    #@11f
    .line 320
    const/16 v6, 0xb

    #@121
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@124
    move-result-object v6

    #@125
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;

    #@128
    move-result-object v6

    #@129
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->pwd:Ljava/lang/String;

    #@12b
    .line 321
    const/16 v6, 0xc

    #@12d
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@130
    move-result-object v6

    #@131
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeEmptyNull(Ljava/lang/String;)Ljava/lang/String;

    #@134
    move-result-object v6

    #@135
    iput-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@137
    .line 327
    iget-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@139
    if-eqz v6, :cond_154

    #@13b
    iget-object v6, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@13d
    const-string v7, "#"

    #@13f
    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@142
    move-result v6

    #@143
    if-eqz v6, :cond_154

    #@145
    const-string v6, "#"

    #@147
    invoke-virtual {p0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@14a
    move-result v6

    #@14b
    if-eqz v6, :cond_154

    #@14d
    .line 330
    new-instance v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@14f
    .end local v4           #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    invoke-direct {v4, p1, p2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)V

    #@152
    .line 331
    .restart local v4       #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    iput-object p0, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->poundString:Ljava/lang/String;

    #@154
    .line 335
    :cond_154
    iput-object p0, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialString:Ljava/lang/String;

    #@156
    .line 371
    :cond_156
    :goto_156
    if-eqz v4, :cond_172

    #@158
    const-string v6, "GSM"

    #@15a
    new-instance v7, Ljava/lang/StringBuilder;

    #@15c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@15f
    const-string v8, "newFromDialString().. return ret.dialingNumber = "

    #@161
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v7

    #@165
    iget-object v8, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@167
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v7

    #@16b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16e
    move-result-object v7

    #@16f
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@172
    :cond_172
    move-object v5, v4

    #@173
    .line 374
    .restart local v5       #ret:Ljava/lang/Object;
    goto/16 :goto_81

    #@175
    .line 301
    .end local v5           #ret:Ljava/lang/Object;
    .restart local v0       #DialCode:Ljava/lang/String;
    .restart local v1       #DialNumber:Ljava/lang/String;
    .restart local v2       #DialPrefix:Ljava/lang/String;
    :cond_175
    const-string v6, "82"

    #@177
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17a
    move-result v6

    #@17b
    if-eqz v6, :cond_ce

    #@17d
    if-eqz v1, :cond_ce

    #@17f
    .line 303
    new-instance v6, Ljava/lang/StringBuilder;

    #@181
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@184
    const-string v7, "*31#"

    #@186
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v6

    #@18a
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v6

    #@18e
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@191
    move-result-object v6

    #@192
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@195
    move-result-object p0

    #@196
    goto/16 :goto_ce

    #@198
    .line 341
    .end local v0           #DialCode:Ljava/lang/String;
    .end local v1           #DialNumber:Ljava/lang/String;
    .end local v2           #DialPrefix:Ljava/lang/String;
    :cond_198
    if-eqz p0, :cond_1b4

    #@19a
    const-string v6, "#"

    #@19c
    invoke-virtual {p0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@19f
    move-result v6

    #@1a0
    if-eqz v6, :cond_1b4

    #@1a2
    const-string v6, "KR"

    #@1a4
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@1a7
    move-result v6

    #@1a8
    if-nez v6, :cond_1b4

    #@1aa
    .line 347
    new-instance v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@1ac
    .end local v4           #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    invoke-direct {v4, p1, p2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)V

    #@1af
    .line 348
    .restart local v4       #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    iput-object p0, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->poundString:Ljava/lang/String;

    #@1b1
    .line 351
    iput-object p0, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialString:Ljava/lang/String;

    #@1b3
    goto :goto_156

    #@1b4
    .line 354
    :cond_1b4
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@1b7
    move-result-object v6

    #@1b8
    invoke-static {v6, p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isTwoDigitShortCode(Landroid/content/Context;Ljava/lang/String;)Z

    #@1bb
    move-result v6

    #@1bc
    if-eqz v6, :cond_1c0

    #@1be
    .line 356
    const/4 v4, 0x0

    #@1bf
    goto :goto_156

    #@1c0
    .line 358
    :cond_1c0
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isShortCode(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;)Z

    #@1c3
    move-result v6

    #@1c4
    if-eqz v6, :cond_156

    #@1c6
    const-string v6, "KR"

    #@1c8
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@1cb
    move-result v6

    #@1cc
    if-nez v6, :cond_156

    #@1ce
    .line 361
    new-instance v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@1d0
    .end local v4           #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    invoke-direct {v4, p1, p2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)V

    #@1d3
    .line 362
    .restart local v4       #ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    iput-object p0, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@1d5
    .line 365
    iput-object p0, v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialString:Ljava/lang/String;

    #@1d7
    goto/16 :goto_156
.end method

.method static newFromUssdUserInput(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;
    .registers 5
    .parameter "ussdMessge"
    .parameter "phone"
    .parameter "app"

    #@0
    .prologue
    .line 401
    new-instance v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@2
    invoke-direct {v0, p1, p2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)V

    #@5
    .line 403
    .local v0, ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    iput-object p0, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@7
    .line 404
    sget-object v1, Lcom/android/internal/telephony/MmiCode$State;->PENDING:Lcom/android/internal/telephony/MmiCode$State;

    #@9
    iput-object v1, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@b
    .line 405
    const/4 v1, 0x1

    #@c
    iput-boolean v1, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPendingUSSD:Z

    #@e
    .line 407
    return-object v0
.end method

.method static newNetworkInitiatedUssd(Ljava/lang/String;ZLcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;
    .registers 6
    .parameter "ussdMessage"
    .parameter "isUssdRequest"
    .parameter "phone"
    .parameter "app"

    #@0
    .prologue
    .line 382
    new-instance v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@2
    invoke-direct {v0, p2, p3}, Lcom/android/internal/telephony/gsm/GsmMmiCode;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)V

    #@5
    .line 384
    .local v0, ret:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    iput-object p0, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@7
    .line 385
    iput-boolean p1, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isUssdRequest:Z

    #@9
    .line 388
    if-eqz p1, :cond_13

    #@b
    .line 389
    const/4 v1, 0x1

    #@c
    iput-boolean v1, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPendingUSSD:Z

    #@e
    .line 390
    sget-object v1, Lcom/android/internal/telephony/MmiCode$State;->PENDING:Lcom/android/internal/telephony/MmiCode$State;

    #@10
    iput-object v1, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@12
    .line 395
    :goto_12
    return-object v0

    #@13
    .line 392
    :cond_13
    sget-object v1, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@15
    iput-object v1, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@17
    goto :goto_12
.end method

.method private onGetClirComplete(Landroid/os/AsyncResult;)V
    .registers 9
    .parameter "ar"

    #@0
    .prologue
    const v6, 0x10400b5

    #@3
    const v5, 0x10400b2

    #@6
    const/4 v3, 0x0

    #@7
    const/4 v4, 0x0

    #@8
    .line 1623
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getScString()Ljava/lang/CharSequence;

    #@d
    move-result-object v2

    #@e
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@11
    .line 1624
    .local v1, sb:Ljava/lang/StringBuilder;
    const-string v2, "\n"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 1626
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@18
    if-eqz v2, :cond_94

    #@1a
    .line 1627
    sget-object v2, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@1c
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@1e
    .line 1628
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getErrorMessage(Landroid/os/AsyncResult;)Ljava/lang/CharSequence;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@25
    .line 1632
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const-string v3, "*"

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getErrorCode(Landroid/os/AsyncResult;)Ljava/lang/CharSequence;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    const-string v3, "#"

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@4a
    .line 1635
    sget-boolean v2, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ENABLE_PRIVACY_LOG_CALL:Z

    #@4c
    if-eqz v2, :cond_68

    #@4e
    .line 1636
    const-string v2, "GSM"

    #@50
    new-instance v3, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v4, "onGetClirComplete exception ussd="

    #@57
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@5d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v3

    #@61
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 1750
    :cond_68
    :goto_68
    sget-boolean v2, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ENABLE_PRIVACY_LOG_CALL:Z

    #@6a
    if-eqz v2, :cond_8c

    #@6c
    .line 1751
    const-string v2, "GSM"

    #@6e
    new-instance v3, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v4, "onGetClirComplete( return "

    #@75
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v3

    #@79
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@7b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v3

    #@7f
    const-string v4, " )"

    #@81
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v3

    #@85
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v3

    #@89
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    .line 1757
    :cond_8c
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@8e
    .line 1758
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@90
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@93
    .line 1759
    return-void

    #@94
    .line 1646
    :cond_94
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@96
    check-cast v2, [I

    #@98
    move-object v0, v2

    #@99
    check-cast v0, [I

    #@9b
    .line 1649
    .local v0, clirArgs:[I
    const/4 v2, 0x1

    #@9c
    aget v2, v0, v2

    #@9e
    packed-switch v2, :pswitch_data_13a

    #@a1
    goto :goto_68

    #@a2
    .line 1651
    :pswitch_a2
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@a4
    const v3, 0x10400b6

    #@a7
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@aa
    move-result-object v2

    #@ab
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@ae
    .line 1653
    sget-object v2, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@b0
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@b2
    .line 1657
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@b4
    goto :goto_68

    #@b5
    .line 1664
    :pswitch_b5
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@b7
    const v3, 0x10400b7

    #@ba
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@bd
    move-result-object v2

    #@be
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@c1
    .line 1666
    sget-object v2, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@c3
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@c5
    .line 1670
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@c7
    goto :goto_68

    #@c8
    .line 1677
    :pswitch_c8
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@ca
    const v3, 0x1040091

    #@cd
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@d0
    move-result-object v2

    #@d1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@d4
    .line 1679
    sget-object v2, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@d6
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@d8
    .line 1683
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@da
    goto :goto_68

    #@db
    .line 1692
    :pswitch_db
    aget v2, v0, v3

    #@dd
    packed-switch v2, :pswitch_data_148

    #@e0
    .line 1695
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@e2
    invoke-virtual {v2, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@e5
    move-result-object v2

    #@e6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@e9
    .line 1707
    :goto_e9
    sget-object v2, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@eb
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@ed
    .line 1711
    const-string v2, "31*5****1#"

    #@ef
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@f1
    goto/16 :goto_68

    #@f3
    .line 1699
    :pswitch_f3
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@f5
    invoke-virtual {v2, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@f8
    move-result-object v2

    #@f9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@fc
    goto :goto_e9

    #@fd
    .line 1703
    :pswitch_fd
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@ff
    const v3, 0x10400b3

    #@102
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@105
    move-result-object v2

    #@106
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@109
    goto :goto_e9

    #@10a
    .line 1719
    :pswitch_10a
    aget v2, v0, v3

    #@10c
    packed-switch v2, :pswitch_data_150

    #@10f
    .line 1722
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@111
    invoke-virtual {v2, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@114
    move-result-object v2

    #@115
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@118
    .line 1735
    :goto_118
    sget-object v2, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@11a
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@11c
    .line 1739
    const-string v2, "31*5****2#"

    #@11e
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@120
    goto/16 :goto_68

    #@122
    .line 1726
    :pswitch_122
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@124
    const v3, 0x10400b4

    #@127
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@12a
    move-result-object v2

    #@12b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@12e
    goto :goto_118

    #@12f
    .line 1730
    :pswitch_12f
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@131
    invoke-virtual {v2, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@134
    move-result-object v2

    #@135
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@138
    goto :goto_118

    #@139
    .line 1649
    nop

    #@13a
    :pswitch_data_13a
    .packed-switch 0x0
        :pswitch_a2
        :pswitch_b5
        :pswitch_c8
        :pswitch_db
        :pswitch_10a
    .end packed-switch

    #@148
    .line 1692
    :pswitch_data_148
    .packed-switch 0x1
        :pswitch_f3
        :pswitch_fd
    .end packed-switch

    #@150
    .line 1719
    :pswitch_data_150
    .packed-switch 0x1
        :pswitch_122
        :pswitch_12f
    .end packed-switch
.end method

.method private onQueryCfComplete(Landroid/os/AsyncResult;)V
    .registers 11
    .parameter "ar"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1909
    new-instance v3, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getScString()Ljava/lang/CharSequence;

    #@6
    move-result-object v6

    #@7
    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@a
    .line 1910
    .local v3, sb:Ljava/lang/StringBuilder;
    const-string v6, "\n"

    #@c
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 1914
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@11
    const-string v7, "21"

    #@13
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v6

    #@17
    if-eqz v6, :cond_41

    #@19
    .line 1915
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@1b
    const v7, 0x2090153

    #@1e
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@21
    move-result-object v6

    #@22
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@25
    .line 1923
    :cond_25
    :goto_25
    const-string v6, "\n"

    #@27
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 1925
    iget-object v6, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2c
    if-eqz v6, :cond_86

    #@2e
    .line 1926
    sget-object v6, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@30
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@32
    .line 1927
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getErrorMessage(Landroid/os/AsyncResult;)Ljava/lang/CharSequence;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@39
    .line 1970
    :goto_39
    iput-object v3, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@3b
    .line 1971
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3d
    invoke-virtual {v6, p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@40
    .line 1973
    return-void

    #@41
    .line 1916
    :cond_41
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@43
    const-string v7, "67"

    #@45
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v6

    #@49
    if-eqz v6, :cond_58

    #@4b
    .line 1917
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@4d
    const v7, 0x2090154

    #@50
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@53
    move-result-object v6

    #@54
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@57
    goto :goto_25

    #@58
    .line 1918
    :cond_58
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@5a
    const-string v7, "61"

    #@5c
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v6

    #@60
    if-eqz v6, :cond_6f

    #@62
    .line 1919
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@64
    const v7, 0x2090155

    #@67
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@6a
    move-result-object v6

    #@6b
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@6e
    goto :goto_25

    #@6f
    .line 1920
    :cond_6f
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@71
    const-string v7, "62"

    #@73
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v6

    #@77
    if-eqz v6, :cond_25

    #@79
    .line 1921
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@7b
    const v7, 0x2090156

    #@7e
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@81
    move-result-object v6

    #@82
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@85
    goto :goto_25

    #@86
    .line 1931
    :cond_86
    iget-object v6, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@88
    check-cast v6, [Lcom/android/internal/telephony/CallForwardInfo;

    #@8a
    move-object v1, v6

    #@8b
    check-cast v1, [Lcom/android/internal/telephony/CallForwardInfo;

    #@8d
    .line 1933
    .local v1, infos:[Lcom/android/internal/telephony/CallForwardInfo;
    array-length v6, v1

    #@8e
    if-nez v6, :cond_b0

    #@90
    .line 1935
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@92
    const v7, 0x1040096

    #@95
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@98
    move-result-object v6

    #@99
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@9c
    .line 1938
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@9e
    if-eqz v6, :cond_ab

    #@a0
    .line 1939
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@a2
    invoke-virtual {v6, v8}, Lcom/android/internal/telephony/gsm/GSMPhone;->setCallForwardingPreference(Z)V

    #@a5
    .line 1940
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@a7
    const/4 v7, 0x1

    #@a8
    invoke-virtual {v6, v7, v8}, Lcom/android/internal/telephony/uicc/IccRecords;->setVoiceCallForwardingFlag(IZ)V

    #@ab
    .line 1967
    :cond_ab
    :goto_ab
    sget-object v6, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@ad
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@af
    goto :goto_39

    #@b0
    .line 1944
    :cond_b0
    new-instance v5, Landroid/text/SpannableStringBuilder;

    #@b2
    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    #@b5
    .line 1952
    .local v5, tb:Landroid/text/SpannableStringBuilder;
    const/4 v4, 0x1

    #@b6
    .line 1953
    .local v4, serviceClassMask:I
    :goto_b6
    const/16 v6, 0x100

    #@b8
    if-gt v4, v6, :cond_d9

    #@ba
    .line 1956
    const/4 v0, 0x0

    #@bb
    .local v0, i:I
    array-length v2, v1

    #@bc
    .local v2, s:I
    :goto_bc
    if-ge v0, v2, :cond_d6

    #@be
    .line 1957
    aget-object v6, v1, v0

    #@c0
    iget v6, v6, Lcom/android/internal/telephony/CallForwardInfo;->serviceClass:I

    #@c2
    and-int/2addr v6, v4

    #@c3
    if-eqz v6, :cond_d3

    #@c5
    .line 1958
    aget-object v6, v1, v0

    #@c7
    invoke-direct {p0, v6, v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->makeCFQueryResultMessage(Lcom/android/internal/telephony/CallForwardInfo;I)Ljava/lang/CharSequence;

    #@ca
    move-result-object v6

    #@cb
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@ce
    .line 1960
    const-string v6, "\n"

    #@d0
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@d3
    .line 1956
    :cond_d3
    add-int/lit8 v0, v0, 0x1

    #@d5
    goto :goto_bc

    #@d6
    .line 1954
    :cond_d6
    shl-int/lit8 v4, v4, 0x1

    #@d8
    goto :goto_b6

    #@d9
    .line 1964
    .end local v0           #i:I
    .end local v2           #s:I
    :cond_d9
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@dc
    goto :goto_ab
.end method

.method private onQueryComplete(Landroid/os/AsyncResult;)V
    .registers 13
    .parameter "ar"

    #@0
    .prologue
    const v10, 0x1040091

    #@3
    const/4 v9, 0x2

    #@4
    const/4 v8, 0x0

    #@5
    const/4 v7, 0x1

    #@6
    .line 1977
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getScString()Ljava/lang/CharSequence;

    #@b
    move-result-object v4

    #@c
    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@f
    .line 1978
    .local v3, sb:Ljava/lang/StringBuilder;
    const-string v4, "\n"

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 1980
    iget-object v4, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@16
    if-eqz v4, :cond_92

    #@18
    .line 1981
    sget-object v4, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@1a
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@1c
    .line 1982
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getErrorMessage(Landroid/os/AsyncResult;)Ljava/lang/CharSequence;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@23
    .line 1986
    new-instance v4, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    const-string v5, "*"

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getErrorCode(Landroid/os/AsyncResult;)Ljava/lang/CharSequence;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    const-string v5, "#"

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@48
    .line 1988
    sget-boolean v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ENABLE_PRIVACY_LOG_CALL:Z

    #@4a
    if-eqz v4, :cond_66

    #@4c
    .line 1989
    const-string v4, "GSM"

    #@4e
    new-instance v5, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v6, "onQueryComplete exception ussd="

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v5

    #@63
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 2098
    :cond_66
    :goto_66
    sget-boolean v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ENABLE_PRIVACY_LOG_CALL:Z

    #@68
    if-eqz v4, :cond_8a

    #@6a
    .line 2099
    const-string v4, "GSM"

    #@6c
    new-instance v5, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v6, "onQueryComplete( return "

    #@73
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@79
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v5

    #@7d
    const-string v6, " )"

    #@7f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v5

    #@83
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v5

    #@87
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 2105
    :cond_8a
    iput-object v3, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@8c
    .line 2106
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@8e
    invoke-virtual {v4, p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@91
    .line 2107
    return-void

    #@92
    .line 1996
    :cond_92
    iget-object v4, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@94
    check-cast v4, [I

    #@96
    move-object v0, v4

    #@97
    check-cast v0, [I

    #@99
    .line 2001
    .local v0, ints:[I
    sget-boolean v4, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ENABLE_PRIVACY_LOG_CALL:Z

    #@9b
    if-eqz v4, :cond_d0

    #@9d
    .line 2002
    const/4 v2, 0x0

    #@9e
    .local v2, loop:I
    :goto_9e
    array-length v4, v0

    #@9f
    if-ge v2, v4, :cond_d0

    #@a1
    .line 2003
    const-string v4, "GSM"

    #@a3
    new-instance v5, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v6, "onQueryComplete ints["

    #@aa
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v5

    #@ae
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@b1
    move-result-object v6

    #@b2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v5

    #@b6
    const-string v6, "]="

    #@b8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v5

    #@bc
    aget v6, v0, v2

    #@be
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@c1
    move-result-object v6

    #@c2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v5

    #@c6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v5

    #@ca
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cd
    .line 2002
    add-int/lit8 v2, v2, 0x1

    #@cf
    goto :goto_9e

    #@d0
    .line 2010
    .end local v2           #loop:I
    :cond_d0
    array-length v4, v0

    #@d1
    if-eqz v4, :cond_1fa

    #@d3
    .line 2014
    const/4 v4, 0x0

    #@d4
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@d6
    .line 2018
    aget v4, v0, v8

    #@d8
    if-nez v4, :cond_146

    #@da
    .line 2019
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@dc
    const v5, 0x1040096

    #@df
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@e2
    move-result-object v4

    #@e3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@e6
    .line 2027
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@e8
    const-string v5, "43"

    #@ea
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ed
    move-result v4

    #@ee
    if-eqz v4, :cond_137

    #@f0
    .line 2028
    const-string v4, "43*4#"

    #@f2
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@f4
    .line 2077
    :cond_f4
    :goto_f4
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@f6
    if-nez v4, :cond_1f4

    #@f8
    .line 2079
    new-instance v4, Ljava/lang/StringBuilder;

    #@fa
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@fd
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@ff
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v4

    #@103
    const-string v5, "*"

    #@105
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v4

    #@109
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v4

    #@10d
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@10f
    .line 2080
    array-length v1, v0

    #@110
    .line 2081
    .local v1, ints_length:I
    const/4 v2, 0x0

    #@111
    .restart local v2       #loop:I
    :goto_111
    if-ge v2, v1, :cond_1dd

    #@113
    .line 2082
    new-instance v4, Ljava/lang/StringBuilder;

    #@115
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@118
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@11a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v4

    #@11e
    const-string v5, "*"

    #@120
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v4

    #@124
    aget v5, v0, v2

    #@126
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@129
    move-result-object v5

    #@12a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v4

    #@12e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@131
    move-result-object v4

    #@132
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@134
    .line 2081
    add-int/lit8 v2, v2, 0x1

    #@136
    goto :goto_111

    #@137
    .line 2029
    .end local v1           #ints_length:I
    .end local v2           #loop:I
    :cond_137
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@139
    const-string v5, "351"

    #@13b
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13e
    move-result v4

    #@13f
    if-eqz v4, :cond_f4

    #@141
    .line 2030
    const-string v4, "351*4#"

    #@143
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@145
    goto :goto_f4

    #@146
    .line 2034
    :cond_146
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@148
    const-string v5, "43"

    #@14a
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14d
    move-result v4

    #@14e
    if-eqz v4, :cond_17b

    #@150
    .line 2037
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@152
    invoke-virtual {v4}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@155
    move-result-object v4

    #@156
    const-string v5, "nwservice"

    #@158
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@15b
    move-result v4

    #@15c
    if-nez v4, :cond_168

    #@15e
    .line 2038
    aget v4, v0, v7

    #@160
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->createQueryCallWaitingResultMessage(I)Ljava/lang/CharSequence;

    #@163
    move-result-object v4

    #@164
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@167
    goto :goto_f4

    #@168
    .line 2042
    :cond_168
    aget v4, v0, v8

    #@16a
    if-ne v4, v7, :cond_171

    #@16c
    .line 2043
    const-string v4, "43*5#"

    #@16e
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@170
    goto :goto_f4

    #@171
    .line 2044
    :cond_171
    aget v4, v0, v8

    #@173
    if-ne v4, v9, :cond_f4

    #@175
    .line 2045
    const-string v4, "43*0#"

    #@177
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@179
    goto/16 :goto_f4

    #@17b
    .line 2051
    :cond_17b
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@17d
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isServiceCodeCallBarring(Ljava/lang/String;)Z

    #@180
    move-result v4

    #@181
    if-eqz v4, :cond_1c0

    #@183
    .line 2053
    aget v4, v0, v8

    #@185
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->createQueryCallBarringResultMessage(I)Ljava/lang/CharSequence;

    #@188
    move-result-object v4

    #@189
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@18c
    .line 2056
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@18e
    const-string v5, "351"

    #@190
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@193
    move-result v4

    #@194
    if-eqz v4, :cond_f4

    #@196
    .line 2057
    aget v4, v0, v8

    #@198
    if-ne v4, v7, :cond_1a4

    #@19a
    aget v4, v0, v7

    #@19c
    if-ne v4, v7, :cond_1a4

    #@19e
    .line 2058
    const-string v4, "351**24*16*32#"

    #@1a0
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@1a2
    goto/16 :goto_f4

    #@1a4
    .line 2059
    :cond_1a4
    aget v4, v0, v8

    #@1a6
    if-ne v4, v7, :cond_1b2

    #@1a8
    aget v4, v0, v7

    #@1aa
    if-nez v4, :cond_1b2

    #@1ac
    .line 2060
    const-string v4, "351**24#"

    #@1ae
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@1b0
    goto/16 :goto_f4

    #@1b2
    .line 2061
    :cond_1b2
    aget v4, v0, v8

    #@1b4
    if-ne v4, v9, :cond_f4

    #@1b6
    aget v4, v0, v7

    #@1b8
    if-nez v4, :cond_f4

    #@1ba
    .line 2062
    const-string v4, "351*0#"

    #@1bc
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@1be
    goto/16 :goto_f4

    #@1c0
    .line 2068
    :cond_1c0
    aget v4, v0, v8

    #@1c2
    if-ne v4, v7, :cond_1d2

    #@1c4
    .line 2070
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@1c6
    const v5, 0x1040094

    #@1c9
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1cc
    move-result-object v4

    #@1cd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@1d0
    goto/16 :goto_f4

    #@1d2
    .line 2072
    :cond_1d2
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@1d4
    invoke-virtual {v4, v10}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1d7
    move-result-object v4

    #@1d8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@1db
    goto/16 :goto_f4

    #@1dd
    .line 2084
    .restart local v1       #ints_length:I
    .restart local v2       #loop:I
    :cond_1dd
    new-instance v4, Ljava/lang/StringBuilder;

    #@1df
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e2
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@1e4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e7
    move-result-object v4

    #@1e8
    const-string v5, "#"

    #@1ea
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ed
    move-result-object v4

    #@1ee
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f1
    move-result-object v4

    #@1f2
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@1f4
    .line 2092
    .end local v1           #ints_length:I
    .end local v2           #loop:I
    :cond_1f4
    :goto_1f4
    sget-object v4, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@1f6
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@1f8
    goto/16 :goto_66

    #@1fa
    .line 2090
    :cond_1fa
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@1fc
    invoke-virtual {v4, v10}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1ff
    move-result-object v4

    #@200
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@203
    goto :goto_1f4
.end method

.method private onSetComplete(Landroid/os/AsyncResult;)V
    .registers 14
    .parameter "ar"

    #@0
    .prologue
    const v11, 0x1040308

    #@3
    const v10, 0x104009b

    #@6
    const v9, 0x1040097

    #@9
    const v7, 0x1040091

    #@c
    const/4 v8, 0x0

    #@d
    .line 1460
    new-instance v5, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getScString()Ljava/lang/CharSequence;

    #@12
    move-result-object v6

    #@13
    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@16
    .line 1461
    .local v5, sb:Ljava/lang/StringBuilder;
    const-string v6, "\n"

    #@18
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    .line 1463
    iget-object v6, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    if-eqz v6, :cond_171

    #@1f
    .line 1464
    sget-object v6, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@21
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@23
    .line 1465
    iget-object v6, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@25
    instance-of v6, v6, Lcom/android/internal/telephony/CommandException;

    #@27
    if-eqz v6, :cond_166

    #@29
    .line 1466
    iget-object v6, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2b
    check-cast v6, Lcom/android/internal/telephony/CommandException;

    #@2d
    check-cast v6, Lcom/android/internal/telephony/CommandException;

    #@2f
    invoke-virtual {v6}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    #@32
    move-result-object v0

    #@33
    .line 1467
    .local v0, err:Lcom/android/internal/telephony/CommandException$Error;
    sget-object v6, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@35
    if-ne v0, v6, :cond_108

    #@37
    .line 1468
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPinCommand()Z

    #@3a
    move-result v6

    #@3b
    if-eqz v6, :cond_fa

    #@3d
    .line 1471
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@3f
    const-string v7, "05"

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v6

    #@45
    if-nez v6, :cond_51

    #@47
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@49
    const-string v7, "052"

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v6

    #@4f
    if-eqz v6, :cond_cb

    #@51
    .line 1472
    :cond_51
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@53
    const v7, 0x104009c

    #@56
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@59
    move-result-object v6

    #@5a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@5d
    .line 1475
    iget-object v6, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@5f
    if-eqz v6, :cond_80

    #@61
    iget-object v6, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@63
    check-cast v6, [I

    #@65
    check-cast v6, [I

    #@67
    array-length v6, v6

    #@68
    if-eqz v6, :cond_80

    #@6a
    .line 1476
    iget-object v6, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@6c
    check-cast v6, [I

    #@6e
    check-cast v6, [I

    #@70
    aget v4, v6, v8

    #@72
    .line 1477
    .local v4, pukAttemptsRemaining:I
    if-ltz v4, :cond_80

    #@74
    .line 1478
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@76
    invoke-virtual {v6, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@7d
    .line 1480
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    .line 1524
    .end local v4           #pukAttemptsRemaining:I
    :cond_80
    :goto_80
    new-instance v6, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@87
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v6

    #@8b
    const-string v7, "*"

    #@8d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v6

    #@91
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getErrorCode(Landroid/os/AsyncResult;)Ljava/lang/CharSequence;

    #@94
    move-result-object v7

    #@95
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v6

    #@99
    const-string v7, "#"

    #@9b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v6

    #@9f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v6

    #@a3
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@a5
    .line 1526
    sget-boolean v6, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ENABLE_PRIVACY_LOG_CALL:Z

    #@a7
    if-eqz v6, :cond_c3

    #@a9
    .line 1527
    const-string v6, "GSM"

    #@ab
    new-instance v7, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v8, "onSetComplete exception ussd="

    #@b2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v7

    #@b6
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@b8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v7

    #@bc
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v7

    #@c0
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c3
    .line 1617
    .end local v0           #err:Lcom/android/internal/telephony/CommandException$Error;
    :cond_c3
    :goto_c3
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@c5
    .line 1618
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@c7
    invoke-virtual {v6, p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@ca
    .line 1619
    return-void

    #@cb
    .line 1484
    .restart local v0       #err:Lcom/android/internal/telephony/CommandException$Error;
    :cond_cb
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@cd
    invoke-virtual {v6, v10}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@d0
    move-result-object v6

    #@d1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@d4
    .line 1487
    iget-object v6, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@d6
    if-eqz v6, :cond_80

    #@d8
    .line 1488
    iget-object v6, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@da
    check-cast v6, [I

    #@dc
    move-object v3, v6

    #@dd
    check-cast v3, [I

    #@df
    .line 1491
    .local v3, pinAttemptsRemaining:[I
    array-length v6, v3

    #@e0
    if-lez v6, :cond_80

    #@e2
    aget v6, v3, v8

    #@e4
    if-ltz v6, :cond_80

    #@e6
    .line 1492
    const-string v6, "\n"

    #@e8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    .line 1493
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@ed
    invoke-virtual {v6, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@f0
    move-result-object v6

    #@f1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@f4
    .line 1494
    aget v6, v3, v8

    #@f6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9
    goto :goto_80

    #@fa
    .line 1501
    .end local v3           #pinAttemptsRemaining:[I
    :cond_fa
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@fc
    const v7, 0x1040099

    #@ff
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@102
    move-result-object v6

    #@103
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@106
    goto/16 :goto_80

    #@108
    .line 1504
    :cond_108
    sget-object v6, Lcom/android/internal/telephony/CommandException$Error;->SIM_PUK2:Lcom/android/internal/telephony/CommandException$Error;

    #@10a
    if-ne v0, v6, :cond_128

    #@10c
    .line 1505
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@10e
    invoke-virtual {v6, v10}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@111
    move-result-object v6

    #@112
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@115
    .line 1507
    const-string v6, "\n"

    #@117
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    .line 1508
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@11c
    const v7, 0x10400a1

    #@11f
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@122
    move-result-object v6

    #@123
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@126
    goto/16 :goto_80

    #@128
    .line 1510
    :cond_128
    sget-object v6, Lcom/android/internal/telephony/CommandException$Error;->REQUEST_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@12a
    if-ne v0, v6, :cond_144

    #@12c
    .line 1511
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@12e
    const-string v7, "04"

    #@130
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@133
    move-result v6

    #@134
    if-eqz v6, :cond_80

    #@136
    .line 1512
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@138
    const v7, 0x10400a2

    #@13b
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@13e
    move-result-object v6

    #@13f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@142
    goto/16 :goto_80

    #@144
    .line 1515
    :cond_144
    sget-object v6, Lcom/android/internal/telephony/CommandException$Error;->FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@146
    if-ne v0, v6, :cond_15d

    #@148
    .line 1516
    const-string v6, "GSM"

    #@14a
    const-string v7, "FDN_CHECK_FAILURE"

    #@14c
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@14f
    .line 1517
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@151
    const v7, 0x1040093

    #@154
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@157
    move-result-object v6

    #@158
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@15b
    goto/16 :goto_80

    #@15d
    .line 1519
    :cond_15d
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getErrorMessage(Landroid/os/AsyncResult;)Ljava/lang/CharSequence;

    #@160
    move-result-object v6

    #@161
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@164
    goto/16 :goto_80

    #@166
    .line 1535
    .end local v0           #err:Lcom/android/internal/telephony/CommandException$Error;
    :cond_166
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@168
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@16b
    move-result-object v6

    #@16c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@16f
    goto/16 :goto_c3

    #@171
    .line 1538
    :cond_171
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@174
    move-result v6

    #@175
    if-eqz v6, :cond_222

    #@177
    .line 1539
    sget-object v6, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@179
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@17b
    .line 1541
    iget-boolean v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isCallFwdRegister:Z

    #@17d
    if-eqz v6, :cond_19c

    #@17f
    .line 1542
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@181
    invoke-virtual {v6, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@184
    move-result-object v6

    #@185
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@188
    .line 1543
    iput-boolean v8, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isCallFwdRegister:Z

    #@18a
    .line 1576
    :cond_18a
    :goto_18a
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@18c
    const-string v7, "31"

    #@18e
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@191
    move-result v6

    #@192
    if-eqz v6, :cond_c3

    #@194
    .line 1577
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@196
    const/4 v7, 0x1

    #@197
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/gsm/GSMPhone;->saveClirSetting(I)V

    #@19a
    goto/16 :goto_c3

    #@19c
    .line 1545
    :cond_19c
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@19e
    const v7, 0x1040094

    #@1a1
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1a4
    move-result-object v6

    #@1a5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@1a8
    .line 1549
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@1aa
    const-string v7, "43"

    #@1ac
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1af
    move-result v6

    #@1b0
    if-eqz v6, :cond_1d5

    #@1b2
    .line 1550
    const-string v6, "43*5#"

    #@1b4
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@1b6
    .line 1565
    :cond_1b6
    :goto_1b6
    sget-boolean v6, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ENABLE_PRIVACY_LOG_CALL:Z

    #@1b8
    if-eqz v6, :cond_18a

    #@1ba
    .line 1566
    const-string v6, "GSM"

    #@1bc
    new-instance v7, Ljava/lang/StringBuilder;

    #@1be
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1c1
    const-string v8, "onSetComplete Activate ussd="

    #@1c3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c6
    move-result-object v7

    #@1c7
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@1c9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v7

    #@1cd
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d0
    move-result-object v7

    #@1d1
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d4
    goto :goto_18a

    #@1d5
    .line 1551
    :cond_1d5
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@1d7
    const-string v7, "351"

    #@1d9
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1dc
    move-result v6

    #@1dd
    if-eqz v6, :cond_1b6

    #@1df
    .line 1552
    iget-object v6, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1e1
    check-cast v6, [I

    #@1e3
    move-object v1, v6

    #@1e4
    check-cast v1, [I

    #@1e6
    .line 1554
    .local v1, ints:[I
    const/4 v2, 0x0

    #@1e7
    .local v2, loop:I
    :goto_1e7
    array-length v6, v1

    #@1e8
    if-ge v2, v6, :cond_21d

    #@1ea
    .line 1556
    sget-boolean v6, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ENABLE_PRIVACY_LOG_CALL:Z

    #@1ec
    if-eqz v6, :cond_21a

    #@1ee
    .line 1557
    const-string v6, "GSM"

    #@1f0
    new-instance v7, Ljava/lang/StringBuilder;

    #@1f2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1f5
    const-string v8, "onSetComplete ints["

    #@1f7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v7

    #@1fb
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1fe
    move-result-object v8

    #@1ff
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@202
    move-result-object v7

    #@203
    const-string v8, "]="

    #@205
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@208
    move-result-object v7

    #@209
    aget v8, v1, v2

    #@20b
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@20e
    move-result-object v8

    #@20f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v7

    #@213
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@216
    move-result-object v7

    #@217
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21a
    .line 1554
    :cond_21a
    add-int/lit8 v2, v2, 0x1

    #@21c
    goto :goto_1e7

    #@21d
    .line 1562
    :cond_21d
    const-string v6, "351*5#"

    #@21f
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@221
    goto :goto_1b6

    #@222
    .line 1579
    .end local v1           #ints:[I
    .end local v2           #loop:I
    :cond_222
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isDeactivate()Z

    #@225
    move-result v6

    #@226
    if-eqz v6, :cond_286

    #@228
    .line 1580
    sget-object v6, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@22a
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@22c
    .line 1581
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@22e
    const v7, 0x1040096

    #@231
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@234
    move-result-object v6

    #@235
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@238
    .line 1584
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@23a
    const-string v7, "31"

    #@23c
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23f
    move-result v6

    #@240
    if-eqz v6, :cond_268

    #@242
    .line 1585
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@244
    const/4 v7, 0x2

    #@245
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/gsm/GSMPhone;->saveClirSetting(I)V

    #@248
    .line 1595
    :cond_248
    :goto_248
    sget-boolean v6, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ENABLE_PRIVACY_LOG_CALL:Z

    #@24a
    if-eqz v6, :cond_c3

    #@24c
    .line 1596
    const-string v6, "GSM"

    #@24e
    new-instance v7, Ljava/lang/StringBuilder;

    #@250
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@253
    const-string v8, "onSetComplete Deactivate ussd="

    #@255
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@258
    move-result-object v7

    #@259
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@25b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25e
    move-result-object v7

    #@25f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@262
    move-result-object v7

    #@263
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@266
    goto/16 :goto_c3

    #@268
    .line 1589
    :cond_268
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@26a
    const-string v7, "43"

    #@26c
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26f
    move-result v6

    #@270
    if-eqz v6, :cond_277

    #@272
    .line 1590
    const-string v6, "43*4#"

    #@274
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@276
    goto :goto_248

    #@277
    .line 1591
    :cond_277
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@279
    const-string v7, "351"

    #@27b
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27e
    move-result v6

    #@27f
    if-eqz v6, :cond_248

    #@281
    .line 1592
    const-string v6, "351*4#"

    #@283
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@285
    goto :goto_248

    #@286
    .line 1603
    :cond_286
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isRegister()Z

    #@289
    move-result v6

    #@28a
    if-eqz v6, :cond_29b

    #@28c
    .line 1604
    sget-object v6, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@28e
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@290
    .line 1605
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@292
    invoke-virtual {v6, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@295
    move-result-object v6

    #@296
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@299
    goto/16 :goto_c3

    #@29b
    .line 1607
    :cond_29b
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isErasure()Z

    #@29e
    move-result v6

    #@29f
    if-eqz v6, :cond_2b3

    #@2a1
    .line 1608
    sget-object v6, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@2a3
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@2a5
    .line 1609
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@2a7
    const v7, 0x1040098

    #@2aa
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@2ad
    move-result-object v6

    #@2ae
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@2b1
    goto/16 :goto_c3

    #@2b3
    .line 1612
    :cond_2b3
    sget-object v6, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@2b5
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@2b7
    .line 1613
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@2b9
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@2bc
    move-result-object v6

    #@2bd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@2c0
    goto/16 :goto_c3
.end method

.method static scToBarringFacility(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "sc"

    #@0
    .prologue
    .line 663
    if-nez p0, :cond_a

    #@2
    .line 664
    new-instance v0, Ljava/lang/RuntimeException;

    #@4
    const-string v1, "invalid call barring sc"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 667
    :cond_a
    const-string v0, "33"

    #@c
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_15

    #@12
    .line 668
    const-string v0, "AO"

    #@14
    .line 682
    :goto_14
    return-object v0

    #@15
    .line 669
    :cond_15
    const-string v0, "331"

    #@17
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_20

    #@1d
    .line 670
    const-string v0, "OI"

    #@1f
    goto :goto_14

    #@20
    .line 671
    :cond_20
    const-string v0, "332"

    #@22
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_2b

    #@28
    .line 672
    const-string v0, "OX"

    #@2a
    goto :goto_14

    #@2b
    .line 673
    :cond_2b
    const-string v0, "35"

    #@2d
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_36

    #@33
    .line 674
    const-string v0, "AI"

    #@35
    goto :goto_14

    #@36
    .line 675
    :cond_36
    const-string v0, "351"

    #@38
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_41

    #@3e
    .line 676
    const-string v0, "IR"

    #@40
    goto :goto_14

    #@41
    .line 677
    :cond_41
    const-string v0, "330"

    #@43
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v0

    #@47
    if-eqz v0, :cond_4c

    #@49
    .line 678
    const-string v0, "AB"

    #@4b
    goto :goto_14

    #@4c
    .line 679
    :cond_4c
    const-string v0, "333"

    #@4e
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v0

    #@52
    if-eqz v0, :cond_57

    #@54
    .line 680
    const-string v0, "AG"

    #@56
    goto :goto_14

    #@57
    .line 681
    :cond_57
    const-string v0, "353"

    #@59
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c
    move-result v0

    #@5d
    if-eqz v0, :cond_62

    #@5f
    .line 682
    const-string v0, "AC"

    #@61
    goto :goto_14

    #@62
    .line 684
    :cond_62
    new-instance v0, Ljava/lang/RuntimeException;

    #@64
    const-string v1, "invalid call barring sc"

    #@66
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@69
    throw v0
.end method

.method private static scToCallForwardReason(Ljava/lang/String;)I
    .registers 3
    .parameter "sc"

    #@0
    .prologue
    .line 561
    if-nez p0, :cond_a

    #@2
    .line 562
    new-instance v0, Ljava/lang/RuntimeException;

    #@4
    const-string v1, "invalid call forward sc"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 565
    :cond_a
    const-string v0, "002"

    #@c
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    .line 566
    const/4 v0, 0x4

    #@13
    .line 576
    :goto_13
    return v0

    #@14
    .line 567
    :cond_14
    const-string v0, "21"

    #@16
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_1e

    #@1c
    .line 568
    const/4 v0, 0x0

    #@1d
    goto :goto_13

    #@1e
    .line 569
    :cond_1e
    const-string v0, "67"

    #@20
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_28

    #@26
    .line 570
    const/4 v0, 0x1

    #@27
    goto :goto_13

    #@28
    .line 571
    :cond_28
    const-string v0, "62"

    #@2a
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_32

    #@30
    .line 572
    const/4 v0, 0x3

    #@31
    goto :goto_13

    #@32
    .line 573
    :cond_32
    const-string v0, "61"

    #@34
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_3c

    #@3a
    .line 574
    const/4 v0, 0x2

    #@3b
    goto :goto_13

    #@3c
    .line 575
    :cond_3c
    const-string v0, "004"

    #@3e
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v0

    #@42
    if-eqz v0, :cond_46

    #@44
    .line 576
    const/4 v0, 0x5

    #@45
    goto :goto_13

    #@46
    .line 578
    :cond_46
    new-instance v0, Ljava/lang/RuntimeException;

    #@48
    const-string v1, "invalid call forward sc"

    #@4a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@4d
    throw v0
.end method

.method private serviceClassToCFString(I)Ljava/lang/CharSequence;
    .registers 4
    .parameter "serviceClass"

    #@0
    .prologue
    .line 1769
    sparse-switch p1, :sswitch_data_56

    #@3
    .line 1787
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 1771
    :sswitch_5
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@7
    const v1, 0x10400c1

    #@a
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@d
    move-result-object v0

    #@e
    goto :goto_4

    #@f
    .line 1773
    :sswitch_f
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@11
    const v1, 0x10400c2

    #@14
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@17
    move-result-object v0

    #@18
    goto :goto_4

    #@19
    .line 1775
    :sswitch_19
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@1b
    const v1, 0x10400c3

    #@1e
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@21
    move-result-object v0

    #@22
    goto :goto_4

    #@23
    .line 1777
    :sswitch_23
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@25
    const v1, 0x10400c4

    #@28
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@2b
    move-result-object v0

    #@2c
    goto :goto_4

    #@2d
    .line 1779
    :sswitch_2d
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@2f
    const v1, 0x10400c6

    #@32
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@35
    move-result-object v0

    #@36
    goto :goto_4

    #@37
    .line 1781
    :sswitch_37
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@39
    const v1, 0x10400c5

    #@3c
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@3f
    move-result-object v0

    #@40
    goto :goto_4

    #@41
    .line 1783
    :sswitch_41
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@43
    const v1, 0x10400c7

    #@46
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@49
    move-result-object v0

    #@4a
    goto :goto_4

    #@4b
    .line 1785
    :sswitch_4b
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@4d
    const v1, 0x10400c8

    #@50
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@53
    move-result-object v0

    #@54
    goto :goto_4

    #@55
    .line 1769
    nop

    #@56
    :sswitch_data_56
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_f
        0x4 -> :sswitch_19
        0x8 -> :sswitch_23
        0x10 -> :sswitch_2d
        0x20 -> :sswitch_37
        0x40 -> :sswitch_41
        0x80 -> :sswitch_4b
    .end sparse-switch
.end method

.method private static siToServiceClass(Ljava/lang/String;)I
    .registers 5
    .parameter "si"

    #@0
    .prologue
    .line 584
    if-eqz p0, :cond_8

    #@2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_a

    #@8
    .line 585
    :cond_8
    const/4 v1, 0x0

    #@9
    .line 613
    :goto_9
    return v1

    #@a
    .line 588
    :cond_a
    const/16 v1, 0xa

    #@c
    invoke-static {p0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@f
    move-result v0

    #@10
    .line 590
    .local v0, serviceCode:I
    sparse-switch v0, :sswitch_data_50

    #@13
    .line 616
    new-instance v1, Ljava/lang/RuntimeException;

    #@15
    new-instance v2, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v3, "unsupported MMI service code "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v1

    #@2c
    .line 591
    :sswitch_2c
    const/16 v1, 0xd

    #@2e
    goto :goto_9

    #@2f
    .line 592
    :sswitch_2f
    const/4 v1, 0x1

    #@30
    goto :goto_9

    #@31
    .line 593
    :sswitch_31
    const/16 v1, 0xc

    #@33
    goto :goto_9

    #@34
    .line 594
    :sswitch_34
    const/4 v1, 0x4

    #@35
    goto :goto_9

    #@36
    .line 596
    :sswitch_36
    const/16 v1, 0x8

    #@38
    goto :goto_9

    #@39
    .line 598
    :sswitch_39
    const/4 v1, 0x5

    #@3a
    goto :goto_9

    #@3b
    .line 606
    :sswitch_3b
    const/16 v1, 0x30

    #@3d
    goto :goto_9

    #@3e
    .line 608
    :sswitch_3e
    const/16 v1, 0xa0

    #@40
    goto :goto_9

    #@41
    .line 609
    :sswitch_41
    const/16 v1, 0x50

    #@43
    goto :goto_9

    #@44
    .line 610
    :sswitch_44
    const/16 v1, 0x10

    #@46
    goto :goto_9

    #@47
    .line 611
    :sswitch_47
    const/16 v1, 0x20

    #@49
    goto :goto_9

    #@4a
    .line 612
    :sswitch_4a
    const/16 v1, 0x11

    #@4c
    goto :goto_9

    #@4d
    .line 613
    :sswitch_4d
    const/16 v1, 0x40

    #@4f
    goto :goto_9

    #@50
    .line 590
    :sswitch_data_50
    .sparse-switch
        0xa -> :sswitch_2c
        0xb -> :sswitch_2f
        0xc -> :sswitch_31
        0xd -> :sswitch_34
        0x10 -> :sswitch_36
        0x13 -> :sswitch_39
        0x14 -> :sswitch_3b
        0x15 -> :sswitch_3e
        0x16 -> :sswitch_41
        0x18 -> :sswitch_44
        0x19 -> :sswitch_47
        0x1a -> :sswitch_4a
        0x63 -> :sswitch_4d
    .end sparse-switch
.end method

.method private static siToTime(Ljava/lang/String;)I
    .registers 2
    .parameter "si"

    #@0
    .prologue
    .line 623
    if-eqz p0, :cond_8

    #@2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    .line 624
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 627
    :goto_9
    return v0

    #@a
    :cond_a
    const/16 v0, 0xa

    #@c
    invoke-static {p0, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@f
    move-result v0

    #@10
    goto :goto_9
.end method


# virtual methods
.method public cancel()V
    .registers 3

    #@0
    .prologue
    .line 723
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@2
    sget-object v1, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@4
    if-eq v0, v1, :cond_c

    #@6
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@8
    sget-object v1, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@a
    if-ne v0, v1, :cond_d

    #@c
    .line 748
    :cond_c
    :goto_c
    return-void

    #@d
    .line 727
    :cond_d
    sget-object v0, Lcom/android/internal/telephony/MmiCode$State;->CANCELLED:Lcom/android/internal/telephony/MmiCode$State;

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@11
    .line 729
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPendingUSSD:Z

    #@13
    if-eqz v0, :cond_22

    #@15
    .line 734
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@17
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@19
    const/4 v1, 0x7

    #@1a
    invoke-virtual {p0, v1, p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1d
    move-result-object v1

    #@1e
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->cancelPendingUssd(Landroid/os/Message;)V

    #@21
    goto :goto_c

    #@22
    .line 745
    :cond_22
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@24
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@27
    goto :goto_c
.end method

.method getCLIRMode()I
    .registers 3

    #@0
    .prologue
    .line 946
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_1e

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@6
    const-string v1, "31"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_1e

    #@e
    .line 947
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_16

    #@14
    .line 948
    const/4 v0, 0x2

    #@15
    .line 954
    :goto_15
    return v0

    #@16
    .line 949
    :cond_16
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isDeactivate()Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_1e

    #@1c
    .line 950
    const/4 v0, 0x1

    #@1d
    goto :goto_15

    #@1e
    .line 954
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_15
.end method

.method getCLIRModeKorea()I
    .registers 3

    #@0
    .prologue
    .line 930
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_16

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@6
    const-string v1, "23"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_16

    #@e
    .line 931
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_2c

    #@14
    .line 932
    const/4 v0, 0x1

    #@15
    .line 937
    :goto_15
    return v0

    #@16
    .line 933
    :cond_16
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@18
    if-eqz v0, :cond_2c

    #@1a
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@1c
    const-string v1, "230"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_2c

    #@24
    .line 934
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_2c

    #@2a
    .line 935
    const/4 v0, 0x2

    #@2b
    goto :goto_15

    #@2c
    .line 937
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_15
.end method

.method public getDialString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2149
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialString:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMessage()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 711
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getPhone()Lcom/android/internal/telephony/Phone;
    .registers 2

    #@0
    .prologue
    .line 716
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2
    return-object v0
.end method

.method public getState()Lcom/android/internal/telephony/MmiCode$State;
    .registers 2

    #@0
    .prologue
    .line 706
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@2
    return-object v0
.end method

.method public getUssdCode()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 2145
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1288
    iget v2, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v2, :pswitch_data_7e

    #@6
    .line 1349
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1290
    :pswitch_7
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    check-cast v2, Landroid/os/AsyncResult;

    #@b
    move-object v0, v2

    #@c
    check-cast v0, Landroid/os/AsyncResult;

    #@e
    .line 1292
    .local v0, ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onSetComplete(Landroid/os/AsyncResult;)V

    #@11
    goto :goto_6

    #@12
    .line 1296
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_12
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@14
    check-cast v2, Landroid/os/AsyncResult;

    #@16
    move-object v0, v2

    #@17
    check-cast v0, Landroid/os/AsyncResult;

    #@19
    .line 1302
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1b
    if-nez v2, :cond_36

    #@1d
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@1f
    if-ne v2, v3, :cond_36

    #@21
    .line 1303
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@23
    if-ne v2, v3, :cond_3a

    #@25
    move v1, v3

    #@26
    .line 1304
    .local v1, cffEnabled:Z
    :goto_26
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@28
    if-eqz v2, :cond_36

    #@2a
    .line 1305
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@2c
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@2e
    invoke-virtual {v2, v3, v1, v4}, Lcom/android/internal/telephony/uicc/IccRecords;->setVoiceCallForwardingFlag(IZLjava/lang/String;)V

    #@31
    .line 1306
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@33
    invoke-virtual {v2, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->setCallForwardingPreference(Z)V

    #@36
    .line 1310
    .end local v1           #cffEnabled:Z
    :cond_36
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onSetComplete(Landroid/os/AsyncResult;)V

    #@39
    goto :goto_6

    #@3a
    .line 1303
    :cond_3a
    const/4 v1, 0x0

    #@3b
    goto :goto_26

    #@3c
    .line 1314
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_3c
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3e
    check-cast v2, Landroid/os/AsyncResult;

    #@40
    move-object v0, v2

    #@41
    check-cast v0, Landroid/os/AsyncResult;

    #@43
    .line 1315
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onGetClirComplete(Landroid/os/AsyncResult;)V

    #@46
    goto :goto_6

    #@47
    .line 1319
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_47
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@49
    check-cast v2, Landroid/os/AsyncResult;

    #@4b
    move-object v0, v2

    #@4c
    check-cast v0, Landroid/os/AsyncResult;

    #@4e
    .line 1320
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onQueryCfComplete(Landroid/os/AsyncResult;)V

    #@51
    goto :goto_6

    #@52
    .line 1324
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_52
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@54
    check-cast v2, Landroid/os/AsyncResult;

    #@56
    move-object v0, v2

    #@57
    check-cast v0, Landroid/os/AsyncResult;

    #@59
    .line 1325
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onQueryComplete(Landroid/os/AsyncResult;)V

    #@5c
    goto :goto_6

    #@5d
    .line 1329
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_5d
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5f
    check-cast v2, Landroid/os/AsyncResult;

    #@61
    move-object v0, v2

    #@62
    check-cast v0, Landroid/os/AsyncResult;

    #@64
    .line 1331
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@66
    if-eqz v2, :cond_6

    #@68
    .line 1332
    sget-object v2, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@6a
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@6c
    .line 1333
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getErrorMessage(Landroid/os/AsyncResult;)Ljava/lang/CharSequence;

    #@6f
    move-result-object v2

    #@70
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@72
    .line 1335
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@74
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@77
    goto :goto_6

    #@78
    .line 1346
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_78
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@7a
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@7d
    goto :goto_6

    #@7e
    .line 1288
    :pswitch_data_7e
    .packed-switch 0x1
        :pswitch_7
        :pswitch_3c
        :pswitch_47
        :pswitch_5d
        :pswitch_52
        :pswitch_12
        :pswitch_78
    .end packed-switch
.end method

.method isActivate()Z
    .registers 3

    #@0
    .prologue
    .line 958
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@6
    const-string v1, "*"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public isCancelable()Z
    .registers 2

    #@0
    .prologue
    .line 752
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPendingUSSD:Z

    #@2
    return v0
.end method

.method isDeactivate()Z
    .registers 3

    #@0
    .prologue
    .line 962
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@6
    const-string v1, "#"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method isErasure()Z
    .registers 3

    #@0
    .prologue
    .line 974
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@6
    const-string v1, "##"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method isGlobalDevMmi()Z
    .registers 3

    #@0
    .prologue
    .line 883
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_1a

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@6
    const-string v1, "86"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_18

    #@e
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@10
    const-string v1, "611"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1a

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method isInterrogate()Z
    .registers 3

    #@0
    .prologue
    .line 966
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@6
    const-string v1, "*#"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method isMMI()Z
    .registers 2

    #@0
    .prologue
    .line 760
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->poundString:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isPendingUSSD()Z
    .registers 2

    #@0
    .prologue
    .line 982
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPendingUSSD:Z

    #@2
    return v0
.end method

.method isPinCommand()Z
    .registers 3

    #@0
    .prologue
    .line 874
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_2e

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@6
    const-string v1, "04"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_2c

    #@e
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@10
    const-string v1, "042"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_2c

    #@18
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@1a
    const-string v1, "05"

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_2c

    #@22
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@24
    const-string v1, "052"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v0

    #@2a
    if-eqz v0, :cond_2e

    #@2c
    :cond_2c
    const/4 v0, 0x1

    #@2d
    :goto_2d
    return v0

    #@2e
    :cond_2e
    const/4 v0, 0x0

    #@2f
    goto :goto_2d
.end method

.method isRegister()Z
    .registers 3

    #@0
    .prologue
    .line 970
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@6
    const-string v1, "**"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method isShortCode()Z
    .registers 3

    #@0
    .prologue
    .line 766
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->poundString:Ljava/lang/String;

    #@2
    if-nez v0, :cond_13

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@6
    if-eqz v0, :cond_13

    #@8
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@a
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@d
    move-result v0

    #@e
    const/4 v1, 0x2

    #@f
    if-gt v0, v1, :cond_13

    #@11
    const/4 v0, 0x1

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method public isSsInfo()Z
    .registers 2

    #@0
    .prologue
    .line 990
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isSsInfo:Z

    #@2
    return v0
.end method

.method isTemporaryModeCLIR()Z
    .registers 3

    #@0
    .prologue
    .line 896
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_20

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@6
    const-string v1, "31"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_20

    #@e
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@10
    if-eqz v0, :cond_20

    #@12
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_1e

    #@18
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isDeactivate()Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_20

    #@1e
    :cond_1e
    const/4 v0, 0x1

    #@1f
    :goto_1f
    return v0

    #@20
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_1f
.end method

.method isTemporaryModeCLIRKorea()Z
    .registers 4

    #@0
    .prologue
    .line 903
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "isTemporaryModeCLIRKorea() : sc = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, " dialingNumber = "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 904
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@28
    if-eqz v0, :cond_44

    #@2a
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2c
    const-string v1, "23"

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v0

    #@32
    if-nez v0, :cond_3e

    #@34
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@36
    const-string v1, "230"

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_44

    #@3e
    :cond_3e
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@40
    if-eqz v0, :cond_44

    #@42
    const/4 v0, 0x1

    #@43
    :goto_43
    return v0

    #@44
    :cond_44
    const/4 v0, 0x0

    #@45
    goto :goto_43
.end method

.method isTemporaryModeKTKorea()Z
    .registers 4

    #@0
    .prologue
    .line 920
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "isTemporaryModeKTKorea() : sc = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, " dialingNumber = "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 921
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@28
    if-eqz v0, :cond_62

    #@2a
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2c
    const-string v1, "61"

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v0

    #@32
    if-nez v0, :cond_52

    #@34
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@36
    const-string v1, "62"

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v0

    #@3c
    if-nez v0, :cond_52

    #@3e
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@40
    const-string v1, "63"

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v0

    #@46
    if-nez v0, :cond_52

    #@48
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@4a
    const-string v1, "65"

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v0

    #@50
    if-eqz v0, :cond_56

    #@52
    :cond_52
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@54
    if-nez v0, :cond_60

    #@56
    :cond_56
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@58
    const-string v1, "22"

    #@5a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v0

    #@5e
    if-eqz v0, :cond_62

    #@60
    :cond_60
    const/4 v0, 0x1

    #@61
    :goto_61
    return v0

    #@62
    :cond_62
    const/4 v0, 0x0

    #@63
    goto :goto_61
.end method

.method isTemporaryModeSKTKorea()Z
    .registers 4

    #@0
    .prologue
    .line 910
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "isTemporaryModeSKTKorea() : sc = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 911
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@1c
    if-eqz v0, :cond_52

    #@1e
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@20
    const-string v1, "61"

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_50

    #@28
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2a
    const-string v1, "62"

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v0

    #@30
    if-nez v0, :cond_50

    #@32
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@34
    const-string v1, "63"

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v0

    #@3a
    if-nez v0, :cond_50

    #@3c
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@3e
    const-string v1, "65"

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v0

    #@44
    if-nez v0, :cond_50

    #@46
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@48
    const-string v1, "22"

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v0

    #@4e
    if-eqz v0, :cond_52

    #@50
    :cond_50
    const/4 v0, 0x1

    #@51
    :goto_51
    return v0

    #@52
    :cond_52
    const/4 v0, 0x0

    #@53
    goto :goto_51
.end method

.method public isUssdRequest()Z
    .registers 2

    #@0
    .prologue
    .line 986
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isUssdRequest:Z

    #@2
    return v0
.end method

.method onUssdFinished(Ljava/lang/String;Z)V
    .registers 5
    .parameter "ussdMessage"
    .parameter "isUssdRequest"

    #@0
    .prologue
    .line 1231
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@2
    sget-object v1, Lcom/android/internal/telephony/MmiCode$State;->PENDING:Lcom/android/internal/telephony/MmiCode$State;

    #@4
    if-ne v0, v1, :cond_20

    #@6
    .line 1232
    if-nez p1, :cond_21

    #@8
    .line 1233
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@a
    const v1, 0x104009a

    #@d
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@13
    .line 1244
    :goto_13
    iput-boolean p2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isUssdRequest:Z

    #@15
    .line 1246
    if-nez p2, :cond_1b

    #@17
    .line 1247
    sget-object v0, Lcom/android/internal/telephony/MmiCode$State;->COMPLETE:Lcom/android/internal/telephony/MmiCode$State;

    #@19
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@1b
    .line 1250
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1d
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@20
    .line 1252
    :cond_20
    return-void

    #@21
    .line 1235
    :cond_21
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@23
    .line 1239
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->ussdCode:Ljava/lang/CharSequence;

    #@25
    goto :goto_13
.end method

.method onUssdFinishedError()V
    .registers 3

    #@0
    .prologue
    .line 1262
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@2
    sget-object v1, Lcom/android/internal/telephony/MmiCode$State;->PENDING:Lcom/android/internal/telephony/MmiCode$State;

    #@4
    if-ne v0, v1, :cond_1a

    #@6
    .line 1263
    sget-object v0, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@a
    .line 1264
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@c
    const v1, 0x1040091

    #@f
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@15
    .line 1266
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@17
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@1a
    .line 1268
    :cond_1a
    return-void
.end method

.method parseSsData(Lcom/android/internal/telephony/gsm/SsData;)V
    .registers 9
    .parameter "ssData"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 429
    iget v3, p1, Lcom/android/internal/telephony/gsm/SsData;->result:I

    #@4
    invoke-static {v3}, Lcom/android/internal/telephony/CommandException;->fromRilErrno(I)Lcom/android/internal/telephony/CommandException;

    #@7
    move-result-object v1

    #@8
    .line 430
    .local v1, ex:Lcom/android/internal/telephony/CommandException;
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/SsData;->serviceType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@a
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getScStringFromScType(Lcom/android/internal/telephony/gsm/SsData$ServiceType;)Ljava/lang/String;

    #@d
    move-result-object v3

    #@e
    iput-object v3, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@10
    .line 431
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/SsData;->requestType:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@12
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getActionStringFromReqType(Lcom/android/internal/telephony/gsm/SsData$RequestType;)Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    iput-object v3, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@18
    .line 432
    const-string v3, "GSM"

    #@1a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v5, "parseSsData sc = "

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, ", action = "

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    const-string v5, ", ex = "

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 434
    sget-object v3, Lcom/android/internal/telephony/gsm/GsmMmiCode$1;->$SwitchMap$com$android$internal$telephony$gsm$SsData$RequestType:[I

    #@4a
    iget-object v4, p1, Lcom/android/internal/telephony/gsm/SsData;->requestType:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@4c
    invoke-virtual {v4}, Lcom/android/internal/telephony/gsm/SsData$RequestType;->ordinal()I

    #@4f
    move-result v4

    #@50
    aget v3, v3, v4

    #@52
    packed-switch v3, :pswitch_data_112

    #@55
    .line 472
    const-string v2, "GSM"

    #@57
    new-instance v3, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v4, "Invaid requestType in SSData : "

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    iget-object v4, p1, Lcom/android/internal/telephony/gsm/SsData;->requestType:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 475
    :goto_6f
    return-void

    #@70
    .line 439
    :pswitch_70
    iget v3, p1, Lcom/android/internal/telephony/gsm/SsData;->result:I

    #@72
    if-nez v3, :cond_bb

    #@74
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/SsData;->serviceType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@76
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->isTypeUnConditional()Z

    #@79
    move-result v3

    #@7a
    if-eqz v3, :cond_bb

    #@7c
    .line 446
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/SsData;->requestType:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@7e
    sget-object v4, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_ACTIVATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@80
    if-eq v3, v4, :cond_88

    #@82
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/SsData;->requestType:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@84
    sget-object v4, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_REGISTRATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@86
    if-ne v3, v4, :cond_c6

    #@88
    :cond_88
    iget v3, p1, Lcom/android/internal/telephony/gsm/SsData;->serviceClass:I

    #@8a
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isServiceClassVoiceorNone(I)Z

    #@8d
    move-result v3

    #@8e
    if-eqz v3, :cond_c6

    #@90
    move v0, v2

    #@91
    .line 450
    .local v0, cffEnabled:Z
    :goto_91
    const-string v3, "GSM"

    #@93
    new-instance v4, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v5, "setVoiceCallForwardingFlag cffEnabled: "

    #@9a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v4

    #@9e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v4

    #@a2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v4

    #@a6
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 451
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@ab
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@ad
    if-eqz v3, :cond_c8

    #@af
    .line 452
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@b1
    invoke-virtual {v3, v2, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->setVoiceCallForwardingFlag(IZ)V

    #@b4
    .line 453
    const-string v2, "GSM"

    #@b6
    const-string v3, "setVoiceCallForwardingFlag done from SS Info."

    #@b8
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    .line 458
    .end local v0           #cffEnabled:Z
    :cond_bb
    :goto_bb
    new-instance v2, Landroid/os/AsyncResult;

    #@bd
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/SsData;->cfInfo:[Lcom/android/internal/telephony/CallForwardInfo;

    #@bf
    invoke-direct {v2, v6, v3, v1}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@c2
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onSetComplete(Landroid/os/AsyncResult;)V

    #@c5
    goto :goto_6f

    #@c6
    .line 446
    :cond_c6
    const/4 v0, 0x0

    #@c7
    goto :goto_91

    #@c8
    .line 455
    .restart local v0       #cffEnabled:Z
    :cond_c8
    const-string v2, "GSM"

    #@ca
    const-string v3, "setVoiceCallForwardingFlag aborted. sim records is null."

    #@cc
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    goto :goto_bb

    #@d0
    .line 461
    .end local v0           #cffEnabled:Z
    :pswitch_d0
    iget-object v2, p1, Lcom/android/internal/telephony/gsm/SsData;->serviceType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@d2
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->isTypeClir()Z

    #@d5
    move-result v2

    #@d6
    if-eqz v2, :cond_ea

    #@d8
    .line 462
    const-string v2, "GSM"

    #@da
    const-string v3, "CLIR INTERROGATION"

    #@dc
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    .line 463
    new-instance v2, Landroid/os/AsyncResult;

    #@e1
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/SsData;->ssInfo:[I

    #@e3
    invoke-direct {v2, v6, v3, v1}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@e6
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onGetClirComplete(Landroid/os/AsyncResult;)V

    #@e9
    goto :goto_6f

    #@ea
    .line 464
    :cond_ea
    iget-object v2, p1, Lcom/android/internal/telephony/gsm/SsData;->serviceType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@ec
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->isTypeCF()Z

    #@ef
    move-result v2

    #@f0
    if-eqz v2, :cond_105

    #@f2
    .line 465
    const-string v2, "GSM"

    #@f4
    const-string v3, "CALL FORWARD INTERROGATION"

    #@f6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f9
    .line 466
    new-instance v2, Landroid/os/AsyncResult;

    #@fb
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/SsData;->cfInfo:[Lcom/android/internal/telephony/CallForwardInfo;

    #@fd
    invoke-direct {v2, v6, v3, v1}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@100
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onQueryCfComplete(Landroid/os/AsyncResult;)V

    #@103
    goto/16 :goto_6f

    #@105
    .line 468
    :cond_105
    new-instance v2, Landroid/os/AsyncResult;

    #@107
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/SsData;->ssInfo:[I

    #@109
    invoke-direct {v2, v6, v3, v1}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@10c
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onQueryComplete(Landroid/os/AsyncResult;)V

    #@10f
    goto/16 :goto_6f

    #@111
    .line 434
    nop

    #@112
    :pswitch_data_112
    .packed-switch 0x1
        :pswitch_70
        :pswitch_70
        :pswitch_70
        :pswitch_70
        :pswitch_d0
    .end packed-switch
.end method

.method processCode()V
    .registers 26

    #@0
    .prologue
    .line 1002
    :try_start_0
    const-string v4, "*333#"

    #@2
    move-object/from16 v0, p0

    #@4
    iget-object v10, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->poundString:Ljava/lang/String;

    #@6
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_4a

    #@c
    .line 1004
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@f
    move-result-object v16

    #@10
    .line 1005
    .local v16, SimInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@13
    move-result-object v4

    #@14
    if-eqz v4, :cond_4a

    #@16
    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    const-string v10, "510"

    #@1c
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_4a

    #@22
    .line 1007
    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMnc()Ljava/lang/String;

    #@25
    move-result-object v4

    #@26
    if-eqz v4, :cond_4a

    #@28
    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMnc()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    const-string v10, "08"

    #@2e
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v4

    #@32
    if-nez v4, :cond_40

    #@34
    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMnc()Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    const-string v10, "89"

    #@3a
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v4

    #@3e
    if-eqz v4, :cond_4a

    #@40
    .line 1009
    :cond_40
    move-object/from16 v0, p0

    #@42
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->poundString:Ljava/lang/String;

    #@44
    move-object/from16 v0, p0

    #@46
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sendUssd(Ljava/lang/String;)V

    #@49
    .line 1209
    .end local v16           #SimInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    :cond_49
    :goto_49
    return-void

    #@4a
    .line 1016
    :cond_4a
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isShortCode()Z

    #@4d
    move-result v4

    #@4e
    if-eqz v4, :cond_81

    #@50
    .line 1017
    const-string v4, "GSM"

    #@52
    const-string v10, "isShortCode"

    #@54
    invoke-static {v4, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 1019
    move-object/from16 v0, p0

    #@59
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@5b
    move-object/from16 v0, p0

    #@5d
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sendUssd(Ljava/lang/String;)V
    :try_end_60
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_60} :catch_61

    #@60
    goto :goto_49

    #@61
    .line 1204
    :catch_61
    move-exception v17

    #@62
    .line 1205
    .local v17, exc:Ljava/lang/RuntimeException;
    sget-object v4, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@64
    move-object/from16 v0, p0

    #@66
    iput-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@68
    .line 1206
    move-object/from16 v0, p0

    #@6a
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@6c
    const v10, 0x1040091

    #@6f
    invoke-virtual {v4, v10}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@72
    move-result-object v4

    #@73
    move-object/from16 v0, p0

    #@75
    iput-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@77
    .line 1207
    move-object/from16 v0, p0

    #@79
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@7b
    move-object/from16 v0, p0

    #@7d
    invoke-virtual {v4, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@80
    goto :goto_49

    #@81
    .line 1020
    .end local v17           #exc:Ljava/lang/RuntimeException;
    :cond_81
    :try_start_81
    move-object/from16 v0, p0

    #@83
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@85
    if-eqz v4, :cond_8f

    #@87
    .line 1022
    new-instance v4, Ljava/lang/RuntimeException;

    #@89
    const-string v10, "Invalid or Unsupported MMI Code"

    #@8b
    invoke-direct {v4, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@8e
    throw v4

    #@8f
    .line 1023
    :cond_8f
    move-object/from16 v0, p0

    #@91
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@93
    if-eqz v4, :cond_c9

    #@95
    move-object/from16 v0, p0

    #@97
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@99
    const-string v10, "30"

    #@9b
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9e
    move-result v4

    #@9f
    if-eqz v4, :cond_c9

    #@a1
    .line 1024
    const-string v4, "GSM"

    #@a3
    const-string v10, "is CLIP"

    #@a5
    invoke-static {v4, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    .line 1025
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isInterrogate()Z

    #@ab
    move-result v4

    #@ac
    if-eqz v4, :cond_c1

    #@ae
    .line 1026
    move-object/from16 v0, p0

    #@b0
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@b2
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@b4
    const/4 v10, 0x5

    #@b5
    move-object/from16 v0, p0

    #@b7
    move-object/from16 v1, p0

    #@b9
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@bc
    move-result-object v10

    #@bd
    invoke-interface {v4, v10}, Lcom/android/internal/telephony/CommandsInterface;->queryCLIP(Landroid/os/Message;)V

    #@c0
    goto :goto_49

    #@c1
    .line 1029
    :cond_c1
    new-instance v4, Ljava/lang/RuntimeException;

    #@c3
    const-string v10, "Invalid or Unsupported MMI Code"

    #@c5
    invoke-direct {v4, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c8
    throw v4

    #@c9
    .line 1031
    :cond_c9
    move-object/from16 v0, p0

    #@cb
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@cd
    if-eqz v4, :cond_13a

    #@cf
    move-object/from16 v0, p0

    #@d1
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@d3
    const-string v10, "31"

    #@d5
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d8
    move-result v4

    #@d9
    if-eqz v4, :cond_13a

    #@db
    .line 1032
    const-string v4, "GSM"

    #@dd
    const-string v10, "is CLIR"

    #@df
    invoke-static {v4, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    .line 1033
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@e5
    move-result v4

    #@e6
    if-eqz v4, :cond_fd

    #@e8
    .line 1034
    move-object/from16 v0, p0

    #@ea
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@ec
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@ee
    const/4 v10, 0x1

    #@ef
    const/4 v12, 0x1

    #@f0
    move-object/from16 v0, p0

    #@f2
    move-object/from16 v1, p0

    #@f4
    invoke-virtual {v0, v12, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@f7
    move-result-object v12

    #@f8
    invoke-interface {v4, v10, v12}, Lcom/android/internal/telephony/CommandsInterface;->setCLIR(ILandroid/os/Message;)V

    #@fb
    goto/16 :goto_49

    #@fd
    .line 1036
    :cond_fd
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isDeactivate()Z

    #@100
    move-result v4

    #@101
    if-eqz v4, :cond_118

    #@103
    .line 1037
    move-object/from16 v0, p0

    #@105
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@107
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@109
    const/4 v10, 0x2

    #@10a
    const/4 v12, 0x1

    #@10b
    move-object/from16 v0, p0

    #@10d
    move-object/from16 v1, p0

    #@10f
    invoke-virtual {v0, v12, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@112
    move-result-object v12

    #@113
    invoke-interface {v4, v10, v12}, Lcom/android/internal/telephony/CommandsInterface;->setCLIR(ILandroid/os/Message;)V

    #@116
    goto/16 :goto_49

    #@118
    .line 1039
    :cond_118
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isInterrogate()Z

    #@11b
    move-result v4

    #@11c
    if-eqz v4, :cond_132

    #@11e
    .line 1040
    move-object/from16 v0, p0

    #@120
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@122
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@124
    const/4 v10, 0x2

    #@125
    move-object/from16 v0, p0

    #@127
    move-object/from16 v1, p0

    #@129
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@12c
    move-result-object v10

    #@12d
    invoke-interface {v4, v10}, Lcom/android/internal/telephony/CommandsInterface;->getCLIR(Landroid/os/Message;)V

    #@130
    goto/16 :goto_49

    #@132
    .line 1043
    :cond_132
    new-instance v4, Ljava/lang/RuntimeException;

    #@134
    const-string v10, "Invalid or Unsupported MMI Code"

    #@136
    invoke-direct {v4, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@139
    throw v4

    #@13a
    .line 1045
    :cond_13a
    move-object/from16 v0, p0

    #@13c
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@13e
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isServiceCodeCallForwarding(Ljava/lang/String;)Z

    #@141
    move-result v4

    #@142
    if-eqz v4, :cond_1eb

    #@144
    .line 1046
    const-string v4, "GSM"

    #@146
    const-string v10, "is CF"

    #@148
    invoke-static {v4, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14b
    .line 1048
    move-object/from16 v0, p0

    #@14d
    iget-object v8, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sia:Ljava/lang/String;

    #@14f
    .line 1049
    .local v8, dialingNumber:Ljava/lang/String;
    move-object/from16 v0, p0

    #@151
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sib:Ljava/lang/String;

    #@153
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->siToServiceClass(Ljava/lang/String;)I

    #@156
    move-result v7

    #@157
    .line 1050
    .local v7, serviceClass:I
    move-object/from16 v0, p0

    #@159
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@15b
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->scToCallForwardReason(Ljava/lang/String;)I

    #@15e
    move-result v6

    #@15f
    .line 1051
    .local v6, reason:I
    move-object/from16 v0, p0

    #@161
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sic:Ljava/lang/String;

    #@163
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->siToTime(Ljava/lang/String;)I

    #@166
    move-result v9

    #@167
    .line 1053
    .local v9, time:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isInterrogate()Z

    #@16a
    move-result v4

    #@16b
    if-eqz v4, :cond_181

    #@16d
    .line 1054
    move-object/from16 v0, p0

    #@16f
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@171
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@173
    const/4 v10, 0x3

    #@174
    move-object/from16 v0, p0

    #@176
    move-object/from16 v1, p0

    #@178
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@17b
    move-result-object v10

    #@17c
    invoke-interface {v4, v6, v7, v8, v10}, Lcom/android/internal/telephony/CommandsInterface;->queryCallForwardStatus(IILjava/lang/String;Landroid/os/Message;)V

    #@17f
    goto/16 :goto_49

    #@181
    .line 1060
    :cond_181
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@184
    move-result v4

    #@185
    if-eqz v4, :cond_1c5

    #@187
    .line 1062
    if-eqz v8, :cond_1c3

    #@189
    .line 1063
    const/4 v4, 0x1

    #@18a
    move-object/from16 v0, p0

    #@18c
    iput-boolean v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isCallFwdRegister:Z

    #@18e
    .line 1064
    const/4 v5, 0x3

    #@18f
    .line 1079
    .local v5, cfAction:I
    :goto_18f
    if-eqz v6, :cond_194

    #@191
    const/4 v4, 0x4

    #@192
    if-ne v6, v4, :cond_1e5

    #@194
    :cond_194
    and-int/lit8 v4, v7, 0x1

    #@196
    if-nez v4, :cond_19a

    #@198
    if-nez v7, :cond_1e5

    #@19a
    :cond_19a
    const/16 v19, 0x1

    #@19c
    .line 1085
    .local v19, isSettingUnconditionalVoice:I
    :goto_19c
    const/4 v4, 0x1

    #@19d
    if-eq v5, v4, :cond_1a2

    #@19f
    const/4 v4, 0x3

    #@1a0
    if-ne v5, v4, :cond_1e8

    #@1a2
    :cond_1a2
    const/16 v18, 0x1

    #@1a4
    .line 1089
    .local v18, isEnableDesired:I
    :goto_1a4
    const-string v4, "GSM"

    #@1a6
    const-string v10, "is CF setCallForward"

    #@1a8
    invoke-static {v4, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ab
    .line 1090
    move-object/from16 v0, p0

    #@1ad
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1af
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1b1
    const/4 v10, 0x6

    #@1b2
    move-object/from16 v0, p0

    #@1b4
    move/from16 v1, v19

    #@1b6
    move/from16 v2, v18

    #@1b8
    move-object/from16 v3, p0

    #@1ba
    invoke-virtual {v0, v10, v1, v2, v3}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@1bd
    move-result-object v10

    #@1be
    invoke-interface/range {v4 .. v10}, Lcom/android/internal/telephony/CommandsInterface;->setCallForward(IIILjava/lang/String;ILandroid/os/Message;)V

    #@1c1
    goto/16 :goto_49

    #@1c3
    .line 1066
    .end local v5           #cfAction:I
    .end local v18           #isEnableDesired:I
    .end local v19           #isSettingUnconditionalVoice:I
    :cond_1c3
    const/4 v5, 0x1

    #@1c4
    .restart local v5       #cfAction:I
    goto :goto_18f

    #@1c5
    .line 1069
    .end local v5           #cfAction:I
    :cond_1c5
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isDeactivate()Z

    #@1c8
    move-result v4

    #@1c9
    if-eqz v4, :cond_1cd

    #@1cb
    .line 1070
    const/4 v5, 0x0

    #@1cc
    .restart local v5       #cfAction:I
    goto :goto_18f

    #@1cd
    .line 1071
    .end local v5           #cfAction:I
    :cond_1cd
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isRegister()Z

    #@1d0
    move-result v4

    #@1d1
    if-eqz v4, :cond_1d5

    #@1d3
    .line 1072
    const/4 v5, 0x3

    #@1d4
    .restart local v5       #cfAction:I
    goto :goto_18f

    #@1d5
    .line 1073
    .end local v5           #cfAction:I
    :cond_1d5
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isErasure()Z

    #@1d8
    move-result v4

    #@1d9
    if-eqz v4, :cond_1dd

    #@1db
    .line 1074
    const/4 v5, 0x4

    #@1dc
    .restart local v5       #cfAction:I
    goto :goto_18f

    #@1dd
    .line 1076
    .end local v5           #cfAction:I
    :cond_1dd
    new-instance v4, Ljava/lang/RuntimeException;

    #@1df
    const-string v10, "invalid action"

    #@1e1
    invoke-direct {v4, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1e4
    throw v4

    #@1e5
    .line 1079
    .restart local v5       #cfAction:I
    :cond_1e5
    const/16 v19, 0x0

    #@1e7
    goto :goto_19c

    #@1e8
    .line 1085
    .restart local v19       #isSettingUnconditionalVoice:I
    :cond_1e8
    const/16 v18, 0x0

    #@1ea
    goto :goto_1a4

    #@1eb
    .line 1096
    .end local v5           #cfAction:I
    .end local v6           #reason:I
    .end local v7           #serviceClass:I
    .end local v8           #dialingNumber:Ljava/lang/String;
    .end local v9           #time:I
    .end local v19           #isSettingUnconditionalVoice:I
    :cond_1eb
    move-object/from16 v0, p0

    #@1ed
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@1ef
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isServiceCodeCallBarring(Ljava/lang/String;)Z

    #@1f2
    move-result v4

    #@1f3
    if-eqz v4, :cond_250

    #@1f5
    .line 1100
    move-object/from16 v0, p0

    #@1f7
    iget-object v13, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sia:Ljava/lang/String;

    #@1f9
    .line 1101
    .local v13, password:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1fb
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sib:Ljava/lang/String;

    #@1fd
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->siToServiceClass(Ljava/lang/String;)I

    #@200
    move-result v7

    #@201
    .line 1102
    .restart local v7       #serviceClass:I
    move-object/from16 v0, p0

    #@203
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@205
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->scToBarringFacility(Ljava/lang/String;)Ljava/lang/String;

    #@208
    move-result-object v11

    #@209
    .line 1104
    .local v11, facility:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isInterrogate()Z

    #@20c
    move-result v4

    #@20d
    if-eqz v4, :cond_223

    #@20f
    .line 1105
    move-object/from16 v0, p0

    #@211
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@213
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@215
    const/4 v10, 0x5

    #@216
    move-object/from16 v0, p0

    #@218
    move-object/from16 v1, p0

    #@21a
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@21d
    move-result-object v10

    #@21e
    invoke-interface {v4, v11, v13, v7, v10}, Lcom/android/internal/telephony/CommandsInterface;->queryFacilityLock(Ljava/lang/String;Ljava/lang/String;ILandroid/os/Message;)V

    #@221
    goto/16 :goto_49

    #@223
    .line 1107
    :cond_223
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@226
    move-result v4

    #@227
    if-nez v4, :cond_22f

    #@229
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isDeactivate()Z

    #@22c
    move-result v4

    #@22d
    if-eqz v4, :cond_248

    #@22f
    .line 1108
    :cond_22f
    move-object/from16 v0, p0

    #@231
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@233
    iget-object v10, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@235
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@238
    move-result v12

    #@239
    const/4 v4, 0x1

    #@23a
    move-object/from16 v0, p0

    #@23c
    move-object/from16 v1, p0

    #@23e
    invoke-virtual {v0, v4, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@241
    move-result-object v15

    #@242
    move v14, v7

    #@243
    invoke-interface/range {v10 .. v15}, Lcom/android/internal/telephony/CommandsInterface;->setFacilityLock(Ljava/lang/String;ZLjava/lang/String;ILandroid/os/Message;)V

    #@246
    goto/16 :goto_49

    #@248
    .line 1111
    :cond_248
    new-instance v4, Ljava/lang/RuntimeException;

    #@24a
    const-string v10, "Invalid or Unsupported MMI Code"

    #@24c
    invoke-direct {v4, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24f
    throw v4

    #@250
    .line 1114
    .end local v7           #serviceClass:I
    .end local v11           #facility:Ljava/lang/String;
    .end local v13           #password:Ljava/lang/String;
    :cond_250
    move-object/from16 v0, p0

    #@252
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@254
    if-eqz v4, :cond_2c7

    #@256
    move-object/from16 v0, p0

    #@258
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@25a
    const-string v10, "03"

    #@25c
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25f
    move-result v4

    #@260
    if-eqz v4, :cond_2c7

    #@262
    .line 1120
    move-object/from16 v0, p0

    #@264
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sib:Ljava/lang/String;

    #@266
    move-object/from16 v23, v0

    #@268
    .line 1121
    .local v23, oldPwd:Ljava/lang/String;
    move-object/from16 v0, p0

    #@26a
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sic:Ljava/lang/String;

    #@26c
    move-object/from16 v21, v0

    #@26e
    .line 1122
    .local v21, newPwd:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@271
    move-result v4

    #@272
    if-nez v4, :cond_27a

    #@274
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isRegister()Z

    #@277
    move-result v4

    #@278
    if-eqz v4, :cond_2bf

    #@27a
    .line 1124
    :cond_27a
    const-string v4, "**"

    #@27c
    move-object/from16 v0, p0

    #@27e
    iput-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@280
    .line 1126
    move-object/from16 v0, p0

    #@282
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sia:Ljava/lang/String;

    #@284
    if-nez v4, :cond_2ac

    #@286
    .line 1128
    const-string v11, "AB"

    #@288
    .line 1132
    .restart local v11       #facility:Ljava/lang/String;
    :goto_288
    move-object/from16 v0, p0

    #@28a
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->pwd:Ljava/lang/String;

    #@28c
    move-object/from16 v0, v21

    #@28e
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@291
    move-result v4

    #@292
    if-eqz v4, :cond_2b5

    #@294
    .line 1133
    move-object/from16 v0, p0

    #@296
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@298
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@29a
    const/4 v10, 0x1

    #@29b
    move-object/from16 v0, p0

    #@29d
    move-object/from16 v1, p0

    #@29f
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2a2
    move-result-object v10

    #@2a3
    move-object/from16 v0, v23

    #@2a5
    move-object/from16 v1, v21

    #@2a7
    invoke-interface {v4, v11, v0, v1, v10}, Lcom/android/internal/telephony/CommandsInterface;->changeBarringPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@2aa
    goto/16 :goto_49

    #@2ac
    .line 1130
    .end local v11           #facility:Ljava/lang/String;
    :cond_2ac
    move-object/from16 v0, p0

    #@2ae
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sia:Ljava/lang/String;

    #@2b0
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->scToBarringFacility(Ljava/lang/String;)Ljava/lang/String;

    #@2b3
    move-result-object v11

    #@2b4
    .restart local v11       #facility:Ljava/lang/String;
    goto :goto_288

    #@2b5
    .line 1137
    :cond_2b5
    const v4, 0x1040099

    #@2b8
    move-object/from16 v0, p0

    #@2ba
    invoke-direct {v0, v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->handlePasswordError(I)V

    #@2bd
    goto/16 :goto_49

    #@2bf
    .line 1140
    .end local v11           #facility:Ljava/lang/String;
    :cond_2bf
    new-instance v4, Ljava/lang/RuntimeException;

    #@2c1
    const-string v10, "Invalid or Unsupported MMI Code"

    #@2c3
    invoke-direct {v4, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2c6
    throw v4

    #@2c7
    .line 1143
    .end local v21           #newPwd:Ljava/lang/String;
    .end local v23           #oldPwd:Ljava/lang/String;
    :cond_2c7
    move-object/from16 v0, p0

    #@2c9
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2cb
    if-eqz v4, :cond_327

    #@2cd
    move-object/from16 v0, p0

    #@2cf
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@2d1
    const-string v10, "43"

    #@2d3
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d6
    move-result v4

    #@2d7
    if-eqz v4, :cond_327

    #@2d9
    .line 1145
    move-object/from16 v0, p0

    #@2db
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sia:Ljava/lang/String;

    #@2dd
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->siToServiceClass(Ljava/lang/String;)I

    #@2e0
    move-result v7

    #@2e1
    .line 1147
    .restart local v7       #serviceClass:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@2e4
    move-result v4

    #@2e5
    if-nez v4, :cond_2ed

    #@2e7
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isDeactivate()Z

    #@2ea
    move-result v4

    #@2eb
    if-eqz v4, :cond_305

    #@2ed
    .line 1148
    :cond_2ed
    move-object/from16 v0, p0

    #@2ef
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2f1
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2f3
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isActivate()Z

    #@2f6
    move-result v10

    #@2f7
    const/4 v12, 0x1

    #@2f8
    move-object/from16 v0, p0

    #@2fa
    move-object/from16 v1, p0

    #@2fc
    invoke-virtual {v0, v12, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2ff
    move-result-object v12

    #@300
    invoke-interface {v4, v10, v7, v12}, Lcom/android/internal/telephony/CommandsInterface;->setCallWaiting(ZILandroid/os/Message;)V

    #@303
    goto/16 :goto_49

    #@305
    .line 1150
    :cond_305
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isInterrogate()Z

    #@308
    move-result v4

    #@309
    if-eqz v4, :cond_31f

    #@30b
    .line 1151
    move-object/from16 v0, p0

    #@30d
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@30f
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@311
    const/4 v10, 0x5

    #@312
    move-object/from16 v0, p0

    #@314
    move-object/from16 v1, p0

    #@316
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@319
    move-result-object v10

    #@31a
    invoke-interface {v4, v7, v10}, Lcom/android/internal/telephony/CommandsInterface;->queryCallWaiting(ILandroid/os/Message;)V

    #@31d
    goto/16 :goto_49

    #@31f
    .line 1154
    :cond_31f
    new-instance v4, Ljava/lang/RuntimeException;

    #@321
    const-string v10, "Invalid or Unsupported MMI Code"

    #@323
    invoke-direct {v4, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@326
    throw v4

    #@327
    .line 1156
    .end local v7           #serviceClass:I
    :cond_327
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPinCommand()Z

    #@32a
    move-result v4

    #@32b
    if-eqz v4, :cond_42e

    #@32d
    .line 1160
    move-object/from16 v0, p0

    #@32f
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sia:Ljava/lang/String;

    #@331
    move-object/from16 v22, v0

    #@333
    .line 1161
    .local v22, oldPinOrPuk:Ljava/lang/String;
    move-object/from16 v0, p0

    #@335
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sib:Ljava/lang/String;

    #@337
    move-object/from16 v20, v0

    #@339
    .line 1162
    .local v20, newPin:Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    #@33c
    move-result v24

    #@33d
    .line 1163
    .local v24, pinLen:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isRegister()Z

    #@340
    move-result v4

    #@341
    if-eqz v4, :cond_426

    #@343
    .line 1164
    move-object/from16 v0, p0

    #@345
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sic:Ljava/lang/String;

    #@347
    move-object/from16 v0, v20

    #@349
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34c
    move-result v4

    #@34d
    if-nez v4, :cond_359

    #@34f
    .line 1166
    const v4, 0x104009d

    #@352
    move-object/from16 v0, p0

    #@354
    invoke-direct {v0, v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->handlePasswordError(I)V

    #@357
    goto/16 :goto_49

    #@359
    .line 1167
    :cond_359
    const/4 v4, 0x4

    #@35a
    move/from16 v0, v24

    #@35c
    if-lt v0, v4, :cond_364

    #@35e
    const/16 v4, 0x8

    #@360
    move/from16 v0, v24

    #@362
    if-le v0, v4, :cond_36e

    #@364
    .line 1169
    :cond_364
    const v4, 0x104009e

    #@367
    move-object/from16 v0, p0

    #@369
    invoke-direct {v0, v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->handlePasswordError(I)V

    #@36c
    goto/16 :goto_49

    #@36e
    .line 1170
    :cond_36e
    move-object/from16 v0, p0

    #@370
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@372
    const-string v10, "04"

    #@374
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@377
    move-result v4

    #@378
    if-eqz v4, :cond_396

    #@37a
    move-object/from16 v0, p0

    #@37c
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@37e
    if-eqz v4, :cond_396

    #@380
    move-object/from16 v0, p0

    #@382
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@384
    invoke-virtual {v4}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@387
    move-result-object v4

    #@388
    sget-object v10, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_PUK:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@38a
    if-ne v4, v10, :cond_396

    #@38c
    .line 1174
    const v4, 0x10400a0

    #@38f
    move-object/from16 v0, p0

    #@391
    invoke-direct {v0, v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->handlePasswordError(I)V

    #@394
    goto/16 :goto_49

    #@396
    .line 1177
    :cond_396
    move-object/from16 v0, p0

    #@398
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@39a
    const-string v10, "04"

    #@39c
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39f
    move-result v4

    #@3a0
    if-eqz v4, :cond_3ba

    #@3a2
    .line 1178
    move-object/from16 v0, p0

    #@3a4
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3a6
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3a8
    const/4 v10, 0x1

    #@3a9
    move-object/from16 v0, p0

    #@3ab
    move-object/from16 v1, p0

    #@3ad
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@3b0
    move-result-object v10

    #@3b1
    move-object/from16 v0, v22

    #@3b3
    move-object/from16 v1, v20

    #@3b5
    invoke-interface {v4, v0, v1, v10}, Lcom/android/internal/telephony/CommandsInterface;->changeIccPin(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@3b8
    goto/16 :goto_49

    #@3ba
    .line 1180
    :cond_3ba
    move-object/from16 v0, p0

    #@3bc
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@3be
    const-string v10, "042"

    #@3c0
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c3
    move-result v4

    #@3c4
    if-eqz v4, :cond_3de

    #@3c6
    .line 1181
    move-object/from16 v0, p0

    #@3c8
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3ca
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3cc
    const/4 v10, 0x1

    #@3cd
    move-object/from16 v0, p0

    #@3cf
    move-object/from16 v1, p0

    #@3d1
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@3d4
    move-result-object v10

    #@3d5
    move-object/from16 v0, v22

    #@3d7
    move-object/from16 v1, v20

    #@3d9
    invoke-interface {v4, v0, v1, v10}, Lcom/android/internal/telephony/CommandsInterface;->changeIccPin2(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@3dc
    goto/16 :goto_49

    #@3de
    .line 1183
    :cond_3de
    move-object/from16 v0, p0

    #@3e0
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@3e2
    const-string v10, "05"

    #@3e4
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e7
    move-result v4

    #@3e8
    if-eqz v4, :cond_402

    #@3ea
    .line 1184
    move-object/from16 v0, p0

    #@3ec
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3ee
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3f0
    const/4 v10, 0x1

    #@3f1
    move-object/from16 v0, p0

    #@3f3
    move-object/from16 v1, p0

    #@3f5
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@3f8
    move-result-object v10

    #@3f9
    move-object/from16 v0, v22

    #@3fb
    move-object/from16 v1, v20

    #@3fd
    invoke-interface {v4, v0, v1, v10}, Lcom/android/internal/telephony/CommandsInterface;->supplyIccPuk(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@400
    goto/16 :goto_49

    #@402
    .line 1186
    :cond_402
    move-object/from16 v0, p0

    #@404
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@406
    const-string v10, "052"

    #@408
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40b
    move-result v4

    #@40c
    if-eqz v4, :cond_49

    #@40e
    .line 1187
    move-object/from16 v0, p0

    #@410
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@412
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@414
    const/4 v10, 0x1

    #@415
    move-object/from16 v0, p0

    #@417
    move-object/from16 v1, p0

    #@419
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@41c
    move-result-object v10

    #@41d
    move-object/from16 v0, v22

    #@41f
    move-object/from16 v1, v20

    #@421
    invoke-interface {v4, v0, v1, v10}, Lcom/android/internal/telephony/CommandsInterface;->supplyIccPuk2(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@424
    goto/16 :goto_49

    #@426
    .line 1192
    :cond_426
    new-instance v4, Ljava/lang/RuntimeException;

    #@428
    const-string v10, "Invalid or Unsupported MMI Code"

    #@42a
    invoke-direct {v4, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@42d
    throw v4

    #@42e
    .line 1194
    .end local v20           #newPin:Ljava/lang/String;
    .end local v22           #oldPinOrPuk:Ljava/lang/String;
    .end local v24           #pinLen:I
    :cond_42e
    move-object/from16 v0, p0

    #@430
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@432
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isServiceCodeUnsupported(Ljava/lang/String;)Z

    #@435
    move-result v4

    #@436
    if-eqz v4, :cond_474

    #@438
    .line 1195
    const-string v4, "GSM"

    #@43a
    new-instance v10, Ljava/lang/StringBuilder;

    #@43c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@43f
    const-string v12, "Unsupported MMI code: "

    #@441
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@444
    move-result-object v10

    #@445
    move-object/from16 v0, p0

    #@447
    iget-object v12, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@449
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44c
    move-result-object v10

    #@44d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@450
    move-result-object v10

    #@451
    invoke-static {v4, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@454
    .line 1196
    sget-object v4, Lcom/android/internal/telephony/MmiCode$State;->FAILED:Lcom/android/internal/telephony/MmiCode$State;

    #@456
    move-object/from16 v0, p0

    #@458
    iput-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->state:Lcom/android/internal/telephony/MmiCode$State;

    #@45a
    .line 1197
    move-object/from16 v0, p0

    #@45c
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->context:Landroid/content/Context;

    #@45e
    const v10, 0x1040092

    #@461
    invoke-virtual {v4, v10}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@464
    move-result-object v4

    #@465
    move-object/from16 v0, p0

    #@467
    iput-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->message:Ljava/lang/CharSequence;

    #@469
    .line 1198
    move-object/from16 v0, p0

    #@46b
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@46d
    move-object/from16 v0, p0

    #@46f
    invoke-virtual {v4, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@472
    goto/16 :goto_49

    #@474
    .line 1199
    :cond_474
    move-object/from16 v0, p0

    #@476
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->poundString:Ljava/lang/String;

    #@478
    if-eqz v4, :cond_485

    #@47a
    .line 1200
    move-object/from16 v0, p0

    #@47c
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->poundString:Ljava/lang/String;

    #@47e
    move-object/from16 v0, p0

    #@480
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sendUssd(Ljava/lang/String;)V

    #@483
    goto/16 :goto_49

    #@485
    .line 1202
    :cond_485
    new-instance v4, Ljava/lang/RuntimeException;

    #@487
    const-string v10, "Invalid or Unsupported MMI Code"

    #@489
    invoke-direct {v4, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@48c
    throw v4
    :try_end_48d
    .catch Ljava/lang/RuntimeException; {:try_start_81 .. :try_end_48d} :catch_61
.end method

.method processSsData(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "data"

    #@0
    .prologue
    .line 413
    const-string v2, "GSM"

    #@2
    const-string v3, "In processSsData"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 415
    const/4 v2, 0x1

    #@8
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isSsInfo:Z

    #@a
    .line 417
    :try_start_a
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c
    check-cast v1, Lcom/android/internal/telephony/gsm/SsData;

    #@e
    .line 418
    .local v1, ssData:Lcom/android/internal/telephony/gsm/SsData;
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->parseSsData(Lcom/android/internal/telephony/gsm/SsData;)V
    :try_end_11
    .catch Ljava/lang/ClassCastException; {:try_start_a .. :try_end_11} :catch_12
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_11} :catch_2c

    #@11
    .line 424
    .end local v1           #ssData:Lcom/android/internal/telephony/gsm/SsData;
    :goto_11
    return-void

    #@12
    .line 419
    :catch_12
    move-exception v0

    #@13
    .line 420
    .local v0, ex:Ljava/lang/ClassCastException;
    const-string v2, "GSM"

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v4, "Exception in parsing SS Data : "

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_11

    #@2c
    .line 421
    .end local v0           #ex:Ljava/lang/ClassCastException;
    :catch_2c
    move-exception v0

    #@2d
    .line 422
    .local v0, ex:Ljava/lang/NullPointerException;
    const-string v2, "GSM"

    #@2f
    new-instance v3, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v4, "Exception in parsing SS Data : "

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    goto :goto_11
.end method

.method sendUssd(Ljava/lang/String;)V
    .registers 4
    .parameter "ussdMessage"

    #@0
    .prologue
    .line 1272
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPendingUSSD:Z

    #@3
    .line 1279
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@5
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    const/4 v1, 0x4

    #@8
    invoke-virtual {p0, v1, p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v1

    #@c
    invoke-interface {v0, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->sendUSSD(Ljava/lang/String;Landroid/os/Message;)V

    #@f
    .line 1281
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 2163
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "GsmMmiCode {"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 2165
    .local v0, sb:Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "State="

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getState()Lcom/android/internal/telephony/MmiCode$State;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 2166
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@23
    if-eqz v1, :cond_3d

    #@25
    new-instance v1, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v2, " action="

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->action:Ljava/lang/String;

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    .line 2167
    :cond_3d
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@3f
    if-eqz v1, :cond_59

    #@41
    new-instance v1, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v2, " sc="

    #@48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v1

    #@4c
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sc:Ljava/lang/String;

    #@4e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v1

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    .line 2168
    :cond_59
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sia:Ljava/lang/String;

    #@5b
    if-eqz v1, :cond_75

    #@5d
    new-instance v1, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v2, " sia="

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v1

    #@68
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sia:Ljava/lang/String;

    #@6a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v1

    #@6e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v1

    #@72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    .line 2169
    :cond_75
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sib:Ljava/lang/String;

    #@77
    if-eqz v1, :cond_91

    #@79
    new-instance v1, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v2, " sib="

    #@80
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v1

    #@84
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sib:Ljava/lang/String;

    #@86
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v1

    #@8a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v1

    #@8e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    .line 2170
    :cond_91
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sic:Ljava/lang/String;

    #@93
    if-eqz v1, :cond_ad

    #@95
    new-instance v1, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v2, " sic="

    #@9c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v1

    #@a0
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sic:Ljava/lang/String;

    #@a2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v1

    #@a6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v1

    #@aa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    .line 2171
    :cond_ad
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->poundString:Ljava/lang/String;

    #@af
    if-eqz v1, :cond_c9

    #@b1
    new-instance v1, Ljava/lang/StringBuilder;

    #@b3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b6
    const-string v2, " poundString="

    #@b8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v1

    #@bc
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->poundString:Ljava/lang/String;

    #@be
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v1

    #@c2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v1

    #@c6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    .line 2172
    :cond_c9
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@cb
    if-eqz v1, :cond_e5

    #@cd
    new-instance v1, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v2, " dialingNumber="

    #@d4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v1

    #@d8
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@da
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v1

    #@de
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v1

    #@e2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    .line 2173
    :cond_e5
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->pwd:Ljava/lang/String;

    #@e7
    if-eqz v1, :cond_101

    #@e9
    new-instance v1, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    const-string v2, " pwd="

    #@f0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v1

    #@f4
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->pwd:Ljava/lang/String;

    #@f6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v1

    #@fa
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v1

    #@fe
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    .line 2174
    :cond_101
    const-string v1, "}"

    #@103
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    .line 2175
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v1

    #@10a
    return-object v1
.end method
