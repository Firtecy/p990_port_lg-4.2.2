.class Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;
.super Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;
.source "IccPhonebookProviderBackendImp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "qctIO"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;,
        Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$Result;
    }
.end annotation


# instance fields
.field private final mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer",
            "<",
            "Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;",
            ">;"
        }
    .end annotation
.end field

.field private mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

.field final synthetic this$0:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;)V
    .registers 5
    .parameter

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 162
    iput-object p1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->this$0:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;

    #@3
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;-><init>()V

    #@6
    .line 203
    new-instance v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@8
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->this$0:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;

    #@a
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;-><init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$1;)V

    #@d
    iput-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@f
    .line 204
    iput-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@11
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;-><init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;)V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->updateGroup(Landroid/content/Context;ILandroid/content/ContentValues;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1100(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;I)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->deleteGroup(Landroid/content/Context;I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;I)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->read(Landroid/content/Context;I)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)Landroid/database/Cursor;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->newQueryEntryCursor(Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)Landroid/database/Cursor;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->insert(Landroid/content/Context;ILandroid/content/ContentValues;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->update(Landroid/content/Context;ILandroid/content/ContentValues;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;I)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->delete(Landroid/content/Context;I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;I)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->readGroup(Landroid/content/Context;I)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)Landroid/database/Cursor;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->newQueryGroupCursor(Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)Landroid/database/Cursor;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;Landroid/content/ContentValues;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->insertGroup(Landroid/content/Context;Landroid/content/ContentValues;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private delete(Landroid/content/Context;I)I
    .registers 7
    .parameter "context"
    .parameter "simIndex"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 397
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@3
    if-nez v2, :cond_c

    #@5
    .line 398
    new-instance v2, Lcom/android/internal/telephony/uicc/UsimService;

    #@7
    invoke-direct {v2, p1}, Lcom/android/internal/telephony/uicc/UsimService;-><init>(Landroid/content/Context;)V

    #@a
    iput-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@c
    .line 400
    :cond_c
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@e
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/uicc/UsimService;->registerCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@11
    .line 402
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@13
    invoke-static {v2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1300(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)V

    #@16
    .line 403
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@18
    invoke-virtual {v2, v1, p2}, Lcom/android/internal/telephony/uicc/UsimService;->PBMDeleteRecord(II)V

    #@1b
    .line 404
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@1d
    invoke-static {v2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1400(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;

    #@23
    .line 405
    .local v0, result:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@25
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/uicc/UsimService;->unregisterCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@28
    .line 407
    if-eqz v0, :cond_4e

    #@2a
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->isSuccess()Z

    #@2d
    move-result v2

    #@2e
    if-eqz v2, :cond_4e

    #@30
    .line 408
    const-string v1, "IccPhonebookProvider"

    #@32
    new-instance v2, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v3, "delete result code="

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    iget v3, v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->mReturnCode:I

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 409
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->getIndex()I

    #@4d
    move-result v1

    #@4e
    .line 411
    :cond_4e
    return v1
.end method

.method private deleteGroup(Landroid/content/Context;I)I
    .registers 7
    .parameter "context"
    .parameter "groupIndex"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 483
    if-gtz p2, :cond_b

    #@3
    .line 484
    const-string v2, "IccPhonebookProvider"

    #@5
    const-string v3, "gas_id MUST be greater than 0!"

    #@7
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 498
    :cond_a
    :goto_a
    return v1

    #@b
    .line 487
    :cond_b
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@d
    if-nez v2, :cond_16

    #@f
    .line 488
    new-instance v2, Lcom/android/internal/telephony/uicc/UsimService;

    #@11
    invoke-direct {v2, p1}, Lcom/android/internal/telephony/uicc/UsimService;-><init>(Landroid/content/Context;)V

    #@14
    iput-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@16
    .line 490
    :cond_16
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@18
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/uicc/UsimService;->registerCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@1b
    .line 491
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@1d
    invoke-static {v2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1300(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)V

    #@20
    .line 492
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@22
    const/16 v3, 0x20

    #@24
    invoke-virtual {v2, v3, p2}, Lcom/android/internal/telephony/uicc/UsimService;->PBMDeleteRecord(II)V

    #@27
    .line 493
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@29
    invoke-static {v2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1400(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)Ljava/lang/Object;

    #@2c
    move-result-object v0

    #@2d
    check-cast v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;

    #@2f
    .line 494
    .local v0, result:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@31
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/uicc/UsimService;->unregisterCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@34
    .line 496
    if-eqz v0, :cond_a

    #@36
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->isSuccess()Z

    #@39
    move-result v2

    #@3a
    if-eqz v2, :cond_a

    #@3c
    .line 497
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->getIndex()I

    #@3f
    move-result v1

    #@40
    goto :goto_a
.end method

.method private getInt(Landroid/content/ContentValues;Ljava/lang/String;I)I
    .registers 7
    .parameter "values"
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 360
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 362
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@d
    move-result p3

    #@e
    .line 366
    .end local p3
    :goto_e
    return p3

    #@f
    .line 364
    .restart local p3
    :cond_f
    const-string v0, "IccPhonebookProvider"

    #@11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, " (int) column is ommitted, use default value="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_e
.end method

.method private getString(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "values"
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 372
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 373
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object p3

    #@a
    .line 377
    .end local p3
    :goto_a
    return-object p3

    #@b
    .line 375
    .restart local p3
    :cond_b
    const-string v0, "IccPhonebookProvider"

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, " (String) column is ommitted, use defalue value="

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    goto :goto_a
.end method

.method private insert(Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 8
    .parameter "context"
    .parameter "simIndex"
    .parameter "values"

    #@0
    .prologue
    .line 230
    if-gtz p2, :cond_1c

    #@2
    .line 231
    const-string v1, "IccPhonebookProvider"

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "invalid sim index="

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 232
    const/4 v1, 0x0

    #@1b
    .line 237
    :goto_1b
    return v1

    #@1c
    .line 234
    :cond_1c
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->newNativeRecordWithoutSimIndex(Landroid/content/ContentValues;)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@1f
    move-result-object v0

    #@20
    .line 235
    .local v0, records:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    iput p2, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@22
    .line 237
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->write(Landroid/content/Context;Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)I

    #@25
    move-result v1

    #@26
    goto :goto_1b
.end method

.method private insertGroup(Landroid/content/Context;Landroid/content/ContentValues;)I
    .registers 6
    .parameter "context"
    .parameter "values"

    #@0
    .prologue
    .line 435
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->newNativeGroupRecordWithoutGroupIndex(Landroid/content/ContentValues;)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@3
    move-result-object v0

    #@4
    .line 436
    .local v0, records:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    iget v1, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@6
    if-eqz v1, :cond_f

    #@8
    .line 437
    const-string v1, "IccPhonebookProvider"

    #@a
    const-string v2, "index MUST be 0, provided group index will be ignored"

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 439
    :cond_f
    const/4 v1, 0x0

    #@10
    iput v1, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@12
    .line 441
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->write(Landroid/content/Context;Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)I

    #@15
    move-result v1

    #@16
    return v1
.end method

.method private newNativeGroupRecordWithoutGroupIndex(Landroid/content/ContentValues;)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    .registers 5
    .parameter "values"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 447
    new-instance v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@3
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;-><init>()V

    #@6
    .line 449
    .local v0, records:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    const/16 v1, 0x20

    #@8
    iput v1, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->device:I

    #@a
    .line 450
    iput v2, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->sync_cnt:I

    #@c
    .line 451
    iput v2, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->type:I

    #@e
    .line 452
    iput v2, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@10
    .line 454
    iput v2, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->gas_id:I

    #@12
    .line 455
    iput v2, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@14
    .line 456
    sget-object v1, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Group$Column;->NAME:Ljava/lang/String;

    #@16
    const-string v2, ""

    #@18
    invoke-direct {p0, p1, v1, v2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->getString(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    iput-object v1, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@1e
    .line 457
    iget-object v1, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@20
    if-nez v1, :cond_26

    #@22
    const-string v1, ""

    #@24
    iput-object v1, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@26
    .line 458
    :cond_26
    const-string v1, ""

    #@28
    iput-object v1, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->number:Ljava/lang/String;

    #@2a
    .line 459
    const-string v1, ""

    #@2c
    iput-object v1, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number:Ljava/lang/String;

    #@2e
    .line 460
    const-string v1, ""

    #@30
    iput-object v1, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_a:Ljava/lang/String;

    #@32
    .line 461
    const-string v1, ""

    #@34
    iput-object v1, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_b:Ljava/lang/String;

    #@36
    .line 462
    const-string v1, ""

    #@38
    iput-object v1, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->email_address:Ljava/lang/String;

    #@3a
    .line 464
    return-object v0
.end method

.method private newNativeRecordWithoutSimIndex(Landroid/content/ContentValues;)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    .registers 13
    .parameter "values"

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    const/4 v9, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 243
    new-instance v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@5
    invoke-direct {v5}, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;-><init>()V

    #@8
    .line 245
    .local v5, records:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    iput v8, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->device:I

    #@a
    .line 246
    iput v8, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->sync_cnt:I

    #@c
    .line 247
    iput v8, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->type:I

    #@e
    .line 248
    iput v8, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@10
    .line 253
    sget-object v6, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;->ALPHA_TAG:Ljava/lang/String;

    #@12
    const-string v7, ""

    #@14
    invoke-direct {p0, p1, v6, v7}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->getString(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v6

    #@18
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@1a
    .line 254
    iget-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@1c
    if-nez v6, :cond_22

    #@1e
    const-string v6, ""

    #@20
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@22
    .line 256
    :cond_22
    sget-object v6, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;->NUMBER:Ljava/lang/String;

    #@24
    const-string v7, ""

    #@26
    invoke-direct {p0, p1, v6, v7}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->getString(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    .line 258
    .local v4, number:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2d
    move-result v6

    #@2e
    if-eqz v6, :cond_73

    #@30
    .line 259
    const-string v6, ""

    #@32
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->number:Ljava/lang/String;

    #@34
    .line 265
    :goto_34
    sget-object v6, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;->ANRS:Ljava/lang/String;

    #@36
    const-string v7, ""

    #@38
    invoke-direct {p0, p1, v6, v7}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->getString(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3b
    move-result-object v6

    #@3c
    const-string v7, "~"

    #@3e
    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@41
    move-result-object v0

    #@42
    .line 266
    .local v0, anrs:[Ljava/lang/String;
    if-eqz v0, :cond_47

    #@44
    array-length v6, v0

    #@45
    if-nez v6, :cond_82

    #@47
    .line 267
    :cond_47
    const-string v6, ""

    #@49
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number:Ljava/lang/String;

    #@4b
    .line 268
    const-string v6, ""

    #@4d
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_a:Ljava/lang/String;

    #@4f
    .line 269
    const-string v6, ""

    #@51
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_b:Ljava/lang/String;

    #@53
    .line 309
    :cond_53
    sget-object v6, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;->EMAILS:Ljava/lang/String;

    #@55
    const-string v7, ""

    #@57
    invoke-direct {p0, p1, v6, v7}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->getString(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5a
    move-result-object v6

    #@5b
    const-string v7, "~"

    #@5d
    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@60
    move-result-object v1

    #@61
    .line 310
    .local v1, emails:[Ljava/lang/String;
    if-eqz v1, :cond_66

    #@63
    array-length v6, v1

    #@64
    if-nez v6, :cond_e9

    #@66
    .line 311
    :cond_66
    const-string v6, ""

    #@68
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->email_address:Ljava/lang/String;

    #@6a
    .line 326
    :goto_6a
    sget-object v6, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;->GROUP_INDEX:Ljava/lang/String;

    #@6c
    invoke-direct {p0, p1, v6, v8}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->getInt(Landroid/content/ContentValues;Ljava/lang/String;I)I

    #@6f
    move-result v6

    #@70
    iput v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->gas_id:I

    #@72
    .line 331
    return-object v5

    #@73
    .line 261
    .end local v0           #anrs:[Ljava/lang/String;
    .end local v1           #emails:[Ljava/lang/String;
    :cond_73
    invoke-static {v4}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1500(Ljava/lang/String;)I

    #@76
    move-result v6

    #@77
    iput v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->type:I

    #@79
    .line 262
    iget v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->type:I

    #@7b
    invoke-static {v4, v6, v9}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1600(Ljava/lang/String;IZ)Ljava/lang/String;

    #@7e
    move-result-object v6

    #@7f
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->number:Ljava/lang/String;

    #@81
    goto :goto_34

    #@82
    .line 278
    .restart local v0       #anrs:[Ljava/lang/String;
    :cond_82
    array-length v3, v0

    #@83
    .line 280
    .local v3, length:I
    const-string v6, ""

    #@85
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number:Ljava/lang/String;

    #@87
    .line 281
    const-string v6, ""

    #@89
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_a:Ljava/lang/String;

    #@8b
    .line 282
    const-string v6, ""

    #@8d
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_b:Ljava/lang/String;

    #@8f
    .line 284
    const/4 v2, 0x0

    #@90
    .local v2, i:I
    :goto_90
    if-ge v2, v3, :cond_53

    #@92
    .line 285
    packed-switch v2, :pswitch_data_fe

    #@95
    .line 284
    :cond_95
    :goto_95
    add-int/lit8 v2, v2, 0x1

    #@97
    goto :goto_90

    #@98
    .line 287
    :pswitch_98
    aget-object v6, v0, v8

    #@9a
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9d
    move-result v6

    #@9e
    if-nez v6, :cond_95

    #@a0
    .line 288
    aget-object v6, v0, v8

    #@a2
    invoke-static {v6}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1500(Ljava/lang/String;)I

    #@a5
    move-result v6

    #@a6
    iput v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@a8
    .line 289
    aget-object v6, v0, v8

    #@aa
    iget v7, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@ac
    invoke-static {v6, v7, v9}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1600(Ljava/lang/String;IZ)Ljava/lang/String;

    #@af
    move-result-object v6

    #@b0
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number:Ljava/lang/String;

    #@b2
    goto :goto_95

    #@b3
    .line 294
    :pswitch_b3
    aget-object v6, v0, v9

    #@b5
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b8
    move-result v6

    #@b9
    if-nez v6, :cond_95

    #@bb
    .line 295
    aget-object v6, v0, v9

    #@bd
    invoke-static {v6}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1500(Ljava/lang/String;)I

    #@c0
    move-result v6

    #@c1
    iput v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@c3
    .line 296
    aget-object v6, v0, v9

    #@c5
    iget v7, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@c7
    invoke-static {v6, v7, v9}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1600(Ljava/lang/String;IZ)Ljava/lang/String;

    #@ca
    move-result-object v6

    #@cb
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_a:Ljava/lang/String;

    #@cd
    goto :goto_95

    #@ce
    .line 300
    :pswitch_ce
    aget-object v6, v0, v10

    #@d0
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d3
    move-result v6

    #@d4
    if-nez v6, :cond_95

    #@d6
    .line 301
    aget-object v6, v0, v10

    #@d8
    invoke-static {v6}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1500(Ljava/lang/String;)I

    #@db
    move-result v6

    #@dc
    iput v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@de
    .line 302
    aget-object v6, v0, v10

    #@e0
    iget v7, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@e2
    invoke-static {v6, v7, v9}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1600(Ljava/lang/String;IZ)Ljava/lang/String;

    #@e5
    move-result-object v6

    #@e6
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_b:Ljava/lang/String;

    #@e8
    goto :goto_95

    #@e9
    .line 320
    .end local v2           #i:I
    .end local v3           #length:I
    .restart local v1       #emails:[Ljava/lang/String;
    :cond_e9
    aget-object v6, v1, v8

    #@eb
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@ee
    move-result v6

    #@ef
    if-nez v6, :cond_f7

    #@f1
    .line 321
    aget-object v6, v1, v8

    #@f3
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->email_address:Ljava/lang/String;

    #@f5
    goto/16 :goto_6a

    #@f7
    .line 323
    :cond_f7
    const-string v6, ""

    #@f9
    iput-object v6, v5, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->email_address:Ljava/lang/String;

    #@fb
    goto/16 :goto_6a

    #@fd
    .line 285
    nop

    #@fe
    :pswitch_data_fe
    .packed-switch 0x0
        :pswitch_98
        :pswitch_b3
        :pswitch_ce
    .end packed-switch
.end method

.method private newQueryEntryCursor(Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)Landroid/database/Cursor;
    .registers 11
    .parameter "record"

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v5, 0x0

    #@5
    .line 504
    new-instance v1, Landroid/database/MatrixCursor;

    #@7
    sget-object v3, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@9
    invoke-direct {v1, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    #@c
    .line 505
    .local v1, cursor:Landroid/database/MatrixCursor;
    sget-object v3, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@e
    array-length v3, v3

    #@f
    new-array v2, v3, [Ljava/lang/Object;

    #@11
    .line 506
    .local v2, row:[Ljava/lang/Object;
    if-nez p1, :cond_34

    #@13
    .line 507
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v3

    #@17
    aput-object v3, v2, v5

    #@19
    .line 508
    const-string v3, ""

    #@1b
    aput-object v3, v2, v4

    #@1d
    .line 509
    const-string v3, ""

    #@1f
    aput-object v3, v2, v6

    #@21
    .line 510
    const-string v3, ""

    #@23
    aput-object v3, v2, v7

    #@25
    .line 511
    const-string v3, ""

    #@27
    aput-object v3, v2, v8

    #@29
    .line 512
    const/4 v3, 0x5

    #@2a
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d
    move-result-object v4

    #@2e
    aput-object v4, v2, v3

    #@30
    .line 534
    :goto_30
    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    #@33
    .line 535
    return-object v1

    #@34
    .line 514
    :cond_34
    iget v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@36
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v3

    #@3a
    aput-object v3, v2, v5

    #@3c
    .line 515
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@3e
    if-nez v3, :cond_b5

    #@40
    const-string v3, ""

    #@42
    :goto_42
    aput-object v3, v2, v4

    #@44
    .line 516
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->number:Ljava/lang/String;

    #@46
    iget v4, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->type:I

    #@48
    invoke-static {v3, v4, v5}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1600(Ljava/lang/String;IZ)Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    aput-object v3, v2, v6

    #@4e
    .line 517
    new-instance v0, Ljava/lang/StringBuilder;

    #@50
    const-string v3, ""

    #@52
    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@55
    .line 518
    .local v0, anrs:Ljava/lang/StringBuilder;
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number:Ljava/lang/String;

    #@57
    iget v4, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@59
    invoke-static {v3, v4, v5}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1600(Ljava/lang/String;IZ)Ljava/lang/String;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    .line 519
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_a:Ljava/lang/String;

    #@62
    if-eqz v3, :cond_7e

    #@64
    const-string v3, ""

    #@66
    iget-object v4, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_a:Ljava/lang/String;

    #@68
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v3

    #@6c
    if-nez v3, :cond_7e

    #@6e
    .line 520
    const-string v3, "~"

    #@70
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    .line 521
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_a:Ljava/lang/String;

    #@75
    iget v4, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@77
    invoke-static {v3, v4, v5}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1600(Ljava/lang/String;IZ)Ljava/lang/String;

    #@7a
    move-result-object v3

    #@7b
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    .line 524
    :cond_7e
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_b:Ljava/lang/String;

    #@80
    if-eqz v3, :cond_9c

    #@82
    const-string v3, ""

    #@84
    iget-object v4, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_b:Ljava/lang/String;

    #@86
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@89
    move-result v3

    #@8a
    if-nez v3, :cond_9c

    #@8c
    .line 525
    const-string v3, "~"

    #@8e
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    .line 526
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_b:Ljava/lang/String;

    #@93
    iget v4, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@95
    invoke-static {v3, v4, v5}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1600(Ljava/lang/String;IZ)Ljava/lang/String;

    #@98
    move-result-object v3

    #@99
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    .line 529
    :cond_9c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v3

    #@a0
    aput-object v3, v2, v7

    #@a2
    .line 531
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->email_address:Ljava/lang/String;

    #@a4
    if-nez v3, :cond_b8

    #@a6
    const-string v3, ""

    #@a8
    :goto_a8
    aput-object v3, v2, v8

    #@aa
    .line 532
    const/4 v3, 0x5

    #@ab
    iget v4, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->gas_id:I

    #@ad
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b0
    move-result-object v4

    #@b1
    aput-object v4, v2, v3

    #@b3
    goto/16 :goto_30

    #@b5
    .line 515
    .end local v0           #anrs:Ljava/lang/StringBuilder;
    :cond_b5
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@b7
    goto :goto_42

    #@b8
    .line 531
    .restart local v0       #anrs:Ljava/lang/StringBuilder;
    :cond_b8
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->email_address:Ljava/lang/String;

    #@ba
    goto :goto_a8
.end method

.method private newQueryGroupCursor(Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)Landroid/database/Cursor;
    .registers 8
    .parameter "record"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 541
    new-instance v0, Landroid/database/MatrixCursor;

    #@5
    sget-object v2, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->GROUP_PROJECTION:[Ljava/lang/String;

    #@7
    invoke-direct {v0, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    #@a
    .line 542
    .local v0, cursor:Landroid/database/MatrixCursor;
    if-nez p1, :cond_1c

    #@c
    .line 543
    new-array v1, v5, [Ljava/lang/Object;

    #@e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v2

    #@12
    aput-object v2, v1, v3

    #@14
    const-string v2, ""

    #@16
    aput-object v2, v1, v4

    #@18
    .line 546
    .local v1, row:[Ljava/lang/Object;
    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    #@1b
    .line 554
    :goto_1b
    return-object v0

    #@1c
    .line 548
    .end local v1           #row:[Ljava/lang/Object;
    :cond_1c
    new-array v1, v5, [Ljava/lang/Object;

    #@1e
    iget v2, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@20
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v2

    #@24
    aput-object v2, v1, v3

    #@26
    iget-object v2, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@28
    if-nez v2, :cond_32

    #@2a
    const-string v2, ""

    #@2c
    :goto_2c
    aput-object v2, v1, v4

    #@2e
    .line 551
    .restart local v1       #row:[Ljava/lang/Object;
    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    #@31
    goto :goto_1b

    #@32
    .line 548
    .end local v1           #row:[Ljava/lang/Object;
    :cond_32
    iget-object v2, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@34
    goto :goto_2c
.end method

.method private read(Landroid/content/Context;I)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    .registers 6
    .parameter "context"
    .parameter "simIndex"

    #@0
    .prologue
    .line 210
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@2
    if-nez v1, :cond_b

    #@4
    .line 211
    new-instance v1, Lcom/android/internal/telephony/uicc/UsimService;

    #@6
    invoke-direct {v1, p1}, Lcom/android/internal/telephony/uicc/UsimService;-><init>(Landroid/content/Context;)V

    #@9
    iput-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@b
    .line 214
    :cond_b
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@d
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/UsimService;->registerCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@10
    .line 216
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@12
    invoke-static {v1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1300(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)V

    #@15
    .line 217
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@17
    const/4 v2, 0x0

    #@18
    invoke-virtual {v1, v2, p2}, Lcom/android/internal/telephony/uicc/UsimService;->PBMReadRecord(II)V

    #@1b
    .line 218
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@1d
    invoke-static {v1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1400(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;

    #@23
    .line 219
    .local v0, result:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@25
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/UsimService;->unregisterCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@28
    .line 220
    if-eqz v0, :cond_30

    #@2a
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->isSuccess()Z

    #@2d
    move-result v1

    #@2e
    if-nez v1, :cond_32

    #@30
    .line 221
    :cond_30
    const/4 v1, 0x0

    #@31
    .line 224
    :goto_31
    return-object v1

    #@32
    :cond_32
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->getNativeRecord()Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@35
    move-result-object v1

    #@36
    goto :goto_31
.end method

.method private readGroup(Landroid/content/Context;I)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    .registers 6
    .parameter "context"
    .parameter "groupIndex"

    #@0
    .prologue
    .line 417
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@2
    if-nez v1, :cond_b

    #@4
    .line 418
    new-instance v1, Lcom/android/internal/telephony/uicc/UsimService;

    #@6
    invoke-direct {v1, p1}, Lcom/android/internal/telephony/uicc/UsimService;-><init>(Landroid/content/Context;)V

    #@9
    iput-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@b
    .line 420
    :cond_b
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@d
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/UsimService;->registerCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@10
    .line 422
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@12
    invoke-static {v1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1300(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)V

    #@15
    .line 423
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@17
    const/16 v2, 0x20

    #@19
    invoke-virtual {v1, v2, p2}, Lcom/android/internal/telephony/uicc/UsimService;->PBMReadRecord(II)V

    #@1c
    .line 424
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@1e
    invoke-static {v1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1400(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;

    #@24
    .line 425
    .local v0, result:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@26
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/UsimService;->unregisterCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@29
    .line 426
    if-eqz v0, :cond_31

    #@2b
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->isSuccess()Z

    #@2e
    move-result v1

    #@2f
    if-nez v1, :cond_33

    #@31
    .line 427
    :cond_31
    const/4 v1, 0x0

    #@32
    .line 429
    :goto_32
    return-object v1

    #@33
    :cond_33
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->getNativeRecord()Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@36
    move-result-object v1

    #@37
    goto :goto_32
.end method

.method private update(Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 8
    .parameter "context"
    .parameter "simIndex"
    .parameter "values"

    #@0
    .prologue
    .line 384
    if-gtz p2, :cond_1c

    #@2
    .line 385
    const-string v1, "IccPhonebookProvider"

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "invalid sim index="

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 386
    const/4 v1, 0x0

    #@1b
    .line 391
    :goto_1b
    return v1

    #@1c
    .line 388
    :cond_1c
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->newNativeRecordWithoutSimIndex(Landroid/content/ContentValues;)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@1f
    move-result-object v0

    #@20
    .line 389
    .local v0, records:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    iput p2, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@22
    .line 391
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->write(Landroid/content/Context;Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)I

    #@25
    move-result v1

    #@26
    goto :goto_1b
.end method

.method private updateGroup(Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 8
    .parameter "context"
    .parameter "groupIndex"
    .parameter "values"

    #@0
    .prologue
    .line 470
    if-gtz p2, :cond_1c

    #@2
    .line 471
    const-string v1, "IccPhonebookProvider"

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "invalid group index="

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 472
    const/4 v1, 0x0

    #@1b
    .line 477
    :goto_1b
    return v1

    #@1c
    .line 474
    :cond_1c
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->newNativeGroupRecordWithoutGroupIndex(Landroid/content/ContentValues;)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@1f
    move-result-object v0

    #@20
    .line 475
    .local v0, records:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    iput p2, v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@22
    .line 477
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->write(Landroid/content/Context;Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)I

    #@25
    move-result v1

    #@26
    goto :goto_1b
.end method

.method private write(Landroid/content/Context;Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)I
    .registers 7
    .parameter "context"
    .parameter "records"

    #@0
    .prologue
    .line 338
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@2
    if-nez v1, :cond_b

    #@4
    .line 339
    new-instance v1, Lcom/android/internal/telephony/uicc/UsimService;

    #@6
    invoke-direct {v1, p1}, Lcom/android/internal/telephony/uicc/UsimService;-><init>(Landroid/content/Context;)V

    #@9
    iput-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@b
    .line 341
    :cond_b
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@d
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/UsimService;->registerCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@10
    .line 343
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@12
    invoke-static {v1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1300(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)V

    #@15
    .line 344
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@17
    invoke-virtual {v1, p2}, Lcom/android/internal/telephony/uicc/UsimService;->PBMWriteRecord(Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)V

    #@1a
    .line 345
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@1c
    invoke-static {v1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1400(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;

    #@22
    .line 346
    .local v0, result:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;
    iget v1, v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->mReturnCode:I

    #@24
    if-eqz v1, :cond_54

    #@26
    .line 347
    const-string v1, "IccPhonebookProvider"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "write failed? error code="

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    iget v3, v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->mReturnCode:I

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    const-string v3, "(index="

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->getIndex()I

    #@42
    move-result v3

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    const-string v3, ")"

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 349
    :cond_54
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@56
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/UsimService;->unregisterCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@59
    .line 351
    if-eqz v0, :cond_66

    #@5b
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->isSuccess()Z

    #@5e
    move-result v1

    #@5f
    if-eqz v1, :cond_66

    #@61
    .line 352
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;->getIndex()I

    #@64
    move-result v1

    #@65
    .line 354
    :goto_65
    return v1

    #@66
    :cond_66
    const/4 v1, 0x0

    #@67
    goto :goto_65
.end method


# virtual methods
.method public onPBMDeleteCB(II)V
    .registers 5
    .parameter "retPbmResult"
    .parameter "deleteIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 563
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@2
    new-instance v1, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;

    #@4
    invoke-direct {v1, p0, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;-><init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;II)V

    #@7
    invoke-static {v0, v1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1700(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;Ljava/lang/Object;)V

    #@a
    .line 564
    return-void
.end method

.method public onPBMInfoCB(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V
    .registers 5
    .parameter "info"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 569
    const-string v0, "IccPhonebookProvider"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Unplaned get info operation [info="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "]"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 570
    return-void
.end method

.method public onPBMReadCB(Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)V
    .registers 4
    .parameter "records"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 566
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@2
    new-instance v1, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;

    #@4
    invoke-direct {v1, p0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;-><init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)V

    #@7
    invoke-static {v0, v1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1700(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;Ljava/lang/Object;)V

    #@a
    .line 567
    return-void
.end method

.method public onPBMWriteCB(II)V
    .registers 5
    .parameter "retPbmResult"
    .parameter "writtenIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 560
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->mAsyncCallHandler:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@2
    new-instance v1, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;

    #@4
    invoke-direct {v1, p0, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO$PBMResult;-><init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;II)V

    #@7
    invoke-static {v0, v1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1700(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;Ljava/lang/Object;)V

    #@a
    .line 561
    return-void
.end method
