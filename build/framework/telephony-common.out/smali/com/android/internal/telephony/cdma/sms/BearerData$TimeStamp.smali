.class public Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;
.super Landroid/text/format/Time;
.source "BearerData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cdma/sms/BearerData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimeStamp"
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 259
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-direct {p0, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@b
    .line 260
    return-void
.end method

.method public static encodefromByteArray([B)Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;
    .registers 12
    .parameter "data"

    #@0
    .prologue
    const/16 v10, 0x3b

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    .line 288
    new-instance v5, Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@6
    invoke-direct {v5}, Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;-><init>()V

    #@9
    .line 289
    .local v5, ts:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;
    const/4 v8, 0x0

    #@a
    aget-byte v8, p0, v8

    #@c
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@f
    move-result v6

    #@10
    .line 290
    .local v6, year:I
    const/16 v8, 0x63

    #@12
    if-gt v6, v8, :cond_16

    #@14
    if-gez v6, :cond_18

    #@16
    :cond_16
    move-object v5, v7

    #@17
    .line 309
    .end local v5           #ts:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;
    :goto_17
    return-object v5

    #@18
    .line 291
    .restart local v5       #ts:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;
    :cond_18
    iput v6, v5, Landroid/text/format/Time;->year:I

    #@1a
    .line 292
    aget-byte v8, p0, v9

    #@1c
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@1f
    move-result v3

    #@20
    .line 293
    .local v3, month:I
    if-lt v3, v9, :cond_26

    #@22
    const/16 v8, 0xc

    #@24
    if-le v3, v8, :cond_28

    #@26
    :cond_26
    move-object v5, v7

    #@27
    goto :goto_17

    #@28
    .line 294
    :cond_28
    iput v3, v5, Landroid/text/format/Time;->month:I

    #@2a
    .line 295
    const/4 v8, 0x2

    #@2b
    aget-byte v8, p0, v8

    #@2d
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@30
    move-result v0

    #@31
    .line 296
    .local v0, day:I
    if-lt v0, v9, :cond_37

    #@33
    const/16 v8, 0x1f

    #@35
    if-le v0, v8, :cond_39

    #@37
    :cond_37
    move-object v5, v7

    #@38
    goto :goto_17

    #@39
    .line 297
    :cond_39
    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    #@3b
    .line 298
    const/4 v8, 0x3

    #@3c
    aget-byte v8, p0, v8

    #@3e
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@41
    move-result v1

    #@42
    .line 299
    .local v1, hour:I
    if-ltz v1, :cond_48

    #@44
    const/16 v8, 0x17

    #@46
    if-le v1, v8, :cond_4a

    #@48
    :cond_48
    move-object v5, v7

    #@49
    goto :goto_17

    #@4a
    .line 300
    :cond_4a
    iput v1, v5, Landroid/text/format/Time;->hour:I

    #@4c
    .line 301
    const/4 v8, 0x4

    #@4d
    aget-byte v8, p0, v8

    #@4f
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@52
    move-result v2

    #@53
    .line 302
    .local v2, minute:I
    if-ltz v2, :cond_57

    #@55
    if-le v2, v10, :cond_59

    #@57
    :cond_57
    move-object v5, v7

    #@58
    goto :goto_17

    #@59
    .line 303
    :cond_59
    iput v2, v5, Landroid/text/format/Time;->minute:I

    #@5b
    .line 304
    const/4 v8, 0x5

    #@5c
    aget-byte v8, p0, v8

    #@5e
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@61
    move-result v4

    #@62
    .line 305
    .local v4, second:I
    if-ltz v4, :cond_66

    #@64
    if-le v4, v10, :cond_68

    #@66
    :cond_66
    move-object v5, v7

    #@67
    goto :goto_17

    #@68
    .line 306
    :cond_68
    iput v4, v5, Landroid/text/format/Time;->second:I

    #@6a
    .line 307
    const-string v7, "SMS"

    #@6c
    new-instance v8, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v9, "(encode)TimeStamp ---> "

    #@73
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v8

    #@77
    iget v9, v5, Landroid/text/format/Time;->year:I

    #@79
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    const-string v9, "//"

    #@7f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v8

    #@83
    iget v9, v5, Landroid/text/format/Time;->month:I

    #@85
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@88
    move-result-object v8

    #@89
    const-string v9, "//"

    #@8b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v8

    #@8f
    iget v9, v5, Landroid/text/format/Time;->monthDay:I

    #@91
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v8

    #@95
    const-string v9, "//"

    #@97
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v8

    #@9b
    iget v9, v5, Landroid/text/format/Time;->hour:I

    #@9d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v8

    #@a1
    const-string v9, "//"

    #@a3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v8

    #@a7
    iget v9, v5, Landroid/text/format/Time;->minute:I

    #@a9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v8

    #@ad
    const-string v9, "//"

    #@af
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v8

    #@b3
    iget v9, v5, Landroid/text/format/Time;->second:I

    #@b5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v8

    #@b9
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v8

    #@bd
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c0
    goto/16 :goto_17
.end method

.method public static fromByteArray([B)Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;
    .registers 12
    .parameter "data"

    #@0
    .prologue
    const/16 v10, 0x3b

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v8, 0x0

    #@4
    .line 263
    new-instance v5, Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@6
    invoke-direct {v5}, Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;-><init>()V

    #@9
    .line 265
    .local v5, ts:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;
    const/4 v7, 0x0

    #@a
    aget-byte v7, p0, v7

    #@c
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@f
    move-result v6

    #@10
    .line 266
    .local v6, year:I
    const/16 v7, 0x63

    #@12
    if-gt v6, v7, :cond_16

    #@14
    if-gez v6, :cond_18

    #@16
    :cond_16
    move-object v5, v8

    #@17
    .line 283
    .end local v5           #ts:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;
    :goto_17
    return-object v5

    #@18
    .line 267
    .restart local v5       #ts:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;
    :cond_18
    const/16 v7, 0x60

    #@1a
    if-lt v6, v7, :cond_2e

    #@1c
    add-int/lit16 v7, v6, 0x76c

    #@1e
    :goto_1e
    iput v7, v5, Landroid/text/format/Time;->year:I

    #@20
    .line 268
    aget-byte v7, p0, v9

    #@22
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@25
    move-result v3

    #@26
    .line 269
    .local v3, month:I
    if-lt v3, v9, :cond_2c

    #@28
    const/16 v7, 0xc

    #@2a
    if-le v3, v7, :cond_31

    #@2c
    :cond_2c
    move-object v5, v8

    #@2d
    goto :goto_17

    #@2e
    .line 267
    .end local v3           #month:I
    :cond_2e
    add-int/lit16 v7, v6, 0x7d0

    #@30
    goto :goto_1e

    #@31
    .line 270
    .restart local v3       #month:I
    :cond_31
    add-int/lit8 v7, v3, -0x1

    #@33
    iput v7, v5, Landroid/text/format/Time;->month:I

    #@35
    .line 271
    const/4 v7, 0x2

    #@36
    aget-byte v7, p0, v7

    #@38
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@3b
    move-result v0

    #@3c
    .line 272
    .local v0, day:I
    if-lt v0, v9, :cond_42

    #@3e
    const/16 v7, 0x1f

    #@40
    if-le v0, v7, :cond_44

    #@42
    :cond_42
    move-object v5, v8

    #@43
    goto :goto_17

    #@44
    .line 273
    :cond_44
    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    #@46
    .line 274
    const/4 v7, 0x3

    #@47
    aget-byte v7, p0, v7

    #@49
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@4c
    move-result v1

    #@4d
    .line 275
    .local v1, hour:I
    if-ltz v1, :cond_53

    #@4f
    const/16 v7, 0x17

    #@51
    if-le v1, v7, :cond_55

    #@53
    :cond_53
    move-object v5, v8

    #@54
    goto :goto_17

    #@55
    .line 276
    :cond_55
    iput v1, v5, Landroid/text/format/Time;->hour:I

    #@57
    .line 277
    const/4 v7, 0x4

    #@58
    aget-byte v7, p0, v7

    #@5a
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@5d
    move-result v2

    #@5e
    .line 278
    .local v2, minute:I
    if-ltz v2, :cond_62

    #@60
    if-le v2, v10, :cond_64

    #@62
    :cond_62
    move-object v5, v8

    #@63
    goto :goto_17

    #@64
    .line 279
    :cond_64
    iput v2, v5, Landroid/text/format/Time;->minute:I

    #@66
    .line 280
    const/4 v7, 0x5

    #@67
    aget-byte v7, p0, v7

    #@69
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaBcdByteToInt(B)I

    #@6c
    move-result v4

    #@6d
    .line 281
    .local v4, second:I
    if-ltz v4, :cond_71

    #@6f
    if-le v4, v10, :cond_73

    #@71
    :cond_71
    move-object v5, v8

    #@72
    goto :goto_17

    #@73
    .line 282
    :cond_73
    iput v4, v5, Landroid/text/format/Time;->second:I

    #@75
    goto :goto_17
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 316
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "TimeStamp "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 317
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "{ year="

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget v2, p0, Landroid/text/format/Time;->year:I

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 318
    new-instance v1, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v2, ", month="

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    iget v2, p0, Landroid/text/format/Time;->month:I

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    .line 319
    new-instance v1, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v2, ", day="

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    iget v2, p0, Landroid/text/format/Time;->monthDay:I

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    .line 320
    new-instance v1, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v2, ", hour="

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    iget v2, p0, Landroid/text/format/Time;->hour:I

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    .line 321
    new-instance v1, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v2, ", minute="

    #@71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    iget v2, p0, Landroid/text/format/Time;->minute:I

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v1

    #@7f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    .line 322
    new-instance v1, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v2, ", second="

    #@89
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v1

    #@8d
    iget v2, p0, Landroid/text/format/Time;->second:I

    #@8f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@92
    move-result-object v1

    #@93
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v1

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    .line 323
    const-string v1, " }"

    #@9c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    .line 324
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v1

    #@a3
    return-object v1
.end method
