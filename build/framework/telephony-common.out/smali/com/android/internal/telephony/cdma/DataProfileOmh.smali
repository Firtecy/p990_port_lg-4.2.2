.class public Lcom/android/internal/telephony/cdma/DataProfileOmh;
.super Lcom/android/internal/telephony/DataProfile;
.source "DataProfileOmh.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    }
.end annotation


# static fields
.field private static PROFILE_TYPE:Ljava/lang/String;


# instance fields
.field private DATA_PROFILE_OMH_PRIORITY_HIGHEST:I

.field private DATA_PROFILE_OMH_PRIORITY_LOWEST:I

.field private mDataProfileModem:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

.field private mPriority:I

.field private mProfileId:I

.field private serviceTypeMasks:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 114
    const-string v0, "DataProfileOmh"

    #@2
    sput-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->PROFILE_TYPE:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>()V
    .registers 12

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 128
    sget-object v3, Lcom/android/internal/telephony/cdma/DataProfileOmh;->PROFILE_TYPE:Ljava/lang/String;

    #@4
    const/4 v6, 0x3

    #@5
    const-string v0, "persist.telephony.cdma.protocol"

    #@7
    const-string v4, "IP"

    #@9
    invoke-static {v0, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v8

    #@d
    const-string v0, "persist.telephony.cdma.rproto"

    #@f
    const-string v4, "IP"

    #@11
    invoke-static {v0, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v9

    #@15
    move-object v0, p0

    #@16
    move-object v4, v2

    #@17
    move-object v5, v2

    #@18
    move-object v7, v2

    #@19
    move v10, v1

    #@1a
    invoke-direct/range {v0 .. v10}, Lcom/android/internal/telephony/cdma/DataProfileOmh;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    #@1d
    .line 134
    iput v1, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mProfileId:I

    #@1f
    .line 135
    iput v1, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mPriority:I

    #@21
    .line 136
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4
    .parameter "profileId"
    .parameter "priority"

    #@0
    .prologue
    .line 139
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/DataProfileOmh;-><init>()V

    #@3
    .line 140
    iput p1, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mProfileId:I

    #@5
    .line 141
    iput p2, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mPriority:I

    #@7
    .line 142
    const/4 v0, 0x0

    #@8
    new-array v0, v0, [Ljava/lang/String;

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@c
    .line 143
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 22
    .parameter "id"
    .parameter "numeric"
    .parameter "name"
    .parameter "user"
    .parameter "password"
    .parameter "authType"
    .parameter "types"
    .parameter "protocol"
    .parameter "roamingProtocol"
    .parameter "bearer"

    #@0
    .prologue
    .line 118
    const-string v3, ""

    #@2
    move-object v0, p0

    #@3
    move v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v4, p4

    #@6
    move-object/from16 v5, p5

    #@8
    move/from16 v6, p6

    #@a
    move-object/from16 v7, p7

    #@c
    move-object/from16 v8, p8

    #@e
    move-object/from16 v9, p9

    #@10
    move/from16 v10, p10

    #@12
    invoke-direct/range {v0 .. v10}, Lcom/android/internal/telephony/DataProfile;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    #@15
    .line 100
    const/16 v0, 0xff

    #@17
    iput v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->DATA_PROFILE_OMH_PRIORITY_LOWEST:I

    #@19
    .line 102
    const/4 v0, 0x0

    #@1a
    iput v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->DATA_PROFILE_OMH_PRIORITY_HIGHEST:I

    #@1c
    .line 106
    const/4 v0, 0x0

    #@1d
    iput v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->serviceTypeMasks:I

    #@1f
    .line 109
    const/4 v0, 0x0

    #@20
    iput v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mProfileId:I

    #@22
    .line 112
    const/4 v0, 0x0

    #@23
    iput v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mPriority:I

    #@25
    .line 120
    return-void
.end method

.method private isValidPriority(I)Z
    .registers 3
    .parameter "priority"

    #@0
    .prologue
    .line 209
    iget v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->DATA_PROFILE_OMH_PRIORITY_HIGHEST:I

    #@2
    if-lt p1, v0, :cond_a

    #@4
    iget v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->DATA_PROFILE_OMH_PRIORITY_LOWEST:I

    #@6
    if-gt p1, v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method


# virtual methods
.method public addServiceType(Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;)V
    .registers 9
    .parameter "modemProfile"

    #@0
    .prologue
    .line 226
    iget v5, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->serviceTypeMasks:I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getid()I

    #@5
    move-result v6

    #@6
    or-int/2addr v5, v6

    #@7
    iput v5, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->serviceTypeMasks:I

    #@9
    .line 229
    new-instance v4, Ljava/util/ArrayList;

    #@b
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@e
    .line 230
    .local v4, serviceTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->values()[Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@11
    move-result-object v0

    #@12
    .local v0, arr$:[Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    array-length v3, v0

    #@13
    .local v3, len$:I
    const/4 v2, 0x0

    #@14
    .local v2, i$:I
    :goto_14
    if-ge v2, v3, :cond_2b

    #@16
    aget-object v1, v0, v2

    #@18
    .line 231
    .local v1, dpt:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    iget v5, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->serviceTypeMasks:I

    #@1a
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getid()I

    #@1d
    move-result v6

    #@1e
    and-int/2addr v5, v6

    #@1f
    if-eqz v5, :cond_28

    #@21
    .line 232
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getDataServiceType()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28
    .line 230
    :cond_28
    add-int/lit8 v2, v2, 0x1

    #@2a
    goto :goto_14

    #@2b
    .line 235
    .end local v1           #dpt:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    :cond_2b
    const/4 v5, 0x0

    #@2c
    new-array v5, v5, [Ljava/lang/String;

    #@2e
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@31
    move-result-object v5

    #@32
    check-cast v5, [Ljava/lang/String;

    #@34
    iput-object v5, p0, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@36
    .line 236
    return-void
.end method

.method public canHandleType(Ljava/lang/String;)Z
    .registers 4
    .parameter "serviceType"

    #@0
    .prologue
    .line 147
    iget v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->serviceTypeMasks:I

    #@2
    invoke-static {p1}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getDataProfileTypeModem(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getid()I

    #@9
    move-result v1

    #@a
    and-int/2addr v0, v1

    #@b
    if-eqz v0, :cond_f

    #@d
    const/4 v0, 0x1

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public getDataProfileType()Lcom/android/internal/telephony/DataProfile$DataProfileType;
    .registers 2

    #@0
    .prologue
    .line 153
    sget-object v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_OMH:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@2
    return-object v0
.end method

.method public getDataProfileTypeModem()Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    .registers 2

    #@0
    .prologue
    .line 182
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mDataProfileModem:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@2
    return-object v0
.end method

.method public getPriority()I
    .registers 2

    #@0
    .prologue
    .line 217
    iget v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mPriority:I

    #@2
    return v0
.end method

.method public getProfileId()I
    .registers 2

    #@0
    .prologue
    .line 213
    iget v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mProfileId:I

    #@2
    return v0
.end method

.method public getServiceTypes()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 221
    const/4 v0, 0x0

    #@1
    .line 222
    .local v0, dummy:[Ljava/lang/String;
    return-object v0
.end method

.method public isPriorityHigher(I)Z
    .registers 3
    .parameter "priority"

    #@0
    .prologue
    .line 195
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->isValidPriority(I)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    iget v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mPriority:I

    #@8
    if-ge v0, p1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isPriorityLower(I)Z
    .registers 3
    .parameter "priority"

    #@0
    .prologue
    .line 200
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->isValidPriority(I)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    iget v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mPriority:I

    #@8
    if-le v0, p1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isValidPriority()Z
    .registers 2

    #@0
    .prologue
    .line 204
    iget v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mPriority:I

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->isValidPriority(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public setDataProfileTypeModem(Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;)V
    .registers 2
    .parameter "modemProfile"

    #@0
    .prologue
    .line 178
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mDataProfileModem:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@2
    .line 179
    return-void
.end method

.method public setPriority(I)V
    .registers 2
    .parameter "priority"

    #@0
    .prologue
    .line 190
    iput p1, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mPriority:I

    #@2
    .line 191
    return-void
.end method

.method public setProfileId(I)V
    .registers 2
    .parameter "profileId"

    #@0
    .prologue
    .line 186
    iput p1, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mProfileId:I

    #@2
    .line 187
    return-void
.end method

.method public toHash()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->toString()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    iget v1, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mProfileId:I

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget v1, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mPriority:I

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method

.method public toShortString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 158
    const-string v0, "DataProfile OMH"

    #@2
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 170
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-super {p0}, Lcom/android/internal/telephony/DataProfile;->toString()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mProfileId:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, ", "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget v2, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh;->mPriority:I

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    .line 173
    const-string v1, "]"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 174
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    return-object v1
.end method
