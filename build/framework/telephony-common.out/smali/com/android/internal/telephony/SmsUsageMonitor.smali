.class public Lcom/android/internal/telephony/SmsUsageMonitor;
.super Ljava/lang/Object;
.source "SmsUsageMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/SmsUsageMonitor$SettingsObserverHandler;,
        Lcom/android/internal/telephony/SmsUsageMonitor$SettingsObserver;,
        Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;
    }
.end annotation


# static fields
.field private static final ATTR_COUNTRY:Ljava/lang/String; = "country"

.field private static final ATTR_FREE:Ljava/lang/String; = "free"

.field private static final ATTR_PACKAGE_NAME:Ljava/lang/String; = "name"

.field private static final ATTR_PACKAGE_SMS_POLICY:Ljava/lang/String; = "sms-policy"

.field private static final ATTR_PATTERN:Ljava/lang/String; = "pattern"

.field private static final ATTR_PREMIUM:Ljava/lang/String; = "premium"

.field private static final ATTR_STANDARD:Ljava/lang/String; = "standard"

.field static final CATEGORY_FREE_SHORT_CODE:I = 0x1

.field static final CATEGORY_NOT_SHORT_CODE:I = 0x0

.field static final CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE:I = 0x3

.field static final CATEGORY_PREMIUM_SHORT_CODE:I = 0x4

.field static final CATEGORY_STANDARD_SHORT_CODE:I = 0x2

.field private static final DBG:Z = false

#the value of this static final field might be set in the static constructor
.field private static final DEFAULT_SMS_CHECK_PERIOD:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final DEFAULT_SMS_MAX_COUNT:I = 0x0

.field public static final PREMIUM_SMS_PERMISSION_ALWAYS_ALLOW:I = 0x3

.field public static final PREMIUM_SMS_PERMISSION_ASK_USER:I = 0x1

.field public static final PREMIUM_SMS_PERMISSION_NEVER_ALLOW:I = 0x2

.field public static final PREMIUM_SMS_PERMISSION_UNKNOWN:I = 0x0

.field private static final SHORT_CODE_PATH:Ljava/lang/String; = "/data/misc/sms/codes"

.field private static final SMS_POLICY_FILE_DIRECTORY:Ljava/lang/String; = "/data/misc/sms"

.field private static final SMS_POLICY_FILE_NAME:Ljava/lang/String; = "premium_sms_policy.xml"

.field private static final TAG:Ljava/lang/String; = "SmsUsageMonitor"

.field private static final TAG_PACKAGE:Ljava/lang/String; = "package"

.field private static final TAG_SHORTCODE:Ljava/lang/String; = "shortcode"

.field private static final TAG_SHORTCODES:Ljava/lang/String; = "shortcodes"

.field private static final TAG_SMS_POLICY_BODY:Ljava/lang/String; = "premium-sms-policy"

.field private static final VDBG:Z


# instance fields
.field private final mCheckEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mCheckPeriod:I

.field private final mContext:Landroid/content/Context;

.field private mCurrentCountry:Ljava/lang/String;

.field private mCurrentPatternMatcher:Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;

.field private final mMaxAllowed:I

.field private final mPatternFile:Ljava/io/File;

.field private mPatternFileLastModified:J

.field private mPolicyFile:Landroid/util/AtomicFile;

.field private final mPremiumSmsPolicy:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettingsObserverHandler:Lcom/android/internal/telephony/SmsUsageMonitor$SettingsObserverHandler;

.field private mSettingsShortCodePatterns:Ljava/lang/String;

.field private final mSmsStamp:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 86
    const-string v0, "limitless_unauthorized_SMS_usage"

    #@4
    invoke-static {v1, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-ne v0, v2, :cond_26

    #@a
    .line 87
    const/4 v0, 0x0

    #@b
    sput v0, Lcom/android/internal/telephony/SmsUsageMonitor;->DEFAULT_SMS_CHECK_PERIOD:I

    #@d
    .line 99
    :goto_d
    const-string v0, "change_limit_number_unauthorized_SMS_usage"

    #@f
    invoke-static {v1, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@12
    move-result v0

    #@13
    if-ne v0, v2, :cond_2c

    #@15
    .line 100
    const-string v0, "limit_number_unauthorized_SMS_usage"

    #@17
    invoke-static {v1, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@22
    move-result v0

    #@23
    sput v0, Lcom/android/internal/telephony/SmsUsageMonitor;->DEFAULT_SMS_MAX_COUNT:I

    #@25
    .line 106
    :goto_25
    return-void

    #@26
    .line 89
    :cond_26
    const v0, 0x1b7740

    #@29
    sput v0, Lcom/android/internal/telephony/SmsUsageMonitor;->DEFAULT_SMS_CHECK_PERIOD:I

    #@2b
    goto :goto_d

    #@2c
    .line 101
    :cond_2c
    const-string v0, "increase_user_permit_sent_sms_max_count"

    #@2e
    invoke-static {v1, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@31
    move-result v0

    #@32
    if-ne v0, v2, :cond_39

    #@34
    .line 102
    const/16 v0, 0x3e8

    #@36
    sput v0, Lcom/android/internal/telephony/SmsUsageMonitor;->DEFAULT_SMS_MAX_COUNT:I

    #@38
    goto :goto_25

    #@39
    .line 104
    :cond_39
    const/16 v0, 0x1e

    #@3b
    sput v0, Lcom/android/internal/telephony/SmsUsageMonitor;->DEFAULT_SMS_MAX_COUNT:I

    #@3d
    goto :goto_25
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 291
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 145
    new-instance v1, Ljava/util/HashMap;

    #@5
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mSmsStamp:Ljava/util/HashMap;

    #@a
    .line 158
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@c
    const/4 v2, 0x1

    #@d
    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@10
    iput-object v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCheckEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@12
    .line 167
    new-instance v1, Ljava/io/File;

    #@14
    const-string v2, "/data/misc/sms/codes"

    #@16
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@19
    iput-object v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFile:Ljava/io/File;

    #@1b
    .line 170
    const-wide/16 v1, 0x0

    #@1d
    iput-wide v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFileLastModified:J

    #@1f
    .line 203
    new-instance v1, Ljava/util/HashMap;

    #@21
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@24
    iput-object v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPremiumSmsPolicy:Ljava/util/HashMap;

    #@26
    .line 292
    iput-object p1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mContext:Landroid/content/Context;

    #@28
    .line 293
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2b
    move-result-object v0

    #@2c
    .line 295
    .local v0, resolver:Landroid/content/ContentResolver;
    const-string v1, "sms_outgoing_check_max_count"

    #@2e
    sget v2, Lcom/android/internal/telephony/SmsUsageMonitor;->DEFAULT_SMS_MAX_COUNT:I

    #@30
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@33
    move-result v1

    #@34
    iput v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mMaxAllowed:I

    #@36
    .line 299
    const-string v1, "sms_outgoing_check_interval_ms"

    #@38
    sget v2, Lcom/android/internal/telephony/SmsUsageMonitor;->DEFAULT_SMS_CHECK_PERIOD:I

    #@3a
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3d
    move-result v1

    #@3e
    iput v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCheckPeriod:I

    #@40
    .line 303
    new-instance v1, Lcom/android/internal/telephony/SmsUsageMonitor$SettingsObserverHandler;

    #@42
    iget-object v2, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mContext:Landroid/content/Context;

    #@44
    iget-object v3, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCheckEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@46
    invoke-direct {v1, v2, v3}, Lcom/android/internal/telephony/SmsUsageMonitor$SettingsObserverHandler;-><init>(Landroid/content/Context;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    #@49
    iput-object v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mSettingsObserverHandler:Lcom/android/internal/telephony/SmsUsageMonitor$SettingsObserverHandler;

    #@4b
    .line 305
    invoke-direct {p0}, Lcom/android/internal/telephony/SmsUsageMonitor;->loadPremiumSmsPolicyDb()V

    #@4e
    .line 306
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/SmsUsageMonitor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Lcom/android/internal/telephony/SmsUsageMonitor;->writePremiumSmsPolicyDb()V

    #@3
    return-void
.end method

.method private static checkCallerIsSystemOrPhoneApp()V
    .registers 5

    #@0
    .prologue
    .line 650
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v1

    #@4
    .line 651
    .local v1, uid:I
    invoke-static {v1}, Landroid/os/UserHandle;->getAppId(I)I

    #@7
    move-result v0

    #@8
    .line 652
    .local v0, appId:I
    const/16 v2, 0x3e8

    #@a
    if-eq v0, v2, :cond_12

    #@c
    const/16 v2, 0x3e9

    #@e
    if-eq v0, v2, :cond_12

    #@10
    if-nez v1, :cond_13

    #@12
    .line 653
    :cond_12
    return-void

    #@13
    .line 655
    :cond_13
    new-instance v2, Ljava/lang/SecurityException;

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v4, "Disallowed call for uid "

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v2
.end method

.method private static checkCallerIsSystemOrSameApp(Ljava/lang/String;)V
    .registers 7
    .parameter "pkg"

    #@0
    .prologue
    .line 633
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v2

    #@4
    .line 634
    .local v2, uid:I
    invoke-static {v2}, Landroid/os/UserHandle;->getAppId(I)I

    #@7
    move-result v3

    #@8
    const/16 v4, 0x3e8

    #@a
    if-eq v3, v4, :cond_e

    #@c
    if-nez v2, :cond_f

    #@e
    .line 647
    :cond_e
    return-void

    #@f
    .line 638
    :cond_f
    :try_start_f
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@12
    move-result-object v3

    #@13
    const/4 v4, 0x0

    #@14
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@17
    move-result v5

    #@18
    invoke-interface {v3, p0, v4, v5}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    #@1b
    move-result-object v0

    #@1c
    .line 640
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@1e
    invoke-static {v3, v2}, Landroid/os/UserHandle;->isSameApp(II)Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_e

    #@24
    .line 641
    new-instance v3, Ljava/lang/SecurityException;

    #@26
    new-instance v4, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v5, "Calling uid "

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string v5, " gave package"

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    const-string v5, " which is owned by uid "

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    iget v5, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@52
    throw v3
    :try_end_53
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_53} :catch_53

    #@53
    .line 644
    .end local v0           #ai:Landroid/content/pm/ApplicationInfo;
    :catch_53
    move-exception v1

    #@54
    .line 645
    .local v1, re:Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/SecurityException;

    #@56
    new-instance v4, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v5, "Unknown package "

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    const-string v5, "\n"

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v4

    #@6f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v4

    #@73
    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@76
    throw v3
.end method

.method private getPatternMatcherFromFile(Ljava/lang/String;)Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;
    .registers 9
    .parameter "country"

    #@0
    .prologue
    .line 314
    const/4 v2, 0x0

    #@1
    .line 315
    .local v2, patternReader:Ljava/io/FileReader;
    const/4 v1, 0x0

    #@2
    .line 317
    .local v1, parser:Lorg/xmlpull/v1/XmlPullParser;
    :try_start_2
    new-instance v3, Ljava/io/FileReader;

    #@4
    iget-object v4, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFile:Ljava/io/File;

    #@6
    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_9
    .catchall {:try_start_2 .. :try_end_9} :catchall_52
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_9} :catch_23
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_9} :catch_3a

    #@9
    .line 318
    .end local v2           #patternReader:Ljava/io/FileReader;
    .local v3, patternReader:Ljava/io/FileReader;
    :try_start_9
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@c
    move-result-object v1

    #@d
    .line 319
    invoke-interface {v1, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    #@10
    .line 320
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/SmsUsageMonitor;->getPatternMatcherFromXmlParser(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;
    :try_end_13
    .catchall {:try_start_9 .. :try_end_13} :catchall_67
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_13} :catch_6d
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_13} :catch_6a

    #@13
    move-result-object v4

    #@14
    .line 326
    iget-object v5, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFile:Ljava/io/File;

    #@16
    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    #@19
    move-result-wide v5

    #@1a
    iput-wide v5, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFileLastModified:J

    #@1c
    .line 327
    if-eqz v3, :cond_21

    #@1e
    .line 329
    :try_start_1e
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_21} :catch_61

    #@21
    :cond_21
    :goto_21
    move-object v2, v3

    #@22
    .line 333
    .end local v3           #patternReader:Ljava/io/FileReader;
    .restart local v2       #patternReader:Ljava/io/FileReader;
    :goto_22
    return-object v4

    #@23
    .line 321
    :catch_23
    move-exception v0

    #@24
    .line 322
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_24
    :try_start_24
    const-string v4, "SmsUsageMonitor"

    #@26
    const-string v5, "Short Code Pattern File not found"

    #@28
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2b
    .catchall {:try_start_24 .. :try_end_2b} :catchall_52

    #@2b
    .line 326
    iget-object v4, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFile:Ljava/io/File;

    #@2d
    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    #@30
    move-result-wide v4

    #@31
    iput-wide v4, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFileLastModified:J

    #@33
    .line 327
    if-eqz v2, :cond_38

    #@35
    .line 329
    :try_start_35
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_38} :catch_63

    #@38
    .line 333
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :cond_38
    :goto_38
    const/4 v4, 0x0

    #@39
    goto :goto_22

    #@3a
    .line 323
    :catch_3a
    move-exception v0

    #@3b
    .line 324
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_3b
    :try_start_3b
    const-string v4, "SmsUsageMonitor"

    #@3d
    const-string v5, "XML parser exception reading short code pattern file"

    #@3f
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_42
    .catchall {:try_start_3b .. :try_end_42} :catchall_52

    #@42
    .line 326
    iget-object v4, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFile:Ljava/io/File;

    #@44
    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    #@47
    move-result-wide v4

    #@48
    iput-wide v4, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFileLastModified:J

    #@4a
    .line 327
    if-eqz v2, :cond_38

    #@4c
    .line 329
    :try_start_4c
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_4f
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_4f} :catch_50

    #@4f
    goto :goto_38

    #@50
    .line 330
    :catch_50
    move-exception v4

    #@51
    goto :goto_38

    #@52
    .line 326
    .end local v0           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_52
    move-exception v4

    #@53
    :goto_53
    iget-object v5, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFile:Ljava/io/File;

    #@55
    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    #@58
    move-result-wide v5

    #@59
    iput-wide v5, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFileLastModified:J

    #@5b
    .line 327
    if-eqz v2, :cond_60

    #@5d
    .line 329
    :try_start_5d
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_60
    .catch Ljava/io/IOException; {:try_start_5d .. :try_end_60} :catch_65

    #@60
    .line 330
    :cond_60
    :goto_60
    throw v4

    #@61
    .end local v2           #patternReader:Ljava/io/FileReader;
    .restart local v3       #patternReader:Ljava/io/FileReader;
    :catch_61
    move-exception v5

    #@62
    goto :goto_21

    #@63
    .end local v3           #patternReader:Ljava/io/FileReader;
    .local v0, e:Ljava/io/FileNotFoundException;
    .restart local v2       #patternReader:Ljava/io/FileReader;
    :catch_63
    move-exception v4

    #@64
    goto :goto_38

    #@65
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_65
    move-exception v5

    #@66
    goto :goto_60

    #@67
    .line 326
    .end local v2           #patternReader:Ljava/io/FileReader;
    .restart local v3       #patternReader:Ljava/io/FileReader;
    :catchall_67
    move-exception v4

    #@68
    move-object v2, v3

    #@69
    .end local v3           #patternReader:Ljava/io/FileReader;
    .restart local v2       #patternReader:Ljava/io/FileReader;
    goto :goto_53

    #@6a
    .line 323
    .end local v2           #patternReader:Ljava/io/FileReader;
    .restart local v3       #patternReader:Ljava/io/FileReader;
    :catch_6a
    move-exception v0

    #@6b
    move-object v2, v3

    #@6c
    .end local v3           #patternReader:Ljava/io/FileReader;
    .restart local v2       #patternReader:Ljava/io/FileReader;
    goto :goto_3b

    #@6d
    .line 321
    .end local v2           #patternReader:Ljava/io/FileReader;
    .restart local v3       #patternReader:Ljava/io/FileReader;
    :catch_6d
    move-exception v0

    #@6e
    move-object v2, v3

    #@6f
    .end local v3           #patternReader:Ljava/io/FileReader;
    .restart local v2       #patternReader:Ljava/io/FileReader;
    goto :goto_24
.end method

.method private getPatternMatcherFromResource(Ljava/lang/String;)Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;
    .registers 5
    .parameter "country"

    #@0
    .prologue
    .line 337
    const v0, 0x10f000e

    #@3
    .line 338
    .local v0, id:I
    const/4 v1, 0x0

    #@4
    .line 340
    .local v1, parser:Landroid/content/res/XmlResourceParser;
    :try_start_4
    iget-object v2, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@d
    move-result-object v1

    #@e
    .line 341
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/SmsUsageMonitor;->getPatternMatcherFromXmlParser(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_18

    #@11
    move-result-object v2

    #@12
    .line 343
    if-eqz v1, :cond_17

    #@14
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    #@17
    :cond_17
    return-object v2

    #@18
    :catchall_18
    move-exception v2

    #@19
    if-eqz v1, :cond_1e

    #@1b
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    #@1e
    :cond_1e
    throw v2
.end method

.method private getPatternMatcherFromXmlParser(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;
    .registers 14
    .parameter "parser"
    .parameter "country"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 350
    :try_start_1
    const-string v7, "shortcodes"

    #@3
    invoke-static {p1, v7}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@6
    .line 353
    :cond_6
    :goto_6
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@9
    .line 354
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    .line 355
    .local v2, element:Ljava/lang/String;
    if-nez v2, :cond_18

    #@f
    .line 356
    const-string v7, "SmsUsageMonitor"

    #@11
    const-string v9, "Parsing pattern data found null"

    #@13
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .end local v2           #element:Ljava/lang/String;
    :goto_16
    move-object v7, v8

    #@17
    .line 380
    :goto_17
    return-object v7

    #@18
    .line 360
    .restart local v2       #element:Ljava/lang/String;
    :cond_18
    const-string v7, "shortcode"

    #@1a
    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v7

    #@1e
    if-eqz v7, :cond_58

    #@20
    .line 361
    const/4 v7, 0x0

    #@21
    const-string v9, "country"

    #@23
    invoke-interface {p1, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 363
    .local v0, currentCountry:Ljava/lang/String;
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v7

    #@2b
    if-eqz v7, :cond_6

    #@2d
    .line 364
    const/4 v7, 0x0

    #@2e
    const-string v9, "pattern"

    #@30
    invoke-interface {p1, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    .line 365
    .local v4, pattern:Ljava/lang/String;
    const/4 v7, 0x0

    #@35
    const-string v9, "premium"

    #@37
    invoke-interface {p1, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    .line 366
    .local v5, premium:Ljava/lang/String;
    const/4 v7, 0x0

    #@3c
    const-string v9, "free"

    #@3e
    invoke-interface {p1, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    .line 367
    .local v3, free:Ljava/lang/String;
    const/4 v7, 0x0

    #@43
    const-string v9, "standard"

    #@45
    invoke-interface {p1, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    .line 368
    .local v6, standard:Ljava/lang/String;
    new-instance v7, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;

    #@4b
    invoke-direct {v7, v4, v5, v3, v6}, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_4e} :catch_4f
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_4e} :catch_71

    #@4e
    goto :goto_17

    #@4f
    .line 374
    .end local v0           #currentCountry:Ljava/lang/String;
    .end local v2           #element:Ljava/lang/String;
    .end local v3           #free:Ljava/lang/String;
    .end local v4           #pattern:Ljava/lang/String;
    .end local v5           #premium:Ljava/lang/String;
    .end local v6           #standard:Ljava/lang/String;
    :catch_4f
    move-exception v1

    #@50
    .line 375
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v7, "SmsUsageMonitor"

    #@52
    const-string v9, "XML parser exception reading short code patterns"

    #@54
    invoke-static {v7, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@57
    goto :goto_16

    #@58
    .line 371
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v2       #element:Ljava/lang/String;
    :cond_58
    :try_start_58
    const-string v7, "SmsUsageMonitor"

    #@5a
    new-instance v9, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v10, "Error: skipping unknown XML tag "

    #@61
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v9

    #@65
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v9

    #@69
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v9

    #@6d
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_70
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_58 .. :try_end_70} :catch_4f
    .catch Ljava/io/IOException; {:try_start_58 .. :try_end_70} :catch_71

    #@70
    goto :goto_6

    #@71
    .line 376
    .end local v2           #element:Ljava/lang/String;
    :catch_71
    move-exception v1

    #@72
    .line 377
    .local v1, e:Ljava/io/IOException;
    const-string v7, "SmsUsageMonitor"

    #@74
    const-string v9, "I/O exception reading short code patterns"

    #@76
    invoke-static {v7, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@79
    goto :goto_16
.end method

.method private isUnderLimit(Ljava/util/ArrayList;I)Z
    .registers 13
    .parameter
    .parameter "smsWaiting"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;I)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p1, sent:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v5, 0x0

    #@1
    .line 678
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@4
    move-result-wide v6

    #@5
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@8
    move-result-object v2

    #@9
    .line 679
    .local v2, ct:Ljava/lang/Long;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@c
    move-result-wide v6

    #@d
    iget v4, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCheckPeriod:I

    #@f
    int-to-long v8, v4

    #@10
    sub-long v0, v6, v8

    #@12
    .line 683
    .local v0, beginCheckPeriod:J
    :goto_12
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    #@15
    move-result v4

    #@16
    if-nez v4, :cond_2a

    #@18
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v4

    #@1c
    check-cast v4, Ljava/lang/Long;

    #@1e
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    #@21
    move-result-wide v6

    #@22
    cmp-long v4, v6, v0

    #@24
    if-gez v4, :cond_2a

    #@26
    .line 684
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@29
    goto :goto_12

    #@2a
    .line 687
    :cond_2a
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@2d
    move-result v4

    #@2e
    add-int/2addr v4, p2

    #@2f
    iget v6, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mMaxAllowed:I

    #@31
    if-gt v4, v6, :cond_3e

    #@33
    .line 688
    const/4 v3, 0x0

    #@34
    .local v3, i:I
    :goto_34
    if-ge v3, p2, :cond_3c

    #@36
    .line 689
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@39
    .line 688
    add-int/lit8 v3, v3, 0x1

    #@3b
    goto :goto_34

    #@3c
    .line 691
    :cond_3c
    const/4 v4, 0x1

    #@3d
    .line 693
    .end local v3           #i:I
    :goto_3d
    return v4

    #@3e
    :cond_3e
    move v4, v5

    #@3f
    goto :goto_3d
.end method

.method private loadPremiumSmsPolicyDb()V
    .registers 12

    #@0
    .prologue
    .line 488
    iget-object v8, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPremiumSmsPolicy:Ljava/util/HashMap;

    #@2
    monitor-enter v8

    #@3
    .line 489
    :try_start_3
    iget-object v7, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPolicyFile:Landroid/util/AtomicFile;

    #@5
    if-nez v7, :cond_43

    #@7
    .line 490
    new-instance v0, Ljava/io/File;

    #@9
    const-string v7, "/data/misc/sms"

    #@b
    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@e
    .line 491
    .local v0, dir:Ljava/io/File;
    new-instance v7, Landroid/util/AtomicFile;

    #@10
    new-instance v9, Ljava/io/File;

    #@12
    const-string v10, "premium_sms_policy.xml"

    #@14
    invoke-direct {v9, v0, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@17
    invoke-direct {v7, v9}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@1a
    iput-object v7, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPolicyFile:Landroid/util/AtomicFile;

    #@1c
    .line 493
    iget-object v7, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPremiumSmsPolicy:Ljava/util/HashMap;

    #@1e
    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_f3

    #@21
    .line 495
    const/4 v3, 0x0

    #@22
    .line 497
    .local v3, infile:Ljava/io/FileInputStream;
    :try_start_22
    iget-object v7, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPolicyFile:Landroid/util/AtomicFile;

    #@24
    invoke-virtual {v7}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@27
    move-result-object v3

    #@28
    .line 498
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@2b
    move-result-object v5

    #@2c
    .line 499
    .local v5, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v7, 0x0

    #@2d
    invoke-interface {v5, v3, v7}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@30
    .line 501
    const-string v7, "premium-sms-policy"

    #@32
    invoke-static {v5, v7}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@35
    .line 504
    :goto_35
    invoke-static {v5}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@38
    .line 506
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;
    :try_end_3b
    .catchall {:try_start_22 .. :try_end_3b} :catchall_ec
    .catch Ljava/io/FileNotFoundException; {:try_start_22 .. :try_end_3b} :catch_65
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_3b} :catch_78
    .catch Ljava/lang/NumberFormatException; {:try_start_22 .. :try_end_3b} :catch_b0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_22 .. :try_end_3b} :catch_da

    #@3b
    move-result-object v2

    #@3c
    .line 507
    .local v2, element:Ljava/lang/String;
    if-nez v2, :cond_45

    #@3e
    .line 534
    if-eqz v3, :cond_43

    #@40
    .line 536
    :try_start_40
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_f3
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_43} :catch_f6

    #@43
    .line 542
    .end local v0           #dir:Ljava/io/File;
    .end local v2           #element:Ljava/lang/String;
    .end local v3           #infile:Ljava/io/FileInputStream;
    .end local v5           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :cond_43
    :goto_43
    :try_start_43
    monitor-exit v8
    :try_end_44
    .catchall {:try_start_43 .. :try_end_44} :catchall_f3

    #@44
    .line 543
    return-void

    #@45
    .line 509
    .restart local v0       #dir:Ljava/io/File;
    .restart local v2       #element:Ljava/lang/String;
    .restart local v3       #infile:Ljava/io/FileInputStream;
    .restart local v5       #parser:Lorg/xmlpull/v1/XmlPullParser;
    :cond_45
    :try_start_45
    const-string v7, "package"

    #@47
    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v7

    #@4b
    if-eqz v7, :cond_c0

    #@4d
    .line 510
    const/4 v7, 0x0

    #@4e
    const-string v9, "name"

    #@50
    invoke-interface {v5, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@53
    move-result-object v4

    #@54
    .line 511
    .local v4, packageName:Ljava/lang/String;
    const/4 v7, 0x0

    #@55
    const-string v9, "sms-policy"

    #@57
    invoke-interface {v5, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5a
    move-result-object v6

    #@5b
    .line 512
    .local v6, policy:Ljava/lang/String;
    if-nez v4, :cond_6e

    #@5d
    .line 513
    const-string v7, "SmsUsageMonitor"

    #@5f
    const-string v9, "Error: missing package name attribute"

    #@61
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_64
    .catchall {:try_start_45 .. :try_end_64} :catchall_ec
    .catch Ljava/io/FileNotFoundException; {:try_start_45 .. :try_end_64} :catch_65
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_64} :catch_78
    .catch Ljava/lang/NumberFormatException; {:try_start_45 .. :try_end_64} :catch_b0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_45 .. :try_end_64} :catch_da

    #@64
    goto :goto_35

    #@65
    .line 525
    .end local v2           #element:Ljava/lang/String;
    .end local v4           #packageName:Ljava/lang/String;
    .end local v5           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v6           #policy:Ljava/lang/String;
    :catch_65
    move-exception v7

    #@66
    .line 534
    if-eqz v3, :cond_43

    #@68
    .line 536
    :try_start_68
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6b
    .catchall {:try_start_68 .. :try_end_6b} :catchall_f3
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_6b} :catch_6c

    #@6b
    goto :goto_43

    #@6c
    .line 537
    :catch_6c
    move-exception v7

    #@6d
    goto :goto_43

    #@6e
    .line 514
    .restart local v2       #element:Ljava/lang/String;
    .restart local v4       #packageName:Ljava/lang/String;
    .restart local v5       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6       #policy:Ljava/lang/String;
    :cond_6e
    if-nez v6, :cond_88

    #@70
    .line 515
    :try_start_70
    const-string v7, "SmsUsageMonitor"

    #@72
    const-string v9, "Error: missing package policy attribute"

    #@74
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_77
    .catchall {:try_start_70 .. :try_end_77} :catchall_ec
    .catch Ljava/io/FileNotFoundException; {:try_start_70 .. :try_end_77} :catch_65
    .catch Ljava/io/IOException; {:try_start_70 .. :try_end_77} :catch_78
    .catch Ljava/lang/NumberFormatException; {:try_start_70 .. :try_end_77} :catch_b0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_70 .. :try_end_77} :catch_da

    #@77
    goto :goto_35

    #@78
    .line 527
    .end local v2           #element:Ljava/lang/String;
    .end local v4           #packageName:Ljava/lang/String;
    .end local v5           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v6           #policy:Ljava/lang/String;
    :catch_78
    move-exception v1

    #@79
    .line 528
    .local v1, e:Ljava/io/IOException;
    :try_start_79
    const-string v7, "SmsUsageMonitor"

    #@7b
    const-string v9, "Unable to read premium SMS policy database"

    #@7d
    invoke-static {v7, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_80
    .catchall {:try_start_79 .. :try_end_80} :catchall_ec

    #@80
    .line 534
    if-eqz v3, :cond_43

    #@82
    .line 536
    :try_start_82
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_85
    .catchall {:try_start_82 .. :try_end_85} :catchall_f3
    .catch Ljava/io/IOException; {:try_start_82 .. :try_end_85} :catch_86

    #@85
    goto :goto_43

    #@86
    .line 537
    :catch_86
    move-exception v7

    #@87
    goto :goto_43

    #@88
    .line 517
    .end local v1           #e:Ljava/io/IOException;
    .restart local v2       #element:Ljava/lang/String;
    .restart local v4       #packageName:Ljava/lang/String;
    .restart local v5       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6       #policy:Ljava/lang/String;
    :cond_88
    :try_start_88
    iget-object v7, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPremiumSmsPolicy:Ljava/util/HashMap;

    #@8a
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8d
    move-result v9

    #@8e
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@91
    move-result-object v9

    #@92
    invoke-virtual {v7, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_95
    .catchall {:try_start_88 .. :try_end_95} :catchall_ec
    .catch Ljava/lang/NumberFormatException; {:try_start_88 .. :try_end_95} :catch_96
    .catch Ljava/io/FileNotFoundException; {:try_start_88 .. :try_end_95} :catch_65
    .catch Ljava/io/IOException; {:try_start_88 .. :try_end_95} :catch_78
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_88 .. :try_end_95} :catch_da

    #@95
    goto :goto_35

    #@96
    .line 518
    :catch_96
    move-exception v1

    #@97
    .line 519
    .local v1, e:Ljava/lang/NumberFormatException;
    :try_start_97
    const-string v7, "SmsUsageMonitor"

    #@99
    new-instance v9, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    const-string v10, "Error: non-numeric policy type "

    #@a0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v9

    #@a4
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v9

    #@a8
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v9

    #@ac
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_af
    .catchall {:try_start_97 .. :try_end_af} :catchall_ec
    .catch Ljava/io/FileNotFoundException; {:try_start_97 .. :try_end_af} :catch_65
    .catch Ljava/io/IOException; {:try_start_97 .. :try_end_af} :catch_78
    .catch Ljava/lang/NumberFormatException; {:try_start_97 .. :try_end_af} :catch_b0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_97 .. :try_end_af} :catch_da

    #@af
    goto :goto_35

    #@b0
    .line 529
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .end local v2           #element:Ljava/lang/String;
    .end local v4           #packageName:Ljava/lang/String;
    .end local v5           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v6           #policy:Ljava/lang/String;
    :catch_b0
    move-exception v1

    #@b1
    .line 530
    .restart local v1       #e:Ljava/lang/NumberFormatException;
    :try_start_b1
    const-string v7, "SmsUsageMonitor"

    #@b3
    const-string v9, "Unable to parse premium SMS policy database"

    #@b5
    invoke-static {v7, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b8
    .catchall {:try_start_b1 .. :try_end_b8} :catchall_ec

    #@b8
    .line 534
    if-eqz v3, :cond_43

    #@ba
    .line 536
    :try_start_ba
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_bd
    .catchall {:try_start_ba .. :try_end_bd} :catchall_f3
    .catch Ljava/io/IOException; {:try_start_ba .. :try_end_bd} :catch_be

    #@bd
    goto :goto_43

    #@be
    .line 537
    :catch_be
    move-exception v7

    #@bf
    goto :goto_43

    #@c0
    .line 522
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .restart local v2       #element:Ljava/lang/String;
    .restart local v5       #parser:Lorg/xmlpull/v1/XmlPullParser;
    :cond_c0
    :try_start_c0
    const-string v7, "SmsUsageMonitor"

    #@c2
    new-instance v9, Ljava/lang/StringBuilder;

    #@c4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c7
    const-string v10, "Error: skipping unknown XML tag "

    #@c9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v9

    #@cd
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v9

    #@d1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v9

    #@d5
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d8
    .catchall {:try_start_c0 .. :try_end_d8} :catchall_ec
    .catch Ljava/io/FileNotFoundException; {:try_start_c0 .. :try_end_d8} :catch_65
    .catch Ljava/io/IOException; {:try_start_c0 .. :try_end_d8} :catch_78
    .catch Ljava/lang/NumberFormatException; {:try_start_c0 .. :try_end_d8} :catch_b0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_c0 .. :try_end_d8} :catch_da

    #@d8
    goto/16 :goto_35

    #@da
    .line 531
    .end local v2           #element:Ljava/lang/String;
    .end local v5           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_da
    move-exception v1

    #@db
    .line 532
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_db
    const-string v7, "SmsUsageMonitor"

    #@dd
    const-string v9, "Unable to parse premium SMS policy database"

    #@df
    invoke-static {v7, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e2
    .catchall {:try_start_db .. :try_end_e2} :catchall_ec

    #@e2
    .line 534
    if-eqz v3, :cond_43

    #@e4
    .line 536
    :try_start_e4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_e7
    .catchall {:try_start_e4 .. :try_end_e7} :catchall_f3
    .catch Ljava/io/IOException; {:try_start_e4 .. :try_end_e7} :catch_e9

    #@e7
    goto/16 :goto_43

    #@e9
    .line 537
    :catch_e9
    move-exception v7

    #@ea
    goto/16 :goto_43

    #@ec
    .line 534
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_ec
    move-exception v7

    #@ed
    if-eqz v3, :cond_f2

    #@ef
    .line 536
    :try_start_ef
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_f2
    .catchall {:try_start_ef .. :try_end_f2} :catchall_f3
    .catch Ljava/io/IOException; {:try_start_ef .. :try_end_f2} :catch_f9

    #@f2
    .line 538
    :cond_f2
    :goto_f2
    :try_start_f2
    throw v7

    #@f3
    .line 542
    .end local v0           #dir:Ljava/io/File;
    .end local v3           #infile:Ljava/io/FileInputStream;
    :catchall_f3
    move-exception v7

    #@f4
    monitor-exit v8
    :try_end_f5
    .catchall {:try_start_f2 .. :try_end_f5} :catchall_f3

    #@f5
    throw v7

    #@f6
    .line 537
    .restart local v0       #dir:Ljava/io/File;
    .restart local v2       #element:Ljava/lang/String;
    .restart local v3       #infile:Ljava/io/FileInputStream;
    .restart local v5       #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_f6
    move-exception v7

    #@f7
    goto/16 :goto_43

    #@f9
    .end local v2           #element:Ljava/lang/String;
    .end local v5           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_f9
    move-exception v9

    #@fa
    goto :goto_f2
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "msg"

    #@0
    .prologue
    .line 697
    const-string v0, "SmsUsageMonitor"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 698
    return-void
.end method

.method public static mergeShortCodeCategories(II)I
    .registers 2
    .parameter "type1"
    .parameter "type2"

    #@0
    .prologue
    .line 126
    if-le p0, p1, :cond_3

    #@2
    .line 127
    .end local p0
    :goto_2
    return p0

    #@3
    .restart local p0
    :cond_3
    move p0, p1

    #@4
    goto :goto_2
.end method

.method private removeExpiredTimestamps()V
    .registers 10

    #@0
    .prologue
    .line 663
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v5

    #@4
    iget v7, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCheckPeriod:I

    #@6
    int-to-long v7, v7

    #@7
    sub-long v0, v5, v7

    #@9
    .line 665
    .local v0, beginCheckPeriod:J
    iget-object v6, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mSmsStamp:Ljava/util/HashMap;

    #@b
    monitor-enter v6

    #@c
    .line 666
    :try_start_c
    iget-object v5, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mSmsStamp:Ljava/util/HashMap;

    #@e
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@11
    move-result-object v5

    #@12
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v3

    #@16
    .line 667
    .local v3, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Long;>;>;>;"
    :cond_16
    :goto_16
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v5

    #@1a
    if-eqz v5, :cond_49

    #@1c
    .line 668
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, Ljava/util/Map$Entry;

    #@22
    .line 669
    .local v2, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Long;>;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@25
    move-result-object v4

    #@26
    check-cast v4, Ljava/util/ArrayList;

    #@28
    .line 670
    .local v4, oldList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    #@2b
    move-result v5

    #@2c
    if-nez v5, :cond_42

    #@2e
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v5

    #@32
    add-int/lit8 v5, v5, -0x1

    #@34
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@37
    move-result-object v5

    #@38
    check-cast v5, Ljava/lang/Long;

    #@3a
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    #@3d
    move-result-wide v7

    #@3e
    cmp-long v5, v7, v0

    #@40
    if-gez v5, :cond_16

    #@42
    .line 671
    :cond_42
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    #@45
    goto :goto_16

    #@46
    .line 674
    .end local v2           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Long;>;>;"
    .end local v3           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Long;>;>;>;"
    .end local v4           #oldList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catchall_46
    move-exception v5

    #@47
    monitor-exit v6
    :try_end_48
    .catchall {:try_start_c .. :try_end_48} :catchall_46

    #@48
    throw v5

    #@49
    .restart local v3       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Long;>;>;>;"
    :cond_49
    :try_start_49
    monitor-exit v6
    :try_end_4a
    .catchall {:try_start_49 .. :try_end_4a} :catchall_46

    #@4a
    .line 675
    return-void
.end method

.method private writePremiumSmsPolicyDb()V
    .registers 10

    #@0
    .prologue
    .line 550
    iget-object v6, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPremiumSmsPolicy:Ljava/util/HashMap;

    #@2
    monitor-enter v6

    #@3
    .line 551
    const/4 v3, 0x0

    #@4
    .line 553
    .local v3, outfile:Ljava/io/FileOutputStream;
    :try_start_4
    iget-object v5, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPolicyFile:Landroid/util/AtomicFile;

    #@6
    invoke-virtual {v5}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@9
    move-result-object v3

    #@a
    .line 555
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    #@c
    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@f
    .line 556
    .local v2, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string v5, "utf-8"

    #@11
    invoke-interface {v2, v3, v5}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@14
    .line 558
    const/4 v5, 0x0

    #@15
    const/4 v7, 0x1

    #@16
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@19
    move-result-object v7

    #@1a
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@1d
    .line 560
    const/4 v5, 0x0

    #@1e
    const-string v7, "premium-sms-policy"

    #@20
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@23
    .line 562
    iget-object v5, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPremiumSmsPolicy:Ljava/util/HashMap;

    #@25
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@28
    move-result-object v5

    #@29
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2c
    move-result-object v1

    #@2d
    .local v1, i$:Ljava/util/Iterator;
    :goto_2d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@30
    move-result v5

    #@31
    if-eqz v5, :cond_73

    #@33
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@36
    move-result-object v4

    #@37
    check-cast v4, Ljava/util/Map$Entry;

    #@39
    .line 563
    .local v4, policy:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    #@3a
    const-string v7, "package"

    #@3c
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3f
    .line 564
    const/4 v7, 0x0

    #@40
    const-string v8, "name"

    #@42
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@45
    move-result-object v5

    #@46
    check-cast v5, Ljava/lang/String;

    #@48
    invoke-interface {v2, v7, v8, v5}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4b
    .line 565
    const/4 v7, 0x0

    #@4c
    const-string v8, "sms-policy"

    #@4e
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@51
    move-result-object v5

    #@52
    check-cast v5, Ljava/lang/Integer;

    #@54
    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    #@57
    move-result-object v5

    #@58
    invoke-interface {v2, v7, v8, v5}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5b
    .line 566
    const/4 v5, 0x0

    #@5c
    const-string v7, "package"

    #@5e
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_61
    .catchall {:try_start_4 .. :try_end_61} :catchall_82
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_61} :catch_62

    #@61
    goto :goto_2d

    #@62
    .line 573
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #out:Lorg/xmlpull/v1/XmlSerializer;
    .end local v4           #policy:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :catch_62
    move-exception v0

    #@63
    .line 574
    .local v0, e:Ljava/io/IOException;
    :try_start_63
    const-string v5, "SmsUsageMonitor"

    #@65
    const-string v7, "Unable to write premium SMS policy database"

    #@67
    invoke-static {v5, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6a
    .line 575
    if-eqz v3, :cond_71

    #@6c
    .line 576
    iget-object v5, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPolicyFile:Landroid/util/AtomicFile;

    #@6e
    invoke-virtual {v5, v3}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@71
    .line 579
    .end local v0           #e:Ljava/io/IOException;
    :cond_71
    :goto_71
    monitor-exit v6
    :try_end_72
    .catchall {:try_start_63 .. :try_end_72} :catchall_82

    #@72
    .line 580
    return-void

    #@73
    .line 569
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #out:Lorg/xmlpull/v1/XmlSerializer;
    :cond_73
    const/4 v5, 0x0

    #@74
    :try_start_74
    const-string v7, "premium-sms-policy"

    #@76
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@79
    .line 570
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@7c
    .line 572
    iget-object v5, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPolicyFile:Landroid/util/AtomicFile;

    #@7e
    invoke-virtual {v5, v3}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_81
    .catchall {:try_start_74 .. :try_end_81} :catchall_82
    .catch Ljava/io/IOException; {:try_start_74 .. :try_end_81} :catch_62

    #@81
    goto :goto_71

    #@82
    .line 579
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #out:Lorg/xmlpull/v1/XmlSerializer;
    :catchall_82
    move-exception v5

    #@83
    :try_start_83
    monitor-exit v6
    :try_end_84
    .catchall {:try_start_83 .. :try_end_84} :catchall_82

    #@84
    throw v5
.end method


# virtual methods
.method public check(Ljava/lang/String;I)Z
    .registers 6
    .parameter "appName"
    .parameter "smsWaiting"

    #@0
    .prologue
    .line 399
    iget-object v2, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mSmsStamp:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 400
    :try_start_3
    invoke-direct {p0}, Lcom/android/internal/telephony/SmsUsageMonitor;->removeExpiredTimestamps()V

    #@6
    .line 402
    iget-object v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mSmsStamp:Ljava/util/HashMap;

    #@8
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Ljava/util/ArrayList;

    #@e
    .line 403
    .local v0, sentList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    if-nez v0, :cond_1a

    #@10
    .line 404
    new-instance v0, Ljava/util/ArrayList;

    #@12
    .end local v0           #sentList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@15
    .line 405
    .restart local v0       #sentList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-object v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mSmsStamp:Ljava/util/HashMap;

    #@17
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    .line 408
    :cond_1a
    invoke-direct {p0, v0, p2}, Lcom/android/internal/telephony/SmsUsageMonitor;->isUnderLimit(Ljava/util/ArrayList;I)Z

    #@1d
    move-result v1

    #@1e
    monitor-exit v2

    #@1f
    return v1

    #@20
    .line 409
    .end local v0           #sentList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catchall_20
    move-exception v1

    #@21
    monitor-exit v2
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_20

    #@22
    throw v1
.end method

.method public checkDestination(Ljava/lang/String;Ljava/lang/String;)I
    .registers 10
    .parameter "destAddress"
    .parameter "countryIso"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 427
    iget-object v2, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mSettingsObserverHandler:Lcom/android/internal/telephony/SmsUsageMonitor$SettingsObserverHandler;

    #@4
    monitor-enter v2

    #@5
    .line 429
    :try_start_5
    invoke-static {p1, p2}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;Ljava/lang/String;)Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_d

    #@b
    .line 431
    monitor-exit v2

    #@c
    .line 477
    :goto_c
    return v0

    #@d
    .line 434
    :cond_d
    iget-object v3, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCheckEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@f
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@12
    move-result v3

    #@13
    if-nez v3, :cond_1a

    #@15
    .line 436
    monitor-exit v2

    #@16
    goto :goto_c

    #@17
    .line 480
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v2
    :try_end_19
    .catchall {:try_start_5 .. :try_end_19} :catchall_17

    #@19
    throw v0

    #@1a
    .line 439
    :cond_1a
    if-eqz p2, :cond_44

    #@1c
    .line 440
    :try_start_1c
    iget-object v3, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCurrentCountry:Ljava/lang/String;

    #@1e
    if-eqz v3, :cond_34

    #@20
    iget-object v3, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCurrentCountry:Ljava/lang/String;

    #@22
    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_34

    #@28
    iget-object v3, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFile:Ljava/io/File;

    #@2a
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    #@2d
    move-result-wide v3

    #@2e
    iget-wide v5, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFileLastModified:J

    #@30
    cmp-long v3, v3, v5

    #@32
    if-eqz v3, :cond_44

    #@34
    .line 442
    :cond_34
    iget-object v3, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPatternFile:Ljava/io/File;

    #@36
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@39
    move-result v3

    #@3a
    if-eqz v3, :cond_50

    #@3c
    .line 444
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/SmsUsageMonitor;->getPatternMatcherFromFile(Ljava/lang/String;)Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;

    #@3f
    move-result-object v3

    #@40
    iput-object v3, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCurrentPatternMatcher:Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;

    #@42
    .line 449
    :goto_42
    iput-object p2, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCurrentCountry:Ljava/lang/String;

    #@44
    .line 453
    :cond_44
    iget-object v3, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCurrentPatternMatcher:Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;

    #@46
    if-eqz v3, :cond_57

    #@48
    .line 454
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCurrentPatternMatcher:Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;

    #@4a
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->getNumberCategory(Ljava/lang/String;)I

    #@4d
    move-result v0

    #@4e
    monitor-exit v2

    #@4f
    goto :goto_c

    #@50
    .line 447
    :cond_50
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/SmsUsageMonitor;->getPatternMatcherFromResource(Ljava/lang/String;)Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;

    #@53
    move-result-object v3

    #@54
    iput-object v3, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mCurrentPatternMatcher:Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;

    #@56
    goto :goto_42

    #@57
    .line 457
    :cond_57
    const-string v3, "SmsUsageMonitor"

    #@59
    new-instance v4, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v5, "No patterns for \""

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    const-string v5, "\": using generic short code rule"

    #@6a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v4

    #@72
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 458
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@78
    move-result v3

    #@79
    const/4 v4, 0x5

    #@7a
    if-gt v3, v4, :cond_b8

    #@7c
    .line 460
    iget-object v3, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mContext:Landroid/content/Context;

    #@7e
    const-string v4, "confirmRead"

    #@80
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@83
    move-result v3

    #@84
    if-ne v3, v1, :cond_97

    #@86
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isReleaseOperator()Z

    #@89
    move-result v3

    #@8a
    if-ne v3, v1, :cond_97

    #@8c
    .line 462
    const-string v3, "#431"

    #@8e
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@91
    move-result v3

    #@92
    if-eqz v3, :cond_97

    #@94
    .line 463
    monitor-exit v2

    #@95
    goto/16 :goto_c

    #@97
    .line 468
    :cond_97
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mContext:Landroid/content/Context;

    #@99
    const-string v3, "tmus_no_premium_sms_vvm"

    #@9b
    invoke-static {v0, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9e
    move-result v0

    #@9f
    if-ne v0, v1, :cond_b4

    #@a1
    .line 469
    const-string v0, "122"

    #@a3
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a6
    move-result v0

    #@a7
    if-eqz v0, :cond_b4

    #@a9
    .line 470
    const-string v0, "SmsUsageMonitor"

    #@ab
    const-string v3, "TMUS VVM app send SMS to 122, 122 is free short code "

    #@ad
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 471
    monitor-exit v2

    #@b1
    move v0, v1

    #@b2
    goto/16 :goto_c

    #@b4
    .line 475
    :cond_b4
    const/4 v0, 0x3

    #@b5
    monitor-exit v2

    #@b6
    goto/16 :goto_c

    #@b8
    .line 477
    :cond_b8
    monitor-exit v2
    :try_end_b9
    .catchall {:try_start_1c .. :try_end_b9} :catchall_17

    #@b9
    goto/16 :goto_c
.end method

.method dispose()V
    .registers 2

    #@0
    .prologue
    .line 385
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mSmsStamp:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 386
    return-void
.end method

.method public getPremiumSmsPermission(Ljava/lang/String;)I
    .registers 5
    .parameter "packageName"

    #@0
    .prologue
    .line 594
    invoke-static {p1}, Lcom/android/internal/telephony/SmsUsageMonitor;->checkCallerIsSystemOrSameApp(Ljava/lang/String;)V

    #@3
    .line 595
    iget-object v2, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPremiumSmsPolicy:Ljava/util/HashMap;

    #@5
    monitor-enter v2

    #@6
    .line 596
    :try_start_6
    iget-object v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPremiumSmsPolicy:Ljava/util/HashMap;

    #@8
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Ljava/lang/Integer;

    #@e
    .line 597
    .local v0, policy:Ljava/lang/Integer;
    if-nez v0, :cond_13

    #@10
    .line 598
    const/4 v1, 0x0

    #@11
    monitor-exit v2

    #@12
    .line 600
    :goto_12
    return v1

    #@13
    :cond_13
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@16
    move-result v1

    #@17
    monitor-exit v2

    #@18
    goto :goto_12

    #@19
    .line 602
    .end local v0           #policy:Ljava/lang/Integer;
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_6 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method public setPremiumSmsPermission(Ljava/lang/String;I)V
    .registers 6
    .parameter "packageName"
    .parameter "permission"

    #@0
    .prologue
    .line 615
    invoke-static {}, Lcom/android/internal/telephony/SmsUsageMonitor;->checkCallerIsSystemOrPhoneApp()V

    #@3
    .line 616
    const/4 v0, 0x1

    #@4
    if-lt p2, v0, :cond_9

    #@6
    const/4 v0, 0x3

    #@7
    if-le p2, v0, :cond_22

    #@9
    .line 618
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "invalid SMS permission type "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 620
    :cond_22
    iget-object v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPremiumSmsPolicy:Ljava/util/HashMap;

    #@24
    monitor-enter v1

    #@25
    .line 621
    :try_start_25
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor;->mPremiumSmsPolicy:Ljava/util/HashMap;

    #@27
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    .line 622
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_25 .. :try_end_2f} :catchall_3d

    #@2f
    .line 624
    new-instance v0, Ljava/lang/Thread;

    #@31
    new-instance v1, Lcom/android/internal/telephony/SmsUsageMonitor$1;

    #@33
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/SmsUsageMonitor$1;-><init>(Lcom/android/internal/telephony/SmsUsageMonitor;)V

    #@36
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@39
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@3c
    .line 630
    return-void

    #@3d
    .line 622
    :catchall_3d
    move-exception v0

    #@3e
    :try_start_3e
    monitor-exit v1
    :try_end_3f
    .catchall {:try_start_3e .. :try_end_3f} :catchall_3d

    #@3f
    throw v0
.end method
