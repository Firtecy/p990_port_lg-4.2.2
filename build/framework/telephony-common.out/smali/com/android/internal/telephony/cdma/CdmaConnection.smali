.class public Lcom/android/internal/telephony/cdma/CdmaConnection;
.super Lcom/android/internal/telephony/Connection;
.source "CdmaConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/CdmaConnection$1;,
        Lcom/android/internal/telephony/cdma/CdmaConnection$MyHandler;
    }
.end annotation


# static fields
.field static final EVENT_DTMF_DONE:I = 0x1

.field static final EVENT_NEXT_POST_DIAL:I = 0x3

.field static final EVENT_PAUSE_DONE:I = 0x2

.field static final EVENT_WAKE_LOCK_TIMEOUT:I = 0x4

.field static final KDDI_PI_PRESENTATION_ALLOWED:I = 0x0

.field static final KDDI_PI_PRESENTATION_RESTRICTED:I = 0x1

.field static final LOG_TAG:Ljava/lang/String; = "CDMA"

.field static final PAUSE_DELAY_MILLIS:I = 0x7d0

.field static final WAKE_LOCK_TIMEOUT_MILLIS:I = 0xea60


# instance fields
.field address:Ljava/lang/String;

.field cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field cdnipNumber:Ljava/lang/String;

.field connectTime:J

.field createTime:J

.field cwKddi:Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;

.field dialString:Ljava/lang/String;

.field disconnectTime:J

.field disconnected:Z

.field duration:J

.field h:Landroid/os/Handler;

.field holdingStartTime:J

.field index:I

.field isCdmaInfoRecReceivedKddi:Z

.field isIncoming:Z

.field isNotifyRingingConnectionKddi:Z

.field isNumberInfoReceivedKddi:Z

.field private mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field nextPostDialChar:I

.field numberPresentation:I

.field owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

.field parent:Lcom/android/internal/telephony/cdma/CdmaCall;

.field postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

.field postDialString:Ljava/lang/String;

.field presentationIndicatorsKddi:I

.field redialNumber:Ljava/lang/String;

.field redirectNumber:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/DriverCall;Lcom/android/internal/telephony/cdma/CdmaCallTracker;I)V
    .registers 7
    .parameter "context"
    .parameter "dc"
    .parameter "ct"
    .parameter "index"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 149
    invoke-direct {p0}, Lcom/android/internal/telephony/Connection;-><init>()V

    #@4
    .line 68
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cdnipNumber:Ljava/lang/String;

    #@7
    .line 90
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@b
    .line 91
    sget-object v0, Lcom/android/internal/telephony/Connection$PostDialState;->NOT_STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@d
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@f
    .line 92
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@11
    iput v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@13
    .line 105
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@15
    .line 106
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNumberInfoReceivedKddi:Z

    #@17
    .line 107
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNotifyRingingConnectionKddi:Z

    #@19
    .line 150
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->createWakeLock(Landroid/content/Context;)V

    #@1c
    .line 151
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->acquireWakeLock()V

    #@1f
    .line 153
    iput-object p3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@21
    .line 154
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaConnection$MyHandler;

    #@23
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@25
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->getLooper()Landroid/os/Looper;

    #@28
    move-result-object v1

    #@29
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection$MyHandler;-><init>(Lcom/android/internal/telephony/cdma/CdmaConnection;Landroid/os/Looper;)V

    #@2c
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@2e
    .line 156
    iget-object v0, p2, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@30
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@32
    .line 158
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@34
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->setRedialNumber(Ljava/lang/String;)V

    #@37
    .line 161
    iget-boolean v0, p2, Lcom/android/internal/telephony/DriverCall;->isMT:Z

    #@39
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@3b
    .line 162
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3e
    move-result-wide v0

    #@3f
    iput-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->createTime:J

    #@41
    .line 163
    iget-object v0, p2, Lcom/android/internal/telephony/DriverCall;->name:Ljava/lang/String;

    #@43
    iput-object v0, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@45
    .line 164
    iget v0, p2, Lcom/android/internal/telephony/DriverCall;->namePresentation:I

    #@47
    iput v0, p0, Lcom/android/internal/telephony/Connection;->cnapNamePresentation:I

    #@49
    .line 165
    iget v0, p2, Lcom/android/internal/telephony/DriverCall;->numberPresentation:I

    #@4b
    iput v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@4d
    .line 167
    iget-object v0, p2, Lcom/android/internal/telephony/DriverCall;->cdnipNumber:Ljava/lang/String;

    #@4f
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cdnipNumber:Ljava/lang/String;

    #@51
    .line 171
    const-string v0, "KDDI"

    #@53
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@56
    move-result v0

    #@57
    if-eqz v0, :cond_6b

    #@59
    .line 172
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@5b
    if-eqz v0, :cond_6b

    #@5d
    .line 173
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@5f
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@61
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@68
    move-result v0

    #@69
    if-eqz v0, :cond_7b

    #@6b
    .line 189
    :cond_6b
    :goto_6b
    iput p4, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->index:I

    #@6d
    .line 191
    iget-object v0, p2, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@6f
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->parentFromDCState(Lcom/android/internal/telephony/DriverCall$State;)Lcom/android/internal/telephony/cdma/CdmaCall;

    #@72
    move-result-object v0

    #@73
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@75
    .line 192
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@77
    invoke-virtual {v0, p0, p2}, Lcom/android/internal/telephony/cdma/CdmaCall;->attach(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/DriverCall;)V

    #@7a
    .line 193
    return-void

    #@7b
    .line 177
    :cond_7b
    const-string v0, "persist.radio.hwtest"

    #@7d
    const-string v1, "false"

    #@7f
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@82
    move-result-object v0

    #@83
    const-string v1, "true"

    #@85
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@88
    move-result v0

    #@89
    if-nez v0, :cond_6b

    #@8b
    .line 182
    const-string v0, ""

    #@8d
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@8f
    goto :goto_6b
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;Lcom/android/internal/telephony/cdma/CdmaCallTracker;Lcom/android/internal/telephony/cdma/CdmaCall;)V
    .registers 7
    .parameter "context"
    .parameter "cw"
    .parameter "ct"
    .parameter "parent"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 240
    invoke-direct {p0}, Lcom/android/internal/telephony/Connection;-><init>()V

    #@4
    .line 68
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cdnipNumber:Ljava/lang/String;

    #@7
    .line 90
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@b
    .line 91
    sget-object v0, Lcom/android/internal/telephony/Connection$PostDialState;->NOT_STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@d
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@f
    .line 92
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@11
    iput v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@13
    .line 105
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@15
    .line 106
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNumberInfoReceivedKddi:Z

    #@17
    .line 107
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNotifyRingingConnectionKddi:Z

    #@19
    .line 241
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->createWakeLock(Landroid/content/Context;)V

    #@1c
    .line 242
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->acquireWakeLock()V

    #@1f
    .line 244
    iput-object p3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@21
    .line 245
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaConnection$MyHandler;

    #@23
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@25
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->getLooper()Landroid/os/Looper;

    #@28
    move-result-object v1

    #@29
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection$MyHandler;-><init>(Lcom/android/internal/telephony/cdma/CdmaConnection;Landroid/os/Looper;)V

    #@2c
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@2e
    .line 246
    iget-object v0, p2, Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;->number:Ljava/lang/String;

    #@30
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@32
    .line 248
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@34
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->setRedialNumber(Ljava/lang/String;)V

    #@37
    .line 250
    iget v0, p2, Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;->numberPresentation:I

    #@39
    iput v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@3b
    .line 251
    iget-object v0, p2, Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;->name:Ljava/lang/String;

    #@3d
    iput-object v0, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@3f
    .line 252
    iget v0, p2, Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;->namePresentation:I

    #@41
    iput v0, p0, Lcom/android/internal/telephony/Connection;->cnapNamePresentation:I

    #@43
    .line 253
    const/4 v0, -0x1

    #@44
    iput v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->index:I

    #@46
    .line 254
    const/4 v0, 0x1

    #@47
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@49
    .line 255
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@4c
    move-result-wide v0

    #@4d
    iput-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->createTime:J

    #@4f
    .line 256
    const-wide/16 v0, 0x0

    #@51
    iput-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->connectTime:J

    #@53
    .line 258
    iget-object v0, p2, Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;->cdnipNumber:Ljava/lang/String;

    #@55
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cdnipNumber:Ljava/lang/String;

    #@57
    .line 260
    iput-object p4, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@59
    .line 261
    sget-object v0, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    #@5b
    invoke-virtual {p4, p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->attachFake(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/Call$State;)V

    #@5e
    .line 263
    iput-object p2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cwKddi:Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;

    #@60
    .line 265
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/internal/telephony/cdma/CdmaCallTracker;Lcom/android/internal/telephony/cdma/CdmaCall;)V
    .registers 10
    .parameter "context"
    .parameter "dialString"
    .parameter "ct"
    .parameter "parent"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 197
    invoke-direct {p0}, Lcom/android/internal/telephony/Connection;-><init>()V

    #@5
    .line 68
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cdnipNumber:Ljava/lang/String;

    #@7
    .line 90
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@b
    .line 91
    sget-object v0, Lcom/android/internal/telephony/Connection$PostDialState;->NOT_STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@d
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@f
    .line 92
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@11
    iput v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@13
    .line 105
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@15
    .line 106
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNumberInfoReceivedKddi:Z

    #@17
    .line 107
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNotifyRingingConnectionKddi:Z

    #@19
    .line 198
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->createWakeLock(Landroid/content/Context;)V

    #@1c
    .line 199
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->acquireWakeLock()V

    #@1f
    .line 201
    iput-object p3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@21
    .line 202
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaConnection$MyHandler;

    #@23
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@25
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->getLooper()Landroid/os/Looper;

    #@28
    move-result-object v1

    #@29
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection$MyHandler;-><init>(Lcom/android/internal/telephony/cdma/CdmaConnection;Landroid/os/Looper;)V

    #@2c
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@2e
    .line 204
    iput-object p2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->dialString:Ljava/lang/String;

    #@30
    .line 206
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/cdma/CdmaConnection;->setRedialNumber(Ljava/lang/String;)V

    #@33
    .line 208
    const-string v0, "CDMA"

    #@35
    new-instance v1, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v2, "[CDMAConn] CdmaConnection: dialString="

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v1

    #@48
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 209
    invoke-static {p2}, Lcom/android/internal/telephony/cdma/CdmaConnection;->formatDialString(Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object p2

    #@4f
    .line 210
    const-string v0, "CDMA"

    #@51
    new-instance v1, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v2, "[CDMAConn] CdmaConnection:formated dialString="

    #@58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v1

    #@5c
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v1

    #@64
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 212
    invoke-static {p2}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@6a
    move-result-object v0

    #@6b
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@6d
    .line 213
    invoke-static {p2}, Landroid/telephony/PhoneNumberUtils;->extractPostDialPortion(Ljava/lang/String;)Ljava/lang/String;

    #@70
    move-result-object v0

    #@71
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialString:Ljava/lang/String;

    #@73
    .line 215
    const/4 v0, -0x1

    #@74
    iput v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->index:I

    #@76
    .line 217
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@78
    .line 218
    iput-object v4, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@7a
    .line 219
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@7c
    iput v0, p0, Lcom/android/internal/telephony/Connection;->cnapNamePresentation:I

    #@7e
    .line 220
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@80
    iput v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@82
    .line 221
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@85
    move-result-wide v0

    #@86
    iput-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->createTime:J

    #@88
    .line 223
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cdnipNumber:Ljava/lang/String;

    #@8a
    .line 226
    if-eqz p4, :cond_99

    #@8c
    .line 227
    iput-object p4, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@8e
    .line 230
    iget-object v0, p4, Lcom/android/internal/telephony/cdma/CdmaCall;->state:Lcom/android/internal/telephony/Call$State;

    #@90
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@92
    if-ne v0, v1, :cond_9a

    #@94
    .line 231
    sget-object v0, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@96
    invoke-virtual {p4, p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->attachFake(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/Call$State;)V

    #@99
    .line 236
    :cond_99
    :goto_99
    return-void

    #@9a
    .line 233
    :cond_9a
    sget-object v0, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@9c
    invoke-virtual {p4, p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->attachFake(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/Call$State;)V

    #@9f
    goto :goto_99
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/cdma/CdmaConnection;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 49
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->releaseWakeLock()V

    #@3
    return-void
.end method

.method private acquireWakeLock()V
    .registers 2

    #@0
    .prologue
    .line 998
    const-string v0, "acquireWakeLock"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->log(Ljava/lang/String;)V

    #@5
    .line 999
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@7
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@a
    .line 1000
    return-void
.end method

.method private createWakeLock(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 993
    const-string v1, "power"

    #@2
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/PowerManager;

    #@8
    .line 994
    .local v0, pm:Landroid/os/PowerManager;
    const/4 v1, 0x1

    #@9
    const-string v2, "CDMA"

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@11
    .line 995
    return-void
.end method

.method private doDisconnect()V
    .registers 5

    #@0
    .prologue
    .line 785
    const/4 v0, -0x1

    #@1
    iput v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->index:I

    #@3
    .line 786
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@6
    move-result-wide v0

    #@7
    iput-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->disconnectTime:J

    #@9
    .line 787
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@c
    move-result-wide v0

    #@d
    iget-wide v2, p0, Lcom/android/internal/telephony/Connection;->connectTimeReal:J

    #@f
    sub-long/2addr v0, v2

    #@10
    iput-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->duration:J

    #@12
    .line 788
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->disconnected:Z

    #@15
    .line 789
    return-void
.end method

.method static equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 272
    if-nez p0, :cond_8

    #@2
    if-nez p1, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5

    #@8
    :cond_8
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method private static findNextPCharOrNonPOrNonWCharIndex(Ljava/lang/String;I)I
    .registers 7
    .parameter "phoneNumber"
    .parameter "currIndex"

    #@0
    .prologue
    .line 1024
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    #@3
    move-result v4

    #@4
    invoke-static {v4}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isWait(C)Z

    #@7
    move-result v3

    #@8
    .line 1025
    .local v3, wMatched:Z
    add-int/lit8 v1, p1, 0x1

    #@a
    .line 1026
    .local v1, index:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@d
    move-result v2

    #@e
    .line 1027
    .local v2, length:I
    :goto_e
    if-ge v1, v2, :cond_27

    #@10
    .line 1028
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@13
    move-result v0

    #@14
    .line 1030
    .local v0, cNext:C
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isWait(C)Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_1b

    #@1a
    .line 1031
    const/4 v3, 0x1

    #@1b
    .line 1035
    :cond_1b
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isWait(C)Z

    #@1e
    move-result v4

    #@1f
    if-nez v4, :cond_3c

    #@21
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isPause(C)Z

    #@24
    move-result v4

    #@25
    if-nez v4, :cond_3c

    #@27
    .line 1043
    .end local v0           #cNext:C
    :cond_27
    if-ge v1, v2, :cond_3b

    #@29
    add-int/lit8 v4, p1, 0x1

    #@2b
    if-le v1, v4, :cond_3b

    #@2d
    if-nez v3, :cond_3b

    #@2f
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    #@32
    move-result v4

    #@33
    invoke-static {v4}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isPause(C)Z

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_3b

    #@39
    .line 1045
    add-int/lit8 v1, p1, 0x1

    #@3b
    .line 1047
    .end local v1           #index:I
    :cond_3b
    return v1

    #@3c
    .line 1038
    .restart local v0       #cNext:C
    .restart local v1       #index:I
    :cond_3c
    add-int/lit8 v1, v1, 0x1

    #@3e
    .line 1039
    goto :goto_e
.end method

.method private static findPOrWCharToAppend(Ljava/lang/String;II)C
    .registers 6
    .parameter "phoneNumber"
    .parameter "currPwIndex"
    .parameter "nextNonPwCharIndex"

    #@0
    .prologue
    .line 1055
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    #@3
    move-result v0

    #@4
    .line 1059
    .local v0, c:C
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isPause(C)Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_13

    #@a
    const/16 v1, 0x2c

    #@c
    .line 1067
    .local v1, ret:C
    :goto_c
    add-int/lit8 v2, p1, 0x1

    #@e
    if-le p2, v2, :cond_12

    #@10
    .line 1068
    const/16 v1, 0x3b

    #@12
    .line 1070
    :cond_12
    return v1

    #@13
    .line 1059
    .end local v1           #ret:C
    :cond_13
    const/16 v1, 0x3b

    #@15
    goto :goto_c
.end method

.method public static formatDialString(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 1091
    if-nez p0, :cond_4

    #@2
    .line 1092
    const/4 v6, 0x0

    #@3
    .line 1125
    :goto_3
    return-object v6

    #@4
    .line 1094
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v2

    #@8
    .line 1095
    .local v2, length:I
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    .line 1097
    .local v5, ret:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@e
    .line 1099
    .local v1, currIndex:I
    :goto_e
    if-ge v1, v2, :cond_43

    #@10
    .line 1100
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@13
    move-result v0

    #@14
    .line 1101
    .local v0, c:C
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isPause(C)Z

    #@17
    move-result v6

    #@18
    if-nez v6, :cond_20

    #@1a
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isWait(C)Z

    #@1d
    move-result v6

    #@1e
    if-eqz v6, :cond_3f

    #@20
    .line 1102
    :cond_20
    add-int/lit8 v6, v2, -0x1

    #@22
    if-ge v1, v6, :cond_37

    #@24
    .line 1104
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->findNextPCharOrNonPOrNonWCharIndex(Ljava/lang/String;I)I

    #@27
    move-result v3

    #@28
    .line 1106
    .local v3, nextIndex:I
    if-ge v3, v2, :cond_3a

    #@2a
    .line 1107
    invoke-static {p0, v1, v3}, Lcom/android/internal/telephony/cdma/CdmaConnection;->findPOrWCharToAppend(Ljava/lang/String;II)C

    #@2d
    move-result v4

    #@2e
    .line 1108
    .local v4, pC:C
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@31
    .line 1112
    add-int/lit8 v6, v1, 0x1

    #@33
    if-le v3, v6, :cond_37

    #@35
    .line 1113
    add-int/lit8 v1, v3, -0x1

    #@37
    .line 1123
    .end local v3           #nextIndex:I
    .end local v4           #pC:C
    :cond_37
    :goto_37
    add-int/lit8 v1, v1, 0x1

    #@39
    goto :goto_e

    #@3a
    .line 1115
    .restart local v3       #nextIndex:I
    :cond_3a
    if-ne v3, v2, :cond_37

    #@3c
    .line 1117
    add-int/lit8 v1, v2, -0x1

    #@3e
    goto :goto_37

    #@3f
    .line 1121
    .end local v3           #nextIndex:I
    :cond_3f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@42
    goto :goto_37

    #@43
    .line 1125
    .end local v0           #c:C
    :cond_43
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    invoke-static {v6}, Landroid/telephony/PhoneNumberUtils;->cdmaCheckAndProcessPlusCode(Ljava/lang/String;)Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    goto :goto_3
.end method

.method private isConnectingInOrOut()Z
    .registers 3

    #@0
    .prologue
    .line 939
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2
    if-eqz v0, :cond_1c

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@6
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@8
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@a
    if-eq v0, v1, :cond_1c

    #@c
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@e
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCall;->state:Lcom/android/internal/telephony/Call$State;

    #@10
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@12
    if-eq v0, v1, :cond_1c

    #@14
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@16
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCall;->state:Lcom/android/internal/telephony/Call$State;

    #@18
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ALERTING:Lcom/android/internal/telephony/Call$State;

    #@1a
    if-ne v0, v1, :cond_1e

    #@1c
    :cond_1c
    const/4 v0, 0x1

    #@1d
    :goto_1d
    return v0

    #@1e
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_1d
.end method

.method private static isPause(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 1012
    const/16 v0, 0x2c

    #@2
    if-ne p0, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private static isWait(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 1016
    const/16 v0, 0x3b

    #@2
    if-ne p0, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private log(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 1129
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[CDMAConn] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1130
    return-void
.end method

.method private parentFromDCState(Lcom/android/internal/telephony/DriverCall$State;)Lcom/android/internal/telephony/cdma/CdmaCall;
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 946
    sget-object v0, Lcom/android/internal/telephony/cdma/CdmaConnection$1;->$SwitchMap$com$android$internal$telephony$DriverCall$State:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/telephony/DriverCall$State;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_34

    #@b
    .line 963
    new-instance v0, Ljava/lang/RuntimeException;

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "illegal call state: "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 950
    :pswitch_24
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@26
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@28
    .line 959
    :goto_28
    return-object v0

    #@29
    .line 954
    :pswitch_29
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2b
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2d
    goto :goto_28

    #@2e
    .line 959
    :pswitch_2e
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@30
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@32
    goto :goto_28

    #@33
    .line 946
    nop

    #@34
    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_29
        :pswitch_2e
        :pswitch_2e
    .end packed-switch
.end method

.method private processPostDialChar(C)Z
    .registers 8
    .parameter "c"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 802
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->is12Key(C)Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_32

    #@8
    .line 804
    const/4 v3, 0x0

    #@9
    const-string v4, "support_send_burst_dtmf"

    #@b
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_24

    #@11
    .line 805
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .line 806
    .local v0, dtmfStr:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@17
    iget-object v3, v3, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@19
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@1b
    invoke-virtual {v4, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1e
    move-result-object v4

    #@1f
    invoke-interface {v3, v0, v1, v1, v4}, Lcom/android/internal/telephony/CommandsInterface;->sendBurstDtmf(Ljava/lang/String;IILandroid/os/Message;)V

    #@22
    .end local v0           #dtmfStr:Ljava/lang/String;
    :goto_22
    move v1, v2

    #@23
    .line 827
    :cond_23
    return v1

    #@24
    .line 809
    :cond_24
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@26
    iget-object v1, v1, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@28
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@2a
    invoke-virtual {v3, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@2d
    move-result-object v3

    #@2e
    invoke-interface {v1, p1, v3}, Lcom/android/internal/telephony/CommandsInterface;->sendDtmf(CLandroid/os/Message;)V

    #@31
    goto :goto_22

    #@32
    .line 811
    :cond_32
    const/16 v3, 0x2c

    #@34
    if-ne p1, v3, :cond_4a

    #@36
    .line 812
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->PAUSE:Lcom/android/internal/telephony/Connection$PostDialState;

    #@38
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@3b
    .line 817
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@3d
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@3f
    const/4 v4, 0x2

    #@40
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@43
    move-result-object v3

    #@44
    const-wide/16 v4, 0x7d0

    #@46
    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@49
    goto :goto_22

    #@4a
    .line 819
    :cond_4a
    const/16 v3, 0x3b

    #@4c
    if-ne p1, v3, :cond_54

    #@4e
    .line 820
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->WAIT:Lcom/android/internal/telephony/Connection$PostDialState;

    #@50
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@53
    goto :goto_22

    #@54
    .line 821
    :cond_54
    const/16 v3, 0x4e

    #@56
    if-ne p1, v3, :cond_23

    #@58
    .line 822
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->WILD:Lcom/android/internal/telephony/Connection$PostDialState;

    #@5a
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@5d
    goto :goto_22
.end method

.method private releaseWakeLock()V
    .registers 3

    #@0
    .prologue
    .line 1003
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    monitor-enter v1

    #@3
    .line 1004
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@5
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_15

    #@b
    .line 1005
    const-string v0, "releaseWakeLock"

    #@d
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->log(Ljava/lang/String;)V

    #@10
    .line 1006
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@12
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@15
    .line 1008
    :cond_15
    monitor-exit v1

    #@16
    .line 1009
    return-void

    #@17
    .line 1008
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method private setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V
    .registers 7
    .parameter "s"

    #@0
    .prologue
    const/4 v2, 0x4

    #@1
    .line 974
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@3
    if-eq p1, v1, :cond_9

    #@5
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->PAUSE:Lcom/android/internal/telephony/Connection$PostDialState;

    #@7
    if-ne p1, v1, :cond_34

    #@9
    .line 976
    :cond_9
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@b
    monitor-enter v2

    #@c
    .line 977
    :try_start_c
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@e
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_2d

    #@14
    .line 978
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@16
    const/4 v3, 0x4

    #@17
    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@1a
    .line 982
    :goto_1a
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@1c
    const/4 v3, 0x4

    #@1d
    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@20
    move-result-object v0

    #@21
    .line 983
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@23
    const-wide/32 v3, 0xea60

    #@26
    invoke-virtual {v1, v0, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@29
    .line 984
    monitor-exit v2
    :try_end_2a
    .catchall {:try_start_c .. :try_end_2a} :catchall_31

    #@2a
    .line 989
    .end local v0           #msg:Landroid/os/Message;
    :goto_2a
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2c
    .line 990
    return-void

    #@2d
    .line 980
    :cond_2d
    :try_start_2d
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->acquireWakeLock()V

    #@30
    goto :goto_1a

    #@31
    .line 984
    :catchall_31
    move-exception v1

    #@32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_2d .. :try_end_33} :catchall_31

    #@33
    throw v1

    #@34
    .line 986
    :cond_34
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@36
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@39
    .line 987
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->releaseWakeLock()V

    #@3c
    goto :goto_2a
.end method

.method private setRedialNumber(Ljava/lang/String;)V
    .registers 2
    .parameter "redialNumber"

    #@0
    .prologue
    .line 300
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->redialNumber:Ljava/lang/String;

    #@2
    .line 301
    return-void
.end method


# virtual methods
.method public cancelPostDial()V
    .registers 2

    #@0
    .prologue
    .line 448
    sget-object v0, Lcom/android/internal/telephony/Connection$PostDialState;->CANCELLED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@5
    .line 449
    return-void
.end method

.method compareTo(Lcom/android/internal/telephony/DriverCall;)Z
    .registers 6
    .parameter "c"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 282
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@3
    if-nez v2, :cond_a

    #@5
    iget-boolean v2, p1, Lcom/android/internal/telephony/DriverCall;->isMT:Z

    #@7
    if-nez v2, :cond_a

    #@9
    .line 288
    :cond_9
    :goto_9
    return v1

    #@a
    .line 287
    :cond_a
    iget-object v2, p1, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@c
    iget v3, p1, Lcom/android/internal/telephony/DriverCall;->TOA:I

    #@e
    invoke-static {v2, v3}, Landroid/telephony/PhoneNumberUtils;->stringFromStringAndTOA(Ljava/lang/String;I)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 288
    .local v0, cAddress:Ljava/lang/String;
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@14
    iget-boolean v3, p1, Lcom/android/internal/telephony/DriverCall;->isMT:Z

    #@16
    if-ne v2, v3, :cond_20

    #@18
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@1a
    invoke-static {v2, v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@1d
    move-result v2

    #@1e
    if-nez v2, :cond_9

    #@20
    :cond_20
    const/4 v1, 0x0

    #@21
    goto :goto_9
.end method

.method disconnectCauseFromCode(I)Lcom/android/internal/telephony/Connection$DisconnectCause;
    .registers 8
    .parameter "causeCode"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    .line 474
    sparse-switch p1, :sswitch_data_7a

    #@4
    .line 514
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@6
    iget-object v1, v4, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@8
    .line 515
    .local v1, phone:Lcom/android/internal/telephony/cdma/CDMAPhone;
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@b
    move-result-object v4

    #@c
    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getState()I

    #@f
    move-result v2

    #@10
    .line 516
    .local v2, serviceState:I
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@17
    move-result-object v0

    #@18
    .line 519
    .local v0, app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eqz v0, :cond_5a

    #@1a
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@1d
    move-result-object v3

    #@1e
    .line 520
    .local v3, uiccAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;
    :goto_1e
    const/4 v4, 0x3

    #@1f
    if-ne v2, v4, :cond_5d

    #@21
    .line 521
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->POWER_OFF:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@23
    .line 532
    .end local v0           #app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .end local v1           #phone:Lcom/android/internal/telephony/cdma/CDMAPhone;
    .end local v2           #serviceState:I
    .end local v3           #uiccAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;
    :goto_23
    return-object v4

    #@24
    .line 476
    :sswitch_24
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->BUSY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@26
    goto :goto_23

    #@27
    .line 478
    :sswitch_27
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CONGESTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@29
    goto :goto_23

    #@2a
    .line 480
    :sswitch_2a
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->LIMIT_EXCEEDED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2c
    goto :goto_23

    #@2d
    .line 482
    :sswitch_2d
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CALL_BARRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2f
    goto :goto_23

    #@30
    .line 484
    :sswitch_30
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->FDN_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@32
    goto :goto_23

    #@33
    .line 486
    :sswitch_33
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_USSD:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@35
    goto :goto_23

    #@36
    .line 488
    :sswitch_36
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_SS:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@38
    goto :goto_23

    #@39
    .line 490
    :sswitch_39
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3b
    goto :goto_23

    #@3c
    .line 492
    :sswitch_3c
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_LOCKED_UNTIL_POWER_CYCLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3e
    goto :goto_23

    #@3f
    .line 494
    :sswitch_3f
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_DROP:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@41
    goto :goto_23

    #@42
    .line 496
    :sswitch_42
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_INTERCEPT:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@44
    goto :goto_23

    #@45
    .line 498
    :sswitch_45
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_REORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@47
    goto :goto_23

    #@48
    .line 500
    :sswitch_48
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_SO_REJECT:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4a
    goto :goto_23

    #@4b
    .line 502
    :sswitch_4b
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_RETRY_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4d
    goto :goto_23

    #@4e
    .line 504
    :sswitch_4e
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_ACCESS_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@50
    goto :goto_23

    #@51
    .line 506
    :sswitch_51
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_PREEMPTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@53
    goto :goto_23

    #@54
    .line 508
    :sswitch_54
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_NOT_EMERGENCY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@56
    goto :goto_23

    #@57
    .line 510
    :sswitch_57
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_ACCESS_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@59
    goto :goto_23

    #@5a
    .line 519
    .restart local v0       #app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .restart local v1       #phone:Lcom/android/internal/telephony/cdma/CDMAPhone;
    .restart local v2       #serviceState:I
    :cond_5a
    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_UNKNOWN:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@5c
    goto :goto_1e

    #@5d
    .line 522
    .restart local v3       #uiccAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;
    :cond_5d
    const/4 v4, 0x1

    #@5e
    if-eq v2, v4, :cond_62

    #@60
    if-ne v2, v5, :cond_65

    #@62
    .line 524
    :cond_62
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->OUT_OF_SERVICE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@64
    goto :goto_23

    #@65
    .line 525
    :cond_65
    iget v4, v1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCdmaSubscriptionSource:I

    #@67
    if-nez v4, :cond_70

    #@69
    sget-object v4, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@6b
    if-eq v3, v4, :cond_70

    #@6d
    .line 528
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->ICC_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@6f
    goto :goto_23

    #@70
    .line 529
    :cond_70
    const/16 v4, 0x10

    #@72
    if-ne p1, v4, :cond_77

    #@74
    .line 530
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->NORMAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@76
    goto :goto_23

    #@77
    .line 532
    :cond_77
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->ERROR_UNSPECIFIED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@79
    goto :goto_23

    #@7a
    .line 474
    :sswitch_data_7a
    .sparse-switch
        0x11 -> :sswitch_24
        0x22 -> :sswitch_27
        0x44 -> :sswitch_2a
        0xf0 -> :sswitch_2d
        0xf1 -> :sswitch_30
        0xf4 -> :sswitch_33
        0xf5 -> :sswitch_36
        0xf6 -> :sswitch_39
        0x3e8 -> :sswitch_3c
        0x3e9 -> :sswitch_3f
        0x3ea -> :sswitch_42
        0x3eb -> :sswitch_45
        0x3ec -> :sswitch_48
        0x3ed -> :sswitch_4b
        0x3ee -> :sswitch_4e
        0x3ef -> :sswitch_51
        0x3f0 -> :sswitch_54
        0x3f1 -> :sswitch_57
    .end sparse-switch
.end method

.method public dispose()V
    .registers 1

    #@0
    .prologue
    .line 268
    return-void
.end method

.method fakeHoldBeforeDial()V
    .registers 3

    #@0
    .prologue
    .line 739
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 740
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@6
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/cdma/CdmaCall;->detach(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@9
    .line 743
    :cond_9
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@b
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@d
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@f
    .line 744
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@11
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@13
    invoke-virtual {v0, p0, v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->attachFake(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/Call$State;)V

    #@16
    .line 746
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onStartedHolding()V

    #@19
    .line 747
    return-void
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 871
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 872
    const-string v0, "CDMA"

    #@a
    const-string v1, "[CdmaConn] UNEXPECTED; mPartialWakeLock is held when finalizing."

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 874
    :cond_f
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->releaseWakeLock()V

    #@12
    .line 875
    return-void
.end method

.method public getAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 308
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getBeforeFowardingNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1146
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->redirectNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getCDMAIndex()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 751
    iget v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->index:I

    #@2
    if-ltz v0, :cond_9

    #@4
    .line 752
    iget v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->index:I

    #@6
    add-int/lit8 v0, v0, 0x1

    #@8
    return v0

    #@9
    .line 754
    :cond_9
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@b
    const-string v1, "CDMA connection index not assigned"

    #@d
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0
.end method

.method public bridge synthetic getCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getCall()Lcom/android/internal/telephony/cdma/CdmaCall;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getCall()Lcom/android/internal/telephony/cdma/CdmaCall;
    .registers 2

    #@0
    .prologue
    .line 312
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2
    return-object v0
.end method

.method public getCdnipNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 356
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cdnipNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getConnectTime()J
    .registers 3

    #@0
    .prologue
    .line 320
    iget-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->connectTime:J

    #@2
    return-wide v0
.end method

.method public getCreateTime()J
    .registers 3

    #@0
    .prologue
    .line 316
    iget-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->createTime:J

    #@2
    return-wide v0
.end method

.method public getDisconnectCause()Lcom/android/internal/telephony/Connection$DisconnectCause;
    .registers 2

    #@0
    .prologue
    .line 347
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    return-object v0
.end method

.method public getDisconnectTime()J
    .registers 3

    #@0
    .prologue
    .line 324
    iget-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->disconnectTime:J

    #@2
    return-wide v0
.end method

.method public getDurationMillis()J
    .registers 5

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 328
    iget-wide v2, p0, Lcom/android/internal/telephony/Connection;->connectTimeReal:J

    #@4
    cmp-long v2, v2, v0

    #@6
    if-nez v2, :cond_9

    #@8
    .line 333
    :goto_8
    return-wide v0

    #@9
    .line 330
    :cond_9
    iget-wide v2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->duration:J

    #@b
    cmp-long v0, v2, v0

    #@d
    if-nez v0, :cond_17

    #@f
    .line 331
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@12
    move-result-wide v0

    #@13
    iget-wide v2, p0, Lcom/android/internal/telephony/Connection;->connectTimeReal:J

    #@15
    sub-long/2addr v0, v2

    #@16
    goto :goto_8

    #@17
    .line 333
    :cond_17
    iget-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->duration:J

    #@19
    goto :goto_8
.end method

.method public getHoldDurationMillis()J
    .registers 5

    #@0
    .prologue
    .line 338
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getState()Lcom/android/internal/telephony/Call$State;

    #@3
    move-result-object v0

    #@4
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@6
    if-eq v0, v1, :cond_b

    #@8
    .line 340
    const-wide/16 v0, 0x0

    #@a
    .line 342
    :goto_a
    return-wide v0

    #@b
    :cond_b
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@e
    move-result-wide v0

    #@f
    iget-wide v2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->holdingStartTime:J

    #@11
    sub-long/2addr v0, v2

    #@12
    goto :goto_a
.end method

.method public getNumberPresentation()I
    .registers 2

    #@0
    .prologue
    .line 1134
    iget v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@2
    return v0
.end method

.method public getOrigDialString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 293
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->dialString:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPostDialState()Lcom/android/internal/telephony/Connection$PostDialState;
    .registers 2

    #@0
    .prologue
    .line 385
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2
    return-object v0
.end method

.method public getRedialNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 304
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->redialNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getRemainingPostDialString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 831
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@3
    sget-object v4, Lcom/android/internal/telephony/Connection$PostDialState;->CANCELLED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@5
    if-eq v3, v4, :cond_1b

    #@7
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@9
    sget-object v4, Lcom/android/internal/telephony/Connection$PostDialState;->COMPLETE:Lcom/android/internal/telephony/Connection$PostDialState;

    #@b
    if-eq v3, v4, :cond_1b

    #@d
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialString:Ljava/lang/String;

    #@f
    if-eqz v3, :cond_1b

    #@11
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialString:Ljava/lang/String;

    #@13
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@16
    move-result v3

    #@17
    iget v4, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->nextPostDialChar:I

    #@19
    if-gt v3, v4, :cond_1e

    #@1b
    .line 835
    :cond_1b
    const-string v1, ""

    #@1d
    .line 849
    :cond_1d
    :goto_1d
    return-object v1

    #@1e
    .line 838
    :cond_1e
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialString:Ljava/lang/String;

    #@20
    iget v4, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->nextPostDialChar:I

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    .line 839
    .local v1, subStr:Ljava/lang/String;
    if-eqz v1, :cond_1d

    #@28
    .line 840
    const/16 v3, 0x3b

    #@2a
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    #@2d
    move-result v2

    #@2e
    .line 841
    .local v2, wIndex:I
    const/16 v3, 0x2c

    #@30
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    #@33
    move-result v0

    #@34
    .line 843
    .local v0, pIndex:I
    if-lez v2, :cond_3f

    #@36
    if-lt v2, v0, :cond_3a

    #@38
    if-gtz v0, :cond_3f

    #@3a
    .line 844
    :cond_3a
    invoke-virtual {v1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    goto :goto_1d

    #@3f
    .line 845
    :cond_3f
    if-lez v0, :cond_1d

    #@41
    .line 846
    invoke-virtual {v1, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    goto :goto_1d
.end method

.method public getState()Lcom/android/internal/telephony/Call$State;
    .registers 2

    #@0
    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->disconnected:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 362
    sget-object v0, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@6
    .line 364
    :goto_6
    return-object v0

    #@7
    :cond_7
    invoke-super {p0}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    #@a
    move-result-object v0

    #@b
    goto :goto_6
.end method

.method public getUUSInfo()Lcom/android/internal/telephony/UUSInfo;
    .registers 2

    #@0
    .prologue
    .line 1140
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public hangup()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 369
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->disconnected:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 370
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@6
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangup(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@9
    .line 374
    return-void

    #@a
    .line 372
    :cond_a
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "disconnected"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0
.end method

.method public isIncoming()Z
    .registers 2

    #@0
    .prologue
    .line 351
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@2
    return v0
.end method

.method onConnectedInOrOut()V
    .registers 4

    #@0
    .prologue
    .line 763
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v0

    #@4
    iput-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->connectTime:J

    #@6
    .line 764
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@9
    move-result-wide v0

    #@a
    iput-wide v0, p0, Lcom/android/internal/telephony/Connection;->connectTimeReal:J

    #@c
    .line 765
    const-wide/16 v0, 0x0

    #@e
    iput-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->duration:J

    #@10
    .line 770
    new-instance v0, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v1, "onConnectedInOrOut: connectTime="

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget-wide v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->connectTime:J

    #@1d
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->log(Ljava/lang/String;)V

    #@28
    .line 773
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@2a
    if-nez v0, :cond_30

    #@2c
    .line 775
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->processNextPostDialChar()V

    #@2f
    .line 781
    :goto_2f
    return-void

    #@30
    .line 779
    :cond_30
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->releaseWakeLock()V

    #@33
    goto :goto_2f
.end method

.method onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V
    .registers 3
    .parameter "cause"

    #@0
    .prologue
    .line 559
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    .line 561
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->disconnected:Z

    #@4
    if-nez v0, :cond_19

    #@6
    .line 562
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->doDisconnect()V

    #@9
    .line 566
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@b
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@d
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyDisconnect(Lcom/android/internal/telephony/Connection;)V

    #@10
    .line 568
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@12
    if-eqz v0, :cond_19

    #@14
    .line 569
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@16
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/cdma/CdmaCall;->connectionDisconnected(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@19
    .line 572
    :cond_19
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->releaseWakeLock()V

    #@1c
    .line 573
    return-void
.end method

.method onHangupLocal()V
    .registers 2

    #@0
    .prologue
    .line 458
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4
    .line 459
    return-void
.end method

.method onHangupLocalMissed()V
    .registers 2

    #@0
    .prologue
    .line 464
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_MISSED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4
    .line 465
    return-void
.end method

.method onHangupRemoteKddi()V
    .registers 2

    #@0
    .prologue
    .line 1157
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4
    .line 1158
    return-void
.end method

.method onLocalDisconnect()V
    .registers 2

    #@0
    .prologue
    .line 578
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->disconnected:Z

    #@2
    if-nez v0, :cond_10

    #@4
    .line 579
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->doDisconnect()V

    #@7
    .line 583
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 584
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@d
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/cdma/CdmaCall;->detach(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@10
    .line 587
    :cond_10
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->releaseWakeLock()V

    #@13
    .line 588
    return-void
.end method

.method onRemoteDisconnect(I)V
    .registers 6
    .parameter "causeCode"

    #@0
    .prologue
    .line 540
    const/4 v1, 0x0

    #@1
    const-string v2, "support_network_change_auto_retry"

    #@3
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_55

    #@9
    .line 541
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getAddress()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_55

    #@13
    .line 542
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->disconnectCauseFromCode(I)Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@16
    move-result-object v0

    #@17
    .line 543
    .local v0, cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->NORMAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@19
    if-eq v0, v1, :cond_1f

    #@1b
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1d
    if-ne v0, v1, :cond_55

    #@1f
    .line 544
    :cond_1f
    new-instance v1, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v2, "cause = "

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v2, "getAddress() = "

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getAddress()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->log(Ljava/lang/String;)V

    #@43
    .line 546
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@45
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@47
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4e
    move-result-object v1

    #@4f
    const-string v2, "network_change_auto_retry"

    #@51
    const/4 v3, 0x0

    #@52
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@55
    .line 553
    .end local v0           #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    :cond_55
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->disconnectCauseFromCode(I)Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@58
    move-result-object v1

    #@59
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V

    #@5c
    .line 554
    return-void
.end method

.method onStartedHolding()V
    .registers 3

    #@0
    .prologue
    .line 793
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    iput-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->holdingStartTime:J

    #@6
    .line 794
    return-void
.end method

.method public proceedAfterWaitChar()V
    .registers 4

    #@0
    .prologue
    .line 389
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->WAIT:Lcom/android/internal/telephony/Connection$PostDialState;

    #@4
    if-eq v0, v1, :cond_21

    #@6
    .line 390
    const-string v0, "CDMA"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "CdmaConnection.proceedAfterWaitChar(): Expected getPostDialState() to be WAIT but was "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 398
    :goto_20
    return-void

    #@21
    .line 395
    :cond_21
    sget-object v0, Lcom/android/internal/telephony/Connection$PostDialState;->STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@23
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@26
    .line 397
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->processNextPostDialChar()V

    #@29
    goto :goto_20
.end method

.method public proceedAfterWildChar(Ljava/lang/String;)V
    .registers 6
    .parameter "str"

    #@0
    .prologue
    .line 401
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2
    sget-object v2, Lcom/android/internal/telephony/Connection$PostDialState;->WILD:Lcom/android/internal/telephony/Connection$PostDialState;

    #@4
    if-eq v1, v2, :cond_21

    #@6
    .line 402
    const-string v1, "CDMA"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "CdmaConnection.proceedAfterWaitChar(): Expected getPostDialState() to be WILD but was "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 445
    :goto_20
    return-void

    #@21
    .line 407
    :cond_21
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@23
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@26
    .line 434
    new-instance v0, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@2b
    .line 435
    .local v0, buf:Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialString:Ljava/lang/String;

    #@2d
    iget v2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->nextPostDialChar:I

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 436
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialString:Ljava/lang/String;

    #@3c
    .line 437
    const/4 v1, 0x0

    #@3d
    iput v1, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->nextPostDialChar:I

    #@3f
    .line 439
    new-instance v1, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v2, "proceedAfterWildChar: new postDialString is "

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialString:Ljava/lang/String;

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->log(Ljava/lang/String;)V

    #@57
    .line 443
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->processNextPostDialChar()V

    #@5a
    goto :goto_20
.end method

.method processNextPostDialChar()V
    .registers 10

    #@0
    .prologue
    .line 878
    const/4 v1, 0x0

    #@1
    .line 881
    .local v1, c:C
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@3
    sget-object v7, Lcom/android/internal/telephony/Connection$PostDialState;->CANCELLED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@5
    if-ne v6, v7, :cond_b

    #@7
    .line 882
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->releaseWakeLock()V

    #@a
    .line 931
    :cond_a
    :goto_a
    return-void

    #@b
    .line 887
    :cond_b
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialString:Ljava/lang/String;

    #@d
    if-eqz v6, :cond_19

    #@f
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialString:Ljava/lang/String;

    #@11
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@14
    move-result v6

    #@15
    iget v7, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->nextPostDialChar:I

    #@17
    if-gt v6, v7, :cond_40

    #@19
    .line 889
    :cond_19
    sget-object v6, Lcom/android/internal/telephony/Connection$PostDialState;->COMPLETE:Lcom/android/internal/telephony/Connection$PostDialState;

    #@1b
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@1e
    .line 892
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->releaseWakeLock()V

    #@21
    .line 895
    const/4 v1, 0x0

    #@22
    .line 914
    :cond_22
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@24
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@26
    iget-object v4, v6, Lcom/android/internal/telephony/cdma/CDMAPhone;->mPostDialHandler:Landroid/os/Registrant;

    #@28
    .line 918
    .local v4, postDialHandler:Landroid/os/Registrant;
    if-eqz v4, :cond_a

    #@2a
    invoke-virtual {v4}, Landroid/os/Registrant;->messageForRegistrant()Landroid/os/Message;

    #@2d
    move-result-object v3

    #@2e
    .local v3, notifyMessage:Landroid/os/Message;
    if-eqz v3, :cond_a

    #@30
    .line 921
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@32
    .line 922
    .local v5, state:Lcom/android/internal/telephony/Connection$PostDialState;
    invoke-static {v3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@35
    move-result-object v0

    #@36
    .line 923
    .local v0, ar:Landroid/os/AsyncResult;
    iput-object p0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@38
    .line 924
    iput-object v5, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@3a
    .line 927
    iput v1, v3, Landroid/os/Message;->arg1:I

    #@3c
    .line 929
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@3f
    goto :goto_a

    #@40
    .line 899
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v3           #notifyMessage:Landroid/os/Message;
    .end local v4           #postDialHandler:Landroid/os/Registrant;
    .end local v5           #state:Lcom/android/internal/telephony/Connection$PostDialState;
    :cond_40
    sget-object v6, Lcom/android/internal/telephony/Connection$PostDialState;->STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@42
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@45
    .line 901
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->postDialString:Ljava/lang/String;

    #@47
    iget v7, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->nextPostDialChar:I

    #@49
    add-int/lit8 v8, v7, 0x1

    #@4b
    iput v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->nextPostDialChar:I

    #@4d
    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    #@50
    move-result v1

    #@51
    .line 903
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->processPostDialChar(C)Z

    #@54
    move-result v2

    #@55
    .line 905
    .local v2, isValid:Z
    if-nez v2, :cond_22

    #@57
    .line 907
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->h:Landroid/os/Handler;

    #@59
    const/4 v7, 0x3

    #@5a
    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@5d
    move-result-object v6

    #@5e
    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    #@61
    .line 909
    const-string v6, "CDMA"

    #@63
    new-instance v7, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v8, "processNextPostDialChar: c="

    #@6a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@71
    move-result-object v7

    #@72
    const-string v8, " isn\'t valid!"

    #@74
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v7

    #@78
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v7

    #@7c
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    goto :goto_a
.end method

.method public separate()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->disconnected:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 378
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@6
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->separate(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@9
    .line 382
    return-void

    #@a
    .line 380
    :cond_a
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "disconnected"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0
.end method

.method update(Lcom/android/internal/telephony/DriverCall;)Z
    .registers 12
    .parameter "dc"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 594
    const/4 v0, 0x0

    #@3
    .line 595
    .local v0, changed:Z
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isConnectingInOrOut()Z

    #@6
    move-result v4

    #@7
    .line 596
    .local v4, wasConnectingInOrOut:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getState()Lcom/android/internal/telephony/Call$State;

    #@a
    move-result-object v8

    #@b
    sget-object v9, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@d
    if-ne v8, v9, :cond_168

    #@f
    move v5, v6

    #@10
    .line 598
    .local v5, wasHolding:Z
    :goto_10
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNumberInfoReceivedKddi:Z

    #@12
    if-eqz v8, :cond_16b

    #@14
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->presentationIndicatorsKddi:I

    #@16
    if-nez v8, :cond_16b

    #@18
    move v1, v6

    #@19
    .line 602
    .local v1, isPresentationAlloewdKddi:Z
    :goto_19
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@1b
    if-nez v8, :cond_33

    #@1d
    const-string v8, "KDDI"

    #@1f
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@22
    move-result v8

    #@23
    if-eqz v8, :cond_33

    #@25
    .line 603
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@27
    iget-object v8, v8, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@29
    invoke-virtual {v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@2c
    move-result-object v8

    #@2d
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@30
    move-result v8

    #@31
    if-eqz v8, :cond_16e

    #@33
    .line 618
    :cond_33
    :goto_33
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@35
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaConnection;->parentFromDCState(Lcom/android/internal/telephony/DriverCall$State;)Lcom/android/internal/telephony/cdma/CdmaCall;

    #@38
    move-result-object v2

    #@39
    .line 620
    .local v2, newParent:Lcom/android/internal/telephony/cdma/CdmaCall;
    new-instance v8, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v9, "parent= "

    #@40
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v8

    #@44
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@46
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v8

    #@4a
    const-string v9, ", newParent= "

    #@4c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v8

    #@50
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v8

    #@54
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v8

    #@58
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaConnection;->log(Ljava/lang/String;)V

    #@5b
    .line 622
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@5d
    iget-object v9, p1, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@5f
    invoke-static {v8, v9}, Lcom/android/internal/telephony/cdma/CdmaConnection;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@62
    move-result v8

    #@63
    if-nez v8, :cond_b3

    #@65
    .line 623
    const-string v8, "update: phone # changed!"

    #@67
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaConnection;->log(Ljava/lang/String;)V

    #@6a
    .line 625
    const-string v8, "KDDI"

    #@6c
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@6f
    move-result v8

    #@70
    if-eqz v8, :cond_1c0

    #@72
    .line 627
    new-instance v8, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v9, "update() isIncoming:"

    #@79
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    iget-boolean v9, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@7f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@82
    move-result-object v8

    #@83
    const-string v9, " isNumberInfoReceivedKddi:"

    #@85
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v8

    #@89
    iget-boolean v9, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNumberInfoReceivedKddi:Z

    #@8b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v8

    #@8f
    const-string v9, " isPresentationAlloewdKddi:"

    #@91
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v8

    #@95
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@98
    move-result-object v8

    #@99
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v8

    #@9d
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaConnection;->log(Ljava/lang/String;)V

    #@a0
    .line 630
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@a2
    iget-object v8, v8, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@a4
    invoke-virtual {v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@a7
    move-result-object v8

    #@a8
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@ab
    move-result v8

    #@ac
    if-eqz v8, :cond_190

    #@ae
    .line 631
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@b0
    iput-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@b2
    .line 632
    const/4 v0, 0x1

    #@b3
    .line 661
    :cond_b3
    :goto_b3
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->name:Ljava/lang/String;

    #@b5
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b8
    move-result v8

    #@b9
    if-eqz v8, :cond_1c7

    #@bb
    .line 662
    iget-object v8, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@bd
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c0
    move-result v8

    #@c1
    if-nez v8, :cond_c8

    #@c3
    .line 663
    const/4 v0, 0x1

    #@c4
    .line 664
    const-string v8, ""

    #@c6
    iput-object v8, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@c8
    .line 671
    :cond_c8
    :goto_c8
    new-instance v8, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v9, "--dssds----"

    #@cf
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v8

    #@d3
    iget-object v9, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@d5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v8

    #@d9
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v8

    #@dd
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaConnection;->log(Ljava/lang/String;)V

    #@e0
    .line 672
    iget v8, p1, Lcom/android/internal/telephony/DriverCall;->namePresentation:I

    #@e2
    iput v8, p0, Lcom/android/internal/telephony/Connection;->cnapNamePresentation:I

    #@e4
    .line 674
    const-string v8, "KDDI"

    #@e6
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@e9
    move-result v8

    #@ea
    if-eqz v8, :cond_1f8

    #@ec
    .line 675
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@ee
    iget-object v8, v8, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@f0
    invoke-virtual {v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@f3
    move-result-object v8

    #@f4
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@f7
    move-result v8

    #@f8
    if-eqz v8, :cond_1d8

    #@fa
    .line 676
    iget v8, p1, Lcom/android/internal/telephony/DriverCall;->numberPresentation:I

    #@fc
    iput v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@fe
    .line 693
    :cond_fe
    :goto_fe
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@100
    if-eq v2, v8, :cond_1fe

    #@102
    .line 694
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@104
    if-eqz v6, :cond_10b

    #@106
    .line 695
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@108
    invoke-virtual {v6, p0}, Lcom/android/internal/telephony/cdma/CdmaCall;->detach(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@10b
    .line 697
    :cond_10b
    invoke-virtual {v2, p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->attach(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/DriverCall;)V

    #@10e
    .line 698
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@110
    .line 699
    const/4 v0, 0x1

    #@111
    .line 708
    :goto_111
    new-instance v6, Ljava/lang/StringBuilder;

    #@113
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@116
    const-string v7, "Update, wasConnectingInOrOut="

    #@118
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v6

    #@11c
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v6

    #@120
    const-string v7, ", wasHolding="

    #@122
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v6

    #@126
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@129
    move-result-object v6

    #@12a
    const-string v7, ", isConnectingInOrOut="

    #@12c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v6

    #@130
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isConnectingInOrOut()Z

    #@133
    move-result v7

    #@134
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@137
    move-result-object v6

    #@138
    const-string v7, ", changed="

    #@13a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v6

    #@13e
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@141
    move-result-object v6

    #@142
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v6

    #@146
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaConnection;->log(Ljava/lang/String;)V

    #@149
    .line 715
    if-eqz v4, :cond_154

    #@14b
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isConnectingInOrOut()Z

    #@14e
    move-result v6

    #@14f
    if-nez v6, :cond_154

    #@151
    .line 716
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onConnectedInOrOut()V

    #@154
    .line 719
    :cond_154
    if-eqz v0, :cond_163

    #@156
    if-nez v5, :cond_163

    #@158
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getState()Lcom/android/internal/telephony/Call$State;

    #@15b
    move-result-object v6

    #@15c
    sget-object v7, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@15e
    if-ne v6, v7, :cond_163

    #@160
    .line 721
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onStartedHolding()V

    #@163
    .line 725
    :cond_163
    iget-object v6, p1, Lcom/android/internal/telephony/DriverCall;->cdnipNumber:Ljava/lang/String;

    #@165
    iput-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cdnipNumber:Ljava/lang/String;

    #@167
    .line 728
    return v0

    #@168
    .end local v1           #isPresentationAlloewdKddi:Z
    .end local v2           #newParent:Lcom/android/internal/telephony/cdma/CdmaCall;
    .end local v5           #wasHolding:Z
    :cond_168
    move v5, v7

    #@169
    .line 596
    goto/16 :goto_10

    #@16b
    .restart local v5       #wasHolding:Z
    :cond_16b
    move v1, v7

    #@16c
    .line 598
    goto/16 :goto_19

    #@16e
    .line 607
    .restart local v1       #isPresentationAlloewdKddi:Z
    :cond_16e
    const-string v8, "persist.radio.hwtest"

    #@170
    const-string v9, "false"

    #@172
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@175
    move-result-object v8

    #@176
    const-string v9, "true"

    #@178
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17b
    move-result v8

    #@17c
    if-nez v8, :cond_33

    #@17e
    .line 611
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@180
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->ACTIVE:Lcom/android/internal/telephony/DriverCall$State;

    #@182
    if-ne v8, v9, :cond_33

    #@184
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@186
    iget-boolean v8, v8, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsReceivedAocr:Z

    #@188
    if-nez v8, :cond_33

    #@18a
    .line 612
    sget-object v8, Lcom/android/internal/telephony/DriverCall$State;->ALERTING:Lcom/android/internal/telephony/DriverCall$State;

    #@18c
    iput-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@18e
    goto/16 :goto_33

    #@190
    .line 636
    .restart local v2       #newParent:Lcom/android/internal/telephony/cdma/CdmaCall;
    :cond_190
    const-string v8, "persist.radio.hwtest"

    #@192
    const-string v9, "false"

    #@194
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@197
    move-result-object v8

    #@198
    const-string v9, "true"

    #@19a
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19d
    move-result v8

    #@19e
    if-eqz v8, :cond_1a7

    #@1a0
    .line 637
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@1a2
    iput-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@1a4
    .line 638
    const/4 v0, 0x1

    #@1a5
    goto/16 :goto_b3

    #@1a7
    .line 642
    :cond_1a7
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@1a9
    if-eqz v8, :cond_1b1

    #@1ab
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNumberInfoReceivedKddi:Z

    #@1ad
    if-eqz v8, :cond_b3

    #@1af
    if-eqz v1, :cond_b3

    #@1b1
    .line 645
    :cond_1b1
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@1b3
    iput-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@1b5
    .line 647
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@1b7
    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    #@1ba
    move-result-object v8

    #@1bb
    iput-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@1bd
    .line 648
    const/4 v0, 0x1

    #@1be
    goto/16 :goto_b3

    #@1c0
    .line 653
    :cond_1c0
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@1c2
    iput-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@1c4
    .line 654
    const/4 v0, 0x1

    #@1c5
    goto/16 :goto_b3

    #@1c7
    .line 666
    :cond_1c7
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->name:Ljava/lang/String;

    #@1c9
    iget-object v9, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@1cb
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ce
    move-result v8

    #@1cf
    if-nez v8, :cond_c8

    #@1d1
    .line 667
    const/4 v0, 0x1

    #@1d2
    .line 668
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->name:Ljava/lang/String;

    #@1d4
    iput-object v8, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@1d6
    goto/16 :goto_c8

    #@1d8
    .line 680
    :cond_1d8
    const-string v8, "persist.radio.hwtest"

    #@1da
    const-string v9, "false"

    #@1dc
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1df
    move-result-object v8

    #@1e0
    const-string v9, "true"

    #@1e2
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e5
    move-result v8

    #@1e6
    if-eqz v8, :cond_1ee

    #@1e8
    .line 681
    iget v8, p1, Lcom/android/internal/telephony/DriverCall;->numberPresentation:I

    #@1ea
    iput v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@1ec
    goto/16 :goto_fe

    #@1ee
    .line 685
    :cond_1ee
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@1f0
    if-nez v8, :cond_fe

    #@1f2
    .line 686
    iget v8, p1, Lcom/android/internal/telephony/DriverCall;->numberPresentation:I

    #@1f4
    iput v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@1f6
    goto/16 :goto_fe

    #@1f8
    .line 691
    :cond_1f8
    iget v8, p1, Lcom/android/internal/telephony/DriverCall;->numberPresentation:I

    #@1fa
    iput v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@1fc
    goto/16 :goto_fe

    #@1fe
    .line 702
    :cond_1fe
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@200
    invoke-virtual {v8, p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->update(Lcom/android/internal/telephony/cdma/CdmaConnection;Lcom/android/internal/telephony/DriverCall;)Z

    #@203
    move-result v3

    #@204
    .line 703
    .local v3, parentStateChange:Z
    if-nez v0, :cond_208

    #@206
    if-eqz v3, :cond_20b

    #@208
    :cond_208
    move v0, v6

    #@209
    :goto_209
    goto/16 :goto_111

    #@20b
    :cond_20b
    move v0, v7

    #@20c
    goto :goto_209
.end method

.method public updateParent(Lcom/android/internal/telephony/cdma/CdmaCall;Lcom/android/internal/telephony/cdma/CdmaCall;)V
    .registers 4
    .parameter "oldParent"
    .parameter "newParent"

    #@0
    .prologue
    .line 853
    if-eq p2, p1, :cond_e

    #@2
    .line 854
    if-eqz p1, :cond_7

    #@4
    .line 855
    invoke-virtual {p1, p0}, Lcom/android/internal/telephony/cdma/CdmaCall;->detach(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@7
    .line 857
    :cond_7
    sget-object v0, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@9
    invoke-virtual {p2, p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->attachFake(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/Call$State;)V

    #@c
    .line 858
    iput-object p2, p0, Lcom/android/internal/telephony/cdma/CdmaConnection;->parent:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@e
    .line 860
    :cond_e
    return-void
.end method
