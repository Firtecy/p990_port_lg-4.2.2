.class public Lcom/android/internal/telephony/IccSmsInterfaceManager;
.super Lcom/android/internal/telephony/ISms$Stub;
.source "IccSmsInterfaceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;,
        Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;
    }
.end annotation


# static fields
.field protected static final DBG:Z = true

.field private static final EVENT_COPY_SMS_DONE:I = 0x6

.field private static final EVENT_GET_MAX_EF_SMS:I = 0x7d

.field private static final EVENT_GET_SMSCADDRESS:I = 0x5

.field private static final EVENT_ITEMREADCHECK_LOAD_DONE:I = 0x80

.field private static final EVENT_LOAD_DONE:I = 0x1

.field private static final EVENT_SET_BROADCAST_ACTIVATION_DONE:I = 0x3

.field private static final EVENT_SET_BROADCAST_CONFIG_DONE:I = 0x4

.field private static final EVENT_UPDATE_DONE:I = 0x2

.field private static final EVENT_UPDATE_SMS_STATUS_READ_DONE:I = 0x7e

.field private static final EVENT_UPDATE_SMS_STATUS_UPDATE_DONE:I = 0x7f

.field protected static final LOG_TAG:Ljava/lang/String; = "RIL_IccSms"

.field private static final SMS_CB_CODE_SCHEME_MAX:I = 0xff

.field private static final SMS_CB_CODE_SCHEME_MIN:I = 0x0

.field private static final SMS_FORMAT_CSIM:I = 0x2

.field private static final SMS_FORMAT_USIM:I = 0x1


# instance fields
.field private mCdmaBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;

.field private mCellBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;

.field protected mContext:Landroid/content/Context;

.field protected mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

.field mHandler:Landroid/os/Handler;

.field private mImsDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

.field private mIndexOnIcc:I

.field private mItemNumber:I

.field private final mLock:Ljava/lang/Object;

.field private mMessages:Ljava/util/ArrayList;

.field protected mPhone:Lcom/android/internal/telephony/PhoneBase;

.field private mResult:I

.field private mSCAddr:Ljava/lang/String;

.field private mSms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/SmsRawData;",
            ">;"
        }
    .end annotation
.end field

.field private mSuccess:Z

.field private mUiccType:I

.field private mValidityperiodvalue:I

.field private recordSize:I


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/PhoneBase;)V
    .registers 4
    .parameter "phone"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 346
    invoke-direct {p0}, Lcom/android/internal/telephony/ISms$Stub;-><init>()V

    #@4
    .line 66
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@b
    .line 70
    new-instance v0, Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;

    #@d
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;-><init>(Lcom/android/internal/telephony/IccSmsInterfaceManager;)V

    #@10
    iput-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mCellBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;

    #@12
    .line 72
    new-instance v0, Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;

    #@14
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;-><init>(Lcom/android/internal/telephony/IccSmsInterfaceManager;)V

    #@17
    iput-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mCdmaBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;

    #@19
    .line 105
    iput v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mIndexOnIcc:I

    #@1b
    .line 107
    iput v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mResult:I

    #@1d
    .line 124
    new-instance v0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;

    #@1f
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;-><init>(Lcom/android/internal/telephony/IccSmsInterfaceManager;)V

    #@22
    iput-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@24
    .line 348
    iput-object p1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@26
    .line 349
    invoke-virtual {p1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@29
    move-result-object v0

    #@2a
    iput-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@2c
    .line 350
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->initDispatchers()V

    #@2f
    .line 351
    const-string v0, "isms"

    #@31
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@34
    move-result-object v0

    #@35
    if-nez v0, :cond_3c

    #@37
    .line 352
    const-string v0, "isms"

    #@39
    invoke-static {v0, p0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@3c
    .line 354
    :cond_3c
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/telephony/IccSmsInterfaceManager;[Lcom/android/internal/telephony/gsm/SmsBroadcastConfigInfo;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->setCellBroadcastConfig([Lcom/android/internal/telephony/gsm/SmsBroadcastConfigInfo;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$102(Lcom/android/internal/telephony/IccSmsInterfaceManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/internal/telephony/IccSmsInterfaceManager;[Lcom/android/internal/telephony/cdma/CdmaSmsBroadcastConfigInfo;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->setCdmaBroadcastConfig([Lcom/android/internal/telephony/cdma/CdmaSmsBroadcastConfigInfo;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/android/internal/telephony/IccSmsInterfaceManager;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/IccSmsInterfaceManager;Ljava/util/ArrayList;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->markMessagesAsRead(Ljava/util/ArrayList;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/IccSmsInterfaceManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mItemNumber:I

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/IccSmsInterfaceManager;Ljava/util/ArrayList;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->markMessagesAsRead(Ljava/util/ArrayList;I)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSCAddr:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Lcom/android/internal/telephony/IccSmsInterfaceManager;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSCAddr:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$700(Lcom/android/internal/telephony/IccSmsInterfaceManager;[BILjava/lang/Boolean;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->updateSmsOnSimReadStatusWrite([BILjava/lang/Boolean;)V

    #@3
    return-void
.end method

.method static synthetic access$802(Lcom/android/internal/telephony/IccSmsInterfaceManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput p1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->recordSize:I

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/android/internal/telephony/IccSmsInterfaceManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mIndexOnIcc:I

    #@2
    return v0
.end method

.method static synthetic access$902(Lcom/android/internal/telephony/IccSmsInterfaceManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput p1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mIndexOnIcc:I

    #@2
    return p1
.end method

.method private isCdmaPhoneType()Z
    .registers 3

    #@0
    .prologue
    .line 1654
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "isCdmaPhoneType(), Current Phone Type = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@10
    move-result v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1c
    .line 1655
    const/4 v0, 0x2

    #@1d
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1f
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@22
    move-result v1

    #@23
    if-ne v0, v1, :cond_27

    #@25
    const/4 v0, 0x1

    #@26
    :goto_26
    return v0

    #@27
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_26
.end method

.method private markMessagesAsRead(Ljava/util/ArrayList;)V
    .registers 15
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    const/4 v4, 0x0

    #@1
    const/4 v12, 0x1

    #@2
    const/4 v11, 0x0

    #@3
    .line 280
    if-nez p1, :cond_6

    #@5
    .line 306
    :cond_5
    :goto_5
    return-void

    #@6
    .line 285
    :cond_6
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@b
    move-result-object v0

    #@c
    .line 286
    .local v0, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-nez v0, :cond_14

    #@e
    .line 289
    const-string v1, "markMessagesAsRead(), aborting, no icc card present."

    #@10
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@13
    goto :goto_5

    #@14
    .line 293
    :cond_14
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@17
    move-result v7

    #@18
    .line 295
    .local v7, count:I
    const/4 v8, 0x0

    #@19
    .local v8, i:I
    :goto_19
    if-ge v8, v7, :cond_5

    #@1b
    .line 296
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v6

    #@1f
    check-cast v6, [B

    #@21
    .line 297
    .local v6, ba:[B
    aget-byte v1, v6, v11

    #@23
    const/4 v2, 0x3

    #@24
    if-ne v1, v2, :cond_5a

    #@26
    .line 298
    array-length v9, v6

    #@27
    .line 299
    .local v9, n:I
    add-int/lit8 v1, v9, -0x1

    #@29
    new-array v10, v1, [B

    #@2b
    .line 300
    .local v10, nba:[B
    add-int/lit8 v1, v9, -0x1

    #@2d
    invoke-static {v6, v12, v10, v11, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@30
    .line 301
    invoke-virtual {p0, v12, v10}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->makeSmsRecordData(I[B)[B

    #@33
    move-result-object v3

    #@34
    .line 302
    .local v3, record:[B
    const/16 v1, 0x6f3c

    #@36
    add-int/lit8 v2, v8, 0x1

    #@38
    move-object v5, v4

    #@39
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFLinearFixed(II[BLjava/lang/String;Landroid/os/Message;)V

    #@3c
    .line 303
    new-instance v1, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v2, "SMS "

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    add-int/lit8 v2, v8, 0x1

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    const-string v2, " marked as read"

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@5a
    .line 295
    .end local v3           #record:[B
    .end local v9           #n:I
    .end local v10           #nba:[B
    :cond_5a
    add-int/lit8 v8, v8, 0x1

    #@5c
    goto :goto_19
.end method

.method private markMessagesAsRead(Ljava/util/ArrayList;I)V
    .registers 15
    .parameter
    .parameter "ItemNumber"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;I)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    const/4 v4, 0x0

    #@1
    const/4 v11, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 310
    if-nez p1, :cond_6

    #@5
    .line 340
    :cond_5
    :goto_5
    return-void

    #@6
    .line 315
    :cond_6
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@b
    move-result-object v0

    #@c
    .line 316
    .local v0, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-nez v0, :cond_14

    #@e
    .line 319
    const-string v1, "markMessagesAsRead(), markMessagesAsRead with ItemNumber - aborting, no icc card present."

    #@10
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@13
    goto :goto_5

    #@14
    .line 323
    :cond_14
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@17
    move-result v7

    #@18
    .line 325
    .local v7, count:I
    const/4 v8, 0x0

    #@19
    .local v8, i:I
    :goto_19
    if-ge v8, v7, :cond_5

    #@1b
    .line 326
    add-int/lit8 v1, p2, -0x1

    #@1d
    if-ne v8, v1, :cond_5f

    #@1f
    .line 327
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@22
    move-result-object v6

    #@23
    check-cast v6, [B

    #@25
    .line 328
    .local v6, ba:[B
    aget-byte v1, v6, v5

    #@27
    const/4 v2, 0x3

    #@28
    if-ne v1, v2, :cond_5

    #@2a
    .line 329
    array-length v9, v6

    #@2b
    .line 330
    .local v9, n:I
    add-int/lit8 v1, v9, -0x1

    #@2d
    new-array v10, v1, [B

    #@2f
    .line 331
    .local v10, nba:[B
    add-int/lit8 v1, v9, -0x1

    #@31
    invoke-static {v6, v11, v10, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@34
    .line 332
    invoke-virtual {p0, v11, v10}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->makeSmsRecordData(I[B)[B

    #@37
    move-result-object v3

    #@38
    .line 333
    .local v3, record:[B
    const/16 v1, 0x6f3c

    #@3a
    add-int/lit8 v2, v8, 0x1

    #@3c
    move-object v5, v4

    #@3d
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFLinearFixed(II[BLjava/lang/String;Landroid/os/Message;)V

    #@40
    .line 335
    new-instance v1, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v2, "SMS "

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    add-int/lit8 v2, v8, 0x1

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    const-string v2, " marked as read with ItemNumber"

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@5e
    goto :goto_5

    #@5f
    .line 325
    .end local v3           #record:[B
    .end local v6           #ba:[B
    .end local v9           #n:I
    .end local v10           #nba:[B
    :cond_5f
    add-int/lit8 v8, v8, 0x1

    #@61
    goto :goto_19
.end method

.method private setCdmaBroadcastActivation(Z)Z
    .registers 7
    .parameter "activate"

    #@0
    .prologue
    .line 1502
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "Calling setCdmaBroadcastActivation("

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    const-string v3, ")"

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@1c
    .line 1504
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@1e
    monitor-enter v3

    #@1f
    .line 1505
    :try_start_1f
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@21
    const/4 v4, 0x3

    #@22
    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@25
    move-result-object v1

    #@26
    .line 1507
    .local v1, response:Landroid/os/Message;
    const/4 v2, 0x0

    #@27
    iput-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@29
    .line 1508
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2b
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2d
    invoke-interface {v2, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->setCdmaBroadcastActivation(ZLandroid/os/Message;)V
    :try_end_30
    .catchall {:try_start_1f .. :try_end_30} :catchall_40

    #@30
    .line 1511
    :try_start_30
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@32
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_35
    .catchall {:try_start_30 .. :try_end_35} :catchall_40
    .catch Ljava/lang/InterruptedException; {:try_start_30 .. :try_end_35} :catch_39

    #@35
    .line 1515
    :goto_35
    :try_start_35
    monitor-exit v3
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_40

    #@36
    .line 1517
    iget-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@38
    return v2

    #@39
    .line 1512
    :catch_39
    move-exception v0

    #@3a
    .line 1513
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_3a
    const-string v2, "interrupted while trying to set cdma broadcast activation"

    #@3c
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@3f
    goto :goto_35

    #@40
    .line 1515
    .end local v0           #e:Ljava/lang/InterruptedException;
    .end local v1           #response:Landroid/os/Message;
    :catchall_40
    move-exception v2

    #@41
    monitor-exit v3
    :try_end_42
    .catchall {:try_start_3a .. :try_end_42} :catchall_40

    #@42
    throw v2
.end method

.method private setCdmaBroadcastConfig([Lcom/android/internal/telephony/cdma/CdmaSmsBroadcastConfigInfo;)Z
    .registers 7
    .parameter "configs"

    #@0
    .prologue
    .line 1482
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "Calling setCdmaBroadcastConfig with "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    array-length v3, p1

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    const-string v3, " configurations"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@1d
    .line 1484
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@1f
    monitor-enter v3

    #@20
    .line 1485
    :try_start_20
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@22
    const/4 v4, 0x4

    #@23
    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@26
    move-result-object v1

    #@27
    .line 1487
    .local v1, response:Landroid/os/Message;
    const/4 v2, 0x0

    #@28
    iput-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@2a
    .line 1488
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2c
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2e
    invoke-interface {v2, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->setCdmaBroadcastConfig([Lcom/android/internal/telephony/cdma/CdmaSmsBroadcastConfigInfo;Landroid/os/Message;)V
    :try_end_31
    .catchall {:try_start_20 .. :try_end_31} :catchall_41

    #@31
    .line 1491
    :try_start_31
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@33
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_36
    .catchall {:try_start_31 .. :try_end_36} :catchall_41
    .catch Ljava/lang/InterruptedException; {:try_start_31 .. :try_end_36} :catch_3a

    #@36
    .line 1495
    :goto_36
    :try_start_36
    monitor-exit v3
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_41

    #@37
    .line 1497
    iget-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@39
    return v2

    #@3a
    .line 1492
    :catch_3a
    move-exception v0

    #@3b
    .line 1493
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_3b
    const-string v2, "interrupted while trying to set cdma broadcast config"

    #@3d
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@40
    goto :goto_36

    #@41
    .line 1495
    .end local v0           #e:Ljava/lang/InterruptedException;
    .end local v1           #response:Landroid/os/Message;
    :catchall_41
    move-exception v2

    #@42
    monitor-exit v3
    :try_end_43
    .catchall {:try_start_3b .. :try_end_43} :catchall_41

    #@43
    throw v2
.end method

.method private setCellBroadcastActivation(Z)Z
    .registers 7
    .parameter "activate"

    #@0
    .prologue
    .line 1461
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "Calling setCellBroadcastActivation("

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    const/16 v3, 0x29

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@1c
    .line 1463
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@1e
    monitor-enter v3

    #@1f
    .line 1464
    :try_start_1f
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@21
    const/4 v4, 0x3

    #@22
    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@25
    move-result-object v1

    #@26
    .line 1466
    .local v1, response:Landroid/os/Message;
    const/4 v2, 0x0

    #@27
    iput-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@29
    .line 1467
    const-string v2, "setCellBroadcastActivation(), IccSmsInterfaceManager --> RIL"

    #@2b
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@2e
    .line 1468
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@30
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@32
    invoke-interface {v2, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->setGsmBroadcastActivation(ZLandroid/os/Message;)V
    :try_end_35
    .catchall {:try_start_1f .. :try_end_35} :catchall_45

    #@35
    .line 1471
    :try_start_35
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@37
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_3a
    .catchall {:try_start_35 .. :try_end_3a} :catchall_45
    .catch Ljava/lang/InterruptedException; {:try_start_35 .. :try_end_3a} :catch_3e

    #@3a
    .line 1475
    :goto_3a
    :try_start_3a
    monitor-exit v3
    :try_end_3b
    .catchall {:try_start_3a .. :try_end_3b} :catchall_45

    #@3b
    .line 1477
    iget-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@3d
    return v2

    #@3e
    .line 1472
    :catch_3e
    move-exception v0

    #@3f
    .line 1473
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_3f
    const-string v2, "interrupted while trying to set cell broadcast activation"

    #@41
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@44
    goto :goto_3a

    #@45
    .line 1475
    .end local v0           #e:Ljava/lang/InterruptedException;
    .end local v1           #response:Landroid/os/Message;
    :catchall_45
    move-exception v2

    #@46
    monitor-exit v3
    :try_end_47
    .catchall {:try_start_3f .. :try_end_47} :catchall_45

    #@47
    throw v2
.end method

.method private setCellBroadcastConfig([Lcom/android/internal/telephony/gsm/SmsBroadcastConfigInfo;)Z
    .registers 7
    .parameter "configs"

    #@0
    .prologue
    .line 1440
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "Calling setGsmBroadcastConfig with "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    array-length v3, p1

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    const-string v3, " configurations"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@1d
    .line 1442
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@1f
    monitor-enter v3

    #@20
    .line 1443
    :try_start_20
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@22
    const/4 v4, 0x4

    #@23
    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@26
    move-result-object v1

    #@27
    .line 1445
    .local v1, response:Landroid/os/Message;
    const/4 v2, 0x0

    #@28
    iput-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@2a
    .line 1446
    const-string v2, "setCellBroadcastConfig(), IccSmsInterfaceManager --> RIL"

    #@2c
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@2f
    .line 1447
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@31
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@33
    invoke-interface {v2, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->setGsmBroadcastConfig([Lcom/android/internal/telephony/gsm/SmsBroadcastConfigInfo;Landroid/os/Message;)V
    :try_end_36
    .catchall {:try_start_20 .. :try_end_36} :catchall_46

    #@36
    .line 1450
    :try_start_36
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@38
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_3b
    .catchall {:try_start_36 .. :try_end_3b} :catchall_46
    .catch Ljava/lang/InterruptedException; {:try_start_36 .. :try_end_3b} :catch_3f

    #@3b
    .line 1454
    :goto_3b
    :try_start_3b
    monitor-exit v3
    :try_end_3c
    .catchall {:try_start_3b .. :try_end_3c} :catchall_46

    #@3c
    .line 1456
    iget-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@3e
    return v2

    #@3f
    .line 1451
    :catch_3f
    move-exception v0

    #@40
    .line 1452
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_40
    const-string v2, "interrupted while trying to set cell broadcast config"

    #@42
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@45
    goto :goto_3b

    #@46
    .line 1454
    .end local v0           #e:Ljava/lang/InterruptedException;
    .end local v1           #response:Landroid/os/Message;
    :catchall_46
    move-exception v2

    #@47
    monitor-exit v3
    :try_end_48
    .catchall {:try_start_40 .. :try_end_48} :catchall_46

    #@48
    throw v2
.end method

.method private updateSmsOnSimReadStatusWrite([BILjava/lang/Boolean;)V
    .registers 10
    .parameter "data"
    .parameter "index"
    .parameter "read"

    #@0
    .prologue
    .line 1612
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0x7f

    #@4
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v5

    #@8
    .line 1613
    .local v5, response:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@d
    move-result-object v0

    #@e
    .line 1615
    .local v0, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-nez v0, :cond_16

    #@10
    .line 1616
    const-string v1, "updateSmsOnSimReadStatusWrite(), Cannot load Sms records. No icc card?"

    #@12
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@15
    .line 1621
    :goto_15
    return-void

    #@16
    .line 1619
    :cond_16
    const/4 v2, 0x0

    #@17
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_2a

    #@1d
    const/4 v1, 0x1

    #@1e
    :goto_1e
    int-to-byte v1, v1

    #@1f
    aput-byte v1, p1, v2

    #@21
    .line 1620
    const/16 v1, 0x6f3c

    #@23
    const/4 v4, 0x0

    #@24
    move v2, p2

    #@25
    move-object v3, p1

    #@26
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFLinearFixed(II[BLjava/lang/String;Landroid/os/Message;)V

    #@29
    goto :goto_15

    #@2a
    .line 1619
    :cond_2a
    const/4 v1, 0x3

    #@2b
    goto :goto_1e
.end method


# virtual methods
.method public SendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .registers 19
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .parameter "cbAddress"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 948
    .local p3, parts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.SEND_SMS"

    #@4
    const-string v3, "Sending SMS message"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 951
    const-string v1, "SMS"

    #@b
    const/4 v2, 0x2

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_67

    #@12
    .line 952
    const/4 v8, 0x0

    #@13
    .line 953
    .local v8, i:I
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v10

    #@17
    .local v10, i$:Ljava/util/Iterator;
    :goto_17
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_67

    #@1d
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v11

    #@21
    check-cast v11, Ljava/lang/String;

    #@23
    .line 954
    .local v11, part:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v2, "sendMultipartText: destAddr="

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    const-string v2, ", srAddr="

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    const-string v2, ", part["

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    add-int/lit8 v9, v8, 0x1

    #@44
    .end local v8           #i:I
    .local v9, i:I
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    const-string v2, "]="

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    const-string v2, ", cbAddress="

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    move-object/from16 v0, p6

    #@5a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@65
    move v8, v9

    #@66
    .end local v9           #i:I
    .restart local v8       #i:I
    goto :goto_17

    #@67
    .line 958
    .end local v8           #i:I
    .end local v10           #i$:Ljava/util/Iterator;
    .end local v11           #part:Ljava/lang/String;
    :cond_67
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@69
    move-object v4, p3

    #@6a
    check-cast v4, Ljava/util/ArrayList;

    #@6c
    move-object/from16 v5, p4

    #@6e
    check-cast v5, Ljava/util/ArrayList;

    #@70
    move-object/from16 v6, p5

    #@72
    check-cast v6, Ljava/util/ArrayList;

    #@74
    move-object v2, p1

    #@75
    move-object v3, p2

    #@76
    move-object/from16 v7, p6

    #@78
    invoke-virtual/range {v1 .. v7}, Lcom/android/internal/telephony/SMSDispatcher;->SendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    #@7b
    .line 960
    return-void
.end method

.method public SendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 14
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 874
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    const-string v1, "android.permission.SEND_SMS"

    #@8
    const-string v2, "Sending SMS message"

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 877
    const-string v0, "SMS"

    #@f
    const/4 v1, 0x2

    #@10
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_5e

    #@16
    .line 878
    new-instance v0, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v1, "sendText: destAddr="

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    const-string v1, " scAddr="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, " text=\'"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    const-string v1, "\' sentIntent="

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    const-string v1, " deliveryIntent="

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, " cbAddress="

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@5e
    .line 882
    :cond_5e
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@60
    move-object v1, p1

    #@61
    move-object v2, p2

    #@62
    move-object v3, p3

    #@63
    move-object v4, p4

    #@64
    move-object v5, p5

    #@65
    move-object v6, p6

    #@66
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/SMSDispatcher;->SendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V

    #@69
    .line 883
    return-void
.end method

.method protected buildValidRawData(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/SmsRawData;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1076
    .local p1, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v1

    #@4
    .line 1079
    .local v1, count:I
    new-instance v3, Ljava/util/ArrayList;

    #@6
    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@9
    .line 1081
    .local v3, ret:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/SmsRawData;>;"
    const/4 v2, 0x0

    #@a
    .local v2, i:I
    :goto_a
    if-ge v2, v1, :cond_2d

    #@c
    .line 1082
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, [B

    #@12
    .line 1083
    .local v0, ba:[B
    const/4 v4, 0x0

    #@13
    aget-byte v4, v0, v4

    #@15
    if-nez v4, :cond_1e

    #@17
    .line 1084
    const/4 v4, 0x0

    #@18
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1b
    .line 1081
    :goto_1b
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_a

    #@1e
    .line 1086
    :cond_1e
    new-instance v5, Lcom/android/internal/telephony/SmsRawData;

    #@20
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@23
    move-result-object v4

    #@24
    check-cast v4, [B

    #@26
    invoke-direct {v5, v4}, Lcom/android/internal/telephony/SmsRawData;-><init>([B)V

    #@29
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2c
    goto :goto_1b

    #@2d
    .line 1090
    .end local v0           #ba:[B
    :cond_2d
    return-object v3
.end method

.method public copyMessageToIccEf(I[B[B)Z
    .registers 10
    .parameter "status"
    .parameter "pdu"
    .parameter "smsc"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 552
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@3
    const-string v3, "hide_privacy_log"

    #@5
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8
    move-result v2

    #@9
    if-ne v2, v5, :cond_97

    #@b
    .line 553
    const-string v2, "1"

    #@d
    const-string v3, "persist.service.privacy.enable"

    #@f
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_57

    #@19
    .line 554
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v3, "copyMessageToIccEf: status="

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    const-string v3, " ==> "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    const-string v3, "pdu=("

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-static {p2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    const-string v3, "), smsm=("

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-static {p3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    const-string v3, ")"

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@57
    .line 564
    :cond_57
    :goto_57
    const-string v2, "Copying message to Icc"

    #@59
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->enforceReceiveAndSend(Ljava/lang/String;)V

    #@5c
    .line 565
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@5e
    monitor-enter v3

    #@5f
    .line 566
    const/4 v2, 0x0

    #@60
    :try_start_60
    iput-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@62
    .line 567
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@64
    const/4 v4, 0x2

    #@65
    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@68
    move-result-object v1

    #@69
    .line 571
    .local v1, response:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@6b
    const-string v4, "control_uicc_storage"

    #@6d
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@70
    move-result v2

    #@71
    if-eqz v2, :cond_ee

    #@73
    .line 572
    const-string v2, "copyMessageToIccEf(), KEY_CONTROL_UICC_STORAGE is Defined"

    #@75
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@78
    .line 573
    invoke-direct {p0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->isCdmaPhoneType()Z

    #@7b
    move-result v2

    #@7c
    if-eqz v2, :cond_d6

    #@7e
    .line 574
    const-string v2, "copyMessageToIccEf(), PhoneType: CDMA Phone -> writeSmsToRuim"

    #@80
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@83
    .line 575
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@85
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@87
    invoke-static {p2}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@8a
    move-result-object v4

    #@8b
    invoke-interface {v2, p1, v4, v1}, Lcom/android/internal/telephony/CommandsInterface;->writeSmsToRuim(ILjava/lang/String;Landroid/os/Message;)V
    :try_end_8e
    .catchall {:try_start_60 .. :try_end_8e} :catchall_eb

    #@8e
    .line 598
    :goto_8e
    :try_start_8e
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@90
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_93
    .catchall {:try_start_8e .. :try_end_93} :catchall_eb
    .catch Ljava/lang/InterruptedException; {:try_start_8e .. :try_end_93} :catch_123

    #@93
    .line 602
    :goto_93
    :try_start_93
    monitor-exit v3
    :try_end_94
    .catchall {:try_start_93 .. :try_end_94} :catchall_eb

    #@94
    .line 603
    iget-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@96
    return v2

    #@97
    .line 559
    .end local v1           #response:Landroid/os/Message;
    :cond_97
    new-instance v2, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v3, "copyMessageToIccEf: status="

    #@9e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v2

    #@a2
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v2

    #@a6
    const-string v3, " ==> "

    #@a8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v2

    #@ac
    const-string v3, "pdu=("

    #@ae
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v2

    #@b2
    invoke-static {p2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@b5
    move-result-object v3

    #@b6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v2

    #@ba
    const-string v3, "), smsm=("

    #@bc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v2

    #@c0
    invoke-static {p3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@c3
    move-result-object v3

    #@c4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v2

    #@c8
    const-string v3, ")"

    #@ca
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v2

    #@ce
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d1
    move-result-object v2

    #@d2
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@d5
    goto :goto_57

    #@d6
    .line 577
    .restart local v1       #response:Landroid/os/Message;
    :cond_d6
    :try_start_d6
    const-string v2, "copyMessageToIccEf(), PhoneType: GSM Phone -> writeSmsToSim"

    #@d8
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@db
    .line 578
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@dd
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@df
    invoke-static {p3}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@e2
    move-result-object v4

    #@e3
    invoke-static {p2}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@e6
    move-result-object v5

    #@e7
    invoke-interface {v2, p1, v4, v5, v1}, Lcom/android/internal/telephony/CommandsInterface;->writeSmsToSim(ILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@ea
    goto :goto_8e

    #@eb
    .line 602
    .end local v1           #response:Landroid/os/Message;
    :catchall_eb
    move-exception v2

    #@ec
    monitor-exit v3
    :try_end_ed
    .catchall {:try_start_d6 .. :try_end_ed} :catchall_eb

    #@ed
    throw v2

    #@ee
    .line 584
    .restart local v1       #response:Landroid/os/Message;
    :cond_ee
    :try_start_ee
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f0
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@f3
    move-result v2

    #@f4
    if-eq v5, v2, :cond_100

    #@f6
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@f8
    const-string v4, "save_usim_3gpp_in_cdma"

    #@fa
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@fd
    move-result v2

    #@fe
    if-ne v2, v5, :cond_116

    #@100
    .line 587
    :cond_100
    const-string v2, "copyMessageToIccEf(), IccSmsInterfaceManager --> RIL"

    #@102
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@105
    .line 589
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@107
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@109
    invoke-static {p3}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@10c
    move-result-object v4

    #@10d
    invoke-static {p2}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@110
    move-result-object v5

    #@111
    invoke-interface {v2, p1, v4, v5, v1}, Lcom/android/internal/telephony/CommandsInterface;->writeSmsToSim(ILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@114
    goto/16 :goto_8e

    #@116
    .line 592
    :cond_116
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@118
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@11a
    invoke-static {p2}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@11d
    move-result-object v4

    #@11e
    invoke-interface {v2, p1, v4, v1}, Lcom/android/internal/telephony/CommandsInterface;->writeSmsToRuim(ILjava/lang/String;Landroid/os/Message;)V

    #@121
    goto/16 :goto_8e

    #@123
    .line 599
    :catch_123
    move-exception v0

    #@124
    .line 600
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v2, "interrupted while trying to update by index"

    #@126
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V
    :try_end_129
    .catchall {:try_start_ee .. :try_end_129} :catchall_eb

    #@129
    goto/16 :goto_93
.end method

.method public copySmsToIccEf(I[B[B)I
    .registers 10
    .parameter "status"
    .parameter "pdu"
    .parameter "smsc"

    #@0
    .prologue
    .line 1544
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "copyMessageToIccEf: status="

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    const-string v3, " ==> "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "pdu=("

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {p2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, "), smsm=("

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-static {p3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    const-string v3, ")"

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@3e
    .line 1549
    const-string v2, "Copying sms to UIcc"

    #@40
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->enforceReceiveAndSend(Ljava/lang/String;)V

    #@43
    .line 1550
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@45
    monitor-enter v3

    #@46
    .line 1551
    const/4 v2, 0x0

    #@47
    :try_start_47
    iput-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@49
    .line 1552
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@4b
    const/4 v4, 0x6

    #@4c
    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@4f
    move-result-object v1

    #@50
    .line 1553
    .local v1, response:Landroid/os/Message;
    const-string v2, "copySmsToIccEf(), IccSmsInterfaceManager --> RIL"

    #@52
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@55
    .line 1555
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@57
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@59
    invoke-static {p3}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    invoke-static {p2}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@60
    move-result-object v5

    #@61
    invoke-interface {v2, p1, v4, v5, v1}, Lcom/android/internal/telephony/CommandsInterface;->writeSmsToSim(ILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    :try_end_64
    .catchall {:try_start_47 .. :try_end_64} :catchall_8c

    #@64
    .line 1559
    :try_start_64
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@66
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_69
    .catchall {:try_start_64 .. :try_end_69} :catchall_8c
    .catch Ljava/lang/InterruptedException; {:try_start_64 .. :try_end_69} :catch_85

    #@69
    .line 1563
    :goto_69
    :try_start_69
    monitor-exit v3
    :try_end_6a
    .catchall {:try_start_69 .. :try_end_6a} :catchall_8c

    #@6a
    .line 1564
    new-instance v2, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v3, "copyMessageToIccEf: mIndexOnIcc="

    #@71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v2

    #@75
    iget v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mIndexOnIcc:I

    #@77
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v2

    #@7b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@82
    .line 1565
    iget v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mIndexOnIcc:I

    #@84
    return v2

    #@85
    .line 1560
    :catch_85
    move-exception v0

    #@86
    .line 1561
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_86
    const-string v2, "interrupted while trying to update by index"

    #@88
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@8b
    goto :goto_69

    #@8c
    .line 1563
    .end local v0           #e:Ljava/lang/InterruptedException;
    .end local v1           #response:Landroid/os/Message;
    :catchall_8c
    move-exception v2

    #@8d
    monitor-exit v3
    :try_end_8e
    .catchall {:try_start_86 .. :try_end_8e} :catchall_8c

    #@8e
    throw v2
.end method

.method public copySmsToIccEfPrivate(I[B[BI)I
    .registers 11
    .parameter "status"
    .parameter "pdu"
    .parameter "smsc"
    .parameter "sms_format"

    #@0
    .prologue
    .line 685
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "copySmsToIccEfPrivate: status="

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    const-string v3, " ==> "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "pdu=("

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {p2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, "), smsc=("

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-static {p3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    const-string v3, ")"

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, "SMS_format : "

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@48
    .line 689
    const-string v2, "Copying sms to UIcc"

    #@4a
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->enforceReceiveAndSend(Ljava/lang/String;)V

    #@4d
    .line 691
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@4f
    monitor-enter v3

    #@50
    .line 693
    const/4 v2, 0x0

    #@51
    :try_start_51
    iput-boolean v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@53
    .line 694
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@55
    const/4 v4, 0x6

    #@56
    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@59
    move-result-object v1

    #@5a
    .line 696
    .local v1, response:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@5c
    const-string v4, "control_uicc_storage"

    #@5e
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@61
    move-result v2

    #@62
    if-eqz v2, :cond_80

    #@64
    .line 697
    const-string v2, "copySmsToIccEfPrivate(), KEY_CONTROL_UICC_STORAGE is Defined"

    #@66
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@69
    .line 698
    const/4 v2, 0x1

    #@6a
    if-ne p4, v2, :cond_a1

    #@6c
    .line 699
    const-string v2, "copySmsToIccEfPrivate(), 3GPP SMS format-writeSmsToSim"

    #@6e
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@71
    .line 700
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@73
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@75
    invoke-static {p3}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@78
    move-result-object v4

    #@79
    invoke-static {p2}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@7c
    move-result-object v5

    #@7d
    invoke-interface {v2, p1, v4, v5, v1}, Lcom/android/internal/telephony/CommandsInterface;->writeSmsToSim(ILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    :try_end_80
    .catchall {:try_start_51 .. :try_end_80} :catchall_ae

    #@80
    .line 709
    :cond_80
    :goto_80
    :try_start_80
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@82
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_85
    .catchall {:try_start_80 .. :try_end_85} :catchall_ae
    .catch Ljava/lang/InterruptedException; {:try_start_80 .. :try_end_85} :catch_b1

    #@85
    .line 713
    :goto_85
    :try_start_85
    monitor-exit v3
    :try_end_86
    .catchall {:try_start_85 .. :try_end_86} :catchall_ae

    #@86
    .line 714
    new-instance v2, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v3, "copySmsToIccEfPrivate(), mIndexOnIcc = "

    #@8d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v2

    #@91
    iget v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mIndexOnIcc:I

    #@93
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@96
    move-result-object v2

    #@97
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v2

    #@9b
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@9e
    .line 715
    iget v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mIndexOnIcc:I

    #@a0
    return v2

    #@a1
    .line 703
    :cond_a1
    :try_start_a1
    const-string v2, "copySmsToIccEfPrivate(), 3GPP2 SMS format-writeSmsToSim"

    #@a3
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@a6
    .line 704
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a8
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@aa
    invoke-interface {v2, p1, p2, v1}, Lcom/android/internal/telephony/CommandsInterface;->writeSmsToCsim(I[BLandroid/os/Message;)V

    #@ad
    goto :goto_80

    #@ae
    .line 713
    .end local v1           #response:Landroid/os/Message;
    :catchall_ae
    move-exception v2

    #@af
    monitor-exit v3
    :try_end_b0
    .catchall {:try_start_a1 .. :try_end_b0} :catchall_ae

    #@b0
    throw v2

    #@b1
    .line 710
    .restart local v1       #response:Landroid/os/Message;
    :catch_b1
    move-exception v0

    #@b2
    .line 711
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_b2
    const-string v2, "interrupted while trying to update by index"

    #@b4
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V
    :try_end_b7
    .catchall {:try_start_b2 .. :try_end_b7} :catchall_ae

    #@b7
    goto :goto_85
.end method

.method public disableCdmaBroadcast(I)Z
    .registers 3
    .parameter "messageIdentifier"

    #@0
    .prologue
    .line 1300
    invoke-virtual {p0, p1, p1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->disableCdmaBroadcastRange(II)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public declared-synchronized disableCdmaBroadcastRange(II)Z
    .registers 9
    .parameter "startMessageId"
    .parameter "endMessageId"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1332
    monitor-enter p0

    #@3
    :try_start_3
    const-string v4, "disableCdmaBroadcastRange"

    #@5
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@8
    .line 1334
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@d
    move-result-object v1

    #@e
    .line 1336
    .local v1, context:Landroid/content/Context;
    const-string v4, "android.permission.RECEIVE_SMS"

    #@10
    const-string v5, "Disabling cell broadcast SMS"

    #@12
    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 1340
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@18
    move-result-object v4

    #@19
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1c
    move-result v5

    #@1d
    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    .line 1343
    .local v0, client:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mCdmaBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;

    #@23
    invoke-virtual {v4, p1, p2, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;->disableRange(IILjava/lang/String;)Z

    #@26
    move-result v4

    #@27
    if-nez v4, :cond_55

    #@29
    .line 1344
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "Failed to remove cdma broadcast subscription for MID range "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    const-string v4, " to "

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    const-string v4, " from client "

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V
    :try_end_53
    .catchall {:try_start_3 .. :try_end_53} :catchall_8d

    #@53
    .line 1355
    :goto_53
    monitor-exit p0

    #@54
    return v2

    #@55
    .line 1350
    :cond_55
    :try_start_55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "Removed cdma broadcast subscription for MID range "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    const-string v5, " to "

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    const-string v5, " from client "

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@7f
    .line 1353
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mCdmaBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;

    #@81
    invoke-virtual {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;->isEmpty()Z

    #@84
    move-result v4

    #@85
    if-nez v4, :cond_88

    #@87
    move v2, v3

    #@88
    :cond_88
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->setCdmaBroadcastActivation(Z)Z
    :try_end_8b
    .catchall {:try_start_55 .. :try_end_8b} :catchall_8d

    #@8b
    move v2, v3

    #@8c
    .line 1355
    goto :goto_53

    #@8d
    .line 1332
    .end local v0           #client:Ljava/lang/String;
    .end local v1           #context:Landroid/content/Context;
    :catchall_8d
    move-exception v2

    #@8e
    monitor-exit p0

    #@8f
    throw v2
.end method

.method public disableCellBroadcast(I)Z
    .registers 3
    .parameter "messageIdentifier"

    #@0
    .prologue
    .line 1238
    invoke-virtual {p0, p1, p1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->disableCellBroadcastRange(II)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public declared-synchronized disableCellBroadcastRange(II)Z
    .registers 9
    .parameter "startMessageId"
    .parameter "endMessageId"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1269
    monitor-enter p0

    #@3
    :try_start_3
    const-string v4, "disableCellBroadcastRange"

    #@5
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@8
    .line 1271
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@d
    move-result-object v1

    #@e
    .line 1273
    .local v1, context:Landroid/content/Context;
    const-string v4, "android.permission.RECEIVE_SMS"

    #@10
    const-string v5, "Disabling cell broadcast SMS"

    #@12
    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 1277
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@18
    move-result-object v4

    #@19
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1c
    move-result v5

    #@1d
    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    .line 1280
    .local v0, client:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mCellBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;

    #@23
    invoke-virtual {v4, p1, p2, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;->disableRange(IILjava/lang/String;)Z

    #@26
    move-result v4

    #@27
    if-nez v4, :cond_55

    #@29
    .line 1281
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "Failed to remove cell broadcast subscription for MID range "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    const-string v4, " to "

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    const-string v4, " from client "

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V
    :try_end_53
    .catchall {:try_start_3 .. :try_end_53} :catchall_8d

    #@53
    .line 1292
    :goto_53
    monitor-exit p0

    #@54
    return v2

    #@55
    .line 1287
    :cond_55
    :try_start_55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "Removed cell broadcast subscription for MID range "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    const-string v5, " to "

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    const-string v5, " from client "

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@7f
    .line 1290
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mCellBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;

    #@81
    invoke-virtual {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;->isEmpty()Z

    #@84
    move-result v4

    #@85
    if-nez v4, :cond_88

    #@87
    move v2, v3

    #@88
    :cond_88
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->setCellBroadcastActivation(Z)Z
    :try_end_8b
    .catchall {:try_start_55 .. :try_end_8b} :catchall_8d

    #@8b
    move v2, v3

    #@8c
    .line 1292
    goto :goto_53

    #@8d
    .line 1269
    .end local v0           #client:Ljava/lang/String;
    .end local v1           #context:Landroid/content/Context;
    :catchall_8d
    move-exception v2

    #@8e
    monitor-exit p0

    #@8f
    throw v2
.end method

.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 368
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SMSDispatcher;->dispose()V

    #@5
    .line 369
    return-void
.end method

.method public enableAutoDCDisconnect(I)V
    .registers 3
    .parameter "timeOut"

    #@0
    .prologue
    .line 1627
    const-string v0, "enableAutoDCDisconnect(), start"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@5
    .line 1628
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@7
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->enableAutoDCDisconnect(I)V

    #@a
    .line 1629
    return-void
.end method

.method public enableCdmaBroadcast(I)Z
    .registers 3
    .parameter "messageIdentifier"

    #@0
    .prologue
    .line 1296
    invoke-virtual {p0, p1, p1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->enableCdmaBroadcastRange(II)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public declared-synchronized enableCdmaBroadcastRange(II)Z
    .registers 9
    .parameter "startMessageId"
    .parameter "endMessageId"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1305
    monitor-enter p0

    #@3
    :try_start_3
    const-string v4, "enableCdmaBroadcastRange"

    #@5
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@8
    .line 1307
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@d
    move-result-object v1

    #@e
    .line 1309
    .local v1, context:Landroid/content/Context;
    const-string v4, "android.permission.RECEIVE_SMS"

    #@10
    const-string v5, "Enabling cdma broadcast SMS"

    #@12
    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 1313
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@18
    move-result-object v4

    #@19
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1c
    move-result v5

    #@1d
    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    .line 1316
    .local v0, client:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mCdmaBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;

    #@23
    invoke-virtual {v4, p1, p2, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;->enableRange(IILjava/lang/String;)Z

    #@26
    move-result v4

    #@27
    if-nez v4, :cond_55

    #@29
    .line 1317
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "Failed to add cdma broadcast subscription for MID range "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    const-string v4, " to "

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    const-string v4, " from client "

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V
    :try_end_53
    .catchall {:try_start_3 .. :try_end_53} :catchall_8d

    #@53
    .line 1328
    :goto_53
    monitor-exit p0

    #@54
    return v2

    #@55
    .line 1323
    :cond_55
    :try_start_55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "Added cdma broadcast subscription for MID range "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    const-string v5, " to "

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    const-string v5, " from client "

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@7f
    .line 1326
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mCdmaBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;

    #@81
    invoke-virtual {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager$CdmaBroadcastRangeManager;->isEmpty()Z

    #@84
    move-result v4

    #@85
    if-nez v4, :cond_88

    #@87
    move v2, v3

    #@88
    :cond_88
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->setCdmaBroadcastActivation(Z)Z
    :try_end_8b
    .catchall {:try_start_55 .. :try_end_8b} :catchall_8d

    #@8b
    move v2, v3

    #@8c
    .line 1328
    goto :goto_53

    #@8d
    .line 1305
    .end local v0           #client:Ljava/lang/String;
    .end local v1           #context:Landroid/content/Context;
    :catchall_8d
    move-exception v2

    #@8e
    monitor-exit p0

    #@8f
    throw v2
.end method

.method public enableCellBroadcast(I)Z
    .registers 3
    .parameter "messageIdentifier"

    #@0
    .prologue
    .line 1234
    invoke-virtual {p0, p1, p1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->enableCellBroadcastRange(II)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public declared-synchronized enableCellBroadcastRange(II)Z
    .registers 9
    .parameter "startMessageId"
    .parameter "endMessageId"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1242
    monitor-enter p0

    #@3
    :try_start_3
    const-string v4, "enableCellBroadcastRange"

    #@5
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@8
    .line 1244
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@d
    move-result-object v1

    #@e
    .line 1246
    .local v1, context:Landroid/content/Context;
    const-string v4, "android.permission.RECEIVE_SMS"

    #@10
    const-string v5, "Enabling cell broadcast SMS"

    #@12
    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 1250
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@18
    move-result-object v4

    #@19
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1c
    move-result v5

    #@1d
    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    .line 1253
    .local v0, client:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mCellBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;

    #@23
    invoke-virtual {v4, p1, p2, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;->enableRange(IILjava/lang/String;)Z

    #@26
    move-result v4

    #@27
    if-nez v4, :cond_55

    #@29
    .line 1254
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "Failed to add cell broadcast subscription for MID range "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    const-string v4, " to "

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    const-string v4, " from client "

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V
    :try_end_53
    .catchall {:try_start_3 .. :try_end_53} :catchall_8d

    #@53
    .line 1265
    :goto_53
    monitor-exit p0

    #@54
    return v2

    #@55
    .line 1260
    :cond_55
    :try_start_55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "Added cell broadcast subscription for MID range "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    const-string v5, " to "

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    const-string v5, " from client "

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@7f
    .line 1263
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mCellBroadcastRangeManager:Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;

    #@81
    invoke-virtual {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager$CellBroadcastRangeManager;->isEmpty()Z

    #@84
    move-result v4

    #@85
    if-nez v4, :cond_88

    #@87
    move v2, v3

    #@88
    :cond_88
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->setCellBroadcastActivation(Z)Z
    :try_end_8b
    .catchall {:try_start_55 .. :try_end_8b} :catchall_8d

    #@8b
    move v2, v3

    #@8c
    .line 1265
    goto :goto_53

    #@8d
    .line 1242
    .end local v0           #client:Ljava/lang/String;
    .end local v1           #context:Landroid/content/Context;
    :catchall_8d
    move-exception v2

    #@8e
    monitor-exit p0

    #@8f
    throw v2
.end method

.method protected enforceReceiveAndSend(Ljava/lang/String;)V
    .registers 4
    .parameter "message"

    #@0
    .prologue
    .line 398
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.RECEIVE_SMS"

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 400
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@9
    const-string v1, "android.permission.SEND_SMS"

    #@b
    invoke-virtual {v0, v1, p1}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 402
    return-void
.end method

.method protected finalize()V
    .registers 2

    #@0
    .prologue
    .line 372
    const-string v0, "finalize(), start"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 373
    return-void
.end method

.method public getAllMessagesFromIccEf()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/SmsRawData;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 612
    const-string v3, "getAllMessagesFromEF"

    #@2
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@5
    .line 614
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@7
    const-string v4, "android.permission.RECEIVE_SMS"

    #@9
    const-string v5, "Reading messages from Icc"

    #@b
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 617
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@10
    monitor-enter v4

    #@11
    .line 619
    :try_start_11
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@13
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@16
    move-result-object v1

    #@17
    .line 620
    .local v1, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-nez v1, :cond_2b

    #@19
    .line 621
    const-string v3, "getAllMessagesFromIccEf(), Cannot load Sms records. No icc card?"

    #@1b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@1e
    .line 622
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@20
    if-eqz v3, :cond_2b

    #@22
    .line 623
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@24
    invoke-interface {v3}, Ljava/util/List;->clear()V

    #@27
    .line 624
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@29
    monitor-exit v4

    #@2a
    .line 637
    :goto_2a
    return-object v3

    #@2b
    .line 628
    :cond_2b
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@2d
    const/4 v5, 0x1

    #@2e
    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@31
    move-result-object v2

    #@32
    .line 629
    .local v2, response:Landroid/os/Message;
    const/16 v3, 0x6f3c

    #@34
    invoke-virtual {v1, v3, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V
    :try_end_37
    .catchall {:try_start_11 .. :try_end_37} :catchall_47

    #@37
    .line 632
    :try_start_37
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@39
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_3c
    .catchall {:try_start_37 .. :try_end_3c} :catchall_47
    .catch Ljava/lang/InterruptedException; {:try_start_37 .. :try_end_3c} :catch_40

    #@3c
    .line 636
    :goto_3c
    :try_start_3c
    monitor-exit v4
    :try_end_3d
    .catchall {:try_start_3c .. :try_end_3d} :catchall_47

    #@3d
    .line 637
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@3f
    goto :goto_2a

    #@40
    .line 633
    :catch_40
    move-exception v0

    #@41
    .line 634
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_41
    const-string v3, "interrupted while trying to load from the Icc"

    #@43
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@46
    goto :goto_3c

    #@47
    .line 636
    .end local v0           #e:Ljava/lang/InterruptedException;
    .end local v1           #fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    .end local v2           #response:Landroid/os/Message;
    :catchall_47
    move-exception v3

    #@48
    monitor-exit v4
    :try_end_49
    .catchall {:try_start_41 .. :try_end_49} :catchall_47

    #@49
    throw v3
.end method

.method public getAllMessagesFromIccEf3GPP()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/SmsRawData;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 724
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.RECEIVE_SMS"

    #@4
    const-string v5, "Reading messages from SIM"

    #@6
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 727
    const-string v3, "getAllMessagesFromIccEf3GPP()"

    #@b
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@e
    .line 728
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@10
    monitor-enter v4

    #@11
    .line 730
    :try_start_11
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@13
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@16
    move-result-object v1

    #@17
    .line 731
    .local v1, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-nez v1, :cond_2b

    #@19
    .line 732
    const-string v3, "getAllMessagesFromIccEf3GPP(), Cannot load Sms records. No icc card?"

    #@1b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@1e
    .line 733
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@20
    if-eqz v3, :cond_27

    #@22
    .line 734
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@24
    invoke-interface {v3}, Ljava/util/List;->clear()V

    #@27
    .line 736
    :cond_27
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@29
    monitor-exit v4

    #@2a
    .line 747
    :goto_2a
    return-object v3

    #@2b
    .line 739
    :cond_2b
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@2d
    const/4 v5, 0x1

    #@2e
    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@31
    move-result-object v2

    #@32
    .line 740
    .local v2, response:Landroid/os/Message;
    const/16 v3, 0x6f3c

    #@34
    invoke-virtual {v1, v3, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V
    :try_end_37
    .catchall {:try_start_11 .. :try_end_37} :catchall_40

    #@37
    .line 742
    :try_start_37
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@39
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_3c
    .catchall {:try_start_37 .. :try_end_3c} :catchall_40
    .catch Ljava/lang/InterruptedException; {:try_start_37 .. :try_end_3c} :catch_43

    #@3c
    .line 747
    :goto_3c
    :try_start_3c
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@3e
    monitor-exit v4

    #@3f
    goto :goto_2a

    #@40
    .line 748
    .end local v1           #fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    .end local v2           #response:Landroid/os/Message;
    :catchall_40
    move-exception v3

    #@41
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_3c .. :try_end_42} :catchall_40

    #@42
    throw v3

    #@43
    .line 743
    .restart local v1       #fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    .restart local v2       #response:Landroid/os/Message;
    :catch_43
    move-exception v0

    #@44
    .line 744
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_44
    const-string v3, "interrupted while trying to load from the UIcc"

    #@46
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V
    :try_end_49
    .catchall {:try_start_44 .. :try_end_49} :catchall_40

    #@49
    goto :goto_3c
.end method

.method public getAllMessagesFromIccEf3GPP2()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/SmsRawData;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 757
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.RECEIVE_SMS"

    #@4
    const-string v5, "Reading messages from SIM"

    #@6
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 760
    const-string v3, "getAllMessagesFromIccEf3GPP2()"

    #@b
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@e
    .line 761
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@10
    monitor-enter v4

    #@11
    .line 763
    :try_start_11
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@13
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@16
    move-result-object v1

    #@17
    .line 764
    .local v1, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-nez v1, :cond_2b

    #@19
    .line 765
    const-string v3, "getAllMessagesFromIccEf3GPP2(), Cannot load Sms records. No icc card?"

    #@1b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@1e
    .line 766
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@20
    if-eqz v3, :cond_27

    #@22
    .line 767
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@24
    invoke-interface {v3}, Ljava/util/List;->clear()V

    #@27
    .line 769
    :cond_27
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@29
    monitor-exit v4

    #@2a
    .line 781
    :goto_2a
    return-object v3

    #@2b
    .line 772
    :cond_2b
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@2d
    const/4 v5, 0x1

    #@2e
    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@31
    move-result-object v2

    #@32
    .line 773
    .local v2, response:Landroid/os/Message;
    const/16 v3, 0x6f3c

    #@34
    invoke-virtual {v1, v3, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V
    :try_end_37
    .catchall {:try_start_11 .. :try_end_37} :catchall_40

    #@37
    .line 776
    :try_start_37
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@39
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_3c
    .catchall {:try_start_37 .. :try_end_3c} :catchall_40
    .catch Ljava/lang/InterruptedException; {:try_start_37 .. :try_end_3c} :catch_43

    #@3c
    .line 781
    :goto_3c
    :try_start_3c
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@3e
    monitor-exit v4

    #@3f
    goto :goto_2a

    #@40
    .line 782
    .end local v1           #fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    .end local v2           #response:Landroid/os/Message;
    :catchall_40
    move-exception v3

    #@41
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_3c .. :try_end_42} :catchall_40

    #@42
    throw v3

    #@43
    .line 777
    .restart local v1       #fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    .restart local v2       #response:Landroid/os/Message;
    :catch_43
    move-exception v0

    #@44
    .line 778
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_44
    const-string v3, "interrupted while trying to load from the UIcc"

    #@46
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V
    :try_end_49
    .catchall {:try_start_44 .. :try_end_49} :catchall_40

    #@49
    goto :goto_3c
.end method

.method public getImsSmsFormat()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1529
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SMSDispatcher;->getImsSmsFormat()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getMaxEfSms()I
    .registers 8

    #@0
    .prologue
    .line 1569
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    .line 1570
    .local v0, context:Landroid/content/Context;
    const-string v4, "android.permission.RECEIVE_SMS"

    #@8
    const-string v5, "Reading messages from SIM"

    #@a
    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 1573
    iget-object v5, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@f
    monitor-enter v5

    #@10
    .line 1574
    :try_start_10
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@12
    const/16 v6, 0x7d

    #@14
    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@17
    move-result-object v3

    #@18
    .line 1575
    .local v3, response:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1a
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1d
    move-result-object v2

    #@1e
    .line 1576
    .local v2, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-nez v2, :cond_23

    #@20
    .line 1577
    const/4 v4, -0x1

    #@21
    monitor-exit v5

    #@22
    .line 1587
    :goto_22
    return v4

    #@23
    .line 1579
    :cond_23
    const/16 v4, 0x6f3c

    #@25
    invoke-virtual {v2, v4, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFLinearRecordSize(ILandroid/os/Message;)V
    :try_end_28
    .catchall {:try_start_10 .. :try_end_28} :catchall_54

    #@28
    .line 1581
    :try_start_28
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@2a
    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_2d
    .catchall {:try_start_28 .. :try_end_2d} :catchall_54
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_2d} :catch_4d

    #@2d
    .line 1585
    :goto_2d
    :try_start_2d
    monitor-exit v5
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_54

    #@2e
    .line 1586
    new-instance v4, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v5, "getMaxEfSms(), recordSize="

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    iget v5, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->recordSize:I

    #@3b
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4a
    .line 1587
    iget v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->recordSize:I

    #@4c
    goto :goto_22

    #@4d
    .line 1582
    :catch_4d
    move-exception v1

    #@4e
    .line 1583
    .local v1, e:Ljava/lang/Exception;
    :try_start_4e
    const-string v4, "getMaxEfSms(), getRecordsSize, interrupted while trying to load from the SIM"

    #@50
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@53
    goto :goto_2d

    #@54
    .line 1585
    .end local v1           #e:Ljava/lang/Exception;
    .end local v2           #fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    .end local v3           #response:Landroid/os/Message;
    :catchall_54
    move-exception v4

    #@55
    monitor-exit v5
    :try_end_56
    .catchall {:try_start_4e .. :try_end_56} :catchall_54

    #@56
    throw v4
.end method

.method public getPremiumSmsPermission(Ljava/lang/String;)I
    .registers 3
    .parameter "packageName"

    #@0
    .prologue
    .line 983
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->getPremiumSmsPermission(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getReadItemMessagesFromIccEf(I)Ljava/util/List;
    .registers 8
    .parameter "item"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/SmsRawData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 644
    const-string v3, "getReadItemMessagesFromIccEf"

    #@2
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@5
    .line 645
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@7
    const-string v4, "android.permission.RECEIVE_SMS"

    #@9
    const-string v5, "Reading messages from SIM"

    #@b
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 647
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@10
    monitor-enter v4

    #@11
    .line 649
    :try_start_11
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@13
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@16
    move-result-object v1

    #@17
    .line 650
    .local v1, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-eqz v1, :cond_31

    #@19
    .line 651
    iput p1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mItemNumber:I

    #@1b
    .line 652
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@1d
    const/16 v5, 0x80

    #@1f
    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@22
    move-result-object v2

    #@23
    .line 654
    .local v2, response:Landroid/os/Message;
    const/16 v3, 0x6f3c

    #@25
    invoke-virtual {v1, v3, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V
    :try_end_28
    .catchall {:try_start_11 .. :try_end_28} :catchall_43

    #@28
    .line 664
    .end local v2           #response:Landroid/os/Message;
    :cond_28
    :try_start_28
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@2a
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_2d
    .catchall {:try_start_28 .. :try_end_2d} :catchall_43
    .catch Ljava/lang/InterruptedException; {:try_start_28 .. :try_end_2d} :catch_46

    #@2d
    .line 668
    :goto_2d
    :try_start_2d
    monitor-exit v4
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_43

    #@2e
    .line 669
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@30
    :goto_30
    return-object v3

    #@31
    .line 656
    :cond_31
    :try_start_31
    const-string v3, "getReadItemMessagesFromIccEf(), Cannot load Sms records. No icc card?"

    #@33
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@36
    .line 657
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@38
    if-eqz v3, :cond_28

    #@3a
    .line 658
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@3c
    invoke-interface {v3}, Ljava/util/List;->clear()V

    #@3f
    .line 659
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSms:Ljava/util/List;

    #@41
    monitor-exit v4

    #@42
    goto :goto_30

    #@43
    .line 668
    .end local v1           #fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    :catchall_43
    move-exception v3

    #@44
    monitor-exit v4
    :try_end_45
    .catchall {:try_start_31 .. :try_end_45} :catchall_43

    #@45
    throw v3

    #@46
    .line 665
    .restart local v1       #fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    :catch_46
    move-exception v0

    #@47
    .line 666
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_47
    const-string v3, "interrupted while trying to load from the UIcc"

    #@49
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V
    :try_end_4c
    .catchall {:try_start_47 .. :try_end_4c} :catchall_43

    #@4c
    goto :goto_2d
.end method

.method public getSmsIsRoaming()Z
    .registers 3

    #@0
    .prologue
    .line 1698
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "getSmsIsRoaming(), get value = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@d
    invoke-static {}, Lcom/android/internal/telephony/SMSDispatcher;->getSmsIsRoaming()Z

    #@10
    move-result v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1c
    .line 1700
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@1e
    invoke-static {}, Lcom/android/internal/telephony/SMSDispatcher;->getSmsIsRoaming()Z

    #@21
    move-result v0

    #@22
    return v0
.end method

.method public getSmscenterAddress()Ljava/lang/String;
    .registers 12

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 1130
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@3
    const-string v8, "hide_privacy_log"

    #@5
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8
    move-result v7

    #@9
    if-ne v7, v10, :cond_87

    #@b
    .line 1131
    const-string v7, "1"

    #@d
    const-string v8, "persist.service.privacy.enable"

    #@f
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v8

    #@13
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v7

    #@17
    if-eqz v7, :cond_31

    #@19
    .line 1132
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v8, "getSmscenterAddress(), mSCAddr = "

    #@20
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v7

    #@24
    iget-object v8, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSCAddr:Ljava/lang/String;

    #@26
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v7

    #@2e
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@31
    .line 1140
    :cond_31
    :goto_31
    const-string v7, "getSmscAddress"

    #@33
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->enforceReceiveAndSend(Ljava/lang/String;)V

    #@36
    .line 1141
    iget-object v8, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@38
    monitor-enter v8

    #@39
    .line 1142
    :try_start_39
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@3b
    const/4 v9, 0x5

    #@3c
    invoke-virtual {v7, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@3f
    move-result-object v3

    #@40
    .line 1143
    .local v3, response:Landroid/os/Message;
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@42
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@44
    invoke-interface {v7, v3}, Lcom/android/internal/telephony/CommandsInterface;->getSmscAddress(Landroid/os/Message;)V
    :try_end_47
    .catchall {:try_start_39 .. :try_end_47} :catchall_a7

    #@47
    .line 1145
    :try_start_47
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@49
    invoke-virtual {v7}, Ljava/lang/Object;->wait()V
    :try_end_4c
    .catchall {:try_start_47 .. :try_end_4c} :catchall_a7
    .catch Ljava/lang/InterruptedException; {:try_start_47 .. :try_end_4c} :catch_a0

    #@4c
    .line 1149
    :goto_4c
    :try_start_4c
    monitor-exit v8
    :try_end_4d
    .catchall {:try_start_4c .. :try_end_4d} :catchall_a7

    #@4d
    .line 1152
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@4f
    const-string v8, "hide_privacy_log"

    #@51
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@54
    move-result v7

    #@55
    if-ne v7, v10, :cond_aa

    #@57
    .line 1153
    const-string v7, "1"

    #@59
    const-string v8, "persist.service.privacy.enable"

    #@5b
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5e
    move-result-object v8

    #@5f
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@62
    move-result v7

    #@63
    if-eqz v7, :cond_7d

    #@65
    .line 1154
    new-instance v7, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v8, "getSmscenterAddress(), SC Address = "

    #@6c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v7

    #@70
    iget-object v8, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSCAddr:Ljava/lang/String;

    #@72
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v7

    #@7a
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@7d
    .line 1160
    :cond_7d
    :goto_7d
    const-string v4, ""

    #@7f
    .line 1161
    .local v4, ret:Ljava/lang/String;
    const-string v6, ""

    #@81
    .line 1163
    .local v6, temp:Ljava/lang/String;
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSCAddr:Ljava/lang/String;

    #@83
    if-nez v7, :cond_c3

    #@85
    move-object v5, v4

    #@86
    .line 1190
    .end local v4           #ret:Ljava/lang/String;
    .local v5, ret:Ljava/lang/String;
    :goto_86
    return-object v5

    #@87
    .line 1135
    .end local v3           #response:Landroid/os/Message;
    .end local v5           #ret:Ljava/lang/String;
    .end local v6           #temp:Ljava/lang/String;
    :cond_87
    new-instance v7, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v8, "getSmscenterAddress(), mSCAddr = "

    #@8e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v7

    #@92
    iget-object v8, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSCAddr:Ljava/lang/String;

    #@94
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v7

    #@98
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v7

    #@9c
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@9f
    goto :goto_31

    #@a0
    .line 1146
    .restart local v3       #response:Landroid/os/Message;
    :catch_a0
    move-exception v1

    #@a1
    .line 1147
    .local v1, e:Ljava/lang/InterruptedException;
    :try_start_a1
    const-string v7, "interrupted while trying to update by index"

    #@a3
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@a6
    goto :goto_4c

    #@a7
    .line 1149
    .end local v1           #e:Ljava/lang/InterruptedException;
    .end local v3           #response:Landroid/os/Message;
    :catchall_a7
    move-exception v7

    #@a8
    monitor-exit v8
    :try_end_a9
    .catchall {:try_start_a1 .. :try_end_a9} :catchall_a7

    #@a9
    throw v7

    #@aa
    .line 1157
    .restart local v3       #response:Landroid/os/Message;
    :cond_aa
    new-instance v7, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v8, "getSmscenterAddress(), SC Address = "

    #@b1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v7

    #@b5
    iget-object v8, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSCAddr:Ljava/lang/String;

    #@b7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v7

    #@bb
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v7

    #@bf
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@c2
    goto :goto_7d

    #@c3
    .line 1168
    .restart local v4       #ret:Ljava/lang/String;
    .restart local v6       #temp:Ljava/lang/String;
    :cond_c3
    :try_start_c3
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSCAddr:Ljava/lang/String;

    #@c5
    const-string v8, ","

    #@c7
    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@ca
    move-result v7

    #@cb
    if-eqz v7, :cond_127

    #@cd
    .line 1169
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSCAddr:Ljava/lang/String;

    #@cf
    const-string v8, ","

    #@d1
    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@d4
    move-result-object v7

    #@d5
    const/4 v8, 0x0

    #@d6
    aget-object v6, v7, v8

    #@d8
    .line 1173
    :goto_d8
    const/4 v7, 0x1

    #@d9
    new-array v0, v7, [C

    #@db
    const/4 v7, 0x0

    #@dc
    const/16 v8, 0x22

    #@de
    aput-char v8, v0, v7

    #@e0
    .line 1174
    .local v0, char_quote:[C
    new-instance v2, Ljava/lang/String;

    #@e2
    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V

    #@e5
    .line 1175
    .local v2, quote:Ljava/lang/String;
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@e8
    move-result v7

    #@e9
    if-eqz v7, :cond_124

    #@eb
    .line 1176
    const/4 v7, 0x1

    #@ec
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@ef
    move-result v8

    #@f0
    add-int/lit8 v8, v8, -0x1

    #@f2
    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@f5
    move-result-object v4

    #@f6
    .line 1178
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@f8
    const-string v8, "hide_privacy_log"

    #@fa
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@fd
    move-result v7

    #@fe
    if-ne v7, v10, :cond_12a

    #@100
    .line 1179
    const-string v7, "1"

    #@102
    const-string v8, "persist.service.privacy.enable"

    #@104
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@107
    move-result-object v8

    #@108
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10b
    move-result v7

    #@10c
    if-eqz v7, :cond_124

    #@10e
    .line 1180
    new-instance v7, Ljava/lang/StringBuilder;

    #@110
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@113
    const-string v8, "getSmscenterAddress(), ret = "

    #@115
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v7

    #@119
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v7

    #@11d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v7

    #@121
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@124
    .end local v0           #char_quote:[C
    .end local v2           #quote:Ljava/lang/String;
    :cond_124
    :goto_124
    move-object v5, v4

    #@125
    .line 1190
    .end local v4           #ret:Ljava/lang/String;
    .restart local v5       #ret:Ljava/lang/String;
    goto/16 :goto_86

    #@127
    .line 1171
    .end local v5           #ret:Ljava/lang/String;
    .restart local v4       #ret:Ljava/lang/String;
    :cond_127
    iget-object v6, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSCAddr:Ljava/lang/String;

    #@129
    goto :goto_d8

    #@12a
    .line 1183
    .restart local v0       #char_quote:[C
    .restart local v2       #quote:Ljava/lang/String;
    :cond_12a
    new-instance v7, Ljava/lang/StringBuilder;

    #@12c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@12f
    const-string v8, "getSmscenterAddress(), ret = "

    #@131
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v7

    #@135
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v7

    #@139
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13c
    move-result-object v7

    #@13d
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I
    :try_end_140
    .catch Ljava/lang/NullPointerException; {:try_start_c3 .. :try_end_140} :catch_141

    #@140
    goto :goto_124

    #@141
    .line 1187
    .end local v0           #char_quote:[C
    .end local v2           #quote:Ljava/lang/String;
    :catch_141
    move-exception v1

    #@142
    .line 1188
    .local v1, e:Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@145
    goto :goto_124
.end method

.method public getUiccType()I
    .registers 2

    #@0
    .prologue
    .line 1642
    iget v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mUiccType:I

    #@2
    return v0
.end method

.method protected initDispatchers()V
    .registers 5

    #@0
    .prologue
    .line 357
    const-string v0, "initDispatchers(), start"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 358
    new-instance v0, Lcom/android/internal/telephony/ImsSMSDispatcher;

    #@7
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@9
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mSmsStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@d
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mSmsUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@11
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/ImsSMSDispatcher;-><init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;)V

    #@14
    iput-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@16
    .line 361
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isIPPhoneSupported()Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_20

    #@1c
    .line 362
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@1e
    iput-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mImsDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@20
    .line 365
    :cond_20
    return-void
.end method

.method public isFdnEnabled()Z
    .registers 6

    #@0
    .prologue
    .line 1661
    const/4 v0, 0x0

    #@1
    .line 1663
    .local v0, ret:Z
    :try_start_1
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getIccCard()Lcom/android/internal/telephony/IccCard;

    #@6
    move-result-object v3

    #@7
    if-nez v3, :cond_b

    #@9
    move v1, v0

    #@a
    .line 1670
    .end local v0           #ret:Z
    .local v1, ret:I
    :goto_a
    return v1

    #@b
    .line 1666
    .end local v1           #ret:I
    .restart local v0       #ret:Z
    :cond_b
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getIccCard()Lcom/android/internal/telephony/IccCard;

    #@10
    move-result-object v3

    #@11
    invoke-interface {v3}, Lcom/android/internal/telephony/IccCard;->getIccFdnEnabled()Z
    :try_end_14
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_14} :catch_17

    #@14
    move-result v0

    #@15
    :goto_15
    move v1, v0

    #@16
    .line 1670
    .restart local v1       #ret:I
    goto :goto_a

    #@17
    .line 1667
    .end local v1           #ret:I
    :catch_17
    move-exception v2

    #@18
    .line 1668
    .local v2, tr:Ljava/lang/RuntimeException;
    const-string v3, "RIL_IccSms"

    #@1a
    const-string v4, "cannot get FdnEnabled value: "

    #@1c
    invoke-static {v3, v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f
    goto :goto_15
.end method

.method public isGsmMo()Z
    .registers 2

    #@0
    .prologue
    .line 1634
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SMSDispatcher;->isCdmaMo()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isImsSmsSupported()Z
    .registers 2

    #@0
    .prologue
    .line 1525
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SMSDispatcher;->isIms()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected log(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 1521
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IccSmsInterfaceManager] "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 1522
    return-void
.end method

.method protected makeSmsRecordData(I[B)[B
    .registers 8
    .parameter "status"
    .parameter "pdu"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1102
    iget-object v2, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@7
    move-result v2

    #@8
    if-ne v4, v2, :cond_23

    #@a
    .line 1103
    const/16 v2, 0xb0

    #@c
    new-array v0, v2, [B

    #@e
    .line 1109
    .local v0, data:[B
    :goto_e
    and-int/lit8 v2, p1, 0x7

    #@10
    int-to-byte v2, v2

    #@11
    aput-byte v2, v0, v3

    #@13
    .line 1111
    array-length v2, p2

    #@14
    invoke-static {p2, v3, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@17
    .line 1114
    array-length v2, p2

    #@18
    add-int/lit8 v1, v2, 0x1

    #@1a
    .local v1, j:I
    :goto_1a
    array-length v2, v0

    #@1b
    if-ge v1, v2, :cond_28

    #@1d
    .line 1115
    const/4 v2, -0x1

    #@1e
    aput-byte v2, v0, v1

    #@20
    .line 1114
    add-int/lit8 v1, v1, 0x1

    #@22
    goto :goto_1a

    #@23
    .line 1105
    .end local v0           #data:[B
    .end local v1           #j:I
    :cond_23
    const/16 v2, 0xff

    #@25
    new-array v0, v2, [B

    #@27
    .restart local v0       #data:[B
    goto :goto_e

    #@28
    .line 1118
    .restart local v1       #j:I
    :cond_28
    return-object v0
.end method

.method public sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 14
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "destPort"
    .parameter "data"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    .line 813
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.SEND_SMS"

    #@4
    const-string v2, "Sending SMS message"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 816
    const-string v0, "SMS"

    #@b
    const/4 v1, 0x2

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_5e

    #@12
    .line 817
    new-instance v0, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v1, "sendData: destAddr="

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v1, " scAddr="

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    const-string v1, " destPort="

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, " data=\'"

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-static {p4}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    const-string v1, "\' sentIntent="

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, " deliveryIntent="

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@5e
    .line 821
    :cond_5e
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@60
    move-object v1, p1

    #@61
    move-object v2, p2

    #@62
    move v3, p3

    #@63
    move-object v4, p4

    #@64
    move-object v5, p5

    #@65
    move-object v6, p6

    #@66
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/SMSDispatcher;->sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@69
    .line 822
    return-void
.end method

.method public sendEmailoverMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .registers 16
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 966
    .local p3, parts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.SEND_SMS"

    #@4
    const-string v2, "Sending SMS message"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 970
    const/4 v6, 0x0

    #@a
    .line 972
    .local v6, i:I
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v8

    #@e
    .local v8, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_52

    #@14
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v9

    #@18
    check-cast v9, Ljava/lang/String;

    #@1a
    .line 973
    .local v9, part:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v1, "sendMultipartText: destAddr = "

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", srAddr = "

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    const-string v1, ", part["

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    add-int/lit8 v7, v6, 0x1

    #@3b
    .end local v6           #i:I
    .local v7, i:I
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, "]="

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@50
    move v6, v7

    #@51
    .end local v7           #i:I
    .restart local v6       #i:I
    goto :goto_e

    #@52
    .line 977
    .end local v9           #part:Ljava/lang/String;
    :cond_52
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@54
    move-object v3, p3

    #@55
    check-cast v3, Ljava/util/ArrayList;

    #@57
    move-object v4, p4

    #@58
    check-cast v4, Ljava/util/ArrayList;

    #@5a
    move-object v5, p5

    #@5b
    check-cast v5, Ljava/util/ArrayList;

    #@5d
    move-object v1, p1

    #@5e
    move-object v2, p2

    #@5f
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendEmailoverMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    #@62
    .line 979
    return-void
.end method

.method public sendEmailoverText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 12
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    .line 889
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.SEND_SMS"

    #@4
    const-string v2, "Sending SMS message"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 893
    new-instance v0, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v1, "sendText: destAddr = "

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    const-string v1, " scAddr = "

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    const-string v1, " text = \'"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v0

    #@2c
    const-string v1, "\' sentIntent = "

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    const-string v1, " deliveryIntent = "

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v0

    #@44
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@47
    .line 895
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@49
    move-object v1, p1

    #@4a
    move-object v2, p2

    #@4b
    move-object v3, p3

    #@4c
    move-object v4, p4

    #@4d
    move-object v5, p5

    #@4e
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendEmailoverText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@51
    .line 896
    return-void
.end method

.method public sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .registers 16
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 926
    .local p3, parts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.SEND_SMS"

    #@4
    const-string v2, "Sending SMS message"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 929
    const-string v0, "SMS"

    #@b
    const/4 v1, 0x2

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_5b

    #@12
    .line 930
    const/4 v6, 0x0

    #@13
    .line 931
    .local v6, i:I
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v8

    #@17
    .local v8, i$:Ljava/util/Iterator;
    :goto_17
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_5b

    #@1d
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v9

    #@21
    check-cast v9, Ljava/lang/String;

    #@23
    .line 932
    .local v9, part:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v1, "sendMultipartText: destAddr="

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    const-string v1, ", srAddr="

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    const-string v1, ", part["

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    add-int/lit8 v7, v6, 0x1

    #@44
    .end local v6           #i:I
    .local v7, i:I
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v0

    #@48
    const-string v1, "]="

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v0

    #@4e
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v0

    #@52
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v0

    #@56
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@59
    move v6, v7

    #@5a
    .end local v7           #i:I
    .restart local v6       #i:I
    goto :goto_17

    #@5b
    .line 936
    .end local v6           #i:I
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v9           #part:Ljava/lang/String;
    :cond_5b
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@5d
    move-object v3, p3

    #@5e
    check-cast v3, Ljava/util/ArrayList;

    #@60
    move-object v4, p4

    #@61
    check-cast v4, Ljava/util/ArrayList;

    #@63
    move-object v5, p5

    #@64
    check-cast v5, Ljava/util/ArrayList;

    #@66
    move-object v1, p1

    #@67
    move-object v2, p2

    #@68
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    #@6b
    .line 938
    return-void
.end method

.method public sendMultipartTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;III)V
    .registers 25
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1009
    .local p3, parts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    const-string v2, "android.permission.SEND_SMS"

    #@8
    const-string v3, "Sending SMS message"

    #@a
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 1012
    const-string v1, "SMS"

    #@f
    const/4 v2, 0x2

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_63

    #@16
    .line 1013
    const/4 v11, 0x0

    #@17
    .line 1014
    .local v11, i:I
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v13

    #@1b
    .local v13, i$:Ljava/util/Iterator;
    :goto_1b
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_63

    #@21
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v14

    #@25
    check-cast v14, Ljava/lang/String;

    #@27
    .line 1015
    .local v14, part:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "sendMultipartText: destAddr="

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    move-object/from16 v0, p1

    #@34
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    const-string v2, ", srAddr="

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    move-object/from16 v0, p2

    #@40
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    const-string v2, ", part["

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    add-int/lit8 v12, v11, 0x1

    #@4c
    .end local v11           #i:I
    .local v12, i:I
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    const-string v2, "]="

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@61
    move v11, v12

    #@62
    .end local v12           #i:I
    .restart local v11       #i:I
    goto :goto_1b

    #@63
    .line 1019
    .end local v11           #i:I
    .end local v13           #i$:Ljava/util/Iterator;
    .end local v14           #part:Ljava/lang/String;
    :cond_63
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@65
    move-object/from16 v4, p3

    #@67
    check-cast v4, Ljava/util/ArrayList;

    #@69
    move-object/from16 v5, p4

    #@6b
    check-cast v5, Ljava/util/ArrayList;

    #@6d
    move-object/from16 v6, p5

    #@6f
    check-cast v6, Ljava/util/ArrayList;

    #@71
    move-object/from16 v2, p1

    #@73
    move-object/from16 v3, p2

    #@75
    move-object/from16 v7, p6

    #@77
    move/from16 v8, p7

    #@79
    move/from16 v9, p8

    #@7b
    move/from16 v10, p9

    #@7d
    invoke-virtual/range {v1 .. v10}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V

    #@80
    .line 1021
    return-void
.end method

.method public sendMultipartTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;III)V
    .registers 25
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1028
    .local p3, parts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    const-string v2, "android.permission.SEND_SMS"

    #@8
    const-string v3, "Sending SMS message"

    #@a
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 1031
    const-string v1, "SMS"

    #@f
    const/4 v2, 0x2

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_63

    #@16
    .line 1032
    const/4 v11, 0x0

    #@17
    .line 1033
    .local v11, i:I
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v13

    #@1b
    .local v13, i$:Ljava/util/Iterator;
    :goto_1b
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_63

    #@21
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v14

    #@25
    check-cast v14, Ljava/lang/String;

    #@27
    .line 1034
    .local v14, part:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "sendMultipartText: destAddr="

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    move-object/from16 v0, p1

    #@34
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    const-string v2, ", srAddr="

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    move-object/from16 v0, p2

    #@40
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    const-string v2, ", part["

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    add-int/lit8 v12, v11, 0x1

    #@4c
    .end local v11           #i:I
    .local v12, i:I
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    const-string v2, "]="

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@61
    move v11, v12

    #@62
    .end local v12           #i:I
    .restart local v11       #i:I
    goto :goto_1b

    #@63
    .line 1038
    .end local v11           #i:I
    .end local v13           #i$:Ljava/util/Iterator;
    .end local v14           #part:Ljava/lang/String;
    :cond_63
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@65
    move-object/from16 v4, p3

    #@67
    check-cast v4, Ljava/util/ArrayList;

    #@69
    move-object/from16 v5, p4

    #@6b
    check-cast v5, Ljava/util/ArrayList;

    #@6d
    move-object/from16 v6, p5

    #@6f
    check-cast v6, Ljava/util/ArrayList;

    #@71
    move-object/from16 v2, p1

    #@73
    move-object/from16 v3, p2

    #@75
    move-object/from16 v7, p6

    #@77
    move/from16 v8, p7

    #@79
    move/from16 v9, p8

    #@7b
    move/from16 v10, p9

    #@7d
    invoke-virtual/range {v1 .. v10}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V

    #@80
    .line 1040
    return-void
.end method

.method public sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 12
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    .line 850
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    const-string v1, "android.permission.SEND_SMS"

    #@8
    const-string v2, "Sending SMS message"

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 853
    const-string v0, "SMS"

    #@f
    const/4 v1, 0x2

    #@10
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_54

    #@16
    .line 854
    new-instance v0, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v1, "sendText: destAddr="

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    const-string v1, " scAddr="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, " text=\'"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    const-string v1, "\' sentIntent="

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    const-string v1, " deliveryIntent="

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@54
    .line 858
    :cond_54
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->isGsmMo()Z

    #@57
    move-result v0

    #@58
    if-eqz v0, :cond_6a

    #@5a
    .line 859
    const-string v0, "sendText(), IccSmsInterfaceManager --> GsmSMSDispatcher"

    #@5c
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@5f
    .line 864
    :goto_5f
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@61
    move-object v1, p1

    #@62
    move-object v2, p2

    #@63
    move-object v3, p3

    #@64
    move-object v4, p4

    #@65
    move-object v5, p5

    #@66
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@69
    .line 865
    return-void

    #@6a
    .line 861
    :cond_6a
    const-string v0, "sendText(), IccSmsInterfaceManager --> CdmaSMSDispatcher"

    #@6c
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@6f
    goto :goto_5f
.end method

.method public sendTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V
    .registers 20
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"

    #@0
    .prologue
    .line 994
    const-string v0, "sendTextLge(), iccinterface > sendTextLge > dispatcher"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@5
    .line 995
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@a
    move-result-object v0

    #@b
    const-string v1, "android.permission.SEND_SMS"

    #@d
    const-string v2, "Sending SMS message"

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 998
    const-string v0, "SMS"

    #@14
    const/4 v1, 0x2

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_59

    #@1b
    .line 999
    new-instance v0, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v1, "sendText: destAddr="

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    const-string v1, " scAddr="

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    const-string v1, " text=\'"

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v0

    #@3a
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v0

    #@3e
    const-string v1, "\' sentIntent="

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v0

    #@48
    const-string v1, " deliveryIntent="

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v0

    #@4e
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v0

    #@52
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v0

    #@56
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@59
    .line 1003
    :cond_59
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@5b
    move-object v1, p1

    #@5c
    move-object v2, p2

    #@5d
    move-object v3, p3

    #@5e
    move-object v4, p4

    #@5f
    move-object v5, p5

    #@60
    move-object/from16 v6, p6

    #@62
    move/from16 v7, p7

    #@64
    move/from16 v8, p8

    #@66
    move/from16 v9, p9

    #@68
    invoke-virtual/range {v0 .. v9}, Lcom/android/internal/telephony/SMSDispatcher;->sendTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@6b
    .line 1004
    return-void
.end method

.method public sendTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V
    .registers 20
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"

    #@0
    .prologue
    .line 1045
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    const-string v1, "android.permission.SEND_SMS"

    #@8
    const-string v2, "Sending SMS message"

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 1048
    const-string v0, "SMS"

    #@f
    const/4 v1, 0x2

    #@10
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_54

    #@16
    .line 1049
    new-instance v0, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v1, "sendText: destAddr="

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    const-string v1, " scAddr="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, " text=\'"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    const-string v1, "\' sentIntent="

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    const-string v1, " deliveryIntent="

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@54
    .line 1053
    :cond_54
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@56
    move-object v1, p1

    #@57
    move-object v2, p2

    #@58
    move-object v3, p3

    #@59
    move-object v4, p4

    #@5a
    move-object v5, p5

    #@5b
    move-object/from16 v6, p6

    #@5d
    move/from16 v7, p7

    #@5f
    move/from16 v8, p8

    #@61
    move/from16 v9, p9

    #@63
    invoke-virtual/range {v0 .. v9}, Lcom/android/internal/telephony/SMSDispatcher;->sendTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@66
    .line 1054
    return-void
.end method

.method public setIPSMSDispatcher(Lcom/android/internal/telephony/SMSDispatcher;)V
    .registers 4
    .parameter "ipSMSDispatcher"

    #@0
    .prologue
    .line 378
    if-nez p1, :cond_e

    #@2
    .line 379
    const-string v0, "RIL_IccSms"

    #@4
    const-string v1, "switching to Normal mode"

    #@6
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 380
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mImsDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@b
    iput-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@d
    .line 386
    :goto_d
    return-void

    #@e
    .line 383
    :cond_e
    const-string v0, "RIL_IccSms"

    #@10
    const-string v1, "switching to IP mode"

    #@12
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 384
    iput-object p1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@17
    goto :goto_d
.end method

.method public setMultipartTextValidityPeriod(I)V
    .registers 3
    .parameter "validityperiod"

    #@0
    .prologue
    .line 1064
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->setMultipartTextValidityPeriod(I)V

    #@5
    .line 1065
    return-void
.end method

.method public setPremiumSmsPermission(Ljava/lang/String;I)V
    .registers 4
    .parameter "packageName"
    .parameter "permission"

    #@0
    .prologue
    .line 987
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/telephony/SMSDispatcher;->setPremiumSmsPermission(Ljava/lang/String;I)V

    #@5
    .line 988
    return-void
.end method

.method public setSmsIsRoaming(Z)V
    .registers 4
    .parameter "isRoaming"

    #@0
    .prologue
    .line 1687
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "setSmsIsRoaming(), isRoaming = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 1689
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@18
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->setSmsIsRoaming(Z)V

    #@1b
    .line 1690
    return-void
.end method

.method public setSmsPriority(I)V
    .registers 3
    .parameter "priority"

    #@0
    .prologue
    .line 1648
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->setSmsPriority(I)V

    #@5
    .line 1649
    return-void
.end method

.method public setSmscenterAddress(Ljava/lang/String;)Z
    .registers 14
    .parameter "smsc"

    #@0
    .prologue
    const/16 v11, 0x22

    #@2
    const/4 v10, 0x0

    #@3
    .line 1200
    const-string v0, ""

    #@5
    .line 1201
    .local v0, RilSmscvalue:Ljava/lang/String;
    const/4 v9, 0x1

    #@6
    new-array v1, v9, [C

    #@8
    aput-char v11, v1, v10

    #@a
    .line 1202
    .local v1, char_quote:[C
    new-instance v5, Ljava/lang/String;

    #@c
    invoke-direct {v5, v1}, Ljava/lang/String;-><init>([C)V

    #@f
    .line 1203
    .local v5, quote:Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@12
    move-result v9

    #@13
    if-nez v9, :cond_7c

    #@15
    .line 1204
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    #@18
    move-result-object v7

    #@19
    .line 1205
    .local v7, temp:[C
    array-length v4, v7

    #@1a
    .line 1206
    .local v4, length:I
    add-int/lit8 v9, v4, 0x2

    #@1c
    new-array v8, v9, [C

    #@1e
    .line 1207
    .local v8, temp1:[C
    aput-char v11, v8, v10

    #@20
    .line 1208
    const/4 v3, 0x1

    #@21
    .local v3, i:I
    :goto_21
    if-gt v3, v4, :cond_5e

    #@23
    .line 1209
    add-int/lit8 v9, v3, -0x1

    #@25
    aget-char v9, v7, v9

    #@27
    aput-char v9, v8, v3

    #@29
    .line 1210
    new-instance v9, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v10, "setSmscenterAddress(), temp1[i] = "

    #@30
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v9

    #@34
    aget-char v10, v8, v3

    #@36
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@39
    move-result-object v9

    #@3a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v9

    #@3e
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@41
    .line 1211
    new-instance v9, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v10, "setSmscenterAddress(), temp[i-1] = "

    #@48
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v9

    #@4c
    add-int/lit8 v10, v3, -0x1

    #@4e
    aget-char v10, v7, v10

    #@50
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@53
    move-result-object v9

    #@54
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v9

    #@58
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5b
    .line 1208
    add-int/lit8 v3, v3, 0x1

    #@5d
    goto :goto_21

    #@5e
    .line 1213
    :cond_5e
    add-int/lit8 v9, v4, 0x1

    #@60
    aput-char v11, v8, v9

    #@62
    .line 1214
    invoke-static {v8}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    #@65
    move-result-object v0

    #@66
    .line 1215
    new-instance v9, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v10, "setSmscenterAddress(), RilSmscvalue = "

    #@6d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v9

    #@71
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v9

    #@75
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v9

    #@79
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7c
    .line 1217
    .end local v3           #i:I
    .end local v4           #length:I
    .end local v7           #temp:[C
    .end local v8           #temp1:[C
    :cond_7c
    const-string v9, "setSmscenterAddress"

    #@7e
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->enforceReceiveAndSend(Ljava/lang/String;)V

    #@81
    .line 1218
    iget-object v10, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@83
    monitor-enter v10

    #@84
    .line 1219
    const/4 v9, 0x0

    #@85
    :try_start_85
    iput-boolean v9, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@87
    .line 1220
    iget-object v9, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@89
    const/4 v11, 0x2

    #@8a
    invoke-virtual {v9, v11}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@8d
    move-result-object v6

    #@8e
    .line 1221
    .local v6, response:Landroid/os/Message;
    iget-object v9, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@90
    iget-object v9, v9, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@92
    invoke-interface {v9, v0, v6}, Lcom/android/internal/telephony/CommandsInterface;->setSmscAddress(Ljava/lang/String;Landroid/os/Message;)V
    :try_end_95
    .catchall {:try_start_85 .. :try_end_95} :catchall_a7

    #@95
    .line 1223
    :try_start_95
    iget-object v9, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@97
    invoke-virtual {v9}, Ljava/lang/Object;->wait()V
    :try_end_9a
    .catchall {:try_start_95 .. :try_end_9a} :catchall_a7
    .catch Ljava/lang/InterruptedException; {:try_start_95 .. :try_end_9a} :catch_a0

    #@9a
    .line 1227
    :goto_9a
    :try_start_9a
    monitor-exit v10
    :try_end_9b
    .catchall {:try_start_9a .. :try_end_9b} :catchall_a7

    #@9b
    .line 1228
    iput-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSCAddr:Ljava/lang/String;

    #@9d
    .line 1229
    iget-boolean v9, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@9f
    return v9

    #@a0
    .line 1224
    :catch_a0
    move-exception v2

    #@a1
    .line 1225
    .local v2, e:Ljava/lang/InterruptedException;
    :try_start_a1
    const-string v9, "interrupted while trying to update SCAddress"

    #@a3
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@a6
    goto :goto_9a

    #@a7
    .line 1227
    .end local v2           #e:Ljava/lang/InterruptedException;
    .end local v6           #response:Landroid/os/Message;
    :catchall_a7
    move-exception v9

    #@a8
    monitor-exit v10
    :try_end_a9
    .catchall {:try_start_a1 .. :try_end_a9} :catchall_a7

    #@a9
    throw v9
.end method

.method public setUiccType(I)V
    .registers 2
    .parameter "uiccType"

    #@0
    .prologue
    .line 1638
    iput p1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mUiccType:I

    #@2
    .line 1639
    return-void
.end method

.method public startTestCase(Ljava/lang/String;I)V
    .registers 4
    .parameter "pdu"
    .parameter "num"

    #@0
    .prologue
    .line 1676
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    check-cast v0, Lcom/android/internal/telephony/ImsSMSDispatcher;

    #@4
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/telephony/ImsSMSDispatcher;->startTestCase(Ljava/lang/String;I)V

    #@7
    .line 1677
    return-void
.end method

.method public updateMessageOnIccEf(II[B)Z
    .registers 12
    .parameter "index"
    .parameter "status"
    .parameter "pdu"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 417
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v2, "updateMessageOnIccEf: index="

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    const-string v2, " status="

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, " ==> "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    const-string v2, "("

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-static {p3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v2, ")"

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@3b
    .line 420
    const-string v1, "Updating message on Icc"

    #@3d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->enforceReceiveAndSend(Ljava/lang/String;)V

    #@40
    .line 421
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@42
    monitor-enter v7

    #@43
    .line 422
    const/4 v1, 0x0

    #@44
    :try_start_44
    iput-boolean v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@46
    .line 423
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@48
    const/4 v2, 0x2

    #@49
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@4c
    move-result-object v5

    #@4d
    .line 425
    .local v5, response:Landroid/os/Message;
    if-nez p2, :cond_c1

    #@4f
    .line 432
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@51
    const-string v2, "control_uicc_storage"

    #@53
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@56
    move-result v1

    #@57
    if-eqz v1, :cond_84

    #@59
    .line 433
    invoke-direct {p0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->isCdmaPhoneType()Z

    #@5c
    move-result v1

    #@5d
    if-eqz v1, :cond_74

    #@5f
    .line 434
    const-string v1, "updateMessageOnIccEf(), PhoneType : CDMA Phone -> deleteSmsOnRuim()"

    #@61
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@64
    .line 435
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@66
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@68
    invoke-interface {v1, p1, v5}, Lcom/android/internal/telephony/CommandsInterface;->deleteSmsOnRuim(ILandroid/os/Message;)V
    :try_end_6b
    .catchall {:try_start_44 .. :try_end_6b} :catchall_81

    #@6b
    .line 471
    :goto_6b
    :try_start_6b
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@6d
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_70
    .catchall {:try_start_6b .. :try_end_70} :catchall_81
    .catch Ljava/lang/InterruptedException; {:try_start_6b .. :try_end_70} :catch_dc

    #@70
    .line 475
    :goto_70
    :try_start_70
    monitor-exit v7
    :try_end_71
    .catchall {:try_start_70 .. :try_end_71} :catchall_81

    #@71
    .line 476
    iget-boolean v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@73
    :goto_73
    return v1

    #@74
    .line 437
    :cond_74
    :try_start_74
    const-string v1, "updateMessageOnIccEf(), PhoneType : GSM Phone -> deleteSmsOnSim()"

    #@76
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@79
    .line 438
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7b
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7d
    invoke-interface {v1, p1, v5}, Lcom/android/internal/telephony/CommandsInterface;->deleteSmsOnSim(ILandroid/os/Message;)V

    #@80
    goto :goto_6b

    #@81
    .line 475
    .end local v5           #response:Landroid/os/Message;
    :catchall_81
    move-exception v1

    #@82
    monitor-exit v7
    :try_end_83
    .catchall {:try_start_74 .. :try_end_83} :catchall_81

    #@83
    throw v1

    #@84
    .line 444
    .restart local v5       #response:Landroid/os/Message;
    :cond_84
    :try_start_84
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@86
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@89
    move-result v1

    #@8a
    if-eq v4, v1, :cond_96

    #@8c
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@8e
    const-string v2, "save_usim_3gpp_in_cdma"

    #@90
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@93
    move-result v1

    #@94
    if-ne v1, v4, :cond_b9

    #@96
    .line 448
    :cond_96
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@98
    const-string v2, "check_sim_full_state"

    #@9a
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9d
    move-result v1

    #@9e
    if-eqz v1, :cond_ac

    #@a0
    .line 449
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@a2
    new-instance v2, Landroid/content/Intent;

    #@a4
    const-string v4, "com.android.prividers.telephony.deleteSim"

    #@a6
    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a9
    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@ac
    .line 452
    :cond_ac
    const-string v1, "updateMessageOnIccEf(), IccSmsInterfaceManager --> RIL"

    #@ae
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@b1
    .line 453
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b3
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@b5
    invoke-interface {v1, p1, v5}, Lcom/android/internal/telephony/CommandsInterface;->deleteSmsOnSim(ILandroid/os/Message;)V

    #@b8
    goto :goto_6b

    #@b9
    .line 455
    :cond_b9
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@bb
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@bd
    invoke-interface {v1, p1, v5}, Lcom/android/internal/telephony/CommandsInterface;->deleteSmsOnRuim(ILandroid/os/Message;)V

    #@c0
    goto :goto_6b

    #@c1
    .line 460
    :cond_c1
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@c3
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@c6
    move-result-object v0

    #@c7
    .line 461
    .local v0, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-nez v0, :cond_d0

    #@c9
    .line 462
    invoke-virtual {v5}, Landroid/os/Message;->recycle()V

    #@cc
    .line 463
    iget-boolean v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@ce
    monitor-exit v7

    #@cf
    goto :goto_73

    #@d0
    .line 465
    :cond_d0
    invoke-virtual {p0, p2, p3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->makeSmsRecordData(I[B)[B

    #@d3
    move-result-object v3

    #@d4
    .line 466
    .local v3, record:[B
    const/16 v1, 0x6f3c

    #@d6
    const/4 v4, 0x0

    #@d7
    move v2, p1

    #@d8
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFLinearFixed(II[BLjava/lang/String;Landroid/os/Message;)V

    #@db
    goto :goto_6b

    #@dc
    .line 472
    .end local v0           #fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    .end local v3           #record:[B
    :catch_dc
    move-exception v6

    #@dd
    .line 473
    .local v6, e:Ljava/lang/InterruptedException;
    const-string v1, "interrupted while trying to update by index"

    #@df
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V
    :try_end_e2
    .catchall {:try_start_84 .. :try_end_e2} :catchall_81

    #@e2
    goto :goto_70
.end method

.method public updateMessageOnIccEfMultiMode(II[BI)Z
    .registers 13
    .parameter "index"
    .parameter "status"
    .parameter "pdu"
    .parameter "smsformat"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    .line 494
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v2, "updateMessageOnIccEfMultiMode: index="

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    const-string v2, " status="

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, " ==> "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    const-string v2, "("

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-static {p3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v2, ")"

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    const-string v2, "smsformat = "

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@45
    .line 498
    const-string v1, "Updating message on UIcc"

    #@47
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->enforceReceiveAndSend(Ljava/lang/String;)V

    #@4a
    .line 499
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@4c
    monitor-enter v7

    #@4d
    .line 500
    const/4 v1, 0x0

    #@4e
    :try_start_4e
    iput-boolean v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@50
    .line 501
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@52
    const/4 v2, 0x2

    #@53
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@56
    move-result-object v5

    #@57
    .line 503
    .local v5, response:Landroid/os/Message;
    if-nez p2, :cond_89

    #@59
    .line 508
    if-ne p4, v4, :cond_70

    #@5b
    .line 509
    const-string v1, "updateMessageOnIccEfMultiMode(), SMS_FORMAT_CSIM -> deleteSmsOnRuim()"

    #@5d
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@60
    .line 510
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@62
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@64
    invoke-interface {v1, p1, v5}, Lcom/android/internal/telephony/CommandsInterface;->deleteSmsOnRuim(ILandroid/os/Message;)V
    :try_end_67
    .catchall {:try_start_4e .. :try_end_67} :catchall_80

    #@67
    .line 531
    :goto_67
    :try_start_67
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@69
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_6c
    .catchall {:try_start_67 .. :try_end_6c} :catchall_80
    .catch Ljava/lang/InterruptedException; {:try_start_67 .. :try_end_6c} :catch_a4

    #@6c
    .line 535
    :goto_6c
    :try_start_6c
    monitor-exit v7
    :try_end_6d
    .catchall {:try_start_6c .. :try_end_6d} :catchall_80

    #@6d
    .line 536
    iget-boolean v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@6f
    :goto_6f
    return v1

    #@70
    .line 511
    :cond_70
    const/4 v1, 0x1

    #@71
    if-ne p4, v1, :cond_83

    #@73
    .line 512
    :try_start_73
    const-string v1, "updateMessageOnIccEfMultiMode(), SMS_FORMAT_USIM -> deleteSmsOnSim()"

    #@75
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@78
    .line 513
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7a
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7c
    invoke-interface {v1, p1, v5}, Lcom/android/internal/telephony/CommandsInterface;->deleteSmsOnSim(ILandroid/os/Message;)V

    #@7f
    goto :goto_67

    #@80
    .line 535
    .end local v5           #response:Landroid/os/Message;
    :catchall_80
    move-exception v1

    #@81
    monitor-exit v7
    :try_end_82
    .catchall {:try_start_73 .. :try_end_82} :catchall_80

    #@82
    throw v1

    #@83
    .line 515
    .restart local v5       #response:Landroid/os/Message;
    :cond_83
    :try_start_83
    const-string v1, "updateMessageOnIccEfMultiMode(), SMS_FORMAT_NONE -> nothing"

    #@85
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@88
    goto :goto_67

    #@89
    .line 520
    :cond_89
    iget-object v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8b
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@8e
    move-result-object v0

    #@8f
    .line 521
    .local v0, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-nez v0, :cond_98

    #@91
    .line 522
    invoke-virtual {v5}, Landroid/os/Message;->recycle()V

    #@94
    .line 523
    iget-boolean v1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@96
    monitor-exit v7

    #@97
    goto :goto_6f

    #@98
    .line 525
    :cond_98
    invoke-virtual {p0, p2, p3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->makeSmsRecordData(I[B)[B

    #@9b
    move-result-object v3

    #@9c
    .line 526
    .local v3, record:[B
    const/16 v1, 0x6f3c

    #@9e
    const/4 v4, 0x0

    #@9f
    move v2, p1

    #@a0
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFLinearFixed(II[BLjava/lang/String;Landroid/os/Message;)V

    #@a3
    goto :goto_67

    #@a4
    .line 532
    .end local v0           #fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    .end local v3           #record:[B
    :catch_a4
    move-exception v6

    #@a5
    .line 533
    .local v6, e:Ljava/lang/InterruptedException;
    const-string v1, "interrupted while trying to update by index"

    #@a7
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V
    :try_end_aa
    .catchall {:try_start_83 .. :try_end_aa} :catchall_80

    #@aa
    goto :goto_6c
.end method

.method public updatePhoneObject(Lcom/android/internal/telephony/PhoneBase;)V
    .registers 3
    .parameter "phone"

    #@0
    .prologue
    .line 393
    iput-object p1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    .line 394
    iget-object v0, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@4
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->updatePhoneObject(Lcom/android/internal/telephony/PhoneBase;)V

    #@7
    .line 395
    return-void
.end method

.method public updateSmsOnSimReadStatus(IZ)Z
    .registers 11
    .parameter "index"
    .parameter "read"

    #@0
    .prologue
    .line 1591
    const-string v3, "Updating status of a SMS on SIM"

    #@2
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->enforceReceiveAndSend(Ljava/lang/String;)V

    #@5
    .line 1592
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@7
    monitor-enter v4

    #@8
    .line 1593
    const/4 v3, 0x0

    #@9
    :try_start_9
    iput-boolean v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@b
    .line 1594
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mHandler:Landroid/os/Handler;

    #@d
    const/16 v5, 0x7e

    #@f
    const/4 v6, 0x0

    #@10
    new-instance v7, Ljava/lang/Boolean;

    #@12
    invoke-direct {v7, p2}, Ljava/lang/Boolean;-><init>(Z)V

    #@15
    invoke-virtual {v3, v5, p1, v6, v7}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@18
    move-result-object v2

    #@19
    .line 1595
    .local v2, response:Landroid/os/Message;
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1b
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1e
    move-result-object v1

    #@1f
    .line 1597
    .local v1, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-nez v1, :cond_2a

    #@21
    .line 1598
    const-string v3, "updateSmsOnSimReadStatus(), Cannot load Sms records. No icc card?"

    #@23
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@26
    .line 1599
    iget-boolean v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@28
    monitor-exit v4

    #@29
    .line 1608
    :goto_29
    return v3

    #@2a
    .line 1601
    :cond_2a
    const/16 v3, 0x6f3c

    #@2c
    invoke-virtual {v1, v3, p1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V
    :try_end_2f
    .catchall {:try_start_9 .. :try_end_2f} :catchall_3f

    #@2f
    .line 1603
    :try_start_2f
    iget-object v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mLock:Ljava/lang/Object;

    #@31
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_34
    .catchall {:try_start_2f .. :try_end_34} :catchall_3f
    .catch Ljava/lang/InterruptedException; {:try_start_2f .. :try_end_34} :catch_38

    #@34
    .line 1607
    :goto_34
    :try_start_34
    monitor-exit v4
    :try_end_35
    .catchall {:try_start_34 .. :try_end_35} :catchall_3f

    #@35
    .line 1608
    iget-boolean v3, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mSuccess:Z

    #@37
    goto :goto_29

    #@38
    .line 1604
    :catch_38
    move-exception v0

    #@39
    .line 1605
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_39
    const-string v3, "updateSmsOnSimReadStatus(), interrupted while trying to update read status of sms on sim"

    #@3b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@3e
    goto :goto_34

    #@3f
    .line 1607
    .end local v0           #e:Ljava/lang/InterruptedException;
    .end local v1           #fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    .end local v2           #response:Landroid/os/Message;
    :catchall_3f
    move-exception v3

    #@40
    monitor-exit v4
    :try_end_41
    .catchall {:try_start_39 .. :try_end_41} :catchall_3f

    #@41
    throw v3
.end method
