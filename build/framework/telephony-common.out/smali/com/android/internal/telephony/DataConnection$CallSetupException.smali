.class public Lcom/android/internal/telephony/DataConnection$CallSetupException;
.super Ljava/lang/Exception;
.source "DataConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CallSetupException"
.end annotation


# instance fields
.field private mRetryOverride:I


# direct methods
.method constructor <init>(I)V
    .registers 3
    .parameter "retryOverride"

    #@0
    .prologue
    .line 329
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    #@3
    .line 327
    const/4 v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/telephony/DataConnection$CallSetupException;->mRetryOverride:I

    #@6
    .line 330
    iput p1, p0, Lcom/android/internal/telephony/DataConnection$CallSetupException;->mRetryOverride:I

    #@8
    .line 331
    return-void
.end method


# virtual methods
.method public getRetryOverride()I
    .registers 2

    #@0
    .prologue
    .line 334
    iget v0, p0, Lcom/android/internal/telephony/DataConnection$CallSetupException;->mRetryOverride:I

    #@2
    return v0
.end method
