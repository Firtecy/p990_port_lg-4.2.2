.class Lcom/android/internal/telephony/sip/SipCommandInterface;
.super Lcom/android/internal/telephony/BaseCommands;
.source "SipCommandInterface.java"

# interfaces
.implements Lcom/android/internal/telephony/CommandsInterface;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/BaseCommands;-><init>(Landroid/content/Context;)V

    #@3
    .line 40
    return-void
.end method


# virtual methods
.method public SAPConncetionrequest(ILandroid/os/Message;)V
    .registers 3
    .parameter "req"
    .parameter "result"

    #@0
    .prologue
    .line 575
    return-void
.end method

.method public SAPrequest(ILjava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "op_type"
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 573
    return-void
.end method

.method public acceptCall(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 128
    return-void
.end method

.method public acknowledgeIncomingGsmSmsWithPdu(ZLjava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "success"
    .parameter "ackPdu"
    .parameter "result"

    #@0
    .prologue
    .line 253
    return-void
.end method

.method public acknowledgeLastIncomingCdmaSms(ZILandroid/os/Message;)V
    .registers 4
    .parameter "success"
    .parameter "cause"
    .parameter "result"

    #@0
    .prologue
    .line 249
    return-void
.end method

.method public acknowledgeLastIncomingGsmSms(ZILandroid/os/Message;)V
    .registers 4
    .parameter "success"
    .parameter "cause"
    .parameter "result"

    #@0
    .prologue
    .line 245
    return-void
.end method

.method public cancelManualSearchingRequest(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 286
    return-void
.end method

.method public cancelPendingUssd(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 338
    return-void
.end method

.method public changeBarringPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "facility"
    .parameter "oldPwd"
    .parameter "newPwd"
    .parameter "result"

    #@0
    .prologue
    .line 68
    return-void
.end method

.method public changeIccPin(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "oldPin"
    .parameter "newPin"
    .parameter "result"

    #@0
    .prologue
    .line 61
    return-void
.end method

.method public changeIccPin2(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "oldPin2"
    .parameter "newPin2"
    .parameter "result"

    #@0
    .prologue
    .line 64
    return-void
.end method

.method public changeIccPin2ForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "oldPin2"
    .parameter "newPin2"
    .parameter "aidPtr"
    .parameter "response"

    #@0
    .prologue
    .line 475
    return-void
.end method

.method public changeIccPinForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "oldPin"
    .parameter "newPin"
    .parameter "aidPtr"
    .parameter "response"

    #@0
    .prologue
    .line 470
    return-void
.end method

.method public closeImsPdn(ILandroid/os/Message;)V
    .registers 3
    .parameter "reason"
    .parameter "result"

    #@0
    .prologue
    .line 517
    return-void
.end method

.method public conference(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 115
    return-void
.end method

.method public deactivateDataCall(IILandroid/os/Message;)V
    .registers 4
    .parameter "cid"
    .parameter "reason"
    .parameter "result"

    #@0
    .prologue
    .line 235
    return-void
.end method

.method public deleteSmsOnRuim(ILandroid/os/Message;)V
    .registers 3
    .parameter "index"
    .parameter "response"

    #@0
    .prologue
    .line 215
    return-void
.end method

.method public deleteSmsOnSim(ILandroid/os/Message;)V
    .registers 3
    .parameter "index"
    .parameter "response"

    #@0
    .prologue
    .line 212
    return-void
.end method

.method public dial(Ljava/lang/String;ILandroid/os/Message;)V
    .registers 4
    .parameter "address"
    .parameter "clirMode"
    .parameter "result"

    #@0
    .prologue
    .line 83
    return-void
.end method

.method public dial(Ljava/lang/String;ILcom/android/internal/telephony/UUSInfo;Landroid/os/Message;)V
    .registers 5
    .parameter "address"
    .parameter "clirMode"
    .parameter "uusInfo"
    .parameter "result"

    #@0
    .prologue
    .line 87
    return-void
.end method

.method public doNVControl(IILjava/lang/String;IILandroid/os/Message;)V
    .registers 7
    .parameter "nv_control_command"
    .parameter "nv_item_number"
    .parameter "nv_data"
    .parameter "nv_length"
    .parameter "mode"
    .parameter "result"

    #@0
    .prologue
    .line 584
    return-void
.end method

.method public emulNetworkState(I)V
    .registers 2
    .parameter "cmdID"

    #@0
    .prologue
    .line 481
    return-void
.end method

.method public exitEmergencyCallbackMode(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 446
    return-void
.end method

.method public explicitCallTransfer(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 134
    return-void
.end method

.method public getAvailableNetworks(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 298
    return-void
.end method

.method public getBasebandVersion(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 312
    return-void
.end method

.method public getCDMASubscription(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 410
    return-void
.end method

.method public getCLIR(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 263
    return-void
.end method

.method public getCdmaBroadcastConfig(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 434
    return-void
.end method

.method public getCdmaSubscriptionSource(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 394
    return-void
.end method

.method public getCurrentCalls(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 74
    return-void
.end method

.method public getDataCallList(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 80
    return-void
.end method

.method public getDataCallProfile(ILandroid/os/Message;)V
    .registers 3
    .parameter "appType"
    .parameter "result"

    #@0
    .prologue
    .line 492
    return-void
.end method

.method public getDataRegistrationState(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 176
    return-void
.end method

.method public getDebugInfo(II)[I
    .registers 4
    .parameter "type"
    .parameter "num"

    #@0
    .prologue
    .line 533
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getDeviceIdentity(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 407
    return-void
.end method

.method public getEhrpdInfoForIms(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 519
    return-void
.end method

.method public getGsmBroadcastConfig(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 397
    return-void
.end method

.method public getIMEI(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 96
    return-void
.end method

.method public getIMEISV(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 99
    return-void
.end method

.method public getIMSI(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 90
    return-void
.end method

.method public getIMSIForApp(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "aid"
    .parameter "result"

    #@0
    .prologue
    .line 93
    return-void
.end method

.method public getIccCardStatus(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 46
    return-void
.end method

.method public getImsRegistrationState(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 209
    return-void
.end method

.method public getImsTest(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 160
    return-void
.end method

.method public getLastCallFailCause(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 137
    return-void
.end method

.method public getLastDataCallFailCause(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 144
    return-void
.end method

.method public getLastPdpFailCause(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 141
    return-void
.end method

.method public getLteEmmErrorCode(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 485
    return-void
.end method

.method public getLteInfoForIms(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 518
    return-void
.end method

.method public getMute(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 150
    return-void
.end method

.method public getNeighboringCids(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 375
    return-void
.end method

.method public getNetworkSelectionMode(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 295
    return-void
.end method

.method public getNvTest(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 158
    return-void
.end method

.method public getOperator(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 179
    return-void
.end method

.method public getPDPContextList(Landroid/os/Message;)V
    .registers 2
    .parameter "result"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 77
    return-void
.end method

.method public getPcscfAddress(ILjava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "cid"
    .parameter "ipv"
    .parameter "result"

    #@0
    .prologue
    .line 522
    return-void
.end method

.method public getPreferredNetworkType(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 372
    return-void
.end method

.method public getPreferredVoicePrivacy(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 122
    return-void
.end method

.method public getRssiTest(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 170
    return-void
.end method

.method public getSearchStatus(ILandroid/os/Message;)V
    .registers 3
    .parameter "ReqType"
    .parameter "result"

    #@0
    .prologue
    .line 542
    return-void
.end method

.method public getSearchStatus(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 543
    return-void
.end method

.method public getSignalStrength(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 153
    return-void
.end method

.method public getSmscAddress(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 381
    return-void
.end method

.method public getVoiceRadioTechnology(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 489
    return-void
.end method

.method public getVoiceRegistrationState(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 173
    return-void
.end method

.method public getVssTest(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 156
    return-void
.end method

.method public handleCallSetupRequestFromSim(ZLandroid/os/Message;)V
    .registers 3
    .parameter "accept"
    .parameter "response"

    #@0
    .prologue
    .line 366
    return-void
.end method

.method public hangupConnection(ILandroid/os/Message;)V
    .registers 3
    .parameter "gsmIndex"
    .parameter "result"

    #@0
    .prologue
    .line 103
    return-void
.end method

.method public hangupForegroundResumeBackground(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 109
    return-void
.end method

.method public hangupWaitingOrBackground(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 106
    return-void
.end method

.method public iccIO(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 10
    .parameter "command"
    .parameter "fileid"
    .parameter "path"
    .parameter "p1"
    .parameter "p2"
    .parameter "p3"
    .parameter "data"
    .parameter "pin2"
    .parameter "result"

    #@0
    .prologue
    .line 257
    return-void
.end method

.method public iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 11
    .parameter "command"
    .parameter "fileid"
    .parameter "path"
    .parameter "p1"
    .parameter "p2"
    .parameter "p3"
    .parameter "data"
    .parameter "pin2"
    .parameter "aid"
    .parameter "result"

    #@0
    .prologue
    .line 260
    return-void
.end method

.method public invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .registers 3
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 344
    return-void
.end method

.method public invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "strings"
    .parameter "response"

    #@0
    .prologue
    .line 347
    return-void
.end method

.method public queryAvailableBandMode(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 353
    return-void
.end method

.method public queryCLIP(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 309
    return-void
.end method

.method public queryCallForwardStatus(IILjava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "cfReason"
    .parameter "serviceClass"
    .parameter "number"
    .parameter "response"

    #@0
    .prologue
    .line 306
    return-void
.end method

.method public queryCallWaiting(ILandroid/os/Message;)V
    .registers 3
    .parameter "serviceClass"
    .parameter "response"

    #@0
    .prologue
    .line 269
    return-void
.end method

.method public queryCdmaRoamingPreference(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 416
    return-void
.end method

.method public queryFacilityLock(Ljava/lang/String;Ljava/lang/String;ILandroid/os/Message;)V
    .registers 5
    .parameter "facility"
    .parameter "password"
    .parameter "serviceClass"
    .parameter "response"

    #@0
    .prologue
    .line 317
    return-void
.end method

.method public queryFacilityLockForApp(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "facility"
    .parameter "password"
    .parameter "serviceClass"
    .parameter "appId"
    .parameter "response"

    #@0
    .prologue
    .line 322
    return-void
.end method

.method public queryTTYMode(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 425
    return-void
.end method

.method public rejectCall(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 131
    return-void
.end method

.method public reportSmsMemoryStatus(ZLandroid/os/Message;)V
    .registers 3
    .parameter "available"
    .parameter "result"

    #@0
    .prologue
    .line 387
    return-void
.end method

.method public reportStkServiceIsRunning(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 390
    return-void
.end method

.method public requestIsimAuthentication(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "nonce"
    .parameter "response"

    #@0
    .prologue
    .line 478
    return-void
.end method

.method public resetRadio(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 341
    return-void
.end method

.method public sendBurstDtmf(Ljava/lang/String;IILandroid/os/Message;)V
    .registers 5
    .parameter "dtmfString"
    .parameter "on"
    .parameter "off"
    .parameter "result"

    #@0
    .prologue
    .line 192
    return-void
.end method

.method public sendCDMAFeatureCode(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "FeatureCode"
    .parameter "response"

    #@0
    .prologue
    .line 431
    return-void
.end method

.method public sendCdmaSms([BLandroid/os/Message;)V
    .registers 3
    .parameter "pdu"
    .parameter "result"

    #@0
    .prologue
    .line 198
    return-void
.end method

.method public sendDefaultAttachProfile(ILandroid/os/Message;)V
    .registers 3
    .parameter "profileId"
    .parameter "result"

    #@0
    .prologue
    .line 525
    return-void
.end method

.method public sendDefaultPdnTable(IILjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 12
    .parameter "pdnId"
    .parameter "apnLength"
    .parameter "apn"
    .parameter "ipType"
    .parameter "inactivityTime"
    .parameter "enable"
    .parameter "authType"
    .parameter "esminfo"
    .parameter "username"
    .parameter "password"
    .parameter "result"

    #@0
    .prologue
    .line 513
    return-void
.end method

.method public sendDtmf(CLandroid/os/Message;)V
    .registers 3
    .parameter "c"
    .parameter "result"

    #@0
    .prologue
    .line 182
    return-void
.end method

.method public sendEnvelope(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "contents"
    .parameter "response"

    #@0
    .prologue
    .line 359
    return-void
.end method

.method public sendEnvelopeWithStatus(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "contents"
    .parameter "response"

    #@0
    .prologue
    .line 362
    return-void
.end method

.method public sendImsCdmaSms([BIILandroid/os/Message;)V
    .registers 5
    .parameter "pdu"
    .parameter "retry"
    .parameter "messageRef"
    .parameter "response"

    #@0
    .prologue
    .line 206
    return-void
.end method

.method public sendImsGsmSms(Ljava/lang/String;Ljava/lang/String;IILandroid/os/Message;)V
    .registers 6
    .parameter "smscPDU"
    .parameter "pdu"
    .parameter "retry"
    .parameter "messageRef"
    .parameter "response"

    #@0
    .prologue
    .line 202
    return-void
.end method

.method public sendMPDNTable(Landroid/os/Message;Z)V
    .registers 3
    .parameter "result"
    .parameter "isotaattach"

    #@0
    .prologue
    .line 511
    return-void
.end method

.method public sendPdnTable(IILjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 12
    .parameter "pdnId"
    .parameter "apnLength"
    .parameter "apn"
    .parameter "ipType"
    .parameter "inactivityTime"
    .parameter "enable"
    .parameter "authType"
    .parameter "esminfo"
    .parameter "username"
    .parameter "password"
    .parameter "result"

    #@0
    .prologue
    .line 510
    return-void
.end method

.method public sendSMS(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "smscPDU"
    .parameter "pdu"
    .parameter "result"

    #@0
    .prologue
    .line 195
    return-void
.end method

.method public sendTerminalResponse(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "contents"
    .parameter "response"

    #@0
    .prologue
    .line 356
    return-void
.end method

.method public sendUSSD(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "ussdString"
    .parameter "response"

    #@0
    .prologue
    .line 335
    return-void
.end method

.method public separateConnection(ILandroid/os/Message;)V
    .registers 3
    .parameter "gsmIndex"
    .parameter "result"

    #@0
    .prologue
    .line 125
    return-void
.end method

.method public setBandMode(ILandroid/os/Message;)V
    .registers 3
    .parameter "bandMode"
    .parameter "response"

    #@0
    .prologue
    .line 350
    return-void
.end method

.method public setCLIR(ILandroid/os/Message;)V
    .registers 3
    .parameter "clirMode"
    .parameter "result"

    #@0
    .prologue
    .line 266
    return-void
.end method

.method public setCSGSelectionManual(ILandroid/os/Message;)V
    .registers 3
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 588
    return-void
.end method

.method public setCallForward(IIILjava/lang/String;ILandroid/os/Message;)V
    .registers 7
    .parameter "action"
    .parameter "cfReason"
    .parameter "serviceClass"
    .parameter "number"
    .parameter "timeSeconds"
    .parameter "response"

    #@0
    .prologue
    .line 302
    return-void
.end method

.method public setCallWaiting(ZILandroid/os/Message;)V
    .registers 4
    .parameter "enable"
    .parameter "serviceClass"
    .parameter "response"

    #@0
    .prologue
    .line 273
    return-void
.end method

.method public setCdmaBroadcastActivation(ZLandroid/os/Message;)V
    .registers 3
    .parameter "activate"
    .parameter "response"

    #@0
    .prologue
    .line 443
    return-void
.end method

.method public setCdmaBroadcastConfig([ILandroid/os/Message;)V
    .registers 3
    .parameter "configValuesArray"
    .parameter "response"

    #@0
    .prologue
    .line 437
    return-void
.end method

.method public setCdmaBroadcastConfig([Lcom/android/internal/telephony/cdma/CdmaSmsBroadcastConfigInfo;Landroid/os/Message;)V
    .registers 3
    .parameter "configs"
    .parameter "response"

    #@0
    .prologue
    .line 440
    return-void
.end method

.method public setCdmaEriVersion(ILandroid/os/Message;)V
    .registers 3
    .parameter "value"
    .parameter "result"

    #@0
    .prologue
    .line 454
    return-void
.end method

.method public setCdmaFactoryReset(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 496
    return-void
.end method

.method public setCdmaRoamingPreference(ILandroid/os/Message;)V
    .registers 3
    .parameter "cdmaRoamingType"
    .parameter "response"

    #@0
    .prologue
    .line 419
    return-void
.end method

.method public setCdmaSubscriptionSource(ILandroid/os/Message;)V
    .registers 3
    .parameter "cdmaSubscription"
    .parameter "response"

    #@0
    .prologue
    .line 422
    return-void
.end method

.method public setDataSubscription(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 507
    return-void
.end method

.method public setEhrpdIpv6ControlSetting(ILandroid/os/Message;)V
    .registers 3
    .parameter "enable"
    .parameter "result"

    #@0
    .prologue
    .line 529
    return-void
.end method

.method public setFacilityLock(Ljava/lang/String;ZLjava/lang/String;ILandroid/os/Message;)V
    .registers 6
    .parameter "facility"
    .parameter "lockState"
    .parameter "password"
    .parameter "serviceClass"
    .parameter "response"

    #@0
    .prologue
    .line 327
    return-void
.end method

.method public setFacilityLockForApp(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "facility"
    .parameter "lockState"
    .parameter "password"
    .parameter "serviceClass"
    .parameter "appId"
    .parameter "response"

    #@0
    .prologue
    .line 332
    return-void
.end method

.method public setGsmBroadcastActivation(ZLandroid/os/Message;)V
    .registers 3
    .parameter "activate"
    .parameter "response"

    #@0
    .prologue
    .line 403
    return-void
.end method

.method public setGsmBroadcastConfig([Lcom/android/internal/telephony/gsm/SmsBroadcastConfigInfo;Landroid/os/Message;)V
    .registers 3
    .parameter "config"
    .parameter "response"

    #@0
    .prologue
    .line 400
    return-void
.end method

.method public setLocationUpdates(ZLandroid/os/Message;)V
    .registers 3
    .parameter "enable"
    .parameter "response"

    #@0
    .prologue
    .line 378
    return-void
.end method

.method public setModemReset(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 165
    return-void
.end method

.method public setMute(ZLandroid/os/Message;)V
    .registers 3
    .parameter "enableMute"
    .parameter "response"

    #@0
    .prologue
    .line 147
    return-void
.end method

.method public setNetworkSelectionModeAutomatic(Landroid/os/Message;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 276
    return-void
.end method

.method public setNetworkSelectionModeManual(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "operatorNumeric"
    .parameter "response"

    #@0
    .prologue
    .line 280
    return-void
.end method

.method public setNetworkSelectionModeManual(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "operatorNumeric"
    .parameter "operatorRat"
    .parameter "response"

    #@0
    .prologue
    .line 291
    return-void
.end method

.method public setOnNITZTime(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 4
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 43
    return-void
.end method

.method public setPhoneType(I)V
    .registers 2
    .parameter "phoneType"

    #@0
    .prologue
    .line 413
    return-void
.end method

.method public setPreferredNetworkType(ILandroid/os/Message;)V
    .registers 3
    .parameter "networkType"
    .parameter "response"

    #@0
    .prologue
    .line 369
    return-void
.end method

.method public setPreferredVoicePrivacy(ZLandroid/os/Message;)V
    .registers 3
    .parameter "enable"
    .parameter "result"

    #@0
    .prologue
    .line 119
    return-void
.end method

.method public setPreviousNetworkSelectionModeManual(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "operatorNumeric"
    .parameter "operatorRat"
    .parameter "response"

    #@0
    .prologue
    .line 283
    return-void
.end method

.method public setQcril(I)V
    .registers 2
    .parameter "set"

    #@0
    .prologue
    .line 579
    return-void
.end method

.method public setRadioPower(ZLandroid/os/Message;)V
    .registers 3
    .parameter "on"
    .parameter "result"

    #@0
    .prologue
    .line 238
    return-void
.end method

.method public setRmnetAutoconnect(ILandroid/os/Message;)V
    .registers 3
    .parameter "param"
    .parameter "result"

    #@0
    .prologue
    .line 538
    return-void
.end method

.method public setRssiTestAntConf(ILandroid/os/Message;)V
    .registers 3
    .parameter "AntType"
    .parameter "result"

    #@0
    .prologue
    .line 169
    return-void
.end method

.method public setSmscAddress(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "address"
    .parameter "result"

    #@0
    .prologue
    .line 384
    return-void
.end method

.method public setSubscriptionMode(ILandroid/os/Message;)V
    .registers 3
    .parameter "subscriptionMode"
    .parameter "response"

    #@0
    .prologue
    .line 500
    return-void
.end method

.method public setSuppServiceNotifications(ZLandroid/os/Message;)V
    .registers 3
    .parameter "enable"
    .parameter "result"

    #@0
    .prologue
    .line 241
    return-void
.end method

.method public setTTYMode(ILandroid/os/Message;)V
    .registers 3
    .parameter "ttyMode"
    .parameter "response"

    #@0
    .prologue
    .line 428
    return-void
.end method

.method public setTestMode(I)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 480
    return-void
.end method

.method public setUiccSubscription(IIIILandroid/os/Message;)V
    .registers 6
    .parameter "slotId"
    .parameter "appIndex"
    .parameter "subId"
    .parameter "subStatus"
    .parameter "response"

    #@0
    .prologue
    .line 504
    return-void
.end method

.method public setupDataCall(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 9
    .parameter "radioTechnology"
    .parameter "profile"
    .parameter "apn"
    .parameter "user"
    .parameter "password"
    .parameter "authType"
    .parameter "protocol"
    .parameter "result"

    #@0
    .prologue
    .line 232
    return-void
.end method

.method public startDtmf(CLandroid/os/Message;)V
    .registers 3
    .parameter "c"
    .parameter "result"

    #@0
    .prologue
    .line 185
    return-void
.end method

.method public stopDtmf(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 188
    return-void
.end method

.method public supplyDepersonalization(Ljava/lang/String;ILandroid/os/Message;)V
    .registers 4
    .parameter "netpin"
    .parameter "type"
    .parameter "result"

    #@0
    .prologue
    .line 71
    return-void
.end method

.method public supplyIccPin(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "pin"
    .parameter "result"

    #@0
    .prologue
    .line 49
    return-void
.end method

.method public supplyIccPin2(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "pin"
    .parameter "result"

    #@0
    .prologue
    .line 55
    return-void
.end method

.method public supplyIccPin2ForApp(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "pin2"
    .parameter "aid"
    .parameter "response"

    #@0
    .prologue
    .line 462
    return-void
.end method

.method public supplyIccPinForApp(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "pin"
    .parameter "aid"
    .parameter "response"

    #@0
    .prologue
    .line 450
    return-void
.end method

.method public supplyIccPuk(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "puk"
    .parameter "newPin"
    .parameter "result"

    #@0
    .prologue
    .line 52
    return-void
.end method

.method public supplyIccPuk2(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "puk"
    .parameter "newPin2"
    .parameter "result"

    #@0
    .prologue
    .line 58
    return-void
.end method

.method public supplyIccPuk2ForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "puk2"
    .parameter "newPin2"
    .parameter "aid"
    .parameter "response"

    #@0
    .prologue
    .line 466
    return-void
.end method

.method public supplyIccPukForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "puk"
    .parameter "newPin"
    .parameter "aid"
    .parameter "response"

    #@0
    .prologue
    .line 458
    return-void
.end method

.method public switchWaitingOrHoldingAndActive(Landroid/os/Message;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 112
    return-void
.end method

.method public uiccAkaAuthenticate(I[B[BLandroid/os/Message;)V
    .registers 5
    .parameter "sessionId"
    .parameter "rand"
    .parameter "autn"
    .parameter "response"

    #@0
    .prologue
    .line 556
    return-void
.end method

.method public uiccApplicationIO(IIILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 11
    .parameter "sessionId"
    .parameter "command"
    .parameter "fileid"
    .parameter "path"
    .parameter "p1"
    .parameter "p2"
    .parameter "p3"
    .parameter "data"
    .parameter "pin"
    .parameter "response"

    #@0
    .prologue
    .line 561
    return-void
.end method

.method public uiccDeactivateApplication(ILandroid/os/Message;)V
    .registers 3
    .parameter "sessionId"
    .parameter "response"

    #@0
    .prologue
    .line 552
    return-void
.end method

.method public uiccGbaAuthenticateBootstrap(I[B[BLandroid/os/Message;)V
    .registers 5
    .parameter "sessionId"
    .parameter "rand"
    .parameter "autn"
    .parameter "response"

    #@0
    .prologue
    .line 565
    return-void
.end method

.method public uiccGbaAuthenticateNaf(I[BLandroid/os/Message;)V
    .registers 4
    .parameter "sessionId"
    .parameter "nafId"
    .parameter "response"

    #@0
    .prologue
    .line 568
    return-void
.end method

.method public uiccSelectApplication(Ljava/lang/String;Landroid/os/Message;)V
    .registers 3
    .parameter "aid"
    .parameter "response"

    #@0
    .prologue
    .line 549
    return-void
.end method

.method public writeSmsToCsim(I[BLandroid/os/Message;)V
    .registers 4
    .parameter "status"
    .parameter "pdu"
    .parameter "response"

    #@0
    .prologue
    .line 226
    return-void
.end method

.method public writeSmsToRuim(ILjava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "status"
    .parameter "pdu"
    .parameter "response"

    #@0
    .prologue
    .line 221
    return-void
.end method

.method public writeSmsToSim(ILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "status"
    .parameter "smsc"
    .parameter "pdu"
    .parameter "response"

    #@0
    .prologue
    .line 218
    return-void
.end method
