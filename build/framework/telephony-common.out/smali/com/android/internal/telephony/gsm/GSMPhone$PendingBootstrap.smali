.class Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;
.super Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;
.source "GSMPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gsm/GSMPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PendingBootstrap"
.end annotation


# instance fields
.field private mAutn:[B

.field private mRand:[B

.field final synthetic this$0:Lcom/android/internal/telephony/gsm/GSMPhone;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/gsm/GSMPhone;[B[BLandroid/os/Message;)V
    .registers 5
    .parameter
    .parameter "rand"
    .parameter "autn"
    .parameter "onComplete"

    #@0
    .prologue
    .line 3234
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2
    .line 3235
    invoke-direct {p0, p1, p4}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Landroid/os/Message;)V

    #@5
    .line 3236
    iput-object p2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;->mRand:[B

    #@7
    iput-object p3, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;->mAutn:[B

    #@9
    .line 3237
    return-void
.end method


# virtual methods
.method public onSessionStarted(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .registers 10
    .parameter "res"
    .parameter "e"

    #@0
    .prologue
    .line 3241
    if-eqz p2, :cond_6

    #@2
    .line 3242
    invoke-super {p0, p1, p2}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;->onSessionStarted(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@5
    .line 3247
    :goto_5
    return-void

    #@6
    .line 3244
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@8
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@a
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@c
    invoke-static {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$1600(Lcom/android/internal/telephony/gsm/GSMPhone;)I

    #@f
    move-result v1

    #@10
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;->mRand:[B

    #@12
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;->mAutn:[B

    #@14
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@16
    const/16 v5, 0x29

    #@18
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;->mOnComplete:Landroid/os/Message;

    #@1a
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1d
    move-result-object v4

    #@1e
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/CommandsInterface;->uiccGbaAuthenticateBootstrap(I[B[BLandroid/os/Message;)V

    #@21
    goto :goto_5
.end method
