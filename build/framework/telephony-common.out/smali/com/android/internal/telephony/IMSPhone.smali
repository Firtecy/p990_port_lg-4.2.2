.class public Lcom/android/internal/telephony/IMSPhone;
.super Lcom/android/internal/telephony/IIMSPhone$Stub;
.source "IMSPhone.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/IMSPhone$1;,
        Lcom/android/internal/telephony/IMSPhone$MyHandler;
    }
.end annotation


# static fields
.field private static final BAL_ITEM_RADIO_IMS_CALL_STATE:I = 0xb

.field private static final BAL_ITEM_RADIO_SIP_REG_SENDING:I = 0xa

.field private static final BAL_ITEM_RADIO_VOLTE_NV_ITEMS:I = 0xc

.field private static DEBUG_LOG:Z = false

.field private static final OPERATOR:Ljava/lang/String; = null

.field private static final OPERATOR_ATT:Ljava/lang/String; = "ATT"

.field private static final OPERATOR_KDDI:Ljava/lang/String; = "KDDI"

.field private static final OPERATOR_KT:Ljava/lang/String; = "KT"

.field private static final OPERATOR_LGU:Ljava/lang/String; = "LGU"

.field private static final OPERATOR_SKT:Ljava/lang/String; = "SKT"

.field private static final OPERATOR_TMUS:Ljava/lang/String; = "TMO"

.field private static final OPERATOR_VZW:Ljava/lang/String; = "VZW"

.field private static final PHONE_STATE_1X_REGISTRATION:I = 0x2

.field private static final PHONE_STATE_CDMA_LOCK_INFO:I = 0x1

.field private static final PHONE_STATE_ICC_EF_READ_DONE:I = 0x3

.field private static final RIL_ICC_EF_LINEAR_FIXED_READ_DONE:I = 0x65

.field private static final RIL_ICC_EF_TRANSPARENT_READ_DONE:I = 0x66

.field private static final RIL_IMS_STAUS_SET_DONE_FOR_DAN:I = 0x67

.field private static final RIL_RESPONSE:I = 0x64

.field private static final SYS_INFO_ACCESS_NETWORK_INFO:I = 0x3

.field private static final SYS_INFO_APN:I = 0x9

.field private static final SYS_INFO_CLOSE_DATA_CONNECTION:I = 0x5

.field private static final SYS_INFO_CS_CALL_PROTECTION:I = 0x6

.field private static final SYS_INFO_ICC_EF_READ:I = 0x7

.field private static final SYS_INFO_IMS_REGISTRATION_STATE:I = 0x4

.field private static final SYS_INFO_IMS_REG_STATUS_FOR_DAN:I = 0xb

.field private static final SYS_INFO_LTE_NETWORK_FEATURE:I = 0x8

.field private static final SYS_INFO_MODEM_INFO:I = 0x1

.field private static final SYS_INFO_PCSCF_ADDRESS:I = 0x2

.field private static final SYS_INFO_SRVCC_CALL_INFO:I = 0xa

.field private static final TAG:Ljava/lang/String; = "LGIMS"

.field private static mContext:Landroid/content/Context;

.field private static mIMSPhone:Lcom/android/internal/telephony/IMSPhone;

.field private static mSysMonitor:Lcom/android/internal/telephony/ISysMonitor;


# instance fields
.field private mCM:Lcom/android/internal/telephony/CommandsInterface;

.field private mConnectedNumber:Ljava/lang/String;

.field private mDataConnMngr:Lcom/android/internal/telephony/DataConnectionManager;

.field private mHandler:Landroid/os/Handler;

.field private mPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 53
    const-string v0, "ro.build.target_operator"

    #@3
    const-string v1, ""

    #@5
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    sput-object v0, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@b
    .line 98
    sput-object v2, Lcom/android/internal/telephony/IMSPhone;->mIMSPhone:Lcom/android/internal/telephony/IMSPhone;

    #@d
    .line 99
    sput-object v2, Lcom/android/internal/telephony/IMSPhone;->mContext:Landroid/content/Context;

    #@f
    .line 100
    sput-object v2, Lcom/android/internal/telephony/IMSPhone;->mSysMonitor:Lcom/android/internal/telephony/ISysMonitor;

    #@11
    .line 101
    const/4 v0, 0x1

    #@12
    sput-boolean v0, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@14
    return-void
.end method

.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 141
    invoke-direct {p0}, Lcom/android/internal/telephony/IIMSPhone$Stub;-><init>()V

    #@4
    .line 103
    iput-object v0, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@6
    .line 104
    iput-object v0, p0, Lcom/android/internal/telephony/IMSPhone;->mHandler:Landroid/os/Handler;

    #@8
    .line 105
    iput-object v0, p0, Lcom/android/internal/telephony/IMSPhone;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@a
    .line 106
    iput-object v0, p0, Lcom/android/internal/telephony/IMSPhone;->mDataConnMngr:Lcom/android/internal/telephony/DataConnectionManager;

    #@c
    .line 109
    iput-object v0, p0, Lcom/android/internal/telephony/IMSPhone;->mConnectedNumber:Ljava/lang/String;

    #@e
    .line 142
    const-string v0, "user"

    #@10
    const-string v1, "ro.build.type"

    #@12
    const-string v2, "userdebug"

    #@14
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 143
    const/4 v0, 0x0

    #@1f
    sput-boolean v0, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@21
    .line 146
    :cond_21
    sget-boolean v0, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@23
    if-eqz v0, :cond_2c

    #@25
    .line 147
    const-string v0, "LGIMS"

    #@27
    const-string v1, "IMSPhone is created..."

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 149
    :cond_2c
    return-void
.end method

.method static synthetic access$100()Z
    .registers 1

    #@0
    .prologue
    .line 50
    sget-boolean v0, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@2
    return v0
.end method

.method public static getInstance()Lcom/android/internal/telephony/IMSPhone;
    .registers 2

    #@0
    .prologue
    .line 112
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->mIMSPhone:Lcom/android/internal/telephony/IMSPhone;

    #@2
    if-nez v0, :cond_1d

    #@4
    .line 113
    new-instance v0, Lcom/android/internal/telephony/IMSPhone;

    #@6
    invoke-direct {v0}, Lcom/android/internal/telephony/IMSPhone;-><init>()V

    #@9
    sput-object v0, Lcom/android/internal/telephony/IMSPhone;->mIMSPhone:Lcom/android/internal/telephony/IMSPhone;

    #@b
    .line 114
    const-string v0, "com.lge.ims.phone"

    #@d
    sget-object v1, Lcom/android/internal/telephony/IMSPhone;->mIMSPhone:Lcom/android/internal/telephony/IMSPhone;

    #@f
    invoke-static {v0, v1}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@12
    .line 116
    sget-boolean v0, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@14
    if-eqz v0, :cond_1d

    #@16
    .line 117
    const-string v0, "LGIMS"

    #@18
    const-string v1, "IMS phone sevice is Added to service manager"

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 121
    :cond_1d
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->mIMSPhone:Lcom/android/internal/telephony/IMSPhone;

    #@1f
    return-object v0
.end method

.method public static getSysMonitor()Lcom/android/internal/telephony/ISysMonitor;
    .registers 2

    #@0
    .prologue
    .line 197
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->mSysMonitor:Lcom/android/internal/telephony/ISysMonitor;

    #@2
    if-nez v0, :cond_f

    #@4
    .line 198
    sget-boolean v0, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 199
    const-string v0, "LGIMS"

    #@a
    const-string v1, "ISysMonitor is null"

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 206
    :cond_f
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->mSysMonitor:Lcom/android/internal/telephony/ISysMonitor;

    #@11
    return-object v0
.end method

.method public static isPhoneRequired(Landroid/content/Context;)Z
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 125
    sget-object v2, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@3
    const-string v3, "LGU"

    #@5
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_15

    #@b
    sget-object v2, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@d
    const-string v3, "VZW"

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_16

    #@15
    .line 137
    :cond_15
    :goto_15
    return v1

    #@16
    .line 129
    :cond_16
    if-eqz p0, :cond_26

    #@18
    .line 130
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1b
    move-result-object v0

    #@1c
    .line 132
    .local v0, pm:Landroid/content/pm/PackageManager;
    if-eqz v0, :cond_26

    #@1e
    const-string v2, "com.lge.ims.imsphone"

    #@20
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@23
    move-result v2

    #@24
    if-nez v2, :cond_15

    #@26
    .line 137
    .end local v0           #pm:Landroid/content/pm/PackageManager;
    :cond_26
    const/4 v1, 0x0

    #@27
    goto :goto_15
.end method

.method private setBalItem(II)V
    .registers 7
    .parameter "item"
    .parameter "data"

    #@0
    .prologue
    .line 630
    sget-boolean v1, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@2
    if-eqz v1, :cond_2c

    #@4
    .line 631
    const-string v1, "LGIMS"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "setBalItem :: item ("

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "), data ("

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, ")"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 634
    :cond_2c
    iget-object v1, p0, Lcom/android/internal/telephony/IMSPhone;->mHandler:Landroid/os/Handler;

    #@2e
    if-nez v1, :cond_3c

    #@30
    .line 635
    sget-boolean v1, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@32
    if-eqz v1, :cond_3b

    #@34
    .line 636
    const-string v1, "LGIMS"

    #@36
    const-string v2, "mHandler is null"

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 712
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 641
    :cond_3c
    iget-object v1, p0, Lcom/android/internal/telephony/IMSPhone;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3e
    if-nez v1, :cond_4c

    #@40
    .line 642
    sget-boolean v1, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@42
    if-eqz v1, :cond_3b

    #@44
    .line 643
    const-string v1, "LGIMS"

    #@46
    const-string v2, "mCM is null"

    #@48
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    goto :goto_3b

    #@4c
    .line 648
    :cond_4c
    const/4 v0, -0x1

    #@4d
    .line 650
    .local v0, index:I
    sget-object v1, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@4f
    const-string v2, "LGU"

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v1

    #@55
    if-eqz v1, :cond_93

    #@57
    .line 651
    packed-switch p1, :pswitch_data_140

    #@5a
    .line 657
    sget-boolean v1, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@5c
    if-eqz v1, :cond_7c

    #@5e
    .line 658
    const-string v1, "LGIMS"

    #@60
    new-instance v2, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v3, "Unknown item ("

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    const-string v3, ")"

    #@71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v2

    #@75
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v2

    #@79
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 703
    :cond_7c
    :goto_7c
    const/4 v1, -0x1

    #@7d
    if-eq v0, v1, :cond_3b

    #@7f
    .line 707
    iget-object v1, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@81
    if-eqz v1, :cond_3b

    #@83
    .line 711
    iget-object v1, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@85
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mHandler:Landroid/os/Handler;

    #@87
    const/16 v3, 0x64

    #@89
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@8c
    move-result-object v2

    #@8d
    invoke-interface {v1, v0, p2, v2}, Lcom/android/internal/telephony/Phone;->setModemIntegerItem(IILandroid/os/Message;)V

    #@90
    goto :goto_3b

    #@91
    .line 653
    :pswitch_91
    const/4 v0, 0x7

    #@92
    .line 654
    goto :goto_7c

    #@93
    .line 662
    :cond_93
    sget-object v1, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@95
    const-string v2, "SKT"

    #@97
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9a
    move-result v1

    #@9b
    if-eqz v1, :cond_c7

    #@9d
    .line 663
    packed-switch p1, :pswitch_data_146

    #@a0
    .line 669
    sget-boolean v1, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@a2
    if-eqz v1, :cond_7c

    #@a4
    .line 670
    const-string v1, "LGIMS"

    #@a6
    new-instance v2, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v3, "Unknown item ("

    #@ad
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v2

    #@b1
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v2

    #@b5
    const-string v3, ")"

    #@b7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v2

    #@bb
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v2

    #@bf
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    goto :goto_7c

    #@c3
    .line 665
    :pswitch_c3
    const v0, 0x6002d

    #@c6
    .line 666
    goto :goto_7c

    #@c7
    .line 674
    :cond_c7
    sget-object v1, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@c9
    const-string v2, "KT"

    #@cb
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ce
    move-result v1

    #@cf
    if-eqz v1, :cond_fb

    #@d1
    .line 675
    packed-switch p1, :pswitch_data_14c

    #@d4
    .line 681
    sget-boolean v1, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@d6
    if-eqz v1, :cond_7c

    #@d8
    .line 682
    const-string v1, "LGIMS"

    #@da
    new-instance v2, Ljava/lang/StringBuilder;

    #@dc
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@df
    const-string v3, "Unknown item ("

    #@e1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v2

    #@e5
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v2

    #@e9
    const-string v3, ")"

    #@eb
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v2

    #@ef
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v2

    #@f3
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f6
    goto :goto_7c

    #@f7
    .line 677
    :pswitch_f7
    const v0, 0x6002d

    #@fa
    .line 678
    goto :goto_7c

    #@fb
    .line 686
    :cond_fb
    sget-object v1, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@fd
    const-string v2, "TMO"

    #@ff
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@102
    move-result v1

    #@103
    if-nez v1, :cond_10f

    #@105
    sget-object v1, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@107
    const-string v2, "ATT"

    #@109
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10c
    move-result v1

    #@10d
    if-eqz v1, :cond_7c

    #@10f
    .line 687
    :cond_10f
    packed-switch p1, :pswitch_data_152

    #@112
    .line 696
    sget-boolean v1, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@114
    if-eqz v1, :cond_7c

    #@116
    .line 697
    const-string v1, "LGIMS"

    #@118
    new-instance v2, Ljava/lang/StringBuilder;

    #@11a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11d
    const-string v3, "Unknown item ("

    #@11f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v2

    #@123
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@126
    move-result-object v2

    #@127
    const-string v3, ")"

    #@129
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v2

    #@12d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v2

    #@131
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@134
    goto/16 :goto_7c

    #@136
    .line 689
    :pswitch_136
    const v0, 0x6002d

    #@139
    .line 690
    goto/16 :goto_7c

    #@13b
    .line 693
    :pswitch_13b
    const/16 v0, 0x1e

    #@13d
    .line 694
    goto/16 :goto_7c

    #@13f
    .line 651
    nop

    #@140
    :pswitch_data_140
    .packed-switch 0xa
        :pswitch_91
    .end packed-switch

    #@146
    .line 663
    :pswitch_data_146
    .packed-switch 0xb
        :pswitch_c3
    .end packed-switch

    #@14c
    .line 675
    :pswitch_data_14c
    .packed-switch 0xb
        :pswitch_f7
    .end packed-switch

    #@152
    .line 687
    :pswitch_data_152
    .packed-switch 0xb
        :pswitch_136
        :pswitch_13b
    .end packed-switch
.end method


# virtual methods
.method public getConnectedNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 716
    iget-object v0, p0, Lcom/android/internal/telephony/IMSPhone;->mConnectedNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSysInfo(IILjava/lang/String;)[Ljava/lang/String;
    .registers 11
    .parameter "item"
    .parameter "param"
    .parameter "paramEx"

    #@0
    .prologue
    const v4, 0xffff

    #@3
    const/4 v1, 0x0

    #@4
    const/4 v3, 0x1

    #@5
    const/4 v6, 0x0

    #@6
    .line 409
    packed-switch p1, :pswitch_data_17a

    #@9
    .line 539
    :pswitch_9
    sget-boolean v2, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@b
    if-eqz v2, :cond_2b

    #@d
    .line 540
    const-string v2, "LGIMS"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "Unknown item ("

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, ")"

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 545
    :cond_2b
    :goto_2b
    return-object v1

    #@2c
    .line 413
    :pswitch_2c
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@2e
    if-eqz v2, :cond_2b

    #@30
    .line 417
    const/4 v2, 0x4

    #@31
    if-ne p2, v2, :cond_57

    #@33
    .line 418
    if-eqz p3, :cond_3d

    #@35
    if-eqz p3, :cond_4a

    #@37
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_4a

    #@3d
    .line 419
    :cond_3d
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@3f
    sget-object v3, Lcom/android/internal/telephony/IPVersion;->INET:Lcom/android/internal/telephony/IPVersion;

    #@41
    invoke-virtual {v3}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-interface {v2, v3}, Lcom/android/internal/telephony/Phone;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    goto :goto_2b

    #@4a
    .line 421
    :cond_4a
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@4c
    sget-object v3, Lcom/android/internal/telephony/IPVersion;->INET:Lcom/android/internal/telephony/IPVersion;

    #@4e
    invoke-virtual {v3}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@51
    move-result-object v3

    #@52
    invoke-interface {v2, v3, p3}, Lcom/android/internal/telephony/Phone;->getPcscfAddress(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    #@55
    move-result-object v1

    #@56
    goto :goto_2b

    #@57
    .line 423
    :cond_57
    const/4 v2, 0x6

    #@58
    if-ne p2, v2, :cond_7e

    #@5a
    .line 424
    if-eqz p3, :cond_64

    #@5c
    if-eqz p3, :cond_71

    #@5e
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    #@61
    move-result v2

    #@62
    if-eqz v2, :cond_71

    #@64
    .line 425
    :cond_64
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@66
    sget-object v3, Lcom/android/internal/telephony/IPVersion;->INET6:Lcom/android/internal/telephony/IPVersion;

    #@68
    invoke-virtual {v3}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    invoke-interface {v2, v3}, Lcom/android/internal/telephony/Phone;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    goto :goto_2b

    #@71
    .line 427
    :cond_71
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@73
    sget-object v3, Lcom/android/internal/telephony/IPVersion;->INET6:Lcom/android/internal/telephony/IPVersion;

    #@75
    invoke-virtual {v3}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@78
    move-result-object v3

    #@79
    invoke-interface {v2, v3, p3}, Lcom/android/internal/telephony/Phone;->getPcscfAddress(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    #@7c
    move-result-object v1

    #@7d
    goto :goto_2b

    #@7e
    .line 431
    :cond_7e
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@80
    invoke-interface {v2, p3}, Lcom/android/internal/telephony/Phone;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@83
    move-result-object v1

    #@84
    goto :goto_2b

    #@85
    .line 438
    :pswitch_85
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@87
    if-eqz v2, :cond_2b

    #@89
    .line 442
    const/16 v2, 0xd

    #@8b
    if-ne p2, v2, :cond_94

    #@8d
    .line 443
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@8f
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getLteInfo()[Ljava/lang/String;

    #@92
    move-result-object v1

    #@93
    goto :goto_2b

    #@94
    .line 444
    :cond_94
    const/16 v2, 0xe

    #@96
    if-ne p2, v2, :cond_2b

    #@98
    .line 445
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@9a
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getCdmaInfo()[Ljava/lang/String;

    #@9d
    move-result-object v1

    #@9e
    goto :goto_2b

    #@9f
    .line 453
    :pswitch_9f
    new-array v1, v3, [Ljava/lang/String;

    #@a1
    .line 455
    .local v1, result:[Ljava/lang/String;
    const-string v2, "FAILURE"

    #@a3
    aput-object v2, v1, v6

    #@a5
    .line 457
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@a7
    if-eqz v2, :cond_2b

    #@a9
    .line 461
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mHandler:Landroid/os/Handler;

    #@ab
    if-eqz v2, :cond_2b

    #@ad
    .line 465
    if-eqz p3, :cond_b7

    #@af
    if-eqz p3, :cond_b9

    #@b1
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    #@b4
    move-result v2

    #@b5
    if-nez v2, :cond_b9

    #@b7
    .line 466
    :cond_b7
    const-string p3, "linear_fixed"

    #@b9
    .line 469
    :cond_b9
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@bb
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@be
    move-result-object v0

    #@bf
    .line 471
    .local v0, iccFH:Lcom/android/internal/telephony/uicc/IccFileHandler;
    if-eqz v0, :cond_2b

    #@c1
    .line 472
    const-string v2, "linear_fixed"

    #@c3
    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c6
    move-result v2

    #@c7
    if-eqz v2, :cond_e0

    #@c9
    .line 474
    and-int v2, p2, v4

    #@cb
    iget-object v3, p0, Lcom/android/internal/telephony/IMSPhone;->mHandler:Landroid/os/Handler;

    #@cd
    const/16 v4, 0x65

    #@cf
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d2
    move-result-object v5

    #@d3
    invoke-virtual {v3, v4, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@d6
    move-result-object v3

    #@d7
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V

    #@da
    .line 476
    const-string v2, "SUCCESS"

    #@dc
    aput-object v2, v1, v6

    #@de
    goto/16 :goto_2b

    #@e0
    .line 477
    :cond_e0
    const-string v2, "transparent"

    #@e2
    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e5
    move-result v2

    #@e6
    if-eqz v2, :cond_2b

    #@e8
    .line 478
    and-int v2, p2, v4

    #@ea
    iget-object v3, p0, Lcom/android/internal/telephony/IMSPhone;->mHandler:Landroid/os/Handler;

    #@ec
    const/16 v4, 0x66

    #@ee
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f1
    move-result-object v5

    #@f2
    invoke-virtual {v3, v4, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@f5
    move-result-object v3

    #@f6
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@f9
    .line 480
    const-string v2, "SUCCESS"

    #@fb
    aput-object v2, v1, v6

    #@fd
    goto/16 :goto_2b

    #@ff
    .line 490
    .end local v0           #iccFH:Lcom/android/internal/telephony/uicc/IccFileHandler;
    .end local v1           #result:[Ljava/lang/String;
    :pswitch_ff
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@101
    if-eqz v2, :cond_2b

    #@103
    .line 494
    const/4 v1, 0x0

    #@104
    .line 498
    .restart local v1       #result:[Ljava/lang/String;
    if-ne p2, v3, :cond_14f

    #@106
    .line 499
    new-array v1, v3, [Ljava/lang/String;

    #@108
    .line 501
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@10a
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->isEmergencyCallSupportedOnLte()Z

    #@10d
    move-result v2

    #@10e
    if-eqz v2, :cond_148

    #@110
    .line 502
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@113
    move-result-object v2

    #@114
    aput-object v2, v1, v6

    #@116
    .line 516
    :cond_116
    :goto_116
    if-eqz v1, :cond_2b

    #@118
    .line 517
    sget-boolean v2, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@11a
    if-eqz v2, :cond_2b

    #@11c
    .line 518
    const-string v2, "LGIMS"

    #@11e
    new-instance v3, Ljava/lang/StringBuilder;

    #@120
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@123
    const-string v4, "LTE network feature :: param("

    #@125
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v3

    #@129
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v3

    #@12d
    const-string v4, "), result("

    #@12f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v3

    #@133
    aget-object v4, v1, v6

    #@135
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v3

    #@139
    const-string v4, ")"

    #@13b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v3

    #@13f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@142
    move-result-object v3

    #@143
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    goto/16 :goto_2b

    #@148
    .line 504
    :cond_148
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@14b
    move-result-object v2

    #@14c
    aput-object v2, v1, v6

    #@14e
    goto :goto_116

    #@14f
    .line 506
    :cond_14f
    const/4 v2, 0x2

    #@150
    if-ne p2, v2, :cond_116

    #@152
    .line 507
    new-array v1, v3, [Ljava/lang/String;

    #@154
    .line 509
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@156
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->isVoiceCallSupprotedOnLte()Z

    #@159
    move-result v2

    #@15a
    if-eqz v2, :cond_163

    #@15c
    .line 510
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@15f
    move-result-object v2

    #@160
    aput-object v2, v1, v6

    #@162
    goto :goto_116

    #@163
    .line 512
    :cond_163
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@166
    move-result-object v2

    #@167
    aput-object v2, v1, v6

    #@169
    goto :goto_116

    #@16a
    .line 527
    .end local v1           #result:[Ljava/lang/String;
    :pswitch_16a
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@16c
    if-eqz v2, :cond_2b

    #@16e
    .line 531
    new-array v1, v3, [Ljava/lang/String;

    #@170
    .line 533
    .restart local v1       #result:[Ljava/lang/String;
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@172
    invoke-interface {v2, p3}, Lcom/android/internal/telephony/Phone;->getActiveApnHost(Ljava/lang/String;)Ljava/lang/String;

    #@175
    move-result-object v2

    #@176
    aput-object v2, v1, v6

    #@178
    goto/16 :goto_2b

    #@17a
    .line 409
    :pswitch_data_17a
    .packed-switch 0x2
        :pswitch_2c
        :pswitch_85
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9f
        :pswitch_ff
        :pswitch_16a
    .end packed-switch
.end method

.method public handleRemoteException()V
    .registers 4

    #@0
    .prologue
    .line 210
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/android/internal/telephony/IMSPhone;->mSysMonitor:Lcom/android/internal/telephony/ISysMonitor;

    #@3
    .line 212
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->mContext:Landroid/content/Context;

    #@5
    new-instance v1, Landroid/content/Intent;

    #@7
    const-string v2, "com.lge.ims.action.IMS_PHONE_STARTED"

    #@9
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@f
    .line 214
    sget-boolean v0, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@11
    if-eqz v0, :cond_1a

    #@13
    .line 215
    const-string v0, "LGIMS"

    #@15
    const-string v1, "handling remote exception"

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 217
    :cond_1a
    return-void
.end method

.method public setDefaultPhone(Landroid/content/Context;Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 7
    .parameter "context"
    .parameter "phone"
    .parameter "cm"

    #@0
    .prologue
    .line 152
    sget-boolean v0, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 153
    const-string v0, "LGIMS"

    #@6
    const-string v1, "setDefaultPhone is called..."

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 156
    :cond_b
    if-eqz p1, :cond_11

    #@d
    if-eqz p2, :cond_11

    #@f
    if-nez p3, :cond_1d

    #@11
    .line 157
    :cond_11
    sget-boolean v0, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@13
    if-eqz v0, :cond_1c

    #@15
    .line 158
    const-string v0, "LGIMS"

    #@17
    const-string v1, "context or phone or cm is null"

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 194
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 163
    :cond_1d
    sput-object p1, Lcom/android/internal/telephony/IMSPhone;->mContext:Landroid/content/Context;

    #@1f
    .line 164
    iput-object p3, p0, Lcom/android/internal/telephony/IMSPhone;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@21
    .line 165
    iput-object p2, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@23
    .line 167
    iget-object v0, p0, Lcom/android/internal/telephony/IMSPhone;->mHandler:Landroid/os/Handler;

    #@25
    if-nez v0, :cond_2f

    #@27
    .line 168
    new-instance v0, Lcom/android/internal/telephony/IMSPhone$MyHandler;

    #@29
    const/4 v1, 0x0

    #@2a
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/IMSPhone$MyHandler;-><init>(Lcom/android/internal/telephony/IMSPhone;Lcom/android/internal/telephony/IMSPhone$1;)V

    #@2d
    iput-object v0, p0, Lcom/android/internal/telephony/IMSPhone;->mHandler:Landroid/os/Handler;

    #@2f
    .line 171
    :cond_2f
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@31
    const-string v1, "ATT"

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_46

    #@39
    .line 193
    :cond_39
    :goto_39
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->mContext:Landroid/content/Context;

    #@3b
    new-instance v1, Landroid/content/Intent;

    #@3d
    const-string v2, "com.lge.ims.action.IMS_PHONE_STARTED"

    #@3f
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@42
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@45
    goto :goto_1c

    #@46
    .line 173
    :cond_46
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@48
    const-string v1, "KT"

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v0

    #@4e
    if-nez v0, :cond_39

    #@50
    .line 175
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@52
    const-string v1, "LGU"

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v0

    #@58
    if-eqz v0, :cond_5f

    #@5a
    .line 176
    iget-object v0, p0, Lcom/android/internal/telephony/IMSPhone;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5c
    if-eqz v0, :cond_39

    #@5e
    goto :goto_39

    #@5f
    .line 180
    :cond_5f
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@61
    const-string v1, "SKT"

    #@63
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v0

    #@67
    if-eqz v0, :cond_76

    #@69
    .line 181
    iget-object v0, p0, Lcom/android/internal/telephony/IMSPhone;->mDataConnMngr:Lcom/android/internal/telephony/DataConnectionManager;

    #@6b
    if-nez v0, :cond_39

    #@6d
    .line 182
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->mContext:Landroid/content/Context;

    #@6f
    invoke-static {v0}, Lcom/android/internal/telephony/DataConnectionManager;->getInstance(Landroid/content/Context;)Lcom/android/internal/telephony/DataConnectionManager;

    #@72
    move-result-object v0

    #@73
    iput-object v0, p0, Lcom/android/internal/telephony/IMSPhone;->mDataConnMngr:Lcom/android/internal/telephony/DataConnectionManager;

    #@75
    goto :goto_39

    #@76
    .line 184
    :cond_76
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@78
    const-string v1, "TMO"

    #@7a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7d
    move-result v0

    #@7e
    if-nez v0, :cond_39

    #@80
    .line 186
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@82
    const-string v1, "VZW"

    #@84
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@87
    move-result v0

    #@88
    if-nez v0, :cond_39

    #@8a
    .line 188
    sget-object v0, Lcom/android/internal/telephony/IMSPhone;->OPERATOR:Ljava/lang/String;

    #@8c
    const-string v1, "KDDI"

    #@8e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@91
    move-result v0

    #@92
    if-eqz v0, :cond_39

    #@94
    goto :goto_39
.end method

.method public setListener(Lcom/android/internal/telephony/ISysMonitor;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 622
    sget-boolean v0, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@2
    if-eqz v0, :cond_1c

    #@4
    .line 623
    const-string v0, "LGIMS"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "setListener listener "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 625
    :cond_1c
    sput-object p1, Lcom/android/internal/telephony/IMSPhone;->mSysMonitor:Lcom/android/internal/telephony/ISysMonitor;

    #@1e
    .line 626
    return-void
.end method

.method public setSysInfo(IIILjava/lang/String;)V
    .registers 9
    .parameter "item"
    .parameter "param1"
    .parameter "param2"
    .parameter "paramEx"

    #@0
    .prologue
    .line 550
    packed-switch p1, :pswitch_data_96

    #@3
    .line 614
    :pswitch_3
    sget-boolean v1, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@5
    if-eqz v1, :cond_25

    #@7
    .line 615
    const-string v1, "LGIMS"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Unknown item ("

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    const-string v3, ")"

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 619
    :cond_25
    :goto_25
    return-void

    #@26
    .line 553
    :pswitch_26
    invoke-direct {p0, p2, p3}, Lcom/android/internal/telephony/IMSPhone;->setBalItem(II)V

    #@29
    goto :goto_25

    #@2a
    .line 559
    :pswitch_2a
    iget-object v1, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@2c
    if-eqz v1, :cond_25

    #@2e
    .line 563
    sget-boolean v1, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@30
    if-eqz v1, :cond_4a

    #@32
    .line 564
    const-string v1, "LGIMS"

    #@34
    new-instance v2, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v3, "REG STATE = "

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 567
    :cond_4a
    if-lez p2, :cond_53

    #@4c
    const/4 v0, 0x1

    #@4d
    .line 569
    .local v0, regState:Z
    :goto_4d
    iget-object v1, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@4f
    invoke-interface {v1, v0}, Lcom/android/internal/telephony/Phone;->setIMSRegistate(Z)V

    #@52
    goto :goto_25

    #@53
    .line 567
    .end local v0           #regState:Z
    :cond_53
    const/4 v0, 0x0

    #@54
    goto :goto_4d

    #@55
    .line 576
    :pswitch_55
    iget-object v1, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@57
    if-eqz v1, :cond_25

    #@59
    .line 580
    iget-object v1, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@5b
    invoke-interface {v1, p2}, Lcom/android/internal/telephony/Phone;->closeImsPdn(I)V

    #@5e
    goto :goto_25

    #@5f
    .line 586
    :pswitch_5f
    iget-object v1, p0, Lcom/android/internal/telephony/IMSPhone;->mDataConnMngr:Lcom/android/internal/telephony/DataConnectionManager;

    #@61
    if-eqz v1, :cond_25

    #@63
    goto :goto_25

    #@64
    .line 593
    :pswitch_64
    iput-object p4, p0, Lcom/android/internal/telephony/IMSPhone;->mConnectedNumber:Ljava/lang/String;

    #@66
    goto :goto_25

    #@67
    .line 600
    :pswitch_67
    iget-object v1, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@69
    if-eqz v1, :cond_25

    #@6b
    .line 605
    sget-boolean v1, Lcom/android/internal/telephony/IMSPhone;->DEBUG_LOG:Z

    #@6d
    if-eqz v1, :cond_87

    #@6f
    .line 606
    const-string v1, "LGIMS"

    #@71
    new-instance v2, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v3, "IMS REG is DONE"

    #@78
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v2

    #@7c
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v2

    #@80
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v2

    #@84
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 608
    :cond_87
    iget-object v1, p0, Lcom/android/internal/telephony/IMSPhone;->mPhone:Lcom/android/internal/telephony/Phone;

    #@89
    iget-object v2, p0, Lcom/android/internal/telephony/IMSPhone;->mHandler:Landroid/os/Handler;

    #@8b
    const/16 v3, 0x67

    #@8d
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@90
    move-result-object v2

    #@91
    invoke-interface {v1, p2, v2}, Lcom/android/internal/telephony/Phone;->setImsStatusForDan(ILandroid/os/Message;)V

    #@94
    goto :goto_25

    #@95
    .line 550
    nop

    #@96
    :pswitch_data_96
    .packed-switch 0x1
        :pswitch_26
        :pswitch_3
        :pswitch_3
        :pswitch_2a
        :pswitch_55
        :pswitch_5f
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_64
        :pswitch_67
    .end packed-switch
.end method
