.class public Lcom/android/internal/telephony/LgeTimeZoneMonitor;
.super Landroid/os/Handler;
.source "LgeTimeZoneMonitor.java"


# static fields
.field public static final EVENT_CHECK_LOST_NITZ_INFO:I = 0xb

.field public static final EVENT_CHECK_NITZ_RECEIVED:I = 0x4

.field public static final EVENT_CLEAR_NITZ_RECEIVED:I = 0x5

.field public static final EVENT_NETWORK_ATTACHED:I = 0x1

.field public static final EVENT_NETWORK_TIMEZONE_SETTING:I = 0x3

.field public static final EVENT_NITZ_RECEIVED:I = 0x2

.field public static final EVENT_REVERT_TO_NITZ_TIMEZONE:I = 0x6

.field private static final LOG_TAG:Ljava/lang/String; = "CALL_FRW"

.field private static final NITZ_VALIDITY_DURATION:I = 0xafc8

.field private static final SHAREDPREF_KEY_LAST_CHECKED:Ljava/lang/String; = "last_checked"

.field private static final SHAREDPREF_KEY_LAST_MCC:Ljava/lang/String; = "last_mcc"

.field private static final SHAREDPREF_KEY_LAST_NITZMCC:Ljava/lang/String; = "last_nitzmcc"

.field private static final SHAREDPREF_TIMEZONEMONITOR:Ljava/lang/String; = "timezone_mon"

.field private static mLostNitzInfo:Landroid/os/AsyncResult;

.field private static mWaitingTimeout:I

.field private static sInstance:Lcom/android/internal/telephony/LgeTimeZoneMonitor;


# instance fields
.field private mAutoTimeZoneObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mInitialized:Z

.field private mLastChecked:Z

.field private mLastMcc:Ljava/lang/String;

.field private mLastMccChanged:Z

.field private mLastNitzMcc:Ljava/lang/String;

.field private mLocalAreas:[Ljava/lang/String;

.field mManagedTimeZoneSettingDialog:Landroid/app/AlertDialog;

.field private mNetworkTimeZoneReceived:Z

.field private mNitzReceived:Z

.field private mNitzReceiver:Landroid/content/BroadcastReceiver;

.field private mSST:Lcom/android/internal/telephony/ServiceStateTracker;

.field private mSupportManagedTimeSetting:Z

.field private mSupportManualSettingPopup:Z

.field onManagedTimeZoneSettingDialogClick:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 50
    const/16 v0, 0x3a98

    #@2
    sput v0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mWaitingTimeout:I

    #@4
    .line 74
    const/4 v0, 0x0

    #@5
    sput-object v0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLostNitzInfo:Landroid/os/AsyncResult;

    #@7
    .line 103
    new-instance v0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@9
    invoke-direct {v0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;-><init>()V

    #@c
    sput-object v0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->sInstance:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@e
    return-void
.end method

.method private constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    .line 100
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@6
    .line 57
    iput-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@8
    .line 58
    iput-boolean v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mInitialized:Z

    #@a
    .line 59
    const-string v0, ""

    #@c
    iput-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@e
    .line 60
    iput-boolean v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastChecked:Z

    #@10
    .line 61
    const-string v0, ""

    #@12
    iput-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastNitzMcc:Ljava/lang/String;

    #@14
    .line 62
    iput-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSST:Lcom/android/internal/telephony/ServiceStateTracker;

    #@16
    .line 64
    iput-boolean v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSupportManagedTimeSetting:Z

    #@18
    .line 65
    iput-boolean v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSupportManualSettingPopup:Z

    #@1a
    .line 66
    iput-boolean v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@1c
    .line 67
    iput-boolean v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNetworkTimeZoneReceived:Z

    #@1e
    .line 68
    iput-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLocalAreas:[Ljava/lang/String;

    #@20
    .line 69
    iput-boolean v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMccChanged:Z

    #@22
    .line 71
    iput-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mManagedTimeZoneSettingDialog:Landroid/app/AlertDialog;

    #@24
    .line 77
    new-instance v0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$1;

    #@26
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor$1;-><init>(Lcom/android/internal/telephony/LgeTimeZoneMonitor;)V

    #@29
    iput-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceiver:Landroid/content/BroadcastReceiver;

    #@2b
    .line 91
    new-instance v0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$2;

    #@2d
    new-instance v1, Landroid/os/Handler;

    #@2f
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@32
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor$2;-><init>(Lcom/android/internal/telephony/LgeTimeZoneMonitor;Landroid/os/Handler;)V

    #@35
    iput-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mAutoTimeZoneObserver:Landroid/database/ContentObserver;

    #@37
    .line 282
    new-instance v0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$3;

    #@39
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor$3;-><init>(Lcom/android/internal/telephony/LgeTimeZoneMonitor;)V

    #@3c
    iput-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->onManagedTimeZoneSettingDialogClick:Landroid/content/DialogInterface$OnClickListener;

    #@3e
    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/LgeTimeZoneMonitor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->receivedNetworkSetting()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/LgeTimeZoneMonitor;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private clearPopupIfAny()V
    .registers 2

    #@0
    .prologue
    .line 258
    iget-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mManagedTimeZoneSettingDialog:Landroid/app/AlertDialog;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 259
    const-string v0, "Clear manual timezone setting popup"

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@9
    .line 260
    iget-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mManagedTimeZoneSettingDialog:Landroid/app/AlertDialog;

    #@b
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    #@e
    .line 261
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mManagedTimeZoneSettingDialog:Landroid/app/AlertDialog;

    #@11
    .line 263
    :cond_11
    return-void
.end method

.method private static getArray(Ljava/lang/String;)[Ljava/lang/String;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 174
    if-nez p0, :cond_4

    #@2
    const/4 v3, 0x0

    #@3
    .line 190
    :cond_3
    return-object v3

    #@4
    .line 176
    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    #@6
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@9
    .line 178
    .local v1, list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/StringTokenizer;

    #@b
    const-string v4, ","

    #@d
    invoke-direct {v2, p0, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 180
    .local v2, st:Ljava/util/StringTokenizer;
    :goto_10
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_1e

    #@16
    .line 181
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1d
    goto :goto_10

    #@1e
    .line 184
    :cond_1e
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@21
    move-result v4

    #@22
    new-array v3, v4, [Ljava/lang/String;

    #@24
    .line 186
    .local v3, strArray:[Ljava/lang/String;
    const/4 v0, 0x0

    #@25
    .local v0, i:I
    :goto_25
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@28
    move-result v4

    #@29
    if-ge v0, v4, :cond_3

    #@2b
    .line 187
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v4

    #@2f
    check-cast v4, Ljava/lang/String;

    #@31
    aput-object v4, v3, v0

    #@33
    .line 186
    add-int/lit8 v0, v0, 0x1

    #@35
    goto :goto_25
.end method

.method private getAutoTime()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 159
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v2

    #@7
    const-string v3, "auto_time"

    #@9
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_c
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_c} :catch_12

    #@c
    move-result v2

    #@d
    if-lez v2, :cond_10

    #@f
    .line 161
    :goto_f
    return v1

    #@10
    .line 159
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f

    #@12
    .line 160
    :catch_12
    move-exception v0

    #@13
    .line 161
    .local v0, snfe:Landroid/provider/Settings$SettingNotFoundException;
    goto :goto_f
.end method

.method private getAutoTimeZone()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 167
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v2

    #@7
    const-string v3, "auto_time_zone"

    #@9
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_c
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_c} :catch_12

    #@c
    move-result v2

    #@d
    if-lez v2, :cond_10

    #@f
    .line 169
    :goto_f
    return v1

    #@10
    .line 167
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f

    #@12
    .line 168
    :catch_12
    move-exception v0

    #@13
    .line 169
    .local v0, snfe:Landroid/provider/Settings$SettingNotFoundException;
    goto :goto_f
.end method

.method public static getDefault()Lcom/android/internal/telephony/LgeTimeZoneMonitor;
    .registers 1

    #@0
    .prologue
    .line 106
    sget-object v0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->sInstance:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@2
    return-object v0
.end method

.method private isLocalArea(Ljava/lang/String;)Z
    .registers 8
    .parameter "cur_mcc"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 194
    iget-object v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLocalAreas:[Ljava/lang/String;

    #@3
    if-nez v5, :cond_6

    #@5
    .line 203
    :cond_5
    :goto_5
    return v4

    #@6
    .line 196
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLocalAreas:[Ljava/lang/String;

    #@8
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@9
    .local v3, len$:I
    const/4 v2, 0x0

    #@a
    .local v2, i$:I
    :goto_a
    if-ge v2, v3, :cond_5

    #@c
    aget-object v1, v0, v2

    #@e
    .line 197
    .local v1, el:Ljava/lang/String;
    if-eqz v1, :cond_44

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v5

    #@14
    if-eqz v5, :cond_44

    #@16
    .line 198
    new-instance v4, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v5, "mLocalAreas = "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    iget-object v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLocalAreas:[Ljava/lang/String;

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, ", el = "

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    const-string v5, ", cur_mcc = "

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@42
    .line 199
    const/4 v4, 0x1

    #@43
    goto :goto_5

    #@44
    .line 196
    :cond_44
    add-int/lit8 v2, v2, 0x1

    #@46
    goto :goto_a
.end method

.method private receivedNetworkSetting()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 220
    iget-boolean v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSupportManagedTimeSetting:Z

    #@3
    if-nez v0, :cond_b

    #@5
    .line 221
    const-string v0, "[receivedNetworkSetting()] Auto timezone setting is not set"

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@a
    .line 229
    :goto_a
    return-void

    #@b
    .line 224
    :cond_b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->removeMessages(I)V

    #@e
    .line 225
    const/4 v0, 0x5

    #@f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->removeMessages(I)V

    #@12
    .line 227
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->sendEmptyMessage(I)Z

    #@15
    .line 228
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->clearPopupIfAny()V

    #@18
    goto :goto_a
.end method

.method private registerForTelephonyIntents()V
    .registers 4

    #@0
    .prologue
    .line 149
    iget-object v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 155
    :goto_4
    return-void

    #@5
    .line 151
    :cond_5
    new-instance v0, Landroid/content/IntentFilter;

    #@7
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@a
    .line 153
    .local v0, intentFilter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.NETWORK_SET_TIMEZONE"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 154
    iget-object v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@11
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceiver:Landroid/content/BroadcastReceiver;

    #@13
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@16
    goto :goto_4
.end method

.method private revertToNitzTimeZone()V
    .registers 5

    #@0
    .prologue
    .line 232
    iget-boolean v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSupportManagedTimeSetting:Z

    #@2
    if-eqz v2, :cond_e

    #@4
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->getAutoTimeZone()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_e

    #@a
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSST:Lcom/android/internal/telephony/ServiceStateTracker;

    #@c
    if-nez v2, :cond_f

    #@e
    .line 245
    :cond_e
    :goto_e
    return-void

    #@f
    .line 234
    :cond_f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "Reverting to NITZ TimeZone: mLastMcc = "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    iget-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, ", mNitzReceived = "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    iget-boolean v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@33
    .line 236
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSST:Lcom/android/internal/telephony/ServiceStateTracker;

    #@35
    iget-object v2, v2, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@37
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    .line 237
    .local v1, opnum:Ljava/lang/String;
    if-eqz v1, :cond_e

    #@3d
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@40
    move-result v2

    #@41
    const/4 v3, 0x4

    #@42
    if-le v2, v3, :cond_e

    #@44
    .line 238
    const/4 v2, 0x0

    #@45
    const/4 v3, 0x3

    #@46
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    .line 239
    .local v0, mcc:Ljava/lang/String;
    const-string v2, "450"

    #@4c
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v2

    #@50
    if-nez v2, :cond_e

    #@52
    const-string v2, "000"

    #@54
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v2

    #@58
    if-nez v2, :cond_e

    #@5a
    const-string v2, "001"

    #@5c
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v2

    #@60
    if-nez v2, :cond_e

    #@62
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->isLocalArea(Ljava/lang/String;)Z

    #@65
    move-result v2

    #@66
    if-nez v2, :cond_e

    #@68
    .line 240
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@6a
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6d
    move-result v2

    #@6e
    if-eqz v2, :cond_e

    #@70
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@72
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@75
    move-result v2

    #@76
    const/4 v3, 0x2

    #@77
    if-le v2, v3, :cond_e

    #@79
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@7b
    iget-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastNitzMcc:Ljava/lang/String;

    #@7d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@80
    move-result v2

    #@81
    if-nez v2, :cond_e

    #@83
    .line 241
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@85
    iget-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@87
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->changeLocalTimeForMcc(Landroid/content/Context;Ljava/lang/String;)Z

    #@8a
    goto :goto_e
.end method

.method public static setLostNitzInfo(Landroid/os/AsyncResult;)V
    .registers 2
    .parameter "lostNitzInfo"

    #@0
    .prologue
    .line 410
    sget-object v0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLostNitzInfo:Landroid/os/AsyncResult;

    #@2
    if-eq v0, p0, :cond_6

    #@4
    sput-object p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLostNitzInfo:Landroid/os/AsyncResult;

    #@6
    .line 411
    :cond_6
    return-void
.end method

.method private showPopup()V
    .registers 5

    #@0
    .prologue
    .line 267
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@2
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@7
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@a
    move-result-object v2

    #@b
    const v3, 0x20902dc

    #@e
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@19
    move-result-object v1

    #@1a
    const v2, 0x104000a

    #@1d
    iget-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->onManagedTimeZoneSettingDialogClick:Landroid/content/DialogInterface$OnClickListener;

    #@1f
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@22
    move-result-object v1

    #@23
    const/high16 v2, 0x104

    #@25
    iget-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->onManagedTimeZoneSettingDialogClick:Landroid/content/DialogInterface$OnClickListener;

    #@27
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@2e
    move-result-object v0

    #@2f
    .line 274
    .local v0, managedTimeSettingDialog:Landroid/app/AlertDialog;
    if-eqz v0, :cond_44

    #@31
    .line 275
    const-string v1, "Show manual timezone setting popup"

    #@33
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@36
    .line 276
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@39
    move-result-object v1

    #@3a
    const/16 v2, 0x7d3

    #@3c
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@3f
    .line 277
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@42
    .line 278
    iput-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mManagedTimeZoneSettingDialog:Landroid/app/AlertDialog;

    #@44
    .line 280
    :cond_44
    return-void
.end method

.method private triggerFakeNitzEvent(Landroid/os/AsyncResult;)V
    .registers 3
    .parameter "lostNitzInfo"

    #@0
    .prologue
    .line 414
    iget-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSST:Lcom/android/internal/telephony/ServiceStateTracker;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 415
    iget-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSST:Lcom/android/internal/telephony/ServiceStateTracker;

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/ServiceStateTracker;->sendNitzEvent(Landroid/os/AsyncResult;)V

    #@9
    .line 416
    const/4 v0, 0x0

    #@a
    invoke-static {v0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->setLostNitzInfo(Landroid/os/AsyncResult;)V

    #@d
    .line 418
    :cond_d
    return-void
.end method

.method private updateLastNitzMcc()V
    .registers 6

    #@0
    .prologue
    .line 248
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@2
    if-eqz v2, :cond_28

    #@4
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@6
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@9
    move-result v2

    #@a
    const/4 v3, 0x2

    #@b
    if-le v2, v3, :cond_28

    #@d
    .line 249
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@f
    const-string v3, "timezone_mon"

    #@11
    const/4 v4, 0x0

    #@12
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@15
    move-result-object v1

    #@16
    .line 250
    .local v1, prefs:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@19
    move-result-object v0

    #@1a
    .line 251
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "last_nitzmcc"

    #@1c
    iget-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@1e
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@21
    .line 252
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@24
    .line 253
    iget-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@26
    iput-object v2, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastNitzMcc:Ljava/lang/String;

    #@28
    .line 255
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v1           #prefs:Landroid/content/SharedPreferences;
    :cond_28
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 14
    .parameter "msg"

    #@0
    .prologue
    const-wide/32 v10, 0xafc8

    #@3
    const/4 v6, 0x5

    #@4
    const/4 v9, 0x4

    #@5
    const/4 v8, 0x1

    #@6
    const/4 v7, 0x0

    #@7
    .line 305
    iget v5, p1, Landroid/os/Message;->what:I

    #@9
    packed-switch v5, :pswitch_data_1d8

    #@c
    .line 406
    :cond_c
    :goto_c
    :pswitch_c
    return-void

    #@d
    .line 308
    :pswitch_d
    iget-object v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSST:Lcom/android/internal/telephony/ServiceStateTracker;

    #@f
    if-eqz v5, :cond_c

    #@11
    .line 310
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSupportManagedTimeSetting:Z

    #@13
    if-nez v5, :cond_1b

    #@15
    .line 311
    const-string v5, "Auto timezone setting is not set"

    #@17
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@1a
    goto :goto_c

    #@1b
    .line 315
    :cond_1b
    iput-boolean v7, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMccChanged:Z

    #@1d
    .line 316
    iget-object v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSST:Lcom/android/internal/telephony/ServiceStateTracker;

    #@1f
    iget-object v5, v5, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@21
    invoke-virtual {v5}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    .line 317
    .local v2, opnum:Ljava/lang/String;
    if-eqz v2, :cond_ee

    #@27
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@2a
    move-result v5

    #@2b
    if-le v5, v9, :cond_ee

    #@2d
    .line 318
    const/4 v5, 0x3

    #@2e
    invoke-virtual {v2, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    .line 319
    .local v1, mcc:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v6, "Current mcc = "

    #@39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@48
    .line 320
    const-string v5, "000"

    #@4a
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v5

    #@4e
    if-nez v5, :cond_ee

    #@50
    const-string v5, "001"

    #@52
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v5

    #@56
    if-nez v5, :cond_ee

    #@58
    .line 321
    new-instance v5, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v6, "mLastMcc = "

    #@5f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v5

    #@63
    iget-object v6, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    const-string v6, ", mLastChecked = "

    #@6b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    iget-boolean v6, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastChecked:Z

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v5

    #@79
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@7c
    .line 322
    iget-object v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@7e
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@81
    move-result v5

    #@82
    if-eqz v5, :cond_88

    #@84
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastChecked:Z

    #@86
    if-nez v5, :cond_ee

    #@88
    .line 323
    :cond_88
    iput-object v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@8a
    .line 324
    iput-boolean v7, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastChecked:Z

    #@8c
    .line 325
    iput-boolean v8, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMccChanged:Z

    #@8e
    .line 327
    iget-object v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@90
    const-string v6, "timezone_mon"

    #@92
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@95
    move-result-object v3

    #@96
    .line 328
    .local v3, prefs:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@99
    move-result-object v0

    #@9a
    .line 329
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v5, "last_mcc"

    #@9c
    iget-object v6, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@9e
    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@a1
    .line 331
    new-instance v5, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v6, "mNitzReceived = "

    #@a8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v5

    #@ac
    iget-boolean v6, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@ae
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v5

    #@b2
    const-string v6, ", mNetworkTimeZoneReceived = "

    #@b4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v5

    #@b8
    iget-boolean v6, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNetworkTimeZoneReceived:Z

    #@ba
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v5

    #@be
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v5

    #@c2
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@c5
    .line 332
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@c7
    if-nez v5, :cond_cd

    #@c9
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNetworkTimeZoneReceived:Z

    #@cb
    if-eqz v5, :cond_10c

    #@cd
    .line 333
    :cond_cd
    iput-boolean v8, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastChecked:Z

    #@cf
    .line 334
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@d1
    if-eqz v5, :cond_e0

    #@d3
    .line 335
    const-string v5, "last_nitzmcc"

    #@d5
    iget-object v6, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@d7
    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@da
    .line 336
    iget-object v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@dc
    iput-object v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastNitzMcc:Ljava/lang/String;

    #@de
    .line 337
    iput-boolean v7, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMccChanged:Z

    #@e0
    .line 344
    :cond_e0
    :goto_e0
    const-string v6, "last_checked"

    #@e2
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastChecked:Z

    #@e4
    if-eqz v5, :cond_141

    #@e6
    const-string v5, "true"

    #@e8
    :goto_e8
    invoke-interface {v0, v6, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@eb
    .line 345
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@ee
    .line 349
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v1           #mcc:Ljava/lang/String;
    .end local v3           #prefs:Landroid/content/SharedPreferences;
    :cond_ee
    new-instance v5, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    const-string v6, "mLastChecked = "

    #@f5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v5

    #@f9
    iget-boolean v6, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastChecked:Z

    #@fb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v5

    #@ff
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@102
    move-result-object v5

    #@103
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@106
    .line 350
    iput-boolean v7, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@108
    .line 351
    iput-boolean v7, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNetworkTimeZoneReceived:Z

    #@10a
    goto/16 :goto_c

    #@10c
    .line 339
    .restart local v0       #editor:Landroid/content/SharedPreferences$Editor;
    .restart local v1       #mcc:Ljava/lang/String;
    .restart local v3       #prefs:Landroid/content/SharedPreferences;
    :cond_10c
    iget-object v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@10e
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->isLocalArea(Ljava/lang/String;)Z

    #@111
    move-result v5

    #@112
    if-nez v5, :cond_e0

    #@114
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->getAutoTimeZone()Z

    #@117
    move-result v5

    #@118
    if-eqz v5, :cond_e0

    #@11a
    .line 340
    iput-boolean v8, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastChecked:Z

    #@11c
    .line 341
    new-instance v5, Ljava/lang/StringBuilder;

    #@11e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@121
    const-string v6, "Waiting NITZ from attached network for "

    #@123
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v5

    #@127
    sget v6, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mWaitingTimeout:I

    #@129
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v5

    #@12d
    const-string v6, " msec."

    #@12f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v5

    #@133
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@136
    move-result-object v5

    #@137
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@13a
    .line 342
    sget v5, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mWaitingTimeout:I

    #@13c
    int-to-long v5, v5

    #@13d
    invoke-virtual {p0, v9, v5, v6}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->sendEmptyMessageDelayed(IJ)Z

    #@140
    goto :goto_e0

    #@141
    .line 344
    :cond_141
    const-string v5, "false"

    #@143
    goto :goto_e8

    #@144
    .line 355
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v1           #mcc:Ljava/lang/String;
    .end local v2           #opnum:Ljava/lang/String;
    .end local v3           #prefs:Landroid/content/SharedPreferences;
    :pswitch_144
    const-string v5, "NITZ is received from network."

    #@146
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@149
    .line 356
    iput-boolean v8, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@14b
    .line 357
    invoke-virtual {p0, v6, v10, v11}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->sendEmptyMessageDelayed(IJ)Z

    #@14e
    goto/16 :goto_c

    #@150
    .line 361
    :pswitch_150
    const-string v5, "Network time or timezone setting intent received"

    #@152
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@155
    .line 362
    iput-boolean v8, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNetworkTimeZoneReceived:Z

    #@157
    .line 363
    invoke-virtual {p0, v6, v10, v11}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->sendEmptyMessageDelayed(IJ)Z

    #@15a
    goto/16 :goto_c

    #@15c
    .line 367
    :pswitch_15c
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@15e
    if-nez v5, :cond_199

    #@160
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNetworkTimeZoneReceived:Z

    #@162
    if-nez v5, :cond_199

    #@164
    .line 368
    new-instance v5, Ljava/lang/StringBuilder;

    #@166
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@169
    const-string v6, "NITZ is not received even waiting for "

    #@16b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v5

    #@16f
    sget v6, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mWaitingTimeout:I

    #@171
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@174
    move-result-object v5

    #@175
    const-string v6, " msec."

    #@177
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v5

    #@17b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17e
    move-result-object v5

    #@17f
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@182
    .line 369
    iget-object v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@184
    iget-object v6, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@186
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->changeLocalTimeForMcc(Landroid/content/Context;Ljava/lang/String;)Z

    #@189
    move-result v4

    #@18a
    .line 370
    .local v4, tzresult:Z
    if-nez v4, :cond_193

    #@18c
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSupportManualSettingPopup:Z

    #@18e
    if-eqz v5, :cond_193

    #@190
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->showPopup()V

    #@193
    .line 375
    .end local v4           #tzresult:Z
    :cond_193
    :goto_193
    iput-boolean v7, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@195
    .line 376
    iput-boolean v7, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNetworkTimeZoneReceived:Z

    #@197
    goto/16 :goto_c

    #@199
    .line 371
    :cond_199
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@19b
    if-eqz v5, :cond_193

    #@19d
    .line 372
    const-string v5, "EVENT_CHECK_NITZ_RECEIVED: NITZ received, updateLastNitzMcc()"

    #@19f
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@1a2
    .line 373
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->updateLastNitzMcc()V

    #@1a5
    goto :goto_193

    #@1a6
    .line 380
    :pswitch_1a6
    const-string v5, "Expire current NITZ info. 45000 msec elapsed since it received."

    #@1a8
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@1ab
    .line 381
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@1ad
    if-eqz v5, :cond_1bb

    #@1af
    iget-boolean v5, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMccChanged:Z

    #@1b1
    if-eqz v5, :cond_1bb

    #@1b3
    .line 382
    const-string v5, "EVENT_CLEAR_NITZ_RECEIVED: updateLastNitzMcc()"

    #@1b5
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@1b8
    .line 383
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->updateLastNitzMcc()V

    #@1bb
    .line 385
    :cond_1bb
    iput-boolean v7, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMccChanged:Z

    #@1bd
    .line 386
    iput-boolean v7, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNitzReceived:Z

    #@1bf
    .line 387
    iput-boolean v7, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mNetworkTimeZoneReceived:Z

    #@1c1
    goto/16 :goto_c

    #@1c3
    .line 391
    :pswitch_1c3
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->revertToNitzTimeZone()V

    #@1c6
    goto/16 :goto_c

    #@1c8
    .line 396
    :pswitch_1c8
    sget-object v5, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLostNitzInfo:Landroid/os/AsyncResult;

    #@1ca
    if-eqz v5, :cond_c

    #@1cc
    .line 397
    const-string v5, "There is lost NITZ info. Trigger fake NITZ reception event."

    #@1ce
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@1d1
    .line 398
    sget-object v5, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLostNitzInfo:Landroid/os/AsyncResult;

    #@1d3
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->triggerFakeNitzEvent(Landroid/os/AsyncResult;)V

    #@1d6
    goto/16 :goto_c

    #@1d8
    .line 305
    :pswitch_data_1d8
    .packed-switch 0x1
        :pswitch_d
        :pswitch_144
        :pswitch_150
        :pswitch_15c
        :pswitch_1a6
        :pswitch_1c3
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_1c8
    .end packed-switch
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 422
    const-string v0, "CALL_FRW"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[LgeTimeZoneMonitor] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 423
    return-void
.end method

.method public receivedNitz()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 207
    iget-boolean v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSupportManagedTimeSetting:Z

    #@3
    if-nez v0, :cond_b

    #@5
    .line 208
    const-string v0, "[receivedNitz()] Auto timezone setting is not set"

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@a
    .line 217
    :goto_a
    return-void

    #@b
    .line 211
    :cond_b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->removeMessages(I)V

    #@e
    .line 212
    const/4 v0, 0x3

    #@f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->removeMessages(I)V

    #@12
    .line 213
    const/4 v0, 0x5

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->removeMessages(I)V

    #@16
    .line 215
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->sendEmptyMessage(I)Z

    #@19
    .line 216
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->clearPopupIfAny()V

    #@1c
    goto :goto_a
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 110
    iget-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@4
    if-eq p1, v3, :cond_1e

    #@6
    .line 111
    iput-object p1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@8
    .line 112
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->registerForTelephonyIntents()V

    #@b
    .line 113
    iget-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10
    move-result-object v0

    #@11
    .line 114
    .local v0, cr:Landroid/content/ContentResolver;
    if-eqz v0, :cond_1e

    #@13
    const-string v3, "auto_time_zone"

    #@15
    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@18
    move-result-object v3

    #@19
    iget-object v4, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mAutoTimeZoneObserver:Landroid/database/ContentObserver;

    #@1b
    invoke-virtual {v0, v3, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1e
    .line 118
    .end local v0           #cr:Landroid/content/ContentResolver;
    :cond_1e
    const/4 v3, 0x0

    #@1f
    :try_start_1f
    const-string v4, "NITZ_WAITING_TIMEOUT"

    #@21
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@28
    move-result v2

    #@29
    .line 119
    .local v2, timeout:I
    const/16 v3, 0x3e7

    #@2b
    if-le v2, v3, :cond_2f

    #@2d
    sput v2, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mWaitingTimeout:I
    :try_end_2f
    .catch Ljava/lang/NumberFormatException; {:try_start_1f .. :try_end_2f} :catch_83

    #@2f
    .line 122
    .end local v2           #timeout:I
    :cond_2f
    :goto_2f
    const-string v3, "MANAGED_TIME_SETTING"

    #@31
    invoke-static {v5, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@34
    move-result v3

    #@35
    iput-boolean v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSupportManagedTimeSetting:Z

    #@37
    .line 123
    const-string v3, "MAUNAL_TIMEZONE_SETTING_POPUP"

    #@39
    invoke-static {v5, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3c
    move-result v3

    #@3d
    iput-boolean v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSupportManualSettingPopup:Z

    #@3f
    .line 124
    const-string v3, "MANAGED_TIME_LOCAL_AREAS"

    #@41
    invoke-static {v5, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-static {v3}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->getArray(Ljava/lang/String;)[Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    iput-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLocalAreas:[Ljava/lang/String;

    #@4b
    .line 126
    iget-boolean v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mInitialized:Z

    #@4d
    if-nez v3, :cond_82

    #@4f
    iget-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@51
    if-eqz v3, :cond_82

    #@53
    .line 127
    iget-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mContext:Landroid/content/Context;

    #@55
    const-string v4, "timezone_mon"

    #@57
    const/4 v5, 0x0

    #@58
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@5b
    move-result-object v1

    #@5c
    .line 128
    .local v1, prefs:Landroid/content/SharedPreferences;
    const-string v3, "last_mcc"

    #@5e
    const-string v4, ""

    #@60
    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    iput-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastMcc:Ljava/lang/String;

    #@66
    .line 129
    const-string v3, "last_checked"

    #@68
    const-string v4, ""

    #@6a
    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6d
    move-result-object v3

    #@6e
    const-string v4, "true"

    #@70
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@73
    move-result v3

    #@74
    iput-boolean v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastChecked:Z

    #@76
    .line 130
    const-string v3, "last_nitzmcc"

    #@78
    const-string v4, ""

    #@7a
    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7d
    move-result-object v3

    #@7e
    iput-object v3, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mLastNitzMcc:Ljava/lang/String;

    #@80
    .line 131
    iput-boolean v6, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mInitialized:Z

    #@82
    .line 133
    .end local v1           #prefs:Landroid/content/SharedPreferences;
    :cond_82
    return-void

    #@83
    .line 120
    :catch_83
    move-exception v3

    #@84
    goto :goto_2f
.end method

.method public setServiceStateTracker(Lcom/android/internal/telephony/ServiceStateTracker;)V
    .registers 5
    .parameter "sst"

    #@0
    .prologue
    .line 136
    iput-object p1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSST:Lcom/android/internal/telephony/ServiceStateTracker;

    #@2
    .line 137
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->clearPopupIfAny()V

    #@5
    .line 139
    const/16 v0, 0xb

    #@7
    const-wide/16 v1, 0xfa0

    #@9
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->sendEmptyMessageDelayed(IJ)Z

    #@c
    .line 141
    return-void
.end method

.method public unsetServiceStateTracker()V
    .registers 2

    #@0
    .prologue
    .line 144
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mSST:Lcom/android/internal/telephony/ServiceStateTracker;

    #@3
    .line 145
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->clearPopupIfAny()V

    #@6
    .line 146
    return-void
.end method
