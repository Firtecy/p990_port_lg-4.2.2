.class Lcom/android/internal/telephony/uicc/VoiceMailConstants;
.super Ljava/lang/Object;
.source "VoiceMailConstants.java"


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "GSM"

.field static final NAME:I = 0x0

.field static final NUMBER:I = 0x1

.field static final PARTNER_VOICEMAIL_PATH:Ljava/lang/String; = "etc/voicemail-conf.xml"

.field static final SIZE:I = 0x3

.field static final TAG:I = 0x2


# instance fields
.field private CarrierVmMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 50
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/VoiceMailConstants;->CarrierVmMap:Ljava/util/HashMap;

    #@a
    .line 51
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/VoiceMailConstants;->loadVoiceMail()V

    #@d
    .line 52
    return-void
.end method

.method private loadVoiceMail()V
    .registers 11

    #@0
    .prologue
    .line 76
    new-instance v5, Ljava/io/File;

    #@2
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    #@5
    move-result-object v7

    #@6
    const-string v8, "etc/voicemail-conf.xml"

    #@8
    invoke-direct {v5, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    .line 80
    .local v5, vmFile:Ljava/io/File;
    :try_start_b
    new-instance v6, Ljava/io/FileReader;

    #@d
    invoke-direct {v6, v5}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_10} :catch_31

    #@10
    .line 88
    .local v6, vmReader:Ljava/io/FileReader;
    :try_start_10
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@13
    move-result-object v4

    #@14
    .line 89
    .local v4, parser:Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v4, v6}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    #@17
    .line 91
    const-string v7, "voicemail"

    #@19
    invoke-static {v4, v7}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@1c
    .line 94
    :goto_1c
    invoke-static {v4}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1f
    .line 96
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    .line 97
    .local v2, name:Ljava/lang/String;
    const-string v7, "voicemail"

    #@25
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_28
    .catchall {:try_start_10 .. :try_end_28} :catchall_ca
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_10 .. :try_end_28} :catch_89
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_28} :catch_aa

    #@28
    move-result v7

    #@29
    if-nez v7, :cond_5b

    #@2b
    .line 115
    if-eqz v6, :cond_30

    #@2d
    .line 116
    :try_start_2d
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V
    :try_end_30
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_30} :catch_a8

    #@30
    .line 120
    .end local v2           #name:Ljava/lang/String;
    .end local v4           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v6           #vmReader:Ljava/io/FileReader;
    :cond_30
    :goto_30
    return-void

    #@31
    .line 81
    :catch_31
    move-exception v1

    #@32
    .line 82
    .local v1, e:Ljava/io/FileNotFoundException;
    const-string v7, "GSM"

    #@34
    new-instance v8, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v9, "Can\'t open "

    #@3b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    #@42
    move-result-object v9

    #@43
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    const-string v9, "/"

    #@49
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v8

    #@4d
    const-string v9, "etc/voicemail-conf.xml"

    #@4f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v8

    #@53
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v8

    #@57
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    goto :goto_30

    #@5b
    .line 101
    .end local v1           #e:Ljava/io/FileNotFoundException;
    .restart local v2       #name:Ljava/lang/String;
    .restart local v4       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6       #vmReader:Ljava/io/FileReader;
    :cond_5b
    const/4 v7, 0x3

    #@5c
    :try_start_5c
    new-array v0, v7, [Ljava/lang/String;

    #@5e
    .line 102
    .local v0, data:[Ljava/lang/String;
    const/4 v7, 0x0

    #@5f
    const-string v8, "numeric"

    #@61
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    .line 103
    .local v3, numeric:Ljava/lang/String;
    const/4 v7, 0x0

    #@66
    const/4 v8, 0x0

    #@67
    const-string v9, "carrier"

    #@69
    invoke-interface {v4, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6c
    move-result-object v8

    #@6d
    aput-object v8, v0, v7

    #@6f
    .line 104
    const/4 v7, 0x1

    #@70
    const/4 v8, 0x0

    #@71
    const-string v9, "vmnumber"

    #@73
    invoke-interface {v4, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@76
    move-result-object v8

    #@77
    aput-object v8, v0, v7

    #@79
    .line 105
    const/4 v7, 0x2

    #@7a
    const/4 v8, 0x0

    #@7b
    const-string v9, "vmtag"

    #@7d
    invoke-interface {v4, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@80
    move-result-object v8

    #@81
    aput-object v8, v0, v7

    #@83
    .line 107
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/VoiceMailConstants;->CarrierVmMap:Ljava/util/HashMap;

    #@85
    invoke-virtual {v7, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_88
    .catchall {:try_start_5c .. :try_end_88} :catchall_ca
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5c .. :try_end_88} :catch_89
    .catch Ljava/io/IOException; {:try_start_5c .. :try_end_88} :catch_aa

    #@88
    goto :goto_1c

    #@89
    .line 109
    .end local v0           #data:[Ljava/lang/String;
    .end local v2           #name:Ljava/lang/String;
    .end local v3           #numeric:Ljava/lang/String;
    .end local v4           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_89
    move-exception v1

    #@8a
    .line 110
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_8a
    const-string v7, "GSM"

    #@8c
    new-instance v8, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v9, "Exception in Voicemail parser "

    #@93
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v8

    #@97
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v8

    #@9b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v8

    #@9f
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a2
    .catchall {:try_start_8a .. :try_end_a2} :catchall_ca

    #@a2
    .line 115
    if-eqz v6, :cond_30

    #@a4
    .line 116
    :try_start_a4
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V
    :try_end_a7
    .catch Ljava/io/IOException; {:try_start_a4 .. :try_end_a7} :catch_a8

    #@a7
    goto :goto_30

    #@a8
    .line 118
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_a8
    move-exception v7

    #@a9
    goto :goto_30

    #@aa
    .line 111
    :catch_aa
    move-exception v1

    #@ab
    .line 112
    .local v1, e:Ljava/io/IOException;
    :try_start_ab
    const-string v7, "GSM"

    #@ad
    new-instance v8, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v9, "Exception in Voicemail parser "

    #@b4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v8

    #@b8
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v8

    #@bc
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v8

    #@c0
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c3
    .catchall {:try_start_ab .. :try_end_c3} :catchall_ca

    #@c3
    .line 115
    if-eqz v6, :cond_30

    #@c5
    .line 116
    :try_start_c5
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V
    :try_end_c8
    .catch Ljava/io/IOException; {:try_start_c5 .. :try_end_c8} :catch_a8

    #@c8
    goto/16 :goto_30

    #@ca
    .line 114
    .end local v1           #e:Ljava/io/IOException;
    :catchall_ca
    move-exception v7

    #@cb
    .line 115
    if-eqz v6, :cond_d0

    #@cd
    .line 116
    :try_start_cd
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V
    :try_end_d0
    .catch Ljava/io/IOException; {:try_start_cd .. :try_end_d0} :catch_d1

    #@d0
    .line 114
    :cond_d0
    :goto_d0
    throw v7

    #@d1
    .line 118
    :catch_d1
    move-exception v8

    #@d2
    goto :goto_d0
.end method


# virtual methods
.method containsCarrier(Ljava/lang/String;)Z
    .registers 3
    .parameter "carrier"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/VoiceMailConstants;->CarrierVmMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getCarrierName(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "carrier"

    #@0
    .prologue
    .line 59
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/VoiceMailConstants;->CarrierVmMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Ljava/lang/String;

    #@8
    .line 60
    .local v0, data:[Ljava/lang/String;
    const/4 v1, 0x0

    #@9
    aget-object v1, v0, v1

    #@b
    return-object v1
.end method

.method getVoiceMailNumber(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "carrier"

    #@0
    .prologue
    .line 64
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/VoiceMailConstants;->CarrierVmMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Ljava/lang/String;

    #@8
    .line 65
    .local v0, data:[Ljava/lang/String;
    const/4 v1, 0x1

    #@9
    aget-object v1, v0, v1

    #@b
    return-object v1
.end method

.method getVoiceMailTag(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "carrier"

    #@0
    .prologue
    .line 69
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/VoiceMailConstants;->CarrierVmMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Ljava/lang/String;

    #@8
    .line 70
    .local v0, data:[Ljava/lang/String;
    const/4 v1, 0x2

    #@9
    aget-object v1, v0, v1

    #@b
    return-object v1
.end method
