.class Lcom/android/internal/telephony/SMSDispatcher$1;
.super Landroid/content/BroadcastReceiver;
.source "SMSDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/SMSDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/SMSDispatcher;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/SMSDispatcher;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3754
    iput-object p1, p0, Lcom/android/internal/telephony/SMSDispatcher$1;->this$0:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 3757
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v2

    #@5
    const-string v3, "android.provider.Telephony.SMS_CB_RECEIVED"

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_25

    #@d
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    const-string v3, "android.provider.Telephony.SMS_EMERGENCY_CB_RECEIVED"

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v2

    #@17
    if-nez v2, :cond_25

    #@19
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "com.lge.provider.Telephony.LGE_CMAS_RECEIVED"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v2

    #@23
    if-eqz v2, :cond_26

    #@25
    .line 3777
    :cond_25
    :goto_25
    return-void

    #@26
    .line 3764
    :cond_26
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_44

    #@32
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher$1;->this$0:Lcom/android/internal/telephony/SMSDispatcher;

    #@34
    iget-object v2, v2, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@36
    const-string v3, "sprint_reassembly_sms"

    #@38
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3b
    move-result v2

    #@3c
    if-eqz v2, :cond_44

    #@3e
    .line 3765
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher$1;->this$0:Lcom/android/internal/telephony/SMSDispatcher;

    #@40
    invoke-static {v2}, Lcom/android/internal/telephony/SMSDispatcher;->access$100(Lcom/android/internal/telephony/SMSDispatcher;)V

    #@43
    goto :goto_25

    #@44
    .line 3770
    :cond_44
    invoke-virtual {p0}, Lcom/android/internal/telephony/SMSDispatcher$1;->getResultCode()I

    #@47
    move-result v0

    #@48
    .line 3771
    .local v0, rc:I
    const/4 v2, -0x1

    #@49
    if-eq v0, v2, :cond_4d

    #@4b
    if-ne v0, v1, :cond_54

    #@4d
    .line 3775
    .local v1, success:Z
    :cond_4d
    :goto_4d
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher$1;->this$0:Lcom/android/internal/telephony/SMSDispatcher;

    #@4f
    const/4 v3, 0x0

    #@50
    invoke-virtual {v2, v1, v0, v3}, Lcom/android/internal/telephony/SMSDispatcher;->acknowledgeLastIncomingSms(ZILandroid/os/Message;)V

    #@53
    goto :goto_25

    #@54
    .line 3771
    .end local v1           #success:Z
    :cond_54
    const/4 v1, 0x0

    #@55
    goto :goto_4d
.end method
