.class public final Lcom/android/internal/telephony/uicc/AdnRecordCache;
.super Landroid/os/Handler;
.source "AdnRecordCache.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccConstants;


# static fields
.field static final EVENT_LOAD_ALL_ADN_LIKE_DONE:I = 0x1

.field static final EVENT_UPDATE_ADN_DONE:I = 0x2


# instance fields
.field adnLikeFiles:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/uicc/AdnRecord;",
            ">;>;"
        }
    .end annotation
.end field

.field adnLikeWaiters:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Message;",
            ">;>;"
        }
    .end annotation
.end field

.field private mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

.field private mUsimPhoneBookManager:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;

.field userWriteResponse:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V
    .registers 4
    .parameter "fh"

    #@0
    .prologue
    .line 59
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 40
    new-instance v0, Landroid/util/SparseArray;

    #@5
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeFiles:Landroid/util/SparseArray;

    #@a
    .line 44
    new-instance v0, Landroid/util/SparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeWaiters:Landroid/util/SparseArray;

    #@11
    .line 48
    new-instance v0, Landroid/util/SparseArray;

    #@13
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->userWriteResponse:Landroid/util/SparseArray;

    #@18
    .line 60
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1a
    .line 61
    new-instance v0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;

    #@1c
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1e
    invoke-direct {v0, v1, p0}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;Lcom/android/internal/telephony/uicc/AdnRecordCache;)V

    #@21
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->mUsimPhoneBookManager:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;

    #@23
    .line 62
    return-void
.end method

.method private clearUserWriters()V
    .registers 5

    #@0
    .prologue
    .line 89
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->userWriteResponse:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@5
    move-result v1

    #@6
    .line 90
    .local v1, size:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_19

    #@9
    .line 91
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->userWriteResponse:Landroid/util/SparseArray;

    #@b
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/os/Message;

    #@11
    const-string v3, "AdnCace reset"

    #@13
    invoke-direct {p0, v2, v3}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->sendErrorResponse(Landroid/os/Message;Ljava/lang/String;)V

    #@16
    .line 90
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_7

    #@19
    .line 93
    :cond_19
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->userWriteResponse:Landroid/util/SparseArray;

    #@1b
    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    #@1e
    .line 94
    return-void
.end method

.method private clearWaiters()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 79
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeWaiters:Landroid/util/SparseArray;

    #@3
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    #@6
    move-result v2

    #@7
    .line 80
    .local v2, size:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v2, :cond_24

    #@a
    .line 81
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeWaiters:Landroid/util/SparseArray;

    #@c
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Ljava/util/ArrayList;

    #@12
    .line 82
    .local v3, waiters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Message;>;"
    new-instance v0, Landroid/os/AsyncResult;

    #@14
    new-instance v4, Ljava/lang/RuntimeException;

    #@16
    const-string v5, "AdnCache reset"

    #@18
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1b
    invoke-direct {v0, v6, v6, v4}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@1e
    .line 83
    .local v0, ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v3, v0}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->notifyWaiters(Ljava/util/ArrayList;Landroid/os/AsyncResult;)V

    #@21
    .line 80
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_8

    #@24
    .line 85
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v3           #waiters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Message;>;"
    :cond_24
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeWaiters:Landroid/util/SparseArray;

    #@26
    invoke-virtual {v4}, Landroid/util/SparseArray;->clear()V

    #@29
    .line 86
    return-void
.end method

.method private notifyWaiters(Ljava/util/ArrayList;Landroid/os/AsyncResult;)V
    .registers 8
    .parameter
    .parameter "ar"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Message;",
            ">;",
            "Landroid/os/AsyncResult;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 316
    .local p1, waiters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Message;>;"
    if-nez p1, :cond_3

    #@2
    .line 326
    :cond_2
    return-void

    #@3
    .line 320
    :cond_3
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v1

    #@8
    .local v1, s:I
    :goto_8
    if-ge v0, v1, :cond_2

    #@a
    .line 321
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v2

    #@e
    check-cast v2, Landroid/os/Message;

    #@10
    .line 323
    .local v2, waiter:Landroid/os/Message;
    iget-object v3, p2, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@12
    iget-object v4, p2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@14
    invoke-static {v2, v3, v4}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@17
    .line 324
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    #@1a
    .line 320
    add-int/lit8 v0, v0, 0x1

    #@1c
    goto :goto_8
.end method

.method private sendErrorResponse(Landroid/os/Message;Ljava/lang/String;)V
    .registers 5
    .parameter "response"
    .parameter "errString"

    #@0
    .prologue
    .line 132
    if-eqz p1, :cond_10

    #@2
    .line 133
    new-instance v0, Ljava/lang/RuntimeException;

    #@4
    invoke-direct {v0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    .line 134
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@a
    move-result-object v1

    #@b
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@d
    .line 135
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@10
    .line 137
    .end local v0           #e:Ljava/lang/Exception;
    :cond_10
    return-void
.end method


# virtual methods
.method public extensionEfForEf(I)I
    .registers 5
    .parameter "efid"

    #@0
    .prologue
    const/16 v0, 0x6f4a

    #@2
    .line 112
    sparse-switch p1, :sswitch_data_22

    #@5
    .line 127
    const/4 v0, -0x1

    #@6
    :cond_6
    :goto_6
    :sswitch_6
    return v0

    #@7
    .line 113
    :sswitch_7
    const/16 v0, 0x6fc8

    #@9
    goto :goto_6

    #@a
    .line 115
    :sswitch_a
    const/16 v0, 0x6f4c

    #@c
    goto :goto_6

    #@d
    .line 116
    :sswitch_d
    const/16 v0, 0x6f4b

    #@f
    goto :goto_6

    #@10
    .line 120
    :sswitch_10
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@12
    iget-object v1, v1, Lcom/android/internal/telephony/uicc/IccFileHandler;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@14
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@17
    move-result-object v1

    #@18
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_USIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@1a
    if-ne v1, v2, :cond_6

    #@1c
    .line 121
    const/16 v0, 0x6f4e

    #@1e
    goto :goto_6

    #@1f
    .line 126
    :sswitch_1f
    const/4 v0, 0x0

    #@20
    goto :goto_6

    #@21
    .line 112
    nop

    #@22
    :sswitch_data_22
    .sparse-switch
        0x4f30 -> :sswitch_1f
        0x6f3a -> :sswitch_6
        0x6f3b -> :sswitch_d
        0x6f40 -> :sswitch_10
        0x6f49 -> :sswitch_a
        0x6fc7 -> :sswitch_7
    .end sparse-switch
.end method

.method public getRecordsIfLoaded(I)Ljava/util/ArrayList;
    .registers 3
    .parameter "efid"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/uicc/AdnRecord;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeFiles:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/util/ArrayList;

    #@8
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    .line 335
    iget v6, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v6, :pswitch_data_66

    #@5
    .line 369
    :goto_5
    return-void

    #@6
    .line 338
    :pswitch_6
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v1, Landroid/os/AsyncResult;

    #@a
    .line 339
    .local v1, ar:Landroid/os/AsyncResult;
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@c
    .line 342
    .local v2, efid:I
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeWaiters:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v5

    #@12
    check-cast v5, Ljava/util/ArrayList;

    #@14
    .line 343
    .local v5, waiters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Message;>;"
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeWaiters:Landroid/util/SparseArray;

    #@16
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->delete(I)V

    #@19
    .line 345
    iget-object v6, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1b
    if-nez v6, :cond_26

    #@1d
    .line 346
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeFiles:Landroid/util/SparseArray;

    #@1f
    iget-object v6, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@21
    check-cast v6, Ljava/util/ArrayList;

    #@23
    invoke-virtual {v7, v2, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@26
    .line 348
    :cond_26
    invoke-direct {p0, v5, v1}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->notifyWaiters(Ljava/util/ArrayList;Landroid/os/AsyncResult;)V

    #@29
    goto :goto_5

    #@2a
    .line 351
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v2           #efid:I
    .end local v5           #waiters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Message;>;"
    :pswitch_2a
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2c
    check-cast v1, Landroid/os/AsyncResult;

    #@2e
    .line 352
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@30
    .line 353
    .restart local v2       #efid:I
    iget v3, p1, Landroid/os/Message;->arg2:I

    #@32
    .line 354
    .local v3, index:I
    iget-object v6, v1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@34
    check-cast v6, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@36
    move-object v0, v6

    #@37
    check-cast v0, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@39
    .line 356
    .local v0, adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    iget-object v6, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3b
    if-nez v6, :cond_4f

    #@3d
    .line 357
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeFiles:Landroid/util/SparseArray;

    #@3f
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@42
    move-result-object v6

    #@43
    check-cast v6, Ljava/util/ArrayList;

    #@45
    add-int/lit8 v7, v3, -0x1

    #@47
    invoke-virtual {v6, v7, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@4a
    .line 358
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->mUsimPhoneBookManager:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;

    #@4c
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->invalidateCache()V

    #@4f
    .line 361
    :cond_4f
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->userWriteResponse:Landroid/util/SparseArray;

    #@51
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@54
    move-result-object v4

    #@55
    check-cast v4, Landroid/os/Message;

    #@57
    .line 362
    .local v4, response:Landroid/os/Message;
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->userWriteResponse:Landroid/util/SparseArray;

    #@59
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->delete(I)V

    #@5c
    .line 364
    const/4 v6, 0x0

    #@5d
    iget-object v7, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5f
    invoke-static {v4, v6, v7}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@62
    .line 365
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    #@65
    goto :goto_5

    #@66
    .line 335
    :pswitch_data_66
    .packed-switch 0x1
        :pswitch_6
        :pswitch_2a
    .end packed-switch
.end method

.method public requestLoadAllAdnLike(IILandroid/os/Message;)V
    .registers 10
    .parameter "efid"
    .parameter "extensionEf"
    .parameter "response"

    #@0
    .prologue
    .line 259
    const/16 v2, 0x4f30

    #@2
    if-ne p1, v2, :cond_18

    #@4
    .line 260
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->mUsimPhoneBookManager:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;

    #@6
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->loadEfFilesFromUsim()Ljava/util/ArrayList;

    #@9
    move-result-object v0

    #@a
    .line 266
    .local v0, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    :goto_a
    if-eqz v0, :cond_1d

    #@c
    .line 267
    if-eqz p3, :cond_17

    #@e
    .line 268
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@11
    move-result-object v2

    #@12
    iput-object v0, v2, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@14
    .line 269
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@17
    .line 309
    :cond_17
    :goto_17
    return-void

    #@18
    .line 262
    .end local v0           #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    :cond_18
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->getRecordsIfLoaded(I)Ljava/util/ArrayList;

    #@1b
    move-result-object v0

    #@1c
    .restart local v0       #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    goto :goto_a

    #@1d
    .line 277
    :cond_1d
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeWaiters:Landroid/util/SparseArray;

    #@1f
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Ljava/util/ArrayList;

    #@25
    .line 279
    .local v1, waiters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Message;>;"
    if-eqz v1, :cond_2b

    #@27
    .line 283
    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2a
    goto :goto_17

    #@2b
    .line 289
    :cond_2b
    new-instance v1, Ljava/util/ArrayList;

    #@2d
    .end local v1           #waiters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Message;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@30
    .line 290
    .restart local v1       #waiters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Message;>;"
    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@33
    .line 292
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeWaiters:Landroid/util/SparseArray;

    #@35
    invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@38
    .line 295
    if-gez p2, :cond_5e

    #@3a
    .line 298
    if-eqz p3, :cond_17

    #@3c
    .line 299
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@3f
    move-result-object v2

    #@40
    new-instance v3, Ljava/lang/RuntimeException;

    #@42
    new-instance v4, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v5, "EF is not known ADN-like EF:"

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@58
    iput-object v3, v2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5a
    .line 301
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@5d
    goto :goto_17

    #@5e
    .line 307
    :cond_5e
    new-instance v2, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@60
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@62
    invoke-direct {v2, v3}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@65
    const/4 v3, 0x1

    #@66
    const/4 v4, 0x0

    #@67
    invoke-virtual {p0, v3, p1, v4}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->obtainMessage(III)Landroid/os/Message;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v2, p1, p2, v3}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->loadAllFromEF(IILandroid/os/Message;)V

    #@6e
    goto :goto_17
.end method

.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->adnLikeFiles:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    #@5
    .line 71
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->mUsimPhoneBookManager:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;

    #@7
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->reset()V

    #@a
    .line 73
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->clearWaiters()V

    #@d
    .line 74
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->clearUserWriters()V

    #@10
    .line 76
    return-void
.end method

.method public updateAdnByIndex(ILcom/android/internal/telephony/uicc/AdnRecord;ILjava/lang/String;Landroid/os/Message;)V
    .registers 14
    .parameter "efid"
    .parameter "adn"
    .parameter "recordIndex"
    .parameter "pin2"
    .parameter "response"

    #@0
    .prologue
    .line 152
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->extensionEfForEf(I)I

    #@3
    move-result v3

    #@4
    .line 153
    .local v3, extensionEF:I
    if-gez v3, :cond_1d

    #@6
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v1, "EF is not known ADN-like EF:"

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-direct {p0, p5, v0}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->sendErrorResponse(Landroid/os/Message;Ljava/lang/String;)V

    #@1c
    .line 169
    :goto_1c
    return-void

    #@1d
    .line 158
    :cond_1d
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->userWriteResponse:Landroid/util/SparseArray;

    #@1f
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@22
    move-result-object v7

    #@23
    check-cast v7, Landroid/os/Message;

    #@25
    .line 159
    .local v7, pendingResponse:Landroid/os/Message;
    if-eqz v7, :cond_3e

    #@27
    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v1, "Have pending update for EF:"

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    invoke-direct {p0, p5, v0}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->sendErrorResponse(Landroid/os/Message;Ljava/lang/String;)V

    #@3d
    goto :goto_1c

    #@3e
    .line 164
    :cond_3e
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->userWriteResponse:Landroid/util/SparseArray;

    #@40
    invoke-virtual {v0, p1, p5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@43
    .line 166
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@45
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@47
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@4a
    const/4 v1, 0x2

    #@4b
    invoke-virtual {p0, v1, p1, p3, p2}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@4e
    move-result-object v6

    #@4f
    move-object v1, p2

    #@50
    move v2, p1

    #@51
    move v4, p3

    #@52
    move-object v5, p4

    #@53
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->updateEF(Lcom/android/internal/telephony/uicc/AdnRecord;IIILjava/lang/String;Landroid/os/Message;)V

    #@56
    goto :goto_1c
.end method

.method public updateAdnBySearch(ILcom/android/internal/telephony/uicc/AdnRecord;Lcom/android/internal/telephony/uicc/AdnRecord;Ljava/lang/String;Landroid/os/Message;)V
    .registers 19
    .parameter "efid"
    .parameter "oldAdn"
    .parameter "newAdn"
    .parameter "pin2"
    .parameter "response"

    #@0
    .prologue
    .line 189
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->extensionEfForEf(I)I

    #@3
    move-result v4

    #@4
    .line 191
    .local v4, extensionEF:I
    if-gez v4, :cond_1f

    #@6
    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "EF is not known ADN-like EF:"

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    move-object/from16 v0, p5

    #@1b
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->sendErrorResponse(Landroid/os/Message;Ljava/lang/String;)V

    #@1e
    .line 247
    :goto_1e
    return-void

    #@1f
    .line 198
    :cond_1f
    const/16 v1, 0x4f30

    #@21
    if-ne p1, v1, :cond_44

    #@23
    .line 199
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->mUsimPhoneBookManager:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;

    #@25
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->loadEfFilesFromUsim()Ljava/util/ArrayList;

    #@28
    move-result-object v11

    #@29
    .line 204
    .local v11, oldAdnList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    :goto_29
    if-nez v11, :cond_49

    #@2b
    .line 205
    new-instance v1, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v2, "Adn list not exist for EF:"

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    move-object/from16 v0, p5

    #@40
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->sendErrorResponse(Landroid/os/Message;Ljava/lang/String;)V

    #@43
    goto :goto_1e

    #@44
    .line 201
    .end local v11           #oldAdnList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    :cond_44
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->getRecordsIfLoaded(I)Ljava/util/ArrayList;

    #@47
    move-result-object v11

    #@48
    .restart local v11       #oldAdnList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    goto :goto_29

    #@49
    .line 209
    :cond_49
    const/4 v5, -0x1

    #@4a
    .line 210
    .local v5, index:I
    const/4 v8, 0x1

    #@4b
    .line 211
    .local v8, count:I
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@4e
    move-result-object v10

    #@4f
    .local v10, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    :goto_4f
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@52
    move-result v1

    #@53
    if-eqz v1, :cond_62

    #@55
    .line 212
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@58
    move-result-object v1

    #@59
    check-cast v1, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@5b
    invoke-virtual {p2, v1}, Lcom/android/internal/telephony/uicc/AdnRecord;->isEqual(Lcom/android/internal/telephony/uicc/AdnRecord;)Z

    #@5e
    move-result v1

    #@5f
    if-eqz v1, :cond_7e

    #@61
    .line 213
    move v5, v8

    #@62
    .line 219
    :cond_62
    const/4 v1, -0x1

    #@63
    if-ne v5, v1, :cond_81

    #@65
    .line 220
    new-instance v1, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v2, "Adn record don\'t exist for "

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v1

    #@74
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v1

    #@78
    move-object/from16 v0, p5

    #@7a
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->sendErrorResponse(Landroid/os/Message;Ljava/lang/String;)V

    #@7d
    goto :goto_1e

    #@7e
    .line 216
    :cond_7e
    add-int/lit8 v8, v8, 0x1

    #@80
    goto :goto_4f

    #@81
    .line 224
    :cond_81
    const/16 v1, 0x4f30

    #@83
    if-ne p1, v1, :cond_9f

    #@85
    .line 225
    add-int/lit8 v1, v5, -0x1

    #@87
    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8a
    move-result-object v9

    #@8b
    check-cast v9, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@8d
    .line 226
    .local v9, foundAdn:Lcom/android/internal/telephony/uicc/AdnRecord;
    iget p1, v9, Lcom/android/internal/telephony/uicc/AdnRecord;->efid:I

    #@8f
    .line 227
    iget v4, v9, Lcom/android/internal/telephony/uicc/AdnRecord;->extRecord:I

    #@91
    .line 228
    iget v5, v9, Lcom/android/internal/telephony/uicc/AdnRecord;->recordNumber:I

    #@93
    .line 230
    move-object/from16 v0, p3

    #@95
    iput p1, v0, Lcom/android/internal/telephony/uicc/AdnRecord;->efid:I

    #@97
    .line 231
    move-object/from16 v0, p3

    #@99
    iput v4, v0, Lcom/android/internal/telephony/uicc/AdnRecord;->extRecord:I

    #@9b
    .line 232
    move-object/from16 v0, p3

    #@9d
    iput v5, v0, Lcom/android/internal/telephony/uicc/AdnRecord;->recordNumber:I

    #@9f
    .line 235
    .end local v9           #foundAdn:Lcom/android/internal/telephony/uicc/AdnRecord;
    :cond_9f
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->userWriteResponse:Landroid/util/SparseArray;

    #@a1
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@a4
    move-result-object v12

    #@a5
    check-cast v12, Landroid/os/Message;

    #@a7
    .line 237
    .local v12, pendingResponse:Landroid/os/Message;
    if-eqz v12, :cond_c3

    #@a9
    .line 238
    new-instance v1, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    const-string v2, "Have pending update for EF:"

    #@b0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v1

    #@b4
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v1

    #@b8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v1

    #@bc
    move-object/from16 v0, p5

    #@be
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->sendErrorResponse(Landroid/os/Message;Ljava/lang/String;)V

    #@c1
    goto/16 :goto_1e

    #@c3
    .line 242
    :cond_c3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->userWriteResponse:Landroid/util/SparseArray;

    #@c5
    move-object/from16 v0, p5

    #@c7
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@ca
    .line 244
    new-instance v1, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@cc
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordCache;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@ce
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@d1
    const/4 v2, 0x2

    #@d2
    move-object/from16 v0, p3

    #@d4
    invoke-virtual {p0, v2, p1, v5, v0}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@d7
    move-result-object v7

    #@d8
    move-object/from16 v2, p3

    #@da
    move v3, p1

    #@db
    move-object/from16 v6, p4

    #@dd
    invoke-virtual/range {v1 .. v7}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->updateEF(Lcom/android/internal/telephony/uicc/AdnRecord;IIILjava/lang/String;Landroid/os/Message;)V

    #@e0
    goto/16 :goto_1e
.end method
