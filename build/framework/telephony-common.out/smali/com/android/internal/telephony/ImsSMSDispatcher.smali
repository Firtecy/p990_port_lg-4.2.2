.class public Lcom/android/internal/telephony/ImsSMSDispatcher;
.super Lcom/android/internal/telephony/SMSDispatcher;
.source "ImsSMSDispatcher.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RIL_ImsSms"


# instance fields
.field protected mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

.field protected mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

.field private mIms:Z

.field private mImsSmsFormat:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;)V
    .registers 7
    .parameter "phone"
    .parameter "storageMonitor"
    .parameter "usageMonitor"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/SMSDispatcher;-><init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;)V

    #@4
    .line 58
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mIms:Z

    #@7
    .line 59
    const-string v0, "unknown"

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mImsSmsFormat:Ljava/lang/String;

    #@b
    .line 65
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/telephony/ImsSMSDispatcher;->initDispatchers(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;)V

    #@e
    .line 67
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    const/16 v1, 0xa

    #@12
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->registerForOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@15
    .line 68
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    const/16 v1, 0xb

    #@19
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->registerForImsNetworkStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@1c
    .line 69
    return-void
.end method

.method private isCdmaFormat(Ljava/lang/String;)Z
    .registers 3
    .parameter "format"

    #@0
    .prologue
    .line 655
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method private isDomainNotiMessage(Ljava/util/HashMap;)Z
    .registers 11
    .parameter "data"

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    const/4 v6, 0x0

    #@2
    .line 520
    const/4 v1, 0x0

    #@3
    .line 521
    .local v1, pdu:[B
    const/4 v3, 0x0

    #@4
    .line 522
    .local v3, userData:Ljava/lang/String;
    const/4 v4, 0x0

    #@5
    .line 525
    .local v4, userDataByte:[B
    if-nez p1, :cond_e

    #@7
    .line 527
    const-string v5, "isDomainNotiMessage(), [KDDI][DAN] HashMap data is null !! return false"

    #@9
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@c
    move v5, v6

    #@d
    .line 571
    :goto_d
    return v5

    #@e
    .line 530
    :cond_e
    const-string v5, "pdu"

    #@10
    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v5

    #@14
    check-cast v5, [B

    #@16
    move-object v1, v5

    #@17
    check-cast v1, [B

    #@19
    .line 533
    if-nez v1, :cond_22

    #@1b
    .line 535
    const-string v5, "isDomainNotiMessage(), [KDDI][DAN] pdu is null !! return false"

    #@1d
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@20
    move v5, v6

    #@21
    .line 536
    goto :goto_d

    #@22
    .line 539
    :cond_22
    const-string v5, "isDomainNotiMessage(), [KDDI][DAN] Retry .."

    #@24
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@27
    .line 540
    new-instance v5, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v7, "isDomainNotiMessage(), [KDDI][DAN] Pdu : "

    #@2e
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    invoke-static {v1}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@35
    move-result-object v7

    #@36
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v5

    #@3e
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@41
    .line 542
    const/4 v5, 0x2

    #@42
    aget-byte v5, v1, v5

    #@44
    and-int/lit16 v5, v5, 0xff

    #@46
    shl-int/lit8 v2, v5, 0x8

    #@48
    .line 543
    .local v2, teleService:I
    aget-byte v5, v1, v8

    #@4a
    and-int/lit16 v5, v5, 0xff

    #@4c
    or-int/2addr v2, v5

    #@4d
    .line 544
    new-instance v5, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v7, "isDomainNotiMessage(), [KDDI][DAN] TeleService : "

    #@54
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@63
    .line 546
    const/16 v5, 0x1092

    #@65
    if-eq v2, v5, :cond_69

    #@67
    move v5, v6

    #@68
    .line 548
    goto :goto_d

    #@69
    .line 551
    :cond_69
    const-string v5, "text"

    #@6b
    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6e
    move-result-object v3

    #@6f
    .end local v3           #userData:Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    #@71
    .line 553
    .restart local v3       #userData:Ljava/lang/String;
    if-nez v3, :cond_7a

    #@73
    .line 555
    const-string v5, "isDomainNotiMessage(), [KDDI][DAN] userData is null !! return false"

    #@75
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@78
    move v5, v6

    #@79
    .line 556
    goto :goto_d

    #@7a
    .line 560
    :cond_7a
    invoke-static {v3}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@7d
    move-result-object v4

    #@7e
    .line 561
    new-instance v5, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v7, "isDomainNotiMessage(), [KDDI][DAN] UserData : "

    #@85
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v5

    #@89
    invoke-static {v4}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@8c
    move-result-object v7

    #@8d
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v5

    #@95
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@98
    .line 563
    aget-byte v5, v4, v8

    #@9a
    and-int/lit16 v0, v5, 0xff

    #@9c
    .line 564
    .local v0, domainStatus:I
    new-instance v5, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v7, "isDomainNotiMessage(), [KDDI][DAN] Domain Status : "

    #@a3
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v5

    #@a7
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v5

    #@ab
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae
    move-result-object v5

    #@af
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@b2
    .line 565
    iget v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->domainNotiMsgCsOnly:I

    #@b4
    if-eq v0, v5, :cond_b9

    #@b6
    move v5, v6

    #@b7
    .line 567
    goto/16 :goto_d

    #@b9
    .line 571
    :cond_b9
    const/4 v5, 0x1

    #@ba
    goto/16 :goto_d
.end method

.method private setImsSmsFormat(I)V
    .registers 3
    .parameter "format"

    #@0
    .prologue
    .line 136
    packed-switch p1, :pswitch_data_12

    #@3
    .line 144
    const-string v0, "unknown"

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mImsSmsFormat:Ljava/lang/String;

    #@7
    .line 147
    :goto_7
    return-void

    #@8
    .line 138
    :pswitch_8
    const-string v0, "3gpp"

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mImsSmsFormat:Ljava/lang/String;

    #@c
    goto :goto_7

    #@d
    .line 141
    :pswitch_d
    const-string v0, "3gpp2"

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mImsSmsFormat:Ljava/lang/String;

    #@11
    goto :goto_7

    #@12
    .line 136
    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_8
        :pswitch_d
    .end packed-switch
.end method

.method private updateImsInfo(Landroid/os/AsyncResult;)V
    .registers 8
    .parameter "ar"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 150
    const-string v1, "updateImsInfo(), start"

    #@4
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7
    .line 151
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@9
    check-cast v1, [I

    #@b
    move-object v0, v1

    #@c
    check-cast v0, [I

    #@e
    .line 153
    .local v0, responseArray:[I
    iput-boolean v5, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mIms:Z

    #@10
    .line 154
    aget v1, v0, v5

    #@12
    if-ne v1, v4, :cond_8e

    #@14
    .line 155
    const-string v1, "updateImsInfo(), IMS is registered!"

    #@16
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@19
    .line 156
    iput-boolean v4, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mIms:Z

    #@1b
    .line 162
    :goto_1b
    const/4 v1, 0x0

    #@1c
    const-string v2, "kddi_domain_notification"

    #@1e
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@21
    move-result v1

    #@22
    if-ne v1, v4, :cond_29

    #@24
    .line 163
    aget v1, v0, v5

    #@26
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ImsSMSDispatcher;->dispatchImsInfo(I)V

    #@29
    .line 167
    :cond_29
    const-string v1, "RIL_ImsSms"

    #@2b
    new-instance v2, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v3, "updateImsInfo()- PHONE TYPE FORMAT = "

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    aget v3, v0, v4

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 169
    aget v1, v0, v4

    #@45
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/ImsSMSDispatcher;->setImsSmsFormat(I)V

    #@48
    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v2, "updateImsInfo(), mImsSmsFormat = "

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    iget-object v2, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mImsSmsFormat:Ljava/lang/String;

    #@55
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v1

    #@59
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v1

    #@5d
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@60
    .line 172
    const-string v1, "unknown"

    #@62
    iget-object v2, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mImsSmsFormat:Ljava/lang/String;

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v1

    #@68
    if-eqz v1, :cond_71

    #@6a
    .line 173
    const-string v1, "updateImsInfo(), IMS format was unknown!"

    #@6c
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@6f
    .line 175
    iput-boolean v5, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mIms:Z

    #@71
    .line 188
    :cond_71
    const-string v2, "persist.radio.sms_ims"

    #@73
    iget-boolean v1, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mIms:Z

    #@75
    if-eqz v1, :cond_94

    #@77
    const-string v1, "true"

    #@79
    :goto_79
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7c
    .line 189
    const-string v2, "persist.radio.sms_cdma_format"

    #@7e
    const-string v1, "3gpp2"

    #@80
    iget-object v3, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mImsSmsFormat:Ljava/lang/String;

    #@82
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@85
    move-result v1

    #@86
    if-eqz v1, :cond_97

    #@88
    const-string v1, "true"

    #@8a
    :goto_8a
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@8d
    .line 191
    return-void

    #@8e
    .line 158
    :cond_8e
    const-string v1, "updateImsInfo(), IMS is NOT registered!"

    #@90
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@93
    goto :goto_1b

    #@94
    .line 188
    :cond_94
    const-string v1, "false"

    #@96
    goto :goto_79

    #@97
    .line 189
    :cond_97
    const-string v1, "false"

    #@99
    goto :goto_8a
.end method


# virtual methods
.method protected SendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V
    .registers 14
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .parameter "cbAddress"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 237
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_12

    #@6
    .line 238
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@8
    move-object v1, p1

    #@9
    move-object v2, p2

    #@a
    move-object v3, p3

    #@b
    move-object v4, p4

    #@c
    move-object v5, p5

    #@d
    move-object v6, p6

    #@e
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/SMSDispatcher;->SendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    #@11
    .line 244
    :goto_11
    return-void

    #@12
    .line 241
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@14
    move-object v1, p1

    #@15
    move-object v2, p2

    #@16
    move-object v3, p3

    #@17
    move-object v4, p4

    #@18
    move-object v5, p5

    #@19
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    #@1c
    goto :goto_11
.end method

.method protected SendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 14
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 288
    const-string v0, "sendText(), start"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 289
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_1c

    #@b
    .line 290
    const-string v0, "sendText(), mCdmaDispatcher.sendText with call back number"

    #@d
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@10
    .line 291
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@12
    move-object v1, p1

    #@13
    move-object v2, p2

    #@14
    move-object v3, p3

    #@15
    move-object v4, p4

    #@16
    move-object v5, p5

    #@17
    move-object v6, p6

    #@18
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/SMSDispatcher;->SendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V

    #@1b
    .line 298
    :goto_1b
    return-void

    #@1c
    .line 294
    :cond_1c
    const-string v0, "sendText(), mGsmDispatcher.sendText"

    #@1e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@21
    .line 295
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@23
    move-object v1, p1

    #@24
    move-object v2, p2

    #@25
    move-object v3, p3

    #@26
    move-object v4, p4

    #@27
    move-object v5, p5

    #@28
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@2b
    goto :goto_1b
.end method

.method protected acknowledgeLastIncomingSms(ZILandroid/os/Message;)V
    .registers 5
    .parameter "success"
    .parameter "result"
    .parameter "response"

    #@0
    .prologue
    .line 197
    const-string v0, "acknowledgeLastIncomingSms(), acknowledgeLastIncomingSms should never be called from here!"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 198
    return-void
.end method

.method protected calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 4
    .parameter "messageBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 513
    const-string v0, "calculateLGLength(), Error! Not implemented for IMS."

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 514
    const/4 v0, 0x0

    #@6
    return-object v0
.end method

.method protected calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 4
    .parameter "messageBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 505
    const-string v0, "calculateLength(), Error! Not implemented for IMS."

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 506
    const/4 v0, 0x0

    #@6
    return-object v0
.end method

.method protected dispatchMessage(Lcom/android/internal/telephony/SmsMessageBase;)I
    .registers 3
    .parameter "sms"

    #@0
    .prologue
    .line 203
    const-string v0, "dispatchMessage(), dispatchMessage should never be called from here!"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 204
    const/4 v0, 0x2

    #@6
    return v0
.end method

.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOn(Landroid/os/Handler;)V

    #@5
    .line 95
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForImsNetworkStateChanged(Landroid/os/Handler;)V

    #@a
    .line 96
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/telephony/SMSDispatcher;->dispose()V

    #@f
    .line 97
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@11
    invoke-virtual {v0}, Lcom/android/internal/telephony/SMSDispatcher;->dispose()V

    #@14
    .line 98
    return-void
.end method

.method protected enableAutoDCDisconnect(I)V
    .registers 3
    .parameter "timeOut"

    #@0
    .prologue
    .line 668
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 669
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->enableAutoDCDisconnect(I)V

    #@b
    .line 673
    :goto_b
    return-void

    #@c
    .line 671
    :cond_c
    const-string v0, "enableAutoDCDisconnect(), not supported in WCDMA"

    #@e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@11
    goto :goto_b
.end method

.method protected getFormat()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 498
    const-string v0, "getFormat(), getFormat should never be called from here!"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 499
    const-string v0, "unknown"

    #@7
    return-object v0
.end method

.method public getImsSmsFormat()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 645
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mImsSmsFormat:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 109
    iget v1, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v1, :pswitch_data_4a

    #@5
    .line 130
    invoke-super {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->handleMessage(Landroid/os/Message;)V

    #@8
    .line 132
    :goto_8
    return-void

    #@9
    .line 112
    :pswitch_9
    const-string v1, "handleMessage(), EVENT_IMS_STATE_CHANGED!"

    #@b
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@e
    .line 113
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    const/16 v2, 0xc

    #@12
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/ImsSMSDispatcher;->obtainMessage(I)Landroid/os/Message;

    #@15
    move-result-object v2

    #@16
    invoke-interface {v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->getImsRegistrationState(Landroid/os/Message;)V

    #@19
    goto :goto_8

    #@1a
    .line 117
    :pswitch_1a
    const-string v1, "handleMessage(), EVENT_IMS_STATE_DONE!"

    #@1c
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1f
    .line 118
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    check-cast v0, Landroid/os/AsyncResult;

    #@23
    .line 120
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@25
    if-nez v1, :cond_30

    #@27
    .line 121
    const-string v1, "handleMessage(), updateImsInfo()!"

    #@29
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2c
    .line 122
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->updateImsInfo(Landroid/os/AsyncResult;)V

    #@2f
    goto :goto_8

    #@30
    .line 124
    :cond_30
    new-instance v1, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v2, "handleMessage(), IMS State query failed with exp "

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@48
    goto :goto_8

    #@49
    .line 109
    nop

    #@4a
    :pswitch_data_4a
    .packed-switch 0xa
        :pswitch_9
        :pswitch_9
        :pswitch_1a
    .end packed-switch
.end method

.method protected initDispatchers(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;)V
    .registers 6
    .parameter "phone"
    .parameter "storageMonitor"
    .parameter "usageMonitor"

    #@0
    .prologue
    .line 73
    const-string v0, "initDispatchers(), start"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 74
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;

    #@7
    invoke-direct {v0, p1, p2, p3, p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;-><init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;Lcom/android/internal/telephony/ImsSMSDispatcher;)V

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@c
    .line 76
    new-instance v0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;

    #@e
    invoke-direct {v0, p1, p2, p3, p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;-><init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;Lcom/android/internal/telephony/ImsSMSDispatcher;)V

    #@11
    iput-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@13
    .line 80
    const-string v0, "persist.radio.sms_ims"

    #@15
    const-string v1, "false"

    #@17
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 82
    return-void
.end method

.method public isIms()Z
    .registers 3

    #@0
    .prologue
    .line 621
    const/4 v0, 0x0

    #@1
    const-string v1, "lgu_disable_smsoverims_in_gsm"

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_7c

    #@9
    .line 622
    new-instance v0, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v1, "isIms(), usim = "

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    new-instance v1, Lcom/android/internal/telephony/uicc/UsimService;

    #@16
    invoke-direct {v1}, Lcom/android/internal/telephony/uicc/UsimService;-><init>()V

    #@19
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UsimService;->getUsimType()I

    #@1c
    move-result v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v1, " roaming = "

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@2e
    move-result v1

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    const-string v1, " ims = "

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    iget-boolean v1, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mIms:Z

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@46
    .line 625
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimService;

    #@48
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/UsimService;-><init>()V

    #@4b
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UsimService;->getUsimType()I

    #@4e
    move-result v0

    #@4f
    const/4 v1, 0x5

    #@50
    if-ne v0, v1, :cond_7c

    #@52
    const/4 v0, 0x1

    #@53
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@5a
    move-result v1

    #@5b
    if-ne v0, v1, :cond_7c

    #@5d
    .line 627
    new-instance v0, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v1, "isIms(), sms over ims is disable in gsm, phone type = "

    #@64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v0

    #@68
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@6a
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@6d
    move-result v1

    #@6e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@79
    .line 628
    const/4 v0, 0x0

    #@7a
    iput-boolean v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mIms:Z

    #@7c
    .line 632
    :cond_7c
    iget-boolean v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mIms:Z

    #@7e
    return v0
.end method

.method protected processOperatorMessage([[BLcom/android/internal/telephony/SmsMessageBase;ZZ)I
    .registers 6
    .parameter "pdus"
    .parameter "smsb"
    .parameter "bportAddrs"
    .parameter "bConcat"

    #@0
    .prologue
    .line 660
    const/4 v0, 0x5

    #@1
    .line 661
    .local v0, result:I
    return v0
.end method

.method protected sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 14
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "destPort"
    .parameter "data"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_12

    #@6
    .line 211
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@8
    move-object v1, p1

    #@9
    move-object v2, p2

    #@a
    move v3, p3

    #@b
    move-object v4, p4

    #@c
    move-object v5, p5

    #@d
    move-object v6, p6

    #@e
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/SMSDispatcher;->sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@11
    .line 217
    :goto_11
    return-void

    #@12
    .line 214
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@14
    move-object v1, p1

    #@15
    move-object v2, p2

    #@16
    move v3, p3

    #@17
    move-object v4, p4

    #@18
    move-object v5, p5

    #@19
    move-object v6, p6

    #@1a
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/SMSDispatcher;->sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@1d
    goto :goto_11
.end method

.method protected sendEmailoverMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 12
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 252
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const-string v0, "sendEmailoverMultipartText(), start"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 254
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_16

    #@b
    .line 255
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@d
    move-object v1, p1

    #@e
    move-object v2, p2

    #@f
    move-object v3, p3

    #@10
    move-object v4, p4

    #@11
    move-object v5, p5

    #@12
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    #@15
    .line 261
    :goto_15
    return-void

    #@16
    .line 258
    :cond_16
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@18
    move-object v1, p1

    #@19
    move-object v2, p2

    #@1a
    move-object v3, p3

    #@1b
    move-object v4, p4

    #@1c
    move-object v5, p5

    #@1d
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendEmailoverMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    #@20
    goto :goto_15
.end method

.method protected sendEmailoverText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 12
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    .line 305
    const-string v0, "sendEmailoverText(), start"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 307
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_16

    #@b
    .line 308
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@d
    move-object v1, p1

    #@e
    move-object v2, p2

    #@f
    move-object v3, p3

    #@10
    move-object v4, p4

    #@11
    move-object v5, p5

    #@12
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@15
    .line 314
    :goto_15
    return-void

    #@16
    .line 311
    :cond_16
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@18
    move-object v1, p1

    #@19
    move-object v2, p2

    #@1a
    move-object v3, p3

    #@1b
    move-object v4, p4

    #@1c
    move-object v5, p5

    #@1d
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendEmailoverText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@20
    goto :goto_15
.end method

.method protected sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 12
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 223
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 224
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@8
    move-object v1, p1

    #@9
    move-object v2, p2

    #@a
    move-object v3, p3

    #@b
    move-object v4, p4

    #@c
    move-object v5, p5

    #@d
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    #@10
    .line 230
    :goto_10
    return-void

    #@11
    .line 227
    :cond_11
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@13
    move-object v1, p1

    #@14
    move-object v2, p2

    #@15
    move-object v3, p3

    #@16
    move-object v4, p4

    #@17
    move-object v5, p5

    #@18
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    #@1b
    goto :goto_10
.end method

.method protected sendMultipartTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V
    .registers 20
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    #@0
    .prologue
    .line 451
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 454
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@8
    move-object v1, p1

    #@9
    move-object v2, p2

    #@a
    move-object v3, p3

    #@b
    move-object v4, p4

    #@c
    move-object v5, p5

    #@d
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    #@10
    .line 459
    :goto_10
    return-void

    #@11
    .line 457
    :cond_11
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@13
    move-object v1, p1

    #@14
    move-object v2, p2

    #@15
    move-object v3, p3

    #@16
    move-object v4, p4

    #@17
    move-object v5, p5

    #@18
    move-object/from16 v6, p6

    #@1a
    move/from16 v7, p7

    #@1c
    move/from16 v8, p8

    #@1e
    move/from16 v9, p9

    #@20
    invoke-virtual/range {v0 .. v9}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V

    #@23
    goto :goto_10
.end method

.method protected sendMultipartTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V
    .registers 20
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    #@0
    .prologue
    .line 469
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_19

    #@6
    .line 470
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@8
    move-object v1, p1

    #@9
    move-object v2, p2

    #@a
    move-object v3, p3

    #@b
    move-object v4, p4

    #@c
    move-object v5, p5

    #@d
    move-object/from16 v6, p6

    #@f
    move/from16 v7, p7

    #@11
    move/from16 v8, p8

    #@13
    move/from16 v9, p9

    #@15
    invoke-virtual/range {v0 .. v9}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V

    #@18
    .line 474
    :goto_18
    return-void

    #@19
    .line 472
    :cond_19
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@1b
    move-object v1, p1

    #@1c
    move-object v2, p2

    #@1d
    move-object v3, p3

    #@1e
    move-object v4, p4

    #@1f
    move-object v5, p5

    #@20
    move-object/from16 v6, p6

    #@22
    move/from16 v7, p7

    #@24
    move/from16 v8, p8

    #@26
    move/from16 v9, p9

    #@28
    invoke-virtual/range {v0 .. v9}, Lcom/android/internal/telephony/SMSDispatcher;->sendMultipartTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V

    #@2b
    goto :goto_18
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V
    .registers 10
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "format"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"

    #@0
    .prologue
    .line 588
    const-string v0, "sendNewSubmitPdu(), Error! Not implemented for IMS."

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 589
    return-void
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;)V
    .registers 11
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 605
    const-string v0, "sendNewSubmitPdu(), Error! Not implemented for IMS."

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 606
    return-void
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;Z)V
    .registers 12
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"
    .parameter "cbAddress"
    .parameter "isMultiPart"

    #@0
    .prologue
    .line 614
    const-string v0, "sendNewSubmitPdu(), Error! Not implemented for IMS."

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 615
    return-void
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZZ)V
    .registers 11
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"
    .parameter "isMultiPart"

    #@0
    .prologue
    .line 596
    const-string v0, "sendNewSubmitPdu(), Error! Not implemented for IMS."

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 597
    return-void
.end method

.method protected sendNewSubmitPduforEmailoverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V
    .registers 10
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"

    #@0
    .prologue
    .line 639
    const-string v0, "sendNewSubmitPduforEmailoverSms(), Error! Not implemented for IMS."

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 640
    return-void
.end method

.method public sendRetrySms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 16
    .parameter "tracker"

    #@0
    .prologue
    .line 319
    iget-object v7, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mFormat:Ljava/lang/String;

    #@2
    .line 322
    .local v7, oldFormat:Ljava/lang/String;
    const/4 v11, 0x2

    #@3
    iget-object v12, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@8
    move-result v12

    #@9
    if-ne v11, v12, :cond_3e

    #@b
    iget-object v11, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@d
    invoke-virtual {v11}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@10
    move-result-object v6

    #@11
    .line 327
    .local v6, newFormat:Ljava/lang/String;
    :goto_11
    const/4 v11, 0x0

    #@12
    const-string v12, "check_radio_tech_for_msg_sending"

    #@14
    invoke-static {v11, v12}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@17
    move-result v11

    #@18
    const/4 v12, 0x1

    #@19
    if-ne v11, v12, :cond_27

    #@1b
    .line 329
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@1e
    move-result v11

    #@1f
    if-eqz v11, :cond_45

    #@21
    iget-object v11, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@23
    invoke-virtual {v11}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    .line 339
    :cond_27
    :goto_27
    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v11

    #@2b
    if-eqz v11, :cond_57

    #@2d
    .line 340
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaFormat(Ljava/lang/String;)Z

    #@30
    move-result v11

    #@31
    if-eqz v11, :cond_4c

    #@33
    .line 341
    const-string v11, "sendRetrySms(), old format matched new format (cdma)"

    #@35
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@38
    .line 342
    iget-object v11, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@3a
    invoke-virtual {v11, p1}, Lcom/android/internal/telephony/SMSDispatcher;->sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@3d
    .line 426
    :cond_3d
    :goto_3d
    return-void

    #@3e
    .line 322
    .end local v6           #newFormat:Ljava/lang/String;
    :cond_3e
    iget-object v11, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@40
    invoke-virtual {v11}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@43
    move-result-object v6

    #@44
    goto :goto_11

    #@45
    .line 329
    .restart local v6       #newFormat:Ljava/lang/String;
    :cond_45
    iget-object v11, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@47
    invoke-virtual {v11}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    goto :goto_27

    #@4c
    .line 345
    :cond_4c
    const-string v11, "sendRetrySms(), old format matched new format (gsm)"

    #@4e
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@51
    .line 346
    iget-object v11, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@53
    invoke-virtual {v11, p1}, Lcom/android/internal/telephony/SMSDispatcher;->sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@56
    goto :goto_3d

    #@57
    .line 352
    :cond_57
    iget-object v5, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mData:Ljava/util/HashMap;

    #@59
    .line 355
    .local v5, map:Ljava/util/HashMap;
    const/4 v11, 0x0

    #@5a
    const-string v12, "kddi_domain_notification"

    #@5c
    invoke-static {v11, v12}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@5f
    move-result v11

    #@60
    const/4 v12, 0x1

    #@61
    if-ne v11, v12, :cond_6f

    #@63
    .line 356
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isDomainNotiMessage(Ljava/util/HashMap;)Z

    #@66
    move-result v11

    #@67
    if-eqz v11, :cond_6f

    #@69
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaFormat(Ljava/lang/String;)Z

    #@6c
    move-result v11

    #@6d
    if-eqz v11, :cond_3d

    #@6f
    .line 367
    :cond_6f
    const-string v11, "scAddr"

    #@71
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@74
    move-result v11

    #@75
    if-eqz v11, :cond_97

    #@77
    const-string v11, "destAddr"

    #@79
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@7c
    move-result v11

    #@7d
    if-eqz v11, :cond_97

    #@7f
    const-string v11, "text"

    #@81
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@84
    move-result v11

    #@85
    if-nez v11, :cond_ac

    #@87
    const-string v11, "data"

    #@89
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@8c
    move-result v11

    #@8d
    if-eqz v11, :cond_97

    #@8f
    const-string v11, "destPort"

    #@91
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@94
    move-result v11

    #@95
    if-nez v11, :cond_ac

    #@97
    .line 371
    :cond_97
    const-string v11, "sendRetrySms(), old format matched new format (gsm)"

    #@99
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@9c
    .line 372
    iget-object v11, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@9e
    if-eqz v11, :cond_3d

    #@a0
    .line 373
    const/4 v4, 0x1

    #@a1
    .line 376
    .local v4, error:I
    :try_start_a1
    iget-object v11, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@a3
    iget-object v12, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@a5
    const/4 v13, 0x0

    #@a6
    invoke-virtual {v11, v12, v4, v13}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_a9
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_a1 .. :try_end_a9} :catch_aa

    #@a9
    goto :goto_3d

    #@aa
    .line 377
    :catch_aa
    move-exception v11

    #@ab
    goto :goto_3d

    #@ac
    .line 381
    .end local v4           #error:I
    :cond_ac
    const-string v11, "scAddr"

    #@ae
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b1
    move-result-object v9

    #@b2
    check-cast v9, Ljava/lang/String;

    #@b4
    .line 382
    .local v9, scAddr:Ljava/lang/String;
    const-string v11, "destAddr"

    #@b6
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b9
    move-result-object v1

    #@ba
    check-cast v1, Ljava/lang/String;

    #@bc
    .line 384
    .local v1, destAddr:Ljava/lang/String;
    const/4 v8, 0x0

    #@bd
    .line 386
    .local v8, pdu:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    const-string v11, "text"

    #@bf
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@c2
    move-result v11

    #@c3
    if-eqz v11, :cond_11c

    #@c5
    .line 387
    const-string v11, "sendRetrySms(), sms failed was text"

    #@c7
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@ca
    .line 388
    const-string v11, "text"

    #@cc
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@cf
    move-result-object v10

    #@d0
    check-cast v10, Ljava/lang/String;

    #@d2
    .line 390
    .local v10, text:Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaFormat(Ljava/lang/String;)Z

    #@d5
    move-result v11

    #@d6
    if-eqz v11, :cond_10a

    #@d8
    .line 391
    const-string v11, "sendRetrySms(), old format (gsm) ==> new format (cdma)"

    #@da
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@dd
    .line 392
    iget-object v11, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mDeliveryIntent:Landroid/app/PendingIntent;

    #@df
    if-eqz v11, :cond_108

    #@e1
    const/4 v11, 0x1

    #@e2
    :goto_e2
    const/4 v12, 0x0

    #@e3
    invoke-static {v9, v1, v10, v11, v12}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@e6
    move-result-object v8

    #@e7
    .line 418
    .end local v10           #text:Ljava/lang/String;
    :cond_e7
    :goto_e7
    const-string v11, "smsc"

    #@e9
    iget-object v12, v8, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@eb
    invoke-virtual {v5, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@ee
    .line 419
    const-string v11, "pdu"

    #@f0
    iget-object v12, v8, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@f2
    invoke-virtual {v5, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f5
    .line 421
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaFormat(Ljava/lang/String;)Z

    #@f8
    move-result v11

    #@f9
    if-eqz v11, :cond_16d

    #@fb
    iget-object v3, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@fd
    .line 424
    .local v3, dispatcher:Lcom/android/internal/telephony/SMSDispatcher;
    :goto_fd
    invoke-virtual {v3}, Lcom/android/internal/telephony/SMSDispatcher;->getFormat()Ljava/lang/String;

    #@100
    move-result-object v11

    #@101
    iput-object v11, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mFormat:Ljava/lang/String;

    #@103
    .line 425
    invoke-virtual {v3, p1}, Lcom/android/internal/telephony/SMSDispatcher;->sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@106
    goto/16 :goto_3d

    #@108
    .line 392
    .end local v3           #dispatcher:Lcom/android/internal/telephony/SMSDispatcher;
    .restart local v10       #text:Ljava/lang/String;
    :cond_108
    const/4 v11, 0x0

    #@109
    goto :goto_e2

    #@10a
    .line 395
    :cond_10a
    const-string v11, "sendRetrySms(), old format (cdma) ==> new format (gsm)"

    #@10c
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@10f
    .line 396
    iget-object v11, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mDeliveryIntent:Landroid/app/PendingIntent;

    #@111
    if-eqz v11, :cond_11a

    #@113
    const/4 v11, 0x1

    #@114
    :goto_114
    const/4 v12, 0x0

    #@115
    invoke-static {v9, v1, v10, v11, v12}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@118
    move-result-object v8

    #@119
    goto :goto_e7

    #@11a
    :cond_11a
    const/4 v11, 0x0

    #@11b
    goto :goto_114

    #@11c
    .line 399
    .end local v10           #text:Ljava/lang/String;
    :cond_11c
    const-string v11, "data"

    #@11e
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@121
    move-result v11

    #@122
    if-eqz v11, :cond_e7

    #@124
    .line 400
    const-string v11, "sendRetrySms(), sms failed was data"

    #@126
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@129
    .line 401
    const-string v11, "data"

    #@12b
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12e
    move-result-object v11

    #@12f
    check-cast v11, [B

    #@131
    move-object v0, v11

    #@132
    check-cast v0, [B

    #@134
    .line 402
    .local v0, data:[B
    const-string v11, "destPort"

    #@136
    invoke-virtual {v5, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@139
    move-result-object v2

    #@13a
    check-cast v2, Ljava/lang/Integer;

    #@13c
    .line 404
    .local v2, destPort:Ljava/lang/Integer;
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaFormat(Ljava/lang/String;)Z

    #@13f
    move-result v11

    #@140
    if-eqz v11, :cond_157

    #@142
    .line 405
    const-string v11, "sendRetrySms(), old format (gsm) ==> new format (cdma)"

    #@144
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@147
    .line 406
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@14a
    move-result v12

    #@14b
    iget-object v11, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mDeliveryIntent:Landroid/app/PendingIntent;

    #@14d
    if-eqz v11, :cond_155

    #@14f
    const/4 v11, 0x1

    #@150
    :goto_150
    invoke-static {v9, v1, v12, v0, v11}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;I[BZ)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@153
    move-result-object v8

    #@154
    goto :goto_e7

    #@155
    :cond_155
    const/4 v11, 0x0

    #@156
    goto :goto_150

    #@157
    .line 410
    :cond_157
    const-string v11, "sendRetrySms(), old format (cdma) ==> new format (gsm)"

    #@159
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@15c
    .line 411
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@15f
    move-result v12

    #@160
    iget-object v11, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mDeliveryIntent:Landroid/app/PendingIntent;

    #@162
    if-eqz v11, :cond_16b

    #@164
    const/4 v11, 0x1

    #@165
    :goto_165
    invoke-static {v9, v1, v12, v0, v11}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;I[BZ)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@168
    move-result-object v8

    #@169
    goto/16 :goto_e7

    #@16b
    :cond_16b
    const/4 v11, 0x0

    #@16c
    goto :goto_165

    #@16d
    .line 421
    .end local v0           #data:[B
    .end local v2           #destPort:Ljava/lang/Integer;
    :cond_16d
    iget-object v3, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@16f
    goto :goto_fd
.end method

.method protected sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 3
    .parameter "tracker"

    #@0
    .prologue
    .line 268
    const-string v0, "sendSms(), sendSms should never be called from here!"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 269
    return-void
.end method

.method protected sendSmsMore(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 2
    .parameter "tracker"

    #@0
    .prologue
    .line 492
    return-void
.end method

.method protected sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 12
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    .line 274
    const-string v0, "sendText(), start"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 275
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_16

    #@b
    .line 276
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@d
    move-object v1, p1

    #@e
    move-object v2, p2

    #@f
    move-object v3, p3

    #@10
    move-object v4, p4

    #@11
    move-object v5, p5

    #@12
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@15
    .line 282
    :goto_15
    return-void

    #@16
    .line 279
    :cond_16
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@18
    move-object v1, p1

    #@19
    move-object v2, p2

    #@1a
    move-object v3, p3

    #@1b
    move-object v4, p4

    #@1c
    move-object v5, p5

    #@1d
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@20
    goto :goto_15
.end method

.method protected sendTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V
    .registers 20
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"

    #@0
    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 438
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@8
    move-object v1, p1

    #@9
    move-object v2, p2

    #@a
    move-object v3, p3

    #@b
    move-object v4, p4

    #@c
    move-object v5, p5

    #@d
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/SMSDispatcher;->sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@10
    .line 443
    :goto_10
    return-void

    #@11
    .line 441
    :cond_11
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@13
    move-object v1, p1

    #@14
    move-object v2, p2

    #@15
    move-object v3, p3

    #@16
    move-object v4, p4

    #@17
    move-object v5, p5

    #@18
    move-object/from16 v6, p6

    #@1a
    move/from16 v7, p7

    #@1c
    move/from16 v8, p8

    #@1e
    move/from16 v9, p9

    #@20
    invoke-virtual/range {v0 .. v9}, Lcom/android/internal/telephony/SMSDispatcher;->sendTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@23
    goto :goto_10
.end method

.method protected sendTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V
    .registers 20
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"

    #@0
    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isCdmaMo()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_19

    #@6
    .line 482
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@8
    move-object v1, p1

    #@9
    move-object v2, p2

    #@a
    move-object v3, p3

    #@b
    move-object v4, p4

    #@c
    move-object v5, p5

    #@d
    move-object/from16 v6, p6

    #@f
    move/from16 v7, p7

    #@11
    move/from16 v8, p8

    #@13
    move/from16 v9, p9

    #@15
    invoke-virtual/range {v0 .. v9}, Lcom/android/internal/telephony/SMSDispatcher;->sendTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@18
    .line 486
    :goto_18
    return-void

    #@19
    .line 484
    :cond_19
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@1b
    move-object v1, p1

    #@1c
    move-object v2, p2

    #@1d
    move-object v3, p3

    #@1e
    move-object v4, p4

    #@1f
    move-object v5, p5

    #@20
    move-object/from16 v6, p6

    #@22
    move/from16 v7, p7

    #@24
    move/from16 v8, p8

    #@26
    move/from16 v9, p9

    #@28
    invoke-virtual/range {v0 .. v9}, Lcom/android/internal/telephony/SMSDispatcher;->sendTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@2b
    goto :goto_18
.end method

.method protected startTestCase(Ljava/lang/String;I)V
    .registers 5
    .parameter "pdu"
    .parameter "num"

    #@0
    .prologue
    .line 579
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "startTestCase(), [KDDI] startTestCase  pdu: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "  num :"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@20
    .line 580
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@22
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;

    #@24
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->startTestCase(Ljava/lang/String;I)V

    #@27
    .line 581
    return-void
.end method

.method protected updatePhoneObject(Lcom/android/internal/telephony/PhoneBase;)V
    .registers 3
    .parameter "phone"

    #@0
    .prologue
    .line 87
    const-string v0, "updatePhoneObject(), In IMS updatePhoneObject "

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 88
    invoke-super {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->updatePhoneObject(Lcom/android/internal/telephony/PhoneBase;)V

    #@8
    .line 89
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mCdmaDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@a
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->updatePhoneObject(Lcom/android/internal/telephony/PhoneBase;)V

    #@d
    .line 90
    iget-object v0, p0, Lcom/android/internal/telephony/ImsSMSDispatcher;->mGsmDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@f
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->updatePhoneObject(Lcom/android/internal/telephony/PhoneBase;)V

    #@12
    .line 91
    return-void
.end method
