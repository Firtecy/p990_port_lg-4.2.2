.class Lcom/android/internal/telephony/DataConnectionTracker$3;
.super Ljava/lang/Object;
.source "DataConnectionTracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnectionTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/DataConnectionTracker;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/DataConnectionTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 901
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnectionTracker$3;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 904
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker$3;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->updateDataActivity()V

    #@5
    .line 906
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker$3;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@7
    iget-boolean v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsScreenOn:Z

    #@9
    if-eqz v0, :cond_2c

    #@b
    .line 907
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker$3;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@d
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker$3;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@f
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mResolver:Landroid/content/ContentResolver;

    #@11
    const-string v2, "pdp_watchdog_poll_interval_ms"

    #@13
    const/16 v3, 0x3e8

    #@15
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@18
    move-result v1

    #@19
    iput v1, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollPeriod:I

    #@1b
    .line 915
    :goto_1b
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker$3;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1d
    iget-boolean v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollEnabled:Z

    #@1f
    if-eqz v0, :cond_2b

    #@21
    .line 916
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker$3;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@23
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker$3;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@25
    iget v1, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollPeriod:I

    #@27
    int-to-long v1, v1

    #@28
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->postDelayed(Ljava/lang/Runnable;J)Z

    #@2b
    .line 918
    :cond_2b
    return-void

    #@2c
    .line 910
    :cond_2c
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker$3;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2e
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker$3;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@30
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mResolver:Landroid/content/ContentResolver;

    #@32
    const-string v2, "pdp_watchdog_long_poll_interval_ms"

    #@34
    const v3, 0x927c0

    #@37
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3a
    move-result v1

    #@3b
    iput v1, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollPeriod:I

    #@3d
    goto :goto_1b
.end method
