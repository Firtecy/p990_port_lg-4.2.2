.class public Lcom/android/internal/telephony/SmsResponse;
.super Ljava/lang/Object;
.source "SmsResponse.java"


# instance fields
.field ackPdu:Ljava/lang/String;

.field errorCode:I

.field messageRef:I


# direct methods
.method public constructor <init>(ILjava/lang/String;I)V
    .registers 4
    .parameter "messageRef"
    .parameter "ackPdu"
    .parameter "errorCode"

    #@0
    .prologue
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 36
    iput p1, p0, Lcom/android/internal/telephony/SmsResponse;->messageRef:I

    #@5
    .line 37
    iput-object p2, p0, Lcom/android/internal/telephony/SmsResponse;->ackPdu:Ljava/lang/String;

    #@7
    .line 38
    iput p3, p0, Lcom/android/internal/telephony/SmsResponse;->errorCode:I

    #@9
    .line 39
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 42
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "{ messageRef = "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    iget v2, p0, Lcom/android/internal/telephony/SmsResponse;->messageRef:I

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", errorCode = "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget v2, p0, Lcom/android/internal/telephony/SmsResponse;->errorCode:I

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, ", ackPdu = "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    iget-object v2, p0, Lcom/android/internal/telephony/SmsResponse;->ackPdu:Ljava/lang/String;

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, "}"

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    .line 46
    .local v0, ret:Ljava/lang/String;
    return-object v0
.end method
