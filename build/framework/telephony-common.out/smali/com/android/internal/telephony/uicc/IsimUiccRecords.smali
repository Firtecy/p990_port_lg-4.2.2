.class public final Lcom/android/internal/telephony/uicc/IsimUiccRecords;
.super Lcom/android/internal/telephony/uicc/IccRecords;
.source "IsimUiccRecords.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IsimRecords;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/uicc/IsimUiccRecords$1;,
        Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;,
        Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimIstLoaded;,
        Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimDomainLoaded;,
        Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimImpuLoaded;,
        Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimImpiLoaded;
    }
.end annotation


# static fields
.field public static final ACTION_REREAD_ISIM:Ljava/lang/String; = "com.movial.reread_isim_records"

.field private static final DBG:Z = true

.field private static final DUMP_RECORDS:Z = true

.field private static final ENABLE_ISIM_TEST:Z = false

.field private static ENABLE_PRIVACY_LOG:Z = false

.field protected static final EVENT_AKA_AUTHENTICATE_DONE:I = 0x5a

.field private static final EVENT_APP_READY:I = 0x1

.field protected static final EVENT_DEACTIVATE_APP_DONE:I = 0x5c

.field protected static final EVENT_GBA_AUTHENTICATE_BOOTSTRAP_DONE:I = 0x5d

.field protected static final EVENT_GBA_AUTHENTICATE_NAF_DONE:I = 0x5e

.field private static final EVENT_ISIM_REFRESH:I = 0x1f

.field protected static final EVENT_SELECT_APPLICATION_DONE:I = 0x5b

.field public static final INTENT_GBA_INIT:Ljava/lang/String; = "com.movial.gba_initialized"

.field protected static final LOG_TAG:Ljava/lang/String; = "GSM"

.field private static final TAG_ISIM_VALUE:I = 0x80

.field static isim_session_id:I


# instance fields
.field aka_autn:Ljava/lang/String;

.field aka_rand:Ljava/lang/String;

.field auts:Ljava/lang/String;

.field ck:Ljava/lang/String;

.field gba_autn:Ljava/lang/String;

.field gba_rand:Ljava/lang/String;

.field ik:Ljava/lang/String;

.field kc:Ljava/lang/String;

.field private mBtid:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mGbaRecordsRequested:Z

.field private mIsGbaSupported:Z

.field private mIsimDomain:Ljava/lang/String;

.field private mIsimGbabp:Ljava/lang/String;

.field private mIsimImpi:Ljava/lang/String;

.field private mIsimImpu:[Ljava/lang/String;

.field private mIsimIst:Ljava/lang/String;

.field mIsimRecordsLoaded:Z

.field private mKeyLifetime:Ljava/lang/String;

.field private mRand:[B

.field naf_id:Ljava/lang/String;

.field res:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 77
    const-string v2, "persist.service.privacy.enable"

    #@3
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    const-string v3, "ATT"

    #@9
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_1b

    #@f
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    const-string v3, "TMO"

    #@15
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_31

    #@1b
    :cond_1b
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    const-string v3, "US"

    #@21
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_31

    #@27
    move v0, v1

    #@28
    :goto_28
    invoke-static {v2, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@2b
    move-result v0

    #@2c
    sput-boolean v0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->ENABLE_PRIVACY_LOG:Z

    #@2e
    .line 123
    sput v1, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->isim_session_id:I

    #@30
    return-void

    #@31
    .line 77
    :cond_31
    const/4 v0, 0x1

    #@32
    goto :goto_28
.end method

.method public constructor <init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 7
    .parameter "app"
    .parameter "c"
    .parameter "ci"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 150
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/uicc/IccRecords;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@5
    .line 130
    const-string v0, "651FC366CD44F4A71C8AFB1FFCAA0DAE"

    #@7
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->aka_rand:Ljava/lang/String;

    #@9
    .line 131
    const-string v0, "021CCAB40B9E0000799256368D4BBE31"

    #@b
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->aka_autn:Ljava/lang/String;

    #@d
    .line 132
    const-string v0, "6EEC6907BFEE16EC254C2DD1C9E186F9"

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->gba_rand:Ljava/lang/String;

    #@11
    .line 133
    const-string v0, "70B4E32940B500002B63BEFB6EFDD5D2"

    #@13
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->gba_autn:Ljava/lang/String;

    #@15
    .line 135
    const-string v0, "70696D73796E632E6D73672E656E672E742D6D6F62696C652E636F6D0100000002"

    #@17
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->naf_id:Ljava/lang/String;

    #@19
    .line 142
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mGbaRecordsRequested:Z

    #@1b
    .line 152
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@1d
    .line 155
    iput v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@1f
    .line 157
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mContext:Landroid/content/Context;

    #@21
    .line 162
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@23
    const/16 v1, 0x1f

    #@25
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->registerForIccAppRefresh(Landroid/os/Handler;ILjava/lang/Object;)V

    #@28
    .line 165
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2a
    const/4 v1, 0x1

    #@2b
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->registerForReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@2e
    .line 166
    return-void
.end method

.method static synthetic access$1000(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsGbaSupported:Z

    #@2
    return v0
.end method

.method static synthetic access$1002(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsGbaSupported:Z

    #@2
    return p1
.end method

.method static synthetic access$1200(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)[B
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mRand:[B

    #@2
    return-object v0
.end method

.method static synthetic access$1202(Lcom/android/internal/telephony/uicc/IsimUiccRecords;[B)[B
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mRand:[B

    #@2
    return-object p1
.end method

.method static synthetic access$1300(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mBtid:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1302(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mBtid:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1400(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mKeyLifetime:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1402(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mKeyLifetime:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1500(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimGbabp:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1502(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimGbabp:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimImpi:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimImpi:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$500([B)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    invoke-static {p0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->isimTlvToString([B)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$600()Z
    .registers 1

    #@0
    .prologue
    .line 73
    sget-boolean v0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->ENABLE_PRIVACY_LOG:Z

    #@2
    return v0
.end method

.method static synthetic access$700(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimImpu:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$702(Lcom/android/internal/telephony/uicc/IsimUiccRecords;[Ljava/lang/String;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimImpu:[Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$800(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimDomain:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$802(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimDomain:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$900(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimIst:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$902(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimIst:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method private handleFileUpdate(I)V
    .registers 7
    .parameter "efid"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/16 v3, 0x64

    #@3
    .line 463
    packed-switch p1, :pswitch_data_66

    #@6
    .line 495
    :pswitch_6
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->fetchIsimGbaRecords()V

    #@9
    .line 498
    :goto_9
    return-void

    #@a
    .line 465
    :pswitch_a
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@c
    const/16 v1, 0x6f02

    #@e
    new-instance v2, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimImpiLoaded;

    #@10
    invoke-direct {v2, p0, v4}, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimImpiLoaded;-><init>(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Lcom/android/internal/telephony/uicc/IsimUiccRecords$1;)V

    #@13
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@1a
    .line 467
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@1c
    add-int/lit8 v0, v0, 0x1

    #@1e
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@20
    goto :goto_9

    #@21
    .line 471
    :pswitch_21
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@23
    const/16 v1, 0x6f04

    #@25
    new-instance v2, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimImpuLoaded;

    #@27
    invoke-direct {v2, p0, v4}, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimImpuLoaded;-><init>(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Lcom/android/internal/telephony/uicc/IsimUiccRecords$1;)V

    #@2a
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V

    #@31
    .line 473
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@33
    add-int/lit8 v0, v0, 0x1

    #@35
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@37
    goto :goto_9

    #@38
    .line 477
    :pswitch_38
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@3a
    const/16 v1, 0x6f03

    #@3c
    new-instance v2, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimDomainLoaded;

    #@3e
    invoke-direct {v2, p0, v4}, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimDomainLoaded;-><init>(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Lcom/android/internal/telephony/uicc/IsimUiccRecords$1;)V

    #@41
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@48
    .line 479
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@4a
    add-int/lit8 v0, v0, 0x1

    #@4c
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@4e
    goto :goto_9

    #@4f
    .line 483
    :pswitch_4f
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@51
    const/16 v1, 0x6f07

    #@53
    new-instance v2, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimIstLoaded;

    #@55
    invoke-direct {v2, p0, v4}, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimIstLoaded;-><init>(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Lcom/android/internal/telephony/uicc/IsimUiccRecords$1;)V

    #@58
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@5b
    move-result-object v2

    #@5c
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@5f
    .line 485
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@61
    add-int/lit8 v0, v0, 0x1

    #@63
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@65
    goto :goto_9

    #@66
    .line 463
    :pswitch_data_66
    .packed-switch 0x6f02
        :pswitch_a
        :pswitch_38
        :pswitch_21
        :pswitch_6
        :pswitch_6
        :pswitch_4f
    .end packed-switch
.end method

.method private handleSimRefresh(Landroid/os/AsyncResult;)V
    .registers 5
    .parameter "ar"

    #@0
    .prologue
    .line 502
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;

    #@4
    .line 503
    .local v0, refreshResponse:Lcom/android/internal/telephony/uicc/IccRefreshResponse;
    if-nez v0, :cond_c

    #@6
    .line 504
    const-string v1, "handleSimRefresh received without input"

    #@8
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@b
    .line 573
    :goto_b
    return-void

    #@c
    .line 508
    :cond_c
    sget-boolean v1, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->ENABLE_PRIVACY_LOG:Z

    #@e
    if-eqz v1, :cond_58

    #@10
    .line 510
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "refreshResponse.refreshResult : "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->refreshResult:I

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@28
    .line 511
    new-instance v1, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v2, "refreshResponse.efId : "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->efId:I

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@40
    .line 512
    new-instance v1, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v2, "refreshResponse.aid : "

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->aid:Ljava/lang/String;

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@58
    .line 515
    :cond_58
    iget v1, v0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->refreshResult:I

    #@5a
    packed-switch v1, :pswitch_data_8a

    #@5d
    .line 570
    :pswitch_5d
    const-string v1, "handleSimRefresh with unknown operation"

    #@5f
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@62
    goto :goto_b

    #@63
    .line 517
    :pswitch_63
    const-string v1, "handleSimRefresh with REFRESH_RESULT_FILE_UPDATE"

    #@65
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@68
    .line 518
    iget v1, v0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->efId:I

    #@6a
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->handleFileUpdate(I)V

    #@6d
    goto :goto_b

    #@6e
    .line 526
    :pswitch_6e
    const-string v1, "handleSimRefresh with REFRESH_RESULT_INIT"

    #@70
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@73
    .line 531
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->fetchIsimGbaRecords()V

    #@76
    goto :goto_b

    #@77
    .line 542
    :pswitch_77
    const-string v1, "handleSimRefresh with REFRESH_RESULT_APP_INIT"

    #@79
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@7c
    .line 547
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->fetchIsimGbaRecords()V

    #@7f
    goto :goto_b

    #@80
    .line 559
    :pswitch_80
    const-string v1, "handleSimRefresh with REFRESH_RESULT_RESET"

    #@82
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@85
    .line 564
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->fetchIsimGbaRecords()V

    #@88
    goto :goto_b

    #@89
    .line 515
    nop

    #@8a
    :pswitch_data_8a
    .packed-switch 0x0
        :pswitch_63
        :pswitch_6e
        :pswitch_80
        :pswitch_5d
        :pswitch_77
    .end packed-switch
.end method

.method private static isimTlvToString([B)Ljava/lang/String;
    .registers 5
    .parameter "record"

    #@0
    .prologue
    .line 710
    new-instance v0, Lcom/android/internal/telephony/gsm/SimTlv;

    #@2
    const/4 v1, 0x0

    #@3
    array-length v2, p0

    #@4
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/telephony/gsm/SimTlv;-><init>([BII)V

    #@7
    .line 712
    .local v0, tlv:Lcom/android/internal/telephony/gsm/SimTlv;
    :cond_7
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/SimTlv;->getTag()I

    #@a
    move-result v1

    #@b
    const/16 v2, 0x80

    #@d
    if-ne v1, v2, :cond_1f

    #@f
    .line 713
    new-instance v1, Ljava/lang/String;

    #@11
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/SimTlv;->getData()[B

    #@14
    move-result-object v2

    #@15
    const-string v3, "UTF-8"

    #@17
    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    #@1a
    move-result-object v3

    #@1b
    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    #@1e
    .line 718
    :goto_1e
    return-object v1

    #@1f
    .line 715
    :cond_1f
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/SimTlv;->nextObject()Z

    #@22
    move-result v1

    #@23
    if-nez v1, :cond_7

    #@25
    .line 717
    const-string v1, "GSM"

    #@27
    const-string v2, "[ISIM] can\'t find TLV tag in ISIM record, returning null"

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 718
    const/4 v1, 0x0

    #@2d
    goto :goto_1e
.end method


# virtual methods
.method public clearISimRecords()V
    .registers 3

    #@0
    .prologue
    .line 433
    const-string v0, "GSM"

    #@2
    const-string v1, "clearing ISIM records"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 435
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->resetRecords()V

    #@a
    .line 436
    return-void
.end method

.method public dispose()V
    .registers 3

    #@0
    .prologue
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Disposing "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@16
    .line 174
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@18
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForIccAppRefresh(Landroid/os/Handler;)V

    #@1b
    .line 176
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1d
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForReady(Landroid/os/Handler;)V

    #@20
    .line 177
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->resetRecords()V

    #@23
    .line 178
    invoke-super {p0}, Lcom/android/internal/telephony/uicc/IccRecords;->dispose()V

    #@26
    .line 179
    return-void
.end method

.method public fetchIsimGbaRecords()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/16 v3, 0x64

    #@3
    .line 404
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@6
    .line 406
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@8
    const/16 v1, 0x6f02

    #@a
    new-instance v2, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimImpiLoaded;

    #@c
    invoke-direct {v2, p0, v4}, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimImpiLoaded;-><init>(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Lcom/android/internal/telephony/uicc/IsimUiccRecords$1;)V

    #@f
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@16
    .line 408
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@18
    add-int/lit8 v0, v0, 0x1

    #@1a
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@1c
    .line 410
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1e
    const/16 v1, 0x6f04

    #@20
    new-instance v2, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimImpuLoaded;

    #@22
    invoke-direct {v2, p0, v4}, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimImpuLoaded;-><init>(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Lcom/android/internal/telephony/uicc/IsimUiccRecords$1;)V

    #@25
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V

    #@2c
    .line 412
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@2e
    add-int/lit8 v0, v0, 0x1

    #@30
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@32
    .line 414
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@34
    const/16 v1, 0x6f03

    #@36
    new-instance v2, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimDomainLoaded;

    #@38
    invoke-direct {v2, p0, v4}, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimDomainLoaded;-><init>(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Lcom/android/internal/telephony/uicc/IsimUiccRecords$1;)V

    #@3b
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@42
    .line 416
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@44
    add-int/lit8 v0, v0, 0x1

    #@46
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@48
    .line 419
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@4a
    const/16 v1, 0x6f07

    #@4c
    new-instance v2, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimIstLoaded;

    #@4e
    invoke-direct {v2, p0, v4}, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimIstLoaded;-><init>(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Lcom/android/internal/telephony/uicc/IsimUiccRecords$1;)V

    #@51
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@58
    .line 421
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@5a
    add-int/lit8 v0, v0, 0x1

    #@5c
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@5e
    .line 426
    new-instance v0, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v1, "fetchIsimGbaRecords "

    #@65
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v0

    #@69
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@6b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v0

    #@6f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v0

    #@73
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@76
    .line 428
    return-void
.end method

.method public fetchIsimRecords()V
    .registers 4

    #@0
    .prologue
    .line 395
    const-string v0, "GSM"

    #@2
    const-string v1, "Selecting ISIM Application"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 396
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@b
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getAid()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    const/16 v2, 0x5b

    #@11
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->obtainMessage(I)Landroid/os/Message;

    #@14
    move-result-object v2

    #@15
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->uiccSelectApplication(Ljava/lang/String;Landroid/os/Message;)V

    #@18
    .line 397
    return-void
.end method

.method public getDisplayRule(Ljava/lang/String;)I
    .registers 3
    .parameter "plmn"

    #@0
    .prologue
    .line 797
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getIsimBtid()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 863
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mBtid:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getIsimDomain()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 781
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimDomain:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getIsimImpi()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 771
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimImpi:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getIsimImpu()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 791
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimImpu:[Ljava/lang/String;

    #@2
    if-eqz v0, :cond_d

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimImpu:[Ljava/lang/String;

    #@6
    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, [Ljava/lang/String;

    #@c
    :goto_c
    return-object v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public getIsimKeyLifetime()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 867
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mKeyLifetime:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getIsimRand()[B
    .registers 2

    #@0
    .prologue
    .line 859
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mRand:[B

    #@2
    return-object v0
.end method

.method public getVoiceMessageCount()I
    .registers 2

    #@0
    .prologue
    .line 849
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 183
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@3
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@6
    move-result v5

    #@7
    if-eqz v5, :cond_34

    #@9
    .line 184
    const-string v5, "GSM"

    #@b
    new-instance v6, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v7, "Received message "

    #@12
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v6

    #@1a
    const-string v7, "["

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    iget v7, p1, Landroid/os/Message;->what:I

    #@22
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    const-string v7, "] while being destroyed. Ignoring."

    #@28
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v6

    #@30
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 392
    :cond_33
    :goto_33
    :sswitch_33
    return-void

    #@34
    .line 190
    :cond_34
    :try_start_34
    iget v5, p1, Landroid/os/Message;->what:I

    #@36
    sparse-switch v5, :sswitch_data_11c

    #@39
    .line 385
    invoke-super {p0, p1}, Lcom/android/internal/telephony/uicc/IccRecords;->handleMessage(Landroid/os/Message;)V
    :try_end_3c
    .catch Ljava/lang/RuntimeException; {:try_start_34 .. :try_end_3c} :catch_3d

    #@3c
    goto :goto_33

    #@3d
    .line 388
    :catch_3d
    move-exception v2

    #@3e
    .line 390
    .local v2, exc:Ljava/lang/RuntimeException;
    const-string v5, "GSM"

    #@40
    const-string v6, "Exception parsing SIM record"

    #@42
    invoke-static {v5, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@45
    goto :goto_33

    #@46
    .line 192
    .end local v2           #exc:Ljava/lang/RuntimeException;
    :sswitch_46
    :try_start_46
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->onReady()V

    #@49
    goto :goto_33

    #@4a
    .line 198
    :sswitch_4a
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4c
    check-cast v1, Landroid/os/AsyncResult;

    #@4e
    .line 199
    .local v1, ar:Landroid/os/AsyncResult;
    const-string v5, "GSM"

    #@50
    new-instance v6, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v7, "ISim REFRESH(EVENT_ISIM_REFRESH) with exception: "

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    iget-object v7, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v6

    #@61
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v6

    #@65
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 200
    iget-object v5, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@6a
    if-nez v5, :cond_33

    #@6c
    .line 208
    const-string v5, "GSM"

    #@6e
    const-string v6, "[ISIM] Send Intent - IPUtils.ACTION_REREAD_ISIM"

    #@70
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 210
    new-instance v4, Landroid/content/Intent;

    #@75
    const-string v5, "com.movial.reread_isim_records"

    #@77
    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7a
    .line 211
    .local v4, intent:Landroid/content/Intent;
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mContext:Landroid/content/Context;

    #@7c
    invoke-virtual {v5, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@7f
    goto :goto_33

    #@80
    .line 232
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v4           #intent:Landroid/content/Intent;
    :sswitch_80
    const-string v5, "GSM"

    #@82
    const-string v6, "EVENT_SELECT_APPLICATION_DONE"

    #@84
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 234
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@89
    check-cast v1, Landroid/os/AsyncResult;

    #@8b
    .line 236
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v5, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@8d
    if-eqz v5, :cond_be

    #@8f
    .line 237
    const-string v5, "GSM"

    #@91
    new-instance v6, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v7, "Exception ISIM Select Application : "

    #@98
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v6

    #@9c
    iget-object v7, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@9e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v6

    #@a2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v6

    #@a6
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 240
    const-string v5, "GSM"

    #@ab
    const-string v6, "[ISIM] Send Intent - IPUtils.INTENT_GBA_INIT"

    #@ad
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 242
    new-instance v4, Landroid/content/Intent;

    #@b2
    const-string v5, "com.movial.gba_initialized"

    #@b4
    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b7
    .line 243
    .restart local v4       #intent:Landroid/content/Intent;
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mContext:Landroid/content/Context;

    #@b9
    invoke-virtual {v5, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@bc
    goto/16 :goto_33

    #@be
    .line 247
    .end local v4           #intent:Landroid/content/Intent;
    :cond_be
    iget-object v5, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c0
    check-cast v5, [I

    #@c2
    move-object v0, v5

    #@c3
    check-cast v0, [I

    #@c5
    move-object v3, v0

    #@c6
    .line 249
    .local v3, int_session_id:[I
    iget-object v5, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@c8
    if-nez v5, :cond_cd

    #@ca
    array-length v5, v3

    #@cb
    if-eq v5, v7, :cond_f4

    #@cd
    .line 250
    :cond_cd
    const-string v5, "GSM"

    #@cf
    new-instance v6, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v7, "Error on Getting ISIM AID with exp "

    #@d6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v6

    #@da
    iget-object v7, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@dc
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v6

    #@e0
    const-string v7, " length "

    #@e2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v6

    #@e6
    array-length v7, v3

    #@e7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v6

    #@eb
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ee
    move-result-object v6

    #@ef
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f2
    goto/16 :goto_33

    #@f4
    .line 253
    :cond_f4
    const-string v5, "GSM"

    #@f6
    new-instance v6, Ljava/lang/StringBuilder;

    #@f8
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@fb
    const-string v7, "int_session_id="

    #@fd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v6

    #@101
    const/4 v7, 0x0

    #@102
    aget v7, v3, v7

    #@104
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@107
    move-result-object v6

    #@108
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10b
    move-result-object v6

    #@10c
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10f
    .line 254
    const/4 v5, 0x0

    #@110
    aget v5, v3, v5

    #@112
    sput v5, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->isim_session_id:I

    #@114
    .line 256
    const/4 v5, 0x1

    #@115
    iput-boolean v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mGbaRecordsRequested:Z

    #@117
    .line 257
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->fetchIsimGbaRecords()V
    :try_end_11a
    .catch Ljava/lang/RuntimeException; {:try_start_46 .. :try_end_11a} :catch_3d

    #@11a
    goto/16 :goto_33

    #@11c
    .line 190
    :sswitch_data_11c
    .sparse-switch
        0x1 -> :sswitch_46
        0x1f -> :sswitch_4a
        0x5a -> :sswitch_33
        0x5b -> :sswitch_80
        0x5c -> :sswitch_33
        0x5d -> :sswitch_33
        0x5e -> :sswitch_33
    .end sparse-switch
.end method

.method public isGbaSupported()Z
    .registers 2

    #@0
    .prologue
    .line 855
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsGbaSupported:Z

    #@2
    return v0
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 840
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[ISIM] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 841
    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 845
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[ISIM] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 846
    return-void
.end method

.method protected onAllRecordsLoaded()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 760
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsLoadedRegistrants:Landroid/os/RegistrantList;

    #@3
    new-instance v1, Landroid/os/AsyncResult;

    #@5
    invoke-direct {v1, v2, v2, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@8
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@b
    .line 762
    return-void
.end method

.method public onReady()V
    .registers 3

    #@0
    .prologue
    .line 803
    const-string v0, "GSM"

    #@2
    const-string v1, "[ISIM] onReady()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 804
    const-string v0, "GSM"

    #@9
    const-string v1, "[ISIM] ENABLE_ISIM_TEST = false"

    #@b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 810
    return-void
.end method

.method protected onRecordLoaded()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 725
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@3
    add-int/lit8 v1, v1, -0x1

    #@5
    iput v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@7
    .line 729
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mGbaRecordsRequested:Z

    #@9
    if-eqz v1, :cond_2b

    #@b
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@d
    if-nez v1, :cond_2b

    #@f
    .line 730
    const-string v1, "GSM"

    #@11
    const-string v2, "GBA records loaded"

    #@13
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 732
    const-string v1, "GSM"

    #@18
    const-string v2, "[ISIM] Send Intent - IPUtils.INTENT_GBA_INIT"

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 734
    new-instance v0, Landroid/content/Intent;

    #@1f
    const-string v1, "com.movial.gba_initialized"

    #@21
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@24
    .line 735
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mContext:Landroid/content/Context;

    #@26
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@29
    .line 736
    iput-boolean v3, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mGbaRecordsRequested:Z

    #@2b
    .line 750
    .end local v0           #intent:Landroid/content/Intent;
    :cond_2b
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@2d
    if-nez v1, :cond_38

    #@2f
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@31
    const/4 v2, 0x1

    #@32
    if-ne v1, v2, :cond_38

    #@34
    .line 751
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->onAllRecordsLoaded()V

    #@37
    .line 756
    :cond_37
    :goto_37
    return-void

    #@38
    .line 752
    :cond_38
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@3a
    if-gez v1, :cond_37

    #@3c
    .line 753
    const-string v1, "recordsToLoad <0, programmer error suspected"

    #@3e
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->loge(Ljava/lang/String;)V

    #@41
    .line 754
    iput v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@43
    goto :goto_37
.end method

.method public onRefresh(Z[I)V
    .registers 3
    .parameter "fileChanged"
    .parameter "fileList"

    #@0
    .prologue
    .line 815
    if-eqz p1, :cond_5

    #@2
    .line 822
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->fetchIsimGbaRecords()V

    #@5
    .line 825
    :cond_5
    return-void
.end method

.method public reset()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 883
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimImpi:Ljava/lang/String;

    #@3
    .line 884
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimDomain:Ljava/lang/String;

    #@5
    .line 885
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimImpu:[Ljava/lang/String;

    #@7
    .line 886
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsGbaSupported:Z

    #@a
    .line 887
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mRand:[B

    #@c
    .line 888
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mBtid:Ljava/lang/String;

    #@e
    .line 889
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mKeyLifetime:Ljava/lang/String;

    #@10
    .line 890
    return-void
.end method

.method protected resetRecords()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 444
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@4
    .line 447
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimImpi:Ljava/lang/String;

    #@6
    .line 448
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimDomain:Ljava/lang/String;

    #@8
    .line 449
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimImpu:[Ljava/lang/String;

    #@a
    .line 450
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimIst:Ljava/lang/String;

    #@c
    .line 451
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimGbabp:Ljava/lang/String;

    #@e
    .line 453
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsGbaSupported:Z

    #@10
    .line 454
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mRand:[B

    #@12
    .line 455
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mBtid:Ljava/lang/String;

    #@14
    .line 456
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mKeyLifetime:Ljava/lang/String;

    #@16
    .line 457
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mIsimRecordsLoaded:Z

    #@18
    .line 459
    return-void
.end method

.method public setIsimBtid(Ljava/lang/String;)V
    .registers 2
    .parameter "btid"

    #@0
    .prologue
    .line 875
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mBtid:Ljava/lang/String;

    #@2
    .line 876
    return-void
.end method

.method public setIsimKeyLifetime(Ljava/lang/String;)V
    .registers 2
    .parameter "keylifetime"

    #@0
    .prologue
    .line 879
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mKeyLifetime:Ljava/lang/String;

    #@2
    .line 880
    return-void
.end method

.method public setIsimRand([B)V
    .registers 2
    .parameter "rand"

    #@0
    .prologue
    .line 871
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->mRand:[B

    #@2
    .line 872
    return-void
.end method

.method public setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "alphaTag"
    .parameter "voiceNumber"
    .parameter "onComplete"

    #@0
    .prologue
    .line 831
    return-void
.end method

.method public setVoiceMessageWaiting(IILandroid/os/Message;)V
    .registers 4
    .parameter "line"
    .parameter "countWaiting"
    .parameter "onComplete"

    #@0
    .prologue
    .line 836
    return-void
.end method
