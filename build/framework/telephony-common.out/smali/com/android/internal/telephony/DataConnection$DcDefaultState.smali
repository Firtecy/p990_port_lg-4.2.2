.class Lcom/android/internal/telephony/DataConnection$DcDefaultState;
.super Lcom/android/internal/util/State;
.source "DataConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DcDefaultState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/DataConnection;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/DataConnection;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1053
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1053
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/DataConnection$DcDefaultState;-><init>(Lcom/android/internal/telephony/DataConnection;)V

    #@3
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 5

    #@0
    .prologue
    .line 1056
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@4
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@6
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@8
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->getHandler()Landroid/os/Handler;

    #@b
    move-result-object v1

    #@c
    const v2, 0x40005

    #@f
    const/4 v3, 0x0

    #@10
    invoke-interface {v0, v1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForRilConnected(Landroid/os/Handler;ILjava/lang/Object;)V

    #@13
    .line 1057
    return-void
.end method

.method public exit()V
    .registers 3

    #@0
    .prologue
    .line 1060
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@4
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@6
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@8
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->getHandler()Landroid/os/Handler;

    #@b
    move-result-object v1

    #@c
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForRilConnected(Landroid/os/Handler;)V

    #@f
    .line 1061
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@11
    invoke-static {v0}, Lcom/android/internal/telephony/DataConnection;->access$000(Lcom/android/internal/telephony/DataConnection;)V

    #@14
    .line 1062
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 20
    .parameter "msg"

    #@0
    .prologue
    .line 1065
    const/16 v16, 0x1

    #@2
    .line 1068
    .local v16, retVal:Z
    move-object/from16 v0, p1

    #@4
    iget v1, v0, Landroid/os/Message;->what:I

    #@6
    sparse-switch v1, :sswitch_data_36a

    #@9
    .line 1238
    move-object/from16 v0, p0

    #@b
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "DcDefaultState: shouldn\'t happen but ignore msg.what=0x"

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    move-object/from16 v0, p1

    #@1a
    iget v3, v0, Landroid/os/Message;->what:I

    #@1c
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@2b
    .line 1244
    :goto_2b
    return v16

    #@2c
    .line 1070
    :sswitch_2c
    move-object/from16 v0, p0

    #@2e
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@30
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@32
    if-eqz v1, :cond_44

    #@34
    .line 1072
    move-object/from16 v0, p0

    #@36
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@38
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@3a
    const v2, 0x11002

    #@3d
    const/4 v3, 0x3

    #@3e
    move-object/from16 v0, p1

    #@40
    invoke-virtual {v1, v0, v2, v3}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;II)V

    #@43
    goto :goto_2b

    #@44
    .line 1075
    :cond_44
    move-object/from16 v0, p0

    #@46
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@48
    new-instance v2, Lcom/android/internal/util/AsyncChannel;

    #@4a
    invoke-direct {v2}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@4d
    iput-object v2, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@4f
    .line 1076
    move-object/from16 v0, p0

    #@51
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@53
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@55
    const/4 v2, 0x0

    #@56
    move-object/from16 v0, p0

    #@58
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@5a
    invoke-virtual {v3}, Lcom/android/internal/telephony/DataConnection;->getHandler()Landroid/os/Handler;

    #@5d
    move-result-object v3

    #@5e
    move-object/from16 v0, p1

    #@60
    iget-object v4, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@62
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/internal/util/AsyncChannel;->connected(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@65
    .line 1078
    move-object/from16 v0, p0

    #@67
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@69
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@6b
    const v3, 0x11002

    #@6e
    const/4 v4, 0x0

    #@6f
    move-object/from16 v0, p0

    #@71
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@73
    invoke-static {v2}, Lcom/android/internal/telephony/DataConnection;->access$100(Lcom/android/internal/telephony/DataConnection;)I

    #@76
    move-result v5

    #@77
    const-string v6, "hi"

    #@79
    move-object/from16 v2, p1

    #@7b
    invoke-virtual/range {v1 .. v6}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;IIILjava/lang/Object;)V

    #@7e
    goto :goto_2b

    #@7f
    .line 1085
    :sswitch_7f
    move-object/from16 v0, p0

    #@81
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@83
    invoke-static {v1}, Lcom/android/internal/telephony/DataConnection;->access$200(Lcom/android/internal/telephony/DataConnection;)V

    #@86
    goto :goto_2b

    #@87
    .line 1089
    :sswitch_87
    move-object/from16 v0, p0

    #@89
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@8b
    invoke-static {v1}, Lcom/android/internal/telephony/DataConnection;->access$300(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/util/IState;

    #@8e
    move-result-object v1

    #@8f
    move-object/from16 v0, p0

    #@91
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@93
    invoke-static {v2}, Lcom/android/internal/telephony/DataConnection;->access$400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@96
    move-result-object v2

    #@97
    if-ne v1, v2, :cond_ae

    #@99
    const/16 v17, 0x1

    #@9b
    .line 1091
    .local v17, val:Z
    :goto_9b
    move-object/from16 v0, p0

    #@9d
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@9f
    iget-object v2, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@a1
    const v3, 0x41001

    #@a4
    if-eqz v17, :cond_b1

    #@a6
    const/4 v1, 0x1

    #@a7
    :goto_a7
    move-object/from16 v0, p1

    #@a9
    invoke-virtual {v2, v0, v3, v1}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;II)V

    #@ac
    goto/16 :goto_2b

    #@ae
    .line 1089
    .end local v17           #val:Z
    :cond_ae
    const/16 v17, 0x0

    #@b0
    goto :goto_9b

    #@b1
    .line 1091
    .restart local v17       #val:Z
    :cond_b1
    const/4 v1, 0x0

    #@b2
    goto :goto_a7

    #@b3
    .line 1096
    .end local v17           #val:Z
    :sswitch_b3
    move-object/from16 v0, p0

    #@b5
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@b7
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@b9
    const v2, 0x41003

    #@bc
    move-object/from16 v0, p0

    #@be
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@c0
    iget v3, v3, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@c2
    move-object/from16 v0, p1

    #@c4
    invoke-virtual {v1, v0, v2, v3}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;II)V

    #@c7
    goto/16 :goto_2b

    #@c9
    .line 1101
    :sswitch_c9
    move-object/from16 v0, p0

    #@cb
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@cd
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@cf
    const v2, 0x41005

    #@d2
    move-object/from16 v0, p0

    #@d4
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@d6
    iget-object v3, v3, Lcom/android/internal/telephony/DataConnection;->mApn:Lcom/android/internal/telephony/DataProfile;

    #@d8
    move-object/from16 v0, p1

    #@da
    invoke-virtual {v1, v0, v2, v3}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V

    #@dd
    goto/16 :goto_2b

    #@df
    .line 1105
    :sswitch_df
    new-instance v12, Landroid/net/LinkProperties;

    #@e1
    move-object/from16 v0, p0

    #@e3
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@e5
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@e7
    invoke-direct {v12, v1}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    #@ea
    .line 1107
    .local v12, lp:Landroid/net/LinkProperties;
    move-object/from16 v0, p0

    #@ec
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@ee
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@f0
    const v2, 0x41007

    #@f3
    move-object/from16 v0, p1

    #@f5
    invoke-virtual {v1, v0, v2, v12}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V

    #@f8
    goto/16 :goto_2b

    #@fa
    .line 1111
    .end local v12           #lp:Landroid/net/LinkProperties;
    :sswitch_fa
    move-object/from16 v0, p1

    #@fc
    iget-object v14, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@fe
    check-cast v14, Landroid/net/ProxyProperties;

    #@100
    .line 1113
    .local v14, proxy:Landroid/net/ProxyProperties;
    move-object/from16 v0, p0

    #@102
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@104
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@106
    invoke-virtual {v1, v14}, Landroid/net/LinkProperties;->setHttpProxy(Landroid/net/ProxyProperties;)V

    #@109
    .line 1114
    move-object/from16 v0, p0

    #@10b
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@10d
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@10f
    const v2, 0x41009

    #@112
    move-object/from16 v0, p1

    #@114
    invoke-virtual {v1, v0, v2}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;I)V

    #@117
    goto/16 :goto_2b

    #@119
    .line 1118
    .end local v14           #proxy:Landroid/net/ProxyProperties;
    :sswitch_119
    move-object/from16 v0, p1

    #@11b
    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@11d
    check-cast v13, Lcom/android/internal/telephony/DataCallState;

    #@11f
    .line 1119
    .local v13, newState:Lcom/android/internal/telephony/DataCallState;
    move-object/from16 v0, p0

    #@121
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@123
    invoke-static {v1, v13}, Lcom/android/internal/telephony/DataConnection;->access$500(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataCallState;)Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;

    #@126
    move-result-object v15

    #@127
    .line 1125
    .local v15, result:Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    move-object/from16 v0, p0

    #@129
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@12b
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@12d
    const v2, 0x4100d

    #@130
    move-object/from16 v0, p1

    #@132
    invoke-virtual {v1, v0, v2, v15}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V

    #@135
    goto/16 :goto_2b

    #@137
    .line 1131
    .end local v13           #newState:Lcom/android/internal/telephony/DataCallState;
    .end local v15           #result:Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    :sswitch_137
    new-instance v11, Landroid/net/LinkCapabilities;

    #@139
    move-object/from16 v0, p0

    #@13b
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@13d
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mCapabilities:Landroid/net/LinkCapabilities;

    #@13f
    invoke-direct {v11, v1}, Landroid/net/LinkCapabilities;-><init>(Landroid/net/LinkCapabilities;)V

    #@142
    .line 1133
    .local v11, lc:Landroid/net/LinkCapabilities;
    move-object/from16 v0, p0

    #@144
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@146
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@148
    const v2, 0x4100b

    #@14b
    move-object/from16 v0, p1

    #@14d
    invoke-virtual {v1, v0, v2, v11}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V

    #@150
    goto/16 :goto_2b

    #@152
    .line 1138
    .end local v11           #lc:Landroid/net/LinkCapabilities;
    :sswitch_152
    move-object/from16 v0, p0

    #@154
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@156
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@158
    const v2, 0x4100f

    #@15b
    move-object/from16 v0, p1

    #@15d
    invoke-virtual {v1, v0, v2}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;I)V

    #@160
    .line 1139
    move-object/from16 v0, p0

    #@162
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@164
    move-object/from16 v0, p0

    #@166
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@168
    invoke-static {v2}, Lcom/android/internal/telephony/DataConnection;->access$400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@16b
    move-result-object v2

    #@16c
    invoke-static {v1, v2}, Lcom/android/internal/telephony/DataConnection;->access$600(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@16f
    goto/16 :goto_2b

    #@171
    .line 1143
    :sswitch_171
    move-object/from16 v0, p0

    #@173
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@175
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@177
    const v2, 0x41011

    #@17a
    move-object/from16 v0, p0

    #@17c
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@17e
    iget v3, v3, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@180
    move-object/from16 v0, p1

    #@182
    invoke-virtual {v1, v0, v2, v3}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;II)V

    #@185
    goto/16 :goto_2b

    #@187
    .line 1147
    :sswitch_187
    move-object/from16 v0, p1

    #@189
    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@18b
    check-cast v7, Lcom/android/internal/telephony/ApnContext;

    #@18d
    .line 1149
    .local v7, apnContext:Lcom/android/internal/telephony/ApnContext;
    move-object/from16 v0, p0

    #@18f
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@191
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mApnList:Ljava/util/List;

    #@193
    invoke-interface {v1, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@196
    move-result v1

    #@197
    if-nez v1, :cond_1a2

    #@199
    .line 1150
    move-object/from16 v0, p0

    #@19b
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@19d
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mApnList:Ljava/util/List;

    #@19f
    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1a2
    .line 1152
    :cond_1a2
    move-object/from16 v0, p0

    #@1a4
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1a6
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@1a8
    const v2, 0x41013

    #@1ab
    move-object/from16 v0, p1

    #@1ad
    invoke-virtual {v1, v0, v2}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;I)V

    #@1b0
    goto/16 :goto_2b

    #@1b2
    .line 1156
    .end local v7           #apnContext:Lcom/android/internal/telephony/ApnContext;
    :sswitch_1b2
    move-object/from16 v0, p1

    #@1b4
    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1b6
    check-cast v7, Lcom/android/internal/telephony/ApnContext;

    #@1b8
    .line 1158
    .restart local v7       #apnContext:Lcom/android/internal/telephony/ApnContext;
    move-object/from16 v0, p0

    #@1ba
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1bc
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mApnList:Ljava/util/List;

    #@1be
    invoke-interface {v1, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@1c1
    .line 1159
    move-object/from16 v0, p0

    #@1c3
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1c5
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@1c7
    const v2, 0x41015

    #@1ca
    move-object/from16 v0, p1

    #@1cc
    invoke-virtual {v1, v0, v2}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;I)V

    #@1cf
    goto/16 :goto_2b

    #@1d1
    .line 1164
    .end local v7           #apnContext:Lcom/android/internal/telephony/ApnContext;
    :sswitch_1d1
    move-object/from16 v0, p0

    #@1d3
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1d5
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@1d7
    const v2, 0x41017

    #@1da
    new-instance v3, Ljava/util/ArrayList;

    #@1dc
    move-object/from16 v0, p0

    #@1de
    iget-object v4, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1e0
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection;->mApnList:Ljava/util/List;

    #@1e2
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@1e5
    move-object/from16 v0, p1

    #@1e7
    invoke-virtual {v1, v0, v2, v3}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V

    #@1ea
    goto/16 :goto_2b

    #@1ec
    .line 1169
    :sswitch_1ec
    move-object/from16 v0, p1

    #@1ee
    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1f0
    check-cast v10, Landroid/app/PendingIntent;

    #@1f2
    .line 1171
    .local v10, intent:Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    #@1f4
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1f6
    iput-object v10, v1, Lcom/android/internal/telephony/DataConnection;->mReconnectIntent:Landroid/app/PendingIntent;

    #@1f8
    .line 1172
    move-object/from16 v0, p0

    #@1fa
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1fc
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@1fe
    const v2, 0x41019

    #@201
    move-object/from16 v0, p1

    #@203
    invoke-virtual {v1, v0, v2}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;I)V

    #@206
    goto/16 :goto_2b

    #@208
    .line 1177
    .end local v10           #intent:Landroid/app/PendingIntent;
    :sswitch_208
    move-object/from16 v0, p0

    #@20a
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@20c
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@20e
    const v2, 0x4101b

    #@211
    move-object/from16 v0, p0

    #@213
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@215
    iget-object v3, v3, Lcom/android/internal/telephony/DataConnection;->mReconnectIntent:Landroid/app/PendingIntent;

    #@217
    move-object/from16 v0, p1

    #@219
    invoke-virtual {v1, v0, v2, v3}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V

    #@21c
    goto/16 :goto_2b

    #@21e
    .line 1182
    :sswitch_21e
    move-object/from16 v0, p0

    #@220
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@222
    const-string v2, "DcDefaultState: msg.what=EVENT_CONNECT, fail not expected"

    #@224
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@227
    .line 1184
    move-object/from16 v0, p0

    #@229
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@22b
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@22d
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@22f
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@232
    move-result-object v1

    #@233
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@235
    if-eqz v1, :cond_29d

    #@237
    .line 1185
    sget v1, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@239
    add-int/lit8 v1, v1, 0x1

    #@23b
    sput v1, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@23d
    .line 1186
    move-object/from16 v0, p0

    #@23f
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@241
    new-instance v2, Ljava/lang/StringBuilder;

    #@243
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@246
    const-string v3, "[LGE_DATA]reset count = "

    #@248
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24b
    move-result-object v2

    #@24c
    sget v3, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@24e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@251
    move-result-object v2

    #@252
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@255
    move-result-object v2

    #@256
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@259
    .line 1188
    move-object/from16 v0, p0

    #@25b
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@25d
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@25f
    if-eqz v1, :cond_29d

    #@261
    .line 1189
    move-object/from16 v0, p0

    #@263
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@265
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@267
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@26a
    move-result-object v1

    #@26b
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@26e
    move-result-object v1

    #@26f
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@272
    move-result v1

    #@273
    if-nez v1, :cond_29d

    #@275
    move-object/from16 v0, p0

    #@277
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@279
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@27b
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@27e
    move-result-object v1

    #@27f
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@282
    move-result-object v1

    #@283
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@286
    move-result v1

    #@287
    if-nez v1, :cond_29d

    #@289
    move-object/from16 v0, p0

    #@28b
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@28d
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@28f
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@292
    move-result-object v1

    #@293
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@296
    move-result-object v1

    #@297
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@29a
    move-result v1

    #@29b
    if-eqz v1, :cond_2ae

    #@29d
    .line 1204
    :cond_29d
    move-object/from16 v0, p1

    #@29f
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2a1
    check-cast v9, Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@2a3
    .line 1205
    .local v9, cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    move-object/from16 v0, p0

    #@2a5
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2a7
    sget-object v2, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2a9
    invoke-static {v1, v9, v2}, Lcom/android/internal/telephony/DataConnection;->access$800(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@2ac
    goto/16 :goto_2b

    #@2ae
    .line 1194
    .end local v9           #cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    :cond_2ae
    sget v1, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@2b0
    const/4 v2, 0x6

    #@2b1
    if-lt v1, v2, :cond_29d

    #@2b3
    .line 1195
    const/4 v1, 0x0

    #@2b4
    sput v1, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@2b6
    .line 1196
    move-object/from16 v0, p0

    #@2b8
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2ba
    const-string v2, "[LGE_DATA]reset radio!!"

    #@2bc
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@2bf
    .line 1197
    move-object/from16 v0, p0

    #@2c1
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2c3
    invoke-static {v1}, Lcom/android/internal/telephony/DataConnection;->access$700(Lcom/android/internal/telephony/DataConnection;)V

    #@2c6
    goto/16 :goto_2b

    #@2c8
    .line 1210
    :sswitch_2c8
    move-object/from16 v0, p0

    #@2ca
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2cc
    new-instance v2, Ljava/lang/StringBuilder;

    #@2ce
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d1
    const-string v3, "DcDefaultState deferring msg.what=EVENT_DISCONNECT"

    #@2d3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d6
    move-result-object v2

    #@2d7
    move-object/from16 v0, p0

    #@2d9
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2db
    iget v3, v3, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@2dd
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e0
    move-result-object v2

    #@2e1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e4
    move-result-object v2

    #@2e5
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@2e8
    .line 1212
    move-object/from16 v0, p0

    #@2ea
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2ec
    move-object/from16 v0, p1

    #@2ee
    invoke-static {v1, v0}, Lcom/android/internal/telephony/DataConnection;->access$900(Lcom/android/internal/telephony/DataConnection;Landroid/os/Message;)V

    #@2f1
    goto/16 :goto_2b

    #@2f3
    .line 1217
    :sswitch_2f3
    move-object/from16 v0, p0

    #@2f5
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2f7
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2fc
    const-string v3, "DcDefaultState deferring msg.what=EVENT_DISCONNECT_ALL"

    #@2fe
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@301
    move-result-object v2

    #@302
    move-object/from16 v0, p0

    #@304
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@306
    iget v3, v3, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@308
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30b
    move-result-object v2

    #@30c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30f
    move-result-object v2

    #@310
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@313
    .line 1219
    move-object/from16 v0, p0

    #@315
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@317
    move-object/from16 v0, p1

    #@319
    invoke-static {v1, v0}, Lcom/android/internal/telephony/DataConnection;->access$1000(Lcom/android/internal/telephony/DataConnection;Landroid/os/Message;)V

    #@31c
    goto/16 :goto_2b

    #@31e
    .line 1223
    :sswitch_31e
    move-object/from16 v0, p1

    #@320
    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@322
    check-cast v8, Landroid/os/AsyncResult;

    #@324
    .line 1224
    .local v8, ar:Landroid/os/AsyncResult;
    iget-object v1, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@326
    if-nez v1, :cond_358

    #@328
    .line 1225
    move-object/from16 v0, p0

    #@32a
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@32c
    iget-object v1, v8, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@32e
    check-cast v1, Ljava/lang/Integer;

    #@330
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@333
    move-result v1

    #@334
    iput v1, v2, Lcom/android/internal/telephony/DataConnection;->mRilVersion:I

    #@336
    .line 1227
    move-object/from16 v0, p0

    #@338
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@33a
    new-instance v2, Ljava/lang/StringBuilder;

    #@33c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@33f
    const-string v3, "DcDefaultState: msg.what=EVENT_RIL_CONNECTED mRilVersion="

    #@341
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@344
    move-result-object v2

    #@345
    move-object/from16 v0, p0

    #@347
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@349
    iget v3, v3, Lcom/android/internal/telephony/DataConnection;->mRilVersion:I

    #@34b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34e
    move-result-object v2

    #@34f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@352
    move-result-object v2

    #@353
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@356
    goto/16 :goto_2b

    #@358
    .line 1231
    :cond_358
    move-object/from16 v0, p0

    #@35a
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@35c
    const-string v2, "Unexpected exception on EVENT_RIL_CONNECTED"

    #@35e
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@361
    .line 1232
    move-object/from16 v0, p0

    #@363
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@365
    const/4 v2, -0x1

    #@366
    iput v2, v1, Lcom/android/internal/telephony/DataConnection;->mRilVersion:I

    #@368
    goto/16 :goto_2b

    #@36a
    .line 1068
    :sswitch_data_36a
    .sparse-switch
        0x11001 -> :sswitch_2c
        0x11004 -> :sswitch_7f
        0x40000 -> :sswitch_21e
        0x40004 -> :sswitch_2c8
        0x40005 -> :sswitch_31e
        0x40006 -> :sswitch_2f3
        0x41000 -> :sswitch_87
        0x41002 -> :sswitch_b3
        0x41004 -> :sswitch_c9
        0x41006 -> :sswitch_df
        0x41008 -> :sswitch_fa
        0x4100a -> :sswitch_137
        0x4100c -> :sswitch_119
        0x4100e -> :sswitch_152
        0x41010 -> :sswitch_171
        0x41012 -> :sswitch_187
        0x41014 -> :sswitch_1b2
        0x41016 -> :sswitch_1d1
        0x41018 -> :sswitch_1ec
        0x4101a -> :sswitch_208
    .end sparse-switch
.end method
