.class Lcom/android/internal/telephony/gfit/GfitUtils$6;
.super Ljava/lang/Object;
.source "GfitUtils.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gfit/GfitUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/gfit/GfitUtils;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/gfit/GfitUtils;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1204
    iput-object p1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$6;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 9
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    const/16 v4, 0xd1

    #@2
    .line 1207
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils$6;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@4
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/gfit/GfitUtils;->hasMessages(I)Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_f

    #@a
    .line 1208
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils$6;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@c
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/gfit/GfitUtils;->removeMessages(I)V

    #@f
    .line 1211
    :cond_f
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils$6;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@11
    iget-object v3, v3, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@13
    aget-object v3, v3, p2

    #@15
    invoke-virtual {v3}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->getOperatorNumeric()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    .line 1212
    .local v1, operatorNumeric:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils$6;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@1b
    iget-object v3, v3, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@1d
    aget-object v3, v3, p2

    #@1f
    invoke-virtual {v3}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->getRAT()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    .line 1214
    .local v2, operatorRat:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils$6;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@25
    iget-object v3, v3, Lcom/android/internal/telephony/gfit/GfitUtils;->mPlmnList:[Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;

    #@27
    aget-object v3, v3, p2

    #@29
    invoke-virtual {v3}, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->getOperatorAlphaLong()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 1215
    .local v0, operatorAlphaLong:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils$6;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@2f
    new-instance v4, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v5, "which = "

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    const-string v5, ". select "

    #@40
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    const-string v5, "/"

    #@4a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v4

    #@4e
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    const-string v5, "/"

    #@54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v4

    #@60
    invoke-static {v3, v4}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$300(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V

    #@63
    .line 1217
    iget-object v3, p0, Lcom/android/internal/telephony/gfit/GfitUtils$6;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@65
    invoke-virtual {v3, v1, v2, v0}, Lcom/android/internal/telephony/gfit/GfitUtils;->sendNetworkSelectionModeManual(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 1219
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    #@6b
    .line 1220
    return-void
.end method
