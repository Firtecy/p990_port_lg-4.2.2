.class public final Lcom/android/internal/telephony/cdma/EriManager;
.super Ljava/lang/Object;
.source "EriManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/EriManager$EriCrcCalculator;,
        Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;,
        Lcom/android/internal/telephony/cdma/EriManager$EriImg;,
        Lcom/android/internal/telephony/cdma/EriManager$EriPrmpt;,
        Lcom/android/internal/telephony/cdma/EriManager$EriFile;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field public static final ENCODING_ASCII:I = 0x2

.field public static final ENCODING_GSM7:I = 0x9

.field public static final ENCODING_IA5:I = 0x3

.field public static final ENCODING_IS_91:I = 0x1

.field public static final ENCODING_KOREAN:I = 0x6

.field public static final ENCODING_LATIN:I = 0x8

.field public static final ENCODING_LATIN_HEBREW:I = 0x7

.field public static final ENCODING_OCTET:I = 0x0

.field public static final ENCODING_OTHERS:I = 0xa

.field public static final ENCODING_SHIFT_JIS:I = 0x5

.field public static final ENCODING_UNICODE:I = 0x4

.field public static final ERI_FROM_FILE_SYSTEM:I = 0x1

.field public static final ERI_FROM_MODEM:I = 0x2

.field public static final ERI_FROM_XML:I = 0x0

.field public static final LENGTH_BITS_CALL_PROMPT_AMOUNT_OF_TEXT_DATA:I = 0x8

.field public static final LENGTH_BITS_CALL_PROMPT_CALL_PROMPT_ID:I = 0x4

.field public static final LENGTH_BITS_CALL_PROMPT_CHARACTER_ENCODING_TYPE:I = 0x5

.field public static final LENGTH_BITS_CALL_PROMPT_RESERVED_BITS1:I = 0x8

.field public static final LENGTH_BITS_CALL_PROMPT_RESERVED_BITS2:I = 0x4

.field public static final LENGTH_BITS_CALL_PROMPT_RESERVED_BITS3:I = 0x3

.field public static final LENGTH_BITS_CALL_PROMPT_TEXT_DATA:I = 0x0

.field public static final LENGTH_BITS_ERI_FILE_CRC:I = 0x10

.field public static final LENGTH_BITS_ERI_TYPE:I = 0x3

.field public static final LENGTH_BITS_ICON_IMAGE_AMOUNT_OF_TEXT_DATA:I = 0x8

.field public static final LENGTH_BITS_ICON_IMAGE_ICON_FILE_NAME:I = 0x0

.field public static final LENGTH_BITS_ICON_IMAGE_IMAGE_ID:I = 0x4

.field public static final LENGTH_BITS_ICON_IMAGE_RESERVED_BITS:I = 0x4

.field public static final LENGTH_BITS_ICON_IMAGE_TYPE:I = 0x3

.field public static final LENGTH_BITS_NUMBER_OF_ERI_ENTRIES:I = 0x6

.field public static final LENGTH_BITS_NUMBER_OF_ICON_IMAGES:I = 0x4

.field public static final LENGTH_BITS_RESERVED_PAD_BITS:I = 0x8

.field public static final LENGTH_BITS_ROAMING_INDICATOR_ALERT_ID:I = 0x3

.field public static final LENGTH_BITS_ROAMING_INDICATOR_AMOUNT_OF_TEXT_DATA:I = 0x8

.field public static final LENGTH_BITS_ROAMING_INDICATOR_CALL_PROMPT_ID:I = 0x2

.field public static final LENGTH_BITS_ROAMING_INDICATOR_CHARACTER_ENCODING_TYPE:I = 0x5

.field public static final LENGTH_BITS_ROAMING_INDICATOR_ERI_TEXT:I = 0x0

.field public static final LENGTH_BITS_ROAMING_INDICATOR_ICON_INDEX:I = 0x4

.field public static final LENGTH_BITS_ROAMING_INDICATOR_ICON_MODE:I = 0x2

.field public static final LENGTH_BITS_ROAMING_INDICATOR_ROAMING_INDICATOR:I = 0x8

.field public static final LENGTH_BITS_VERSION_NUMBER:I = 0x10

.field private static final LOG_TAG:Ljava/lang/String; = "CDMA"

.field private static final VDBG:Z


# instance fields
.field final ERI_BACKUP_FILE_NAME:Ljava/lang/String;

.field final ERI_FILE_NAME:Ljava/lang/String;

.field private indexofhomesystem:Ljava/lang/String;

.field private isEriFileLoaded:Z

.field private mContext:Landroid/content/Context;

.field private mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

.field private mEriFileSource:I

.field private mPhone:Lcom/android/internal/telephony/PhoneBase;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/PhoneBase;Landroid/content/Context;I)V
    .registers 5
    .parameter "phone"
    .parameter "context"
    .parameter "eriFileSource"

    #@0
    .prologue
    .line 273
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 261
    const-string v0, "/eri/eri.bin"

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->ERI_FILE_NAME:Ljava/lang/String;

    #@7
    .line 262
    const-string v0, "/system/etc/eri.bin"

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->ERI_BACKUP_FILE_NAME:Ljava/lang/String;

    #@b
    .line 266
    const/4 v0, 0x0

    #@c
    iput v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFileSource:I

    #@e
    .line 270
    const-string v0, ""

    #@10
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->indexofhomesystem:Ljava/lang/String;

    #@12
    .line 274
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/EriManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@14
    .line 275
    iput-object p2, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@16
    .line 276
    iput p3, p0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFileSource:I

    #@18
    .line 277
    new-instance v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@1a
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/cdma/EriManager$EriFile;-><init>(Lcom/android/internal/telephony/cdma/EriManager;)V

    #@1d
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@1f
    .line 278
    return-void
.end method

.method private getEriDisplayInformation(II)Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    .registers 15
    .parameter "roamInd"
    .parameter "defRoamInd"

    #@0
    .prologue
    const v11, 0x10400c9

    #@3
    const/4 v10, 0x2

    #@4
    const/4 v9, 0x0

    #@5
    const/4 v8, 0x1

    #@6
    const/4 v7, 0x0

    #@7
    .line 829
    iget-boolean v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->isEriFileLoaded:Z

    #@9
    if-eqz v4, :cond_1e

    #@b
    .line 830
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/EriManager;->getEriInfo(I)Lcom/android/internal/telephony/cdma/EriInfo;

    #@e
    move-result-object v1

    #@f
    .line 831
    .local v1, eriInfo:Lcom/android/internal/telephony/cdma/EriInfo;
    if-eqz v1, :cond_1e

    #@11
    .line 833
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@13
    iget v4, v1, Lcom/android/internal/telephony/cdma/EriInfo;->mIconIndex:I

    #@15
    iget v5, v1, Lcom/android/internal/telephony/cdma/EriInfo;->mIconMode:I

    #@17
    iget-object v6, v1, Lcom/android/internal/telephony/cdma/EriInfo;->mEriText:Ljava/lang/String;

    #@19
    invoke-direct {v2, p0, v4, v5, v6}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@1c
    .local v2, ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    move-object v3, v2

    #@1d
    .line 1115
    .end local v1           #eriInfo:Lcom/android/internal/telephony/cdma/EriInfo;
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    .local v3, ret:Ljava/lang/Object;
    :goto_1d
    return-object v3

    #@1e
    .line 842
    .end local v3           #ret:Ljava/lang/Object;
    :cond_1e
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@20
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@27
    move-result v4

    #@28
    if-nez v4, :cond_33

    #@2a
    const-string v4, "SPR"

    #@2c
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@2f
    move-result v4

    #@30
    if-eqz v4, :cond_33

    #@32
    .line 843
    const/4 p1, 0x1

    #@33
    .line 847
    :cond_33
    packed-switch p1, :pswitch_data_2b2

    #@36
    .line 980
    const/16 v4, 0x63

    #@38
    if-ne p1, v4, :cond_194

    #@3a
    .line 981
    const-string v4, "vzw_eri"

    #@3c
    invoke-static {v9, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3f
    move-result v4

    #@40
    if-eqz v4, :cond_194

    #@42
    .line 982
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@44
    const-string v4, ""

    #@46
    invoke-direct {v2, p0, v7, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@49
    .line 986
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    const-string v4, "CDMA"

    #@4b
    const-string v5, "create Eriinfo for femto(99)"

    #@4d
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    :goto_50
    move-object v3, v2

    #@51
    .line 1115
    .restart local v3       #ret:Ljava/lang/Object;
    goto :goto_1d

    #@52
    .line 851
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    .end local v3           #ret:Ljava/lang/Object;
    :pswitch_52
    const-string v4, "vzw_eri"

    #@54
    invoke-static {v9, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@57
    move-result v4

    #@58
    if-eqz v4, :cond_62

    #@5a
    .line 852
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@5c
    const-string v4, ""

    #@5e
    invoke-direct {v2, p0, v7, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@61
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto :goto_50

    #@62
    .line 858
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_62
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@64
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@66
    invoke-virtual {v4, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@6d
    move-result-object v4

    #@6e
    invoke-direct {v2, p0, v7, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@71
    .line 863
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto :goto_50

    #@72
    .line 867
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_72
    const-string v4, "vzw_eri"

    #@74
    invoke-static {v9, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@77
    move-result v4

    #@78
    if-eqz v4, :cond_82

    #@7a
    .line 868
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@7c
    const-string v4, ""

    #@7e
    invoke-direct {v2, p0, v8, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@81
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto :goto_50

    #@82
    .line 875
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_82
    const-string v4, "SPR"

    #@84
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@87
    move-result v4

    #@88
    if-eqz v4, :cond_96

    #@8a
    .line 876
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@8c
    const-string v4, "ro.cdma.home.operator.alpha"

    #@8e
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@91
    move-result-object v4

    #@92
    invoke-direct {v2, p0, v8, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@95
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto :goto_50

    #@96
    .line 882
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_96
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@98
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@9a
    const v5, 0x10400ca

    #@9d
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@a0
    move-result-object v4

    #@a1
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@a4
    move-result-object v4

    #@a5
    invoke-direct {v2, p0, v8, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@a8
    .line 887
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto :goto_50

    #@a9
    .line 891
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_a9
    const-string v4, "vzw_eri"

    #@ab
    invoke-static {v9, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@ae
    move-result v4

    #@af
    if-eqz v4, :cond_b9

    #@b1
    .line 892
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@b3
    const-string v4, ""

    #@b5
    invoke-direct {v2, p0, v10, v8, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@b8
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto :goto_50

    #@b9
    .line 897
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_b9
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@bb
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@bd
    const v5, 0x10400cb

    #@c0
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@c3
    move-result-object v4

    #@c4
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@c7
    move-result-object v4

    #@c8
    invoke-direct {v2, p0, v10, v8, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@cb
    .line 903
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto :goto_50

    #@cc
    .line 908
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_cc
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@ce
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@d0
    const v5, 0x10400cc

    #@d3
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@d6
    move-result-object v4

    #@d7
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@da
    move-result-object v4

    #@db
    invoke-direct {v2, p0, p1, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@de
    .line 912
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@e0
    .line 915
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_e0
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@e2
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@e4
    const v5, 0x10400cd

    #@e7
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@ea
    move-result-object v4

    #@eb
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@ee
    move-result-object v4

    #@ef
    invoke-direct {v2, p0, p1, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@f2
    .line 919
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@f4
    .line 922
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_f4
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@f6
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@f8
    const v5, 0x10400ce

    #@fb
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@fe
    move-result-object v4

    #@ff
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@102
    move-result-object v4

    #@103
    invoke-direct {v2, p0, p1, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@106
    .line 926
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@108
    .line 929
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_108
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@10a
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@10c
    const v5, 0x10400cf

    #@10f
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@112
    move-result-object v4

    #@113
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@116
    move-result-object v4

    #@117
    invoke-direct {v2, p0, p1, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@11a
    .line 933
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@11c
    .line 936
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_11c
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@11e
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@120
    const v5, 0x10400d0

    #@123
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@126
    move-result-object v4

    #@127
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@12a
    move-result-object v4

    #@12b
    invoke-direct {v2, p0, p1, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@12e
    .line 940
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@130
    .line 943
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_130
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@132
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@134
    const v5, 0x10400d1

    #@137
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@13a
    move-result-object v4

    #@13b
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13e
    move-result-object v4

    #@13f
    invoke-direct {v2, p0, p1, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@142
    .line 947
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@144
    .line 950
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_144
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@146
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@148
    const v5, 0x10400d2

    #@14b
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@14e
    move-result-object v4

    #@14f
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@152
    move-result-object v4

    #@153
    invoke-direct {v2, p0, p1, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@156
    .line 954
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@158
    .line 957
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_158
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@15a
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@15c
    const v5, 0x10400d3

    #@15f
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@162
    move-result-object v4

    #@163
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@166
    move-result-object v4

    #@167
    invoke-direct {v2, p0, p1, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@16a
    .line 961
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@16c
    .line 964
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_16c
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@16e
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@170
    const v5, 0x10400d4

    #@173
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@176
    move-result-object v4

    #@177
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@17a
    move-result-object v4

    #@17b
    invoke-direct {v2, p0, p1, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@17e
    .line 968
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@180
    .line 971
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_180
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@182
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@184
    const v5, 0x10400d5

    #@187
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@18a
    move-result-object v4

    #@18b
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@18e
    move-result-object v4

    #@18f
    invoke-direct {v2, p0, p1, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@192
    .line 975
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@194
    .line 991
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_194
    iget-boolean v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->isEriFileLoaded:Z

    #@196
    if-nez v4, :cond_244

    #@198
    .line 993
    const-string v4, "CDMA"

    #@19a
    const-string v5, "ERI File not loaded"

    #@19c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19f
    .line 994
    if-le p2, v10, :cond_1b5

    #@1a1
    .line 996
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@1a3
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@1a5
    const v5, 0x10400cb

    #@1a8
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1ab
    move-result-object v4

    #@1ac
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1af
    move-result-object v4

    #@1b0
    invoke-direct {v2, p0, v10, v8, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@1b3
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@1b5
    .line 1003
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_1b5
    packed-switch p2, :pswitch_data_2d0

    #@1b8
    .line 1064
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@1ba
    const/4 v4, -0x1

    #@1bb
    const/4 v5, -0x1

    #@1bc
    const-string v6, "ERI text"

    #@1be
    invoke-direct {v2, p0, v4, v5, v6}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@1c1
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@1c3
    .line 1006
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_1c3
    const-string v4, "vzw_eri"

    #@1c5
    invoke-static {v9, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1c8
    move-result v4

    #@1c9
    if-eqz v4, :cond_1d4

    #@1cb
    .line 1007
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@1cd
    const-string v4, ""

    #@1cf
    invoke-direct {v2, p0, v7, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@1d2
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@1d4
    .line 1012
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_1d4
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@1d6
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@1d8
    invoke-virtual {v4, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1db
    move-result-object v4

    #@1dc
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1df
    move-result-object v4

    #@1e0
    invoke-direct {v2, p0, v7, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@1e3
    .line 1019
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@1e5
    .line 1023
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_1e5
    const-string v4, "vzw_eri"

    #@1e7
    invoke-static {v9, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1ea
    move-result v4

    #@1eb
    if-eqz v4, :cond_1f6

    #@1ed
    .line 1024
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@1ef
    const-string v4, ""

    #@1f1
    invoke-direct {v2, p0, v8, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@1f4
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@1f6
    .line 1031
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_1f6
    const-string v4, "SPR"

    #@1f8
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@1fb
    move-result v4

    #@1fc
    if-eqz v4, :cond_20b

    #@1fe
    .line 1032
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@200
    const-string v4, "ro.cdma.home.operator.alpha"

    #@202
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@205
    move-result-object v4

    #@206
    invoke-direct {v2, p0, v8, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@209
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@20b
    .line 1038
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_20b
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@20d
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@20f
    const v5, 0x10400ca

    #@212
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@215
    move-result-object v4

    #@216
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@219
    move-result-object v4

    #@21a
    invoke-direct {v2, p0, v8, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@21d
    .line 1043
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@21f
    .line 1047
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :pswitch_21f
    const-string v4, "vzw_eri"

    #@221
    invoke-static {v9, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@224
    move-result v4

    #@225
    if-eqz v4, :cond_230

    #@227
    .line 1048
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@229
    const-string v4, ""

    #@22b
    invoke-direct {v2, p0, v10, v8, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@22e
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@230
    .line 1054
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_230
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@232
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@234
    const v5, 0x10400cb

    #@237
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@23a
    move-result-object v4

    #@23b
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@23e
    move-result-object v4

    #@23f
    invoke-direct {v2, p0, v10, v8, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@242
    .line 1061
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@244
    .line 1069
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_244
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/EriManager;->getEriInfo(I)Lcom/android/internal/telephony/cdma/EriInfo;

    #@247
    move-result-object v1

    #@248
    .line 1070
    .restart local v1       #eriInfo:Lcom/android/internal/telephony/cdma/EriInfo;
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/cdma/EriManager;->getEriInfo(I)Lcom/android/internal/telephony/cdma/EriInfo;

    #@24b
    move-result-object v0

    #@24c
    .line 1071
    .local v0, defEriInfo:Lcom/android/internal/telephony/cdma/EriInfo;
    if-nez v1, :cond_2a5

    #@24e
    .line 1076
    if-nez v0, :cond_298

    #@250
    .line 1077
    const-string v4, "CDMA"

    #@252
    new-instance v5, Ljava/lang/StringBuilder;

    #@254
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@257
    const-string v6, "ERI defRoamInd "

    #@259
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25c
    move-result-object v5

    #@25d
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@260
    move-result-object v5

    #@261
    const-string v6, " not found in ERI file ...on"

    #@263
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@266
    move-result-object v5

    #@267
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26a
    move-result-object v5

    #@26b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26e
    .line 1080
    const-string v4, "vzw_eri"

    #@270
    invoke-static {v9, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@273
    move-result v4

    #@274
    if-eqz v4, :cond_287

    #@276
    .line 1081
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@278
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@27a
    invoke-virtual {v4, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@27d
    move-result-object v4

    #@27e
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@281
    move-result-object v4

    #@282
    invoke-direct {v2, p0, v10, v8, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@285
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@287
    .line 1087
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_287
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@289
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@28b
    invoke-virtual {v4, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@28e
    move-result-object v4

    #@28f
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@292
    move-result-object v4

    #@293
    invoke-direct {v2, p0, v7, v7, v4}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@296
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@298
    .line 1099
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_298
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@29a
    iget v4, v0, Lcom/android/internal/telephony/cdma/EriInfo;->mIconIndex:I

    #@29c
    iget v5, v0, Lcom/android/internal/telephony/cdma/EriInfo;->mIconMode:I

    #@29e
    iget-object v6, v0, Lcom/android/internal/telephony/cdma/EriInfo;->mEriText:Ljava/lang/String;

    #@2a0
    invoke-direct {v2, p0, v4, v5, v6}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@2a3
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@2a5
    .line 1106
    .end local v2           #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    :cond_2a5
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@2a7
    iget v4, v1, Lcom/android/internal/telephony/cdma/EriInfo;->mIconIndex:I

    #@2a9
    iget v5, v1, Lcom/android/internal/telephony/cdma/EriInfo;->mIconMode:I

    #@2ab
    iget-object v6, v1, Lcom/android/internal/telephony/cdma/EriInfo;->mEriText:Ljava/lang/String;

    #@2ad
    invoke-direct {v2, p0, v4, v5, v6}, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IILjava/lang/String;)V

    #@2b0
    .restart local v2       #ret:Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;
    goto/16 :goto_50

    #@2b2
    .line 847
    :pswitch_data_2b2
    .packed-switch 0x0
        :pswitch_52
        :pswitch_72
        :pswitch_a9
        :pswitch_cc
        :pswitch_e0
        :pswitch_f4
        :pswitch_108
        :pswitch_11c
        :pswitch_130
        :pswitch_144
        :pswitch_158
        :pswitch_16c
        :pswitch_180
    .end packed-switch

    #@2d0
    .line 1003
    :pswitch_data_2d0
    .packed-switch 0x0
        :pswitch_1c3
        :pswitch_1e5
        :pswitch_21f
    .end packed-switch
.end method

.method private getEriInfo(I)Lcom/android/internal/telephony/cdma/EriInfo;
    .registers 4
    .parameter "roamingIndicator"

    #@0
    .prologue
    .line 818
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mRoamIndTable:Ljava/util/HashMap;

    #@4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_1d

    #@e
    .line 819
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@10
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mRoamIndTable:Ljava/util/HashMap;

    #@12
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Lcom/android/internal/telephony/cdma/EriInfo;

    #@1c
    .line 821
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_1c
.end method

.method private loadEriFileFromFileSystem()V
    .registers 15

    #@0
    .prologue
    .line 619
    const/4 v8, 0x0

    #@1
    .line 620
    .local v8, stream:Ljava/io/FileInputStream;
    new-instance v3, Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@3
    invoke-direct {v3, p0}, Lcom/android/internal/telephony/cdma/EriManager$EriFile;-><init>(Lcom/android/internal/telephony/cdma/EriManager;)V

    #@6
    .line 621
    .local v3, eriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;
    const/4 v1, 0x0

    #@7
    .line 626
    .local v1, count:I
    :try_start_7
    new-instance v4, Ljava/io/File;

    #@9
    const-string v10, "/eri/eri.bin"

    #@b
    invoke-direct {v4, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@e
    .line 627
    .local v4, eriFileObj:Ljava/io/File;
    new-instance v9, Ljava/io/FileInputStream;

    #@10
    invoke-direct {v9, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_13
    .catchall {:try_start_7 .. :try_end_13} :catchall_83
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_13} :catch_73

    #@13
    .line 628
    .end local v8           #stream:Ljava/io/FileInputStream;
    .local v9, stream:Ljava/io/FileInputStream;
    :try_start_13
    invoke-virtual {v4}, Ljava/io/File;->length()J

    #@16
    move-result-wide v10

    #@17
    const-wide/16 v12, 0x0

    #@19
    cmp-long v10, v10, v12

    #@1b
    if-nez v10, :cond_27

    #@1d
    .line 629
    const-string v10, "CDMA"

    #@1f
    const-string v11, "//eriFileObjlength : 0//"

    #@21
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 630
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/EriManager;->recoverEriFromFileSystem()V
    :try_end_27
    .catchall {:try_start_13 .. :try_end_27} :catchall_df
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_27} :catch_e2

    #@27
    .line 637
    :cond_27
    :try_start_27
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_2a} :catch_d5

    #@2a
    .line 644
    .end local v4           #eriFileObj:Ljava/io/File;
    :goto_2a
    :try_start_2a
    new-instance v8, Ljava/io/FileInputStream;

    #@2c
    const-string v10, "/eri/eri.bin"

    #@2e
    invoke-direct {v8, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_31
    .catchall {:try_start_2a .. :try_end_31} :catchall_bc
    .catch Ljava/io/FileNotFoundException; {:try_start_2a .. :try_end_31} :catch_dc
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_31} :catch_a9

    #@31
    .line 645
    .end local v9           #stream:Ljava/io/FileInputStream;
    .restart local v8       #stream:Ljava/io/FileInputStream;
    :try_start_31
    invoke-virtual {v8}, Ljava/io/FileInputStream;->available()I

    #@34
    move-result v1

    #@35
    .line 646
    new-array v5, v1, [B

    #@37
    .line 648
    .local v5, eriRawData:[B
    :cond_37
    invoke-virtual {v8, v5}, Ljava/io/FileInputStream;->read([B)I
    :try_end_3a
    .catchall {:try_start_31 .. :try_end_3a} :catchall_d8
    .catch Ljava/io/FileNotFoundException; {:try_start_31 .. :try_end_3a} :catch_91
    .catch Ljava/io/IOException; {:try_start_31 .. :try_end_3a} :catch_da

    #@3a
    move-result v10

    #@3b
    const/4 v11, -0x1

    #@3c
    if-ne v10, v11, :cond_37

    #@3e
    .line 652
    :try_start_3e
    invoke-direct {p0, v5, v3}, Lcom/android/internal/telephony/cdma/EriManager;->xlateEriData([BLcom/android/internal/telephony/cdma/EriManager$EriFile;)V

    #@41
    .line 654
    new-instance v10, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    iget-object v11, p0, Lcom/android/internal/telephony/cdma/EriManager;->indexofhomesystem:Ljava/lang/String;

    #@48
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v10

    #@4c
    const-string v11, "1"

    #@4e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v10

    #@52
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v10

    #@56
    iput-object v10, p0, Lcom/android/internal/telephony/cdma/EriManager;->indexofhomesystem:Ljava/lang/String;

    #@58
    .line 655
    new-instance v10, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    iget-object v11, p0, Lcom/android/internal/telephony/cdma/EriManager;->indexofhomesystem:Ljava/lang/String;

    #@5f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v10

    #@63
    const-string v11, ",64,65,76,77,78,79,80,81,82,83,99"

    #@65
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v10

    #@69
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v10

    #@6d
    iput-object v10, p0, Lcom/android/internal/telephony/cdma/EriManager;->indexofhomesystem:Ljava/lang/String;
    :try_end_6f
    .catchall {:try_start_3e .. :try_end_6f} :catchall_d8
    .catch Lcom/android/internal/util/BitwiseInputStream$AccessException; {:try_start_3e .. :try_end_6f} :catch_88
    .catch Ljava/io/FileNotFoundException; {:try_start_3e .. :try_end_6f} :catch_91
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_6f} :catch_da

    #@6f
    .line 670
    :goto_6f
    :try_start_6f
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_72
    .catch Ljava/lang/Exception; {:try_start_6f .. :try_end_72} :catch_cf

    #@72
    .line 677
    .end local v5           #eriRawData:[B
    :goto_72
    return-void

    #@73
    .line 632
    :catch_73
    move-exception v6

    #@74
    .line 633
    .local v6, fnfe:Ljava/io/FileNotFoundException;
    :goto_74
    :try_start_74
    const-string v10, "CDMA"

    #@76
    const-string v11, "//loadEriFileFromFileSystem : file not found//"

    #@78
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 634
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/EriManager;->recoverEriFromFileSystem()V
    :try_end_7e
    .catchall {:try_start_74 .. :try_end_7e} :catchall_83

    #@7e
    .line 637
    :try_start_7e
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_81
    .catch Ljava/lang/Exception; {:try_start_7e .. :try_end_81} :catch_c4

    #@81
    :goto_81
    move-object v9, v8

    #@82
    .line 641
    .end local v8           #stream:Ljava/io/FileInputStream;
    .restart local v9       #stream:Ljava/io/FileInputStream;
    goto :goto_2a

    #@83
    .line 636
    .end local v6           #fnfe:Ljava/io/FileNotFoundException;
    .end local v9           #stream:Ljava/io/FileInputStream;
    .restart local v8       #stream:Ljava/io/FileInputStream;
    :catchall_83
    move-exception v10

    #@84
    .line 637
    :goto_84
    :try_start_84
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_87
    .catch Ljava/lang/Exception; {:try_start_84 .. :try_end_87} :catch_c2

    #@87
    .line 636
    :goto_87
    throw v10

    #@88
    .line 656
    .restart local v5       #eriRawData:[B
    :catch_88
    move-exception v0

    #@89
    .line 658
    .local v0, ae:Lcom/android/internal/util/BitwiseInputStream$AccessException;
    :try_start_89
    const-string v10, "CDMA"

    #@8b
    const-string v11, "loadEriFileFromFileSystem : acess exception"

    #@8d
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_90
    .catchall {:try_start_89 .. :try_end_90} :catchall_d8
    .catch Ljava/io/FileNotFoundException; {:try_start_89 .. :try_end_90} :catch_91
    .catch Ljava/io/IOException; {:try_start_89 .. :try_end_90} :catch_da

    #@90
    goto :goto_6f

    #@91
    .line 661
    .end local v0           #ae:Lcom/android/internal/util/BitwiseInputStream$AccessException;
    .end local v5           #eriRawData:[B
    :catch_91
    move-exception v6

    #@92
    .line 663
    .restart local v6       #fnfe:Ljava/io/FileNotFoundException;
    :goto_92
    :try_start_92
    const-string v10, "CDMA"

    #@94
    const-string v11, "loadEriFileFromFileSystem : file not found"

    #@96
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@99
    .line 664
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/EriManager;->loadEriFileFromXml()V
    :try_end_9c
    .catchall {:try_start_92 .. :try_end_9c} :catchall_d8

    #@9c
    .line 670
    :try_start_9c
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_9f
    .catch Ljava/lang/Exception; {:try_start_9c .. :try_end_9f} :catch_a0

    #@9f
    goto :goto_72

    #@a0
    .line 671
    :catch_a0
    move-exception v2

    #@a1
    .line 673
    .local v2, e:Ljava/lang/Exception;
    const-string v10, "CDMA"

    #@a3
    const-string v11, "loadEriFileFromFileSystem : exception"

    #@a5
    .end local v6           #fnfe:Ljava/io/FileNotFoundException;
    :goto_a5
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    goto :goto_72

    #@a9
    .line 665
    .end local v2           #e:Ljava/lang/Exception;
    .end local v8           #stream:Ljava/io/FileInputStream;
    .restart local v9       #stream:Ljava/io/FileInputStream;
    :catch_a9
    move-exception v7

    #@aa
    move-object v8, v9

    #@ab
    .line 667
    .end local v9           #stream:Ljava/io/FileInputStream;
    .local v7, ioe:Ljava/io/IOException;
    .restart local v8       #stream:Ljava/io/FileInputStream;
    :goto_ab
    :try_start_ab
    const-string v10, "CDMA"

    #@ad
    const-string v11, "loadEriFileFromFileSystem : IO exception"

    #@af
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b2
    .catchall {:try_start_ab .. :try_end_b2} :catchall_d8

    #@b2
    .line 670
    :try_start_b2
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_b5
    .catch Ljava/lang/Exception; {:try_start_b2 .. :try_end_b5} :catch_b6

    #@b5
    goto :goto_72

    #@b6
    .line 671
    :catch_b6
    move-exception v2

    #@b7
    .line 673
    .restart local v2       #e:Ljava/lang/Exception;
    const-string v10, "CDMA"

    #@b9
    const-string v11, "loadEriFileFromFileSystem : exception"

    #@bb
    goto :goto_a5

    #@bc
    .line 669
    .end local v2           #e:Ljava/lang/Exception;
    .end local v7           #ioe:Ljava/io/IOException;
    .end local v8           #stream:Ljava/io/FileInputStream;
    .restart local v9       #stream:Ljava/io/FileInputStream;
    :catchall_bc
    move-exception v10

    #@bd
    move-object v8, v9

    #@be
    .line 670
    .end local v9           #stream:Ljava/io/FileInputStream;
    .restart local v8       #stream:Ljava/io/FileInputStream;
    :goto_be
    :try_start_be
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_c1
    .catch Ljava/lang/Exception; {:try_start_be .. :try_end_c1} :catch_c6

    #@c1
    .line 669
    :goto_c1
    throw v10

    #@c2
    .line 638
    :catch_c2
    move-exception v11

    #@c3
    goto :goto_87

    #@c4
    .restart local v6       #fnfe:Ljava/io/FileNotFoundException;
    :catch_c4
    move-exception v10

    #@c5
    goto :goto_81

    #@c6
    .line 671
    .end local v6           #fnfe:Ljava/io/FileNotFoundException;
    :catch_c6
    move-exception v2

    #@c7
    .line 673
    .restart local v2       #e:Ljava/lang/Exception;
    const-string v11, "CDMA"

    #@c9
    const-string v12, "loadEriFileFromFileSystem : exception"

    #@cb
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    goto :goto_c1

    #@cf
    .line 671
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v5       #eriRawData:[B
    :catch_cf
    move-exception v2

    #@d0
    .line 673
    .restart local v2       #e:Ljava/lang/Exception;
    const-string v10, "CDMA"

    #@d2
    const-string v11, "loadEriFileFromFileSystem : exception"

    #@d4
    goto :goto_a5

    #@d5
    .line 638
    .end local v2           #e:Ljava/lang/Exception;
    .end local v5           #eriRawData:[B
    .end local v8           #stream:Ljava/io/FileInputStream;
    .restart local v4       #eriFileObj:Ljava/io/File;
    .restart local v9       #stream:Ljava/io/FileInputStream;
    :catch_d5
    move-exception v10

    #@d6
    goto/16 :goto_2a

    #@d8
    .line 669
    .end local v4           #eriFileObj:Ljava/io/File;
    .end local v9           #stream:Ljava/io/FileInputStream;
    .restart local v8       #stream:Ljava/io/FileInputStream;
    :catchall_d8
    move-exception v10

    #@d9
    goto :goto_be

    #@da
    .line 665
    :catch_da
    move-exception v7

    #@db
    goto :goto_ab

    #@dc
    .line 661
    .end local v8           #stream:Ljava/io/FileInputStream;
    .restart local v9       #stream:Ljava/io/FileInputStream;
    :catch_dc
    move-exception v6

    #@dd
    move-object v8, v9

    #@de
    .end local v9           #stream:Ljava/io/FileInputStream;
    .restart local v8       #stream:Ljava/io/FileInputStream;
    goto :goto_92

    #@df
    .line 636
    .end local v8           #stream:Ljava/io/FileInputStream;
    .restart local v4       #eriFileObj:Ljava/io/File;
    .restart local v9       #stream:Ljava/io/FileInputStream;
    :catchall_df
    move-exception v10

    #@e0
    move-object v8, v9

    #@e1
    .end local v9           #stream:Ljava/io/FileInputStream;
    .restart local v8       #stream:Ljava/io/FileInputStream;
    goto :goto_84

    #@e2
    .line 632
    .end local v8           #stream:Ljava/io/FileInputStream;
    .restart local v9       #stream:Ljava/io/FileInputStream;
    :catch_e2
    move-exception v6

    #@e3
    move-object v8, v9

    #@e4
    .end local v9           #stream:Ljava/io/FileInputStream;
    .restart local v8       #stream:Ljava/io/FileInputStream;
    goto :goto_74
.end method

.method private loadEriFileFromModem()V
    .registers 1

    #@0
    .prologue
    .line 311
    return-void
.end method

.method private loadEriFileFromXml()V
    .registers 21

    #@0
    .prologue
    .line 684
    const/4 v13, 0x0

    #@1
    .line 685
    .local v13, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v15, 0x0

    #@2
    .line 686
    .local v15, stream:Ljava/io/FileInputStream;
    move-object/from16 v0, p0

    #@4
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v14

    #@a
    .line 689
    .local v14, r:Landroid/content/res/Resources;
    :try_start_a
    const-string v2, "CDMA"

    #@c
    const-string v18, "loadEriFileFromXml: check for alternate file"

    #@e
    move-object/from16 v0, v18

    #@10
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 690
    new-instance v16, Ljava/io/FileInputStream;

    #@15
    const v2, 0x10404ae

    #@18
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    move-object/from16 v0, v16

    #@1e
    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_21
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_21} :catch_164
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_a .. :try_end_21} :catch_171

    #@21
    .line 692
    .end local v15           #stream:Ljava/io/FileInputStream;
    .local v16, stream:Ljava/io/FileInputStream;
    :try_start_21
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@24
    move-result-object v13

    #@25
    .line 693
    const/4 v2, 0x0

    #@26
    move-object/from16 v0, v16

    #@28
    invoke-interface {v13, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@2b
    .line 694
    const-string v2, "CDMA"

    #@2d
    const-string v18, "loadEriFileFromXml: opened alternate file"

    #@2f
    move-object/from16 v0, v18

    #@31
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_34
    .catch Ljava/io/FileNotFoundException; {:try_start_21 .. :try_end_34} :catch_271
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_21 .. :try_end_34} :catch_26c

    #@34
    move-object/from16 v15, v16

    #@36
    .line 703
    .end local v16           #stream:Ljava/io/FileInputStream;
    .restart local v15       #stream:Ljava/io/FileInputStream;
    :goto_36
    if-nez v13, :cond_48

    #@38
    .line 704
    const-string v2, "CDMA"

    #@3a
    const-string v18, "loadEriFileFromXml: open normal file"

    #@3c
    move-object/from16 v0, v18

    #@3e
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 705
    const v2, 0x10f0002

    #@44
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@47
    move-result-object v13

    #@48
    .line 709
    :cond_48
    :try_start_48
    const-string v2, "EriFile"

    #@4a
    invoke-static {v13, v2}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@4d
    .line 711
    const/4 v2, 0x0

    #@4e
    const-string v18, "vzw_eri"

    #@50
    move-object/from16 v0, v18

    #@52
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@55
    move-result v2

    #@56
    if-eqz v2, :cond_68

    #@58
    .line 712
    move-object/from16 v0, p0

    #@5a
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@5c
    const/16 v18, -0x1

    #@5e
    move/from16 v0, v18

    #@60
    iput v0, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mVersionNumber:I

    #@62
    .line 713
    const/4 v2, 0x0

    #@63
    move-object/from16 v0, p0

    #@65
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/EriManager;->sendEriWrite(I)V

    #@68
    .line 716
    :cond_68
    move-object/from16 v0, p0

    #@6a
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@6c
    const/16 v18, 0x0

    #@6e
    const-string v19, "VersionNumber"

    #@70
    move-object/from16 v0, v18

    #@72
    move-object/from16 v1, v19

    #@74
    invoke-interface {v13, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@77
    move-result-object v18

    #@78
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@7b
    move-result v18

    #@7c
    move/from16 v0, v18

    #@7e
    iput v0, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mVersionNumber:I

    #@80
    .line 718
    move-object/from16 v0, p0

    #@82
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@84
    const/16 v18, 0x0

    #@86
    const-string v19, "NumberOfEriEntries"

    #@88
    move-object/from16 v0, v18

    #@8a
    move-object/from16 v1, v19

    #@8c
    invoke-interface {v13, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8f
    move-result-object v18

    #@90
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@93
    move-result v18

    #@94
    move/from16 v0, v18

    #@96
    iput v0, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mNumberOfEriEntries:I

    #@98
    .line 720
    move-object/from16 v0, p0

    #@9a
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@9c
    const/16 v18, 0x0

    #@9e
    const-string v19, "EriFileType"

    #@a0
    move-object/from16 v0, v18

    #@a2
    move-object/from16 v1, v19

    #@a4
    invoke-interface {v13, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a7
    move-result-object v18

    #@a8
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@ab
    move-result v18

    #@ac
    move/from16 v0, v18

    #@ae
    iput v0, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mEriFileType:I

    #@b0
    .line 723
    const/4 v12, 0x0

    #@b1
    .line 725
    .local v12, parsedEriEntries:I
    :cond_b1
    :goto_b1
    invoke-static {v13}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@b4
    .line 726
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@b7
    move-result-object v11

    #@b8
    .line 727
    .local v11, name:Ljava/lang/String;
    if-nez v11, :cond_17e

    #@ba
    .line 728
    move-object/from16 v0, p0

    #@bc
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@be
    iget v2, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mNumberOfEriEntries:I

    #@c0
    if-eq v12, v2, :cond_fa

    #@c2
    .line 729
    const-string v2, "CDMA"

    #@c4
    new-instance v18, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v19, "Error Parsing ERI file: "

    #@cb
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v18

    #@cf
    move-object/from16 v0, p0

    #@d1
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@d3
    move-object/from16 v19, v0

    #@d5
    move-object/from16 v0, v19

    #@d7
    iget v0, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mNumberOfEriEntries:I

    #@d9
    move/from16 v19, v0

    #@db
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@de
    move-result-object v18

    #@df
    const-string v19, " defined, "

    #@e1
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v18

    #@e5
    move-object/from16 v0, v18

    #@e7
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v18

    #@eb
    const-string v19, " parsed!"

    #@ed
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v18

    #@f1
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v18

    #@f5
    move-object/from16 v0, v18

    #@f7
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@fa
    .line 756
    :cond_fa
    const-string v2, "CDMA"

    #@fc
    const-string v18, "loadEriFileFromXml: eri parsing successful, file loaded"

    #@fe
    move-object/from16 v0, v18

    #@100
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@103
    .line 758
    const/4 v2, 0x0

    #@104
    const-string v18, "vzw_eri"

    #@106
    move-object/from16 v0, v18

    #@108
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@10b
    move-result v2

    #@10c
    if-eqz v2, :cond_150

    #@10e
    .line 759
    new-instance v2, Ljava/lang/StringBuilder;

    #@110
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@113
    move-object/from16 v0, p0

    #@115
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/EriManager;->indexofhomesystem:Ljava/lang/String;

    #@117
    move-object/from16 v18, v0

    #@119
    move-object/from16 v0, v18

    #@11b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v2

    #@11f
    const-string v18, "1"

    #@121
    move-object/from16 v0, v18

    #@123
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v2

    #@127
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12a
    move-result-object v2

    #@12b
    move-object/from16 v0, p0

    #@12d
    iput-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->indexofhomesystem:Ljava/lang/String;

    #@12f
    .line 760
    new-instance v2, Ljava/lang/StringBuilder;

    #@131
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@134
    move-object/from16 v0, p0

    #@136
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/EriManager;->indexofhomesystem:Ljava/lang/String;

    #@138
    move-object/from16 v18, v0

    #@13a
    move-object/from16 v0, v18

    #@13c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v2

    #@140
    const-string v18, ",64,65,76,77,78,79,80,81,82,83"

    #@142
    move-object/from16 v0, v18

    #@144
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v2

    #@148
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14b
    move-result-object v2

    #@14c
    move-object/from16 v0, p0

    #@14e
    iput-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->indexofhomesystem:Ljava/lang/String;

    #@150
    .line 763
    :cond_150
    const/4 v2, 0x1

    #@151
    move-object/from16 v0, p0

    #@153
    iput-boolean v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->isEriFileLoaded:Z
    :try_end_155
    .catchall {:try_start_48 .. :try_end_155} :catchall_1ea
    .catch Ljava/lang/Exception; {:try_start_48 .. :try_end_155} :catch_1ab

    #@155
    .line 768
    instance-of v2, v13, Landroid/content/res/XmlResourceParser;

    #@157
    if-eqz v2, :cond_15e

    #@159
    .line 769
    check-cast v13, Landroid/content/res/XmlResourceParser;

    #@15b
    .end local v13           #parser:Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v13}, Landroid/content/res/XmlResourceParser;->close()V

    #@15e
    .line 772
    :cond_15e
    if-eqz v15, :cond_163

    #@160
    .line 773
    :try_start_160
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_163
    .catch Ljava/io/IOException; {:try_start_160 .. :try_end_163} :catch_1c4

    #@163
    .line 779
    .end local v11           #name:Ljava/lang/String;
    .end local v12           #parsedEriEntries:I
    :cond_163
    :goto_163
    return-void

    #@164
    .line 695
    .restart local v13       #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_164
    move-exception v9

    #@165
    .line 696
    .local v9, e:Ljava/io/FileNotFoundException;
    :goto_165
    const-string v2, "CDMA"

    #@167
    const-string v18, "loadEriFileFromXml: no alternate file"

    #@169
    move-object/from16 v0, v18

    #@16b
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16e
    .line 697
    const/4 v13, 0x0

    #@16f
    .line 701
    goto/16 :goto_36

    #@171
    .line 698
    .end local v9           #e:Ljava/io/FileNotFoundException;
    :catch_171
    move-exception v9

    #@172
    .line 699
    .local v9, e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_172
    const-string v2, "CDMA"

    #@174
    const-string v18, "loadEriFileFromXml: no parser for alternate file"

    #@176
    move-object/from16 v0, v18

    #@178
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17b
    .line 700
    const/4 v13, 0x0

    #@17c
    goto/16 :goto_36

    #@17e
    .line 732
    .end local v9           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v11       #name:Ljava/lang/String;
    .restart local v12       #parsedEriEntries:I
    :cond_17e
    :try_start_17e
    const-string v2, "CallPromptId"

    #@180
    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@183
    move-result v2

    #@184
    if-eqz v2, :cond_1fc

    #@186
    .line 733
    const/4 v2, 0x0

    #@187
    const-string v18, "Id"

    #@189
    move-object/from16 v0, v18

    #@18b
    invoke-interface {v13, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@18e
    move-result-object v2

    #@18f
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@192
    move-result v10

    #@193
    .line 734
    .local v10, id:I
    const/4 v2, 0x0

    #@194
    const-string v18, "CallPromptText"

    #@196
    move-object/from16 v0, v18

    #@198
    invoke-interface {v13, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@19b
    move-result-object v17

    #@19c
    .line 735
    .local v17, text:Ljava/lang/String;
    if-ltz v10, :cond_1c6

    #@19e
    const/4 v2, 0x2

    #@19f
    if-gt v10, v2, :cond_1c6

    #@1a1
    .line 736
    move-object/from16 v0, p0

    #@1a3
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@1a5
    iget-object v2, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mCallPromptId:[Ljava/lang/String;

    #@1a7
    aput-object v17, v2, v10
    :try_end_1a9
    .catchall {:try_start_17e .. :try_end_1a9} :catchall_1ea
    .catch Ljava/lang/Exception; {:try_start_17e .. :try_end_1a9} :catch_1ab

    #@1a9
    goto/16 :goto_b1

    #@1ab
    .line 765
    .end local v10           #id:I
    .end local v11           #name:Ljava/lang/String;
    .end local v12           #parsedEriEntries:I
    .end local v17           #text:Ljava/lang/String;
    :catch_1ab
    move-exception v9

    #@1ac
    .line 766
    .local v9, e:Ljava/lang/Exception;
    :try_start_1ac
    const-string v2, "CDMA"

    #@1ae
    const-string v18, "Got exception while loading ERI file."

    #@1b0
    move-object/from16 v0, v18

    #@1b2
    invoke-static {v2, v0, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1b5
    .catchall {:try_start_1ac .. :try_end_1b5} :catchall_1ea

    #@1b5
    .line 768
    instance-of v2, v13, Landroid/content/res/XmlResourceParser;

    #@1b7
    if-eqz v2, :cond_1be

    #@1b9
    .line 769
    check-cast v13, Landroid/content/res/XmlResourceParser;

    #@1bb
    .end local v13           #parser:Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v13}, Landroid/content/res/XmlResourceParser;->close()V

    #@1be
    .line 772
    :cond_1be
    if-eqz v15, :cond_163

    #@1c0
    .line 773
    :try_start_1c0
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_1c3
    .catch Ljava/io/IOException; {:try_start_1c0 .. :try_end_1c3} :catch_1c4

    #@1c3
    goto :goto_163

    #@1c4
    .line 775
    .end local v9           #e:Ljava/lang/Exception;
    :catch_1c4
    move-exception v2

    #@1c5
    goto :goto_163

    #@1c6
    .line 738
    .restart local v10       #id:I
    .restart local v11       #name:Ljava/lang/String;
    .restart local v12       #parsedEriEntries:I
    .restart local v13       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v17       #text:Ljava/lang/String;
    :cond_1c6
    :try_start_1c6
    const-string v2, "CDMA"

    #@1c8
    new-instance v18, Ljava/lang/StringBuilder;

    #@1ca
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@1cd
    const-string v19, "Error Parsing ERI file: found"

    #@1cf
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v18

    #@1d3
    move-object/from16 v0, v18

    #@1d5
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v18

    #@1d9
    const-string v19, " CallPromptId"

    #@1db
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1de
    move-result-object v18

    #@1df
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e2
    move-result-object v18

    #@1e3
    move-object/from16 v0, v18

    #@1e5
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1e8
    .catchall {:try_start_1c6 .. :try_end_1e8} :catchall_1ea
    .catch Ljava/lang/Exception; {:try_start_1c6 .. :try_end_1e8} :catch_1ab

    #@1e8
    goto/16 :goto_b1

    #@1ea
    .line 768
    .end local v10           #id:I
    .end local v11           #name:Ljava/lang/String;
    .end local v12           #parsedEriEntries:I
    .end local v17           #text:Ljava/lang/String;
    :catchall_1ea
    move-exception v2

    #@1eb
    instance-of v0, v13, Landroid/content/res/XmlResourceParser;

    #@1ed
    move/from16 v18, v0

    #@1ef
    if-eqz v18, :cond_1f6

    #@1f1
    .line 769
    check-cast v13, Landroid/content/res/XmlResourceParser;

    #@1f3
    .end local v13           #parser:Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v13}, Landroid/content/res/XmlResourceParser;->close()V

    #@1f6
    .line 772
    :cond_1f6
    if-eqz v15, :cond_1fb

    #@1f8
    .line 773
    :try_start_1f8
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_1fb
    .catch Ljava/io/IOException; {:try_start_1f8 .. :try_end_1fb} :catch_26a

    #@1fb
    .line 768
    :cond_1fb
    :goto_1fb
    throw v2

    #@1fc
    .line 741
    .restart local v11       #name:Ljava/lang/String;
    .restart local v12       #parsedEriEntries:I
    .restart local v13       #parser:Lorg/xmlpull/v1/XmlPullParser;
    :cond_1fc
    :try_start_1fc
    const-string v2, "EriInfo"

    #@1fe
    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@201
    move-result v2

    #@202
    if-eqz v2, :cond_b1

    #@204
    .line 742
    const/4 v2, 0x0

    #@205
    const-string v18, "RoamingIndicator"

    #@207
    move-object/from16 v0, v18

    #@209
    invoke-interface {v13, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@20c
    move-result-object v2

    #@20d
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@210
    move-result v3

    #@211
    .line 744
    .local v3, roamingIndicator:I
    const/4 v2, 0x0

    #@212
    const-string v18, "IconIndex"

    #@214
    move-object/from16 v0, v18

    #@216
    invoke-interface {v13, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@219
    move-result-object v2

    #@21a
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@21d
    move-result v4

    #@21e
    .line 745
    .local v4, iconIndex:I
    const/4 v2, 0x0

    #@21f
    const-string v18, "IconMode"

    #@221
    move-object/from16 v0, v18

    #@223
    invoke-interface {v13, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@226
    move-result-object v2

    #@227
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@22a
    move-result v5

    #@22b
    .line 746
    .local v5, iconMode:I
    const/4 v2, 0x0

    #@22c
    const-string v18, "EriText"

    #@22e
    move-object/from16 v0, v18

    #@230
    invoke-interface {v13, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@233
    move-result-object v6

    #@234
    .line 747
    .local v6, eriText:Ljava/lang/String;
    const/4 v2, 0x0

    #@235
    const-string v18, "CallPromptId"

    #@237
    move-object/from16 v0, v18

    #@239
    invoke-interface {v13, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@23c
    move-result-object v2

    #@23d
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@240
    move-result v7

    #@241
    .line 749
    .local v7, callPromptId:I
    const/4 v2, 0x0

    #@242
    const-string v18, "AlertId"

    #@244
    move-object/from16 v0, v18

    #@246
    invoke-interface {v13, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@249
    move-result-object v2

    #@24a
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@24d
    move-result v8

    #@24e
    .line 750
    .local v8, alertId:I
    add-int/lit8 v12, v12, 0x1

    #@250
    .line 751
    move-object/from16 v0, p0

    #@252
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@254
    iget-object v0, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mRoamIndTable:Ljava/util/HashMap;

    #@256
    move-object/from16 v18, v0

    #@258
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25b
    move-result-object v19

    #@25c
    new-instance v2, Lcom/android/internal/telephony/cdma/EriInfo;

    #@25e
    invoke-direct/range {v2 .. v8}, Lcom/android/internal/telephony/cdma/EriInfo;-><init>(IIILjava/lang/String;II)V

    #@261
    move-object/from16 v0, v18

    #@263
    move-object/from16 v1, v19

    #@265
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_268
    .catchall {:try_start_1fc .. :try_end_268} :catchall_1ea
    .catch Ljava/lang/Exception; {:try_start_1fc .. :try_end_268} :catch_1ab

    #@268
    goto/16 :goto_b1

    #@26a
    .line 775
    .end local v3           #roamingIndicator:I
    .end local v4           #iconIndex:I
    .end local v5           #iconMode:I
    .end local v6           #eriText:Ljava/lang/String;
    .end local v7           #callPromptId:I
    .end local v8           #alertId:I
    .end local v11           #name:Ljava/lang/String;
    .end local v12           #parsedEriEntries:I
    .end local v13           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_26a
    move-exception v18

    #@26b
    goto :goto_1fb

    #@26c
    .line 698
    .end local v15           #stream:Ljava/io/FileInputStream;
    .restart local v13       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v16       #stream:Ljava/io/FileInputStream;
    :catch_26c
    move-exception v9

    #@26d
    move-object/from16 v15, v16

    #@26f
    .end local v16           #stream:Ljava/io/FileInputStream;
    .restart local v15       #stream:Ljava/io/FileInputStream;
    goto/16 :goto_172

    #@271
    .line 695
    .end local v15           #stream:Ljava/io/FileInputStream;
    .restart local v16       #stream:Ljava/io/FileInputStream;
    :catch_271
    move-exception v9

    #@272
    move-object/from16 v15, v16

    #@274
    .end local v16           #stream:Ljava/io/FileInputStream;
    .restart local v15       #stream:Ljava/io/FileInputStream;
    goto/16 :goto_165
.end method

.method private recoverEriFromFileSystem()V
    .registers 11

    #@0
    .prologue
    .line 584
    const/4 v3, 0x0

    #@1
    .line 585
    .local v3, fis:Ljava/io/FileInputStream;
    const/4 v5, 0x0

    #@2
    .line 587
    .local v5, fos:Ljava/io/FileOutputStream;
    :try_start_2
    const-string v7, "CDMA"

    #@4
    const-string v8, "Eri_ recoverEriFromFileSystem() start!! "

    #@6
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 588
    new-instance v4, Ljava/io/FileInputStream;

    #@b
    const-string v7, "/system/etc/eri.bin"

    #@d
    invoke-direct {v4, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_10
    .catchall {:try_start_2 .. :try_end_10} :catchall_59
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_10} :catch_87

    #@10
    .line 589
    .end local v3           #fis:Ljava/io/FileInputStream;
    .local v4, fis:Ljava/io/FileInputStream;
    :try_start_10
    const-string v7, "CDMA"

    #@12
    const-string v8, "Eri_ recover input ERI_BACKUP_FILE_NAME = /system/etc/eri.bin"

    #@14
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 590
    new-instance v6, Ljava/io/FileOutputStream;

    #@19
    const-string v7, "/eri/eri.bin"

    #@1b
    invoke-direct {v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1e
    .catchall {:try_start_10 .. :try_end_1e} :catchall_80
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_1e} :catch_89

    #@1e
    .line 591
    .end local v5           #fos:Ljava/io/FileOutputStream;
    .local v6, fos:Ljava/io/FileOutputStream;
    :try_start_1e
    const-string v7, "CDMA"

    #@20
    const-string v8, "Eri_ revover output ERI_FILE_NAME = /eri/eri.bin"

    #@22
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 593
    const/4 v0, 0x0

    #@26
    .line 594
    .local v0, data:I
    :goto_26
    invoke-virtual {v4}, Ljava/io/FileInputStream;->read()I

    #@29
    move-result v0

    #@2a
    const/4 v7, -0x1

    #@2b
    if-eq v0, v7, :cond_46

    #@2d
    invoke-virtual {v6, v0}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_30
    .catchall {:try_start_1e .. :try_end_30} :catchall_83
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_30} :catch_31

    #@30
    goto :goto_26

    #@31
    .line 597
    .end local v0           #data:I
    :catch_31
    move-exception v1

    #@32
    move-object v5, v6

    #@33
    .end local v6           #fos:Ljava/io/FileOutputStream;
    .restart local v5       #fos:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@34
    .line 598
    .end local v4           #fis:Ljava/io/FileInputStream;
    .local v1, e:Ljava/io/IOException;
    .restart local v3       #fis:Ljava/io/FileInputStream;
    :goto_34
    :try_start_34
    const-string v7, "CDMA"

    #@36
    const-string v8, "Fail to recovery eri.bin"

    #@38
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3b
    .catchall {:try_start_34 .. :try_end_3b} :catchall_59

    #@3b
    .line 601
    if-eqz v3, :cond_40

    #@3d
    :try_start_3d
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    #@40
    .line 602
    :cond_40
    if-eqz v5, :cond_45

    #@42
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_45
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_45} :catch_6e

    #@45
    .line 608
    .end local v1           #e:Ljava/io/IOException;
    :cond_45
    :goto_45
    return-void

    #@46
    .line 595
    .end local v3           #fis:Ljava/io/FileInputStream;
    .end local v5           #fos:Ljava/io/FileOutputStream;
    .restart local v0       #data:I
    .restart local v4       #fis:Ljava/io/FileInputStream;
    .restart local v6       #fos:Ljava/io/FileOutputStream;
    :cond_46
    :try_start_46
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@49
    .line 596
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_4c
    .catchall {:try_start_46 .. :try_end_4c} :catchall_83
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_4c} :catch_31

    #@4c
    .line 601
    if-eqz v4, :cond_51

    #@4e
    :try_start_4e
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@51
    .line 602
    :cond_51
    if-eqz v6, :cond_56

    #@53
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_56
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_56} :catch_77

    #@56
    :cond_56
    :goto_56
    move-object v5, v6

    #@57
    .end local v6           #fos:Ljava/io/FileOutputStream;
    .restart local v5       #fos:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@58
    .line 607
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v3       #fis:Ljava/io/FileInputStream;
    goto :goto_45

    #@59
    .line 600
    .end local v0           #data:I
    :catchall_59
    move-exception v7

    #@5a
    .line 601
    :goto_5a
    if-eqz v3, :cond_5f

    #@5c
    :try_start_5c
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    #@5f
    .line 602
    :cond_5f
    if-eqz v5, :cond_64

    #@61
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_64
    .catch Ljava/io/IOException; {:try_start_5c .. :try_end_64} :catch_65

    #@64
    .line 600
    :cond_64
    :goto_64
    throw v7

    #@65
    .line 604
    :catch_65
    move-exception v2

    #@66
    .line 605
    .local v2, ex:Ljava/io/IOException;
    const-string v8, "CDMA"

    #@68
    const-string v9, "Fail to close file."

    #@6a
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    goto :goto_64

    #@6e
    .line 604
    .end local v2           #ex:Ljava/io/IOException;
    .restart local v1       #e:Ljava/io/IOException;
    :catch_6e
    move-exception v2

    #@6f
    .line 605
    .restart local v2       #ex:Ljava/io/IOException;
    const-string v7, "CDMA"

    #@71
    const-string v8, "Fail to close file."

    #@73
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    goto :goto_45

    #@77
    .line 604
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #ex:Ljava/io/IOException;
    .end local v3           #fis:Ljava/io/FileInputStream;
    .end local v5           #fos:Ljava/io/FileOutputStream;
    .restart local v0       #data:I
    .restart local v4       #fis:Ljava/io/FileInputStream;
    .restart local v6       #fos:Ljava/io/FileOutputStream;
    :catch_77
    move-exception v2

    #@78
    .line 605
    .restart local v2       #ex:Ljava/io/IOException;
    const-string v7, "CDMA"

    #@7a
    const-string v8, "Fail to close file."

    #@7c
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    goto :goto_56

    #@80
    .line 600
    .end local v0           #data:I
    .end local v2           #ex:Ljava/io/IOException;
    .end local v6           #fos:Ljava/io/FileOutputStream;
    .restart local v5       #fos:Ljava/io/FileOutputStream;
    :catchall_80
    move-exception v7

    #@81
    move-object v3, v4

    #@82
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v3       #fis:Ljava/io/FileInputStream;
    goto :goto_5a

    #@83
    .end local v3           #fis:Ljava/io/FileInputStream;
    .end local v5           #fos:Ljava/io/FileOutputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    .restart local v6       #fos:Ljava/io/FileOutputStream;
    :catchall_83
    move-exception v7

    #@84
    move-object v5, v6

    #@85
    .end local v6           #fos:Ljava/io/FileOutputStream;
    .restart local v5       #fos:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@86
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v3       #fis:Ljava/io/FileInputStream;
    goto :goto_5a

    #@87
    .line 597
    :catch_87
    move-exception v1

    #@88
    goto :goto_34

    #@89
    .end local v3           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    :catch_89
    move-exception v1

    #@8a
    move-object v3, v4

    #@8b
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v3       #fis:Ljava/io/FileInputStream;
    goto :goto_34
.end method

.method private xlateEriData([BLcom/android/internal/telephony/cdma/EriManager$EriFile;)V
    .registers 34
    .parameter "eri_data_ptr"
    .parameter "eri_ptr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    .line 316
    new-instance v24, Lcom/android/internal/util/BitwiseInputStream;

    #@2
    move-object/from16 v0, v24

    #@4
    move-object/from16 v1, p1

    #@6
    invoke-direct {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@9
    .line 318
    .local v24, bis:Lcom/android/internal/util/BitwiseInputStream;
    const/16 v27, 0x0

    #@b
    .line 322
    .local v27, data_pos:I
    const/16 v30, 0x0

    #@d
    .line 323
    .local v30, num_reserved_padbits:I
    const/16 v26, 0x0

    #@f
    .line 326
    .local v26, crc_calc:C
    const/16 v2, 0x8

    #@11
    move-object/from16 v0, v24

    #@13
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@16
    move-result v2

    #@17
    shl-int/lit8 v2, v2, 0x8

    #@19
    const/16 v3, 0x8

    #@1b
    move-object/from16 v0, v24

    #@1d
    invoke-virtual {v0, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@20
    move-result v3

    #@21
    or-int/2addr v2, v3

    #@22
    move-object/from16 v0, p2

    #@24
    iput v2, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mVersionNumber:I

    #@26
    .line 328
    add-int/lit8 v27, v27, 0x10

    #@28
    .line 330
    move-object/from16 v0, p0

    #@2a
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@2c
    move-object/from16 v0, p2

    #@2e
    iget v3, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mVersionNumber:I

    #@30
    iput v3, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mVersionNumber:I

    #@32
    .line 333
    const-string v2, "CDMA"

    #@34
    const-string v3, "sendEriWrite"

    #@36
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 335
    const/4 v2, 0x6

    #@3a
    move-object/from16 v0, v24

    #@3c
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@3f
    move-result v2

    #@40
    move-object/from16 v0, p2

    #@42
    iput v2, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mNumberOfEriEntries:I

    #@44
    .line 337
    add-int/lit8 v27, v27, 0x6

    #@46
    .line 339
    move-object/from16 v0, p0

    #@48
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@4a
    move-object/from16 v0, p2

    #@4c
    iget v3, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mNumberOfEriEntries:I

    #@4e
    iput v3, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mNumberOfEriEntries:I

    #@50
    .line 341
    const/4 v2, 0x3

    #@51
    move-object/from16 v0, v24

    #@53
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@56
    move-result v2

    #@57
    move-object/from16 v0, p2

    #@59
    iput v2, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mEriFileType:I

    #@5b
    .line 343
    add-int/lit8 v27, v27, 0x3

    #@5d
    .line 345
    move-object/from16 v0, p0

    #@5f
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@61
    move-object/from16 v0, p2

    #@63
    iget v3, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mEriFileType:I

    #@65
    iput v3, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mEriFileType:I

    #@67
    .line 347
    const/4 v2, 0x4

    #@68
    move-object/from16 v0, v24

    #@6a
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@6d
    move-result v2

    #@6e
    move-object/from16 v0, p2

    #@70
    iput v2, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mNumberOfIconImages:I

    #@72
    .line 349
    add-int/lit8 v27, v27, 0x4

    #@74
    .line 351
    const/4 v2, 0x3

    #@75
    move-object/from16 v0, v24

    #@77
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@7a
    move-result v2

    #@7b
    move-object/from16 v0, p2

    #@7d
    iput v2, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mIconImageType:I

    #@7f
    .line 353
    add-int/lit8 v27, v27, 0x3

    #@81
    .line 355
    const/16 v28, 0x0

    #@83
    .local v28, i:I
    :goto_83
    const/4 v2, 0x3

    #@84
    move/from16 v0, v28

    #@86
    if-ge v0, v2, :cond_10b

    #@88
    .line 362
    const-string v10, ""

    #@8a
    .line 365
    .local v10, textData:Ljava/lang/String;
    const/16 v2, 0x8

    #@8c
    move-object/from16 v0, v24

    #@8e
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@91
    move-result v4

    #@92
    .line 367
    .local v4, reservedBits1:I
    add-int/lit8 v27, v27, 0x8

    #@94
    .line 370
    const/4 v2, 0x4

    #@95
    move-object/from16 v0, v24

    #@97
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@9a
    move-result v5

    #@9b
    .line 372
    .local v5, callPromptId:I
    add-int/lit8 v27, v27, 0x4

    #@9d
    .line 375
    const/4 v2, 0x4

    #@9e
    move-object/from16 v0, v24

    #@a0
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@a3
    move-result v6

    #@a4
    .line 377
    .local v6, reservedBits2:I
    add-int/lit8 v27, v27, 0x4

    #@a6
    .line 380
    const/4 v2, 0x3

    #@a7
    move-object/from16 v0, v24

    #@a9
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@ac
    move-result v7

    #@ad
    .line 382
    .local v7, reservedBits3:I
    add-int/lit8 v27, v27, 0x3

    #@af
    .line 385
    const/4 v2, 0x5

    #@b0
    move-object/from16 v0, v24

    #@b2
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@b5
    move-result v8

    #@b6
    .line 387
    .local v8, characterEncodingType:I
    add-int/lit8 v27, v27, 0x5

    #@b8
    .line 389
    packed-switch v8, :pswitch_data_25e

    #@bb
    .line 406
    :pswitch_bb
    const/16 v25, 0x0

    #@bd
    .line 411
    .local v25, char_bit_len:B
    :goto_bd
    const/16 v2, 0x8

    #@bf
    move-object/from16 v0, v24

    #@c1
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@c4
    move-result v9

    #@c5
    .line 413
    .local v9, amountOfTextData:I
    add-int/lit8 v27, v27, 0x8

    #@c7
    .line 416
    const/16 v29, 0x0

    #@c9
    .local v29, j:I
    :goto_c9
    move/from16 v0, v29

    #@cb
    if-ge v0, v9, :cond_f1

    #@cd
    .line 418
    new-instance v2, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v2

    #@d6
    invoke-virtual/range {v24 .. v25}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@d9
    move-result v3

    #@da
    int-to-char v3, v3

    #@db
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@de
    move-result-object v2

    #@df
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e2
    move-result-object v10

    #@e3
    .line 419
    add-int v27, v27, v25

    #@e5
    .line 416
    add-int/lit8 v29, v29, 0x1

    #@e7
    goto :goto_c9

    #@e8
    .line 392
    .end local v9           #amountOfTextData:I
    .end local v25           #char_bit_len:B
    .end local v29           #j:I
    :pswitch_e8
    const/16 v25, 0x8

    #@ea
    .line 393
    .restart local v25       #char_bit_len:B
    goto :goto_bd

    #@eb
    .line 398
    .end local v25           #char_bit_len:B
    :pswitch_eb
    const/16 v25, 0x8

    #@ed
    .line 399
    .restart local v25       #char_bit_len:B
    goto :goto_bd

    #@ee
    .line 402
    .end local v25           #char_bit_len:B
    :pswitch_ee
    const/16 v25, 0x10

    #@f0
    .line 403
    .restart local v25       #char_bit_len:B
    goto :goto_bd

    #@f1
    .line 422
    .restart local v9       #amountOfTextData:I
    .restart local v29       #j:I
    :cond_f1
    move-object/from16 v0, p0

    #@f3
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@f5
    iget-object v11, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mCallPrmptTable:Ljava/util/HashMap;

    #@f7
    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@fa
    move-result-object v16

    #@fb
    new-instance v2, Lcom/android/internal/telephony/cdma/EriManager$EriPrmpt;

    #@fd
    move-object/from16 v3, p0

    #@ff
    invoke-direct/range {v2 .. v10}, Lcom/android/internal/telephony/cdma/EriManager$EriPrmpt;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IIIIIILjava/lang/String;)V

    #@102
    move-object/from16 v0, v16

    #@104
    invoke-virtual {v11, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@107
    .line 355
    add-int/lit8 v28, v28, 0x1

    #@109
    goto/16 :goto_83

    #@10b
    .line 432
    .end local v4           #reservedBits1:I
    .end local v5           #callPromptId:I
    .end local v6           #reservedBits2:I
    .end local v7           #reservedBits3:I
    .end local v8           #characterEncodingType:I
    .end local v9           #amountOfTextData:I
    .end local v10           #textData:Ljava/lang/String;
    .end local v25           #char_bit_len:B
    .end local v29           #j:I
    :cond_10b
    const/16 v28, 0x0

    #@10d
    :goto_10d
    move-object/from16 v0, p2

    #@10f
    iget v2, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mNumberOfEriEntries:I

    #@111
    move/from16 v0, v28

    #@113
    if-ge v0, v2, :cond_19f

    #@115
    .line 436
    const-string v15, ""

    #@117
    .line 443
    .local v15, eriText:Ljava/lang/String;
    const/16 v2, 0x8

    #@119
    move-object/from16 v0, v24

    #@11b
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@11e
    move-result v12

    #@11f
    .line 445
    .local v12, roamingIndicator:I
    add-int/lit8 v27, v27, 0x8

    #@121
    .line 448
    const/4 v2, 0x4

    #@122
    move-object/from16 v0, v24

    #@124
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@127
    move-result v13

    #@128
    .line 450
    .local v13, iconIndex:I
    add-int/lit8 v27, v27, 0x4

    #@12a
    .line 453
    const/4 v2, 0x2

    #@12b
    move-object/from16 v0, v24

    #@12d
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@130
    move-result v14

    #@131
    .line 455
    .local v14, iconMode:I
    add-int/lit8 v27, v27, 0x2

    #@133
    .line 458
    const/4 v2, 0x2

    #@134
    move-object/from16 v0, v24

    #@136
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@139
    move-result v5

    #@13a
    .line 460
    .restart local v5       #callPromptId:I
    add-int/lit8 v27, v27, 0x2

    #@13c
    .line 463
    const/4 v2, 0x3

    #@13d
    move-object/from16 v0, v24

    #@13f
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@142
    move-result v17

    #@143
    .line 465
    .local v17, alertId:I
    add-int/lit8 v27, v27, 0x3

    #@145
    .line 468
    const/4 v2, 0x5

    #@146
    move-object/from16 v0, v24

    #@148
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@14b
    move-result v8

    #@14c
    .line 470
    .restart local v8       #characterEncodingType:I
    add-int/lit8 v27, v27, 0x5

    #@14e
    .line 472
    packed-switch v8, :pswitch_data_274

    #@151
    .line 489
    :pswitch_151
    const/16 v25, 0x0

    #@153
    .line 493
    .restart local v25       #char_bit_len:B
    :goto_153
    const/16 v2, 0x8

    #@155
    move-object/from16 v0, v24

    #@157
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@15a
    move-result v9

    #@15b
    .line 495
    .restart local v9       #amountOfTextData:I
    add-int/lit8 v27, v27, 0x8

    #@15d
    .line 498
    const/16 v29, 0x0

    #@15f
    .restart local v29       #j:I
    :goto_15f
    move/from16 v0, v29

    #@161
    if-ge v0, v9, :cond_187

    #@163
    .line 500
    new-instance v2, Ljava/lang/StringBuilder;

    #@165
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@168
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16b
    move-result-object v2

    #@16c
    invoke-virtual/range {v24 .. v25}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@16f
    move-result v3

    #@170
    int-to-char v3, v3

    #@171
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@174
    move-result-object v2

    #@175
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@178
    move-result-object v15

    #@179
    .line 501
    add-int v27, v27, v25

    #@17b
    .line 498
    add-int/lit8 v29, v29, 0x1

    #@17d
    goto :goto_15f

    #@17e
    .line 475
    .end local v9           #amountOfTextData:I
    .end local v25           #char_bit_len:B
    .end local v29           #j:I
    :pswitch_17e
    const/16 v25, 0x8

    #@180
    .line 476
    .restart local v25       #char_bit_len:B
    goto :goto_153

    #@181
    .line 481
    .end local v25           #char_bit_len:B
    :pswitch_181
    const/16 v25, 0x8

    #@183
    .line 482
    .restart local v25       #char_bit_len:B
    goto :goto_153

    #@184
    .line 485
    .end local v25           #char_bit_len:B
    :pswitch_184
    const/16 v25, 0x10

    #@186
    .line 486
    .restart local v25       #char_bit_len:B
    goto :goto_153

    #@187
    .line 505
    .restart local v9       #amountOfTextData:I
    .restart local v29       #j:I
    :cond_187
    move-object/from16 v0, p0

    #@189
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@18b
    iget-object v2, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mRoamIndTable:Ljava/util/HashMap;

    #@18d
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@190
    move-result-object v3

    #@191
    new-instance v11, Lcom/android/internal/telephony/cdma/EriInfo;

    #@193
    move/from16 v16, v5

    #@195
    invoke-direct/range {v11 .. v17}, Lcom/android/internal/telephony/cdma/EriInfo;-><init>(IIILjava/lang/String;II)V

    #@198
    invoke-virtual {v2, v3, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19b
    .line 432
    add-int/lit8 v28, v28, 0x1

    #@19d
    goto/16 :goto_10d

    #@19f
    .line 514
    .end local v5           #callPromptId:I
    .end local v8           #characterEncodingType:I
    .end local v9           #amountOfTextData:I
    .end local v12           #roamingIndicator:I
    .end local v13           #iconIndex:I
    .end local v14           #iconMode:I
    .end local v15           #eriText:Ljava/lang/String;
    .end local v17           #alertId:I
    .end local v25           #char_bit_len:B
    .end local v29           #j:I
    :cond_19f
    const/16 v28, 0x0

    #@1a1
    :goto_1a1
    move-object/from16 v0, p2

    #@1a3
    iget v2, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mNumberOfIconImages:I

    #@1a5
    move/from16 v0, v28

    #@1a7
    if-ge v0, v2, :cond_207

    #@1a9
    .line 518
    const-string v23, ""

    #@1ab
    .line 521
    .local v23, iconFileName:Ljava/lang/String;
    const/4 v2, 0x4

    #@1ac
    move-object/from16 v0, v24

    #@1ae
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1b1
    move-result v20

    #@1b2
    .line 523
    .local v20, imageId:I
    add-int/lit8 v27, v27, 0x4

    #@1b4
    .line 526
    const/4 v2, 0x4

    #@1b5
    move-object/from16 v0, v24

    #@1b7
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1ba
    move-result v21

    #@1bb
    .line 528
    .local v21, reservedBits:I
    add-int/lit8 v27, v27, 0x4

    #@1bd
    .line 531
    const/16 v2, 0x8

    #@1bf
    move-object/from16 v0, v24

    #@1c1
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1c4
    move-result v9

    #@1c5
    .line 533
    .restart local v9       #amountOfTextData:I
    add-int/lit8 v27, v27, 0x8

    #@1c7
    .line 535
    const/16 v25, 0x8

    #@1c9
    .line 538
    .restart local v25       #char_bit_len:B
    const/16 v29, 0x0

    #@1cb
    .restart local v29       #j:I
    :goto_1cb
    move/from16 v0, v29

    #@1cd
    if-ge v0, v9, :cond_1ec

    #@1cf
    .line 540
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d4
    move-object/from16 v0, v23

    #@1d6
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v2

    #@1da
    invoke-virtual/range {v24 .. v25}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1dd
    move-result v3

    #@1de
    int-to-char v3, v3

    #@1df
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1e2
    move-result-object v2

    #@1e3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e6
    move-result-object v23

    #@1e7
    .line 541
    add-int v27, v27, v25

    #@1e9
    .line 538
    add-int/lit8 v29, v29, 0x1

    #@1eb
    goto :goto_1cb

    #@1ec
    .line 544
    :cond_1ec
    move-object/from16 v0, p0

    #@1ee
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@1f0
    iget-object v2, v2, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mIconImgTable:Ljava/util/HashMap;

    #@1f2
    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f5
    move-result-object v3

    #@1f6
    new-instance v18, Lcom/android/internal/telephony/cdma/EriManager$EriImg;

    #@1f8
    move-object/from16 v19, p0

    #@1fa
    move/from16 v22, v9

    #@1fc
    invoke-direct/range {v18 .. v23}, Lcom/android/internal/telephony/cdma/EriManager$EriImg;-><init>(Lcom/android/internal/telephony/cdma/EriManager;IIILjava/lang/String;)V

    #@1ff
    move-object/from16 v0, v18

    #@201
    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@204
    .line 514
    add-int/lit8 v28, v28, 0x1

    #@206
    goto :goto_1a1

    #@207
    .line 551
    .end local v9           #amountOfTextData:I
    .end local v20           #imageId:I
    .end local v21           #reservedBits:I
    .end local v23           #iconFileName:Ljava/lang/String;
    .end local v25           #char_bit_len:B
    .end local v29           #j:I
    :cond_207
    rem-int/lit8 v2, v27, 0x8

    #@209
    rsub-int/lit8 v30, v2, 0x8

    #@20b
    .line 552
    const/16 v2, 0x8

    #@20d
    move/from16 v0, v30

    #@20f
    if-ne v0, v2, :cond_213

    #@211
    .line 553
    const/16 v30, 0x0

    #@213
    .line 557
    :cond_213
    if-eqz v30, :cond_223

    #@215
    .line 558
    move-object/from16 v0, v24

    #@217
    move/from16 v1, v30

    #@219
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@21c
    move-result v2

    #@21d
    move-object/from16 v0, p2

    #@21f
    iput v2, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mReservedPadBits:I

    #@221
    .line 560
    add-int v27, v27, v30

    #@223
    .line 564
    :cond_223
    const/16 v2, 0x8

    #@225
    :try_start_225
    move-object/from16 v0, v24

    #@227
    invoke-virtual {v0, v2}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@22a
    move-result v2

    #@22b
    shl-int/lit8 v2, v2, 0x8

    #@22d
    const/16 v3, 0x8

    #@22f
    move-object/from16 v0, v24

    #@231
    invoke-virtual {v0, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@234
    move-result v3

    #@235
    or-int/2addr v2, v3

    #@236
    move-object/from16 v0, p2

    #@238
    iput v2, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mEriFileCrc:I

    #@23a
    .line 568
    const/16 v26, 0x0

    #@23c
    .line 569
    div-int/lit8 v2, v27, 0x8

    #@23e
    int-to-char v2, v2

    #@23f
    move/from16 v0, v26

    #@241
    move-object/from16 v1, p1

    #@243
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/EriManager$EriCrcCalculator;->crc_16_step(C[BC)C

    #@246
    move-result v26

    #@247
    .line 571
    move-object/from16 v0, p2

    #@249
    iget v2, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mEriFileCrc:I
    :try_end_24b
    .catchall {:try_start_225 .. :try_end_24b} :catchall_255
    .catch Lcom/android/internal/util/BitwiseInputStream$AccessException; {:try_start_225 .. :try_end_24b} :catch_25c

    #@24b
    move/from16 v0, v26

    #@24d
    if-eq v0, v2, :cond_24f

    #@24f
    .line 578
    :cond_24f
    :goto_24f
    const/4 v2, 0x1

    #@250
    move-object/from16 v0, p0

    #@252
    iput-boolean v2, v0, Lcom/android/internal/telephony/cdma/EriManager;->isEriFileLoaded:Z

    #@254
    .line 580
    return-void

    #@255
    .line 578
    :catchall_255
    move-exception v2

    #@256
    const/4 v3, 0x1

    #@257
    move-object/from16 v0, p0

    #@259
    iput-boolean v3, v0, Lcom/android/internal/telephony/cdma/EriManager;->isEriFileLoaded:Z

    #@25b
    throw v2

    #@25c
    .line 574
    :catch_25c
    move-exception v2

    #@25d
    goto :goto_24f

    #@25e
    .line 389
    :pswitch_data_25e
    .packed-switch 0x0
        :pswitch_eb
        :pswitch_bb
        :pswitch_e8
        :pswitch_e8
        :pswitch_ee
        :pswitch_bb
        :pswitch_bb
        :pswitch_eb
        :pswitch_eb
    .end packed-switch

    #@274
    .line 472
    :pswitch_data_274
    .packed-switch 0x0
        :pswitch_181
        :pswitch_151
        :pswitch_17e
        :pswitch_17e
        :pswitch_184
        :pswitch_151
        :pswitch_151
        :pswitch_181
        :pswitch_181
    .end packed-switch
.end method


# virtual methods
.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 281
    new-instance v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@2
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/cdma/EriManager$EriFile;-><init>(Lcom/android/internal/telephony/cdma/EriManager;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@7
    .line 282
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->isEriFileLoaded:Z

    #@a
    .line 283
    return-void
.end method

.method public getAlertId(II)I
    .registers 7
    .parameter "roamInd"
    .parameter "defRoamInd"

    #@0
    .prologue
    .line 1131
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/EriManager;->getEriInfo(I)Lcom/android/internal/telephony/cdma/EriInfo;

    #@3
    move-result-object v0

    #@4
    .line 1132
    .local v0, eriInfo:Lcom/android/internal/telephony/cdma/EriInfo;
    if-nez v0, :cond_8

    #@6
    .line 1133
    const/4 v1, -0x1

    #@7
    .line 1136
    :goto_7
    return v1

    #@8
    .line 1135
    :cond_8
    const-string v1, "CDMA"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "eriInfo.mAlertId = "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    iget v3, v0, Lcom/android/internal/telephony/cdma/EriInfo;->mAlertId:I

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1136
    iget v1, v0, Lcom/android/internal/telephony/cdma/EriInfo;->mAlertId:I

    #@24
    goto :goto_7
.end method

.method public getCdmaEriHomeSystems()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1140
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->indexofhomesystem:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getCdmaEriIconIndex(II)I
    .registers 4
    .parameter "roamInd"
    .parameter "defRoamInd"

    #@0
    .prologue
    .line 1119
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/cdma/EriManager;->getEriDisplayInformation(II)Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@3
    move-result-object v0

    #@4
    iget v0, v0, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;->mEriIconIndex:I

    #@6
    return v0
.end method

.method public getCdmaEriIconMode(II)I
    .registers 4
    .parameter "roamInd"
    .parameter "defRoamInd"

    #@0
    .prologue
    .line 1123
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/cdma/EriManager;->getEriDisplayInformation(II)Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@3
    move-result-object v0

    #@4
    iget v0, v0, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;->mEriIconMode:I

    #@6
    return v0
.end method

.method public getCdmaEriText(II)Ljava/lang/String;
    .registers 4
    .parameter "roamInd"
    .parameter "defRoamInd"

    #@0
    .prologue
    .line 1127
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/cdma/EriManager;->getEriDisplayInformation(II)Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;

    #@3
    move-result-object v0

    #@4
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/EriManager$EriDisplayInformation;->mEriIconText:Ljava/lang/String;

    #@6
    return-object v0
.end method

.method public getEriFileType()I
    .registers 2

    #@0
    .prologue
    .line 802
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@2
    iget v0, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mEriFileType:I

    #@4
    return v0
.end method

.method public getEriFileVersion()I
    .registers 2

    #@0
    .prologue
    .line 786
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@2
    iget v0, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mVersionNumber:I

    #@4
    return v0
.end method

.method public getEriNumberOfEntries()I
    .registers 2

    #@0
    .prologue
    .line 794
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFile:Lcom/android/internal/telephony/cdma/EriManager$EriFile;

    #@2
    iget v0, v0, Lcom/android/internal/telephony/cdma/EriManager$EriFile;->mNumberOfEriEntries:I

    #@4
    return v0
.end method

.method public isEriFileLoaded()Z
    .registers 2

    #@0
    .prologue
    .line 810
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->isEriFileLoaded:Z

    #@2
    return v0
.end method

.method public loadEriFile()V
    .registers 2

    #@0
    .prologue
    .line 287
    iget v0, p0, Lcom/android/internal/telephony/cdma/EriManager;->mEriFileSource:I

    #@2
    packed-switch v0, :pswitch_data_12

    #@5
    .line 298
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/EriManager;->loadEriFileFromXml()V

    #@8
    .line 301
    :goto_8
    return-void

    #@9
    .line 289
    :pswitch_9
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/EriManager;->loadEriFileFromModem()V

    #@c
    goto :goto_8

    #@d
    .line 293
    :pswitch_d
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/EriManager;->loadEriFileFromFileSystem()V

    #@10
    goto :goto_8

    #@11
    .line 287
    nop

    #@12
    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_d
        :pswitch_9
    .end packed-switch
.end method

.method sendEriWrite(I)V
    .registers 6
    .parameter "eri_version"

    #@0
    .prologue
    .line 1144
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.ACTION_ERI_VERSION_WRITE"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1145
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "CDMA"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "LGP_CDMA_ERI_VZW_REQ_ERI_VERSION : (sendEriWrite) ERI version is "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 1146
    const-string v1, "phoneEriVersionWrite"

    #@21
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@24
    .line 1147
    const/4 v1, 0x0

    #@25
    const/4 v2, -0x1

    #@26
    invoke-static {v0, v1, v2}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    #@29
    .line 1148
    return-void
.end method
