.class abstract Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;
.super Lcom/android/internal/util/State;
.source "LgeVoiceProtectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LgeVoiceProtectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "BaseState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 253
    iput-object p1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 253
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;-><init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)V

    #@3
    return-void
.end method


# virtual methods
.method public processMessage(Landroid/os/Message;)Z
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const v2, 0x25ceb

    #@4
    .line 259
    iget v0, p1, Landroid/os/Message;->what:I

    #@6
    sparse-switch v0, :sswitch_data_98

    #@9
    .line 301
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Expected Event Received : "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    iget v2, p1, Landroid/os/Message;->what:I

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@23
    .line 306
    :cond_23
    :goto_23
    const/4 v0, 0x1

    #@24
    return v0

    #@25
    .line 262
    :sswitch_25
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@27
    const-string v1, "ICC changed."

    #@29
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$300(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@2c
    .line 263
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@2e
    invoke-virtual {v0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->updateIccAvailability()V

    #@31
    goto :goto_23

    #@32
    .line 267
    :sswitch_32
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@34
    const-string v1, "Records loaded."

    #@36
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$300(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@39
    .line 269
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@3b
    invoke-virtual {v0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->onRecordsLoaded()V

    #@3e
    goto :goto_23

    #@3f
    .line 273
    :sswitch_3f
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@41
    const-string v1, "VP Status changed."

    #@43
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$300(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@46
    .line 274
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@48
    invoke-virtual {v0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->onVpStatusChanged()V

    #@4b
    .line 276
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@4d
    iget-object v0, v0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    #@4f
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@52
    move-result v0

    #@53
    if-eqz v0, :cond_23

    #@55
    .line 277
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@57
    iget-object v0, v0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    #@59
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@5c
    goto :goto_23

    #@5d
    .line 282
    :sswitch_5d
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@5f
    const-string v1, "Data call list changed."

    #@61
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$300(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@64
    .line 284
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@66
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@68
    check-cast v0, Landroid/os/AsyncResult;

    #@6a
    invoke-virtual {v1, v0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->onDataCallListChanged(Landroid/os/AsyncResult;)V

    #@6d
    .line 285
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@6f
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sendMessage(I)V

    #@72
    goto :goto_23

    #@73
    .line 289
    :sswitch_73
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@75
    const-string v1, "Voice call is started"

    #@77
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$300(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@7a
    .line 290
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@7c
    invoke-static {v0, v3}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$100(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V

    #@7f
    .line 291
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@81
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sendMessage(I)V

    #@84
    goto :goto_23

    #@85
    .line 295
    :sswitch_85
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@87
    const-string v1, "Voice call is ended"

    #@89
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$300(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@8c
    .line 296
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@8e
    invoke-static {v0, v3}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$200(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V

    #@91
    .line 297
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@93
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sendMessage(I)V

    #@96
    goto :goto_23

    #@97
    .line 259
    nop

    #@98
    :sswitch_data_98
    .sparse-switch
        0x25ceb -> :sswitch_3f
        0x42002 -> :sswitch_32
        0x42004 -> :sswitch_5d
        0x42007 -> :sswitch_73
        0x42008 -> :sswitch_85
        0x42021 -> :sswitch_25
    .end sparse-switch
.end method
