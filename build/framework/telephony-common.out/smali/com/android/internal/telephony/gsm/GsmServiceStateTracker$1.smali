.class Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;
.super Landroid/content/BroadcastReceiver;
.source "GsmServiceStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 379
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 15
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v11, 0x3

    #@1
    const/4 v10, 0x2

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    const/4 v8, 0x0

    #@5
    .line 382
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@7
    iget-object v5, v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@9
    iget-boolean v5, v5, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@b
    if-nez v5, :cond_2c

    #@d
    .line 383
    const-string v5, "GSM"

    #@f
    new-instance v6, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v7, "Received Intent "

    #@16
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v6

    #@1e
    const-string v7, " while being destroyed. Ignoring."

    #@20
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v6

    #@24
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v6

    #@28
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 561
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 388
    :cond_2c
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    const-string v6, "android.intent.action.LOCALE_CHANGED"

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v5

    #@36
    if-eqz v5, :cond_3e

    #@38
    .line 390
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@3a
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@3d
    goto :goto_2b

    #@3e
    .line 393
    :cond_3e
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    const-string v6, "android.intent.action.LG_NVITEM_RESET"

    #@44
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47
    move-result v5

    #@48
    if-eqz v5, :cond_70

    #@4a
    .line 394
    const-string v5, "KR"

    #@4c
    const-string v6, "SKT"

    #@4e
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@51
    move-result v5

    #@52
    if-nez v5, :cond_5e

    #@54
    const-string v5, "KR"

    #@56
    const-string v6, "KT"

    #@58
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@5b
    move-result v5

    #@5c
    if-eqz v5, :cond_2b

    #@5e
    .line 395
    :cond_5e
    const-string v5, "GSM"

    #@60
    const-string v6, "Receive NV init Intent!"

    #@62
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 396
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@67
    iget-object v5, v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@69
    const v6, 0x20036

    #@6c
    invoke-virtual {v5, v6, v8, v7}, Lcom/android/internal/telephony/gsm/GSMPhone;->setModemIntegerItem(IILandroid/os/Message;)V

    #@6f
    goto :goto_2b

    #@70
    .line 402
    :cond_70
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@73
    move-result-object v5

    #@74
    const-string v6, "android.intent.action.FA_CHANGE"

    #@76
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@79
    move-result v5

    #@7a
    if-eqz v5, :cond_f5

    #@7c
    .line 404
    :try_start_7c
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@7e
    const-string v5, "FA"

    #@80
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@83
    move-result-object v5

    #@84
    check-cast v5, Ljava/lang/String;

    #@86
    invoke-static {v6, v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$002(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;Ljava/lang/String;)Ljava/lang/String;
    :try_end_89
    .catch Ljava/lang/ClassCastException; {:try_start_7c .. :try_end_89} :catch_c4

    #@89
    .line 408
    :goto_89
    const-string v5, "GSM"

    #@8b
    new-instance v6, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v7, "android.intent.action.FA_CHANGE is received = "

    #@92
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v6

    #@96
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@98
    invoke-static {v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$000(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)Ljava/lang/String;

    #@9b
    move-result-object v7

    #@9c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v6

    #@a0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v6

    #@a4
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 409
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@a9
    invoke-static {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$000(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)Ljava/lang/String;

    #@ac
    move-result-object v5

    #@ad
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b0
    move-result v5

    #@b1
    if-nez v5, :cond_de

    #@b3
    .line 410
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@b5
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@b7
    const/16 v7, 0x3f2

    #@b9
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@bc
    move-result-object v6

    #@bd
    const-wide/16 v7, 0x0

    #@bf
    invoke-virtual {v5, v6, v7, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@c2
    goto/16 :goto_2b

    #@c4
    .line 405
    :catch_c4
    move-exception v1

    #@c5
    .line 406
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v5, "GSM"

    #@c7
    new-instance v6, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v7, "android.intent.action.FA_CHANGE ClassCastException="

    #@ce
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v6

    #@d2
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v6

    #@d6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v6

    #@da
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@dd
    goto :goto_89

    #@de
    .line 413
    .end local v1           #e:Ljava/lang/ClassCastException;
    :cond_de
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@e0
    iget-object v5, v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@e2
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@e5
    move-result-object v5

    #@e6
    const-string v6, "skt_fa_changed_fail"

    #@e8
    invoke-static {v6}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@eb
    move-result-object v6

    #@ec
    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@ef
    move-result-object v4

    #@f0
    .line 414
    .local v4, rebootToast:Landroid/widget/Toast;
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    #@f3
    goto/16 :goto_2b

    #@f5
    .line 422
    .end local v4           #rebootToast:Landroid/widget/Toast;
    :cond_f5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@f8
    move-result-object v5

    #@f9
    const-string v6, "android.intent.action.MASTER_CLEAR"

    #@fb
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fe
    move-result v5

    #@ff
    if-eqz v5, :cond_137

    #@101
    .line 424
    const-string v5, "JP"

    #@103
    const-string v6, "DCM"

    #@105
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@108
    move-result v5

    #@109
    if-eqz v5, :cond_2b

    #@10b
    .line 425
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10e
    move-result-object v5

    #@10f
    const-string v6, "mobile_data"

    #@111
    invoke-static {v5, v6, v9}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@114
    move-result v5

    #@115
    if-ne v5, v9, :cond_12d

    #@117
    .line 426
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@119
    invoke-static {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$100(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)Lcom/android/internal/telephony/CommandsInterface;

    #@11c
    move-result-object v5

    #@11d
    const/16 v6, 0x9

    #@11f
    invoke-interface {v5, v6, v7}, Lcom/android/internal/telephony/CommandsInterface;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@122
    .line 429
    :goto_122
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@124
    invoke-static {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$300(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)Lcom/android/internal/telephony/CommandsInterface;

    #@127
    move-result-object v5

    #@128
    invoke-interface {v5, v7}, Lcom/android/internal/telephony/CommandsInterface;->setNetworkSelectionModeAutomatic(Landroid/os/Message;)V

    #@12b
    goto/16 :goto_2b

    #@12d
    .line 428
    :cond_12d
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@12f
    invoke-static {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$200(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)Lcom/android/internal/telephony/CommandsInterface;

    #@132
    move-result-object v5

    #@133
    invoke-interface {v5, v11, v7}, Lcom/android/internal/telephony/CommandsInterface;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@136
    goto :goto_122

    #@137
    .line 435
    :cond_137
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@13a
    move-result-object v5

    #@13b
    const-string v6, "com.android.internal.telephony.STOP_SUPPRESSING_SRV_STATE_IND"

    #@13d
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@140
    move-result v5

    #@141
    if-eqz v5, :cond_15b

    #@143
    .line 436
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@145
    const-string v6, "NONET_SUPP - Suppressing timer expired. No network will be indicated."

    #@147
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@14a
    .line 438
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@14c
    invoke-static {v5, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$402(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;

    #@14f
    .line 440
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@151
    invoke-static {v5, v10}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$502(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;I)I

    #@154
    .line 441
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@156
    invoke-static {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$600(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V

    #@159
    goto/16 :goto_2b

    #@15b
    .line 445
    :cond_15b
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@15e
    move-result-object v5

    #@15f
    const-string v6, "android.intent.action.SIM_STATE_CHANGED"

    #@161
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@164
    move-result v5

    #@165
    if-eqz v5, :cond_285

    #@167
    .line 447
    const-string v5, "GSM"

    #@169
    const-string v6, "Enter ACTION_SIM_STATE_CHANGED!!"

    #@16b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16e
    .line 449
    const-string v5, "ss"

    #@170
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@173
    move-result-object v2

    #@174
    .line 450
    .local v2, icc_state:Ljava/lang/String;
    if-eqz v2, :cond_199

    #@176
    const-string v5, "CARD_IO_ERROR"

    #@178
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17b
    move-result v5

    #@17c
    if-eqz v5, :cond_199

    #@17e
    .line 452
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@181
    move-result-object v5

    #@182
    const-string v6, "ATT"

    #@184
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@187
    move-result v5

    #@188
    if-eqz v5, :cond_199

    #@18a
    .line 453
    const-string v5, "GSM"

    #@18c
    const-string v6, "updateSpnDisplay for ACTION_SIM_STATE_CHANGED / INTENT_VALUE_ICC_CARD_IO_ERROR status"

    #@18e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@191
    .line 454
    invoke-static {v9}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$702(Z)Z

    #@194
    .line 455
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@196
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@199
    .line 459
    :cond_199
    const-string v5, "KR"

    #@19b
    const-string v6, "LGU"

    #@19d
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1a0
    move-result v5

    #@1a1
    if-eqz v5, :cond_1d3

    #@1a3
    .line 460
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@1a5
    new-instance v6, Ljava/lang/StringBuilder;

    #@1a7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1aa
    const-string v7, "icc_state : "

    #@1ac
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v6

    #@1b0
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b3
    move-result-object v6

    #@1b4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b7
    move-result-object v6

    #@1b8
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1bb
    .line 461
    const-string v5, "ABSENT"

    #@1bd
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c0
    move-result v5

    #@1c1
    if-eqz v5, :cond_1f6

    #@1c3
    .line 462
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@1c5
    iget-object v5, v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLgeRegStateNotification:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@1c7
    sget-object v6, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@1c9
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->setSimState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@1cc
    .line 463
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@1ce
    const-string v6, "sim is absent"

    #@1d0
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1d3
    .line 474
    :cond_1d3
    :goto_1d3
    const-string v5, "reason"

    #@1d5
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@1d8
    move-result-object v3

    #@1d9
    .line 476
    .local v3, lock_reason:Ljava/lang/String;
    if-eqz v3, :cond_20f

    #@1db
    const-string v5, "NETWORK"

    #@1dd
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e0
    move-result v5

    #@1e1
    if-eqz v5, :cond_20f

    #@1e3
    .line 478
    const-string v5, "GSM"

    #@1e5
    const-string v6, "updateSpnDisplay for ACTION_SIM_STATE_CHANGED / INTENT_VALUE_LOCKED_NETWORK status"

    #@1e7
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ea
    .line 479
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@1ec
    invoke-static {v5, v9}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$802(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;I)I

    #@1ef
    .line 480
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@1f1
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@1f4
    goto/16 :goto_2b

    #@1f6
    .line 464
    .end local v3           #lock_reason:Ljava/lang/String;
    :cond_1f6
    const-string v5, "READY"

    #@1f8
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1fb
    move-result v5

    #@1fc
    if-eqz v5, :cond_1d3

    #@1fe
    .line 465
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@200
    iget-object v5, v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLgeRegStateNotification:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@202
    sget-object v6, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@204
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->setSimState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@207
    .line 466
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@209
    const-string v6, "sim is ready"

    #@20b
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@20e
    goto :goto_1d3

    #@20f
    .line 482
    .restart local v3       #lock_reason:Ljava/lang/String;
    :cond_20f
    if-eqz v3, :cond_22c

    #@211
    const-string v5, "SERVICE PROVIDER"

    #@213
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@216
    move-result v5

    #@217
    if-eqz v5, :cond_22c

    #@219
    .line 484
    const-string v5, "GSM"

    #@21b
    const-string v6, "updateSpnDisplay for ACTION_SIM_STATE_CHANGED / INTENT_VALUE_LOCKED_SERVICE_PROVIDER status"

    #@21d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@220
    .line 485
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@222
    invoke-static {v5, v10}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$802(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;I)I

    #@225
    .line 486
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@227
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@22a
    goto/16 :goto_2b

    #@22c
    .line 488
    :cond_22c
    if-eqz v3, :cond_249

    #@22e
    const-string v5, "NETWORK SUBSET"

    #@230
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@233
    move-result v5

    #@234
    if-eqz v5, :cond_249

    #@236
    .line 490
    const-string v5, "GSM"

    #@238
    const-string v6, "updateSpnDisplay for ACTION_SIM_STATE_CHANGED / INTENT_VALUE_LOCKED_NETWORK_SUBSET status"

    #@23a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23d
    .line 491
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@23f
    invoke-static {v5, v11}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$802(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;I)I

    #@242
    .line 492
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@244
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@247
    goto/16 :goto_2b

    #@249
    .line 494
    :cond_249
    if-eqz v3, :cond_267

    #@24b
    const-string v5, "CORPORATE"

    #@24d
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@250
    move-result v5

    #@251
    if-eqz v5, :cond_267

    #@253
    .line 496
    const-string v5, "GSM"

    #@255
    const-string v6, "updateSpnDisplay for ACTION_SIM_STATE_CHANGED / INTENT_VALUE_LOCKED_CORPORATE status"

    #@257
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25a
    .line 497
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@25c
    const/4 v6, 0x4

    #@25d
    invoke-static {v5, v6}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$802(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;I)I

    #@260
    .line 498
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@262
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@265
    goto/16 :goto_2b

    #@267
    .line 500
    :cond_267
    if-eqz v3, :cond_2b

    #@269
    const-string v5, "SIM"

    #@26b
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26e
    move-result v5

    #@26f
    if-eqz v5, :cond_2b

    #@271
    .line 502
    const-string v5, "GSM"

    #@273
    const-string v6, "updateSpnDisplay for ACTION_SIM_STATE_CHANGED / INTENT_VALUE_LOCKED_SIM status"

    #@275
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@278
    .line 503
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@27a
    const/4 v6, 0x5

    #@27b
    invoke-static {v5, v6}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$802(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;I)I

    #@27e
    .line 504
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@280
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@283
    goto/16 :goto_2b

    #@285
    .line 513
    .end local v2           #icc_state:Ljava/lang/String;
    .end local v3           #lock_reason:Ljava/lang/String;
    :cond_285
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@288
    move-result-object v5

    #@289
    const-string v6, "android.intent.action.AIRPLANE_MODE"

    #@28b
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28e
    move-result v5

    #@28f
    if-eqz v5, :cond_29f

    #@291
    .line 515
    const-string v5, "state"

    #@293
    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@296
    move-result v5

    #@297
    if-eqz v5, :cond_2b

    #@299
    .line 517
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@29b
    iput-boolean v8, v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSimIsInvalid:Z

    #@29d
    goto/16 :goto_2b

    #@29f
    .line 522
    :cond_29f
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2a2
    move-result-object v5

    #@2a3
    const-string v6, "android.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@2a5
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a8
    move-result v5

    #@2a9
    if-eqz v5, :cond_2b4

    #@2ab
    .line 524
    const-string v5, "GSM"

    #@2ad
    const-string v6, "[jksoh] ACTION_DATA_PDP_REJECT_CAUSE is received"

    #@2af
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b2
    goto/16 :goto_2b

    #@2b4
    .line 526
    :cond_2b4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2b7
    move-result-object v5

    #@2b8
    const-string v6, "com.lge.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@2ba
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2bd
    move-result v5

    #@2be
    if-eqz v5, :cond_2c9

    #@2c0
    .line 528
    const-string v5, "GSM"

    #@2c2
    const-string v6, "[jksoh] ACTION_DATA_PDP_REJECT_CAUSE_LGE is received"

    #@2c4
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c7
    goto/16 :goto_2b

    #@2c9
    .line 530
    :cond_2c9
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2cc
    move-result-object v5

    #@2cd
    const-string v6, "com.lge.mms.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@2cf
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d2
    move-result v5

    #@2d3
    if-eqz v5, :cond_2de

    #@2d5
    .line 532
    const-string v5, "GSM"

    #@2d7
    const-string v6, "[jksoh] ACTION_DATA_PDP_REJECT_CAUSE_LGE_MMS is received"

    #@2d9
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2dc
    goto/16 :goto_2b

    #@2de
    .line 538
    :cond_2de
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2e1
    move-result-object v5

    #@2e2
    const-string v6, "android.intent.action.ACTION_RADIO_OFF"

    #@2e4
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e7
    move-result v5

    #@2e8
    if-eqz v5, :cond_303

    #@2ea
    .line 540
    const-string v5, "GSM"

    #@2ec
    const-string v6, "[GsmServiceStateTracker]intent received"

    #@2ee
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f1
    .line 541
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2f3
    invoke-static {v5, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$902(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;Z)Z

    #@2f6
    .line 542
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2f8
    iget-object v5, v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2fa
    iget-object v0, v5, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2fc
    .line 543
    .local v0, dcTracker:Lcom/android/internal/telephony/DataConnectionTracker;
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2fe
    invoke-virtual {v5, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->powerOffRadioSafely(Lcom/android/internal/telephony/DataConnectionTracker;)V

    #@301
    goto/16 :goto_2b

    #@303
    .line 547
    .end local v0           #dcTracker:Lcom/android/internal/telephony/DataConnectionTracker;
    :cond_303
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@306
    move-result-object v5

    #@307
    const-string v6, "lge.intent.action.LTE_NETWORK_SIB_INFO"

    #@309
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30c
    move-result v5

    #@30d
    if-eqz v5, :cond_36f

    #@30f
    .line 548
    const-string v5, "Emer_Camped_CID"

    #@311
    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@314
    move-result v5

    #@315
    sput v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->EmerCampedCID:I

    #@317
    .line 549
    const-string v5, "Emer_Camped_TAC"

    #@319
    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@31c
    move-result v5

    #@31d
    sput v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->EmerCampedTAC:I

    #@31f
    .line 550
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@321
    invoke-static {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$1000(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)[Ljava/lang/String;

    #@324
    move-result-object v5

    #@325
    sget v6, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->EmerCampedCID:I

    #@327
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@32a
    move-result-object v6

    #@32b
    aput-object v6, v5, v10

    #@32d
    .line 551
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@32f
    invoke-static {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$1100(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)[Ljava/lang/String;

    #@332
    move-result-object v5

    #@333
    sget v6, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->EmerCampedTAC:I

    #@335
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@338
    move-result-object v6

    #@339
    aput-object v6, v5, v11

    #@33b
    .line 552
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@33d
    new-instance v6, Ljava/lang/StringBuilder;

    #@33f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@342
    const-string v7, "[EPDN] ACTION_VOLTE_NETWORK_SIB_INFO: mEmerCampedCID = "

    #@344
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@347
    move-result-object v6

    #@348
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@34a
    invoke-static {v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$1200(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)[Ljava/lang/String;

    #@34d
    move-result-object v7

    #@34e
    aget-object v7, v7, v10

    #@350
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@353
    move-result-object v6

    #@354
    const-string v7, ", mEmerCampedTAC = "

    #@356
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@359
    move-result-object v6

    #@35a
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@35c
    invoke-static {v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->access$1300(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)[Ljava/lang/String;

    #@35f
    move-result-object v7

    #@360
    aget-object v7, v7, v11

    #@362
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@365
    move-result-object v6

    #@366
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@369
    move-result-object v6

    #@36a
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@36d
    goto/16 :goto_2b

    #@36f
    .line 557
    :cond_36f
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@372
    move-result-object v5

    #@373
    const-string v6, "android.intent.action.LTE_EMM_REJECT"

    #@375
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@378
    move-result v5

    #@379
    if-eqz v5, :cond_2b

    #@37b
    .line 558
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;->this$0:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@37d
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@380
    goto/16 :goto_2b
.end method
