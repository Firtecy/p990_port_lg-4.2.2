.class public abstract Lcom/android/internal/telephony/uicc/IccRecords;
.super Landroid/os/Handler;
.source "IccRecords.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;
    }
.end annotation


# static fields
.field protected static final DBG:Z = true

.field public static final EVENT_CFI:I = 0x1

.field public static final EVENT_GET_ICC_RECORD_DONE:I = 0x64

.field public static final EVENT_HPLMNWACT:I = 0xb

.field public static final EVENT_IMSI_M:I = 0xa

.field public static final EVENT_MWI:I = 0x0

.field protected static final EVENT_SET_MSISDN_DONE:I = 0x1e

.field public static final EVENT_SPN:I = 0x2

.field public static final LGE_KT_USIM:I = 0x2

.field public static final LGE_LGU_USIM:I = 0x5

.field public static final LGE_NOT_DECIDED:I = 0x0

.field public static final LGE_OUTBOUND_USIM:I = 0x3

.field public static final LGE_SKT_USIM:I = 0x1

.field public static final LGE_TEST_USIM:I = 0x4

.field public static final LGE_USIM_IS_EMPTY:I = 0x2

.field public static final LGE_USIM_IS_HOME_IMSI:I = 0x1

.field public static final LGE_USIM_IS_NOT_EMPTY:I = 0x1

.field public static final LGE_USIM_IS_SPON1_IMSI:I = 0x2

.field public static final LGE_USIM_IS_SPON2_IMSI:I = 0x3

.field public static final LGE_USIM_IS_SPON3_IMSI:I = 0x4

.field public static final SIM_ICCID_VALUE:Ljava/lang/String; = "sim_iccid_value"

.field protected static final SPN_RULE_SHOW_PLMN:I = 0x2

.field protected static final SPN_RULE_SHOW_SPN:I = 0x1

.field protected static final UNINITIALIZED:I = -0x1

.field protected static final UNKNOWN:I

.field protected static iccidToLoad:Z

.field protected static isSimChanged:Z

.field public static isSimSmsDeleteAll:Z

.field public static mIsEmptyUsim:I

.field public static mIsSponIMSI:I

.field protected static mMccMnc:Ljava/lang/String;

.field public static mUiccType:I

.field public static mWriteEFRoaming:Z

.field public static mncLengthAd:I


# instance fields
.field protected adnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

.field protected countVoiceMessages:I

.field public iccid:Ljava/lang/String;

.field public imsi_m:Ljava/lang/String;

.field protected imsi_m_provisioned:Z

.field protected isVoiceMailFixed:Z

.field protected mActHplmn:Ljava/lang/String;

.field protected mCi:Lcom/android/internal/telephony/CommandsInterface;

.field protected mContext:Landroid/content/Context;

.field protected mDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

.field protected mImsi:Ljava/lang/String;

.field protected mImsiReadyRegistrants:Landroid/os/RegistrantList;

.field protected mMFImsi:Ljava/lang/String;

.field protected mNetworkSelectionModeAutomaticRegistrants:Landroid/os/RegistrantList;

.field protected mNewSmsRegistrants:Landroid/os/RegistrantList;

.field protected mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

.field protected mRecordsEventsRegistrants:Landroid/os/RegistrantList;

.field protected mSponImsi1:Ljava/lang/String;

.field protected mSponImsi2:Ljava/lang/String;

.field protected mSponImsi3:Ljava/lang/String;

.field protected mVmsIsNotInSIM:Z

.field protected mailboxIndex:I

.field protected mncLength:I

.field protected msisdn:Ljava/lang/String;

.field protected msisdnTag:Ljava/lang/String;

.field protected newVoiceMailNum:Ljava/lang/String;

.field protected newVoiceMailTag:Ljava/lang/String;

.field protected recordsLoadedRegistrants:Landroid/os/RegistrantList;

.field protected recordsRequested:Z

.field protected recordsToLoad:I

.field protected spn:Ljava/lang/String;

.field protected voiceMailNum:Ljava/lang/String;

.field protected voiceMailTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 67
    sput v1, Lcom/android/internal/telephony/uicc/IccRecords;->mUiccType:I

    #@3
    .line 68
    sput v1, Lcom/android/internal/telephony/uicc/IccRecords;->mIsEmptyUsim:I

    #@5
    .line 71
    const/4 v0, -0x1

    #@6
    sput v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLengthAd:I

    #@8
    .line 79
    sput v1, Lcom/android/internal/telephony/uicc/IccRecords;->mIsSponIMSI:I

    #@a
    .line 81
    sput-boolean v1, Lcom/android/internal/telephony/uicc/IccRecords;->mWriteEFRoaming:Z

    #@c
    .line 121
    const/4 v0, 0x0

    #@d
    sput-object v0, Lcom/android/internal/telephony/uicc/IccRecords;->mMccMnc:Ljava/lang/String;

    #@f
    .line 169
    sput-boolean v1, Lcom/android/internal/telephony/uicc/IccRecords;->iccidToLoad:Z

    #@11
    .line 170
    sput-boolean v1, Lcom/android/internal/telephony/uicc/IccRecords;->isSimChanged:Z

    #@13
    .line 174
    sput-boolean v1, Lcom/android/internal/telephony/uicc/IccRecords;->isSimSmsDeleteAll:Z

    #@15
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 7
    .parameter "app"
    .parameter "c"
    .parameter "ci"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 193
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    .line 88
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@7
    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@c
    .line 94
    new-instance v0, Landroid/os/RegistrantList;

    #@e
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@11
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsLoadedRegistrants:Landroid/os/RegistrantList;

    #@13
    .line 95
    new-instance v0, Landroid/os/RegistrantList;

    #@15
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@18
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsiReadyRegistrants:Landroid/os/RegistrantList;

    #@1a
    .line 96
    new-instance v0, Landroid/os/RegistrantList;

    #@1c
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1f
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mRecordsEventsRegistrants:Landroid/os/RegistrantList;

    #@21
    .line 97
    new-instance v0, Landroid/os/RegistrantList;

    #@23
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@26
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mNewSmsRegistrants:Landroid/os/RegistrantList;

    #@28
    .line 98
    new-instance v0, Landroid/os/RegistrantList;

    #@2a
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@2d
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mNetworkSelectionModeAutomaticRegistrants:Landroid/os/RegistrantList;

    #@2f
    .line 106
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@31
    .line 109
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@33
    .line 110
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdnTag:Ljava/lang/String;

    #@35
    .line 111
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailNum:Ljava/lang/String;

    #@37
    .line 112
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailTag:Ljava/lang/String;

    #@39
    .line 113
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->newVoiceMailNum:Ljava/lang/String;

    #@3b
    .line 114
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->newVoiceMailTag:Ljava/lang/String;

    #@3d
    .line 115
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->isVoiceMailFixed:Z

    #@3f
    .line 116
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->countVoiceMessages:I

    #@41
    .line 120
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mMFImsi:Ljava/lang/String;

    #@43
    .line 122
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi1:Ljava/lang/String;

    #@45
    .line 123
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi2:Ljava/lang/String;

    #@47
    .line 124
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi3:Ljava/lang/String;

    #@49
    .line 127
    const/4 v0, -0x1

    #@4a
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@4c
    .line 128
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mailboxIndex:I

    #@4e
    .line 132
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mVmsIsNotInSIM:Z

    #@50
    .line 136
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->imsi_m:Ljava/lang/String;

    #@52
    .line 137
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mActHplmn:Ljava/lang/String;

    #@54
    .line 157
    const/4 v0, 0x1

    #@55
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->imsi_m_provisioned:Z

    #@57
    .line 194
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@59
    .line 195
    iput-object p3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@5b
    .line 196
    invoke-virtual {p1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@5e
    move-result-object v0

    #@5f
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@61
    .line 197
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@63
    .line 198
    return-void
.end method


# virtual methods
.method public IsVMNumberNotInSIM()Z
    .registers 3

    #@0
    .prologue
    .line 568
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "IsVMNumberNotInSIM> mVmsIsNotInSIM "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mVmsIsNotInSIM:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@18
    .line 569
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mVmsIsNotInSIM:Z

    #@1a
    return v0
.end method

.method public dispose()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 204
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@3
    const/4 v1, 0x1

    #@4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@7
    .line 205
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    .line 206
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@b
    .line 207
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@d
    .line 208
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@f
    .line 209
    return-void
.end method

.method public getAdnCache()Lcom/android/internal/telephony/uicc/AdnRecordCache;
    .registers 2

    #@0
    .prologue
    .line 215
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->adnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@2
    return-object v0
.end method

.method public getCommandsInterface()Lcom/android/internal/telephony/CommandsInterface;
    .registers 2

    #@0
    .prologue
    .line 589
    const-string v0, "##### LGE FUNCTION USED ##### getCommandsInterface"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@5
    .line 590
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 579
    const-string v0, "##### LGE FUNCTION USED ##### getContext "

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@5
    .line 580
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@7
    return-object v0
.end method

.method public abstract getDisplayRule(Ljava/lang/String;)I
.end method

.method public getIMSI()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 287
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getIMSI_M_RecordsLoaded()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 422
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->imsi_m_provisioned:Z

    #@3
    if-ne v1, v0, :cond_6

    #@5
    .line 425
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;
    .registers 2

    #@0
    .prologue
    .line 584
    const-string v0, "##### LGE FUNCTION USED ##### getIccFileHandler"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@5
    .line 585
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@7
    return-object v0
.end method

.method public getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;
    .registers 2

    #@0
    .prologue
    .line 557
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getMncLength()I
    .registers 3

    #@0
    .prologue
    .line 643
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "IccRecords: getMncLength() "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@18
    .line 644
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1a
    return v0
.end method

.method public getMsisdnAlphaTag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 343
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdnTag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMsisdnNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 300
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getOperatorNumeric()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 489
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getRecordsLoaded()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 414
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@3
    if-nez v1, :cond_a

    #@5
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@7
    if-ne v1, v0, :cond_a

    #@9
    .line 417
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public getServiceProviderName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 355
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSimIccid()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 627
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccRecords;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@7
    move-result-object v0

    #@8
    .line 628
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "sim_iccid_value"

    #@a
    const/4 v2, 0x0

    #@b
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    return-object v1
.end method

.method public getUiccCardApplication()Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .registers 2

    #@0
    .prologue
    .line 594
    const-string v0, "##### LGE FUNCTION USED ##### getUiccCardApplication"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@5
    .line 595
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@7
    return-object v0
.end method

.method public getUsimIsEmpty()I
    .registers 3

    #@0
    .prologue
    .line 618
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "getUsimIsEmpty() "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget v1, Lcom/android/internal/telephony/uicc/IccRecords;->mIsEmptyUsim:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@18
    .line 619
    sget v0, Lcom/android/internal/telephony/uicc/IccRecords;->mIsEmptyUsim:I

    #@1a
    return v0
.end method

.method public getUsimIsSponIMSI()I
    .registers 3

    #@0
    .prologue
    .line 622
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "getUsimIsSponIMSI() "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget v1, Lcom/android/internal/telephony/uicc/IccRecords;->mIsSponIMSI:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@18
    .line 623
    sget v0, Lcom/android/internal/telephony/uicc/IccRecords;->mIsSponIMSI:I

    #@1a
    return v0
.end method

.method public getUsimServiceTable()Lcom/android/internal/telephony/uicc/UsimServiceTable;
    .registers 2

    #@0
    .prologue
    .line 561
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getUsimType()I
    .registers 4

    #@0
    .prologue
    .line 603
    const-string v1, "########## NO MORE USE THIS METHOD : getUsimType() ##########"

    #@2
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@5
    .line 604
    const-string v1, "########## USE SYSTEM PROPERTY     : gsm.sim.type ###########"

    #@7
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@a
    .line 605
    const-string v1, "gsm.sim.type"

    #@c
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    .line 606
    .local v0, gsmSimType:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "getUsimType() "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@26
    .line 607
    const-string v1, "skt"

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v1

    #@2c
    if-eqz v1, :cond_30

    #@2e
    const/4 v1, 0x1

    #@2f
    .line 615
    :goto_2f
    return v1

    #@30
    .line 608
    :cond_30
    const-string v1, "kt"

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v1

    #@36
    if-eqz v1, :cond_3a

    #@38
    const/4 v1, 0x2

    #@39
    goto :goto_2f

    #@3a
    .line 609
    :cond_3a
    const-string v1, "lgu"

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v1

    #@40
    if-eqz v1, :cond_44

    #@42
    const/4 v1, 0x5

    #@43
    goto :goto_2f

    #@44
    .line 613
    :cond_44
    const-string v1, "test"

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v1

    #@4a
    if-eqz v1, :cond_4e

    #@4c
    const/4 v1, 0x4

    #@4d
    goto :goto_2f

    #@4e
    .line 614
    :cond_4e
    const-string v1, "unknown"

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v1

    #@54
    if-eqz v1, :cond_58

    #@56
    const/4 v1, 0x3

    #@57
    goto :goto_2f

    #@58
    .line 615
    :cond_58
    const/4 v1, 0x0

    #@59
    goto :goto_2f
.end method

.method public getVoiceCallForwardingFlag()Z
    .registers 2

    #@0
    .prologue
    .line 506
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getVoiceMailAlphaTag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 386
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailTag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getVoiceMailNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 347
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailNum:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public abstract getVoiceMessageCount()I
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 433
    iget v3, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v3, :pswitch_data_70

    #@5
    .line 455
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@8
    .line 457
    :goto_8
    return-void

    #@9
    .line 436
    :pswitch_9
    :try_start_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b
    check-cast v0, Landroid/os/AsyncResult;

    #@d
    .line 437
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v2, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@f
    check-cast v2, Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;

    #@11
    .line 438
    .local v2, recordLoaded:Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    invoke-interface {v2}, Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;->getEfName()Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, " LOADED"

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@2b
    .line 440
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2d
    if-eqz v3, :cond_4b

    #@2f
    .line 441
    new-instance v3, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v4, "Record Load Exception: "

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/IccRecords;->loge(Ljava/lang/String;)V
    :try_end_47
    .catchall {:try_start_9 .. :try_end_47} :catchall_6a
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_47} :catch_4f

    #@47
    .line 450
    :goto_47
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccRecords;->onRecordLoaded()V

    #@4a
    goto :goto_8

    #@4b
    .line 443
    :cond_4b
    :try_start_4b
    invoke-interface {v2, v0}, Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;->onRecordLoaded(Landroid/os/AsyncResult;)V
    :try_end_4e
    .catchall {:try_start_4b .. :try_end_4e} :catchall_6a
    .catch Ljava/lang/RuntimeException; {:try_start_4b .. :try_end_4e} :catch_4f

    #@4e
    goto :goto_47

    #@4f
    .line 445
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v2           #recordLoaded:Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;
    :catch_4f
    move-exception v1

    #@50
    .line 447
    .local v1, exc:Ljava/lang/RuntimeException;
    :try_start_50
    new-instance v3, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v4, "Exception parsing SIM record: "

    #@57
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/IccRecords;->loge(Ljava/lang/String;)V
    :try_end_66
    .catchall {:try_start_50 .. :try_end_66} :catchall_6a

    #@66
    .line 450
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccRecords;->onRecordLoaded()V

    #@69
    goto :goto_8

    #@6a
    .end local v1           #exc:Ljava/lang/RuntimeException;
    :catchall_6a
    move-exception v3

    #@6b
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccRecords;->onRecordLoaded()V

    #@6e
    throw v3

    #@6f
    .line 433
    nop

    #@70
    :pswitch_data_70
    .packed-switch 0x64
        :pswitch_9
    .end packed-switch
.end method

.method public isCallForwardStatusStored()Z
    .registers 2

    #@0
    .prologue
    .line 497
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isCspPlmnEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 479
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isProvisioned()Z
    .registers 2

    #@0
    .prologue
    .line 535
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected abstract log(Ljava/lang/String;)V
.end method

.method protected abstract loge(Ljava/lang/String;)V
.end method

.method protected abstract onAllRecordsLoaded()V
.end method

.method public abstract onReady()V
.end method

.method protected abstract onRecordLoaded()V
.end method

.method public abstract onRefresh(Z[I)V
.end method

.method protected powerOffOnSimReset()Z
    .registers 3

    #@0
    .prologue
    .line 574
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x1110045

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_11

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public registerForImsiReady(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 8
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 237
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@3
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_a

    #@9
    .line 249
    :cond_9
    :goto_9
    return-void

    #@a
    .line 241
    :cond_a
    new-instance v0, Landroid/os/Registrant;

    #@c
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@f
    .line 242
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsiReadyRegistrants:Landroid/os/RegistrantList;

    #@11
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@14
    .line 245
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@16
    if-eqz v1, :cond_9

    #@18
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1a
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@1d
    move-result-object v1

    #@1e
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@20
    if-ne v1, v2, :cond_9

    #@22
    .line 247
    new-instance v1, Landroid/os/AsyncResult;

    #@24
    invoke-direct {v1, v3, v3, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@27
    invoke-virtual {v0, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@2a
    goto :goto_9
.end method

.method public registerForNetworkSelectionModeAutomatic(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 272
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 273
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mNetworkSelectionModeAutomaticRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 274
    return-void
.end method

.method public registerForNewSms(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 263
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 264
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mNewSmsRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 265
    return-void
.end method

.method public registerForRecordsEvents(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 255
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 256
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mRecordsEventsRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 257
    return-void
.end method

.method public registerForRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 8
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 219
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@3
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_a

    #@9
    .line 231
    :cond_9
    :goto_9
    return-void

    #@a
    .line 223
    :cond_a
    new-instance v0, Landroid/os/Registrant;

    #@c
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@f
    .line 224
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsLoadedRegistrants:Landroid/os/RegistrantList;

    #@11
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@14
    .line 227
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@16
    if-nez v1, :cond_9

    #@18
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@1a
    const/4 v2, 0x1

    #@1b
    if-ne v1, v2, :cond_9

    #@1d
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1f
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@22
    move-result-object v1

    #@23
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@25
    if-ne v1, v2, :cond_9

    #@27
    .line 229
    new-instance v1, Landroid/os/AsyncResult;

    #@29
    invoke-direct {v1, v3, v3, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@2c
    invoke-virtual {v0, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@2f
    goto :goto_9
.end method

.method public setImsi(Ljava/lang/String;)V
    .registers 3
    .parameter "imsi"

    #@0
    .prologue
    .line 295
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@2
    .line 296
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsiReadyRegistrants:Landroid/os/RegistrantList;

    #@4
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@7
    .line 297
    return-void
.end method

.method public setMsisdnNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 11
    .parameter "alphaTag"
    .parameter "number"
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/16 v2, 0x6f40

    #@3
    const/16 v6, 0x1e

    #@5
    const/4 v4, 0x1

    #@6
    .line 321
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@8
    .line 322
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdnTag:Ljava/lang/String;

    #@a
    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Set MSISDN: "

    #@11
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdnTag:Ljava/lang/String;

    #@17
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    const-string v3, " "

    #@1d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@23
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->log(Ljava/lang/String;)V

    #@2e
    .line 327
    new-instance v1, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@30
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdnTag:Ljava/lang/String;

    #@32
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@34
    invoke-direct {v1, v0, v3}, Lcom/android/internal/telephony/uicc/AdnRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 330
    .local v1, adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    const-string v3, "DCM"

    #@3d
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v0

    #@41
    if-eqz v0, :cond_60

    #@43
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@45
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@47
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@4a
    move-result-object v0

    #@4b
    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_USIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@4d
    if-ne v0, v3, :cond_60

    #@4f
    .line 332
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@51
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@53
    invoke-direct {v0, v3}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@56
    const/16 v3, 0x6f4e

    #@58
    invoke-virtual {p0, v6, p3}, Lcom/android/internal/telephony/uicc/IccRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->updateEF(Lcom/android/internal/telephony/uicc/AdnRecord;IIILjava/lang/String;Landroid/os/Message;)V

    #@5f
    .line 340
    :goto_5f
    return-void

    #@60
    .line 336
    :cond_60
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@62
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@64
    invoke-direct {v0, v3}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@67
    const/16 v3, 0x6f4a

    #@69
    invoke-virtual {p0, v6, p3}, Lcom/android/internal/telephony/uicc/IccRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@6c
    move-result-object v6

    #@6d
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->updateEF(Lcom/android/internal/telephony/uicc/AdnRecord;IIILjava/lang/String;Landroid/os/Message;)V

    #@70
    goto :goto_5f
.end method

.method public setSimIccid(Ljava/lang/String;)V
    .registers 5
    .parameter "iccid"

    #@0
    .prologue
    .line 632
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccRecords;->getContext()Landroid/content/Context;

    #@3
    move-result-object v2

    #@4
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@7
    move-result-object v1

    #@8
    .line 633
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@b
    move-result-object v0

    #@c
    .line 634
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "sim_iccid_value"

    #@e
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@11
    .line 635
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@14
    .line 636
    return-void
.end method

.method public setVoiceCallForwardingFlag(IZ)V
    .registers 3
    .parameter "line"
    .parameter "enable"

    #@0
    .prologue
    .line 516
    return-void
.end method

.method public setVoiceCallForwardingFlag(IZLjava/lang/String;)V
    .registers 4
    .parameter "line"
    .parameter "enable"
    .parameter "number"

    #@0
    .prologue
    .line 526
    return-void
.end method

.method public abstract setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract setVoiceMessageWaiting(IILandroid/os/Message;)V
.end method

.method public unregisterForImsiReady(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 251
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsiReadyRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 252
    return-void
.end method

.method public unregisterForNetworkSelectionModeAutomatic(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 276
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mNetworkSelectionModeAutomaticRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 277
    return-void
.end method

.method public unregisterForNewSms(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 267
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mNewSmsRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 268
    return-void
.end method

.method public unregisterForRecordsEvents(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 259
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mRecordsEventsRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 260
    return-void
.end method

.method public unregisterForRecordsLoaded(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 233
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsLoadedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 234
    return-void
.end method
