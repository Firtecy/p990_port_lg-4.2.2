.class Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;
.super Ljava/lang/Object;
.source "SmsMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gsm/SmsMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PduParser"
.end annotation


# instance fields
.field cur:I

.field mUserDataSeptetPadding:I

.field mUserDataSize:I

.field pdu:[B

.field userData:[B

.field userDataHeader:Lcom/android/internal/telephony/SmsHeader;


# direct methods
.method constructor <init>([B)V
    .registers 3
    .parameter "pdu"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 2119
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 2120
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@6
    .line 2121
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@8
    .line 2122
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserDataSeptetPadding:I

    #@a
    .line 2123
    return-void
.end method


# virtual methods
.method constructUserData(ZZ)I
    .registers 16
    .parameter "hasUserDataHeader"
    .parameter "dataInSeptets"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 2256
    iget v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@3
    .line 2257
    .local v4, offset:I
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@5
    add-int/lit8 v5, v4, 0x1

    #@7
    .end local v4           #offset:I
    .local v5, offset:I
    aget-byte v9, v9, v4

    #@9
    and-int/lit16 v8, v9, 0xff

    #@b
    .line 2258
    .local v8, userDataLength:I
    const/4 v3, 0x0

    #@c
    .line 2259
    .local v3, headerSeptets:I
    const/4 v7, 0x0

    #@d
    .line 2261
    .local v7, userDataHeaderLength:I
    if-eqz p1, :cond_68

    #@f
    .line 2262
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@11
    add-int/lit8 v4, v5, 0x1

    #@13
    .end local v5           #offset:I
    .restart local v4       #offset:I
    aget-byte v9, v9, v5

    #@15
    and-int/lit16 v7, v9, 0xff

    #@17
    .line 2264
    new-array v6, v7, [B

    #@19
    .line 2265
    .local v6, udh:[B
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@1b
    invoke-static {v9, v4, v6, v10, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1e
    .line 2266
    invoke-static {v6}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@21
    move-result-object v9

    #@22
    iput-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@24
    .line 2267
    add-int/2addr v4, v7

    #@25
    .line 2269
    add-int/lit8 v9, v7, 0x1

    #@27
    mul-int/lit8 v2, v9, 0x8

    #@29
    .line 2270
    .local v2, headerBits:I
    div-int/lit8 v3, v2, 0x7

    #@2b
    .line 2271
    rem-int/lit8 v9, v2, 0x7

    #@2d
    if-lez v9, :cond_54

    #@2f
    const/4 v9, 0x1

    #@30
    :goto_30
    add-int/2addr v3, v9

    #@31
    .line 2272
    mul-int/lit8 v9, v3, 0x7

    #@33
    sub-int/2addr v9, v2

    #@34
    iput v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserDataSeptetPadding:I

    #@36
    .line 2276
    .end local v2           #headerBits:I
    .end local v6           #udh:[B
    :goto_36
    if-eqz p2, :cond_56

    #@38
    .line 2282
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@3a
    array-length v9, v9

    #@3b
    sub-int v0, v9, v4

    #@3d
    .line 2294
    .local v0, bufferLen:I
    :cond_3d
    :goto_3d
    new-array v9, v0, [B

    #@3f
    iput-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->userData:[B

    #@41
    .line 2295
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@43
    iget-object v11, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->userData:[B

    #@45
    iget-object v12, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->userData:[B

    #@47
    array-length v12, v12

    #@48
    invoke-static {v9, v4, v11, v10, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@4b
    .line 2296
    iput v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@4d
    .line 2298
    if-eqz p2, :cond_64

    #@4f
    .line 2300
    sub-int v1, v8, v3

    #@51
    .line 2302
    .local v1, count:I
    if-gez v1, :cond_62

    #@53
    .line 2305
    .end local v1           #count:I
    :goto_53
    return v10

    #@54
    .end local v0           #bufferLen:I
    .restart local v2       #headerBits:I
    .restart local v6       #udh:[B
    :cond_54
    move v9, v10

    #@55
    .line 2271
    goto :goto_30

    #@56
    .line 2288
    .end local v2           #headerBits:I
    .end local v6           #udh:[B
    :cond_56
    if-eqz p1, :cond_60

    #@58
    add-int/lit8 v9, v7, 0x1

    #@5a
    :goto_5a
    sub-int v0, v8, v9

    #@5c
    .line 2289
    .restart local v0       #bufferLen:I
    if-gez v0, :cond_3d

    #@5e
    .line 2290
    const/4 v0, 0x0

    #@5f
    goto :goto_3d

    #@60
    .end local v0           #bufferLen:I
    :cond_60
    move v9, v10

    #@61
    .line 2288
    goto :goto_5a

    #@62
    .restart local v0       #bufferLen:I
    .restart local v1       #count:I
    :cond_62
    move v10, v1

    #@63
    .line 2302
    goto :goto_53

    #@64
    .line 2305
    .end local v1           #count:I
    :cond_64
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->userData:[B

    #@66
    array-length v10, v9

    #@67
    goto :goto_53

    #@68
    .end local v0           #bufferLen:I
    .end local v4           #offset:I
    .restart local v5       #offset:I
    :cond_68
    move v4, v5

    #@69
    .end local v5           #offset:I
    .restart local v4       #offset:I
    goto :goto_36
.end method

.method countVpoctets(I)I
    .registers 3
    .parameter "count"

    #@0
    .prologue
    .line 2422
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@2
    add-int/2addr v0, p1

    #@3
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@5
    .line 2423
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@7
    return v0
.end method

.method getAddress()Lcom/android/internal/telephony/gsm/GsmSmsAddress;
    .registers 7

    #@0
    .prologue
    .line 2173
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@2
    iget v5, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@4
    aget-byte v4, v4, v5

    #@6
    and-int/lit16 v0, v4, 0xff

    #@8
    .line 2174
    .local v0, addressLength:I
    add-int/lit8 v4, v0, 0x1

    #@a
    div-int/lit8 v4, v4, 0x2

    #@c
    add-int/lit8 v2, v4, 0x2

    #@e
    .line 2177
    .local v2, lengthBytes:I
    :try_start_e
    new-instance v3, Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    #@10
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@12
    iget v5, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@14
    invoke-direct {v3, v4, v5, v2}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;-><init>([BII)V
    :try_end_17
    .catch Ljava/text/ParseException; {:try_start_e .. :try_end_17} :catch_1d

    #@17
    .line 2183
    .local v3, ret:Lcom/android/internal/telephony/gsm/GsmSmsAddress;
    :goto_17
    iget v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@19
    add-int/2addr v4, v2

    #@1a
    iput v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@1c
    .line 2185
    return-object v3

    #@1d
    .line 2178
    .end local v3           #ret:Lcom/android/internal/telephony/gsm/GsmSmsAddress;
    :catch_1d
    move-exception v1

    #@1e
    .line 2179
    .local v1, e:Ljava/text/ParseException;
    const-string v4, "GSM"

    #@20
    invoke-virtual {v1}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    #@23
    move-result-object v5

    #@24
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 2180
    const/4 v3, 0x0

    #@28
    .restart local v3       #ret:Lcom/android/internal/telephony/gsm/GsmSmsAddress;
    goto :goto_17
.end method

.method getByte()I
    .registers 4

    #@0
    .prologue
    .line 2159
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@2
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@4
    add-int/lit8 v2, v1, 0x1

    #@6
    iput v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@8
    aget-byte v0, v0, v1

    #@a
    and-int/lit16 v0, v0, 0xff

    #@c
    return v0
.end method

.method getSCAddress()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 2134
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@3
    move-result v0

    #@4
    .line 2136
    .local v0, len:I
    if-nez v0, :cond_d

    #@6
    .line 2138
    const/4 v1, 0x0

    #@7
    .line 2150
    .local v1, ret:Ljava/lang/String;
    :goto_7
    iget v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@9
    add-int/2addr v3, v0

    #@a
    iput v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@c
    .line 2152
    return-object v1

    #@d
    .line 2142
    .end local v1           #ret:Ljava/lang/String;
    :cond_d
    :try_start_d
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@f
    iget v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@11
    invoke-static {v3, v4, v0}, Landroid/telephony/PhoneNumberUtils;->calledPartyBCDToString([BII)Ljava/lang/String;
    :try_end_14
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_14} :catch_16

    #@14
    move-result-object v1

    #@15
    .restart local v1       #ret:Ljava/lang/String;
    goto :goto_7

    #@16
    .line 2144
    .end local v1           #ret:Ljava/lang/String;
    :catch_16
    move-exception v2

    #@17
    .line 2145
    .local v2, tr:Ljava/lang/RuntimeException;
    const-string v3, "GSM"

    #@19
    const-string v4, "invalid SC address: "

    #@1b
    invoke-static {v3, v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e
    .line 2146
    const/4 v1, 0x0

    #@1f
    .restart local v1       #ret:Ljava/lang/String;
    goto :goto_7
.end method

.method getSCTimestampMillis()J
    .registers 15

    #@0
    .prologue
    const/16 v13, 0x5a

    #@2
    const/4 v12, 0x1

    #@3
    .line 2195
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@5
    iget v10, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@7
    add-int/lit8 v11, v10, 0x1

    #@9
    iput v11, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@b
    aget-byte v9, v9, v10

    #@d
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    #@10
    move-result v8

    #@11
    .line 2196
    .local v8, year:I
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@13
    iget v10, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@15
    add-int/lit8 v11, v10, 0x1

    #@17
    iput v11, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@19
    aget-byte v9, v9, v10

    #@1b
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    #@1e
    move-result v3

    #@1f
    .line 2197
    .local v3, month:I
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@21
    iget v10, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@23
    add-int/lit8 v11, v10, 0x1

    #@25
    iput v11, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@27
    aget-byte v9, v9, v10

    #@29
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    #@2c
    move-result v0

    #@2d
    .line 2198
    .local v0, day:I
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@2f
    iget v10, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@31
    add-int/lit8 v11, v10, 0x1

    #@33
    iput v11, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@35
    aget-byte v9, v9, v10

    #@37
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    #@3a
    move-result v1

    #@3b
    .line 2199
    .local v1, hour:I
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@3d
    iget v10, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@3f
    add-int/lit8 v11, v10, 0x1

    #@41
    iput v11, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@43
    aget-byte v9, v9, v10

    #@45
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    #@48
    move-result v2

    #@49
    .line 2200
    .local v2, minute:I
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@4b
    iget v10, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@4d
    add-int/lit8 v11, v10, 0x1

    #@4f
    iput v11, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@51
    aget-byte v9, v9, v10

    #@53
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    #@56
    move-result v4

    #@57
    .line 2207
    .local v4, second:I
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@59
    iget v10, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@5b
    add-int/lit8 v11, v10, 0x1

    #@5d
    iput v11, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@5f
    aget-byte v7, v9, v10

    #@61
    .line 2210
    .local v7, tzByte:B
    and-int/lit8 v9, v7, -0x9

    #@63
    int-to-byte v9, v9

    #@64
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    #@67
    move-result v6

    #@68
    .line 2212
    .local v6, timezoneOffset:I
    and-int/lit8 v9, v7, 0x8

    #@6a
    if-nez v9, :cond_93

    #@6c
    .line 2215
    :goto_6c
    if-nez v6, :cond_98

    #@6e
    const/4 v9, 0x0

    #@6f
    const-string v10, "TimeZoneExceptionHandling"

    #@71
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@74
    move-result v9

    #@75
    if-ne v9, v12, :cond_98

    #@77
    .line 2216
    new-instance v5, Landroid/text/format/Time;

    #@79
    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    #@7c
    .line 2219
    .local v5, time:Landroid/text/format/Time;
    if-lt v8, v13, :cond_95

    #@7e
    add-int/lit16 v9, v8, 0x76c

    #@80
    :goto_80
    iput v9, v5, Landroid/text/format/Time;->year:I

    #@82
    .line 2220
    add-int/lit8 v9, v3, -0x1

    #@84
    iput v9, v5, Landroid/text/format/Time;->month:I

    #@86
    .line 2221
    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    #@88
    .line 2222
    iput v1, v5, Landroid/text/format/Time;->hour:I

    #@8a
    .line 2223
    iput v2, v5, Landroid/text/format/Time;->minute:I

    #@8c
    .line 2224
    iput v4, v5, Landroid/text/format/Time;->second:I

    #@8e
    .line 2225
    invoke-virtual {v5, v12}, Landroid/text/format/Time;->toMillis(Z)J

    #@91
    move-result-wide v9

    #@92
    .line 2240
    :goto_92
    return-wide v9

    #@93
    .line 2212
    .end local v5           #time:Landroid/text/format/Time;
    :cond_93
    neg-int v6, v6

    #@94
    goto :goto_6c

    #@95
    .line 2219
    .restart local v5       #time:Landroid/text/format/Time;
    :cond_95
    add-int/lit16 v9, v8, 0x7d0

    #@97
    goto :goto_80

    #@98
    .line 2228
    .end local v5           #time:Landroid/text/format/Time;
    :cond_98
    new-instance v5, Landroid/text/format/Time;

    #@9a
    const-string v9, "UTC"

    #@9c
    invoke-direct {v5, v9}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@9f
    .line 2229
    .restart local v5       #time:Landroid/text/format/Time;
    new-instance v9, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v10, "getSCTimestampMillis(), year:"

    #@a6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v9

    #@aa
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v9

    #@ae
    const-string v10, " month:"

    #@b0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v9

    #@b4
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v9

    #@b8
    const-string v10, " day:"

    #@ba
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v9

    #@be
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v9

    #@c2
    const-string v10, " hour:"

    #@c4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v9

    #@c8
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v9

    #@cc
    const-string v10, " minute:"

    #@ce
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v9

    #@d2
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v9

    #@d6
    const-string v10, " second:"

    #@d8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v9

    #@dc
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@df
    move-result-object v9

    #@e0
    const-string v10, " timezoneOffset:"

    #@e2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v9

    #@e6
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v9

    #@ea
    const-string v10, " localtimezon:"

    #@ec
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v9

    #@f0
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    #@f3
    move-result-object v10

    #@f4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v9

    #@f8
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v9

    #@fc
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@ff
    .line 2232
    if-lt v8, v13, :cond_11f

    #@101
    add-int/lit16 v9, v8, 0x76c

    #@103
    :goto_103
    iput v9, v5, Landroid/text/format/Time;->year:I

    #@105
    .line 2233
    add-int/lit8 v9, v3, -0x1

    #@107
    iput v9, v5, Landroid/text/format/Time;->month:I

    #@109
    .line 2234
    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    #@10b
    .line 2235
    iput v1, v5, Landroid/text/format/Time;->hour:I

    #@10d
    .line 2236
    iput v2, v5, Landroid/text/format/Time;->minute:I

    #@10f
    .line 2237
    iput v4, v5, Landroid/text/format/Time;->second:I

    #@111
    .line 2240
    invoke-virtual {v5, v12}, Landroid/text/format/Time;->toMillis(Z)J

    #@114
    move-result-wide v9

    #@115
    mul-int/lit8 v11, v6, 0xf

    #@117
    mul-int/lit8 v11, v11, 0x3c

    #@119
    mul-int/lit16 v11, v11, 0x3e8

    #@11b
    int-to-long v11, v11

    #@11c
    sub-long/2addr v9, v11

    #@11d
    goto/16 :goto_92

    #@11f
    .line 2232
    :cond_11f
    add-int/lit16 v9, v8, 0x7d0

    #@121
    goto :goto_103
.end method

.method getUserData()[B
    .registers 2

    #@0
    .prologue
    .line 2315
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->userData:[B

    #@2
    return-object v0
.end method

.method getUserDataGSM7Bit(III)Ljava/lang/String;
    .registers 11
    .parameter "septetCount"
    .parameter "languageTable"
    .parameter "languageShiftTable"

    #@0
    .prologue
    .line 2367
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@2
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@4
    iget v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserDataSeptetPadding:I

    #@6
    move v2, p1

    #@7
    move v4, p2

    #@8
    move v5, p3

    #@9
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BIIIII)Ljava/lang/String;

    #@c
    move-result-object v6

    #@d
    .line 2370
    .local v6, ret:Ljava/lang/String;
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@f
    mul-int/lit8 v1, p1, 0x7

    #@11
    div-int/lit8 v1, v1, 0x8

    #@13
    add-int/2addr v0, v1

    #@14
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@16
    .line 2372
    return-object v6
.end method

.method getUserDataGSM8Bit(I)Ljava/lang/String;
    .registers 5
    .parameter "byteCount"

    #@0
    .prologue
    .line 2348
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@2
    iget v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@4
    invoke-static {v1, v2, p1}, Lcom/android/internal/telephony/GsmAlphabet;->gsm8BitUnpackedToString([BII)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 2350
    .local v0, ret:Ljava/lang/String;
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@a
    add-int/2addr v1, p1

    #@b
    iput v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@d
    .line 2352
    return-object v0
.end method

.method getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;
    .registers 2

    #@0
    .prologue
    .line 2335
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@2
    return-object v0
.end method

.method getUserDataKSC5601(I)Ljava/lang/String;
    .registers 7
    .parameter "byteCount"

    #@0
    .prologue
    .line 2407
    :try_start_0
    new-instance v1, Ljava/lang/String;

    #@2
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@4
    iget v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@6
    const-string v4, "KSC5601"

    #@8
    invoke-direct {v1, v2, v3, p1, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_b} :catch_11

    #@b
    .line 2413
    .local v1, ret:Ljava/lang/String;
    :goto_b
    iget v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@d
    add-int/2addr v2, p1

    #@e
    iput v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@10
    .line 2414
    return-object v1

    #@11
    .line 2408
    .end local v1           #ret:Ljava/lang/String;
    :catch_11
    move-exception v0

    #@12
    .line 2409
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    const-string v1, ""

    #@14
    .line 2410
    .restart local v1       #ret:Ljava/lang/String;
    const-string v2, "GSM"

    #@16
    const-string v3, "implausible UnsupportedEncodingException"

    #@18
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b
    goto :goto_b
.end method

.method getUserDataSeptetPadding()I
    .registers 2

    #@0
    .prologue
    .line 2326
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserDataSeptetPadding:I

    #@2
    return v0
.end method

.method getUserDataUCS2(I)Ljava/lang/String;
    .registers 7
    .parameter "byteCount"

    #@0
    .prologue
    .line 2386
    :try_start_0
    new-instance v1, Ljava/lang/String;

    #@2
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@4
    iget v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@6
    const-string v4, "utf-16"

    #@8
    invoke-direct {v1, v2, v3, p1, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_b} :catch_11

    #@b
    .line 2392
    .local v1, ret:Ljava/lang/String;
    :goto_b
    iget v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@d
    add-int/2addr v2, p1

    #@e
    iput v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@10
    .line 2393
    return-object v1

    #@11
    .line 2387
    .end local v1           #ret:Ljava/lang/String;
    :catch_11
    move-exception v0

    #@12
    .line 2388
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    const-string v1, ""

    #@14
    .line 2389
    .restart local v1       #ret:Ljava/lang/String;
    const-string v2, "GSM"

    #@16
    const-string v3, "implausible UnsupportedEncodingException"

    #@18
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b
    goto :goto_b
.end method

.method moreDataPresent()Z
    .registers 3

    #@0
    .prologue
    .line 2418
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->pdu:[B

    #@2
    array-length v0, v0

    #@3
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->cur:I

    #@5
    if-le v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method
