.class Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;
.super Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;
.source "LgeVoiceProtectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LgeVoiceProtectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IdleState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 310
    iput-object p1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;-><init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;)V

    #@6
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 310
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;-><init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)V

    #@3
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 3

    #@0
    .prologue
    .line 314
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@2
    const-string v1, "Enter to IDLE State"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@7
    .line 315
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@9
    iget-object v0, v0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    #@b
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_18

    #@11
    .line 316
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@13
    iget-object v0, v0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    #@15
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@18
    .line 318
    :cond_18
    return-void
.end method

.method public exit()V
    .registers 3

    #@0
    .prologue
    .line 322
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@2
    const-string v1, "Exit from IDLE State"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@7
    .line 323
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 329
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    .line 332
    invoke-super {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->processMessage(Landroid/os/Message;)Z

    #@5
    .line 336
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 341
    const-string v0, "[IDLE] "

    #@2
    return-object v0
.end method
