.class final Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;
.super Ljava/lang/Object;
.source "SmsUsageMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/SmsUsageMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ShortCodePatternMatcher"
.end annotation


# instance fields
.field private final mFreeShortCodePattern:Ljava/util/regex/Pattern;

.field private final mPremiumShortCodePattern:Ljava/util/regex/Pattern;

.field private final mShortCodePattern:Ljava/util/regex/Pattern;

.field private final mStandardShortCodePattern:Ljava/util/regex/Pattern;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "shortCodeRegex"
    .parameter "premiumShortCodeRegex"
    .parameter "freeShortCodeRegex"
    .parameter "standardShortCodeRegex"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 227
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 228
    if-eqz p1, :cond_25

    #@6
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@9
    move-result-object v0

    #@a
    :goto_a
    iput-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mShortCodePattern:Ljava/util/regex/Pattern;

    #@c
    .line 229
    if-eqz p2, :cond_27

    #@e
    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@11
    move-result-object v0

    #@12
    :goto_12
    iput-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mPremiumShortCodePattern:Ljava/util/regex/Pattern;

    #@14
    .line 231
    if-eqz p3, :cond_29

    #@16
    invoke-static {p3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@19
    move-result-object v0

    #@1a
    :goto_1a
    iput-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mFreeShortCodePattern:Ljava/util/regex/Pattern;

    #@1c
    .line 233
    if-eqz p4, :cond_22

    #@1e
    invoke-static {p4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@21
    move-result-object v1

    #@22
    :cond_22
    iput-object v1, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mStandardShortCodePattern:Ljava/util/regex/Pattern;

    #@24
    .line 235
    return-void

    #@25
    :cond_25
    move-object v0, v1

    #@26
    .line 228
    goto :goto_a

    #@27
    :cond_27
    move-object v0, v1

    #@28
    .line 229
    goto :goto_12

    #@29
    :cond_29
    move-object v0, v1

    #@2a
    .line 231
    goto :goto_1a
.end method


# virtual methods
.method getNumberCategory(Ljava/lang/String;)I
    .registers 3
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mFreeShortCodePattern:Ljava/util/regex/Pattern;

    #@2
    if-eqz v0, :cond_12

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mFreeShortCodePattern:Ljava/util/regex/Pattern;

    #@6
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    .line 240
    const/4 v0, 0x1

    #@11
    .line 253
    :goto_11
    return v0

    #@12
    .line 242
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mStandardShortCodePattern:Ljava/util/regex/Pattern;

    #@14
    if-eqz v0, :cond_24

    #@16
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mStandardShortCodePattern:Ljava/util/regex/Pattern;

    #@18
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_24

    #@22
    .line 244
    const/4 v0, 0x2

    #@23
    goto :goto_11

    #@24
    .line 246
    :cond_24
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mPremiumShortCodePattern:Ljava/util/regex/Pattern;

    #@26
    if-eqz v0, :cond_36

    #@28
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mPremiumShortCodePattern:Ljava/util/regex/Pattern;

    #@2a
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_36

    #@34
    .line 248
    const/4 v0, 0x4

    #@35
    goto :goto_11

    #@36
    .line 250
    :cond_36
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mShortCodePattern:Ljava/util/regex/Pattern;

    #@38
    if-eqz v0, :cond_48

    #@3a
    iget-object v0, p0, Lcom/android/internal/telephony/SmsUsageMonitor$ShortCodePatternMatcher;->mShortCodePattern:Ljava/util/regex/Pattern;

    #@3c
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@3f
    move-result-object v0

    #@40
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@43
    move-result v0

    #@44
    if-eqz v0, :cond_48

    #@46
    .line 251
    const/4 v0, 0x3

    #@47
    goto :goto_11

    #@48
    .line 253
    :cond_48
    const/4 v0, 0x0

    #@49
    goto :goto_11
.end method
