.class Lcom/android/internal/telephony/cat/LanguageResponseData;
.super Lcom/android/internal/telephony/cat/ResponseData;
.source "ResponseData.java"


# instance fields
.field private lang:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "lang"

    #@0
    .prologue
    .line 168
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/ResponseData;-><init>()V

    #@3
    .line 169
    iput-object p1, p0, Lcom/android/internal/telephony/cat/LanguageResponseData;->lang:Ljava/lang/String;

    #@5
    .line 170
    return-void
.end method


# virtual methods
.method public format(Ljava/io/ByteArrayOutputStream;)V
    .registers 9
    .parameter "buf"

    #@0
    .prologue
    .line 174
    if-nez p1, :cond_3

    #@2
    .line 196
    :cond_2
    return-void

    #@3
    .line 179
    :cond_3
    sget-object v6, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->LANGUAGE:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@5
    invoke-virtual {v6}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@8
    move-result v6

    #@9
    or-int/lit16 v5, v6, 0x80

    #@b
    .line 180
    .local v5, tag:I
    invoke-virtual {p1, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@e
    .line 184
    iget-object v6, p0, Lcom/android/internal/telephony/cat/LanguageResponseData;->lang:Ljava/lang/String;

    #@10
    if-eqz v6, :cond_31

    #@12
    iget-object v6, p0, Lcom/android/internal/telephony/cat/LanguageResponseData;->lang:Ljava/lang/String;

    #@14
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@17
    move-result v6

    #@18
    if-lez v6, :cond_31

    #@1a
    .line 185
    iget-object v6, p0, Lcom/android/internal/telephony/cat/LanguageResponseData;->lang:Ljava/lang/String;

    #@1c
    invoke-static {v6}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm8BitPacked(Ljava/lang/String;)[B

    #@1f
    move-result-object v2

    #@20
    .line 191
    .local v2, data:[B
    :goto_20
    array-length v6, v2

    #@21
    invoke-virtual {p1, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@24
    .line 193
    move-object v0, v2

    #@25
    .local v0, arr$:[B
    array-length v4, v0

    #@26
    .local v4, len$:I
    const/4 v3, 0x0

    #@27
    .local v3, i$:I
    :goto_27
    if-ge v3, v4, :cond_2

    #@29
    aget-byte v1, v0, v3

    #@2b
    .line 194
    .local v1, b:B
    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2e
    .line 193
    add-int/lit8 v3, v3, 0x1

    #@30
    goto :goto_27

    #@31
    .line 188
    .end local v0           #arr$:[B
    .end local v1           #b:B
    .end local v2           #data:[B
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_31
    const/4 v6, 0x0

    #@32
    new-array v2, v6, [B

    #@34
    .restart local v2       #data:[B
    goto :goto_20
.end method
