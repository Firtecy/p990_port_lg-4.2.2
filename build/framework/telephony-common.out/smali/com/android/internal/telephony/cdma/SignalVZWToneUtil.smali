.class public Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;
.super Ljava/lang/Object;
.source "SignalVZWToneUtil.java"


# static fields
.field public static final CDMA_INVALID_TONE:I = -0x1

.field public static final IS95_CONST_IR_ALERT_HIGH:I = 0x1

.field public static final IS95_CONST_IR_ALERT_LOW:I = 0x2

.field public static final IS95_CONST_IR_ALERT_MED:I = 0x0

.field public static final IS95_CONST_IR_SIGNAL_IS54B:I = 0x2

.field public static final IS95_CONST_IR_SIGNAL_ISDN:I = 0x1

.field public static final IS95_CONST_IR_SIGNAL_TONE:I = 0x0

.field public static final IS95_CONST_IR_SIGNAL_USR_DEFD_ALERT:I = 0x4

.field public static final IS95_CONST_IR_SIG_IS54B_L:I = 0x1

.field public static final IS95_CONST_IR_SIG_IS54B_NO_TONE:I = 0x0

.field public static final IS95_CONST_IR_SIG_IS54B_PBX_L:I = 0x7

.field public static final IS95_CONST_IR_SIG_IS54B_PBX_SLS:I = 0xa

.field public static final IS95_CONST_IR_SIG_IS54B_PBX_SS:I = 0x8

.field public static final IS95_CONST_IR_SIG_IS54B_PBX_SSL:I = 0x9

.field public static final IS95_CONST_IR_SIG_IS54B_PBX_S_X4:I = 0xb

.field public static final IS95_CONST_IR_SIG_IS54B_PPP:I = 0xc

.field public static final IS95_CONST_IR_SIG_IS54B_SLS:I = 0x5

.field public static final IS95_CONST_IR_SIG_IS54B_SS:I = 0x2

.field public static final IS95_CONST_IR_SIG_IS54B_SSL:I = 0x3

.field public static final IS95_CONST_IR_SIG_IS54B_SS_2:I = 0x4

.field public static final IS95_CONST_IR_SIG_IS54B_S_X4:I = 0x6

.field public static final IS95_CONST_IR_SIG_ISDN_INTGRP:I = 0x1

.field public static final IS95_CONST_IR_SIG_ISDN_NORMAL:I = 0x0

.field public static final IS95_CONST_IR_SIG_ISDN_OFF:I = 0xf

.field public static final IS95_CONST_IR_SIG_ISDN_PAT_3:I = 0x3

.field public static final IS95_CONST_IR_SIG_ISDN_PAT_5:I = 0x5

.field public static final IS95_CONST_IR_SIG_ISDN_PAT_6:I = 0x6

.field public static final IS95_CONST_IR_SIG_ISDN_PAT_7:I = 0x7

.field public static final IS95_CONST_IR_SIG_ISDN_PING:I = 0x4

.field public static final IS95_CONST_IR_SIG_ISDN_SP_PRI:I = 0x2

.field public static final IS95_CONST_IR_SIG_TONE_ABBR_ALRT:I = 0x0

.field public static final IS95_CONST_IR_SIG_TONE_ABB_INT:I = 0x3

.field public static final IS95_CONST_IR_SIG_TONE_ABB_RE:I = 0x5

.field public static final IS95_CONST_IR_SIG_TONE_ANSWER:I = 0x8

.field public static final IS95_CONST_IR_SIG_TONE_BUSY:I = 0x6

.field public static final IS95_CONST_IR_SIG_TONE_CALL_W:I = 0x9

.field public static final IS95_CONST_IR_SIG_TONE_CONFIRM:I = 0x7

.field public static final IS95_CONST_IR_SIG_TONE_DIAL:I = 0x0

.field public static final IS95_CONST_IR_SIG_TONE_INT:I = 0x2

.field public static final IS95_CONST_IR_SIG_TONE_NO_TONE:I = 0x3f

.field public static final IS95_CONST_IR_SIG_TONE_PIP:I = 0xa

.field public static final IS95_CONST_IR_SIG_TONE_REORDER:I = 0x4

.field public static final IS95_CONST_IR_SIG_TONE_RING:I = 0x1

.field public static final TAPIAMSSCDMA_SIGNAL_PITCH_UNKNOWN:I

.field private static hm:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v6, 0x4

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x2

    #@4
    const/4 v3, 0x0

    #@5
    .line 84
    new-instance v0, Ljava/util/HashMap;

    #@7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@a
    sput-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@c
    .line 118
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@e
    invoke-static {v5, v3, v3}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@11
    move-result-object v1

    #@12
    const/16 v2, 0x2d

    #@14
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 123
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@1d
    invoke-static {v5, v3, v5}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@20
    move-result-object v1

    #@21
    const/16 v2, 0x2e

    #@23
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    .line 129
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@2c
    invoke-static {v5, v3, v4}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@2f
    move-result-object v1

    #@30
    const/16 v2, 0x2f

    #@32
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@39
    .line 134
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@3b
    invoke-static {v5, v3, v7}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@3e
    move-result-object v1

    #@3f
    const/16 v2, 0x30

    #@41
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@48
    .line 139
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@4a
    invoke-static {v5, v3, v6}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@4d
    move-result-object v1

    #@4e
    const/16 v2, 0x31

    #@50
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@57
    .line 144
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@59
    const/4 v1, 0x5

    #@5a
    invoke-static {v5, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@5d
    move-result-object v1

    #@5e
    const/16 v2, 0x32

    #@60
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@63
    move-result-object v2

    #@64
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@67
    .line 148
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@69
    const/4 v1, 0x6

    #@6a
    invoke-static {v5, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@6d
    move-result-object v1

    #@6e
    const/16 v2, 0x33

    #@70
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@77
    .line 153
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@79
    const/4 v1, 0x7

    #@7a
    invoke-static {v5, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@7d
    move-result-object v1

    #@7e
    const/16 v2, 0x34

    #@80
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@87
    .line 158
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@89
    const/16 v1, 0xf

    #@8b
    invoke-static {v5, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@8e
    move-result-object v1

    #@8f
    const/16 v2, 0x62

    #@91
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@94
    move-result-object v2

    #@95
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@98
    .line 164
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@9a
    invoke-static {v3, v3, v3}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@9d
    move-result-object v1

    #@9e
    const/16 v2, 0x22

    #@a0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a3
    move-result-object v2

    #@a4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a7
    .line 169
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@a9
    invoke-static {v3, v4, v3}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@ac
    move-result-object v1

    #@ad
    const/16 v2, 0x22

    #@af
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b2
    move-result-object v2

    #@b3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b6
    .line 172
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@b8
    invoke-static {v3, v3, v3}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@bb
    move-result-object v1

    #@bc
    const/16 v2, 0x22

    #@be
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c1
    move-result-object v2

    #@c2
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c5
    .line 175
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@c7
    invoke-static {v3, v5, v3}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@ca
    move-result-object v1

    #@cb
    const/16 v2, 0x22

    #@cd
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d0
    move-result-object v2

    #@d1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d4
    .line 177
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@d6
    invoke-static {v3, v3, v5}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@d9
    move-result-object v1

    #@da
    const/16 v2, 0x23

    #@dc
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@df
    move-result-object v2

    #@e0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e3
    .line 182
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@e5
    invoke-static {v3, v4, v5}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@e8
    move-result-object v1

    #@e9
    const/16 v2, 0x23

    #@eb
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ee
    move-result-object v2

    #@ef
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f2
    .line 185
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@f4
    invoke-static {v3, v3, v5}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@f7
    move-result-object v1

    #@f8
    const/16 v2, 0x23

    #@fa
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@fd
    move-result-object v2

    #@fe
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@101
    .line 188
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@103
    invoke-static {v3, v5, v5}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@106
    move-result-object v1

    #@107
    const/16 v2, 0x23

    #@109
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10c
    move-result-object v2

    #@10d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@110
    .line 193
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@112
    invoke-static {v3, v3, v4}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@115
    move-result-object v1

    #@116
    const/16 v2, 0x24

    #@118
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11b
    move-result-object v2

    #@11c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11f
    .line 197
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@121
    invoke-static {v3, v4, v4}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@124
    move-result-object v1

    #@125
    const/16 v2, 0x24

    #@127
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12a
    move-result-object v2

    #@12b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@12e
    .line 200
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@130
    invoke-static {v3, v3, v4}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@133
    move-result-object v1

    #@134
    const/16 v2, 0x24

    #@136
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@139
    move-result-object v2

    #@13a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@13d
    .line 203
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@13f
    invoke-static {v3, v5, v4}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@142
    move-result-object v1

    #@143
    const/16 v2, 0x24

    #@145
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@148
    move-result-object v2

    #@149
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14c
    .line 207
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@14e
    invoke-static {v3, v3, v7}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@151
    move-result-object v1

    #@152
    const/16 v2, 0x25

    #@154
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@157
    move-result-object v2

    #@158
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@15b
    .line 211
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@15d
    invoke-static {v3, v4, v7}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@160
    move-result-object v1

    #@161
    const/16 v2, 0x25

    #@163
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@166
    move-result-object v2

    #@167
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@16a
    .line 214
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@16c
    invoke-static {v3, v3, v7}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@16f
    move-result-object v1

    #@170
    const/16 v2, 0x25

    #@172
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@175
    move-result-object v2

    #@176
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@179
    .line 217
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@17b
    invoke-static {v3, v5, v7}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@17e
    move-result-object v1

    #@17f
    const/16 v2, 0x25

    #@181
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@184
    move-result-object v2

    #@185
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@188
    .line 221
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@18a
    invoke-static {v3, v3, v6}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@18d
    move-result-object v1

    #@18e
    const/16 v2, 0x26

    #@190
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@193
    move-result-object v2

    #@194
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@197
    .line 225
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@199
    invoke-static {v3, v4, v6}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@19c
    move-result-object v1

    #@19d
    const/16 v2, 0x26

    #@19f
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a2
    move-result-object v2

    #@1a3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a6
    .line 228
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@1a8
    invoke-static {v3, v3, v6}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@1ab
    move-result-object v1

    #@1ac
    const/16 v2, 0x26

    #@1ae
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b1
    move-result-object v2

    #@1b2
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b5
    .line 231
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@1b7
    invoke-static {v3, v5, v6}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@1ba
    move-result-object v1

    #@1bb
    const/16 v2, 0x26

    #@1bd
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c0
    move-result-object v2

    #@1c1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c4
    .line 236
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@1c6
    const/4 v1, 0x5

    #@1c7
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@1ca
    move-result-object v1

    #@1cb
    const/16 v2, 0x27

    #@1cd
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d0
    move-result-object v2

    #@1d1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1d4
    .line 240
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@1d6
    const/4 v1, 0x5

    #@1d7
    invoke-static {v3, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@1da
    move-result-object v1

    #@1db
    const/16 v2, 0x27

    #@1dd
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e0
    move-result-object v2

    #@1e1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e4
    .line 243
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@1e6
    const/4 v1, 0x5

    #@1e7
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@1ea
    move-result-object v1

    #@1eb
    const/16 v2, 0x27

    #@1ed
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f0
    move-result-object v2

    #@1f1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f4
    .line 246
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@1f6
    const/4 v1, 0x5

    #@1f7
    invoke-static {v3, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@1fa
    move-result-object v1

    #@1fb
    const/16 v2, 0x27

    #@1fd
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@200
    move-result-object v2

    #@201
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@204
    .line 250
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@206
    const/4 v1, 0x6

    #@207
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@20a
    move-result-object v1

    #@20b
    const/16 v2, 0x28

    #@20d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@210
    move-result-object v2

    #@211
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@214
    .line 254
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@216
    const/4 v1, 0x6

    #@217
    invoke-static {v3, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@21a
    move-result-object v1

    #@21b
    const/16 v2, 0x28

    #@21d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@220
    move-result-object v2

    #@221
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@224
    .line 257
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@226
    const/4 v1, 0x6

    #@227
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@22a
    move-result-object v1

    #@22b
    const/16 v2, 0x28

    #@22d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@230
    move-result-object v2

    #@231
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@234
    .line 260
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@236
    const/4 v1, 0x6

    #@237
    invoke-static {v3, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@23a
    move-result-object v1

    #@23b
    const/16 v2, 0x28

    #@23d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@240
    move-result-object v2

    #@241
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@244
    .line 265
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@246
    const/4 v1, 0x7

    #@247
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@24a
    move-result-object v1

    #@24b
    const/16 v2, 0x29

    #@24d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@250
    move-result-object v2

    #@251
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@254
    .line 269
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@256
    const/4 v1, 0x7

    #@257
    invoke-static {v3, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@25a
    move-result-object v1

    #@25b
    const/16 v2, 0x29

    #@25d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@260
    move-result-object v2

    #@261
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@264
    .line 272
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@266
    const/4 v1, 0x7

    #@267
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@26a
    move-result-object v1

    #@26b
    const/16 v2, 0x29

    #@26d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@270
    move-result-object v2

    #@271
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@274
    .line 275
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@276
    const/4 v1, 0x7

    #@277
    invoke-static {v3, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@27a
    move-result-object v1

    #@27b
    const/16 v2, 0x29

    #@27d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@280
    move-result-object v2

    #@281
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@284
    .line 279
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@286
    const/16 v1, 0x8

    #@288
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@28b
    move-result-object v1

    #@28c
    const/16 v2, 0x2a

    #@28e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@291
    move-result-object v2

    #@292
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@295
    .line 283
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@297
    const/16 v1, 0x8

    #@299
    invoke-static {v3, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@29c
    move-result-object v1

    #@29d
    const/16 v2, 0x2a

    #@29f
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a2
    move-result-object v2

    #@2a3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2a6
    .line 286
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@2a8
    const/16 v1, 0x8

    #@2aa
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@2ad
    move-result-object v1

    #@2ae
    const/16 v2, 0x2a

    #@2b0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b3
    move-result-object v2

    #@2b4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2b7
    .line 289
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@2b9
    const/16 v1, 0x8

    #@2bb
    invoke-static {v3, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@2be
    move-result-object v1

    #@2bf
    const/16 v2, 0x2a

    #@2c1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c4
    move-result-object v2

    #@2c5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c8
    .line 293
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@2ca
    const/16 v1, 0x9

    #@2cc
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@2cf
    move-result-object v1

    #@2d0
    const/16 v2, 0x2b

    #@2d2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d5
    move-result-object v2

    #@2d6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d9
    .line 297
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@2db
    const/16 v1, 0x9

    #@2dd
    invoke-static {v3, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@2e0
    move-result-object v1

    #@2e1
    const/16 v2, 0x2b

    #@2e3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e6
    move-result-object v2

    #@2e7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2ea
    .line 300
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@2ec
    const/16 v1, 0x9

    #@2ee
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@2f1
    move-result-object v1

    #@2f2
    const/16 v2, 0x2b

    #@2f4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f7
    move-result-object v2

    #@2f8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2fb
    .line 303
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@2fd
    const/16 v1, 0x9

    #@2ff
    invoke-static {v3, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@302
    move-result-object v1

    #@303
    const/16 v2, 0x2b

    #@305
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@308
    move-result-object v2

    #@309
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@30c
    .line 307
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@30e
    const/16 v1, 0xa

    #@310
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@313
    move-result-object v1

    #@314
    const/16 v2, 0x2c

    #@316
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@319
    move-result-object v2

    #@31a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@31d
    .line 311
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@31f
    const/16 v1, 0xa

    #@321
    invoke-static {v3, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@324
    move-result-object v1

    #@325
    const/16 v2, 0x2c

    #@327
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@32a
    move-result-object v2

    #@32b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@32e
    .line 314
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@330
    const/16 v1, 0xa

    #@332
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@335
    move-result-object v1

    #@336
    const/16 v2, 0x2c

    #@338
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@33b
    move-result-object v2

    #@33c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@33f
    .line 317
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@341
    const/16 v1, 0xa

    #@343
    invoke-static {v3, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@346
    move-result-object v1

    #@347
    const/16 v2, 0x2c

    #@349
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34c
    move-result-object v2

    #@34d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@350
    .line 321
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@352
    const/16 v1, 0x3f

    #@354
    invoke-static {v3, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@357
    move-result-object v1

    #@358
    const/16 v2, 0x62

    #@35a
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@35d
    move-result-object v2

    #@35e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@361
    .line 329
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@363
    invoke-static {v4, v5, v3}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@366
    move-result-object v1

    #@367
    const/16 v2, 0x62

    #@369
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@36c
    move-result-object v2

    #@36d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@370
    .line 332
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@372
    invoke-static {v4, v3, v3}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@375
    move-result-object v1

    #@376
    const/16 v2, 0x62

    #@378
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@37b
    move-result-object v2

    #@37c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@37f
    .line 335
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@381
    invoke-static {v4, v4, v3}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@384
    move-result-object v1

    #@385
    const/16 v2, 0x62

    #@387
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38a
    move-result-object v2

    #@38b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@38e
    .line 337
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@390
    invoke-static {v4, v5, v5}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@393
    move-result-object v1

    #@394
    const/16 v2, 0x35

    #@396
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@399
    move-result-object v2

    #@39a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@39d
    .line 340
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@39f
    invoke-static {v4, v3, v5}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@3a2
    move-result-object v1

    #@3a3
    const/16 v2, 0x36

    #@3a5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3a8
    move-result-object v2

    #@3a9
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3ac
    .line 343
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@3ae
    invoke-static {v4, v4, v5}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@3b1
    move-result-object v1

    #@3b2
    const/16 v2, 0x37

    #@3b4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3b7
    move-result-object v2

    #@3b8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3bb
    .line 346
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@3bd
    invoke-static {v4, v5, v4}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@3c0
    move-result-object v1

    #@3c1
    const/16 v2, 0x38

    #@3c3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3c6
    move-result-object v2

    #@3c7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3ca
    .line 349
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@3cc
    invoke-static {v4, v3, v4}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@3cf
    move-result-object v1

    #@3d0
    const/16 v2, 0x39

    #@3d2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3d5
    move-result-object v2

    #@3d6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3d9
    .line 352
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@3db
    invoke-static {v4, v4, v4}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@3de
    move-result-object v1

    #@3df
    const/16 v2, 0x3a

    #@3e1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3e4
    move-result-object v2

    #@3e5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3e8
    .line 355
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@3ea
    invoke-static {v4, v5, v7}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@3ed
    move-result-object v1

    #@3ee
    const/16 v2, 0x3b

    #@3f0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f3
    move-result-object v2

    #@3f4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f7
    .line 358
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@3f9
    invoke-static {v4, v3, v7}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@3fc
    move-result-object v1

    #@3fd
    const/16 v2, 0x3c

    #@3ff
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@402
    move-result-object v2

    #@403
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@406
    .line 361
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@408
    invoke-static {v4, v4, v7}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@40b
    move-result-object v1

    #@40c
    const/16 v2, 0x3d

    #@40e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@411
    move-result-object v2

    #@412
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@415
    .line 364
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@417
    invoke-static {v4, v5, v6}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@41a
    move-result-object v1

    #@41b
    const/16 v2, 0x3e

    #@41d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@420
    move-result-object v2

    #@421
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@424
    .line 367
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@426
    invoke-static {v4, v3, v6}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@429
    move-result-object v1

    #@42a
    const/16 v2, 0x3f

    #@42c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@42f
    move-result-object v2

    #@430
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@433
    .line 370
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@435
    invoke-static {v4, v4, v6}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@438
    move-result-object v1

    #@439
    const/16 v2, 0x40

    #@43b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@43e
    move-result-object v2

    #@43f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@442
    .line 373
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@444
    const/4 v1, 0x5

    #@445
    invoke-static {v4, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@448
    move-result-object v1

    #@449
    const/16 v2, 0x41

    #@44b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@44e
    move-result-object v2

    #@44f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@452
    .line 376
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@454
    const/4 v1, 0x5

    #@455
    invoke-static {v4, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@458
    move-result-object v1

    #@459
    const/16 v2, 0x42

    #@45b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@45e
    move-result-object v2

    #@45f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@462
    .line 379
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@464
    const/4 v1, 0x5

    #@465
    invoke-static {v4, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@468
    move-result-object v1

    #@469
    const/16 v2, 0x43

    #@46b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@46e
    move-result-object v2

    #@46f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@472
    .line 382
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@474
    const/4 v1, 0x6

    #@475
    invoke-static {v4, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@478
    move-result-object v1

    #@479
    const/16 v2, 0x44

    #@47b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@47e
    move-result-object v2

    #@47f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@482
    .line 385
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@484
    const/4 v1, 0x6

    #@485
    invoke-static {v4, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@488
    move-result-object v1

    #@489
    const/16 v2, 0x45

    #@48b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@48e
    move-result-object v2

    #@48f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@492
    .line 388
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@494
    const/4 v1, 0x6

    #@495
    invoke-static {v4, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@498
    move-result-object v1

    #@499
    const/16 v2, 0x46

    #@49b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@49e
    move-result-object v2

    #@49f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4a2
    .line 391
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@4a4
    const/4 v1, 0x7

    #@4a5
    invoke-static {v4, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@4a8
    move-result-object v1

    #@4a9
    const/16 v2, 0x47

    #@4ab
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4ae
    move-result-object v2

    #@4af
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4b2
    .line 394
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@4b4
    const/4 v1, 0x7

    #@4b5
    invoke-static {v4, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@4b8
    move-result-object v1

    #@4b9
    const/16 v2, 0x48

    #@4bb
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4be
    move-result-object v2

    #@4bf
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4c2
    .line 397
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@4c4
    const/4 v1, 0x7

    #@4c5
    invoke-static {v4, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@4c8
    move-result-object v1

    #@4c9
    const/16 v2, 0x49

    #@4cb
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4ce
    move-result-object v2

    #@4cf
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4d2
    .line 400
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@4d4
    const/16 v1, 0x8

    #@4d6
    invoke-static {v4, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@4d9
    move-result-object v1

    #@4da
    const/16 v2, 0x4a

    #@4dc
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4df
    move-result-object v2

    #@4e0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4e3
    .line 403
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@4e5
    const/16 v1, 0x8

    #@4e7
    invoke-static {v4, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@4ea
    move-result-object v1

    #@4eb
    const/16 v2, 0x4b

    #@4ed
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4f0
    move-result-object v2

    #@4f1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4f4
    .line 406
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@4f6
    const/16 v1, 0x8

    #@4f8
    invoke-static {v4, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@4fb
    move-result-object v1

    #@4fc
    const/16 v2, 0x4c

    #@4fe
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@501
    move-result-object v2

    #@502
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@505
    .line 409
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@507
    const/16 v1, 0x9

    #@509
    invoke-static {v4, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@50c
    move-result-object v1

    #@50d
    const/16 v2, 0x4d

    #@50f
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@512
    move-result-object v2

    #@513
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@516
    .line 412
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@518
    const/16 v1, 0x9

    #@51a
    invoke-static {v4, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@51d
    move-result-object v1

    #@51e
    const/16 v2, 0x4e

    #@520
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@523
    move-result-object v2

    #@524
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@527
    .line 415
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@529
    const/16 v1, 0x9

    #@52b
    invoke-static {v4, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@52e
    move-result-object v1

    #@52f
    const/16 v2, 0x4f

    #@531
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@534
    move-result-object v2

    #@535
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@538
    .line 418
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@53a
    const/16 v1, 0xa

    #@53c
    invoke-static {v4, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@53f
    move-result-object v1

    #@540
    const/16 v2, 0x50

    #@542
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@545
    move-result-object v2

    #@546
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@549
    .line 421
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@54b
    const/16 v1, 0xa

    #@54d
    invoke-static {v4, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@550
    move-result-object v1

    #@551
    const/16 v2, 0x51

    #@553
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@556
    move-result-object v2

    #@557
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@55a
    .line 424
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@55c
    const/16 v1, 0xa

    #@55e
    invoke-static {v4, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@561
    move-result-object v1

    #@562
    const/16 v2, 0x52

    #@564
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@567
    move-result-object v2

    #@568
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@56b
    .line 427
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@56d
    const/16 v1, 0xb

    #@56f
    invoke-static {v4, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@572
    move-result-object v1

    #@573
    const/16 v2, 0x53

    #@575
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@578
    move-result-object v2

    #@579
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@57c
    .line 430
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@57e
    const/16 v1, 0xb

    #@580
    invoke-static {v4, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@583
    move-result-object v1

    #@584
    const/16 v2, 0x54

    #@586
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@589
    move-result-object v2

    #@58a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@58d
    .line 433
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@58f
    const/16 v1, 0xb

    #@591
    invoke-static {v4, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@594
    move-result-object v1

    #@595
    const/16 v2, 0x55

    #@597
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59a
    move-result-object v2

    #@59b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@59e
    .line 440
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@5a0
    const/16 v1, 0xc

    #@5a2
    invoke-static {v4, v5, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@5a5
    move-result-object v1

    #@5a6
    const/16 v2, 0x2c

    #@5a8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5ab
    move-result-object v2

    #@5ac
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5af
    .line 443
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@5b1
    const/16 v1, 0xc

    #@5b3
    invoke-static {v4, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@5b6
    move-result-object v1

    #@5b7
    const/16 v2, 0x2c

    #@5b9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5bc
    move-result-object v2

    #@5bd
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5c0
    .line 446
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@5c2
    const/16 v1, 0xc

    #@5c4
    invoke-static {v4, v4, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@5c7
    move-result-object v1

    #@5c8
    const/16 v2, 0x2c

    #@5ca
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5cd
    move-result-object v2

    #@5ce
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5d1
    .line 449
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@5d3
    invoke-static {v6, v3, v3}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@5d6
    move-result-object v1

    #@5d7
    const/16 v2, 0x61

    #@5d9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5dc
    move-result-object v2

    #@5dd
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5e0
    .line 453
    sget-object v0, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@5e2
    const/16 v1, 0x3f

    #@5e4
    invoke-static {v6, v3, v1}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@5e7
    move-result-object v1

    #@5e8
    const/16 v2, 0x61

    #@5ea
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5ed
    move-result-object v2

    #@5ee
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5f1
    .line 457
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 460
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 461
    return-void
.end method

.method public static getAudioToneFromSignalInfo(III)I
    .registers 6
    .parameter "signalType"
    .parameter "alertPitch"
    .parameter "signal"

    #@0
    .prologue
    .line 104
    sget-object v1, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->hm:Ljava/util/HashMap;

    #@2
    invoke-static {p0, p1, p2}, Lcom/android/internal/telephony/cdma/SignalVZWToneUtil;->signalParamHash(III)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/Integer;

    #@c
    .line 105
    .local v0, result:Ljava/lang/Integer;
    if-nez v0, :cond_10

    #@e
    .line 106
    const/4 v1, -0x1

    #@f
    .line 108
    :goto_f
    return v1

    #@10
    :cond_10
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@13
    move-result v1

    #@14
    goto :goto_f
.end method

.method private static signalParamHash(III)Ljava/lang/Integer;
    .registers 6
    .parameter "signalType"
    .parameter "alertPitch"
    .parameter "signal"

    #@0
    .prologue
    const/16 v0, 0x100

    #@2
    .line 87
    if-ltz p0, :cond_e

    #@4
    if-gt p0, v0, :cond_e

    #@6
    if-gt p1, v0, :cond_e

    #@8
    if-ltz p1, :cond_e

    #@a
    if-gt p2, v0, :cond_e

    #@c
    if-gez p2, :cond_15

    #@e
    .line 89
    :cond_e
    new-instance v0, Ljava/lang/Integer;

    #@10
    const/4 v1, -0x1

    #@11
    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    #@14
    .line 100
    :goto_14
    return-object v0

    #@15
    .line 97
    :cond_15
    const/4 v0, 0x2

    #@16
    if-eq p0, v0, :cond_19

    #@18
    .line 98
    const/4 p1, 0x0

    #@19
    .line 100
    :cond_19
    new-instance v0, Ljava/lang/Integer;

    #@1b
    mul-int/lit16 v1, p0, 0x100

    #@1d
    mul-int/lit16 v1, v1, 0x100

    #@1f
    mul-int/lit16 v2, p1, 0x100

    #@21
    add-int/2addr v1, v2

    #@22
    add-int/2addr v1, p2

    #@23
    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    #@26
    goto :goto_14
.end method
