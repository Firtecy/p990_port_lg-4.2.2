.class public Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;
.super Ljava/lang/Object;
.source "LGDataConnectionTrackerMsg.java"


# instance fields
.field private final DBG:Z

.field private final LOG_TAG:Ljava/lang/String;

.field public apntype_n:I

.field public cause:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public enable:I

.field public reason:Ljava/lang/String;

.field public success:Z

.field public tag:I

.field public type:Ljava/lang/String;

.field public valid:Z

.field public what:I


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 20
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 21
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->DBG:Z

    #@7
    .line 22
    const-string v0, "LGDataconenctionTrackerMsg"

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->LOG_TAG:Ljava/lang/String;

    #@b
    .line 24
    iput-boolean v1, p0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->success:Z

    #@d
    .line 25
    iput v1, p0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->enable:I

    #@f
    .line 26
    iput v1, p0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->tag:I

    #@11
    .line 27
    iput v1, p0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->apntype_n:I

    #@13
    .line 28
    const-string v0, ""

    #@15
    iput-object v0, p0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->type:Ljava/lang/String;

    #@17
    .line 29
    const-string v0, ""

    #@19
    iput-object v0, p0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->reason:Ljava/lang/String;

    #@1b
    .line 30
    iput-boolean v1, p0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->valid:Z

    #@1d
    .line 31
    iput v1, p0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->what:I

    #@1f
    return-void
.end method
