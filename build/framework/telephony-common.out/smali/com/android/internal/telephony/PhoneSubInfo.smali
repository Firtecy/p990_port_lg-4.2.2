.class public Lcom/android/internal/telephony/PhoneSubInfo;
.super Lcom/android/internal/telephony/IPhoneSubInfo$Stub;
.source "PhoneSubInfo.java"


# static fields
.field private static final CALL_PRIVILEGED:Ljava/lang/String; = "android.permission.CALL_PRIVILEGED"

.field static final LOG_TAG:Ljava/lang/String; = "PHONE"

.field private static final READ_PHONE_STATE:Ljava/lang/String; = "android.permission.READ_PHONE_STATE"

.field private static final READ_PRIVILEGED_PHONE_STATE:Ljava/lang/String; = "android.permission.READ_PRIVILEGED_PHONE_STATE"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/Phone;)V
    .registers 3
    .parameter "phone"

    #@0
    .prologue
    .line 41
    invoke-direct {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;-><init>()V

    #@3
    .line 42
    iput-object p1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@5
    .line 43
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@b
    .line 44
    return-void
.end method


# virtual methods
.method public dispose()V
    .registers 1

    #@0
    .prologue
    .line 47
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 309
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.DUMP"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_33

    #@a
    .line 311
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "Permission Denial: can\'t dump PhoneSubInfo from from pid="

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v1

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", uid="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v1

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 320
    :goto_32
    return-void

    #@33
    .line 317
    :cond_33
    const-string v0, "Phone Subscriber Info:"

    #@35
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v1, "  Phone Type = "

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@45
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@54
    .line 319
    new-instance v0, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v1, "  Device ID = "

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@61
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getDeviceId()Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v0

    #@69
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v0

    #@6d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@70
    goto :goto_32
.end method

.method protected finalize()V
    .registers 4

    #@0
    .prologue
    .line 51
    :try_start_0
    invoke-super {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->finalize()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_3} :catch_b

    #@3
    .line 55
    :goto_3
    const-string v1, "PHONE"

    #@5
    const-string v2, "PhoneSubInfo finalized"

    #@7
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 56
    return-void

    #@b
    .line 52
    :catch_b
    move-exception v0

    #@c
    .line 53
    .local v0, throwable:Ljava/lang/Throwable;
    const-string v1, "PHONE"

    #@e
    const-string v2, "Error while finalizing:"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    goto :goto_3
.end method

.method public getBtid()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 239
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.READ_PRIVILEGED_PHONE_STATE"

    #@4
    const-string v3, "Requires READ_PRIVILEGED_PHONE_STATE"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 241
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@e
    move-result-object v0

    #@f
    .line 242
    .local v0, isim:Lcom/android/internal/telephony/uicc/IsimRecords;
    if-eqz v0, :cond_16

    #@11
    .line 243
    invoke-interface {v0}, Lcom/android/internal/telephony/uicc/IsimRecords;->getIsimBtid()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 245
    :goto_15
    return-object v1

    #@16
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_15
.end method

.method public getCompleteVoiceMailNumber()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 143
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CALL_PRIVILEGED"

    #@4
    const-string v3, "Requires CALL_PRIVILEGED"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 145
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getVoiceMailNumber()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 146
    .local v0, number:Ljava/lang/String;
    const-string v1, "PHONE"

    #@11
    const-string v2, "VM: PhoneSubInfo.getCompleteVoiceMailNUmber: "

    #@13
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 147
    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v2, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 63
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getDeviceId()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getDeviceId_type(I)Ljava/lang/String;
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 289
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v2, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 290
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getDeviceId(I)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getDeviceSvn()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v2, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 72
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getDeviceSvn()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getIPPhoneState()Z
    .registers 4

    #@0
    .prologue
    .line 303
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v2, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 304
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getIPPhoneState()Z

    #@e
    move-result v0

    #@f
    return v0
.end method

.method public getIccSerialNumber()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v2, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 88
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getIccSerialNumber()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getInternationalMdnVoiceMailNumberForVZW()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 130
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v3, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 131
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getInternationalMdnVoiceMailNumberForVZW()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 132
    .local v0, number:Ljava/lang/String;
    const-string v1, ""

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "** HEE** VM: PhoneSubInfo.getInternationalMdnVoiceMailNumberForVZW: "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 133
    return-object v0
.end method

.method public getIsimDomain()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 178
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.READ_PRIVILEGED_PHONE_STATE"

    #@4
    const-string v3, "Requires READ_PRIVILEGED_PHONE_STATE"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 180
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@e
    move-result-object v0

    #@f
    .line 181
    .local v0, isim:Lcom/android/internal/telephony/uicc/IsimRecords;
    if-eqz v0, :cond_16

    #@11
    .line 182
    invoke-interface {v0}, Lcom/android/internal/telephony/uicc/IsimRecords;->getIsimDomain()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 184
    :goto_15
    return-object v1

    #@16
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_15
.end method

.method public getIsimImpi()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 163
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.READ_PRIVILEGED_PHONE_STATE"

    #@4
    const-string v3, "Requires READ_PRIVILEGED_PHONE_STATE"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 165
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@e
    move-result-object v0

    #@f
    .line 166
    .local v0, isim:Lcom/android/internal/telephony/uicc/IsimRecords;
    if-eqz v0, :cond_16

    #@11
    .line 167
    invoke-interface {v0}, Lcom/android/internal/telephony/uicc/IsimRecords;->getIsimImpi()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 169
    :goto_15
    return-object v1

    #@16
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_15
.end method

.method public getIsimImpu()[Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 194
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.READ_PRIVILEGED_PHONE_STATE"

    #@4
    const-string v3, "Requires READ_PRIVILEGED_PHONE_STATE"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 196
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@e
    move-result-object v0

    #@f
    .line 197
    .local v0, isim:Lcom/android/internal/telephony/uicc/IsimRecords;
    if-eqz v0, :cond_16

    #@11
    .line 198
    invoke-interface {v0}, Lcom/android/internal/telephony/uicc/IsimRecords;->getIsimImpu()[Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 200
    :goto_15
    return-object v1

    #@16
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_15
.end method

.method public getKeyLifetime()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 250
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.READ_PRIVILEGED_PHONE_STATE"

    #@4
    const-string v3, "Requires READ_PRIVILEGED_PHONE_STATE"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 252
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@e
    move-result-object v0

    #@f
    .line 253
    .local v0, isim:Lcom/android/internal/telephony/uicc/IsimRecords;
    if-eqz v0, :cond_16

    #@11
    .line 254
    invoke-interface {v0}, Lcom/android/internal/telephony/uicc/IsimRecords;->getIsimKeyLifetime()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 256
    :goto_15
    return-object v1

    #@16
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_15
.end method

.method public getLine1AlphaTag()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v2, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 104
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getLine1AlphaTag()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getLine1Number()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v2, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 96
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getLine1Number()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getMSIN()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 297
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getMSIN()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getMsisdn()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v2, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 112
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getMsisdn()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getRand()[B
    .registers 5

    #@0
    .prologue
    .line 227
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.READ_PRIVILEGED_PHONE_STATE"

    #@4
    const-string v3, "Requires READ_PRIVILEGED_PHONE_STATE"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 229
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@e
    move-result-object v0

    #@f
    .line 230
    .local v0, isim:Lcom/android/internal/telephony/uicc/IsimRecords;
    if-eqz v0, :cond_16

    #@11
    .line 231
    invoke-interface {v0}, Lcom/android/internal/telephony/uicc/IsimRecords;->getIsimRand()[B

    #@14
    move-result-object v1

    #@15
    .line 233
    :goto_15
    return-object v1

    #@16
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_15
.end method

.method public getSubscriberId()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v2, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 80
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getSubscriberId()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getVoiceMailAlphaTag()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 154
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v2, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 155
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getVoiceMailAlphaTag()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getVoiceMailAlphaTagForALS(I)Ljava/lang/String;
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 282
    const-string v0, "PHONE"

    #@2
    const-string v1, "getVoiceMailAlphaTagForALS is not support!!"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 283
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneSubInfo;->getVoiceMailAlphaTag()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public getVoiceMailNumber()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 119
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.READ_PHONE_STATE"

    #@4
    const-string v3, "Requires READ_PHONE_STATE"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 120
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getVoiceMailNumber()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    .line 121
    .local v0, number:Ljava/lang/String;
    const-string v1, "PHONE"

    #@15
    const-string v2, "VM: PhoneSubInfo.getVoiceMailNUmber: "

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 122
    return-object v0
.end method

.method public getVoiceMailNumberForALS(I)Ljava/lang/String;
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 272
    const-string v0, "PHONE"

    #@2
    const-string v1, "getVoiceMailNumberForALS is not support!!"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 273
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneSubInfo;->getVoiceMailNumber()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public hasIsim()Z
    .registers 4

    #@0
    .prologue
    .line 209
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_PRIVILEGED_PHONE_STATE"

    #@4
    const-string v2, "Requires READ_PRIVILEGED_PHONE_STATE"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 211
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->hasIsim()Z

    #@e
    move-result v0

    #@f
    return v0
.end method

.method public isGbaSupported()Z
    .registers 5

    #@0
    .prologue
    .line 215
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.READ_PRIVILEGED_PHONE_STATE"

    #@4
    const-string v3, "Requires READ_PRIVILEGED_PHONE_STATE"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 217
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneSubInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    #@b
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@e
    move-result-object v0

    #@f
    .line 218
    .local v0, isim:Lcom/android/internal/telephony/uicc/IsimRecords;
    if-eqz v0, :cond_16

    #@11
    .line 219
    invoke-interface {v0}, Lcom/android/internal/telephony/uicc/IsimRecords;->isGbaSupported()Z

    #@14
    move-result v1

    #@15
    .line 221
    :goto_15
    return v1

    #@16
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_15
.end method
