.class public Lcom/android/internal/telephony/DataCallState;
.super Ljava/lang/Object;
.source "DataCallState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/DataCallState$SetupResult;
    }
.end annotation


# instance fields
.field private final DBG:Z

.field private final LOG_TAG:Ljava/lang/String;

.field public active:I

.field public addresses:[Ljava/lang/String;

.field public cid:I

.field public dnses:[Ljava/lang/String;

.field public gateways:[Ljava/lang/String;

.field public ifname:Ljava/lang/String;

.field public status:I

.field public suggestedRetryTime:I

.field public type:Ljava/lang/String;

.field public version:I


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 37
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 38
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/telephony/DataCallState;->DBG:Z

    #@7
    .line 39
    const-string v0, "GSM"

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/DataCallState;->LOG_TAG:Ljava/lang/String;

    #@b
    .line 41
    iput v1, p0, Lcom/android/internal/telephony/DataCallState;->version:I

    #@d
    .line 42
    iput v1, p0, Lcom/android/internal/telephony/DataCallState;->status:I

    #@f
    .line 43
    iput v1, p0, Lcom/android/internal/telephony/DataCallState;->cid:I

    #@11
    .line 44
    iput v1, p0, Lcom/android/internal/telephony/DataCallState;->active:I

    #@13
    .line 45
    const-string v0, ""

    #@15
    iput-object v0, p0, Lcom/android/internal/telephony/DataCallState;->type:Ljava/lang/String;

    #@17
    .line 46
    const-string v0, ""

    #@19
    iput-object v0, p0, Lcom/android/internal/telephony/DataCallState;->ifname:Ljava/lang/String;

    #@1b
    .line 47
    new-array v0, v1, [Ljava/lang/String;

    #@1d
    iput-object v0, p0, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@1f
    .line 48
    new-array v0, v1, [Ljava/lang/String;

    #@21
    iput-object v0, p0, Lcom/android/internal/telephony/DataCallState;->dnses:[Ljava/lang/String;

    #@23
    .line 49
    new-array v0, v1, [Ljava/lang/String;

    #@25
    iput-object v0, p0, Lcom/android/internal/telephony/DataCallState;->gateways:[Ljava/lang/String;

    #@27
    .line 50
    const/4 v0, -0x1

    #@28
    iput v0, p0, Lcom/android/internal/telephony/DataCallState;->suggestedRetryTime:I

    #@2a
    .line 55
    return-void
.end method


# virtual methods
.method public setLinkProperties(Landroid/net/LinkProperties;Z)Lcom/android/internal/telephony/DataCallState$SetupResult;
    .registers 21
    .parameter "linkProperties"
    .parameter "okToUseSystemPropertyDns"

    #@0
    .prologue
    .line 114
    if-nez p1, :cond_61

    #@2
    .line 115
    new-instance p1, Landroid/net/LinkProperties;

    #@4
    .end local p1
    invoke-direct/range {p1 .. p1}, Landroid/net/LinkProperties;-><init>()V

    #@7
    .line 119
    .restart local p1
    :goto_7
    move-object/from16 v0, p0

    #@9
    iget v15, v0, Lcom/android/internal/telephony/DataCallState;->status:I

    #@b
    sget-object v16, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@d
    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/telephony/DataConnection$FailCause;->getErrorCode()I

    #@10
    move-result v16

    #@11
    move/from16 v0, v16

    #@13
    if-ne v15, v0, :cond_2ac

    #@15
    .line 120
    new-instance v15, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v16, "net."

    #@1c
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v15

    #@20
    move-object/from16 v0, p0

    #@22
    iget-object v0, v0, Lcom/android/internal/telephony/DataCallState;->ifname:Ljava/lang/String;

    #@24
    move-object/from16 v16, v0

    #@26
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v15

    #@2a
    const-string v16, "."

    #@2c
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v15

    #@30
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v12

    #@34
    .line 124
    .local v12, propertyPrefix:Ljava/lang/String;
    :try_start_34
    move-object/from16 v0, p0

    #@36
    iget-object v15, v0, Lcom/android/internal/telephony/DataCallState;->ifname:Ljava/lang/String;

    #@38
    move-object/from16 v0, p1

    #@3a
    invoke-virtual {v0, v15}, Landroid/net/LinkProperties;->setInterfaceName(Ljava/lang/String;)V

    #@3d
    .line 127
    move-object/from16 v0, p0

    #@3f
    iget-object v15, v0, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@41
    if-eqz v15, :cond_134

    #@43
    move-object/from16 v0, p0

    #@45
    iget-object v15, v0, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@47
    array-length v15, v15

    #@48
    if-lez v15, :cond_134

    #@4a
    .line 128
    move-object/from16 v0, p0

    #@4c
    iget-object v4, v0, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@4e
    .local v4, arr$:[Ljava/lang/String;
    array-length v11, v4

    #@4f
    .local v11, len$:I
    const/4 v8, 0x0

    #@50
    .local v8, i$:I
    :goto_50
    if-ge v8, v11, :cond_153

    #@52
    aget-object v1, v4, v8

    #@54
    .line 129
    .local v1, addr:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@57
    move-result-object v1

    #@58
    .line 130
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
    :try_end_5b
    .catch Ljava/net/UnknownHostException; {:try_start_34 .. :try_end_5b} :catch_bf

    #@5b
    move-result v15

    #@5c
    if-eqz v15, :cond_65

    #@5e
    .line 128
    :cond_5e
    :goto_5e
    add-int/lit8 v8, v8, 0x1

    #@60
    goto :goto_50

    #@61
    .line 117
    .end local v1           #addr:Ljava/lang/String;
    .end local v4           #arr$:[Ljava/lang/String;
    .end local v8           #i$:I
    .end local v11           #len$:I
    .end local v12           #propertyPrefix:Ljava/lang/String;
    :cond_61
    invoke-virtual/range {p1 .. p1}, Landroid/net/LinkProperties;->clear()V

    #@64
    goto :goto_7

    #@65
    .line 134
    .restart local v1       #addr:Ljava/lang/String;
    .restart local v4       #arr$:[Ljava/lang/String;
    .restart local v8       #i$:I
    .restart local v11       #len$:I
    .restart local v12       #propertyPrefix:Ljava/lang/String;
    :cond_65
    :try_start_65
    const-string v15, "/"

    #@67
    invoke-virtual {v1, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@6a
    move-result-object v3

    #@6b
    .line 135
    .local v3, ap:[Ljava/lang/String;
    array-length v15, v3

    #@6c
    const/16 v16, 0x2

    #@6e
    move/from16 v0, v16

    #@70
    if-ne v15, v0, :cond_111

    #@72
    .line 136
    const/4 v15, 0x0

    #@73
    aget-object v1, v3, v15

    #@75
    .line 137
    const/4 v15, 0x1

    #@76
    aget-object v15, v3, v15

    #@78
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_7b
    .catch Ljava/net/UnknownHostException; {:try_start_65 .. :try_end_7b} :catch_bf

    #@7b
    move-result v2

    #@7c
    .line 143
    .local v2, addrPrefixLen:I
    :goto_7c
    :try_start_7c
    invoke-static {v1}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_7f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7c .. :try_end_7f} :catch_114
    .catch Ljava/net/UnknownHostException; {:try_start_7c .. :try_end_7f} :catch_bf

    #@7f
    move-result-object v9

    #@80
    .line 147
    .local v9, ia:Ljava/net/InetAddress;
    :try_start_80
    invoke-virtual {v9}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    #@83
    move-result v15

    #@84
    if-nez v15, :cond_5e

    #@86
    .line 148
    if-nez v2, :cond_8e

    #@88
    .line 150
    instance-of v15, v9, Ljava/net/Inet4Address;

    #@8a
    if-eqz v15, :cond_130

    #@8c
    const/16 v2, 0x20

    #@8e
    .line 152
    :cond_8e
    :goto_8e
    const-string v15, "GSM"

    #@90
    new-instance v16, Ljava/lang/StringBuilder;

    #@92
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v17, "addr/pl="

    #@97
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v16

    #@9b
    move-object/from16 v0, v16

    #@9d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v16

    #@a1
    const-string v17, "/"

    #@a3
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v16

    #@a7
    move-object/from16 v0, v16

    #@a9
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v16

    #@ad
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v16

    #@b1
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    .line 153
    new-instance v10, Landroid/net/LinkAddress;

    #@b6
    invoke-direct {v10, v9, v2}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@b9
    .line 154
    .local v10, la:Landroid/net/LinkAddress;
    move-object/from16 v0, p1

    #@bb
    invoke-virtual {v0, v10}, Landroid/net/LinkProperties;->addLinkAddress(Landroid/net/LinkAddress;)V
    :try_end_be
    .catch Ljava/net/UnknownHostException; {:try_start_80 .. :try_end_be} :catch_bf

    #@be
    goto :goto_5e

    #@bf
    .line 221
    .end local v1           #addr:Ljava/lang/String;
    .end local v2           #addrPrefixLen:I
    .end local v3           #ap:[Ljava/lang/String;
    .end local v4           #arr$:[Ljava/lang/String;
    .end local v8           #i$:I
    .end local v9           #ia:Ljava/net/InetAddress;
    .end local v10           #la:Landroid/net/LinkAddress;
    .end local v11           #len$:I
    :catch_bf
    move-exception v7

    #@c0
    .line 222
    .local v7, e:Ljava/net/UnknownHostException;
    const-string v15, "GSM"

    #@c2
    new-instance v16, Ljava/lang/StringBuilder;

    #@c4
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@c7
    const-string v17, "setLinkProperties: UnknownHostException "

    #@c9
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v16

    #@cd
    move-object/from16 v0, v16

    #@cf
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v16

    #@d3
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v16

    #@d7
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@da
    .line 223
    invoke-virtual {v7}, Ljava/net/UnknownHostException;->printStackTrace()V

    #@dd
    .line 224
    sget-object v13, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_UnacceptableParameter:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@df
    .line 235
    .end local v7           #e:Ljava/net/UnknownHostException;
    .end local v12           #propertyPrefix:Ljava/lang/String;
    .local v13, result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    :goto_df
    sget-object v15, Lcom/android/internal/telephony/DataCallState$SetupResult;->SUCCESS:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@e1
    if-eq v13, v15, :cond_110

    #@e3
    .line 237
    const-string v15, "GSM"

    #@e5
    new-instance v16, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v17, "setLinkProperties: error clearing LinkProperties status="

    #@ec
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v16

    #@f0
    move-object/from16 v0, p0

    #@f2
    iget v0, v0, Lcom/android/internal/telephony/DataCallState;->status:I

    #@f4
    move/from16 v17, v0

    #@f6
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v16

    #@fa
    const-string v17, " result="

    #@fc
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v16

    #@100
    move-object/from16 v0, v16

    #@102
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v16

    #@106
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v16

    #@10a
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10d
    .line 240
    invoke-virtual/range {p1 .. p1}, Landroid/net/LinkProperties;->clear()V

    #@110
    .line 243
    :cond_110
    return-object v13

    #@111
    .line 139
    .end local v13           #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    .restart local v1       #addr:Ljava/lang/String;
    .restart local v3       #ap:[Ljava/lang/String;
    .restart local v4       #arr$:[Ljava/lang/String;
    .restart local v8       #i$:I
    .restart local v11       #len$:I
    .restart local v12       #propertyPrefix:Ljava/lang/String;
    :cond_111
    const/4 v2, 0x0

    #@112
    .restart local v2       #addrPrefixLen:I
    goto/16 :goto_7c

    #@114
    .line 144
    :catch_114
    move-exception v7

    #@115
    .line 145
    .local v7, e:Ljava/lang/IllegalArgumentException;
    :try_start_115
    new-instance v15, Ljava/net/UnknownHostException;

    #@117
    new-instance v16, Ljava/lang/StringBuilder;

    #@119
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@11c
    const-string v17, "Non-numeric ip addr="

    #@11e
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v16

    #@122
    move-object/from16 v0, v16

    #@124
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@127
    move-result-object v16

    #@128
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12b
    move-result-object v16

    #@12c
    invoke-direct/range {v15 .. v16}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    #@12f
    throw v15

    #@130
    .line 150
    .end local v7           #e:Ljava/lang/IllegalArgumentException;
    .restart local v9       #ia:Ljava/net/InetAddress;
    :cond_130
    const/16 v2, 0x80

    #@132
    goto/16 :goto_8e

    #@134
    .line 158
    .end local v1           #addr:Ljava/lang/String;
    .end local v2           #addrPrefixLen:I
    .end local v3           #ap:[Ljava/lang/String;
    .end local v4           #arr$:[Ljava/lang/String;
    .end local v8           #i$:I
    .end local v9           #ia:Ljava/net/InetAddress;
    .end local v11           #len$:I
    :cond_134
    new-instance v15, Ljava/net/UnknownHostException;

    #@136
    new-instance v16, Ljava/lang/StringBuilder;

    #@138
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v17, "no address for ifname="

    #@13d
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v16

    #@141
    move-object/from16 v0, p0

    #@143
    iget-object v0, v0, Lcom/android/internal/telephony/DataCallState;->ifname:Ljava/lang/String;

    #@145
    move-object/from16 v17, v0

    #@147
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v16

    #@14b
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14e
    move-result-object v16

    #@14f
    invoke-direct/range {v15 .. v16}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    #@152
    throw v15

    #@153
    .line 162
    .restart local v4       #arr$:[Ljava/lang/String;
    .restart local v8       #i$:I
    .restart local v11       #len$:I
    :cond_153
    move-object/from16 v0, p0

    #@155
    iget-object v15, v0, Lcom/android/internal/telephony/DataCallState;->dnses:[Ljava/lang/String;

    #@157
    if-eqz v15, :cond_1a3

    #@159
    move-object/from16 v0, p0

    #@15b
    iget-object v15, v0, Lcom/android/internal/telephony/DataCallState;->dnses:[Ljava/lang/String;

    #@15d
    array-length v15, v15

    #@15e
    if-lez v15, :cond_1a3

    #@160
    .line 163
    move-object/from16 v0, p0

    #@162
    iget-object v4, v0, Lcom/android/internal/telephony/DataCallState;->dnses:[Ljava/lang/String;

    #@164
    array-length v11, v4

    #@165
    const/4 v8, 0x0

    #@166
    :goto_166
    if-ge v8, v11, :cond_228

    #@168
    aget-object v1, v4, v8

    #@16a
    .line 164
    .restart local v1       #addr:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@16d
    move-result-object v1

    #@16e
    .line 165
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
    :try_end_171
    .catch Ljava/net/UnknownHostException; {:try_start_115 .. :try_end_171} :catch_bf

    #@171
    move-result v15

    #@172
    if-eqz v15, :cond_177

    #@174
    .line 163
    :cond_174
    :goto_174
    add-int/lit8 v8, v8, 0x1

    #@176
    goto :goto_166

    #@177
    .line 168
    :cond_177
    :try_start_177
    invoke-static {v1}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_17a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_177 .. :try_end_17a} :catch_187
    .catch Ljava/net/UnknownHostException; {:try_start_177 .. :try_end_17a} :catch_bf

    #@17a
    move-result-object v9

    #@17b
    .line 172
    .restart local v9       #ia:Ljava/net/InetAddress;
    :try_start_17b
    invoke-virtual {v9}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    #@17e
    move-result v15

    #@17f
    if-nez v15, :cond_174

    #@181
    .line 173
    move-object/from16 v0, p1

    #@183
    invoke-virtual {v0, v9}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@186
    goto :goto_174

    #@187
    .line 169
    .end local v9           #ia:Ljava/net/InetAddress;
    :catch_187
    move-exception v7

    #@188
    .line 170
    .restart local v7       #e:Ljava/lang/IllegalArgumentException;
    new-instance v15, Ljava/net/UnknownHostException;

    #@18a
    new-instance v16, Ljava/lang/StringBuilder;

    #@18c
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@18f
    const-string v17, "Non-numeric dns addr="

    #@191
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v16

    #@195
    move-object/from16 v0, v16

    #@197
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v16

    #@19b
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19e
    move-result-object v16

    #@19f
    invoke-direct/range {v15 .. v16}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    #@1a2
    throw v15

    #@1a3
    .line 176
    .end local v1           #addr:Ljava/lang/String;
    .end local v7           #e:Ljava/lang/IllegalArgumentException;
    :cond_1a3
    if-eqz p2, :cond_220

    #@1a5
    .line 177
    const/4 v15, 0x2

    #@1a6
    new-array v6, v15, [Ljava/lang/String;

    #@1a8
    .line 178
    .local v6, dnsServers:[Ljava/lang/String;
    const/4 v15, 0x0

    #@1a9
    new-instance v16, Ljava/lang/StringBuilder;

    #@1ab
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@1ae
    move-object/from16 v0, v16

    #@1b0
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b3
    move-result-object v16

    #@1b4
    const-string v17, "dns1"

    #@1b6
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v16

    #@1ba
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bd
    move-result-object v16

    #@1be
    invoke-static/range {v16 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1c1
    move-result-object v16

    #@1c2
    aput-object v16, v6, v15

    #@1c4
    .line 179
    const/4 v15, 0x1

    #@1c5
    new-instance v16, Ljava/lang/StringBuilder;

    #@1c7
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@1ca
    move-object/from16 v0, v16

    #@1cc
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cf
    move-result-object v16

    #@1d0
    const-string v17, "dns2"

    #@1d2
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v16

    #@1d6
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d9
    move-result-object v16

    #@1da
    invoke-static/range {v16 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1dd
    move-result-object v16

    #@1de
    aput-object v16, v6, v15

    #@1e0
    .line 180
    move-object v4, v6

    #@1e1
    array-length v11, v4

    #@1e2
    const/4 v8, 0x0

    #@1e3
    :goto_1e3
    if-ge v8, v11, :cond_228

    #@1e5
    aget-object v5, v4, v8

    #@1e7
    .line 181
    .local v5, dnsAddr:Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@1ea
    move-result-object v5

    #@1eb
    .line 182
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z
    :try_end_1ee
    .catch Ljava/net/UnknownHostException; {:try_start_17b .. :try_end_1ee} :catch_bf

    #@1ee
    move-result v15

    #@1ef
    if-eqz v15, :cond_1f4

    #@1f1
    .line 180
    :cond_1f1
    :goto_1f1
    add-int/lit8 v8, v8, 0x1

    #@1f3
    goto :goto_1e3

    #@1f4
    .line 185
    :cond_1f4
    :try_start_1f4
    invoke-static {v5}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_1f7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1f4 .. :try_end_1f7} :catch_204
    .catch Ljava/net/UnknownHostException; {:try_start_1f4 .. :try_end_1f7} :catch_bf

    #@1f7
    move-result-object v9

    #@1f8
    .line 189
    .restart local v9       #ia:Ljava/net/InetAddress;
    :try_start_1f8
    invoke-virtual {v9}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    #@1fb
    move-result v15

    #@1fc
    if-nez v15, :cond_1f1

    #@1fe
    .line 190
    move-object/from16 v0, p1

    #@200
    invoke-virtual {v0, v9}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@203
    goto :goto_1f1

    #@204
    .line 186
    .end local v9           #ia:Ljava/net/InetAddress;
    :catch_204
    move-exception v7

    #@205
    .line 187
    .restart local v7       #e:Ljava/lang/IllegalArgumentException;
    new-instance v15, Ljava/net/UnknownHostException;

    #@207
    new-instance v16, Ljava/lang/StringBuilder;

    #@209
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@20c
    const-string v17, "Non-numeric dns addr="

    #@20e
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@211
    move-result-object v16

    #@212
    move-object/from16 v0, v16

    #@214
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@217
    move-result-object v16

    #@218
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21b
    move-result-object v16

    #@21c
    invoke-direct/range {v15 .. v16}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    #@21f
    throw v15

    #@220
    .line 194
    .end local v5           #dnsAddr:Ljava/lang/String;
    .end local v6           #dnsServers:[Ljava/lang/String;
    .end local v7           #e:Ljava/lang/IllegalArgumentException;
    :cond_220
    new-instance v15, Ljava/net/UnknownHostException;

    #@222
    const-string v16, "Empty dns response and no system default dns"

    #@224
    invoke-direct/range {v15 .. v16}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    #@227
    throw v15

    #@228
    .line 198
    :cond_228
    move-object/from16 v0, p0

    #@22a
    iget-object v15, v0, Lcom/android/internal/telephony/DataCallState;->gateways:[Ljava/lang/String;

    #@22c
    if-eqz v15, :cond_235

    #@22e
    move-object/from16 v0, p0

    #@230
    iget-object v15, v0, Lcom/android/internal/telephony/DataCallState;->gateways:[Ljava/lang/String;

    #@232
    array-length v15, v15

    #@233
    if-nez v15, :cond_258

    #@235
    .line 199
    :cond_235
    new-instance v15, Ljava/lang/StringBuilder;

    #@237
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@23a
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23d
    move-result-object v15

    #@23e
    const-string v16, "gw"

    #@240
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@243
    move-result-object v15

    #@244
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@247
    move-result-object v15

    #@248
    invoke-static {v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@24b
    move-result-object v14

    #@24c
    .line 200
    .local v14, sysGateways:Ljava/lang/String;
    if-eqz v14, :cond_26f

    #@24e
    .line 201
    const-string v15, " "

    #@250
    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@253
    move-result-object v15

    #@254
    move-object/from16 v0, p0

    #@256
    iput-object v15, v0, Lcom/android/internal/telephony/DataCallState;->gateways:[Ljava/lang/String;

    #@258
    .line 206
    .end local v14           #sysGateways:Ljava/lang/String;
    :cond_258
    :goto_258
    move-object/from16 v0, p0

    #@25a
    iget-object v4, v0, Lcom/android/internal/telephony/DataCallState;->gateways:[Ljava/lang/String;

    #@25c
    array-length v11, v4

    #@25d
    const/4 v8, 0x0

    #@25e
    :goto_25e
    if-ge v8, v11, :cond_2a8

    #@260
    aget-object v1, v4, v8

    #@262
    .line 207
    .restart local v1       #addr:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@265
    move-result-object v1

    #@266
    .line 208
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    #@269
    move-result v15

    #@26a
    if-eqz v15, :cond_277

    #@26c
    .line 206
    :cond_26c
    :goto_26c
    add-int/lit8 v8, v8, 0x1

    #@26e
    goto :goto_25e

    #@26f
    .line 203
    .end local v1           #addr:Ljava/lang/String;
    .restart local v14       #sysGateways:Ljava/lang/String;
    :cond_26f
    const/4 v15, 0x0

    #@270
    new-array v15, v15, [Ljava/lang/String;

    #@272
    move-object/from16 v0, p0

    #@274
    iput-object v15, v0, Lcom/android/internal/telephony/DataCallState;->gateways:[Ljava/lang/String;
    :try_end_276
    .catch Ljava/net/UnknownHostException; {:try_start_1f8 .. :try_end_276} :catch_bf

    #@276
    goto :goto_258

    #@277
    .line 211
    .end local v14           #sysGateways:Ljava/lang/String;
    .restart local v1       #addr:Ljava/lang/String;
    :cond_277
    :try_start_277
    invoke-static {v1}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_27a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_277 .. :try_end_27a} :catch_28c
    .catch Ljava/net/UnknownHostException; {:try_start_277 .. :try_end_27a} :catch_bf

    #@27a
    move-result-object v9

    #@27b
    .line 215
    .restart local v9       #ia:Ljava/net/InetAddress;
    :try_start_27b
    invoke-virtual {v9}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    #@27e
    move-result v15

    #@27f
    if-nez v15, :cond_26c

    #@281
    .line 216
    new-instance v15, Landroid/net/RouteInfo;

    #@283
    invoke-direct {v15, v9}, Landroid/net/RouteInfo;-><init>(Ljava/net/InetAddress;)V

    #@286
    move-object/from16 v0, p1

    #@288
    invoke-virtual {v0, v15}, Landroid/net/LinkProperties;->addRoute(Landroid/net/RouteInfo;)V

    #@28b
    goto :goto_26c

    #@28c
    .line 212
    .end local v9           #ia:Ljava/net/InetAddress;
    :catch_28c
    move-exception v7

    #@28d
    .line 213
    .restart local v7       #e:Ljava/lang/IllegalArgumentException;
    new-instance v15, Ljava/net/UnknownHostException;

    #@28f
    new-instance v16, Ljava/lang/StringBuilder;

    #@291
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@294
    const-string v17, "Non-numeric gateway addr="

    #@296
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@299
    move-result-object v16

    #@29a
    move-object/from16 v0, v16

    #@29c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29f
    move-result-object v16

    #@2a0
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a3
    move-result-object v16

    #@2a4
    invoke-direct/range {v15 .. v16}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    #@2a7
    throw v15

    #@2a8
    .line 220
    .end local v1           #addr:Ljava/lang/String;
    .end local v7           #e:Ljava/lang/IllegalArgumentException;
    :cond_2a8
    sget-object v13, Lcom/android/internal/telephony/DataCallState$SetupResult;->SUCCESS:Lcom/android/internal/telephony/DataCallState$SetupResult;
    :try_end_2aa
    .catch Ljava/net/UnknownHostException; {:try_start_27b .. :try_end_2aa} :catch_bf

    #@2aa
    .restart local v13       #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    goto/16 :goto_df

    #@2ac
    .line 227
    .end local v4           #arr$:[Ljava/lang/String;
    .end local v8           #i$:I
    .end local v11           #len$:I
    .end local v12           #propertyPrefix:Ljava/lang/String;
    .end local v13           #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    :cond_2ac
    move-object/from16 v0, p0

    #@2ae
    iget v15, v0, Lcom/android/internal/telephony/DataCallState;->version:I

    #@2b0
    const/16 v16, 0x4

    #@2b2
    move/from16 v0, v16

    #@2b4
    if-ge v15, v0, :cond_2ba

    #@2b6
    .line 228
    sget-object v13, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_GetLastErrorFromRil:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@2b8
    .restart local v13       #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    goto/16 :goto_df

    #@2ba
    .line 230
    .end local v13           #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    :cond_2ba
    sget-object v13, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_RilError:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@2bc
    .restart local v13       #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    goto/16 :goto_df
.end method

.method public toString()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 77
    new-instance v4, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 78
    .local v4, sb:Ljava/lang/StringBuffer;
    const-string v5, "DataCallState: {"

    #@7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a
    move-result-object v5

    #@b
    const-string v6, "version="

    #@d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@10
    move-result-object v5

    #@11
    iget v6, p0, Lcom/android/internal/telephony/DataCallState;->version:I

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@16
    move-result-object v5

    #@17
    const-string v6, " status="

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1c
    move-result-object v5

    #@1d
    iget v6, p0, Lcom/android/internal/telephony/DataCallState;->status:I

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@22
    move-result-object v5

    #@23
    const-string v6, " retry="

    #@25
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@28
    move-result-object v5

    #@29
    iget v6, p0, Lcom/android/internal/telephony/DataCallState;->suggestedRetryTime:I

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@2e
    move-result-object v5

    #@2f
    const-string v6, " cid="

    #@31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@34
    move-result-object v5

    #@35
    iget v6, p0, Lcom/android/internal/telephony/DataCallState;->cid:I

    #@37
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@3a
    move-result-object v5

    #@3b
    const-string v6, " active="

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@40
    move-result-object v5

    #@41
    iget v6, p0, Lcom/android/internal/telephony/DataCallState;->active:I

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@46
    move-result-object v5

    #@47
    const-string v6, " type="

    #@49
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@4c
    move-result-object v5

    #@4d
    iget-object v6, p0, Lcom/android/internal/telephony/DataCallState;->type:Ljava/lang/String;

    #@4f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@52
    move-result-object v5

    #@53
    const-string v6, "\' ifname=\'"

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@58
    move-result-object v5

    #@59
    iget-object v6, p0, Lcom/android/internal/telephony/DataCallState;->ifname:Ljava/lang/String;

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@5e
    .line 86
    const-string v5, "\' addresses=["

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@63
    .line 87
    iget-object v1, p0, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@65
    .local v1, arr$:[Ljava/lang/String;
    array-length v3, v1

    #@66
    .local v3, len$:I
    const/4 v2, 0x0

    #@67
    .local v2, i$:I
    :goto_67
    if-ge v2, v3, :cond_76

    #@69
    aget-object v0, v1, v2

    #@6b
    .line 88
    .local v0, addr:Ljava/lang/String;
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@6e
    .line 89
    const-string v5, ","

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@73
    .line 87
    add-int/lit8 v2, v2, 0x1

    #@75
    goto :goto_67

    #@76
    .line 91
    .end local v0           #addr:Ljava/lang/String;
    :cond_76
    iget-object v5, p0, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@78
    array-length v5, v5

    #@79
    if-lez v5, :cond_84

    #@7b
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    #@7e
    move-result v5

    #@7f
    add-int/lit8 v5, v5, -0x1

    #@81
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    #@84
    .line 92
    :cond_84
    const-string v5, "] dnses=["

    #@86
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@89
    .line 93
    iget-object v1, p0, Lcom/android/internal/telephony/DataCallState;->dnses:[Ljava/lang/String;

    #@8b
    array-length v3, v1

    #@8c
    const/4 v2, 0x0

    #@8d
    :goto_8d
    if-ge v2, v3, :cond_9c

    #@8f
    aget-object v0, v1, v2

    #@91
    .line 94
    .restart local v0       #addr:Ljava/lang/String;
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@94
    .line 95
    const-string v5, ","

    #@96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@99
    .line 93
    add-int/lit8 v2, v2, 0x1

    #@9b
    goto :goto_8d

    #@9c
    .line 97
    .end local v0           #addr:Ljava/lang/String;
    :cond_9c
    iget-object v5, p0, Lcom/android/internal/telephony/DataCallState;->dnses:[Ljava/lang/String;

    #@9e
    array-length v5, v5

    #@9f
    if-lez v5, :cond_aa

    #@a1
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    #@a4
    move-result v5

    #@a5
    add-int/lit8 v5, v5, -0x1

    #@a7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    #@aa
    .line 98
    :cond_aa
    const-string v5, "] gateways=["

    #@ac
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@af
    .line 99
    iget-object v1, p0, Lcom/android/internal/telephony/DataCallState;->gateways:[Ljava/lang/String;

    #@b1
    array-length v3, v1

    #@b2
    const/4 v2, 0x0

    #@b3
    :goto_b3
    if-ge v2, v3, :cond_c2

    #@b5
    aget-object v0, v1, v2

    #@b7
    .line 100
    .restart local v0       #addr:Ljava/lang/String;
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@ba
    .line 101
    const-string v5, ","

    #@bc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@bf
    .line 99
    add-int/lit8 v2, v2, 0x1

    #@c1
    goto :goto_b3

    #@c2
    .line 103
    .end local v0           #addr:Ljava/lang/String;
    :cond_c2
    iget-object v5, p0, Lcom/android/internal/telephony/DataCallState;->gateways:[Ljava/lang/String;

    #@c4
    array-length v5, v5

    #@c5
    if-lez v5, :cond_d0

    #@c7
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    #@ca
    move-result v5

    #@cb
    add-int/lit8 v5, v5, -0x1

    #@cd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    #@d0
    .line 104
    :cond_d0
    const-string v5, "]}"

    #@d2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@d5
    .line 105
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@d8
    move-result-object v5

    #@d9
    return-object v5
.end method
