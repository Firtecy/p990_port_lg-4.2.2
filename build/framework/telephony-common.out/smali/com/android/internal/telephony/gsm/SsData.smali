.class public Lcom/android/internal/telephony/gsm/SsData;
.super Ljava/lang/Object;
.source "SsData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;,
        Lcom/android/internal/telephony/gsm/SsData$RequestType;,
        Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    }
.end annotation


# instance fields
.field public cfInfo:[Lcom/android/internal/telephony/CallForwardInfo;

.field public requestType:Lcom/android/internal/telephony/gsm/SsData$RequestType;

.field public result:I

.field public serviceClass:I

.field public serviceType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

.field public ssInfo:[I

.field public teleserviceType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public RequestTypeFromRILInt(I)Lcom/android/internal/telephony/gsm/SsData$RequestType;
    .registers 6
    .parameter "type"

    #@0
    .prologue
    .line 152
    packed-switch p1, :pswitch_data_2c

    #@3
    .line 159
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Unrecognized SS RequestType "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 153
    :pswitch_1c
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_ACTIVATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@1e
    .line 163
    .local v0, newType:Lcom/android/internal/telephony/gsm/SsData$RequestType;
    :goto_1e
    return-object v0

    #@1f
    .line 154
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$RequestType;
    :pswitch_1f
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_DEACTIVATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@21
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$RequestType;
    goto :goto_1e

    #@22
    .line 155
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$RequestType;
    :pswitch_22
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_INTERROGATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@24
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$RequestType;
    goto :goto_1e

    #@25
    .line 156
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$RequestType;
    :pswitch_25
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_REGISTRATION:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@27
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$RequestType;
    goto :goto_1e

    #@28
    .line 157
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$RequestType;
    :pswitch_28
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$RequestType;->SS_ERASURE:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@2a
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$RequestType;
    goto :goto_1e

    #@2b
    .line 152
    nop

    #@2c
    :pswitch_data_2c
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
        :pswitch_28
    .end packed-switch
.end method

.method public ServiceTypeFromRILInt(I)Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    .registers 6
    .parameter "type"

    #@0
    .prologue
    .line 121
    packed-switch p1, :pswitch_data_56

    #@3
    .line 142
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Unrecognized SS ServiceType "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 122
    :pswitch_1c
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CFU:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@1e
    .line 146
    .local v0, newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :goto_1e
    return-object v0

    #@1f
    .line 123
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_1f
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CF_BUSY:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@21
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@22
    .line 124
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_22
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CF_NO_REPLY:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@24
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@25
    .line 125
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_25
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CF_NOT_REACHABLE:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@27
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@28
    .line 126
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_28
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CF_ALL:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@2a
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@2b
    .line 127
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_2b
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CF_ALL_CONDITIONAL:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@2d
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@2e
    .line 128
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_2e
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CLIP:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@30
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@31
    .line 129
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_31
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_CLIR:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@33
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@34
    .line 130
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_34
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_COLP:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@36
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@37
    .line 131
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_37
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_COLR:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@39
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@3a
    .line 132
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_3a
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_WAIT:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@3c
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@3d
    .line 133
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_3d
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_BAOC:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@3f
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@40
    .line 134
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_40
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_BAOIC:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@42
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@43
    .line 135
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_43
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_BAOIC_EXC_HOME:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@45
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@46
    .line 136
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_46
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_BAIC:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@48
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@49
    .line 137
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_49
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_BAIC_ROAMING:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@4b
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@4c
    .line 138
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_4c
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_ALL_BARRING:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@4e
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@4f
    .line 139
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_4f
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_OUTGOING_BARRING:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@51
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@52
    .line 140
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    :pswitch_52
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->SS_INCOMING_BARRING:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@54
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;
    goto :goto_1e

    #@55
    .line 121
    nop

    #@56
    :pswitch_data_56
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_2b
        :pswitch_2e
        :pswitch_31
        :pswitch_34
        :pswitch_37
        :pswitch_3a
        :pswitch_3d
        :pswitch_40
        :pswitch_43
        :pswitch_46
        :pswitch_49
        :pswitch_4c
        :pswitch_4f
        :pswitch_52
    .end packed-switch
.end method

.method public TeleserviceTypeFromRILInt(I)Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    .registers 6
    .parameter "type"

    #@0
    .prologue
    .line 169
    packed-switch p1, :pswitch_data_2e

    #@3
    .line 177
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Unrecognized SS TeleserviceType "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 170
    :pswitch_1c
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;->SS_ALL_TELE_AND_BEARER_SERVICES:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;

    #@1e
    .line 181
    .local v0, newType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    :goto_1e
    return-object v0

    #@1f
    .line 171
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    :pswitch_1f
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;->SS_ALL_TELESEVICES:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;

    #@21
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    goto :goto_1e

    #@22
    .line 172
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    :pswitch_22
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;->SS_TELEPHONY:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;

    #@24
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    goto :goto_1e

    #@25
    .line 173
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    :pswitch_25
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;->SS_ALL_DATA_TELESERVICES:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;

    #@27
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    goto :goto_1e

    #@28
    .line 174
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    :pswitch_28
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;->SS_SMS_SERVICES:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;

    #@2a
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    goto :goto_1e

    #@2b
    .line 175
    .end local v0           #newType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    :pswitch_2b
    sget-object v0, Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;->SS_ALL_TELESERVICES_EXCEPT_SMS:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;

    #@2d
    .restart local v0       #newType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;
    goto :goto_1e

    #@2e
    .line 169
    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_2b
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[SsData] ServiceType: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SsData;->serviceType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " RequestType: "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SsData;->requestType:Lcom/android/internal/telephony/gsm/SsData$RequestType;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " TeleserviceType: "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SsData;->teleserviceType:Lcom/android/internal/telephony/gsm/SsData$TeleserviceType;

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, " ServiceClass: "

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Lcom/android/internal/telephony/gsm/SsData;->serviceClass:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, " Result: "

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Lcom/android/internal/telephony/gsm/SsData;->result:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, " Is Service Type CF: "

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SsData;->serviceType:Lcom/android/internal/telephony/gsm/SsData$ServiceType;

    #@49
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SsData$ServiceType;->isTypeCF()Z

    #@4c
    move-result v1

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    return-object v0
.end method
