.class public Lcom/android/internal/telephony/CommandException;
.super Ljava/lang/RuntimeException;
.source "CommandException.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/CommandException$Error;
    }
.end annotation


# instance fields
.field private e:Lcom/android/internal/telephony/CommandException$Error;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/CommandException$Error;)V
    .registers 3
    .parameter "e"

    #@0
    .prologue
    .line 71
    invoke-virtual {p1}, Lcom/android/internal/telephony/CommandException$Error;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    .line 72
    iput-object p1, p0, Lcom/android/internal/telephony/CommandException;->e:Lcom/android/internal/telephony/CommandException$Error;

    #@9
    .line 73
    return-void
.end method

.method public static fromLgeRilErrno(I)Lcom/android/internal/telephony/CommandException;
    .registers 3
    .parameter "ril_errno"

    #@0
    .prologue
    .line 151
    packed-switch p0, :pswitch_data_1e

    #@3
    .line 159
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 153
    :pswitch_5
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@7
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->MISSING_RESOURCE:Lcom/android/internal/telephony/CommandException$Error;

    #@9
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@c
    goto :goto_4

    #@d
    .line 155
    :pswitch_d
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@f
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->NO_SUCH_ELEMENT:Lcom/android/internal/telephony/CommandException$Error;

    #@11
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@14
    goto :goto_4

    #@15
    .line 157
    :pswitch_15
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@17
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->INVALID_PARAMETER:Lcom/android/internal/telephony/CommandException$Error;

    #@19
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@1c
    goto :goto_4

    #@1d
    .line 151
    nop

    #@1e
    :pswitch_data_1e
    .packed-switch 0x1b
        :pswitch_5
        :pswitch_d
        :pswitch_15
    .end packed-switch
.end method

.method public static fromRilErrno(I)Lcom/android/internal/telephony/CommandException;
    .registers 5
    .parameter "ril_errno"

    #@0
    .prologue
    .line 77
    packed-switch p0, :pswitch_data_108

    #@3
    .line 136
    :pswitch_3
    invoke-static {p0}, Lcom/android/internal/telephony/CommandException;->fromLgeRilErrno(I)Lcom/android/internal/telephony/CommandException;

    #@6
    move-result-object v0

    #@7
    .line 137
    .local v0, e:Lcom/android/internal/telephony/CommandException;
    if-eqz v0, :cond_e7

    #@9
    .line 140
    .end local v0           #e:Lcom/android/internal/telephony/CommandException;
    :goto_9
    return-object v0

    #@a
    .line 78
    :pswitch_a
    const/4 v0, 0x0

    #@b
    goto :goto_9

    #@c
    .line 80
    :pswitch_c
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@e
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->INVALID_RESPONSE:Lcom/android/internal/telephony/CommandException$Error;

    #@10
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@13
    goto :goto_9

    #@14
    .line 82
    :pswitch_14
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@16
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    #@18
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@1b
    goto :goto_9

    #@1c
    .line 84
    :pswitch_1c
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@1e
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->GENERIC_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@20
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@23
    goto :goto_9

    #@24
    .line 86
    :pswitch_24
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@26
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@28
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@2b
    goto :goto_9

    #@2c
    .line 88
    :pswitch_2c
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@2e
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SIM_PIN2:Lcom/android/internal/telephony/CommandException$Error;

    #@30
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@33
    goto :goto_9

    #@34
    .line 90
    :pswitch_34
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@36
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SIM_PUK2:Lcom/android/internal/telephony/CommandException$Error;

    #@38
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@3b
    goto :goto_9

    #@3c
    .line 92
    :pswitch_3c
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@3e
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->REQUEST_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@40
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@43
    goto :goto_9

    #@44
    .line 94
    :pswitch_44
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@46
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->OP_NOT_ALLOWED_DURING_VOICE_CALL:Lcom/android/internal/telephony/CommandException$Error;

    #@48
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@4b
    goto :goto_9

    #@4c
    .line 96
    :pswitch_4c
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@4e
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->OP_NOT_ALLOWED_BEFORE_REG_NW:Lcom/android/internal/telephony/CommandException$Error;

    #@50
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@53
    goto :goto_9

    #@54
    .line 98
    :pswitch_54
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@56
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SMS_FAIL_RETRY:Lcom/android/internal/telephony/CommandException$Error;

    #@58
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@5b
    goto :goto_9

    #@5c
    .line 100
    :pswitch_5c
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@5e
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SIM_ABSENT:Lcom/android/internal/telephony/CommandException$Error;

    #@60
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@63
    goto :goto_9

    #@64
    .line 102
    :pswitch_64
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@66
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SUBSCRIPTION_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    #@68
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@6b
    goto :goto_9

    #@6c
    .line 104
    :pswitch_6c
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@6e
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->MODE_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@70
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@73
    goto :goto_9

    #@74
    .line 106
    :pswitch_74
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@76
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@78
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@7b
    goto :goto_9

    #@7c
    .line 108
    :pswitch_7c
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@7e
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->ILLEGAL_SIM_OR_ME:Lcom/android/internal/telephony/CommandException$Error;

    #@80
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@83
    goto :goto_9

    #@84
    .line 110
    :pswitch_84
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@86
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->DIAL_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

    #@88
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@8b
    goto/16 :goto_9

    #@8d
    .line 112
    :pswitch_8d
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@8f
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->DIAL_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

    #@91
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@94
    goto/16 :goto_9

    #@96
    .line 114
    :pswitch_96
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@98
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->DIAL_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@9a
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@9d
    goto/16 :goto_9

    #@9f
    .line 116
    :pswitch_9f
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@a1
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@a3
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@a6
    goto/16 :goto_9

    #@a8
    .line 118
    :pswitch_a8
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@aa
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

    #@ac
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@af
    goto/16 :goto_9

    #@b1
    .line 120
    :pswitch_b1
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@b3
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

    #@b5
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@b8
    goto/16 :goto_9

    #@ba
    .line 122
    :pswitch_ba
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@bc
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@be
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@c1
    goto/16 :goto_9

    #@c3
    .line 124
    :pswitch_c3
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@c5
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

    #@c7
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@ca
    goto/16 :goto_9

    #@cc
    .line 126
    :pswitch_cc
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@ce
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

    #@d0
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@d3
    goto/16 :goto_9

    #@d5
    .line 128
    :pswitch_d5
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@d7
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SUBSCRIPTION_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@d9
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@dc
    goto/16 :goto_9

    #@de
    .line 131
    :pswitch_de
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@e0
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->NET_SEL_FAILED_BY_PLMN_NOT_ALLOWED:Lcom/android/internal/telephony/CommandException$Error;

    #@e2
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@e5
    goto/16 :goto_9

    #@e7
    .line 139
    .restart local v0       #e:Lcom/android/internal/telephony/CommandException;
    :cond_e7
    const-string v1, "GSM"

    #@e9
    new-instance v2, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    const-string v3, "Unrecognized RIL errno "

    #@f0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v2

    #@f4
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v2

    #@f8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v2

    #@fc
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ff
    .line 140
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@101
    .end local v0           #e:Lcom/android/internal/telephony/CommandException;
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->INVALID_RESPONSE:Lcom/android/internal/telephony/CommandException$Error;

    #@103
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@106
    goto/16 :goto_9

    #@108
    .line 77
    :pswitch_data_108
    .packed-switch -0x1
        :pswitch_c
        :pswitch_a
        :pswitch_14
        :pswitch_1c
        :pswitch_24
        :pswitch_2c
        :pswitch_34
        :pswitch_3c
        :pswitch_3
        :pswitch_44
        :pswitch_4c
        :pswitch_54
        :pswitch_5c
        :pswitch_64
        :pswitch_6c
        :pswitch_74
        :pswitch_7c
        :pswitch_3
        :pswitch_84
        :pswitch_8d
        :pswitch_96
        :pswitch_9f
        :pswitch_a8
        :pswitch_b1
        :pswitch_ba
        :pswitch_c3
        :pswitch_cc
        :pswitch_d5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_de
    .end packed-switch
.end method


# virtual methods
.method public getCommandError()Lcom/android/internal/telephony/CommandException$Error;
    .registers 2

    #@0
    .prologue
    .line 145
    iget-object v0, p0, Lcom/android/internal/telephony/CommandException;->e:Lcom/android/internal/telephony/CommandException$Error;

    #@2
    return-object v0
.end method
