.class public abstract Lcom/android/internal/telephony/uicc/IccServiceTable;
.super Ljava/lang/Object;
.source "IccServiceTable.java"


# instance fields
.field protected final mServiceTable:[B


# direct methods
.method protected constructor <init>([B)V
    .registers 2
    .parameter "table"

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 28
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IccServiceTable;->mServiceTable:[B

    #@5
    .line 29
    return-void
.end method


# virtual methods
.method protected abstract getTag()Ljava/lang/String;
.end method

.method protected abstract getValues()[Ljava/lang/Object;
.end method

.method protected isAvailable(I)Z
    .registers 8
    .parameter "service"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 43
    div-int/lit8 v1, p1, 0x8

    #@4
    .line 44
    .local v1, offset:I
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccServiceTable;->mServiceTable:[B

    #@6
    array-length v4, v4

    #@7
    if-lt v1, v4, :cond_35

    #@9
    .line 46
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccServiceTable;->getTag()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "isAvailable for service "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    add-int/lit8 v5, p1, 0x1

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    const-string v5, " fails, max service is "

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IccServiceTable;->mServiceTable:[B

    #@26
    array-length v5, v5

    #@27
    mul-int/lit8 v5, v5, 0x8

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 51
    :goto_34
    return v3

    #@35
    .line 50
    :cond_35
    rem-int/lit8 v0, p1, 0x8

    #@37
    .line 51
    .local v0, bit:I
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccServiceTable;->mServiceTable:[B

    #@39
    aget-byte v4, v4, v1

    #@3b
    shl-int v5, v2, v0

    #@3d
    and-int/2addr v4, v5

    #@3e
    if-eqz v4, :cond_42

    #@40
    :goto_40
    move v3, v2

    #@41
    goto :goto_34

    #@42
    :cond_42
    move v2, v3

    #@43
    goto :goto_40
.end method

.method public toString()Ljava/lang/String;
    .registers 11

    #@0
    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccServiceTable;->getValues()[Ljava/lang/Object;

    #@3
    move-result-object v7

    #@4
    .line 56
    .local v7, values:[Ljava/lang/Object;
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccServiceTable;->mServiceTable:[B

    #@6
    array-length v5, v8

    #@7
    .line 57
    .local v5, numBytes:I
    new-instance v8, Ljava/lang/StringBuilder;

    #@9
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccServiceTable;->getTag()Ljava/lang/String;

    #@c
    move-result-object v9

    #@d
    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@10
    const/16 v9, 0x5b

    #@12
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@15
    move-result-object v8

    #@16
    mul-int/lit8 v9, v5, 0x8

    #@18
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v8

    #@1c
    const-string v9, "]={ "

    #@1e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    .line 60
    .local v2, builder:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    #@23
    .line 61
    .local v0, addComma:Z
    const/4 v4, 0x0

    #@24
    .local v4, i:I
    :goto_24
    if-ge v4, v5, :cond_5b

    #@26
    .line 62
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccServiceTable;->mServiceTable:[B

    #@28
    aget-byte v3, v8, v4

    #@2a
    .line 63
    .local v3, currentByte:B
    const/4 v1, 0x0

    #@2b
    .local v1, bit:I
    :goto_2b
    const/16 v8, 0x8

    #@2d
    if-ge v1, v8, :cond_58

    #@2f
    .line 64
    const/4 v8, 0x1

    #@30
    shl-int/2addr v8, v1

    #@31
    and-int/2addr v8, v3

    #@32
    if-eqz v8, :cond_47

    #@34
    .line 65
    if-eqz v0, :cond_4a

    #@36
    .line 66
    const-string v8, ", "

    #@38
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    .line 70
    :goto_3b
    mul-int/lit8 v8, v4, 0x8

    #@3d
    add-int v6, v8, v1

    #@3f
    .line 71
    .local v6, ordinal:I
    array-length v8, v7

    #@40
    if-ge v6, v8, :cond_4c

    #@42
    .line 72
    aget-object v8, v7, v6

    #@44
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    .line 63
    .end local v6           #ordinal:I
    :cond_47
    :goto_47
    add-int/lit8 v1, v1, 0x1

    #@49
    goto :goto_2b

    #@4a
    .line 68
    :cond_4a
    const/4 v0, 0x1

    #@4b
    goto :goto_3b

    #@4c
    .line 74
    .restart local v6       #ordinal:I
    :cond_4c
    const/16 v8, 0x23

    #@4e
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@51
    move-result-object v8

    #@52
    add-int/lit8 v9, v6, 0x1

    #@54
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    goto :goto_47

    #@58
    .line 61
    .end local v6           #ordinal:I
    :cond_58
    add-int/lit8 v4, v4, 0x1

    #@5a
    goto :goto_24

    #@5b
    .line 79
    .end local v1           #bit:I
    .end local v3           #currentByte:B
    :cond_5b
    const-string v8, " }"

    #@5d
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v8

    #@61
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v8

    #@65
    return-object v8
.end method
