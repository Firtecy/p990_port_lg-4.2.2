.class public Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;
.super Ljava/lang/Object;
.source "CdmaSmsStateQeTest.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CDMA/SMS/SmsStateQeTest"

.field private static m_nPrl:I


# instance fields
.field private m_bIsTestInKorea:Z

.field private m_bKoreaDomesticPrl:Z

.field private m_bRoamingState:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 29
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_nPrl:I

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 31
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bIsTestInKorea:Z

    #@6
    .line 33
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bKoreaDomesticPrl:Z

    #@8
    .line 34
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bRoamingState:Z

    #@a
    .line 37
    const-string v0, "CDMA/SMS/SmsStateQeTest"

    #@c
    const-string v1, "CdmaSmsStateQeTest created"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 38
    return-void
.end method

.method private CheckPrl()Z
    .registers 5

    #@0
    .prologue
    .line 56
    const-string v1, "CDMA/SMS/SmsStateQeTest"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "CheckPrl() - m_nPrl : "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget v3, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_nPrl:I

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 58
    const/4 v0, 0x0

    #@1b
    .line 60
    .local v0, bRetValue:Z
    sget v1, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_nPrl:I

    #@1d
    const/16 v2, 0x2713

    #@1f
    if-eq v1, v2, :cond_27

    #@21
    sget v1, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_nPrl:I

    #@23
    const/16 v2, 0x2716

    #@25
    if-ne v1, v2, :cond_28

    #@27
    .line 61
    :cond_27
    const/4 v0, 0x1

    #@28
    .line 64
    :cond_28
    return v0
.end method

.method private UpdateState()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 41
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@4
    move-result-object v0

    #@5
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@8
    move-result v0

    #@9
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bRoamingState:Z

    #@b
    .line 42
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->CheckPrl()Z

    #@e
    move-result v0

    #@f
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bKoreaDomesticPrl:Z

    #@11
    .line 45
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bRoamingState:Z

    #@13
    if-nez v0, :cond_4e

    #@15
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bKoreaDomesticPrl:Z

    #@17
    if-ne v0, v1, :cond_4e

    #@19
    .line 46
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bIsTestInKorea:Z

    #@1b
    .line 51
    :goto_1b
    const-string v0, "CDMA/SMS/SmsStateQeTest"

    #@1d
    new-instance v1, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v2, "UpdateState() - isRoaming : "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bRoamingState:Z

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v2, ", isKoreaPrl : "

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bKoreaDomesticPrl:Z

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    const-string v2, ", KSC5601 Encoding : "

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bIsTestInKorea:Z

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v1

    #@4a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 53
    return-void

    #@4e
    .line 48
    :cond_4e
    const/4 v0, 0x0

    #@4f
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bIsTestInKorea:Z

    #@51
    goto :goto_1b
.end method


# virtual methods
.method public getPrl()I
    .registers 2

    #@0
    .prologue
    .line 78
    sget v0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_nPrl:I

    #@2
    return v0
.end method

.method public isTestInKorea()Z
    .registers 2

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->UpdateState()V

    #@3
    .line 84
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_bIsTestInKorea:Z

    #@5
    return v0
.end method

.method public setPrl(Ljava/lang/String;)V
    .registers 6
    .parameter "prl"

    #@0
    .prologue
    .line 68
    const-string v1, "CDMA/SMS/SmsStateQeTest"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "setPrl() - PRL : "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 71
    :try_start_18
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1b
    move-result v1

    #@1c
    sput v1, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->m_nPrl:I
    :try_end_1e
    .catch Ljava/lang/NumberFormatException; {:try_start_18 .. :try_end_1e} :catch_1f

    #@1e
    .line 75
    :goto_1e
    return-void

    #@1f
    .line 72
    :catch_1f
    move-exception v0

    #@20
    .line 73
    .local v0, ex:Ljava/lang/NumberFormatException;
    const-string v1, "CDMA/SMS/SmsStateQeTest"

    #@22
    const-string v2, "setPrl() - NumberFormatException"

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    goto :goto_1e
.end method
