.class final enum Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
.super Ljava/lang/Enum;
.source "DataProfileOmh.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cdma/DataProfileOmh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "DataProfileTypeModem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

.field public static final enum PROFILE_TYPE_LBS:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

.field public static final enum PROFILE_TYPE_MMS:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

.field public static final enum PROFILE_TYPE_TETHERED:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

.field public static final enum PROFILE_TYPE_UNSPECIFIED:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;


# instance fields
.field id:I

.field serviceType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    .line 62
    new-instance v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@6
    const-string v1, "PROFILE_TYPE_UNSPECIFIED"

    #@8
    const-string v2, "default"

    #@a
    invoke-direct {v0, v1, v6, v4, v2}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    #@d
    sput-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_UNSPECIFIED:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@f
    .line 63
    new-instance v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@11
    const-string v1, "PROFILE_TYPE_MMS"

    #@13
    const-string v2, "mms"

    #@15
    invoke-direct {v0, v1, v4, v5, v2}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    #@18
    sput-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_MMS:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@1a
    .line 64
    new-instance v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@1c
    const-string v1, "PROFILE_TYPE_LBS"

    #@1e
    const/16 v2, 0x20

    #@20
    const-string v3, "supl"

    #@22
    invoke-direct {v0, v1, v5, v2, v3}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    #@25
    sput-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_LBS:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@27
    .line 65
    new-instance v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@29
    const-string v1, "PROFILE_TYPE_TETHERED"

    #@2b
    const/16 v2, 0x40

    #@2d
    const-string v3, "dun"

    #@2f
    invoke-direct {v0, v1, v7, v2, v3}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    #@32
    sput-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_TETHERED:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@34
    .line 60
    const/4 v0, 0x4

    #@35
    new-array v0, v0, [Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@37
    sget-object v1, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_UNSPECIFIED:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@39
    aput-object v1, v0, v6

    #@3b
    sget-object v1, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_MMS:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@3d
    aput-object v1, v0, v4

    #@3f
    sget-object v1, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_LBS:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@41
    aput-object v1, v0, v5

    #@43
    sget-object v1, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_TETHERED:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@45
    aput-object v1, v0, v7

    #@47
    sput-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->$VALUES:[Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@49
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter "i"
    .parameter "serviceType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 71
    iput p3, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->id:I

    #@5
    .line 72
    iput-object p4, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->serviceType:Ljava/lang/String;

    #@7
    .line 73
    return-void
.end method

.method public static getDataProfileTypeModem(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    .registers 2
    .parameter "serviceType"

    #@0
    .prologue
    .line 85
    const-string v0, "default"

    #@2
    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 86
    sget-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_UNSPECIFIED:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@a
    .line 95
    :goto_a
    return-object v0

    #@b
    .line 87
    :cond_b
    const-string v0, "mms"

    #@d
    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_16

    #@13
    .line 88
    sget-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_MMS:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@15
    goto :goto_a

    #@16
    .line 89
    :cond_16
    const-string v0, "supl"

    #@18
    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 90
    sget-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_LBS:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@20
    goto :goto_a

    #@21
    .line 91
    :cond_21
    const-string v0, "dun"

    #@23
    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_2c

    #@29
    .line 92
    sget-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_TETHERED:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@2b
    goto :goto_a

    #@2c
    .line 95
    :cond_2c
    sget-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_UNSPECIFIED:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@2e
    goto :goto_a
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 60
    const-class v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    .registers 1

    #@0
    .prologue
    .line 60
    sget-object v0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->$VALUES:[Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@8
    return-object v0
.end method


# virtual methods
.method public getDataServiceType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->serviceType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getid()I
    .registers 2

    #@0
    .prologue
    .line 76
    iget v0, p0, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->id:I

    #@2
    return v0
.end method
