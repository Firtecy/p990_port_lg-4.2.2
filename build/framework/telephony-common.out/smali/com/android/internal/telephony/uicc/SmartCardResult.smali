.class public Lcom/android/internal/telephony/uicc/SmartCardResult;
.super Ljava/lang/Object;
.source "SmartCardResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/telephony/uicc/SmartCardResult;",
            ">;"
        }
    .end annotation
.end field

.field private static ENABLE_PRIVACY_LOG:Z = false

.field private static final TAG:Ljava/lang/String; = "SmartCardResult"


# instance fields
.field public data:[B

.field public data_length:I

.field public ret:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 18
    const-string v1, "persist.service.privacy.enable"

    #@2
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    const-string v2, "ATT"

    #@8
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_26

    #@e
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    const-string v2, "TMO"

    #@14
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_26

    #@1a
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    const-string v2, "VZW"

    #@20
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_41

    #@26
    :cond_26
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    const-string v2, "US"

    #@2c
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_41

    #@32
    const/4 v0, 0x0

    #@33
    :goto_33
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@36
    move-result v0

    #@37
    sput-boolean v0, Lcom/android/internal/telephony/uicc/SmartCardResult;->ENABLE_PRIVACY_LOG:Z

    #@39
    .line 20
    new-instance v0, Lcom/android/internal/telephony/uicc/SmartCardResult$1;

    #@3b
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/SmartCardResult$1;-><init>()V

    #@3e
    sput-object v0, Lcom/android/internal/telephony/uicc/SmartCardResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@40
    return-void

    #@41
    .line 18
    :cond_41
    const/4 v0, 0x1

    #@42
    goto :goto_33
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 2
    .parameter "source"

    #@0
    .prologue
    .line 37
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 38
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/SmartCardResult;->readFromParcel(Landroid/os/Parcel;)V

    #@6
    .line 39
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 79
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 6
    .parameter "source"

    #@0
    .prologue
    .line 54
    const/4 v0, 0x0

    #@1
    .line 55
    .local v0, i:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4
    move-result v1

    #@5
    iput v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->ret:I

    #@7
    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v1

    #@b
    iput v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@d
    .line 59
    sget-boolean v1, Lcom/android/internal/telephony/uicc/SmartCardResult;->ENABLE_PRIVACY_LOG:Z

    #@f
    if-eqz v1, :cond_2b

    #@11
    .line 61
    const-string v1, "SmartCardResult"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "[readFromParcel]data_length  : "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    iget v3, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 64
    :cond_2b
    iget v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@2d
    new-array v1, v1, [B

    #@2f
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data:[B

    #@31
    .line 65
    iget v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->ret:I

    #@33
    if-nez v1, :cond_74

    #@35
    .line 66
    const/4 v0, 0x0

    #@36
    :goto_36
    iget v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@38
    if-ge v0, v1, :cond_48

    #@3a
    .line 67
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data:[B

    #@3c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3f
    move-result v2

    #@40
    and-int/lit16 v2, v2, 0xff

    #@42
    int-to-byte v2, v2

    #@43
    aput-byte v2, v1, v0

    #@45
    .line 66
    add-int/lit8 v0, v0, 0x1

    #@47
    goto :goto_36

    #@48
    .line 71
    :cond_48
    sget-boolean v1, Lcom/android/internal/telephony/uicc/SmartCardResult;->ENABLE_PRIVACY_LOG:Z

    #@4a
    if-eqz v1, :cond_74

    #@4c
    .line 73
    const-string v1, "SmartCardResult"

    #@4e
    new-instance v2, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v3, "[readFromParcel]data "

    #@55
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    const-string v3, " : "

    #@5f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v2

    #@63
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data:[B

    #@65
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@68
    move-result-object v3

    #@69
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v2

    #@6d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v2

    #@71
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 76
    :cond_74
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, "[toString] ret : "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->ret:I

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, " data: "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data:[B

    #@21
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 7
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 42
    const/4 v0, 0x0

    #@1
    .line 43
    .local v0, i:I
    iget v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->ret:I

    #@3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 44
    iget v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@8
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@b
    .line 45
    const/4 v0, 0x0

    #@c
    :goto_c
    iget v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@e
    if-ge v0, v1, :cond_1a

    #@10
    .line 46
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data:[B

    #@12
    aget-byte v1, v1, v0

    #@14
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 45
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_c

    #@1a
    .line 48
    :cond_1a
    sget-boolean v1, Lcom/android/internal/telephony/uicc/SmartCardResult;->ENABLE_PRIVACY_LOG:Z

    #@1c
    if-eqz v1, :cond_46

    #@1e
    .line 50
    const-string v1, "SmartCardResult"

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "[writeToParcel]data "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, " : "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/SmartCardResult;->data:[B

    #@37
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 52
    :cond_46
    return-void
.end method
