.class Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;
.super Ljava/lang/Object;
.source "SMSDispatcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/SMSDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EMSSegmentExpirationRunnable"
.end annotation


# instance fields
.field private address:Ljava/lang/String;

.field private mFirstTime:J

.field private msgCount:I

.field private refNumber:I

.field final synthetic this$0:Lcom/android/internal/telephony/SMSDispatcher;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;IIJ)V
    .registers 9
    .parameter
    .parameter "mAddress"
    .parameter "referenceNumber"
    .parameter "messageCount"
    .parameter "firstTime"

    #@0
    .prologue
    .line 3884
    iput-object p1, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->this$0:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 3885
    iput p3, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->refNumber:I

    #@7
    .line 3886
    iput p4, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->msgCount:I

    #@9
    .line 3887
    iput-wide p5, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->mFirstTime:J

    #@b
    .line 3889
    if-eqz p2, :cond_10

    #@d
    .line 3890
    iput-object p2, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->address:Ljava/lang/String;

    #@f
    .line 3895
    :goto_f
    return-void

    #@10
    .line 3893
    :cond_10
    new-instance v0, Ljava/lang/String;

    #@12
    const-string v1, "Unkown sender"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@17
    iput-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->address:Ljava/lang/String;

    #@19
    goto :goto_f
.end method

.method private sendUncompletedEMS(Ljava/lang/String;)V
    .registers 16
    .parameter "where"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 3916
    const/4 v6, 0x0

    #@2
    .line 3917
    .local v6, cursor:Landroid/database/Cursor;
    const/4 v0, 0x1

    #@3
    new-array v4, v0, [Ljava/lang/String;

    #@5
    const/4 v0, 0x0

    #@6
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->address:Ljava/lang/String;

    #@8
    aput-object v1, v4, v0

    #@a
    .line 3918
    .local v4, whereArgs:[Ljava/lang/String;
    check-cast v12, [[B

    #@c
    .line 3921
    .local v12, pdus:[[B
    :try_start_c
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->this$0:Lcom/android/internal/telephony/SMSDispatcher;

    #@e
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@10
    sget-object v1, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@12
    invoke-static {}, Lcom/android/internal/telephony/SMSDispatcher;->access$200()[Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    const/4 v5, 0x0

    #@17
    move-object v3, p1

    #@18
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1b
    move-result-object v6

    #@1c
    .line 3926
    if-eqz v6, :cond_4c

    #@1e
    .line 3927
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@21
    move-result v7

    #@22
    .line 3928
    .local v7, cursorCount:I
    const-string v0, "pdu"

    #@24
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@27
    move-result v11

    #@28
    .line 3929
    .local v11, pduColumn:I
    const-string v0, "sequence"

    #@2a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@2d
    move-result v13

    #@2e
    .line 3931
    .local v13, sequenceColumn:I
    iget v0, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->msgCount:I

    #@30
    new-array v12, v0, [[B

    #@32
    .line 3932
    const/4 v10, 0x0

    #@33
    .local v10, i:I
    :goto_33
    if-ge v10, v7, :cond_52

    #@35
    .line 3933
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@38
    .line 3934
    invoke-interface {v6, v13}, Landroid/database/Cursor;->getLong(I)J

    #@3b
    move-result-wide v0

    #@3c
    long-to-int v8, v0

    #@3d
    .line 3935
    .local v8, cursorSequence:I
    add-int/lit8 v0, v8, -0x1

    #@3f
    invoke-interface {v6, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-static {v1}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@46
    move-result-object v1

    #@47
    aput-object v1, v12, v0

    #@49
    .line 3932
    add-int/lit8 v10, v10, 0x1

    #@4b
    goto :goto_33

    #@4c
    .line 3939
    .end local v7           #cursorCount:I
    .end local v8           #cursorSequence:I
    .end local v10           #i:I
    .end local v11           #pduColumn:I
    .end local v13           #sequenceColumn:I
    :cond_4c
    const-string v0, "sendUncompletedEMS(), cursorCount is null"

    #@4e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@51
    .line 3940
    const/4 v7, 0x0

    #@52
    .line 3944
    .restart local v7       #cursorCount:I
    :cond_52
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->this$0:Lcom/android/internal/telephony/SMSDispatcher;

    #@54
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@56
    sget-object v1, Lcom/android/internal/telephony/SMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@58
    invoke-virtual {v0, v1, p1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5b
    .catchall {:try_start_c .. :try_end_5b} :catchall_7c
    .catch Landroid/database/SQLException; {:try_start_c .. :try_end_5b} :catch_61
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_5b} :catch_6a
    .catch Ljava/lang/NullPointerException; {:try_start_c .. :try_end_5b} :catch_73

    #@5b
    .line 3954
    if-eqz v6, :cond_60

    #@5d
    .line 3955
    .end local v7           #cursorCount:I
    :goto_5d
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@60
    .line 3958
    :cond_60
    return-void

    #@61
    .line 3945
    :catch_61
    move-exception v9

    #@62
    .line 3946
    .local v9, e:Landroid/database/SQLException;
    :try_start_62
    const-string v0, "sendUncompletedEMS(), send uncompleted EMS msg : query exception catch"

    #@64
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@67
    .line 3954
    if-eqz v6, :cond_60

    #@69
    goto :goto_5d

    #@6a
    .line 3948
    .end local v9           #e:Landroid/database/SQLException;
    :catch_6a
    move-exception v9

    #@6b
    .line 3949
    .local v9, e:Ljava/lang/IllegalArgumentException;
    const-string v0, "sendUncompletedEMS(), send uncompleted EMS msg : query IllegalArgumentException catch"

    #@6d
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@70
    .line 3954
    if-eqz v6, :cond_60

    #@72
    goto :goto_5d

    #@73
    .line 3951
    .end local v9           #e:Ljava/lang/IllegalArgumentException;
    :catch_73
    move-exception v9

    #@74
    .line 3952
    .local v9, e:Ljava/lang/NullPointerException;
    const-string v0, "sendUncompletedEMS(), send uncompleted EMS msg : null pointer exception catch"

    #@76
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_79
    .catchall {:try_start_62 .. :try_end_79} :catchall_7c

    #@79
    .line 3954
    if-eqz v6, :cond_60

    #@7b
    goto :goto_5d

    #@7c
    .end local v9           #e:Ljava/lang/NullPointerException;
    :catchall_7c
    move-exception v0

    #@7d
    if-eqz v6, :cond_82

    #@7f
    .line 3955
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@82
    .line 3954
    :cond_82
    throw v0
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 3900
    const-wide/32 v2, 0x493e0

    #@3
    :try_start_3
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_6} :catch_29

    #@6
    .line 3906
    :goto_6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    const-string v2, "reference_number ="

    #@a
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@d
    .line 3907
    .local v1, where:Ljava/lang/StringBuilder;
    iget v2, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->refNumber:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    .line 3908
    const-string v2, " AND "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 3909
    const-string v2, "time ="

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 3910
    iget-wide v2, p0, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->mFirstTime:J

    #@1e
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@21
    .line 3912
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/SMSDispatcher$EMSSegmentExpirationRunnable;->sendUncompletedEMS(Ljava/lang/String;)V

    #@28
    .line 3913
    return-void

    #@29
    .line 3901
    .end local v1           #where:Ljava/lang/StringBuilder;
    :catch_29
    move-exception v0

    #@2a
    .line 3902
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v2, "EMSSegmentExpirationRunnable:run(), Thread Interrupted exception catch"

    #@2c
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@2f
    goto :goto_6
.end method
