.class public abstract Lcom/android/internal/telephony/SmsMessageBase;
.super Ljava/lang/Object;
.source "SmsMessageBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;,
        Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "SMS"

.field protected static final TP_VPF_ABSOLUTE:I = 0x3

.field protected static final TP_VPF_ENHANCED:I = 0x1

.field protected static final TP_VPF_NONE:I = 0x0

.field protected static final TP_VPF_RELATIVE:I = 0x2

.field protected static timeSmsOnSim:J

.field protected static vp:I


# instance fields
.field protected cbAddress:Lcom/android/internal/telephony/SmsAddress;

.field protected destinationAddress:Lcom/android/internal/telephony/SmsAddress;

.field protected emailBody:Ljava/lang/String;

.field protected emailFrom:Ljava/lang/String;

.field protected indexOnIcc:I

.field protected isEmail:Z

.field protected isMwi:Z

.field protected mPdu:[B

.field protected messageBody:Ljava/lang/String;

.field public messageRef:I

.field protected mwiDontStore:Z

.field protected mwiSense:Z

.field protected originatingAddress:Lcom/android/internal/telephony/SmsAddress;

.field protected pseudoSubject:Ljava/lang/String;

.field protected scAddress:Ljava/lang/String;

.field protected scTimeMillis:J

.field protected statusOnIcc:I

.field protected statusReportReq:I

.field protected userData:[B

.field protected userDataHeader:Lcom/android/internal/telephony/SmsHeader;

.field protected vpFormatPresent:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 36
    const/4 v0, -0x1

    #@1
    sput v0, Lcom/android/internal/telephony/SmsMessageBase;->vp:I

    #@3
    .line 44
    const-wide/16 v0, 0x0

    #@5
    sput-wide v0, Lcom/android/internal/telephony/SmsMessageBase;->timeSmsOnSim:J

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 41
    iput v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->statusReportReq:I

    #@6
    .line 68
    iput v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->vpFormatPresent:I

    #@8
    .line 119
    iput v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->statusOnIcc:I

    #@a
    .line 124
    iput v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->indexOnIcc:I

    #@c
    .line 147
    return-void
.end method

.method public static getTimeforSMSonSIM()J
    .registers 2

    #@0
    .prologue
    .line 249
    sget-wide v0, Lcom/android/internal/telephony/SmsMessageBase;->timeSmsOnSim:J

    #@2
    return-wide v0
.end method

.method public static setTimeforSMSonSIM(J)V
    .registers 2
    .parameter "timemillisec"

    #@0
    .prologue
    .line 239
    sput-wide p0, Lcom/android/internal/telephony/SmsMessageBase;->timeSmsOnSim:J

    #@2
    .line 240
    return-void
.end method

.method public static setValidityPeriod(I)V
    .registers 1
    .parameter "validityperiod"

    #@0
    .prologue
    .line 413
    sput p0, Lcom/android/internal/telephony/SmsMessageBase;->vp:I

    #@2
    .line 414
    return-void
.end method


# virtual methods
.method protected extractEmailAddressFromMessageBody()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v5, -0x1

    #@3
    const/4 v4, 0x1

    #@4
    .line 525
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@6
    const-string v3, "( /)|( )"

    #@8
    invoke-virtual {v2, v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 526
    .local v1, parts:[Ljava/lang/String;
    array-length v2, v1

    #@d
    if-ge v2, v7, :cond_10

    #@f
    .line 585
    :cond_f
    :goto_f
    return-void

    #@10
    .line 527
    :cond_10
    aget-object v2, v1, v6

    #@12
    iput-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailFrom:Ljava/lang/String;

    #@14
    .line 528
    aget-object v2, v1, v4

    #@16
    iput-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailBody:Ljava/lang/String;

    #@18
    .line 532
    const/4 v2, 0x0

    #@19
    const-string v3, "vzw_sms_fromvtext"

    #@1b
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1e
    move-result v2

    #@1f
    if-ne v2, v4, :cond_79

    #@21
    .line 533
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@23
    invoke-virtual {v2}, Lcom/android/internal/telephony/SmsAddress;->couldBeEmailGateway()Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_70

    #@29
    .line 534
    iput-boolean v4, p0, Lcom/android/internal/telephony/SmsMessageBase;->isEmail:Z

    #@2b
    .line 545
    :goto_2b
    iget-boolean v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->isEmail:Z

    #@2d
    if-eqz v2, :cond_f

    #@2f
    .line 549
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailFrom:Ljava/lang/String;

    #@31
    invoke-static {v2}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@34
    move-result v2

    #@35
    if-nez v2, :cond_f

    #@37
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailFrom:Ljava/lang/String;

    #@39
    const-string v3, "("

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@3e
    move-result v2

    #@3f
    if-nez v2, :cond_f

    #@41
    .line 550
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@43
    const-string v3, ")"

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@48
    move-result v0

    #@49
    .line 551
    .local v0, parenthesis:I
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailFrom:Ljava/lang/String;

    #@4b
    const-string v3, ")"

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@50
    move-result v2

    #@51
    if-ne v2, v5, :cond_f

    #@53
    if-eq v0, v5, :cond_f

    #@55
    .line 552
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@57
    add-int/lit8 v3, v0, 0x1

    #@59
    invoke-virtual {v2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    iput-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailFrom:Ljava/lang/String;

    #@5f
    .line 553
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@61
    add-int/lit8 v3, v0, 0x2

    #@63
    iget-object v4, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@65
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@68
    move-result v4

    #@69
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6c
    move-result-object v2

    #@6d
    iput-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailBody:Ljava/lang/String;

    #@6f
    goto :goto_f

    #@70
    .line 536
    .end local v0           #parenthesis:I
    :cond_70
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailFrom:Ljava/lang/String;

    #@72
    invoke-static {v2}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@75
    move-result v2

    #@76
    iput-boolean v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->isEmail:Z

    #@78
    goto :goto_2b

    #@79
    .line 583
    :cond_79
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailFrom:Ljava/lang/String;

    #@7b
    invoke-static {v2}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@7e
    move-result v2

    #@7f
    iput-boolean v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->isEmail:Z

    #@81
    goto :goto_f
.end method

.method public getCallbackNum()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 386
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->cbAddress:Lcom/android/internal/telephony/SmsAddress;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 387
    const/4 v0, 0x0

    #@5
    .line 389
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->cbAddress:Lcom/android/internal/telephony/SmsAddress;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsAddress;->getAddressString()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getDestinationAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 216
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->destinationAddress:Lcom/android/internal/telephony/SmsAddress;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 217
    const/4 v0, 0x0

    #@5
    .line 220
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->destinationAddress:Lcom/android/internal/telephony/SmsAddress;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsAddress;->getAddressString()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getDisplayMessageBody()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 272
    iget-boolean v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->isEmail:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 273
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailBody:Ljava/lang/String;

    #@6
    .line 275
    :goto_6
    return-object v0

    #@7
    :cond_7
    invoke-virtual {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getMessageBody()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    goto :goto_6
.end method

.method public getDisplayMessageBody(Z)Ljava/lang/String;
    .registers 3
    .parameter "notUseEmail"

    #@0
    .prologue
    .line 287
    if-nez p1, :cond_7

    #@2
    .line 288
    invoke-virtual {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getDisplayMessageBody()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 290
    :goto_6
    return-object v0

    #@7
    :cond_7
    invoke-virtual {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getMessageBody()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    goto :goto_6
.end method

.method public getDisplayMessageBodyEx()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 614
    invoke-virtual {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getMessageBody()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getDisplayOriginatingAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->isEmail:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 187
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailFrom:Ljava/lang/String;

    #@6
    .line 189
    :goto_6
    return-object v0

    #@7
    :cond_7
    invoke-virtual {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    goto :goto_6
.end method

.method public getDisplayOriginatingAddress(Z)Ljava/lang/String;
    .registers 3
    .parameter "notUseEmail"

    #@0
    .prologue
    .line 201
    if-nez p1, :cond_7

    #@2
    .line 202
    invoke-virtual {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getDisplayOriginatingAddress()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 204
    :goto_6
    return-object v0

    #@7
    :cond_7
    invoke-virtual {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    goto :goto_6
.end method

.method public getDisplayOriginatingAddressEx()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 604
    invoke-virtual {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getEmailBody()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 325
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailBody:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getEmailFrom()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 333
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->emailFrom:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getIndexOnIcc()I
    .registers 2

    #@0
    .prologue
    .line 474
    iget v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->indexOnIcc:I

    #@2
    return v0
.end method

.method public getMessageBody()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 259
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public abstract getMessageClass()Lcom/android/internal/telephony/SmsConstants$MessageClass;
.end method

.method public abstract getNumOfVoicemails()I
.end method

.method public getOriginatingAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 173
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 174
    const/4 v0, 0x0

    #@5
    .line 177
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsAddress;->getAddressString()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getPdu()[B
    .registers 2

    #@0
    .prologue
    .line 428
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->mPdu:[B

    #@2
    return-object v0
.end method

.method public abstract getProtocolIdentifier()I
.end method

.method public getPseudoSubject()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 300
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->pseudoSubject:Ljava/lang/String;

    #@2
    if-nez v0, :cond_7

    #@4
    const-string v0, ""

    #@6
    :goto_6
    return-object v0

    #@7
    :cond_7
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->pseudoSubject:Ljava/lang/String;

    #@9
    goto :goto_6
.end method

.method public getServiceCenterAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->scAddress:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public abstract getSmsDisplayMode()I
.end method

.method public abstract getStatus()I
.end method

.method public getStatusOnIcc()I
    .registers 2

    #@0
    .prologue
    .line 465
    iget v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->statusOnIcc:I

    #@2
    return v0
.end method

.method public getStatusReportReq()I
    .registers 2

    #@0
    .prologue
    .line 229
    iget v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->statusReportReq:I

    #@2
    return v0
.end method

.method public getTimestampMillis()J
    .registers 3

    #@0
    .prologue
    .line 307
    iget-wide v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->scTimeMillis:J

    #@2
    return-wide v0
.end method

.method public getUserData()[B
    .registers 2

    #@0
    .prologue
    .line 399
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->userData:[B

    #@2
    return-object v0
.end method

.method public getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;
    .registers 2

    #@0
    .prologue
    .line 408
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@2
    return-object v0
.end method

.method public abstract isCphsMwiMessage()Z
.end method

.method public isEmail()Z
    .registers 2

    #@0
    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->isEmail:Z

    #@2
    return v0
.end method

.method public abstract isMWIClearMessage()Z
.end method

.method public abstract isMWISetMessage()Z
.end method

.method public abstract isMwiDontStore()Z
.end method

.method public abstract isMwiUrgentMessage()Z
.end method

.method public abstract isReplace()Z
.end method

.method public abstract isReplyPathPresent()Z
.end method

.method public abstract isStatusReportMessage()Z
.end method

.method protected parseMessageBody()V
    .registers 2

    #@0
    .prologue
    .line 500
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsAddress;->couldBeEmailGateway()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_f

    #@c
    .line 501
    invoke-virtual {p0}, Lcom/android/internal/telephony/SmsMessageBase;->extractEmailAddressFromMessageBody()V

    #@f
    .line 503
    :cond_f
    return-void
.end method

.method public replaceMessageBody(Ljava/lang/String;)Z
    .registers 3
    .parameter "newText"

    #@0
    .prologue
    .line 620
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setIndexOnIcc(I)V
    .registers 2
    .parameter "index"

    #@0
    .prologue
    .line 483
    iput p1, p0, Lcom/android/internal/telephony/SmsMessageBase;->indexOnIcc:I

    #@2
    .line 484
    return-void
.end method
