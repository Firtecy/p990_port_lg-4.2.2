.class synthetic Lcom/android/internal/telephony/LGEDataConnectionTracker$2;
.super Ljava/lang/Object;
.source "LGEDataConnectionTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGEDataConnectionTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$android$internal$telephony$DctConstants$State:[I

.field static final synthetic $SwitchMap$com$android$internal$telephony$PhoneConstants$DataState:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 1836
    invoke-static {}, Lcom/android/internal/telephony/DctConstants$State;->values()[Lcom/android/internal/telephony/DctConstants$State;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    new-array v0, v0, [I

    #@7
    sput-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$DctConstants$State:[I

    #@9
    :try_start_9
    sget-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$DctConstants$State:[I

    #@b
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@d
    invoke-virtual {v1}, Lcom/android/internal/telephony/DctConstants$State;->ordinal()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x1

    #@12
    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_93

    #@14
    :goto_14
    :try_start_14
    sget-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$DctConstants$State:[I

    #@16
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->DISCONNECTING:Lcom/android/internal/telephony/DctConstants$State;

    #@18
    invoke-virtual {v1}, Lcom/android/internal/telephony/DctConstants$State;->ordinal()I

    #@1b
    move-result v1

    #@1c
    const/4 v2, 0x2

    #@1d
    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_91

    #@1f
    :goto_1f
    :try_start_1f
    sget-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$DctConstants$State:[I

    #@21
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->CONNECTING:Lcom/android/internal/telephony/DctConstants$State;

    #@23
    invoke-virtual {v1}, Lcom/android/internal/telephony/DctConstants$State;->ordinal()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x3

    #@28
    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_8f

    #@2a
    :goto_2a
    :try_start_2a
    sget-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$DctConstants$State:[I

    #@2c
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->INITING:Lcom/android/internal/telephony/DctConstants$State;

    #@2e
    invoke-virtual {v1}, Lcom/android/internal/telephony/DctConstants$State;->ordinal()I

    #@31
    move-result v1

    #@32
    const/4 v2, 0x4

    #@33
    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_35} :catch_8d

    #@35
    :goto_35
    :try_start_35
    sget-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$DctConstants$State:[I

    #@37
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@39
    invoke-virtual {v1}, Lcom/android/internal/telephony/DctConstants$State;->ordinal()I

    #@3c
    move-result v1

    #@3d
    const/4 v2, 0x5

    #@3e
    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_40} :catch_8b

    #@40
    :goto_40
    :try_start_40
    sget-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$DctConstants$State:[I

    #@42
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->SCANNING:Lcom/android/internal/telephony/DctConstants$State;

    #@44
    invoke-virtual {v1}, Lcom/android/internal/telephony/DctConstants$State;->ordinal()I

    #@47
    move-result v1

    #@48
    const/4 v2, 0x6

    #@49
    aput v2, v0, v1
    :try_end_4b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_4b} :catch_89

    #@4b
    .line 430
    :goto_4b
    invoke-static {}, Lcom/android/internal/telephony/PhoneConstants$DataState;->values()[Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@4e
    move-result-object v0

    #@4f
    array-length v0, v0

    #@50
    new-array v0, v0, [I

    #@52
    sput-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$PhoneConstants$DataState:[I

    #@54
    :try_start_54
    sget-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$PhoneConstants$DataState:[I

    #@56
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@58
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneConstants$DataState;->ordinal()I

    #@5b
    move-result v1

    #@5c
    const/4 v2, 0x1

    #@5d
    aput v2, v0, v1
    :try_end_5f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_54 .. :try_end_5f} :catch_87

    #@5f
    :goto_5f
    :try_start_5f
    sget-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$PhoneConstants$DataState:[I

    #@61
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTING:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@63
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneConstants$DataState;->ordinal()I

    #@66
    move-result v1

    #@67
    const/4 v2, 0x2

    #@68
    aput v2, v0, v1
    :try_end_6a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5f .. :try_end_6a} :catch_85

    #@6a
    :goto_6a
    :try_start_6a
    sget-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$PhoneConstants$DataState:[I

    #@6c
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$DataState;->SUSPENDED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@6e
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneConstants$DataState;->ordinal()I

    #@71
    move-result v1

    #@72
    const/4 v2, 0x3

    #@73
    aput v2, v0, v1
    :try_end_75
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6a .. :try_end_75} :catch_83

    #@75
    :goto_75
    :try_start_75
    sget-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$PhoneConstants$DataState:[I

    #@77
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@79
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneConstants$DataState;->ordinal()I

    #@7c
    move-result v1

    #@7d
    const/4 v2, 0x4

    #@7e
    aput v2, v0, v1
    :try_end_80
    .catch Ljava/lang/NoSuchFieldError; {:try_start_75 .. :try_end_80} :catch_81

    #@80
    :goto_80
    return-void

    #@81
    :catch_81
    move-exception v0

    #@82
    goto :goto_80

    #@83
    :catch_83
    move-exception v0

    #@84
    goto :goto_75

    #@85
    :catch_85
    move-exception v0

    #@86
    goto :goto_6a

    #@87
    :catch_87
    move-exception v0

    #@88
    goto :goto_5f

    #@89
    .line 1836
    :catch_89
    move-exception v0

    #@8a
    goto :goto_4b

    #@8b
    :catch_8b
    move-exception v0

    #@8c
    goto :goto_40

    #@8d
    :catch_8d
    move-exception v0

    #@8e
    goto :goto_35

    #@8f
    :catch_8f
    move-exception v0

    #@90
    goto :goto_2a

    #@91
    :catch_91
    move-exception v0

    #@92
    goto :goto_1f

    #@93
    :catch_93
    move-exception v0

    #@94
    goto :goto_14
.end method
