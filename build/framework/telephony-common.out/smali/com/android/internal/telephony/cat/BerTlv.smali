.class Lcom/android/internal/telephony/cat/BerTlv;
.super Ljava/lang/Object;
.source "BerTlv.java"


# static fields
.field public static final BER_EVENT_DOWNLOAD_TAG:I = 0xd6

.field public static final BER_MENU_SELECTION_TAG:I = 0xd3

.field public static final BER_PROACTIVE_COMMAND_TAG:I = 0xd0

.field public static final BER_UNKNOWN_TAG:I


# instance fields
.field private mCompTlvs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;"
        }
    .end annotation
.end field

.field private mTag:I


# direct methods
.method private constructor <init>(ILjava/util/List;)V
    .registers 4
    .parameter "tag"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 37
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/internal/telephony/cat/BerTlv;->mTag:I

    #@6
    .line 30
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/android/internal/telephony/cat/BerTlv;->mCompTlvs:Ljava/util/List;

    #@9
    .line 38
    iput p1, p0, Lcom/android/internal/telephony/cat/BerTlv;->mTag:I

    #@b
    .line 39
    iput-object p2, p0, Lcom/android/internal/telephony/cat/BerTlv;->mCompTlvs:Ljava/util/List;

    #@d
    .line 40
    return-void
.end method

.method public static decode([B)Lcom/android/internal/telephony/cat/BerTlv;
    .registers 13
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v9, 0x80

    #@2
    .line 68
    const/4 v1, 0x0

    #@3
    .line 69
    .local v1, curIndex:I
    array-length v4, p0

    #@4
    .line 70
    .local v4, endIndex:I
    const/4 v5, 0x0

    #@5
    .line 74
    .local v5, length:I
    add-int/lit8 v2, v1, 0x1

    #@7
    .end local v1           #curIndex:I
    .local v2, curIndex:I
    :try_start_7
    aget-byte v8, p0, v1
    :try_end_9
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_7 .. :try_end_9} :catch_8a
    .catch Lcom/android/internal/telephony/cat/ResultException; {:try_start_7 .. :try_end_9} :catch_f8

    #@9
    and-int/lit16 v6, v8, 0xff

    #@b
    .line 75
    .local v6, tag:I
    const/16 v8, 0xd0

    #@d
    if-ne v6, v8, :cond_ea

    #@f
    .line 77
    add-int/lit8 v1, v2, 0x1

    #@11
    .end local v2           #curIndex:I
    .restart local v1       #curIndex:I
    :try_start_11
    aget-byte v8, p0, v2
    :try_end_13
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_11 .. :try_end_13} :catch_e8
    .catch Lcom/android/internal/telephony/cat/ResultException; {:try_start_11 .. :try_end_13} :catch_110

    #@13
    and-int/lit16 v7, v8, 0xff

    #@15
    .line 78
    .local v7, temp:I
    if-ge v7, v9, :cond_4b

    #@17
    .line 79
    move v5, v7

    #@18
    .line 112
    .end local v7           #temp:I
    :goto_18
    sub-int v8, v4, v1

    #@1a
    if-ge v8, v5, :cond_106

    #@1c
    .line 113
    new-instance v8, Lcom/android/internal/telephony/cat/ResultException;

    #@1e
    sget-object v9, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@20
    new-instance v10, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v11, "Command had extra data endIndex="

    #@27
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v10

    #@2b
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v10

    #@2f
    const-string v11, " curIndex="

    #@31
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v10

    #@35
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v10

    #@39
    const-string v11, " length="

    #@3b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v10

    #@3f
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v10

    #@43
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v10

    #@47
    invoke-direct {v8, v9, v10}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V

    #@4a
    throw v8

    #@4b
    .line 80
    .restart local v7       #temp:I
    :cond_4b
    const/16 v8, 0x81

    #@4d
    if-ne v7, v8, :cond_b5

    #@4f
    .line 81
    add-int/lit8 v2, v1, 0x1

    #@51
    .end local v1           #curIndex:I
    .restart local v2       #curIndex:I
    :try_start_51
    aget-byte v8, p0, v1

    #@53
    and-int/lit16 v7, v8, 0xff

    #@55
    .line 82
    if-ge v7, v9, :cond_b1

    #@57
    .line 83
    new-instance v8, Lcom/android/internal/telephony/cat/ResultException;

    #@59
    sget-object v9, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@5b
    new-instance v10, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v11, "length < 0x80 length="

    #@62
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v10

    #@66
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@69
    move-result-object v11

    #@6a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v10

    #@6e
    const-string v11, " curIndex="

    #@70
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v10

    #@74
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v10

    #@78
    const-string v11, " endIndex="

    #@7a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v10

    #@7e
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81
    move-result-object v10

    #@82
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v10

    #@86
    invoke-direct {v8, v9, v10}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V

    #@89
    throw v8
    :try_end_8a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_51 .. :try_end_8a} :catch_8a
    .catch Lcom/android/internal/telephony/cat/ResultException; {:try_start_51 .. :try_end_8a} :catch_f8

    #@8a
    .line 103
    .end local v6           #tag:I
    .end local v7           #temp:I
    :catch_8a
    move-exception v3

    #@8b
    move v1, v2

    #@8c
    .line 104
    .end local v2           #curIndex:I
    .restart local v1       #curIndex:I
    .local v3, e:Ljava/lang/IndexOutOfBoundsException;
    :goto_8c
    new-instance v8, Lcom/android/internal/telephony/cat/ResultException;

    #@8e
    sget-object v9, Lcom/android/internal/telephony/cat/ResultCode;->REQUIRED_VALUES_MISSING:Lcom/android/internal/telephony/cat/ResultCode;

    #@90
    new-instance v10, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v11, "IndexOutOfBoundsException  curIndex="

    #@97
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v10

    #@9b
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v10

    #@9f
    const-string v11, " endIndex="

    #@a1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v10

    #@a5
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v10

    #@a9
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v10

    #@ad
    invoke-direct {v8, v9, v10}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V

    #@b0
    throw v8

    #@b1
    .line 89
    .end local v1           #curIndex:I
    .end local v3           #e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v2       #curIndex:I
    .restart local v6       #tag:I
    .restart local v7       #temp:I
    :cond_b1
    move v5, v7

    #@b2
    move v1, v2

    #@b3
    .end local v2           #curIndex:I
    .restart local v1       #curIndex:I
    goto/16 :goto_18

    #@b5
    .line 91
    :cond_b5
    :try_start_b5
    new-instance v8, Lcom/android/internal/telephony/cat/ResultException;

    #@b7
    sget-object v9, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@b9
    new-instance v10, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v11, "Expected first byte to be length or a length tag and < 0x81 byte= "

    #@c0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v10

    #@c4
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@c7
    move-result-object v11

    #@c8
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v10

    #@cc
    const-string v11, " curIndex="

    #@ce
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v10

    #@d2
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v10

    #@d6
    const-string v11, " endIndex="

    #@d8
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v10

    #@dc
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@df
    move-result-object v10

    #@e0
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v10

    #@e4
    invoke-direct {v8, v9, v10}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V

    #@e7
    throw v8
    :try_end_e8
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_b5 .. :try_end_e8} :catch_e8
    .catch Lcom/android/internal/telephony/cat/ResultException; {:try_start_b5 .. :try_end_e8} :catch_110

    #@e8
    .line 103
    .end local v7           #temp:I
    :catch_e8
    move-exception v3

    #@e9
    goto :goto_8c

    #@ea
    .line 98
    .end local v1           #curIndex:I
    .restart local v2       #curIndex:I
    :cond_ea
    :try_start_ea
    sget-object v8, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->COMMAND_DETAILS:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@ec
    invoke-virtual {v8}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I
    :try_end_ef
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_ea .. :try_end_ef} :catch_8a
    .catch Lcom/android/internal/telephony/cat/ResultException; {:try_start_ea .. :try_end_ef} :catch_f8

    #@ef
    move-result v8

    #@f0
    and-int/lit16 v9, v6, -0x81

    #@f2
    if-ne v8, v9, :cond_112

    #@f4
    .line 99
    const/4 v6, 0x0

    #@f5
    .line 100
    const/4 v1, 0x0

    #@f6
    .end local v2           #curIndex:I
    .restart local v1       #curIndex:I
    goto/16 :goto_18

    #@f8
    .line 107
    .end local v1           #curIndex:I
    .end local v6           #tag:I
    .restart local v2       #curIndex:I
    :catch_f8
    move-exception v3

    #@f9
    move v1, v2

    #@fa
    .line 108
    .end local v2           #curIndex:I
    .restart local v1       #curIndex:I
    .local v3, e:Lcom/android/internal/telephony/cat/ResultException;
    :goto_fa
    new-instance v8, Lcom/android/internal/telephony/cat/ResultException;

    #@fc
    sget-object v9, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@fe
    invoke-virtual {v3}, Lcom/android/internal/telephony/cat/ResultException;->explanation()Ljava/lang/String;

    #@101
    move-result-object v10

    #@102
    invoke-direct {v8, v9, v10}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V

    #@105
    throw v8

    #@106
    .line 118
    .end local v3           #e:Lcom/android/internal/telephony/cat/ResultException;
    .restart local v6       #tag:I
    :cond_106
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->decodeMany([BI)Ljava/util/List;

    #@109
    move-result-object v0

    #@10a
    .line 121
    .local v0, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    new-instance v8, Lcom/android/internal/telephony/cat/BerTlv;

    #@10c
    invoke-direct {v8, v6, v0}, Lcom/android/internal/telephony/cat/BerTlv;-><init>(ILjava/util/List;)V

    #@10f
    return-object v8

    #@110
    .line 107
    .end local v0           #ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    :catch_110
    move-exception v3

    #@111
    goto :goto_fa

    #@112
    .end local v1           #curIndex:I
    .restart local v2       #curIndex:I
    :cond_112
    move v1, v2

    #@113
    .end local v2           #curIndex:I
    .restart local v1       #curIndex:I
    goto/16 :goto_18
.end method


# virtual methods
.method public getComprehensionTlvs()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/internal/telephony/cat/BerTlv;->mCompTlvs:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public getTag()I
    .registers 2

    #@0
    .prologue
    .line 57
    iget v0, p0, Lcom/android/internal/telephony/cat/BerTlv;->mTag:I

    #@2
    return v0
.end method
