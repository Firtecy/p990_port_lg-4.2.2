.class public Lcom/android/internal/telephony/gsm/SimTlv;
.super Ljava/lang/Object;
.source "SimTlv.java"


# instance fields
.field curDataLength:I

.field curDataOffset:I

.field curOffset:I

.field hasValidTlvObject:Z

.field record:[B

.field tlvLength:I

.field tlvOffset:I


# direct methods
.method public constructor <init>([BII)V
    .registers 5
    .parameter "record"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/SimTlv;->record:[B

    #@5
    .line 41
    iput p2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->tlvOffset:I

    #@7
    .line 42
    iput p3, p0, Lcom/android/internal/telephony/gsm/SimTlv;->tlvLength:I

    #@9
    .line 43
    iput p2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curOffset:I

    #@b
    .line 45
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SimTlv;->parseCurrentTlvObject()Z

    #@e
    move-result v0

    #@f
    iput-boolean v0, p0, Lcom/android/internal/telephony/gsm/SimTlv;->hasValidTlvObject:Z

    #@11
    .line 46
    return-void
.end method

.method private parseCurrentTlvObject()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 92
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->record:[B

    #@3
    iget v3, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curOffset:I

    #@5
    aget-byte v2, v2, v3

    #@7
    if-eqz v2, :cond_15

    #@9
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->record:[B

    #@b
    iget v3, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curOffset:I

    #@d
    aget-byte v2, v2, v3

    #@f
    and-int/lit16 v2, v2, 0xff

    #@11
    const/16 v3, 0xff

    #@13
    if-ne v2, v3, :cond_16

    #@15
    .line 115
    :cond_15
    :goto_15
    return v1

    #@16
    .line 96
    :cond_16
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->record:[B

    #@18
    iget v3, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curOffset:I

    #@1a
    add-int/lit8 v3, v3, 0x1

    #@1c
    aget-byte v2, v2, v3

    #@1e
    and-int/lit16 v2, v2, 0xff

    #@20
    const/16 v3, 0x80

    #@22
    if-ge v2, v3, :cond_44

    #@24
    .line 98
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->record:[B

    #@26
    iget v3, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curOffset:I

    #@28
    add-int/lit8 v3, v3, 0x1

    #@2a
    aget-byte v2, v2, v3

    #@2c
    and-int/lit16 v2, v2, 0xff

    #@2e
    iput v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curDataLength:I

    #@30
    .line 99
    iget v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curOffset:I

    #@32
    add-int/lit8 v2, v2, 0x2

    #@34
    iput v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curDataOffset:I
    :try_end_36
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_36} :catch_65

    #@36
    .line 111
    :goto_36
    iget v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curDataLength:I

    #@38
    iget v3, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curDataOffset:I

    #@3a
    add-int/2addr v2, v3

    #@3b
    iget v3, p0, Lcom/android/internal/telephony/gsm/SimTlv;->tlvOffset:I

    #@3d
    iget v4, p0, Lcom/android/internal/telephony/gsm/SimTlv;->tlvLength:I

    #@3f
    add-int/2addr v3, v4

    #@40
    if-gt v2, v3, :cond_15

    #@42
    .line 115
    const/4 v1, 0x1

    #@43
    goto :goto_15

    #@44
    .line 100
    :cond_44
    :try_start_44
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->record:[B

    #@46
    iget v3, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curOffset:I

    #@48
    add-int/lit8 v3, v3, 0x1

    #@4a
    aget-byte v2, v2, v3

    #@4c
    and-int/lit16 v2, v2, 0xff

    #@4e
    const/16 v3, 0x81

    #@50
    if-ne v2, v3, :cond_15

    #@52
    .line 102
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->record:[B

    #@54
    iget v3, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curOffset:I

    #@56
    add-int/lit8 v3, v3, 0x2

    #@58
    aget-byte v2, v2, v3

    #@5a
    and-int/lit16 v2, v2, 0xff

    #@5c
    iput v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curDataLength:I

    #@5e
    .line 103
    iget v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curOffset:I

    #@60
    add-int/lit8 v2, v2, 0x3

    #@62
    iput v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curDataOffset:I
    :try_end_64
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_44 .. :try_end_64} :catch_65

    #@64
    goto :goto_36

    #@65
    .line 107
    :catch_65
    move-exception v0

    #@66
    .line 108
    .local v0, ex:Ljava/lang/ArrayIndexOutOfBoundsException;
    goto :goto_15
.end method


# virtual methods
.method public getData()[B
    .registers 6

    #@0
    .prologue
    .line 76
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/SimTlv;->hasValidTlvObject:Z

    #@2
    if-nez v1, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 80
    :goto_5
    return-object v0

    #@6
    .line 78
    :cond_6
    iget v1, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curDataLength:I

    #@8
    new-array v0, v1, [B

    #@a
    .line 79
    .local v0, ret:[B
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SimTlv;->record:[B

    #@c
    iget v2, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curDataOffset:I

    #@e
    const/4 v3, 0x0

    #@f
    iget v4, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curDataLength:I

    #@11
    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@14
    goto :goto_5
.end method

.method public getTag()I
    .registers 3

    #@0
    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/SimTlv;->hasValidTlvObject:Z

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 67
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SimTlv;->record:[B

    #@8
    iget v1, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curOffset:I

    #@a
    aget-byte v0, v0, v1

    #@c
    and-int/lit16 v0, v0, 0xff

    #@e
    goto :goto_5
.end method

.method public isValidObject()Z
    .registers 2

    #@0
    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/SimTlv;->hasValidTlvObject:Z

    #@2
    return v0
.end method

.method public nextObject()Z
    .registers 3

    #@0
    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/SimTlv;->hasValidTlvObject:Z

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 52
    :goto_5
    return v0

    #@6
    .line 50
    :cond_6
    iget v0, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curDataOffset:I

    #@8
    iget v1, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curDataLength:I

    #@a
    add-int/2addr v0, v1

    #@b
    iput v0, p0, Lcom/android/internal/telephony/gsm/SimTlv;->curOffset:I

    #@d
    .line 51
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SimTlv;->parseCurrentTlvObject()Z

    #@10
    move-result v0

    #@11
    iput-boolean v0, p0, Lcom/android/internal/telephony/gsm/SimTlv;->hasValidTlvObject:Z

    #@13
    .line 52
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/SimTlv;->hasValidTlvObject:Z

    #@15
    goto :goto_5
.end method
