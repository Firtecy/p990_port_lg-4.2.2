.class public final enum Lcom/android/internal/telephony/cat/ResultCode;
.super Ljava/lang/Enum;
.source "ResultCode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/cat/ResultCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum ACCESS_TECH_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum BACKWARD_MOVE_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum BIP_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum CMD_NUM_NOT_KNOWN:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum CMD_TYPE_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum CONTRADICTION_WITH_TIMER:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum FRAMES_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum HELP_INFO_REQUIRED:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum LAUNCH_BROWSER_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum MMS_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum MMS_TEMPORARY:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum MULTI_CARDS_CMD_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum NAA_CALL_CONTROL_TEMPORARY:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum NETWORK_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum NO_RESPONSE_FROM_USER:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum OK:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum PRFRMD_ICON_NOT_DISPLAYED:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum PRFRMD_LIMITED_SERVICE:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum PRFRMD_MODIFIED_BY_NAA:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum PRFRMD_NAA_NOT_ACTIVE:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum PRFRMD_TONE_NOT_PLAYED:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum PRFRMD_WITH_ADDITIONAL_EFS_READ:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum PRFRMD_WITH_MISSING_INFO:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum PRFRMD_WITH_MODIFICATION:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum PRFRMD_WITH_PARTIAL_COMPREHENSION:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum REQUIRED_VALUES_MISSING:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum SMS_RP_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum SS_RETURN_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum TERMINAL_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum UICC_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum USER_CLEAR_DOWN_CALL:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum USER_NOT_ACCEPT:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum USIM_CALL_CONTROL_PERMANENT:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum USSD_RETURN_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

.field public static final enum USSD_SS_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;


# instance fields
.field private mCode:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 34
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@7
    const-string v1, "OK"

    #@9
    invoke-direct {v0, v1, v4, v4}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@e
    .line 37
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@10
    const-string v1, "PRFRMD_WITH_PARTIAL_COMPREHENSION"

    #@12
    invoke-direct {v0, v1, v5, v5}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_WITH_PARTIAL_COMPREHENSION:Lcom/android/internal/telephony/cat/ResultCode;

    #@17
    .line 40
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@19
    const-string v1, "PRFRMD_WITH_MISSING_INFO"

    #@1b
    invoke-direct {v0, v1, v6, v6}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_WITH_MISSING_INFO:Lcom/android/internal/telephony/cat/ResultCode;

    #@20
    .line 43
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@22
    const-string v1, "PRFRMD_WITH_ADDITIONAL_EFS_READ"

    #@24
    invoke-direct {v0, v1, v7, v7}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_WITH_ADDITIONAL_EFS_READ:Lcom/android/internal/telephony/cat/ResultCode;

    #@29
    .line 49
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@2b
    const-string v1, "PRFRMD_ICON_NOT_DISPLAYED"

    #@2d
    invoke-direct {v0, v1, v8, v8}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_ICON_NOT_DISPLAYED:Lcom/android/internal/telephony/cat/ResultCode;

    #@32
    .line 52
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@34
    const-string v1, "PRFRMD_MODIFIED_BY_NAA"

    #@36
    const/4 v2, 0x5

    #@37
    const/4 v3, 0x5

    #@38
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@3b
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_MODIFIED_BY_NAA:Lcom/android/internal/telephony/cat/ResultCode;

    #@3d
    .line 55
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@3f
    const-string v1, "PRFRMD_LIMITED_SERVICE"

    #@41
    const/4 v2, 0x6

    #@42
    const/4 v3, 0x6

    #@43
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@46
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_LIMITED_SERVICE:Lcom/android/internal/telephony/cat/ResultCode;

    #@48
    .line 58
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@4a
    const-string v1, "PRFRMD_WITH_MODIFICATION"

    #@4c
    const/4 v2, 0x7

    #@4d
    const/4 v3, 0x7

    #@4e
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@51
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_WITH_MODIFICATION:Lcom/android/internal/telephony/cat/ResultCode;

    #@53
    .line 61
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@55
    const-string v1, "PRFRMD_NAA_NOT_ACTIVE"

    #@57
    const/16 v2, 0x8

    #@59
    const/16 v3, 0x8

    #@5b
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@5e
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_NAA_NOT_ACTIVE:Lcom/android/internal/telephony/cat/ResultCode;

    #@60
    .line 64
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@62
    const-string v1, "PRFRMD_TONE_NOT_PLAYED"

    #@64
    const/16 v2, 0x9

    #@66
    const/16 v3, 0x9

    #@68
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@6b
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_TONE_NOT_PLAYED:Lcom/android/internal/telephony/cat/ResultCode;

    #@6d
    .line 67
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@6f
    const-string v1, "UICC_SESSION_TERM_BY_USER"

    #@71
    const/16 v2, 0xa

    #@73
    const/16 v3, 0x10

    #@75
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@78
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->UICC_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    #@7a
    .line 70
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@7c
    const-string v1, "BACKWARD_MOVE_BY_USER"

    #@7e
    const/16 v2, 0xb

    #@80
    const/16 v3, 0x11

    #@82
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@85
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->BACKWARD_MOVE_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    #@87
    .line 73
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@89
    const-string v1, "NO_RESPONSE_FROM_USER"

    #@8b
    const/16 v2, 0xc

    #@8d
    const/16 v3, 0x12

    #@8f
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@92
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->NO_RESPONSE_FROM_USER:Lcom/android/internal/telephony/cat/ResultCode;

    #@94
    .line 76
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@96
    const-string v1, "HELP_INFO_REQUIRED"

    #@98
    const/16 v2, 0xd

    #@9a
    const/16 v3, 0x13

    #@9c
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@9f
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->HELP_INFO_REQUIRED:Lcom/android/internal/telephony/cat/ResultCode;

    #@a1
    .line 79
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@a3
    const-string v1, "USSD_SS_SESSION_TERM_BY_USER"

    #@a5
    const/16 v2, 0xe

    #@a7
    const/16 v3, 0x14

    #@a9
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@ac
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->USSD_SS_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    #@ae
    .line 88
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@b0
    const-string v1, "TERMINAL_CRNTLY_UNABLE_TO_PROCESS"

    #@b2
    const/16 v2, 0xf

    #@b4
    const/16 v3, 0x20

    #@b6
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@b9
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->TERMINAL_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    #@bb
    .line 91
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@bd
    const-string v1, "NETWORK_CRNTLY_UNABLE_TO_PROCESS"

    #@bf
    const/16 v2, 0x10

    #@c1
    const/16 v3, 0x21

    #@c3
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@c6
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->NETWORK_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    #@c8
    .line 94
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@ca
    const-string v1, "USER_NOT_ACCEPT"

    #@cc
    const/16 v2, 0x11

    #@ce
    const/16 v3, 0x22

    #@d0
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@d3
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->USER_NOT_ACCEPT:Lcom/android/internal/telephony/cat/ResultCode;

    #@d5
    .line 97
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@d7
    const-string v1, "USER_CLEAR_DOWN_CALL"

    #@d9
    const/16 v2, 0x12

    #@db
    const/16 v3, 0x23

    #@dd
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@e0
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->USER_CLEAR_DOWN_CALL:Lcom/android/internal/telephony/cat/ResultCode;

    #@e2
    .line 100
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@e4
    const-string v1, "CONTRADICTION_WITH_TIMER"

    #@e6
    const/16 v2, 0x13

    #@e8
    const/16 v3, 0x24

    #@ea
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@ed
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->CONTRADICTION_WITH_TIMER:Lcom/android/internal/telephony/cat/ResultCode;

    #@ef
    .line 103
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@f1
    const-string v1, "NAA_CALL_CONTROL_TEMPORARY"

    #@f3
    const/16 v2, 0x14

    #@f5
    const/16 v3, 0x25

    #@f7
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@fa
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->NAA_CALL_CONTROL_TEMPORARY:Lcom/android/internal/telephony/cat/ResultCode;

    #@fc
    .line 106
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@fe
    const-string v1, "LAUNCH_BROWSER_ERROR"

    #@100
    const/16 v2, 0x15

    #@102
    const/16 v3, 0x26

    #@104
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@107
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->LAUNCH_BROWSER_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@109
    .line 109
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@10b
    const-string v1, "MMS_TEMPORARY"

    #@10d
    const/16 v2, 0x16

    #@10f
    const/16 v3, 0x27

    #@111
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@114
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->MMS_TEMPORARY:Lcom/android/internal/telephony/cat/ResultCode;

    #@116
    .line 119
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@118
    const-string v1, "BEYOND_TERMINAL_CAPABILITY"

    #@11a
    const/16 v2, 0x17

    #@11c
    const/16 v3, 0x30

    #@11e
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@121
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    #@123
    .line 122
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@125
    const-string v1, "CMD_TYPE_NOT_UNDERSTOOD"

    #@127
    const/16 v2, 0x18

    #@129
    const/16 v3, 0x31

    #@12b
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@12e
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->CMD_TYPE_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@130
    .line 125
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@132
    const-string v1, "CMD_DATA_NOT_UNDERSTOOD"

    #@134
    const/16 v2, 0x19

    #@136
    const/16 v3, 0x32

    #@138
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@13b
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@13d
    .line 128
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@13f
    const-string v1, "CMD_NUM_NOT_KNOWN"

    #@141
    const/16 v2, 0x1a

    #@143
    const/16 v3, 0x33

    #@145
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@148
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->CMD_NUM_NOT_KNOWN:Lcom/android/internal/telephony/cat/ResultCode;

    #@14a
    .line 131
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@14c
    const-string v1, "SS_RETURN_ERROR"

    #@14e
    const/16 v2, 0x1b

    #@150
    const/16 v3, 0x34

    #@152
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@155
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->SS_RETURN_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@157
    .line 134
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@159
    const-string v1, "SMS_RP_ERROR"

    #@15b
    const/16 v2, 0x1c

    #@15d
    const/16 v3, 0x35

    #@15f
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@162
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->SMS_RP_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@164
    .line 137
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@166
    const-string v1, "REQUIRED_VALUES_MISSING"

    #@168
    const/16 v2, 0x1d

    #@16a
    const/16 v3, 0x36

    #@16c
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@16f
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->REQUIRED_VALUES_MISSING:Lcom/android/internal/telephony/cat/ResultCode;

    #@171
    .line 140
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@173
    const-string v1, "USSD_RETURN_ERROR"

    #@175
    const/16 v2, 0x1e

    #@177
    const/16 v3, 0x37

    #@179
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@17c
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->USSD_RETURN_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@17e
    .line 143
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@180
    const-string v1, "MULTI_CARDS_CMD_ERROR"

    #@182
    const/16 v2, 0x1f

    #@184
    const/16 v3, 0x38

    #@186
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@189
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->MULTI_CARDS_CMD_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@18b
    .line 149
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@18d
    const-string v1, "USIM_CALL_CONTROL_PERMANENT"

    #@18f
    const/16 v2, 0x20

    #@191
    const/16 v3, 0x39

    #@193
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@196
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->USIM_CALL_CONTROL_PERMANENT:Lcom/android/internal/telephony/cat/ResultCode;

    #@198
    .line 152
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@19a
    const-string v1, "BIP_ERROR"

    #@19c
    const/16 v2, 0x21

    #@19e
    const/16 v3, 0x3a

    #@1a0
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@1a3
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->BIP_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@1a5
    .line 155
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@1a7
    const-string v1, "ACCESS_TECH_UNABLE_TO_PROCESS"

    #@1a9
    const/16 v2, 0x22

    #@1ab
    const/16 v3, 0x3b

    #@1ad
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@1b0
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->ACCESS_TECH_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    #@1b2
    .line 158
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@1b4
    const-string v1, "FRAMES_ERROR"

    #@1b6
    const/16 v2, 0x23

    #@1b8
    const/16 v3, 0x3c

    #@1ba
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@1bd
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->FRAMES_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@1bf
    .line 161
    new-instance v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@1c1
    const-string v1, "MMS_ERROR"

    #@1c3
    const/16 v2, 0x24

    #@1c5
    const/16 v3, 0x3d

    #@1c7
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/ResultCode;-><init>(Ljava/lang/String;II)V

    #@1ca
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->MMS_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@1cc
    .line 27
    const/16 v0, 0x25

    #@1ce
    new-array v0, v0, [Lcom/android/internal/telephony/cat/ResultCode;

    #@1d0
    sget-object v1, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@1d2
    aput-object v1, v0, v4

    #@1d4
    sget-object v1, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_WITH_PARTIAL_COMPREHENSION:Lcom/android/internal/telephony/cat/ResultCode;

    #@1d6
    aput-object v1, v0, v5

    #@1d8
    sget-object v1, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_WITH_MISSING_INFO:Lcom/android/internal/telephony/cat/ResultCode;

    #@1da
    aput-object v1, v0, v6

    #@1dc
    sget-object v1, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_WITH_ADDITIONAL_EFS_READ:Lcom/android/internal/telephony/cat/ResultCode;

    #@1de
    aput-object v1, v0, v7

    #@1e0
    sget-object v1, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_ICON_NOT_DISPLAYED:Lcom/android/internal/telephony/cat/ResultCode;

    #@1e2
    aput-object v1, v0, v8

    #@1e4
    const/4 v1, 0x5

    #@1e5
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_MODIFIED_BY_NAA:Lcom/android/internal/telephony/cat/ResultCode;

    #@1e7
    aput-object v2, v0, v1

    #@1e9
    const/4 v1, 0x6

    #@1ea
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_LIMITED_SERVICE:Lcom/android/internal/telephony/cat/ResultCode;

    #@1ec
    aput-object v2, v0, v1

    #@1ee
    const/4 v1, 0x7

    #@1ef
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_WITH_MODIFICATION:Lcom/android/internal/telephony/cat/ResultCode;

    #@1f1
    aput-object v2, v0, v1

    #@1f3
    const/16 v1, 0x8

    #@1f5
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_NAA_NOT_ACTIVE:Lcom/android/internal/telephony/cat/ResultCode;

    #@1f7
    aput-object v2, v0, v1

    #@1f9
    const/16 v1, 0x9

    #@1fb
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_TONE_NOT_PLAYED:Lcom/android/internal/telephony/cat/ResultCode;

    #@1fd
    aput-object v2, v0, v1

    #@1ff
    const/16 v1, 0xa

    #@201
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->UICC_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    #@203
    aput-object v2, v0, v1

    #@205
    const/16 v1, 0xb

    #@207
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->BACKWARD_MOVE_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    #@209
    aput-object v2, v0, v1

    #@20b
    const/16 v1, 0xc

    #@20d
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->NO_RESPONSE_FROM_USER:Lcom/android/internal/telephony/cat/ResultCode;

    #@20f
    aput-object v2, v0, v1

    #@211
    const/16 v1, 0xd

    #@213
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->HELP_INFO_REQUIRED:Lcom/android/internal/telephony/cat/ResultCode;

    #@215
    aput-object v2, v0, v1

    #@217
    const/16 v1, 0xe

    #@219
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->USSD_SS_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    #@21b
    aput-object v2, v0, v1

    #@21d
    const/16 v1, 0xf

    #@21f
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->TERMINAL_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    #@221
    aput-object v2, v0, v1

    #@223
    const/16 v1, 0x10

    #@225
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->NETWORK_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    #@227
    aput-object v2, v0, v1

    #@229
    const/16 v1, 0x11

    #@22b
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->USER_NOT_ACCEPT:Lcom/android/internal/telephony/cat/ResultCode;

    #@22d
    aput-object v2, v0, v1

    #@22f
    const/16 v1, 0x12

    #@231
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->USER_CLEAR_DOWN_CALL:Lcom/android/internal/telephony/cat/ResultCode;

    #@233
    aput-object v2, v0, v1

    #@235
    const/16 v1, 0x13

    #@237
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->CONTRADICTION_WITH_TIMER:Lcom/android/internal/telephony/cat/ResultCode;

    #@239
    aput-object v2, v0, v1

    #@23b
    const/16 v1, 0x14

    #@23d
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->NAA_CALL_CONTROL_TEMPORARY:Lcom/android/internal/telephony/cat/ResultCode;

    #@23f
    aput-object v2, v0, v1

    #@241
    const/16 v1, 0x15

    #@243
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->LAUNCH_BROWSER_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@245
    aput-object v2, v0, v1

    #@247
    const/16 v1, 0x16

    #@249
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->MMS_TEMPORARY:Lcom/android/internal/telephony/cat/ResultCode;

    #@24b
    aput-object v2, v0, v1

    #@24d
    const/16 v1, 0x17

    #@24f
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    #@251
    aput-object v2, v0, v1

    #@253
    const/16 v1, 0x18

    #@255
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->CMD_TYPE_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@257
    aput-object v2, v0, v1

    #@259
    const/16 v1, 0x19

    #@25b
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@25d
    aput-object v2, v0, v1

    #@25f
    const/16 v1, 0x1a

    #@261
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->CMD_NUM_NOT_KNOWN:Lcom/android/internal/telephony/cat/ResultCode;

    #@263
    aput-object v2, v0, v1

    #@265
    const/16 v1, 0x1b

    #@267
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->SS_RETURN_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@269
    aput-object v2, v0, v1

    #@26b
    const/16 v1, 0x1c

    #@26d
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->SMS_RP_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@26f
    aput-object v2, v0, v1

    #@271
    const/16 v1, 0x1d

    #@273
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->REQUIRED_VALUES_MISSING:Lcom/android/internal/telephony/cat/ResultCode;

    #@275
    aput-object v2, v0, v1

    #@277
    const/16 v1, 0x1e

    #@279
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->USSD_RETURN_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@27b
    aput-object v2, v0, v1

    #@27d
    const/16 v1, 0x1f

    #@27f
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->MULTI_CARDS_CMD_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@281
    aput-object v2, v0, v1

    #@283
    const/16 v1, 0x20

    #@285
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->USIM_CALL_CONTROL_PERMANENT:Lcom/android/internal/telephony/cat/ResultCode;

    #@287
    aput-object v2, v0, v1

    #@289
    const/16 v1, 0x21

    #@28b
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->BIP_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@28d
    aput-object v2, v0, v1

    #@28f
    const/16 v1, 0x22

    #@291
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->ACCESS_TECH_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    #@293
    aput-object v2, v0, v1

    #@295
    const/16 v1, 0x23

    #@297
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->FRAMES_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@299
    aput-object v2, v0, v1

    #@29b
    const/16 v1, 0x24

    #@29d
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->MMS_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    #@29f
    aput-object v2, v0, v1

    #@2a1
    sput-object v0, Lcom/android/internal/telephony/cat/ResultCode;->$VALUES:[Lcom/android/internal/telephony/cat/ResultCode;

    #@2a3
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "code"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 167
    iput p3, p0, Lcom/android/internal/telephony/cat/ResultCode;->mCode:I

    #@5
    .line 168
    return-void
.end method

.method public static fromInt(I)Lcom/android/internal/telephony/cat/ResultCode;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 179
    invoke-static {}, Lcom/android/internal/telephony/cat/ResultCode;->values()[Lcom/android/internal/telephony/cat/ResultCode;

    #@3
    move-result-object v0

    #@4
    .local v0, arr$:[Lcom/android/internal/telephony/cat/ResultCode;
    array-length v2, v0

    #@5
    .local v2, len$:I
    const/4 v1, 0x0

    #@6
    .local v1, i$:I
    :goto_6
    if-ge v1, v2, :cond_12

    #@8
    aget-object v3, v0, v1

    #@a
    .line 180
    .local v3, r:Lcom/android/internal/telephony/cat/ResultCode;
    iget v4, v3, Lcom/android/internal/telephony/cat/ResultCode;->mCode:I

    #@c
    if-ne v4, p0, :cond_f

    #@e
    .line 184
    .end local v3           #r:Lcom/android/internal/telephony/cat/ResultCode;
    :goto_e
    return-object v3

    #@f
    .line 179
    .restart local v3       #r:Lcom/android/internal/telephony/cat/ResultCode;
    :cond_f
    add-int/lit8 v1, v1, 0x1

    #@11
    goto :goto_6

    #@12
    .line 184
    .end local v3           #r:Lcom/android/internal/telephony/cat/ResultCode;
    :cond_12
    const/4 v3, 0x0

    #@13
    goto :goto_e
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/cat/ResultCode;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 27
    const-class v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/cat/ResultCode;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/cat/ResultCode;
    .registers 1

    #@0
    .prologue
    .line 27
    sget-object v0, Lcom/android/internal/telephony/cat/ResultCode;->$VALUES:[Lcom/android/internal/telephony/cat/ResultCode;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/cat/ResultCode;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/cat/ResultCode;

    #@8
    return-object v0
.end method


# virtual methods
.method public value()I
    .registers 2

    #@0
    .prologue
    .line 175
    iget v0, p0, Lcom/android/internal/telephony/cat/ResultCode;->mCode:I

    #@2
    return v0
.end method
