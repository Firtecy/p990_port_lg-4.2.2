.class Lcom/android/internal/telephony/uicc/SIMRecords$3;
.super Ljava/lang/Object;
.source "SIMRecords.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/telephony/uicc/SIMRecords;->onRecordLoaded()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/uicc/SIMRecords;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/uicc/SIMRecords;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2828
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/SIMRecords$3;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 8
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2831
    const/4 v1, -0x1

    #@2
    if-ne p2, v1, :cond_9c

    #@4
    .line 2832
    const-string v1, "GSM"

    #@6
    const-string v2, "[LGE][SBP] Send new Intent - CUST_CHNAGED_INFO"

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 2835
    const-string v1, "3"

    #@d
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords$3;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@f
    iget v2, v2, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@11
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_8d

    #@1b
    .line 2836
    const-string v1, "persist.radio.first-mccmnc"

    #@1d
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords$3;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@1f
    iget-object v2, v2, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@21
    const/4 v3, 0x6

    #@22
    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 2842
    :goto_29
    new-instance v0, Landroid/content/Intent;

    #@2b
    const-string v1, "com.lge.action.CUST_CHANGED_INFO"

    #@2d
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@30
    .line 2843
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "cust_old_path"

    #@32
    const-string v2, "ro.lge.capp_cupss.rootdir"

    #@34
    const-string v3, "/cust"

    #@36
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3d
    .line 2844
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords$3;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@3f
    iget-object v1, v1, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@41
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@44
    .line 2845
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords$3;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@46
    new-instance v2, Landroid/app/AlertDialog$Builder;

    #@48
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/SIMRecords$3;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@4a
    iget-object v3, v3, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@4c
    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@4f
    const-string v3, "Please Wait..."

    #@51
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@54
    move-result-object v2

    #@55
    const-string v3, "Device is getting update with new Configuration"

    #@57
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@5e
    move-result-object v2

    #@5f
    invoke-static {v1, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$002(Lcom/android/internal/telephony/uicc/SIMRecords;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    #@62
    .line 2850
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords$3;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@64
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$000(Lcom/android/internal/telephony/uicc/SIMRecords;)Landroid/app/AlertDialog;

    #@67
    move-result-object v1

    #@68
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@6b
    move-result-object v1

    #@6c
    const/16 v2, 0x7d3

    #@6e
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@71
    .line 2851
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords$3;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@73
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$000(Lcom/android/internal/telephony/uicc/SIMRecords;)Landroid/app/AlertDialog;

    #@76
    move-result-object v1

    #@77
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    #@7a
    .line 2852
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords$3;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@7c
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$000(Lcom/android/internal/telephony/uicc/SIMRecords;)Landroid/app/AlertDialog;

    #@7f
    move-result-object v1

    #@80
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    #@83
    .line 2853
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords$3;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@85
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$000(Lcom/android/internal/telephony/uicc/SIMRecords;)Landroid/app/AlertDialog;

    #@88
    move-result-object v1

    #@89
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@8c
    .line 2859
    .end local v0           #intent:Landroid/content/Intent;
    :goto_8c
    return-void

    #@8d
    .line 2839
    :cond_8d
    const-string v1, "persist.radio.first-mccmnc"

    #@8f
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords$3;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@91
    iget-object v2, v2, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@93
    const/4 v3, 0x5

    #@94
    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@97
    move-result-object v2

    #@98
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9b
    goto :goto_29

    #@9c
    .line 2857
    :cond_9c
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    #@9f
    goto :goto_8c
.end method
