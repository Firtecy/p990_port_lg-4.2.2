.class public Lcom/android/internal/telephony/DefaultPhoneNotifier;
.super Ljava/lang/Object;
.source "DefaultPhoneNotifier.java"

# interfaces
.implements Lcom/android/internal/telephony/PhoneNotifier;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/DefaultPhoneNotifier$1;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field static final LOG_TAG:Ljava/lang/String; = "GSM"


# instance fields
.field protected mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;


# direct methods
.method protected constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    const-string v0, "telephony.registry"

    #@5
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@8
    move-result-object v0

    #@9
    invoke-static {v0}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephonyRegistry;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@f
    .line 45
    return-void
.end method

.method public static convertCallState(Lcom/android/internal/telephony/PhoneConstants$State;)I
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 203
    sget-object v0, Lcom/android/internal/telephony/DefaultPhoneNotifier$1;->$SwitchMap$com$android$internal$telephony$PhoneConstants$State:[I

    #@2
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneConstants$State;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_12

    #@b
    .line 209
    const/4 v0, 0x0

    #@c
    :goto_c
    return v0

    #@d
    .line 205
    :pswitch_d
    const/4 v0, 0x1

    #@e
    goto :goto_c

    #@f
    .line 207
    :pswitch_f
    const/4 v0, 0x2

    #@10
    goto :goto_c

    #@11
    .line 203
    nop

    #@12
    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_d
        :pswitch_f
    .end packed-switch
.end method

.method public static convertCallState(I)Lcom/android/internal/telephony/PhoneConstants$State;
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 218
    packed-switch p0, :pswitch_data_c

    #@3
    .line 224
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@5
    :goto_5
    return-object v0

    #@6
    .line 220
    :pswitch_6
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$State;->RINGING:Lcom/android/internal/telephony/PhoneConstants$State;

    #@8
    goto :goto_5

    #@9
    .line 222
    :pswitch_9
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@b
    goto :goto_5

    #@c
    .line 218
    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
    .end packed-switch
.end method

.method public static convertDataActivityState(Lcom/android/internal/telephony/Phone$DataActivityState;)I
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 267
    sget-object v0, Lcom/android/internal/telephony/DefaultPhoneNotifier$1;->$SwitchMap$com$android$internal$telephony$Phone$DataActivityState:[I

    #@2
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone$DataActivityState;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_16

    #@b
    .line 277
    const/4 v0, 0x0

    #@c
    :goto_c
    return v0

    #@d
    .line 269
    :pswitch_d
    const/4 v0, 0x1

    #@e
    goto :goto_c

    #@f
    .line 271
    :pswitch_f
    const/4 v0, 0x2

    #@10
    goto :goto_c

    #@11
    .line 273
    :pswitch_11
    const/4 v0, 0x3

    #@12
    goto :goto_c

    #@13
    .line 275
    :pswitch_13
    const/4 v0, 0x4

    #@14
    goto :goto_c

    #@15
    .line 267
    nop

    #@16
    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_d
        :pswitch_f
        :pswitch_11
        :pswitch_13
    .end packed-switch
.end method

.method public static convertDataActivityState(I)Lcom/android/internal/telephony/Phone$DataActivityState;
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 286
    packed-switch p0, :pswitch_data_12

    #@3
    .line 296
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->NONE:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@5
    :goto_5
    return-object v0

    #@6
    .line 288
    :pswitch_6
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAIN:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@8
    goto :goto_5

    #@9
    .line 290
    :pswitch_9
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@b
    goto :goto_5

    #@c
    .line 292
    :pswitch_c
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAINANDOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@e
    goto :goto_5

    #@f
    .line 294
    :pswitch_f
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DORMANT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@11
    goto :goto_5

    #@12
    .line 286
    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
    .end packed-switch
.end method

.method public static convertDataState(Lcom/android/internal/telephony/PhoneConstants$DataState;)I
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 233
    sget-object v0, Lcom/android/internal/telephony/DefaultPhoneNotifier$1;->$SwitchMap$com$android$internal$telephony$PhoneConstants$DataState:[I

    #@2
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneConstants$DataState;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_14

    #@b
    .line 241
    const/4 v0, 0x0

    #@c
    :goto_c
    return v0

    #@d
    .line 235
    :pswitch_d
    const/4 v0, 0x1

    #@e
    goto :goto_c

    #@f
    .line 237
    :pswitch_f
    const/4 v0, 0x2

    #@10
    goto :goto_c

    #@11
    .line 239
    :pswitch_11
    const/4 v0, 0x3

    #@12
    goto :goto_c

    #@13
    .line 233
    nop

    #@14
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_d
        :pswitch_f
        :pswitch_11
    .end packed-switch
.end method

.method public static convertDataState(I)Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 250
    packed-switch p0, :pswitch_data_10

    #@3
    .line 258
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@5
    :goto_5
    return-object v0

    #@6
    .line 252
    :pswitch_6
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTING:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@8
    goto :goto_5

    #@9
    .line 254
    :pswitch_9
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@b
    goto :goto_5

    #@c
    .line 256
    :pswitch_c
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->SUSPENDED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@e
    goto :goto_5

    #@f
    .line 250
    nop

    #@10
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
        :pswitch_c
    .end packed-switch
.end method

.method private doNotifyDataConnection(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;Lcom/android/internal/telephony/DataConnection$FailCause;)V
    .registers 20
    .parameter "sender"
    .parameter "reason"
    .parameter "apnType"
    .parameter "state"
    .parameter "cause"

    #@0
    .prologue
    .line 128
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v13

    #@4
    .line 129
    .local v13, telephony:Landroid/telephony/TelephonyManager;
    const/4 v7, 0x0

    #@5
    .line 130
    .local v7, linkProperties:Landroid/net/LinkProperties;
    const/4 v8, 0x0

    #@6
    .line 131
    .local v8, linkCapabilities:Landroid/net/LinkCapabilities;
    const/4 v10, 0x0

    #@7
    .line 133
    .local v10, roaming:Z
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@9
    move-object/from16 v0, p4

    #@b
    if-ne v0, v1, :cond_19

    #@d
    .line 134
    move-object/from16 v0, p3

    #@f
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->getLinkProperties(Ljava/lang/String;)Landroid/net/LinkProperties;

    #@12
    move-result-object v7

    #@13
    .line 135
    move-object/from16 v0, p3

    #@15
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->getLinkCapabilities(Ljava/lang/String;)Landroid/net/LinkCapabilities;

    #@18
    move-result-object v8

    #@19
    .line 137
    :cond_19
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@1c
    move-result-object v12

    #@1d
    .line 138
    .local v12, ss:Landroid/telephony/ServiceState;
    if-eqz v12, :cond_23

    #@1f
    invoke-virtual {v12}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@22
    move-result v10

    #@23
    .line 141
    :cond_23
    :try_start_23
    iget-object v1, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@25
    invoke-static/range {p4 .. p4}, Lcom/android/internal/telephony/DefaultPhoneNotifier;->convertDataState(Lcom/android/internal/telephony/PhoneConstants$DataState;)I

    #@28
    move-result v2

    #@29
    move-object/from16 v0, p3

    #@2b
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->isDataConnectivityPossible(Ljava/lang/String;)Z

    #@2e
    move-result v3

    #@2f
    move-object/from16 v0, p3

    #@31
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->getActiveApnHost(Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    if-eqz v13, :cond_49

    #@37
    invoke-virtual {v13}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@3a
    move-result v9

    #@3b
    :goto_3b
    if-eqz p5, :cond_4b

    #@3d
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/DataConnection$FailCause;->getErrorCode()I

    #@40
    move-result v11

    #@41
    :goto_41
    move-object/from16 v4, p2

    #@43
    move-object/from16 v6, p3

    #@45
    invoke-interface/range {v1 .. v11}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifyDataConnection(IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;IZI)V
    :try_end_48
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_48} :catch_4d

    #@48
    .line 158
    :goto_48
    return-void

    #@49
    .line 141
    :cond_49
    const/4 v9, 0x0

    #@4a
    goto :goto_3b

    #@4b
    :cond_4b
    const/4 v11, 0x0

    #@4c
    goto :goto_41

    #@4d
    .line 155
    :catch_4d
    move-exception v1

    #@4e
    goto :goto_48
.end method

.method private log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 195
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[PhoneNotifier] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 196
    return-void
.end method


# virtual methods
.method public notifyCallForwardingChanged(Lcom/android/internal/telephony/Phone;)V
    .registers 4
    .parameter "sender"

    #@0
    .prologue
    .line 91
    :try_start_0
    iget-object v0, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@2
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getCallForwardingIndicator()Z

    #@5
    move-result v1

    #@6
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifyCallForwardingChanged(Z)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 95
    :goto_9
    return-void

    #@a
    .line 92
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public notifyCellInfo(Lcom/android/internal/telephony/Phone;Ljava/util/List;)V
    .registers 4
    .parameter "sender"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/Phone;",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 180
    .local p2, cellInfo:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    :try_start_0
    iget-object v0, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@2
    invoke-interface {v0, p2}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifyCellInfo(Ljava/util/List;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 184
    :goto_5
    return-void

    #@6
    .line 181
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public notifyCellLocation(Lcom/android/internal/telephony/Phone;)V
    .registers 4
    .parameter "sender"

    #@0
    .prologue
    .line 169
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 170
    .local v0, data:Landroid/os/Bundle;
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getCellLocation()Landroid/telephony/CellLocation;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v1, v0}, Landroid/telephony/CellLocation;->fillInNotifierBundle(Landroid/os/Bundle;)V

    #@c
    .line 172
    :try_start_c
    iget-object v1, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@e
    invoke-interface {v1, v0}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifyCellLocation(Landroid/os/Bundle;)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_11} :catch_12

    #@11
    .line 176
    :goto_11
    return-void

    #@12
    .line 173
    :catch_12
    move-exception v1

    #@13
    goto :goto_11
.end method

.method public notifyDataActivity(Lcom/android/internal/telephony/Phone;)V
    .registers 4
    .parameter "sender"

    #@0
    .prologue
    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@2
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getDataActivityState()Lcom/android/internal/telephony/Phone$DataActivityState;

    #@5
    move-result-object v1

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/DefaultPhoneNotifier;->convertDataActivityState(Lcom/android/internal/telephony/Phone$DataActivityState;)I

    #@9
    move-result v1

    #@a
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifyDataActivity(I)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_e

    #@d
    .line 103
    :goto_d
    return-void

    #@e
    .line 100
    :catch_e
    move-exception v0

    #@f
    goto :goto_d
.end method

.method public notifyDataConnection(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;)V
    .registers 11
    .parameter "sender"
    .parameter "reason"
    .parameter "apnType"
    .parameter "state"

    #@0
    .prologue
    .line 108
    sget-object v5, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move-object v4, p4

    #@7
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/DefaultPhoneNotifier;->doNotifyDataConnection(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@a
    .line 111
    return-void
.end method

.method public notifyDataConnection(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;Lcom/android/internal/telephony/DataConnection$FailCause;)V
    .registers 6
    .parameter "sender"
    .parameter "reason"
    .parameter "apnType"
    .parameter "state"
    .parameter "cause"

    #@0
    .prologue
    .line 116
    invoke-direct/range {p0 .. p5}, Lcom/android/internal/telephony/DefaultPhoneNotifier;->doNotifyDataConnection(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@3
    .line 117
    return-void
.end method

.method public notifyDataConnectionFailed(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "sender"
    .parameter "reason"
    .parameter "apnType"

    #@0
    .prologue
    .line 162
    :try_start_0
    iget-object v0, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@2
    invoke-interface {v0, p2, p3}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifyDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 166
    :goto_5
    return-void

    #@6
    .line 163
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public notifyMessageWaitingChanged(Lcom/android/internal/telephony/Phone;)V
    .registers 4
    .parameter "sender"

    #@0
    .prologue
    .line 83
    :try_start_0
    iget-object v0, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@2
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getMessageWaitingIndicator()Z

    #@5
    move-result v1

    #@6
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifyMessageWaitingChanged(Z)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 87
    :goto_9
    return-void

    #@a
    .line 84
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public notifyOtaspChanged(Lcom/android/internal/telephony/Phone;I)V
    .registers 4
    .parameter "sender"
    .parameter "otaspMode"

    #@0
    .prologue
    .line 188
    :try_start_0
    iget-object v0, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@2
    invoke-interface {v0, p2}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifyOtaspChanged(I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 192
    :goto_5
    return-void

    #@6
    .line 189
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public notifyPhoneState(Lcom/android/internal/telephony/Phone;)V
    .registers 6
    .parameter "sender"

    #@0
    .prologue
    .line 48
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@3
    move-result-object v1

    #@4
    .line 49
    .local v1, ringingCall:Lcom/android/internal/telephony/Call;
    const-string v0, ""

    #@6
    .line 50
    .local v0, incomingNumber:Ljava/lang/String;
    if-eqz v1, :cond_16

    #@8
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getEarliestConnection()Lcom/android/internal/telephony/Connection;

    #@b
    move-result-object v2

    #@c
    if-eqz v2, :cond_16

    #@e
    .line 51
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getEarliestConnection()Lcom/android/internal/telephony/Connection;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 54
    :cond_16
    :try_start_16
    iget-object v2, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@18
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v3}, Lcom/android/internal/telephony/DefaultPhoneNotifier;->convertCallState(Lcom/android/internal/telephony/PhoneConstants$State;)I

    #@1f
    move-result v3

    #@20
    invoke-interface {v2, v3, v0}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifyCallState(ILjava/lang/String;)V
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_23} :catch_24

    #@23
    .line 58
    :goto_23
    return-void

    #@24
    .line 55
    :catch_24
    move-exception v2

    #@25
    goto :goto_23
.end method

.method public notifyServiceState(Lcom/android/internal/telephony/Phone;)V
    .registers 4
    .parameter "sender"

    #@0
    .prologue
    .line 61
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@3
    move-result-object v0

    #@4
    .line 62
    .local v0, ss:Landroid/telephony/ServiceState;
    if-nez v0, :cond_e

    #@6
    .line 63
    new-instance v0, Landroid/telephony/ServiceState;

    #@8
    .end local v0           #ss:Landroid/telephony/ServiceState;
    invoke-direct {v0}, Landroid/telephony/ServiceState;-><init>()V

    #@b
    .line 64
    .restart local v0       #ss:Landroid/telephony/ServiceState;
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@e
    .line 67
    :cond_e
    :try_start_e
    iget-object v1, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@10
    invoke-interface {v1, v0}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifyServiceState(Landroid/telephony/ServiceState;)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_13} :catch_14

    #@13
    .line 71
    :goto_13
    return-void

    #@14
    .line 68
    :catch_14
    move-exception v1

    #@15
    goto :goto_13
.end method

.method public notifySignalStrength(Lcom/android/internal/telephony/Phone;)V
    .registers 4
    .parameter "sender"

    #@0
    .prologue
    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/android/internal/telephony/DefaultPhoneNotifier;->mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@2
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getSignalStrength()Landroid/telephony/SignalStrength;

    #@5
    move-result-object v1

    #@6
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifySignalStrength(Landroid/telephony/SignalStrength;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 79
    :goto_9
    return-void

    #@a
    .line 76
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method
