.class public Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;
.super Lcom/android/internal/telephony/SMSDispatcher;
.source "CdmaSMSDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;
    }
.end annotation


# static fields
.field public static final ADDRESS:Ljava/lang/String; = "address"

.field private static final ALLRECEIVE_MODE:B = 0x3t

.field private static final APP_DIRECTED_SMS_FORMATTED:I = 0x0

.field private static final APP_DIRECTED_SMS_NORMAL:I = -0x1

.field private static final APP_DIRECTED_SMS_PROCESSED:I = 0x1

.field private static final APP_DIRECTED_SMS_TAG:Ljava/lang/String; = "APP_DIRECTED_SMS"

.field private static final CBS_SESSION_MAX:I = 0xa

.field private static final COMMERCIAL_MODE:B = 0x0t

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final DATE:Ljava/lang/String; = "date"

.field private static final DELIMETER:I = 0x1d

.field private static final EMERGENCY_MSG:I = 0x2be

.field private static final EMERGENCY_MSG_START_SEQ:I = 0x0

.field private static final END_DELIMETER:I = 0x3

.field private static final IMS_MSG:I = 0x7000

.field private static final KDDITEST_MODE:B = 0x2t

.field private static final LGE_SMS_CBS_ENABLED:I = 0x1

.field public static final LMS_MAX_COUNT:I = 0x32

.field public static final LMS_MAX_SEGMENT:I = 0x3

.field public static final LMS_URI:Landroid/net/Uri; = null

.field private static final MANUFACTURETEST_MODE:B = 0x1t

.field public static final PDU:Ljava/lang/String; = "pdu"

.field public static final REFERENCE_NUMBER:Ljava/lang/String; = "reference_number"

.field public static final SEQUENCE:Ljava/lang/String; = "sequence"

.field private static final SEQ_EOS:I = 0x1

.field private static final SMS_RECEIVED_TYPE_ACCOUNT_IS_ADDED:Ljava/lang/String; = "VZWSIP"

.field private static final SMS_RECEIVED_TYPE_COMPLETE_SYNC_INBOX:Ljava/lang/String; = "VZWSCI"

.field private static final SMS_RECEIVED_TYPE_NEW_MAIL:Ljava/lang/String; = "VZWNMN"

.field private static final SMS_RECEIVED_TYPE_NOTIFICATION_FOR_CHANGES:Ljava/lang/String; = "VZWUEP"

.field private static final SMS_RECEIVED_TYPE_NOTIFICATION_FOR_DEACTIVATION:Ljava/lang/String; = "VZWRSC"

.field private static final SMS_RECEIVED_TYPE_PIN_NOTIFICATION:Ljava/lang/String; = "VZWPIN"

.field public static final SOURCE_MIN:Ljava/lang/String; = "source_min"

.field private static final TAG:Ljava/lang/String; = "CDMA"

.field public static final TID:Ljava/lang/String; = "tid"

.field private static final URL_DELIMETER:I = 0x1d

.field private static msgref:I = 0x0

.field private static final prefixVZW:Ljava/lang/String; = "//VZW"

.field private static sSessionIdLink:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static sSessionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final APPLICATION_PERMISSION:Ljava/lang/String;

.field IMS_AUTHORITY:Ljava/lang/String;

.field IMS_CONTENT_URI:Landroid/net/Uri;

.field public final METADATA_NAME:Ljava/lang/String;

.field private SIGNATURES:[Landroid/content/pm/Signature;

.field SMS_OVER_IP_NETWORK:Ljava/lang/String;

.field TABLE_NAME:Ljava/lang/String;

.field private VZWSignature:[Landroid/content/pm/Signature;

.field private cdmaDanSentReceiver:Landroid/content/BroadcastReceiver;

.field private cdmaDanStatusReportReceiver:Landroid/content/BroadcastReceiver;

.field private cdmaDomainNotificationReceiver:Landroid/content/BroadcastReceiver;

.field private domainStatusCsIms:I

.field private domainStatusCsOnly:I

.field private domainStatusImsOnly:I

.field private final mCheckForDuplicatePortsInOmadmWapPush:Z

.field private mImsSMSDispatcher:Lcom/android/internal/telephony/ImsSMSDispatcher;

.field private mLastAcknowledgedSmsFingerprint:[B

.field private mLastDispatchedSmsFingerprint:[B

.field private mResultCode:I

.field private final mScpResultsReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 141
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->msgref:I

    #@3
    .line 146
    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@5
    const-string v1, "raw"

    #@7
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@a
    move-result-object v0

    #@b
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@d
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;Lcom/android/internal/telephony/ImsSMSDispatcher;)V
    .registers 12
    .parameter "phone"
    .parameter "storageMonitor"
    .parameter "usageMonitor"
    .parameter "imsSMSDispatcher"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 211
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/SMSDispatcher;-><init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;)V

    #@5
    .line 177
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@8
    move-result-object v3

    #@9
    const v4, 0x111003b

    #@c
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@f
    move-result v3

    #@10
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mCheckForDuplicatePortsInOmadmWapPush:Z

    #@12
    .line 188
    const-string v3, "lgims_com_vzw_service_sms"

    #@14
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->TABLE_NAME:Ljava/lang/String;

    #@16
    .line 189
    const-string v3, "com.lge.ims.provider.lgims"

    #@18
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->IMS_AUTHORITY:Ljava/lang/String;

    #@1a
    .line 190
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "content://"

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->IMS_AUTHORITY:Ljava/lang/String;

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    const-string v4, "/"

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->TABLE_NAME:Ljava/lang/String;

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@3e
    move-result-object v3

    #@3f
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->IMS_CONTENT_URI:Landroid/net/Uri;

    #@41
    .line 191
    const-string v3, "sdm_sms_over_ip_network"

    #@43
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SMS_OVER_IP_NETWORK:Ljava/lang/String;

    #@45
    .line 1127
    const-string v3, "com.verizon.permissions.appdirectedsms"

    #@47
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->APPLICATION_PERMISSION:Ljava/lang/String;

    #@49
    .line 1128
    const-string v3, "com.verizon.directedAppSMS"

    #@4b
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->METADATA_NAME:Ljava/lang/String;

    #@4d
    .line 1350
    new-instance v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$1;

    #@4f
    invoke-direct {v3, p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$1;-><init>(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)V

    #@52
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mScpResultsReceiver:Landroid/content/BroadcastReceiver;

    #@54
    .line 2568
    new-instance v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$2;

    #@56
    invoke-direct {v3, p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$2;-><init>(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)V

    #@59
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->cdmaDomainNotificationReceiver:Landroid/content/BroadcastReceiver;

    #@5b
    .line 2581
    new-instance v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$3;

    #@5d
    invoke-direct {v3, p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$3;-><init>(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)V

    #@60
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->cdmaDanSentReceiver:Landroid/content/BroadcastReceiver;

    #@62
    .line 2597
    new-instance v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$4;

    #@64
    invoke-direct {v3, p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$4;-><init>(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)V

    #@67
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->cdmaDanStatusReportReceiver:Landroid/content/BroadcastReceiver;

    #@69
    .line 2613
    const/4 v3, 0x0

    #@6a
    iput v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->domainStatusCsOnly:I

    #@6c
    .line 2614
    iput v5, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->domainStatusImsOnly:I

    #@6e
    .line 2615
    const/4 v3, 0x2

    #@6f
    iput v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->domainStatusCsIms:I

    #@71
    .line 212
    iput-object p4, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mImsSMSDispatcher:Lcom/android/internal/telephony/ImsSMSDispatcher;

    #@73
    .line 213
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@75
    invoke-interface {v3, p0, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->setOnNewCdmaSms(Landroid/os/Handler;ILjava/lang/Object;)V

    #@78
    .line 214
    const-string v3, "CdmaSMSDispatcher(), created"

    #@7a
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7d
    .line 216
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@7f
    const/16 v4, 0x15

    #@81
    invoke-interface {v3, p0, v4, v6}, Lcom/android/internal/telephony/CommandsInterface;->setOnDanStatus(Landroid/os/Handler;ILjava/lang/Object;)V

    #@84
    .line 219
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@86
    const-string v4, "lgu_dispatch"

    #@88
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8b
    move-result v3

    #@8c
    if-ne v3, v5, :cond_9c

    #@8e
    .line 220
    new-instance v3, Ljava/util/HashMap;

    #@90
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    #@93
    sput-object v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sSessionMap:Ljava/util/HashMap;

    #@95
    .line 221
    new-instance v3, Ljava/util/LinkedList;

    #@97
    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    #@9a
    sput-object v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sSessionIdLink:Ljava/util/LinkedList;

    #@9c
    .line 226
    :cond_9c
    const-string v3, "kddi_domain_notification"

    #@9e
    invoke-static {v6, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a1
    move-result v3

    #@a2
    if-ne v3, v5, :cond_d7

    #@a4
    .line 227
    new-instance v0, Landroid/content/IntentFilter;

    #@a6
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@a9
    .line 228
    .local v0, filter_DAN_send:Landroid/content/IntentFilter;
    const-string v3, "com.lge.ims.action.DOMAIN_NOTIFICATION"

    #@ab
    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@ae
    .line 229
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@b0
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->cdmaDomainNotificationReceiver:Landroid/content/BroadcastReceiver;

    #@b2
    invoke-virtual {v3, v4, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@b5
    .line 230
    new-instance v1, Landroid/content/IntentFilter;

    #@b7
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@ba
    .line 231
    .local v1, filter_DAN_sent:Landroid/content/IntentFilter;
    new-instance v2, Landroid/content/IntentFilter;

    #@bc
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    #@bf
    .line 232
    .local v2, filter_DAN_status_report:Landroid/content/IntentFilter;
    const-string v3, "com.lge.kddi.intent.action.DAN_SENT"

    #@c1
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c4
    .line 233
    const-string v3, "com.lge.kddi.intent.action.DAN_DELIVERED"

    #@c6
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c9
    .line 234
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@cb
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->cdmaDanSentReceiver:Landroid/content/BroadcastReceiver;

    #@cd
    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@d0
    .line 235
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@d2
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->cdmaDanStatusReportReceiver:Landroid/content/BroadcastReceiver;

    #@d4
    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@d7
    .line 238
    .end local v0           #filter_DAN_send:Landroid/content/IntentFilter;
    .end local v1           #filter_DAN_sent:Landroid/content/IntentFilter;
    .end local v2           #filter_DAN_status_report:Landroid/content/IntentFilter;
    :cond_d7
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)Lcom/android/internal/telephony/CommandsInterface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 134
    iget v0, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->domainStatusCsOnly:I

    #@2
    return v0
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 134
    iget v0, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mResultCode:I

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 134
    iput p1, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mResultCode:I

    #@2
    return p1
.end method

.method private static checkDuplicatePortOmadmWappush([BI)Z
    .registers 8
    .parameter "origPdu"
    .parameter "index"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1307
    add-int/lit8 p1, p1, 0x4

    #@3
    .line 1308
    array-length v5, p0

    #@4
    sub-int/2addr v5, p1

    #@5
    new-array v1, v5, [B

    #@7
    .line 1309
    .local v1, omaPdu:[B
    array-length v5, v1

    #@8
    invoke-static {p0, p1, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@b
    .line 1311
    new-instance v2, Lcom/android/internal/telephony/WspTypeDecoder;

    #@d
    invoke-direct {v2, v1}, Lcom/android/internal/telephony/WspTypeDecoder;-><init>([B)V

    #@10
    .line 1312
    .local v2, pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;
    const/4 v3, 0x2

    #@11
    .line 1315
    .local v3, wspIndex:I
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeUintvarInteger(I)Z

    #@14
    move-result v5

    #@15
    if-nez v5, :cond_18

    #@17
    .line 1330
    :cond_17
    :goto_17
    return v4

    #@18
    .line 1319
    :cond_18
    invoke-virtual {v2}, Lcom/android/internal/telephony/WspTypeDecoder;->getDecodedDataLength()I

    #@1b
    move-result v5

    #@1c
    add-int/2addr v3, v5

    #@1d
    .line 1322
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeContentType(I)Z

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_17

    #@23
    .line 1326
    invoke-virtual {v2}, Lcom/android/internal/telephony/WspTypeDecoder;->getValueString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 1327
    .local v0, mimeType:Ljava/lang/String;
    if-eqz v0, :cond_17

    #@29
    const-string v5, "application/vnd.syncml.notification"

    #@2b
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v5

    #@2f
    if-eqz v5, :cond_17

    #@31
    .line 1328
    const/4 v4, 0x1

    #@32
    goto :goto_17
.end method

.method private checkMmsDuplicated(Ljava/lang/String;)Z
    .registers 12
    .parameter "msgId"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 1717
    const-string v0, "checkMmsDuplicated()"

    #@4
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@7
    .line 1718
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    sget-object v5, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    #@e
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v5, "/inbox"

    #@14
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1f
    move-result-object v1

    #@20
    .line 1719
    .local v1, uri:Landroid/net/Uri;
    const-string v3, "m_id = ?"

    #@22
    .line 1720
    .local v3, where:Ljava/lang/String;
    new-array v4, v8, [Ljava/lang/String;

    #@24
    aput-object p1, v4, v9

    #@26
    .line 1723
    .local v4, whereArgs:[Ljava/lang/String;
    new-array v2, v8, [Ljava/lang/String;

    #@28
    const-string v0, "_id"

    #@2a
    aput-object v0, v2, v9

    #@2c
    .line 1726
    .local v2, projection:[Ljava/lang/String;
    const/4 v6, 0x0

    #@2d
    .line 1728
    .local v6, cursor:Landroid/database/Cursor;
    :try_start_2d
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@2f
    const/4 v5, 0x0

    #@30
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@33
    move-result-object v6

    #@34
    .line 1730
    if-eqz v6, :cond_3c

    #@36
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@39
    move-result v0

    #@3a
    if-nez v0, :cond_48

    #@3c
    .line 1731
    :cond_3c
    const-string v0, "checkMmsDuplicated(), cursor is null or moveToFirst error"

    #@3e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_41
    .catchall {:try_start_2d .. :try_end_41} :catchall_6c
    .catch Landroid/database/SQLException; {:try_start_2d .. :try_end_41} :catch_5c

    #@41
    .line 1744
    if-eqz v6, :cond_46

    #@43
    .line 1745
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@46
    :cond_46
    move v0, v9

    #@47
    .line 1742
    :goto_47
    return v0

    #@48
    .line 1734
    :cond_48
    :try_start_48
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_4b
    .catchall {:try_start_48 .. :try_end_4b} :catchall_6c
    .catch Landroid/database/SQLException; {:try_start_48 .. :try_end_4b} :catch_5c

    #@4b
    move-result v0

    #@4c
    if-lez v0, :cond_55

    #@4e
    .line 1744
    if-eqz v6, :cond_53

    #@50
    .line 1745
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@53
    :cond_53
    move v0, v8

    #@54
    .line 1735
    goto :goto_47

    #@55
    .line 1744
    :cond_55
    if-eqz v6, :cond_5a

    #@57
    .line 1745
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@5a
    :cond_5a
    move v0, v9

    #@5b
    .line 1737
    goto :goto_47

    #@5c
    .line 1739
    :catch_5c
    move-exception v7

    #@5d
    .line 1740
    .local v7, e:Landroid/database/SQLException;
    :try_start_5d
    invoke-virtual {v7}, Landroid/database/SQLException;->printStackTrace()V

    #@60
    .line 1741
    const-string v0, "checkMmsDuplicated(), SQLException occurs"

    #@62
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_65
    .catchall {:try_start_5d .. :try_end_65} :catchall_6c

    #@65
    .line 1744
    if-eqz v6, :cond_6a

    #@67
    .line 1745
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@6a
    :cond_6a
    move v0, v9

    #@6b
    .line 1742
    goto :goto_47

    #@6c
    .line 1744
    .end local v7           #e:Landroid/database/SQLException;
    :catchall_6c
    move-exception v0

    #@6d
    if-eqz v6, :cond_72

    #@6f
    .line 1745
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@72
    .line 1744
    :cond_72
    throw v0
.end method

.method private clearSession(I)V
    .registers 4
    .parameter "sessionId"

    #@0
    .prologue
    .line 2396
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSessionMap()Ljava/util/HashMap;

    #@3
    move-result-object v0

    #@4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    .line 2397
    new-instance v0, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v1, "clearSession(), CBS Cleared Session ="

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@21
    .line 2398
    return-void
.end method

.method private clearSession(Lcom/android/internal/telephony/cdma/SmsMessage;)V
    .registers 4
    .parameter "sms"

    #@0
    .prologue
    .line 2391
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSessionMap()Ljava/util/HashMap;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@7
    move-result-object v1

    #@8
    iget v1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessioinId:I

    #@a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    .line 2392
    new-instance v0, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v1, "clearSession(), CBS Cleared Session ="

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@1f
    move-result-object v1

    #@20
    iget v1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessioinId:I

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@2d
    .line 2393
    return-void
.end method

.method private completeOrInsertLms(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III[B[[BI)I
    .registers 11
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "currentSegment"
    .parameter "totalSegment"
    .parameter "pdu"
    .parameter "pdus"
    .parameter "sameMsgCount"

    #@0
    .prologue
    .line 2123
    add-int/lit8 v1, p8, 0x1

    #@2
    if-ne v1, p5, :cond_17

    #@4
    .line 2125
    add-int/lit8 v1, p4, -0x1

    #@6
    aput-object p6, p7, v1

    #@8
    .line 2126
    move v0, p5

    #@9
    .local v0, loop:I
    :goto_9
    const/4 v1, 0x3

    #@a
    if-ge v0, v1, :cond_12

    #@c
    .line 2127
    const/4 v1, 0x0

    #@d
    aput-object v1, p7, v0

    #@f
    .line 2126
    add-int/lit8 v0, v0, 0x1

    #@11
    goto :goto_9

    #@12
    .line 2129
    :cond_12
    invoke-virtual/range {p0 .. p5}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->deleteCompleteLmsMsg(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III)V

    #@15
    .line 2130
    const/4 v1, -0x1

    #@16
    .line 2133
    .end local v0           #loop:I
    :goto_16
    return v1

    #@17
    .line 2132
    :cond_17
    invoke-virtual/range {p0 .. p6}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->insertLmsMsg(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III[B)V

    #@1a
    .line 2133
    const/4 v1, 0x1

    #@1b
    goto :goto_16
.end method

.method private static decode_Base64([B)[B
    .registers 4
    .parameter "data"

    #@0
    .prologue
    .line 2561
    const-string v1, "CDMA"

    #@2
    const-string v2, "decode_Base64()"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2562
    const/16 v1, 0xdc

    #@9
    new-array v0, v1, [B

    #@b
    .line 2563
    .local v0, b64:[B
    invoke-static {p0}, Lorg/apache/commons/codec/binary/Base64;->decodeBase64([B)[B

    #@e
    move-result-object v0

    #@f
    .line 2564
    return-object v0
.end method

.method private getMmsMsgId(Lcom/android/internal/telephony/cdma/SmsMessage;)Ljava/lang/String;
    .registers 11
    .parameter "sms"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1693
    const-string v5, "getMmsMsgId()"

    #@3
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@6
    .line 1695
    const/4 v3, -0x1

    #@7
    .line 1696
    .local v3, markPos:I
    const/4 v1, 0x0

    #@8
    .line 1697
    .local v1, found:Z
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserData()[B

    #@b
    move-result-object v4

    #@c
    .line 1698
    .local v4, userData:[B
    const/4 v2, 0x0

    #@d
    .local v2, loop:I
    :goto_d
    array-length v5, v4

    #@e
    if-ge v2, v5, :cond_3d

    #@10
    .line 1699
    const/16 v5, 0x4d

    #@12
    aget-byte v7, v4, v2

    #@14
    if-ne v5, v7, :cond_21

    #@16
    const/16 v5, 0x3a

    #@18
    add-int/lit8 v7, v2, 0x1

    #@1a
    aget-byte v7, v4, v7

    #@1c
    if-ne v5, v7, :cond_21

    #@1e
    .line 1700
    const/4 v1, 0x1

    #@1f
    .line 1701
    add-int/lit8 v3, v2, 0x2

    #@21
    .line 1703
    :cond_21
    const/4 v5, 0x1

    #@22
    if-ne v5, v1, :cond_3a

    #@24
    const/16 v5, 0x20

    #@26
    aget-byte v7, v4, v2

    #@28
    if-ne v5, v7, :cond_3a

    #@2a
    .line 1705
    :try_start_2a
    new-instance v5, Ljava/lang/String;

    #@2c
    sub-int v7, v2, v3

    #@2e
    const-string v8, "ksc5601"

    #@30
    invoke-direct {v5, v4, v3, v7, v8}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_33
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2a .. :try_end_33} :catch_34

    #@33
    .line 1713
    :goto_33
    return-object v5

    #@34
    .line 1707
    :catch_34
    move-exception v0

    #@35
    .line 1708
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    #@38
    move-object v5, v6

    #@39
    .line 1709
    goto :goto_33

    #@3a
    .line 1698
    .end local v0           #e:Ljava/io/UnsupportedEncodingException;
    :cond_3a
    add-int/lit8 v2, v2, 0x1

    #@3c
    goto :goto_d

    #@3d
    :cond_3d
    move-object v5, v6

    #@3e
    .line 1713
    goto :goto_33
.end method

.method private getSMSoverIPNetworksIndication()Z
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    .line 1080
    const-string v5, "false"

    #@4
    .line 1081
    .local v5, sms_over_ip_network_indication:Ljava/lang/String;
    const-string v0, "/data/data/com.lge.ims/databases/lgims.db"

    #@6
    .line 1082
    .local v0, IMS_CONFIG_DB:Ljava/lang/String;
    const-string v1, "lgims_com_vzw_service_sms"

    #@8
    .line 1083
    .local v1, LGIMS_SMS_TABLE:Ljava/lang/String;
    const/4 v4, 0x0

    #@9
    .line 1084
    .local v4, imsDB:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    #@a
    .line 1087
    .local v2, cursor:Landroid/database/Cursor;
    const/4 v7, 0x0

    #@b
    const/4 v8, 0x0

    #@c
    :try_start_c
    invoke-static {v0, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_f} :catch_18

    #@f
    move-result-object v4

    #@10
    .line 1093
    if-nez v4, :cond_1d

    #@12
    .line 1094
    const-string v7, "getSMSoverIPNetworksIndication(), imsDB is null"

    #@14
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@17
    .line 1121
    :goto_17
    return v6

    #@18
    .line 1088
    :catch_18
    move-exception v3

    #@19
    .line 1089
    .local v3, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@1c
    goto :goto_17

    #@1d
    .line 1098
    .end local v3           #e:Landroid/database/sqlite/SQLiteException;
    :cond_1d
    new-instance v7, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v8, "select sdm_sms_over_ip_network from "

    #@24
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v7

    #@28
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v7

    #@2c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v7

    #@30
    invoke-virtual {v4, v7, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    #@33
    move-result-object v2

    #@34
    .line 1099
    if-eqz v2, :cond_49

    #@36
    .line 1101
    :try_start_36
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@39
    move-result v7

    #@3a
    if-eqz v7, :cond_7f

    #@3c
    .line 1102
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SMS_OVER_IP_NETWORK:Ljava/lang/String;

    #@3e
    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@41
    move-result v7

    #@42
    invoke-interface {v2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_45
    .catchall {:try_start_36 .. :try_end_45} :catchall_85

    #@45
    move-result-object v5

    #@46
    .line 1107
    :goto_46
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@49
    .line 1110
    :cond_49
    if-eqz v4, :cond_53

    #@4b
    .line 1111
    const-string v7, "getSMSoverIPNetworksIndication(), imsDB make be close"

    #@4d
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@50
    .line 1112
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@53
    .line 1114
    :cond_53
    new-instance v7, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v8, "getSMSoverIPNetworksIndication(), sms_over_ip_network_indication=["

    #@5a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v7

    #@5e
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v7

    #@62
    const-string v8, "] "

    #@64
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v7

    #@68
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v7

    #@6c
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@6f
    .line 1116
    if-eqz v5, :cond_79

    #@71
    const-string v7, "false"

    #@73
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v7

    #@77
    if-eqz v7, :cond_8a

    #@79
    .line 1117
    :cond_79
    const-string v7, "getSMSoverIPNetworksIndication(), smsOverIPNetworksIndication is false"

    #@7b
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7e
    goto :goto_17

    #@7f
    .line 1104
    :cond_7f
    :try_start_7f
    const-string v7, "getSMSoverIPNetworksIndication(), cursor moveToFirst error"

    #@81
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_84
    .catchall {:try_start_7f .. :try_end_84} :catchall_85

    #@84
    goto :goto_46

    #@85
    .line 1107
    :catchall_85
    move-exception v6

    #@86
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@89
    throw v6

    #@8a
    .line 1120
    :cond_8a
    const-string v6, "getSMSoverIPNetworksIndication(), smsOverIPNetworksIndication is true"

    #@8c
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@8f
    .line 1121
    const/4 v6, 0x1

    #@90
    goto :goto_17
.end method

.method private getVZWSignatures(Landroid/content/pm/PackageManager;)Z
    .registers 12
    .parameter "pm"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1136
    :try_start_1
    const-string v8, "com.verizon.permissions.appdirectedsms"

    #@3
    const/16 v9, 0x40

    #@5
    invoke-virtual {p1, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_8
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_8} :catch_19

    #@8
    move-result-object v5

    #@9
    .line 1142
    .local v5, permissionPkgInfo:Landroid/content/pm/PackageInfo;
    if-eqz v5, :cond_64

    #@b
    .line 1143
    iget-object v8, v5, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@d
    iput-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@f
    .line 1145
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@11
    if-nez v8, :cond_20

    #@13
    .line 1146
    const-string v8, "getVZWSignatures(), Can\'t find permission package signatures"

    #@15
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@18
    .line 1156
    .end local v5           #permissionPkgInfo:Landroid/content/pm/PackageInfo;
    :goto_18
    return v7

    #@19
    .line 1137
    :catch_19
    move-exception v1

    #@1a
    .line 1138
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "getVZWSignatures(), Can\'t find permission package: com.verizon.permissions.appdirectedsms"

    #@1c
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@1f
    goto :goto_18

    #@20
    .line 1149
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v5       #permissionPkgInfo:Landroid/content/pm/PackageInfo;
    :cond_20
    const/4 v3, 0x0

    #@21
    .line 1150
    .local v3, index:I
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@23
    .local v0, arr$:[Landroid/content/pm/Signature;
    array-length v4, v0

    #@24
    .local v4, len$:I
    const/4 v2, 0x0

    #@25
    .local v2, i$:I
    :goto_25
    if-ge v2, v4, :cond_64

    #@27
    aget-object v6, v0, v2

    #@29
    .line 1151
    .local v6, signature:Landroid/content/pm/Signature;
    new-instance v7, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v8, "getVZWSignatures(), VZWSignature: index = [ "

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v7

    #@38
    const-string v8, " ]"

    #@3a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v7

    #@3e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v7

    #@42
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@45
    .line 1152
    new-instance v7, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v8, "getVZWSignatures(), VZWSignature : "

    #@4c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v7

    #@50
    invoke-virtual {v6}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    #@53
    move-result-object v8

    #@54
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v7

    #@58
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v7

    #@5c
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@5f
    .line 1153
    add-int/lit8 v3, v3, 0x1

    #@61
    .line 1150
    add-int/lit8 v2, v2, 0x1

    #@63
    goto :goto_25

    #@64
    .line 1156
    .end local v0           #arr$:[Landroid/content/pm/Signature;
    .end local v2           #i$:I
    .end local v3           #index:I
    .end local v4           #len$:I
    .end local v6           #signature:Landroid/content/pm/Signature;
    :cond_64
    const/4 v7, 0x1

    #@65
    goto :goto_18
.end method

.method private handleCdmaStatusReport(Lcom/android/internal/telephony/cdma/SmsMessage;)V
    .registers 9
    .parameter "sms"

    #@0
    .prologue
    .line 251
    const/4 v2, 0x0

    #@1
    .local v2, i:I
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->deliveryPendingList:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .local v0, count:I
    :goto_7
    if-ge v2, v0, :cond_3b

    #@9
    .line 252
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->deliveryPendingList:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v4

    #@f
    check-cast v4, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@11
    .line 253
    .local v4, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    iget v5, v4, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mMessageRef:I

    #@13
    iget v6, p1, Lcom/android/internal/telephony/SmsMessageBase;->messageRef:I

    #@15
    if-ne v5, v6, :cond_3c

    #@17
    .line 255
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->deliveryPendingList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1c
    .line 256
    iget-object v3, v4, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mDeliveryIntent:Landroid/app/PendingIntent;

    #@1e
    .line 257
    .local v3, intent:Landroid/app/PendingIntent;
    new-instance v1, Landroid/content/Intent;

    #@20
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@23
    .line 258
    .local v1, fillIn:Landroid/content/Intent;
    const-string v5, "pdu"

    #@25
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getPdu()[B

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    #@2c
    .line 259
    const-string v5, "format"

    #@2e
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getFormat()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@35
    .line 261
    :try_start_35
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@37
    const/4 v6, -0x1

    #@38
    invoke-virtual {v3, v5, v6, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_3b
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_35 .. :try_end_3b} :catch_3f

    #@3b
    .line 266
    .end local v1           #fillIn:Landroid/content/Intent;
    .end local v3           #intent:Landroid/app/PendingIntent;
    .end local v4           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 251
    .restart local v4       #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :cond_3c
    add-int/lit8 v2, v2, 0x1

    #@3e
    goto :goto_7

    #@3f
    .line 262
    .restart local v1       #fillIn:Landroid/content/Intent;
    .restart local v3       #intent:Landroid/app/PendingIntent;
    :catch_3f
    move-exception v5

    #@40
    goto :goto_3b
.end method

.method private handleLguMessage(Lcom/android/internal/telephony/cdma/SmsMessage;I)I
    .registers 16
    .parameter "sms"
    .parameter "teleService"

    #@0
    .prologue
    const/16 v12, 0xa

    #@2
    const/4 v11, 0x3

    #@3
    const/4 v10, 0x0

    #@4
    const/4 v8, -0x1

    #@5
    const/4 v7, 0x1

    #@6
    .line 1599
    new-array v4, v7, [[B

    #@8
    .line 1600
    .local v4, pdus:[[B
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getPdu()[B

    #@b
    move-result-object v9

    #@c
    aput-object v9, v4, v10

    #@e
    .line 1605
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->checkValidLmsMessage(I)Z

    #@11
    move-result v9

    #@12
    if-ne v7, v9, :cond_48

    #@14
    .line 1606
    new-array v4, v11, [[B

    #@16
    .line 1607
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getPdu()[B

    #@19
    move-result-object v9

    #@1a
    aput-object v9, v4, v10

    #@1c
    .line 1608
    invoke-virtual {p0, p1, v4}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->processLmsMessage(Lcom/android/internal/telephony/cdma/SmsMessage;[[B)I

    #@1f
    move-result v6

    #@20
    .line 1609
    .local v6, ret:I
    if-eq v6, v8, :cond_23

    #@22
    .line 1670
    .end local v6           #ret:I
    :cond_22
    :goto_22
    return v6

    #@23
    .line 1614
    .restart local v6       #ret:I
    :cond_23
    const/4 v0, 0x0

    #@24
    .line 1615
    .local v0, count:I
    const/4 v1, 0x0

    #@25
    .line 1616
    .local v1, loop:I
    const/4 v1, 0x0

    #@26
    :goto_26
    if-ge v1, v11, :cond_2c

    #@28
    .line 1617
    aget-object v9, v4, v1

    #@2a
    if-nez v9, :cond_39

    #@2c
    .line 1621
    :cond_2c
    move v0, v1

    #@2d
    .line 1622
    new-array v2, v0, [[B

    #@2f
    .line 1623
    .local v2, newPuds:[[B
    const/4 v1, 0x0

    #@30
    :goto_30
    if-ge v1, v0, :cond_3c

    #@32
    .line 1624
    aget-object v9, v4, v1

    #@34
    aput-object v9, v2, v1

    #@36
    .line 1623
    add-int/lit8 v1, v1, 0x1

    #@38
    goto :goto_30

    #@39
    .line 1616
    .end local v2           #newPuds:[[B
    :cond_39
    add-int/lit8 v1, v1, 0x1

    #@3b
    goto :goto_26

    #@3c
    .line 1626
    .restart local v2       #newPuds:[[B
    :cond_3c
    new-array v4, v0, [[B

    #@3e
    .line 1627
    const/4 v1, 0x0

    #@3f
    :goto_3f
    if-ge v1, v0, :cond_79

    #@41
    .line 1628
    aget-object v9, v2, v1

    #@43
    aput-object v9, v4, v1

    #@45
    .line 1627
    add-int/lit8 v1, v1, 0x1

    #@47
    goto :goto_3f

    #@48
    .line 1630
    .end local v0           #count:I
    .end local v1           #loop:I
    .end local v2           #newPuds:[[B
    .end local v6           #ret:I
    :cond_48
    const/high16 v9, 0x5

    #@4a
    if-ne v9, p2, :cond_79

    #@4c
    .line 1631
    new-array v4, v12, [[B

    #@4e
    .line 1632
    invoke-virtual {p0, p1, v4}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->processCbsMessage(Lcom/android/internal/telephony/cdma/SmsMessage;[[B)I

    #@51
    move-result v6

    #@52
    .line 1633
    .restart local v6       #ret:I
    if-ne v6, v8, :cond_22

    #@54
    .line 1638
    const/4 v0, 0x0

    #@55
    .line 1639
    .restart local v0       #count:I
    const/4 v1, 0x0

    #@56
    .line 1640
    .restart local v1       #loop:I
    const/4 v1, 0x0

    #@57
    :goto_57
    if-ge v1, v12, :cond_5d

    #@59
    .line 1641
    aget-object v9, v4, v1

    #@5b
    if-nez v9, :cond_6a

    #@5d
    .line 1645
    :cond_5d
    move v0, v1

    #@5e
    .line 1646
    new-array v2, v0, [[B

    #@60
    .line 1647
    .restart local v2       #newPuds:[[B
    const/4 v1, 0x0

    #@61
    :goto_61
    if-ge v1, v0, :cond_6d

    #@63
    .line 1648
    aget-object v9, v4, v1

    #@65
    aput-object v9, v2, v1

    #@67
    .line 1647
    add-int/lit8 v1, v1, 0x1

    #@69
    goto :goto_61

    #@6a
    .line 1640
    .end local v2           #newPuds:[[B
    :cond_6a
    add-int/lit8 v1, v1, 0x1

    #@6c
    goto :goto_57

    #@6d
    .line 1650
    .restart local v2       #newPuds:[[B
    :cond_6d
    new-array v4, v0, [[B

    #@6f
    .line 1651
    const/4 v1, 0x0

    #@70
    :goto_70
    if-ge v1, v0, :cond_79

    #@72
    .line 1652
    aget-object v9, v2, v1

    #@74
    aput-object v9, v4, v1

    #@76
    .line 1651
    add-int/lit8 v1, v1, 0x1

    #@78
    goto :goto_70

    #@79
    .line 1657
    .end local v0           #count:I
    .end local v1           #loop:I
    .end local v2           #newPuds:[[B
    .end local v6           #ret:I
    :cond_79
    const/4 v5, 0x1

    #@7a
    .line 1658
    .local v5, result:I
    const-string v9, "CdmaSmsLgtMessage"

    #@7c
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@7e
    invoke-static {v9, p0, v10, p1, v4}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@81
    move-result-object v9

    #@82
    if-nez v9, :cond_86

    #@84
    move v6, v7

    #@85
    .line 1659
    goto :goto_22

    #@86
    .line 1661
    :cond_86
    const-string v9, "CdmaSmsLgtMessage"

    #@88
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@8a
    invoke-static {v9, p0, v10, p1, v4}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@8d
    move-result-object v3

    #@8e
    .line 1662
    .local v3, operatorMsg:Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    if-eqz v3, :cond_94

    #@90
    .line 1663
    invoke-interface {v3, p0}, Lcom/android/internal/telephony/SmsOperatorBasicMessage;->dispatch(Lcom/android/internal/telephony/SMSDispatcher;)I

    #@93
    move-result v5

    #@94
    .line 1667
    :cond_94
    const/4 v9, 0x2

    #@95
    if-ne v5, v9, :cond_99

    #@97
    move v6, v8

    #@98
    .line 1668
    goto :goto_22

    #@99
    :cond_99
    move v6, v7

    #@9a
    .line 1670
    goto :goto_22
.end method

.method private handleServiceCategoryProgramData(Lcom/android/internal/telephony/cdma/SmsMessage;)V
    .registers 6
    .parameter "sms"

    #@0
    .prologue
    .line 275
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSmsCbProgramData()Ljava/util/ArrayList;

    #@3
    move-result-object v1

    #@4
    .line 276
    .local v1, programDataList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/cdma/CdmaSmsCbProgramData;>;"
    if-nez v1, :cond_e

    #@6
    .line 277
    const-string v2, "CDMA"

    #@8
    const-string v3, "handleServiceCategoryProgramData: program data list is null!"

    #@a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 301
    :goto_d
    return-void

    #@e
    .line 282
    :cond_e
    const-string v2, "US"

    #@10
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_1c

    #@16
    .line 290
    const-string v2, "handleServiceCategoryProgramData(), VZW SCPT Teleservice is removed."

    #@18
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1b
    goto :goto_d

    #@1c
    .line 295
    :cond_1c
    new-instance v0, Landroid/content/Intent;

    #@1e
    const-string v2, "android.provider.Telephony.SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED"

    #@20
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@23
    .line 296
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "sender"

    #@25
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2c
    .line 297
    const-string v2, "program_data"

    #@2e
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    #@31
    .line 298
    const-string v2, "android.permission.RECEIVE_SMS"

    #@33
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mScpResultsReceiver:Landroid/content/BroadcastReceiver;

    #@35
    invoke-virtual {p0, v0, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;)V

    #@38
    goto :goto_d
.end method

.method private isEos(Lcom/android/internal/telephony/cdma/SmsMessage;)Z
    .registers 9
    .parameter "sms"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2413
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSessionMap()Ljava/util/HashMap;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@9
    move-result-object v6

    #@a
    iget v6, v6, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessioinId:I

    #@c
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v3, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    check-cast v3, Ljava/util/TreeMap;

    #@16
    move-object v2, v3

    #@17
    check-cast v2, Ljava/util/TreeMap;

    #@19
    .line 2415
    .local v2, tSeqMap:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    if-nez v2, :cond_1d

    #@1b
    move v3, v4

    #@1c
    .line 2426
    :goto_1c
    return v3

    #@1d
    .line 2418
    :cond_1d
    invoke-virtual {v2}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    #@20
    move-result-object v3

    #@21
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@24
    move-result-object v0

    #@25
    .local v0, i$:Ljava/util/Iterator;
    :cond_25
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_47

    #@2b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2e
    move-result-object v1

    #@2f
    check-cast v1, Ljava/lang/Integer;

    #@31
    .line 2419
    .local v1, nkey:Ljava/lang/Integer;
    invoke-virtual {v2, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    move-result-object v3

    #@35
    check-cast v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;

    #@37
    if-nez v3, :cond_3b

    #@39
    move v3, v4

    #@3a
    .line 2420
    goto :goto_1c

    #@3b
    .line 2422
    :cond_3b
    invoke-virtual {v2, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3e
    move-result-object v3

    #@3f
    check-cast v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;

    #@41
    iget v3, v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;->mEos:I

    #@43
    if-ne v3, v5, :cond_25

    #@45
    move v3, v5

    #@46
    .line 2423
    goto :goto_1c

    #@47
    .end local v1           #nkey:Ljava/lang/Integer;
    :cond_47
    move v3, v4

    #@48
    .line 2426
    goto :goto_1c
.end method

.method private isItSignedByVZW(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .registers 14
    .parameter "pm"
    .parameter "pkgName"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1162
    const-string v9, "isItSignedByVZW(), Non-system app"

    #@3
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@6
    .line 1165
    const/16 v9, 0x40

    #@8
    :try_start_8
    invoke-virtual {p1, p2, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8 .. :try_end_b} :catch_f

    #@b
    move-result-object v7

    #@c
    .line 1171
    .local v7, pkgInfo:Landroid/content/pm/PackageInfo;
    if-nez v7, :cond_27

    #@e
    .line 1195
    .end local v7           #pkgInfo:Landroid/content/pm/PackageInfo;
    :goto_e
    return v8

    #@f
    .line 1166
    :catch_f
    move-exception v3

    #@10
    .line 1167
    .local v3, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v9, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v10, "isItSignedByVZW(), Can\'t find applicaiton: "

    #@17
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v9

    #@1b
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v9

    #@1f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v9

    #@23
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@26
    goto :goto_e

    #@27
    .line 1176
    .end local v3           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v7       #pkgInfo:Landroid/content/pm/PackageInfo;
    :cond_27
    iget-object v1, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@29
    .line 1178
    .local v1, appSignatures:[Landroid/content/pm/Signature;
    if-nez v1, :cond_31

    #@2b
    .line 1179
    const-string v9, "isItSignedByVZW(), Can\'t find signatures"

    #@2d
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@30
    goto :goto_e

    #@31
    .line 1183
    :cond_31
    move-object v2, v1

    #@32
    .local v2, arr$:[Landroid/content/pm/Signature;
    array-length v6, v2

    #@33
    .local v6, len$:I
    const/4 v5, 0x0

    #@34
    .local v5, i$:I
    :goto_34
    if-ge v5, v6, :cond_73

    #@36
    aget-object v0, v2, v5

    #@38
    .line 1184
    .local v0, appSignature:Landroid/content/pm/Signature;
    new-instance v9, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v10, "isItSignedByVZW(), application Signature : "

    #@3f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v9

    #@43
    invoke-virtual {v0}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    #@46
    move-result-object v10

    #@47
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v9

    #@4b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v9

    #@4f
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@52
    .line 1185
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@54
    if-eqz v9, :cond_70

    #@56
    .line 1186
    const/4 v4, 0x0

    #@57
    .local v4, i:I
    :goto_57
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@59
    array-length v9, v9

    #@5a
    if-ge v4, v9, :cond_70

    #@5c
    .line 1187
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@5e
    aget-object v9, v9, v4

    #@60
    invoke-virtual {v9, v0}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v9

    #@64
    if-eqz v9, :cond_6d

    #@66
    .line 1188
    const-string v8, "isItSignedByVZW(), signature Match"

    #@68
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@6b
    .line 1189
    const/4 v8, 0x1

    #@6c
    goto :goto_e

    #@6d
    .line 1186
    :cond_6d
    add-int/lit8 v4, v4, 0x1

    #@6f
    goto :goto_57

    #@70
    .line 1183
    .end local v4           #i:I
    :cond_70
    add-int/lit8 v5, v5, 0x1

    #@72
    goto :goto_34

    #@73
    .line 1194
    .end local v0           #appSignature:Landroid/content/pm/Signature;
    :cond_73
    const-string v9, "isItSignedByVZW(), not signature Match"

    #@75
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@78
    goto :goto_e
.end method

.method private isMsgEnd(Lcom/android/internal/telephony/cdma/SmsMessage;)Z
    .registers 4
    .parameter "sms"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2402
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@4
    move-result-object v1

    #@5
    iget v1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessioinId:I

    #@7
    if-nez v1, :cond_a

    #@9
    .line 2407
    :cond_9
    :goto_9
    return v0

    #@a
    .line 2404
    :cond_a
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isEos(Lcom/android/internal/telephony/cdma/SmsMessage;)Z

    #@d
    move-result v1

    #@e
    if-ne v1, v0, :cond_16

    #@10
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isSeqFull(Lcom/android/internal/telephony/cdma/SmsMessage;)Z

    #@13
    move-result v1

    #@14
    if-eq v1, v0, :cond_9

    #@16
    .line 2407
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_9
.end method

.method private isSeqFull(Lcom/android/internal/telephony/cdma/SmsMessage;)Z
    .registers 10
    .parameter "sms"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 2431
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSessionMap()Ljava/util/HashMap;

    #@5
    move-result-object v4

    #@6
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@9
    move-result-object v7

    #@a
    iget v7, v7, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessioinId:I

    #@c
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v7

    #@10
    invoke-virtual {v4, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v4

    #@14
    check-cast v4, Ljava/util/TreeMap;

    #@16
    move-object v3, v4

    #@17
    check-cast v3, Ljava/util/TreeMap;

    #@19
    .line 2433
    .local v3, tSeqMap:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    if-nez v3, :cond_1d

    #@1b
    move v4, v5

    #@1c
    .line 2449
    :goto_1c
    return v4

    #@1d
    .line 2435
    :cond_1d
    const/4 v0, 0x0

    #@1e
    .line 2436
    .local v0, count:I
    invoke-virtual {v3}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    #@21
    move-result-object v4

    #@22
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@25
    move-result-object v1

    #@26
    .local v1, i$:Ljava/util/Iterator;
    :goto_26
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_57

    #@2c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2f
    move-result-object v2

    #@30
    check-cast v2, Ljava/lang/Integer;

    #@32
    .line 2437
    .local v2, nkey:Ljava/lang/Integer;
    invoke-virtual {v3, v2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@35
    move-result-object v4

    #@36
    check-cast v4, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;

    #@38
    if-nez v4, :cond_3c

    #@3a
    move v4, v5

    #@3b
    .line 2438
    goto :goto_1c

    #@3c
    .line 2440
    :cond_3c
    invoke-virtual {v3, v2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    move-result-object v4

    #@40
    check-cast v4, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;

    #@42
    iget v4, v4, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;->mEos:I

    #@44
    if-ne v4, v6, :cond_54

    #@46
    .line 2441
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v2, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v4

    #@4e
    if-eqz v4, :cond_52

    #@50
    move v4, v6

    #@51
    .line 2442
    goto :goto_1c

    #@52
    :cond_52
    move v4, v5

    #@53
    .line 2444
    goto :goto_1c

    #@54
    .line 2447
    :cond_54
    add-int/lit8 v0, v0, 0x1

    #@56
    goto :goto_26

    #@57
    .end local v2           #nkey:Ljava/lang/Integer;
    :cond_57
    move v4, v5

    #@58
    .line 2449
    goto :goto_1c
.end method

.method private isSmsOverImsMo()Z
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 1062
    const/4 v1, 0x0

    #@2
    .line 1063
    .local v1, bIsSmsOverImsMo:Z
    const/4 v0, 0x0

    #@3
    .line 1064
    .local v0, bIsImsRegi:Z
    const/4 v2, 0x0

    #@4
    .line 1065
    .local v2, bSMSoverIPNetworksIndication:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isIms()Z

    #@7
    move-result v0

    #@8
    .line 1066
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSMSoverIPNetworksIndication()Z

    #@b
    move-result v2

    #@c
    .line 1067
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "isSmsOverImsMo(), bIsImsRegi : "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    const-string v4, "    bSMSoverIPNetworksIndication: "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2c
    .line 1068
    if-ne v0, v5, :cond_31

    #@2e
    if-ne v2, v5, :cond_31

    #@30
    .line 1069
    const/4 v1, 0x1

    #@31
    .line 1071
    :cond_31
    new-instance v3, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v4, "isSmsOverImsMo(), bIsSmsOverImsMo : "

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@47
    .line 1072
    return v1
.end method

.method private makeCompleteText(Lcom/android/internal/telephony/cdma/SmsMessage;[[B)V
    .registers 8
    .parameter "sms"
    .parameter "pdus"

    #@0
    .prologue
    .line 2347
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSessionMap()Ljava/util/HashMap;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@7
    move-result-object v4

    #@8
    iget v4, v4, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessioinId:I

    #@a
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v3

    #@12
    check-cast v3, Ljava/util/TreeMap;

    #@14
    move-object v2, v3

    #@15
    check-cast v2, Ljava/util/TreeMap;

    #@17
    .line 2349
    .local v2, tSeqMap:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    if-nez v2, :cond_1a

    #@19
    .line 2364
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 2352
    :cond_1a
    invoke-virtual {v2}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    #@1d
    move-result-object v3

    #@1e
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@21
    move-result-object v0

    #@22
    .local v0, i$:Ljava/util/Iterator;
    :cond_22
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_19

    #@28
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2b
    move-result-object v1

    #@2c
    check-cast v1, Ljava/lang/Integer;

    #@2e
    .line 2353
    .local v1, nKey:Ljava/lang/Integer;
    invoke-virtual {v2, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v3

    #@32
    check-cast v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;

    #@34
    if-eqz v3, :cond_19

    #@36
    .line 2356
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@39
    move-result v4

    #@3a
    invoke-virtual {v2, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    move-result-object v3

    #@3e
    check-cast v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;

    #@40
    iget-object v3, v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;->mPdu:[B

    #@42
    aput-object v3, p2, v4

    #@44
    .line 2359
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@47
    move-result-object v3

    #@48
    iget v3, v3, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessioinId:I

    #@4a
    if-eqz v3, :cond_19

    #@4c
    invoke-virtual {v2, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4f
    move-result-object v3

    #@50
    check-cast v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;

    #@52
    iget v3, v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;->mEos:I

    #@54
    const/4 v4, 0x1

    #@55
    if-ne v3, v4, :cond_22

    #@57
    goto :goto_19
.end method

.method protected static mmsTestBedGetMsgId([B)[B
    .registers 11
    .parameter "pdu"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 2524
    const-string v7, "CDMA"

    #@3
    const-string v8, "mmsTestBedGetMsgId()"

    #@5
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2525
    array-length v7, p0

    #@9
    add-int/lit8 v7, v7, -0x4

    #@b
    new-array v2, v7, [B

    #@d
    .line 2526
    .local v2, mms_userdata:[B
    array-length v7, v2

    #@e
    new-array v5, v7, [B

    #@10
    .line 2527
    .local v5, new_userdata:[B
    const/16 v7, 0x14

    #@12
    new-array v3, v7, [C

    #@14
    .line 2530
    .local v3, msgid:[C
    const/4 v7, 0x5

    #@15
    array-length v8, p0

    #@16
    add-int/lit8 v8, v8, -0x5

    #@18
    invoke-static {p0, v7, v2, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1b
    .line 2531
    array-length v1, v2

    #@1c
    .line 2533
    .local v1, len:I
    const/4 v0, 0x0

    #@1d
    .local v0, i:I
    :goto_1d
    if-ge v0, v1, :cond_43

    #@1f
    .line 2534
    aget-byte v7, v2, v0

    #@21
    const/16 v8, 0x2f

    #@23
    if-eq v7, v8, :cond_2d

    #@25
    .line 2535
    aget-byte v7, v2, v0

    #@27
    int-to-char v7, v7

    #@28
    aput-char v7, v3, v0

    #@2a
    .line 2533
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_1d

    #@2d
    .line 2537
    :cond_2d
    aput-char v9, v3, v0

    #@2f
    .line 2540
    invoke-static {v3, v9, v0}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    #@32
    move-result-object v6

    #@33
    .line 2541
    .local v6, tmp:Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@36
    move-result v7

    #@37
    sput v7, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->msgref:I

    #@39
    .line 2543
    array-length v7, v5

    #@3a
    sub-int v4, v7, v0

    #@3c
    .line 2544
    .local v4, new_len:I
    add-int/lit8 v7, v0, 0x1

    #@3e
    add-int/lit8 v8, v4, -0x1

    #@40
    invoke-static {v2, v7, v5, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@43
    .line 2548
    .end local v4           #new_len:I
    .end local v6           #tmp:Ljava/lang/String;
    :cond_43
    invoke-static {v5}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->decode_Base64([B)[B

    #@46
    move-result-object v7

    #@47
    return-object v7
.end method

.method private mmsTestBedPushCheck([B)Z
    .registers 7
    .parameter "user_data"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2552
    const-string v3, "CDMA"

    #@3
    const-string v4, "mmsTestBedPushCheck()"

    #@5
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2553
    new-instance v1, Ljava/lang/String;

    #@a
    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    #@d
    .line 2555
    .local v1, pData:Ljava/lang/String;
    const-string v3, "//LG/"

    #@f
    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    #@12
    move-result v0

    #@13
    .line 2557
    .local v0, index:I
    if-nez v0, :cond_16

    #@15
    const/4 v2, 0x1

    #@16
    :cond_16
    return v2
.end method

.method private parseImsMsg([B)I
    .registers 10
    .parameter "userData"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2367
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "parseImsMsg(), CBS IMS_MSG userData[0]="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    aget-byte v3, p1, v4

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1a
    .line 2368
    new-instance v1, Landroid/content/Intent;

    #@1c
    const-string v2, " "

    #@1e
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@21
    .line 2369
    .local v1, intent:Landroid/content/Intent;
    aget-byte v2, p1, v4

    #@23
    const/16 v3, 0x30

    #@25
    if-ne v2, v3, :cond_58

    #@27
    .line 2370
    const-string v2, "order"

    #@29
    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@2c
    .line 2371
    const-string v2, "com.lge.ims.enable"

    #@2e
    const-string v3, "true"

    #@30
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    .line 2372
    const-string v2, "parseImsMsg(), CBS 0x30 "

    #@35
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@38
    .line 2379
    :cond_38
    :goto_38
    :try_start_38
    const-string v2, "contentfield"

    #@3a
    new-instance v3, Ljava/lang/String;

    #@3c
    const/4 v4, 0x1

    #@3d
    array-length v5, p1

    #@3e
    add-int/lit8 v5, v5, -0x1

    #@40
    const-string v6, "KSC5601"

    #@42
    invoke-direct {v3, p1, v4, v5, v6}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@45
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@48
    .line 2380
    const-string v2, "parseImsMsg(), CBS try "

    #@4a
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I
    :try_end_4d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_38 .. :try_end_4d} :catch_70

    #@4d
    .line 2385
    :goto_4d
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@4f
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@52
    .line 2386
    const-string v2, "parseImsMsg(), CBS     context.sendBroadcast "

    #@54
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@57
    .line 2387
    return v7

    #@58
    .line 2373
    :cond_58
    aget-byte v2, p1, v4

    #@5a
    const/16 v3, 0x31

    #@5c
    if-ne v2, v3, :cond_38

    #@5e
    .line 2374
    const-string v2, "order"

    #@60
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@63
    .line 2375
    const-string v2, "com.lge.ims.enable"

    #@65
    const-string v3, "false"

    #@67
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6a
    .line 2376
    const-string v2, "parseImsMsg(), CBS 0x31 "

    #@6c
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@6f
    goto :goto_38

    #@70
    .line 2381
    :catch_70
    move-exception v0

    #@71
    .line 2382
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    #@74
    .line 2383
    const-string v2, "parseImsMsg(), CBS Exception "

    #@76
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@79
    goto :goto_4d
.end method

.method private preHandleMessage(Lcom/android/internal/telephony/cdma/SmsMessage;)Z
    .registers 4
    .parameter "sms"

    #@0
    .prologue
    .line 1683
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleService()I

    #@3
    move-result v0

    #@4
    const v1, 0xc258

    #@7
    if-eq v0, v1, :cond_12

    #@9
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleService()I

    #@c
    move-result v0

    #@d
    const v1, 0xc280

    #@10
    if-ne v0, v1, :cond_14

    #@12
    .line 1686
    :cond_12
    const/4 v0, 0x1

    #@13
    .line 1688
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method private static resultToCause(I)I
    .registers 2
    .parameter "rc"

    #@0
    .prologue
    .line 1043
    packed-switch p0, :pswitch_data_e

    #@3
    .line 1054
    :pswitch_3
    const/16 v0, 0x60

    #@5
    :goto_5
    return v0

    #@6
    .line 1047
    :pswitch_6
    const/4 v0, 0x0

    #@7
    goto :goto_5

    #@8
    .line 1049
    :pswitch_8
    const/16 v0, 0x23

    #@a
    goto :goto_5

    #@b
    .line 1051
    :pswitch_b
    const/4 v0, 0x4

    #@c
    goto :goto_5

    #@d
    .line 1043
    nop

    #@e
    :pswitch_data_e
    .packed-switch -0x1
        :pswitch_6
        :pswitch_3
        :pswitch_6
        :pswitch_3
        :pswitch_8
        :pswitch_b
    .end packed-switch
.end method

.method private setPreConditionForQeTest()V
    .registers 5

    #@0
    .prologue
    .line 2466
    :try_start_0
    new-instance v1, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;

    #@2
    invoke-direct {v1}, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;-><init>()V

    #@5
    .line 2468
    .local v1, smsState:Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->getPrl()I

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_14

    #@b
    .line 2469
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getCdmaPrlVersion()Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->setPrl(Ljava/lang/String;)V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_14} :catch_15

    #@14
    .line 2474
    .end local v1           #smsState:Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;
    :cond_14
    :goto_14
    return-void

    #@15
    .line 2471
    :catch_15
    move-exception v0

    #@16
    .line 2472
    .local v0, ex:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "setPreConditionForQeTest(), State Check Fail : "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@2c
    goto :goto_14
.end method


# virtual methods
.method protected SendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 15
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 667
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "cdma_priority_indicator"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 668
    sget v0, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitPriority:I

    #@c
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->setSmsPriority(I)V

    #@f
    .line 673
    :cond_f
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@11
    const-string v1, "KSC5601EncodingUSCDMA"

    #@13
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_1c

    #@19
    .line 674
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->setPreConditionForQeTest()V

    #@1c
    .line 678
    :cond_1c
    if-eqz p5, :cond_30

    #@1e
    const/4 v3, 0x1

    #@1f
    :goto_1f
    const/4 v4, 0x0

    #@20
    move-object v0, p2

    #@21
    move-object v1, p1

    #@22
    move-object v2, p3

    #@23
    move-object v5, p6

    #@24
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;Ljava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@27
    move-result-object v4

    #@28
    .line 681
    .local v4, pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    if-nez v4, :cond_32

    #@2a
    .line 682
    const-string v0, "SendText(), fail : pdu is null"

    #@2c
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@2f
    .line 690
    :goto_2f
    return-void

    #@30
    .line 678
    .end local v4           #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_30
    const/4 v3, 0x0

    #@31
    goto :goto_1f

    #@32
    .restart local v4       #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_32
    move-object v0, p0

    #@33
    move-object v1, p1

    #@34
    move-object v2, p2

    #@35
    move-object v3, p3

    #@36
    move-object v5, p6

    #@37
    .line 686
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;Ljava/lang/String;)Ljava/util/HashMap;

    #@3a
    move-result-object v6

    #@3b
    .line 687
    .local v6, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getFormat()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    invoke-virtual {p0, v6, p4, p5, v0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@42
    move-result-object v7

    #@43
    .line 689
    .local v7, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendSubmitPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@46
    goto :goto_2f
.end method

.method protected acknowledgeLastIncomingSms(ZILandroid/os/Message;)V
    .registers 8
    .parameter "success"
    .parameter "result"
    .parameter "response"

    #@0
    .prologue
    .line 1028
    const-string v2, "ril.cdma.inecmmode"

    #@2
    const-string v3, "false"

    #@4
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 1029
    .local v1, inEcm:Ljava/lang/String;
    const-string v2, "true"

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_11

    #@10
    .line 1040
    :goto_10
    return-void

    #@11
    .line 1033
    :cond_11
    invoke-static {p2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->resultToCause(I)I

    #@14
    move-result v0

    #@15
    .line 1034
    .local v0, causeCode:I
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    invoke-interface {v2, p1, v0, p3}, Lcom/android/internal/telephony/CommandsInterface;->acknowledgeLastIncomingCdmaSms(ZILandroid/os/Message;)V

    #@1a
    .line 1036
    if-nez v0, :cond_20

    #@1c
    .line 1037
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mLastDispatchedSmsFingerprint:[B

    #@1e
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mLastAcknowledgedSmsFingerprint:[B

    #@20
    .line 1039
    :cond_20
    const/4 v2, 0x0

    #@21
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mLastDispatchedSmsFingerprint:[B

    #@23
    goto :goto_10
.end method

.method protected calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 4
    .parameter "messageBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 723
    invoke-static {p1, p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 4
    .parameter "messageBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 715
    invoke-static {p1, p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public checkCompleteMsg(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III[B[[B)I
    .registers 38
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "currentSegment"
    .parameter "totalSegment"
    .parameter "pdu"
    .parameter "pdus"

    #@0
    .prologue
    .line 2003
    const-string v2, "deleteAllExpiredMsg()"

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 2004
    const/16 v19, 0x0

    #@7
    .line 2006
    .local v19, cursor:Landroid/database/Cursor;
    new-instance v29, Ljava/util/ArrayList;

    #@9
    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    #@c
    .line 2007
    .local v29, whereArgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v28, Ljava/lang/StringBuilder;

    #@e
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    .line 2009
    .local v28, where:Ljava/lang/StringBuilder;
    const-string v2, "source_min = ? "

    #@13
    move-object/from16 v0, v28

    #@15
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 2010
    move-object/from16 v0, v29

    #@1a
    move-object/from16 v1, p2

    #@1c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1f
    .line 2014
    const-string v2, " AND reference_number = ?"

    #@21
    move-object/from16 v0, v28

    #@23
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 2015
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    move-object/from16 v0, v29

    #@2c
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    .line 2016
    const-string v2, " AND count = ?"

    #@31
    move-object/from16 v0, v28

    #@33
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 2017
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    move-object/from16 v0, v29

    #@3c
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3f
    .line 2018
    const-string v2, " AND tid = ?"

    #@41
    move-object/from16 v0, v28

    #@43
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    .line 2019
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleService()I

    #@49
    move-result v2

    #@4a
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    move-object/from16 v0, v29

    #@50
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@53
    .line 2022
    const-string v2, " AND address = ?"

    #@55
    move-object/from16 v0, v28

    #@57
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    .line 2023
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    move-object/from16 v0, v29

    #@60
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@63
    .line 2028
    const/16 v2, 0x8

    #@65
    new-array v4, v2, [Ljava/lang/String;

    #@67
    const/4 v2, 0x0

    #@68
    const-string v3, "_id"

    #@6a
    aput-object v3, v4, v2

    #@6c
    const/4 v2, 0x1

    #@6d
    const-string v3, "source_min"

    #@6f
    aput-object v3, v4, v2

    #@71
    const/4 v2, 0x2

    #@72
    const-string v3, "reference_number"

    #@74
    aput-object v3, v4, v2

    #@76
    const/4 v2, 0x3

    #@77
    const-string v3, "count"

    #@79
    aput-object v3, v4, v2

    #@7b
    const/4 v2, 0x4

    #@7c
    const-string v3, "sequence"

    #@7e
    aput-object v3, v4, v2

    #@80
    const/4 v2, 0x5

    #@81
    const-string v3, "pdu"

    #@83
    aput-object v3, v4, v2

    #@85
    const/4 v2, 0x6

    #@86
    const-string v3, "address"

    #@88
    aput-object v3, v4, v2

    #@8a
    const/4 v2, 0x7

    #@8b
    const-string v3, "date"

    #@8d
    aput-object v3, v4, v2

    #@8f
    .line 2046
    .local v4, projection:[Ljava/lang/String;
    :try_start_8f
    new-instance v25, Ljava/util/ArrayList;

    #@91
    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    #@94
    .line 2047
    .local v25, rowIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/16 v21, 0x0

    #@96
    .line 2048
    .local v21, expiredMsgCount:I
    const/4 v13, 0x0

    #@97
    .line 2050
    .local v13, sameMsgCount:I
    move-object/from16 v0, p0

    #@99
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@9b
    sget-object v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@9d
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v5

    #@a1
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    #@a4
    move-result v6

    #@a5
    new-array v6, v6, [Ljava/lang/String;

    #@a7
    move-object/from16 v0, v29

    #@a9
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@ac
    move-result-object v6

    #@ad
    check-cast v6, [Ljava/lang/String;

    #@af
    const-string v7, "date"

    #@b1
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@b4
    move-result-object v19

    #@b5
    .line 2056
    if-nez v19, :cond_c3

    #@b7
    .line 2057
    const-string v2, "checkCompleteMsg(), cursor is null"

    #@b9
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_bc
    .catchall {:try_start_8f .. :try_end_bc} :catchall_261
    .catch Landroid/database/SQLException; {:try_start_8f .. :try_end_bc} :catch_254

    #@bc
    .line 2058
    const/4 v2, 0x1

    #@bd
    .line 2114
    if-eqz v19, :cond_c2

    #@bf
    .line 2115
    .end local v13           #sameMsgCount:I
    .end local v21           #expiredMsgCount:I
    .end local v25           #rowIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :goto_bf
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@c2
    .line 2112
    :cond_c2
    return v2

    #@c3
    .line 2061
    .restart local v13       #sameMsgCount:I
    .restart local v21       #expiredMsgCount:I
    .restart local v25       #rowIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_c3
    :try_start_c3
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    #@c6
    move-result v13

    #@c7
    .line 2062
    new-instance v2, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v3, "checkCompleteMsg(), sameMsgCount = "

    #@ce
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v2

    #@d2
    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v2

    #@d6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v2

    #@da
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@dd
    .line 2063
    if-gtz v13, :cond_f6

    #@df
    move-object/from16 v5, p0

    #@e1
    move-object/from16 v6, p1

    #@e3
    move-object/from16 v7, p2

    #@e5
    move/from16 v8, p3

    #@e7
    move/from16 v9, p4

    #@e9
    move/from16 v10, p5

    #@eb
    move-object/from16 v11, p6

    #@ed
    move-object/from16 v12, p7

    #@ef
    .line 2064
    invoke-direct/range {v5 .. v13}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->completeOrInsertLms(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III[B[[BI)I

    #@f2
    move-result v2

    #@f3
    .line 2114
    if-eqz v19, :cond_c2

    #@f5
    goto :goto_bf

    #@f6
    .line 2068
    :cond_f6
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    #@f9
    move-result v2

    #@fa
    if-nez v2, :cond_105

    #@fc
    .line 2069
    const-string v2, "checkCompleteMsg(), cursor moveToFirst error"

    #@fe
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@101
    .line 2070
    const/4 v2, 0x1

    #@102
    .line 2114
    if-eqz v19, :cond_c2

    #@104
    goto :goto_bf

    #@105
    .line 2074
    :cond_105
    const-string v2, "_id"

    #@107
    move-object/from16 v0, v19

    #@109
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@10c
    move-result v2

    #@10d
    move-object/from16 v0, v19

    #@10f
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    #@112
    move-result-wide v23

    #@113
    .line 2075
    .local v23, rowId:J
    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@116
    move-result-object v2

    #@117
    move-object/from16 v0, v25

    #@119
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11c
    .line 2076
    const-string v2, "sequence"

    #@11e
    move-object/from16 v0, v19

    #@120
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@123
    move-result v2

    #@124
    move-object/from16 v0, v19

    #@126
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    #@129
    move-result v16

    #@12a
    .line 2077
    .local v16, currSegment:I
    const-string v2, "date"

    #@12c
    move-object/from16 v0, v19

    #@12e
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@131
    move-result v2

    #@132
    move-object/from16 v0, v19

    #@134
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    #@137
    move-result-wide v2

    #@138
    const-wide/16 v5, 0x3e8

    #@13a
    div-long v26, v2, v5

    #@13c
    .line 2078
    .local v26, time:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@13f
    move-result-wide v2

    #@140
    const-wide/16 v5, 0x3e8

    #@142
    div-long v17, v2, v5

    #@144
    .line 2080
    .local v17, currentTime:J
    const-string v2, "address"

    #@146
    move-object/from16 v0, v19

    #@148
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@14b
    move-result v2

    #@14c
    move-object/from16 v0, v19

    #@14e
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@151
    move-result-object v14

    #@152
    .line 2081
    .local v14, callback:Ljava/lang/String;
    const-string v2, "pdu"

    #@154
    move-object/from16 v0, v19

    #@156
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@159
    move-result v2

    #@15a
    move-object/from16 v0, v19

    #@15c
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@15f
    move-result-object v2

    #@160
    invoke-static {v2}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@163
    move-result-object v15

    #@164
    .line 2084
    .local v15, currPdu:[B
    add-int/lit8 v2, v16, -0x1

    #@166
    aput-object v15, p7, v2

    #@168
    .line 2086
    new-instance v2, Ljava/lang/StringBuilder;

    #@16a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16d
    const-string v3, "checkCompleteMsg(), rowId = "

    #@16f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v2

    #@173
    move-wide/from16 v0, v23

    #@175
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@178
    move-result-object v2

    #@179
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17c
    move-result-object v2

    #@17d
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@180
    .line 2087
    new-instance v2, Ljava/lang/StringBuilder;

    #@182
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@185
    const-string v3, "checkCompleteMsg(), time = "

    #@187
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v2

    #@18b
    move-wide/from16 v0, v26

    #@18d
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@190
    move-result-object v2

    #@191
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@194
    move-result-object v2

    #@195
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@198
    .line 2088
    new-instance v2, Ljava/lang/StringBuilder;

    #@19a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19d
    const-string v3, "checkCompleteMsg(), currentTime = "

    #@19f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v2

    #@1a3
    move-wide/from16 v0, v17

    #@1a5
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v2

    #@1a9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ac
    move-result-object v2

    #@1ad
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1b0
    .line 2089
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b5
    const-string v3, "checkCompleteMsg(), text = "

    #@1b7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ba
    move-result-object v2

    #@1bb
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1be
    move-result-object v2

    #@1bf
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c2
    move-result-object v2

    #@1c3
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1c6
    .line 2090
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1cb
    const-string v3, "checkCompleteMsg(), callback = "

    #@1cd
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d0
    move-result-object v2

    #@1d1
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v2

    #@1d5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d8
    move-result-object v2

    #@1d9
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1dc
    .line 2092
    sub-long v2, v17, v26

    #@1de
    const-wide/16 v5, 0x258

    #@1e0
    cmp-long v2, v2, v5

    #@1e2
    if-lez v2, :cond_1fd

    #@1e4
    .line 2093
    add-int/lit8 v21, v21, 0x1

    #@1e6
    .line 2094
    move-object/from16 v0, p0

    #@1e8
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@1ea
    sget-object v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@1ec
    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1ef
    move-result-object v5

    #@1f0
    invoke-virtual {v5}, Ljava/lang/Long;->toString()Ljava/lang/String;

    #@1f3
    move-result-object v5

    #@1f4
    invoke-static {v3, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@1f7
    move-result-object v3

    #@1f8
    const/4 v5, 0x0

    #@1f9
    const/4 v6, 0x0

    #@1fa
    invoke-virtual {v2, v3, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@1fd
    .line 2097
    :cond_1fd
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    #@200
    move-result v2

    #@201
    if-nez v2, :cond_105

    #@203
    .line 2099
    if-lez v21, :cond_23c

    #@205
    .line 2100
    const/16 v22, 0x0

    #@207
    .local v22, loop:I
    :goto_207
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    #@20a
    move-result v2

    #@20b
    move/from16 v0, v22

    #@20d
    if-ge v0, v2, :cond_237

    #@20f
    .line 2101
    move-object/from16 v0, p0

    #@211
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@213
    sget-object v5, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@215
    move-object/from16 v0, v25

    #@217
    move/from16 v1, v22

    #@219
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21c
    move-result-object v2

    #@21d
    check-cast v2, Ljava/lang/Long;

    #@21f
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@222
    move-result-wide v6

    #@223
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@226
    move-result-object v2

    #@227
    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    #@22a
    move-result-object v2

    #@22b
    invoke-static {v5, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@22e
    move-result-object v2

    #@22f
    const/4 v5, 0x0

    #@230
    const/4 v6, 0x0

    #@231
    invoke-virtual {v3, v2, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@234
    .line 2100
    add-int/lit8 v22, v22, 0x1

    #@236
    goto :goto_207

    #@237
    .line 2105
    :cond_237
    const/4 v2, 0x1

    #@238
    .line 2114
    if-eqz v19, :cond_c2

    #@23a
    goto/16 :goto_bf

    #@23c
    .end local v22           #loop:I
    :cond_23c
    move-object/from16 v5, p0

    #@23e
    move-object/from16 v6, p1

    #@240
    move-object/from16 v7, p2

    #@242
    move/from16 v8, p3

    #@244
    move/from16 v9, p4

    #@246
    move/from16 v10, p5

    #@248
    move-object/from16 v11, p6

    #@24a
    move-object/from16 v12, p7

    #@24c
    .line 2107
    invoke-direct/range {v5 .. v13}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->completeOrInsertLms(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III[B[[BI)I
    :try_end_24f
    .catchall {:try_start_c3 .. :try_end_24f} :catchall_261
    .catch Landroid/database/SQLException; {:try_start_c3 .. :try_end_24f} :catch_254

    #@24f
    move-result v2

    #@250
    .line 2114
    if-eqz v19, :cond_c2

    #@252
    goto/16 :goto_bf

    #@254
    .line 2110
    .end local v13           #sameMsgCount:I
    .end local v14           #callback:Ljava/lang/String;
    .end local v15           #currPdu:[B
    .end local v16           #currSegment:I
    .end local v17           #currentTime:J
    .end local v21           #expiredMsgCount:I
    .end local v23           #rowId:J
    .end local v25           #rowIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v26           #time:J
    :catch_254
    move-exception v20

    #@255
    .line 2111
    .local v20, e:Landroid/database/SQLException;
    :try_start_255
    const-string v2, "checkCompleteMsg(), SQLException occurs"

    #@257
    move-object/from16 v0, v20

    #@259
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_25c
    .catchall {:try_start_255 .. :try_end_25c} :catchall_261

    #@25c
    .line 2112
    const/4 v2, 0x1

    #@25d
    .line 2114
    if-eqz v19, :cond_c2

    #@25f
    goto/16 :goto_bf

    #@261
    .end local v20           #e:Landroid/database/SQLException;
    :catchall_261
    move-exception v2

    #@262
    if-eqz v19, :cond_267

    #@264
    .line 2115
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@267
    .line 2114
    :cond_267
    throw v2
.end method

.method public checkDeletedMms(Ljava/lang/String;)Z
    .registers 9
    .parameter "msgId"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1750
    const-string v6, "checkDeletedMms()"

    #@4
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@7
    .line 1752
    const-string v2, "mms_id = ?"

    #@9
    .line 1753
    .local v2, where:Ljava/lang/String;
    new-array v3, v5, [Ljava/lang/String;

    #@b
    aput-object p1, v3, v4

    #@d
    .line 1756
    .local v3, whereArgs:[Ljava/lang/String;
    const/4 v0, 0x0

    #@e
    .line 1762
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_1c

    #@10
    :try_start_10
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_13
    .catchall {:try_start_10 .. :try_end_13} :catchall_37
    .catch Landroid/database/SQLException; {:try_start_10 .. :try_end_13} :catch_2b

    #@13
    move-result v6

    #@14
    if-nez v6, :cond_1c

    #@16
    .line 1775
    if-eqz v0, :cond_1b

    #@18
    .line 1776
    :goto_18
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@1b
    .line 1773
    :cond_1b
    :goto_1b
    return v4

    #@1c
    .line 1765
    :cond_1c
    if-eqz v0, :cond_3e

    #@1e
    :try_start_1e
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_21
    .catchall {:try_start_1e .. :try_end_21} :catchall_37
    .catch Landroid/database/SQLException; {:try_start_1e .. :try_end_21} :catch_2b

    #@21
    move-result v6

    #@22
    if-lez v6, :cond_3e

    #@24
    .line 1775
    if-eqz v0, :cond_29

    #@26
    .line 1776
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@29
    :cond_29
    move v4, v5

    #@2a
    .line 1766
    goto :goto_1b

    #@2b
    .line 1770
    :catch_2b
    move-exception v1

    #@2c
    .line 1771
    .local v1, e:Landroid/database/SQLException;
    :try_start_2c
    invoke-virtual {v1}, Landroid/database/SQLException;->printStackTrace()V

    #@2f
    .line 1772
    const-string v5, "checkDeletedMms(), SQLException occurs"

    #@31
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_34
    .catchall {:try_start_2c .. :try_end_34} :catchall_37

    #@34
    .line 1775
    if-eqz v0, :cond_1b

    #@36
    goto :goto_18

    #@37
    .end local v1           #e:Landroid/database/SQLException;
    :catchall_37
    move-exception v4

    #@38
    if-eqz v0, :cond_3d

    #@3a
    .line 1776
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@3d
    .line 1775
    :cond_3d
    throw v4

    #@3e
    :cond_3e
    if-eqz v0, :cond_1b

    #@40
    goto :goto_18
.end method

.method public checkLmsDuplicated(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III[B)Z
    .registers 29
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "totalSegment"
    .parameter "currentSegment"
    .parameter "pdu"

    #@0
    .prologue
    .line 1809
    const-string v2, "checkLmsDuplicated()"

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 1810
    invoke-static/range {p6 .. p6}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@8
    move-result-object v16

    #@9
    .line 1811
    .local v16, pduStr:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "checkLmsDuplicated(), pduStr = "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    move-object/from16 v0, v16

    #@16
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@21
    .line 1812
    const/4 v11, 0x0

    #@22
    .line 1813
    .local v11, cursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    #@23
    .line 1814
    .local v8, bRet:Z
    new-instance v21, Ljava/util/ArrayList;

    #@25
    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    #@28
    .line 1815
    .local v21, whereArgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v20, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    .line 1817
    .local v20, where:Ljava/lang/StringBuilder;
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@30
    move-result v2

    #@31
    if-nez v2, :cond_180

    #@33
    .line 1818
    const-string v2, "source_min = ? "

    #@35
    move-object/from16 v0, v20

    #@37
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    .line 1819
    move-object/from16 v0, v21

    #@3c
    move-object/from16 v1, p2

    #@3e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@41
    .line 1823
    :goto_41
    const-string v2, " AND reference_number = ?"

    #@43
    move-object/from16 v0, v20

    #@45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    .line 1824
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    move-object/from16 v0, v21

    #@4e
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@51
    .line 1825
    const-string v2, " AND count = ?"

    #@53
    move-object/from16 v0, v20

    #@55
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    .line 1826
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    move-object/from16 v0, v21

    #@5e
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@61
    .line 1827
    const-string v2, " AND sequence = ?"

    #@63
    move-object/from16 v0, v20

    #@65
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    .line 1828
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6b
    move-result-object v2

    #@6c
    move-object/from16 v0, v21

    #@6e
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@71
    .line 1837
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@74
    move-result-object v2

    #@75
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@78
    move-result v2

    #@79
    if-nez v2, :cond_189

    #@7b
    .line 1838
    const-string v2, " AND address = ?"

    #@7d
    move-object/from16 v0, v20

    #@7f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    .line 1839
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@85
    move-result-object v2

    #@86
    move-object/from16 v0, v21

    #@88
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8b
    .line 1843
    :goto_8b
    const-string v2, " AND tid = ?"

    #@8d
    move-object/from16 v0, v20

    #@8f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    .line 1844
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleService()I

    #@95
    move-result v2

    #@96
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@99
    move-result-object v2

    #@9a
    move-object/from16 v0, v21

    #@9c
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9f
    .line 1846
    new-instance v2, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v3, "checkLmsDuplicated(), where = "

    #@a6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v2

    #@aa
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v3

    #@ae
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v2

    #@b2
    const-string v3, ", whereArgs.toString() = "

    #@b4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v2

    #@b8
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    #@bb
    move-result-object v3

    #@bc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v2

    #@c0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v2

    #@c4
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@c7
    .line 1848
    const/4 v2, 0x2

    #@c8
    new-array v4, v2, [Ljava/lang/String;

    #@ca
    const/4 v2, 0x0

    #@cb
    const-string v3, "_id"

    #@cd
    aput-object v3, v4, v2

    #@cf
    const/4 v2, 0x1

    #@d0
    const-string v3, "pdu"

    #@d2
    aput-object v3, v4, v2

    #@d4
    .line 1853
    .local v4, projection:[Ljava/lang/String;
    :try_start_d4
    move-object/from16 v0, p0

    #@d6
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@d8
    sget-object v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@da
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v5

    #@de
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    #@e1
    move-result v6

    #@e2
    new-array v6, v6, [Ljava/lang/String;

    #@e4
    move-object/from16 v0, v21

    #@e6
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@e9
    move-result-object v6

    #@ea
    check-cast v6, [Ljava/lang/String;

    #@ec
    const/4 v7, 0x0

    #@ed
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@f0
    move-result-object v11

    #@f1
    .line 1859
    if-eqz v11, :cond_17a

    #@f3
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    #@f6
    move-result v2

    #@f7
    if-lez v2, :cond_17a

    #@f9
    .line 1860
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    #@fc
    .line 1861
    const/4 v12, 0x0

    #@fd
    .line 1863
    .local v12, different:Z
    :cond_fd
    const/4 v2, 0x1

    #@fe
    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@101
    move-result-object v9

    #@102
    .line 1864
    .local v9, body:Ljava/lang/String;
    invoke-static {v9}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@105
    move-result-object v10

    #@106
    .line 1865
    .local v10, body_cursor:[B
    invoke-static {v10}, Lcom/android/internal/telephony/cdma/SmsMessage;->createFromPdu([B)Lcom/android/internal/telephony/cdma/SmsMessage;

    #@109
    move-result-object v17

    #@10a
    .line 1867
    .local v17, sm:Lcom/android/internal/telephony/cdma/SmsMessage;
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserData()[B

    #@10d
    move-result-object v18

    #@10e
    .line 1868
    .local v18, ud:[B
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserData()[B

    #@111
    move-result-object v19

    #@112
    .line 1870
    .local v19, ud_curr:[B
    move-object/from16 v0, v18

    #@114
    array-length v15, v0

    #@115
    .line 1871
    .local v15, len:I
    const/4 v14, 0x0

    #@116
    .local v14, i:I
    :goto_116
    if-ge v14, v15, :cond_122

    #@118
    .line 1872
    aget-byte v2, v19, v14

    #@11a
    aget-byte v3, v18, v14

    #@11c
    if-eq v2, v3, :cond_192

    #@11e
    .line 1873
    const/4 v12, 0x1

    #@11f
    .line 1874
    invoke-interface {v11}, Landroid/database/Cursor;->moveToLast()Z

    #@122
    .line 1879
    :cond_122
    new-instance v2, Ljava/lang/StringBuilder;

    #@124
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@127
    const-string v3, "checkLmsDuplicated(), pduStr_cursor = "

    #@129
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v2

    #@12d
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v2

    #@131
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@134
    move-result-object v2

    #@135
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@138
    .line 1880
    new-instance v2, Ljava/lang/StringBuilder;

    #@13a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13d
    const-string v3, "checkLmsDuplicated(), userdata1 = "

    #@13f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v2

    #@143
    invoke-static/range {v19 .. v19}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@146
    move-result-object v3

    #@147
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v2

    #@14b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14e
    move-result-object v2

    #@14f
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@152
    .line 1881
    new-instance v2, Ljava/lang/StringBuilder;

    #@154
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@157
    const-string v3, "checkLmsDuplicated(), userdata2 = "

    #@159
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v2

    #@15d
    invoke-static/range {v18 .. v18}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@160
    move-result-object v3

    #@161
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v2

    #@165
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@168
    move-result-object v2

    #@169
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@16c
    .line 1882
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    #@16f
    move-result v2

    #@170
    if-nez v2, :cond_fd

    #@172
    .line 1884
    if-eqz v12, :cond_195

    #@174
    .line 1885
    const-string v2, "checkLmsDuplicated(), pduStr not equals pduStr_cursor"

    #@176
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I
    :try_end_179
    .catchall {:try_start_d4 .. :try_end_179} :catchall_1a6
    .catch Landroid/database/SQLException; {:try_start_d4 .. :try_end_179} :catch_19c

    #@179
    .line 1886
    const/4 v8, 0x0

    #@17a
    .line 1896
    .end local v9           #body:Ljava/lang/String;
    .end local v10           #body_cursor:[B
    .end local v12           #different:Z
    .end local v14           #i:I
    .end local v15           #len:I
    .end local v17           #sm:Lcom/android/internal/telephony/cdma/SmsMessage;
    .end local v18           #ud:[B
    .end local v19           #ud_curr:[B
    :cond_17a
    :goto_17a
    if-eqz v11, :cond_17f

    #@17c
    .line 1897
    :goto_17c
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@17f
    .line 1900
    :cond_17f
    return v8

    #@180
    .line 1821
    .end local v4           #projection:[Ljava/lang/String;
    :cond_180
    const-string v2, "source_min is null"

    #@182
    move-object/from16 v0, v20

    #@184
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    goto/16 :goto_41

    #@189
    .line 1841
    :cond_189
    const-string v2, " AND address is null"

    #@18b
    move-object/from16 v0, v20

    #@18d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@190
    goto/16 :goto_8b

    #@192
    .line 1871
    .restart local v4       #projection:[Ljava/lang/String;
    .restart local v9       #body:Ljava/lang/String;
    .restart local v10       #body_cursor:[B
    .restart local v12       #different:Z
    .restart local v14       #i:I
    .restart local v15       #len:I
    .restart local v17       #sm:Lcom/android/internal/telephony/cdma/SmsMessage;
    .restart local v18       #ud:[B
    .restart local v19       #ud_curr:[B
    :cond_192
    add-int/lit8 v14, v14, 0x1

    #@194
    goto :goto_116

    #@195
    .line 1888
    :cond_195
    :try_start_195
    const-string v2, "checkLmsDuplicated(), pduStr equals pduStr_cursor "

    #@197
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I
    :try_end_19a
    .catchall {:try_start_195 .. :try_end_19a} :catchall_1a6
    .catch Landroid/database/SQLException; {:try_start_195 .. :try_end_19a} :catch_19c

    #@19a
    .line 1889
    const/4 v8, 0x1

    #@19b
    goto :goto_17a

    #@19c
    .line 1892
    .end local v9           #body:Ljava/lang/String;
    .end local v10           #body_cursor:[B
    .end local v12           #different:Z
    .end local v14           #i:I
    .end local v15           #len:I
    .end local v17           #sm:Lcom/android/internal/telephony/cdma/SmsMessage;
    .end local v18           #ud:[B
    .end local v19           #ud_curr:[B
    :catch_19c
    move-exception v13

    #@19d
    .line 1893
    .local v13, e:Landroid/database/SQLException;
    :try_start_19d
    const-string v2, "checkLmsDuplicated(), SQLException occurs"

    #@19f
    invoke-static {v2, v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1a2
    .catchall {:try_start_19d .. :try_end_1a2} :catchall_1a6

    #@1a2
    .line 1894
    const/4 v8, 0x1

    #@1a3
    .line 1896
    if-eqz v11, :cond_17f

    #@1a5
    goto :goto_17c

    #@1a6
    .end local v13           #e:Landroid/database/SQLException;
    :catchall_1a6
    move-exception v2

    #@1a7
    if-eqz v11, :cond_1ac

    #@1a9
    .line 1897
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@1ac
    .line 1896
    :cond_1ac
    throw v2
.end method

.method public checkValidLmsMessage(I)Z
    .registers 3
    .parameter "teleService"

    #@0
    .prologue
    .line 1787
    const v0, 0xf6fe

    #@3
    if-eq v0, p1, :cond_23

    #@5
    const v0, 0xc256

    #@8
    if-eq v0, p1, :cond_23

    #@a
    const v0, 0xc264

    #@d
    if-eq v0, p1, :cond_23

    #@f
    const v0, 0xc266

    #@12
    if-eq v0, p1, :cond_23

    #@14
    const v0, 0xc268

    #@17
    if-eq v0, p1, :cond_23

    #@19
    const v0, 0xc006

    #@1c
    if-eq v0, p1, :cond_23

    #@1e
    const v0, 0xc00b

    #@21
    if-ne v0, p1, :cond_25

    #@23
    .line 1794
    :cond_23
    const/4 v0, 0x1

    #@24
    .line 1796
    :goto_24
    return v0

    #@25
    :cond_25
    const/4 v0, 0x0

    #@26
    goto :goto_24
.end method

.method public deleteAllExpiredMsg(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III)V
    .registers 20
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "currentSegment"
    .parameter "totalSegment"

    #@0
    .prologue
    .line 1947
    const-string v0, "deleteAllExpiredMsg()"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@5
    .line 1949
    const/4 v8, 0x0

    #@6
    .line 1951
    .local v8, cursor:Landroid/database/Cursor;
    const/4 v0, 0x2

    #@7
    new-array v2, v0, [Ljava/lang/String;

    #@9
    const/4 v0, 0x0

    #@a
    const-string v1, "_id"

    #@c
    aput-object v1, v2, v0

    #@e
    const/4 v0, 0x1

    #@f
    const-string v1, "date"

    #@11
    aput-object v1, v2, v0

    #@13
    .line 1961
    .local v2, projection:[Ljava/lang/String;
    :try_start_13
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@15
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x0

    #@19
    const-string v5, "date"

    #@1b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1e
    move-result-object v8

    #@1f
    .line 1967
    if-eqz v8, :cond_27

    #@21
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@24
    move-result v0

    #@25
    if-nez v0, :cond_32

    #@27
    .line 1968
    :cond_27
    const-string v0, "deleteAllExpiredMsg(), cursor is null or moveToFirst error"

    #@29
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_2c
    .catchall {:try_start_13 .. :try_end_2c} :catchall_c5
    .catch Landroid/database/SQLException; {:try_start_13 .. :try_end_2c} :catch_bb

    #@2c
    .line 1990
    if-eqz v8, :cond_31

    #@2e
    .line 1991
    :goto_2e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@31
    .line 1994
    :cond_31
    return-void

    #@32
    .line 1972
    :cond_32
    :try_start_32
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@35
    move-result-wide v0

    #@36
    const-wide/16 v3, 0x3e8

    #@38
    div-long v6, v0, v3

    #@3a
    .line 1975
    .local v6, currentTime:J
    :cond_3a
    const-string v0, "_id"

    #@3c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3f
    move-result v0

    #@40
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    #@43
    move-result-wide v10

    #@44
    .line 1976
    .local v10, rowId:J
    const-string v0, "date"

    #@46
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@49
    move-result v0

    #@4a
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    #@4d
    move-result-wide v0

    #@4e
    const-wide/16 v3, 0x3e8

    #@50
    div-long v12, v0, v3

    #@52
    .line 1978
    .local v12, time:J
    new-instance v0, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v1, "deleteAllExpiredMsg(), rowId = "

    #@59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@68
    .line 1979
    new-instance v0, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v1, "deleteAllExpiredMsg(), time = "

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@76
    move-result-object v0

    #@77
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v0

    #@7b
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@7e
    .line 1980
    new-instance v0, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v1, "deleteAllExpiredMsg(), currentTime = "

    #@85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v0

    #@89
    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v0

    #@8d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v0

    #@91
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@94
    .line 1982
    sub-long v0, v6, v12

    #@96
    const-wide/16 v3, 0x258

    #@98
    cmp-long v0, v0, v3

    #@9a
    if-lez v0, :cond_b1

    #@9c
    .line 1983
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@9e
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@a0
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@a3
    move-result-object v3

    #@a4
    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    #@a7
    move-result-object v3

    #@a8
    invoke-static {v1, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@ab
    move-result-object v1

    #@ac
    const/4 v3, 0x0

    #@ad
    const/4 v4, 0x0

    #@ae
    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@b1
    .line 1986
    :cond_b1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_b4
    .catchall {:try_start_32 .. :try_end_b4} :catchall_c5
    .catch Landroid/database/SQLException; {:try_start_32 .. :try_end_b4} :catch_bb

    #@b4
    move-result v0

    #@b5
    if-nez v0, :cond_3a

    #@b7
    .line 1990
    if-eqz v8, :cond_31

    #@b9
    goto/16 :goto_2e

    #@bb
    .line 1987
    .end local v6           #currentTime:J
    .end local v10           #rowId:J
    .end local v12           #time:J
    :catch_bb
    move-exception v9

    #@bc
    .line 1988
    .local v9, e:Landroid/database/SQLException;
    :try_start_bc
    const-string v0, "deleteAllExpiredMsg(), SQLException occurs"

    #@be
    invoke-static {v0, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c1
    .catchall {:try_start_bc .. :try_end_c1} :catchall_c5

    #@c1
    .line 1990
    if-eqz v8, :cond_31

    #@c3
    goto/16 :goto_2e

    #@c5
    .end local v9           #e:Landroid/database/SQLException;
    :catchall_c5
    move-exception v0

    #@c6
    if-eqz v8, :cond_cb

    #@c8
    .line 1991
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@cb
    .line 1990
    :cond_cb
    throw v0
.end method

.method public deleteCompleteLmsMsg(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III)V
    .registers 18
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "currentSegment"
    .parameter "totalSegment"

    #@0
    .prologue
    .line 2201
    const-string v0, "deleteCompleteLmsMsg()"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@5
    .line 2202
    const/4 v6, 0x0

    #@6
    .line 2205
    .local v6, cursor:Landroid/database/Cursor;
    new-instance v11, Ljava/util/ArrayList;

    #@8
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    #@b
    .line 2206
    .local v11, whereArgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v10, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    .line 2208
    .local v10, where:Ljava/lang/StringBuilder;
    const-string v0, "source_min = ? "

    #@12
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 2209
    invoke-virtual {v11, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@18
    .line 2213
    const-string v0, " AND reference_number = ?"

    #@1a
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 2214
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@24
    .line 2215
    const-string v0, " AND count = ?"

    #@26
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    .line 2216
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@30
    .line 2217
    const-string v0, " AND tid = ?"

    #@32
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    .line 2218
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleService()I

    #@38
    move-result v0

    #@39
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@40
    .line 2221
    const-string v0, " AND address = ?"

    #@42
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    .line 2222
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4c
    .line 2227
    const/4 v0, 0x1

    #@4d
    new-array v2, v0, [Ljava/lang/String;

    #@4f
    const/4 v0, 0x0

    #@50
    const-string v1, "_id"

    #@52
    aput-object v1, v2, v0

    #@54
    .line 2232
    .local v2, projection:[Ljava/lang/String;
    :try_start_54
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@56
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@58
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@5f
    move-result v4

    #@60
    new-array v4, v4, [Ljava/lang/String;

    #@62
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@65
    move-result-object v4

    #@66
    check-cast v4, [Ljava/lang/String;

    #@68
    const-string v5, "date"

    #@6a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@6d
    move-result-object v6

    #@6e
    .line 2238
    if-eqz v6, :cond_76

    #@70
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@73
    move-result v0

    #@74
    if-nez v0, :cond_81

    #@76
    .line 2239
    :cond_76
    const-string v0, "deleteCompleteLmsMsg(), cursor is null or moveToFirst error"

    #@78
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_7b
    .catchall {:try_start_54 .. :try_end_7b} :catchall_c9
    .catch Landroid/database/SQLException; {:try_start_54 .. :try_end_7b} :catch_c0

    #@7b
    .line 2253
    if-eqz v6, :cond_80

    #@7d
    .line 2254
    :goto_7d
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@80
    .line 2257
    :cond_80
    return-void

    #@81
    .line 2244
    :cond_81
    :try_start_81
    const-string v0, "_id"

    #@83
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@86
    move-result v0

    #@87
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    #@8a
    move-result-wide v8

    #@8b
    .line 2245
    .local v8, rowId:J
    new-instance v0, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v1, "deleteCompleteLmsMsg(), rowId = "

    #@92
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v0

    #@96
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@99
    move-result-object v0

    #@9a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v0

    #@9e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@a1
    .line 2246
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@a3
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@a5
    new-instance v3, Ljava/lang/Long;

    #@a7
    invoke-direct {v3, v8, v9}, Ljava/lang/Long;-><init>(J)V

    #@aa
    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    #@ad
    move-result-object v3

    #@ae
    invoke-static {v1, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@b1
    move-result-object v1

    #@b2
    const/4 v3, 0x0

    #@b3
    const/4 v4, 0x0

    #@b4
    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@b7
    .line 2248
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_ba
    .catchall {:try_start_81 .. :try_end_ba} :catchall_c9
    .catch Landroid/database/SQLException; {:try_start_81 .. :try_end_ba} :catch_c0

    #@ba
    move-result v0

    #@bb
    if-nez v0, :cond_81

    #@bd
    .line 2253
    if-eqz v6, :cond_80

    #@bf
    goto :goto_7d

    #@c0
    .line 2249
    .end local v8           #rowId:J
    :catch_c0
    move-exception v7

    #@c1
    .line 2250
    .local v7, e:Landroid/database/SQLException;
    :try_start_c1
    const-string v0, "deleteCompleteLmsMsg(), SQLException occurs"

    #@c3
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_c6
    .catchall {:try_start_c1 .. :try_end_c6} :catchall_c9

    #@c6
    .line 2253
    if-eqz v6, :cond_80

    #@c8
    goto :goto_7d

    #@c9
    .end local v7           #e:Landroid/database/SQLException;
    :catchall_c9
    move-exception v0

    #@ca
    if-eqz v6, :cond_cf

    #@cc
    .line 2254
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@cf
    .line 2253
    :cond_cf
    throw v0
.end method

.method protected dispatchMessage(Lcom/android/internal/telephony/SmsMessageBase;)I
    .registers 15
    .parameter "smsb"

    #@0
    .prologue
    .line 307
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@2
    const-string v11, "lgu_dispatch"

    #@4
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v10

    #@8
    const/4 v11, 0x1

    #@9
    if-ne v10, v11, :cond_10

    #@b
    .line 308
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->dispatchMessageLgu(Lcom/android/internal/telephony/SmsMessageBase;)I

    #@e
    move-result v10

    #@f
    .line 484
    :goto_f
    return v10

    #@10
    .line 313
    :cond_10
    if-nez p1, :cond_1b

    #@12
    .line 314
    const-string v10, "CDMA"

    #@14
    const-string v11, "dispatchMessage: message is null"

    #@16
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 315
    const/4 v10, 0x2

    #@1a
    goto :goto_f

    #@1b
    .line 318
    :cond_1b
    const-string v10, "ril.cdma.inecmmode"

    #@1d
    const-string v11, "false"

    #@1f
    invoke-static {v10, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    .line 319
    .local v4, inEcm:Ljava/lang/String;
    const-string v10, "true"

    #@25
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v10

    #@29
    if-eqz v10, :cond_2d

    #@2b
    .line 320
    const/4 v10, -0x1

    #@2c
    goto :goto_f

    #@2d
    .line 323
    :cond_2d
    iget-boolean v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsReceiveDisabled:Z

    #@2f
    if-eqz v10, :cond_3a

    #@31
    .line 325
    const-string v10, "CDMA"

    #@33
    const-string v11, "Received short message on device which doesn\'t support receiving SMS. Ignored."

    #@35
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 327
    const/4 v10, 0x1

    #@39
    goto :goto_f

    #@3a
    :cond_3a
    move-object v7, p1

    #@3b
    .line 330
    check-cast v7, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@3d
    .line 333
    .local v7, sms:Lcom/android/internal/telephony/cdma/SmsMessage;
    const/4 v10, 0x1

    #@3e
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getMessageType()I

    #@41
    move-result v11

    #@42
    if-ne v10, v11, :cond_56

    #@44
    .line 334
    const-string v10, "CDMA"

    #@46
    const-string v11, "Broadcast type message"

    #@48
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 335
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->parseBroadcastSms()Landroid/telephony/SmsCbMessage;

    #@4e
    move-result-object v6

    #@4f
    .line 336
    .local v6, message:Landroid/telephony/SmsCbMessage;
    if-eqz v6, :cond_54

    #@51
    .line 337
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->dispatchBroadcastMessage(Landroid/telephony/SmsCbMessage;)V

    #@54
    .line 339
    :cond_54
    const/4 v10, 0x1

    #@55
    goto :goto_f

    #@56
    .line 343
    .end local v6           #message:Landroid/telephony/SmsCbMessage;
    :cond_56
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getIncomingSmsFingerprint()[B

    #@59
    move-result-object v10

    #@5a
    iput-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mLastDispatchedSmsFingerprint:[B

    #@5c
    .line 344
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mLastAcknowledgedSmsFingerprint:[B

    #@5e
    if-eqz v10, :cond_74

    #@60
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mLastDispatchedSmsFingerprint:[B

    #@62
    iget-object v11, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mLastAcknowledgedSmsFingerprint:[B

    #@64
    invoke-static {v10, v11}, Ljava/util/Arrays;->equals([B[B)Z

    #@67
    move-result v10

    #@68
    if-eqz v10, :cond_74

    #@6a
    .line 347
    const/4 v10, 0x0

    #@6b
    const-string v11, "kddi_message_duplicate_check"

    #@6d
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@70
    move-result v10

    #@71
    const/4 v11, 0x1

    #@72
    if-ne v10, v11, :cond_a6

    #@74
    .line 355
    :cond_74
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->parseSms()V

    #@77
    .line 356
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleService()I

    #@7a
    move-result v8

    #@7b
    .line 357
    .local v8, teleService:I
    const/4 v3, 0x0

    #@7c
    .line 360
    .local v3, handled:Z
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@7e
    const-string v11, "support_sprint_lock_and_wipe"

    #@80
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@83
    move-result v10

    #@84
    if-eqz v10, :cond_a9

    #@86
    .line 361
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@88
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@8b
    move-result-object v10

    #@8c
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8f
    move-result-object v10

    #@90
    const-string v11, "lg_omadm_lwmo_lock_state"

    #@92
    const/4 v12, 0x0

    #@93
    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@96
    move-result v2

    #@97
    .line 364
    .local v2, dmLockState:I
    const/4 v10, 0x1

    #@98
    if-ne v2, v10, :cond_a9

    #@9a
    const/16 v10, 0x1004

    #@9c
    if-eq v8, v10, :cond_a9

    #@9e
    .line 366
    const-string v10, "dispatchMessage(), Lock and Wipe - enabled - Block Inbound SMS"

    #@a0
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@a3
    .line 367
    const/4 v10, 0x2

    #@a4
    goto/16 :goto_f

    #@a6
    .line 350
    .end local v2           #dmLockState:I
    .end local v3           #handled:Z
    .end local v8           #teleService:I
    :cond_a6
    const/4 v10, 0x1

    #@a7
    goto/16 :goto_f

    #@a9
    .line 372
    .restart local v3       #handled:Z
    .restart local v8       #teleService:I
    :cond_a9
    const/16 v10, 0x1003

    #@ab
    if-eq v10, v8, :cond_b1

    #@ad
    const/high16 v10, 0x4

    #@af
    if-ne v10, v8, :cond_135

    #@b1
    .line 375
    :cond_b1
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@b3
    const-string v11, "support_sprint_vvm"

    #@b5
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@b8
    move-result v10

    #@b9
    if-eqz v10, :cond_f3

    #@bb
    .line 376
    const-string v10, "ro.chameleon.vvm"

    #@bd
    const-string v11, "1"

    #@bf
    invoke-static {v10, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c2
    move-result-object v0

    #@c3
    .line 377
    .local v0, chameleon_VVM:Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v11, "dispatchMessage(), chameleon_VVM : "

    #@ca
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v10

    #@ce
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v10

    #@d2
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v10

    #@d6
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@d9
    .line 379
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@db
    const-string v11, "cdma_sms_cdg2"

    #@dd
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e0
    move-result v10

    #@e1
    if-nez v10, :cond_f3

    #@e3
    const-string v10, "1"

    #@e5
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e8
    move-result v10

    #@e9
    if-eqz v10, :cond_f3

    #@eb
    .line 381
    const-string v10, "dispatchMessage(), teleService: Ignore VMN or MWI"

    #@ed
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@f0
    .line 382
    const/4 v10, 0x1

    #@f1
    goto/16 :goto_f

    #@f3
    .line 388
    .end local v0           #chameleon_VVM:Ljava/lang/String;
    :cond_f3
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getNumOfVoicemails()I

    #@f6
    move-result v9

    #@f7
    .line 389
    .local v9, voicemailCount:I
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getNumOfVoicemails()I

    #@fa
    move-result v10

    #@fb
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->updateMessageWaitingIndicator(I)V

    #@fe
    .line 391
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@100
    const-string v11, "cdma_urgent_vmwi"

    #@102
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@105
    move-result v10

    #@106
    if-eqz v10, :cond_12f

    #@108
    .line 392
    new-instance v10, Ljava/lang/StringBuilder;

    #@10a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@10d
    const-string v11, "dispatchMessage(), [SMS_VOICEMAIL] urgent voicemail sms.isMwiUrgentMessage=("

    #@10f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v10

    #@113
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->isMwiUrgentMessage()Z

    #@116
    move-result v11

    #@117
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v10

    #@11b
    const-string v11, ")"

    #@11d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v10

    #@121
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v10

    #@125
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@128
    .line 393
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->isMwiUrgentMessage()Z

    #@12b
    move-result v10

    #@12c
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->updateMessageWaitingIndicatorPriority(Z)V

    #@12f
    .line 396
    :cond_12f
    const/4 v3, 0x1

    #@130
    .line 412
    .end local v9           #voicemailCount:I
    :cond_130
    :goto_130
    if-eqz v3, :cond_159

    #@132
    .line 413
    const/4 v10, 0x1

    #@133
    goto/16 :goto_f

    #@135
    .line 397
    :cond_135
    const/16 v10, 0x1002

    #@137
    if-eq v10, v8, :cond_13d

    #@139
    const/16 v10, 0x1005

    #@13b
    if-ne v10, v8, :cond_148

    #@13d
    :cond_13d
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->isStatusReportMessage()Z

    #@140
    move-result v10

    #@141
    if-eqz v10, :cond_148

    #@143
    .line 400
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->handleCdmaStatusReport(Lcom/android/internal/telephony/cdma/SmsMessage;)V

    #@146
    .line 401
    const/4 v3, 0x1

    #@147
    goto :goto_130

    #@148
    .line 402
    :cond_148
    const/16 v10, 0x1006

    #@14a
    if-ne v10, v8, :cond_151

    #@14c
    .line 403
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->handleServiceCategoryProgramData(Lcom/android/internal/telephony/cdma/SmsMessage;)V

    #@14f
    .line 404
    const/4 v3, 0x1

    #@150
    goto :goto_130

    #@151
    .line 405
    :cond_151
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserData()[B

    #@154
    move-result-object v10

    #@155
    if-nez v10, :cond_130

    #@157
    .line 409
    const/4 v3, 0x1

    #@158
    goto :goto_130

    #@159
    .line 416
    :cond_159
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@15b
    invoke-virtual {v10}, Lcom/android/internal/telephony/SmsStorageMonitor;->isStorageAvailable()Z

    #@15e
    move-result v10

    #@15f
    if-nez v10, :cond_16c

    #@161
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getMessageClass()Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@164
    move-result-object v10

    #@165
    sget-object v11, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_0:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@167
    if-eq v10, v11, :cond_16c

    #@169
    .line 421
    const/4 v10, 0x3

    #@16a
    goto/16 :goto_f

    #@16c
    .line 424
    :cond_16c
    const/16 v10, 0x1004

    #@16e
    if-ne v10, v8, :cond_180

    #@170
    .line 425
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserData()[B

    #@173
    move-result-object v10

    #@174
    iget v11, v7, Lcom/android/internal/telephony/SmsMessageBase;->messageRef:I

    #@176
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@179
    move-result-object v12

    #@17a
    invoke-virtual {p0, v10, v11, v12}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->processCdmaWapPdu([BILjava/lang/String;)I

    #@17d
    move-result v10

    #@17e
    goto/16 :goto_f

    #@180
    .line 432
    :cond_180
    const/16 v10, 0x1002

    #@182
    if-eq v10, v8, :cond_192

    #@184
    const/16 v10, 0x1005

    #@186
    if-eq v10, v8, :cond_192

    #@188
    const/4 v10, 0x1

    #@189
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getMessageType()I

    #@18c
    move-result v11

    #@18d
    if-eq v10, v11, :cond_192

    #@18f
    .line 435
    const/4 v10, 0x4

    #@190
    goto/16 :goto_f

    #@192
    .line 439
    :cond_192
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@194
    const-string v11, "app_directed_sms"

    #@196
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@199
    move-result v10

    #@19a
    if-eqz v10, :cond_1cb

    #@19c
    .line 440
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->parseDirectedSMS(Lcom/android/internal/telephony/cdma/SmsMessage;)I

    #@19f
    move-result v1

    #@1a0
    .line 441
    .local v1, directedSmsStatus:I
    const/4 v10, 0x1

    #@1a1
    if-ne v10, v1, :cond_1ab

    #@1a3
    .line 442
    const-string v10, "dispatchMessage(), return parseDirectedSMS = true"

    #@1a5
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1a8
    .line 443
    const/4 v10, 0x1

    #@1a9
    goto/16 :goto_f

    #@1ab
    .line 444
    :cond_1ab
    if-nez v1, :cond_1b5

    #@1ad
    .line 445
    const-string v10, "dispatchMessage(), Discard!! there is no application for Application Directed SMS"

    #@1af
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1b2
    .line 446
    const/4 v10, 0x1

    #@1b3
    goto/16 :goto_f

    #@1b5
    .line 448
    :cond_1b5
    new-instance v10, Ljava/lang/StringBuilder;

    #@1b7
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1ba
    const-string v11, "dispatchMessage(), directedSmsStatus = "

    #@1bc
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v10

    #@1c0
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c3
    move-result-object v10

    #@1c4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c7
    move-result-object v10

    #@1c8
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1cb
    .line 454
    .end local v1           #directedSmsStatus:I
    :cond_1cb
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@1cd
    const-string v11, "vzw_snc_email_sms"

    #@1cf
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1d2
    move-result v10

    #@1d3
    if-eqz v10, :cond_1e3

    #@1d5
    .line 455
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isSncMessage(Lcom/android/internal/telephony/cdma/SmsMessage;)Z

    #@1d8
    move-result v10

    #@1d9
    if-eqz v10, :cond_1e3

    #@1db
    .line 456
    const-string v10, "dispatchMessage(), [SNC] AFW return isSncMessage = true"

    #@1dd
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1e0
    .line 457
    const/4 v10, 0x1

    #@1e1
    goto/16 :goto_f

    #@1e3
    .line 463
    :cond_1e3
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@1e5
    const-string v11, "cdma_kr_testbed_mms_receive"

    #@1e7
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1ea
    move-result v10

    #@1eb
    if-eqz v10, :cond_212

    #@1ed
    .line 464
    const/16 v10, 0x1002

    #@1ef
    if-ne v10, v8, :cond_212

    #@1f1
    .line 465
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserData()[B

    #@1f4
    move-result-object v10

    #@1f5
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mmsTestBedPushCheck([B)Z

    #@1f8
    move-result v10

    #@1f9
    const/4 v11, 0x1

    #@1fa
    if-ne v10, v11, :cond_212

    #@1fc
    .line 466
    const/16 v8, 0x1004

    #@1fe
    .line 467
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserData()[B

    #@201
    move-result-object v10

    #@202
    invoke-static {v10}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mmsTestBedGetMsgId([B)[B

    #@205
    move-result-object v10

    #@206
    sget v11, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->msgref:I

    #@208
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@20b
    move-result-object v12

    #@20c
    invoke-virtual {p0, v10, v11, v12}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->processCdmaWapPduKRTestBed([BILjava/lang/String;)I

    #@20f
    move-result v10

    #@210
    goto/16 :goto_f

    #@212
    .line 476
    :cond_212
    const/4 v10, 0x0

    #@213
    const-string v11, "emergency_alert_filtering"

    #@215
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@218
    move-result v10

    #@219
    const/4 v11, 0x1

    #@21a
    if-ne v10, v11, :cond_254

    #@21c
    .line 478
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSmsEnvelope()Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@21f
    move-result-object v5

    #@220
    .line 479
    .local v5, mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    new-instance v10, Ljava/lang/StringBuilder;

    #@222
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@225
    const-string v11, "dispatchMessage(),[KDDI] 1x Broadcasting Filtering start servicecategory = "

    #@227
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v10

    #@22b
    iget v11, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@22d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@230
    move-result-object v10

    #@231
    const-string v11, " sms type = "

    #@233
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@236
    move-result-object v10

    #@237
    iget v11, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@239
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23c
    move-result-object v10

    #@23d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@240
    move-result-object v10

    #@241
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@244
    .line 480
    iget v10, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@246
    const/4 v11, 0x1

    #@247
    if-ne v10, v11, :cond_254

    #@249
    iget v10, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@24b
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->kddiFilterCategory(I)Z

    #@24e
    move-result v10

    #@24f
    if-nez v10, :cond_254

    #@251
    .line 481
    const/4 v10, -0x1

    #@252
    goto/16 :goto_f

    #@254
    .line 484
    .end local v5           #mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    :cond_254
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->dispatchNormalMessage(Lcom/android/internal/telephony/SmsMessageBase;)I

    #@257
    move-result v10

    #@258
    goto/16 :goto_f
.end method

.method protected dispatchMessageLgu(Lcom/android/internal/telephony/SmsMessageBase;)I
    .registers 10
    .parameter "smsb"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 1481
    if-nez p1, :cond_a

    #@3
    .line 1482
    const-string v5, "dispatchMessageLgu(), message is null"

    #@5
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@8
    .line 1483
    const/4 v5, 0x2

    #@9
    .line 1595
    :cond_9
    :goto_9
    return v5

    #@a
    .line 1486
    :cond_a
    const-string v6, "ril.cdma.inecmmode"

    #@c
    const-string v7, "false"

    #@e
    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .line 1487
    .local v1, inEcm:Ljava/lang/String;
    const-string v6, "true"

    #@14
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v6

    #@18
    if-eqz v6, :cond_1c

    #@1a
    .line 1488
    const/4 v5, -0x1

    #@1b
    goto :goto_9

    #@1c
    .line 1491
    :cond_1c
    iget-boolean v6, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsReceiveDisabled:Z

    #@1e
    if-eqz v6, :cond_26

    #@20
    .line 1493
    const-string v6, "dispatchMessageLgu(), Received short message on device which doesn\'t support receiving SMS. Ignored."

    #@22
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@25
    goto :goto_9

    #@26
    :cond_26
    move-object v2, p1

    #@27
    .line 1498
    check-cast v2, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@29
    .line 1513
    .local v2, sms:Lcom/android/internal/telephony/cdma/SmsMessage;
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getIncomingSmsFingerprint()[B

    #@2c
    move-result-object v6

    #@2d
    iput-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mLastDispatchedSmsFingerprint:[B

    #@2f
    .line 1514
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mLastAcknowledgedSmsFingerprint:[B

    #@31
    if-eqz v6, :cond_3d

    #@33
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mLastDispatchedSmsFingerprint:[B

    #@35
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mLastAcknowledgedSmsFingerprint:[B

    #@37
    invoke-static {v6, v7}, Ljava/util/Arrays;->equals([B[B)Z

    #@3a
    move-result v6

    #@3b
    if-nez v6, :cond_9

    #@3d
    .line 1518
    :cond_3d
    const-string v6, "dispatchMessageLgu(), next: SmsMessage.parseSms"

    #@3f
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@42
    .line 1520
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->parseSms()V

    #@45
    .line 1521
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleService()I

    #@48
    move-result v3

    #@49
    .line 1522
    .local v3, teleService:I
    const/4 v0, 0x0

    #@4a
    .line 1524
    .local v0, handled:Z
    const/16 v6, 0x1003

    #@4c
    if-eq v6, v3, :cond_52

    #@4e
    const/high16 v6, 0x4

    #@50
    if-ne v6, v3, :cond_73

    #@52
    .line 1527
    :cond_52
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getNumOfVoicemails()I

    #@55
    move-result v4

    #@56
    .line 1532
    .local v4, voicemailCount:I
    if-nez v4, :cond_59

    #@58
    .line 1533
    const/4 v0, 0x1

    #@59
    .line 1561
    .end local v4           #voicemailCount:I
    :cond_59
    :goto_59
    if-nez v0, :cond_9

    #@5b
    .line 1565
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@5d
    invoke-virtual {v5}, Lcom/android/internal/telephony/SmsStorageMonitor;->isStorageAvailable()Z

    #@60
    move-result v5

    #@61
    if-nez v5, :cond_94

    #@63
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getMessageClass()Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@66
    move-result-object v5

    #@67
    sget-object v6, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_0:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@69
    if-eq v5, v6, :cond_94

    #@6b
    .line 1572
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->isMessageProcessTid(I)Z

    #@6e
    move-result v5

    #@6f
    if-eqz v5, :cond_94

    #@71
    .line 1573
    const/4 v5, 0x3

    #@72
    goto :goto_9

    #@73
    .line 1536
    :cond_73
    const/16 v6, 0x1005

    #@75
    if-ne v6, v3, :cond_82

    #@77
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->isStatusReportMessage()Z

    #@7a
    move-result v6

    #@7b
    if-eqz v6, :cond_82

    #@7d
    .line 1542
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->handleCdmaStatusReport(Lcom/android/internal/telephony/cdma/SmsMessage;)V

    #@80
    .line 1543
    const/4 v0, 0x1

    #@81
    goto :goto_59

    #@82
    .line 1544
    :cond_82
    const/16 v6, 0x1006

    #@84
    if-ne v6, v3, :cond_88

    #@86
    .line 1549
    const/4 v0, 0x1

    #@87
    goto :goto_59

    #@88
    .line 1550
    :cond_88
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserData()[B

    #@8b
    move-result-object v6

    #@8c
    if-nez v6, :cond_59

    #@8e
    const/16 v6, 0x1002

    #@90
    if-ne v3, v6, :cond_59

    #@92
    .line 1558
    const/4 v0, 0x1

    #@93
    goto :goto_59

    #@94
    .line 1578
    :cond_94
    const/16 v5, 0x1004

    #@96
    if-ne v5, v3, :cond_a8

    #@98
    .line 1579
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserData()[B

    #@9b
    move-result-object v5

    #@9c
    iget v6, v2, Lcom/android/internal/telephony/SmsMessageBase;->messageRef:I

    #@9e
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@a1
    move-result-object v7

    #@a2
    invoke-virtual {p0, v5, v6, v7}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->processCdmaWapPdu([BILjava/lang/String;)I

    #@a5
    move-result v5

    #@a6
    goto/16 :goto_9

    #@a8
    .line 1595
    :cond_a8
    invoke-direct {p0, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->handleLguMessage(Lcom/android/internal/telephony/cdma/SmsMessage;I)I

    #@ab
    move-result v5

    #@ac
    goto/16 :goto_9
.end method

.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnNewCdmaSms(Landroid/os/Handler;)V

    #@5
    .line 243
    return-void
.end method

.method protected enableAutoDCDisconnect(I)V
    .registers 3
    .parameter "timeOut"

    #@0
    .prologue
    .line 1466
    const-string v0, "enableAutoDCDisconnect()"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 1467
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->enableAutoDCDisconnect(I)V

    #@a
    .line 1468
    return-void
.end method

.method protected getFormat()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 247
    const-string v0, "3gpp2"

    #@2
    return-object v0
.end method

.method public getImsSmsFormat()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1422
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mImsSMSDispatcher:Lcom/android/internal/telephony/ImsSMSDispatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->getImsSmsFormat()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSessionIdLink()Ljava/util/LinkedList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2264
    sget-object v0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sSessionIdLink:Ljava/util/LinkedList;

    #@2
    return-object v0
.end method

.method public getSessionMap()Ljava/util/HashMap;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2260
    sget-object v0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sSessionMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public insertLmsMsg(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III[B)V
    .registers 19
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "currentSegment"
    .parameter "totalSegment"
    .parameter "pdu"

    #@0
    .prologue
    .line 2141
    const-string v0, "insertLmsMsg()"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@5
    .line 2143
    const/4 v6, 0x0

    #@6
    .line 2145
    .local v6, cursor:Landroid/database/Cursor;
    const/4 v0, 0x1

    #@7
    new-array v2, v0, [Ljava/lang/String;

    #@9
    const/4 v0, 0x0

    #@a
    const-string v1, "_id"

    #@c
    aput-object v1, v2, v0

    #@e
    .line 2150
    .local v2, projection:[Ljava/lang/String;
    :try_start_e
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@10
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x0

    #@14
    const/4 v5, 0x0

    #@15
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@18
    move-result-object v6

    #@19
    .line 2152
    if-nez v6, :cond_26

    #@1b
    .line 2153
    const-string v0, "insertLmsMsg(), cursor is null"

    #@1d
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_20
    .catchall {:try_start_e .. :try_end_20} :catchall_7f
    .catch Landroid/database/SQLException; {:try_start_e .. :try_end_20} :catch_76

    #@20
    .line 2178
    if-eqz v6, :cond_25

    #@22
    .line 2179
    :goto_22
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@25
    .line 2196
    :cond_25
    :goto_25
    return-void

    #@26
    .line 2157
    :cond_26
    :try_start_26
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@29
    move-result v0

    #@2a
    const/16 v1, 0x32

    #@2c
    if-lt v0, v1, :cond_87

    #@2e
    .line 2159
    const/4 v0, 0x1

    #@2f
    new-array v8, v0, [Ljava/lang/String;

    #@31
    const/4 v0, 0x0

    #@32
    const-string v1, "_id"

    #@34
    aput-object v1, v8, v0
    :try_end_36
    .catchall {:try_start_26 .. :try_end_36} :catchall_7f
    .catch Landroid/database/SQLException; {:try_start_26 .. :try_end_36} :catch_76

    #@36
    .line 2162
    .end local v2           #projection:[Ljava/lang/String;
    .local v8, projection:[Ljava/lang/String;
    :try_start_36
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@39
    move-result v0

    #@3a
    if-nez v0, :cond_48

    #@3c
    .line 2163
    const-string v0, "insertLmsMsg(), cursor moveToLast error"

    #@3e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_41
    .catchall {:try_start_36 .. :try_end_41} :catchall_ee
    .catch Landroid/database/SQLException; {:try_start_36 .. :try_end_41} :catch_f1

    #@41
    .line 2178
    if-eqz v6, :cond_46

    #@43
    .line 2179
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@46
    :cond_46
    move-object v2, v8

    #@47
    .line 2164
    .end local v8           #projection:[Ljava/lang/String;
    .restart local v2       #projection:[Ljava/lang/String;
    goto :goto_25

    #@48
    .line 2167
    .end local v2           #projection:[Ljava/lang/String;
    .restart local v8       #projection:[Ljava/lang/String;
    :cond_48
    :try_start_48
    const-string v0, "_id"

    #@4a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@4d
    move-result v0

    #@4e
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    #@51
    move-result-wide v9

    #@52
    .line 2168
    .local v9, rowId:J
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@54
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@56
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    invoke-static {v1, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@61
    move-result-object v1

    #@62
    const/4 v3, 0x0

    #@63
    const/4 v4, 0x0

    #@64
    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@67
    move-result v0

    #@68
    if-gez v0, :cond_86

    #@6a
    .line 2170
    const-string v0, "insertLmsMsg(), oldest lms delete fail"

    #@6c
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_6f
    .catchall {:try_start_48 .. :try_end_6f} :catchall_ee
    .catch Landroid/database/SQLException; {:try_start_48 .. :try_end_6f} :catch_f1

    #@6f
    .line 2178
    if-eqz v6, :cond_74

    #@71
    .line 2179
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@74
    :cond_74
    move-object v2, v8

    #@75
    .line 2171
    .end local v8           #projection:[Ljava/lang/String;
    .restart local v2       #projection:[Ljava/lang/String;
    goto :goto_25

    #@76
    .line 2174
    .end local v9           #rowId:J
    :catch_76
    move-exception v7

    #@77
    .line 2175
    .local v7, e:Landroid/database/SQLException;
    :goto_77
    :try_start_77
    const-string v0, "insertLmsMsg(), SQLException occurs"

    #@79
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_7c
    .catchall {:try_start_77 .. :try_end_7c} :catchall_7f

    #@7c
    .line 2178
    if-eqz v6, :cond_25

    #@7e
    goto :goto_22

    #@7f
    .end local v7           #e:Landroid/database/SQLException;
    :catchall_7f
    move-exception v0

    #@80
    :goto_80
    if-eqz v6, :cond_85

    #@82
    .line 2179
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@85
    .line 2178
    :cond_85
    throw v0

    #@86
    .end local v2           #projection:[Ljava/lang/String;
    .restart local v8       #projection:[Ljava/lang/String;
    .restart local v9       #rowId:J
    :cond_86
    move-object v2, v8

    #@87
    .end local v8           #projection:[Ljava/lang/String;
    .end local v9           #rowId:J
    .restart local v2       #projection:[Ljava/lang/String;
    :cond_87
    if-eqz v6, :cond_8c

    #@89
    .line 2179
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@8c
    .line 2183
    :cond_8c
    new-instance v11, Landroid/content/ContentValues;

    #@8e
    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    #@91
    .line 2184
    .local v11, value:Landroid/content/ContentValues;
    const-string v0, "source_min"

    #@93
    invoke-virtual {v11, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@96
    .line 2185
    const-string v0, "reference_number"

    #@98
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9b
    move-result-object v1

    #@9c
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@9f
    .line 2186
    const-string v0, "count"

    #@a1
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a4
    move-result-object v1

    #@a5
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@a8
    .line 2187
    const-string v0, "sequence"

    #@aa
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ad
    move-result-object v1

    #@ae
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@b1
    .line 2188
    const-string v0, "pdu"

    #@b3
    invoke-static/range {p6 .. p6}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@b6
    move-result-object v1

    #@b7
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@ba
    .line 2189
    const-string v0, "address"

    #@bc
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@bf
    move-result-object v1

    #@c0
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c3
    .line 2190
    const-string v0, "date"

    #@c5
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTimestampMillis()J

    #@c8
    move-result-wide v3

    #@c9
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@cc
    move-result-object v1

    #@cd
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@d0
    .line 2191
    const-string v0, "tid"

    #@d2
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleService()I

    #@d5
    move-result v1

    #@d6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d9
    move-result-object v1

    #@da
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@dd
    .line 2192
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@df
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@e1
    invoke-virtual {v0, v1, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@e4
    move-result-object v0

    #@e5
    if-nez v0, :cond_25

    #@e7
    .line 2193
    const-string v0, "insertLmsMsg(), lms insert fail"

    #@e9
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@ec
    goto/16 :goto_25

    #@ee
    .line 2178
    .end local v2           #projection:[Ljava/lang/String;
    .end local v11           #value:Landroid/content/ContentValues;
    .restart local v8       #projection:[Ljava/lang/String;
    :catchall_ee
    move-exception v0

    #@ef
    move-object v2, v8

    #@f0
    .end local v8           #projection:[Ljava/lang/String;
    .restart local v2       #projection:[Ljava/lang/String;
    goto :goto_80

    #@f1
    .line 2174
    .end local v2           #projection:[Ljava/lang/String;
    .restart local v8       #projection:[Ljava/lang/String;
    :catch_f1
    move-exception v7

    #@f2
    move-object v2, v8

    #@f3
    .end local v8           #projection:[Ljava/lang/String;
    .restart local v2       #projection:[Ljava/lang/String;
    goto :goto_77
.end method

.method public isIms()Z
    .registers 2

    #@0
    .prologue
    .line 1417
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mImsSMSDispatcher:Lcom/android/internal/telephony/ImsSMSDispatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isIms()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected isSncMessage(Lcom/android/internal/telephony/cdma/SmsMessage;)Z
    .registers 7
    .parameter "sms"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1429
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 1430
    .local v0, bodyMessage:Ljava/lang/String;
    if-nez v0, :cond_9

    #@7
    move v1, v3

    #@8
    .line 1461
    :cond_8
    :goto_8
    return v1

    #@9
    .line 1434
    :cond_9
    const/4 v1, 0x0

    #@a
    .line 1436
    .local v1, isSncSms:Z
    const-string v4, "VZWPIN"

    #@c
    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_22

    #@12
    .line 1437
    const/4 v1, 0x1

    #@13
    .line 1456
    :cond_13
    :goto_13
    if-eqz v1, :cond_8

    #@15
    .line 1457
    const/4 v4, 0x1

    #@16
    new-array v2, v4, [[B

    #@18
    .line 1458
    .local v2, pdus:[[B
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getPdu()[B

    #@1b
    move-result-object v4

    #@1c
    aput-object v4, v2, v3

    #@1e
    .line 1459
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->dispatchSncSms([[B)V

    #@21
    goto :goto_8

    #@22
    .line 1438
    .end local v2           #pdus:[[B
    :cond_22
    const-string v4, "VZWNMN"

    #@24
    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_2c

    #@2a
    .line 1440
    const/4 v1, 0x1

    #@2b
    goto :goto_13

    #@2c
    .line 1441
    :cond_2c
    const-string v4, "VZWUEP"

    #@2e
    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@31
    move-result v4

    #@32
    if-eqz v4, :cond_36

    #@34
    .line 1443
    const/4 v1, 0x1

    #@35
    goto :goto_13

    #@36
    .line 1444
    :cond_36
    const-string v4, "VZWRSC"

    #@38
    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@3b
    move-result v4

    #@3c
    if-eqz v4, :cond_40

    #@3e
    .line 1446
    const/4 v1, 0x1

    #@3f
    goto :goto_13

    #@40
    .line 1447
    :cond_40
    const-string v4, "VZWSIP"

    #@42
    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@45
    move-result v4

    #@46
    if-eqz v4, :cond_4a

    #@48
    .line 1450
    const/4 v1, 0x1

    #@49
    goto :goto_13

    #@4a
    .line 1451
    :cond_4a
    const-string v4, "VZWSCI"

    #@4c
    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@4f
    move-result v4

    #@50
    if-eqz v4, :cond_13

    #@52
    .line 1453
    const/4 v1, 0x1

    #@53
    goto :goto_13
.end method

.method isSystemApplication(Landroid/content/pm/ApplicationInfo;)Z
    .registers 3
    .parameter "appInfo"

    #@0
    .prologue
    .line 1199
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 1200
    const-string v0, "isSystemApplication(), system app"

    #@8
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@b
    .line 1201
    const/4 v0, 0x1

    #@c
    .line 1203
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public kddiFilterCategory(I)Z
    .registers 15
    .parameter "serviceCategory"

    #@0
    .prologue
    const v12, 0x8021

    #@3
    const v11, 0x8001

    #@6
    const/16 v10, 0x3f

    #@8
    const/16 v9, 0x21

    #@a
    const/4 v8, 0x1

    #@b
    .line 490
    const-string v5, "kddiFilterCategory(), [KDDI] kddiFilterCategory !!"

    #@d
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@10
    .line 491
    const/4 v3, 0x0

    #@11
    .line 495
    .local v3, maintenanceMode:I
    :try_start_11
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@13
    const-string v6, "com.kddi.maintenanceMode"

    #@15
    iget-object v7, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@17
    const/4 v7, 0x2

    #@18
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@1b
    move-result-object v0

    #@1c
    .line 496
    .local v0, context:Landroid/content/Context;
    const-string v5, "pref"

    #@1e
    iget-object v6, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@20
    const/4 v6, 0x4

    #@21
    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@24
    move-result-object v4

    #@25
    .line 498
    .local v4, pref:Landroid/content/SharedPreferences;
    const-string v5, "maintenanceMode"

    #@27
    const/4 v6, 0x0

    #@28
    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_2b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_11 .. :try_end_2b} :catch_5d

    #@2b
    move-result v3

    #@2c
    .line 503
    .end local v0           #context:Landroid/content/Context;
    .end local v4           #pref:Landroid/content/SharedPreferences;
    :goto_2c
    new-instance v5, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v6, "kddiFilterCategory(), [KDDI] Maintainanace mode value = "

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@42
    .line 505
    const/4 v2, 0x0

    #@43
    .line 506
    .local v2, isDelivery:Z
    packed-switch v3, :pswitch_data_b6

    #@46
    .line 533
    :cond_46
    :goto_46
    new-instance v5, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v6, "kddiFilterCategory(), [KDDI] isDelivery = "

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v5

    #@59
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5c
    .line 534
    return v2

    #@5d
    .line 499
    .end local v2           #isDelivery:Z
    :catch_5d
    move-exception v1

    #@5e
    .line 500
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "kddiFilterCategory(), [KDDI] maintenanceMode app not found"

    #@60
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@63
    goto :goto_2c

    #@64
    .line 508
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2       #isDelivery:Z
    :pswitch_64
    const-string v5, "kddiFilterCategory(),[KDDI]  COMMERCIAL_MODE "

    #@66
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@69
    .line 509
    if-eq p1, v8, :cond_6f

    #@6b
    if-gt v9, p1, :cond_46

    #@6d
    if-gt p1, v10, :cond_46

    #@6f
    .line 510
    :cond_6f
    const/4 v2, 0x1

    #@70
    goto :goto_46

    #@71
    .line 515
    :pswitch_71
    const-string v5, "kddiFilterCategory(),[KDDI] TEST_MODE "

    #@73
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@76
    .line 516
    const v5, 0xc001

    #@79
    if-eq p1, v5, :cond_8e

    #@7b
    const v5, 0xc021

    #@7e
    if-gt v5, p1, :cond_85

    #@80
    const v5, 0xc03f

    #@83
    if-le p1, v5, :cond_8e

    #@85
    :cond_85
    if-eq p1, v11, :cond_8e

    #@87
    if-gt v12, p1, :cond_46

    #@89
    const v5, 0x803f

    #@8c
    if-gt p1, v5, :cond_46

    #@8e
    .line 518
    :cond_8e
    const/4 v2, 0x1

    #@8f
    goto :goto_46

    #@90
    .line 522
    :pswitch_90
    const-string v5, "kddiFilterCategory(), [KDDI] ALLRECEIVE_MODE "

    #@92
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@95
    .line 523
    if-eq p1, v8, :cond_b3

    #@97
    if-gt v9, p1, :cond_9b

    #@99
    if-le p1, v10, :cond_b3

    #@9b
    :cond_9b
    const v5, 0xc001

    #@9e
    if-eq p1, v5, :cond_b3

    #@a0
    const v5, 0xc021

    #@a3
    if-gt v5, p1, :cond_aa

    #@a5
    const v5, 0xc03f

    #@a8
    if-le p1, v5, :cond_b3

    #@aa
    :cond_aa
    if-eq p1, v11, :cond_b3

    #@ac
    if-gt v12, p1, :cond_46

    #@ae
    const v5, 0x803f

    #@b1
    if-gt p1, v5, :cond_46

    #@b3
    .line 526
    :cond_b3
    const/4 v2, 0x1

    #@b4
    goto :goto_46

    #@b5
    .line 506
    nop

    #@b6
    :pswitch_data_b6
    .packed-switch 0x0
        :pswitch_64
        :pswitch_71
        :pswitch_71
        :pswitch_90
    .end packed-switch
.end method

.method protected parseDirectedSMS(Lcom/android/internal/telephony/cdma/SmsMessage;)I
    .registers 17
    .parameter "sms"

    #@0
    .prologue
    .line 1207
    const/4 v10, 0x0

    #@1
    .line 1208
    .local v10, processStatus:I
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@4
    move-result-object v13

    #@5
    if-nez v13, :cond_f

    #@7
    .line 1209
    const-string v13, "parseDirectedSMS(), sms.getMessageBody() is NULL "

    #@9
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@c
    .line 1210
    const/4 v10, -0x1

    #@d
    move v11, v10

    #@e
    .line 1291
    .end local v10           #processStatus:I
    .local v11, processStatus:I
    :goto_e
    return v11

    #@f
    .line 1212
    .end local v11           #processStatus:I
    .restart local v10       #processStatus:I
    :cond_f
    new-instance v13, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v14, "parseDirectedSMS(), sms.getMessageBody() = "

    #@16
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v13

    #@1a
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@1d
    move-result-object v14

    #@1e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v13

    #@22
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v13

    #@26
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@29
    .line 1215
    new-instance v4, Ljava/util/ArrayList;

    #@2b
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@2e
    .line 1216
    .local v4, applications:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    #@2f
    .line 1220
    .local v7, packageIndex:I
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@32
    move-result-object v13

    #@33
    const-string v14, "//VZW"

    #@35
    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@38
    move-result v13

    #@39
    if-nez v13, :cond_43

    #@3b
    .line 1221
    const-string v13, "parseDirectedSMS(), normal sms - not startsWith //VZW "

    #@3d
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@40
    .line 1222
    const/4 v10, -0x1

    #@41
    move v11, v10

    #@42
    .end local v10           #processStatus:I
    .restart local v11       #processStatus:I
    goto :goto_e

    #@43
    .line 1225
    .end local v11           #processStatus:I
    .restart local v10       #processStatus:I
    :cond_43
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    .line 1226
    .local v0, appDirectedSMS:Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v14, "parseDirectedSMS(), parseDirectedSMS / appDirectedSMS : "

    #@4e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v13

    #@52
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v13

    #@56
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v13

    #@5a
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@5d
    .line 1229
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@60
    move-result v13

    #@61
    const/16 v14, 0xe

    #@63
    if-le v13, v14, :cond_7c

    #@65
    const/4 v13, 0x0

    #@66
    const/16 v14, 0xd

    #@68
    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6b
    move-result-object v13

    #@6c
    const-string v14, "//VZWLBSROVER"

    #@6e
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v13

    #@72
    if-eqz v13, :cond_7c

    #@74
    .line 1230
    const-string v13, "parseDirectedSMS(), //VZWLBSROVER"

    #@76
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@79
    .line 1231
    const/4 v10, -0x1

    #@7a
    move v11, v10

    #@7b
    .end local v10           #processStatus:I
    .restart local v11       #processStatus:I
    goto :goto_e

    #@7c
    .line 1236
    .end local v11           #processStatus:I
    .restart local v10       #processStatus:I
    :cond_7c
    const-string v13, "//VZW"

    #@7e
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@81
    move-result v13

    #@82
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@85
    move-result v14

    #@86
    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@89
    move-result-object v12

    #@8a
    .line 1239
    .local v12, tempSMS:Ljava/lang/String;
    const-string v13, ":"

    #@8c
    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@8f
    move-result v13

    #@90
    const/4 v14, -0x1

    #@91
    if-ne v13, v14, :cond_9c

    #@93
    .line 1240
    const-string v13, "parseDirectedSMS(), check separator"

    #@95
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@98
    .line 1241
    const/4 v10, -0x1

    #@99
    move v11, v10

    #@9a
    .end local v10           #processStatus:I
    .restart local v11       #processStatus:I
    goto/16 :goto_e

    #@9c
    .line 1245
    .end local v11           #processStatus:I
    .restart local v10       #processStatus:I
    :cond_9c
    iget-object v13, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@9e
    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@a1
    move-result-object v9

    #@a2
    .line 1246
    .local v9, pkgManager:Landroid/content/pm/PackageManager;
    const/16 v13, 0x80

    #@a4
    invoke-virtual {v9, v13}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    #@a7
    move-result-object v6

    #@a8
    .line 1249
    .local v6, installedAppList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getVZWSignatures(Landroid/content/pm/PackageManager;)Z

    #@ab
    .line 1251
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@ae
    move-result-object v5

    #@af
    .local v5, i$:Ljava/util/Iterator;
    :goto_af
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@b2
    move-result v13

    #@b3
    if-eqz v13, :cond_18f

    #@b5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@b8
    move-result-object v1

    #@b9
    check-cast v1, Landroid/content/pm/ApplicationInfo;

    #@bb
    .line 1252
    .local v1, appInfo:Landroid/content/pm/ApplicationInfo;
    new-instance v13, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v14, "parseDirectedSMS(), ("

    #@c2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v13

    #@c6
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v13

    #@ca
    const-string v14, ")"

    #@cc
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v13

    #@d0
    const-string v14, " appInfo.packageName: "

    #@d2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v13

    #@d6
    iget-object v14, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@d8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v13

    #@dc
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v13

    #@e0
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@e3
    .line 1253
    iget-object v13, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@e5
    invoke-direct {p0, v9, v13}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isItSignedByVZW(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    #@e8
    move-result v13

    #@e9
    if-nez v13, :cond_f1

    #@eb
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isSystemApplication(Landroid/content/pm/ApplicationInfo;)Z

    #@ee
    move-result v13

    #@ef
    if-eqz v13, :cond_132

    #@f1
    .line 1255
    :cond_f1
    iget-object v13, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@f3
    if-nez v13, :cond_fd

    #@f5
    .line 1256
    const-string v13, "parseDirectedSMS(), appInfo.metaData == null"

    #@f7
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@fa
    .line 1257
    add-int/lit8 v7, v7, 0x1

    #@fc
    .line 1258
    goto :goto_af

    #@fd
    .line 1262
    :cond_fd
    iget-object v13, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@ff
    const-string v14, "com.verizon.directedAppSMS"

    #@101
    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@104
    move-result-object v2

    #@105
    .line 1263
    .local v2, applicationPrefix:Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    const-string v14, "parseDirectedSMS(), appInfo = "

    #@10c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v13

    #@110
    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v13

    #@114
    const-string v14, " appInfo.metaData = "

    #@116
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v13

    #@11a
    iget-object v14, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@11c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v13

    #@120
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@123
    move-result-object v13

    #@124
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@127
    .line 1265
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@12a
    move-result v13

    #@12b
    if-eqz v13, :cond_136

    #@12d
    .line 1267
    const-string v13, "parseDirectedSMS(), applicationPrefix is Empty"

    #@12f
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@132
    .line 1286
    .end local v2           #applicationPrefix:Ljava/lang/String;
    :cond_132
    add-int/lit8 v7, v7, 0x1

    #@134
    goto/16 :goto_af

    #@136
    .line 1269
    .restart local v2       #applicationPrefix:Ljava/lang/String;
    :cond_136
    new-instance v13, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v14, "parseDirectedSMS(), applicationPrefix: "

    #@13d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v13

    #@141
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v13

    #@145
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@148
    move-result-object v13

    #@149
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@14c
    .line 1272
    const/4 v13, 0x0

    #@14d
    const-string v14, ":"

    #@14f
    invoke-virtual {v12, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@152
    move-result v14

    #@153
    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@156
    move-result-object v3

    #@157
    .line 1273
    .local v3, applicationPrefixFromSMSBody:Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    #@159
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@15c
    const-string v14, "parseDirectedSMS(), applicationPrefixFromSMSBody: "

    #@15e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v13

    #@162
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v13

    #@166
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@169
    move-result-object v13

    #@16a
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@16d
    .line 1275
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@170
    move-result v13

    #@171
    if-eqz v13, :cond_132

    #@173
    .line 1276
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@176
    move-result v13

    #@177
    add-int/lit8 v13, v13, 0x1

    #@179
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@17c
    move-result v14

    #@17d
    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@180
    move-result-object v8

    #@181
    .line 1277
    .local v8, parameters:Ljava/lang/String;
    const-string v13, "parseDirectedSMS(), dispatchDirectedSms!!"

    #@183
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@186
    .line 1279
    iget-object v13, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@188
    invoke-virtual {p0, v13, v8}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->dispatchDirectedSms(Ljava/lang/String;Ljava/lang/String;)V

    #@18b
    .line 1281
    const/4 v10, 0x1

    #@18c
    move v11, v10

    #@18d
    .line 1282
    .end local v10           #processStatus:I
    .restart local v11       #processStatus:I
    goto/16 :goto_e

    #@18f
    .line 1288
    .end local v1           #appInfo:Landroid/content/pm/ApplicationInfo;
    .end local v2           #applicationPrefix:Ljava/lang/String;
    .end local v3           #applicationPrefixFromSMSBody:Ljava/lang/String;
    .end local v8           #parameters:Ljava/lang/String;
    .end local v11           #processStatus:I
    .restart local v10       #processStatus:I
    :cond_18f
    const/4 v10, 0x0

    #@190
    .line 1290
    new-instance v13, Ljava/lang/StringBuilder;

    #@192
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@195
    const-string v14, "parseDirectedSMS(), processStatus=("

    #@197
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v13

    #@19b
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v13

    #@19f
    const-string v14, ")"

    #@1a1
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v13

    #@1a5
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a8
    move-result-object v13

    #@1a9
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1ac
    move v11, v10

    #@1ad
    .line 1291
    .end local v10           #processStatus:I
    .restart local v11       #processStatus:I
    goto/16 :goto_e
.end method

.method public processCbsMessage(Lcom/android/internal/telephony/cdma/SmsMessage;[[B)I
    .registers 15
    .parameter "sms"
    .parameter "pdus"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 2268
    new-instance v9, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v11, "deleteCompleteLmsMsg(), CBS parse star = "

    #@8
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v9

    #@c
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSmsEnvelope()Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@f
    move-result-object v11

    #@10
    iget v11, v11, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@12
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v9

    #@16
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v9

    #@1a
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1d
    .line 2269
    const/4 v4, 0x0

    #@1e
    .line 2270
    .local v4, pdu:[B
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserData()[B

    #@21
    move-result-object v8

    #@22
    .line 2273
    .local v8, userData:[B
    :try_start_22
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getPdu()[B
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_25} :catch_39

    #@25
    move-result-object v4

    #@26
    .line 2278
    :goto_26
    const/16 v9, 0x2be

    #@28
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSmsEnvelope()Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@2b
    move-result-object v11

    #@2c
    iget v11, v11, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@2e
    if-ne v9, v11, :cond_3e

    #@30
    .line 2279
    const-string v9, "deleteCompleteLmsMsg(), CBS EMERGENCY_MSG "

    #@32
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@35
    .line 2284
    :cond_35
    if-nez v4, :cond_4d

    #@37
    move v9, v10

    #@38
    .line 2341
    :goto_38
    return v9

    #@39
    .line 2274
    :catch_39
    move-exception v1

    #@3a
    .line 2275
    .local v1, e1:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@3d
    goto :goto_26

    #@3e
    .line 2280
    .end local v1           #e1:Ljava/lang/Exception;
    :cond_3e
    const/16 v9, 0x7000

    #@40
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSmsEnvelope()Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@43
    move-result-object v11

    #@44
    iget v11, v11, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@46
    if-ne v9, v11, :cond_35

    #@48
    .line 2281
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->parseImsMsg([B)I

    #@4b
    move-result v9

    #@4c
    goto :goto_38

    #@4d
    .line 2289
    :cond_4d
    const/4 v2, 0x0

    #@4e
    .local v2, loop:I
    :goto_4e
    array-length v9, v8

    #@4f
    if-ge v2, v9, :cond_76

    #@51
    .line 2290
    new-instance v9, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v11, "deleteCompleteLmsMsg(), userData["

    #@58
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v9

    #@5c
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v9

    #@60
    const-string v11, "] = "

    #@62
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v9

    #@66
    aget-byte v11, v8, v2

    #@68
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v9

    #@6c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v9

    #@70
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@73
    .line 2289
    add-int/lit8 v2, v2, 0x1

    #@75
    goto :goto_4e

    #@76
    .line 2293
    :cond_76
    new-instance v9, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v11, "deleteCompleteLmsMsg(), CBS session = "

    #@7d
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v9

    #@81
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@84
    move-result-object v11

    #@85
    iget v11, v11, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessioinId:I

    #@87
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v9

    #@8b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v9

    #@8f
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@92
    .line 2294
    new-instance v9, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    const-string v11, "deleteCompleteLmsMsg(), CBS eos = "

    #@99
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v9

    #@9d
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@a0
    move-result-object v11

    #@a1
    iget v11, v11, Lcom/android/internal/telephony/cdma/sms/BearerData;->endOfSession:I

    #@a3
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v9

    #@a7
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v9

    #@ab
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@ae
    .line 2295
    new-instance v9, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v11, "deleteCompleteLmsMsg(), CBS sessionSeq = "

    #@b5
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v9

    #@b9
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@bc
    move-result-object v11

    #@bd
    iget v11, v11, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessionSeq:I

    #@bf
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v9

    #@c3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v9

    #@c7
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@ca
    .line 2296
    new-instance v9, Ljava/lang/StringBuilder;

    #@cc
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@cf
    const-string v11, "deleteCompleteLmsMsg(), CBS Session Map = "

    #@d1
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v9

    #@d5
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSessionMap()Ljava/util/HashMap;

    #@d8
    move-result-object v11

    #@d9
    invoke-virtual {v11}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    #@dc
    move-result-object v11

    #@dd
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v9

    #@e1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e4
    move-result-object v9

    #@e5
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@e8
    .line 2299
    const/4 v7, 0x0

    #@e9
    .line 2300
    .local v7, tSeqMap:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@ec
    move-result-object v9

    #@ed
    iget v0, v9, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessioinId:I

    #@ef
    .line 2301
    .local v0, cbsSid:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSessionMap()Ljava/util/HashMap;

    #@f2
    move-result-object v9

    #@f3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f6
    move-result-object v11

    #@f7
    invoke-virtual {v9, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@fa
    move-result v9

    #@fb
    if-eqz v9, :cond_18d

    #@fd
    .line 2303
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSessionMap()Ljava/util/HashMap;

    #@100
    move-result-object v9

    #@101
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@104
    move-result-object v11

    #@105
    invoke-virtual {v9, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@108
    move-result-object v9

    #@109
    check-cast v9, Ljava/util/TreeMap;

    #@10b
    move-object v7, v9

    #@10c
    check-cast v7, Ljava/util/TreeMap;

    #@10e
    .line 2317
    :goto_10e
    if-nez v7, :cond_115

    #@110
    .line 2319
    new-instance v7, Ljava/util/TreeMap;

    #@112
    .end local v7           #tSeqMap:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    invoke-direct {v7}, Ljava/util/TreeMap;-><init>()V

    #@115
    .line 2323
    .restart local v7       #tSeqMap:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    :cond_115
    new-instance v3, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;

    #@117
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@11a
    move-result-object v9

    #@11b
    iget v9, v9, Lcom/android/internal/telephony/cdma/sms/BearerData;->endOfSession:I

    #@11d
    invoke-direct {v3, p0, v9, v4}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;-><init>(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;I[B)V

    #@120
    .line 2324
    .local v3, msgSeq:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@123
    move-result-object v9

    #@124
    iget v9, v9, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessionSeq:I

    #@126
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@129
    move-result-object v9

    #@12a
    invoke-virtual {v7, v9, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@12d
    .line 2326
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSessionMap()Ljava/util/HashMap;

    #@130
    move-result-object v9

    #@131
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@134
    move-result-object v11

    #@135
    iget v11, v11, Lcom/android/internal/telephony/cdma/sms/BearerData;->sessioinId:I

    #@137
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13a
    move-result-object v11

    #@13b
    invoke-virtual {v9, v11, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@13e
    .line 2330
    new-instance v9, Ljava/lang/StringBuilder;

    #@140
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@143
    const-string v11, "deleteCompleteLmsMsg(), CBS Session Map put ="

    #@145
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@148
    move-result-object v9

    #@149
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSessionMap()Ljava/util/HashMap;

    #@14c
    move-result-object v11

    #@14d
    invoke-virtual {v11}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    #@150
    move-result-object v11

    #@151
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v9

    #@155
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@158
    move-result-object v9

    #@159
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@15c
    .line 2331
    new-instance v9, Ljava/lang/StringBuilder;

    #@15e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@161
    const-string v11, "deleteCompleteLmsMsg(), CBS isEos()= "

    #@163
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v9

    #@167
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isEos(Lcom/android/internal/telephony/cdma/SmsMessage;)Z

    #@16a
    move-result v11

    #@16b
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v9

    #@16f
    const-string v11, ", isSeqFull()="

    #@171
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v9

    #@175
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isSeqFull(Lcom/android/internal/telephony/cdma/SmsMessage;)Z

    #@178
    move-result v11

    #@179
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v9

    #@17d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@180
    move-result-object v9

    #@181
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@184
    .line 2334
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isMsgEnd(Lcom/android/internal/telephony/cdma/SmsMessage;)Z

    #@187
    move-result v9

    #@188
    if-nez v9, :cond_1b0

    #@18a
    move v9, v10

    #@18b
    .line 2335
    goto/16 :goto_38

    #@18d
    .line 2306
    .end local v3           #msgSeq:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;
    :cond_18d
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getSessionIdLink()Ljava/util/LinkedList;

    #@190
    move-result-object v6

    #@191
    .line 2307
    .local v6, sessionLink:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Ljava/lang/Object;>;"
    :cond_191
    :goto_191
    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    #@194
    move-result v9

    #@195
    const/16 v11, 0x9

    #@197
    if-le v9, v11, :cond_1a9

    #@199
    .line 2308
    invoke-virtual {v6}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    #@19c
    move-result-object v5

    #@19d
    check-cast v5, Ljava/lang/Integer;

    #@19f
    .line 2309
    .local v5, session:Ljava/lang/Integer;
    if-eqz v5, :cond_191

    #@1a1
    .line 2310
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@1a4
    move-result v9

    #@1a5
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->clearSession(I)V

    #@1a8
    goto :goto_191

    #@1a9
    .line 2314
    .end local v5           #session:Ljava/lang/Integer;
    :cond_1a9
    new-instance v7, Ljava/util/TreeMap;

    #@1ab
    .end local v7           #tSeqMap:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    invoke-direct {v7}, Ljava/util/TreeMap;-><init>()V

    #@1ae
    .restart local v7       #tSeqMap:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    goto/16 :goto_10e

    #@1b0
    .line 2338
    .end local v6           #sessionLink:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Ljava/lang/Object;>;"
    .restart local v3       #msgSeq:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$MsgSequence;
    :cond_1b0
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->makeCompleteText(Lcom/android/internal/telephony/cdma/SmsMessage;[[B)V

    #@1b3
    .line 2340
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->clearSession(Lcom/android/internal/telephony/cdma/SmsMessage;)V

    #@1b6
    .line 2341
    const/4 v9, -0x1

    #@1b7
    goto/16 :goto_38
.end method

.method protected processCdmaWapPdu([BILjava/lang/String;)I
    .registers 19
    .parameter "pdu"
    .parameter "referenceNumber"
    .parameter "address"

    #@0
    .prologue
    .line 568
    const/4 v11, 0x0

    #@1
    .line 570
    .local v11, index:I
    add-int/lit8 v12, v11, 0x1

    #@3
    .end local v11           #index:I
    .local v12, index:I
    aget-byte v1, p1, v11

    #@5
    and-int/lit16 v13, v1, 0xff

    #@7
    .line 571
    .local v13, msgType:I
    if-eqz v13, :cond_13

    #@9
    .line 572
    const-string v1, "CDMA"

    #@b
    const-string v3, "Received a WAP SMS which is not WDP. Discard."

    #@d
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 573
    const/4 v1, 0x1

    #@11
    move v11, v12

    #@12
    .line 610
    .end local v12           #index:I
    .restart local v11       #index:I
    :goto_12
    return v1

    #@13
    .line 575
    .end local v11           #index:I
    .restart local v12       #index:I
    :cond_13
    add-int/lit8 v11, v12, 0x1

    #@15
    .end local v12           #index:I
    .restart local v11       #index:I
    aget-byte v1, p1, v12

    #@17
    and-int/lit16 v6, v1, 0xff

    #@19
    .line 576
    .local v6, totalSegments:I
    add-int/lit8 v12, v11, 0x1

    #@1b
    .end local v11           #index:I
    .restart local v12       #index:I
    aget-byte v1, p1, v11

    #@1d
    and-int/lit16 v5, v1, 0xff

    #@1f
    .line 578
    .local v5, segment:I
    if-lt v5, v6, :cond_48

    #@21
    .line 579
    const-string v1, "CDMA"

    #@23
    new-instance v3, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v4, "WDP bad segment #"

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    const-string v4, " expecting 0-"

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    add-int/lit8 v4, v6, -0x1

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 580
    const/4 v1, 0x1

    #@46
    move v11, v12

    #@47
    .end local v12           #index:I
    .restart local v11       #index:I
    goto :goto_12

    #@48
    .line 584
    .end local v11           #index:I
    .restart local v12       #index:I
    :cond_48
    const/4 v14, 0x0

    #@49
    .line 585
    .local v14, sourcePort:I
    const/4 v9, 0x0

    #@4a
    .line 586
    .local v9, destinationPort:I
    if-nez v5, :cond_ee

    #@4c
    .line 588
    add-int/lit8 v11, v12, 0x1

    #@4e
    .end local v12           #index:I
    .restart local v11       #index:I
    aget-byte v1, p1, v12

    #@50
    and-int/lit16 v1, v1, 0xff

    #@52
    shl-int/lit8 v14, v1, 0x8

    #@54
    .line 589
    add-int/lit8 v12, v11, 0x1

    #@56
    .end local v11           #index:I
    .restart local v12       #index:I
    aget-byte v1, p1, v11

    #@58
    and-int/lit16 v1, v1, 0xff

    #@5a
    or-int/2addr v14, v1

    #@5b
    .line 590
    add-int/lit8 v11, v12, 0x1

    #@5d
    .end local v12           #index:I
    .restart local v11       #index:I
    aget-byte v1, p1, v12

    #@5f
    and-int/lit16 v1, v1, 0xff

    #@61
    shl-int/lit8 v9, v1, 0x8

    #@63
    .line 591
    add-int/lit8 v12, v11, 0x1

    #@65
    .end local v11           #index:I
    .restart local v12       #index:I
    aget-byte v1, p1, v11

    #@67
    and-int/lit16 v1, v1, 0xff

    #@69
    or-int/2addr v9, v1

    #@6a
    .line 594
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mCheckForDuplicatePortsInOmadmWapPush:Z

    #@6c
    if-eqz v1, :cond_ee

    #@6e
    .line 595
    move-object/from16 v0, p1

    #@70
    invoke-static {v0, v12}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->checkDuplicatePortOmadmWappush([BI)Z

    #@73
    move-result v1

    #@74
    if-eqz v1, :cond_ee

    #@76
    .line 596
    add-int/lit8 v11, v12, 0x4

    #@78
    .line 602
    .end local v12           #index:I
    .restart local v11       #index:I
    :goto_78
    const-string v1, "CDMA"

    #@7a
    new-instance v3, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v4, "Received WAP PDU. Type = "

    #@81
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v3

    #@85
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@88
    move-result-object v3

    #@89
    const-string v4, ", originator = "

    #@8b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v3

    #@8f
    move-object/from16 v0, p3

    #@91
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v3

    #@95
    const-string v4, ", src-port = "

    #@97
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v3

    #@9b
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v3

    #@9f
    const-string v4, ", dst-port = "

    #@a1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v3

    #@a9
    const-string v4, ", ID = "

    #@ab
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v3

    #@af
    move/from16 v0, p2

    #@b1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v3

    #@b5
    const-string v4, ", segment# = "

    #@b7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v3

    #@bb
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@be
    move-result-object v3

    #@bf
    const/16 v4, 0x2f

    #@c1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v3

    #@c5
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v3

    #@c9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v3

    #@cd
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d0
    .line 607
    move-object/from16 v0, p1

    #@d2
    array-length v1, v0

    #@d3
    sub-int/2addr v1, v11

    #@d4
    new-array v2, v1, [B

    #@d6
    .line 608
    .local v2, userData:[B
    const/4 v1, 0x0

    #@d7
    move-object/from16 v0, p1

    #@d9
    array-length v3, v0

    #@da
    sub-int/2addr v3, v11

    #@db
    move-object/from16 v0, p1

    #@dd
    invoke-static {v0, v11, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@e0
    .line 610
    const-wide/16 v7, 0x0

    #@e2
    const/4 v10, 0x1

    #@e3
    move-object v1, p0

    #@e4
    move-object/from16 v3, p3

    #@e6
    move/from16 v4, p2

    #@e8
    invoke-virtual/range {v1 .. v10}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->processMessagePart([BLjava/lang/String;IIIJIZ)I

    #@eb
    move-result v1

    #@ec
    goto/16 :goto_12

    #@ee
    .end local v2           #userData:[B
    .end local v11           #index:I
    .restart local v12       #index:I
    :cond_ee
    move v11, v12

    #@ef
    .end local v12           #index:I
    .restart local v11       #index:I
    goto :goto_78
.end method

.method protected processCdmaWapPduKRTestBed([BILjava/lang/String;)I
    .registers 20
    .parameter "pdu"
    .parameter "referenceNumber"
    .parameter "address"

    #@0
    .prologue
    .line 2477
    const/4 v12, 0x0

    #@1
    .line 2479
    .local v12, index:I
    add-int/lit8 v13, v12, 0x1

    #@3
    .end local v12           #index:I
    .local v13, index:I
    aget-byte v1, p1, v12

    #@5
    and-int/lit16 v14, v1, 0xff

    #@7
    .line 2480
    .local v14, msgType:I
    if-eqz v14, :cond_13

    #@9
    .line 2481
    const-string v1, "CDMA"

    #@b
    const-string v3, "Received a WAP SMS which is not WDP. Discard."

    #@d
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 2482
    const/4 v1, 0x1

    #@11
    move v12, v13

    #@12
    .line 2519
    .end local v13           #index:I
    .restart local v12       #index:I
    :goto_12
    return v1

    #@13
    .line 2484
    .end local v12           #index:I
    .restart local v13       #index:I
    :cond_13
    add-int/lit8 v12, v13, 0x1

    #@15
    .end local v13           #index:I
    .restart local v12       #index:I
    aget-byte v1, p1, v13

    #@17
    and-int/lit16 v6, v1, 0xff

    #@19
    .line 2485
    .local v6, totalSegments:I
    add-int/lit8 v13, v12, 0x1

    #@1b
    .end local v12           #index:I
    .restart local v13       #index:I
    aget-byte v1, p1, v12

    #@1d
    and-int/lit16 v5, v1, 0xff

    #@1f
    .line 2487
    .local v5, segment:I
    if-lt v5, v6, :cond_48

    #@21
    .line 2488
    const-string v1, "CDMA"

    #@23
    new-instance v3, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v4, "WDP bad segment #"

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    const-string v4, " expecting 0-"

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    add-int/lit8 v4, v6, -0x1

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 2489
    const/4 v1, 0x1

    #@46
    move v12, v13

    #@47
    .end local v13           #index:I
    .restart local v12       #index:I
    goto :goto_12

    #@48
    .line 2493
    .end local v12           #index:I
    .restart local v13       #index:I
    :cond_48
    const/4 v15, 0x0

    #@49
    .line 2494
    .local v15, sourcePort:I
    const/4 v9, 0x0

    #@4a
    .line 2495
    .local v9, destinationPort:I
    if-nez v5, :cond_f2

    #@4c
    .line 2497
    add-int/lit8 v12, v13, 0x1

    #@4e
    .end local v13           #index:I
    .restart local v12       #index:I
    aget-byte v1, p1, v13

    #@50
    and-int/lit16 v1, v1, 0xff

    #@52
    shl-int/lit8 v15, v1, 0x8

    #@54
    .line 2498
    add-int/lit8 v13, v12, 0x1

    #@56
    .end local v12           #index:I
    .restart local v13       #index:I
    aget-byte v1, p1, v12

    #@58
    and-int/lit16 v1, v1, 0xff

    #@5a
    or-int/2addr v15, v1

    #@5b
    .line 2499
    add-int/lit8 v12, v13, 0x1

    #@5d
    .end local v13           #index:I
    .restart local v12       #index:I
    aget-byte v1, p1, v13

    #@5f
    and-int/lit16 v1, v1, 0xff

    #@61
    shl-int/lit8 v9, v1, 0x8

    #@63
    .line 2500
    add-int/lit8 v13, v12, 0x1

    #@65
    .end local v12           #index:I
    .restart local v13       #index:I
    aget-byte v1, p1, v12

    #@67
    and-int/lit16 v1, v1, 0xff

    #@69
    or-int/2addr v9, v1

    #@6a
    .line 2503
    move-object/from16 v0, p0

    #@6c
    iget-boolean v1, v0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mCheckForDuplicatePortsInOmadmWapPush:Z

    #@6e
    if-eqz v1, :cond_f2

    #@70
    .line 2504
    move-object/from16 v0, p1

    #@72
    invoke-static {v0, v13}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->checkDuplicatePortOmadmWappush([BI)Z

    #@75
    move-result v1

    #@76
    if-eqz v1, :cond_f2

    #@78
    .line 2505
    add-int/lit8 v12, v13, 0x4

    #@7a
    .line 2511
    .end local v13           #index:I
    .restart local v12       #index:I
    :goto_7a
    const-string v1, "CDMA"

    #@7c
    new-instance v3, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v4, "Received WAP PDU. Type = "

    #@83
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v3

    #@8b
    const-string v4, ", originator = "

    #@8d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v3

    #@91
    move-object/from16 v0, p3

    #@93
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v3

    #@97
    const-string v4, ", src-port = "

    #@99
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v3

    #@9d
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v3

    #@a1
    const-string v4, ", dst-port = "

    #@a3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v3

    #@a7
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v3

    #@ab
    const-string v4, ", ID = "

    #@ad
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v3

    #@b1
    move/from16 v0, p2

    #@b3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v3

    #@b7
    const-string v4, ", segment# = "

    #@b9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v3

    #@bd
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v3

    #@c1
    const/16 v4, 0x2f

    #@c3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v3

    #@c7
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v3

    #@cb
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v3

    #@cf
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d2
    .line 2516
    move-object/from16 v0, p1

    #@d4
    array-length v1, v0

    #@d5
    sub-int/2addr v1, v12

    #@d6
    new-array v2, v1, [B

    #@d8
    .line 2517
    .local v2, userData:[B
    const/4 v1, 0x0

    #@d9
    move-object/from16 v0, p1

    #@db
    array-length v3, v0

    #@dc
    sub-int/2addr v3, v12

    #@dd
    move-object/from16 v0, p1

    #@df
    invoke-static {v0, v12, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@e2
    .line 2519
    const-wide/16 v7, 0x0

    #@e4
    const/4 v10, 0x1

    #@e5
    const/4 v11, 0x0

    #@e6
    move-object/from16 v1, p0

    #@e8
    move-object/from16 v3, p3

    #@ea
    move/from16 v4, p2

    #@ec
    invoke-virtual/range {v1 .. v11}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->processMessagePartKRTestBed([BLjava/lang/String;IIIJIZLjava/lang/String;)I

    #@ef
    move-result v1

    #@f0
    goto/16 :goto_12

    #@f2
    .end local v2           #userData:[B
    .end local v12           #index:I
    .restart local v13       #index:I
    :cond_f2
    move v12, v13

    #@f3
    .end local v13           #index:I
    .restart local v12       #index:I
    goto :goto_7a
.end method

.method public processLmsMessage(Lcom/android/internal/telephony/cdma/SmsMessage;[[B)I
    .registers 19
    .parameter "sms"
    .parameter "pdus"

    #@0
    .prologue
    .line 1907
    const-string v0, "processLmsMessage()"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@5
    .line 1909
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getPdu()[B

    #@8
    move-result-object v6

    #@9
    .line 1910
    .local v6, pdu:[B
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserData()[B

    #@c
    move-result-object v15

    #@d
    .line 1911
    .local v15, userData:[B
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    .line 1912
    .local v2, sourceMin:Ljava/lang/String;
    const/4 v0, 0x0

    #@12
    aget-byte v3, v15, v0

    #@14
    .line 1913
    .local v3, sessionId:I
    const/4 v0, 0x1

    #@15
    aget-byte v0, v15, v0

    #@17
    and-int/lit16 v0, v0, 0xf0

    #@19
    shr-int/lit8 v4, v0, 0x4

    #@1b
    .line 1914
    .local v4, totalSegment:I
    const/4 v0, 0x1

    #@1c
    aget-byte v0, v15, v0

    #@1e
    and-int/lit8 v5, v0, 0xf

    #@20
    .line 1916
    .local v5, currentSegment:I
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, "processLmsMessage(), sourceMin = "

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@36
    .line 1917
    new-instance v0, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v1, "processLmsMessage(), sessionId = "

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@4c
    .line 1918
    new-instance v0, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v1, "processLmsMessage(), totalSegment = "

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v0

    #@5f
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@62
    .line 1919
    new-instance v0, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v1, "processLmsMessage(), currentSegment = "

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v0

    #@6d
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v0

    #@71
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v0

    #@75
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@78
    .line 1920
    new-instance v0, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v1, "processLmsMessage(), tid = "

    #@7f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v0

    #@83
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleService()I

    #@86
    move-result v1

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v0

    #@8f
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@92
    .line 1923
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@95
    move-result-object v0

    #@96
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@98
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@9a
    const/4 v1, 0x3

    #@9b
    if-lt v0, v1, :cond_a3

    #@9d
    const/4 v0, 0x3

    #@9e
    if-gt v4, v0, :cond_a3

    #@a0
    const/4 v0, 0x3

    #@a1
    if-le v5, v0, :cond_aa

    #@a3
    .line 1928
    :cond_a3
    const-string v0, "processLmsMessage(), invalid LMS"

    #@a5
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@a8
    .line 1929
    const/4 v0, 0x1

    #@a9
    .line 1939
    :goto_a9
    return v0

    #@aa
    .line 1931
    :cond_aa
    const/4 v7, 0x1

    #@ab
    move-object/from16 v0, p0

    #@ad
    move-object/from16 v1, p1

    #@af
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->checkLmsDuplicated(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III[B)Z

    #@b2
    move-result v0

    #@b3
    if-ne v7, v0, :cond_bc

    #@b5
    .line 1932
    const-string v0, "processLmsMessage(), LMS ???"

    #@b7
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@ba
    .line 1933
    const/4 v0, 0x1

    #@bb
    goto :goto_a9

    #@bc
    :cond_bc
    move-object/from16 v7, p0

    #@be
    move-object/from16 v8, p1

    #@c0
    move-object v9, v2

    #@c1
    move v10, v3

    #@c2
    move v11, v5

    #@c3
    move v12, v4

    #@c4
    .line 1937
    invoke-virtual/range {v7 .. v12}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->deleteAllExpiredMsg(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III)V

    #@c7
    move-object/from16 v7, p0

    #@c9
    move-object/from16 v8, p1

    #@cb
    move-object v9, v2

    #@cc
    move v10, v3

    #@cd
    move v11, v5

    #@ce
    move v12, v4

    #@cf
    move-object v13, v6

    #@d0
    move-object/from16 v14, p2

    #@d2
    .line 1939
    invoke-virtual/range {v7 .. v14}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->checkCompleteMsg(Lcom/android/internal/telephony/cdma/SmsMessage;Ljava/lang/String;III[B[[B)I

    #@d5
    move-result v0

    #@d6
    goto :goto_a9
.end method

.method protected processOperatorMessage([[BLcom/android/internal/telephony/SmsMessageBase;ZZ)I
    .registers 6
    .parameter "pdus"
    .parameter "smsb"
    .parameter "bportAddrs"
    .parameter "bConcat"

    #@0
    .prologue
    .line 1473
    const/4 v0, 0x5

    #@1
    .line 1474
    .local v0, result:I
    return v0
.end method

.method protected sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 15
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "destPort"
    .parameter "data"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    .line 618
    if-eqz p6, :cond_1c

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    invoke-static {p2, p1, p3, p4, v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;I[BZ)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@6
    move-result-object v5

    #@7
    .local v5, pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    move-object v2, p2

    #@a
    move v3, p3

    #@b
    move-object v4, p4

    #@c
    .line 620
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;I[BLcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@f
    move-result-object v6

    #@10
    .line 621
    .local v6, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getFormat()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {p0, v6, p5, p6, v0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@17
    move-result-object v7

    #@18
    .line 623
    .local v7, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendSubmitPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@1b
    .line 624
    return-void

    #@1c
    .line 618
    .end local v5           #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .end local v6           #map:Ljava/util/HashMap;
    .end local v7           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_3
.end method

.method protected sendDomainNotiMessage(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;I)V
    .registers 25
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "domainStatus"

    #@0
    .prologue
    .line 2619
    const/4 v10, 0x0

    #@1
    .line 2620
    .local v10, msgType:I
    const/4 v9, 0x0

    #@2
    .line 2621
    .local v9, msgId:I
    const/16 v7, 0x8

    #@4
    .line 2622
    .local v7, length:I
    move/from16 v12, p5

    #@6
    .line 2623
    .local v12, status:I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9
    move-result-wide v15

    #@a
    const-wide/16 v17, 0x3e8

    #@c
    div-long v15, v15, v17

    #@e
    long-to-int v13, v15

    #@f
    .line 2624
    .local v13, timeStamp:I
    const/4 v4, 0x0

    #@10
    .line 2626
    .local v4, data:[B
    :try_start_10
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    #@12
    const/16 v15, 0x40

    #@14
    invoke-direct {v3, v15}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@17
    .line 2627
    .local v3, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v5, Ljava/io/DataOutputStream;

    #@19
    invoke-direct {v5, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@1c
    .line 2628
    .local v5, dos:Ljava/io/DataOutputStream;
    invoke-virtual {v5, v10}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@1f
    .line 2629
    invoke-virtual {v5, v9}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@22
    .line 2630
    invoke-virtual {v5, v7}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@25
    .line 2631
    invoke-virtual {v5, v12}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@28
    .line 2632
    invoke-virtual {v5, v13}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@2b
    .line 2633
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V

    #@2e
    .line 2634
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@31
    move-result-object v4

    #@32
    .line 2635
    new-instance v15, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v16, "sendDomainNotiMessage(), [KDDI][DAN] Message Type: "

    #@39
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v15

    #@3d
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v15

    #@41
    const-string v16, " Message ID: "

    #@43
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v15

    #@47
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v15

    #@4b
    const-string v16, " Message Length : "

    #@4d
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v15

    #@51
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v15

    #@55
    const-string v16, " Status : "

    #@57
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v15

    #@5b
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v15

    #@5f
    const-string v16, " Time Stamp : "

    #@61
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v15

    #@65
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v15

    #@69
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v15

    #@6d
    invoke-static {v15}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_70
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_70} :catch_d3

    #@70
    .line 2643
    .end local v3           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v5           #dos:Ljava/io/DataOutputStream;
    :goto_70
    new-instance v15, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v16, "sendDomainNotiMessage(), [KDDI][DAN] UserData(payload) : "

    #@77
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v15

    #@7b
    invoke-static {v4}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@7e
    move-result-object v16

    #@7f
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v15

    #@83
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v15

    #@87
    invoke-static {v15}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@8a
    .line 2644
    const/4 v15, 0x1

    #@8b
    const/16 v16, 0x0

    #@8d
    move-object/from16 v0, p2

    #@8f
    move-object/from16 v1, p1

    #@91
    move-object/from16 v2, v16

    #@93
    invoke-static {v0, v1, v4, v15, v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDomainNotiPdu(Ljava/lang/String;Ljava/lang/String;[BZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@96
    move-result-object v11

    #@97
    .line 2646
    .local v11, pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    new-instance v15, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v16, "sendDomainNotiMessage(), [KDDI][DAN] Submit pdu Data : "

    #@9e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v15

    #@a2
    invoke-virtual {v11}, Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;->toString()Ljava/lang/String;

    #@a5
    move-result-object v16

    #@a6
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v15

    #@aa
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v15

    #@ae
    invoke-static {v15}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@b1
    .line 2647
    invoke-static {v4}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@b4
    move-result-object v15

    #@b5
    move-object/from16 v0, p0

    #@b7
    move-object/from16 v1, p1

    #@b9
    move-object/from16 v2, p2

    #@bb
    invoke-virtual {v0, v1, v2, v15, v11}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@be
    move-result-object v8

    #@bf
    .line 2648
    .local v8, map:Ljava/util/HashMap;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getFormat()Ljava/lang/String;

    #@c2
    move-result-object v15

    #@c3
    move-object/from16 v0, p0

    #@c5
    move-object/from16 v1, p3

    #@c7
    move-object/from16 v2, p4

    #@c9
    invoke-virtual {v0, v8, v1, v2, v15}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@cc
    move-result-object v14

    #@cd
    .line 2650
    .local v14, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    move-object/from16 v0, p0

    #@cf
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendSubmitPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@d2
    .line 2651
    return-void

    #@d3
    .line 2640
    .end local v8           #map:Ljava/util/HashMap;
    .end local v11           #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .end local v14           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :catch_d3
    move-exception v6

    #@d4
    .line 2641
    .local v6, ex:Ljava/io/IOException;
    new-instance v15, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v16, "sendDomainNotiMessage(), [KDDI][DAN]Creating Notification Data failed: "

    #@db
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v15

    #@df
    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v15

    #@e3
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v15

    #@e7
    invoke-static {v15}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@ea
    goto :goto_70
.end method

.method protected sendEmailoverText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 11
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    .line 698
    if-eqz p5, :cond_10

    #@2
    const/4 v3, 0x1

    #@3
    :goto_3
    const/4 v4, 0x0

    #@4
    invoke-static {p2, p1, p3, v3, v4}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@7
    move-result-object v1

    #@8
    .line 700
    .local v1, pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    if-nez v1, :cond_12

    #@a
    .line 701
    const-string v3, "sendEmailoverText(), fail : pdu is null"

    #@c
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@f
    .line 708
    :goto_f
    return-void

    #@10
    .line 698
    .end local v1           #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_10
    const/4 v3, 0x0

    #@11
    goto :goto_3

    #@12
    .line 704
    .restart local v1       #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_12
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@15
    move-result-object v0

    #@16
    .line 705
    .local v0, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getFormat()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {p0, v0, p4, p5, v3}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@1d
    move-result-object v2

    #@1e
    .line 707
    .local v2, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendSubmitPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@21
    goto :goto_f
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V
    .registers 16
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 732
    new-instance v3, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@3
    invoke-direct {v3}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@6
    .line 733
    .local v3, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object p3, v3, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@8
    .line 734
    iput-object p4, v3, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@a
    .line 737
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@c
    const-string v6, "sprint_segment_sms"

    #@e
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@11
    move-result v5

    #@12
    if-eqz v5, :cond_3e

    #@14
    .line 738
    const/4 v5, 0x0

    #@15
    iput-object v5, v3, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@17
    .line 750
    :goto_17
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@19
    const-string v6, "cdma_priority_indicator"

    #@1b
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_26

    #@21
    .line 751
    sget v5, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitPriority:I

    #@23
    invoke-static {v5}, Lcom/android/internal/telephony/cdma/SmsMessage;->setSmsPriority(I)V

    #@26
    .line 759
    :cond_26
    if-eqz p7, :cond_4b

    #@28
    if-eqz p8, :cond_4b

    #@2a
    :goto_2a
    invoke-static {p1, v3, v4}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Lcom/android/internal/telephony/cdma/sms/UserData;Z)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@2d
    move-result-object v1

    #@2e
    .line 762
    .local v1, submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@31
    move-result-object v0

    #@32
    .line 764
    .local v0, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getFormat()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {p0, v0, p6, p7, v4}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@39
    move-result-object v2

    #@3a
    .line 766
    .local v2, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendSubmitPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@3d
    .line 767
    return-void

    #@3e
    .line 740
    .end local v0           #map:Ljava/util/HashMap;
    .end local v1           #submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .end local v2           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :cond_3e
    if-ne p5, v4, :cond_47

    #@40
    .line 741
    const/16 v5, 0x9

    #@42
    iput v5, v3, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@44
    .line 745
    :goto_44
    iput-boolean v4, v3, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@46
    goto :goto_17

    #@47
    .line 743
    :cond_47
    const/4 v5, 0x4

    #@48
    iput v5, v3, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@4a
    goto :goto_44

    #@4b
    .line 759
    :cond_4b
    const/4 v4, 0x0

    #@4c
    goto :goto_2a
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;)V
    .registers 21
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 816
    new-instance v10, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@2
    invoke-direct {v10}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@5
    .line 817
    .local v10, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object p3, v10, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@7
    .line 818
    iput-object p4, v10, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@9
    .line 821
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@b
    const-string v3, "sprint_segment_sms"

    #@d
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_4c

    #@13
    .line 822
    const/4 v2, 0x0

    #@14
    iput-object v2, v10, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@16
    .line 834
    :goto_16
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@18
    const-string v3, "cdma_priority_indicator"

    #@1a
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_25

    #@20
    .line 835
    sget v2, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitPriority:I

    #@22
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->setSmsPriority(I)V

    #@25
    .line 843
    :cond_25
    if-eqz p7, :cond_5d

    #@27
    if-eqz p8, :cond_5d

    #@29
    const/4 v2, 0x1

    #@2a
    :goto_2a
    move-object/from16 v0, p9

    #@2c
    invoke-static {p1, v10, v2, v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Lcom/android/internal/telephony/cdma/sms/UserData;ZLjava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@2f
    move-result-object v6

    #@30
    .line 846
    .local v6, submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    if-eqz v6, :cond_5f

    #@32
    move-object v2, p0

    #@33
    move-object v3, p1

    #@34
    move-object v4, p2

    #@35
    move-object v5, p3

    #@36
    move-object/from16 v7, p9

    #@38
    .line 847
    invoke-virtual/range {v2 .. v7}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;Ljava/lang/String;)Ljava/util/HashMap;

    #@3b
    move-result-object v8

    #@3c
    .line 849
    .local v8, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getFormat()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    move-object/from16 v0, p6

    #@42
    move-object/from16 v1, p7

    #@44
    invoke-virtual {p0, v8, v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@47
    move-result-object v9

    #@48
    .line 851
    .local v9, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendSubmitPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@4b
    .line 856
    .end local v8           #map:Ljava/util/HashMap;
    .end local v9           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_4b
    return-void

    #@4c
    .line 824
    .end local v6           #submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_4c
    const/4 v2, 0x1

    #@4d
    move/from16 v0, p5

    #@4f
    if-ne v0, v2, :cond_59

    #@51
    .line 825
    const/16 v2, 0x9

    #@53
    iput v2, v10, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@55
    .line 829
    :goto_55
    const/4 v2, 0x1

    #@56
    iput-boolean v2, v10, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@58
    goto :goto_16

    #@59
    .line 827
    :cond_59
    const/4 v2, 0x4

    #@5a
    iput v2, v10, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@5c
    goto :goto_55

    #@5d
    .line 843
    :cond_5d
    const/4 v2, 0x0

    #@5e
    goto :goto_2a

    #@5f
    .line 853
    .restart local v6       #submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_5f
    const-string v2, "sendNewSubmitPdu(), getSubmitPdu() returned null"

    #@61
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@64
    goto :goto_4b
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;Z)V
    .registers 23
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"
    .parameter "cbAddress"
    .parameter "isMultiPart"

    #@0
    .prologue
    .line 865
    new-instance v11, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@2
    invoke-direct {v11}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@5
    .line 866
    .local v11, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object p3, v11, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@7
    .line 867
    move-object/from16 v0, p4

    #@9
    iput-object v0, v11, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@b
    .line 868
    const/4 v2, 0x1

    #@c
    move/from16 v0, p5

    #@e
    if-ne v0, v2, :cond_4f

    #@10
    .line 869
    const/16 v2, 0x9

    #@12
    iput v2, v11, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@14
    .line 873
    :goto_14
    const/4 v2, 0x1

    #@15
    iput-boolean v2, v11, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@17
    .line 876
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@19
    const-string v3, "cdma_priority_indicator"

    #@1b
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_26

    #@21
    .line 877
    sget v2, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitPriority:I

    #@23
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->setSmsPriority(I)V

    #@26
    .line 885
    :cond_26
    if-eqz p7, :cond_53

    #@28
    if-eqz p8, :cond_53

    #@2a
    const/4 v2, 0x1

    #@2b
    :goto_2b
    move-object/from16 v0, p9

    #@2d
    invoke-static {p1, v11, v2, v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Lcom/android/internal/telephony/cdma/sms/UserData;ZLjava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@30
    move-result-object v6

    #@31
    .line 888
    .local v6, submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    if-eqz v6, :cond_55

    #@33
    move-object v2, p0

    #@34
    move-object v3, p1

    #@35
    move-object v4, p2

    #@36
    move-object v5, p3

    #@37
    move-object/from16 v7, p9

    #@39
    move/from16 v8, p10

    #@3b
    .line 889
    invoke-virtual/range {v2 .. v8}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;Ljava/lang/String;Z)Ljava/util/HashMap;

    #@3e
    move-result-object v9

    #@3f
    .line 890
    .local v9, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getFormat()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    move-object/from16 v0, p6

    #@45
    move-object/from16 v1, p7

    #@47
    invoke-virtual {p0, v9, v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@4a
    move-result-object v10

    #@4b
    .line 891
    .local v10, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendSubmitPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@4e
    .line 896
    .end local v9           #map:Ljava/util/HashMap;
    .end local v10           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_4e
    return-void

    #@4f
    .line 871
    .end local v6           #submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_4f
    const/4 v2, 0x4

    #@50
    iput v2, v11, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@52
    goto :goto_14

    #@53
    .line 885
    :cond_53
    const/4 v2, 0x0

    #@54
    goto :goto_2b

    #@55
    .line 893
    .restart local v6       #submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_55
    const-string v2, "sendNewSubmitPdu(), getSubmitPdu() returned null"

    #@57
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5a
    goto :goto_4e
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZZ)V
    .registers 21
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"
    .parameter "isMultiPart"

    #@0
    .prologue
    .line 775
    const-string v2, "sendNewSubmitPdu()"

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 776
    new-instance v10, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@7
    invoke-direct {v10}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@a
    .line 777
    .local v10, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object p3, v10, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@c
    .line 778
    iput-object p4, v10, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@e
    .line 779
    const/4 v2, 0x1

    #@f
    move/from16 v0, p5

    #@11
    if-ne v0, v2, :cond_4e

    #@13
    .line 780
    const/16 v2, 0x9

    #@15
    iput v2, v10, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@17
    .line 784
    :goto_17
    const/4 v2, 0x1

    #@18
    iput-boolean v2, v10, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@1a
    .line 787
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@1c
    const-string v3, "cdma_priority_indicator"

    #@1e
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_29

    #@24
    .line 788
    sget v2, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitPriority:I

    #@26
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->setSmsPriority(I)V

    #@29
    .line 796
    :cond_29
    if-eqz p7, :cond_52

    #@2b
    if-eqz p8, :cond_52

    #@2d
    const/4 v2, 0x1

    #@2e
    :goto_2e
    invoke-static {p1, v10, v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Lcom/android/internal/telephony/cdma/sms/UserData;Z)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@31
    move-result-object v6

    #@32
    .line 799
    .local v6, submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    if-eqz v6, :cond_54

    #@34
    move-object v2, p0

    #@35
    move-object v3, p1

    #@36
    move-object v4, p2

    #@37
    move-object v5, p3

    #@38
    move/from16 v7, p9

    #@3a
    .line 800
    invoke-virtual/range {v2 .. v7}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;Z)Ljava/util/HashMap;

    #@3d
    move-result-object v8

    #@3e
    .line 801
    .local v8, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getFormat()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    move-object/from16 v0, p6

    #@44
    move-object/from16 v1, p7

    #@46
    invoke-virtual {p0, v8, v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@49
    move-result-object v9

    #@4a
    .line 802
    .local v9, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendSubmitPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@4d
    .line 807
    .end local v8           #map:Ljava/util/HashMap;
    .end local v9           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_4d
    return-void

    #@4e
    .line 782
    .end local v6           #submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_4e
    const/4 v2, 0x4

    #@4f
    iput v2, v10, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@51
    goto :goto_17

    #@52
    .line 796
    :cond_52
    const/4 v2, 0x0

    #@53
    goto :goto_2e

    #@54
    .line 804
    .restart local v6       #submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_54
    const-string v2, "sendNewSubmitPdu(), getSubmitPdu() returned null"

    #@56
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@59
    goto :goto_4d
.end method

.method protected sendNewSubmitPduforEmailoverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V
    .registers 15
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 905
    new-instance v3, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@3
    invoke-direct {v3}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@6
    .line 906
    .local v3, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object p3, v3, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@8
    .line 907
    iput-object p4, v3, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@a
    .line 908
    if-ne p5, v4, :cond_2c

    #@c
    .line 909
    const/16 v5, 0x9

    #@e
    iput v5, v3, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@10
    .line 913
    :goto_10
    iput-boolean v4, v3, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@12
    .line 919
    if-eqz p7, :cond_30

    #@14
    if-eqz p8, :cond_30

    #@16
    :goto_16
    invoke-static {p1, v3, v4}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Lcom/android/internal/telephony/cdma/sms/UserData;Z)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@19
    move-result-object v1

    #@1a
    .line 922
    .local v1, submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    if-eqz v1, :cond_32

    #@1c
    .line 923
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@1f
    move-result-object v0

    #@20
    .line 925
    .local v0, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getFormat()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {p0, v0, p6, p7, v4}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@27
    move-result-object v2

    #@28
    .line 927
    .local v2, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendSubmitPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@2b
    .line 932
    .end local v0           #map:Ljava/util/HashMap;
    .end local v2           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_2b
    return-void

    #@2c
    .line 911
    .end local v1           #submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_2c
    const/4 v5, 0x4

    #@2d
    iput v5, v3, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@2f
    goto :goto_10

    #@30
    .line 919
    :cond_30
    const/4 v4, 0x0

    #@31
    goto :goto_16

    #@32
    .line 929
    .restart local v1       #submitPdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_32
    const-string v4, "sendNewSubmitPduforEmailoverSms(), getSubmitPdu() returned null"

    #@34
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@37
    goto :goto_2b
.end method

.method public sendRetrySms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 3
    .parameter "tracker"

    #@0
    .prologue
    .line 1022
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->mImsSMSDispatcher:Lcom/android/internal/telephony/ImsSMSDispatcher;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/ImsSMSDispatcher;->sendRetrySms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@5
    .line 1023
    return-void
.end method

.method protected sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 8
    .parameter "tracker"

    #@0
    .prologue
    .line 975
    iget-object v0, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mData:Ljava/util/HashMap;

    #@2
    .line 978
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v3, "pdu"

    #@4
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v3

    #@8
    check-cast v3, [B

    #@a
    move-object v1, v3

    #@b
    check-cast v1, [B

    #@d
    .line 980
    .local v1, pdu:[B
    const/4 v3, 0x2

    #@e
    invoke-virtual {p0, v3, p1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v2

    #@12
    .line 982
    .local v2, reply:Landroid/os/Message;
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "sendSms(),  isIms()="

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isIms()Z

    #@20
    move-result v4

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, " mRetryCount="

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    iget v4, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, " mImsRetry="

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    iget v4, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, " mMessageRef="

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    iget v4, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mMessageRef:I

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    const-string v4, " SS="

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    iget-object v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@51
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@54
    move-result-object v4

    #@55
    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getState()I

    #@58
    move-result v4

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@64
    .line 990
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@66
    const-string v4, "sms_over_lgims"

    #@68
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6b
    move-result v3

    #@6c
    if-eqz v3, :cond_9a

    #@6e
    .line 991
    const-string v3, "sendSms(), Send SMS OVER LG IMS Routine"

    #@70
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@73
    .line 992
    const/4 v3, 0x1

    #@74
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isSmsOverImsMo()Z

    #@77
    move-result v4

    #@78
    if-ne v3, v4, :cond_8f

    #@7a
    .line 993
    const-string v3, "sendSms(), Send SMS using SMS over LG IMS"

    #@7c
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7f
    .line 994
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@81
    iget v4, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@83
    iget v5, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mMessageRef:I

    #@85
    invoke-interface {v3, v1, v4, v5, v2}, Lcom/android/internal/telephony/CommandsInterface;->sendImsCdmaSms([BIILandroid/os/Message;)V

    #@88
    .line 997
    iget v3, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@8a
    add-int/lit8 v3, v3, 0x1

    #@8c
    iput v3, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@8e
    .line 1017
    :goto_8e
    return-void

    #@8f
    .line 999
    :cond_8f
    const-string v3, "sendSms(), Send SMS using 1x"

    #@91
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@94
    .line 1000
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@96
    invoke-interface {v3, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->sendCdmaSms([BLandroid/os/Message;)V

    #@99
    goto :goto_8e

    #@9a
    .line 1007
    :cond_9a
    iget v3, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@9c
    if-nez v3, :cond_aa

    #@9e
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->isIms()Z

    #@a1
    move-result v3

    #@a2
    if-nez v3, :cond_aa

    #@a4
    .line 1008
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@a6
    invoke-interface {v3, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->sendCdmaSms([BLandroid/os/Message;)V

    #@a9
    goto :goto_8e

    #@aa
    .line 1010
    :cond_aa
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@ac
    iget v4, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@ae
    iget v5, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mMessageRef:I

    #@b0
    invoke-interface {v3, v1, v4, v5, v2}, Lcom/android/internal/telephony/CommandsInterface;->sendImsCdmaSms([BIILandroid/os/Message;)V

    #@b3
    .line 1013
    iget v3, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@b5
    add-int/lit8 v3, v3, 0x1

    #@b7
    iput v3, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@b9
    goto :goto_8e
.end method

.method protected sendSubmitPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 4
    .parameter "tracker"

    #@0
    .prologue
    .line 937
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "support_sprint_sms_roaming_guard"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_d

    #@a
    .line 938
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    #@d
    .line 942
    :cond_d
    const-string v0, "ril.cdma.inecmmode"

    #@f
    const/4 v1, 0x0

    #@10
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_21

    #@16
    .line 943
    iget-object v0, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@18
    if-eqz v0, :cond_20

    #@1a
    .line 945
    :try_start_1a
    iget-object v0, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@1c
    const/4 v1, 0x4

    #@1d
    invoke-virtual {v0, v1}, Landroid/app/PendingIntent;->send(I)V
    :try_end_20
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1a .. :try_end_20} :catch_47

    #@20
    .line 970
    :cond_20
    :goto_20
    return-void

    #@21
    .line 955
    :cond_21
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@23
    const-string v1, "support_sprint_sms_roaming_guard"

    #@25
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_41

    #@2b
    .line 956
    invoke-static {}, Lcom/android/internal/telephony/SMSDispatcher;->getSmsIsRoaming()Z

    #@2e
    move-result v0

    #@2f
    if-eqz v0, :cond_41

    #@31
    .line 958
    iget-object v0, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@33
    if-eqz v0, :cond_3b

    #@35
    .line 960
    :try_start_35
    iget-object v0, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mSentIntent:Landroid/app/PendingIntent;

    #@37
    const/4 v1, 0x7

    #@38
    invoke-virtual {v0, v1}, Landroid/app/PendingIntent;->send(I)V
    :try_end_3b
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_35 .. :try_end_3b} :catch_45

    #@3b
    .line 963
    :cond_3b
    :goto_3b
    const-string v0, "sendSubmitPdu(), Return, This is ROAMING Message."

    #@3d
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@40
    goto :goto_20

    #@41
    .line 969
    :cond_41
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@44
    goto :goto_20

    #@45
    .line 961
    :catch_45
    move-exception v0

    #@46
    goto :goto_3b

    #@47
    .line 946
    :catch_47
    move-exception v0

    #@48
    goto :goto_20
.end method

.method protected sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 13
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 631
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@4
    const-string v5, "cdma_priority_indicator"

    #@6
    invoke-static {v3, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_11

    #@c
    .line 632
    sget v3, Lcom/android/internal/telephony/SMSDispatcher;->mSubmitPriority:I

    #@e
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/SmsMessage;->setSmsPriority(I)V

    #@11
    .line 637
    :cond_11
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@13
    const-string v5, "KSC5601EncodingUSCDMA"

    #@15
    invoke-static {v3, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_1e

    #@1b
    .line 638
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->setPreConditionForQeTest()V

    #@1e
    .line 643
    :cond_1e
    if-eqz p5, :cond_34

    #@20
    move v3, v4

    #@21
    :goto_21
    invoke-static {p2, p1, p3, v3, v6}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@24
    move-result-object v1

    #@25
    .line 646
    .local v1, pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    const-string v3, "SendIntentFailure"

    #@27
    invoke-static {v6, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@2a
    move-result v3

    #@2b
    if-ne v3, v4, :cond_36

    #@2d
    .line 647
    if-nez v1, :cond_36

    #@2f
    .line 648
    const/4 v3, 0x3

    #@30
    invoke-static {p4, v3}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Landroid/app/PendingIntent;I)V

    #@33
    .line 657
    :goto_33
    return-void

    #@34
    .line 643
    .end local v1           #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_34
    const/4 v3, 0x0

    #@35
    goto :goto_21

    #@36
    .line 653
    .restart local v1       #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :cond_36
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@39
    move-result-object v0

    #@3a
    .line 654
    .local v0, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->getFormat()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {p0, v0, p4, p5, v3}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@41
    move-result-object v2

    #@42
    .line 656
    .local v2, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendSubmitPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@45
    goto :goto_33
.end method

.method updateMessageWaitingIndicator(I)V
    .registers 3
    .parameter "mwi"

    #@0
    .prologue
    .line 544
    if-gez p1, :cond_c

    #@2
    .line 545
    const/4 p1, -0x1

    #@3
    .line 552
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/PhoneBase;->setVoiceMessageCount(I)V

    #@8
    .line 554
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->storeVoiceMailCount()V

    #@b
    .line 555
    return-void

    #@c
    .line 546
    :cond_c
    const/16 v0, 0x63

    #@e
    if-le p1, v0, :cond_3

    #@10
    .line 549
    const/16 p1, 0x63

    #@12
    goto :goto_3
.end method

.method updateMessageWaitingIndicatorPriority(Z)V
    .registers 6
    .parameter "urgent"

    #@0
    .prologue
    .line 1335
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "updateMessageWaitingIndicatorPriority()"

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 1337
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@18
    invoke-virtual {v2, p1}, Lcom/android/internal/telephony/PhoneBase;->setVoiceMessageUrgent(Z)V

    #@1b
    .line 1340
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@1d
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@20
    move-result-object v1

    #@21
    .line 1341
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@24
    move-result-object v0

    #@25
    .line 1342
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@27
    const-string v2, "vm_priority_key"

    #@29
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    #@2c
    .line 1343
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@2f
    .line 1344
    return-void
.end method
