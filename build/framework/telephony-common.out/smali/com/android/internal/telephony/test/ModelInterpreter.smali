.class public Lcom/android/internal/telephony/test/ModelInterpreter;
.super Ljava/lang/Object;
.source "ModelInterpreter.java"

# interfaces
.implements Ljava/lang/Runnable;
.implements Lcom/android/internal/telephony/test/SimulatedRadioControl;


# static fields
.field static final CONNECTING_PAUSE_MSEC:I = 0x1f4

.field static final LOG_TAG:Ljava/lang/String; = "ModelInterpreter"

.field static final MAX_CALLS:I = 0x6

.field static final PROGRESS_CALL_STATE:I = 0x1

.field static final sDefaultResponses:[[Ljava/lang/String;


# instance fields
.field private finalResponse:Ljava/lang/String;

.field in:Ljava/io/InputStream;

.field lineReader:Lcom/android/internal/telephony/test/LineReader;

.field mHandlerThread:Landroid/os/HandlerThread;

.field out:Ljava/io/OutputStream;

.field pausedResponseCount:I

.field pausedResponseMonitor:Ljava/lang/Object;

.field simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

.field ss:Ljava/net/ServerSocket;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 702
    const/16 v0, 0x1f

    #@6
    new-array v0, v0, [[Ljava/lang/String;

    #@8
    new-array v1, v6, [Ljava/lang/String;

    #@a
    const-string v2, "E0Q0V1"

    #@c
    aput-object v2, v1, v4

    #@e
    aput-object v7, v1, v5

    #@10
    aput-object v1, v0, v4

    #@12
    new-array v1, v6, [Ljava/lang/String;

    #@14
    const-string v2, "+CMEE=2"

    #@16
    aput-object v2, v1, v4

    #@18
    aput-object v7, v1, v5

    #@1a
    aput-object v1, v0, v5

    #@1c
    new-array v1, v6, [Ljava/lang/String;

    #@1e
    const-string v2, "+CREG=2"

    #@20
    aput-object v2, v1, v4

    #@22
    aput-object v7, v1, v5

    #@24
    aput-object v1, v0, v6

    #@26
    const/4 v1, 0x3

    #@27
    new-array v2, v6, [Ljava/lang/String;

    #@29
    const-string v3, "+CGREG=2"

    #@2b
    aput-object v3, v2, v4

    #@2d
    aput-object v7, v2, v5

    #@2f
    aput-object v2, v0, v1

    #@31
    const/4 v1, 0x4

    #@32
    new-array v2, v6, [Ljava/lang/String;

    #@34
    const-string v3, "+CCWA=1"

    #@36
    aput-object v3, v2, v4

    #@38
    aput-object v7, v2, v5

    #@3a
    aput-object v2, v0, v1

    #@3c
    const/4 v1, 0x5

    #@3d
    new-array v2, v6, [Ljava/lang/String;

    #@3f
    const-string v3, "+COPS=0"

    #@41
    aput-object v3, v2, v4

    #@43
    aput-object v7, v2, v5

    #@45
    aput-object v2, v0, v1

    #@47
    const/4 v1, 0x6

    #@48
    new-array v2, v6, [Ljava/lang/String;

    #@4a
    const-string v3, "+CFUN=1"

    #@4c
    aput-object v3, v2, v4

    #@4e
    aput-object v7, v2, v5

    #@50
    aput-object v2, v0, v1

    #@52
    const/4 v1, 0x7

    #@53
    new-array v2, v6, [Ljava/lang/String;

    #@55
    const-string v3, "+CGMI"

    #@57
    aput-object v3, v2, v4

    #@59
    const-string v3, "+CGMI: Android Model AT Interpreter\r"

    #@5b
    aput-object v3, v2, v5

    #@5d
    aput-object v2, v0, v1

    #@5f
    const/16 v1, 0x8

    #@61
    new-array v2, v6, [Ljava/lang/String;

    #@63
    const-string v3, "+CGMM"

    #@65
    aput-object v3, v2, v4

    #@67
    const-string v3, "+CGMM: Android Model AT Interpreter\r"

    #@69
    aput-object v3, v2, v5

    #@6b
    aput-object v2, v0, v1

    #@6d
    const/16 v1, 0x9

    #@6f
    new-array v2, v6, [Ljava/lang/String;

    #@71
    const-string v3, "+CGMR"

    #@73
    aput-object v3, v2, v4

    #@75
    const-string v3, "+CGMR: 1.0\r"

    #@77
    aput-object v3, v2, v5

    #@79
    aput-object v2, v0, v1

    #@7b
    const/16 v1, 0xa

    #@7d
    new-array v2, v6, [Ljava/lang/String;

    #@7f
    const-string v3, "+CGSN"

    #@81
    aput-object v3, v2, v4

    #@83
    const-string v3, "000000000000000\r"

    #@85
    aput-object v3, v2, v5

    #@87
    aput-object v2, v0, v1

    #@89
    const/16 v1, 0xb

    #@8b
    new-array v2, v6, [Ljava/lang/String;

    #@8d
    const-string v3, "+CIMI"

    #@8f
    aput-object v3, v2, v4

    #@91
    const-string v3, "320720000000000\r"

    #@93
    aput-object v3, v2, v5

    #@95
    aput-object v2, v0, v1

    #@97
    const/16 v1, 0xc

    #@99
    new-array v2, v6, [Ljava/lang/String;

    #@9b
    const-string v3, "+CSCS=?"

    #@9d
    aput-object v3, v2, v4

    #@9f
    const-string v3, "+CSCS: (\"HEX\",\"UCS2\")\r"

    #@a1
    aput-object v3, v2, v5

    #@a3
    aput-object v2, v0, v1

    #@a5
    const/16 v1, 0xd

    #@a7
    new-array v2, v6, [Ljava/lang/String;

    #@a9
    const-string v3, "+CFUN?"

    #@ab
    aput-object v3, v2, v4

    #@ad
    const-string v3, "+CFUN: 1\r"

    #@af
    aput-object v3, v2, v5

    #@b1
    aput-object v2, v0, v1

    #@b3
    const/16 v1, 0xe

    #@b5
    new-array v2, v6, [Ljava/lang/String;

    #@b7
    const-string v3, "+COPS=3,0;+COPS?;+COPS=3,1;+COPS?;+COPS=3,2;+COPS?"

    #@b9
    aput-object v3, v2, v4

    #@bb
    const-string v3, "+COPS: 0,0,\"Android\"\r+COPS: 0,1,\"Android\"\r+COPS: 0,2,\"310995\"\r"

    #@bd
    aput-object v3, v2, v5

    #@bf
    aput-object v2, v0, v1

    #@c1
    const/16 v1, 0xf

    #@c3
    new-array v2, v6, [Ljava/lang/String;

    #@c5
    const-string v3, "+CREG?"

    #@c7
    aput-object v3, v2, v4

    #@c9
    const-string v3, "+CREG: 2,5, \"0113\", \"6614\"\r"

    #@cb
    aput-object v3, v2, v5

    #@cd
    aput-object v2, v0, v1

    #@cf
    const/16 v1, 0x10

    #@d1
    new-array v2, v6, [Ljava/lang/String;

    #@d3
    const-string v3, "+CGREG?"

    #@d5
    aput-object v3, v2, v4

    #@d7
    const-string v3, "+CGREG: 2,0\r"

    #@d9
    aput-object v3, v2, v5

    #@db
    aput-object v2, v0, v1

    #@dd
    const/16 v1, 0x11

    #@df
    new-array v2, v6, [Ljava/lang/String;

    #@e1
    const-string v3, "+CSQ"

    #@e3
    aput-object v3, v2, v4

    #@e5
    const-string v3, "+CSQ: 16,99\r"

    #@e7
    aput-object v3, v2, v5

    #@e9
    aput-object v2, v0, v1

    #@eb
    const/16 v1, 0x12

    #@ed
    new-array v2, v6, [Ljava/lang/String;

    #@ef
    const-string v3, "+CNMI?"

    #@f1
    aput-object v3, v2, v4

    #@f3
    const-string v3, "+CNMI: 1,2,2,1,1\r"

    #@f5
    aput-object v3, v2, v5

    #@f7
    aput-object v2, v0, v1

    #@f9
    const/16 v1, 0x13

    #@fb
    new-array v2, v6, [Ljava/lang/String;

    #@fd
    const-string v3, "+CLIR?"

    #@ff
    aput-object v3, v2, v4

    #@101
    const-string v3, "+CLIR: 1,3\r"

    #@103
    aput-object v3, v2, v5

    #@105
    aput-object v2, v0, v1

    #@107
    const/16 v1, 0x14

    #@109
    new-array v2, v6, [Ljava/lang/String;

    #@10b
    const-string v3, "%CPVWI=2"

    #@10d
    aput-object v3, v2, v4

    #@10f
    const-string v3, "%CPVWI: 0\r"

    #@111
    aput-object v3, v2, v5

    #@113
    aput-object v2, v0, v1

    #@115
    const/16 v1, 0x15

    #@117
    new-array v2, v6, [Ljava/lang/String;

    #@119
    const-string v3, "+CUSD=1,\"#646#\""

    #@11b
    aput-object v3, v2, v4

    #@11d
    const-string v3, "+CUSD=0,\"You have used 23 minutes\"\r"

    #@11f
    aput-object v3, v2, v5

    #@121
    aput-object v2, v0, v1

    #@123
    const/16 v1, 0x16

    #@125
    new-array v2, v6, [Ljava/lang/String;

    #@127
    const-string v3, "+CRSM=176,12258,0,0,10"

    #@129
    aput-object v3, v2, v4

    #@12b
    const-string v3, "+CRSM: 144,0,981062200050259429F6\r"

    #@12d
    aput-object v3, v2, v5

    #@12f
    aput-object v2, v0, v1

    #@131
    const/16 v1, 0x17

    #@133
    new-array v2, v6, [Ljava/lang/String;

    #@135
    const-string v3, "+CRSM=192,12258,0,0,15"

    #@137
    aput-object v3, v2, v4

    #@139
    const-string v3, "+CRSM: 144,0,0000000A2FE204000FF55501020000\r"

    #@13b
    aput-object v3, v2, v5

    #@13d
    aput-object v2, v0, v1

    #@13f
    const/16 v1, 0x18

    #@141
    new-array v2, v6, [Ljava/lang/String;

    #@143
    const-string v3, "+CRSM=192,28474,0,0,15"

    #@145
    aput-object v3, v2, v4

    #@147
    const-string v3, "+CRSM: 144,0,0000005a6f3a040011f5220102011e\r"

    #@149
    aput-object v3, v2, v5

    #@14b
    aput-object v2, v0, v1

    #@14d
    const/16 v1, 0x19

    #@14f
    new-array v2, v6, [Ljava/lang/String;

    #@151
    const-string v3, "+CRSM=178,28474,1,4,30"

    #@153
    aput-object v3, v2, v4

    #@155
    const-string v3, "+CRSM: 144,0,437573746f6d65722043617265ffffff07818100398799f7ffffffffffff\r"

    #@157
    aput-object v3, v2, v5

    #@159
    aput-object v2, v0, v1

    #@15b
    const/16 v1, 0x1a

    #@15d
    new-array v2, v6, [Ljava/lang/String;

    #@15f
    const-string v3, "+CRSM=178,28474,2,4,30"

    #@161
    aput-object v3, v2, v4

    #@163
    const-string v3, "+CRSM: 144,0,566f696365204d61696cffffffffffff07918150367742f3ffffffffffff\r"

    #@165
    aput-object v3, v2, v5

    #@167
    aput-object v2, v0, v1

    #@169
    const/16 v1, 0x1b

    #@16b
    new-array v2, v6, [Ljava/lang/String;

    #@16d
    const-string v3, "+CRSM=178,28474,3,4,30"

    #@16f
    aput-object v3, v2, v4

    #@171
    const-string v3, "+CRSM: 144,0,4164676a6dffffffffffffffffffffff0b918188551512c221436587ff01\r"

    #@173
    aput-object v3, v2, v5

    #@175
    aput-object v2, v0, v1

    #@177
    const/16 v1, 0x1c

    #@179
    new-array v2, v6, [Ljava/lang/String;

    #@17b
    const-string v3, "+CRSM=178,28474,4,4,30"

    #@17d
    aput-object v3, v2, v4

    #@17f
    const-string v3, "+CRSM: 144,0,810101c1ffffffffffffffffffffffff068114455245f8ffffffffffffff\r"

    #@181
    aput-object v3, v2, v5

    #@183
    aput-object v2, v0, v1

    #@185
    const/16 v1, 0x1d

    #@187
    new-array v2, v6, [Ljava/lang/String;

    #@189
    const-string v3, "+CRSM=192,28490,0,0,15"

    #@18b
    aput-object v3, v2, v4

    #@18d
    const-string v3, "+CRSM: 144,0,000000416f4a040011f5550102010d\r"

    #@18f
    aput-object v3, v2, v5

    #@191
    aput-object v2, v0, v1

    #@193
    const/16 v1, 0x1e

    #@195
    new-array v2, v6, [Ljava/lang/String;

    #@197
    const-string v3, "+CRSM=178,28490,1,4,13"

    #@199
    aput-object v3, v2, v4

    #@19b
    const-string v3, "+CRSM: 144,0,0206092143658709ffffffffff\r"

    #@19d
    aput-object v3, v2, v5

    #@19f
    aput-object v2, v0, v1

    #@1a1
    sput-object v0, Lcom/android/internal/telephony/test/ModelInterpreter;->sDefaultResponses:[[Ljava/lang/String;

    #@1a3
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .registers 4
    .parameter "in"
    .parameter "out"

    #@0
    .prologue
    .line 168
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 158
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseMonitor:Ljava/lang/Object;

    #@a
    .line 169
    iput-object p1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->in:Ljava/io/InputStream;

    #@c
    .line 170
    iput-object p2, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->out:Ljava/io/OutputStream;

    #@e
    .line 172
    invoke-direct {p0}, Lcom/android/internal/telephony/test/ModelInterpreter;->init()V

    #@11
    .line 173
    return-void
.end method

.method public constructor <init>(Ljava/net/InetSocketAddress;)V
    .registers 4
    .parameter "sa"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 177
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 158
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseMonitor:Ljava/lang/Object;

    #@a
    .line 178
    new-instance v0, Ljava/net/ServerSocket;

    #@c
    invoke-direct {v0}, Ljava/net/ServerSocket;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->ss:Ljava/net/ServerSocket;

    #@11
    .line 180
    iget-object v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->ss:Ljava/net/ServerSocket;

    #@13
    const/4 v1, 0x1

    #@14
    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->setReuseAddress(Z)V

    #@17
    .line 181
    iget-object v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->ss:Ljava/net/ServerSocket;

    #@19
    invoke-virtual {v0, p1}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    #@1c
    .line 183
    invoke-direct {p0}, Lcom/android/internal/telephony/test/ModelInterpreter;->init()V

    #@1f
    .line 184
    return-void
.end method

.method private conference()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 528
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->conference()Z

    #@5
    move-result v0

    #@6
    .line 530
    .local v0, success:Z
    if-nez v0, :cond_10

    #@8
    .line 531
    new-instance v1, Lcom/android/internal/telephony/test/InterpreterEx;

    #@a
    const-string v2, "ERROR"

    #@c
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/test/InterpreterEx;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 533
    :cond_10
    return-void
.end method

.method private init()V
    .registers 4

    #@0
    .prologue
    .line 189
    new-instance v1, Ljava/lang/Thread;

    #@2
    const-string v2, "ModelInterpreter"

    #@4
    invoke-direct {v1, p0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@7
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    #@a
    .line 190
    new-instance v1, Landroid/os/HandlerThread;

    #@c
    const-string v2, "ModelInterpreter"

    #@e
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@11
    iput-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->mHandlerThread:Landroid/os/HandlerThread;

    #@13
    .line 191
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->mHandlerThread:Landroid/os/HandlerThread;

    #@15
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@18
    .line 192
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->mHandlerThread:Landroid/os/HandlerThread;

    #@1a
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@1d
    move-result-object v0

    #@1e
    .line 193
    .local v0, looper:Landroid/os/Looper;
    new-instance v1, Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@20
    invoke-direct {v1, v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;-><init>(Landroid/os/Looper;)V

    #@23
    iput-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@25
    .line 194
    return-void
.end method

.method private onAnswer()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 433
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onAnswer()Z

    #@5
    move-result v0

    #@6
    .line 435
    .local v0, success:Z
    if-nez v0, :cond_10

    #@8
    .line 436
    new-instance v1, Lcom/android/internal/telephony/test/InterpreterEx;

    #@a
    const-string v2, "ERROR"

    #@c
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/test/InterpreterEx;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 438
    :cond_10
    return-void
.end method

.method private onCHLD(Ljava/lang/String;)V
    .registers 7
    .parameter "command"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 459
    const/4 v1, 0x0

    #@1
    .line 462
    .local v1, c1:C
    const/4 v3, 0x6

    #@2
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@5
    move-result v0

    #@6
    .line 464
    .local v0, c0:C
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@9
    move-result v3

    #@a
    const/16 v4, 0x8

    #@c
    if-lt v3, v4, :cond_13

    #@e
    .line 465
    const/4 v3, 0x7

    #@f
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@12
    move-result v1

    #@13
    .line 468
    :cond_13
    iget-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@15
    invoke-virtual {v3, v0, v1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onChld(CC)Z

    #@18
    move-result v2

    #@19
    .line 470
    .local v2, success:Z
    if-nez v2, :cond_23

    #@1b
    .line 471
    new-instance v3, Lcom/android/internal/telephony/test/InterpreterEx;

    #@1d
    const-string v4, "ERROR"

    #@1f
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/test/InterpreterEx;-><init>(Ljava/lang/String;)V

    #@22
    throw v3

    #@23
    .line 473
    :cond_23
    return-void
.end method

.method private onCLCC()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 552
    iget-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v3}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->getClccLines()Ljava/util/List;

    #@5
    move-result-object v1

    #@6
    .line 554
    .local v1, lines:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@a
    move-result v2

    #@b
    .local v2, s:I
    :goto_b
    if-ge v0, v2, :cond_19

    #@d
    .line 555
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Ljava/lang/String;

    #@13
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V

    #@16
    .line 554
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_b

    #@19
    .line 557
    :cond_19
    return-void
.end method

.method private onDial(Ljava/lang/String;)V
    .registers 5
    .parameter "command"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 540
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    const/4 v2, 0x1

    #@3
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onDial(Ljava/lang/String;)Z

    #@a
    move-result v0

    #@b
    .line 542
    .local v0, success:Z
    if-nez v0, :cond_15

    #@d
    .line 543
    new-instance v1, Lcom/android/internal/telephony/test/InterpreterEx;

    #@f
    const-string v2, "ERROR"

    #@11
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/test/InterpreterEx;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 545
    :cond_15
    return-void
.end method

.method private onHangup()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 443
    const/4 v0, 0x0

    #@1
    .line 445
    .local v0, success:Z
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@3
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->onAnswer()Z

    #@6
    move-result v0

    #@7
    .line 447
    if-nez v0, :cond_11

    #@9
    .line 448
    new-instance v1, Lcom/android/internal/telephony/test/InterpreterEx;

    #@b
    const-string v2, "ERROR"

    #@d
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/test/InterpreterEx;-><init>(Ljava/lang/String;)V

    #@10
    throw v1

    #@11
    .line 451
    :cond_11
    const-string v1, "NO CARRIER"

    #@13
    iput-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->finalResponse:Ljava/lang/String;

    #@15
    .line 452
    return-void
.end method

.method private onSMSSend(Ljava/lang/String;)V
    .registers 4
    .parameter "command"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 564
    const-string v1, "> "

    #@2
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/test/ModelInterpreter;->print(Ljava/lang/String;)V

    #@5
    .line 565
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->lineReader:Lcom/android/internal/telephony/test/LineReader;

    #@7
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/LineReader;->getNextLineCtrlZ()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 567
    .local v0, pdu:Ljava/lang/String;
    const-string v1, "+CMGS: 1"

    #@d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V

    #@10
    .line 568
    return-void
.end method

.method private releaseActiveAcceptHeldOrWaiting()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 492
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->releaseActiveAcceptHeldOrWaiting()Z

    #@5
    move-result v0

    #@6
    .line 494
    .local v0, success:Z
    if-nez v0, :cond_10

    #@8
    .line 495
    new-instance v1, Lcom/android/internal/telephony/test/InterpreterEx;

    #@a
    const-string v2, "ERROR"

    #@c
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/test/InterpreterEx;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 497
    :cond_10
    return-void
.end method

.method private releaseHeldOrUDUB()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 480
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->releaseHeldOrUDUB()Z

    #@5
    move-result v0

    #@6
    .line 482
    .local v0, success:Z
    if-nez v0, :cond_10

    #@8
    .line 483
    new-instance v1, Lcom/android/internal/telephony/test/InterpreterEx;

    #@a
    const-string v2, "ERROR"

    #@c
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/test/InterpreterEx;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 485
    :cond_10
    return-void
.end method

.method private separateCall(I)V
    .registers 5
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 516
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->separateCall(I)Z

    #@5
    move-result v0

    #@6
    .line 518
    .local v0, success:Z
    if-nez v0, :cond_10

    #@8
    .line 519
    new-instance v1, Lcom/android/internal/telephony/test/InterpreterEx;

    #@a
    const-string v2, "ERROR"

    #@c
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/test/InterpreterEx;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 521
    :cond_10
    return-void
.end method

.method private switchActiveAndHeldOrWaiting()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 504
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->switchActiveAndHeldOrWaiting()Z

    #@5
    move-result v0

    #@6
    .line 506
    .local v0, success:Z
    if-nez v0, :cond_10

    #@8
    .line 507
    new-instance v1, Lcom/android/internal/telephony/test/InterpreterEx;

    #@a
    const-string v2, "ERROR"

    #@c
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/test/InterpreterEx;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 509
    :cond_10
    return-void
.end method


# virtual methods
.method public pauseResponses()V
    .registers 3

    #@0
    .prologue
    .line 409
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseMonitor:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 410
    :try_start_3
    iget v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseCount:I

    #@5
    add-int/lit8 v0, v0, 0x1

    #@7
    iput v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseCount:I

    #@9
    .line 411
    monitor-exit v1

    #@a
    .line 412
    return-void

    #@b
    .line 411
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method print(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 669
    monitor-enter p0

    #@1
    .line 671
    :try_start_1
    const-string v2, "US-ASCII"

    #@3
    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@6
    move-result-object v0

    #@7
    .line 675
    .local v0, bytes:[B
    iget-object v2, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->out:Ljava/io/OutputStream;

    #@9
    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_13
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_c} :catch_e

    #@c
    .line 679
    .end local v0           #bytes:[B
    :goto_c
    :try_start_c
    monitor-exit p0

    #@d
    .line 680
    return-void

    #@e
    .line 676
    :catch_e
    move-exception v1

    #@f
    .line 677
    .local v1, ex:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@12
    goto :goto_c

    #@13
    .line 679
    .end local v1           #ex:Ljava/io/IOException;
    :catchall_13
    move-exception v2

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_c .. :try_end_15} :catchall_13

    #@15
    throw v2
.end method

.method println(Ljava/lang/String;)V
    .registers 6
    .parameter "s"

    #@0
    .prologue
    .line 652
    monitor-enter p0

    #@1
    .line 654
    :try_start_1
    const-string v2, "US-ASCII"

    #@3
    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@6
    move-result-object v0

    #@7
    .line 658
    .local v0, bytes:[B
    iget-object v2, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->out:Ljava/io/OutputStream;

    #@9
    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write([B)V

    #@c
    .line 659
    iget-object v2, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->out:Ljava/io/OutputStream;

    #@e
    const/16 v3, 0xd

    #@10
    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_1a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_13} :catch_15

    #@13
    .line 663
    .end local v0           #bytes:[B
    :goto_13
    :try_start_13
    monitor-exit p0

    #@14
    .line 664
    return-void

    #@15
    .line 660
    :catch_15
    move-exception v1

    #@16
    .line 661
    .local v1, ex:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@19
    goto :goto_13

    #@1a
    .line 663
    .end local v1           #ex:Ljava/io/IOException;
    :catchall_1a
    move-exception v2

    #@1b
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_13 .. :try_end_1c} :catchall_1a

    #@1c
    throw v2
.end method

.method processLine(Ljava/lang/String;)V
    .registers 10
    .parameter "line"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    .line 575
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/test/ModelInterpreter;->splitCommands(Ljava/lang/String;)[Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 577
    .local v1, commands:[Ljava/lang/String;
    const/4 v3, 0x0

    #@5
    .local v3, i:I
    :goto_5
    array-length v6, v1

    #@6
    if-ge v3, v6, :cond_82

    #@8
    .line 578
    aget-object v0, v1, v3

    #@a
    .line 580
    .local v0, command:Ljava/lang/String;
    const-string v6, "A"

    #@c
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v6

    #@10
    if-eqz v6, :cond_18

    #@12
    .line 581
    invoke-direct {p0}, Lcom/android/internal/telephony/test/ModelInterpreter;->onAnswer()V

    #@15
    .line 577
    :cond_15
    :goto_15
    add-int/lit8 v3, v3, 0x1

    #@17
    goto :goto_5

    #@18
    .line 582
    :cond_18
    const-string v6, "H"

    #@1a
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v6

    #@1e
    if-eqz v6, :cond_24

    #@20
    .line 583
    invoke-direct {p0}, Lcom/android/internal/telephony/test/ModelInterpreter;->onHangup()V

    #@23
    goto :goto_15

    #@24
    .line 584
    :cond_24
    const-string v6, "+CHLD="

    #@26
    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@29
    move-result v6

    #@2a
    if-eqz v6, :cond_30

    #@2c
    .line 585
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/test/ModelInterpreter;->onCHLD(Ljava/lang/String;)V

    #@2f
    goto :goto_15

    #@30
    .line 586
    :cond_30
    const-string v6, "+CLCC"

    #@32
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v6

    #@36
    if-eqz v6, :cond_3c

    #@38
    .line 587
    invoke-direct {p0}, Lcom/android/internal/telephony/test/ModelInterpreter;->onCLCC()V

    #@3b
    goto :goto_15

    #@3c
    .line 588
    :cond_3c
    const-string v6, "D"

    #@3e
    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@41
    move-result v6

    #@42
    if-eqz v6, :cond_48

    #@44
    .line 589
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/test/ModelInterpreter;->onDial(Ljava/lang/String;)V

    #@47
    goto :goto_15

    #@48
    .line 590
    :cond_48
    const-string v6, "+CMGS="

    #@4a
    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@4d
    move-result v6

    #@4e
    if-eqz v6, :cond_54

    #@50
    .line 591
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/test/ModelInterpreter;->onSMSSend(Ljava/lang/String;)V

    #@53
    goto :goto_15

    #@54
    .line 593
    :cond_54
    const/4 v2, 0x0

    #@55
    .line 595
    .local v2, found:Z
    const/4 v4, 0x0

    #@56
    .local v4, j:I
    :goto_56
    sget-object v6, Lcom/android/internal/telephony/test/ModelInterpreter;->sDefaultResponses:[[Ljava/lang/String;

    #@58
    array-length v6, v6

    #@59
    if-ge v4, v6, :cond_75

    #@5b
    .line 596
    sget-object v6, Lcom/android/internal/telephony/test/ModelInterpreter;->sDefaultResponses:[[Ljava/lang/String;

    #@5d
    aget-object v6, v6, v4

    #@5f
    const/4 v7, 0x0

    #@60
    aget-object v6, v6, v7

    #@62
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@65
    move-result v6

    #@66
    if-eqz v6, :cond_7f

    #@68
    .line 597
    sget-object v6, Lcom/android/internal/telephony/test/ModelInterpreter;->sDefaultResponses:[[Ljava/lang/String;

    #@6a
    aget-object v6, v6, v4

    #@6c
    const/4 v7, 0x1

    #@6d
    aget-object v5, v6, v7

    #@6f
    .line 598
    .local v5, r:Ljava/lang/String;
    if-eqz v5, :cond_74

    #@71
    .line 599
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V

    #@74
    .line 601
    :cond_74
    const/4 v2, 0x1

    #@75
    .line 606
    .end local v5           #r:Ljava/lang/String;
    :cond_75
    if-nez v2, :cond_15

    #@77
    .line 607
    new-instance v6, Lcom/android/internal/telephony/test/InterpreterEx;

    #@79
    const-string v7, "ERROR"

    #@7b
    invoke-direct {v6, v7}, Lcom/android/internal/telephony/test/InterpreterEx;-><init>(Ljava/lang/String;)V

    #@7e
    throw v6

    #@7f
    .line 595
    :cond_7f
    add-int/lit8 v4, v4, 0x1

    #@81
    goto :goto_56

    #@82
    .line 611
    .end local v0           #command:Ljava/lang/String;
    .end local v2           #found:Z
    .end local v4           #j:I
    :cond_82
    return-void
.end method

.method public progressConnectingCallState()V
    .registers 2

    #@0
    .prologue
    .line 294
    iget-object v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->progressConnectingCallState()V

    #@5
    .line 295
    return-void
.end method

.method public progressConnectingToActive()V
    .registers 2

    #@0
    .prologue
    .line 302
    iget-object v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->progressConnectingToActive()V

    #@5
    .line 303
    return-void
.end method

.method public resumeResponses()V
    .registers 3

    #@0
    .prologue
    .line 417
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseMonitor:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 418
    :try_start_3
    iget v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseCount:I

    #@5
    add-int/lit8 v0, v0, -0x1

    #@7
    iput v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseCount:I

    #@9
    .line 420
    iget v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseCount:I

    #@b
    if-nez v0, :cond_12

    #@d
    .line 421
    iget-object v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseMonitor:Ljava/lang/Object;

    #@f
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@12
    .line 423
    :cond_12
    monitor-exit v1

    #@13
    .line 424
    return-void

    #@14
    .line 423
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public run()V
    .registers 6

    #@0
    .prologue
    .line 201
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->ss:Ljava/net/ServerSocket;

    #@2
    if-eqz v3, :cond_1d

    #@4
    .line 205
    :try_start_4
    iget-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->ss:Ljava/net/ServerSocket;

    #@6
    invoke-virtual {v3}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_9} :catch_3f

    #@9
    move-result-object v2

    #@a
    .line 213
    .local v2, s:Ljava/net/Socket;
    :try_start_a
    invoke-virtual {v2}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    #@d
    move-result-object v3

    #@e
    iput-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->in:Ljava/io/InputStream;

    #@10
    .line 214
    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    #@13
    move-result-object v3

    #@14
    iput-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->out:Ljava/io/OutputStream;
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_16} :catch_48

    #@16
    .line 221
    const-string v3, "ModelInterpreter"

    #@18
    const-string v4, "New connection accepted"

    #@1a
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 225
    .end local v2           #s:Ljava/net/Socket;
    :cond_1d
    new-instance v3, Lcom/android/internal/telephony/test/LineReader;

    #@1f
    iget-object v4, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->in:Ljava/io/InputStream;

    #@21
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/test/LineReader;-><init>(Ljava/io/InputStream;)V

    #@24
    iput-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->lineReader:Lcom/android/internal/telephony/test/LineReader;

    #@26
    .line 227
    const-string v3, "Welcome"

    #@28
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V

    #@2b
    .line 232
    :goto_2b
    iget-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->lineReader:Lcom/android/internal/telephony/test/LineReader;

    #@2d
    invoke-virtual {v3}, Lcom/android/internal/telephony/test/LineReader;->getNextLine()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    .line 236
    .local v1, line:Ljava/lang/String;
    if-nez v1, :cond_51

    #@33
    .line 263
    const-string v3, "ModelInterpreter"

    #@35
    const-string v4, "Disconnected"

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 265
    iget-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->ss:Ljava/net/ServerSocket;

    #@3c
    if-nez v3, :cond_0

    #@3e
    .line 270
    .end local v1           #line:Ljava/lang/String;
    :goto_3e
    return-void

    #@3f
    .line 206
    :catch_3f
    move-exception v0

    #@40
    .line 207
    .local v0, ex:Ljava/io/IOException;
    const-string v3, "ModelInterpreter"

    #@42
    const-string v4, "IOException on socket.accept(); stopping"

    #@44
    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@47
    goto :goto_3e

    #@48
    .line 215
    .end local v0           #ex:Ljava/io/IOException;
    .restart local v2       #s:Ljava/net/Socket;
    :catch_48
    move-exception v0

    #@49
    .line 216
    .restart local v0       #ex:Ljava/io/IOException;
    const-string v3, "ModelInterpreter"

    #@4b
    const-string v4, "IOException on accepted socket(); re-listening"

    #@4d
    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@50
    goto :goto_0

    #@51
    .line 240
    .end local v0           #ex:Ljava/io/IOException;
    .end local v2           #s:Ljava/net/Socket;
    .restart local v1       #line:Ljava/lang/String;
    :cond_51
    iget-object v4, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseMonitor:Ljava/lang/Object;

    #@53
    monitor-enter v4

    #@54
    .line 241
    :goto_54
    :try_start_54
    iget v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseCount:I
    :try_end_56
    .catchall {:try_start_54 .. :try_end_56} :catchall_73

    #@56
    if-lez v3, :cond_60

    #@58
    .line 243
    :try_start_58
    iget-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->pausedResponseMonitor:Ljava/lang/Object;

    #@5a
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_5d
    .catchall {:try_start_58 .. :try_end_5d} :catchall_73
    .catch Ljava/lang/InterruptedException; {:try_start_58 .. :try_end_5d} :catch_5e

    #@5d
    goto :goto_54

    #@5e
    .line 244
    :catch_5e
    move-exception v3

    #@5f
    goto :goto_54

    #@60
    .line 247
    :cond_60
    :try_start_60
    monitor-exit v4
    :try_end_61
    .catchall {:try_start_60 .. :try_end_61} :catchall_73

    #@61
    .line 249
    monitor-enter p0

    #@62
    .line 251
    :try_start_62
    const-string v3, "OK"

    #@64
    iput-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->finalResponse:Ljava/lang/String;

    #@66
    .line 252
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/test/ModelInterpreter;->processLine(Ljava/lang/String;)V

    #@69
    .line 253
    iget-object v3, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->finalResponse:Ljava/lang/String;

    #@6b
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V
    :try_end_6e
    .catchall {:try_start_62 .. :try_end_6e} :catchall_70
    .catch Lcom/android/internal/telephony/test/InterpreterEx; {:try_start_62 .. :try_end_6e} :catch_76
    .catch Ljava/lang/RuntimeException; {:try_start_62 .. :try_end_6e} :catch_7d

    #@6e
    .line 260
    :goto_6e
    :try_start_6e
    monitor-exit p0

    #@6f
    goto :goto_2b

    #@70
    :catchall_70
    move-exception v3

    #@71
    monitor-exit p0
    :try_end_72
    .catchall {:try_start_6e .. :try_end_72} :catchall_70

    #@72
    throw v3

    #@73
    .line 247
    :catchall_73
    move-exception v3

    #@74
    :try_start_74
    monitor-exit v4
    :try_end_75
    .catchall {:try_start_74 .. :try_end_75} :catchall_73

    #@75
    throw v3

    #@76
    .line 254
    :catch_76
    move-exception v0

    #@77
    .line 255
    .local v0, ex:Lcom/android/internal/telephony/test/InterpreterEx;
    :try_start_77
    iget-object v3, v0, Lcom/android/internal/telephony/test/InterpreterEx;->result:Ljava/lang/String;

    #@79
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V

    #@7c
    goto :goto_6e

    #@7d
    .line 256
    .end local v0           #ex:Lcom/android/internal/telephony/test/InterpreterEx;
    :catch_7d
    move-exception v0

    #@7e
    .line 257
    .local v0, ex:Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    #@81
    .line 258
    const-string v3, "ERROR"

    #@83
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V
    :try_end_86
    .catchall {:try_start_77 .. :try_end_86} :catchall_70

    #@86
    goto :goto_6e
.end method

.method public sendUnsolicited(Ljava/lang/String;)V
    .registers 3
    .parameter "unsol"

    #@0
    .prologue
    .line 369
    monitor-enter p0

    #@1
    .line 370
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V

    #@4
    .line 371
    monitor-exit p0

    #@5
    .line 372
    return-void

    #@6
    .line 371
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_6

    #@8
    throw v0
.end method

.method public setAutoProgressConnectingCall(Z)V
    .registers 3
    .parameter "b"

    #@0
    .prologue
    .line 311
    iget-object v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->setAutoProgressConnectingCall(Z)V

    #@5
    .line 312
    return-void
.end method

.method public setNextCallFailCause(I)V
    .registers 2
    .parameter "gsmCause"

    #@0
    .prologue
    .line 323
    return-void
.end method

.method public setNextDialFailImmediately(Z)V
    .registers 3
    .parameter "b"

    #@0
    .prologue
    .line 317
    iget-object v0, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->setNextDialFailImmediately(Z)V

    #@5
    .line 318
    return-void
.end method

.method public shutdown()V
    .registers 3

    #@0
    .prologue
    .line 686
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->mHandlerThread:Landroid/os/HandlerThread;

    #@2
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@5
    move-result-object v0

    #@6
    .line 687
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_b

    #@8
    .line 688
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    #@b
    .line 692
    :cond_b
    :try_start_b
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->in:Ljava/io/InputStream;

    #@d
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_10} :catch_18

    #@10
    .line 696
    :goto_10
    :try_start_10
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->out:Ljava/io/OutputStream;

    #@12
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_15} :catch_16

    #@15
    .line 699
    :goto_15
    return-void

    #@16
    .line 697
    :catch_16
    move-exception v1

    #@17
    goto :goto_15

    #@18
    .line 693
    :catch_18
    move-exception v1

    #@19
    goto :goto_10
.end method

.method splitCommands(Ljava/lang/String;)[Ljava/lang/String;
    .registers 6
    .parameter "line"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InterpreterEx;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const/4 v2, 0x0

    #@2
    .line 617
    const-string v1, "AT"

    #@4
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_12

    #@a
    .line 618
    new-instance v1, Lcom/android/internal/telephony/test/InterpreterEx;

    #@c
    const-string v2, "ERROR"

    #@e
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/test/InterpreterEx;-><init>(Ljava/lang/String;)V

    #@11
    throw v1

    #@12
    .line 621
    :cond_12
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@15
    move-result v1

    #@16
    if-ne v1, v3, :cond_1b

    #@18
    .line 623
    new-array v0, v2, [Ljava/lang/String;

    #@1a
    .line 631
    :goto_1a
    return-object v0

    #@1b
    .line 626
    :cond_1b
    const/4 v1, 0x1

    #@1c
    new-array v0, v1, [Ljava/lang/String;

    #@1e
    .line 629
    .local v0, ret:[Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    aput-object v1, v0, v2

    #@24
    goto :goto_1a
.end method

.method public triggerHangupAll()V
    .registers 3

    #@0
    .prologue
    .line 359
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->triggerHangupAll()Z

    #@5
    move-result v0

    #@6
    .line 361
    .local v0, success:Z
    if-eqz v0, :cond_d

    #@8
    .line 362
    const-string v1, "NO CARRIER"

    #@a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V

    #@d
    .line 364
    :cond_d
    return-void
.end method

.method public triggerHangupBackground()V
    .registers 3

    #@0
    .prologue
    .line 345
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->triggerHangupBackground()Z

    #@5
    move-result v0

    #@6
    .line 347
    .local v0, success:Z
    if-eqz v0, :cond_d

    #@8
    .line 348
    const-string v1, "NO CARRIER"

    #@a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V

    #@d
    .line 350
    :cond_d
    return-void
.end method

.method public triggerHangupForeground()V
    .registers 3

    #@0
    .prologue
    .line 332
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->triggerHangupForeground()Z

    #@5
    move-result v0

    #@6
    .line 334
    .local v0, success:Z
    if-eqz v0, :cond_d

    #@8
    .line 335
    const-string v1, "NO CARRIER"

    #@a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V

    #@d
    .line 337
    :cond_d
    return-void
.end method

.method public triggerIncomingSMS(Ljava/lang/String;)V
    .registers 2
    .parameter "message"

    #@0
    .prologue
    .line 404
    return-void
.end method

.method public triggerIncomingUssd(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "statusCode"
    .parameter "message"

    #@0
    .prologue
    .line 375
    return-void
.end method

.method public triggerRing(Ljava/lang/String;)V
    .registers 4
    .parameter "number"

    #@0
    .prologue
    .line 279
    monitor-enter p0

    #@1
    .line 282
    :try_start_1
    iget-object v1, p0, Lcom/android/internal/telephony/test/ModelInterpreter;->simulatedCallState:Lcom/android/internal/telephony/test/SimulatedGsmCallState;

    #@3
    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->triggerRing(Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    .line 284
    .local v0, success:Z
    if-eqz v0, :cond_e

    #@9
    .line 285
    const-string v1, "RING"

    #@b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/test/ModelInterpreter;->println(Ljava/lang/String;)V

    #@e
    .line 287
    :cond_e
    monitor-exit p0

    #@f
    .line 288
    return-void

    #@10
    .line 287
    .end local v0           #success:Z
    :catchall_10
    move-exception v1

    #@11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_10

    #@12
    throw v1
.end method

.method public triggerSsn(II)V
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 374
    return-void
.end method
