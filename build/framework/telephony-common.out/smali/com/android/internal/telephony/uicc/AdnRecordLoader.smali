.class public Lcom/android/internal/telephony/uicc/AdnRecordLoader;
.super Landroid/os/Handler;
.source "AdnRecordLoader.java"


# static fields
.field static final EVENT_ADN_LOAD_ALL_DONE:I = 0x3

.field static final EVENT_ADN_LOAD_DONE:I = 0x1

.field static final EVENT_EF_LINEAR_RECORD_SIZE_DONE:I = 0x4

.field static final EVENT_EXT_RECORD_LOAD_DONE:I = 0x2

.field static final EVENT_UPDATE_RECORD_DONE:I = 0x5

.field static final LOG_TAG:Ljava/lang/String; = "RIL_AdnRecordLoader"


# instance fields
.field adns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/uicc/AdnRecord;",
            ">;"
        }
    .end annotation
.end field

.field ef:I

.field extensionEF:I

.field private mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

.field pendingExtLoads:I

.field pin2:Ljava/lang/String;

.field recordNumber:I

.field result:Ljava/lang/Object;

.field userResponse:Landroid/os/Message;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V
    .registers 3
    .parameter "fh"

    #@0
    .prologue
    .line 64
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@7
    .line 65
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@9
    .line 66
    return-void
.end method

.method private getEFPath(I)Ljava/lang/String;
    .registers 3
    .parameter "efid"

    #@0
    .prologue
    .line 69
    const/16 v0, 0x6f3a

    #@2
    if-ne p1, v0, :cond_7

    #@4
    .line 70
    const-string v0, "3F007F10"

    #@6
    .line 73
    :goto_6
    return-object v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 17
    .parameter "msg"

    #@0
    .prologue
    const/4 v14, 0x0

    #@1
    .line 167
    :try_start_1
    move-object/from16 v0, p1

    #@3
    iget v1, v0, Landroid/os/Message;->what:I
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_5} :catch_42

    #@5
    packed-switch v1, :pswitch_data_1d6

    #@8
    .line 304
    :cond_8
    :goto_8
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->userResponse:Landroid/os/Message;

    #@a
    if-eqz v1, :cond_21

    #@c
    iget v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->pendingExtLoads:I

    #@e
    if-nez v1, :cond_21

    #@10
    .line 305
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->userResponse:Landroid/os/Message;

    #@12
    invoke-static {v1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@15
    move-result-object v1

    #@16
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->result:Ljava/lang/Object;

    #@18
    iput-object v2, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1a
    .line 308
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->userResponse:Landroid/os/Message;

    #@1c
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@1f
    .line 309
    iput-object v14, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->userResponse:Landroid/os/Message;

    #@21
    .line 311
    :cond_21
    :goto_21
    return-void

    #@22
    .line 169
    :pswitch_22
    :try_start_22
    move-object/from16 v0, p1

    #@24
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@26
    check-cast v1, Landroid/os/AsyncResult;

    #@28
    move-object v0, v1

    #@29
    check-cast v0, Landroid/os/AsyncResult;

    #@2b
    move-object v8, v0

    #@2c
    .line 170
    .local v8, ar:Landroid/os/AsyncResult;
    iget-object v1, v8, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@2e
    check-cast v1, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@30
    move-object v0, v1

    #@31
    check-cast v0, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@33
    move-object v7, v0

    #@34
    .line 172
    .local v7, adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    iget-object v1, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@36
    if-eqz v1, :cond_57

    #@38
    .line 173
    new-instance v1, Ljava/lang/RuntimeException;

    #@3a
    const-string v2, "get EF record size failed"

    #@3c
    iget-object v3, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3e
    invoke-direct {v1, v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@41
    throw v1
    :try_end_42
    .catch Ljava/lang/RuntimeException; {:try_start_22 .. :try_end_42} :catch_42

    #@42
    .line 292
    .end local v7           #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    .end local v8           #ar:Landroid/os/AsyncResult;
    :catch_42
    move-exception v10

    #@43
    .line 293
    .local v10, exc:Ljava/lang/RuntimeException;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->userResponse:Landroid/os/Message;

    #@45
    if-eqz v1, :cond_21

    #@47
    .line 294
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->userResponse:Landroid/os/Message;

    #@49
    invoke-static {v1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@4c
    move-result-object v1

    #@4d
    iput-object v10, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4f
    .line 296
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->userResponse:Landroid/os/Message;

    #@51
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@54
    .line 299
    iput-object v14, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->userResponse:Landroid/os/Message;

    #@56
    goto :goto_21

    #@57
    .line 177
    .end local v10           #exc:Ljava/lang/RuntimeException;
    .restart local v7       #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    .restart local v8       #ar:Landroid/os/AsyncResult;
    :cond_57
    :try_start_57
    iget-object v1, v8, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@59
    check-cast v1, [I

    #@5b
    move-object v0, v1

    #@5c
    check-cast v0, [I

    #@5e
    move-object v12, v0

    #@5f
    .line 183
    .local v12, recordSize:[I
    array-length v1, v12

    #@60
    const/4 v2, 0x3

    #@61
    if-ne v1, v2, :cond_6a

    #@63
    iget v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->recordNumber:I

    #@65
    const/4 v2, 0x2

    #@66
    aget v2, v12, v2

    #@68
    if-le v1, v2, :cond_74

    #@6a
    .line 184
    :cond_6a
    new-instance v1, Ljava/lang/RuntimeException;

    #@6c
    const-string v2, "get wrong EF record size format"

    #@6e
    iget-object v3, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@70
    invoke-direct {v1, v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@73
    throw v1

    #@74
    .line 188
    :cond_74
    const/4 v1, 0x0

    #@75
    aget v1, v12, v1

    #@77
    invoke-virtual {v7, v1}, Lcom/android/internal/telephony/uicc/AdnRecord;->buildAdnString(I)[B

    #@7a
    move-result-object v4

    #@7b
    .line 190
    .local v4, data:[B
    if-nez v4, :cond_87

    #@7d
    .line 191
    new-instance v1, Ljava/lang/RuntimeException;

    #@7f
    const-string v2, "wrong ADN format"

    #@81
    iget-object v3, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@83
    invoke-direct {v1, v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@86
    throw v1

    #@87
    .line 195
    :cond_87
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@89
    iget v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->ef:I

    #@8b
    iget v3, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->recordNumber:I

    #@8d
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->pin2:Ljava/lang/String;

    #@8f
    const/4 v6, 0x5

    #@90
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->obtainMessage(I)Landroid/os/Message;

    #@93
    move-result-object v6

    #@94
    invoke-virtual/range {v1 .. v6}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFLinearFixed(II[BLjava/lang/String;Landroid/os/Message;)V

    #@97
    .line 198
    const/4 v1, 0x1

    #@98
    iput v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->pendingExtLoads:I

    #@9a
    goto/16 :goto_8

    #@9c
    .line 202
    .end local v4           #data:[B
    .end local v7           #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    .end local v8           #ar:Landroid/os/AsyncResult;
    .end local v12           #recordSize:[I
    :pswitch_9c
    move-object/from16 v0, p1

    #@9e
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a0
    check-cast v1, Landroid/os/AsyncResult;

    #@a2
    move-object v0, v1

    #@a3
    check-cast v0, Landroid/os/AsyncResult;

    #@a5
    move-object v8, v0

    #@a6
    .line 203
    .restart local v8       #ar:Landroid/os/AsyncResult;
    iget-object v1, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@a8
    if-eqz v1, :cond_b4

    #@aa
    .line 204
    new-instance v1, Ljava/lang/RuntimeException;

    #@ac
    const-string v2, "update EF adn record failed"

    #@ae
    iget-object v3, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@b0
    invoke-direct {v1, v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b3
    throw v1

    #@b4
    .line 207
    :cond_b4
    const/4 v1, 0x0

    #@b5
    iput v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->pendingExtLoads:I

    #@b7
    .line 208
    const/4 v1, 0x0

    #@b8
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->result:Ljava/lang/Object;

    #@ba
    goto/16 :goto_8

    #@bc
    .line 211
    .end local v8           #ar:Landroid/os/AsyncResult;
    :pswitch_bc
    move-object/from16 v0, p1

    #@be
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c0
    check-cast v1, Landroid/os/AsyncResult;

    #@c2
    move-object v0, v1

    #@c3
    check-cast v0, Landroid/os/AsyncResult;

    #@c5
    move-object v8, v0

    #@c6
    .line 212
    .restart local v8       #ar:Landroid/os/AsyncResult;
    iget-object v1, v8, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c8
    check-cast v1, [B

    #@ca
    move-object v0, v1

    #@cb
    check-cast v0, [B

    #@cd
    move-object v4, v0

    #@ce
    .line 214
    .restart local v4       #data:[B
    iget-object v1, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@d0
    if-eqz v1, :cond_dc

    #@d2
    .line 215
    new-instance v1, Ljava/lang/RuntimeException;

    #@d4
    const-string v2, "load failed"

    #@d6
    iget-object v3, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@d8
    invoke-direct {v1, v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@db
    throw v1

    #@dc
    .line 225
    :cond_dc
    new-instance v7, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@de
    iget v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->ef:I

    #@e0
    iget v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->recordNumber:I

    #@e2
    invoke-direct {v7, v1, v2, v4}, Lcom/android/internal/telephony/uicc/AdnRecord;-><init>(II[B)V

    #@e5
    .line 226
    .restart local v7       #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    iput-object v7, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->result:Ljava/lang/Object;

    #@e7
    .line 228
    invoke-virtual {v7}, Lcom/android/internal/telephony/uicc/AdnRecord;->hasExtendedRecord()Z

    #@ea
    move-result v1

    #@eb
    if-eqz v1, :cond_8

    #@ed
    .line 233
    const/4 v1, 0x1

    #@ee
    iput v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->pendingExtLoads:I

    #@f0
    .line 235
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@f2
    iget v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->extensionEF:I

    #@f4
    iget v3, v7, Lcom/android/internal/telephony/uicc/AdnRecord;->extRecord:I

    #@f6
    const/4 v5, 0x2

    #@f7
    invoke-virtual {p0, v5, v7}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@fa
    move-result-object v5

    #@fb
    invoke-virtual {v1, v2, v3, v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V

    #@fe
    goto/16 :goto_8

    #@100
    .line 242
    .end local v4           #data:[B
    .end local v7           #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    .end local v8           #ar:Landroid/os/AsyncResult;
    :pswitch_100
    move-object/from16 v0, p1

    #@102
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@104
    check-cast v1, Landroid/os/AsyncResult;

    #@106
    move-object v0, v1

    #@107
    check-cast v0, Landroid/os/AsyncResult;

    #@109
    move-object v8, v0

    #@10a
    .line 243
    .restart local v8       #ar:Landroid/os/AsyncResult;
    iget-object v1, v8, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@10c
    check-cast v1, [B

    #@10e
    move-object v0, v1

    #@10f
    check-cast v0, [B

    #@111
    move-object v4, v0

    #@112
    .line 244
    .restart local v4       #data:[B
    iget-object v1, v8, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@114
    check-cast v1, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@116
    move-object v0, v1

    #@117
    check-cast v0, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@119
    move-object v7, v0

    #@11a
    .line 246
    .restart local v7       #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    iget-object v1, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@11c
    if-eqz v1, :cond_128

    #@11e
    .line 247
    new-instance v1, Ljava/lang/RuntimeException;

    #@120
    const-string v2, "load failed"

    #@122
    iget-object v3, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@124
    invoke-direct {v1, v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@127
    throw v1

    #@128
    .line 250
    :cond_128
    const-string v1, "RIL_AdnRecordLoader"

    #@12a
    new-instance v2, Ljava/lang/StringBuilder;

    #@12c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12f
    const-string v3, "ADN extension EF: 0x"

    #@131
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v2

    #@135
    iget v3, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->extensionEF:I

    #@137
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@13a
    move-result-object v3

    #@13b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v2

    #@13f
    const-string v3, ":"

    #@141
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v2

    #@145
    iget v3, v7, Lcom/android/internal/telephony/uicc/AdnRecord;->extRecord:I

    #@147
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v2

    #@14b
    const-string v3, "\n"

    #@14d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v2

    #@151
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@154
    move-result-object v3

    #@155
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v2

    #@159
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15c
    move-result-object v2

    #@15d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@160
    .line 255
    invoke-virtual {v7, v4}, Lcom/android/internal/telephony/uicc/AdnRecord;->appendExtRecord([B)V

    #@163
    .line 257
    iget v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->pendingExtLoads:I

    #@165
    add-int/lit8 v1, v1, -0x1

    #@167
    iput v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->pendingExtLoads:I

    #@169
    goto/16 :goto_8

    #@16b
    .line 263
    .end local v4           #data:[B
    .end local v7           #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    .end local v8           #ar:Landroid/os/AsyncResult;
    :pswitch_16b
    move-object/from16 v0, p1

    #@16d
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16f
    check-cast v1, Landroid/os/AsyncResult;

    #@171
    move-object v0, v1

    #@172
    check-cast v0, Landroid/os/AsyncResult;

    #@174
    move-object v8, v0

    #@175
    .line 264
    .restart local v8       #ar:Landroid/os/AsyncResult;
    iget-object v1, v8, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@177
    check-cast v1, Ljava/util/ArrayList;

    #@179
    move-object v0, v1

    #@17a
    check-cast v0, Ljava/util/ArrayList;

    #@17c
    move-object v9, v0

    #@17d
    .line 266
    .local v9, datas:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    iget-object v1, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@17f
    if-eqz v1, :cond_18b

    #@181
    .line 267
    new-instance v1, Ljava/lang/RuntimeException;

    #@183
    const-string v2, "load failed"

    #@185
    iget-object v3, v8, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@187
    invoke-direct {v1, v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@18a
    throw v1

    #@18b
    .line 270
    :cond_18b
    new-instance v1, Ljava/util/ArrayList;

    #@18d
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@190
    move-result v2

    #@191
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@194
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->adns:Ljava/util/ArrayList;

    #@196
    .line 271
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->adns:Ljava/util/ArrayList;

    #@198
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->result:Ljava/lang/Object;

    #@19a
    .line 272
    const/4 v1, 0x0

    #@19b
    iput v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->pendingExtLoads:I

    #@19d
    .line 274
    const/4 v11, 0x0

    #@19e
    .local v11, i:I
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@1a1
    move-result v13

    #@1a2
    .local v13, s:I
    :goto_1a2
    if-ge v11, v13, :cond_8

    #@1a4
    .line 275
    new-instance v7, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@1a6
    iget v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->ef:I

    #@1a8
    add-int/lit8 v3, v11, 0x1

    #@1aa
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1ad
    move-result-object v1

    #@1ae
    check-cast v1, [B

    #@1b0
    invoke-direct {v7, v2, v3, v1}, Lcom/android/internal/telephony/uicc/AdnRecord;-><init>(II[B)V

    #@1b3
    .line 276
    .restart local v7       #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->adns:Ljava/util/ArrayList;

    #@1b5
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1b8
    .line 278
    invoke-virtual {v7}, Lcom/android/internal/telephony/uicc/AdnRecord;->hasExtendedRecord()Z

    #@1bb
    move-result v1

    #@1bc
    if-eqz v1, :cond_1d2

    #@1be
    .line 283
    iget v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->pendingExtLoads:I

    #@1c0
    add-int/lit8 v1, v1, 0x1

    #@1c2
    iput v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->pendingExtLoads:I

    #@1c4
    .line 285
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1c6
    iget v2, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->extensionEF:I

    #@1c8
    iget v3, v7, Lcom/android/internal/telephony/uicc/AdnRecord;->extRecord:I

    #@1ca
    const/4 v5, 0x2

    #@1cb
    invoke-virtual {p0, v5, v7}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1ce
    move-result-object v5

    #@1cf
    invoke-virtual {v1, v2, v3, v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V
    :try_end_1d2
    .catch Ljava/lang/RuntimeException; {:try_start_57 .. :try_end_1d2} :catch_42

    #@1d2
    .line 274
    :cond_1d2
    add-int/lit8 v11, v11, 0x1

    #@1d4
    goto :goto_1a2

    #@1d5
    .line 167
    nop

    #@1d6
    :pswitch_data_1d6
    .packed-switch 0x1
        :pswitch_bc
        :pswitch_100
        :pswitch_16b
        :pswitch_22
        :pswitch_9c
    .end packed-switch
.end method

.method public loadAllFromEF(IILandroid/os/Message;)V
    .registers 7
    .parameter "ef"
    .parameter "extensionEF"
    .parameter "response"

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    .line 108
    iput p1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->ef:I

    #@3
    .line 109
    iput p2, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->extensionEF:I

    #@5
    .line 110
    iput-object p3, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->userResponse:Landroid/os/Message;

    #@7
    .line 116
    const/16 v0, 0x6f3a

    #@9
    if-ne p1, v0, :cond_19

    #@b
    .line 118
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@d
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->getEFPath(I)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->obtainMessage(I)Landroid/os/Message;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v0, p1, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILjava/lang/String;Landroid/os/Message;)V

    #@18
    .line 126
    :goto_18
    return-void

    #@19
    .line 122
    :cond_19
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1b
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->obtainMessage(I)Landroid/os/Message;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, p1, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V

    #@22
    goto :goto_18
.end method

.method public loadFromEF(IIILandroid/os/Message;)V
    .registers 8
    .parameter "ef"
    .parameter "extensionEF"
    .parameter "recordNumber"
    .parameter "response"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 83
    iput p1, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->ef:I

    #@3
    .line 84
    iput p2, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->extensionEF:I

    #@5
    .line 85
    iput p3, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->recordNumber:I

    #@7
    .line 86
    iput-object p4, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->userResponse:Landroid/os/Message;

    #@9
    .line 88
    const/16 v0, 0x6f3a

    #@b
    if-ne p1, v0, :cond_1b

    #@d
    .line 89
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@f
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->getEFPath(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->obtainMessage(I)Landroid/os/Message;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v0, p1, v1, p3, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(ILjava/lang/String;ILandroid/os/Message;)V

    #@1a
    .line 98
    :goto_1a
    return-void

    #@1b
    .line 93
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1d
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->obtainMessage(I)Landroid/os/Message;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v0, p1, p3, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V

    #@24
    goto :goto_1a
.end method

.method public updateEF(Lcom/android/internal/telephony/uicc/AdnRecord;IIILjava/lang/String;Landroid/os/Message;)V
    .registers 10
    .parameter "adn"
    .parameter "ef"
    .parameter "extensionEF"
    .parameter "recordNumber"
    .parameter "pin2"
    .parameter "response"

    #@0
    .prologue
    const/4 v2, 0x4

    #@1
    .line 143
    iput p2, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->ef:I

    #@3
    .line 144
    iput p3, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->extensionEF:I

    #@5
    .line 145
    iput p4, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->recordNumber:I

    #@7
    .line 146
    iput-object p6, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->userResponse:Landroid/os/Message;

    #@9
    .line 147
    iput-object p5, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->pin2:Ljava/lang/String;

    #@b
    .line 149
    const/16 v0, 0x6f3a

    #@d
    if-ne p2, v0, :cond_1d

    #@f
    .line 150
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@11
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->getEFPath(I)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {p0, v2, p1}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v0, p2, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFLinearRecordSize(ILjava/lang/String;Landroid/os/Message;)V

    #@1c
    .line 156
    :goto_1c
    return-void

    #@1d
    .line 153
    :cond_1d
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1f
    invoke-virtual {p0, v2, p1}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, p2, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFLinearRecordSize(ILandroid/os/Message;)V

    #@26
    goto :goto_1c
.end method
