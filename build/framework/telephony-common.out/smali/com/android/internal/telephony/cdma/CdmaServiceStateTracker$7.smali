.class Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;
.super Ljava/lang/Object;
.source "CdmaServiceStateTracker.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->PlayVZWERISound(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2406
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .registers 11
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 2408
    if-eqz p1, :cond_27

    #@4
    .line 2410
    :try_start_4
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->stop()V

    #@7
    .line 2411
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->release()V

    #@a
    .line 2412
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@c
    iget-boolean v2, v2, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z

    #@e
    if-ne v2, v6, :cond_27

    #@10
    .line 2413
    new-instance v1, Landroid/content/Intent;

    #@12
    const-string v2, "com.android.finishEriSound"

    #@14
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@17
    .line 2414
    .local v1, finishEriSound:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@19
    iget-object v2, v2, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@1b
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@22
    .line 2415
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@24
    const/4 v3, 0x0

    #@25
    iput-boolean v3, v2, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z
    :try_end_27
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_27} :catch_62

    #@27
    .line 2421
    .end local v1           #finishEriSound:Landroid/content/Intent;
    :cond_27
    :goto_27
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@29
    const/4 v3, 0x0

    #@2a
    invoke-static {v2, v3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->access$502(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    #@2d
    .line 2422
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2f
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->access$400(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)Landroid/media/AudioManager;

    #@32
    move-result-object v2

    #@33
    const/4 v3, 0x3

    #@34
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@36
    invoke-static {v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->access$300(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)I

    #@39
    move-result v4

    #@3a
    invoke-virtual {v2, v3, v4, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@3d
    .line 2423
    invoke-static {}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->access$600()Z

    #@40
    move-result v2

    #@41
    if-eqz v2, :cond_61

    #@43
    const-string v2, "CDMA"

    #@45
    new-instance v3, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v4, "setOnErrorListener, volume is restored. restoreVolume = "

    #@4c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v3

    #@50
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@52
    invoke-static {v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->access$300(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)I

    #@55
    move-result v4

    #@56
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 2424
    :cond_61
    return v6

    #@62
    .line 2417
    :catch_62
    move-exception v0

    #@63
    .line 2418
    .local v0, ex:Ljava/lang/IllegalStateException;
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@65
    new-instance v3, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v4, "MediaPlayer IllegalStateException: "

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v3

    #@74
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v3

    #@78
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@7b
    goto :goto_27
.end method
