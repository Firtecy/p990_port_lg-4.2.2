.class public Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;
.super Lcom/android/internal/telephony/ServiceStateTracker;
.source "GsmServiceStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$10;
    }
.end annotation


# static fields
.field private static final ACTION_MASTER_CLEAR_NOTIFICATION:Ljava/lang/String; = "android.intent.action.MASTER_CLEAR"

.field public static final ACTION_VOLTE_NETWORK_SIB_INFO:Ljava/lang/String; = "lge.intent.action.LTE_NETWORK_SIB_INFO"

.field static final CS_DISABLED:I = 0x3ec

.field static final CS_EMERGENCY_ENABLED:I = 0x3ee

.field static final CS_ENABLED:I = 0x3eb

.field static final CS_NORMAL_ENABLED:I = 0x3ed

.field static final CS_NOTIFICATION:I = 0x3e7

.field static final DBG:Z = true

.field static final DEFAULT_GPRS_CHECK_PERIOD_MILLIS:I = 0xea60

.field private static final DIALOG_TIMEOUT:I = 0x1d4c0

.field protected static final EVENT_DELAYED_REJECT_CAUSE_ACTIVATION:I = 0x3f4

.field private static final EVENT_REATTACH_INTENT_DELAY:I = 0x3f5

.field protected static final EVENT_SKT_FA_CHG_DONE:I = 0x3f3

.field protected static final EVENT_SKT_FA_CHG_START:I = 0x3f2

.field public static EmerCampedCID:I = 0x0

.field public static EmerCampedTAC:I = 0x0

.field private static final FOCUS_BEEP_VOLUME:I = 0x64

.field private static final INTENT_STOP_SUPPRESSING_SRV_STATE_IND:Ljava/lang/String; = "com.android.internal.telephony.STOP_SUPPRESSING_SRV_STATE_IND"

.field static final LOG_TAG:Ljava/lang/String; = "GSM"

.field static final LOG_TAG_2:Ljava/lang/String; = "[GsmServiceStateTracker]"

.field private static final MSG_ID_TIMEOUT:I = 0x1

.field public static final PDP_ACTIVED:I = 0x3ef

.field public static final PDP_NOTIFICATION:I = 0x309

.field public static final PDP_REJECTED:I = 0x3f0

.field static final PS_DISABLED:I = 0x3ea

.field static final PS_ENABLED:I = 0x3e9

.field static final PS_NOTIFICATION:I = 0x378

.field private static final SAVEDMCC:Ljava/lang/String; = "MCC"

.field private static final SHAREDPREF_SAVEDMCC:Ljava/lang/String; = "savedMCC"

.field private static final SUPPRESSING_PERIOD:I = 0x1770

.field private static final SUPP_STATE_NONE:I = 0x0

.field private static final SUPP_STATE_SHOULD_PERFORM_POLLSTATEDONE:I = 0x2

.field private static final SUPP_STATE_SUPPRESS_POLLSTATEDONE:I = 0x1

.field private static UsimPersoFinish:Z = false

.field private static final WAKELOCK_TAG:Ljava/lang/String; = "ServiceStateTracker"

.field private static bAlreadyShownRejectCause:Z

.field private static bDisplayedRejectCause:Z

.field private static isAttImeiLocked:Z

.field private static mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

.field private static prev_rej:I


# instance fields
.field protected CheckATTACH:Z

.field private SimStateReceiver:Landroid/content/BroadcastReceiver;

.field cellLoc:Landroid/telephony/gsm/GsmCellLocation;

.field private cr:Landroid/content/ContentResolver;

.field protected curPlmn:Ljava/lang/String;

.field protected curShowPlmn:Z

.field protected curShowSpn:Z

.field protected curSpn:Ljava/lang/String;

.field private cur_mcc:Ljava/lang/String;

.field private faChannel:Ljava/lang/String;

.field public failCause:I

.field private gprsState:I

.field protected isFixedSync:Z

.field private isMultiTimeZoneArea:Z

.field private isNetworkLoked:I

.field private mAutoTimeObserver:Landroid/database/ContentObserver;

.field private mAutoTimeZoneObserver:Landroid/database/ContentObserver;

.field private mCID:I

.field private final mCellInfoLte:Landroid/telephony/CellInfoLte;

.field private mDataRoaming:Z

.field mDialogOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field protected mEmergencyOnly:Z

.field private mFirstUpdateSpn:Z

.field private mGotCountryCode:Z

.field private mGsmApnObserver:Landroid/database/ContentObserver;

.field private mGsmRoaming:Z

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsLTE:Z

.field private mLAC:I

.field private mLasteCellIdentityLte:Landroid/telephony/CellIdentityLte;

.field mLgeRegStateNotification:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

.field private mMaxDataCalls:I

.field private mNeedFixZoneAfterNitz:Z

.field private mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;

.field private mNewMaxDataCalls:I

.field private mNewReasonDataDenied:I

.field private mNitzUpdatedTime:Z

.field private mNotification:Landroid/app/Notification;

.field protected mPermanentReject:Z

.field mPreferredNetworkType:I

.field private mReasonDataDenied:I

.field mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

.field private mReportedGprsNoReg:Z

.field private mResendCounter:I

.field private mResendEnable:Z

.field mSavedAtTime:J

.field mSavedTime:J

.field mSavedTimeZone:Ljava/lang/String;

.field public mSimIsInvalid:Z

.field private mStartedGprsRegCheck:Z

.field private mStopSuppressingAlarmIntent:Landroid/app/PendingIntent;

.field private mTAC:I

.field mTimeoutHandler:Landroid/os/Handler;

.field private mToneGenerator:Landroid/media/ToneGenerator;

.field private mUsimIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mZoneDst:Z

.field private mZoneOffset:I

.field private mZoneTime:J

.field private networkDialog:Landroid/app/AlertDialog;

.field newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

.field private newGPRSState:I

.field private old_mcc:Ljava/lang/String;

.field protected phone:Lcom/android/internal/telephony/gsm/GSMPhone;

.field private state_of_suppressing_not_registered_ind:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 245
    const/4 v0, 0x0

    #@2
    sput-object v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@4
    .line 253
    sput v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->prev_rej:I

    #@6
    .line 254
    sput-boolean v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bDisplayedRejectCause:Z

    #@8
    .line 255
    sput-boolean v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bAlreadyShownRejectCause:Z

    #@a
    .line 288
    sput v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->EmerCampedCID:I

    #@c
    .line 289
    sput v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->EmerCampedTAC:I

    #@e
    .line 295
    sput-boolean v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isAttImeiLocked:Z

    #@10
    .line 593
    sput-boolean v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->UsimPersoFinish:Z

    #@12
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/gsm/GSMPhone;)V
    .registers 14
    .parameter "phone"

    #@0
    .prologue
    const/4 v9, -0x1

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v11, 0x0

    #@3
    const/4 v7, 0x0

    #@4
    .line 711
    iget-object v5, p1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@6
    new-instance v8, Landroid/telephony/CellInfoGsm;

    #@8
    invoke-direct {v8}, Landroid/telephony/CellInfoGsm;-><init>()V

    #@b
    invoke-direct {p0, p1, v5, v8}, Lcom/android/internal/telephony/ServiceStateTracker;-><init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/CommandsInterface;Landroid/telephony/CellInfo;)V

    #@e
    .line 152
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isFixedSync:Z

    #@10
    .line 161
    new-instance v5, Landroid/telephony/CellInfoLte;

    #@12
    invoke-direct {v5}, Landroid/telephony/CellInfoLte;-><init>()V

    #@15
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@17
    .line 162
    new-instance v5, Landroid/telephony/CellIdentityLte;

    #@19
    invoke-direct {v5}, Landroid/telephony/CellIdentityLte;-><init>()V

    #@1c
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@1e
    .line 163
    new-instance v5, Landroid/telephony/CellIdentityLte;

    #@20
    invoke-direct {v5}, Landroid/telephony/CellIdentityLte;-><init>()V

    #@23
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLasteCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@25
    .line 166
    iput v6, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@27
    .line 167
    iput v6, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@29
    .line 168
    iput v6, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mMaxDataCalls:I

    #@2b
    .line 169
    iput v6, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewMaxDataCalls:I

    #@2d
    .line 170
    iput v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mReasonDataDenied:I

    #@2f
    .line 171
    iput v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewReasonDataDenied:I

    #@31
    .line 173
    iput v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLAC:I

    #@33
    .line 174
    iput v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTAC:I

    #@35
    .line 175
    iput v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCID:I

    #@37
    .line 176
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mIsLTE:Z

    #@39
    .line 191
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmRoaming:Z

    #@3b
    .line 197
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mDataRoaming:Z

    #@3d
    .line 202
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@3f
    .line 209
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@41
    .line 213
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGotCountryCode:Z

    #@43
    .line 217
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNitzUpdatedTime:Z

    #@45
    .line 224
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStartedGprsRegCheck:Z

    #@47
    .line 227
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mReportedGprsNoReg:Z

    #@49
    .line 239
    iput-object v11, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curSpn:Ljava/lang/String;

    #@4b
    .line 240
    iput-object v11, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@4d
    .line 241
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowPlmn:Z

    #@4f
    .line 242
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowSpn:Z

    #@51
    .line 246
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isMultiTimeZoneArea:Z

    #@53
    .line 260
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mPermanentReject:Z

    #@55
    .line 303
    iput-object v11, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLgeRegStateNotification:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@57
    .line 307
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->CheckATTACH:Z

    #@59
    .line 315
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mFirstUpdateSpn:Z

    #@5b
    .line 322
    const-string v5, ""

    #@5d
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->old_mcc:Ljava/lang/String;

    #@5f
    .line 323
    const-string v5, ""

    #@61
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cur_mcc:Ljava/lang/String;

    #@63
    .line 328
    iput v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isNetworkLoked:I

    #@65
    .line 332
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSimIsInvalid:Z

    #@67
    .line 338
    iput-object v11, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStopSuppressingAlarmIntent:Landroid/app/PendingIntent;

    #@69
    .line 348
    iput v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->state_of_suppressing_not_registered_ind:I

    #@6b
    .line 351
    iput v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mResendCounter:I

    #@6d
    .line 352
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mResendEnable:Z

    #@6f
    .line 358
    const-string v5, ""

    #@71
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->faChannel:Ljava/lang/String;

    #@73
    .line 379
    new-instance v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;

    #@75
    invoke-direct {v5, p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$1;-><init>(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V

    #@78
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@7a
    .line 564
    new-instance v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$2;

    #@7c
    new-instance v8, Landroid/os/Handler;

    #@7e
    invoke-direct {v8}, Landroid/os/Handler;-><init>()V

    #@81
    invoke-direct {v5, p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$2;-><init>(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;Landroid/os/Handler;)V

    #@84
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmApnObserver:Landroid/database/ContentObserver;

    #@86
    .line 574
    new-instance v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$3;

    #@88
    new-instance v8, Landroid/os/Handler;

    #@8a
    invoke-direct {v8}, Landroid/os/Handler;-><init>()V

    #@8d
    invoke-direct {v5, p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$3;-><init>(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;Landroid/os/Handler;)V

    #@90
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mAutoTimeObserver:Landroid/database/ContentObserver;

    #@92
    .line 583
    new-instance v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$4;

    #@94
    new-instance v8, Landroid/os/Handler;

    #@96
    invoke-direct {v8}, Landroid/os/Handler;-><init>()V

    #@99
    invoke-direct {v5, p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$4;-><init>(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;Landroid/os/Handler;)V

    #@9c
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mAutoTimeZoneObserver:Landroid/database/ContentObserver;

    #@9e
    .line 595
    new-instance v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$5;

    #@a0
    invoke-direct {v5, p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$5;-><init>(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V

    #@a3
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mUsimIntentReceiver:Landroid/content/BroadcastReceiver;

    #@a5
    .line 632
    new-instance v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$6;

    #@a7
    invoke-direct {v5, p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$6;-><init>(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V

    #@aa
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@ac
    .line 5157
    new-instance v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$7;

    #@ae
    invoke-direct {v5, p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$7;-><init>(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V

    #@b1
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeoutHandler:Landroid/os/Handler;

    #@b3
    .line 5193
    new-instance v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$8;

    #@b5
    invoke-direct {v5, p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$8;-><init>(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V

    #@b8
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mDialogOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    #@ba
    .line 713
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@bc
    .line 714
    new-instance v5, Landroid/telephony/gsm/GsmCellLocation;

    #@be
    invoke-direct {v5}, Landroid/telephony/gsm/GsmCellLocation;-><init>()V

    #@c1
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@c3
    .line 715
    new-instance v5, Landroid/telephony/gsm/GsmCellLocation;

    #@c5
    invoke-direct {v5}, Landroid/telephony/gsm/GsmCellLocation;-><init>()V

    #@c8
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@ca
    .line 716
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@cc
    new-instance v8, Landroid/telephony/CellSignalStrengthLte;

    #@ce
    invoke-direct {v8}, Landroid/telephony/CellSignalStrengthLte;-><init>()V

    #@d1
    invoke-virtual {v5, v8}, Landroid/telephony/CellInfoLte;->setCellSignalStrength(Landroid/telephony/CellSignalStrengthLte;)V

    #@d4
    .line 717
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@d6
    new-instance v8, Landroid/telephony/CellIdentityLte;

    #@d8
    invoke-direct {v8}, Landroid/telephony/CellIdentityLte;-><init>()V

    #@db
    invoke-virtual {v5, v8}, Landroid/telephony/CellInfoLte;->setCellIdentity(Landroid/telephony/CellIdentityLte;)V

    #@de
    .line 719
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@e1
    move-result-object v5

    #@e2
    const-string v8, "power"

    #@e4
    invoke-virtual {v5, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e7
    move-result-object v4

    #@e8
    check-cast v4, Landroid/os/PowerManager;

    #@ea
    .line 721
    .local v4, powerManager:Landroid/os/PowerManager;
    const-string v5, "ServiceStateTracker"

    #@ec
    invoke-virtual {v4, v6, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@ef
    move-result-object v5

    #@f0
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@f2
    .line 723
    iget-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@f4
    const/16 v8, 0xd

    #@f6
    invoke-interface {v5, p0, v8, v11}, Lcom/android/internal/telephony/CommandsInterface;->registerForAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@f9
    .line 724
    iget-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@fb
    invoke-interface {v5, p0, v6, v11}, Lcom/android/internal/telephony/CommandsInterface;->registerForRadioStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@fe
    .line 726
    iget-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@100
    const/4 v8, 0x2

    #@101
    invoke-interface {v5, p0, v8, v11}, Lcom/android/internal/telephony/CommandsInterface;->registerForVoiceNetworkStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@104
    .line 727
    iget-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@106
    const/16 v8, 0xb

    #@108
    invoke-interface {v5, p0, v8, v11}, Lcom/android/internal/telephony/CommandsInterface;->setOnNITZTime(Landroid/os/Handler;ILjava/lang/Object;)V

    #@10b
    .line 728
    iget-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@10d
    const/16 v8, 0x17

    #@10f
    invoke-interface {v5, p0, v8, v11}, Lcom/android/internal/telephony/CommandsInterface;->setOnRestrictedStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@112
    .line 731
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@115
    move-result-object v5

    #@116
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@119
    move-result-object v5

    #@11a
    const-string v8, "airplane_mode_on"

    #@11c
    invoke-static {v5, v8, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11f
    move-result v0

    #@120
    .line 734
    .local v0, airplaneMode:I
    if-gtz v0, :cond_321

    #@122
    move v5, v6

    #@123
    :goto_123
    iput-boolean v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@125
    .line 736
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@128
    move-result-object v5

    #@129
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12c
    move-result-object v5

    #@12d
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@12f
    .line 737
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@131
    const-string v8, "auto_time"

    #@133
    invoke-static {v8}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@136
    move-result-object v8

    #@137
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mAutoTimeObserver:Landroid/database/ContentObserver;

    #@139
    invoke-virtual {v5, v8, v6, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@13c
    .line 740
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@13e
    const-string v8, "auto_time_zone"

    #@140
    invoke-static {v8}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@143
    move-result-object v8

    #@144
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mAutoTimeZoneObserver:Landroid/database/ContentObserver;

    #@146
    invoke-virtual {v5, v8, v6, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@149
    .line 745
    iget-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@14b
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@14e
    move-result-object v5

    #@14f
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_BLOCK_TETHERINGPDN_ON_GSMROAMING_KDDI:Z

    #@151
    if-ne v5, v6, :cond_15a

    #@153
    .line 747
    const-string v5, "GsmServiceStateTracker Constructors: KDDI isFixedSync is set to False"

    #@155
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@158
    .line 748
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isFixedSync:Z

    #@15a
    .line 752
    :cond_15a
    const-string v5, "VZW"

    #@15c
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@15f
    move-result v5

    #@160
    if-eqz v5, :cond_16b

    #@162
    .line 753
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@164
    sget-object v8, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@166
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmApnObserver:Landroid/database/ContentObserver;

    #@168
    invoke-virtual {v5, v8, v6, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@16b
    .line 759
    :cond_16b
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setSignalStrengthDefaultValues()V

    #@16e
    .line 762
    new-instance v2, Landroid/content/IntentFilter;

    #@170
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    #@173
    .line 763
    .local v2, filter:Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.LOCALE_CHANGED"

    #@175
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@178
    .line 765
    const-string v5, "lge.intent.action.LTE_NETWORK_SIB_INFO"

    #@17a
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17d
    .line 770
    const-string v5, "android.intent.action.MASTER_CLEAR"

    #@17f
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@182
    .line 775
    const-string v5, "com.android.internal.telephony.STOP_SUPPRESSING_SRV_STATE_IND"

    #@184
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@187
    .line 777
    const-string v5, "KR"

    #@189
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@18c
    move-result v5

    #@18d
    if-eqz v5, :cond_1b7

    #@18f
    .line 779
    const-string v5, "KR"

    #@191
    const-string v8, "SKT"

    #@193
    invoke-static {v5, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@196
    move-result v5

    #@197
    if-nez v5, :cond_1a3

    #@199
    const-string v5, "KR"

    #@19b
    const-string v8, "KT"

    #@19d
    invoke-static {v5, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1a0
    move-result v5

    #@1a1
    if-eqz v5, :cond_1a8

    #@1a3
    .line 780
    :cond_1a3
    const-string v5, "android.intent.action.LG_NVITEM_RESET"

    #@1a5
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1a8
    .line 785
    :cond_1a8
    const-string v5, "KR"

    #@1aa
    const-string v8, "SKT"

    #@1ac
    invoke-static {v5, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1af
    move-result v5

    #@1b0
    if-eqz v5, :cond_1b7

    #@1b2
    .line 786
    const-string v5, "android.intent.action.FA_CHANGE"

    #@1b4
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1b7
    .line 792
    :cond_1b7
    const-string v5, "android.intent.action.AIRPLANE_MODE"

    #@1b9
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1bc
    .line 796
    const-string v5, "android.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@1be
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c1
    .line 797
    const-string v5, "com.lge.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@1c3
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c6
    .line 798
    const-string v5, "com.lge.mms.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@1c8
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1cb
    .line 805
    const-string v5, "android.intent.action.SIM_STATE_CHANGED"

    #@1cd
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d0
    .line 810
    const-string v5, "KR"

    #@1d2
    const-string v8, "LGU"

    #@1d4
    invoke-static {v5, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1d7
    move-result v5

    #@1d8
    if-eqz v5, :cond_1df

    #@1da
    .line 811
    const-string v5, "android.intent.action.LTE_EMM_REJECT"

    #@1dc
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1df
    .line 816
    :cond_1df
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@1e2
    move-result-object v5

    #@1e3
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1e5
    invoke-virtual {v5, v8, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1e8
    .line 819
    const/4 v5, 0x3

    #@1e9
    invoke-virtual {p1, v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyOtaspChanged(I)V

    #@1ec
    .line 823
    iget-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1ee
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1f1
    move-result-object v5

    #@1f2
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@1f4
    iput-boolean v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->poweroffdelayneed:Z

    #@1f6
    .line 825
    iget-boolean v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->poweroffdelayneed:Z

    #@1f8
    if-ne v5, v6, :cond_211

    #@1fa
    .line 826
    new-instance v2, Landroid/content/IntentFilter;

    #@1fc
    .end local v2           #filter:Landroid/content/IntentFilter;
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    #@1ff
    .line 827
    .restart local v2       #filter:Landroid/content/IntentFilter;
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@202
    move-result-object v5

    #@203
    iput-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->context:Landroid/content/Context;

    #@205
    .line 828
    const-string v5, "android.intent.action.ACTION_RADIO_OFF"

    #@207
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@20a
    .line 829
    iget-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->context:Landroid/content/Context;

    #@20c
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@20e
    invoke-virtual {v5, v8, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@211
    .line 834
    :cond_211
    sget-object v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@213
    if-nez v5, :cond_21b

    #@215
    .line 835
    invoke-static {}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->getDefault()Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@218
    move-result-object v5

    #@219
    sput-object v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@21b
    .line 838
    :cond_21b
    sget-object v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@21d
    if-eqz v5, :cond_229

    #@21f
    .line 839
    sget-object v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@221
    invoke-virtual {p0, v5, v6, v11}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->registerForNetworkAttached(Landroid/os/Handler;ILjava/lang/Object;)V

    #@224
    .line 840
    sget-object v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@226
    invoke-virtual {v5, p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->setServiceStateTracker(Lcom/android/internal/telephony/ServiceStateTracker;)V

    #@229
    .line 846
    :cond_229
    invoke-static {}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getSavedNeedFixZone()Z

    #@22c
    move-result v5

    #@22d
    iput-boolean v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@22f
    .line 847
    iget-boolean v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@231
    if-eqz v5, :cond_258

    #@233
    .line 848
    invoke-static {}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getSavedZoneOffset()I

    #@236
    move-result v5

    #@237
    iput v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@239
    .line 849
    invoke-static {}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getSavedZoneDst()Z

    #@23c
    move-result v5

    #@23d
    iput-boolean v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@23f
    .line 850
    invoke-static {}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getSavedZoneTime()J

    #@242
    move-result-wide v8

    #@243
    iput-wide v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneTime:J

    #@245
    .line 851
    const-string v5, "TimeZone correction was delayed by Phone Switching!"

    #@247
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@24a
    .line 854
    invoke-static {v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setSavedNeedFixZone(Z)V

    #@24d
    .line 855
    invoke-static {v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setSavedZoneOffset(I)V

    #@250
    .line 856
    invoke-static {v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setSavedZoneDst(Z)V

    #@253
    .line 857
    const-wide/16 v8, 0x0

    #@255
    invoke-static {v8, v9}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setSavedZoneTime(J)V

    #@258
    .line 862
    :cond_258
    const-string v5, "USIM_PERSONAL_LOCK"

    #@25a
    invoke-static {v11, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@25d
    move-result v5

    #@25e
    if-eqz v5, :cond_280

    #@260
    .line 863
    new-instance v3, Landroid/content/IntentFilter;

    #@262
    const-string v5, "android.intent.action.SIM_UNLOCKED"

    #@264
    invoke-direct {v3, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@267
    .line 864
    .local v3, mUsimIntentFilter:Landroid/content/IntentFilter;
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@26a
    move-result-object v5

    #@26b
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mUsimIntentReceiver:Landroid/content/BroadcastReceiver;

    #@26d
    invoke-virtual {v5, v8, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@270
    .line 865
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@273
    move-result-object v5

    #@274
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@276
    new-instance v9, Landroid/content/IntentFilter;

    #@278
    const-string v10, "android.intent.action.SIM_STATE_CHANGED"

    #@27a
    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@27d
    invoke-virtual {v5, v8, v9, v11, v11}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@280
    .line 870
    .end local v3           #mUsimIntentFilter:Landroid/content/IntentFilter;
    :cond_280
    const-string v5, "KR_REJECT_CAUSE"

    #@282
    invoke-static {v11, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@285
    move-result v5

    #@286
    if-eqz v5, :cond_2a5

    #@288
    .line 873
    iget-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@28a
    const/16 v8, 0x30

    #@28c
    invoke-interface {v5, p0, v8, v11}, Lcom/android/internal/telephony/CommandsInterface;->registerForWcdmaRejectReceived(Landroid/os/Handler;ILjava/lang/Object;)V

    #@28f
    .line 874
    iget-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@291
    const/16 v8, 0x31

    #@293
    invoke-interface {v5, p0, v8, v11}, Lcom/android/internal/telephony/CommandsInterface;->registerForWcdmaAcceptReceived(Landroid/os/Handler;ILjava/lang/Object;)V

    #@296
    .line 876
    invoke-static {p1}, Lcom/android/internal/telephony/gsm/RejectCauseFactory;->getDefaultRejectCause(Lcom/android/internal/telephony/gsm/GSMPhone;)Lcom/android/internal/telephony/gsm/RejectCause;

    #@299
    move-result-object v5

    #@29a
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@29c
    .line 877
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@29e
    if-eqz v5, :cond_2a5

    #@2a0
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@2a2
    invoke-interface {v5}, Lcom/android/internal/telephony/gsm/RejectCause;->initialize()V

    #@2a5
    .line 882
    :cond_2a5
    const-string v5, "KR"

    #@2a7
    const-string v8, "LGU"

    #@2a9
    invoke-static {v5, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@2ac
    move-result v5

    #@2ad
    if-nez v5, :cond_2b9

    #@2af
    const-string v5, "KR"

    #@2b1
    const-string v8, "SKT"

    #@2b3
    invoke-static {v5, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@2b6
    move-result v5

    #@2b7
    if-eqz v5, :cond_2bf

    #@2b9
    .line 883
    :cond_2b9
    invoke-static {p1}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->getInstance(Lcom/android/internal/telephony/PhoneBase;)Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@2bc
    move-result-object v5

    #@2bd
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLgeRegStateNotification:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@2bf
    .line 888
    :cond_2bf
    const-string v5, "VZW"

    #@2c1
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@2c4
    move-result v5

    #@2c5
    if-eqz v5, :cond_320

    #@2c7
    .line 889
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@2c9
    const-string v8, "apn2_disable"

    #@2cb
    invoke-static {v5, v8, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2ce
    move-result v1

    #@2cf
    .line 890
    .local v1, apn2_disable_Mode:I
    iget-boolean v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@2d1
    if-eqz v5, :cond_324

    #@2d3
    if-gtz v1, :cond_324

    #@2d5
    :goto_2d5
    iput-boolean v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@2d7
    .line 891
    const-string v5, "GSM"

    #@2d9
    new-instance v6, Ljava/lang/StringBuilder;

    #@2db
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2de
    const-string v7, "apn2_disable_Mode: "

    #@2e0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e3
    move-result-object v6

    #@2e4
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e7
    move-result-object v6

    #@2e8
    const-string v7, " ,airplaneMode: "

    #@2ea
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ed
    move-result-object v6

    #@2ee
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f1
    move-result-object v6

    #@2f2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f5
    move-result-object v6

    #@2f6
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2f9
    .line 892
    if-lez v1, :cond_326

    #@2fb
    .line 893
    const-string v5, "ril.current.apn2-disable"

    #@2fd
    const-string v6, "1"

    #@2ff
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@302
    .line 894
    const-string v5, "GSM"

    #@304
    new-instance v6, Ljava/lang/StringBuilder;

    #@306
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@309
    const-string v7, "[APN2 Disable] ril.current.apn2-disable : "

    #@30b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30e
    move-result-object v6

    #@30f
    const-string v7, "ril.current.apn2-disable"

    #@311
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@314
    move-result-object v7

    #@315
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@318
    move-result-object v6

    #@319
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31c
    move-result-object v6

    #@31d
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@320
    .line 901
    .end local v1           #apn2_disable_Mode:I
    :cond_320
    :goto_320
    return-void

    #@321
    .end local v2           #filter:Landroid/content/IntentFilter;
    :cond_321
    move v5, v7

    #@322
    .line 734
    goto/16 :goto_123

    #@324
    .restart local v1       #apn2_disable_Mode:I
    .restart local v2       #filter:Landroid/content/IntentFilter;
    :cond_324
    move v6, v7

    #@325
    .line 890
    goto :goto_2d5

    #@326
    .line 896
    :cond_326
    const-string v5, "ril.current.apn2-disable"

    #@328
    const-string v6, "0"

    #@32a
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@32d
    .line 897
    const-string v5, "GSM"

    #@32f
    new-instance v6, Ljava/lang/StringBuilder;

    #@331
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@334
    const-string v7, "[APN2 Disable] ril.current.apn2-disable : "

    #@336
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@339
    move-result-object v6

    #@33a
    const-string v7, "ril.current.apn2-disable"

    #@33c
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@33f
    move-result-object v7

    #@340
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@343
    move-result-object v6

    #@344
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@347
    move-result-object v6

    #@348
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@34b
    goto :goto_320
.end method

.method private ChangeNWname(Ljava/lang/String;)Ljava/lang/String;
    .registers 15
    .parameter "name"

    #@0
    .prologue
    .line 5442
    const/4 v10, 0x0

    #@1
    .local v10, sim_imsi:Ljava/lang/String;
    const/4 v1, 0x0

    #@2
    .local v1, mcc:Ljava/lang/String;
    const/4 v3, 0x0

    #@3
    .line 5444
    .local v3, mnc:Ljava/lang/String;
    const/4 v5, 0x0

    #@4
    .local v5, oper_imsi:Ljava/lang/String;
    const/4 v6, 0x0

    #@5
    .local v6, oper_mcc:Ljava/lang/String;
    const/4 v7, 0x0

    #@6
    .line 5445
    .local v7, oper_mnc:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 5446
    .local v0, iccRecords:Lcom/android/internal/telephony/uicc/IccRecords;
    const/4 v2, 0x0

    #@9
    .local v2, mcc_sim:I
    const/4 v4, 0x0

    #@a
    .line 5447
    .local v4, mnc_sim:I
    const/4 v8, 0x0

    #@b
    .local v8, serv_mcc:I
    const/4 v9, 0x0

    #@c
    .line 5448
    .local v9, serv_mnc:I
    if-eqz v0, :cond_12

    #@e
    .line 5449
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getOperatorNumeric()Ljava/lang/String;

    #@11
    move-result-object v10

    #@12
    .line 5451
    :cond_12
    iget-object v11, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@14
    invoke-virtual {v11}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@17
    move-result-object v5

    #@18
    .line 5453
    const-string v11, "Enter into ChangeNWname "

    #@1a
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1d
    .line 5454
    if-nez v10, :cond_22

    #@1f
    if-nez v5, :cond_22

    #@21
    .line 5565
    .end local p1
    :cond_21
    :goto_21
    return-object p1

    #@22
    .line 5458
    .restart local p1
    :cond_22
    if-eqz v10, :cond_5e

    #@24
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@27
    move-result v11

    #@28
    const/4 v12, 0x4

    #@29
    if-le v11, v12, :cond_5e

    #@2b
    .line 5460
    const/4 v11, 0x0

    #@2c
    const/4 v12, 0x3

    #@2d
    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    .line 5461
    const/4 v11, 0x3

    #@32
    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    .line 5462
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@39
    move-result v2

    #@3a
    .line 5463
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3d
    move-result v4

    #@3e
    .line 5464
    new-instance v11, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v12, "[GSMTracker]SIM mcc: "

    #@45
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v11

    #@49
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v11

    #@4d
    const-string v12, "mnc: "

    #@4f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v11

    #@53
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v11

    #@57
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v11

    #@5b
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@5e
    .line 5467
    :cond_5e
    if-eqz v5, :cond_9a

    #@60
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@63
    move-result v11

    #@64
    const/4 v12, 0x4

    #@65
    if-le v11, v12, :cond_9a

    #@67
    .line 5469
    const/4 v11, 0x0

    #@68
    const/4 v12, 0x3

    #@69
    invoke-virtual {v5, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6c
    move-result-object v6

    #@6d
    .line 5470
    const/4 v11, 0x3

    #@6e
    invoke-virtual {v5, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@71
    move-result-object v7

    #@72
    .line 5471
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@75
    move-result v8

    #@76
    .line 5472
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@79
    move-result v9

    #@7a
    .line 5473
    new-instance v11, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v12, "[GSMTracker]Serving Cell mcc: "

    #@81
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v11

    #@85
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@88
    move-result-object v11

    #@89
    const-string v12, "serv_mnc: "

    #@8b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v11

    #@8f
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@92
    move-result-object v11

    #@93
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v11

    #@97
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@9a
    .line 5475
    :cond_9a
    new-instance v11, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v12, "sim_imsi: "

    #@a1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v11

    #@a5
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v11

    #@a9
    const-string v12, "mcc_sim: "

    #@ab
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v11

    #@af
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v11

    #@b3
    const-string v12, "mnc_sim: "

    #@b5
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v11

    #@b9
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v11

    #@bd
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v11

    #@c1
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@c4
    .line 5477
    if-lez v2, :cond_21

    #@c6
    .line 5479
    sparse-switch v2, :sswitch_data_1ce

    #@c9
    goto/16 :goto_21

    #@cb
    .line 5545
    :sswitch_cb
    const-string v11, "VODAFONE TR"

    #@cd
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@d0
    move-result v11

    #@d1
    if-eqz v11, :cond_21

    #@d3
    .line 5546
    const-string p1, "Vodafone TR"

    #@d5
    goto/16 :goto_21

    #@d7
    .line 5482
    :sswitch_d7
    const/16 v11, 0xf

    #@d9
    if-ne v4, v11, :cond_e7

    #@db
    const-string v11, "TOT Mobile"

    #@dd
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@e0
    move-result v11

    #@e1
    if-eqz v11, :cond_e7

    #@e3
    .line 5483
    const-string p1, "TOT 3G"

    #@e5
    goto/16 :goto_21

    #@e7
    .line 5485
    :cond_e7
    const/16 v11, 0x17

    #@e9
    if-ne v4, v11, :cond_ff

    #@eb
    const-string v11, "TH GSM"

    #@ed
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@f0
    move-result v11

    #@f1
    if-eqz v11, :cond_ff

    #@f3
    iget-object v11, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@f5
    invoke-virtual {v11}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@f8
    move-result v11

    #@f9
    if-nez v11, :cond_ff

    #@fb
    .line 5486
    const-string p1, "GSM 1800"

    #@fd
    goto/16 :goto_21

    #@ff
    .line 5488
    :cond_ff
    const-string v11, "True"

    #@101
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@104
    move-result v11

    #@105
    if-eqz v11, :cond_10b

    #@107
    .line 5490
    const-string p1, "TRUE"

    #@109
    goto/16 :goto_21

    #@10b
    .line 5492
    :cond_10b
    const-string v11, "CAT 3G"

    #@10d
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@110
    move-result v11

    #@111
    if-eqz v11, :cond_117

    #@113
    .line 5494
    const-string p1, "my"

    #@115
    goto/16 :goto_21

    #@117
    .line 5496
    :cond_117
    const-string v11, "DTAC"

    #@119
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@11c
    move-result v11

    #@11d
    if-eqz v11, :cond_21

    #@11f
    .line 5498
    const-string p1, "dtac"

    #@121
    goto/16 :goto_21

    #@123
    .line 5502
    :sswitch_123
    const/16 v11, 0x1fe

    #@125
    if-ne v8, v11, :cond_132

    #@127
    const/4 v11, 0x1

    #@128
    if-ne v9, v11, :cond_132

    #@12a
    const/16 v11, 0x15

    #@12c
    if-ne v4, v11, :cond_132

    #@12e
    .line 5504
    const-string p1, "INDOSAT"

    #@130
    goto/16 :goto_21

    #@132
    .line 5506
    :cond_132
    const-string v11, "im3"

    #@134
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@137
    move-result v11

    #@138
    if-nez v11, :cond_14a

    #@13a
    const-string v11, "matrix"

    #@13c
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@13f
    move-result v11

    #@140
    if-nez v11, :cond_14a

    #@142
    const-string v11, "mentari"

    #@144
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@147
    move-result v11

    #@148
    if-eqz v11, :cond_14e

    #@14a
    .line 5508
    :cond_14a
    const-string p1, "INDOSAT"

    #@14c
    goto/16 :goto_21

    #@14e
    .line 5511
    :cond_14e
    const-string v11, "TELKOMSE"

    #@150
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@153
    move-result v11

    #@154
    if-nez v11, :cond_15e

    #@156
    const-string v11, "T-SEL"

    #@158
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@15b
    move-result v11

    #@15c
    if-eqz v11, :cond_21

    #@15e
    .line 5513
    :cond_15e
    const-string p1, "TELKOMSEL"

    #@160
    goto/16 :goto_21

    #@162
    .line 5518
    :sswitch_162
    const-string v11, "HUTCH"

    #@164
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@167
    move-result v11

    #@168
    if-nez v11, :cond_172

    #@16a
    const-string v11, "Vodafone"

    #@16c
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@16f
    move-result v11

    #@170
    if-eqz v11, :cond_176

    #@172
    .line 5519
    :cond_172
    const-string p1, "Vodafone IN"

    #@174
    goto/16 :goto_21

    #@176
    .line 5521
    :cond_176
    const-string v11, "BPL Mobile"

    #@178
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@17b
    move-result v11

    #@17c
    if-eqz v11, :cond_182

    #@17e
    .line 5523
    const-string p1, "Loop Mobile"

    #@180
    goto/16 :goto_21

    #@182
    .line 5525
    :cond_182
    const-string v11, "Airtel"

    #@184
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@187
    move-result v11

    #@188
    if-nez v11, :cond_192

    #@18a
    const-string v11, "IND airtel"

    #@18c
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@18f
    move-result v11

    #@190
    if-eqz v11, :cond_196

    #@192
    .line 5527
    :cond_192
    const-string p1, "airtel"

    #@194
    goto/16 :goto_21

    #@196
    .line 5529
    :cond_196
    const-string v11, "UNINOR"

    #@198
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@19b
    move-result v11

    #@19c
    if-eqz v11, :cond_21

    #@19e
    .line 5531
    const-string p1, "Uninor"

    #@1a0
    goto/16 :goto_21

    #@1a2
    .line 5535
    :sswitch_1a2
    const-string v11, "STARHUB"

    #@1a4
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1a7
    move-result v11

    #@1a8
    if-eqz v11, :cond_21

    #@1aa
    .line 5536
    const-string p1, "StarHub"

    #@1ac
    goto/16 :goto_21

    #@1ae
    .line 5540
    :sswitch_1ae
    const-string v11, "GLOBE"

    #@1b0
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b3
    move-result v11

    #@1b4
    if-eqz v11, :cond_21

    #@1b6
    .line 5541
    const-string p1, "Globe"

    #@1b8
    goto/16 :goto_21

    #@1ba
    .line 5550
    :sswitch_1ba
    const-string v11, "IAM"

    #@1bc
    invoke-virtual {p1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1bf
    move-result v11

    #@1c0
    if-eqz v11, :cond_21

    #@1c2
    .line 5551
    const-string p1, "Maroc Telecom"

    #@1c4
    goto/16 :goto_21

    #@1c6
    .line 5556
    :sswitch_1c6
    const/4 v11, 0x2

    #@1c7
    if-ne v4, v11, :cond_21

    #@1c9
    .line 5557
    const-string p1, "TelkomSA"

    #@1cb
    goto/16 :goto_21

    #@1cd
    .line 5479
    nop

    #@1ce
    :sswitch_data_1ce
    .sparse-switch
        0x11e -> :sswitch_cb
        0x194 -> :sswitch_162
        0x195 -> :sswitch_162
        0x1fe -> :sswitch_123
        0x203 -> :sswitch_1ae
        0x208 -> :sswitch_d7
        0x20d -> :sswitch_1a2
        0x25c -> :sswitch_1ba
        0x28f -> :sswitch_1c6
    .end sparse-switch
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->faChannel:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$002(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 147
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->faChannel:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)Lcom/android/internal/telephony/CommandsInterface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 147
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->revertToNitzTime()V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 147
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->revertToNitzTimeZone()V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)Lcom/android/internal/telephony/CommandsInterface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    return-object v0
.end method

.method static synthetic access$1700()Z
    .registers 1

    #@0
    .prologue
    .line 147
    sget-boolean v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->UsimPersoFinish:Z

    #@2
    return v0
.end method

.method static synthetic access$1702(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 147
    sput-boolean p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->UsimPersoFinish:Z

    #@2
    return p0
.end method

.method static synthetic access$1800(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->delayedRejectPopupActivation(Z)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 147
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->checkUsimPersoLockAndFinisehdAndRestoreSavedNetworkSelectonIfBothTrue()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)Lcom/android/internal/telephony/CommandsInterface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->networkDialog:Landroid/app/AlertDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$2002(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 147
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->networkDialog:Landroid/app/AlertDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$2100(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 147
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->postDismissDialog()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)Lcom/android/internal/telephony/CommandsInterface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 147
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStopSuppressingAlarmIntent:Landroid/app/PendingIntent;

    #@2
    return-object p1
.end method

.method static synthetic access$502(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 147
    iput p1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->state_of_suppressing_not_registered_ind:I

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 147
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->pollState()V

    #@3
    return-void
.end method

.method static synthetic access$702(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 147
    sput-boolean p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isAttImeiLocked:Z

    #@2
    return p0
.end method

.method static synthetic access$802(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 147
    iput p1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isNetworkLoked:I

    #@2
    return p1
.end method

.method static synthetic access$902(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 147
    iput-boolean p1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->alarmSwitch:Z

    #@2
    return p1
.end method

.method private cancelStopSuppressingAlarm()V
    .registers 4

    #@0
    .prologue
    .line 371
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStopSuppressingAlarmIntent:Landroid/app/PendingIntent;

    #@2
    if-eqz v1, :cond_1a

    #@4
    .line 372
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@9
    move-result-object v1

    #@a
    const-string v2, "alarm"

    #@c
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/app/AlarmManager;

    #@12
    .line 373
    .local v0, alarm:Landroid/app/AlarmManager;
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStopSuppressingAlarmIntent:Landroid/app/PendingIntent;

    #@14
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@17
    .line 374
    const/4 v1, 0x0

    #@18
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStopSuppressingAlarmIntent:Landroid/app/PendingIntent;

    #@1a
    .line 376
    .end local v0           #alarm:Landroid/app/AlarmManager;
    :cond_1a
    return-void
.end method

.method private checkInternationalRoamingForATT(Z)Z
    .registers 12
    .parameter "roaming"

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    const/4 v7, 0x0

    #@2
    .line 4436
    move v0, p1

    #@3
    .line 4438
    .local v0, isroaming:Z
    iget-object v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@5
    invoke-virtual {v6}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 4439
    .local v1, operatorNumeric:Ljava/lang/String;
    const-string v3, "ffffff"

    #@b
    .line 4440
    .local v3, simNumeric:Ljava/lang/String;
    const-string v4, "ffffff"

    #@d
    .line 4442
    .local v4, temp_simNumeric:Ljava/lang/String;
    if-eqz v3, :cond_27

    #@f
    const-string v6, "ff"

    #@11
    invoke-virtual {v3, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@14
    move-result-object v8

    #@15
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v6

    #@19
    if-nez v6, :cond_27

    #@1b
    const-string v6, "FF"

    #@1d
    invoke-virtual {v3, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@20
    move-result-object v8

    #@21
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v6

    #@25
    if-eqz v6, :cond_67

    #@27
    .line 4451
    :cond_27
    if-eqz v4, :cond_47

    #@29
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2c
    move-result v6

    #@2d
    if-nez v6, :cond_47

    #@2f
    const-string v6, "ff"

    #@31
    invoke-virtual {v4, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@34
    move-result-object v8

    #@35
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v6

    #@39
    if-nez v6, :cond_47

    #@3b
    const-string v6, "FF"

    #@3d
    invoke-virtual {v4, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v6

    #@45
    if-eqz v6, :cond_c5

    #@47
    .line 4454
    :cond_47
    iget-object v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@49
    check-cast v6, Lcom/android/internal/telephony/uicc/SIMRecords;

    #@4b
    invoke-virtual {v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->getOperatorNumeric()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    .line 4455
    const-string v6, "[GsmServiceStateTracker]"

    #@51
    new-instance v8, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v9, "enter HPLMN: "

    #@58
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v8

    #@5c
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v8

    #@60
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v8

    #@64
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 4464
    :cond_67
    :goto_67
    const/4 v2, 0x0

    #@68
    .local v2, serving_cell:Z
    const/4 v5, 0x0

    #@69
    .line 4466
    .local v5, uicc_hplmn:Z
    :try_start_69
    const-string v6, "[GsmServiceStateTracker]"

    #@6b
    new-instance v8, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v9, "regCodeIsRoaming for ATT. operatorNumeric: "

    #@72
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v8

    #@76
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v8

    #@7a
    const-string v9, " simNumeric:"

    #@7c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v8

    #@80
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v8

    #@84
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v8

    #@88
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 4467
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isInternationalRoaming(Ljava/lang/String;)Z

    #@8e
    move-result v2

    #@8f
    .line 4468
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isInternationalRoaming(Ljava/lang/String;)Z
    :try_end_92
    .catch Ljava/lang/Exception; {:try_start_69 .. :try_end_92} :catch_e1

    #@92
    move-result v5

    #@93
    .line 4474
    :goto_93
    const-string v6, "[GsmServiceStateTracker]"

    #@95
    new-instance v8, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v9, "regCodeIsRoaming for ATT. roaming: "

    #@9c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v8

    #@a0
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v8

    #@a4
    const-string v9, " serving_cell:"

    #@a6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v8

    #@aa
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v8

    #@ae
    const-string v9, " uicc_hplmn: "

    #@b0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v8

    #@b4
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v8

    #@b8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v8

    #@bc
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    .line 4475
    if-nez v2, :cond_c3

    #@c1
    if-eqz v5, :cond_df

    #@c3
    :cond_c3
    const/4 v0, 0x1

    #@c4
    .line 4477
    :goto_c4
    return v0

    #@c5
    .line 4459
    .end local v2           #serving_cell:Z
    .end local v5           #uicc_hplmn:Z
    :cond_c5
    move-object v3, v4

    #@c6
    .line 4460
    const-string v6, "[GsmServiceStateTracker]"

    #@c8
    new-instance v8, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v9, "enter ACTHPLMN: "

    #@cf
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v8

    #@d3
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v8

    #@d7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v8

    #@db
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@de
    goto :goto_67

    #@df
    .restart local v2       #serving_cell:Z
    .restart local v5       #uicc_hplmn:Z
    :cond_df
    move v0, v7

    #@e0
    .line 4475
    goto :goto_c4

    #@e1
    .line 4470
    :catch_e1
    move-exception v6

    #@e2
    goto :goto_93
.end method

.method private checkUsimPersoLockAndFinisehdAndRestoreSavedNetworkSelectonIfBothTrue()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 651
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@6
    move-result-object v0

    #@7
    .line 652
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Lcom/android/internal/telephony/TelephonyUtils;->isUsimPersoLocked(Landroid/content/Context;)Z

    #@a
    move-result v1

    #@b
    .line 653
    .local v1, locked:Z
    const/4 v3, 0x0

    #@c
    .line 655
    .local v3, radioDetached:Z
    new-instance v5, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v6, "USIM_PERSO_LOCKED : "

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@22
    .line 656
    if-eqz v1, :cond_86

    #@24
    .line 657
    invoke-static {v0}, Lcom/android/internal/telephony/TelephonyUtils;->getUsimPersoImsi(Landroid/content/Context;)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    .line 658
    .local v2, persoImsi:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2a
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getSubscriberId()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    .line 660
    .local v4, subscriberId:Ljava/lang/String;
    const-string v5, "GSM"

    #@30
    new-instance v6, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v7, "USIM_PERSO_IMSI : "

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    const-string v7, ", getSubscriberId : "

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v6

    #@49
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v6

    #@4d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 662
    invoke-static {v4, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@53
    move-result v5

    #@54
    if-nez v5, :cond_86

    #@56
    sget-boolean v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->UsimPersoFinish:Z

    #@58
    if-nez v5, :cond_86

    #@5a
    .line 663
    const-string v5, "GSM"

    #@5c
    new-instance v6, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v7, "[LGE] disable gw subscription!,setOperatorSelection TelephonyUtils.isQCRIL()="

    #@63
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v6

    #@67
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isQCRIL()Z

    #@6a
    move-result v7

    #@6b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v6

    #@6f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v6

    #@73
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 664
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isQCRIL()Z

    #@79
    move-result v5

    #@7a
    if-eqz v5, :cond_a3

    #@7c
    .line 667
    iget-object v5, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@7e
    const v6, 0x60020

    #@81
    const/4 v7, 0x0

    #@82
    invoke-interface {v5, v6, v8, v7}, Lcom/android/internal/telephony/CommandsInterface;->setModemIntegerItem(IILandroid/os/Message;)V

    #@85
    .line 686
    :goto_85
    const/4 v3, 0x1

    #@86
    .line 691
    .end local v2           #persoImsi:Ljava/lang/String;
    .end local v4           #subscriberId:Ljava/lang/String;
    :cond_86
    if-eqz v3, :cond_ab

    #@88
    .line 693
    const-string v5, "GSM"

    #@8a
    new-instance v6, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v7, "Hold Reject Cause processing :: persoLockChecked="

    #@91
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v6

    #@95
    sget-boolean v7, Lcom/android/internal/telephony/TelephonyUtils;->persoLockChecked:Z

    #@97
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v6

    #@9b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v6

    #@9f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    .line 699
    :goto_a2
    return-void

    #@a3
    .line 683
    .restart local v2       #persoImsi:Ljava/lang/String;
    .restart local v4       #subscriberId:Ljava/lang/String;
    :cond_a3
    const-string v5, "GSM"

    #@a5
    const-string v6, "[LGE] dummy for InFineon "

    #@a7
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    goto :goto_85

    #@ab
    .line 696
    .end local v2           #persoImsi:Ljava/lang/String;
    .end local v4           #subscriberId:Ljava/lang/String;
    :cond_ab
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->delayedRejectPopupActivation(Z)V

    #@ae
    goto :goto_a2
.end method

.method private createStopSuppressingAlarm()V
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 363
    new-instance v1, Landroid/content/Intent;

    #@3
    const-string v2, "com.android.internal.telephony.STOP_SUPPRESSING_SRV_STATE_IND"

    #@5
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@8
    .line 364
    .local v1, alarmIntent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@a
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@d
    move-result-object v2

    #@e
    invoke-static {v2, v3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@11
    move-result-object v2

    #@12
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStopSuppressingAlarmIntent:Landroid/app/PendingIntent;

    #@14
    .line 365
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@16
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@19
    move-result-object v2

    #@1a
    const-string v3, "alarm"

    #@1c
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/app/AlarmManager;

    #@22
    .line 366
    .local v0, alarm:Landroid/app/AlarmManager;
    const/4 v2, 0x2

    #@23
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@26
    move-result-wide v3

    #@27
    const-wide/16 v5, 0x1770

    #@29
    add-long/2addr v3, v5

    #@2a
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStopSuppressingAlarmIntent:Landroid/app/PendingIntent;

    #@2c
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@2f
    .line 367
    return-void
.end method

.method private delayedRejectPopupActivation(Z)V
    .registers 6
    .parameter "needDelay"

    #@0
    .prologue
    .line 703
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "delayedRejectPopupActivation needDelay="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", persoLockChecked="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    sget-boolean v2, Lcom/android/internal/telephony/TelephonyUtils;->persoLockChecked:Z

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 704
    sget-boolean v0, Lcom/android/internal/telephony/TelephonyUtils;->persoLockChecked:Z

    #@26
    if-nez v0, :cond_36

    #@28
    .line 705
    const/16 v0, 0x3f4

    #@2a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@2d
    move-result-object v1

    #@2e
    if-eqz p1, :cond_37

    #@30
    const/16 v0, 0x3e8

    #@32
    :goto_32
    int-to-long v2, v0

    #@33
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@36
    .line 707
    :cond_36
    return-void

    #@37
    .line 705
    :cond_37
    const/4 v0, 0x0

    #@38
    goto :goto_32
.end method

.method private static displayNameFor(I)Ljava/lang/String;
    .registers 7
    .parameter "off"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    .line 3867
    div-int/lit16 v3, p0, 0x3e8

    #@3
    div-int/lit8 p0, v3, 0x3c

    #@5
    .line 3869
    const/16 v3, 0x9

    #@7
    new-array v0, v3, [C

    #@9
    .line 3870
    .local v0, buf:[C
    const/4 v3, 0x0

    #@a
    const/16 v4, 0x47

    #@c
    aput-char v4, v0, v3

    #@e
    .line 3871
    const/4 v3, 0x1

    #@f
    const/16 v4, 0x4d

    #@11
    aput-char v4, v0, v3

    #@13
    .line 3872
    const/4 v3, 0x2

    #@14
    const/16 v4, 0x54

    #@16
    aput-char v4, v0, v3

    #@18
    .line 3874
    if-gez p0, :cond_4f

    #@1a
    .line 3875
    const/16 v3, 0x2d

    #@1c
    aput-char v3, v0, v5

    #@1e
    .line 3876
    neg-int p0, p0

    #@1f
    .line 3881
    :goto_1f
    div-int/lit8 v1, p0, 0x3c

    #@21
    .line 3882
    .local v1, hours:I
    rem-int/lit8 v2, p0, 0x3c

    #@23
    .line 3884
    .local v2, minutes:I
    const/4 v3, 0x4

    #@24
    div-int/lit8 v4, v1, 0xa

    #@26
    add-int/lit8 v4, v4, 0x30

    #@28
    int-to-char v4, v4

    #@29
    aput-char v4, v0, v3

    #@2b
    .line 3885
    const/4 v3, 0x5

    #@2c
    rem-int/lit8 v4, v1, 0xa

    #@2e
    add-int/lit8 v4, v4, 0x30

    #@30
    int-to-char v4, v4

    #@31
    aput-char v4, v0, v3

    #@33
    .line 3887
    const/4 v3, 0x6

    #@34
    const/16 v4, 0x3a

    #@36
    aput-char v4, v0, v3

    #@38
    .line 3889
    const/4 v3, 0x7

    #@39
    div-int/lit8 v4, v2, 0xa

    #@3b
    add-int/lit8 v4, v4, 0x30

    #@3d
    int-to-char v4, v4

    #@3e
    aput-char v4, v0, v3

    #@40
    .line 3890
    const/16 v3, 0x8

    #@42
    rem-int/lit8 v4, v2, 0xa

    #@44
    add-int/lit8 v4, v4, 0x30

    #@46
    int-to-char v4, v4

    #@47
    aput-char v4, v0, v3

    #@49
    .line 3892
    new-instance v3, Ljava/lang/String;

    #@4b
    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    #@4e
    return-object v3

    #@4f
    .line 3878
    .end local v1           #hours:I
    .end local v2           #minutes:I
    :cond_4f
    const/16 v3, 0x2b

    #@51
    aput-char v3, v0, v5

    #@53
    goto :goto_1f
.end method

.method private findTimeZone(IZJ)Ljava/util/TimeZone;
    .registers 15
    .parameter "offset"
    .parameter "dst"
    .parameter "when"

    #@0
    .prologue
    .line 3276
    move v5, p1

    #@1
    .line 3277
    .local v5, rawOffset:I
    if-eqz p2, :cond_7

    #@3
    .line 3278
    const v9, 0x36ee80

    #@6
    sub-int/2addr v5, v9

    #@7
    .line 3280
    :cond_7
    invoke-static {v5}, Ljava/util/TimeZone;->getAvailableIDs(I)[Ljava/lang/String;

    #@a
    move-result-object v8

    #@b
    .line 3281
    .local v8, zones:[Ljava/lang/String;
    const/4 v2, 0x0

    #@c
    .line 3282
    .local v2, guess:Ljava/util/TimeZone;
    new-instance v1, Ljava/util/Date;

    #@e
    invoke-direct {v1, p3, p4}, Ljava/util/Date;-><init>(J)V

    #@11
    .line 3283
    .local v1, d:Ljava/util/Date;
    move-object v0, v8

    #@12
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@13
    .local v4, len$:I
    const/4 v3, 0x0

    #@14
    .local v3, i$:I
    :goto_14
    if-ge v3, v4, :cond_29

    #@16
    aget-object v7, v0, v3

    #@18
    .line 3284
    .local v7, zone:Ljava/lang/String;
    invoke-static {v7}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@1b
    move-result-object v6

    #@1c
    .line 3285
    .local v6, tz:Ljava/util/TimeZone;
    invoke-virtual {v6, p3, p4}, Ljava/util/TimeZone;->getOffset(J)I

    #@1f
    move-result v9

    #@20
    if-ne v9, p1, :cond_2a

    #@22
    invoke-virtual {v6, v1}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    #@25
    move-result v9

    #@26
    if-ne v9, p2, :cond_2a

    #@28
    .line 3287
    move-object v2, v6

    #@29
    .line 3292
    .end local v6           #tz:Ljava/util/TimeZone;
    .end local v7           #zone:Ljava/lang/String;
    :cond_29
    return-object v2

    #@2a
    .line 3283
    .restart local v6       #tz:Ljava/util/TimeZone;
    .restart local v7       #zone:Ljava/lang/String;
    :cond_2a
    add-int/lit8 v3, v3, 0x1

    #@2c
    goto :goto_14
.end method

.method private getAutoTime()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 4079
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v2

    #@b
    const-string v3, "auto_time"

    #@d
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_10
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_10} :catch_16

    #@10
    move-result v2

    #@11
    if-lez v2, :cond_14

    #@13
    .line 4082
    :goto_13
    return v1

    #@14
    .line 4079
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_13

    #@16
    .line 4081
    :catch_16
    move-exception v0

    #@17
    .line 4082
    .local v0, snfe:Landroid/provider/Settings$SettingNotFoundException;
    goto :goto_13
.end method

.method private getAutoTimeZone()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 4088
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v2

    #@b
    const-string v3, "auto_time_zone"

    #@d
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_10
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_10} :catch_16

    #@10
    move-result v2

    #@11
    if-lez v2, :cond_14

    #@13
    .line 4091
    :goto_13
    return v1

    #@14
    .line 4088
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_13

    #@16
    .line 4090
    :catch_16
    move-exception v0

    #@17
    .line 4091
    .local v0, snfe:Landroid/provider/Settings$SettingNotFoundException;
    goto :goto_13
.end method

.method private getMsgFromStatusId()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1910
    const/4 v0, 0x0

    #@1
    .line 1912
    .local v0, msg:Ljava/lang/String;
    const-string v2, "ril.gsm.reject_cause"

    #@3
    const/4 v3, 0x0

    #@4
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v1

    #@8
    .line 1913
    .local v1, statusId:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "getMsgFromStatusId : statusId :"

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1e
    .line 1915
    sparse-switch v1, :sswitch_data_a2

    #@21
    .line 1982
    :goto_21
    return-object v0

    #@22
    .line 1917
    :sswitch_22
    const-string v2, "UPLUS_NRC_02_IMSI_UNKNOWN_IN_HLR"

    #@24
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    .line 1918
    goto :goto_21

    #@29
    .line 1920
    :sswitch_29
    const-string v2, "UPLUS_NRC_03_ILLEGAL_MS"

    #@2b
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    .line 1921
    goto :goto_21

    #@30
    .line 1923
    :sswitch_30
    const-string v2, "UPLUS_NRC_06_ILLEGAL_ME"

    #@32
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    .line 1924
    goto :goto_21

    #@37
    .line 1926
    :sswitch_37
    const-string v2, "UPLUS_NRC_07_GPRS_NOT_ALLOWED"

    #@39
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    .line 1927
    goto :goto_21

    #@3e
    .line 1929
    :sswitch_3e
    const-string v2, "UPLUS_NRC_08_GPRS_AND_NON_GPRS_NOT_ALLOWED"

    #@40
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@43
    move-result-object v0

    #@44
    .line 1930
    goto :goto_21

    #@45
    .line 1932
    :sswitch_45
    const-string v2, "UPLUS_NRC_11_PLMN_NOT_ALLOWED"

    #@47
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    .line 1933
    goto :goto_21

    #@4c
    .line 1935
    :sswitch_4c
    const-string v2, "UPLUS_NRC_12_LOCATION_AREA_NOT_ALLOWED"

    #@4e
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@51
    move-result-object v0

    #@52
    .line 1936
    goto :goto_21

    #@53
    .line 1938
    :sswitch_53
    const-string v2, "UPLUS_NRC_13_ROAMING_NOT_ALLOWED_IN_THIS_LOCATION_AREA"

    #@55
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@58
    move-result-object v0

    #@59
    .line 1939
    goto :goto_21

    #@5a
    .line 1941
    :sswitch_5a
    const-string v2, "UPLUS_NRC_14_GPRS_NOT_ALLOWED_IN_THIS_PLMN"

    #@5c
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@5f
    move-result-object v0

    #@60
    .line 1942
    goto :goto_21

    #@61
    .line 1944
    :sswitch_61
    const-string v2, "UPLUS_NRC_15_NO_SUITABLE_CELLS_IN_LOCATION_AREA"

    #@63
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@66
    move-result-object v0

    #@67
    .line 1945
    goto :goto_21

    #@68
    .line 1947
    :sswitch_68
    const-string v2, "UPLUS_NRC_16_MSC_TEMPORARILY_NOT_REACHABLE"

    #@6a
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@6d
    move-result-object v0

    #@6e
    .line 1948
    goto :goto_21

    #@6f
    .line 1950
    :sswitch_6f
    const-string v2, "UPLUS_NRC_17_NETWORK_FAILURE"

    #@71
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@74
    move-result-object v0

    #@75
    .line 1951
    goto :goto_21

    #@76
    .line 1953
    :sswitch_76
    const-string v2, "UPLUS_NRC_22_INTER_CONGESTION"

    #@78
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@7b
    move-result-object v0

    #@7c
    .line 1954
    goto :goto_21

    #@7d
    .line 1976
    :sswitch_7d
    new-instance v2, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v3, "UPLUS_NRC_ETC"

    #@84
    invoke-static {v3}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@87
    move-result-object v3

    #@88
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v2

    #@8c
    const-string v3, "("

    #@8e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v2

    #@92
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@95
    move-result-object v2

    #@96
    const-string v3, ")"

    #@98
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v2

    #@9c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v0

    #@a0
    .line 1977
    goto :goto_21

    #@a1
    .line 1915
    nop

    #@a2
    :sswitch_data_a2
    .sparse-switch
        0x2 -> :sswitch_22
        0x3 -> :sswitch_29
        0x4 -> :sswitch_7d
        0x5 -> :sswitch_7d
        0x6 -> :sswitch_30
        0x7 -> :sswitch_37
        0x8 -> :sswitch_3e
        0x9 -> :sswitch_7d
        0xa -> :sswitch_7d
        0xb -> :sswitch_45
        0xc -> :sswitch_4c
        0xd -> :sswitch_53
        0xe -> :sswitch_5a
        0xf -> :sswitch_61
        0x10 -> :sswitch_68
        0x11 -> :sswitch_6f
        0x14 -> :sswitch_7d
        0x15 -> :sswitch_7d
        0x16 -> :sswitch_76
        0x17 -> :sswitch_7d
        0x20 -> :sswitch_7d
        0x21 -> :sswitch_7d
        0x22 -> :sswitch_7d
        0x26 -> :sswitch_7d
        0x28 -> :sswitch_7d
        0x30 -> :sswitch_7d
        0x3f -> :sswitch_7d
        0x5f -> :sswitch_7d
        0x60 -> :sswitch_7d
        0x61 -> :sswitch_7d
        0x62 -> :sswitch_7d
        0x63 -> :sswitch_7d
        0x65 -> :sswitch_7d
        0x6f -> :sswitch_7d
    .end sparse-switch
.end method

.method private getNitzTimeZone(IZJ)Ljava/util/TimeZone;
    .registers 8
    .parameter "offset"
    .parameter "dst"
    .parameter "when"

    #@0
    .prologue
    .line 3266
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->findTimeZone(IZJ)Ljava/util/TimeZone;

    #@3
    move-result-object v0

    #@4
    .line 3267
    .local v0, guess:Ljava/util/TimeZone;
    if-nez v0, :cond_d

    #@6
    .line 3269
    if-nez p2, :cond_27

    #@8
    const/4 v1, 0x1

    #@9
    :goto_9
    invoke-direct {p0, p1, v1, p3, p4}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->findTimeZone(IZJ)Ljava/util/TimeZone;

    #@c
    move-result-object v0

    #@d
    .line 3271
    :cond_d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "getNitzTimeZone returning "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    if-nez v0, :cond_29

    #@1a
    move-object v1, v0

    #@1b
    :goto_1b
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@26
    .line 3272
    return-object v0

    #@27
    .line 3269
    :cond_27
    const/4 v1, 0x0

    #@28
    goto :goto_9

    #@29
    .line 3271
    :cond_29
    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    goto :goto_1b
.end method

.method private handleApnChanged()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 5082
    const-string v0, "VZW"

    #@4
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_3a

    #@a
    .line 5083
    const-string v0, "handleApnChanged()"

    #@c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@f
    .line 5084
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isVZWAdminDisabled()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_24

    #@15
    .line 5085
    const-string v0, "VZW APN Disabled"

    #@17
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1a
    .line 5086
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@1c
    const-string v1, "apn2_disable"

    #@1e
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@21
    .line 5087
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setRadioPower(Z)V

    #@24
    .line 5089
    :cond_24
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isVZWAdminEnabled()Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_39

    #@2a
    .line 5090
    const-string v0, "VZW APN Enabled"

    #@2c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@2f
    .line 5091
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@31
    const-string v1, "apn2_disable"

    #@33
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@36
    .line 5092
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setRadioPower(Z)V

    #@39
    .line 5097
    :cond_39
    :goto_39
    return-void

    #@3a
    .line 5095
    :cond_3a
    const-string v0, "Igone handleApnChanged()..."

    #@3c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@3f
    goto :goto_39
.end method

.method private handleLimitedService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .registers 8
    .parameter "plmn"
    .parameter "spn"
    .parameter "showSpn"
    .parameter "showPlmn"

    #@0
    .prologue
    .line 5174
    const-string v1, "GSM"

    #@2
    const-string v2, "handleLimitedService : Limited Service state"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 5175
    new-instance v0, Landroid/content/Intent;

    #@9
    const-string v1, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@b
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e
    .line 5176
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "showSpn"

    #@10
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@13
    .line 5177
    const-string v1, "spn"

    #@15
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@18
    .line 5178
    const-string v1, "showPlmn"

    #@1a
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@1d
    .line 5179
    const-string v1, "plmn"

    #@1f
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@22
    .line 5180
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@24
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@2b
    .line 5181
    return-void
.end method

.method private isGprsConsistent(II)Z
    .registers 4
    .parameter "gprsState"
    .parameter "serviceState"

    #@0
    .prologue
    .line 3258
    if-nez p2, :cond_4

    #@2
    if-nez p1, :cond_6

    #@4
    :cond_4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private isInternationalRoaming(Ljava/lang/String;)Z
    .registers 15
    .parameter "PLMN_code"

    #@0
    .prologue
    const/4 v12, 0x2

    #@1
    const/4 v11, 0x5

    #@2
    const/4 v10, 0x3

    #@3
    const/4 v7, 0x1

    #@4
    const/4 v6, 0x0

    #@5
    .line 4481
    const/4 v8, 0x7

    #@6
    new-array v5, v8, [Ljava/lang/String;

    #@8
    const-string v8, "310"

    #@a
    aput-object v8, v5, v6

    #@c
    const-string v8, "311"

    #@e
    aput-object v8, v5, v7

    #@10
    const-string v8, "312"

    #@12
    aput-object v8, v5, v12

    #@14
    const-string v8, "313"

    #@16
    aput-object v8, v5, v10

    #@18
    const/4 v8, 0x4

    #@19
    const-string v9, "314"

    #@1b
    aput-object v9, v5, v8

    #@1d
    const-string v8, "315"

    #@1f
    aput-object v8, v5, v11

    #@21
    const/4 v8, 0x6

    #@22
    const-string v9, "316"

    #@24
    aput-object v9, v5, v8

    #@26
    .line 4482
    .local v5, usa_mcc:[Ljava/lang/String;
    new-array v1, v11, [Ljava/lang/String;

    #@28
    const-string v8, "310110"

    #@2a
    aput-object v8, v1, v6

    #@2c
    const-string v8, "310140"

    #@2e
    aput-object v8, v1, v7

    #@30
    const-string v8, "310400"

    #@32
    aput-object v8, v1, v12

    #@34
    const-string v8, "310470"

    #@36
    aput-object v8, v1, v10

    #@38
    const/4 v8, 0x4

    #@39
    const-string v9, "311170"

    #@3b
    aput-object v9, v1, v8

    #@3d
    .line 4484
    .local v1, international_plmn:[Ljava/lang/String;
    if-eqz p1, :cond_45

    #@3f
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@42
    move-result v8

    #@43
    if-eqz v8, :cond_46

    #@45
    .line 4506
    :cond_45
    :goto_45
    return v6

    #@46
    .line 4486
    :cond_46
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@49
    move-result v3

    #@4a
    .line 4487
    .local v3, length:I
    if-eq v3, v11, :cond_57

    #@4c
    const/4 v8, 0x6

    #@4d
    if-eq v3, v8, :cond_57

    #@4f
    .line 4488
    const-string v7, "[GsmServiceStateTracker]"

    #@51
    const-string v8, "isInternationalRoaming. PLMN length error "

    #@53
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_45

    #@57
    .line 4493
    :cond_57
    const/4 v4, 0x0

    #@58
    .local v4, tt:I
    :goto_58
    array-length v8, v1

    #@59
    if-ge v4, v8, :cond_68

    #@5b
    .line 4494
    aget-object v8, v1, v4

    #@5d
    invoke-static {v8, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@60
    move-result v8

    #@61
    if-eqz v8, :cond_65

    #@63
    move v6, v7

    #@64
    goto :goto_45

    #@65
    .line 4493
    :cond_65
    add-int/lit8 v4, v4, 0x1

    #@67
    goto :goto_58

    #@68
    .line 4497
    :cond_68
    const/4 v2, 0x0

    #@69
    .local v2, kk:I
    :goto_69
    array-length v8, v5

    #@6a
    if-ge v2, v8, :cond_b1

    #@6c
    .line 4499
    :try_start_6c
    aget-object v8, v5, v2

    #@6e
    const/4 v9, 0x0

    #@6f
    const/4 v10, 0x3

    #@70
    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@73
    move-result-object v9

    #@74
    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_77
    .catch Ljava/lang/NumberFormatException; {:try_start_6c .. :try_end_77} :catch_7d
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_6c .. :try_end_77} :catch_97

    #@77
    move-result v8

    #@78
    if-nez v8, :cond_45

    #@7a
    .line 4497
    :goto_7a
    add-int/lit8 v2, v2, 0x1

    #@7c
    goto :goto_69

    #@7d
    .line 4500
    :catch_7d
    move-exception v0

    #@7e
    .line 4501
    .local v0, ex:Ljava/lang/NumberFormatException;
    const-string v8, "[GsmServiceStateTracker]"

    #@80
    new-instance v9, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v10, "countryCodeForMcc error"

    #@87
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v9

    #@8b
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v9

    #@8f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v9

    #@93
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    goto :goto_7a

    #@97
    .line 4502
    .end local v0           #ex:Ljava/lang/NumberFormatException;
    :catch_97
    move-exception v0

    #@98
    .line 4503
    .local v0, ex:Ljava/lang/StringIndexOutOfBoundsException;
    const-string v8, "[GsmServiceStateTracker]"

    #@9a
    new-instance v9, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v10, "countryCodeForMcc error"

    #@a1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v9

    #@a5
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v9

    #@a9
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v9

    #@ad
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    goto :goto_7a

    #@b1
    .end local v0           #ex:Ljava/lang/StringIndexOutOfBoundsException;
    :cond_b1
    move v6, v7

    #@b2
    .line 4506
    goto :goto_45
.end method

.method private isOemRadTestSettingTrue()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4320
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v2

    #@b
    const-string v3, "oem_rad_test"

    #@d
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10
    move-result v0

    #@11
    .line 4322
    .local v0, oemRadSettingValue:I
    const-string v2, "GSM"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "isOemRadTestSettingTrue : Settings.Secure.OEM_RAD_TEST = "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    const-string v4, ", phone.getPhoneType() ="

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2a
    invoke-virtual {v4}, Lcom/android/internal/telephony/gsm/GSMPhone;->getPhoneType()I

    #@2d
    move-result v4

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 4327
    if-lez v0, :cond_3c

    #@3b
    .line 4328
    const/4 v1, 0x1

    #@3c
    .line 4330
    :cond_3c
    return v1
.end method

.method private isRoamingBetweenOperators(ZLandroid/telephony/ServiceState;)Z
    .registers 23
    .parameter "gsmRoaming"
    .parameter "s"

    #@0
    .prologue
    .line 3526
    const-string v17, "gsm.sim.operator.alpha"

    #@2
    const-string v18, "empty"

    #@4
    move-object/from16 v0, p0

    #@6
    move-object/from16 v1, v17

    #@8
    move-object/from16 v2, v18

    #@a
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getSystemProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v16

    #@e
    .line 3528
    .local v16, spn:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@11
    move-result-object v8

    #@12
    .line 3529
    .local v8, onsl:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/telephony/ServiceState;->getOperatorAlphaShort()Ljava/lang/String;

    #@15
    move-result-object v9

    #@16
    .line 3531
    .local v9, onss:Ljava/lang/String;
    if-eqz v8, :cond_bc

    #@18
    move-object/from16 v0, v16

    #@1a
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v17

    #@1e
    if-eqz v17, :cond_bc

    #@20
    const/4 v6, 0x1

    #@21
    .line 3532
    .local v6, equalsOnsl:Z
    :goto_21
    if-eqz v9, :cond_bf

    #@23
    move-object/from16 v0, v16

    #@25
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v17

    #@29
    if-eqz v17, :cond_bf

    #@2b
    const/4 v7, 0x1

    #@2c
    .line 3534
    .local v7, equalsOnss:Z
    :goto_2c
    const-string v17, "gsm.sim.operator.numeric"

    #@2e
    const-string v18, ""

    #@30
    move-object/from16 v0, p0

    #@32
    move-object/from16 v1, v17

    #@34
    move-object/from16 v2, v18

    #@36
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getSystemProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@39
    move-result-object v13

    #@3a
    .line 3535
    .local v13, simNumeric:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@3d
    move-result-object v12

    #@3e
    .line 3539
    .local v12, operatorNumeric:Ljava/lang/String;
    if-nez v13, :cond_42

    #@40
    const-string v13, ""

    #@42
    .line 3540
    :cond_42
    if-nez v12, :cond_46

    #@44
    const-string v12, ""

    #@46
    .line 3543
    :cond_46
    const/4 v4, 0x1

    #@47
    .line 3545
    .local v4, equalsMcc:Z
    const/16 v17, 0x0

    #@49
    const/16 v18, 0x3

    #@4b
    :try_start_4b
    move/from16 v0, v17

    #@4d
    move/from16 v1, v18

    #@4f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@52
    move-result-object v17

    #@53
    const/16 v18, 0x0

    #@55
    const/16 v19, 0x3

    #@57
    move/from16 v0, v18

    #@59
    move/from16 v1, v19

    #@5b
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5e
    move-result-object v18

    #@5f
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@62
    move-result v4

    #@63
    .line 3549
    const-string v17, "520"

    #@65
    const/16 v18, 0x0

    #@67
    const/16 v19, 0x3

    #@69
    move/from16 v0, v18

    #@6b
    move/from16 v1, v19

    #@6d
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@70
    move-result-object v18

    #@71
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v17

    #@75
    if-nez v17, :cond_9f

    #@77
    const-string v17, "404"

    #@79
    const/16 v18, 0x0

    #@7b
    const/16 v19, 0x3

    #@7d
    move/from16 v0, v18

    #@7f
    move/from16 v1, v19

    #@81
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@84
    move-result-object v18

    #@85
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@88
    move-result v17

    #@89
    if-nez v17, :cond_9f

    #@8b
    const-string v17, "405"

    #@8d
    const/16 v18, 0x0

    #@8f
    const/16 v19, 0x3

    #@91
    move/from16 v0, v18

    #@93
    move/from16 v1, v19

    #@95
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@98
    move-result-object v18

    #@99
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9c
    move-result v17

    #@9d
    if-eqz v17, :cond_c6

    #@9f
    .line 3551
    :cond_9f
    const/16 v17, 0x3

    #@a1
    move/from16 v0, v17

    #@a3
    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@a6
    move-result-object v17

    #@a7
    const/16 v18, 0x3

    #@a9
    move/from16 v0, v18

    #@ab
    invoke-virtual {v12, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@ae
    move-result-object v18

    #@af
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_b2
    .catch Ljava/lang/Exception; {:try_start_4b .. :try_end_b2} :catch_c5

    #@b2
    move-result v5

    #@b3
    .line 3552
    .local v5, equalsMnc:Z
    if-eqz p1, :cond_c2

    #@b5
    if-eqz v4, :cond_b9

    #@b7
    if-nez v5, :cond_c2

    #@b9
    :cond_b9
    const/16 v17, 0x1

    #@bb
    .line 3825
    .end local v5           #equalsMnc:Z
    :goto_bb
    return v17

    #@bc
    .line 3531
    .end local v4           #equalsMcc:Z
    .end local v6           #equalsOnsl:Z
    .end local v7           #equalsOnss:Z
    .end local v12           #operatorNumeric:Ljava/lang/String;
    .end local v13           #simNumeric:Ljava/lang/String;
    :cond_bc
    const/4 v6, 0x0

    #@bd
    goto/16 :goto_21

    #@bf
    .line 3532
    .restart local v6       #equalsOnsl:Z
    :cond_bf
    const/4 v7, 0x0

    #@c0
    goto/16 :goto_2c

    #@c2
    .line 3552
    .restart local v4       #equalsMcc:Z
    .restart local v5       #equalsMnc:Z
    .restart local v7       #equalsOnss:Z
    .restart local v12       #operatorNumeric:Ljava/lang/String;
    .restart local v13       #simNumeric:Ljava/lang/String;
    :cond_c2
    const/16 v17, 0x0

    #@c4
    goto :goto_bb

    #@c5
    .line 3555
    .end local v5           #equalsMnc:Z
    :catch_c5
    move-exception v17

    #@c6
    .line 3563
    :cond_c6
    if-eqz v12, :cond_c6f

    #@c8
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@cb
    move-result v17

    #@cc
    const/16 v18, 0x5

    #@ce
    move/from16 v0, v17

    #@d0
    move/from16 v1, v18

    #@d2
    if-lt v0, v1, :cond_c6f

    #@d4
    if-eqz v13, :cond_c6f

    #@d6
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@d9
    move-result v17

    #@da
    const/16 v18, 0x5

    #@dc
    move/from16 v0, v17

    #@de
    move/from16 v1, v18

    #@e0
    if-lt v0, v1, :cond_c6f

    #@e2
    .line 3564
    const/16 v17, 0x0

    #@e4
    const/16 v18, 0x5

    #@e6
    move/from16 v0, v17

    #@e8
    move/from16 v1, v18

    #@ea
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ed
    move-result-object v17

    #@ee
    const-string v18, "41820"

    #@f0
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@f3
    move-result v17

    #@f4
    if-nez v17, :cond_10a

    #@f6
    const/16 v17, 0x0

    #@f8
    const/16 v18, 0x5

    #@fa
    move/from16 v0, v17

    #@fc
    move/from16 v1, v18

    #@fe
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@101
    move-result-object v17

    #@102
    const-string v18, "41830"

    #@104
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@107
    move-result v17

    #@108
    if-eqz v17, :cond_15a

    #@10a
    :cond_10a
    const/16 v17, 0x0

    #@10c
    const/16 v18, 0x5

    #@10e
    move/from16 v0, v17

    #@110
    move/from16 v1, v18

    #@112
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@115
    move-result-object v17

    #@116
    const-string v18, "42502"

    #@118
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@11b
    move-result v17

    #@11c
    if-nez v17, :cond_132

    #@11e
    const/16 v17, 0x0

    #@120
    const/16 v18, 0x5

    #@122
    move/from16 v0, v17

    #@124
    move/from16 v1, v18

    #@126
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@129
    move-result-object v17

    #@12a
    const-string v18, "42508"

    #@12c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@12f
    move-result v17

    #@130
    if-eqz v17, :cond_15a

    #@132
    :cond_132
    const/16 v17, 0x0

    #@134
    const/16 v18, 0x5

    #@136
    move/from16 v0, v17

    #@138
    move/from16 v1, v18

    #@13a
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@13d
    move-result-object v17

    #@13e
    const-string v18, "42503"

    #@140
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@143
    move-result v17

    #@144
    if-nez v17, :cond_15b

    #@146
    const/16 v17, 0x0

    #@148
    const/16 v18, 0x5

    #@14a
    move/from16 v0, v17

    #@14c
    move/from16 v1, v18

    #@14e
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@151
    move-result-object v17

    #@152
    const-string v18, "42507"

    #@154
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@157
    move-result v17

    #@158
    if-nez v17, :cond_15b

    #@15a
    .line 3567
    :cond_15a
    const/4 v6, 0x1

    #@15b
    .line 3581
    :cond_15b
    :goto_15b
    if-eqz v12, :cond_890

    #@15d
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@160
    move-result v17

    #@161
    const/16 v18, 0x5

    #@163
    move/from16 v0, v17

    #@165
    move/from16 v1, v18

    #@167
    if-lt v0, v1, :cond_890

    #@169
    if-eqz v13, :cond_890

    #@16b
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@16e
    move-result v17

    #@16f
    const/16 v18, 0x5

    #@171
    move/from16 v0, v17

    #@173
    move/from16 v1, v18

    #@175
    if-lt v0, v1, :cond_890

    #@177
    .line 3583
    const-string v17, "TMO"

    #@179
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@17c
    move-result v17

    #@17d
    if-nez v17, :cond_18f

    #@17f
    const-string v17, "ORG"

    #@181
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@184
    move-result v17

    #@185
    if-nez v17, :cond_18f

    #@187
    const-string v17, "OPEN"

    #@189
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@18c
    move-result v17

    #@18d
    if-eqz v17, :cond_27f

    #@18f
    :cond_18f
    const/16 v17, 0x0

    #@191
    const/16 v18, 0x5

    #@193
    move/from16 v0, v17

    #@195
    move/from16 v1, v18

    #@197
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@19a
    move-result-object v17

    #@19b
    const-string v18, "23430"

    #@19d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1a0
    move-result v17

    #@1a1
    if-eqz v17, :cond_207

    #@1a3
    const/16 v17, 0x0

    #@1a5
    const/16 v18, 0x5

    #@1a7
    move/from16 v0, v17

    #@1a9
    move/from16 v1, v18

    #@1ab
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1ae
    move-result-object v17

    #@1af
    const-string v18, "23431"

    #@1b1
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1b4
    move-result v17

    #@1b5
    if-eqz v17, :cond_207

    #@1b7
    const/16 v17, 0x0

    #@1b9
    const/16 v18, 0x5

    #@1bb
    move/from16 v0, v17

    #@1bd
    move/from16 v1, v18

    #@1bf
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c2
    move-result-object v17

    #@1c3
    const-string v18, "23432"

    #@1c5
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1c8
    move-result v17

    #@1c9
    if-eqz v17, :cond_207

    #@1cb
    const/16 v17, 0x0

    #@1cd
    const/16 v18, 0x5

    #@1cf
    move/from16 v0, v17

    #@1d1
    move/from16 v1, v18

    #@1d3
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1d6
    move-result-object v17

    #@1d7
    const-string v18, "23433"

    #@1d9
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1dc
    move-result v17

    #@1dd
    if-eqz v17, :cond_207

    #@1df
    const/16 v17, 0x0

    #@1e1
    const/16 v18, 0x5

    #@1e3
    move/from16 v0, v17

    #@1e5
    move/from16 v1, v18

    #@1e7
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1ea
    move-result-object v17

    #@1eb
    const-string v18, "23434"

    #@1ed
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1f0
    move-result v17

    #@1f1
    if-eqz v17, :cond_207

    #@1f3
    const/16 v17, 0x0

    #@1f5
    const/16 v18, 0x5

    #@1f7
    move/from16 v0, v17

    #@1f9
    move/from16 v1, v18

    #@1fb
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1fe
    move-result-object v17

    #@1ff
    const-string v18, "23486"

    #@201
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@204
    move-result v17

    #@205
    if-nez v17, :cond_27f

    #@207
    :cond_207
    const/16 v17, 0x0

    #@209
    const/16 v18, 0x5

    #@20b
    move/from16 v0, v17

    #@20d
    move/from16 v1, v18

    #@20f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@212
    move-result-object v17

    #@213
    const-string v18, "23430"

    #@215
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@218
    move-result v17

    #@219
    if-eqz v17, :cond_843

    #@21b
    const/16 v17, 0x0

    #@21d
    const/16 v18, 0x5

    #@21f
    move/from16 v0, v17

    #@221
    move/from16 v1, v18

    #@223
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@226
    move-result-object v17

    #@227
    const-string v18, "23431"

    #@229
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@22c
    move-result v17

    #@22d
    if-eqz v17, :cond_843

    #@22f
    const/16 v17, 0x0

    #@231
    const/16 v18, 0x5

    #@233
    move/from16 v0, v17

    #@235
    move/from16 v1, v18

    #@237
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@23a
    move-result-object v17

    #@23b
    const-string v18, "23432"

    #@23d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@240
    move-result v17

    #@241
    if-eqz v17, :cond_843

    #@243
    const/16 v17, 0x0

    #@245
    const/16 v18, 0x5

    #@247
    move/from16 v0, v17

    #@249
    move/from16 v1, v18

    #@24b
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@24e
    move-result-object v17

    #@24f
    const-string v18, "23433"

    #@251
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@254
    move-result v17

    #@255
    if-eqz v17, :cond_843

    #@257
    const/16 v17, 0x0

    #@259
    const/16 v18, 0x5

    #@25b
    move/from16 v0, v17

    #@25d
    move/from16 v1, v18

    #@25f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@262
    move-result-object v17

    #@263
    const-string v18, "23434"

    #@265
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@268
    move-result v17

    #@269
    if-eqz v17, :cond_843

    #@26b
    const/16 v17, 0x0

    #@26d
    const/16 v18, 0x5

    #@26f
    move/from16 v0, v17

    #@271
    move/from16 v1, v18

    #@273
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@276
    move-result-object v17

    #@277
    const-string v18, "23486"

    #@279
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@27c
    move-result v17

    #@27d
    if-eqz v17, :cond_843

    #@27f
    :cond_27f
    const/16 v17, 0x0

    #@281
    const/16 v18, 0x5

    #@283
    move/from16 v0, v17

    #@285
    move/from16 v1, v18

    #@287
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@28a
    move-result-object v17

    #@28b
    const-string v18, "20810"

    #@28d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@290
    move-result v17

    #@291
    if-eqz v17, :cond_2a7

    #@293
    const/16 v17, 0x0

    #@295
    const/16 v18, 0x5

    #@297
    move/from16 v0, v17

    #@299
    move/from16 v1, v18

    #@29b
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@29e
    move-result-object v17

    #@29f
    const-string v18, "20801"

    #@2a1
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@2a4
    move-result v17

    #@2a5
    if-nez v17, :cond_2bb

    #@2a7
    :cond_2a7
    const/16 v17, 0x0

    #@2a9
    const/16 v18, 0x5

    #@2ab
    move/from16 v0, v17

    #@2ad
    move/from16 v1, v18

    #@2af
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2b2
    move-result-object v17

    #@2b3
    const-string v18, "20826"

    #@2b5
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@2b8
    move-result v17

    #@2b9
    if-eqz v17, :cond_843

    #@2bb
    :cond_2bb
    const/16 v17, 0x0

    #@2bd
    const/16 v18, 0x5

    #@2bf
    move/from16 v0, v17

    #@2c1
    move/from16 v1, v18

    #@2c3
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2c6
    move-result-object v17

    #@2c7
    const-string v18, "21401"

    #@2c9
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@2cc
    move-result v17

    #@2cd
    if-nez v17, :cond_30b

    #@2cf
    const/16 v17, 0x0

    #@2d1
    const/16 v18, 0x5

    #@2d3
    move/from16 v0, v17

    #@2d5
    move/from16 v1, v18

    #@2d7
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2da
    move-result-object v17

    #@2db
    const-string v18, "21406"

    #@2dd
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@2e0
    move-result v17

    #@2e1
    if-eqz v17, :cond_843

    #@2e3
    const/16 v17, 0x0

    #@2e5
    const/16 v18, 0x5

    #@2e7
    move/from16 v0, v17

    #@2e9
    move/from16 v1, v18

    #@2eb
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2ee
    move-result-object v17

    #@2ef
    const-string v18, "21408"

    #@2f1
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@2f4
    move-result v17

    #@2f5
    if-eqz v17, :cond_843

    #@2f7
    const/16 v17, 0x0

    #@2f9
    const/16 v18, 0x5

    #@2fb
    move/from16 v0, v17

    #@2fd
    move/from16 v1, v18

    #@2ff
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@302
    move-result-object v17

    #@303
    const-string v18, "21416"

    #@305
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@308
    move-result v17

    #@309
    if-eqz v17, :cond_843

    #@30b
    :cond_30b
    const/16 v17, 0x0

    #@30d
    const/16 v18, 0x5

    #@30f
    move/from16 v0, v17

    #@311
    move/from16 v1, v18

    #@313
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@316
    move-result-object v17

    #@317
    const-string v18, "21404"

    #@319
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@31c
    move-result v17

    #@31d
    if-eqz v17, :cond_333

    #@31f
    const/16 v17, 0x0

    #@321
    const/16 v18, 0x5

    #@323
    move/from16 v0, v17

    #@325
    move/from16 v1, v18

    #@327
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@32a
    move-result-object v17

    #@32b
    const-string v18, "21407"

    #@32d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@330
    move-result v17

    #@331
    if-nez v17, :cond_35b

    #@333
    :cond_333
    const/16 v17, 0x0

    #@335
    const/16 v18, 0x5

    #@337
    move/from16 v0, v17

    #@339
    move/from16 v1, v18

    #@33b
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@33e
    move-result-object v17

    #@33f
    const-string v18, "21404"

    #@341
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@344
    move-result v17

    #@345
    if-eqz v17, :cond_843

    #@347
    const/16 v17, 0x0

    #@349
    const/16 v18, 0x5

    #@34b
    move/from16 v0, v17

    #@34d
    move/from16 v1, v18

    #@34f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@352
    move-result-object v17

    #@353
    const-string v18, "21407"

    #@355
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@358
    move-result v17

    #@359
    if-eqz v17, :cond_843

    #@35b
    :cond_35b
    const/16 v17, 0x0

    #@35d
    const/16 v18, 0x5

    #@35f
    move/from16 v0, v17

    #@361
    move/from16 v1, v18

    #@363
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@366
    move-result-object v17

    #@367
    const-string v18, "21403"

    #@369
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@36c
    move-result v17

    #@36d
    if-nez v17, :cond_383

    #@36f
    const/16 v17, 0x0

    #@371
    const/16 v18, 0x5

    #@373
    move/from16 v0, v17

    #@375
    move/from16 v1, v18

    #@377
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@37a
    move-result-object v17

    #@37b
    const-string v18, "21421"

    #@37d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@380
    move-result v17

    #@381
    if-eqz v17, :cond_843

    #@383
    :cond_383
    const/16 v17, 0x0

    #@385
    const/16 v18, 0x5

    #@387
    move/from16 v0, v17

    #@389
    move/from16 v1, v18

    #@38b
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@38e
    move-result-object v17

    #@38f
    const-string v18, "21403"

    #@391
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@394
    move-result v17

    #@395
    if-nez v17, :cond_3ab

    #@397
    const/16 v17, 0x0

    #@399
    const/16 v18, 0x5

    #@39b
    move/from16 v0, v17

    #@39d
    move/from16 v1, v18

    #@39f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3a2
    move-result-object v17

    #@3a3
    const-string v18, "21419"

    #@3a5
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@3a8
    move-result v17

    #@3a9
    if-eqz v17, :cond_843

    #@3ab
    :cond_3ab
    const/16 v17, 0x0

    #@3ad
    const/16 v18, 0x5

    #@3af
    move/from16 v0, v17

    #@3b1
    move/from16 v1, v18

    #@3b3
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3b6
    move-result-object v17

    #@3b7
    const-string v18, "20810"

    #@3b9
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@3bc
    move-result v17

    #@3bd
    if-eqz v17, :cond_3d3

    #@3bf
    const/16 v17, 0x0

    #@3c1
    const/16 v18, 0x5

    #@3c3
    move/from16 v0, v17

    #@3c5
    move/from16 v1, v18

    #@3c7
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3ca
    move-result-object v17

    #@3cb
    const-string v18, "20811"

    #@3cd
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@3d0
    move-result v17

    #@3d1
    if-nez v17, :cond_3fb

    #@3d3
    :cond_3d3
    const/16 v17, 0x0

    #@3d5
    const/16 v18, 0x5

    #@3d7
    move/from16 v0, v17

    #@3d9
    move/from16 v1, v18

    #@3db
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3de
    move-result-object v17

    #@3df
    const-string v18, "20810"

    #@3e1
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@3e4
    move-result v17

    #@3e5
    if-eqz v17, :cond_843

    #@3e7
    const/16 v17, 0x0

    #@3e9
    const/16 v18, 0x5

    #@3eb
    move/from16 v0, v17

    #@3ed
    move/from16 v1, v18

    #@3ef
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3f2
    move-result-object v17

    #@3f3
    const-string v18, "20811"

    #@3f5
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@3f8
    move-result v17

    #@3f9
    if-eqz v17, :cond_843

    #@3fb
    :cond_3fb
    const/16 v17, 0x0

    #@3fd
    const/16 v18, 0x5

    #@3ff
    move/from16 v0, v17

    #@401
    move/from16 v1, v18

    #@403
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@406
    move-result-object v17

    #@407
    const-string v18, "20801"

    #@409
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@40c
    move-result v17

    #@40d
    if-eqz v17, :cond_437

    #@40f
    const/16 v17, 0x0

    #@411
    const/16 v18, 0x5

    #@413
    move/from16 v0, v17

    #@415
    move/from16 v1, v18

    #@417
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@41a
    move-result-object v17

    #@41b
    const-string v18, "20802"

    #@41d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@420
    move-result v17

    #@421
    if-eqz v17, :cond_437

    #@423
    const/16 v17, 0x0

    #@425
    const/16 v18, 0x5

    #@427
    move/from16 v0, v17

    #@429
    move/from16 v1, v18

    #@42b
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@42e
    move-result-object v17

    #@42f
    const-string v18, "20810"

    #@431
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@434
    move-result v17

    #@435
    if-nez v17, :cond_44b

    #@437
    :cond_437
    const/16 v17, 0x0

    #@439
    const/16 v18, 0x5

    #@43b
    move/from16 v0, v17

    #@43d
    move/from16 v1, v18

    #@43f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@442
    move-result-object v17

    #@443
    const-string v18, "20815"

    #@445
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@448
    move-result v17

    #@449
    if-eqz v17, :cond_843

    #@44b
    :cond_44b
    const/16 v17, 0x0

    #@44d
    const/16 v18, 0x5

    #@44f
    move/from16 v0, v17

    #@451
    move/from16 v1, v18

    #@453
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@456
    move-result-object v17

    #@457
    const-string v18, "21402"

    #@459
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@45c
    move-result v17

    #@45d
    if-eqz v17, :cond_487

    #@45f
    const/16 v17, 0x0

    #@461
    const/16 v18, 0x5

    #@463
    move/from16 v0, v17

    #@465
    move/from16 v1, v18

    #@467
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@46a
    move-result-object v17

    #@46b
    const-string v18, "21405"

    #@46d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@470
    move-result v17

    #@471
    if-eqz v17, :cond_487

    #@473
    const/16 v17, 0x0

    #@475
    const/16 v18, 0x5

    #@477
    move/from16 v0, v17

    #@479
    move/from16 v1, v18

    #@47b
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@47e
    move-result-object v17

    #@47f
    const-string v18, "21407"

    #@481
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@484
    move-result v17

    #@485
    if-nez v17, :cond_4c3

    #@487
    :cond_487
    const/16 v17, 0x0

    #@489
    const/16 v18, 0x5

    #@48b
    move/from16 v0, v17

    #@48d
    move/from16 v1, v18

    #@48f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@492
    move-result-object v17

    #@493
    const-string v18, "21402"

    #@495
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@498
    move-result v17

    #@499
    if-eqz v17, :cond_843

    #@49b
    const/16 v17, 0x0

    #@49d
    const/16 v18, 0x5

    #@49f
    move/from16 v0, v17

    #@4a1
    move/from16 v1, v18

    #@4a3
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4a6
    move-result-object v17

    #@4a7
    const-string v18, "21405"

    #@4a9
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@4ac
    move-result v17

    #@4ad
    if-eqz v17, :cond_843

    #@4af
    const/16 v17, 0x0

    #@4b1
    const/16 v18, 0x5

    #@4b3
    move/from16 v0, v17

    #@4b5
    move/from16 v1, v18

    #@4b7
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4ba
    move-result-object v17

    #@4bb
    const-string v18, "21407"

    #@4bd
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@4c0
    move-result v17

    #@4c1
    if-eqz v17, :cond_843

    #@4c3
    :cond_4c3
    if-eqz v12, :cond_53f

    #@4c5
    if-eqz v13, :cond_53f

    #@4c7
    const/16 v17, 0x0

    #@4c9
    const/16 v18, 0x5

    #@4cb
    move/from16 v0, v17

    #@4cd
    move/from16 v1, v18

    #@4cf
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4d2
    move-result-object v17

    #@4d3
    const-string v18, "20605"

    #@4d5
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@4d8
    move-result v17

    #@4d9
    if-eqz v17, :cond_503

    #@4db
    const/16 v17, 0x0

    #@4dd
    const/16 v18, 0x5

    #@4df
    move/from16 v0, v17

    #@4e1
    move/from16 v1, v18

    #@4e3
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4e6
    move-result-object v17

    #@4e7
    const-string v18, "20601"

    #@4e9
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@4ec
    move-result v17

    #@4ed
    if-eqz v17, :cond_503

    #@4ef
    const/16 v17, 0x0

    #@4f1
    const/16 v18, 0x5

    #@4f3
    move/from16 v0, v17

    #@4f5
    move/from16 v1, v18

    #@4f7
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4fa
    move-result-object v17

    #@4fb
    const-string v18, "20610"

    #@4fd
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@500
    move-result v17

    #@501
    if-nez v17, :cond_53f

    #@503
    :cond_503
    const/16 v17, 0x0

    #@505
    const/16 v18, 0x5

    #@507
    move/from16 v0, v17

    #@509
    move/from16 v1, v18

    #@50b
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@50e
    move-result-object v17

    #@50f
    const-string v18, "20610"

    #@511
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@514
    move-result v17

    #@515
    if-eqz v17, :cond_843

    #@517
    const/16 v17, 0x0

    #@519
    const/16 v18, 0x5

    #@51b
    move/from16 v0, v17

    #@51d
    move/from16 v1, v18

    #@51f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@522
    move-result-object v17

    #@523
    const-string v18, "20601"

    #@525
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@528
    move-result v17

    #@529
    if-eqz v17, :cond_843

    #@52b
    const/16 v17, 0x0

    #@52d
    const/16 v18, 0x5

    #@52f
    move/from16 v0, v17

    #@531
    move/from16 v1, v18

    #@533
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@536
    move-result-object v17

    #@537
    const-string v18, "20605"

    #@539
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@53c
    move-result v17

    #@53d
    if-eqz v17, :cond_843

    #@53f
    :cond_53f
    if-eqz v12, :cond_56b

    #@541
    if-eqz v13, :cond_56b

    #@543
    const/16 v17, 0x0

    #@545
    const/16 v18, 0x5

    #@547
    move/from16 v0, v17

    #@549
    move/from16 v1, v18

    #@54b
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@54e
    move-result-object v17

    #@54f
    const-string v18, "21407"

    #@551
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@554
    move-result v17

    #@555
    if-nez v17, :cond_56b

    #@557
    const/16 v17, 0x0

    #@559
    const/16 v18, 0x5

    #@55b
    move/from16 v0, v17

    #@55d
    move/from16 v1, v18

    #@55f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@562
    move-result-object v17

    #@563
    const-string v18, "21418"

    #@565
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@568
    move-result v17

    #@569
    if-eqz v17, :cond_843

    #@56b
    :cond_56b
    if-eqz v12, :cond_597

    #@56d
    if-eqz v13, :cond_597

    #@56f
    const/16 v17, 0x0

    #@571
    const/16 v18, 0x5

    #@573
    move/from16 v0, v17

    #@575
    move/from16 v1, v18

    #@577
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@57a
    move-result-object v17

    #@57b
    const-string v18, "20408"

    #@57d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@580
    move-result v17

    #@581
    if-nez v17, :cond_597

    #@583
    const/16 v17, 0x0

    #@585
    const/16 v18, 0x5

    #@587
    move/from16 v0, v17

    #@589
    move/from16 v1, v18

    #@58b
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@58e
    move-result-object v17

    #@58f
    const-string v18, "20412"

    #@591
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@594
    move-result v17

    #@595
    if-eqz v17, :cond_843

    #@597
    :cond_597
    const/16 v17, 0x0

    #@599
    const/16 v18, 0x5

    #@59b
    move/from16 v0, v17

    #@59d
    move/from16 v1, v18

    #@59f
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5a2
    move-result-object v17

    #@5a3
    const-string v18, "20416"

    #@5a5
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@5a8
    move-result v17

    #@5a9
    if-eqz v17, :cond_5bf

    #@5ab
    const/16 v17, 0x0

    #@5ad
    const/16 v18, 0x5

    #@5af
    move/from16 v0, v17

    #@5b1
    move/from16 v1, v18

    #@5b3
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5b6
    move-result-object v17

    #@5b7
    const-string v18, "20420"

    #@5b9
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@5bc
    move-result v17

    #@5bd
    if-nez v17, :cond_5d3

    #@5bf
    :cond_5bf
    const/16 v17, 0x0

    #@5c1
    const/16 v18, 0x5

    #@5c3
    move/from16 v0, v17

    #@5c5
    move/from16 v1, v18

    #@5c7
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5ca
    move-result-object v17

    #@5cb
    const-string v18, "20402"

    #@5cd
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@5d0
    move-result v17

    #@5d1
    if-eqz v17, :cond_843

    #@5d3
    :cond_5d3
    if-eqz v12, :cond_627

    #@5d5
    if-eqz v13, :cond_627

    #@5d7
    const/16 v17, 0x0

    #@5d9
    const/16 v18, 0x3

    #@5db
    move/from16 v0, v17

    #@5dd
    move/from16 v1, v18

    #@5df
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5e2
    move-result-object v17

    #@5e3
    const-string v18, "232"

    #@5e5
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@5e8
    move-result v17

    #@5e9
    if-nez v17, :cond_627

    #@5eb
    const/16 v17, 0x0

    #@5ed
    const/16 v18, 0x5

    #@5ef
    move/from16 v0, v17

    #@5f1
    move/from16 v1, v18

    #@5f3
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5f6
    move-result-object v17

    #@5f7
    const-string v18, "23211"

    #@5f9
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@5fc
    move-result v17

    #@5fd
    if-eqz v17, :cond_843

    #@5ff
    const/16 v17, 0x0

    #@601
    const/16 v18, 0x5

    #@603
    move/from16 v0, v17

    #@605
    move/from16 v1, v18

    #@607
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@60a
    move-result-object v17

    #@60b
    const-string v18, "23201"

    #@60d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@610
    move-result v17

    #@611
    if-eqz v17, :cond_843

    #@613
    const/16 v17, 0x0

    #@615
    const/16 v18, 0x5

    #@617
    move/from16 v0, v17

    #@619
    move/from16 v1, v18

    #@61b
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@61e
    move-result-object v17

    #@61f
    const-string v18, "23210"

    #@621
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@624
    move-result v17

    #@625
    if-eqz v17, :cond_843

    #@627
    :cond_627
    if-eqz v12, :cond_6cb

    #@629
    if-eqz v13, :cond_6cb

    #@62b
    const/16 v17, 0x0

    #@62d
    const/16 v18, 0x5

    #@62f
    move/from16 v0, v17

    #@631
    move/from16 v1, v18

    #@633
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@636
    move-result-object v17

    #@637
    const-string v18, "72411"

    #@639
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@63c
    move-result v17

    #@63d
    if-eqz v17, :cond_67b

    #@63f
    const/16 v17, 0x0

    #@641
    const/16 v18, 0x5

    #@643
    move/from16 v0, v17

    #@645
    move/from16 v1, v18

    #@647
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@64a
    move-result-object v17

    #@64b
    const-string v18, "72410"

    #@64d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@650
    move-result v17

    #@651
    if-eqz v17, :cond_67b

    #@653
    const/16 v17, 0x0

    #@655
    const/16 v18, 0x5

    #@657
    move/from16 v0, v17

    #@659
    move/from16 v1, v18

    #@65b
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@65e
    move-result-object v17

    #@65f
    const-string v18, "72406"

    #@661
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@664
    move-result v17

    #@665
    if-eqz v17, :cond_67b

    #@667
    const/16 v17, 0x0

    #@669
    const/16 v18, 0x5

    #@66b
    move/from16 v0, v17

    #@66d
    move/from16 v1, v18

    #@66f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@672
    move-result-object v17

    #@673
    const-string v18, "72423"

    #@675
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@678
    move-result v17

    #@679
    if-nez v17, :cond_6cb

    #@67b
    :cond_67b
    const/16 v17, 0x0

    #@67d
    const/16 v18, 0x5

    #@67f
    move/from16 v0, v17

    #@681
    move/from16 v1, v18

    #@683
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@686
    move-result-object v17

    #@687
    const-string v18, "72411"

    #@689
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@68c
    move-result v17

    #@68d
    if-eqz v17, :cond_843

    #@68f
    const/16 v17, 0x0

    #@691
    const/16 v18, 0x5

    #@693
    move/from16 v0, v17

    #@695
    move/from16 v1, v18

    #@697
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@69a
    move-result-object v17

    #@69b
    const-string v18, "72410"

    #@69d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@6a0
    move-result v17

    #@6a1
    if-eqz v17, :cond_843

    #@6a3
    const/16 v17, 0x0

    #@6a5
    const/16 v18, 0x5

    #@6a7
    move/from16 v0, v17

    #@6a9
    move/from16 v1, v18

    #@6ab
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6ae
    move-result-object v17

    #@6af
    const-string v18, "72406"

    #@6b1
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@6b4
    move-result v17

    #@6b5
    if-eqz v17, :cond_843

    #@6b7
    const/16 v17, 0x0

    #@6b9
    const/16 v18, 0x5

    #@6bb
    move/from16 v0, v17

    #@6bd
    move/from16 v1, v18

    #@6bf
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6c2
    move-result-object v17

    #@6c3
    const-string v18, "72423"

    #@6c5
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@6c8
    move-result v17

    #@6c9
    if-eqz v17, :cond_843

    #@6cb
    :cond_6cb
    if-eqz v12, :cond_71f

    #@6cd
    if-eqz v13, :cond_71f

    #@6cf
    const/16 v17, 0x0

    #@6d1
    const/16 v18, 0x5

    #@6d3
    move/from16 v0, v17

    #@6d5
    move/from16 v1, v18

    #@6d7
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6da
    move-result-object v17

    #@6db
    const-string v18, "26207"

    #@6dd
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@6e0
    move-result v17

    #@6e1
    if-eqz v17, :cond_6f7

    #@6e3
    const/16 v17, 0x0

    #@6e5
    const/16 v18, 0x5

    #@6e7
    move/from16 v0, v17

    #@6e9
    move/from16 v1, v18

    #@6eb
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6ee
    move-result-object v17

    #@6ef
    const-string v18, "20608"

    #@6f1
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@6f4
    move-result v17

    #@6f5
    if-nez v17, :cond_71f

    #@6f7
    :cond_6f7
    const/16 v17, 0x0

    #@6f9
    const/16 v18, 0x5

    #@6fb
    move/from16 v0, v17

    #@6fd
    move/from16 v1, v18

    #@6ff
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@702
    move-result-object v17

    #@703
    const-string v18, "26207"

    #@705
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@708
    move-result v17

    #@709
    if-eqz v17, :cond_843

    #@70b
    const/16 v17, 0x0

    #@70d
    const/16 v18, 0x5

    #@70f
    move/from16 v0, v17

    #@711
    move/from16 v1, v18

    #@713
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@716
    move-result-object v17

    #@717
    const-string v18, "26208"

    #@719
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@71c
    move-result v17

    #@71d
    if-eqz v17, :cond_843

    #@71f
    :cond_71f
    if-eqz v12, :cond_79b

    #@721
    if-eqz v13, :cond_79b

    #@723
    const/16 v17, 0x0

    #@725
    const/16 v18, 0x5

    #@727
    move/from16 v0, v17

    #@729
    move/from16 v1, v18

    #@72b
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@72e
    move-result-object v17

    #@72f
    const-string v18, "72402"

    #@731
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@734
    move-result v17

    #@735
    if-eqz v17, :cond_75f

    #@737
    const/16 v17, 0x0

    #@739
    const/16 v18, 0x5

    #@73b
    move/from16 v0, v17

    #@73d
    move/from16 v1, v18

    #@73f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@742
    move-result-object v17

    #@743
    const-string v18, "72403"

    #@745
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@748
    move-result v17

    #@749
    if-eqz v17, :cond_75f

    #@74b
    const/16 v17, 0x0

    #@74d
    const/16 v18, 0x5

    #@74f
    move/from16 v0, v17

    #@751
    move/from16 v1, v18

    #@753
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@756
    move-result-object v17

    #@757
    const-string v18, "72404"

    #@759
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@75c
    move-result v17

    #@75d
    if-nez v17, :cond_79b

    #@75f
    :cond_75f
    const/16 v17, 0x0

    #@761
    const/16 v18, 0x5

    #@763
    move/from16 v0, v17

    #@765
    move/from16 v1, v18

    #@767
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@76a
    move-result-object v17

    #@76b
    const-string v18, "72402"

    #@76d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@770
    move-result v17

    #@771
    if-eqz v17, :cond_843

    #@773
    const/16 v17, 0x0

    #@775
    const/16 v18, 0x5

    #@777
    move/from16 v0, v17

    #@779
    move/from16 v1, v18

    #@77b
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@77e
    move-result-object v17

    #@77f
    const-string v18, "72403"

    #@781
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@784
    move-result v17

    #@785
    if-eqz v17, :cond_843

    #@787
    const/16 v17, 0x0

    #@789
    const/16 v18, 0x5

    #@78b
    move/from16 v0, v17

    #@78d
    move/from16 v1, v18

    #@78f
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@792
    move-result-object v17

    #@793
    const-string v18, "72404"

    #@795
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@798
    move-result v17

    #@799
    if-eqz v17, :cond_843

    #@79b
    :cond_79b
    if-eqz v12, :cond_7ef

    #@79d
    if-eqz v13, :cond_7ef

    #@79f
    const/16 v17, 0x0

    #@7a1
    const/16 v18, 0x5

    #@7a3
    move/from16 v0, v17

    #@7a5
    move/from16 v1, v18

    #@7a7
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7aa
    move-result-object v17

    #@7ab
    const-string v18, "46000"

    #@7ad
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@7b0
    move-result v17

    #@7b1
    if-eqz v17, :cond_7c7

    #@7b3
    const/16 v17, 0x0

    #@7b5
    const/16 v18, 0x5

    #@7b7
    move/from16 v0, v17

    #@7b9
    move/from16 v1, v18

    #@7bb
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7be
    move-result-object v17

    #@7bf
    const-string v18, "46002"

    #@7c1
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@7c4
    move-result v17

    #@7c5
    if-nez v17, :cond_7ef

    #@7c7
    :cond_7c7
    const/16 v17, 0x0

    #@7c9
    const/16 v18, 0x5

    #@7cb
    move/from16 v0, v17

    #@7cd
    move/from16 v1, v18

    #@7cf
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7d2
    move-result-object v17

    #@7d3
    const-string v18, "46000"

    #@7d5
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@7d8
    move-result v17

    #@7d9
    if-eqz v17, :cond_843

    #@7db
    const/16 v17, 0x0

    #@7dd
    const/16 v18, 0x5

    #@7df
    move/from16 v0, v17

    #@7e1
    move/from16 v1, v18

    #@7e3
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7e6
    move-result-object v17

    #@7e7
    const-string v18, "46002"

    #@7e9
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@7ec
    move-result v17

    #@7ed
    if-eqz v17, :cond_843

    #@7ef
    :cond_7ef
    if-eqz v12, :cond_890

    #@7f1
    if-eqz v13, :cond_890

    #@7f3
    const/16 v17, 0x0

    #@7f5
    const/16 v18, 0x5

    #@7f7
    move/from16 v0, v17

    #@7f9
    move/from16 v1, v18

    #@7fb
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7fe
    move-result-object v17

    #@7ff
    const-string v18, "24002"

    #@801
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@804
    move-result v17

    #@805
    if-eqz v17, :cond_81b

    #@807
    const/16 v17, 0x0

    #@809
    const/16 v18, 0x5

    #@80b
    move/from16 v0, v17

    #@80d
    move/from16 v1, v18

    #@80f
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@812
    move-result-object v17

    #@813
    const-string v18, "24004"

    #@815
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@818
    move-result v17

    #@819
    if-nez v17, :cond_890

    #@81b
    :cond_81b
    const/16 v17, 0x0

    #@81d
    const/16 v18, 0x5

    #@81f
    move/from16 v0, v17

    #@821
    move/from16 v1, v18

    #@823
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@826
    move-result-object v17

    #@827
    const-string v18, "24002"

    #@829
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@82c
    move-result v17

    #@82d
    if-eqz v17, :cond_843

    #@82f
    const/16 v17, 0x0

    #@831
    const/16 v18, 0x5

    #@833
    move/from16 v0, v17

    #@835
    move/from16 v1, v18

    #@837
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@83a
    move-result-object v17

    #@83b
    const-string v18, "24004"

    #@83d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@840
    move-result v17

    #@841
    if-nez v17, :cond_890

    #@843
    .line 3679
    :cond_843
    const-string v17, "LGE_data"

    #@845
    new-instance v18, Ljava/lang/StringBuilder;

    #@847
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@84a
    const-string v19, "skip SPN check for-VPLMN:"

    #@84c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84f
    move-result-object v18

    #@850
    move-object/from16 v0, v18

    #@852
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@855
    move-result-object v18

    #@856
    const-string v19, "HPLMN:"

    #@858
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85b
    move-result-object v18

    #@85c
    move-object/from16 v0, v18

    #@85e
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@861
    move-result-object v18

    #@862
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@865
    move-result-object v18

    #@866
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@869
    .line 3680
    const-string v17, "LGE_data"

    #@86b
    new-instance v18, Ljava/lang/StringBuilder;

    #@86d
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@870
    const-string v19, "Sharath:isroaming skip SPN check for-VPLMN:"

    #@872
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@875
    move-result-object v18

    #@876
    move-object/from16 v0, v18

    #@878
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87b
    move-result-object v18

    #@87c
    const-string v19, "HPLMN:"

    #@87e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@881
    move-result-object v18

    #@882
    move-object/from16 v0, v18

    #@884
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@887
    move-result-object v18

    #@888
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88b
    move-result-object v18

    #@88c
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@88f
    .line 3681
    const/4 v6, 0x1

    #@890
    .line 3688
    :cond_890
    const-string v17, "US"

    #@892
    const-string v18, "TMO"

    #@894
    invoke-static/range {v17 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@897
    move-result v17

    #@898
    if-eqz v17, :cond_bb4

    #@89a
    .line 3689
    if-eqz v12, :cond_bb4

    #@89c
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@89f
    move-result v17

    #@8a0
    const/16 v18, 0x5

    #@8a2
    move/from16 v0, v17

    #@8a4
    move/from16 v1, v18

    #@8a6
    if-lt v0, v1, :cond_bb4

    #@8a8
    if-eqz v13, :cond_bb4

    #@8aa
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@8ad
    move-result v17

    #@8ae
    const/16 v18, 0x5

    #@8b0
    move/from16 v0, v17

    #@8b2
    move/from16 v1, v18

    #@8b4
    if-lt v0, v1, :cond_bb4

    #@8b6
    const/16 v17, 0x0

    #@8b8
    const/16 v18, 0x5

    #@8ba
    move/from16 v0, v17

    #@8bc
    move/from16 v1, v18

    #@8be
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@8c1
    move-result-object v17

    #@8c2
    const-string v18, "00101"

    #@8c4
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@8c7
    move-result v17

    #@8c8
    if-eqz v17, :cond_a52

    #@8ca
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@8cd
    move-result v17

    #@8ce
    const/16 v18, 0x6

    #@8d0
    move/from16 v0, v17

    #@8d2
    move/from16 v1, v18

    #@8d4
    if-ne v0, v1, :cond_bb4

    #@8d6
    const/16 v17, 0x0

    #@8d8
    const/16 v18, 0x6

    #@8da
    move/from16 v0, v17

    #@8dc
    move/from16 v1, v18

    #@8de
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@8e1
    move-result-object v17

    #@8e2
    const-string v18, "310160"

    #@8e4
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@8e7
    move-result v17

    #@8e8
    if-eqz v17, :cond_a52

    #@8ea
    const/16 v17, 0x0

    #@8ec
    const/16 v18, 0x6

    #@8ee
    move/from16 v0, v17

    #@8f0
    move/from16 v1, v18

    #@8f2
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@8f5
    move-result-object v17

    #@8f6
    const-string v18, "310200"

    #@8f8
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@8fb
    move-result v17

    #@8fc
    if-eqz v17, :cond_a52

    #@8fe
    const/16 v17, 0x0

    #@900
    const/16 v18, 0x6

    #@902
    move/from16 v0, v17

    #@904
    move/from16 v1, v18

    #@906
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@909
    move-result-object v17

    #@90a
    const-string v18, "310210"

    #@90c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@90f
    move-result v17

    #@910
    if-eqz v17, :cond_a52

    #@912
    const/16 v17, 0x0

    #@914
    const/16 v18, 0x6

    #@916
    move/from16 v0, v17

    #@918
    move/from16 v1, v18

    #@91a
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@91d
    move-result-object v17

    #@91e
    const-string v18, "310220"

    #@920
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@923
    move-result v17

    #@924
    if-eqz v17, :cond_a52

    #@926
    const/16 v17, 0x0

    #@928
    const/16 v18, 0x6

    #@92a
    move/from16 v0, v17

    #@92c
    move/from16 v1, v18

    #@92e
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@931
    move-result-object v17

    #@932
    const-string v18, "310230"

    #@934
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@937
    move-result v17

    #@938
    if-eqz v17, :cond_a52

    #@93a
    const/16 v17, 0x0

    #@93c
    const/16 v18, 0x6

    #@93e
    move/from16 v0, v17

    #@940
    move/from16 v1, v18

    #@942
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@945
    move-result-object v17

    #@946
    const-string v18, "310240"

    #@948
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@94b
    move-result v17

    #@94c
    if-eqz v17, :cond_a52

    #@94e
    const/16 v17, 0x0

    #@950
    const/16 v18, 0x6

    #@952
    move/from16 v0, v17

    #@954
    move/from16 v1, v18

    #@956
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@959
    move-result-object v17

    #@95a
    const-string v18, "310250"

    #@95c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@95f
    move-result v17

    #@960
    if-eqz v17, :cond_a52

    #@962
    const/16 v17, 0x0

    #@964
    const/16 v18, 0x6

    #@966
    move/from16 v0, v17

    #@968
    move/from16 v1, v18

    #@96a
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@96d
    move-result-object v17

    #@96e
    const-string v18, "310260"

    #@970
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@973
    move-result v17

    #@974
    if-eqz v17, :cond_a52

    #@976
    const/16 v17, 0x0

    #@978
    const/16 v18, 0x6

    #@97a
    move/from16 v0, v17

    #@97c
    move/from16 v1, v18

    #@97e
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@981
    move-result-object v17

    #@982
    const-string v18, "310270"

    #@984
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@987
    move-result v17

    #@988
    if-eqz v17, :cond_a52

    #@98a
    const/16 v17, 0x0

    #@98c
    const/16 v18, 0x6

    #@98e
    move/from16 v0, v17

    #@990
    move/from16 v1, v18

    #@992
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@995
    move-result-object v17

    #@996
    const-string v18, "310300"

    #@998
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@99b
    move-result v17

    #@99c
    if-eqz v17, :cond_a52

    #@99e
    const/16 v17, 0x0

    #@9a0
    const/16 v18, 0x6

    #@9a2
    move/from16 v0, v17

    #@9a4
    move/from16 v1, v18

    #@9a6
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9a9
    move-result-object v17

    #@9aa
    const-string v18, "310310"

    #@9ac
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@9af
    move-result v17

    #@9b0
    if-eqz v17, :cond_a52

    #@9b2
    const/16 v17, 0x0

    #@9b4
    const/16 v18, 0x6

    #@9b6
    move/from16 v0, v17

    #@9b8
    move/from16 v1, v18

    #@9ba
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9bd
    move-result-object v17

    #@9be
    const-string v18, "310490"

    #@9c0
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@9c3
    move-result v17

    #@9c4
    if-eqz v17, :cond_a52

    #@9c6
    const/16 v17, 0x0

    #@9c8
    const/16 v18, 0x6

    #@9ca
    move/from16 v0, v17

    #@9cc
    move/from16 v1, v18

    #@9ce
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9d1
    move-result-object v17

    #@9d2
    const-string v18, "310530"

    #@9d4
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@9d7
    move-result v17

    #@9d8
    if-eqz v17, :cond_a52

    #@9da
    const/16 v17, 0x0

    #@9dc
    const/16 v18, 0x6

    #@9de
    move/from16 v0, v17

    #@9e0
    move/from16 v1, v18

    #@9e2
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9e5
    move-result-object v17

    #@9e6
    const-string v18, "310580"

    #@9e8
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@9eb
    move-result v17

    #@9ec
    if-eqz v17, :cond_a52

    #@9ee
    const/16 v17, 0x0

    #@9f0
    const/16 v18, 0x6

    #@9f2
    move/from16 v0, v17

    #@9f4
    move/from16 v1, v18

    #@9f6
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9f9
    move-result-object v17

    #@9fa
    const-string v18, "310590"

    #@9fc
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@9ff
    move-result v17

    #@a00
    if-eqz v17, :cond_a52

    #@a02
    const/16 v17, 0x0

    #@a04
    const/16 v18, 0x6

    #@a06
    move/from16 v0, v17

    #@a08
    move/from16 v1, v18

    #@a0a
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a0d
    move-result-object v17

    #@a0e
    const-string v18, "310640"

    #@a10
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@a13
    move-result v17

    #@a14
    if-eqz v17, :cond_a52

    #@a16
    const/16 v17, 0x0

    #@a18
    const/16 v18, 0x6

    #@a1a
    move/from16 v0, v17

    #@a1c
    move/from16 v1, v18

    #@a1e
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a21
    move-result-object v17

    #@a22
    const-string v18, "310660"

    #@a24
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@a27
    move-result v17

    #@a28
    if-eqz v17, :cond_a52

    #@a2a
    const/16 v17, 0x0

    #@a2c
    const/16 v18, 0x6

    #@a2e
    move/from16 v0, v17

    #@a30
    move/from16 v1, v18

    #@a32
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a35
    move-result-object v17

    #@a36
    const-string v18, "310800"

    #@a38
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@a3b
    move-result v17

    #@a3c
    if-eqz v17, :cond_a52

    #@a3e
    const/16 v17, 0x0

    #@a40
    const/16 v18, 0x6

    #@a42
    move/from16 v0, v17

    #@a44
    move/from16 v1, v18

    #@a46
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a49
    move-result-object v17

    #@a4a
    const-string v18, "310490"

    #@a4c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@a4f
    move-result v17

    #@a50
    if-nez v17, :cond_bb4

    #@a52
    :cond_a52
    const/16 v17, 0x0

    #@a54
    const/16 v18, 0x3

    #@a56
    move/from16 v0, v17

    #@a58
    move/from16 v1, v18

    #@a5a
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a5d
    move-result-object v17

    #@a5e
    const-string v18, "310"

    #@a60
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@a63
    move-result v17

    #@a64
    if-eqz v17, :cond_ade

    #@a66
    const/16 v17, 0x0

    #@a68
    const/16 v18, 0x3

    #@a6a
    move/from16 v0, v17

    #@a6c
    move/from16 v1, v18

    #@a6e
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a71
    move-result-object v17

    #@a72
    const-string v18, "311"

    #@a74
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@a77
    move-result v17

    #@a78
    if-eqz v17, :cond_ade

    #@a7a
    const/16 v17, 0x0

    #@a7c
    const/16 v18, 0x3

    #@a7e
    move/from16 v0, v17

    #@a80
    move/from16 v1, v18

    #@a82
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a85
    move-result-object v17

    #@a86
    const-string v18, "312"

    #@a88
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@a8b
    move-result v17

    #@a8c
    if-eqz v17, :cond_ade

    #@a8e
    const/16 v17, 0x0

    #@a90
    const/16 v18, 0x3

    #@a92
    move/from16 v0, v17

    #@a94
    move/from16 v1, v18

    #@a96
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a99
    move-result-object v17

    #@a9a
    const-string v18, "313"

    #@a9c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@a9f
    move-result v17

    #@aa0
    if-eqz v17, :cond_ade

    #@aa2
    const/16 v17, 0x0

    #@aa4
    const/16 v18, 0x3

    #@aa6
    move/from16 v0, v17

    #@aa8
    move/from16 v1, v18

    #@aaa
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@aad
    move-result-object v17

    #@aae
    const-string v18, "314"

    #@ab0
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@ab3
    move-result v17

    #@ab4
    if-eqz v17, :cond_ade

    #@ab6
    const/16 v17, 0x0

    #@ab8
    const/16 v18, 0x3

    #@aba
    move/from16 v0, v17

    #@abc
    move/from16 v1, v18

    #@abe
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ac1
    move-result-object v17

    #@ac2
    const-string v18, "315"

    #@ac4
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@ac7
    move-result v17

    #@ac8
    if-eqz v17, :cond_ade

    #@aca
    const/16 v17, 0x0

    #@acc
    const/16 v18, 0x3

    #@ace
    move/from16 v0, v17

    #@ad0
    move/from16 v1, v18

    #@ad2
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ad5
    move-result-object v17

    #@ad6
    const-string v18, "316"

    #@ad8
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@adb
    move-result v17

    #@adc
    if-nez v17, :cond_bb4

    #@ade
    :cond_ade
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@ae1
    move-result v17

    #@ae2
    const/16 v18, 0x6

    #@ae4
    move/from16 v0, v17

    #@ae6
    move/from16 v1, v18

    #@ae8
    if-ne v0, v1, :cond_bb2

    #@aea
    const/16 v17, 0x0

    #@aec
    const/16 v18, 0x6

    #@aee
    move/from16 v0, v17

    #@af0
    move/from16 v1, v18

    #@af2
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@af5
    move-result-object v17

    #@af6
    const-string v18, "310032"

    #@af8
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@afb
    move-result v17

    #@afc
    if-eqz v17, :cond_bb4

    #@afe
    const/16 v17, 0x0

    #@b00
    const/16 v18, 0x6

    #@b02
    move/from16 v0, v17

    #@b04
    move/from16 v1, v18

    #@b06
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b09
    move-result-object v17

    #@b0a
    const-string v18, "310140"

    #@b0c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@b0f
    move-result v17

    #@b10
    if-eqz v17, :cond_bb4

    #@b12
    const/16 v17, 0x0

    #@b14
    const/16 v18, 0x6

    #@b16
    move/from16 v0, v17

    #@b18
    move/from16 v1, v18

    #@b1a
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b1d
    move-result-object v17

    #@b1e
    const-string v18, "310370"

    #@b20
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@b23
    move-result v17

    #@b24
    if-eqz v17, :cond_bb4

    #@b26
    const/16 v17, 0x0

    #@b28
    const/16 v18, 0x6

    #@b2a
    move/from16 v0, v17

    #@b2c
    move/from16 v1, v18

    #@b2e
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b31
    move-result-object v17

    #@b32
    const-string v18, "310400"

    #@b34
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@b37
    move-result v17

    #@b38
    if-eqz v17, :cond_bb4

    #@b3a
    const/16 v17, 0x0

    #@b3c
    const/16 v18, 0x6

    #@b3e
    move/from16 v0, v17

    #@b40
    move/from16 v1, v18

    #@b42
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b45
    move-result-object v17

    #@b46
    const-string v18, "310470"

    #@b48
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@b4b
    move-result v17

    #@b4c
    if-eqz v17, :cond_bb4

    #@b4e
    const/16 v17, 0x0

    #@b50
    const/16 v18, 0x6

    #@b52
    move/from16 v0, v17

    #@b54
    move/from16 v1, v18

    #@b56
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b59
    move-result-object v17

    #@b5a
    const-string v18, "310500"

    #@b5c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@b5f
    move-result v17

    #@b60
    if-eqz v17, :cond_bb4

    #@b62
    const/16 v17, 0x0

    #@b64
    const/16 v18, 0x6

    #@b66
    move/from16 v0, v17

    #@b68
    move/from16 v1, v18

    #@b6a
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b6d
    move-result-object v17

    #@b6e
    const-string v18, "310970"

    #@b70
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@b73
    move-result v17

    #@b74
    if-eqz v17, :cond_bb4

    #@b76
    const/16 v17, 0x0

    #@b78
    const/16 v18, 0x6

    #@b7a
    move/from16 v0, v17

    #@b7c
    move/from16 v1, v18

    #@b7e
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b81
    move-result-object v17

    #@b82
    const-string v18, "310033"

    #@b84
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@b87
    move-result v17

    #@b88
    if-eqz v17, :cond_bb4

    #@b8a
    const/16 v17, 0x0

    #@b8c
    const/16 v18, 0x6

    #@b8e
    move/from16 v0, v17

    #@b90
    move/from16 v1, v18

    #@b92
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b95
    move-result-object v17

    #@b96
    const-string v18, "311170"

    #@b98
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@b9b
    move-result v17

    #@b9c
    if-eqz v17, :cond_bb4

    #@b9e
    const/16 v17, 0x0

    #@ba0
    const/16 v18, 0x6

    #@ba2
    move/from16 v0, v17

    #@ba4
    move/from16 v1, v18

    #@ba6
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ba9
    move-result-object v17

    #@baa
    const-string v18, "311250"

    #@bac
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@baf
    move-result v17

    #@bb0
    if-eqz v17, :cond_bb4

    #@bb2
    .line 3730
    :cond_bb2
    const/4 v6, 0x1

    #@bb3
    .line 3731
    const/4 v4, 0x1

    #@bb4
    .line 3738
    :cond_bb4
    const-string v17, "ro.build.target_region"

    #@bb6
    invoke-static/range {v17 .. v17}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@bb9
    move-result-object v17

    #@bba
    const-string v18, "SCA"

    #@bbc
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bbf
    move-result v17

    #@bc0
    if-eqz v17, :cond_c63

    #@bc2
    .line 3739
    if-eqz v12, :cond_c63

    #@bc4
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@bc7
    move-result v17

    #@bc8
    const/16 v18, 0x5

    #@bca
    move/from16 v0, v17

    #@bcc
    move/from16 v1, v18

    #@bce
    if-lt v0, v1, :cond_c63

    #@bd0
    if-eqz v13, :cond_c63

    #@bd2
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@bd5
    move-result v17

    #@bd6
    const/16 v18, 0x5

    #@bd8
    move/from16 v0, v17

    #@bda
    move/from16 v1, v18

    #@bdc
    if-lt v0, v1, :cond_c63

    #@bde
    .line 3742
    const/16 v17, 0x0

    #@be0
    const/16 v18, 0x5

    #@be2
    move/from16 v0, v17

    #@be4
    move/from16 v1, v18

    #@be6
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@be9
    move-result-object v14

    #@bea
    .line 3743
    .local v14, simNumeric5:Ljava/lang/String;
    const/16 v17, 0x0

    #@bec
    const/16 v18, 0x5

    #@bee
    move/from16 v0, v17

    #@bf0
    move/from16 v1, v18

    #@bf2
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@bf5
    move-result-object v10

    #@bf6
    .line 3744
    .local v10, opNumeric5:Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@bf9
    move-result v17

    #@bfa
    const/16 v18, 0x6

    #@bfc
    move/from16 v0, v17

    #@bfe
    move/from16 v1, v18

    #@c00
    if-lt v0, v1, :cond_cc0

    #@c02
    const/16 v17, 0x0

    #@c04
    const/16 v18, 0x6

    #@c06
    move/from16 v0, v17

    #@c08
    move/from16 v1, v18

    #@c0a
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@c0d
    move-result-object v15

    #@c0e
    .line 3745
    .local v15, simNumeric6:Ljava/lang/String;
    :goto_c0e
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@c11
    move-result v17

    #@c12
    const/16 v18, 0x6

    #@c14
    move/from16 v0, v17

    #@c16
    move/from16 v1, v18

    #@c18
    if-lt v0, v1, :cond_cc3

    #@c1a
    const/16 v17, 0x0

    #@c1c
    const/16 v18, 0x6

    #@c1e
    move/from16 v0, v17

    #@c20
    move/from16 v1, v18

    #@c22
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@c25
    move-result-object v11

    #@c26
    .line 3748
    .local v11, opNumeric6:Ljava/lang/String;
    :goto_c26
    const-string v17, "72432"

    #@c28
    move-object/from16 v0, v17

    #@c2a
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c2d
    move-result v17

    #@c2e
    if-nez v17, :cond_c44

    #@c30
    const-string v17, "72433"

    #@c32
    move-object/from16 v0, v17

    #@c34
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c37
    move-result v17

    #@c38
    if-nez v17, :cond_c44

    #@c3a
    const-string v17, "72434"

    #@c3c
    move-object/from16 v0, v17

    #@c3e
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c41
    move-result v17

    #@c42
    if-eqz v17, :cond_cc6

    #@c44
    :cond_c44
    const-string v17, "72432"

    #@c46
    move-object/from16 v0, v17

    #@c48
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c4b
    move-result v17

    #@c4c
    if-nez v17, :cond_c62

    #@c4e
    const-string v17, "72433"

    #@c50
    move-object/from16 v0, v17

    #@c52
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c55
    move-result v17

    #@c56
    if-nez v17, :cond_c62

    #@c58
    const-string v17, "72434"

    #@c5a
    move-object/from16 v0, v17

    #@c5c
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c5f
    move-result v17

    #@c60
    if-eqz v17, :cond_cc6

    #@c62
    .line 3750
    :cond_c62
    const/4 v6, 0x1

    #@c63
    .line 3825
    .end local v10           #opNumeric5:Ljava/lang/String;
    .end local v11           #opNumeric6:Ljava/lang/String;
    .end local v14           #simNumeric5:Ljava/lang/String;
    .end local v15           #simNumeric6:Ljava/lang/String;
    :cond_c63
    :goto_c63
    if-eqz p1, :cond_f3e

    #@c65
    if-eqz v4, :cond_c6b

    #@c67
    if-nez v6, :cond_f3e

    #@c69
    if-nez v7, :cond_f3e

    #@c6b
    :cond_c6b
    const/16 v17, 0x1

    #@c6d
    goto/16 :goto_bb

    #@c6f
    .line 3570
    :cond_c6f
    if-eqz v12, :cond_15b

    #@c71
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@c74
    move-result v17

    #@c75
    const/16 v18, 0x5

    #@c77
    move/from16 v0, v17

    #@c79
    move/from16 v1, v18

    #@c7b
    if-lt v0, v1, :cond_15b

    #@c7d
    const/16 v17, 0x0

    #@c7f
    const/16 v18, 0x5

    #@c81
    move/from16 v0, v17

    #@c83
    move/from16 v1, v18

    #@c85
    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@c88
    move-result-object v17

    #@c89
    const-string v18, "51001"

    #@c8b
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@c8e
    move-result v17

    #@c8f
    if-nez v17, :cond_15b

    #@c91
    .line 3572
    move-object/from16 v0, p0

    #@c93
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@c95
    move-object/from16 v17, v0

    #@c97
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/gsm/GSMPhone;->getSubscriberId()Ljava/lang/String;

    #@c9a
    move-result-object v3

    #@c9b
    .line 3573
    .local v3, Indosat_imsi:Ljava/lang/String;
    if-eqz v3, :cond_15b

    #@c9d
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@ca0
    move-result v17

    #@ca1
    const/16 v18, 0x5

    #@ca3
    move/from16 v0, v17

    #@ca5
    move/from16 v1, v18

    #@ca7
    if-lt v0, v1, :cond_15b

    #@ca9
    const-string v17, "51021"

    #@cab
    const/16 v18, 0x0

    #@cad
    const/16 v19, 0x5

    #@caf
    move/from16 v0, v18

    #@cb1
    move/from16 v1, v19

    #@cb3
    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@cb6
    move-result-object v18

    #@cb7
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cba
    move-result v17

    #@cbb
    if-eqz v17, :cond_15b

    #@cbd
    .line 3575
    const/4 v6, 0x1

    #@cbe
    goto/16 :goto_15b

    #@cc0
    .line 3744
    .end local v3           #Indosat_imsi:Ljava/lang/String;
    .restart local v10       #opNumeric5:Ljava/lang/String;
    .restart local v14       #simNumeric5:Ljava/lang/String;
    :cond_cc0
    const/4 v15, 0x0

    #@cc1
    goto/16 :goto_c0e

    #@cc3
    .line 3745
    .restart local v15       #simNumeric6:Ljava/lang/String;
    :cond_cc3
    const/4 v11, 0x0

    #@cc4
    goto/16 :goto_c26

    #@cc6
    .line 3753
    .restart local v11       #opNumeric6:Ljava/lang/String;
    :cond_cc6
    const-string v17, "72416"

    #@cc8
    move-object/from16 v0, v17

    #@cca
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ccd
    move-result v17

    #@cce
    if-nez v17, :cond_cda

    #@cd0
    const-string v17, "72431"

    #@cd2
    move-object/from16 v0, v17

    #@cd4
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cd7
    move-result v17

    #@cd8
    if-eqz v17, :cond_cf1

    #@cda
    :cond_cda
    const-string v17, "72416"

    #@cdc
    move-object/from16 v0, v17

    #@cde
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ce1
    move-result v17

    #@ce2
    if-nez v17, :cond_cee

    #@ce4
    const-string v17, "72431"

    #@ce6
    move-object/from16 v0, v17

    #@ce8
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ceb
    move-result v17

    #@cec
    if-eqz v17, :cond_cf1

    #@cee
    .line 3755
    :cond_cee
    const/4 v6, 0x1

    #@cef
    goto/16 :goto_c63

    #@cf1
    .line 3758
    :cond_cf1
    const-string v17, "73001"

    #@cf3
    move-object/from16 v0, v17

    #@cf5
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cf8
    move-result v17

    #@cf9
    if-nez v17, :cond_d05

    #@cfb
    const-string v17, "73010"

    #@cfd
    move-object/from16 v0, v17

    #@cff
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d02
    move-result v17

    #@d03
    if-eqz v17, :cond_d1c

    #@d05
    :cond_d05
    const-string v17, "73001"

    #@d07
    move-object/from16 v0, v17

    #@d09
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d0c
    move-result v17

    #@d0d
    if-nez v17, :cond_d19

    #@d0f
    const-string v17, "73010"

    #@d11
    move-object/from16 v0, v17

    #@d13
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d16
    move-result v17

    #@d17
    if-eqz v17, :cond_d1c

    #@d19
    .line 3760
    :cond_d19
    const/4 v6, 0x1

    #@d1a
    goto/16 :goto_c63

    #@d1c
    .line 3763
    :cond_d1c
    const-string v17, "37002"

    #@d1e
    move-object/from16 v0, v17

    #@d20
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d23
    move-result v17

    #@d24
    if-nez v17, :cond_d30

    #@d26
    const-string v17, "370020"

    #@d28
    move-object/from16 v0, v17

    #@d2a
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d2d
    move-result v17

    #@d2e
    if-eqz v17, :cond_d47

    #@d30
    :cond_d30
    const-string v17, "37002"

    #@d32
    move-object/from16 v0, v17

    #@d34
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d37
    move-result v17

    #@d38
    if-nez v17, :cond_d44

    #@d3a
    const-string v17, "370020"

    #@d3c
    move-object/from16 v0, v17

    #@d3e
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d41
    move-result v17

    #@d42
    if-eqz v17, :cond_d47

    #@d44
    .line 3765
    :cond_d44
    const/4 v6, 0x1

    #@d45
    goto/16 :goto_c63

    #@d47
    .line 3768
    :cond_d47
    const-string v17, "74001"

    #@d49
    move-object/from16 v0, v17

    #@d4b
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d4e
    move-result v17

    #@d4f
    if-nez v17, :cond_d5b

    #@d51
    const-string v17, "740010"

    #@d53
    move-object/from16 v0, v17

    #@d55
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d58
    move-result v17

    #@d59
    if-eqz v17, :cond_d72

    #@d5b
    :cond_d5b
    const-string v17, "74001"

    #@d5d
    move-object/from16 v0, v17

    #@d5f
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d62
    move-result v17

    #@d63
    if-nez v17, :cond_d6f

    #@d65
    const-string v17, "740010"

    #@d67
    move-object/from16 v0, v17

    #@d69
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d6c
    move-result v17

    #@d6d
    if-eqz v17, :cond_d72

    #@d6f
    .line 3770
    :cond_d6f
    const/4 v6, 0x1

    #@d70
    goto/16 :goto_c63

    #@d72
    .line 3773
    :cond_d72
    const-string v17, "74002"

    #@d74
    move-object/from16 v0, v17

    #@d76
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d79
    move-result v17

    #@d7a
    if-eqz v17, :cond_da7

    #@d7c
    const-string v17, "74000"

    #@d7e
    move-object/from16 v0, v17

    #@d80
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d83
    move-result v17

    #@d84
    if-nez v17, :cond_da4

    #@d86
    const-string v17, "74001"

    #@d88
    move-object/from16 v0, v17

    #@d8a
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d8d
    move-result v17

    #@d8e
    if-nez v17, :cond_da4

    #@d90
    const-string v17, "740010"

    #@d92
    move-object/from16 v0, v17

    #@d94
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d97
    move-result v17

    #@d98
    if-nez v17, :cond_da4

    #@d9a
    const-string v17, "74002"

    #@d9c
    move-object/from16 v0, v17

    #@d9e
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@da1
    move-result v17

    #@da2
    if-eqz v17, :cond_da7

    #@da4
    .line 3775
    :cond_da4
    const/4 v6, 0x1

    #@da5
    goto/16 :goto_c63

    #@da7
    .line 3778
    :cond_da7
    const-string v17, "71021"

    #@da9
    move-object/from16 v0, v17

    #@dab
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dae
    move-result v17

    #@daf
    if-nez v17, :cond_dc5

    #@db1
    const-string v17, "71073"

    #@db3
    move-object/from16 v0, v17

    #@db5
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@db8
    move-result v17

    #@db9
    if-nez v17, :cond_dc5

    #@dbb
    const-string v17, "710730"

    #@dbd
    move-object/from16 v0, v17

    #@dbf
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dc2
    move-result v17

    #@dc3
    if-eqz v17, :cond_de6

    #@dc5
    :cond_dc5
    const-string v17, "71021"

    #@dc7
    move-object/from16 v0, v17

    #@dc9
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dcc
    move-result v17

    #@dcd
    if-nez v17, :cond_de3

    #@dcf
    const-string v17, "71073"

    #@dd1
    move-object/from16 v0, v17

    #@dd3
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dd6
    move-result v17

    #@dd7
    if-nez v17, :cond_de3

    #@dd9
    const-string v17, "710730"

    #@ddb
    move-object/from16 v0, v17

    #@ddd
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@de0
    move-result v17

    #@de1
    if-eqz v17, :cond_de6

    #@de3
    .line 3780
    :cond_de3
    const/4 v6, 0x1

    #@de4
    goto/16 :goto_c63

    #@de6
    .line 3783
    :cond_de6
    const-string v17, "70804"

    #@de8
    move-object/from16 v0, v17

    #@dea
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ded
    move-result v17

    #@dee
    if-nez v17, :cond_dfa

    #@df0
    const-string v17, "708040"

    #@df2
    move-object/from16 v0, v17

    #@df4
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@df7
    move-result v17

    #@df8
    if-eqz v17, :cond_e11

    #@dfa
    :cond_dfa
    const-string v17, "70804"

    #@dfc
    move-object/from16 v0, v17

    #@dfe
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e01
    move-result v17

    #@e02
    if-nez v17, :cond_e0e

    #@e04
    const-string v17, "708040"

    #@e06
    move-object/from16 v0, v17

    #@e08
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e0b
    move-result v17

    #@e0c
    if-eqz v17, :cond_e11

    #@e0e
    .line 3785
    :cond_e0e
    const/4 v6, 0x1

    #@e0f
    goto/16 :goto_c63

    #@e11
    .line 3788
    :cond_e11
    const-string v17, "70800"

    #@e13
    move-object/from16 v0, v17

    #@e15
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e18
    move-result v17

    #@e19
    if-nez v17, :cond_e25

    #@e1b
    const-string v17, "708001"

    #@e1d
    move-object/from16 v0, v17

    #@e1f
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e22
    move-result v17

    #@e23
    if-eqz v17, :cond_e3c

    #@e25
    :cond_e25
    const-string v17, "70800"

    #@e27
    move-object/from16 v0, v17

    #@e29
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e2c
    move-result v17

    #@e2d
    if-nez v17, :cond_e39

    #@e2f
    const-string v17, "708001"

    #@e31
    move-object/from16 v0, v17

    #@e33
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e36
    move-result v17

    #@e37
    if-eqz v17, :cond_e3c

    #@e39
    .line 3790
    :cond_e39
    const/4 v6, 0x1

    #@e3a
    goto/16 :goto_c63

    #@e3c
    .line 3793
    :cond_e3c
    const-string v17, "70802"

    #@e3e
    move-object/from16 v0, v17

    #@e40
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e43
    move-result v17

    #@e44
    if-nez v17, :cond_e50

    #@e46
    const-string v17, "708020"

    #@e48
    move-object/from16 v0, v17

    #@e4a
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e4d
    move-result v17

    #@e4e
    if-eqz v17, :cond_e67

    #@e50
    :cond_e50
    const-string v17, "70802"

    #@e52
    move-object/from16 v0, v17

    #@e54
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e57
    move-result v17

    #@e58
    if-nez v17, :cond_e64

    #@e5a
    const-string v17, "708020"

    #@e5c
    move-object/from16 v0, v17

    #@e5e
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e61
    move-result v17

    #@e62
    if-eqz v17, :cond_e67

    #@e64
    .line 3795
    :cond_e64
    const/4 v6, 0x1

    #@e65
    goto/16 :goto_c63

    #@e67
    .line 3798
    :cond_e67
    const-string v17, "70604"

    #@e69
    move-object/from16 v0, v17

    #@e6b
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e6e
    move-result v17

    #@e6f
    if-nez v17, :cond_e7b

    #@e71
    const-string v17, "706040"

    #@e73
    move-object/from16 v0, v17

    #@e75
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e78
    move-result v17

    #@e79
    if-eqz v17, :cond_e92

    #@e7b
    :cond_e7b
    const-string v17, "70604"

    #@e7d
    move-object/from16 v0, v17

    #@e7f
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e82
    move-result v17

    #@e83
    if-nez v17, :cond_e8f

    #@e85
    const-string v17, "706040"

    #@e87
    move-object/from16 v0, v17

    #@e89
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e8c
    move-result v17

    #@e8d
    if-eqz v17, :cond_e92

    #@e8f
    .line 3800
    :cond_e8f
    const/4 v6, 0x1

    #@e90
    goto/16 :goto_c63

    #@e92
    .line 3803
    :cond_e92
    const-string v17, "70403"

    #@e94
    move-object/from16 v0, v17

    #@e96
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e99
    move-result v17

    #@e9a
    if-nez v17, :cond_ea6

    #@e9c
    const-string v17, "704030"

    #@e9e
    move-object/from16 v0, v17

    #@ea0
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ea3
    move-result v17

    #@ea4
    if-eqz v17, :cond_ebd

    #@ea6
    :cond_ea6
    const-string v17, "70403"

    #@ea8
    move-object/from16 v0, v17

    #@eaa
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ead
    move-result v17

    #@eae
    if-nez v17, :cond_eba

    #@eb0
    const-string v17, "704030"

    #@eb2
    move-object/from16 v0, v17

    #@eb4
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@eb7
    move-result v17

    #@eb8
    if-eqz v17, :cond_ebd

    #@eba
    .line 3805
    :cond_eba
    const/4 v6, 0x1

    #@ebb
    goto/16 :goto_c63

    #@ebd
    .line 3808
    :cond_ebd
    const-string v17, "33403"

    #@ebf
    move-object/from16 v0, v17

    #@ec1
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ec4
    move-result v17

    #@ec5
    if-nez v17, :cond_ed1

    #@ec7
    const-string v17, "334030"

    #@ec9
    move-object/from16 v0, v17

    #@ecb
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ece
    move-result v17

    #@ecf
    if-eqz v17, :cond_ee8

    #@ed1
    :cond_ed1
    const-string v17, "33403"

    #@ed3
    move-object/from16 v0, v17

    #@ed5
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ed8
    move-result v17

    #@ed9
    if-nez v17, :cond_ee5

    #@edb
    const-string v17, "334030"

    #@edd
    move-object/from16 v0, v17

    #@edf
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ee2
    move-result v17

    #@ee3
    if-eqz v17, :cond_ee8

    #@ee5
    .line 3810
    :cond_ee5
    const/4 v6, 0x1

    #@ee6
    goto/16 :goto_c63

    #@ee8
    .line 3813
    :cond_ee8
    const-string v17, "71030"

    #@eea
    move-object/from16 v0, v17

    #@eec
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@eef
    move-result v17

    #@ef0
    if-nez v17, :cond_efc

    #@ef2
    const-string v17, "710300"

    #@ef4
    move-object/from16 v0, v17

    #@ef6
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ef9
    move-result v17

    #@efa
    if-eqz v17, :cond_f13

    #@efc
    :cond_efc
    const-string v17, "71030"

    #@efe
    move-object/from16 v0, v17

    #@f00
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f03
    move-result v17

    #@f04
    if-nez v17, :cond_f10

    #@f06
    const-string v17, "710300"

    #@f08
    move-object/from16 v0, v17

    #@f0a
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f0d
    move-result v17

    #@f0e
    if-eqz v17, :cond_f13

    #@f10
    .line 3815
    :cond_f10
    const/4 v6, 0x1

    #@f11
    goto/16 :goto_c63

    #@f13
    .line 3818
    :cond_f13
    const-string v17, "71402"

    #@f15
    move-object/from16 v0, v17

    #@f17
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f1a
    move-result v17

    #@f1b
    if-nez v17, :cond_f27

    #@f1d
    const-string v17, "714020"

    #@f1f
    move-object/from16 v0, v17

    #@f21
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f24
    move-result v17

    #@f25
    if-eqz v17, :cond_c63

    #@f27
    :cond_f27
    const-string v17, "71402"

    #@f29
    move-object/from16 v0, v17

    #@f2b
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f2e
    move-result v17

    #@f2f
    if-nez v17, :cond_f3b

    #@f31
    const-string v17, "714020"

    #@f33
    move-object/from16 v0, v17

    #@f35
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f38
    move-result v17

    #@f39
    if-eqz v17, :cond_c63

    #@f3b
    .line 3820
    :cond_f3b
    const/4 v6, 0x1

    #@f3c
    goto/16 :goto_c63

    #@f3e
    .line 3825
    .end local v10           #opNumeric5:Ljava/lang/String;
    .end local v11           #opNumeric6:Ljava/lang/String;
    .end local v14           #simNumeric5:Ljava/lang/String;
    .end local v15           #simNumeric6:Ljava/lang/String;
    :cond_f3e
    const/16 v17, 0x0

    #@f40
    goto/16 :goto_bb
.end method

.method private isVZWAdminDisabled()Z
    .registers 10

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 5100
    const/4 v7, 0x0

    #@2
    .line 5101
    .local v7, isDisabled:Z
    const/4 v3, 0x0

    #@3
    .line 5102
    .local v3, selection:Ljava/lang/String;
    const-string v0, "gsm.sim.operator.numeric"

    #@5
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v8

    #@9
    .line 5104
    .local v8, operator:Ljava/lang/String;
    const-string v0, "311480"

    #@b
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_13

    #@11
    .line 5105
    const/4 v0, 0x0

    #@12
    .line 5124
    :goto_12
    return v0

    #@13
    .line 5108
    :cond_13
    new-instance v0, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v1, "numeric = \'"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    const-string v1, "\'"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    const-string v1, " and type = \'admin\'"

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    .line 5109
    new-instance v0, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, " and carrier_enabled = 1"

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    .line 5111
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@47
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@4a
    move-result-object v0

    #@4b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4e
    move-result-object v0

    #@4f
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@51
    move-object v4, v2

    #@52
    move-object v5, v2

    #@53
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@56
    move-result-object v6

    #@57
    .line 5114
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_63

    #@59
    .line 5115
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@5c
    move-result v0

    #@5d
    if-lez v0, :cond_7b

    #@5f
    .line 5116
    const/4 v7, 0x0

    #@60
    .line 5120
    :goto_60
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@63
    .line 5123
    :cond_63
    new-instance v0, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v1, "isVZWAdminDisabled() isDisabled = "

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@79
    move v0, v7

    #@7a
    .line 5124
    goto :goto_12

    #@7b
    .line 5118
    :cond_7b
    const/4 v7, 0x1

    #@7c
    goto :goto_60
.end method

.method private isVZWAdminEnabled()Z
    .registers 10

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 5128
    const/4 v7, 0x0

    #@2
    .line 5129
    .local v7, isEnabled:Z
    const/4 v3, 0x0

    #@3
    .line 5130
    .local v3, selection:Ljava/lang/String;
    const-string v0, "gsm.sim.operator.numeric"

    #@5
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v8

    #@9
    .line 5132
    .local v8, operator:Ljava/lang/String;
    const-string v0, "311480"

    #@b
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_13

    #@11
    .line 5133
    const/4 v0, 0x0

    #@12
    .line 5152
    :goto_12
    return v0

    #@13
    .line 5136
    :cond_13
    new-instance v0, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v1, "numeric = \'"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    const-string v1, "\'"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    const-string v1, " and type = \'admin\'"

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    .line 5137
    new-instance v0, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, " and carrier_enabled = 0"

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    .line 5139
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@47
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@4a
    move-result-object v0

    #@4b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4e
    move-result-object v0

    #@4f
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@51
    move-object v4, v2

    #@52
    move-object v5, v2

    #@53
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@56
    move-result-object v6

    #@57
    .line 5142
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_63

    #@59
    .line 5143
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@5c
    move-result v0

    #@5d
    if-lez v0, :cond_7b

    #@5f
    .line 5144
    const/4 v7, 0x0

    #@60
    .line 5148
    :goto_60
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@63
    .line 5151
    :cond_63
    new-instance v0, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v1, "isVZWAdminEnabled() isEnabled = "

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@79
    move v0, v7

    #@7a
    .line 5152
    goto :goto_12

    #@7b
    .line 5146
    :cond_7b
    const/4 v7, 0x1

    #@7c
    goto :goto_60
.end method

.method private onRestrictedStateChanged(Landroid/os/AsyncResult;)V
    .registers 12
    .parameter "ar"

    #@0
    .prologue
    const/16 v9, 0x3ed

    #@2
    const/16 v8, 0x3ec

    #@4
    const/16 v7, 0x3eb

    #@6
    const/4 v5, 0x1

    #@7
    const/4 v4, 0x0

    #@8
    .line 3321
    new-instance v1, Lcom/android/internal/telephony/RestrictedState;

    #@a
    invoke-direct {v1}, Lcom/android/internal/telephony/RestrictedState;-><init>()V

    #@d
    .line 3323
    .local v1, newRs:Lcom/android/internal/telephony/RestrictedState;
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v6, "onRestrictedStateChanged: E rs "

    #@14
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    iget-object v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@1a
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@25
    .line 3325
    iget-object v3, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@27
    if-nez v3, :cond_a6

    #@29
    .line 3326
    iget-object v3, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2b
    check-cast v3, [I

    #@2d
    move-object v0, v3

    #@2e
    check-cast v0, [I

    #@30
    .line 3327
    .local v0, ints:[I
    aget v2, v0, v4

    #@32
    .line 3329
    .local v2, state:I
    and-int/lit8 v3, v2, 0x1

    #@34
    if-nez v3, :cond_3a

    #@36
    and-int/lit8 v3, v2, 0x4

    #@38
    if-eqz v3, :cond_bf

    #@3a
    :cond_3a
    move v3, v5

    #@3b
    :goto_3b
    invoke-virtual {v1, v3}, Lcom/android/internal/telephony/RestrictedState;->setCsEmergencyRestricted(Z)V

    #@3e
    .line 3333
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@40
    if-eqz v3, :cond_5f

    #@42
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@44
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@47
    move-result-object v3

    #@48
    sget-object v6, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@4a
    if-ne v3, v6, :cond_5f

    #@4c
    .line 3334
    and-int/lit8 v3, v2, 0x2

    #@4e
    if-nez v3, :cond_54

    #@50
    and-int/lit8 v3, v2, 0x4

    #@52
    if-eqz v3, :cond_c2

    #@54
    :cond_54
    move v3, v5

    #@55
    :goto_55
    invoke-virtual {v1, v3}, Lcom/android/internal/telephony/RestrictedState;->setCsNormalRestricted(Z)V

    #@58
    .line 3337
    and-int/lit8 v3, v2, 0x10

    #@5a
    if-eqz v3, :cond_c4

    #@5c
    :goto_5c
    invoke-virtual {v1, v5}, Lcom/android/internal/telephony/RestrictedState;->setPsRestricted(Z)V

    #@5f
    .line 3341
    :cond_5f
    new-instance v3, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v4, "onRestrictedStateChanged: new rs "

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@75
    .line 3343
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@77
    invoke-virtual {v3}, Lcom/android/internal/telephony/RestrictedState;->isPsRestricted()Z

    #@7a
    move-result v3

    #@7b
    if-nez v3, :cond_c6

    #@7d
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isPsRestricted()Z

    #@80
    move-result v3

    #@81
    if-eqz v3, :cond_c6

    #@83
    .line 3344
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPsRestrictEnabledRegistrants:Landroid/os/RegistrantList;

    #@85
    invoke-virtual {v3}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@88
    .line 3345
    const/16 v3, 0x3e9

    #@8a
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@8d
    .line 3356
    :cond_8d
    :goto_8d
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@8f
    invoke-virtual {v3}, Lcom/android/internal/telephony/RestrictedState;->isCsRestricted()Z

    #@92
    move-result v3

    #@93
    if-eqz v3, :cond_f5

    #@95
    .line 3359
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsEmergencyRestricted()Z

    #@98
    move-result v3

    #@99
    if-nez v3, :cond_df

    #@9b
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsNormalRestricted()Z

    #@9e
    move-result v3

    #@9f
    if-nez v3, :cond_df

    #@a1
    .line 3362
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@a4
    .line 3413
    :cond_a4
    :goto_a4
    iput-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@a6
    .line 3415
    .end local v0           #ints:[I
    .end local v2           #state:I
    :cond_a6
    new-instance v3, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v4, "onRestrictedStateChanged: X rs "

    #@ad
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v3

    #@b1
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@b3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v3

    #@b7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v3

    #@bb
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@be
    .line 3416
    return-void

    #@bf
    .restart local v0       #ints:[I
    .restart local v2       #state:I
    :cond_bf
    move v3, v4

    #@c0
    .line 3329
    goto/16 :goto_3b

    #@c2
    :cond_c2
    move v3, v4

    #@c3
    .line 3334
    goto :goto_55

    #@c4
    :cond_c4
    move v5, v4

    #@c5
    .line 3337
    goto :goto_5c

    #@c6
    .line 3346
    :cond_c6
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@c8
    invoke-virtual {v3}, Lcom/android/internal/telephony/RestrictedState;->isPsRestricted()Z

    #@cb
    move-result v3

    #@cc
    if-eqz v3, :cond_8d

    #@ce
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isPsRestricted()Z

    #@d1
    move-result v3

    #@d2
    if-nez v3, :cond_8d

    #@d4
    .line 3347
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPsRestrictDisabledRegistrants:Landroid/os/RegistrantList;

    #@d6
    invoke-virtual {v3}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@d9
    .line 3348
    const/16 v3, 0x3ea

    #@db
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@de
    goto :goto_8d

    #@df
    .line 3363
    :cond_df
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsNormalRestricted()Z

    #@e2
    move-result v3

    #@e3
    if-nez v3, :cond_eb

    #@e5
    .line 3365
    const/16 v3, 0x3ee

    #@e7
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@ea
    goto :goto_a4

    #@eb
    .line 3366
    :cond_eb
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsEmergencyRestricted()Z

    #@ee
    move-result v3

    #@ef
    if-nez v3, :cond_a4

    #@f1
    .line 3368
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@f4
    goto :goto_a4

    #@f5
    .line 3370
    :cond_f5
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@f7
    invoke-virtual {v3}, Lcom/android/internal/telephony/RestrictedState;->isCsEmergencyRestricted()Z

    #@fa
    move-result v3

    #@fb
    if-eqz v3, :cond_12a

    #@fd
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@ff
    invoke-virtual {v3}, Lcom/android/internal/telephony/RestrictedState;->isCsNormalRestricted()Z

    #@102
    move-result v3

    #@103
    if-nez v3, :cond_12a

    #@105
    .line 3374
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsEmergencyRestricted()Z

    #@108
    move-result v3

    #@109
    if-nez v3, :cond_115

    #@10b
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsNormalRestricted()Z

    #@10e
    move-result v3

    #@10f
    if-nez v3, :cond_115

    #@111
    .line 3377
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@114
    goto :goto_a4

    #@115
    .line 3378
    :cond_115
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsRestricted()Z

    #@118
    move-result v3

    #@119
    if-eqz v3, :cond_11f

    #@11b
    .line 3380
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@11e
    goto :goto_a4

    #@11f
    .line 3381
    :cond_11f
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsNormalRestricted()Z

    #@122
    move-result v3

    #@123
    if-eqz v3, :cond_a4

    #@125
    .line 3383
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@128
    goto/16 :goto_a4

    #@12a
    .line 3385
    :cond_12a
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@12c
    invoke-virtual {v3}, Lcom/android/internal/telephony/RestrictedState;->isCsEmergencyRestricted()Z

    #@12f
    move-result v3

    #@130
    if-nez v3, :cond_163

    #@132
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@134
    invoke-virtual {v3}, Lcom/android/internal/telephony/RestrictedState;->isCsNormalRestricted()Z

    #@137
    move-result v3

    #@138
    if-eqz v3, :cond_163

    #@13a
    .line 3389
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsEmergencyRestricted()Z

    #@13d
    move-result v3

    #@13e
    if-nez v3, :cond_14b

    #@140
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsNormalRestricted()Z

    #@143
    move-result v3

    #@144
    if-nez v3, :cond_14b

    #@146
    .line 3392
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@149
    goto/16 :goto_a4

    #@14b
    .line 3393
    :cond_14b
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsRestricted()Z

    #@14e
    move-result v3

    #@14f
    if-eqz v3, :cond_156

    #@151
    .line 3395
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@154
    goto/16 :goto_a4

    #@156
    .line 3396
    :cond_156
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsEmergencyRestricted()Z

    #@159
    move-result v3

    #@15a
    if-eqz v3, :cond_a4

    #@15c
    .line 3398
    const/16 v3, 0x3ee

    #@15e
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@161
    goto/16 :goto_a4

    #@163
    .line 3401
    :cond_163
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsRestricted()Z

    #@166
    move-result v3

    #@167
    if-eqz v3, :cond_16e

    #@169
    .line 3403
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@16c
    goto/16 :goto_a4

    #@16e
    .line 3404
    :cond_16e
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsEmergencyRestricted()Z

    #@171
    move-result v3

    #@172
    if-eqz v3, :cond_17b

    #@174
    .line 3406
    const/16 v3, 0x3ee

    #@176
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@179
    goto/16 :goto_a4

    #@17b
    .line 3407
    :cond_17b
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isCsNormalRestricted()Z

    #@17e
    move-result v3

    #@17f
    if-eqz v3, :cond_a4

    #@181
    .line 3409
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@184
    goto/16 :goto_a4
.end method

.method private onSetEhrpdInfo(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    .line 4353
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2
    if-eqz v2, :cond_a

    #@4
    .line 4354
    const-string v2, "onEhrpdInfoReceived, there is Exception"

    #@6
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@9
    .line 4365
    :goto_9
    return-void

    #@a
    .line 4357
    :cond_a
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c
    check-cast v2, Ljava/lang/String;

    #@e
    move-object v1, v2

    #@f
    check-cast v1, Ljava/lang/String;

    #@11
    .line 4358
    .local v1, result:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "[IMS_AFW] GET EHRPD Info: "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@27
    .line 4360
    const/4 v0, 0x0

    #@28
    .local v0, i:I
    :goto_28
    const-string v2, ":"

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    array-length v2, v2

    #@2f
    if-ge v0, v2, :cond_40

    #@31
    .line 4361
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mEhrpdInfo:[Ljava/lang/String;

    #@33
    const-string v3, ":"

    #@35
    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    aget-object v3, v3, v0

    #@3b
    aput-object v3, v2, v0

    #@3d
    .line 4360
    add-int/lit8 v0, v0, 0x1

    #@3f
    goto :goto_28

    #@40
    .line 4363
    :cond_40
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v3, "[IMS_AFW] Sector ID : "

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mEhrpdInfo:[Ljava/lang/String;

    #@4d
    const/4 v4, 0x0

    #@4e
    aget-object v3, v3, v4

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    const-string v3, ", Subnet length : "

    #@56
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v2

    #@5a
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mEhrpdInfo:[Ljava/lang/String;

    #@5c
    const/4 v4, 0x1

    #@5d
    aget-object v3, v3, v4

    #@5f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v2

    #@63
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@6a
    goto :goto_9
.end method

.method private onSetLteInfo(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    .line 4337
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2
    if-eqz v2, :cond_a

    #@4
    .line 4338
    const-string v2, "onLteInfoReceived, there is Exception"

    #@6
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@9
    .line 4350
    :goto_9
    return-void

    #@a
    .line 4341
    :cond_a
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c
    check-cast v2, Ljava/lang/String;

    #@e
    move-object v1, v2

    #@f
    check-cast v1, Ljava/lang/String;

    #@11
    .line 4342
    .local v1, result:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "[IMS_AFW] GET LTE Info: "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@27
    .line 4344
    const/4 v0, 0x0

    #@28
    .local v0, i:I
    :goto_28
    const-string v2, ","

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    array-length v2, v2

    #@2f
    if-ge v0, v2, :cond_40

    #@31
    .line 4345
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@33
    const-string v3, ","

    #@35
    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    aget-object v3, v3, v0

    #@3b
    aput-object v3, v2, v0

    #@3d
    .line 4344
    add-int/lit8 v0, v0, 0x1

    #@3f
    goto :goto_28

    #@40
    .line 4347
    :cond_40
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v3, "[IMS_AFW] MCC : "

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@4d
    const/4 v4, 0x0

    #@4e
    aget-object v3, v3, v4

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    const-string v3, ", MNC : "

    #@56
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v2

    #@5a
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@5c
    const/4 v4, 0x1

    #@5d
    aget-object v3, v3, v4

    #@5f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v2

    #@63
    const-string v3, ", Cell ID : "

    #@65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@6b
    const/4 v4, 0x2

    #@6c
    aget-object v3, v3, v4

    #@6e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v2

    #@72
    const-string v3, ", TAC : "

    #@74
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@7a
    const/4 v4, 0x3

    #@7b
    aget-object v3, v3, v4

    #@7d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v2

    #@81
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v2

    #@85
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@88
    goto :goto_9
.end method

.method private pollState()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2564
    const/4 v0, 0x1

    #@2
    new-array v0, v0, [I

    #@4
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@6
    .line 2565
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@8
    aput v3, v0, v3

    #@a
    .line 2567
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$10;->$SwitchMap$com$android$internal$telephony$CommandsInterface$RadioState:[I

    #@c
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->ordinal()I

    #@15
    move-result v1

    #@16
    aget v0, v0, v1

    #@18
    packed-switch v0, :pswitch_data_98

    #@1b
    .line 2591
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@1d
    aget v1, v0, v3

    #@1f
    add-int/lit8 v1, v1, 0x1

    #@21
    aput v1, v0, v3

    #@23
    .line 2592
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@25
    const/4 v1, 0x6

    #@26
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@28
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2b
    move-result-object v1

    #@2c
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getOperator(Landroid/os/Message;)V

    #@2f
    .line 2596
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@31
    aget v1, v0, v3

    #@33
    add-int/lit8 v1, v1, 0x1

    #@35
    aput v1, v0, v3

    #@37
    .line 2597
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@39
    const/4 v1, 0x5

    #@3a
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@3c
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@3f
    move-result-object v1

    #@40
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getDataRegistrationState(Landroid/os/Message;)V

    #@43
    .line 2601
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@45
    aget v1, v0, v3

    #@47
    add-int/lit8 v1, v1, 0x1

    #@49
    aput v1, v0, v3

    #@4b
    .line 2602
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@4d
    const/4 v1, 0x4

    #@4e
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@50
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@53
    move-result-object v1

    #@54
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getVoiceRegistrationState(Landroid/os/Message;)V

    #@57
    .line 2606
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@59
    aget v1, v0, v3

    #@5b
    add-int/lit8 v1, v1, 0x1

    #@5d
    aput v1, v0, v3

    #@5f
    .line 2607
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@61
    const/16 v1, 0xe

    #@63
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@65
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@68
    move-result-object v1

    #@69
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getNetworkSelectionMode(Landroid/os/Message;)V

    #@6c
    .line 2612
    :goto_6c
    return-void

    #@6d
    .line 2569
    :pswitch_6d
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@6f
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@72
    .line 2570
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@74
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->setStateInvalid()V

    #@77
    .line 2571
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setSignalStrengthDefaultValues()V

    #@7a
    .line 2572
    iput-boolean v3, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGotCountryCode:Z

    #@7c
    .line 2573
    iput-boolean v3, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNitzUpdatedTime:Z

    #@7e
    .line 2574
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->pollStateDone()V

    #@81
    goto :goto_6c

    #@82
    .line 2578
    :pswitch_82
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@84
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->setStateOff()V

    #@87
    .line 2579
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@89
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->setStateInvalid()V

    #@8c
    .line 2580
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setSignalStrengthDefaultValues()V

    #@8f
    .line 2581
    iput-boolean v3, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGotCountryCode:Z

    #@91
    .line 2582
    iput-boolean v3, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNitzUpdatedTime:Z

    #@93
    .line 2583
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->pollStateDone()V

    #@96
    goto :goto_6c

    #@97
    .line 2567
    nop

    #@98
    :pswitch_data_98
    .packed-switch 0x1
        :pswitch_6d
        :pswitch_82
    .end packed-switch
.end method

.method private pollStateDone()V
    .registers 53

    #@0
    .prologue
    .line 2616
    new-instance v46, Ljava/lang/StringBuilder;

    #@2
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v47, "Poll ServiceState done:  oldSS=["

    #@7
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v46

    #@b
    move-object/from16 v0, p0

    #@d
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@f
    move-object/from16 v47, v0

    #@11
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v46

    #@15
    const-string v47, "] newSS=["

    #@17
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v46

    #@1b
    move-object/from16 v0, p0

    #@1d
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@1f
    move-object/from16 v47, v0

    #@21
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v46

    #@25
    const-string v47, "] oldGprs="

    #@27
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v46

    #@2b
    move-object/from16 v0, p0

    #@2d
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@2f
    move/from16 v47, v0

    #@31
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v46

    #@35
    const-string v47, " newData="

    #@37
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v46

    #@3b
    move-object/from16 v0, p0

    #@3d
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@3f
    move/from16 v47, v0

    #@41
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v46

    #@45
    const-string v47, " oldMaxDataCalls="

    #@47
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v46

    #@4b
    move-object/from16 v0, p0

    #@4d
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mMaxDataCalls:I

    #@4f
    move/from16 v47, v0

    #@51
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v46

    #@55
    const-string v47, " mNewMaxDataCalls="

    #@57
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v46

    #@5b
    move-object/from16 v0, p0

    #@5d
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewMaxDataCalls:I

    #@5f
    move/from16 v47, v0

    #@61
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v46

    #@65
    const-string v47, " oldReasonDataDenied="

    #@67
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v46

    #@6b
    move-object/from16 v0, p0

    #@6d
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mReasonDataDenied:I

    #@6f
    move/from16 v47, v0

    #@71
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@74
    move-result-object v46

    #@75
    const-string v47, " mNewReasonDataDenied="

    #@77
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v46

    #@7b
    move-object/from16 v0, p0

    #@7d
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewReasonDataDenied:I

    #@7f
    move/from16 v47, v0

    #@81
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@84
    move-result-object v46

    #@85
    const-string v47, " oldType="

    #@87
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v46

    #@8b
    move-object/from16 v0, p0

    #@8d
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@8f
    move/from16 v47, v0

    #@91
    invoke-static/range {v47 .. v47}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@94
    move-result-object v47

    #@95
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v46

    #@99
    const-string v47, " newType="

    #@9b
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v46

    #@9f
    move-object/from16 v0, p0

    #@a1
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@a3
    move/from16 v47, v0

    #@a5
    invoke-static/range {v47 .. v47}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@a8
    move-result-object v47

    #@a9
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v46

    #@ad
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v46

    #@b1
    move-object/from16 v0, p0

    #@b3
    move-object/from16 v1, v46

    #@b5
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@b8
    .line 2627
    move-object/from16 v0, p0

    #@ba
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@bc
    move-object/from16 v46, v0

    #@be
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getState()I

    #@c1
    move-result v46

    #@c2
    if-eqz v46, :cond_af4

    #@c4
    move-object/from16 v0, p0

    #@c6
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@c8
    move-object/from16 v46, v0

    #@ca
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getState()I

    #@cd
    move-result v46

    #@ce
    if-nez v46, :cond_af4

    #@d0
    const/16 v23, 0x1

    #@d2
    .line 2631
    .local v23, hasRegistered:Z
    :goto_d2
    move-object/from16 v0, p0

    #@d4
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@d6
    move-object/from16 v46, v0

    #@d8
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getState()I

    #@db
    move-result v46

    #@dc
    if-nez v46, :cond_af8

    #@de
    move-object/from16 v0, p0

    #@e0
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@e2
    move-object/from16 v46, v0

    #@e4
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getState()I

    #@e7
    move-result v46

    #@e8
    if-eqz v46, :cond_af8

    #@ea
    const/16 v17, 0x1

    #@ec
    .line 2635
    .local v17, hasDeregistered:Z
    :goto_ec
    move-object/from16 v0, p0

    #@ee
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@f0
    move/from16 v46, v0

    #@f2
    if-eqz v46, :cond_afc

    #@f4
    move-object/from16 v0, p0

    #@f6
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@f8
    move/from16 v46, v0

    #@fa
    if-nez v46, :cond_afc

    #@fc
    const/16 v18, 0x1

    #@fe
    .line 2641
    .local v18, hasGprsAttached:Z
    :goto_fe
    const-string v46, "JP"

    #@100
    const-string v47, "DCM"

    #@102
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@105
    move-result v46

    #@106
    if-eqz v46, :cond_129

    #@108
    move-object/from16 v0, p0

    #@10a
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->state_of_suppressing_not_registered_ind:I

    #@10c
    move/from16 v46, v0

    #@10e
    const/16 v47, 0x2

    #@110
    move/from16 v0, v46

    #@112
    move/from16 v1, v47

    #@114
    if-ne v0, v1, :cond_129

    #@116
    move-object/from16 v0, p0

    #@118
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@11a
    move/from16 v46, v0

    #@11c
    if-nez v46, :cond_129

    #@11e
    .line 2644
    const/16 v18, 0x1

    #@120
    .line 2645
    const-string v46, "NONET_SUPP - Notify PS ATTACH"

    #@122
    move-object/from16 v0, p0

    #@124
    move-object/from16 v1, v46

    #@126
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@129
    .line 2649
    :cond_129
    move-object/from16 v0, p0

    #@12b
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@12d
    move/from16 v46, v0

    #@12f
    if-nez v46, :cond_b00

    #@131
    move-object/from16 v0, p0

    #@133
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@135
    move/from16 v46, v0

    #@137
    if-eqz v46, :cond_b00

    #@139
    const/16 v19, 0x1

    #@13b
    .line 2653
    .local v19, hasGprsDetached:Z
    :goto_13b
    move-object/from16 v0, p0

    #@13d
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@13f
    move/from16 v46, v0

    #@141
    move-object/from16 v0, p0

    #@143
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@145
    move/from16 v47, v0

    #@147
    move/from16 v0, v46

    #@149
    move/from16 v1, v47

    #@14b
    if-eq v0, v1, :cond_b04

    #@14d
    const/16 v22, 0x1

    #@14f
    .line 2655
    .local v22, hasRadioTechnologyChanged:Z
    :goto_14f
    move-object/from16 v0, p0

    #@151
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@153
    move-object/from16 v46, v0

    #@155
    move-object/from16 v0, p0

    #@157
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@159
    move-object/from16 v47, v0

    #@15b
    invoke-virtual/range {v46 .. v47}, Landroid/telephony/ServiceState;->equals(Ljava/lang/Object;)Z

    #@15e
    move-result v46

    #@15f
    if-nez v46, :cond_b08

    #@161
    const/16 v16, 0x1

    #@163
    .line 2657
    .local v16, hasChanged:Z
    :goto_163
    move-object/from16 v0, p0

    #@165
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@167
    move-object/from16 v46, v0

    #@169
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@16c
    move-result v46

    #@16d
    if-nez v46, :cond_b0c

    #@16f
    move-object/from16 v0, p0

    #@171
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@173
    move-object/from16 v46, v0

    #@175
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@178
    move-result v46

    #@179
    if-eqz v46, :cond_b0c

    #@17b
    const/16 v25, 0x1

    #@17d
    .line 2659
    .local v25, hasRoamingOn:Z
    :goto_17d
    move-object/from16 v0, p0

    #@17f
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@181
    move-object/from16 v46, v0

    #@183
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@186
    move-result v46

    #@187
    if-eqz v46, :cond_b10

    #@189
    move-object/from16 v0, p0

    #@18b
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@18d
    move-object/from16 v46, v0

    #@18f
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@192
    move-result v46

    #@193
    if-nez v46, :cond_b10

    #@195
    const/16 v24, 0x1

    #@197
    .line 2661
    .local v24, hasRoamingOff:Z
    :goto_197
    move-object/from16 v0, p0

    #@199
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@19b
    move-object/from16 v46, v0

    #@19d
    move-object/from16 v0, p0

    #@19f
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@1a1
    move-object/from16 v47, v0

    #@1a3
    invoke-virtual/range {v46 .. v47}, Landroid/telephony/gsm/GsmCellLocation;->equals(Ljava/lang/Object;)Z

    #@1a6
    move-result v46

    #@1a7
    if-nez v46, :cond_b14

    #@1a9
    const/16 v20, 0x1

    #@1ab
    .line 2664
    .local v20, hasLocationChanged:Z
    :goto_1ab
    const/16 v21, 0x0

    #@1ad
    .line 2666
    .local v21, hasNotiStateChnaged:Z
    const-string v46, "KR"

    #@1af
    const-string v47, "LGU"

    #@1b1
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1b4
    move-result v46

    #@1b5
    if-nez v46, :cond_1c1

    #@1b7
    const-string v46, "KR"

    #@1b9
    const-string v47, "SKT"

    #@1bb
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1be
    move-result v46

    #@1bf
    if-eqz v46, :cond_243

    #@1c1
    .line 2667
    :cond_1c1
    move-object/from16 v0, p0

    #@1c3
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1c5
    move-object/from16 v46, v0

    #@1c7
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->isVoiceSearching()Z

    #@1ca
    move-result v46

    #@1cb
    move-object/from16 v0, p0

    #@1cd
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@1cf
    move-object/from16 v47, v0

    #@1d1
    invoke-virtual/range {v47 .. v47}, Landroid/telephony/ServiceState;->isVoiceSearching()Z

    #@1d4
    move-result v47

    #@1d5
    move/from16 v0, v46

    #@1d7
    move/from16 v1, v47

    #@1d9
    if-ne v0, v1, :cond_241

    #@1db
    move-object/from16 v0, p0

    #@1dd
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1df
    move-object/from16 v46, v0

    #@1e1
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getState()I

    #@1e4
    move-result v46

    #@1e5
    move-object/from16 v0, p0

    #@1e7
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@1e9
    move-object/from16 v47, v0

    #@1eb
    invoke-virtual/range {v47 .. v47}, Landroid/telephony/ServiceState;->getState()I

    #@1ee
    move-result v47

    #@1ef
    move/from16 v0, v46

    #@1f1
    move/from16 v1, v47

    #@1f3
    if-ne v0, v1, :cond_241

    #@1f5
    move-object/from16 v0, p0

    #@1f7
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1f9
    move-object/from16 v46, v0

    #@1fb
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->isDataSearching()Z

    #@1fe
    move-result v46

    #@1ff
    move-object/from16 v0, p0

    #@201
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@203
    move-object/from16 v47, v0

    #@205
    invoke-virtual/range {v47 .. v47}, Landroid/telephony/ServiceState;->isDataSearching()Z

    #@208
    move-result v47

    #@209
    move/from16 v0, v46

    #@20b
    move/from16 v1, v47

    #@20d
    if-ne v0, v1, :cond_241

    #@20f
    move-object/from16 v0, p0

    #@211
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@213
    move-object/from16 v46, v0

    #@215
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getDataState()I

    #@218
    move-result v46

    #@219
    move-object/from16 v0, p0

    #@21b
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@21d
    move-object/from16 v47, v0

    #@21f
    invoke-virtual/range {v47 .. v47}, Landroid/telephony/ServiceState;->getDataState()I

    #@222
    move-result v47

    #@223
    move/from16 v0, v46

    #@225
    move/from16 v1, v47

    #@227
    if-ne v0, v1, :cond_241

    #@229
    move-object/from16 v0, p0

    #@22b
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@22d
    move-object/from16 v46, v0

    #@22f
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@232
    move-result v46

    #@233
    if-nez v46, :cond_243

    #@235
    move-object/from16 v0, p0

    #@237
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@239
    move-object/from16 v46, v0

    #@23b
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@23e
    move-result v46

    #@23f
    if-eqz v46, :cond_243

    #@241
    .line 2673
    :cond_241
    const/16 v21, 0x1

    #@243
    .line 2679
    :cond_243
    move-object/from16 v0, p0

    #@245
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@247
    move-object/from16 v46, v0

    #@249
    move-object/from16 v0, v46

    #@24b
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@24d
    move-object/from16 v46, v0

    #@24f
    invoke-interface/range {v46 .. v46}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@252
    move-result-object v46

    #@253
    move-object/from16 v0, v46

    #@255
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

    #@257
    move/from16 v46, v0

    #@259
    if-eqz v46, :cond_27f

    #@25b
    .line 2681
    move-object/from16 v0, p0

    #@25d
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@25f
    move/from16 v46, v0

    #@261
    if-eqz v46, :cond_b18

    #@263
    move-object/from16 v0, p0

    #@265
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@267
    move/from16 v46, v0

    #@269
    if-nez v46, :cond_b18

    #@26b
    move-object/from16 v0, p0

    #@26d
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@26f
    move-object/from16 v46, v0

    #@271
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@274
    move-result v46

    #@275
    if-nez v46, :cond_b18

    #@277
    .line 2683
    const/16 v46, 0x1

    #@279
    move/from16 v0, v46

    #@27b
    move-object/from16 v1, p0

    #@27d
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->CheckATTACH:Z

    #@27f
    .line 2697
    :cond_27f
    :goto_27f
    move-object/from16 v0, p0

    #@281
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@283
    move-object/from16 v46, v0

    #@285
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getState()I

    #@288
    move-result v46

    #@289
    move-object/from16 v0, p0

    #@28b
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@28d
    move-object/from16 v47, v0

    #@28f
    invoke-virtual/range {v47 .. v47}, Landroid/telephony/ServiceState;->getState()I

    #@292
    move-result v47

    #@293
    move/from16 v0, v46

    #@295
    move/from16 v1, v47

    #@297
    if-ne v0, v1, :cond_2ab

    #@299
    move-object/from16 v0, p0

    #@29b
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@29d
    move/from16 v46, v0

    #@29f
    move-object/from16 v0, p0

    #@2a1
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@2a3
    move/from16 v47, v0

    #@2a5
    move/from16 v0, v46

    #@2a7
    move/from16 v1, v47

    #@2a9
    if-eq v0, v1, :cond_2f9

    #@2ab
    .line 2698
    :cond_2ab
    const v46, 0xc3c2

    #@2ae
    const/16 v47, 0x4

    #@2b0
    move/from16 v0, v47

    #@2b2
    new-array v0, v0, [Ljava/lang/Object;

    #@2b4
    move-object/from16 v47, v0

    #@2b6
    const/16 v48, 0x0

    #@2b8
    move-object/from16 v0, p0

    #@2ba
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@2bc
    move-object/from16 v49, v0

    #@2be
    invoke-virtual/range {v49 .. v49}, Landroid/telephony/ServiceState;->getState()I

    #@2c1
    move-result v49

    #@2c2
    invoke-static/range {v49 .. v49}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c5
    move-result-object v49

    #@2c6
    aput-object v49, v47, v48

    #@2c8
    const/16 v48, 0x1

    #@2ca
    move-object/from16 v0, p0

    #@2cc
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@2ce
    move/from16 v49, v0

    #@2d0
    invoke-static/range {v49 .. v49}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d3
    move-result-object v49

    #@2d4
    aput-object v49, v47, v48

    #@2d6
    const/16 v48, 0x2

    #@2d8
    move-object/from16 v0, p0

    #@2da
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2dc
    move-object/from16 v49, v0

    #@2de
    invoke-virtual/range {v49 .. v49}, Landroid/telephony/ServiceState;->getState()I

    #@2e1
    move-result v49

    #@2e2
    invoke-static/range {v49 .. v49}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e5
    move-result-object v49

    #@2e6
    aput-object v49, v47, v48

    #@2e8
    const/16 v48, 0x3

    #@2ea
    move-object/from16 v0, p0

    #@2ec
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@2ee
    move/from16 v49, v0

    #@2f0
    invoke-static/range {v49 .. v49}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f3
    move-result-object v49

    #@2f4
    aput-object v49, v47, v48

    #@2f6
    invoke-static/range {v46 .. v47}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@2f9
    .line 2702
    :cond_2f9
    move-object/from16 v0, p0

    #@2fb
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2fd
    move-object/from16 v46, v0

    #@2ff
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@302
    move-result-object v46

    #@303
    const-string v47, "support_assisted_dialing"

    #@305
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@308
    move-result v46

    #@309
    if-nez v46, :cond_31d

    #@30b
    move-object/from16 v0, p0

    #@30d
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@30f
    move-object/from16 v46, v0

    #@311
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@314
    move-result-object v46

    #@315
    const-string v47, "support_smart_dialing"

    #@317
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@31a
    move-result v46

    #@31b
    if-eqz v46, :cond_33e

    #@31d
    .line 2706
    :cond_31d
    move-object/from16 v0, p0

    #@31f
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@321
    move-object/from16 v46, v0

    #@323
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@326
    move-result-object v46

    #@327
    invoke-virtual/range {v46 .. v46}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@32a
    move-result-object v46

    #@32b
    const-string v47, "assist_dial_ota_sid"

    #@32d
    move-object/from16 v0, p0

    #@32f
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@331
    move-object/from16 v48, v0

    #@333
    invoke-virtual/range {v48 .. v48}, Landroid/telephony/ServiceState;->getSystemId()I

    #@336
    move-result v48

    #@337
    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@33a
    move-result-object v48

    #@33b
    invoke-static/range {v46 .. v48}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@33e
    .line 2713
    :cond_33e
    move-object/from16 v0, p0

    #@340
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@342
    move-object/from16 v40, v0

    #@344
    .line 2714
    .local v40, tss:Landroid/telephony/ServiceState;
    move-object/from16 v0, p0

    #@346
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@348
    move-object/from16 v46, v0

    #@34a
    move-object/from16 v0, v46

    #@34c
    move-object/from16 v1, p0

    #@34e
    iput-object v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@350
    .line 2715
    move-object/from16 v0, v40

    #@352
    move-object/from16 v1, p0

    #@354
    iput-object v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@356
    .line 2717
    move-object/from16 v0, p0

    #@358
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@35a
    move-object/from16 v46, v0

    #@35c
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@35f
    .line 2719
    move-object/from16 v0, p0

    #@361
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@363
    move-object/from16 v36, v0

    #@365
    .line 2720
    .local v36, tcl:Landroid/telephony/gsm/GsmCellLocation;
    move-object/from16 v0, p0

    #@367
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@369
    move-object/from16 v46, v0

    #@36b
    move-object/from16 v0, v46

    #@36d
    move-object/from16 v1, p0

    #@36f
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@371
    .line 2721
    move-object/from16 v0, v36

    #@373
    move-object/from16 v1, p0

    #@375
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@377
    .line 2725
    move-object/from16 v0, p0

    #@379
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@37b
    move-object/from16 v46, v0

    #@37d
    invoke-interface/range {v46 .. v46}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@380
    move-result-object v46

    #@381
    move-object/from16 v0, v46

    #@383
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@385
    move/from16 v46, v0

    #@387
    if-eqz v46, :cond_3b5

    #@389
    .line 2726
    move-object/from16 v0, p0

    #@38b
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@38d
    move-object/from16 v46, v0

    #@38f
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@392
    move-result v46

    #@393
    const/16 v47, 0xe

    #@395
    move/from16 v0, v46

    #@397
    move/from16 v1, v47

    #@399
    if-ne v0, v1, :cond_b58

    #@39b
    .line 2727
    const-string v46, "GSM"

    #@39d
    const-string v47, "[IMS_AFW] Radio Tech is LTE, Get LTE Info from modem"

    #@39f
    invoke-static/range {v46 .. v47}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a2
    .line 2728
    move-object/from16 v0, p0

    #@3a4
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@3a6
    move-object/from16 v46, v0

    #@3a8
    const/16 v47, 0x64

    #@3aa
    move-object/from16 v0, p0

    #@3ac
    move/from16 v1, v47

    #@3ae
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@3b1
    move-result-object v47

    #@3b2
    invoke-interface/range {v46 .. v47}, Lcom/android/internal/telephony/CommandsInterface;->getLteInfoForIms(Landroid/os/Message;)V

    #@3b5
    .line 2742
    :cond_3b5
    :goto_3b5
    if-eqz v22, :cond_5ea

    #@3b7
    .line 2744
    move-object/from16 v0, p0

    #@3b9
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3bb
    move-object/from16 v46, v0

    #@3bd
    move-object/from16 v0, v46

    #@3bf
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3c1
    move-object/from16 v46, v0

    #@3c3
    invoke-interface/range {v46 .. v46}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@3c6
    move-result-object v46

    #@3c7
    move-object/from16 v0, v46

    #@3c9
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_RIL_CONN_HISTORY:Z

    #@3cb
    move/from16 v46, v0

    #@3cd
    if-eqz v46, :cond_3e8

    #@3cf
    .line 2746
    move-object/from16 v0, p0

    #@3d1
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3d3
    move-object/from16 v46, v0

    #@3d5
    move-object/from16 v0, v46

    #@3d7
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3d9
    move-object/from16 v46, v0

    #@3db
    invoke-interface/range {v46 .. v46}, Lcom/android/internal/telephony/CommandsInterface;->getMyDebugger()Lcom/android/internal/telephony/MMdebuger;

    #@3de
    move-result-object v46

    #@3df
    move-object/from16 v0, p0

    #@3e1
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@3e3
    move/from16 v47, v0

    #@3e5
    invoke-virtual/range {v46 .. v47}, Lcom/android/internal/telephony/MMdebuger;->savecurrenttech(I)V

    #@3e8
    .line 2750
    :cond_3e8
    move-object/from16 v0, p0

    #@3ea
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3ec
    move-object/from16 v46, v0

    #@3ee
    move-object/from16 v0, v46

    #@3f0
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3f2
    move-object/from16 v46, v0

    #@3f4
    invoke-interface/range {v46 .. v46}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@3f7
    move-result-object v46

    #@3f8
    move-object/from16 v0, v46

    #@3fa
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_GSM_GLOBAL_PREFERED_APN_SPR:Z

    #@3fc
    move/from16 v46, v0

    #@3fe
    if-eqz v46, :cond_43a

    #@400
    .line 2754
    move-object/from16 v0, p0

    #@402
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@404
    move/from16 v46, v0

    #@406
    const/16 v47, 0xe

    #@408
    move/from16 v0, v46

    #@40a
    move/from16 v1, v47

    #@40c
    if-eq v0, v1, :cond_b73

    #@40e
    move-object/from16 v0, p0

    #@410
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@412
    move/from16 v46, v0

    #@414
    const/16 v47, 0xd

    #@416
    move/from16 v0, v46

    #@418
    move/from16 v1, v47

    #@41a
    if-eq v0, v1, :cond_b73

    #@41c
    .line 2756
    move-object/from16 v0, p0

    #@41e
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@420
    move-object/from16 v46, v0

    #@422
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@425
    move-result-object v46

    #@426
    invoke-virtual/range {v46 .. v46}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@429
    move-result-object v46

    #@42a
    const-string v47, "tether_dun_required"

    #@42c
    const/16 v48, 0x0

    #@42e
    invoke-static/range {v46 .. v48}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@431
    .line 2757
    const-string v46, "[bycho] User selected Internet PDN for Tethering on GSM NetWork"

    #@433
    move-object/from16 v0, p0

    #@435
    move-object/from16 v1, v46

    #@437
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@43a
    .line 2776
    :cond_43a
    :goto_43a
    move-object/from16 v0, p0

    #@43c
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@43e
    move-object/from16 v46, v0

    #@440
    invoke-interface/range {v46 .. v46}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@443
    move-result-object v46

    #@444
    move-object/from16 v0, v46

    #@446
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_BLOCK_TETHERINGPDN_ON_GSMROAMING_KDDI:Z

    #@448
    move/from16 v46, v0

    #@44a
    const/16 v47, 0x1

    #@44c
    move/from16 v0, v46

    #@44e
    move/from16 v1, v47

    #@450
    if-ne v0, v1, :cond_50c

    #@452
    .line 2778
    move-object/from16 v0, p0

    #@454
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isFixedSync:Z

    #@456
    move/from16 v46, v0

    #@458
    if-nez v46, :cond_50c

    #@45a
    move-object/from16 v0, p0

    #@45c
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@45e
    move/from16 v46, v0

    #@460
    invoke-static/range {v46 .. v46}, Landroid/telephony/ServiceState;->isGsm(I)Z

    #@463
    move-result v46

    #@464
    if-eqz v46, :cond_50c

    #@466
    move-object/from16 v0, p0

    #@468
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@46a
    move/from16 v46, v0

    #@46c
    const/16 v47, 0xe

    #@46e
    move/from16 v0, v46

    #@470
    move/from16 v1, v47

    #@472
    if-eq v0, v1, :cond_50c

    #@474
    move-object/from16 v0, p0

    #@476
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@478
    move/from16 v46, v0

    #@47a
    const/16 v47, 0xd

    #@47c
    move/from16 v0, v46

    #@47e
    move/from16 v1, v47

    #@480
    if-eq v0, v1, :cond_50c

    #@482
    .line 2781
    move-object/from16 v0, p0

    #@484
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@486
    move-object/from16 v46, v0

    #@488
    move-object/from16 v0, v46

    #@48a
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@48c
    move-object/from16 v46, v0

    #@48e
    move-object/from16 v0, v46

    #@490
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@492
    move-object/from16 v46, v0

    #@494
    if-eqz v46, :cond_50c

    #@496
    move-object/from16 v0, p0

    #@498
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@49a
    move-object/from16 v46, v0

    #@49c
    move-object/from16 v0, v46

    #@49e
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@4a0
    move-object/from16 v46, v0

    #@4a2
    move-object/from16 v0, v46

    #@4a4
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@4a6
    move-object/from16 v46, v0

    #@4a8
    invoke-virtual/range {v46 .. v46}, Ljava/util/ArrayList;->isEmpty()Z

    #@4ab
    move-result v46

    #@4ac
    if-nez v46, :cond_50c

    #@4ae
    .line 2782
    new-instance v46, Ljava/lang/StringBuilder;

    #@4b0
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@4b3
    const-string v47, "Radio Tech is "

    #@4b5
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b8
    move-result-object v46

    #@4b9
    move-object/from16 v0, p0

    #@4bb
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@4bd
    move-object/from16 v47, v0

    #@4bf
    invoke-virtual/range {v47 .. v47}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@4c2
    move-result v47

    #@4c3
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c6
    move-result-object v46

    #@4c7
    const-string v47, ", newSS.getRoaming() = "

    #@4c9
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4cc
    move-result-object v46

    #@4cd
    move-object/from16 v0, p0

    #@4cf
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@4d1
    move-object/from16 v47, v0

    #@4d3
    invoke-virtual/range {v47 .. v47}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@4d6
    move-result v47

    #@4d7
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4da
    move-result-object v46

    #@4db
    const-string v47, " So calling sendPdnTable"

    #@4dd
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e0
    move-result-object v46

    #@4e1
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e4
    move-result-object v46

    #@4e5
    move-object/from16 v0, p0

    #@4e7
    move-object/from16 v1, v46

    #@4e9
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@4ec
    .line 2783
    move-object/from16 v0, p0

    #@4ee
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4f0
    move-object/from16 v46, v0

    #@4f2
    move-object/from16 v0, v46

    #@4f4
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@4f6
    move-object/from16 v46, v0

    #@4f8
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/DataConnectionTracker;->sendPdnTable()V

    #@4fb
    .line 2784
    const-string v46, "pollStateDone: KDDI isFixedSync is set to True"

    #@4fd
    move-object/from16 v0, p0

    #@4ff
    move-object/from16 v1, v46

    #@501
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@504
    .line 2785
    const/16 v46, 0x1

    #@506
    move/from16 v0, v46

    #@508
    move-object/from16 v1, p0

    #@50a
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isFixedSync:Z

    #@50c
    .line 2791
    :cond_50c
    move-object/from16 v0, p0

    #@50e
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@510
    move-object/from16 v46, v0

    #@512
    move-object/from16 v0, v46

    #@514
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@516
    move-object/from16 v46, v0

    #@518
    invoke-interface/range {v46 .. v46}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@51b
    move-result-object v46

    #@51c
    move-object/from16 v0, v46

    #@51e
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_SPR_MTU_CHANGE:Z

    #@520
    move/from16 v46, v0

    #@522
    if-eqz v46, :cond_561

    #@524
    move-object/from16 v0, p0

    #@526
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@528
    move/from16 v46, v0

    #@52a
    if-eqz v46, :cond_561

    #@52c
    .line 2792
    move-object/from16 v0, p0

    #@52e
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@530
    move-object/from16 v46, v0

    #@532
    const-string v47, "net.rmnet.activenetwork"

    #@534
    move-object/from16 v0, p0

    #@536
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@538
    move/from16 v48, v0

    #@53a
    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@53d
    move-result-object v48

    #@53e
    invoke-virtual/range {v46 .. v48}, Lcom/android/internal/telephony/gsm/GSMPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@541
    .line 2795
    new-instance v46, Ljava/lang/StringBuilder;

    #@543
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@546
    const-string v47, "[LG DATA] we set activenetwork : "

    #@548
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54b
    move-result-object v46

    #@54c
    move-object/from16 v0, p0

    #@54e
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@550
    move/from16 v47, v0

    #@552
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@555
    move-result-object v46

    #@556
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@559
    move-result-object v46

    #@55a
    move-object/from16 v0, p0

    #@55c
    move-object/from16 v1, v46

    #@55e
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@561
    .line 2800
    :cond_561
    const/4 v10, -0x1

    #@562
    .line 2801
    .local v10, cid:I
    move-object/from16 v0, p0

    #@564
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@566
    move-object/from16 v46, v0

    #@568
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getCellLocation()Landroid/telephony/CellLocation;

    #@56b
    move-result-object v30

    #@56c
    check-cast v30, Landroid/telephony/gsm/GsmCellLocation;

    #@56e
    .line 2802
    .local v30, loc:Landroid/telephony/gsm/GsmCellLocation;
    if-eqz v30, :cond_574

    #@570
    invoke-virtual/range {v30 .. v30}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    #@573
    move-result v10

    #@574
    .line 2803
    :cond_574
    const v46, 0xc3c0

    #@577
    const/16 v47, 0x3

    #@579
    move/from16 v0, v47

    #@57b
    new-array v0, v0, [Ljava/lang/Object;

    #@57d
    move-object/from16 v47, v0

    #@57f
    const/16 v48, 0x0

    #@581
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@584
    move-result-object v49

    #@585
    aput-object v49, v47, v48

    #@587
    const/16 v48, 0x1

    #@589
    move-object/from16 v0, p0

    #@58b
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@58d
    move/from16 v49, v0

    #@58f
    invoke-static/range {v49 .. v49}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@592
    move-result-object v49

    #@593
    aput-object v49, v47, v48

    #@595
    const/16 v48, 0x2

    #@597
    move-object/from16 v0, p0

    #@599
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@59b
    move/from16 v49, v0

    #@59d
    invoke-static/range {v49 .. v49}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5a0
    move-result-object v49

    #@5a1
    aput-object v49, v47, v48

    #@5a3
    invoke-static/range {v46 .. v47}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@5a6
    .line 2806
    new-instance v46, Ljava/lang/StringBuilder;

    #@5a8
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@5ab
    const-string v47, "RAT switched "

    #@5ad
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b0
    move-result-object v46

    #@5b1
    move-object/from16 v0, p0

    #@5b3
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@5b5
    move/from16 v47, v0

    #@5b7
    invoke-static/range {v47 .. v47}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@5ba
    move-result-object v47

    #@5bb
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5be
    move-result-object v46

    #@5bf
    const-string v47, " -> "

    #@5c1
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c4
    move-result-object v46

    #@5c5
    move-object/from16 v0, p0

    #@5c7
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@5c9
    move/from16 v47, v0

    #@5cb
    invoke-static/range {v47 .. v47}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@5ce
    move-result-object v47

    #@5cf
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d2
    move-result-object v46

    #@5d3
    const-string v47, " at cell "

    #@5d5
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d8
    move-result-object v46

    #@5d9
    move-object/from16 v0, v46

    #@5db
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5de
    move-result-object v46

    #@5df
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e2
    move-result-object v46

    #@5e3
    move-object/from16 v0, p0

    #@5e5
    move-object/from16 v1, v46

    #@5e7
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@5ea
    .line 2813
    .end local v10           #cid:I
    .end local v30           #loc:Landroid/telephony/gsm/GsmCellLocation;
    :cond_5ea
    move-object/from16 v0, p0

    #@5ec
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@5ee
    move-object/from16 v46, v0

    #@5f0
    invoke-interface/range {v46 .. v46}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@5f3
    move-result-object v46

    #@5f4
    move-object/from16 v0, v46

    #@5f6
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_ATTCH_AFTER_10SEC_KR:Z

    #@5f8
    move/from16 v46, v0

    #@5fa
    if-eqz v46, :cond_61c

    #@5fc
    if-eqz v18, :cond_61c

    #@5fe
    .line 2814
    const-string v46, "[pollStateDone] hasGprsAttached 10s !!"

    #@600
    move-object/from16 v0, p0

    #@602
    move-object/from16 v1, v46

    #@604
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@607
    .line 2815
    const/16 v46, 0x3f5

    #@609
    move-object/from16 v0, p0

    #@60b
    move/from16 v1, v46

    #@60d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@610
    move-result-object v46

    #@611
    const-wide/16 v47, 0x2710

    #@613
    move-object/from16 v0, p0

    #@615
    move-object/from16 v1, v46

    #@617
    move-wide/from16 v2, v47

    #@619
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@61c
    .line 2820
    :cond_61c
    move-object/from16 v0, p0

    #@61e
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@620
    move/from16 v46, v0

    #@622
    move/from16 v0, v46

    #@624
    move-object/from16 v1, p0

    #@626
    iput v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@628
    .line 2821
    move-object/from16 v0, p0

    #@62a
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewReasonDataDenied:I

    #@62c
    move/from16 v46, v0

    #@62e
    move/from16 v0, v46

    #@630
    move-object/from16 v1, p0

    #@632
    iput v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mReasonDataDenied:I

    #@634
    .line 2822
    move-object/from16 v0, p0

    #@636
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewMaxDataCalls:I

    #@638
    move/from16 v46, v0

    #@63a
    move/from16 v0, v46

    #@63c
    move-object/from16 v1, p0

    #@63e
    iput v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mMaxDataCalls:I

    #@640
    .line 2823
    move-object/from16 v0, p0

    #@642
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@644
    move/from16 v46, v0

    #@646
    move/from16 v0, v46

    #@648
    move-object/from16 v1, p0

    #@64a
    iput v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@64c
    .line 2825
    const/16 v46, 0x0

    #@64e
    move/from16 v0, v46

    #@650
    move-object/from16 v1, p0

    #@652
    iput v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@654
    .line 2828
    move-object/from16 v0, p0

    #@656
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@658
    move-object/from16 v46, v0

    #@65a
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@65d
    .line 2830
    if-eqz v22, :cond_674

    #@65f
    .line 2831
    move-object/from16 v0, p0

    #@661
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@663
    move-object/from16 v46, v0

    #@665
    const-string v47, "gsm.network.type"

    #@667
    move-object/from16 v0, p0

    #@669
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@66b
    move/from16 v48, v0

    #@66d
    invoke-static/range {v48 .. v48}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@670
    move-result-object v48

    #@671
    invoke-virtual/range {v46 .. v48}, Lcom/android/internal/telephony/gsm/GSMPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@674
    .line 2835
    :cond_674
    if-eqz v23, :cond_6ad

    #@676
    .line 2836
    move-object/from16 v0, p0

    #@678
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNetworkAttachedRegistrants:Landroid/os/RegistrantList;

    #@67a
    move-object/from16 v46, v0

    #@67c
    invoke-virtual/range {v46 .. v46}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@67f
    .line 2839
    new-instance v46, Ljava/lang/StringBuilder;

    #@681
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@684
    const-string v47, "pollStateDone: registering current mNitzUpdatedTime="

    #@686
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@689
    move-result-object v46

    #@68a
    move-object/from16 v0, p0

    #@68c
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNitzUpdatedTime:Z

    #@68e
    move/from16 v47, v0

    #@690
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@693
    move-result-object v46

    #@694
    const-string v47, " changing to false"

    #@696
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@699
    move-result-object v46

    #@69a
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69d
    move-result-object v46

    #@69e
    move-object/from16 v0, p0

    #@6a0
    move-object/from16 v1, v46

    #@6a2
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@6a5
    .line 2842
    const/16 v46, 0x0

    #@6a7
    move/from16 v0, v46

    #@6a9
    move-object/from16 v1, p0

    #@6ab
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNitzUpdatedTime:Z

    #@6ad
    .line 2846
    :cond_6ad
    const-string v46, "KR"

    #@6af
    const-string v47, "LGU"

    #@6b1
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@6b4
    move-result v46

    #@6b5
    if-nez v46, :cond_6c1

    #@6b7
    const-string v46, "KR"

    #@6b9
    const-string v47, "SKT"

    #@6bb
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@6be
    move-result v46

    #@6bf
    if-eqz v46, :cond_6cc

    #@6c1
    .line 2847
    :cond_6c1
    if-eqz v21, :cond_6cc

    #@6c3
    move-object/from16 v0, p0

    #@6c5
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLgeRegStateNotification:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@6c7
    move-object/from16 v46, v0

    #@6c9
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->handleNotification()V

    #@6cc
    .line 2852
    :cond_6cc
    const-string v46, "KR"

    #@6ce
    const-string v47, "LGU"

    #@6d0
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@6d3
    move-result v46

    #@6d4
    if-eqz v46, :cond_6f0

    #@6d6
    const-string v46, "true"

    #@6d8
    const-string v47, "gsm.operator.isroaming"

    #@6da
    invoke-static/range {v47 .. v47}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6dd
    move-result-object v47

    #@6de
    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e1
    move-result v46

    #@6e2
    if-eqz v46, :cond_6f0

    #@6e4
    .line 2853
    const-string v46, "updateSpnDisplay() after polling"

    #@6e6
    move-object/from16 v0, p0

    #@6e8
    move-object/from16 v1, v46

    #@6ea
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@6ed
    .line 2854
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@6f0
    .line 2858
    :cond_6f0
    if-eqz v16, :cond_1005

    #@6f2
    .line 2861
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@6f5
    .line 2863
    move-object/from16 v0, p0

    #@6f7
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@6f9
    move-object/from16 v46, v0

    #@6fb
    const-string v47, "gsm.operator.alpha"

    #@6fd
    move-object/from16 v0, p0

    #@6ff
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@701
    move-object/from16 v48, v0

    #@703
    invoke-virtual/range {v48 .. v48}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@706
    move-result-object v48

    #@707
    invoke-virtual/range {v46 .. v48}, Lcom/android/internal/telephony/gsm/GSMPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@70a
    .line 2866
    const-string v46, "gsm.operator.numeric"

    #@70c
    const-string v47, ""

    #@70e
    invoke-static/range {v46 .. v47}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@711
    move-result-object v34

    #@712
    .line 2868
    .local v34, prevOperatorNumeric:Ljava/lang/String;
    move-object/from16 v0, p0

    #@714
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@716
    move-object/from16 v46, v0

    #@718
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@71b
    move-result-object v33

    #@71c
    .line 2869
    .local v33, operatorNumeric:Ljava/lang/String;
    move-object/from16 v0, p0

    #@71e
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@720
    move-object/from16 v46, v0

    #@722
    const-string v47, "gsm.operator.numeric"

    #@724
    move-object/from16 v0, v46

    #@726
    move-object/from16 v1, v47

    #@728
    move-object/from16 v2, v33

    #@72a
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@72d
    .line 2871
    if-nez v33, :cond_bef

    #@72f
    .line 2872
    const-string v46, "operatorNumeric is null"

    #@731
    move-object/from16 v0, p0

    #@733
    move-object/from16 v1, v46

    #@735
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@738
    .line 2873
    move-object/from16 v0, p0

    #@73a
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@73c
    move-object/from16 v46, v0

    #@73e
    const-string v47, "gsm.operator.iso-country"

    #@740
    const-string v48, ""

    #@742
    invoke-virtual/range {v46 .. v48}, Lcom/android/internal/telephony/gsm/GSMPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@745
    .line 2874
    const/16 v46, 0x0

    #@747
    move/from16 v0, v46

    #@749
    move-object/from16 v1, p0

    #@74b
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGotCountryCode:Z

    #@74d
    .line 2875
    const/16 v46, 0x0

    #@74f
    move/from16 v0, v46

    #@751
    move-object/from16 v1, p0

    #@753
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNitzUpdatedTime:Z

    #@755
    .line 3022
    :cond_755
    :goto_755
    move-object/from16 v0, p0

    #@757
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@759
    move-object/from16 v46, v0

    #@75b
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@75e
    move-result-object v46

    #@75f
    const-string v47, "support_assisted_dialing"

    #@761
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@764
    move-result v46

    #@765
    if-nez v46, :cond_779

    #@767
    move-object/from16 v0, p0

    #@769
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@76b
    move-object/from16 v46, v0

    #@76d
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@770
    move-result-object v46

    #@771
    const-string v47, "support_smart_dialing"

    #@773
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@776
    move-result v46

    #@777
    if-eqz v46, :cond_7c9

    #@779
    .line 3026
    :cond_779
    invoke-static/range {v33 .. v33}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@77c
    move-result v46

    #@77d
    if-nez v46, :cond_7c9

    #@77f
    .line 3029
    const/16 v46, 0x0

    #@781
    const/16 v47, 0x3

    #@783
    :try_start_783
    move-object/from16 v0, v33

    #@785
    move/from16 v1, v46

    #@787
    move/from16 v2, v47

    #@789
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@78c
    move-result-object v8

    #@78d
    .line 3030
    .local v8, assistedDialingMcc:Ljava/lang/String;
    const-string v46, " ***** put System.ASSIST_DIAL_OTA_MCC "

    #@78f
    move-object/from16 v0, p0

    #@791
    move-object/from16 v1, v46

    #@793
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@796
    .line 3031
    move-object/from16 v0, p0

    #@798
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@79a
    move-object/from16 v46, v0

    #@79c
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@79f
    move-result-object v46

    #@7a0
    invoke-virtual/range {v46 .. v46}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7a3
    move-result-object v46

    #@7a4
    const-string v47, "assist_dial_ota_mcc"

    #@7a6
    move-object/from16 v0, v46

    #@7a8
    move-object/from16 v1, v47

    #@7aa
    invoke-static {v0, v1, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@7ad
    .line 3033
    new-instance v46, Ljava/lang/StringBuilder;

    #@7af
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@7b2
    const-string v47, " ***** MCC  "

    #@7b4
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b7
    move-result-object v46

    #@7b8
    move-object/from16 v0, v46

    #@7ba
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7bd
    move-result-object v46

    #@7be
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c1
    move-result-object v46

    #@7c2
    move-object/from16 v0, p0

    #@7c4
    move-object/from16 v1, v46

    #@7c6
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V
    :try_end_7c9
    .catch Ljava/lang/NumberFormatException; {:try_start_783 .. :try_end_7c9} :catch_ff5

    #@7c9
    .line 3041
    .end local v8           #assistedDialingMcc:Ljava/lang/String;
    :cond_7c9
    :goto_7c9
    move-object/from16 v0, p0

    #@7cb
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@7cd
    move-object/from16 v47, v0

    #@7cf
    const-string v48, "gsm.operator.isroaming"

    #@7d1
    move-object/from16 v0, p0

    #@7d3
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@7d5
    move-object/from16 v46, v0

    #@7d7
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@7da
    move-result v46

    #@7db
    if-eqz v46, :cond_1001

    #@7dd
    const-string v46, "true"

    #@7df
    :goto_7df
    move-object/from16 v0, v47

    #@7e1
    move-object/from16 v1, v48

    #@7e3
    move-object/from16 v2, v46

    #@7e5
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@7e8
    .line 3044
    move-object/from16 v0, p0

    #@7ea
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@7ec
    move-object/from16 v46, v0

    #@7ee
    move-object/from16 v0, p0

    #@7f0
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@7f2
    move-object/from16 v47, v0

    #@7f4
    invoke-virtual/range {v46 .. v47}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyServiceStateChanged(Landroid/telephony/ServiceState;)V

    #@7f7
    .line 3048
    move-object/from16 v0, p0

    #@7f9
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mFirstUpdateSpn:Z

    #@7fb
    move/from16 v46, v0

    #@7fd
    if-nez v46, :cond_82a

    #@7ff
    const-string v46, "KR"

    #@801
    invoke-static/range {v46 .. v46}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@804
    move-result v46

    #@805
    if-eqz v46, :cond_82a

    #@807
    .line 3049
    new-instance v46, Ljava/lang/StringBuilder;

    #@809
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@80c
    const-string v47, "mFirstUpdateSpn= "

    #@80e
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@811
    move-result-object v46

    #@812
    move-object/from16 v0, p0

    #@814
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mFirstUpdateSpn:Z

    #@816
    move/from16 v47, v0

    #@818
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@81b
    move-result-object v46

    #@81c
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81f
    move-result-object v46

    #@820
    move-object/from16 v0, p0

    #@822
    move-object/from16 v1, v46

    #@824
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@827
    .line 3050
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@82a
    .line 3069
    .end local v33           #operatorNumeric:Ljava/lang/String;
    .end local v34           #prevOperatorNumeric:Ljava/lang/String;
    :cond_82a
    :goto_82a
    if-eqz v18, :cond_106c

    #@82c
    .line 3070
    move-object/from16 v0, p0

    #@82e
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mAttachedRegistrants:Landroid/os/RegistrantList;

    #@830
    move-object/from16 v46, v0

    #@832
    invoke-virtual/range {v46 .. v46}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@835
    .line 3083
    :cond_835
    :goto_835
    const-string v46, "KR"

    #@837
    const-string v47, "LGU"

    #@839
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@83c
    move-result v46

    #@83d
    if-eqz v46, :cond_8f7

    #@83f
    if-eqz v18, :cond_8f7

    #@841
    move-object/from16 v0, p0

    #@843
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@845
    move/from16 v46, v0

    #@847
    invoke-static/range {v46 .. v46}, Landroid/telephony/ServiceState;->isGsm(I)Z

    #@84a
    move-result v46

    #@84b
    if-eqz v46, :cond_8f7

    #@84d
    .line 3088
    const-string v46, "ril.gsm.mm_cause"

    #@84f
    const/16 v47, 0x0

    #@851
    invoke-static/range {v46 .. v47}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@854
    move-result v46

    #@855
    if-gtz v46, :cond_861

    #@857
    const-string v46, "ril.gsm.gmm_cause"

    #@859
    const/16 v47, 0x0

    #@85b
    invoke-static/range {v46 .. v47}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@85e
    move-result v46

    #@85f
    if-lez v46, :cond_10cf

    #@861
    :cond_861
    const/16 v32, 0x1

    #@863
    .line 3091
    .local v32, nonLteRejectReceived:Z
    :goto_863
    move-object/from16 v0, p0

    #@865
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@867
    move-object/from16 v46, v0

    #@869
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@86c
    move-result v46

    #@86d
    if-eqz v46, :cond_10d3

    #@86f
    .line 3092
    const-string v46, "gsm.lge.lte_esm_reject_cause"

    #@871
    const/16 v47, 0x0

    #@873
    invoke-static/range {v46 .. v47}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@876
    move-result v28

    #@877
    .line 3093
    .local v28, lastRejectCause:I
    const/16 v46, 0x5

    #@879
    move/from16 v0, v28

    #@87b
    move/from16 v1, v46

    #@87d
    if-ne v0, v1, :cond_887

    #@87f
    const/16 v46, 0x6

    #@881
    move/from16 v0, v28

    #@883
    move/from16 v1, v46

    #@885
    if-eq v0, v1, :cond_8ce

    #@887
    .line 3094
    :cond_887
    const-string v46, "gsm.lge.lte_reject_cause"

    #@889
    const-string v47, "0"

    #@88b
    invoke-static/range {v46 .. v47}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@88e
    .line 3095
    const-string v46, "gsm.lge.lte_esm_reject_cause"

    #@890
    const-string v47, "0"

    #@892
    invoke-static/range {v46 .. v47}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@895
    .line 3096
    const-string v46, "GSM/WCDMA/LTE data in-service : set no U+ LTE reject cause "

    #@897
    move-object/from16 v0, p0

    #@899
    move-object/from16 v1, v46

    #@89b
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@89e
    .line 3097
    new-instance v26, Landroid/content/Intent;

    #@8a0
    const-string v46, "android.intent.action.LTE_EMM_REJECT"

    #@8a2
    move-object/from16 v0, v26

    #@8a4
    move-object/from16 v1, v46

    #@8a6
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@8a9
    .line 3098
    .local v26, intent:Landroid/content/Intent;
    const-string v46, "rejectCode"

    #@8ab
    const/16 v47, 0x0

    #@8ad
    move-object/from16 v0, v26

    #@8af
    move-object/from16 v1, v46

    #@8b1
    move/from16 v2, v47

    #@8b3
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@8b6
    .line 3099
    move-object/from16 v0, p0

    #@8b8
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@8ba
    move-object/from16 v46, v0

    #@8bc
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@8bf
    move-result-object v46

    #@8c0
    move-object/from16 v0, v46

    #@8c2
    move-object/from16 v1, v26

    #@8c4
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@8c7
    .line 3100
    const-string v46, "persist.radio.last_ltereject"

    #@8c9
    const-string v47, "0"

    #@8cb
    invoke-static/range {v46 .. v47}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@8ce
    .line 3103
    .end local v26           #intent:Landroid/content/Intent;
    :cond_8ce
    move-object/from16 v0, p0

    #@8d0
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@8d2
    move/from16 v46, v0

    #@8d4
    const/16 v47, 0xe

    #@8d6
    move/from16 v0, v46

    #@8d8
    move/from16 v1, v47

    #@8da
    if-ne v0, v1, :cond_8f4

    #@8dc
    .line 3104
    if-eqz v32, :cond_8f4

    #@8de
    .line 3105
    const-string v46, "LTE roaming in-service : clear GSM/WCDMA reject cause and its notification."

    #@8e0
    move-object/from16 v0, p0

    #@8e2
    move-object/from16 v1, v46

    #@8e4
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@8e7
    .line 3106
    move-object/from16 v0, p0

    #@8e9
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@8eb
    move-object/from16 v46, v0

    #@8ed
    const/16 v47, 0x1

    #@8ef
    const/16 v48, 0x1

    #@8f1
    invoke-interface/range {v46 .. v48}, Lcom/android/internal/telephony/gsm/RejectCause;->clearRejectCause(II)Z

    #@8f4
    .line 3130
    .end local v28           #lastRejectCause:I
    :cond_8f4
    :goto_8f4
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@8f7
    .line 3134
    .end local v32           #nonLteRejectReceived:Z
    :cond_8f7
    if-eqz v19, :cond_902

    #@8f9
    .line 3135
    move-object/from16 v0, p0

    #@8fb
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mDetachedRegistrants:Landroid/os/RegistrantList;

    #@8fd
    move-object/from16 v46, v0

    #@8ff
    invoke-virtual/range {v46 .. v46}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@902
    .line 3138
    :cond_902
    if-eqz v22, :cond_90f

    #@904
    .line 3139
    move-object/from16 v0, p0

    #@906
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@908
    move-object/from16 v46, v0

    #@90a
    const-string v47, "nwTypeChanged"

    #@90c
    invoke-virtual/range {v46 .. v47}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyDataConnection(Ljava/lang/String;)V

    #@90f
    .line 3142
    :cond_90f
    if-eqz v25, :cond_96f

    #@911
    .line 3143
    move-object/from16 v0, p0

    #@913
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOnRegistrants:Landroid/os/RegistrantList;

    #@915
    move-object/from16 v46, v0

    #@917
    invoke-virtual/range {v46 .. v46}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@91a
    .line 3145
    move-object/from16 v0, p0

    #@91c
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@91e
    move-object/from16 v46, v0

    #@920
    invoke-interface/range {v46 .. v46}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@923
    move-result-object v46

    #@924
    move-object/from16 v0, v46

    #@926
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SEND_DATA_ROAM_POPUP_INTENT_VZW:Z

    #@928
    move/from16 v46, v0

    #@92a
    if-eqz v46, :cond_96f

    #@92c
    .line 3147
    move-object/from16 v0, p0

    #@92e
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@930
    move-object/from16 v46, v0

    #@932
    move-object/from16 v0, v46

    #@934
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@936
    move-object/from16 v46, v0

    #@938
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@93b
    move-result v46

    #@93c
    if-nez v46, :cond_96f

    #@93e
    move-object/from16 v0, p0

    #@940
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@942
    move-object/from16 v46, v0

    #@944
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getState()I

    #@947
    move-result v46

    #@948
    if-nez v46, :cond_96f

    #@94a
    .line 3149
    const-string v46, " ***** Send Roam intent  ACTION_DATA_ROAMING_MENU"

    #@94c
    move-object/from16 v0, p0

    #@94e
    move-object/from16 v1, v46

    #@950
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@953
    .line 3150
    new-instance v26, Landroid/content/Intent;

    #@955
    const-string v46, "com.lge.android.intent.action.ACTION_DATA_ROAMING_MENU"

    #@957
    move-object/from16 v0, v26

    #@959
    move-object/from16 v1, v46

    #@95b
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@95e
    .line 3151
    .restart local v26       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@960
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@962
    move-object/from16 v46, v0

    #@964
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@967
    move-result-object v46

    #@968
    move-object/from16 v0, v46

    #@96a
    move-object/from16 v1, v26

    #@96c
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@96f
    .line 3157
    .end local v26           #intent:Landroid/content/Intent;
    :cond_96f
    if-eqz v24, :cond_97a

    #@971
    .line 3158
    move-object/from16 v0, p0

    #@973
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOffRegistrants:Landroid/os/RegistrantList;

    #@975
    move-object/from16 v46, v0

    #@977
    invoke-virtual/range {v46 .. v46}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@97a
    .line 3161
    :cond_97a
    if-eqz v20, :cond_985

    #@97c
    .line 3162
    move-object/from16 v0, p0

    #@97e
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@980
    move-object/from16 v46, v0

    #@982
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyLocationChanged()V

    #@985
    .line 3195
    :cond_985
    move-object/from16 v0, p0

    #@987
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@989
    move/from16 v46, v0

    #@98b
    move-object/from16 v0, p0

    #@98d
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@98f
    move-object/from16 v47, v0

    #@991
    invoke-virtual/range {v47 .. v47}, Landroid/telephony/ServiceState;->getState()I

    #@994
    move-result v47

    #@995
    move-object/from16 v0, p0

    #@997
    move/from16 v1, v46

    #@999
    move/from16 v2, v47

    #@99b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isGprsConsistent(II)Z

    #@99e
    move-result v46

    #@99f
    if-nez v46, :cond_1142

    #@9a1
    .line 3196
    move-object/from16 v0, p0

    #@9a3
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStartedGprsRegCheck:Z

    #@9a5
    move/from16 v46, v0

    #@9a7
    if-nez v46, :cond_9e6

    #@9a9
    move-object/from16 v0, p0

    #@9ab
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mReportedGprsNoReg:Z

    #@9ad
    move/from16 v46, v0

    #@9af
    if-nez v46, :cond_9e6

    #@9b1
    .line 3197
    const/16 v46, 0x1

    #@9b3
    move/from16 v0, v46

    #@9b5
    move-object/from16 v1, p0

    #@9b7
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStartedGprsRegCheck:Z

    #@9b9
    .line 3199
    move-object/from16 v0, p0

    #@9bb
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@9bd
    move-object/from16 v46, v0

    #@9bf
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@9c2
    move-result-object v46

    #@9c3
    invoke-virtual/range {v46 .. v46}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9c6
    move-result-object v46

    #@9c7
    const-string v47, "gprs_register_check_period_ms"

    #@9c9
    const v48, 0xea60

    #@9cc
    invoke-static/range {v46 .. v48}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@9cf
    move-result v9

    #@9d0
    .line 3203
    .local v9, check_period:I
    const/16 v46, 0x16

    #@9d2
    move-object/from16 v0, p0

    #@9d4
    move/from16 v1, v46

    #@9d6
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@9d9
    move-result-object v46

    #@9da
    int-to-long v0, v9

    #@9db
    move-wide/from16 v47, v0

    #@9dd
    move-object/from16 v0, p0

    #@9df
    move-object/from16 v1, v46

    #@9e1
    move-wide/from16 v2, v47

    #@9e3
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@9e6
    .line 3211
    .end local v9           #check_period:I
    :cond_9e6
    :goto_9e6
    const/16 v46, 0x0

    #@9e8
    const-string v47, "vzw_gfit"

    #@9ea
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9ed
    move-result v46

    #@9ee
    if-eqz v46, :cond_9fb

    #@9f0
    .line 3212
    if-eqz v17, :cond_9fb

    #@9f2
    .line 3213
    move-object/from16 v0, p0

    #@9f4
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNoServiceChangedRegistrants:Landroid/os/RegistrantList;

    #@9f6
    move-object/from16 v46, v0

    #@9f8
    invoke-virtual/range {v46 .. v46}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@9fb
    .line 3219
    :cond_9fb
    move-object/from16 v0, p0

    #@9fd
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@9ff
    move-object/from16 v47, v0

    #@a01
    monitor-enter v47

    #@a02
    .line 3220
    :try_start_a02
    move-object/from16 v0, p0

    #@a04
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@a06
    move/from16 v46, v0

    #@a08
    const/16 v48, 0xe

    #@a0a
    move/from16 v0, v46

    #@a0c
    move/from16 v1, v48

    #@a0e
    if-ne v0, v1, :cond_acb

    #@a10
    .line 3221
    move-object/from16 v0, p0

    #@a12
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@a14
    move-object/from16 v46, v0

    #@a16
    move-object/from16 v0, p0

    #@a18
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLasteCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@a1a
    move-object/from16 v48, v0

    #@a1c
    move-object/from16 v0, v46

    #@a1e
    move-object/from16 v1, v48

    #@a20
    invoke-virtual {v0, v1}, Landroid/telephony/CellIdentityLte;->equals(Ljava/lang/Object;)Z

    #@a23
    move-result v46

    #@a24
    if-nez v46, :cond_114c

    #@a26
    const/4 v11, 0x1

    #@a27
    .line 3222
    .local v11, cidChanged:Z
    :goto_a27
    if-nez v23, :cond_a2d

    #@a29
    if-nez v17, :cond_a2d

    #@a2b
    if-eqz v11, :cond_acb

    #@a2d
    .line 3224
    :cond_a2d
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@a30
    move-result-wide v48

    #@a31
    const-wide/16 v50, 0x3e8

    #@a33
    mul-long v38, v48, v50

    #@a35
    .line 3225
    .local v38, timeStamp:J
    move-object/from16 v0, p0

    #@a37
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@a39
    move-object/from16 v46, v0

    #@a3b
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getState()I

    #@a3e
    move-result v46

    #@a3f
    if-nez v46, :cond_114f

    #@a41
    const/16 v35, 0x1

    #@a43
    .line 3226
    .local v35, registered:Z
    :goto_a43
    move-object/from16 v0, p0

    #@a45
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@a47
    move-object/from16 v46, v0

    #@a49
    move-object/from16 v0, v46

    #@a4b
    move-object/from16 v1, p0

    #@a4d
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLasteCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@a4f
    .line 3228
    move-object/from16 v0, p0

    #@a51
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@a53
    move-object/from16 v46, v0

    #@a55
    move-object/from16 v0, v46

    #@a57
    move/from16 v1, v35

    #@a59
    invoke-virtual {v0, v1}, Landroid/telephony/CellInfoLte;->setRegisterd(Z)V

    #@a5c
    .line 3229
    move-object/from16 v0, p0

    #@a5e
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@a60
    move-object/from16 v46, v0

    #@a62
    move-object/from16 v0, p0

    #@a64
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLasteCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@a66
    move-object/from16 v48, v0

    #@a68
    move-object/from16 v0, v46

    #@a6a
    move-object/from16 v1, v48

    #@a6c
    invoke-virtual {v0, v1}, Landroid/telephony/CellInfoLte;->setCellIdentity(Landroid/telephony/CellIdentityLte;)V

    #@a6f
    .line 3231
    new-instance v46, Ljava/lang/StringBuilder;

    #@a71
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@a74
    const-string v48, "pollStateDone: hasRegistered="

    #@a76
    move-object/from16 v0, v46

    #@a78
    move-object/from16 v1, v48

    #@a7a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7d
    move-result-object v46

    #@a7e
    move-object/from16 v0, v46

    #@a80
    move/from16 v1, v23

    #@a82
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a85
    move-result-object v46

    #@a86
    const-string v48, " hasDeregistered="

    #@a88
    move-object/from16 v0, v46

    #@a8a
    move-object/from16 v1, v48

    #@a8c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8f
    move-result-object v46

    #@a90
    move-object/from16 v0, v46

    #@a92
    move/from16 v1, v17

    #@a94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a97
    move-result-object v46

    #@a98
    const-string v48, " cidChanged="

    #@a9a
    move-object/from16 v0, v46

    #@a9c
    move-object/from16 v1, v48

    #@a9e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa1
    move-result-object v46

    #@aa2
    move-object/from16 v0, v46

    #@aa4
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@aa7
    move-result-object v46

    #@aa8
    const-string v48, " mCellInfo="

    #@aaa
    move-object/from16 v0, v46

    #@aac
    move-object/from16 v1, v48

    #@aae
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab1
    move-result-object v46

    #@ab2
    move-object/from16 v0, p0

    #@ab4
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@ab6
    move-object/from16 v48, v0

    #@ab8
    move-object/from16 v0, v46

    #@aba
    move-object/from16 v1, v48

    #@abc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@abf
    move-result-object v46

    #@ac0
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac3
    move-result-object v46

    #@ac4
    move-object/from16 v0, p0

    #@ac6
    move-object/from16 v1, v46

    #@ac8
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@acb
    .line 3239
    .end local v11           #cidChanged:Z
    .end local v35           #registered:Z
    .end local v38           #timeStamp:J
    :cond_acb
    move-object/from16 v0, p0

    #@acd
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@acf
    move-object/from16 v46, v0

    #@ad1
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    #@ad4
    move-result-object v46

    #@ad5
    if-eqz v46, :cond_af2

    #@ad7
    .line 3240
    new-instance v7, Ljava/util/ArrayList;

    #@ad9
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@adc
    .line 3241
    .local v7, arrayCi:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/CellInfo;>;"
    move-object/from16 v0, p0

    #@ade
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@ae0
    move-object/from16 v46, v0

    #@ae2
    move-object/from16 v0, v46

    #@ae4
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ae7
    .line 3242
    move-object/from16 v0, p0

    #@ae9
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@aeb
    move-object/from16 v46, v0

    #@aed
    move-object/from16 v0, v46

    #@aef
    invoke-virtual {v0, v7}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyCellInfo(Ljava/util/List;)V

    #@af2
    .line 3244
    .end local v7           #arrayCi:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/CellInfo;>;"
    :cond_af2
    monitor-exit v47
    :try_end_af3
    .catchall {:try_start_a02 .. :try_end_af3} :catchall_1153

    #@af3
    .line 3248
    return-void

    #@af4
    .line 2627
    .end local v16           #hasChanged:Z
    .end local v17           #hasDeregistered:Z
    .end local v18           #hasGprsAttached:Z
    .end local v19           #hasGprsDetached:Z
    .end local v20           #hasLocationChanged:Z
    .end local v21           #hasNotiStateChnaged:Z
    .end local v22           #hasRadioTechnologyChanged:Z
    .end local v23           #hasRegistered:Z
    .end local v24           #hasRoamingOff:Z
    .end local v25           #hasRoamingOn:Z
    .end local v36           #tcl:Landroid/telephony/gsm/GsmCellLocation;
    .end local v40           #tss:Landroid/telephony/ServiceState;
    :cond_af4
    const/16 v23, 0x0

    #@af6
    goto/16 :goto_d2

    #@af8
    .line 2631
    .restart local v23       #hasRegistered:Z
    :cond_af8
    const/16 v17, 0x0

    #@afa
    goto/16 :goto_ec

    #@afc
    .line 2635
    .restart local v17       #hasDeregistered:Z
    :cond_afc
    const/16 v18, 0x0

    #@afe
    goto/16 :goto_fe

    #@b00
    .line 2649
    .restart local v18       #hasGprsAttached:Z
    :cond_b00
    const/16 v19, 0x0

    #@b02
    goto/16 :goto_13b

    #@b04
    .line 2653
    .restart local v19       #hasGprsDetached:Z
    :cond_b04
    const/16 v22, 0x0

    #@b06
    goto/16 :goto_14f

    #@b08
    .line 2655
    .restart local v22       #hasRadioTechnologyChanged:Z
    :cond_b08
    const/16 v16, 0x0

    #@b0a
    goto/16 :goto_163

    #@b0c
    .line 2657
    .restart local v16       #hasChanged:Z
    :cond_b0c
    const/16 v25, 0x0

    #@b0e
    goto/16 :goto_17d

    #@b10
    .line 2659
    .restart local v25       #hasRoamingOn:Z
    :cond_b10
    const/16 v24, 0x0

    #@b12
    goto/16 :goto_197

    #@b14
    .line 2661
    .restart local v24       #hasRoamingOff:Z
    :cond_b14
    const/16 v20, 0x0

    #@b16
    goto/16 :goto_1ab

    #@b18
    .line 2685
    .restart local v20       #hasLocationChanged:Z
    .restart local v21       #hasNotiStateChnaged:Z
    :cond_b18
    move-object/from16 v0, p0

    #@b1a
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@b1c
    move/from16 v46, v0

    #@b1e
    if-nez v46, :cond_b3e

    #@b20
    move-object/from16 v0, p0

    #@b22
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@b24
    move/from16 v46, v0

    #@b26
    if-nez v46, :cond_b3e

    #@b28
    move-object/from16 v0, p0

    #@b2a
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@b2c
    move-object/from16 v46, v0

    #@b2e
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@b31
    move-result v46

    #@b32
    if-nez v46, :cond_b3e

    #@b34
    .line 2687
    const/16 v46, 0x1

    #@b36
    move/from16 v0, v46

    #@b38
    move-object/from16 v1, p0

    #@b3a
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->CheckATTACH:Z

    #@b3c
    goto/16 :goto_27f

    #@b3e
    .line 2689
    :cond_b3e
    move-object/from16 v0, p0

    #@b40
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@b42
    move/from16 v46, v0

    #@b44
    if-nez v46, :cond_27f

    #@b46
    move-object/from16 v0, p0

    #@b48
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@b4a
    move/from16 v46, v0

    #@b4c
    if-eqz v46, :cond_27f

    #@b4e
    .line 2691
    const/16 v46, 0x0

    #@b50
    move/from16 v0, v46

    #@b52
    move-object/from16 v1, p0

    #@b54
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->CheckATTACH:Z

    #@b56
    goto/16 :goto_27f

    #@b58
    .line 2730
    .restart local v36       #tcl:Landroid/telephony/gsm/GsmCellLocation;
    .restart local v40       #tss:Landroid/telephony/ServiceState;
    :cond_b58
    move-object/from16 v0, p0

    #@b5a
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@b5c
    move-object/from16 v46, v0

    #@b5e
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@b61
    move-result v46

    #@b62
    const/16 v47, 0xd

    #@b64
    move/from16 v0, v46

    #@b66
    move/from16 v1, v47

    #@b68
    if-ne v0, v1, :cond_3b5

    #@b6a
    .line 2731
    const-string v46, "GSM"

    #@b6c
    const-string v47, "[IMS_AFW] Radio Tech is EHRPD, Get CDMA Info from modem"

    #@b6e
    invoke-static/range {v46 .. v47}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b71
    goto/16 :goto_3b5

    #@b73
    .line 2761
    :cond_b73
    move-object/from16 v0, p0

    #@b75
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@b77
    move-object/from16 v46, v0

    #@b79
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@b7c
    move-result-object v46

    #@b7d
    invoke-virtual/range {v46 .. v46}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b80
    move-result-object v46

    #@b81
    const-string v47, "tethering_pdn"

    #@b83
    const/16 v48, 0x1

    #@b85
    invoke-static/range {v46 .. v48}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@b88
    move-result v29

    #@b89
    .line 2763
    .local v29, lg_data_sprint_iot_hidden_menu:I
    new-instance v46, Ljava/lang/StringBuilder;

    #@b8b
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@b8e
    const-string v47, "[bycho]lg_data_sprint_iot_hidden_menu : "

    #@b90
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b93
    move-result-object v46

    #@b94
    move-object/from16 v0, v46

    #@b96
    move/from16 v1, v29

    #@b98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b9b
    move-result-object v46

    #@b9c
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9f
    move-result-object v46

    #@ba0
    move-object/from16 v0, p0

    #@ba2
    move-object/from16 v1, v46

    #@ba4
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@ba7
    .line 2764
    const/16 v46, 0x2

    #@ba9
    move/from16 v0, v29

    #@bab
    move/from16 v1, v46

    #@bad
    if-eq v0, v1, :cond_bcf

    #@baf
    .line 2765
    move-object/from16 v0, p0

    #@bb1
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@bb3
    move-object/from16 v46, v0

    #@bb5
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@bb8
    move-result-object v46

    #@bb9
    invoke-virtual/range {v46 .. v46}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@bbc
    move-result-object v46

    #@bbd
    const-string v47, "tether_dun_required"

    #@bbf
    const/16 v48, 0x1

    #@bc1
    invoke-static/range {v46 .. v48}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@bc4
    .line 2766
    const-string v46, "[bycho]There is APN profile which can handles dun type."

    #@bc6
    move-object/from16 v0, p0

    #@bc8
    move-object/from16 v1, v46

    #@bca
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@bcd
    goto/16 :goto_43a

    #@bcf
    .line 2768
    :cond_bcf
    move-object/from16 v0, p0

    #@bd1
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@bd3
    move-object/from16 v46, v0

    #@bd5
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@bd8
    move-result-object v46

    #@bd9
    invoke-virtual/range {v46 .. v46}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@bdc
    move-result-object v46

    #@bdd
    const-string v47, "tether_dun_required"

    #@bdf
    const/16 v48, 0x0

    #@be1
    invoke-static/range {v46 .. v48}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@be4
    .line 2769
    const-string v46, "[bycho]User selected Internet PDN for Tethering in Hidden Menu...Do not change TEHTER_DUN_REQUIRED value for Sprint IRAT test!!!"

    #@be6
    move-object/from16 v0, p0

    #@be8
    move-object/from16 v1, v46

    #@bea
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@bed
    goto/16 :goto_43a

    #@bef
    .line 2877
    .end local v29           #lg_data_sprint_iot_hidden_menu:I
    .restart local v33       #operatorNumeric:Ljava/lang/String;
    .restart local v34       #prevOperatorNumeric:Ljava/lang/String;
    :cond_bef
    const-string v27, ""

    #@bf1
    .line 2878
    .local v27, iso:Ljava/lang/String;
    const-string v31, ""

    #@bf3
    .line 2881
    .local v31, mcc:Ljava/lang/String;
    :try_start_bf3
    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->length()I

    #@bf6
    move-result v46

    #@bf7
    const/16 v47, 0x3

    #@bf9
    move/from16 v0, v46

    #@bfb
    move/from16 v1, v47

    #@bfd
    if-lt v0, v1, :cond_c15

    #@bff
    .line 2883
    const/16 v46, 0x0

    #@c01
    const/16 v47, 0x3

    #@c03
    move-object/from16 v0, v33

    #@c05
    move/from16 v1, v46

    #@c07
    move/from16 v2, v47

    #@c09
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@c0c
    move-result-object v31

    #@c0d
    .line 2884
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@c10
    move-result v46

    #@c11
    invoke-static/range {v46 .. v46}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;
    :try_end_c14
    .catch Ljava/lang/NumberFormatException; {:try_start_bf3 .. :try_end_c14} :catch_e61
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_bf3 .. :try_end_c14} :catch_e80

    #@c14
    move-result-object v27

    #@c15
    .line 2894
    :cond_c15
    :goto_c15
    move-object/from16 v0, p0

    #@c17
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@c19
    move-object/from16 v46, v0

    #@c1b
    const-string v47, "gsm.operator.iso-country"

    #@c1d
    move-object/from16 v0, v46

    #@c1f
    move-object/from16 v1, v47

    #@c21
    move-object/from16 v2, v27

    #@c23
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@c26
    .line 2895
    const/16 v46, 0x1

    #@c28
    move/from16 v0, v46

    #@c2a
    move-object/from16 v1, p0

    #@c2c
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGotCountryCode:Z

    #@c2e
    .line 2897
    const/16 v44, 0x0

    #@c30
    .line 2899
    .local v44, zone:Ljava/util/TimeZone;
    move-object/from16 v0, p0

    #@c32
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNitzUpdatedTime:Z

    #@c34
    move/from16 v46, v0

    #@c36
    if-nez v46, :cond_ccd

    #@c38
    const-string v46, "000"

    #@c3a
    move-object/from16 v0, v31

    #@c3c
    move-object/from16 v1, v46

    #@c3e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c41
    move-result v46

    #@c42
    if-nez v46, :cond_ccd

    #@c44
    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c47
    move-result v46

    #@c48
    if-nez v46, :cond_ccd

    #@c4a
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getAutoTimeZone()Z

    #@c4d
    move-result v46

    #@c4e
    if-eqz v46, :cond_ccd

    #@c50
    .line 2903
    const-string v46, "telephony.test.ignore.nitz"

    #@c52
    const/16 v47, 0x0

    #@c54
    invoke-static/range {v46 .. v47}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@c57
    move-result v46

    #@c58
    if-eqz v46, :cond_e9f

    #@c5a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@c5d
    move-result-wide v46

    #@c5e
    const-wide/16 v48, 0x1

    #@c60
    and-long v46, v46, v48

    #@c62
    const-wide/16 v48, 0x0

    #@c64
    cmp-long v46, v46, v48

    #@c66
    if-nez v46, :cond_e9f

    #@c68
    const/16 v37, 0x1

    #@c6a
    .line 2907
    .local v37, testOneUniqueOffsetPath:Z
    :goto_c6a
    invoke-static/range {v27 .. v27}, Landroid/util/TimeUtils;->getTimeZonesWithUniqueOffsets(Ljava/lang/String;)Ljava/util/ArrayList;

    #@c6d
    move-result-object v43

    #@c6e
    .line 2908
    .local v43, uniqueZones:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/TimeZone;>;"
    invoke-virtual/range {v43 .. v43}, Ljava/util/ArrayList;->size()I

    #@c71
    move-result v46

    #@c72
    const/16 v47, 0x1

    #@c74
    move/from16 v0, v46

    #@c76
    move/from16 v1, v47

    #@c78
    if-eq v0, v1, :cond_c7c

    #@c7a
    if-eqz v37, :cond_ea3

    #@c7c
    .line 2909
    :cond_c7c
    const/16 v46, 0x0

    #@c7e
    move-object/from16 v0, v43

    #@c80
    move/from16 v1, v46

    #@c82
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c85
    move-result-object v44

    #@c86
    .end local v44           #zone:Ljava/util/TimeZone;
    check-cast v44, Ljava/util/TimeZone;

    #@c88
    .line 2911
    .restart local v44       #zone:Ljava/util/TimeZone;
    new-instance v46, Ljava/lang/StringBuilder;

    #@c8a
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@c8d
    const-string v47, "pollStateDone: no nitz but one TZ for iso-cc="

    #@c8f
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c92
    move-result-object v46

    #@c93
    move-object/from16 v0, v46

    #@c95
    move-object/from16 v1, v27

    #@c97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9a
    move-result-object v46

    #@c9b
    const-string v47, " with zone.getID="

    #@c9d
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca0
    move-result-object v46

    #@ca1
    invoke-virtual/range {v44 .. v44}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@ca4
    move-result-object v47

    #@ca5
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca8
    move-result-object v46

    #@ca9
    const-string v47, " testOneUniqueOffsetPath="

    #@cab
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cae
    move-result-object v46

    #@caf
    move-object/from16 v0, v46

    #@cb1
    move/from16 v1, v37

    #@cb3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@cb6
    move-result-object v46

    #@cb7
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cba
    move-result-object v46

    #@cbb
    move-object/from16 v0, p0

    #@cbd
    move-object/from16 v1, v46

    #@cbf
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@cc2
    .line 2915
    invoke-virtual/range {v44 .. v44}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@cc5
    move-result-object v46

    #@cc6
    move-object/from16 v0, p0

    #@cc8
    move-object/from16 v1, v46

    #@cca
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setAndBroadcastNetworkSetTimeZone(Ljava/lang/String;)V

    #@ccd
    .line 2929
    .end local v37           #testOneUniqueOffsetPath:Z
    .end local v43           #uniqueZones:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/TimeZone;>;"
    :cond_ccd
    :goto_ccd
    move-object/from16 v0, p0

    #@ccf
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@cd1
    move-object/from16 v46, v0

    #@cd3
    move-object/from16 v0, p0

    #@cd5
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@cd7
    move/from16 v47, v0

    #@cd9
    move-object/from16 v0, p0

    #@cdb
    move-object/from16 v1, v46

    #@cdd
    move-object/from16 v2, v33

    #@cdf
    move-object/from16 v3, v34

    #@ce1
    move/from16 v4, v47

    #@ce3
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->shouldFixTimeZoneNow(Lcom/android/internal/telephony/PhoneBase;Ljava/lang/String;Ljava/lang/String;Z)Z

    #@ce6
    move-result v46

    #@ce7
    if-eqz v46, :cond_755

    #@ce9
    .line 2934
    const-string v46, "persist.sys.timezone"

    #@ceb
    invoke-static/range {v46 .. v46}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@cee
    move-result-object v45

    #@cef
    .line 2936
    .local v45, zoneName:Ljava/lang/String;
    new-instance v46, Ljava/lang/StringBuilder;

    #@cf1
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@cf4
    const-string v47, "pollStateDone: fix time zone zoneName=\'"

    #@cf6
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf9
    move-result-object v46

    #@cfa
    move-object/from16 v0, v46

    #@cfc
    move-object/from16 v1, v45

    #@cfe
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d01
    move-result-object v46

    #@d02
    const-string v47, "\' mZoneOffset="

    #@d04
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d07
    move-result-object v46

    #@d08
    move-object/from16 v0, p0

    #@d0a
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@d0c
    move/from16 v47, v0

    #@d0e
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d11
    move-result-object v46

    #@d12
    const-string v47, " mZoneDst="

    #@d14
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d17
    move-result-object v46

    #@d18
    move-object/from16 v0, p0

    #@d1a
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@d1c
    move/from16 v47, v0

    #@d1e
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d21
    move-result-object v46

    #@d22
    const-string v47, " iso-cc=\'"

    #@d24
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d27
    move-result-object v46

    #@d28
    move-object/from16 v0, v46

    #@d2a
    move-object/from16 v1, v27

    #@d2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2f
    move-result-object v46

    #@d30
    const-string v47, "\' iso-cc-idx="

    #@d32
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d35
    move-result-object v46

    #@d36
    sget-object v47, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->GMT_COUNTRY_CODES:[Ljava/lang/String;

    #@d38
    move-object/from16 v0, v47

    #@d3a
    move-object/from16 v1, v27

    #@d3c
    invoke-static {v0, v1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    #@d3f
    move-result v47

    #@d40
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d43
    move-result-object v46

    #@d44
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d47
    move-result-object v46

    #@d48
    move-object/from16 v0, p0

    #@d4a
    move-object/from16 v1, v46

    #@d4c
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@d4f
    .line 2948
    move-object/from16 v0, p0

    #@d51
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@d53
    move/from16 v46, v0

    #@d55
    if-nez v46, :cond_efd

    #@d57
    move-object/from16 v0, p0

    #@d59
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@d5b
    move/from16 v46, v0

    #@d5d
    if-nez v46, :cond_efd

    #@d5f
    if-eqz v45, :cond_efd

    #@d61
    invoke-virtual/range {v45 .. v45}, Ljava/lang/String;->length()I

    #@d64
    move-result v46

    #@d65
    if-lez v46, :cond_efd

    #@d67
    sget-object v46, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->GMT_COUNTRY_CODES:[Ljava/lang/String;

    #@d69
    move-object/from16 v0, v46

    #@d6b
    move-object/from16 v1, v27

    #@d6d
    invoke-static {v0, v1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    #@d70
    move-result v46

    #@d71
    if-gez v46, :cond_efd

    #@d73
    .line 2951
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    #@d76
    move-result-object v44

    #@d77
    .line 2952
    move-object/from16 v0, p0

    #@d79
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@d7b
    move/from16 v46, v0

    #@d7d
    if-eqz v46, :cond_de5

    #@d7f
    .line 2955
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@d82
    move-result-wide v12

    #@d83
    .line 2956
    .local v12, ctm:J
    move-object/from16 v0, v44

    #@d85
    invoke-virtual {v0, v12, v13}, Ljava/util/TimeZone;->getOffset(J)I

    #@d88
    move-result v46

    #@d89
    move/from16 v0, v46

    #@d8b
    int-to-long v0, v0

    #@d8c
    move-wide/from16 v41, v0

    #@d8e
    .line 2958
    .local v41, tzOffset:J
    new-instance v46, Ljava/lang/StringBuilder;

    #@d90
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@d93
    const-string v47, "pollStateDone: tzOffset="

    #@d95
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d98
    move-result-object v46

    #@d99
    move-object/from16 v0, v46

    #@d9b
    move-wide/from16 v1, v41

    #@d9d
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@da0
    move-result-object v46

    #@da1
    const-string v47, " ltod="

    #@da3
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da6
    move-result-object v46

    #@da7
    invoke-static {v12, v13}, Landroid/util/TimeUtils;->logTimeOfDay(J)Ljava/lang/String;

    #@daa
    move-result-object v47

    #@dab
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dae
    move-result-object v46

    #@daf
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db2
    move-result-object v46

    #@db3
    move-object/from16 v0, p0

    #@db5
    move-object/from16 v1, v46

    #@db7
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@dba
    .line 2961
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getAutoTime()Z

    #@dbd
    move-result v46

    #@dbe
    if-eqz v46, :cond_eed

    #@dc0
    .line 2962
    sub-long v5, v12, v41

    #@dc2
    .line 2963
    .local v5, adj:J
    new-instance v46, Ljava/lang/StringBuilder;

    #@dc4
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@dc7
    const-string v47, "pollStateDone: adj ltod="

    #@dc9
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dcc
    move-result-object v46

    #@dcd
    invoke-static {v5, v6}, Landroid/util/TimeUtils;->logTimeOfDay(J)Ljava/lang/String;

    #@dd0
    move-result-object v47

    #@dd1
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd4
    move-result-object v46

    #@dd5
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd8
    move-result-object v46

    #@dd9
    move-object/from16 v0, p0

    #@ddb
    move-object/from16 v1, v46

    #@ddd
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@de0
    .line 2965
    move-object/from16 v0, p0

    #@de2
    invoke-direct {v0, v5, v6}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setAndBroadcastNetworkSetTime(J)V

    #@de5
    .line 2971
    .end local v5           #adj:J
    .end local v12           #ctm:J
    .end local v41           #tzOffset:J
    :cond_de5
    :goto_de5
    const-string v46, "pollStateDone: using default TimeZone"

    #@de7
    move-object/from16 v0, p0

    #@de9
    move-object/from16 v1, v46

    #@deb
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@dee
    .line 2998
    :cond_dee
    :goto_dee
    const/16 v46, 0x0

    #@df0
    const-string v47, "MAUNAL_TIMEZONE_SETTING_POPUP"

    #@df2
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@df5
    move-result v46

    #@df6
    if-eqz v46, :cond_e1b

    #@df8
    .line 2999
    move-object/from16 v0, p0

    #@dfa
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@dfc
    move/from16 v46, v0

    #@dfe
    if-nez v46, :cond_e1b

    #@e00
    move-object/from16 v0, p0

    #@e02
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isMultiTimeZoneArea:Z

    #@e04
    move/from16 v46, v0

    #@e06
    if-eqz v46, :cond_e1b

    #@e08
    .line 3001
    const-string v46, "NITZ not received in multi-time zone area!! zone will be selected thru timezone setting popup"

    #@e0a
    move-object/from16 v0, p0

    #@e0c
    move-object/from16 v1, v46

    #@e0e
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@e11
    .line 3002
    const/16 v44, 0x0

    #@e13
    .line 3003
    const/16 v46, 0x0

    #@e15
    move/from16 v0, v46

    #@e17
    move-object/from16 v1, p0

    #@e19
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isMultiTimeZoneArea:Z

    #@e1b
    .line 3008
    :cond_e1b
    const/16 v46, 0x0

    #@e1d
    move/from16 v0, v46

    #@e1f
    move-object/from16 v1, p0

    #@e21
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@e23
    .line 3010
    if-eqz v44, :cond_fea

    #@e25
    .line 3011
    new-instance v46, Ljava/lang/StringBuilder;

    #@e27
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@e2a
    const-string v47, "pollStateDone: zone != null zone.getID="

    #@e2c
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2f
    move-result-object v46

    #@e30
    invoke-virtual/range {v44 .. v44}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@e33
    move-result-object v47

    #@e34
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e37
    move-result-object v46

    #@e38
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3b
    move-result-object v46

    #@e3c
    move-object/from16 v0, p0

    #@e3e
    move-object/from16 v1, v46

    #@e40
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@e43
    .line 3012
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getAutoTimeZone()Z

    #@e46
    move-result v46

    #@e47
    if-eqz v46, :cond_e54

    #@e49
    .line 3013
    invoke-virtual/range {v44 .. v44}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@e4c
    move-result-object v46

    #@e4d
    move-object/from16 v0, p0

    #@e4f
    move-object/from16 v1, v46

    #@e51
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setAndBroadcastNetworkSetTimeZone(Ljava/lang/String;)V

    #@e54
    .line 3015
    :cond_e54
    invoke-virtual/range {v44 .. v44}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@e57
    move-result-object v46

    #@e58
    move-object/from16 v0, p0

    #@e5a
    move-object/from16 v1, v46

    #@e5c
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->saveNitzTimeZone(Ljava/lang/String;)V

    #@e5f
    goto/16 :goto_755

    #@e61
    .line 2888
    .end local v44           #zone:Ljava/util/TimeZone;
    .end local v45           #zoneName:Ljava/lang/String;
    :catch_e61
    move-exception v15

    #@e62
    .line 2889
    .local v15, ex:Ljava/lang/NumberFormatException;
    new-instance v46, Ljava/lang/StringBuilder;

    #@e64
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@e67
    const-string v47, "pollStateDone: countryCodeForMcc error"

    #@e69
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6c
    move-result-object v46

    #@e6d
    move-object/from16 v0, v46

    #@e6f
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e72
    move-result-object v46

    #@e73
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e76
    move-result-object v46

    #@e77
    move-object/from16 v0, p0

    #@e79
    move-object/from16 v1, v46

    #@e7b
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@e7e
    goto/16 :goto_c15

    #@e80
    .line 2890
    .end local v15           #ex:Ljava/lang/NumberFormatException;
    :catch_e80
    move-exception v15

    #@e81
    .line 2891
    .local v15, ex:Ljava/lang/StringIndexOutOfBoundsException;
    new-instance v46, Ljava/lang/StringBuilder;

    #@e83
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@e86
    const-string v47, "pollStateDone: countryCodeForMcc error"

    #@e88
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8b
    move-result-object v46

    #@e8c
    move-object/from16 v0, v46

    #@e8e
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e91
    move-result-object v46

    #@e92
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e95
    move-result-object v46

    #@e96
    move-object/from16 v0, p0

    #@e98
    move-object/from16 v1, v46

    #@e9a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@e9d
    goto/16 :goto_c15

    #@e9f
    .line 2903
    .end local v15           #ex:Ljava/lang/StringIndexOutOfBoundsException;
    .restart local v44       #zone:Ljava/util/TimeZone;
    :cond_e9f
    const/16 v37, 0x0

    #@ea1
    goto/16 :goto_c6a

    #@ea3
    .line 2918
    .restart local v37       #testOneUniqueOffsetPath:Z
    .restart local v43       #uniqueZones:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/TimeZone;>;"
    :cond_ea3
    new-instance v46, Ljava/lang/StringBuilder;

    #@ea5
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@ea8
    const-string v47, "pollStateDone: there are "

    #@eaa
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ead
    move-result-object v46

    #@eae
    invoke-virtual/range {v43 .. v43}, Ljava/util/ArrayList;->size()I

    #@eb1
    move-result v47

    #@eb2
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@eb5
    move-result-object v46

    #@eb6
    const-string v47, " unique offsets for iso-cc=\'"

    #@eb8
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ebb
    move-result-object v46

    #@ebc
    move-object/from16 v0, v46

    #@ebe
    move-object/from16 v1, v27

    #@ec0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec3
    move-result-object v46

    #@ec4
    const-string v47, " testOneUniqueOffsetPath="

    #@ec6
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec9
    move-result-object v46

    #@eca
    move-object/from16 v0, v46

    #@ecc
    move/from16 v1, v37

    #@ece
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ed1
    move-result-object v46

    #@ed2
    const-string v47, "\', do nothing"

    #@ed4
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed7
    move-result-object v46

    #@ed8
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@edb
    move-result-object v46

    #@edc
    move-object/from16 v0, p0

    #@ede
    move-object/from16 v1, v46

    #@ee0
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@ee3
    .line 2924
    const/16 v46, 0x1

    #@ee5
    move/from16 v0, v46

    #@ee7
    move-object/from16 v1, p0

    #@ee9
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isMultiTimeZoneArea:Z

    #@eeb
    goto/16 :goto_ccd

    #@eed
    .line 2968
    .end local v37           #testOneUniqueOffsetPath:Z
    .end local v43           #uniqueZones:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/TimeZone;>;"
    .restart local v12       #ctm:J
    .restart local v41       #tzOffset:J
    .restart local v45       #zoneName:Ljava/lang/String;
    :cond_eed
    move-object/from16 v0, p0

    #@eef
    iget-wide v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTime:J

    #@ef1
    move-wide/from16 v46, v0

    #@ef3
    sub-long v46, v46, v41

    #@ef5
    move-wide/from16 v0, v46

    #@ef7
    move-object/from16 v2, p0

    #@ef9
    iput-wide v0, v2, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTime:J

    #@efb
    goto/16 :goto_de5

    #@efd
    .line 2972
    .end local v12           #ctm:J
    .end local v41           #tzOffset:J
    :cond_efd
    const-string v46, ""

    #@eff
    move-object/from16 v0, v27

    #@f01
    move-object/from16 v1, v46

    #@f03
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f06
    move-result v46

    #@f07
    if-eqz v46, :cond_f32

    #@f09
    .line 2975
    move-object/from16 v0, p0

    #@f0b
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@f0d
    move/from16 v46, v0

    #@f0f
    move-object/from16 v0, p0

    #@f11
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@f13
    move/from16 v47, v0

    #@f15
    move-object/from16 v0, p0

    #@f17
    iget-wide v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneTime:J

    #@f19
    move-wide/from16 v48, v0

    #@f1b
    move-object/from16 v0, p0

    #@f1d
    move/from16 v1, v46

    #@f1f
    move/from16 v2, v47

    #@f21
    move-wide/from16 v3, v48

    #@f23
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getNitzTimeZone(IZJ)Ljava/util/TimeZone;

    #@f26
    move-result-object v44

    #@f27
    .line 2976
    const-string v46, "pollStateDone: using NITZ TimeZone"

    #@f29
    move-object/from16 v0, p0

    #@f2b
    move-object/from16 v1, v46

    #@f2d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@f30
    goto/16 :goto_dee

    #@f32
    .line 2978
    :cond_f32
    move-object/from16 v0, p0

    #@f34
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@f36
    move/from16 v46, v0

    #@f38
    move-object/from16 v0, p0

    #@f3a
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@f3c
    move/from16 v47, v0

    #@f3e
    move-object/from16 v0, p0

    #@f40
    iget-wide v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneTime:J

    #@f42
    move-wide/from16 v48, v0

    #@f44
    move/from16 v0, v46

    #@f46
    move/from16 v1, v47

    #@f48
    move-wide/from16 v2, v48

    #@f4a
    move-object/from16 v4, v27

    #@f4c
    invoke-static {v0, v1, v2, v3, v4}, Landroid/util/TimeUtils;->getTimeZone(IZJLjava/lang/String;)Ljava/util/TimeZone;

    #@f4f
    move-result-object v44

    #@f50
    .line 2979
    const-string v46, "pollStateDone: using getTimeZone(off, dst, time, iso)"

    #@f52
    move-object/from16 v0, p0

    #@f54
    move-object/from16 v1, v46

    #@f56
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@f59
    .line 2982
    const-string v46, "JP"

    #@f5b
    const-string v47, "DCM"

    #@f5d
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@f60
    move-result v46

    #@f61
    if-eqz v46, :cond_dee

    #@f63
    .line 2983
    if-nez v44, :cond_f83

    #@f65
    .line 2984
    move-object/from16 v0, p0

    #@f67
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@f69
    move/from16 v46, v0

    #@f6b
    move-object/from16 v0, p0

    #@f6d
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@f6f
    move/from16 v47, v0

    #@f71
    move-object/from16 v0, p0

    #@f73
    iget-wide v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneTime:J

    #@f75
    move-wide/from16 v48, v0

    #@f77
    move-object/from16 v0, p0

    #@f79
    move/from16 v1, v46

    #@f7b
    move/from16 v2, v47

    #@f7d
    move-wide/from16 v3, v48

    #@f7f
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getNitzTimeZone(IZJ)Ljava/util/TimeZone;

    #@f82
    move-result-object v44

    #@f83
    .line 2986
    :cond_f83
    if-eqz v44, :cond_fe1

    #@f85
    .line 2987
    const-string v46, "GSM"

    #@f87
    new-instance v47, Ljava/lang/StringBuilder;

    #@f89
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@f8c
    const-string v48, "NITZ: Case 4, zone = "

    #@f8e
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f91
    move-result-object v47

    #@f92
    invoke-virtual/range {v44 .. v44}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@f95
    move-result-object v48

    #@f96
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f99
    move-result-object v47

    #@f9a
    const-string v48, ", mZoneOffset = "

    #@f9c
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9f
    move-result-object v47

    #@fa0
    move-object/from16 v0, p0

    #@fa2
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@fa4
    move/from16 v48, v0

    #@fa6
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fa9
    move-result-object v47

    #@faa
    const-string v48, ", mZoneDst = "

    #@fac
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@faf
    move-result-object v47

    #@fb0
    move-object/from16 v0, p0

    #@fb2
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@fb4
    move/from16 v48, v0

    #@fb6
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@fb9
    move-result-object v47

    #@fba
    const-string v48, ", mZoneTime = "

    #@fbc
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fbf
    move-result-object v47

    #@fc0
    move-object/from16 v0, p0

    #@fc2
    iget-wide v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneTime:J

    #@fc4
    move-wide/from16 v48, v0

    #@fc6
    invoke-virtual/range {v47 .. v49}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@fc9
    move-result-object v47

    #@fca
    const-string v48, ", iso"

    #@fcc
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fcf
    move-result-object v47

    #@fd0
    move-object/from16 v0, v47

    #@fd2
    move-object/from16 v1, v27

    #@fd4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd7
    move-result-object v47

    #@fd8
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fdb
    move-result-object v47

    #@fdc
    invoke-static/range {v46 .. v47}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@fdf
    goto/16 :goto_dee

    #@fe1
    .line 2990
    :cond_fe1
    const-string v46, "GSM"

    #@fe3
    const-string v47, "NITZ: Case 4, zone = null"

    #@fe5
    invoke-static/range {v46 .. v47}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@fe8
    goto/16 :goto_dee

    #@fea
    .line 3017
    :cond_fea
    const-string v46, "pollStateDone: zone == null"

    #@fec
    move-object/from16 v0, p0

    #@fee
    move-object/from16 v1, v46

    #@ff0
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@ff3
    goto/16 :goto_755

    #@ff5
    .line 3034
    .end local v27           #iso:Ljava/lang/String;
    .end local v31           #mcc:Ljava/lang/String;
    .end local v44           #zone:Ljava/util/TimeZone;
    .end local v45           #zoneName:Ljava/lang/String;
    :catch_ff5
    move-exception v14

    #@ff6
    .line 3035
    .local v14, e:Ljava/lang/NumberFormatException;
    const-string v46, "NumberFormatException"

    #@ff8
    move-object/from16 v0, p0

    #@ffa
    move-object/from16 v1, v46

    #@ffc
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@fff
    goto/16 :goto_7c9

    #@1001
    .line 3041
    .end local v14           #e:Ljava/lang/NumberFormatException;
    :cond_1001
    const-string v46, "false"

    #@1003
    goto/16 :goto_7df

    #@1005
    .line 3057
    .end local v33           #operatorNumeric:Ljava/lang/String;
    .end local v34           #prevOperatorNumeric:Ljava/lang/String;
    :cond_1005
    const-string v46, "US"

    #@1007
    const-string v47, "TMO"

    #@1009
    invoke-static/range {v46 .. v47}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@100c
    move-result v46

    #@100d
    if-eqz v46, :cond_82a

    #@100f
    move-object/from16 v0, p0

    #@1011
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1013
    move-object/from16 v46, v0

    #@1015
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getState()I

    #@1018
    move-result v46

    #@1019
    if-nez v46, :cond_82a

    #@101b
    .line 3058
    move-object/from16 v0, p0

    #@101d
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mResendCounter:I

    #@101f
    move/from16 v46, v0

    #@1021
    const/16 v47, 0x3

    #@1023
    move/from16 v0, v46

    #@1025
    move/from16 v1, v47

    #@1027
    if-ge v0, v1, :cond_82a

    #@1029
    .line 3059
    new-instance v46, Ljava/lang/StringBuilder;

    #@102b
    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    #@102e
    const-string v47, "[Radio] SS not Changed!! But, ResendCount = "

    #@1030
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1033
    move-result-object v46

    #@1034
    move-object/from16 v0, p0

    #@1036
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mResendCounter:I

    #@1038
    move/from16 v47, v0

    #@103a
    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@103d
    move-result-object v46

    #@103e
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1041
    move-result-object v46

    #@1042
    move-object/from16 v0, p0

    #@1044
    move-object/from16 v1, v46

    #@1046
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1049
    .line 3060
    const/16 v46, 0x1

    #@104b
    move/from16 v0, v46

    #@104d
    move-object/from16 v1, p0

    #@104f
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mResendEnable:Z

    #@1051
    .line 3061
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@1054
    .line 3062
    const/16 v46, 0x0

    #@1056
    move/from16 v0, v46

    #@1058
    move-object/from16 v1, p0

    #@105a
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mResendEnable:Z

    #@105c
    .line 3063
    move-object/from16 v0, p0

    #@105e
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mResendCounter:I

    #@1060
    move/from16 v46, v0

    #@1062
    add-int/lit8 v46, v46, 0x1

    #@1064
    move/from16 v0, v46

    #@1066
    move-object/from16 v1, p0

    #@1068
    iput v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mResendCounter:I

    #@106a
    goto/16 :goto_82a

    #@106c
    .line 3073
    :cond_106c
    move-object/from16 v0, p0

    #@106e
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1070
    move-object/from16 v46, v0

    #@1072
    move-object/from16 v0, v46

    #@1074
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1076
    move-object/from16 v46, v0

    #@1078
    invoke-interface/range {v46 .. v46}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@107b
    move-result-object v46

    #@107c
    move-object/from16 v0, v46

    #@107e
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

    #@1080
    move/from16 v46, v0

    #@1082
    if-eqz v46, :cond_835

    #@1084
    move-object/from16 v0, p0

    #@1086
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->CheckATTACH:Z

    #@1088
    move/from16 v46, v0

    #@108a
    const/16 v47, 0x1

    #@108c
    move/from16 v0, v46

    #@108e
    move/from16 v1, v47

    #@1090
    if-ne v0, v1, :cond_835

    #@1092
    move-object/from16 v0, p0

    #@1094
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1096
    move-object/from16 v46, v0

    #@1098
    invoke-virtual/range {v46 .. v46}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@109b
    move-result v46

    #@109c
    if-eqz v46, :cond_835

    #@109e
    .line 3076
    const/16 v46, 0x0

    #@10a0
    move/from16 v0, v46

    #@10a2
    move-object/from16 v1, p0

    #@10a4
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->CheckATTACH:Z

    #@10a6
    .line 3077
    const-string v46, "GSM"

    #@10a8
    new-instance v47, Ljava/lang/StringBuilder;

    #@10aa
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@10ad
    const-string v48, "mAttachedRegistrants.notifyRegistrants() CheckATTACH::"

    #@10af
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b2
    move-result-object v47

    #@10b3
    move-object/from16 v0, p0

    #@10b5
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->CheckATTACH:Z

    #@10b7
    move/from16 v48, v0

    #@10b9
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10bc
    move-result-object v47

    #@10bd
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c0
    move-result-object v47

    #@10c1
    invoke-static/range {v46 .. v47}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@10c4
    .line 3078
    move-object/from16 v0, p0

    #@10c6
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mAttachedRegistrants:Landroid/os/RegistrantList;

    #@10c8
    move-object/from16 v46, v0

    #@10ca
    invoke-virtual/range {v46 .. v46}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@10cd
    goto/16 :goto_835

    #@10cf
    .line 3088
    :cond_10cf
    const/16 v32, 0x0

    #@10d1
    goto/16 :goto_863

    #@10d3
    .line 3111
    .restart local v32       #nonLteRejectReceived:Z
    :cond_10d3
    move-object/from16 v0, p0

    #@10d5
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@10d7
    move/from16 v46, v0

    #@10d9
    const/16 v47, 0xe

    #@10db
    move/from16 v0, v46

    #@10dd
    move/from16 v1, v47

    #@10df
    if-ne v0, v1, :cond_8f4

    #@10e1
    .line 3112
    const-string v46, "gsm.lge.lte_reject_cause"

    #@10e3
    const-string v47, "0"

    #@10e5
    invoke-static/range {v46 .. v47}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@10e8
    .line 3113
    const-string v46, "gsm.lge.lte_esm_reject_cause"

    #@10ea
    const-string v47, "0"

    #@10ec
    invoke-static/range {v46 .. v47}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@10ef
    .line 3114
    const-string v46, "LTE data in-service : set no U+ LTE reject cause "

    #@10f1
    move-object/from16 v0, p0

    #@10f3
    move-object/from16 v1, v46

    #@10f5
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@10f8
    .line 3116
    new-instance v26, Landroid/content/Intent;

    #@10fa
    const-string v46, "android.intent.action.LTE_EMM_REJECT"

    #@10fc
    move-object/from16 v0, v26

    #@10fe
    move-object/from16 v1, v46

    #@1100
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1103
    .line 3117
    .restart local v26       #intent:Landroid/content/Intent;
    const-string v46, "rejectCode"

    #@1105
    const/16 v47, 0x0

    #@1107
    move-object/from16 v0, v26

    #@1109
    move-object/from16 v1, v46

    #@110b
    move/from16 v2, v47

    #@110d
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1110
    .line 3118
    move-object/from16 v0, p0

    #@1112
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1114
    move-object/from16 v46, v0

    #@1116
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@1119
    move-result-object v46

    #@111a
    move-object/from16 v0, v46

    #@111c
    move-object/from16 v1, v26

    #@111e
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@1121
    .line 3119
    const-string v46, "persist.radio.last_ltereject"

    #@1123
    const-string v47, "0"

    #@1125
    invoke-static/range {v46 .. v47}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1128
    .line 3123
    if-eqz v32, :cond_8f4

    #@112a
    .line 3124
    const-string v46, "LTE in-service : clear GSM/WCDMA reject cause and its notification."

    #@112c
    move-object/from16 v0, p0

    #@112e
    move-object/from16 v1, v46

    #@1130
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1133
    .line 3125
    move-object/from16 v0, p0

    #@1135
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@1137
    move-object/from16 v46, v0

    #@1139
    const/16 v47, 0x1

    #@113b
    const/16 v48, 0x1

    #@113d
    invoke-interface/range {v46 .. v48}, Lcom/android/internal/telephony/gsm/RejectCause;->clearRejectCause(II)Z

    #@1140
    goto/16 :goto_8f4

    #@1142
    .line 3207
    .end local v26           #intent:Landroid/content/Intent;
    .end local v32           #nonLteRejectReceived:Z
    :cond_1142
    const/16 v46, 0x0

    #@1144
    move/from16 v0, v46

    #@1146
    move-object/from16 v1, p0

    #@1148
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mReportedGprsNoReg:Z

    #@114a
    goto/16 :goto_9e6

    #@114c
    .line 3221
    :cond_114c
    const/4 v11, 0x0

    #@114d
    goto/16 :goto_a27

    #@114f
    .line 3225
    .restart local v11       #cidChanged:Z
    .restart local v38       #timeStamp:J
    :cond_114f
    const/16 v35, 0x0

    #@1151
    goto/16 :goto_a43

    #@1153
    .line 3244
    .end local v11           #cidChanged:Z
    .end local v38           #timeStamp:J
    :catchall_1153
    move-exception v46

    #@1154
    :try_start_1154
    monitor-exit v47
    :try_end_1155
    .catchall {:try_start_1154 .. :try_end_1155} :catchall_1153

    #@1155
    throw v46
.end method

.method private postDismissDialog()V
    .registers 3

    #@0
    .prologue
    .line 5185
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    .line 5186
    .local v0, r:Landroid/content/res/Resources;
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mToneGenerator:Landroid/media/ToneGenerator;

    #@6
    if-eqz v1, :cond_b

    #@8
    .line 5187
    const/4 v1, 0x0

    #@9
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mToneGenerator:Landroid/media/ToneGenerator;

    #@b
    .line 5191
    :cond_b
    return-void
.end method

.method private queueNextSignalStrengthPoll()V
    .registers 4

    #@0
    .prologue
    .line 3296
    iget-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->dontPollSignalStrength:Z

    #@2
    if-eqz v1, :cond_5

    #@4
    .line 3311
    :goto_4
    return-void

    #@5
    .line 3304
    :cond_5
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage()Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 3305
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0xa

    #@b
    iput v1, v0, Landroid/os/Message;->what:I

    #@d
    .line 3310
    const-wide/16 v1, 0x4e20

    #@f
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@12
    goto :goto_4
.end method

.method private regCodeIsRoaming(I)Z
    .registers 3
    .parameter "code"

    #@0
    .prologue
    .line 3493
    const/4 v0, 0x5

    #@1
    if-ne v0, p1, :cond_5

    #@3
    const/4 v0, 0x1

    #@4
    :goto_4
    return v0

    #@5
    :cond_5
    const/4 v0, 0x0

    #@6
    goto :goto_4
.end method

.method private regCodeToServiceState(I)I
    .registers 6
    .parameter "code"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x2

    #@2
    const/4 v0, 0x1

    #@3
    .line 3420
    packed-switch p1, :pswitch_data_13e

    #@6
    .line 3481
    :pswitch_6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "regCodeToServiceState: unexpected service state "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@1c
    .line 3482
    :cond_1c
    :goto_1c
    :pswitch_1c
    return v0

    #@1d
    .line 3428
    :pswitch_1d
    const-string v2, "ATT"

    #@1f
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@22
    move-result v2

    #@23
    if-nez v2, :cond_2f

    #@25
    const-string v2, "US"

    #@27
    const-string v3, "TMO"

    #@29
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_38

    #@2f
    .line 3429
    :cond_2f
    const-string v0, "[GsmServiceStateTracker]"

    #@31
    const-string v2, "Emergency Service"

    #@33
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    move v0, v1

    #@37
    .line 3430
    goto :goto_1c

    #@38
    .line 3432
    :cond_38
    const-string v2, "COM"

    #@3a
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@3d
    move-result v2

    #@3e
    if-nez v2, :cond_a0

    #@40
    const-string v2, "OPEN"

    #@42
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@45
    move-result v2

    #@46
    if-nez v2, :cond_a0

    #@48
    const-string v2, "AU"

    #@4a
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@4d
    move-result v2

    #@4e
    if-nez v2, :cond_a0

    #@50
    const-string v2, "BR"

    #@52
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@55
    move-result v2

    #@56
    if-nez v2, :cond_a0

    #@58
    const-string v2, "MX"

    #@5a
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@5d
    move-result v2

    #@5e
    if-nez v2, :cond_a0

    #@60
    const-string v2, "EU"

    #@62
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@65
    move-result v2

    #@66
    if-nez v2, :cond_a0

    #@68
    const-string v2, "ro.build.target_region"

    #@6a
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    const-string v3, "SCA"

    #@70
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@73
    move-result v2

    #@74
    if-nez v2, :cond_a0

    #@76
    const-string v2, "ro.build.target_region"

    #@78
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7b
    move-result-object v2

    #@7c
    const-string v3, "AME"

    #@7e
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@81
    move-result v2

    #@82
    if-nez v2, :cond_a0

    #@84
    const-string v2, "ro.build.target_region"

    #@86
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@89
    move-result-object v2

    #@8a
    const-string v3, "CIS"

    #@8c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v2

    #@90
    if-nez v2, :cond_a0

    #@92
    const-string v2, "ro.build.target_region"

    #@94
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@97
    move-result-object v2

    #@98
    const-string v3, "ESA"

    #@9a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9d
    move-result v2

    #@9e
    if-eqz v2, :cond_1c

    #@a0
    .line 3436
    :cond_a0
    const-string v0, "[GsmServiceStateTracker]"

    #@a2
    const-string v2, "Emergency Service"

    #@a4
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    move v0, v1

    #@a8
    .line 3437
    goto/16 :goto_1c

    #@aa
    .line 3457
    :pswitch_aa
    const-string v2, "ATT"

    #@ac
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@af
    move-result v2

    #@b0
    if-nez v2, :cond_bc

    #@b2
    const-string v2, "US"

    #@b4
    const-string v3, "TMO"

    #@b6
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@b9
    move-result v2

    #@ba
    if-eqz v2, :cond_c6

    #@bc
    .line 3458
    :cond_bc
    const-string v0, "[GsmServiceStateTracker]"

    #@be
    const-string v2, "Emergency Service"

    #@c0
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c3
    move v0, v1

    #@c4
    .line 3459
    goto/16 :goto_1c

    #@c6
    .line 3461
    :cond_c6
    const-string v2, "COM"

    #@c8
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@cb
    move-result v2

    #@cc
    if-nez v2, :cond_12e

    #@ce
    const-string v2, "OPEN"

    #@d0
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@d3
    move-result v2

    #@d4
    if-nez v2, :cond_12e

    #@d6
    const-string v2, "AU"

    #@d8
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@db
    move-result v2

    #@dc
    if-nez v2, :cond_12e

    #@de
    const-string v2, "BR"

    #@e0
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@e3
    move-result v2

    #@e4
    if-nez v2, :cond_12e

    #@e6
    const-string v2, "MX"

    #@e8
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@eb
    move-result v2

    #@ec
    if-nez v2, :cond_12e

    #@ee
    const-string v2, "EU"

    #@f0
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@f3
    move-result v2

    #@f4
    if-nez v2, :cond_12e

    #@f6
    const-string v2, "ro.build.target_region"

    #@f8
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@fb
    move-result-object v2

    #@fc
    const-string v3, "SCA"

    #@fe
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@101
    move-result v2

    #@102
    if-nez v2, :cond_12e

    #@104
    const-string v2, "ro.build.target_region"

    #@106
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@109
    move-result-object v2

    #@10a
    const-string v3, "AME"

    #@10c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10f
    move-result v2

    #@110
    if-nez v2, :cond_12e

    #@112
    const-string v2, "ro.build.target_region"

    #@114
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@117
    move-result-object v2

    #@118
    const-string v3, "CIS"

    #@11a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11d
    move-result v2

    #@11e
    if-nez v2, :cond_12e

    #@120
    const-string v2, "ro.build.target_region"

    #@122
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@125
    move-result-object v2

    #@126
    const-string v3, "ESA"

    #@128
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12b
    move-result v2

    #@12c
    if-eqz v2, :cond_1c

    #@12e
    .line 3465
    :cond_12e
    const-string v0, "[GsmServiceStateTracker]"

    #@130
    const-string v2, "Emergency Service"

    #@132
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@135
    move v0, v1

    #@136
    .line 3466
    goto/16 :goto_1c

    #@138
    :pswitch_138
    move v0, v2

    #@139
    .line 3474
    goto/16 :goto_1c

    #@13b
    :pswitch_13b
    move v0, v2

    #@13c
    .line 3478
    goto/16 :goto_1c

    #@13e
    .line 3420
    :pswitch_data_13e
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_138
        :pswitch_1c
        :pswitch_1d
        :pswitch_1c
        :pswitch_13b
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_aa
        :pswitch_6
        :pswitch_aa
        :pswitch_aa
        :pswitch_aa
    .end packed-switch
.end method

.method private regCodesIsSearching(I)Z
    .registers 3
    .parameter "code"

    #@0
    .prologue
    .line 3510
    const/4 v0, 0x2

    #@1
    if-eq v0, p1, :cond_7

    #@3
    const/16 v0, 0xc

    #@5
    if-ne v0, p1, :cond_9

    #@7
    :cond_7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method private revertToNitzTime()V
    .registers 7

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 4141
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v0

    #@c
    const-string v1, "auto_time"

    #@e
    const/4 v2, 0x0

    #@f
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_16

    #@15
    .line 4153
    :cond_15
    :goto_15
    return-void

    #@16
    .line 4146
    :cond_16
    new-instance v0, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v1, "Reverting to NITZ Time: mSavedTime="

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    iget-wide v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTime:J

    #@23
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    const-string v1, " mSavedAtTime="

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    iget-wide v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedAtTime:J

    #@2f
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@3a
    .line 4149
    iget-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTime:J

    #@3c
    cmp-long v0, v0, v3

    #@3e
    if-eqz v0, :cond_15

    #@40
    iget-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedAtTime:J

    #@42
    cmp-long v0, v0, v3

    #@44
    if-eqz v0, :cond_15

    #@46
    .line 4150
    iget-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTime:J

    #@48
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@4b
    move-result-wide v2

    #@4c
    iget-wide v4, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedAtTime:J

    #@4e
    sub-long/2addr v2, v4

    #@4f
    add-long/2addr v0, v2

    #@50
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setAndBroadcastNetworkSetTime(J)V

    #@53
    goto :goto_15
.end method

.method private revertToNitzTimeZone()V
    .registers 4

    #@0
    .prologue
    .line 4156
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    const-string v1, "auto_time_zone"

    #@c
    const/4 v2, 0x0

    #@d
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_14

    #@13
    .line 4164
    :cond_13
    :goto_13
    return-void

    #@14
    .line 4160
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v1, "Reverting to NITZ TimeZone: tz=\'"

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTimeZone:Ljava/lang/String;

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@2c
    .line 4161
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTimeZone:Ljava/lang/String;

    #@2e
    if-eqz v0, :cond_13

    #@30
    .line 4162
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTimeZone:Ljava/lang/String;

    #@32
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setAndBroadcastNetworkSetTimeZone(Ljava/lang/String;)V

    #@35
    goto :goto_13
.end method

.method private saveNitzTime(J)V
    .registers 5
    .parameter "time"

    #@0
    .prologue
    .line 4100
    iput-wide p1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTime:J

    #@2
    .line 4101
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@5
    move-result-wide v0

    #@6
    iput-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedAtTime:J

    #@8
    .line 4102
    return-void
.end method

.method private saveNitzTimeZone(Ljava/lang/String;)V
    .registers 2
    .parameter "zoneId"

    #@0
    .prologue
    .line 4096
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTimeZone:Ljava/lang/String;

    #@2
    .line 4097
    return-void
.end method

.method private setAndBroadcastNetworkSetTime(J)V
    .registers 6
    .parameter "time"

    #@0
    .prologue
    .line 4132
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "setAndBroadcastNetworkSetTime: time="

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, "ms"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1c
    .line 4133
    invoke-static {p1, p2}, Landroid/os/SystemClock;->setCurrentTimeMillis(J)Z

    #@1f
    .line 4134
    new-instance v0, Landroid/content/Intent;

    #@21
    const-string v1, "android.intent.action.NETWORK_SET_TIME"

    #@23
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@26
    .line 4135
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2000

    #@28
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@2b
    .line 4136
    const-string v1, "time"

    #@2d
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@30
    .line 4137
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@32
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@35
    move-result-object v1

    #@36
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@38
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@3b
    .line 4138
    return-void
.end method

.method private setAndBroadcastNetworkSetTimeZone(Ljava/lang/String;)V
    .registers 6
    .parameter "zoneId"

    #@0
    .prologue
    .line 4111
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "setAndBroadcastNetworkSetTimeZone: setTimeZone="

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@16
    .line 4112
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@18
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, "alarm"

    #@1e
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/app/AlarmManager;

    #@24
    .line 4114
    .local v0, alarm:Landroid/app/AlarmManager;
    invoke-virtual {v0, p1}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    #@27
    .line 4115
    new-instance v1, Landroid/content/Intent;

    #@29
    const-string v2, "android.intent.action.NETWORK_SET_TIMEZONE"

    #@2b
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2e
    .line 4116
    .local v1, intent:Landroid/content/Intent;
    const/high16 v2, 0x2000

    #@30
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@33
    .line 4117
    const-string v2, "time-zone"

    #@35
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@38
    .line 4118
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3a
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@3d
    move-result-object v2

    #@3e
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@40
    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@43
    .line 4120
    new-instance v2, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v3, "setAndBroadcastNetworkSetTimeZone: call alarm.setTimeZone and broadcast zoneId="

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@59
    .line 4123
    return-void
.end method

.method private setNotification(I)V
    .registers 16
    .parameter "notifyType"

    #@0
    .prologue
    const v13, 0x209029d

    #@3
    const/4 v12, 0x0

    #@4
    .line 4180
    new-instance v9, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v10, "setNotification: create notification "

    #@b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v9

    #@f
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v9

    #@13
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v9

    #@17
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1a
    .line 4181
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1c
    invoke-virtual {v9}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@1f
    move-result-object v0

    #@20
    .line 4183
    .local v0, context:Landroid/content/Context;
    new-instance v9, Landroid/app/Notification;

    #@22
    invoke-direct {v9}, Landroid/app/Notification;-><init>()V

    #@25
    iput-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNotification:Landroid/app/Notification;

    #@27
    .line 4184
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNotification:Landroid/app/Notification;

    #@29
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@2c
    move-result-wide v10

    #@2d
    iput-wide v10, v9, Landroid/app/Notification;->when:J

    #@2f
    .line 4185
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNotification:Landroid/app/Notification;

    #@31
    const/16 v10, 0x10

    #@33
    iput v10, v9, Landroid/app/Notification;->flags:I

    #@35
    .line 4186
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNotification:Landroid/app/Notification;

    #@37
    const v10, 0x108008a

    #@3a
    iput v10, v9, Landroid/app/Notification;->icon:I

    #@3c
    .line 4187
    new-instance v2, Landroid/content/Intent;

    #@3e
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@41
    .line 4188
    .local v2, intent:Landroid/content/Intent;
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNotification:Landroid/app/Notification;

    #@43
    const/high16 v10, 0x1000

    #@45
    invoke-static {v0, v12, v2, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@48
    move-result-object v10

    #@49
    iput-object v10, v9, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@4b
    .line 4191
    const-string v1, ""

    #@4d
    .line 4192
    .local v1, details:Ljava/lang/CharSequence;
    const v9, 0x10400b8

    #@50
    invoke-virtual {v0, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@53
    move-result-object v8

    #@54
    .line 4193
    .local v8, title:Ljava/lang/CharSequence;
    const/16 v6, 0x3e7

    #@56
    .line 4195
    .local v6, notificationId:I
    packed-switch p1, :pswitch_data_200

    #@59
    .line 4263
    :cond_59
    :goto_59
    :pswitch_59
    new-instance v9, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v10, "setNotification: put notification "

    #@60
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v9

    #@64
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v9

    #@68
    const-string v10, " / "

    #@6a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v9

    #@6e
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v9

    #@72
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v9

    #@76
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@79
    .line 4264
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNotification:Landroid/app/Notification;

    #@7b
    iput-object v8, v9, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@7d
    .line 4265
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNotification:Landroid/app/Notification;

    #@7f
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNotification:Landroid/app/Notification;

    #@81
    iget-object v10, v10, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@83
    invoke-virtual {v9, v0, v8, v1, v10}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@86
    .line 4268
    const-string v9, "notification"

    #@88
    invoke-virtual {v0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8b
    move-result-object v7

    #@8c
    check-cast v7, Landroid/app/NotificationManager;

    #@8e
    .line 4271
    .local v7, notificationManager:Landroid/app/NotificationManager;
    const/16 v9, 0x3ea

    #@90
    if-eq p1, v9, :cond_96

    #@92
    const/16 v9, 0x3ec

    #@94
    if-ne p1, v9, :cond_1e5

    #@96
    .line 4273
    :cond_96
    invoke-virtual {v7, v6}, Landroid/app/NotificationManager;->cancel(I)V

    #@99
    .line 4282
    :goto_99
    return-void

    #@9a
    .line 4197
    .end local v7           #notificationManager:Landroid/app/NotificationManager;
    :pswitch_9a
    const/16 v6, 0x378

    #@9c
    .line 4198
    const v9, 0x10400b9

    #@9f
    invoke-virtual {v0, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@a2
    move-result-object v1

    #@a3
    .line 4199
    goto :goto_59

    #@a4
    .line 4201
    :pswitch_a4
    const/16 v6, 0x378

    #@a6
    .line 4202
    goto :goto_59

    #@a7
    .line 4204
    :pswitch_a7
    const v9, 0x10400bc

    #@aa
    invoke-virtual {v0, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@ad
    move-result-object v1

    #@ae
    .line 4205
    goto :goto_59

    #@af
    .line 4207
    :pswitch_af
    const v9, 0x10400bb

    #@b2
    invoke-virtual {v0, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@b5
    move-result-object v1

    #@b6
    .line 4208
    goto :goto_59

    #@b7
    .line 4210
    :pswitch_b7
    const v9, 0x10400ba

    #@ba
    invoke-virtual {v0, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@bd
    move-result-object v1

    #@be
    .line 4211
    goto :goto_59

    #@bf
    .line 4217
    :pswitch_bf
    iget-object v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@c1
    invoke-interface {v9}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@c4
    move-result-object v9

    #@c5
    iget-boolean v9, v9, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_KT:Z

    #@c7
    if-eqz v9, :cond_59

    #@c9
    .line 4218
    const/16 v6, 0x309

    #@cb
    .line 4219
    iget-object v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@cd
    invoke-interface {v9}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@d0
    move-result-object v9

    #@d1
    iget-object v9, v9, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@d3
    const-string v10, "LGTBASE"

    #@d5
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d8
    move-result v9

    #@d9
    if-eqz v9, :cond_1a8

    #@db
    .line 4220
    new-instance v9, Ljava/lang/StringBuilder;

    #@dd
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e0
    const-string v10, "[MIN]LGT pdp reject cause = "

    #@e2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v9

    #@e6
    iget v10, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->failCause:I

    #@e8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v9

    #@ec
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ef
    move-result-object v9

    #@f0
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@f3
    .line 4221
    invoke-virtual {v0, v13}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@f6
    move-result-object v8

    #@f7
    .line 4222
    new-instance v9, Ljava/lang/StringBuilder;

    #@f9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@fc
    invoke-virtual {v0, v13}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@ff
    move-result-object v10

    #@100
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v9

    #@104
    const-string v10, "("

    #@106
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v10

    #@10a
    iget v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->failCause:I

    #@10c
    if-eqz v9, :cond_11b

    #@10e
    iget v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->failCause:I

    #@110
    const v11, 0xffff

    #@113
    if-eq v9, v11, :cond_11b

    #@115
    iget v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->failCause:I

    #@117
    const/high16 v11, 0x1

    #@119
    if-ne v9, v11, :cond_1a0

    #@11b
    :cond_11b
    const-string v9, ""

    #@11d
    :goto_11d
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v9

    #@121
    const-string v10, ")"

    #@123
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v9

    #@127
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12a
    move-result-object v1

    #@12b
    .line 4224
    iget v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->failCause:I

    #@12d
    const/16 v10, 0x21

    #@12f
    if-ne v9, v10, :cond_59

    #@131
    iget-object v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@133
    invoke-interface {v9}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@136
    move-result-object v9

    #@137
    iget-boolean v9, v9, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_REJECT_INTENT_UPLUS:Z

    #@139
    if-eqz v9, :cond_59

    #@13b
    .line 4225
    new-instance v9, Ljava/lang/StringBuilder;

    #@13d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@140
    const-string v10, "[LGE_DATA][PDP_reject] EVENT_DATA_ERROR_FAIL_CAUSE ("

    #@142
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v9

    #@146
    iget v10, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->failCause:I

    #@148
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v9

    #@14c
    const-string v10, ")"

    #@14e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v9

    #@152
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@155
    move-result-object v9

    #@156
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@159
    .line 4228
    new-instance v3, Landroid/content/Intent;

    #@15b
    const-string v9, "android.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@15d
    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@160
    .line 4229
    .local v3, intent_pdp_reject:Landroid/content/Intent;
    const-string v9, "cause"

    #@162
    iget v10, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->failCause:I

    #@164
    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@167
    .line 4230
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@169
    invoke-virtual {v9}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@16c
    move-result-object v9

    #@16d
    invoke-virtual {v9, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@170
    .line 4233
    new-instance v4, Landroid/content/Intent;

    #@172
    const-string v9, "com.lge.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@174
    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@177
    .line 4234
    .local v4, intent_pdp_reject_lge:Landroid/content/Intent;
    const-string v9, "cause"

    #@179
    iget v10, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->failCause:I

    #@17b
    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@17e
    .line 4235
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@180
    invoke-virtual {v9}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@183
    move-result-object v9

    #@184
    invoke-virtual {v9, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@187
    .line 4238
    new-instance v5, Landroid/content/Intent;

    #@189
    const-string v9, "com.lge.mms.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@18b
    invoke-direct {v5, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@18e
    .line 4239
    .local v5, intent_pdp_reject_lge_mms:Landroid/content/Intent;
    const-string v9, "cause"

    #@190
    iget v10, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->failCause:I

    #@192
    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@195
    .line 4240
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@197
    invoke-virtual {v9}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@19a
    move-result-object v9

    #@19b
    invoke-virtual {v9, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@19e
    goto/16 :goto_59

    #@1a0
    .line 4222
    .end local v3           #intent_pdp_reject:Landroid/content/Intent;
    .end local v4           #intent_pdp_reject_lge:Landroid/content/Intent;
    .end local v5           #intent_pdp_reject_lge_mms:Landroid/content/Intent;
    :cond_1a0
    iget v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->failCause:I

    #@1a2
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a5
    move-result-object v9

    #@1a6
    goto/16 :goto_11d

    #@1a8
    .line 4243
    :cond_1a8
    iget-object v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1aa
    invoke-interface {v9}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1ad
    move-result-object v9

    #@1ae
    iget-object v9, v9, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@1b0
    const-string v10, "KTBASE"

    #@1b2
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b5
    move-result v9

    #@1b6
    if-eqz v9, :cond_59

    #@1b8
    .line 4245
    const v9, 0x209029c

    #@1bb
    invoke-virtual {v0, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1be
    move-result-object v8

    #@1bf
    .line 4246
    const v9, 0x2090292

    #@1c2
    invoke-virtual {v0, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1c5
    move-result-object v1

    #@1c6
    .line 4247
    const-string v9, "com.android.phone"

    #@1c8
    const-string v10, "com.android.phone.MobileNetworkSettings"

    #@1ca
    invoke-virtual {v2, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1cd
    .line 4248
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNotification:Landroid/app/Notification;

    #@1cf
    invoke-static {v0, v12, v2, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@1d2
    move-result-object v10

    #@1d3
    iput-object v10, v9, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@1d5
    goto/16 :goto_59

    #@1d7
    .line 4254
    :pswitch_1d7
    iget-object v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1d9
    invoke-interface {v9}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1dc
    move-result-object v9

    #@1dd
    iget-boolean v9, v9, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_KT:Z

    #@1df
    if-eqz v9, :cond_59

    #@1e1
    .line 4255
    const/16 v6, 0x309

    #@1e3
    goto/16 :goto_59

    #@1e5
    .line 4275
    .restart local v7       #notificationManager:Landroid/app/NotificationManager;
    :cond_1e5
    iget-object v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1e7
    invoke-interface {v9}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1ea
    move-result-object v9

    #@1eb
    iget-boolean v9, v9, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_KT:Z

    #@1ed
    if-eqz v9, :cond_1f8

    #@1ef
    const/16 v9, 0x3ef

    #@1f1
    if-ne p1, v9, :cond_1f8

    #@1f3
    .line 4276
    invoke-virtual {v7, v6}, Landroid/app/NotificationManager;->cancel(I)V

    #@1f6
    goto/16 :goto_99

    #@1f8
    .line 4280
    :cond_1f8
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNotification:Landroid/app/Notification;

    #@1fa
    invoke-virtual {v7, v6, v9}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@1fd
    goto/16 :goto_99

    #@1ff
    .line 4195
    nop

    #@200
    :pswitch_data_200
    .packed-switch 0x3e9
        :pswitch_9a
        :pswitch_a4
        :pswitch_a7
        :pswitch_59
        :pswitch_af
        :pswitch_b7
        :pswitch_1d7
        :pswitch_bf
    .end packed-switch
.end method

.method private setSignalStrengthDefaultValues()V
    .registers 3

    #@0
    .prologue
    .line 2552
    new-instance v0, Landroid/telephony/SignalStrength;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-direct {v0, v1}, Landroid/telephony/SignalStrength;-><init>(Z)V

    #@6
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@8
    .line 2553
    return-void
.end method

.method private setTimeFromNITZString(Ljava/lang/String;J)V
    .registers 34
    .parameter "nitz"
    .parameter "nitzReceiveTime"

    #@0
    .prologue
    .line 3902
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v19

    #@4
    .line 3903
    .local v19, start:J
    new-instance v25, Ljava/lang/StringBuilder;

    #@6
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v26, "NITZ: "

    #@b
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v25

    #@f
    move-object/from16 v0, v25

    #@11
    move-object/from16 v1, p1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v25

    #@17
    const-string v26, ","

    #@19
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v25

    #@1d
    move-object/from16 v0, v25

    #@1f
    move-wide/from16 v1, p2

    #@21
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@24
    move-result-object v25

    #@25
    const-string v26, " start="

    #@27
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v25

    #@2b
    move-object/from16 v0, v25

    #@2d
    move-wide/from16 v1, v19

    #@2f
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@32
    move-result-object v25

    #@33
    const-string v26, " delay="

    #@35
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v25

    #@39
    sub-long v26, v19, p2

    #@3b
    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v25

    #@3f
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v25

    #@43
    move-object/from16 v0, p0

    #@45
    move-object/from16 v1, v25

    #@47
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@4a
    .line 3910
    :try_start_4a
    const-string v25, "GMT"

    #@4c
    invoke-static/range {v25 .. v25}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@4f
    move-result-object v25

    #@50
    invoke-static/range {v25 .. v25}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    #@53
    move-result-object v5

    #@54
    .line 3912
    .local v5, c:Ljava/util/Calendar;
    invoke-virtual {v5}, Ljava/util/Calendar;->clear()V

    #@57
    .line 3913
    const/16 v25, 0x10

    #@59
    const/16 v26, 0x0

    #@5b
    move/from16 v0, v25

    #@5d
    move/from16 v1, v26

    #@5f
    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@62
    .line 3915
    const-string v25, "[/:,+-]"

    #@64
    move-object/from16 v0, p1

    #@66
    move-object/from16 v1, v25

    #@68
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@6b
    move-result-object v16

    #@6c
    .line 3917
    .local v16, nitzSubs:[Ljava/lang/String;
    const/16 v25, 0x0

    #@6e
    aget-object v25, v16, v25

    #@70
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@73
    move-result v25

    #@74
    move/from16 v0, v25

    #@76
    add-int/lit16 v0, v0, 0x7d0

    #@78
    move/from16 v23, v0

    #@7a
    .line 3918
    .local v23, year:I
    const/16 v25, 0x1

    #@7c
    move/from16 v0, v25

    #@7e
    move/from16 v1, v23

    #@80
    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@83
    .line 3921
    const/16 v25, 0x1

    #@85
    aget-object v25, v16, v25

    #@87
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8a
    move-result v25

    #@8b
    add-int/lit8 v15, v25, -0x1

    #@8d
    .line 3922
    .local v15, month:I
    const/16 v25, 0x2

    #@8f
    move/from16 v0, v25

    #@91
    invoke-virtual {v5, v0, v15}, Ljava/util/Calendar;->set(II)V

    #@94
    .line 3924
    const/16 v25, 0x2

    #@96
    aget-object v25, v16, v25

    #@98
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@9b
    move-result v6

    #@9c
    .line 3925
    .local v6, date:I
    const/16 v25, 0x5

    #@9e
    move/from16 v0, v25

    #@a0
    invoke-virtual {v5, v0, v6}, Ljava/util/Calendar;->set(II)V

    #@a3
    .line 3927
    const/16 v25, 0x3

    #@a5
    aget-object v25, v16, v25

    #@a7
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@aa
    move-result v9

    #@ab
    .line 3928
    .local v9, hour:I
    const/16 v25, 0xa

    #@ad
    move/from16 v0, v25

    #@af
    invoke-virtual {v5, v0, v9}, Ljava/util/Calendar;->set(II)V

    #@b2
    .line 3930
    const/16 v25, 0x4

    #@b4
    aget-object v25, v16, v25

    #@b6
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@b9
    move-result v14

    #@ba
    .line 3931
    .local v14, minute:I
    const/16 v25, 0xc

    #@bc
    move/from16 v0, v25

    #@be
    invoke-virtual {v5, v0, v14}, Ljava/util/Calendar;->set(II)V

    #@c1
    .line 3933
    const/16 v25, 0x5

    #@c3
    aget-object v25, v16, v25

    #@c5
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@c8
    move-result v17

    #@c9
    .line 3934
    .local v17, second:I
    const/16 v25, 0xd

    #@cb
    move/from16 v0, v25

    #@cd
    move/from16 v1, v17

    #@cf
    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@d2
    .line 3936
    const/16 v25, 0x2d

    #@d4
    move-object/from16 v0, p1

    #@d6
    move/from16 v1, v25

    #@d8
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    #@db
    move-result v25

    #@dc
    const/16 v26, -0x1

    #@de
    move/from16 v0, v25

    #@e0
    move/from16 v1, v26

    #@e2
    if-ne v0, v1, :cond_24e

    #@e4
    const/16 v18, 0x1

    #@e6
    .line 3938
    .local v18, sign:Z
    :goto_e6
    const/16 v25, 0x6

    #@e8
    aget-object v25, v16, v25

    #@ea
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@ed
    move-result v21

    #@ee
    .line 3940
    .local v21, tzOffset:I
    move-object/from16 v0, v16

    #@f0
    array-length v0, v0

    #@f1
    move/from16 v25, v0

    #@f3
    const/16 v26, 0x8

    #@f5
    move/from16 v0, v25

    #@f7
    move/from16 v1, v26

    #@f9
    if-lt v0, v1, :cond_252

    #@fb
    const/16 v25, 0x7

    #@fd
    aget-object v25, v16, v25

    #@ff
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@102
    move-result v7

    #@103
    .line 3950
    .local v7, dst:I
    :goto_103
    if-eqz v18, :cond_255

    #@105
    const/16 v25, 0x1

    #@107
    :goto_107
    mul-int v25, v25, v21

    #@109
    mul-int/lit8 v25, v25, 0xf

    #@10b
    mul-int/lit8 v25, v25, 0x3c

    #@10d
    move/from16 v0, v25

    #@10f
    mul-int/lit16 v0, v0, 0x3e8

    #@111
    move/from16 v21, v0

    #@113
    .line 3952
    const/16 v24, 0x0

    #@115
    .line 3958
    .local v24, zone:Ljava/util/TimeZone;
    move-object/from16 v0, v16

    #@117
    array-length v0, v0

    #@118
    move/from16 v25, v0

    #@11a
    const/16 v26, 0x9

    #@11c
    move/from16 v0, v25

    #@11e
    move/from16 v1, v26

    #@120
    if-lt v0, v1, :cond_132

    #@122
    .line 3959
    const/16 v25, 0x8

    #@124
    aget-object v25, v16, v25

    #@126
    const/16 v26, 0x21

    #@128
    const/16 v27, 0x2f

    #@12a
    invoke-virtual/range {v25 .. v27}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@12d
    move-result-object v22

    #@12e
    .line 3960
    .local v22, tzname:Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@131
    move-result-object v24

    #@132
    .line 3963
    .end local v22           #tzname:Ljava/lang/String;
    :cond_132
    const-string v25, "gsm.operator.iso-country"

    #@134
    const-string v26, ""

    #@136
    move-object/from16 v0, p0

    #@138
    move-object/from16 v1, v25

    #@13a
    move-object/from16 v2, v26

    #@13c
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getSystemProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@13f
    move-result-object v11

    #@140
    .line 3965
    .local v11, iso:Ljava/lang/String;
    if-nez v24, :cond_184

    #@142
    .line 3967
    move-object/from16 v0, p0

    #@144
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGotCountryCode:Z

    #@146
    move/from16 v25, v0

    #@148
    if-eqz v25, :cond_184

    #@14a
    .line 3968
    if-eqz v11, :cond_261

    #@14c
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    #@14f
    move-result v25

    #@150
    if-lez v25, :cond_261

    #@152
    .line 3969
    if-eqz v7, :cond_259

    #@154
    const/16 v25, 0x1

    #@156
    :goto_156
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@159
    move-result-wide v26

    #@15a
    move/from16 v0, v21

    #@15c
    move/from16 v1, v25

    #@15e
    move-wide/from16 v2, v26

    #@160
    invoke-static {v0, v1, v2, v3, v11}, Landroid/util/TimeUtils;->getTimeZone(IZJLjava/lang/String;)Ljava/util/TimeZone;

    #@163
    move-result-object v24

    #@164
    .line 3974
    const-string v25, "JP"

    #@166
    const-string v26, "DCM"

    #@168
    invoke-static/range {v25 .. v26}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@16b
    move-result v25

    #@16c
    if-eqz v25, :cond_184

    #@16e
    .line 3975
    if-nez v24, :cond_184

    #@170
    .line 3976
    if-eqz v7, :cond_25d

    #@172
    const/16 v25, 0x1

    #@174
    :goto_174
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@177
    move-result-wide v26

    #@178
    move-object/from16 v0, p0

    #@17a
    move/from16 v1, v21

    #@17c
    move/from16 v2, v25

    #@17e
    move-wide/from16 v3, v26

    #@180
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getNitzTimeZone(IZJ)Ljava/util/TimeZone;

    #@183
    move-result-object v24

    #@184
    .line 3991
    :cond_184
    :goto_184
    if-eqz v24, :cond_1a2

    #@186
    move-object/from16 v0, p0

    #@188
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@18a
    move/from16 v25, v0

    #@18c
    move/from16 v0, v25

    #@18e
    move/from16 v1, v21

    #@190
    if-ne v0, v1, :cond_1a2

    #@192
    move-object/from16 v0, p0

    #@194
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@196
    move/from16 v26, v0

    #@198
    if-eqz v7, :cond_27a

    #@19a
    const/16 v25, 0x1

    #@19c
    :goto_19c
    move/from16 v0, v26

    #@19e
    move/from16 v1, v25

    #@1a0
    if-eq v0, v1, :cond_214

    #@1a2
    .line 3996
    :cond_1a2
    const/16 v25, 0x1

    #@1a4
    move/from16 v0, v25

    #@1a6
    move-object/from16 v1, p0

    #@1a8
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@1aa
    .line 3997
    move/from16 v0, v21

    #@1ac
    move-object/from16 v1, p0

    #@1ae
    iput v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@1b0
    .line 3998
    if-eqz v7, :cond_27e

    #@1b2
    const/16 v25, 0x1

    #@1b4
    :goto_1b4
    move/from16 v0, v25

    #@1b6
    move-object/from16 v1, p0

    #@1b8
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@1ba
    .line 3999
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@1bd
    move-result-wide v25

    #@1be
    move-wide/from16 v0, v25

    #@1c0
    move-object/from16 v2, p0

    #@1c2
    iput-wide v0, v2, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneTime:J

    #@1c4
    .line 4002
    new-instance v25, Ljava/lang/StringBuilder;

    #@1c6
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@1c9
    const-string v26, "Saving NITZ info. : mNeedFixZoneAfterNitz = "

    #@1cb
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ce
    move-result-object v25

    #@1cf
    move-object/from16 v0, p0

    #@1d1
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@1d3
    move/from16 v26, v0

    #@1d5
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v25

    #@1d9
    const-string v26, ", mZoneOffset = "

    #@1db
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1de
    move-result-object v25

    #@1df
    move-object/from16 v0, p0

    #@1e1
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@1e3
    move/from16 v26, v0

    #@1e5
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v25

    #@1e9
    const-string v26, ", mZoneDst = "

    #@1eb
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v25

    #@1ef
    move-object/from16 v0, p0

    #@1f1
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@1f3
    move/from16 v26, v0

    #@1f5
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v25

    #@1f9
    const-string v26, ", mZoneTime = "

    #@1fb
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v25

    #@1ff
    move-object/from16 v0, p0

    #@201
    iget-wide v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneTime:J

    #@203
    move-wide/from16 v26, v0

    #@205
    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@208
    move-result-object v25

    #@209
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20c
    move-result-object v25

    #@20d
    move-object/from16 v0, p0

    #@20f
    move-object/from16 v1, v25

    #@211
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@214
    .line 4008
    :cond_214
    if-eqz v24, :cond_232

    #@216
    .line 4009
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getAutoTimeZone()Z

    #@219
    move-result v25

    #@21a
    if-eqz v25, :cond_227

    #@21c
    .line 4010
    invoke-virtual/range {v24 .. v24}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@21f
    move-result-object v25

    #@220
    move-object/from16 v0, p0

    #@222
    move-object/from16 v1, v25

    #@224
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setAndBroadcastNetworkSetTimeZone(Ljava/lang/String;)V

    #@227
    .line 4012
    :cond_227
    invoke-virtual/range {v24 .. v24}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@22a
    move-result-object v25

    #@22b
    move-object/from16 v0, p0

    #@22d
    move-object/from16 v1, v25

    #@22f
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->saveNitzTimeZone(Ljava/lang/String;)V

    #@232
    .line 4015
    :cond_232
    const-string v25, "gsm.ignore-nitz"

    #@234
    invoke-static/range {v25 .. v25}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@237
    move-result-object v10

    #@238
    .line 4016
    .local v10, ignore:Ljava/lang/String;
    if-eqz v10, :cond_282

    #@23a
    const-string v25, "yes"

    #@23c
    move-object/from16 v0, v25

    #@23e
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@241
    move-result v25

    #@242
    if-eqz v25, :cond_282

    #@244
    .line 4017
    const-string v25, "NITZ: Not setting clock because gsm.ignore-nitz is set"

    #@246
    move-object/from16 v0, p0

    #@248
    move-object/from16 v1, v25

    #@24a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@24d
    .line 4075
    .end local v5           #c:Ljava/util/Calendar;
    .end local v6           #date:I
    .end local v7           #dst:I
    .end local v9           #hour:I
    .end local v10           #ignore:Ljava/lang/String;
    .end local v11           #iso:Ljava/lang/String;
    .end local v14           #minute:I
    .end local v15           #month:I
    .end local v16           #nitzSubs:[Ljava/lang/String;
    .end local v17           #second:I
    .end local v18           #sign:Z
    .end local v21           #tzOffset:I
    .end local v23           #year:I
    .end local v24           #zone:Ljava/util/TimeZone;
    :goto_24d
    return-void

    #@24e
    .line 3936
    .restart local v5       #c:Ljava/util/Calendar;
    .restart local v6       #date:I
    .restart local v9       #hour:I
    .restart local v14       #minute:I
    .restart local v15       #month:I
    .restart local v16       #nitzSubs:[Ljava/lang/String;
    .restart local v17       #second:I
    .restart local v23       #year:I
    :cond_24e
    const/16 v18, 0x0

    #@250
    goto/16 :goto_e6

    #@252
    .line 3940
    .restart local v18       #sign:Z
    .restart local v21       #tzOffset:I
    :cond_252
    const/4 v7, 0x0

    #@253
    goto/16 :goto_103

    #@255
    .line 3950
    .restart local v7       #dst:I
    :cond_255
    const/16 v25, -0x1

    #@257
    goto/16 :goto_107

    #@259
    .line 3969
    .restart local v11       #iso:Ljava/lang/String;
    .restart local v24       #zone:Ljava/util/TimeZone;
    :cond_259
    const/16 v25, 0x0

    #@25b
    goto/16 :goto_156

    #@25d
    .line 3976
    :cond_25d
    const/16 v25, 0x0

    #@25f
    goto/16 :goto_174

    #@261
    .line 3986
    :cond_261
    if-eqz v7, :cond_277

    #@263
    const/16 v25, 0x1

    #@265
    :goto_265
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@268
    move-result-wide v26

    #@269
    move-object/from16 v0, p0

    #@26b
    move/from16 v1, v21

    #@26d
    move/from16 v2, v25

    #@26f
    move-wide/from16 v3, v26

    #@271
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getNitzTimeZone(IZJ)Ljava/util/TimeZone;
    :try_end_274
    .catch Ljava/lang/RuntimeException; {:try_start_4a .. :try_end_274} :catch_2c5

    #@274
    move-result-object v24

    #@275
    goto/16 :goto_184

    #@277
    :cond_277
    const/16 v25, 0x0

    #@279
    goto :goto_265

    #@27a
    .line 3991
    :cond_27a
    const/16 v25, 0x0

    #@27c
    goto/16 :goto_19c

    #@27e
    .line 3998
    :cond_27e
    const/16 v25, 0x0

    #@280
    goto/16 :goto_1b4

    #@282
    .line 4022
    .restart local v10       #ignore:Ljava/lang/String;
    :cond_282
    :try_start_282
    move-object/from16 v0, p0

    #@284
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@286
    move-object/from16 v25, v0

    #@288
    invoke-virtual/range {v25 .. v25}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@28b
    .line 4024
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getAutoTime()Z

    #@28e
    move-result v25

    #@28f
    if-eqz v25, :cond_393

    #@291
    .line 4025
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@294
    move-result-wide v25

    #@295
    sub-long v12, v25, p2

    #@297
    .line 4028
    .local v12, millisSinceNitzReceived:J
    const-wide/16 v25, 0x0

    #@299
    cmp-long v25, v12, v25

    #@29b
    if-gez v25, :cond_2f2

    #@29d
    .line 4031
    new-instance v25, Ljava/lang/StringBuilder;

    #@29f
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@2a2
    const-string v26, "NITZ: not setting time, clock has rolled backwards since NITZ time was received, "

    #@2a4
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a7
    move-result-object v25

    #@2a8
    move-object/from16 v0, v25

    #@2aa
    move-object/from16 v1, p1

    #@2ac
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2af
    move-result-object v25

    #@2b0
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b3
    move-result-object v25

    #@2b4
    move-object/from16 v0, p0

    #@2b6
    move-object/from16 v1, v25

    #@2b8
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V
    :try_end_2bb
    .catchall {:try_start_282 .. :try_end_2bb} :catchall_3be

    #@2bb
    .line 4070
    :try_start_2bb
    move-object/from16 v0, p0

    #@2bd
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2bf
    move-object/from16 v25, v0

    #@2c1
    invoke-virtual/range {v25 .. v25}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_2c4
    .catch Ljava/lang/RuntimeException; {:try_start_2bb .. :try_end_2c4} :catch_2c5

    #@2c4
    goto :goto_24d

    #@2c5
    .line 4072
    .end local v5           #c:Ljava/util/Calendar;
    .end local v6           #date:I
    .end local v7           #dst:I
    .end local v9           #hour:I
    .end local v10           #ignore:Ljava/lang/String;
    .end local v11           #iso:Ljava/lang/String;
    .end local v12           #millisSinceNitzReceived:J
    .end local v14           #minute:I
    .end local v15           #month:I
    .end local v16           #nitzSubs:[Ljava/lang/String;
    .end local v17           #second:I
    .end local v18           #sign:Z
    .end local v21           #tzOffset:I
    .end local v23           #year:I
    .end local v24           #zone:Ljava/util/TimeZone;
    :catch_2c5
    move-exception v8

    #@2c6
    .line 4073
    .local v8, ex:Ljava/lang/RuntimeException;
    new-instance v25, Ljava/lang/StringBuilder;

    #@2c8
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@2cb
    const-string v26, "NITZ: Parsing NITZ time "

    #@2cd
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d0
    move-result-object v25

    #@2d1
    move-object/from16 v0, v25

    #@2d3
    move-object/from16 v1, p1

    #@2d5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d8
    move-result-object v25

    #@2d9
    const-string v26, " ex="

    #@2db
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2de
    move-result-object v25

    #@2df
    move-object/from16 v0, v25

    #@2e1
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e4
    move-result-object v25

    #@2e5
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e8
    move-result-object v25

    #@2e9
    move-object/from16 v0, p0

    #@2eb
    move-object/from16 v1, v25

    #@2ed
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@2f0
    goto/16 :goto_24d

    #@2f2
    .line 4038
    .end local v8           #ex:Ljava/lang/RuntimeException;
    .restart local v5       #c:Ljava/util/Calendar;
    .restart local v6       #date:I
    .restart local v7       #dst:I
    .restart local v9       #hour:I
    .restart local v10       #ignore:Ljava/lang/String;
    .restart local v11       #iso:Ljava/lang/String;
    .restart local v12       #millisSinceNitzReceived:J
    .restart local v14       #minute:I
    .restart local v15       #month:I
    .restart local v16       #nitzSubs:[Ljava/lang/String;
    .restart local v17       #second:I
    .restart local v18       #sign:Z
    .restart local v21       #tzOffset:I
    .restart local v23       #year:I
    .restart local v24       #zone:Ljava/util/TimeZone;
    :cond_2f2
    const-wide/32 v25, 0x7fffffff

    #@2f5
    cmp-long v25, v12, v25

    #@2f7
    if-lez v25, :cond_329

    #@2f9
    .line 4041
    :try_start_2f9
    new-instance v25, Ljava/lang/StringBuilder;

    #@2fb
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@2fe
    const-string v26, "NITZ: not setting time, processing has taken "

    #@300
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@303
    move-result-object v25

    #@304
    const-wide/32 v26, 0x5265c00

    #@307
    div-long v26, v12, v26

    #@309
    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@30c
    move-result-object v25

    #@30d
    const-string v26, " days"

    #@30f
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@312
    move-result-object v25

    #@313
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@316
    move-result-object v25

    #@317
    move-object/from16 v0, p0

    #@319
    move-object/from16 v1, v25

    #@31b
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V
    :try_end_31e
    .catchall {:try_start_2f9 .. :try_end_31e} :catchall_3be

    #@31e
    .line 4070
    :try_start_31e
    move-object/from16 v0, p0

    #@320
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@322
    move-object/from16 v25, v0

    #@324
    invoke-virtual/range {v25 .. v25}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_327
    .catch Ljava/lang/RuntimeException; {:try_start_31e .. :try_end_327} :catch_2c5

    #@327
    goto/16 :goto_24d

    #@329
    .line 4049
    :cond_329
    const/16 v25, 0xe

    #@32b
    long-to-int v0, v12

    #@32c
    move/from16 v26, v0

    #@32e
    :try_start_32e
    move/from16 v0, v25

    #@330
    move/from16 v1, v26

    #@332
    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->add(II)V

    #@335
    .line 4052
    new-instance v25, Ljava/lang/StringBuilder;

    #@337
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@33a
    const-string v26, "NITZ: Setting time of day to "

    #@33c
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33f
    move-result-object v25

    #@340
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@343
    move-result-object v26

    #@344
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@347
    move-result-object v25

    #@348
    const-string v26, " NITZ receive delay(ms): "

    #@34a
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34d
    move-result-object v25

    #@34e
    move-object/from16 v0, v25

    #@350
    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@353
    move-result-object v25

    #@354
    const-string v26, " gained(ms): "

    #@356
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@359
    move-result-object v25

    #@35a
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@35d
    move-result-wide v26

    #@35e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@361
    move-result-wide v28

    #@362
    sub-long v26, v26, v28

    #@364
    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@367
    move-result-object v25

    #@368
    const-string v26, " from "

    #@36a
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36d
    move-result-object v25

    #@36e
    move-object/from16 v0, v25

    #@370
    move-object/from16 v1, p1

    #@372
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@375
    move-result-object v25

    #@376
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@379
    move-result-object v25

    #@37a
    move-object/from16 v0, p0

    #@37c
    move-object/from16 v1, v25

    #@37e
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@381
    .line 4059
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@384
    move-result-wide v25

    #@385
    move-object/from16 v0, p0

    #@387
    move-wide/from16 v1, v25

    #@389
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setAndBroadcastNetworkSetTime(J)V

    #@38c
    .line 4060
    const-string v25, "GSM"

    #@38e
    const-string v26, "NITZ: after Setting time of day"

    #@390
    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@393
    .line 4062
    .end local v12           #millisSinceNitzReceived:J
    :cond_393
    const-string v25, "gsm.nitz.time"

    #@395
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@398
    move-result-wide v26

    #@399
    invoke-static/range {v26 .. v27}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@39c
    move-result-object v26

    #@39d
    invoke-static/range {v25 .. v26}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@3a0
    .line 4063
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@3a3
    move-result-wide v25

    #@3a4
    move-object/from16 v0, p0

    #@3a6
    move-wide/from16 v1, v25

    #@3a8
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->saveNitzTime(J)V

    #@3ab
    .line 4068
    const/16 v25, 0x1

    #@3ad
    move/from16 v0, v25

    #@3af
    move-object/from16 v1, p0

    #@3b1
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNitzUpdatedTime:Z
    :try_end_3b3
    .catchall {:try_start_32e .. :try_end_3b3} :catchall_3be

    #@3b3
    .line 4070
    :try_start_3b3
    move-object/from16 v0, p0

    #@3b5
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3b7
    move-object/from16 v25, v0

    #@3b9
    invoke-virtual/range {v25 .. v25}, Landroid/os/PowerManager$WakeLock;->release()V

    #@3bc
    goto/16 :goto_24d

    #@3be
    :catchall_3be
    move-exception v25

    #@3bf
    move-object/from16 v0, p0

    #@3c1
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3c3
    move-object/from16 v26, v0

    #@3c5
    invoke-virtual/range {v26 .. v26}, Landroid/os/PowerManager$WakeLock;->release()V

    #@3c8
    throw v25
    :try_end_3c9
    .catch Ljava/lang/RuntimeException; {:try_start_3b3 .. :try_end_3c9} :catch_2c5
.end method

.method private showDialog(Ljava/lang/String;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 5212
    const-string v1, "[GsmServiceStateTracker]"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "showDialog : Rejection code :"

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 5213
    sput-boolean v4, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bDisplayedRejectCause:Z

    #@1b
    .line 5216
    sput-boolean v4, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bAlreadyShownRejectCause:Z

    #@1d
    .line 5218
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@1f
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@21
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@24
    move-result-object v2

    #@25
    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@28
    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@33
    move-result-object v1

    #@34
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->networkDialog:Landroid/app/AlertDialog;

    #@36
    .line 5220
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->networkDialog:Landroid/app/AlertDialog;

    #@38
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@3b
    move-result-object v1

    #@3c
    const/16 v2, 0x7d8

    #@3e
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@41
    .line 5221
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->networkDialog:Landroid/app/AlertDialog;

    #@43
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mDialogOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    #@45
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    #@48
    .line 5222
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->networkDialog:Landroid/app/AlertDialog;

    #@4a
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@4d
    .line 5224
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeoutHandler:Landroid/os/Handler;

    #@4f
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeoutHandler:Landroid/os/Handler;

    #@51
    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@54
    move-result-object v2

    #@55
    const-wide/32 v3, 0x1d4c0

    #@58
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@5b
    .line 5227
    :try_start_5b
    new-instance v1, Landroid/media/ToneGenerator;

    #@5d
    const/4 v2, 0x1

    #@5e
    const/16 v3, 0x64

    #@60
    invoke-direct {v1, v2, v3}, Landroid/media/ToneGenerator;-><init>(II)V

    #@63
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mToneGenerator:Landroid/media/ToneGenerator;
    :try_end_65
    .catch Ljava/lang/RuntimeException; {:try_start_5b .. :try_end_65} :catch_71

    #@65
    .line 5232
    :goto_65
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mToneGenerator:Landroid/media/ToneGenerator;

    #@67
    if-eqz v1, :cond_70

    #@69
    .line 5233
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mToneGenerator:Landroid/media/ToneGenerator;

    #@6b
    const/16 v2, 0x1c

    #@6d
    invoke-virtual {v1, v2}, Landroid/media/ToneGenerator;->startTone(I)Z

    #@70
    .line 5235
    :cond_70
    return-void

    #@71
    .line 5228
    :catch_71
    move-exception v0

    #@72
    .line 5229
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v1, "GSM"

    #@74
    new-instance v2, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v3, "Exception caught while creating local tone generator: "

    #@7b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v2

    #@83
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v2

    #@87
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 5230
    const/4 v1, 0x0

    #@8b
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mToneGenerator:Landroid/media/ToneGenerator;

    #@8d
    goto :goto_65
.end method

.method private static sloge(Ljava/lang/String;)V
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 4391
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[GsmSST] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 4392
    return-void
.end method

.method private static twoDigitsAt(Ljava/lang/String;I)I
    .registers 6
    .parameter "s"
    .parameter "offset"

    #@0
    .prologue
    const/16 v3, 0xa

    #@2
    .line 3831
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    #@5
    move-result v2

    #@6
    invoke-static {v2, v3}, Ljava/lang/Character;->digit(CI)I

    #@9
    move-result v0

    #@a
    .line 3832
    .local v0, a:I
    add-int/lit8 v2, p1, 0x1

    #@c
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v2

    #@10
    invoke-static {v2, v3}, Ljava/lang/Character;->digit(CI)I

    #@13
    move-result v1

    #@14
    .line 3834
    .local v1, b:I
    if-ltz v0, :cond_18

    #@16
    if-gez v1, :cond_20

    #@18
    .line 3836
    :cond_18
    new-instance v2, Ljava/lang/RuntimeException;

    #@1a
    const-string v3, "invalid format"

    #@1c
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v2

    #@20
    .line 3839
    :cond_20
    mul-int/lit8 v2, v0, 0xa

    #@22
    add-int/2addr v2, v1

    #@23
    return v2
.end method

.method private updateSpnDisplayKT()V
    .registers 14

    #@0
    .prologue
    const/high16 v12, 0x2000

    #@2
    const/4 v10, 0x0

    #@3
    const/4 v9, 0x1

    #@4
    .line 4551
    iput-boolean v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mFirstUpdateSpn:Z

    #@6
    .line 4553
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@8
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    .line 4555
    .local v2, plmn:Ljava/lang/String;
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@e
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@10
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@13
    move-result-object v8

    #@14
    if-nez v8, :cond_a9

    #@16
    .line 4557
    iget-boolean v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@18
    if-eqz v8, :cond_72

    #@1a
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1c
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@1f
    move-result-object v8

    #@20
    invoke-virtual {v8}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@23
    move-result v8

    #@24
    if-eqz v8, :cond_72

    #@26
    .line 4558
    const-string v8, "emergency_calls_only_kt"

    #@28
    invoke-static {v8}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    .line 4560
    new-instance v8, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v10, "updateSpnDisplayKT: emergency only and radio is on plmn=\'"

    #@33
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v8

    #@3b
    const-string v10, "\'"

    #@3d
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v8

    #@45
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@48
    .line 4567
    :cond_48
    :goto_48
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@4a
    invoke-static {v2, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@4d
    move-result v8

    #@4e
    if-nez v8, :cond_6f

    #@50
    .line 4568
    new-instance v1, Landroid/content/Intent;

    #@52
    const-string v8, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@54
    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@57
    .line 4569
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@5a
    .line 4570
    const-string v8, "showPlmn"

    #@5c
    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@5f
    .line 4571
    const-string v8, "plmn"

    #@61
    invoke-virtual {v1, v8, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@64
    .line 4572
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@66
    invoke-virtual {v8}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@69
    move-result-object v8

    #@6a
    sget-object v9, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@6c
    invoke-virtual {v8, v1, v9}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@6f
    .line 4575
    .end local v1           #intent:Landroid/content/Intent;
    :cond_6f
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@71
    .line 4699
    :goto_71
    return-void

    #@72
    .line 4561
    :cond_72
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@74
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getState()I

    #@77
    move-result v8

    #@78
    if-ne v8, v9, :cond_48

    #@7a
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@7c
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@7f
    move-result-object v8

    #@80
    invoke-virtual {v8}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@83
    move-result v8

    #@84
    if-eqz v8, :cond_48

    #@86
    .line 4562
    const-string v8, "service_disabled"

    #@88
    invoke-static {v8}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@8b
    move-result-object v2

    #@8c
    .line 4564
    new-instance v8, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v10, "updateSpnDisplayKT: STATE_OUT_OF_SERVICE plmn ="

    #@93
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v8

    #@97
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v8

    #@9b
    const-string v10, "\'"

    #@9d
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v8

    #@a1
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v8

    #@a5
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@a8
    goto :goto_48

    #@a9
    .line 4580
    :cond_a9
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@ab
    .line 4581
    .local v0, iccRecords:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_2be

    #@ad
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@af
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@b2
    move-result-object v8

    #@b3
    invoke-virtual {v0, v8}, Lcom/android/internal/telephony/uicc/IccRecords;->getDisplayRule(Ljava/lang/String;)I

    #@b6
    move-result v4

    #@b7
    .line 4582
    .local v4, rule:I
    :goto_b7
    if-eqz v0, :cond_2c1

    #@b9
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getServiceProviderName()Ljava/lang/String;

    #@bc
    move-result-object v7

    #@bd
    .line 4590
    .local v7, spn:Ljava/lang/String;
    :goto_bd
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c0
    move-result v8

    #@c1
    if-eqz v8, :cond_d1

    #@c3
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@c5
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getOperatorAlphaShort()Ljava/lang/String;

    #@c8
    move-result-object v8

    #@c9
    if-eqz v8, :cond_d1

    #@cb
    .line 4591
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@cd
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getOperatorAlphaShort()Ljava/lang/String;

    #@d0
    move-result-object v2

    #@d1
    .line 4594
    :cond_d1
    if-eqz v7, :cond_dd

    #@d3
    const-string v8, "KTF"

    #@d5
    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@d8
    move-result v8

    #@d9
    if-eqz v8, :cond_dd

    #@db
    .line 4595
    const-string v7, "KT"

    #@dd
    .line 4601
    :cond_dd
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@df
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@e1
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@e4
    move-result-object v8

    #@e5
    if-eqz v8, :cond_fd

    #@e7
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@e9
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getState()I

    #@ec
    move-result v8

    #@ed
    if-nez v8, :cond_fd

    #@ef
    .line 4602
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@f1
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@f3
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@f6
    move-result-object v8

    #@f7
    check-cast v8, Lcom/android/internal/telephony/uicc/IccRecords;

    #@f9
    invoke-virtual {v8}, Lcom/android/internal/telephony/uicc/IccRecords;->getServiceProviderName()Ljava/lang/String;

    #@fc
    move-result-object v7

    #@fd
    .line 4606
    :cond_fd
    iget-boolean v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@ff
    if-eqz v8, :cond_12f

    #@101
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@103
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@106
    move-result-object v8

    #@107
    invoke-virtual {v8}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@10a
    move-result v8

    #@10b
    if-eqz v8, :cond_12f

    #@10d
    .line 4607
    const-string v8, "emergency_calls_only_kt"

    #@10f
    invoke-static {v8}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@112
    move-result-object v2

    #@113
    .line 4609
    new-instance v8, Ljava/lang/StringBuilder;

    #@115
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@118
    const-string v11, "updateSpnDisplayKT: emergency only and radio is on plmn=\'"

    #@11a
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v8

    #@11e
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v8

    #@122
    const-string v11, "\'"

    #@124
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@127
    move-result-object v8

    #@128
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12b
    move-result-object v8

    #@12c
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@12f
    .line 4613
    :cond_12f
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@132
    move-result v8

    #@133
    if-eqz v8, :cond_141

    #@135
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@138
    move-result v8

    #@139
    if-eqz v8, :cond_141

    #@13b
    .line 4614
    const-string v8, "service_disabled"

    #@13d
    invoke-static {v8}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@140
    move-result-object v2

    #@141
    .line 4620
    :cond_141
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@143
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getState()I

    #@146
    move-result v8

    #@147
    if-eqz v8, :cond_164

    #@149
    .line 4621
    const-string v8, "persist.radio.camped_mccmnc"

    #@14b
    const-string v11, "false"

    #@14d
    invoke-static {v8, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@150
    move-result-object v3

    #@151
    .line 4622
    .local v3, prop_mccmnc:Ljava/lang/String;
    const-string v8, "45005"

    #@153
    invoke-virtual {v3, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@156
    move-result v8

    #@157
    if-eqz v8, :cond_164

    #@159
    .line 4623
    const-string v8, "updateSpnDisplaySKT : KT_USIM + SKT band(45005)"

    #@15b
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@15e
    .line 4624
    const-string v8, "kt_network_rej_code_status_nw_skt_scannced"

    #@160
    invoke-static {v8}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@163
    move-result-object v2

    #@164
    .line 4630
    .end local v3           #prop_mccmnc:Ljava/lang/String;
    :cond_164
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@166
    invoke-virtual {v8}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@169
    move-result-object v8

    #@16a
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16d
    move-result-object v8

    #@16e
    const-string v11, "airplane_mode_on"

    #@170
    invoke-static {v8, v11, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@173
    move-result v8

    #@174
    if-ne v8, v9, :cond_185

    #@176
    .line 4631
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@179
    move-result-object v8

    #@17a
    const v11, 0x104010b

    #@17d
    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@180
    move-result-object v8

    #@181
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@184
    move-result-object v2

    #@185
    .line 4635
    :cond_185
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@187
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getState()I

    #@18a
    move-result v8

    #@18b
    if-eqz v8, :cond_1bf

    #@18d
    .line 4636
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@190
    move-result v8

    #@191
    if-eqz v8, :cond_2c5

    #@193
    .line 4637
    new-instance v8, Ljava/lang/StringBuilder;

    #@195
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@198
    const-string v11, "Not Normal Service PLMN Empty to service_disabled - state: "

    #@19a
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v8

    #@19e
    iget-object v11, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1a0
    invoke-virtual {v11}, Landroid/telephony/ServiceState;->getState()I

    #@1a3
    move-result v11

    #@1a4
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v8

    #@1a8
    const-string v11, ", plmn="

    #@1aa
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ad
    move-result-object v8

    #@1ae
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b1
    move-result-object v8

    #@1b2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b5
    move-result-object v8

    #@1b6
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1b9
    .line 4638
    const-string v8, "service_disabled"

    #@1bb
    invoke-static {v8}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@1be
    move-result-object v2

    #@1bf
    .line 4650
    :cond_1bf
    :goto_1bf
    iget-boolean v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@1c1
    if-nez v8, :cond_2fb

    #@1c3
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1c6
    move-result v8

    #@1c7
    if-nez v8, :cond_2fb

    #@1c9
    and-int/lit8 v8, v4, 0x1

    #@1cb
    if-ne v8, v9, :cond_2fb

    #@1cd
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1cf
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getState()I

    #@1d2
    move-result v8

    #@1d3
    if-nez v8, :cond_2fb

    #@1d5
    move v6, v9

    #@1d6
    .line 4653
    .local v6, showSpn:Z
    :goto_1d6
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1d9
    move-result v8

    #@1da
    if-nez v8, :cond_2fe

    #@1dc
    and-int/lit8 v8, v4, 0x2

    #@1de
    const/4 v11, 0x2

    #@1df
    if-ne v8, v11, :cond_2fe

    #@1e1
    move v5, v9

    #@1e2
    .line 4658
    .local v5, showPlmn:Z
    :goto_1e2
    iget-boolean v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowPlmn:Z

    #@1e4
    if-ne v5, v8, :cond_1fa

    #@1e6
    iget-boolean v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowSpn:Z

    #@1e8
    if-ne v6, v8, :cond_1fa

    #@1ea
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curSpn:Ljava/lang/String;

    #@1ec
    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1ef
    move-result v8

    #@1f0
    if-eqz v8, :cond_1fa

    #@1f2
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@1f4
    invoke-static {v2, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1f7
    move-result v8

    #@1f8
    if-nez v8, :cond_2b4

    #@1fa
    .line 4664
    :cond_1fa
    if-ne v6, v9, :cond_20d

    #@1fc
    if-ne v5, v9, :cond_20d

    #@1fe
    if-eqz v7, :cond_206

    #@200
    invoke-virtual {v7, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@203
    move-result v8

    #@204
    if-nez v8, :cond_20c

    #@206
    :cond_206
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@209
    move-result v8

    #@20a
    if-nez v8, :cond_20d

    #@20c
    .line 4667
    :cond_20c
    const/4 v6, 0x0

    #@20d
    .line 4671
    :cond_20d
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@20f
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getState()I

    #@212
    move-result v8

    #@213
    if-nez v8, :cond_301

    #@215
    .line 4672
    new-instance v8, Ljava/lang/StringBuilder;

    #@217
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@21a
    const-string v9, "Normal Service - showPlmn: "

    #@21c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21f
    move-result-object v8

    #@220
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@223
    move-result-object v8

    #@224
    const-string v9, " showSpn: "

    #@226
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@229
    move-result-object v8

    #@22a
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v8

    #@22e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@231
    move-result-object v8

    #@232
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@235
    .line 4673
    new-instance v8, Ljava/lang/StringBuilder;

    #@237
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@23a
    const-string v9, "Normal Service - plmn: "

    #@23c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23f
    move-result-object v8

    #@240
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@243
    move-result-object v8

    #@244
    const-string v9, " spn: "

    #@246
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@249
    move-result-object v8

    #@24a
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24d
    move-result-object v8

    #@24e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@251
    move-result-object v8

    #@252
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@255
    .line 4684
    :goto_255
    new-instance v1, Landroid/content/Intent;

    #@257
    const-string v8, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@259
    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@25c
    .line 4685
    .restart local v1       #intent:Landroid/content/Intent;
    invoke-virtual {v1, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@25f
    .line 4686
    const-string v8, "showSpn"

    #@261
    invoke-virtual {v1, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@264
    .line 4687
    const-string v8, "spn"

    #@266
    invoke-virtual {v1, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@269
    .line 4688
    const-string v8, "showPlmn"

    #@26b
    invoke-virtual {v1, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@26e
    .line 4689
    const-string v8, "plmn"

    #@270
    invoke-virtual {v1, v8, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@273
    .line 4690
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@275
    invoke-virtual {v8}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@278
    move-result-object v8

    #@279
    sget-object v9, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@27b
    invoke-virtual {v8, v1, v9}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@27e
    .line 4692
    const-string v8, "GSM"

    #@280
    new-instance v9, Ljava/lang/StringBuilder;

    #@282
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@285
    const-string v10, "Broadcast showPlmn: "

    #@287
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28a
    move-result-object v9

    #@28b
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@28e
    move-result-object v9

    #@28f
    const-string v10, ", plmn:"

    #@291
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@294
    move-result-object v9

    #@295
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@298
    move-result-object v9

    #@299
    const-string v10, ", showSpn: "

    #@29b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29e
    move-result-object v9

    #@29f
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a2
    move-result-object v9

    #@2a3
    const-string v10, ", spn: "

    #@2a5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a8
    move-result-object v9

    #@2a9
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ac
    move-result-object v9

    #@2ad
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b0
    move-result-object v9

    #@2b1
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b4
    .line 4695
    .end local v1           #intent:Landroid/content/Intent;
    :cond_2b4
    iput-boolean v6, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowSpn:Z

    #@2b6
    .line 4696
    iput-boolean v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowPlmn:Z

    #@2b8
    .line 4697
    iput-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curSpn:Ljava/lang/String;

    #@2ba
    .line 4698
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@2bc
    goto/16 :goto_71

    #@2be
    .end local v4           #rule:I
    .end local v5           #showPlmn:Z
    .end local v6           #showSpn:Z
    .end local v7           #spn:Ljava/lang/String;
    :cond_2be
    move v4, v10

    #@2bf
    .line 4581
    goto/16 :goto_b7

    #@2c1
    .line 4582
    .restart local v4       #rule:I
    :cond_2c1
    const-string v7, ""

    #@2c3
    goto/16 :goto_bd

    #@2c5
    .line 4640
    .restart local v7       #spn:Ljava/lang/String;
    :cond_2c5
    const-string v8, "null"

    #@2c7
    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2ca
    move-result v8

    #@2cb
    if-eqz v8, :cond_1bf

    #@2cd
    .line 4641
    new-instance v8, Ljava/lang/StringBuilder;

    #@2cf
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2d2
    const-string v11, "Not Normal Service PLMN null to service_disabled - state: "

    #@2d4
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d7
    move-result-object v8

    #@2d8
    iget-object v11, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@2da
    invoke-virtual {v11}, Landroid/telephony/ServiceState;->getState()I

    #@2dd
    move-result v11

    #@2de
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e1
    move-result-object v8

    #@2e2
    const-string v11, ", plmn="

    #@2e4
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e7
    move-result-object v8

    #@2e8
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2eb
    move-result-object v8

    #@2ec
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ef
    move-result-object v8

    #@2f0
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@2f3
    .line 4642
    const-string v8, "service_disabled"

    #@2f5
    invoke-static {v8}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@2f8
    move-result-object v2

    #@2f9
    goto/16 :goto_1bf

    #@2fb
    :cond_2fb
    move v6, v10

    #@2fc
    .line 4650
    goto/16 :goto_1d6

    #@2fe
    .restart local v6       #showSpn:Z
    :cond_2fe
    move v5, v10

    #@2ff
    .line 4653
    goto/16 :goto_1e2

    #@301
    .line 4676
    .restart local v5       #showPlmn:Z
    :cond_301
    const/4 v5, 0x1

    #@302
    .line 4677
    const/4 v6, 0x0

    #@303
    .line 4678
    new-instance v8, Ljava/lang/StringBuilder;

    #@305
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@308
    const-string v9, "Not Normal Service - showPlmn: "

    #@30a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30d
    move-result-object v8

    #@30e
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@311
    move-result-object v8

    #@312
    const-string v9, " showSpn "

    #@314
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@317
    move-result-object v8

    #@318
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@31b
    move-result-object v8

    #@31c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31f
    move-result-object v8

    #@320
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@323
    .line 4679
    new-instance v8, Ljava/lang/StringBuilder;

    #@325
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@328
    const-string v9, "Not Normal Service - plmn: "

    #@32a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32d
    move-result-object v8

    #@32e
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@331
    move-result-object v8

    #@332
    const-string v9, " spn: "

    #@334
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@337
    move-result-object v8

    #@338
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33b
    move-result-object v8

    #@33c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33f
    move-result-object v8

    #@340
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@343
    goto/16 :goto_255
.end method

.method private updateSpnDisplayLGU()V
    .registers 19

    #@0
    .prologue
    .line 4843
    const/4 v15, 0x1

    #@1
    move-object/from16 v0, p0

    #@3
    iput-boolean v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mFirstUpdateSpn:Z

    #@5
    .line 4845
    move-object/from16 v0, p0

    #@7
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@9
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@c
    move-result-object v8

    #@d
    .line 4847
    .local v8, plmn:Ljava/lang/String;
    move-object/from16 v0, p0

    #@f
    iget-object v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@11
    iget-object v15, v15, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@13
    invoke-virtual {v15}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@16
    move-result-object v15

    #@17
    if-nez v15, :cond_c6

    #@19
    .line 4849
    move-object/from16 v0, p0

    #@1b
    iget-boolean v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@1d
    if-eqz v15, :cond_85

    #@1f
    move-object/from16 v0, p0

    #@21
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@23
    invoke-interface {v15}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@26
    move-result-object v15

    #@27
    invoke-virtual {v15}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@2a
    move-result v15

    #@2b
    if-eqz v15, :cond_85

    #@2d
    .line 4850
    const-string v15, "emergency_calls_only_kt"

    #@2f
    invoke-static {v15}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@32
    move-result-object v8

    #@33
    .line 4852
    new-instance v15, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v16, "updateSpnDisplayLGU: emergency only and radio is on plmn=\'"

    #@3a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v15

    #@3e
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v15

    #@42
    const-string v16, "\'"

    #@44
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v15

    #@48
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v15

    #@4c
    move-object/from16 v0, p0

    #@4e
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@51
    .line 4859
    :cond_51
    :goto_51
    move-object/from16 v0, p0

    #@53
    iget-object v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@55
    invoke-static {v8, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@58
    move-result v15

    #@59
    if-nez v15, :cond_80

    #@5b
    .line 4860
    new-instance v3, Landroid/content/Intent;

    #@5d
    const-string v15, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@5f
    invoke-direct {v3, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@62
    .line 4861
    .local v3, intent:Landroid/content/Intent;
    const/high16 v15, 0x2000

    #@64
    invoke-virtual {v3, v15}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@67
    .line 4862
    const-string v15, "showPlmn"

    #@69
    const/16 v16, 0x1

    #@6b
    move/from16 v0, v16

    #@6d
    invoke-virtual {v3, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@70
    .line 4863
    const-string v15, "plmn"

    #@72
    invoke-virtual {v3, v15, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@75
    .line 4864
    move-object/from16 v0, p0

    #@77
    iget-object v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@79
    invoke-virtual {v15}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@7c
    move-result-object v15

    #@7d
    invoke-virtual {v15, v3}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@80
    .line 4867
    .end local v3           #intent:Landroid/content/Intent;
    :cond_80
    move-object/from16 v0, p0

    #@82
    iput-object v8, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@84
    .line 5077
    :goto_84
    return-void

    #@85
    .line 4853
    :cond_85
    move-object/from16 v0, p0

    #@87
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@89
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getState()I

    #@8c
    move-result v15

    #@8d
    const/16 v16, 0x1

    #@8f
    move/from16 v0, v16

    #@91
    if-ne v15, v0, :cond_51

    #@93
    move-object/from16 v0, p0

    #@95
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@97
    invoke-interface {v15}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@9a
    move-result-object v15

    #@9b
    invoke-virtual {v15}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@9e
    move-result v15

    #@9f
    if-eqz v15, :cond_51

    #@a1
    .line 4854
    const-string v15, "service_disabled"

    #@a3
    invoke-static {v15}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@a6
    move-result-object v8

    #@a7
    .line 4856
    new-instance v15, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v16, "updateSpnDisplayLGU: STATE_OUT_OF_SERVICE plmn ="

    #@ae
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v15

    #@b2
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v15

    #@b6
    const-string v16, "\'"

    #@b8
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v15

    #@bc
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v15

    #@c0
    move-object/from16 v0, p0

    #@c2
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@c5
    goto :goto_51

    #@c6
    .line 4871
    :cond_c6
    move-object/from16 v0, p0

    #@c8
    iget-object v2, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@ca
    .line 4872
    .local v2, iccRecords:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v2, :cond_483

    #@cc
    move-object/from16 v0, p0

    #@ce
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@d0
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@d3
    move-result-object v15

    #@d4
    invoke-virtual {v2, v15}, Lcom/android/internal/telephony/uicc/IccRecords;->getDisplayRule(Ljava/lang/String;)I

    #@d7
    move-result v11

    #@d8
    .line 4873
    .local v11, rule:I
    :goto_d8
    if-eqz v2, :cond_486

    #@da
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/IccRecords;->getServiceProviderName()Ljava/lang/String;

    #@dd
    move-result-object v14

    #@de
    .line 4893
    .local v14, spn:Ljava/lang/String;
    :goto_de
    move-object/from16 v0, p0

    #@e0
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@e2
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getState()I

    #@e5
    move-result v15

    #@e6
    if-eqz v15, :cond_ee

    #@e8
    move-object/from16 v0, p0

    #@ea
    iget v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@ec
    if-nez v15, :cond_15c

    #@ee
    .line 4900
    :cond_ee
    move-object/from16 v0, p0

    #@f0
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@f2
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getOperatorAlphaShort()Ljava/lang/String;

    #@f5
    move-result-object v15

    #@f6
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@f9
    move-result v15

    #@fa
    if-nez v15, :cond_48a

    #@fc
    .line 4901
    move-object/from16 v0, p0

    #@fe
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@100
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getOperatorAlphaShort()Ljava/lang/String;

    #@103
    move-result-object v8

    #@104
    .line 4909
    :goto_104
    const-string v15, "true"

    #@106
    const-string v16, "gsm.operator.isroaming"

    #@108
    invoke-static/range {v16 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@10b
    move-result-object v16

    #@10c
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10f
    move-result v6

    #@110
    .line 4910
    .local v6, isRoaming:Z
    move-object/from16 v0, p0

    #@112
    iget-object v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@114
    iget-object v15, v15, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@116
    invoke-virtual {v15}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@119
    move-result-object v15

    #@11a
    if-eqz v15, :cond_15c

    #@11c
    if-eqz v6, :cond_15c

    #@11e
    .line 4912
    move-object/from16 v0, p0

    #@120
    iget-object v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@122
    iget-object v15, v15, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@124
    invoke-virtual {v15}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@127
    move-result-object v15

    #@128
    check-cast v15, Lcom/android/internal/telephony/uicc/IccRecords;

    #@12a
    invoke-virtual {v15}, Lcom/android/internal/telephony/uicc/IccRecords;->getUsimIsSponIMSI()I

    #@12d
    move-result v7

    #@12e
    .line 4913
    .local v7, mImsi:I
    new-instance v15, Ljava/lang/StringBuilder;

    #@130
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@133
    const-string v16, "mImsi == "

    #@135
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v15

    #@139
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v15

    #@13d
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@140
    move-result-object v15

    #@141
    move-object/from16 v0, p0

    #@143
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@146
    .line 4914
    const/4 v15, 0x1

    #@147
    if-ne v7, v15, :cond_4a6

    #@149
    .line 4916
    new-instance v15, Ljava/lang/StringBuilder;

    #@14b
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@14e
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v15

    #@152
    const-string v16, " (Zone1)"

    #@154
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@157
    move-result-object v15

    #@158
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15b
    move-result-object v8

    #@15c
    .line 4928
    .end local v6           #isRoaming:Z
    .end local v7           #mImsi:I
    :cond_15c
    :goto_15c
    new-instance v15, Ljava/lang/StringBuilder;

    #@15e
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@161
    const-string v16, "updateSpnDisplayLGU: Global Roaming LG U+ plmn="

    #@163
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v15

    #@167
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v15

    #@16b
    const-string v16, " ,short="

    #@16d
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v15

    #@171
    move-object/from16 v0, p0

    #@173
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@175
    move-object/from16 v16, v0

    #@177
    invoke-virtual/range {v16 .. v16}, Landroid/telephony/ServiceState;->getOperatorAlphaShort()Ljava/lang/String;

    #@17a
    move-result-object v16

    #@17b
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v15

    #@17f
    const-string v16, " ,long="

    #@181
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@184
    move-result-object v15

    #@185
    move-object/from16 v0, p0

    #@187
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@189
    move-object/from16 v16, v0

    #@18b
    invoke-virtual/range {v16 .. v16}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@18e
    move-result-object v16

    #@18f
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@192
    move-result-object v15

    #@193
    const-string v16, " ,ss.getState()="

    #@195
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198
    move-result-object v15

    #@199
    move-object/from16 v0, p0

    #@19b
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@19d
    move-object/from16 v16, v0

    #@19f
    invoke-virtual/range {v16 .. v16}, Landroid/telephony/ServiceState;->getState()I

    #@1a2
    move-result v16

    #@1a3
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v15

    #@1a7
    const-string v16, " ,gprsState="

    #@1a9
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v15

    #@1ad
    move-object/from16 v0, p0

    #@1af
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@1b1
    move/from16 v16, v0

    #@1b3
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b6
    move-result-object v15

    #@1b7
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ba
    move-result-object v15

    #@1bb
    move-object/from16 v0, p0

    #@1bd
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1c0
    .line 4940
    move-object/from16 v0, p0

    #@1c2
    iget-object v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1c4
    iget-object v15, v15, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@1c6
    invoke-virtual {v15}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@1c9
    move-result-object v15

    #@1ca
    if-eqz v15, :cond_1e6

    #@1cc
    move-object/from16 v0, p0

    #@1ce
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1d0
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getState()I

    #@1d3
    move-result v15

    #@1d4
    if-nez v15, :cond_1e6

    #@1d6
    .line 4941
    move-object/from16 v0, p0

    #@1d8
    iget-object v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1da
    iget-object v15, v15, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@1dc
    invoke-virtual {v15}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@1df
    move-result-object v15

    #@1e0
    check-cast v15, Lcom/android/internal/telephony/uicc/IccRecords;

    #@1e2
    invoke-virtual {v15}, Lcom/android/internal/telephony/uicc/IccRecords;->getServiceProviderName()Ljava/lang/String;

    #@1e5
    move-result-object v14

    #@1e6
    .line 4945
    :cond_1e6
    move-object/from16 v0, p0

    #@1e8
    iget-boolean v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@1ea
    if-eqz v15, :cond_21e

    #@1ec
    move-object/from16 v0, p0

    #@1ee
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1f0
    invoke-interface {v15}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@1f3
    move-result-object v15

    #@1f4
    invoke-virtual {v15}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@1f7
    move-result v15

    #@1f8
    if-eqz v15, :cond_21e

    #@1fa
    .line 4946
    const-string v15, "emergency_calls_only_kt"

    #@1fc
    invoke-static {v15}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@1ff
    move-result-object v8

    #@200
    .line 4948
    new-instance v15, Ljava/lang/StringBuilder;

    #@202
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@205
    const-string v16, "updateSpnDisplayLGU: emergency only and radio is on plmn=\'"

    #@207
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v15

    #@20b
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20e
    move-result-object v15

    #@20f
    const-string v16, "\'"

    #@211
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@214
    move-result-object v15

    #@215
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@218
    move-result-object v15

    #@219
    move-object/from16 v0, p0

    #@21b
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@21e
    .line 4952
    :cond_21e
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@221
    move-result v15

    #@222
    if-eqz v15, :cond_230

    #@224
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@227
    move-result v15

    #@228
    if-eqz v15, :cond_230

    #@22a
    .line 4953
    const-string v15, "service_disabled"

    #@22c
    invoke-static {v15}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@22f
    move-result-object v8

    #@230
    .line 4959
    :cond_230
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@233
    move-result v15

    #@234
    if-eqz v15, :cond_23c

    #@236
    .line 4960
    const-string v15, "service_disabled"

    #@238
    invoke-static {v15}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@23b
    move-result-object v8

    #@23c
    .line 4966
    :cond_23c
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getMsgFromStatusId()Ljava/lang/String;

    #@23f
    move-result-object v9

    #@240
    .line 4967
    .local v9, rejectMsg:Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@243
    move-result v15

    #@244
    if-nez v15, :cond_247

    #@246
    .line 4968
    move-object v8, v9

    #@247
    .line 4974
    :cond_247
    const/4 v15, 0x0

    #@248
    const-string v16, "lgu_global_roaming"

    #@24a
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@24d
    move-result v15

    #@24e
    if-eqz v15, :cond_29a

    #@250
    .line 4975
    const-string v15, "true"

    #@252
    const-string v16, "ril.cdma.maintreq"

    #@254
    invoke-static/range {v16 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@257
    move-result-object v16

    #@258
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25b
    move-result v5

    #@25c
    .line 4976
    .local v5, isLockOrder:Z
    const-string v15, "true"

    #@25e
    const-string v16, "ril.cdma.authlock"

    #@260
    invoke-static/range {v16 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@263
    move-result-object v16

    #@264
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@267
    move-result v4

    #@268
    .line 4977
    .local v4, isAuthLock:Z
    const-string v15, "GSM"

    #@26a
    new-instance v16, Ljava/lang/StringBuilder;

    #@26c
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@26f
    const-string v17, "isLockOrder="

    #@271
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@274
    move-result-object v16

    #@275
    move-object/from16 v0, v16

    #@277
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@27a
    move-result-object v16

    #@27b
    const-string v17, ", isAuthLock="

    #@27d
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@280
    move-result-object v16

    #@281
    move-object/from16 v0, v16

    #@283
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@286
    move-result-object v16

    #@287
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28a
    move-result-object v16

    #@28b
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28e
    .line 4978
    if-nez v5, :cond_292

    #@290
    if-eqz v4, :cond_29a

    #@292
    .line 4979
    :cond_292
    if-eqz v5, :cond_4c6

    #@294
    .line 4980
    const-string v15, "lgt_unregister"

    #@296
    invoke-static {v15}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@299
    move-result-object v8

    #@29a
    .line 4990
    .end local v4           #isAuthLock:Z
    .end local v5           #isLockOrder:Z
    :cond_29a
    :goto_29a
    new-instance v15, Ljava/lang/StringBuilder;

    #@29c
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@29f
    const-string v16, "updateSpnDisplayLGU: TelephonyProperties.PROPERTY_LTE_REJECT_CAUSE = "

    #@2a1
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a4
    move-result-object v15

    #@2a5
    const-string v16, "gsm.lge.lte_reject_cause"

    #@2a7
    const/16 v17, 0x0

    #@2a9
    invoke-static/range {v16 .. v17}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@2ac
    move-result v16

    #@2ad
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b0
    move-result-object v15

    #@2b1
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b4
    move-result-object v15

    #@2b5
    move-object/from16 v0, p0

    #@2b7
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@2ba
    .line 4991
    const-string v15, "gsm.lge.lte_reject_cause"

    #@2bc
    const/16 v16, 0x0

    #@2be
    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@2c1
    move-result v10

    #@2c2
    .line 4992
    .local v10, rejectNum:I
    const-string v15, "gsm.lge.lte_esm_reject_cause"

    #@2c4
    const/16 v16, 0x0

    #@2c6
    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@2c9
    move-result v1

    #@2ca
    .line 4993
    .local v1, esmRejectNum:I
    if-lez v10, :cond_2e8

    #@2cc
    .line 4994
    invoke-static {v10, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->toLteRejectCauseString(II)Ljava/lang/String;

    #@2cf
    move-result-object v8

    #@2d0
    .line 4995
    new-instance v15, Ljava/lang/StringBuilder;

    #@2d2
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@2d5
    const-string v16, "updateSpnDisplayLGU: plmn by lte reject : "

    #@2d7
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2da
    move-result-object v15

    #@2db
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2de
    move-result-object v15

    #@2df
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e2
    move-result-object v15

    #@2e3
    move-object/from16 v0, p0

    #@2e5
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@2e8
    .line 5000
    :cond_2e8
    move-object/from16 v0, p0

    #@2ea
    iget-object v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2ec
    invoke-virtual {v15}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@2ef
    move-result-object v15

    #@2f0
    invoke-virtual {v15}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2f3
    move-result-object v15

    #@2f4
    const-string v16, "airplane_mode_on"

    #@2f6
    const/16 v17, 0x0

    #@2f8
    invoke-static/range {v15 .. v17}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2fb
    move-result v15

    #@2fc
    const/16 v16, 0x1

    #@2fe
    move/from16 v0, v16

    #@300
    if-ne v15, v0, :cond_311

    #@302
    .line 5001
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@305
    move-result-object v15

    #@306
    const v16, 0x104010b

    #@309
    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@30c
    move-result-object v15

    #@30d
    invoke-virtual {v15}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@310
    move-result-object v8

    #@311
    .line 5005
    :cond_311
    move-object/from16 v0, p0

    #@313
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@315
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getState()I

    #@318
    move-result v15

    #@319
    if-eqz v15, :cond_353

    #@31b
    .line 5006
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@31e
    move-result v15

    #@31f
    if-eqz v15, :cond_4d0

    #@321
    .line 5007
    new-instance v15, Ljava/lang/StringBuilder;

    #@323
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@326
    const-string v16, "Not Normal Service PLMN Empty to service_disabled - state: "

    #@328
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32b
    move-result-object v15

    #@32c
    move-object/from16 v0, p0

    #@32e
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@330
    move-object/from16 v16, v0

    #@332
    invoke-virtual/range {v16 .. v16}, Landroid/telephony/ServiceState;->getState()I

    #@335
    move-result v16

    #@336
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@339
    move-result-object v15

    #@33a
    const-string v16, ", plmn="

    #@33c
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33f
    move-result-object v15

    #@340
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@343
    move-result-object v15

    #@344
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@347
    move-result-object v15

    #@348
    move-object/from16 v0, p0

    #@34a
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@34d
    .line 5008
    const-string v15, "service_disabled"

    #@34f
    invoke-static {v15}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@352
    move-result-object v8

    #@353
    .line 5019
    :cond_353
    :goto_353
    move-object/from16 v0, p0

    #@355
    iget-boolean v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@357
    if-nez v15, :cond_50c

    #@359
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@35c
    move-result v15

    #@35d
    if-nez v15, :cond_50c

    #@35f
    and-int/lit8 v15, v11, 0x1

    #@361
    const/16 v16, 0x1

    #@363
    move/from16 v0, v16

    #@365
    if-ne v15, v0, :cond_50c

    #@367
    move-object/from16 v0, p0

    #@369
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@36b
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getState()I

    #@36e
    move-result v15

    #@36f
    if-nez v15, :cond_50c

    #@371
    const/4 v13, 0x1

    #@372
    .line 5022
    .local v13, showSpn:Z
    :goto_372
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@375
    move-result v15

    #@376
    if-nez v15, :cond_50f

    #@378
    and-int/lit8 v15, v11, 0x2

    #@37a
    const/16 v16, 0x2

    #@37c
    move/from16 v0, v16

    #@37e
    if-ne v15, v0, :cond_50f

    #@380
    const/4 v12, 0x1

    #@381
    .line 5026
    .local v12, showPlmn:Z
    :goto_381
    move-object/from16 v0, p0

    #@383
    iget-boolean v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowPlmn:Z

    #@385
    if-ne v12, v15, :cond_3a1

    #@387
    move-object/from16 v0, p0

    #@389
    iget-boolean v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowSpn:Z

    #@38b
    if-ne v13, v15, :cond_3a1

    #@38d
    move-object/from16 v0, p0

    #@38f
    iget-object v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curSpn:Ljava/lang/String;

    #@391
    invoke-static {v14, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@394
    move-result v15

    #@395
    if-eqz v15, :cond_3a1

    #@397
    move-object/from16 v0, p0

    #@399
    iget-object v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@39b
    invoke-static {v8, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@39e
    move-result v15

    #@39f
    if-nez v15, :cond_471

    #@3a1
    .line 5033
    :cond_3a1
    const/4 v15, 0x1

    #@3a2
    if-ne v13, v15, :cond_3b8

    #@3a4
    const/4 v15, 0x1

    #@3a5
    if-ne v12, v15, :cond_3b8

    #@3a7
    if-eqz v14, :cond_3af

    #@3a9
    invoke-virtual {v14, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3ac
    move-result v15

    #@3ad
    if-nez v15, :cond_3b7

    #@3af
    :cond_3af
    if-eqz v8, :cond_3b8

    #@3b1
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3b4
    move-result v15

    #@3b5
    if-nez v15, :cond_3b8

    #@3b7
    .line 5036
    :cond_3b7
    const/4 v13, 0x0

    #@3b8
    .line 5040
    :cond_3b8
    move-object/from16 v0, p0

    #@3ba
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@3bc
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getState()I

    #@3bf
    move-result v15

    #@3c0
    if-nez v15, :cond_512

    #@3c2
    .line 5041
    new-instance v15, Ljava/lang/StringBuilder;

    #@3c4
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@3c7
    const-string v16, "Normal Service - showPlmn: "

    #@3c9
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3cc
    move-result-object v15

    #@3cd
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3d0
    move-result-object v15

    #@3d1
    const-string v16, " showSpn: "

    #@3d3
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d6
    move-result-object v15

    #@3d7
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3da
    move-result-object v15

    #@3db
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3de
    move-result-object v15

    #@3df
    move-object/from16 v0, p0

    #@3e1
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@3e4
    .line 5042
    new-instance v15, Ljava/lang/StringBuilder;

    #@3e6
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@3e9
    const-string v16, "Normal Service - plmn: "

    #@3eb
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ee
    move-result-object v15

    #@3ef
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f2
    move-result-object v15

    #@3f3
    const-string v16, " spn: "

    #@3f5
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f8
    move-result-object v15

    #@3f9
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3fc
    move-result-object v15

    #@3fd
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@400
    move-result-object v15

    #@401
    move-object/from16 v0, p0

    #@403
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@406
    .line 5055
    :goto_406
    const/4 v12, 0x1

    #@407
    .line 5056
    const/4 v13, 0x0

    #@408
    .line 5061
    new-instance v3, Landroid/content/Intent;

    #@40a
    const-string v15, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@40c
    invoke-direct {v3, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@40f
    .line 5062
    .restart local v3       #intent:Landroid/content/Intent;
    const/high16 v15, 0x2000

    #@411
    invoke-virtual {v3, v15}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@414
    .line 5063
    const-string v15, "showSpn"

    #@416
    invoke-virtual {v3, v15, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@419
    .line 5064
    const-string v15, "spn"

    #@41b
    invoke-virtual {v3, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@41e
    .line 5065
    const-string v15, "showPlmn"

    #@420
    invoke-virtual {v3, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@423
    .line 5066
    const-string v15, "plmn"

    #@425
    invoke-virtual {v3, v15, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@428
    .line 5067
    move-object/from16 v0, p0

    #@42a
    iget-object v15, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@42c
    invoke-virtual {v15}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@42f
    move-result-object v15

    #@430
    invoke-virtual {v15, v3}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@433
    .line 5069
    const-string v15, "GSM"

    #@435
    new-instance v16, Ljava/lang/StringBuilder;

    #@437
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@43a
    const-string v17, "Broadcast showPlmn: "

    #@43c
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43f
    move-result-object v16

    #@440
    move-object/from16 v0, v16

    #@442
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@445
    move-result-object v16

    #@446
    const-string v17, ", plmn:"

    #@448
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44b
    move-result-object v16

    #@44c
    move-object/from16 v0, v16

    #@44e
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@451
    move-result-object v16

    #@452
    const-string v17, ", showSpn: "

    #@454
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@457
    move-result-object v16

    #@458
    move-object/from16 v0, v16

    #@45a
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@45d
    move-result-object v16

    #@45e
    const-string v17, ", spn: "

    #@460
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@463
    move-result-object v16

    #@464
    move-object/from16 v0, v16

    #@466
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@469
    move-result-object v16

    #@46a
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46d
    move-result-object v16

    #@46e
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@471
    .line 5073
    .end local v3           #intent:Landroid/content/Intent;
    :cond_471
    move-object/from16 v0, p0

    #@473
    iput-boolean v13, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowSpn:Z

    #@475
    .line 5074
    move-object/from16 v0, p0

    #@477
    iput-boolean v12, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowPlmn:Z

    #@479
    .line 5075
    move-object/from16 v0, p0

    #@47b
    iput-object v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curSpn:Ljava/lang/String;

    #@47d
    .line 5076
    move-object/from16 v0, p0

    #@47f
    iput-object v8, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@481
    goto/16 :goto_84

    #@483
    .line 4872
    .end local v1           #esmRejectNum:I
    .end local v9           #rejectMsg:Ljava/lang/String;
    .end local v10           #rejectNum:I
    .end local v11           #rule:I
    .end local v12           #showPlmn:Z
    .end local v13           #showSpn:Z
    .end local v14           #spn:Ljava/lang/String;
    :cond_483
    const/4 v11, 0x0

    #@484
    goto/16 :goto_d8

    #@486
    .line 4873
    .restart local v11       #rule:I
    :cond_486
    const-string v14, ""

    #@488
    goto/16 :goto_de

    #@48a
    .line 4902
    .restart local v14       #spn:Ljava/lang/String;
    :cond_48a
    move-object/from16 v0, p0

    #@48c
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@48e
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@491
    move-result-object v15

    #@492
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@495
    move-result v15

    #@496
    if-nez v15, :cond_4a2

    #@498
    .line 4903
    move-object/from16 v0, p0

    #@49a
    iget-object v15, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@49c
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@49f
    move-result-object v8

    #@4a0
    goto/16 :goto_104

    #@4a2
    .line 4905
    :cond_4a2
    const-string v8, "LG U+"

    #@4a4
    goto/16 :goto_104

    #@4a6
    .line 4917
    .restart local v6       #isRoaming:Z
    .restart local v7       #mImsi:I
    :cond_4a6
    if-nez v7, :cond_4b1

    #@4a8
    .line 4918
    const-string v15, "no Imsi zone"

    #@4aa
    move-object/from16 v0, p0

    #@4ac
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@4af
    goto/16 :goto_15c

    #@4b1
    .line 4920
    :cond_4b1
    new-instance v15, Ljava/lang/StringBuilder;

    #@4b3
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@4b6
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b9
    move-result-object v15

    #@4ba
    const-string v16, " (Zone2)"

    #@4bc
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4bf
    move-result-object v15

    #@4c0
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c3
    move-result-object v8

    #@4c4
    goto/16 :goto_15c

    #@4c6
    .line 4982
    .end local v6           #isRoaming:Z
    .end local v7           #mImsi:I
    .restart local v4       #isAuthLock:Z
    .restart local v5       #isLockOrder:Z
    .restart local v9       #rejectMsg:Ljava/lang/String;
    :cond_4c6
    if-eqz v4, :cond_29a

    #@4c8
    .line 4983
    const-string v15, "lgt_unauthenticated"

    #@4ca
    invoke-static {v15}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@4cd
    move-result-object v8

    #@4ce
    goto/16 :goto_29a

    #@4d0
    .line 5010
    .end local v4           #isAuthLock:Z
    .end local v5           #isLockOrder:Z
    .restart local v1       #esmRejectNum:I
    .restart local v10       #rejectNum:I
    :cond_4d0
    const-string v15, "null"

    #@4d2
    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d5
    move-result v15

    #@4d6
    if-eqz v15, :cond_353

    #@4d8
    .line 5011
    new-instance v15, Ljava/lang/StringBuilder;

    #@4da
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@4dd
    const-string v16, "Not Normal Service PLMN null to service_disabled - state: "

    #@4df
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e2
    move-result-object v15

    #@4e3
    move-object/from16 v0, p0

    #@4e5
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@4e7
    move-object/from16 v16, v0

    #@4e9
    invoke-virtual/range {v16 .. v16}, Landroid/telephony/ServiceState;->getState()I

    #@4ec
    move-result v16

    #@4ed
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f0
    move-result-object v15

    #@4f1
    const-string v16, ", plmn="

    #@4f3
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f6
    move-result-object v15

    #@4f7
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4fa
    move-result-object v15

    #@4fb
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4fe
    move-result-object v15

    #@4ff
    move-object/from16 v0, p0

    #@501
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@504
    .line 5012
    const-string v15, "service_disabled"

    #@506
    invoke-static {v15}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@509
    move-result-object v8

    #@50a
    goto/16 :goto_353

    #@50c
    .line 5019
    :cond_50c
    const/4 v13, 0x0

    #@50d
    goto/16 :goto_372

    #@50f
    .line 5022
    .restart local v13       #showSpn:Z
    :cond_50f
    const/4 v12, 0x0

    #@510
    goto/16 :goto_381

    #@512
    .line 5045
    .restart local v12       #showPlmn:Z
    :cond_512
    const/4 v12, 0x1

    #@513
    .line 5046
    const/4 v13, 0x0

    #@514
    .line 5047
    new-instance v15, Ljava/lang/StringBuilder;

    #@516
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@519
    const-string v16, "Not Normal Service - showPlmn: "

    #@51b
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51e
    move-result-object v15

    #@51f
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@522
    move-result-object v15

    #@523
    const-string v16, " showSpn "

    #@525
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@528
    move-result-object v15

    #@529
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@52c
    move-result-object v15

    #@52d
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@530
    move-result-object v15

    #@531
    move-object/from16 v0, p0

    #@533
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@536
    .line 5048
    new-instance v15, Ljava/lang/StringBuilder;

    #@538
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@53b
    const-string v16, "Not Normal Service - plmn: "

    #@53d
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@540
    move-result-object v15

    #@541
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@544
    move-result-object v15

    #@545
    const-string v16, " spn: "

    #@547
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54a
    move-result-object v15

    #@54b
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54e
    move-result-object v15

    #@54f
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@552
    move-result-object v15

    #@553
    move-object/from16 v0, p0

    #@555
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@558
    goto/16 :goto_406
.end method

.method private updateSpnDisplaySKT()V
    .registers 13

    #@0
    .prologue
    const/high16 v11, 0x2000

    #@2
    const/4 v9, 0x0

    #@3
    const/4 v8, 0x1

    #@4
    .line 4703
    iput-boolean v8, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mFirstUpdateSpn:Z

    #@6
    .line 4705
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@8
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    .line 4707
    .local v2, plmn:Ljava/lang/String;
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@e
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@10
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@13
    move-result-object v7

    #@14
    if-nez v7, :cond_a9

    #@16
    .line 4709
    iget-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@18
    if-eqz v7, :cond_72

    #@1a
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1c
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {v7}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@23
    move-result v7

    #@24
    if-eqz v7, :cond_72

    #@26
    .line 4710
    const-string v7, "emergency_calls_only_skt"

    #@28
    invoke-static {v7}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    .line 4712
    new-instance v7, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v9, "updateSpnDisplaySKT: emergency only and radio is on plmn=\'"

    #@33
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v7

    #@37
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    const-string v9, "\'"

    #@3d
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v7

    #@41
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v7

    #@45
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@48
    .line 4719
    :cond_48
    :goto_48
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@4a
    invoke-static {v2, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@4d
    move-result v7

    #@4e
    if-nez v7, :cond_6f

    #@50
    .line 4720
    new-instance v1, Landroid/content/Intent;

    #@52
    const-string v7, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@54
    invoke-direct {v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@57
    .line 4721
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@5a
    .line 4722
    const-string v7, "showPlmn"

    #@5c
    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@5f
    .line 4723
    const-string v7, "plmn"

    #@61
    invoke-virtual {v1, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@64
    .line 4724
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@66
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@69
    move-result-object v7

    #@6a
    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@6c
    invoke-virtual {v7, v1, v8}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@6f
    .line 4727
    .end local v1           #intent:Landroid/content/Intent;
    :cond_6f
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@71
    .line 4839
    :goto_71
    return-void

    #@72
    .line 4713
    :cond_72
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@74
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getState()I

    #@77
    move-result v7

    #@78
    if-ne v7, v8, :cond_48

    #@7a
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@7c
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@7f
    move-result-object v7

    #@80
    invoke-virtual {v7}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@83
    move-result v7

    #@84
    if-eqz v7, :cond_48

    #@86
    .line 4714
    const-string v7, "service_disabled"

    #@88
    invoke-static {v7}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@8b
    move-result-object v2

    #@8c
    .line 4716
    new-instance v7, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v9, "updateSpnDisplaySKT: STATE_OUT_OF_SERVICE plmn ="

    #@93
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v7

    #@97
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v7

    #@9b
    const-string v9, "\'"

    #@9d
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v7

    #@a1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v7

    #@a5
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@a8
    goto :goto_48

    #@a9
    .line 4732
    :cond_a9
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@ab
    .line 4733
    .local v0, iccRecords:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_295

    #@ad
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@af
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@b2
    move-result-object v7

    #@b3
    invoke-virtual {v0, v7}, Lcom/android/internal/telephony/uicc/IccRecords;->getDisplayRule(Ljava/lang/String;)I

    #@b6
    move-result v3

    #@b7
    .line 4734
    .local v3, rule:I
    :goto_b7
    if-eqz v0, :cond_298

    #@b9
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getServiceProviderName()Ljava/lang/String;

    #@bc
    move-result-object v6

    #@bd
    .line 4742
    .local v6, spn:Ljava/lang/String;
    :goto_bd
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@bf
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getOperatorAlphaShort()Ljava/lang/String;

    #@c2
    move-result-object v2

    #@c3
    .line 4744
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c6
    move-result v7

    #@c7
    if-eqz v7, :cond_d7

    #@c9
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@cb
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@ce
    move-result-object v7

    #@cf
    if-eqz v7, :cond_d7

    #@d1
    .line 4745
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@d3
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@d6
    move-result-object v2

    #@d7
    .line 4751
    :cond_d7
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@d9
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@db
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@de
    move-result-object v7

    #@df
    if-eqz v7, :cond_f7

    #@e1
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@e3
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getState()I

    #@e6
    move-result v7

    #@e7
    if-nez v7, :cond_f7

    #@e9
    .line 4752
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@eb
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@ed
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@f0
    move-result-object v7

    #@f1
    check-cast v7, Lcom/android/internal/telephony/uicc/IccRecords;

    #@f3
    invoke-virtual {v7}, Lcom/android/internal/telephony/uicc/IccRecords;->getServiceProviderName()Ljava/lang/String;

    #@f6
    move-result-object v6

    #@f7
    .line 4756
    :cond_f7
    iget-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@f9
    if-eqz v7, :cond_129

    #@fb
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@fd
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@100
    move-result-object v7

    #@101
    invoke-virtual {v7}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@104
    move-result v7

    #@105
    if-eqz v7, :cond_129

    #@107
    .line 4757
    const-string v7, "emergency_calls_only_skt"

    #@109
    invoke-static {v7}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@10c
    move-result-object v2

    #@10d
    .line 4759
    new-instance v7, Ljava/lang/StringBuilder;

    #@10f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@112
    const-string v10, "updateSpnDisplaySKT: emergency only and radio is on plmn=\'"

    #@114
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v7

    #@118
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v7

    #@11c
    const-string v10, "\'"

    #@11e
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v7

    #@122
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@125
    move-result-object v7

    #@126
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@129
    .line 4763
    :cond_129
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@12c
    move-result v7

    #@12d
    if-eqz v7, :cond_13b

    #@12f
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@132
    move-result v7

    #@133
    if-eqz v7, :cond_13b

    #@135
    .line 4764
    const-string v7, "service_disabled"

    #@137
    invoke-static {v7}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@13a
    move-result-object v2

    #@13b
    .line 4770
    :cond_13b
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@13d
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@140
    move-result-object v7

    #@141
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@144
    move-result-object v7

    #@145
    const-string v10, "airplane_mode_on"

    #@147
    invoke-static {v7, v10, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@14a
    move-result v7

    #@14b
    if-ne v7, v8, :cond_15c

    #@14d
    .line 4771
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@150
    move-result-object v7

    #@151
    const v10, 0x104010b

    #@154
    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@157
    move-result-object v7

    #@158
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@15b
    move-result-object v2

    #@15c
    .line 4775
    :cond_15c
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@15e
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getState()I

    #@161
    move-result v7

    #@162
    if-eqz v7, :cond_196

    #@164
    .line 4776
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@167
    move-result v7

    #@168
    if-eqz v7, :cond_29c

    #@16a
    .line 4777
    new-instance v7, Ljava/lang/StringBuilder;

    #@16c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@16f
    const-string v10, "Not Normal Service PLMN Empty to service_disabled - state: "

    #@171
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v7

    #@175
    iget-object v10, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@177
    invoke-virtual {v10}, Landroid/telephony/ServiceState;->getState()I

    #@17a
    move-result v10

    #@17b
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v7

    #@17f
    const-string v10, ", plmn="

    #@181
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@184
    move-result-object v7

    #@185
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v7

    #@189
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18c
    move-result-object v7

    #@18d
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@190
    .line 4778
    const-string v7, "service_disabled"

    #@192
    invoke-static {v7}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@195
    move-result-object v2

    #@196
    .line 4790
    :cond_196
    :goto_196
    iget-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@198
    if-nez v7, :cond_2d2

    #@19a
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@19d
    move-result v7

    #@19e
    if-nez v7, :cond_2d2

    #@1a0
    and-int/lit8 v7, v3, 0x1

    #@1a2
    if-ne v7, v8, :cond_2d2

    #@1a4
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1a6
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getState()I

    #@1a9
    move-result v7

    #@1aa
    if-nez v7, :cond_2d2

    #@1ac
    move v5, v8

    #@1ad
    .line 4793
    .local v5, showSpn:Z
    :goto_1ad
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1b0
    move-result v7

    #@1b1
    if-nez v7, :cond_2d5

    #@1b3
    and-int/lit8 v7, v3, 0x2

    #@1b5
    const/4 v10, 0x2

    #@1b6
    if-ne v7, v10, :cond_2d5

    #@1b8
    move v4, v8

    #@1b9
    .line 4798
    .local v4, showPlmn:Z
    :goto_1b9
    iget-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowPlmn:Z

    #@1bb
    if-ne v4, v7, :cond_1d1

    #@1bd
    iget-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowSpn:Z

    #@1bf
    if-ne v5, v7, :cond_1d1

    #@1c1
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curSpn:Ljava/lang/String;

    #@1c3
    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1c6
    move-result v7

    #@1c7
    if-eqz v7, :cond_1d1

    #@1c9
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@1cb
    invoke-static {v2, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1ce
    move-result v7

    #@1cf
    if-nez v7, :cond_28b

    #@1d1
    .line 4805
    :cond_1d1
    if-ne v5, v8, :cond_1e4

    #@1d3
    if-ne v4, v8, :cond_1e4

    #@1d5
    if-eqz v6, :cond_1dd

    #@1d7
    invoke-virtual {v6, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1da
    move-result v7

    #@1db
    if-nez v7, :cond_1e3

    #@1dd
    :cond_1dd
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1e0
    move-result v7

    #@1e1
    if-nez v7, :cond_1e4

    #@1e3
    .line 4808
    :cond_1e3
    const/4 v5, 0x0

    #@1e4
    .line 4812
    :cond_1e4
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1e6
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getState()I

    #@1e9
    move-result v7

    #@1ea
    if-nez v7, :cond_2d8

    #@1ec
    .line 4813
    new-instance v7, Ljava/lang/StringBuilder;

    #@1ee
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1f1
    const-string v8, "Normal Service - showPlmn: "

    #@1f3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v7

    #@1f7
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v7

    #@1fb
    const-string v8, " showSpn: "

    #@1fd
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@200
    move-result-object v7

    #@201
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@204
    move-result-object v7

    #@205
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@208
    move-result-object v7

    #@209
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@20c
    .line 4814
    new-instance v7, Ljava/lang/StringBuilder;

    #@20e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@211
    const-string v8, "Normal Service - plmn: "

    #@213
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@216
    move-result-object v7

    #@217
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v7

    #@21b
    const-string v8, " spn: "

    #@21d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v7

    #@221
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@224
    move-result-object v7

    #@225
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@228
    move-result-object v7

    #@229
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@22c
    .line 4824
    :goto_22c
    new-instance v1, Landroid/content/Intent;

    #@22e
    const-string v7, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@230
    invoke-direct {v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@233
    .line 4825
    .restart local v1       #intent:Landroid/content/Intent;
    invoke-virtual {v1, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@236
    .line 4826
    const-string v7, "showSpn"

    #@238
    invoke-virtual {v1, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@23b
    .line 4827
    const-string v7, "spn"

    #@23d
    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@240
    .line 4828
    const-string v7, "showPlmn"

    #@242
    invoke-virtual {v1, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@245
    .line 4829
    const-string v7, "plmn"

    #@247
    invoke-virtual {v1, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@24a
    .line 4830
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@24c
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@24f
    move-result-object v7

    #@250
    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@252
    invoke-virtual {v7, v1, v8}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@255
    .line 4832
    const-string v7, "GSM"

    #@257
    new-instance v8, Ljava/lang/StringBuilder;

    #@259
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@25c
    const-string v9, "Broadcast showPlmn: "

    #@25e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@261
    move-result-object v8

    #@262
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@265
    move-result-object v8

    #@266
    const-string v9, ", plmn:"

    #@268
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26b
    move-result-object v8

    #@26c
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26f
    move-result-object v8

    #@270
    const-string v9, ", showSpn: "

    #@272
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@275
    move-result-object v8

    #@276
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@279
    move-result-object v8

    #@27a
    const-string v9, ", spn: "

    #@27c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27f
    move-result-object v8

    #@280
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@283
    move-result-object v8

    #@284
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@287
    move-result-object v8

    #@288
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28b
    .line 4835
    .end local v1           #intent:Landroid/content/Intent;
    :cond_28b
    iput-boolean v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowSpn:Z

    #@28d
    .line 4836
    iput-boolean v4, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowPlmn:Z

    #@28f
    .line 4837
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curSpn:Ljava/lang/String;

    #@291
    .line 4838
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@293
    goto/16 :goto_71

    #@295
    .end local v3           #rule:I
    .end local v4           #showPlmn:Z
    .end local v5           #showSpn:Z
    .end local v6           #spn:Ljava/lang/String;
    :cond_295
    move v3, v9

    #@296
    .line 4733
    goto/16 :goto_b7

    #@298
    .line 4734
    .restart local v3       #rule:I
    :cond_298
    const-string v6, ""

    #@29a
    goto/16 :goto_bd

    #@29c
    .line 4780
    .restart local v6       #spn:Ljava/lang/String;
    :cond_29c
    const-string v7, "null"

    #@29e
    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a1
    move-result v7

    #@2a2
    if-eqz v7, :cond_196

    #@2a4
    .line 4781
    new-instance v7, Ljava/lang/StringBuilder;

    #@2a6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2a9
    const-string v10, "Not Normal Service PLMN null to service_disabled - state: "

    #@2ab
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ae
    move-result-object v7

    #@2af
    iget-object v10, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@2b1
    invoke-virtual {v10}, Landroid/telephony/ServiceState;->getState()I

    #@2b4
    move-result v10

    #@2b5
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b8
    move-result-object v7

    #@2b9
    const-string v10, ", plmn="

    #@2bb
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2be
    move-result-object v7

    #@2bf
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c2
    move-result-object v7

    #@2c3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c6
    move-result-object v7

    #@2c7
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@2ca
    .line 4782
    const-string v7, "service_disabled"

    #@2cc
    invoke-static {v7}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@2cf
    move-result-object v2

    #@2d0
    goto/16 :goto_196

    #@2d2
    :cond_2d2
    move v5, v9

    #@2d3
    .line 4790
    goto/16 :goto_1ad

    #@2d5
    .restart local v5       #showSpn:Z
    :cond_2d5
    move v4, v9

    #@2d6
    .line 4793
    goto/16 :goto_1b9

    #@2d8
    .line 4817
    .restart local v4       #showPlmn:Z
    :cond_2d8
    const/4 v4, 0x1

    #@2d9
    .line 4818
    const/4 v5, 0x0

    #@2da
    .line 4819
    new-instance v7, Ljava/lang/StringBuilder;

    #@2dc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2df
    const-string v8, "Not Normal Service - showPlmn: "

    #@2e1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e4
    move-result-object v7

    #@2e5
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2e8
    move-result-object v7

    #@2e9
    const-string v8, " showSpn "

    #@2eb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ee
    move-result-object v7

    #@2ef
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2f2
    move-result-object v7

    #@2f3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f6
    move-result-object v7

    #@2f7
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@2fa
    .line 4820
    new-instance v7, Ljava/lang/StringBuilder;

    #@2fc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2ff
    const-string v8, "Not Normal Service - plmn: "

    #@301
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@304
    move-result-object v7

    #@305
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@308
    move-result-object v7

    #@309
    const-string v8, " spn: "

    #@30b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30e
    move-result-object v7

    #@30f
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@312
    move-result-object v7

    #@313
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@316
    move-result-object v7

    #@317
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@31a
    goto/16 :goto_22c
.end method


# virtual methods
.method public bManualSelectionAvailable()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 4513
    const/4 v1, 0x0

    #@2
    const-string v2, "KR_REJECT_CAUSE"

    #@4
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_1e

    #@a
    const-string v1, "KR"

    #@c
    const-string v2, "KT"

    #@e
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_1e

    #@14
    .line 4515
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@16
    if-eqz v1, :cond_1e

    #@18
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@1a
    invoke-interface {v0}, Lcom/android/internal/telephony/gsm/RejectCause;->bManualSelectionAvailable()Z

    #@1d
    move-result v0

    #@1e
    .line 4519
    :cond_1e
    return v0
.end method

.method public dispose()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 905
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->checkCorrectThread()V

    #@4
    .line 906
    const-string v0, "ServiceStateTracker dispose"

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@9
    .line 909
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@b
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForAvailable(Landroid/os/Handler;)V

    #@e
    .line 910
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForRadioStateChanged(Landroid/os/Handler;)V

    #@13
    .line 911
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@15
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForVoiceNetworkStateChanged(Landroid/os/Handler;)V

    #@18
    .line 912
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1a
    if-eqz v0, :cond_21

    #@1c
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1e
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForReady(Landroid/os/Handler;)V

    #@21
    .line 913
    :cond_21
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@23
    if-eqz v0, :cond_2a

    #@25
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@27
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@2a
    .line 914
    :cond_2a
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2c
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnRestrictedStateChanged(Landroid/os/Handler;)V

    #@2f
    .line 916
    const-string v0, "VZW"

    #@31
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_42

    #@37
    .line 917
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmApnObserver:Landroid/database/ContentObserver;

    #@39
    if-eqz v0, :cond_42

    #@3b
    .line 918
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@3d
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmApnObserver:Landroid/database/ContentObserver;

    #@3f
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@42
    .line 922
    :cond_42
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@44
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnNITZTime(Landroid/os/Handler;)V

    #@47
    .line 923
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@49
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mAutoTimeObserver:Landroid/database/ContentObserver;

    #@4b
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@4e
    .line 924
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@50
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mAutoTimeZoneObserver:Landroid/database/ContentObserver;

    #@52
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@55
    .line 925
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@57
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@5a
    move-result-object v0

    #@5b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@5d
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@60
    .line 927
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@62
    if-eqz v0, :cond_69

    #@64
    .line 928
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@66
    invoke-virtual {v0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->unsetServiceStateTracker()V

    #@69
    .line 933
    :cond_69
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@6b
    if-eqz v0, :cond_86

    #@6d
    .line 934
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@6f
    invoke-static {v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setSavedNeedFixZone(Z)V

    #@72
    .line 935
    iget v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@74
    invoke-static {v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setSavedZoneOffset(I)V

    #@77
    .line 936
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@79
    invoke-static {v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setSavedZoneDst(Z)V

    #@7c
    .line 937
    iget-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneTime:J

    #@7e
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setSavedZoneTime(J)V

    #@81
    .line 938
    const-string v0, "TimeZone correction is needed after Phone Switching!"

    #@83
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@86
    .line 943
    :cond_86
    const-string v0, "USIM_PERSONAL_LOCK"

    #@88
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8b
    move-result v0

    #@8c
    if-eqz v0, :cond_a4

    #@8e
    .line 944
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@90
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@93
    move-result-object v0

    #@94
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mUsimIntentReceiver:Landroid/content/BroadcastReceiver;

    #@96
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@99
    .line 945
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@9b
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@9e
    move-result-object v0

    #@9f
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@a1
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@a4
    .line 949
    :cond_a4
    const-string v0, "KR_REJECT_CAUSE"

    #@a6
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a9
    move-result v0

    #@aa
    if-eqz v0, :cond_b9

    #@ac
    .line 950
    invoke-static {}, Lcom/android/internal/telephony/gsm/RejectCauseFactory;->dispose()V

    #@af
    .line 951
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@b1
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForWcdmaRejectReceived(Landroid/os/Handler;)V

    #@b4
    .line 952
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@b6
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForWcdmaAcceptReceived(Landroid/os/Handler;)V

    #@b9
    .line 957
    :cond_b9
    const-string v0, "KR"

    #@bb
    const-string v1, "LGU"

    #@bd
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@c0
    move-result v0

    #@c1
    if-nez v0, :cond_cd

    #@c3
    const-string v0, "KR"

    #@c5
    const-string v1, "SKT"

    #@c7
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@ca
    move-result v0

    #@cb
    if-eqz v0, :cond_d2

    #@cd
    .line 958
    :cond_cd
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLgeRegStateNotification:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@cf
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->dispose()V

    #@d2
    .line 963
    :cond_d2
    const-string v0, "VZW"

    #@d4
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@d7
    move-result v0

    #@d8
    if-eqz v0, :cond_e4

    #@da
    .line 964
    const/16 v0, 0x3ea

    #@dc
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@df
    .line 965
    const/16 v0, 0x3ec

    #@e1
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@e4
    .line 969
    :cond_e4
    invoke-super {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->dispose()V

    #@e7
    .line 970
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 4396
    const-string v0, "GsmServiceStateTracker extends:"

    #@2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 4397
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/telephony/ServiceStateTracker;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@8
    .line 4398
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, " phone="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20
    .line 4399
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, " cellLoc="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 4400
    new-instance v0, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v1, " newCellLoc="

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@50
    .line 4401
    new-instance v0, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v1, " mPreferredNetworkType="

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mPreferredNetworkType:I

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@68
    .line 4402
    new-instance v0, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v1, " gprsState="

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v0

    #@7d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@80
    .line 4403
    new-instance v0, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v1, " newGPRSState="

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@98
    .line 4404
    new-instance v0, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v1, " mMaxDataCalls="

    #@9f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v0

    #@a3
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mMaxDataCalls:I

    #@a5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v0

    #@a9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v0

    #@ad
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b0
    .line 4405
    new-instance v0, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v1, " mNewMaxDataCalls="

    #@b7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v0

    #@bb
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewMaxDataCalls:I

    #@bd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v0

    #@c1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v0

    #@c5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c8
    .line 4406
    new-instance v0, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v1, " mReasonDataDenied="

    #@cf
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v0

    #@d3
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mReasonDataDenied:I

    #@d5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v0

    #@d9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v0

    #@dd
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e0
    .line 4407
    new-instance v0, Ljava/lang/StringBuilder;

    #@e2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e5
    const-string v1, " mNewReasonDataDenied="

    #@e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v0

    #@eb
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewReasonDataDenied:I

    #@ed
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v0

    #@f1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v0

    #@f5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f8
    .line 4408
    new-instance v0, Ljava/lang/StringBuilder;

    #@fa
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@fd
    const-string v1, " mGsmRoaming="

    #@ff
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v0

    #@103
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmRoaming:Z

    #@105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@108
    move-result-object v0

    #@109
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v0

    #@10d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@110
    .line 4409
    new-instance v0, Ljava/lang/StringBuilder;

    #@112
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string v1, " mDataRoaming="

    #@117
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v0

    #@11b
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mDataRoaming:Z

    #@11d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@120
    move-result-object v0

    #@121
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v0

    #@125
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@128
    .line 4410
    new-instance v0, Ljava/lang/StringBuilder;

    #@12a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12d
    const-string v1, " mEmergencyOnly="

    #@12f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v0

    #@133
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@135
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@138
    move-result-object v0

    #@139
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13c
    move-result-object v0

    #@13d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@140
    .line 4411
    new-instance v0, Ljava/lang/StringBuilder;

    #@142
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@145
    const-string v1, " mNeedFixZoneAfterNitz="

    #@147
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v0

    #@14b
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNeedFixZoneAfterNitz:Z

    #@14d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@150
    move-result-object v0

    #@151
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@154
    move-result-object v0

    #@155
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@158
    .line 4412
    new-instance v0, Ljava/lang/StringBuilder;

    #@15a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15d
    const-string v1, " mZoneOffset="

    #@15f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v0

    #@163
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneOffset:I

    #@165
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@168
    move-result-object v0

    #@169
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16c
    move-result-object v0

    #@16d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@170
    .line 4413
    new-instance v0, Ljava/lang/StringBuilder;

    #@172
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@175
    const-string v1, " mZoneDst="

    #@177
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v0

    #@17b
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneDst:Z

    #@17d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@180
    move-result-object v0

    #@181
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@184
    move-result-object v0

    #@185
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@188
    .line 4414
    new-instance v0, Ljava/lang/StringBuilder;

    #@18a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@18d
    const-string v1, " mZoneTime="

    #@18f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@192
    move-result-object v0

    #@193
    iget-wide v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mZoneTime:J

    #@195
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@198
    move-result-object v0

    #@199
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19c
    move-result-object v0

    #@19d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a0
    .line 4415
    new-instance v0, Ljava/lang/StringBuilder;

    #@1a2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1a5
    const-string v1, " mGotCountryCode="

    #@1a7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v0

    #@1ab
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGotCountryCode:Z

    #@1ad
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v0

    #@1b1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b4
    move-result-object v0

    #@1b5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b8
    .line 4416
    new-instance v0, Ljava/lang/StringBuilder;

    #@1ba
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1bd
    const-string v1, " mNitzUpdatedTime="

    #@1bf
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v0

    #@1c3
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNitzUpdatedTime:Z

    #@1c5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v0

    #@1c9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cc
    move-result-object v0

    #@1cd
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d0
    .line 4417
    new-instance v0, Ljava/lang/StringBuilder;

    #@1d2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1d5
    const-string v1, " mSavedTimeZone="

    #@1d7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v0

    #@1db
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTimeZone:Ljava/lang/String;

    #@1dd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e0
    move-result-object v0

    #@1e1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e4
    move-result-object v0

    #@1e5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1e8
    .line 4418
    new-instance v0, Ljava/lang/StringBuilder;

    #@1ea
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1ed
    const-string v1, " mSavedTime="

    #@1ef
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v0

    #@1f3
    iget-wide v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedTime:J

    #@1f5
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v0

    #@1f9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fc
    move-result-object v0

    #@1fd
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@200
    .line 4419
    new-instance v0, Ljava/lang/StringBuilder;

    #@202
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@205
    const-string v1, " mSavedAtTime="

    #@207
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v0

    #@20b
    iget-wide v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSavedAtTime:J

    #@20d
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@210
    move-result-object v0

    #@211
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@214
    move-result-object v0

    #@215
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@218
    .line 4420
    new-instance v0, Ljava/lang/StringBuilder;

    #@21a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@21d
    const-string v1, " mStartedGprsRegCheck="

    #@21f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@222
    move-result-object v0

    #@223
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStartedGprsRegCheck:Z

    #@225
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@228
    move-result-object v0

    #@229
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22c
    move-result-object v0

    #@22d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@230
    .line 4421
    new-instance v0, Ljava/lang/StringBuilder;

    #@232
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@235
    const-string v1, " mReportedGprsNoReg="

    #@237
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23a
    move-result-object v0

    #@23b
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mReportedGprsNoReg:Z

    #@23d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@240
    move-result-object v0

    #@241
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@244
    move-result-object v0

    #@245
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@248
    .line 4422
    new-instance v0, Ljava/lang/StringBuilder;

    #@24a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@24d
    const-string v1, " mNotification="

    #@24f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@252
    move-result-object v0

    #@253
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNotification:Landroid/app/Notification;

    #@255
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@258
    move-result-object v0

    #@259
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25c
    move-result-object v0

    #@25d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@260
    .line 4423
    new-instance v0, Ljava/lang/StringBuilder;

    #@262
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@265
    const-string v1, " mWakeLock="

    #@267
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26a
    move-result-object v0

    #@26b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@26d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@270
    move-result-object v0

    #@271
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@274
    move-result-object v0

    #@275
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@278
    .line 4424
    new-instance v0, Ljava/lang/StringBuilder;

    #@27a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@27d
    const-string v1, " curSpn="

    #@27f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@282
    move-result-object v0

    #@283
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curSpn:Ljava/lang/String;

    #@285
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@288
    move-result-object v0

    #@289
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28c
    move-result-object v0

    #@28d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@290
    .line 4425
    new-instance v0, Ljava/lang/StringBuilder;

    #@292
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@295
    const-string v1, " curShowSpn="

    #@297
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29a
    move-result-object v0

    #@29b
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowSpn:Z

    #@29d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a0
    move-result-object v0

    #@2a1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a4
    move-result-object v0

    #@2a5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2a8
    .line 4426
    new-instance v0, Ljava/lang/StringBuilder;

    #@2aa
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2ad
    const-string v1, " curPlmn="

    #@2af
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b2
    move-result-object v0

    #@2b3
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@2b5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b8
    move-result-object v0

    #@2b9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2bc
    move-result-object v0

    #@2bd
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2c0
    .line 4427
    new-instance v0, Ljava/lang/StringBuilder;

    #@2c2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2c5
    const-string v1, " curShowPlmn="

    #@2c7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ca
    move-result-object v0

    #@2cb
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowPlmn:Z

    #@2cd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2d0
    move-result-object v0

    #@2d1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d4
    move-result-object v0

    #@2d5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2d8
    .line 4428
    return-void
.end method

.method protected faCHGReboot()V
    .registers 9

    #@0
    .prologue
    .line 5404
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@5
    move-result-object v5

    #@6
    const-string v6, "power"

    #@8
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/os/PowerManager;

    #@e
    .line 5406
    .local v0, mPm:Landroid/os/PowerManager;
    const-string v5, "GSM"

    #@10
    const-string v6, "EVENT_SKT_FA_CHG_DONE faCHGReboot++"

    #@12
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 5408
    const v5, 0x1000001a

    #@18
    const-string v6, "GSMSST-FA"

    #@1a
    invoke-virtual {v0, v5, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@1d
    move-result-object v1

    #@1e
    .line 5410
    .local v1, mWakeLockforFA:Landroid/os/PowerManager$WakeLock;
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@21
    .line 5412
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@23
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@26
    move-result-object v5

    #@27
    const-string v6, "skt_fa_changed"

    #@29
    invoke-static {v6}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v6

    #@2d
    const/4 v7, 0x0

    #@2e
    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@31
    move-result-object v3

    #@32
    .line 5414
    .local v3, rebootToast:Landroid/widget/Toast;
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    #@35
    .line 5416
    const-string v5, "GSM"

    #@37
    const-string v6, "EVENT_SKT_FA_CHG_DONE skt_fa_changed display"

    #@39
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 5418
    new-instance v2, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$9;

    #@3e
    invoke-direct {v2, p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker$9;-><init>(Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;)V

    #@41
    .line 5432
    .local v2, myTask:Ljava/util/TimerTask;
    new-instance v4, Ljava/util/Timer;

    #@43
    invoke-direct {v4}, Ljava/util/Timer;-><init>()V

    #@46
    .line 5433
    .local v4, timer:Ljava/util/Timer;
    const-wide/16 v5, 0xbb8

    #@48
    invoke-virtual {v4, v2, v5, v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    #@4b
    .line 5434
    return-void
.end method

.method protected finalize()V
    .registers 2

    #@0
    .prologue
    .line 973
    const-string v0, "finalize"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@5
    .line 974
    return-void
.end method

.method public getAllCellInfo()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 5366
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 5367
    .local v0, arrayList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/CellInfo;>;"
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@7
    monitor-enter v2

    #@8
    .line 5368
    :try_start_8
    iget v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@a
    const/16 v3, 0xe

    #@c
    if-ne v1, v3, :cond_2b

    #@e
    .line 5369
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@10
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@13
    .line 5373
    :goto_13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_8 .. :try_end_14} :catchall_31

    #@14
    .line 5374
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v2, "getAllCellInfo: arrayList="

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@2a
    .line 5375
    return-object v0

    #@2b
    .line 5371
    :cond_2b
    :try_start_2b
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@2d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@30
    goto :goto_13

    #@31
    .line 5373
    :catchall_31
    move-exception v1

    #@32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_2b .. :try_end_33} :catchall_31

    #@33
    throw v1
.end method

.method public getCurrentDataConnectionState()I
    .registers 2

    #@0
    .prologue
    .line 3851
    iget v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@2
    return v0
.end method

.method getCurrentGprsState()I
    .registers 2

    #@0
    .prologue
    .line 3847
    iget v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@2
    return v0
.end method

.method protected getPhone()Lcom/android/internal/telephony/Phone;
    .registers 2

    #@0
    .prologue
    .line 978
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2
    return-object v0
.end method

.method protected getSystemProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "property"
    .parameter "defValue"

    #@0
    .prologue
    .line 3515
    invoke-static {p1, p2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected getUiccCardApplication()Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .registers 3

    #@0
    .prologue
    .line 4285
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 30
    .parameter "msg"

    #@0
    .prologue
    .line 987
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4
    move-object/from16 v24, v0

    #@6
    move-object/from16 v0, v24

    #@8
    iget-boolean v0, v0, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@a
    move/from16 v24, v0

    #@c
    if-nez v24, :cond_7e

    #@e
    .line 988
    const-string v24, "GSM"

    #@10
    new-instance v25, Ljava/lang/StringBuilder;

    #@12
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v26, "Received message "

    #@17
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v25

    #@1b
    move-object/from16 v0, v25

    #@1d
    move-object/from16 v1, p1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v25

    #@23
    const-string v26, "["

    #@25
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v25

    #@29
    move-object/from16 v0, p1

    #@2b
    iget v0, v0, Landroid/os/Message;->what:I

    #@2d
    move/from16 v26, v0

    #@2f
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v25

    #@33
    const-string v26, "] while being destroyed. Ignoring."

    #@35
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v25

    #@39
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v25

    #@3d
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 991
    if-eqz p1, :cond_7d

    #@42
    move-object/from16 v0, p1

    #@44
    iget v0, v0, Landroid/os/Message;->what:I

    #@46
    move/from16 v24, v0

    #@48
    const/16 v25, 0xb

    #@4a
    move/from16 v0, v24

    #@4c
    move/from16 v1, v25

    #@4e
    if-ne v0, v1, :cond_7d

    #@50
    .line 992
    const-string v24, "NITZ received while disposing CDMAPhone!!"

    #@52
    move-object/from16 v0, p0

    #@54
    move-object/from16 v1, v24

    #@56
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@59
    .line 994
    sget-object v24, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@5b
    if-nez v24, :cond_63

    #@5d
    .line 995
    invoke-static {}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->getDefault()Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@60
    move-result-object v24

    #@61
    sput-object v24, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@63
    .line 998
    :cond_63
    sget-object v24, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@65
    if-eqz v24, :cond_7d

    #@67
    .line 999
    const-string v24, "Save NITZ info. to restore it after phone-switching"

    #@69
    move-object/from16 v0, p0

    #@6b
    move-object/from16 v1, v24

    #@6d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@70
    .line 1000
    sget-object v24, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@72
    move-object/from16 v0, p1

    #@74
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@76
    move-object/from16 v24, v0

    #@78
    check-cast v24, Landroid/os/AsyncResult;

    #@7a
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->setLostNitzInfo(Landroid/os/AsyncResult;)V

    #@7d
    .line 1382
    :cond_7d
    :goto_7d
    :sswitch_7d
    return-void

    #@7e
    .line 1006
    :cond_7e
    move-object/from16 v0, p1

    #@80
    iget v0, v0, Landroid/os/Message;->what:I

    #@82
    move/from16 v24, v0

    #@84
    sparse-switch v24, :sswitch_data_7ac

    #@87
    .line 1379
    invoke-super/range {p0 .. p1}, Lcom/android/internal/telephony/ServiceStateTracker;->handleMessage(Landroid/os/Message;)V

    #@8a
    goto :goto_7d

    #@8b
    .line 1016
    :sswitch_8b
    move-object/from16 v0, p0

    #@8d
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@8f
    move-object/from16 v24, v0

    #@91
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@94
    move-result-object v24

    #@95
    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@98
    move-result-object v24

    #@99
    const v25, 0x1110035

    #@9c
    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@9f
    move-result v20

    #@a0
    .line 1019
    .local v20, skipRestoringSelection:Z
    if-nez v20, :cond_ad

    #@a2
    .line 1021
    move-object/from16 v0, p0

    #@a4
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@a6
    move-object/from16 v24, v0

    #@a8
    const/16 v25, 0x0

    #@aa
    invoke-virtual/range {v24 .. v25}, Lcom/android/internal/telephony/gsm/GSMPhone;->restoreSavedNetworkSelection(Landroid/os/Message;)V

    #@ad
    .line 1023
    :cond_ad
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->pollState()V

    #@b0
    .line 1025
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->queueNextSignalStrengthPoll()V

    #@b3
    goto :goto_7d

    #@b4
    .line 1031
    .end local v20           #skipRestoringSelection:Z
    :sswitch_b4
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setPowerStateToDesired()V

    #@b7
    .line 1032
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->pollState()V

    #@ba
    .line 1034
    const-string v24, "VZW"

    #@bc
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@bf
    move-result v24

    #@c0
    if-eqz v24, :cond_7d

    #@c2
    .line 1035
    move-object/from16 v0, p0

    #@c4
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@c6
    move-object/from16 v24, v0

    #@c8
    const-string v25, "apn2_disable"

    #@ca
    const/16 v26, 0x0

    #@cc
    invoke-static/range {v24 .. v26}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@cf
    move-result v24

    #@d0
    const/16 v25, 0x1

    #@d2
    move/from16 v0, v24

    #@d4
    move/from16 v1, v25

    #@d6
    if-ne v0, v1, :cond_fe

    #@d8
    const/4 v5, 0x1

    #@d9
    .line 1036
    .local v5, apn2_disable_Mode:Z
    :goto_d9
    const-string v24, "GSM"

    #@db
    new-instance v25, Ljava/lang/StringBuilder;

    #@dd
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@e0
    const-string v26, "apn2_disable_Mode: "

    #@e2
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v25

    #@e6
    move-object/from16 v0, v25

    #@e8
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v25

    #@ec
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ef
    move-result-object v25

    #@f0
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@f3
    .line 1037
    if-eqz v5, :cond_7d

    #@f5
    .line 1038
    const/16 v24, 0x1

    #@f7
    move/from16 v0, v24

    #@f9
    move-object/from16 v1, p0

    #@fb
    iput-boolean v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@fd
    goto :goto_7d

    #@fe
    .line 1035
    .end local v5           #apn2_disable_Mode:Z
    :cond_fe
    const/4 v5, 0x0

    #@ff
    goto :goto_d9

    #@100
    .line 1045
    :sswitch_100
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->pollState()V

    #@103
    .line 1047
    const/16 v24, 0x0

    #@105
    const-string v25, "KR_REJECT_CAUSE"

    #@107
    invoke-static/range {v24 .. v25}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@10a
    move-result v24

    #@10b
    if-eqz v24, :cond_134

    #@10d
    const-string v24, "KR"

    #@10f
    const-string v25, "LGU"

    #@111
    invoke-static/range {v24 .. v25}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@114
    move-result v24

    #@115
    if-nez v24, :cond_134

    #@117
    .line 1048
    const-string v24, "GSM"

    #@119
    const-string v25, "[LGE] getBal ModemItem.W_BASE.LGE_MODEM_INFO_SERVICE_STATUS"

    #@11b
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11e
    .line 1049
    move-object/from16 v0, p0

    #@120
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@122
    move-object/from16 v24, v0

    #@124
    const v25, 0x6001c

    #@127
    const/16 v26, 0x2d

    #@129
    move-object/from16 v0, p0

    #@12b
    move/from16 v1, v26

    #@12d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@130
    move-result-object v26

    #@131
    invoke-virtual/range {v24 .. v26}, Lcom/android/internal/telephony/gsm/GSMPhone;->getModemStringItem(ILandroid/os/Message;)V

    #@134
    .line 1053
    :cond_134
    const-string v24, "KR"

    #@136
    const-string v25, "LGU"

    #@138
    invoke-static/range {v24 .. v25}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@13b
    move-result v24

    #@13c
    if-nez v24, :cond_148

    #@13e
    const-string v24, "KR"

    #@140
    const-string v25, "SKT"

    #@142
    invoke-static/range {v24 .. v25}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@145
    move-result v24

    #@146
    if-eqz v24, :cond_7d

    #@148
    .line 1054
    :cond_148
    move-object/from16 v0, p0

    #@14a
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLgeRegStateNotification:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@14c
    move-object/from16 v24, v0

    #@14e
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->handleNotification()V

    #@151
    goto/16 :goto_7d

    #@153
    .line 1061
    :sswitch_153
    const/16 v24, 0x0

    #@155
    const-string v25, "KR_REJECT_CAUSE"

    #@157
    invoke-static/range {v24 .. v25}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@15a
    move-result v24

    #@15b
    if-eqz v24, :cond_7d

    #@15d
    .line 1062
    move-object/from16 v0, p0

    #@15f
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@161
    move-object/from16 v24, v0

    #@163
    if-eqz v24, :cond_7d

    #@165
    .line 1063
    move-object/from16 v0, p0

    #@167
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@169
    move-object/from16 v25, v0

    #@16b
    move-object/from16 v0, p1

    #@16d
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16f
    move-object/from16 v24, v0

    #@171
    check-cast v24, Landroid/os/AsyncResult;

    #@173
    move-object/from16 v0, v25

    #@175
    move-object/from16 v1, v24

    #@177
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/gsm/RejectCause;->handleServiceStatusResult(Landroid/os/AsyncResult;)I

    #@17a
    goto/16 :goto_7d

    #@17c
    .line 1069
    :sswitch_17c
    const/16 v24, 0x0

    #@17e
    const-string v25, "KR_REJECT_CAUSE"

    #@180
    invoke-static/range {v24 .. v25}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@183
    move-result v24

    #@184
    if-eqz v24, :cond_7d

    #@186
    .line 1070
    move-object/from16 v0, p0

    #@188
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@18a
    move-object/from16 v24, v0

    #@18c
    if-eqz v24, :cond_1e3

    #@18e
    .line 1071
    move-object/from16 v0, p0

    #@190
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@192
    move-object/from16 v25, v0

    #@194
    move-object/from16 v0, p1

    #@196
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@198
    move-object/from16 v24, v0

    #@19a
    check-cast v24, Landroid/os/AsyncResult;

    #@19c
    move-object/from16 v0, v25

    #@19e
    move-object/from16 v1, v24

    #@1a0
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/gsm/RejectCause;->handleServiceStatusResult(Landroid/os/AsyncResult;)I

    #@1a3
    move-result v19

    #@1a4
    .line 1072
    .local v19, result:I
    const/16 v24, 0x2

    #@1a6
    move/from16 v0, v19

    #@1a8
    move/from16 v1, v24

    #@1aa
    if-ne v0, v1, :cond_1c7

    #@1ac
    .line 1073
    move-object/from16 v0, p0

    #@1ae
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1b0
    move-object/from16 v24, v0

    #@1b2
    const v25, 0x6001c

    #@1b5
    const/16 v26, 0x2d

    #@1b7
    move-object/from16 v0, p0

    #@1b9
    move/from16 v1, v26

    #@1bb
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@1be
    move-result-object v26

    #@1bf
    invoke-virtual/range {v24 .. v26}, Lcom/android/internal/telephony/gsm/GSMPhone;->getModemStringItem(ILandroid/os/Message;)V

    #@1c2
    .line 1083
    .end local v19           #result:I
    :goto_1c2
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@1c5
    goto/16 :goto_7d

    #@1c7
    .line 1075
    .restart local v19       #result:I
    :cond_1c7
    const/16 v24, -0x1

    #@1c9
    move/from16 v0, v19

    #@1cb
    move/from16 v1, v24

    #@1cd
    if-ne v0, v1, :cond_1d9

    #@1cf
    .line 1076
    const-string v24, "mRejectCause ERROR!!!"

    #@1d1
    move-object/from16 v0, p0

    #@1d3
    move-object/from16 v1, v24

    #@1d5
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1d8
    goto :goto_1c2

    #@1d9
    .line 1078
    :cond_1d9
    const-string v24, "mRejectCause process Done"

    #@1db
    move-object/from16 v0, p0

    #@1dd
    move-object/from16 v1, v24

    #@1df
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1e2
    goto :goto_1c2

    #@1e3
    .line 1081
    .end local v19           #result:I
    :cond_1e3
    const-string v24, "mRejectCause null ERROR!!!"

    #@1e5
    move-object/from16 v0, p0

    #@1e7
    move-object/from16 v1, v24

    #@1e9
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@1ec
    goto :goto_1c2

    #@1ed
    .line 1088
    :sswitch_1ed
    move-object/from16 v0, p1

    #@1ef
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1f1
    check-cast v6, Landroid/os/AsyncResult;

    #@1f3
    .line 1089
    .local v6, ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1f5
    move-object/from16 v24, v0

    #@1f7
    if-nez v24, :cond_7d

    #@1f9
    .line 1090
    const-string v25, "GSM"

    #@1fb
    new-instance v24, Ljava/lang/StringBuilder;

    #@1fd
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@200
    const-string v26, "LGT Network Accept: mm_accept = "

    #@202
    move-object/from16 v0, v24

    #@204
    move-object/from16 v1, v26

    #@206
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v26

    #@20a
    iget-object v0, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@20c
    move-object/from16 v24, v0

    #@20e
    check-cast v24, [I

    #@210
    check-cast v24, [I

    #@212
    const/16 v27, 0x0

    #@214
    aget v24, v24, v27

    #@216
    move-object/from16 v0, v26

    #@218
    move/from16 v1, v24

    #@21a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21d
    move-result-object v24

    #@21e
    const-string v26, " ,gmm_accept = "

    #@220
    move-object/from16 v0, v24

    #@222
    move-object/from16 v1, v26

    #@224
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@227
    move-result-object v26

    #@228
    iget-object v0, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@22a
    move-object/from16 v24, v0

    #@22c
    check-cast v24, [I

    #@22e
    check-cast v24, [I

    #@230
    const/16 v27, 0x1

    #@232
    aget v24, v24, v27

    #@234
    move-object/from16 v0, v26

    #@236
    move/from16 v1, v24

    #@238
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23b
    move-result-object v24

    #@23c
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23f
    move-result-object v24

    #@240
    move-object/from16 v0, v25

    #@242
    move-object/from16 v1, v24

    #@244
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@247
    .line 1091
    iget-object v0, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@249
    move-object/from16 v24, v0

    #@24b
    check-cast v24, [I

    #@24d
    move-object/from16 v10, v24

    #@24f
    check-cast v10, [I

    #@251
    .line 1092
    .local v10, ints:[I
    move-object/from16 v0, p0

    #@253
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@255
    move-object/from16 v24, v0

    #@257
    const/16 v25, 0x0

    #@259
    aget v25, v10, v25

    #@25b
    const/16 v26, 0x1

    #@25d
    aget v26, v10, v26

    #@25f
    invoke-interface/range {v24 .. v26}, Lcom/android/internal/telephony/gsm/RejectCause;->clearRejectCause(II)Z

    #@262
    move-result v24

    #@263
    if-nez v24, :cond_27b

    #@265
    .line 1093
    move-object/from16 v0, p0

    #@267
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@269
    move-object/from16 v24, v0

    #@26b
    const v25, 0x6001c

    #@26e
    const/16 v26, 0x2d

    #@270
    move-object/from16 v0, p0

    #@272
    move/from16 v1, v26

    #@274
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@277
    move-result-object v26

    #@278
    invoke-virtual/range {v24 .. v26}, Lcom/android/internal/telephony/gsm/GSMPhone;->getModemStringItem(ILandroid/os/Message;)V

    #@27b
    .line 1095
    :cond_27b
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@27e
    goto/16 :goto_7d

    #@280
    .line 1104
    .end local v6           #ar:Landroid/os/AsyncResult;
    .end local v10           #ints:[I
    :sswitch_280
    move-object/from16 v0, p0

    #@282
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@284
    move-object/from16 v24, v0

    #@286
    invoke-interface/range {v24 .. v24}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@289
    move-result-object v24

    #@28a
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@28d
    move-result v24

    #@28e
    if-eqz v24, :cond_7d

    #@290
    .line 1108
    move-object/from16 v0, p1

    #@292
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@294
    check-cast v6, Landroid/os/AsyncResult;

    #@296
    .line 1109
    .restart local v6       #ar:Landroid/os/AsyncResult;
    const/16 v24, 0x1

    #@298
    move-object/from16 v0, p0

    #@29a
    move/from16 v1, v24

    #@29c
    invoke-virtual {v0, v6, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->onSignalStrengthResult(Landroid/os/AsyncResult;Z)Z

    #@29f
    .line 1110
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->queueNextSignalStrengthPoll()V

    #@2a2
    goto/16 :goto_7d

    #@2a4
    .line 1115
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_2a4
    move-object/from16 v0, p1

    #@2a6
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2a8
    check-cast v6, Landroid/os/AsyncResult;

    #@2aa
    .line 1117
    .restart local v6       #ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2ac
    move-object/from16 v24, v0

    #@2ae
    if-nez v24, :cond_33e

    #@2b0
    .line 1118
    iget-object v0, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2b2
    move-object/from16 v24, v0

    #@2b4
    check-cast v24, [Ljava/lang/String;

    #@2b6
    move-object/from16 v21, v24

    #@2b8
    check-cast v21, [Ljava/lang/String;

    #@2ba
    .line 1119
    .local v21, states:[Ljava/lang/String;
    const/4 v11, -0x1

    #@2bb
    .line 1120
    .local v11, lac:I
    const/4 v7, -0x1

    #@2bc
    .line 1121
    .local v7, cid:I
    move-object/from16 v0, v21

    #@2be
    array-length v0, v0

    #@2bf
    move/from16 v24, v0

    #@2c1
    const/16 v25, 0x3

    #@2c3
    move/from16 v0, v24

    #@2c5
    move/from16 v1, v25

    #@2c7
    if-lt v0, v1, :cond_312

    #@2c9
    .line 1129
    :try_start_2c9
    move-object/from16 v0, p0

    #@2cb
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mIsLTE:Z

    #@2cd
    move/from16 v24, v0

    #@2cf
    if-eqz v24, :cond_343

    #@2d1
    .line 1130
    move-object/from16 v0, v21

    #@2d3
    array-length v0, v0

    #@2d4
    move/from16 v24, v0

    #@2d6
    const/16 v25, 0x7

    #@2d8
    move/from16 v0, v24

    #@2da
    move/from16 v1, v25

    #@2dc
    if-lt v0, v1, :cond_2f8

    #@2de
    const/16 v24, 0x6

    #@2e0
    aget-object v24, v21, v24

    #@2e2
    if-eqz v24, :cond_2f8

    #@2e4
    const/16 v24, 0x6

    #@2e6
    aget-object v24, v21, v24

    #@2e8
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    #@2eb
    move-result v24

    #@2ec
    if-lez v24, :cond_2f8

    #@2ee
    .line 1131
    const/16 v24, 0x6

    #@2f0
    aget-object v24, v21, v24

    #@2f2
    const/16 v25, 0x10

    #@2f4
    invoke-static/range {v24 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@2f7
    move-result v11

    #@2f8
    .line 1139
    :cond_2f8
    :goto_2f8
    const/16 v24, 0x2

    #@2fa
    aget-object v24, v21, v24

    #@2fc
    if-eqz v24, :cond_312

    #@2fe
    const/16 v24, 0x2

    #@300
    aget-object v24, v21, v24

    #@302
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    #@305
    move-result v24

    #@306
    if-lez v24, :cond_312

    #@308
    .line 1140
    const/16 v24, 0x2

    #@30a
    aget-object v24, v21, v24

    #@30c
    const/16 v25, 0x10

    #@30e
    invoke-static/range {v24 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_311
    .catch Ljava/lang/NumberFormatException; {:try_start_2c9 .. :try_end_311} :catch_35e

    #@311
    move-result v7

    #@312
    .line 1148
    :cond_312
    :goto_312
    const-string v24, "JP"

    #@314
    const-string v25, "DCM"

    #@316
    invoke-static/range {v24 .. v25}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@319
    move-result v24

    #@31a
    if-eqz v24, :cond_32a

    #@31c
    move-object/from16 v0, p0

    #@31e
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->state_of_suppressing_not_registered_ind:I

    #@320
    move/from16 v24, v0

    #@322
    const/16 v25, 0x1

    #@324
    move/from16 v0, v24

    #@326
    move/from16 v1, v25

    #@328
    if-eq v0, v1, :cond_37a

    #@32a
    .line 1151
    :cond_32a
    move-object/from16 v0, p0

    #@32c
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@32e
    move-object/from16 v24, v0

    #@330
    move-object/from16 v0, v24

    #@332
    invoke-virtual {v0, v11, v7}, Landroid/telephony/gsm/GsmCellLocation;->setLacAndCid(II)V

    #@335
    .line 1157
    :goto_335
    move-object/from16 v0, p0

    #@337
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@339
    move-object/from16 v24, v0

    #@33b
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyLocationChanged()V

    #@33e
    .line 1162
    .end local v7           #cid:I
    .end local v11           #lac:I
    .end local v21           #states:[Ljava/lang/String;
    :cond_33e
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->disableSingleLocationUpdate()V

    #@341
    goto/16 :goto_7d

    #@343
    .line 1134
    .restart local v7       #cid:I
    .restart local v11       #lac:I
    .restart local v21       #states:[Ljava/lang/String;
    :cond_343
    const/16 v24, 0x1

    #@345
    :try_start_345
    aget-object v24, v21, v24

    #@347
    if-eqz v24, :cond_2f8

    #@349
    const/16 v24, 0x1

    #@34b
    aget-object v24, v21, v24

    #@34d
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    #@350
    move-result v24

    #@351
    if-lez v24, :cond_2f8

    #@353
    .line 1135
    const/16 v24, 0x1

    #@355
    aget-object v24, v21, v24

    #@357
    const/16 v25, 0x10

    #@359
    invoke-static/range {v24 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_35c
    .catch Ljava/lang/NumberFormatException; {:try_start_345 .. :try_end_35c} :catch_35e

    #@35c
    move-result v11

    #@35d
    goto :goto_2f8

    #@35e
    .line 1142
    :catch_35e
    move-exception v8

    #@35f
    .line 1143
    .local v8, ex:Ljava/lang/NumberFormatException;
    const-string v24, "GSM"

    #@361
    new-instance v25, Ljava/lang/StringBuilder;

    #@363
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@366
    const-string v26, "error parsing location: "

    #@368
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36b
    move-result-object v25

    #@36c
    move-object/from16 v0, v25

    #@36e
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@371
    move-result-object v25

    #@372
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@375
    move-result-object v25

    #@376
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@379
    goto :goto_312

    #@37a
    .line 1154
    .end local v8           #ex:Ljava/lang/NumberFormatException;
    :cond_37a
    const-string v24, "NONET_SUPP - EVENT_GET_LOC_DONE, maintain previous cell information."

    #@37c
    move-object/from16 v0, p0

    #@37e
    move-object/from16 v1, v24

    #@380
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@383
    goto :goto_335

    #@384
    .line 1169
    .end local v6           #ar:Landroid/os/AsyncResult;
    .end local v7           #cid:I
    .end local v11           #lac:I
    .end local v21           #states:[Ljava/lang/String;
    :sswitch_384
    move-object/from16 v0, p1

    #@386
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@388
    check-cast v6, Landroid/os/AsyncResult;

    #@38a
    .line 1171
    .restart local v6       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, p1

    #@38c
    iget v0, v0, Landroid/os/Message;->what:I

    #@38e
    move/from16 v24, v0

    #@390
    move-object/from16 v0, p0

    #@392
    move/from16 v1, v24

    #@394
    invoke-virtual {v0, v1, v6}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->handlePollStateResult(ILandroid/os/AsyncResult;)V

    #@397
    goto/16 :goto_7d

    #@399
    .line 1177
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_399
    move-object/from16 v0, p0

    #@39b
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@39d
    move-object/from16 v24, v0

    #@39f
    const/16 v25, 0x3

    #@3a1
    move-object/from16 v0, p0

    #@3a3
    move/from16 v1, v25

    #@3a5
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@3a8
    move-result-object v25

    #@3a9
    invoke-interface/range {v24 .. v25}, Lcom/android/internal/telephony/CommandsInterface;->getSignalStrength(Landroid/os/Message;)V

    #@3ac
    goto/16 :goto_7d

    #@3ae
    .line 1181
    :sswitch_3ae
    move-object/from16 v0, p1

    #@3b0
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3b2
    check-cast v6, Landroid/os/AsyncResult;

    #@3b4
    .line 1183
    .restart local v6       #ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3b6
    move-object/from16 v24, v0

    #@3b8
    check-cast v24, [Ljava/lang/Object;

    #@3ba
    check-cast v24, [Ljava/lang/Object;

    #@3bc
    const/16 v25, 0x0

    #@3be
    aget-object v17, v24, v25

    #@3c0
    check-cast v17, Ljava/lang/String;

    #@3c2
    .line 1184
    .local v17, nitzString:Ljava/lang/String;
    iget-object v0, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3c4
    move-object/from16 v24, v0

    #@3c6
    check-cast v24, [Ljava/lang/Object;

    #@3c8
    check-cast v24, [Ljava/lang/Object;

    #@3ca
    const/16 v25, 0x1

    #@3cc
    aget-object v24, v24, v25

    #@3ce
    check-cast v24, Ljava/lang/Long;

    #@3d0
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    #@3d3
    move-result-wide v15

    #@3d4
    .line 1187
    .local v15, nitzReceiveTime:J
    sget-object v24, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@3d6
    if-eqz v24, :cond_3dd

    #@3d8
    .line 1188
    sget-object v24, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@3da
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->receivedNitz()V

    #@3dd
    .line 1192
    :cond_3dd
    move-object/from16 v0, p0

    #@3df
    move-object/from16 v1, v17

    #@3e1
    move-wide v2, v15

    #@3e2
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setTimeFromNITZString(Ljava/lang/String;J)V

    #@3e5
    goto/16 :goto_7d

    #@3e7
    .line 1199
    .end local v6           #ar:Landroid/os/AsyncResult;
    .end local v15           #nitzReceiveTime:J
    .end local v17           #nitzString:Ljava/lang/String;
    :sswitch_3e7
    move-object/from16 v0, p1

    #@3e9
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3eb
    check-cast v6, Landroid/os/AsyncResult;

    #@3ed
    .line 1203
    .restart local v6       #ar:Landroid/os/AsyncResult;
    const/16 v24, 0x1

    #@3ef
    move/from16 v0, v24

    #@3f1
    move-object/from16 v1, p0

    #@3f3
    iput-boolean v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->dontPollSignalStrength:Z

    #@3f5
    .line 1205
    const/16 v24, 0x1

    #@3f7
    move-object/from16 v0, p0

    #@3f9
    move/from16 v1, v24

    #@3fb
    invoke-virtual {v0, v6, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->onSignalStrengthResult(Landroid/os/AsyncResult;Z)Z

    #@3fe
    goto/16 :goto_7d

    #@400
    .line 1209
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_400
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@403
    .line 1212
    const/16 v24, 0x0

    #@405
    const-string v25, "support_assisted_dialing"

    #@407
    invoke-static/range {v24 .. v25}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@40a
    move-result v24

    #@40b
    if-eqz v24, :cond_7d

    #@40d
    .line 1213
    move-object/from16 v0, p0

    #@40f
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@411
    move-object/from16 v24, v0

    #@413
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/gsm/GSMPhone;->getLine1Number()Ljava/lang/String;

    #@416
    move-result-object v13

    #@417
    .line 1214
    .local v13, mMdn:Ljava/lang/String;
    new-instance v24, Ljava/lang/StringBuilder;

    #@419
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@41c
    const-string v25, "handleMessage EVENT_SIM_RECORDS_LOADED : mMdn = "

    #@41e
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@421
    move-result-object v24

    #@422
    move-object/from16 v0, v24

    #@424
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@427
    move-result-object v24

    #@428
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42b
    move-result-object v24

    #@42c
    move-object/from16 v0, p0

    #@42e
    move-object/from16 v1, v24

    #@430
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@433
    .line 1215
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isIccIdChanged()Z

    #@436
    move-result v24

    #@437
    if-eqz v24, :cond_7d

    #@439
    .line 1216
    const-string v24, "isIccIdChanged = true, Area/Length Update"

    #@43b
    move-object/from16 v0, p0

    #@43d
    move-object/from16 v1, v24

    #@43f
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@442
    .line 1217
    if-eqz v13, :cond_7d

    #@444
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@447
    move-result v24

    #@448
    const/16 v25, 0x3

    #@44a
    move/from16 v0, v24

    #@44c
    move/from16 v1, v25

    #@44e
    if-lt v0, v1, :cond_7d

    #@450
    .line 1218
    move-object/from16 v0, p0

    #@452
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@454
    move-object/from16 v24, v0

    #@456
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@459
    move-result-object v24

    #@45a
    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@45d
    move-result-object v24

    #@45e
    const-string v25, "area"

    #@460
    const/16 v26, 0x0

    #@462
    const/16 v27, 0x3

    #@464
    move/from16 v0, v26

    #@466
    move/from16 v1, v27

    #@468
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@46b
    move-result-object v26

    #@46c
    invoke-static/range {v24 .. v26}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@46f
    .line 1222
    new-instance v23, Landroid/content/ContentValues;

    #@471
    invoke-direct/range {v23 .. v23}, Landroid/content/ContentValues;-><init>()V

    #@474
    .line 1223
    .local v23, values:Landroid/content/ContentValues;
    const-string v24, "area"

    #@476
    const/16 v25, 0x0

    #@478
    const/16 v26, 0x3

    #@47a
    move/from16 v0, v25

    #@47c
    move/from16 v1, v26

    #@47e
    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@481
    move-result-object v25

    #@482
    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@485
    .line 1224
    const-string v24, "length"

    #@487
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@48a
    move-result v25

    #@48b
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@48e
    move-result-object v25

    #@48f
    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@492
    .line 1225
    move-object/from16 v0, p0

    #@494
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@496
    move-object/from16 v24, v0

    #@498
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@49b
    move-result-object v24

    #@49c
    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@49f
    move-result-object v24

    #@4a0
    sget-object v25, Landroid/provider/Settings$AssistDial;->CONTENT_URI:Landroid/net/Uri;

    #@4a2
    const/16 v26, 0x0

    #@4a4
    const/16 v27, 0x0

    #@4a6
    move-object/from16 v0, v24

    #@4a8
    move-object/from16 v1, v25

    #@4aa
    move-object/from16 v2, v23

    #@4ac
    move-object/from16 v3, v26

    #@4ae
    move-object/from16 v4, v27

    #@4b0
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@4b3
    goto/16 :goto_7d

    #@4b5
    .line 1235
    .end local v13           #mMdn:Ljava/lang/String;
    .end local v23           #values:Landroid/content/ContentValues;
    :sswitch_4b5
    move-object/from16 v0, p1

    #@4b7
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4b9
    check-cast v6, Landroid/os/AsyncResult;

    #@4bb
    .line 1237
    .restart local v6       #ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4bd
    move-object/from16 v24, v0

    #@4bf
    if-nez v24, :cond_7d

    #@4c1
    .line 1242
    new-instance v24, Ljava/lang/StringBuilder;

    #@4c3
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@4c6
    const-string v25, "EVENT_LOCATION_UPDATES_ENABLED: getRadioTechnology =\'"

    #@4c8
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4cb
    move-result-object v24

    #@4cc
    move-object/from16 v0, p0

    #@4ce
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4d0
    move-object/from16 v25, v0

    #@4d2
    invoke-virtual/range {v25 .. v25}, Lcom/android/internal/telephony/gsm/GSMPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@4d5
    move-result-object v25

    #@4d6
    invoke-virtual/range {v25 .. v25}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@4d9
    move-result v25

    #@4da
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4dd
    move-result-object v24

    #@4de
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e1
    move-result-object v24

    #@4e2
    move-object/from16 v0, p0

    #@4e4
    move-object/from16 v1, v24

    #@4e6
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@4e9
    .line 1243
    move-object/from16 v0, p0

    #@4eb
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4ed
    move-object/from16 v24, v0

    #@4ef
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/gsm/GSMPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@4f2
    move-result-object v24

    #@4f3
    invoke-virtual/range {v24 .. v24}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@4f6
    move-result v24

    #@4f7
    const/16 v25, 0xe

    #@4f9
    move/from16 v0, v24

    #@4fb
    move/from16 v1, v25

    #@4fd
    if-ne v0, v1, :cond_520

    #@4ff
    .line 1244
    move-object/from16 v0, p0

    #@501
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@503
    move-object/from16 v24, v0

    #@505
    const/16 v25, 0xf

    #@507
    const/16 v26, 0x0

    #@509
    move-object/from16 v0, p0

    #@50b
    move/from16 v1, v25

    #@50d
    move-object/from16 v2, v26

    #@50f
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@512
    move-result-object v25

    #@513
    invoke-interface/range {v24 .. v25}, Lcom/android/internal/telephony/CommandsInterface;->getDataRegistrationState(Landroid/os/Message;)V

    #@516
    .line 1245
    const/16 v24, 0x1

    #@518
    move/from16 v0, v24

    #@51a
    move-object/from16 v1, p0

    #@51c
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mIsLTE:Z

    #@51e
    goto/16 :goto_7d

    #@520
    .line 1247
    :cond_520
    move-object/from16 v0, p0

    #@522
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@524
    move-object/from16 v24, v0

    #@526
    const/16 v25, 0xf

    #@528
    const/16 v26, 0x0

    #@52a
    move-object/from16 v0, p0

    #@52c
    move/from16 v1, v25

    #@52e
    move-object/from16 v2, v26

    #@530
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@533
    move-result-object v25

    #@534
    invoke-interface/range {v24 .. v25}, Lcom/android/internal/telephony/CommandsInterface;->getVoiceRegistrationState(Landroid/os/Message;)V

    #@537
    .line 1248
    const/16 v24, 0x0

    #@539
    move/from16 v0, v24

    #@53b
    move-object/from16 v1, p0

    #@53d
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mIsLTE:Z

    #@53f
    goto/16 :goto_7d

    #@541
    .line 1255
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_541
    move-object/from16 v0, p1

    #@543
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@545
    check-cast v6, Landroid/os/AsyncResult;

    #@547
    .line 1257
    .restart local v6       #ar:Landroid/os/AsyncResult;
    const/16 v24, 0x15

    #@549
    iget-object v0, v6, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@54b
    move-object/from16 v25, v0

    #@54d
    move-object/from16 v0, p0

    #@54f
    move/from16 v1, v24

    #@551
    move-object/from16 v2, v25

    #@553
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@556
    move-result-object v14

    #@557
    .line 1258
    .local v14, message:Landroid/os/Message;
    move-object/from16 v0, p0

    #@559
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@55b
    move-object/from16 v24, v0

    #@55d
    move-object/from16 v0, p0

    #@55f
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mPreferredNetworkType:I

    #@561
    move/from16 v25, v0

    #@563
    move-object/from16 v0, v24

    #@565
    move/from16 v1, v25

    #@567
    invoke-interface {v0, v1, v14}, Lcom/android/internal/telephony/CommandsInterface;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@56a
    goto/16 :goto_7d

    #@56c
    .line 1262
    .end local v6           #ar:Landroid/os/AsyncResult;
    .end local v14           #message:Landroid/os/Message;
    :sswitch_56c
    move-object/from16 v0, p1

    #@56e
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@570
    check-cast v6, Landroid/os/AsyncResult;

    #@572
    .line 1263
    .restart local v6       #ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@574
    move-object/from16 v24, v0

    #@576
    if-eqz v24, :cond_7d

    #@578
    .line 1264
    iget-object v0, v6, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@57a
    move-object/from16 v24, v0

    #@57c
    check-cast v24, Landroid/os/Message;

    #@57e
    invoke-static/range {v24 .. v24}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@581
    move-result-object v24

    #@582
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@584
    move-object/from16 v25, v0

    #@586
    move-object/from16 v0, v25

    #@588
    move-object/from16 v1, v24

    #@58a
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@58c
    .line 1266
    iget-object v0, v6, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@58e
    move-object/from16 v24, v0

    #@590
    check-cast v24, Landroid/os/Message;

    #@592
    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    #@595
    goto/16 :goto_7d

    #@597
    .line 1271
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_597
    move-object/from16 v0, p1

    #@599
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@59b
    check-cast v6, Landroid/os/AsyncResult;

    #@59d
    .line 1273
    .restart local v6       #ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@59f
    move-object/from16 v24, v0

    #@5a1
    if-nez v24, :cond_5d6

    #@5a3
    .line 1274
    iget-object v0, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@5a5
    move-object/from16 v24, v0

    #@5a7
    check-cast v24, [I

    #@5a9
    check-cast v24, [I

    #@5ab
    const/16 v25, 0x0

    #@5ad
    aget v24, v24, v25

    #@5af
    move/from16 v0, v24

    #@5b1
    move-object/from16 v1, p0

    #@5b3
    iput v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mPreferredNetworkType:I

    #@5b5
    .line 1279
    :goto_5b5
    const/16 v24, 0x14

    #@5b7
    iget-object v0, v6, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@5b9
    move-object/from16 v25, v0

    #@5bb
    move-object/from16 v0, p0

    #@5bd
    move/from16 v1, v24

    #@5bf
    move-object/from16 v2, v25

    #@5c1
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@5c4
    move-result-object v14

    #@5c5
    .line 1280
    .restart local v14       #message:Landroid/os/Message;
    const/16 v22, 0x7

    #@5c7
    .line 1282
    .local v22, toggledNetworkType:I
    move-object/from16 v0, p0

    #@5c9
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@5cb
    move-object/from16 v24, v0

    #@5cd
    move-object/from16 v0, v24

    #@5cf
    move/from16 v1, v22

    #@5d1
    invoke-interface {v0, v1, v14}, Lcom/android/internal/telephony/CommandsInterface;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@5d4
    goto/16 :goto_7d

    #@5d6
    .line 1276
    .end local v14           #message:Landroid/os/Message;
    .end local v22           #toggledNetworkType:I
    :cond_5d6
    const/16 v24, 0x7

    #@5d8
    move/from16 v0, v24

    #@5da
    move-object/from16 v1, p0

    #@5dc
    iput v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mPreferredNetworkType:I

    #@5de
    goto :goto_5b5

    #@5df
    .line 1286
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_5df
    move-object/from16 v0, p0

    #@5e1
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@5e3
    move-object/from16 v24, v0

    #@5e5
    if-eqz v24, :cond_641

    #@5e7
    move-object/from16 v0, p0

    #@5e9
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->gprsState:I

    #@5eb
    move/from16 v24, v0

    #@5ed
    move-object/from16 v0, p0

    #@5ef
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@5f1
    move-object/from16 v25, v0

    #@5f3
    invoke-virtual/range {v25 .. v25}, Landroid/telephony/ServiceState;->getState()I

    #@5f6
    move-result v25

    #@5f7
    move-object/from16 v0, p0

    #@5f9
    move/from16 v1, v24

    #@5fb
    move/from16 v2, v25

    #@5fd
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isGprsConsistent(II)Z

    #@600
    move-result v24

    #@601
    if-nez v24, :cond_641

    #@603
    .line 1291
    move-object/from16 v0, p0

    #@605
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@607
    move-object/from16 v24, v0

    #@609
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/gsm/GSMPhone;->getCellLocation()Landroid/telephony/CellLocation;

    #@60c
    move-result-object v12

    #@60d
    check-cast v12, Landroid/telephony/gsm/GsmCellLocation;

    #@60f
    .line 1292
    .local v12, loc:Landroid/telephony/gsm/GsmCellLocation;
    const v25, 0xc3bb

    #@612
    const/16 v24, 0x2

    #@614
    move/from16 v0, v24

    #@616
    new-array v0, v0, [Ljava/lang/Object;

    #@618
    move-object/from16 v26, v0

    #@61a
    const/16 v24, 0x0

    #@61c
    move-object/from16 v0, p0

    #@61e
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@620
    move-object/from16 v27, v0

    #@622
    invoke-virtual/range {v27 .. v27}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@625
    move-result-object v27

    #@626
    aput-object v27, v26, v24

    #@628
    const/16 v27, 0x1

    #@62a
    if-eqz v12, :cond_64b

    #@62c
    invoke-virtual {v12}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    #@62f
    move-result v24

    #@630
    :goto_630
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@633
    move-result-object v24

    #@634
    aput-object v24, v26, v27

    #@636
    invoke-static/range {v25 .. v26}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@639
    .line 1294
    const/16 v24, 0x1

    #@63b
    move/from16 v0, v24

    #@63d
    move-object/from16 v1, p0

    #@63f
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mReportedGprsNoReg:Z

    #@641
    .line 1296
    .end local v12           #loc:Landroid/telephony/gsm/GsmCellLocation;
    :cond_641
    const/16 v24, 0x0

    #@643
    move/from16 v0, v24

    #@645
    move-object/from16 v1, p0

    #@647
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mStartedGprsRegCheck:Z

    #@649
    goto/16 :goto_7d

    #@64b
    .line 1292
    .restart local v12       #loc:Landroid/telephony/gsm/GsmCellLocation;
    :cond_64b
    const/16 v24, -0x1

    #@64d
    goto :goto_630

    #@64e
    .line 1303
    .end local v12           #loc:Landroid/telephony/gsm/GsmCellLocation;
    :sswitch_64e
    const-string v24, "EVENT_RESTRICTED_STATE_CHANGED"

    #@650
    move-object/from16 v0, p0

    #@652
    move-object/from16 v1, v24

    #@654
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@657
    .line 1305
    move-object/from16 v0, p1

    #@659
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@65b
    check-cast v6, Landroid/os/AsyncResult;

    #@65d
    .line 1307
    .restart local v6       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, p0

    #@65f
    invoke-direct {v0, v6}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->onRestrictedStateChanged(Landroid/os/AsyncResult;)V

    #@662
    goto/16 :goto_7d

    #@664
    .line 1311
    .end local v6           #ar:Landroid/os/AsyncResult;
    :sswitch_664
    const-string v24, "[IMS_AFW] EVENT_GET_LTE_INFO_DONE"

    #@666
    move-object/from16 v0, p0

    #@668
    move-object/from16 v1, v24

    #@66a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@66d
    .line 1312
    move-object/from16 v0, p1

    #@66f
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@671
    move-object/from16 v24, v0

    #@673
    check-cast v24, Landroid/os/AsyncResult;

    #@675
    move-object/from16 v0, p0

    #@677
    move-object/from16 v1, v24

    #@679
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->onSetLteInfo(Landroid/os/AsyncResult;)V

    #@67c
    goto/16 :goto_7d

    #@67e
    .line 1315
    :sswitch_67e
    const-string v24, "[IMS_AFW] EVENT_GET_EHRPD_INFO_DONE"

    #@680
    move-object/from16 v0, p0

    #@682
    move-object/from16 v1, v24

    #@684
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@687
    .line 1316
    move-object/from16 v0, p1

    #@689
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@68b
    move-object/from16 v24, v0

    #@68d
    check-cast v24, Landroid/os/AsyncResult;

    #@68f
    move-object/from16 v0, p0

    #@691
    move-object/from16 v1, v24

    #@693
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->onSetEhrpdInfo(Landroid/os/AsyncResult;)V

    #@696
    goto/16 :goto_7d

    #@698
    .line 1323
    :sswitch_698
    const-string v24, "GSM"

    #@69a
    const-string v25, "[GsmServiceStateTracker] case EVENT_CHANGE_IMS_STATE:"

    #@69c
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@69f
    .line 1324
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setPowerStateToDesired()V

    #@6a2
    goto/16 :goto_7d

    #@6a4
    .line 1331
    :sswitch_6a4
    const-string v24, "EVENT_REATTACH_INTENT_DELAY received"

    #@6a6
    move-object/from16 v0, p0

    #@6a8
    move-object/from16 v1, v24

    #@6aa
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@6ad
    .line 1334
    new-instance v9, Landroid/content/Intent;

    #@6af
    const-string v24, "com.lge.GprsAttachedIsTrue"

    #@6b1
    move-object/from16 v0, v24

    #@6b3
    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@6b6
    .line 1335
    .local v9, intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@6b8
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@6ba
    move-object/from16 v24, v0

    #@6bc
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@6bf
    move-result-object v24

    #@6c0
    move-object/from16 v0, v24

    #@6c2
    invoke-virtual {v0, v9}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@6c5
    goto/16 :goto_7d

    #@6c7
    .line 1343
    .end local v9           #intent:Landroid/content/Intent;
    :sswitch_6c7
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->handleApnChanged()V

    #@6ca
    goto/16 :goto_7d

    #@6cc
    .line 1348
    :sswitch_6cc
    const/16 v24, 0x1

    #@6ce
    sput-boolean v24, Lcom/android/internal/telephony/TelephonyUtils;->persoLockChecked:Z

    #@6d0
    .line 1349
    const-string v24, "GSM"

    #@6d2
    new-instance v25, Ljava/lang/StringBuilder;

    #@6d4
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@6d7
    const-string v26, "[LGE] EVENT_DELAYED_REJECT_CAUSE_ACTIVATION : persoLockChecked="

    #@6d9
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6dc
    move-result-object v25

    #@6dd
    sget-boolean v26, Lcom/android/internal/telephony/TelephonyUtils;->persoLockChecked:Z

    #@6df
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6e2
    move-result-object v25

    #@6e3
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e6
    move-result-object v25

    #@6e7
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6ea
    .line 1351
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->pollState()V

    #@6ed
    .line 1352
    const/16 v24, 0x0

    #@6ef
    const-string v25, "KR_REJECT_CAUSE"

    #@6f1
    invoke-static/range {v24 .. v25}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6f4
    move-result v24

    #@6f5
    if-eqz v24, :cond_7d

    #@6f7
    const-string v24, "KR"

    #@6f9
    const-string v25, "LGU"

    #@6fb
    invoke-static/range {v24 .. v25}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@6fe
    move-result v24

    #@6ff
    if-nez v24, :cond_7d

    #@701
    .line 1353
    move-object/from16 v0, p0

    #@703
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@705
    move-object/from16 v24, v0

    #@707
    const v25, 0x6001c

    #@70a
    const/16 v26, 0x2d

    #@70c
    move-object/from16 v0, p0

    #@70e
    move/from16 v1, v26

    #@710
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@713
    move-result-object v26

    #@714
    invoke-virtual/range {v24 .. v26}, Lcom/android/internal/telephony/gsm/GSMPhone;->getModemStringItem(ILandroid/os/Message;)V

    #@717
    goto/16 :goto_7d

    #@719
    .line 1360
    :sswitch_719
    const-string v24, "GSM"

    #@71b
    new-instance v25, Ljava/lang/StringBuilder;

    #@71d
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@720
    const-string v26, "EVENT_SKT_FA_CHG_START, faChannel = "

    #@722
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@725
    move-result-object v25

    #@726
    move-object/from16 v0, p0

    #@728
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->faChannel:Ljava/lang/String;

    #@72a
    move-object/from16 v26, v0

    #@72c
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72f
    move-result-object v25

    #@730
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@733
    move-result-object v25

    #@734
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@737
    .line 1361
    move-object/from16 v0, p0

    #@739
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@73b
    move-object/from16 v24, v0

    #@73d
    const v25, 0x60006

    #@740
    move-object/from16 v0, p0

    #@742
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->faChannel:Ljava/lang/String;

    #@744
    move-object/from16 v26, v0

    #@746
    const/16 v27, 0x3f3

    #@748
    move-object/from16 v0, p0

    #@74a
    move/from16 v1, v27

    #@74c
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@74f
    move-result-object v27

    #@750
    invoke-interface/range {v24 .. v27}, Lcom/android/internal/telephony/CommandsInterface;->setModemStringItem(ILjava/lang/String;Landroid/os/Message;)V

    #@753
    goto/16 :goto_7d

    #@755
    .line 1365
    :sswitch_755
    move-object/from16 v0, p1

    #@757
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@759
    check-cast v6, Landroid/os/AsyncResult;

    #@75b
    .line 1366
    .restart local v6       #ar:Landroid/os/AsyncResult;
    iget-object v0, v6, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@75d
    move-object/from16 v24, v0

    #@75f
    if-eqz v24, :cond_783

    #@761
    .line 1367
    const-string v24, "GSM"

    #@763
    const-string v25, "Error while changing FA"

    #@765
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@768
    .line 1368
    move-object/from16 v0, p0

    #@76a
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@76c
    move-object/from16 v24, v0

    #@76e
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@771
    move-result-object v24

    #@772
    const-string v25, "skt_fa_changed_fail"

    #@774
    invoke-static/range {v25 .. v25}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@777
    move-result-object v25

    #@778
    const/16 v26, 0x0

    #@77a
    invoke-static/range {v24 .. v26}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@77d
    move-result-object v18

    #@77e
    .line 1369
    .local v18, rebootToast:Landroid/widget/Toast;
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    #@781
    goto/16 :goto_7d

    #@783
    .line 1372
    .end local v18           #rebootToast:Landroid/widget/Toast;
    :cond_783
    const-string v24, "GSM"

    #@785
    new-instance v25, Ljava/lang/StringBuilder;

    #@787
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@78a
    const-string v26, "OK! FA Changed"

    #@78c
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78f
    move-result-object v25

    #@790
    iget-object v0, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@792
    move-object/from16 v26, v0

    #@794
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@797
    move-result-object v25

    #@798
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79b
    move-result-object v25

    #@79c
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@79f
    .line 1373
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->faCHGReboot()V

    #@7a2
    .line 1374
    const-string v24, "GSM"

    #@7a4
    const-string v25, "EVENT_SKT_FA_CHG_DONE, faCHGReboot--"

    #@7a6
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a9
    goto/16 :goto_7d

    #@7ab
    .line 1006
    nop

    #@7ac
    :sswitch_data_7ac
    .sparse-switch
        0x1 -> :sswitch_b4
        0x2 -> :sswitch_100
        0x3 -> :sswitch_280
        0x4 -> :sswitch_384
        0x5 -> :sswitch_384
        0x6 -> :sswitch_384
        0xa -> :sswitch_399
        0xb -> :sswitch_3ae
        0xc -> :sswitch_3e7
        0xd -> :sswitch_7d
        0xe -> :sswitch_384
        0xf -> :sswitch_2a4
        0x10 -> :sswitch_400
        0x11 -> :sswitch_8b
        0x12 -> :sswitch_4b5
        0x13 -> :sswitch_597
        0x14 -> :sswitch_541
        0x15 -> :sswitch_56c
        0x16 -> :sswitch_5df
        0x17 -> :sswitch_64e
        0x2c -> :sswitch_698
        0x2d -> :sswitch_153
        0x30 -> :sswitch_17c
        0x31 -> :sswitch_1ed
        0x34 -> :sswitch_6c7
        0x64 -> :sswitch_664
        0x65 -> :sswitch_67e
        0x3f2 -> :sswitch_719
        0x3f3 -> :sswitch_755
        0x3f4 -> :sswitch_6cc
        0x3f5 -> :sswitch_6a4
    .end sparse-switch
.end method

.method public handleNetworkRejection(I)V
    .registers 13
    .parameter "rejCode"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 5240
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@5
    move-result-object v2

    #@6
    .line 5243
    .local v2, r:Landroid/content/res/Resources;
    const/4 v5, 0x0

    #@7
    .line 5244
    .local v5, spn:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@9
    invoke-virtual {v6}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    .line 5245
    .local v1, plmn:Ljava/lang/String;
    const/4 v4, 0x1

    #@e
    .line 5246
    .local v4, showSpn:Z
    const/4 v3, 0x1

    #@f
    .line 5247
    .local v3, showPlmn:Z
    const/4 v0, 0x0

    #@10
    .line 5249
    .local v0, msg:Ljava/lang/String;
    const-string v6, "[GsmServiceStateTracker]"

    #@12
    new-instance v7, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v8, "handleNetworkRejection : Rejection code :"

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v7

    #@21
    const-string v8, ",prev_rej : "

    #@23
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v7

    #@27
    sget v8, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->prev_rej:I

    #@29
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    const-string v8, ", Shown :"

    #@2f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    sget-boolean v8, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bAlreadyShownRejectCause:Z

    #@35
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@38
    move-result-object v7

    #@39
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v7

    #@3d
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 5250
    const-string v6, "[GsmServiceStateTracker]"

    #@42
    const-string v7, "[Helen] Enter handleNetworkRejection"

    #@44
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 5253
    sget v6, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->prev_rej:I

    #@49
    if-eq v6, p1, :cond_4f

    #@4b
    .line 5255
    sput-boolean v10, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bAlreadyShownRejectCause:Z

    #@4d
    .line 5256
    sput p1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->prev_rej:I

    #@4f
    .line 5260
    :cond_4f
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@51
    iget-object v6, v6, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@53
    if-eqz v6, :cond_67

    #@55
    .line 5262
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@57
    iget-object v6, v6, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@59
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5c
    move-result-object v6

    #@5d
    check-cast v6, Lcom/android/internal/telephony/uicc/IccRecords;

    #@5f
    invoke-virtual {v6}, Lcom/android/internal/telephony/uicc/IccRecords;->getServiceProviderName()Ljava/lang/String;

    #@62
    move-result-object v5

    #@63
    .line 5266
    :goto_63
    sparse-switch p1, :sswitch_data_128

    #@66
    .line 5357
    :cond_66
    :goto_66
    return-void

    #@67
    .line 5264
    :cond_67
    const-string v6, "[GsmServiceStateTracker]"

    #@69
    const-string v7, "[Helen] IccRecords is null - spn is null"

    #@6b
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    goto :goto_63

    #@6f
    .line 5273
    :sswitch_6f
    const v6, 0x2090139

    #@72
    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    .line 5274
    sget-boolean v6, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bAlreadyShownRejectCause:Z

    #@78
    if-nez v6, :cond_7d

    #@7a
    .line 5275
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->showDialog(Ljava/lang/String;)V

    #@7d
    .line 5277
    :cond_7d
    const-string v6, "US"

    #@7f
    const-string v7, "TMO"

    #@81
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@84
    move-result v6

    #@85
    if-eqz v6, :cond_66

    #@87
    .line 5278
    iput-boolean v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mPermanentReject:Z

    #@89
    .line 5279
    const-string v6, "[GsmServiceStateTracker]"

    #@8b
    const-string v7, "[CHOSH] Authentication failure, show No service "

    #@8d
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    goto :goto_66

    #@91
    .line 5284
    :sswitch_91
    const v6, 0x209013a

    #@94
    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@97
    move-result-object v0

    #@98
    .line 5285
    sget-boolean v6, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bAlreadyShownRejectCause:Z

    #@9a
    if-nez v6, :cond_9f

    #@9c
    .line 5286
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->showDialog(Ljava/lang/String;)V

    #@9f
    .line 5288
    :cond_9f
    const-string v6, "US"

    #@a1
    const-string v7, "TMO"

    #@a3
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@a6
    move-result v6

    #@a7
    if-eqz v6, :cond_66

    #@a9
    .line 5289
    iput-boolean v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mPermanentReject:Z

    #@ab
    .line 5290
    const-string v6, "[GsmServiceStateTracker]"

    #@ad
    const-string v7, "[CHOSH] Authentication failure, show No service "

    #@af
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b2
    goto :goto_66

    #@b3
    .line 5295
    :sswitch_b3
    const v6, 0x209013b

    #@b6
    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@b9
    move-result-object v0

    #@ba
    .line 5296
    sget-boolean v6, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bAlreadyShownRejectCause:Z

    #@bc
    if-nez v6, :cond_c1

    #@be
    .line 5297
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->showDialog(Ljava/lang/String;)V

    #@c1
    .line 5299
    :cond_c1
    const-string v6, "US"

    #@c3
    const-string v7, "TMO"

    #@c5
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@c8
    move-result v6

    #@c9
    if-eqz v6, :cond_66

    #@cb
    .line 5300
    iput-boolean v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mPermanentReject:Z

    #@cd
    .line 5301
    const-string v6, "[GsmServiceStateTracker]"

    #@cf
    const-string v7, "[CHOSH] Authentication failure, show No service "

    #@d1
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    goto :goto_66

    #@d5
    .line 5306
    :sswitch_d5
    const v6, 0x209013c

    #@d8
    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@db
    move-result-object v0

    #@dc
    .line 5307
    sget-boolean v6, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bAlreadyShownRejectCause:Z

    #@de
    if-nez v6, :cond_e3

    #@e0
    .line 5308
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->showDialog(Ljava/lang/String;)V

    #@e3
    .line 5310
    :cond_e3
    const-string v6, "US"

    #@e5
    const-string v7, "TMO"

    #@e7
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@ea
    move-result v6

    #@eb
    if-eqz v6, :cond_66

    #@ed
    .line 5311
    iput-boolean v9, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mPermanentReject:Z

    #@ef
    .line 5312
    const-string v6, "[GsmServiceStateTracker]"

    #@f1
    const-string v7, "[CHOSH] Authentication failure, show No service "

    #@f3
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f6
    goto/16 :goto_66

    #@f8
    .line 5326
    :sswitch_f8
    const-string v6, "US"

    #@fa
    const-string v7, "TMO"

    #@fc
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@ff
    move-result v6

    #@100
    if-nez v6, :cond_66

    #@102
    .line 5328
    const/4 v1, 0x0

    #@103
    .line 5329
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@106
    move-result-object v6

    #@107
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@10a
    move-result-object v7

    #@10b
    invoke-direct {p0, v1, v5, v6, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->handleLimitedService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    #@10e
    goto/16 :goto_66

    #@110
    .line 5338
    :sswitch_110
    const-string v6, "US"

    #@112
    const-string v7, "TMO"

    #@114
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@117
    move-result v6

    #@118
    if-nez v6, :cond_66

    #@11a
    .line 5340
    const/4 v1, 0x0

    #@11b
    .line 5341
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@11e
    move-result-object v6

    #@11f
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@122
    move-result-object v7

    #@123
    invoke-direct {p0, v1, v5, v6, v7}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->handleLimitedService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    #@126
    goto/16 :goto_66

    #@128
    .line 5266
    :sswitch_data_128
    .sparse-switch
        0x2 -> :sswitch_91
        0x3 -> :sswitch_b3
        0x6 -> :sswitch_d5
        0xb -> :sswitch_f8
        0xc -> :sswitch_f8
        0xd -> :sswitch_f8
        0xf -> :sswitch_f8
        0x11 -> :sswitch_110
        0xff -> :sswitch_6f
    .end sparse-switch
.end method

.method public handleNetworkRejectionForVIV(I)V
    .registers 10
    .parameter "rejCode"

    #@0
    .prologue
    const v4, 0x2090128

    #@3
    const/4 v7, 0x1

    #@4
    const/4 v6, 0x0

    #@5
    .line 1876
    const/4 v2, 0x0

    #@6
    .line 1877
    .local v2, spn:Ljava/lang/String;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .line 1878
    .local v1, plmn:Ljava/lang/String;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    .line 1880
    .local v0, msg:Ljava/lang/String;
    const-string v3, "[GsmServiceStateTracker]"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "handleNetworkRejection : Rejection code :"

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    const-string v5, ",prev_rej : "

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    sget v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->prev_rej:I

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    const-string v5, ", Shown :"

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    sget-boolean v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bAlreadyShownRejectCause:Z

    #@43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 1882
    sget v3, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->prev_rej:I

    #@50
    if-eq v3, p1, :cond_56

    #@52
    .line 1884
    sput-boolean v6, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bAlreadyShownRejectCause:Z

    #@54
    .line 1885
    sput p1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->prev_rej:I

    #@56
    .line 1888
    :cond_56
    packed-switch p1, :pswitch_data_70

    #@59
    .line 1905
    :cond_59
    :goto_59
    :pswitch_59
    return-void

    #@5a
    .line 1892
    :pswitch_5a
    sget-boolean v3, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bAlreadyShownRejectCause:Z

    #@5c
    if-nez v3, :cond_59

    #@5e
    .line 1893
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->showDialog(Ljava/lang/String;)V

    #@61
    .line 1894
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@64
    move-result-object v3

    #@65
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@68
    move-result-object v4

    #@69
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->handleLimitedService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    #@6c
    .line 1896
    iput-boolean v7, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSimIsInvalid:Z

    #@6e
    goto :goto_59

    #@6f
    .line 1888
    nop

    #@70
    :pswitch_data_70
    .packed-switch 0x2
        :pswitch_5a
        :pswitch_5a
        :pswitch_59
        :pswitch_59
        :pswitch_5a
    .end packed-switch
.end method

.method protected handlePollStateResult(ILandroid/os/AsyncResult;)V
    .registers 38
    .parameter "what"
    .parameter "ar"

    #@0
    .prologue
    .line 1994
    move-object/from16 v0, p2

    #@2
    iget-object v3, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@4
    move-object/from16 v0, p0

    #@6
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@8
    move-object/from16 v33, v0

    #@a
    move-object/from16 v0, v33

    #@c
    if-eq v3, v0, :cond_f

    #@e
    .line 2549
    :cond_e
    :goto_e
    return-void

    #@f
    .line 1996
    :cond_f
    move-object/from16 v0, p2

    #@11
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@13
    if-eqz v3, :cond_16c

    #@15
    .line 1997
    const/4 v13, 0x0

    #@16
    .line 1999
    .local v13, err:Lcom/android/internal/telephony/CommandException$Error;
    move-object/from16 v0, p2

    #@18
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1a
    instance-of v3, v3, Lcom/android/internal/telephony/CommandException;

    #@1c
    if-eqz v3, :cond_2a

    #@1e
    .line 2000
    move-object/from16 v0, p2

    #@20
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@22
    check-cast v3, Lcom/android/internal/telephony/CommandException;

    #@24
    check-cast v3, Lcom/android/internal/telephony/CommandException;

    #@26
    invoke-virtual {v3}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    #@29
    move-result-object v13

    #@2a
    .line 2003
    :cond_2a
    sget-object v3, Lcom/android/internal/telephony/CommandException$Error;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    #@2c
    if-ne v13, v3, :cond_32

    #@2e
    .line 2005
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cancelPollState()V

    #@31
    goto :goto_e

    #@32
    .line 2009
    :cond_32
    move-object/from16 v0, p0

    #@34
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@36
    invoke-interface {v3}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@3d
    move-result v3

    #@3e
    if-nez v3, :cond_44

    #@40
    .line 2011
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cancelPollState()V

    #@43
    goto :goto_e

    #@44
    .line 2015
    :cond_44
    sget-object v3, Lcom/android/internal/telephony/CommandException$Error;->OP_NOT_ALLOWED_BEFORE_REG_NW:Lcom/android/internal/telephony/CommandException$Error;

    #@46
    if-eq v13, v3, :cond_6a

    #@48
    .line 2016
    new-instance v3, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v33, "RIL implementation has returned an error where it must succeed"

    #@4f
    move-object/from16 v0, v33

    #@51
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    move-object/from16 v0, p2

    #@57
    iget-object v0, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@59
    move-object/from16 v33, v0

    #@5b
    move-object/from16 v0, v33

    #@5d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v3

    #@61
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    move-object/from16 v0, p0

    #@67
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@6a
    .line 2433
    .end local v13           #err:Lcom/android/internal/telephony/CommandException$Error;
    :cond_6a
    :goto_6a
    move-object/from16 v0, p0

    #@6c
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@6e
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@75
    move-result-object v3

    #@76
    const v33, 0x1110030

    #@79
    move/from16 v0, v33

    #@7b
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@7e
    move-result v32

    #@7f
    .line 2436
    .local v32, voice_capable:Z
    if-nez v32, :cond_96

    #@81
    move-object/from16 v0, p0

    #@83
    iget v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@85
    if-nez v3, :cond_96

    #@87
    .line 2437
    move-object/from16 v0, p0

    #@89
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@8b
    move-object/from16 v0, p0

    #@8d
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@8f
    move/from16 v33, v0

    #@91
    move/from16 v0, v33

    #@93
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setState(I)V

    #@96
    .line 2440
    :cond_96
    move-object/from16 v0, p0

    #@98
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@9a
    const/16 v33, 0x0

    #@9c
    aget v34, v3, v33

    #@9e
    add-int/lit8 v34, v34, -0x1

    #@a0
    aput v34, v3, v33

    #@a2
    .line 2442
    move-object/from16 v0, p0

    #@a4
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@a6
    const/16 v33, 0x0

    #@a8
    aget v3, v3, v33

    #@aa
    if-nez v3, :cond_e

    #@ac
    .line 2445
    const-string v3, "JP"

    #@ae
    const-string v33, "DCM"

    #@b0
    move-object/from16 v0, v33

    #@b2
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@b5
    move-result v3

    #@b6
    if-eqz v3, :cond_a01

    #@b8
    .line 2446
    move-object/from16 v0, p0

    #@ba
    iget v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->state_of_suppressing_not_registered_ind:I

    #@bc
    if-nez v3, :cond_9a6

    #@be
    move-object/from16 v0, p0

    #@c0
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@c2
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I

    #@c5
    move-result v3

    #@c6
    if-nez v3, :cond_9a6

    #@c8
    move-object/from16 v0, p0

    #@ca
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@cc
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I

    #@cf
    move-result v3

    #@d0
    if-eqz v3, :cond_9a6

    #@d2
    .line 2448
    const-string v3, "NONET_SUPP - No network is reported. Start suppressing."

    #@d4
    move-object/from16 v0, p0

    #@d6
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@d9
    .line 2450
    const/4 v3, 0x1

    #@da
    move-object/from16 v0, p0

    #@dc
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->state_of_suppressing_not_registered_ind:I

    #@de
    .line 2451
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->createStopSuppressingAlarm()V

    #@e1
    .line 2459
    :cond_e1
    :goto_e1
    move-object/from16 v0, p0

    #@e3
    iget v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->state_of_suppressing_not_registered_ind:I

    #@e5
    const/16 v33, 0x1

    #@e7
    move/from16 v0, v33

    #@e9
    if-eq v3, v0, :cond_e

    #@eb
    .line 2469
    move-object/from16 v0, p0

    #@ed
    iget-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmRoaming:Z

    #@ef
    if-nez v3, :cond_f7

    #@f1
    move-object/from16 v0, p0

    #@f3
    iget-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mDataRoaming:Z

    #@f5
    if-eqz v3, :cond_9cb

    #@f7
    :cond_f7
    const/16 v28, 0x1

    #@f9
    .line 2470
    .local v28, roaming:Z
    :goto_f9
    move-object/from16 v0, p0

    #@fb
    iget-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmRoaming:Z

    #@fd
    if-eqz v3, :cond_115

    #@ff
    move-object/from16 v0, p0

    #@101
    iget-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmRoaming:Z

    #@103
    move-object/from16 v0, p0

    #@105
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@107
    move-object/from16 v33, v0

    #@109
    move-object/from16 v0, p0

    #@10b
    move-object/from16 v1, v33

    #@10d
    invoke-direct {v0, v3, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isRoamingBetweenOperators(ZLandroid/telephony/ServiceState;)Z

    #@110
    move-result v3

    #@111
    if-nez v3, :cond_115

    #@113
    .line 2471
    const/16 v28, 0x0

    #@115
    .line 2473
    :cond_115
    move-object/from16 v0, p0

    #@117
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@119
    move/from16 v0, v28

    #@11b
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setRoaming(Z)V

    #@11e
    .line 2474
    move-object/from16 v0, p0

    #@120
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@122
    move-object/from16 v0, p0

    #@124
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@126
    move/from16 v33, v0

    #@128
    move/from16 v0, v33

    #@12a
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setEmergencyOnly(Z)V

    #@12d
    .line 2476
    move-object/from16 v0, p0

    #@12f
    iget v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@131
    const/16 v33, 0xe

    #@133
    move/from16 v0, v33

    #@135
    if-ne v3, v0, :cond_9e8

    #@137
    .line 2477
    move-object/from16 v0, p0

    #@139
    iget v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLAC:I

    #@13b
    const/16 v33, -0x1

    #@13d
    move/from16 v0, v33

    #@13f
    if-ne v3, v0, :cond_9cf

    #@141
    .line 2478
    move-object/from16 v0, p0

    #@143
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@145
    move-object/from16 v0, p0

    #@147
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTAC:I

    #@149
    move/from16 v33, v0

    #@14b
    move-object/from16 v0, p0

    #@14d
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCID:I

    #@14f
    move/from16 v34, v0

    #@151
    move/from16 v0, v33

    #@153
    move/from16 v1, v34

    #@155
    invoke-virtual {v3, v0, v1}, Landroid/telephony/gsm/GsmCellLocation;->setLacAndCid(II)V

    #@158
    .line 2487
    :goto_158
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->pollStateDone()V

    #@15b
    .line 2489
    move-object/from16 v0, p0

    #@15d
    iget v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->state_of_suppressing_not_registered_ind:I

    #@15f
    const/16 v33, 0x2

    #@161
    move/from16 v0, v33

    #@163
    if-ne v3, v0, :cond_e

    #@165
    .line 2490
    const/4 v3, 0x0

    #@166
    move-object/from16 v0, p0

    #@168
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->state_of_suppressing_not_registered_ind:I

    #@16a
    goto/16 :goto_e

    #@16c
    .line 2020
    .end local v28           #roaming:Z
    .end local v32           #voice_capable:Z
    :cond_16c
    sparse-switch p1, :sswitch_data_b1c

    #@16f
    goto/16 :goto_6a

    #@171
    .line 2022
    :sswitch_171
    :try_start_171
    move-object/from16 v0, p2

    #@173
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@175
    check-cast v3, [Ljava/lang/String;

    #@177
    move-object v0, v3

    #@178
    check-cast v0, [Ljava/lang/String;

    #@17a
    move-object/from16 v30, v0

    #@17c
    .line 2027
    .local v30, states:[Ljava/lang/String;
    const/16 v26, -0x1

    #@17e
    .line 2028
    .local v26, regState:I
    const/16 v25, -0x1

    #@180
    .line 2029
    .local v25, reasonRegStateDenied:I
    const/16 v24, -0x1

    #@182
    .line 2031
    .local v24, psc:I
    const/4 v9, 0x0

    #@183
    .line 2033
    .local v9, CStype:I
    move-object/from16 v0, v30

    #@185
    array-length v3, v0
    :try_end_186
    .catch Ljava/lang/RuntimeException; {:try_start_171 .. :try_end_186} :catch_425

    #@186
    if-lez v3, :cond_257

    #@188
    .line 2035
    const/4 v3, 0x0

    #@189
    :try_start_189
    aget-object v3, v30, v3

    #@18b
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@18e
    move-result v26

    #@18f
    .line 2036
    move-object/from16 v0, v30

    #@191
    array-length v3, v0

    #@192
    const/16 v33, 0x3

    #@194
    move/from16 v0, v33

    #@196
    if-lt v3, v0, :cond_1d2

    #@198
    .line 2037
    const/4 v3, 0x1

    #@199
    aget-object v3, v30, v3

    #@19b
    if-eqz v3, :cond_1b5

    #@19d
    const/4 v3, 0x1

    #@19e
    aget-object v3, v30, v3

    #@1a0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@1a3
    move-result v3

    #@1a4
    if-lez v3, :cond_1b5

    #@1a6
    .line 2040
    const/4 v3, 0x1

    #@1a7
    aget-object v3, v30, v3

    #@1a9
    const/16 v33, 0x10

    #@1ab
    move/from16 v0, v33

    #@1ad
    invoke-static {v3, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@1b0
    move-result v3

    #@1b1
    move-object/from16 v0, p0

    #@1b3
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLAC:I

    #@1b5
    .line 2043
    :cond_1b5
    const/4 v3, 0x2

    #@1b6
    aget-object v3, v30, v3

    #@1b8
    if-eqz v3, :cond_1d2

    #@1ba
    const/4 v3, 0x2

    #@1bb
    aget-object v3, v30, v3

    #@1bd
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@1c0
    move-result v3

    #@1c1
    if-lez v3, :cond_1d2

    #@1c3
    .line 2046
    const/4 v3, 0x2

    #@1c4
    aget-object v3, v30, v3

    #@1c6
    const/16 v33, 0x10

    #@1c8
    move/from16 v0, v33

    #@1ca
    invoke-static {v3, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@1cd
    move-result v3

    #@1ce
    move-object/from16 v0, p0

    #@1d0
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCID:I

    #@1d2
    .line 2051
    :cond_1d2
    const-string v3, "JP"

    #@1d4
    const-string v33, "DCM"

    #@1d6
    move-object/from16 v0, v33

    #@1d8
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1db
    move-result v3

    #@1dc
    if-eqz v3, :cond_232

    #@1de
    .line 2052
    move-object/from16 v0, v30

    #@1e0
    array-length v3, v0

    #@1e1
    const/16 v33, 0x4

    #@1e3
    move/from16 v0, v33

    #@1e5
    if-lt v3, v0, :cond_1f3

    #@1e7
    const/4 v3, 0x3

    #@1e8
    aget-object v3, v30, v3

    #@1ea
    if-eqz v3, :cond_1f3

    #@1ec
    .line 2053
    const/4 v3, 0x3

    #@1ed
    aget-object v3, v30, v3

    #@1ef
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1f2
    move-result v9

    #@1f3
    .line 2056
    :cond_1f3
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f8
    const-string v33, "EVENT_POLL_STATE_REGISTRATION. mNewRadioTechnology = "

    #@1fa
    move-object/from16 v0, v33

    #@1fc
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ff
    move-result-object v3

    #@200
    move-object/from16 v0, p0

    #@202
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@204
    move/from16 v33, v0

    #@206
    move/from16 v0, v33

    #@208
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20b
    move-result-object v3

    #@20c
    const-string v33, ", CStype = "

    #@20e
    move-object/from16 v0, v33

    #@210
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@213
    move-result-object v3

    #@214
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@217
    move-result-object v3

    #@218
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21b
    move-result-object v3

    #@21c
    move-object/from16 v0, p0

    #@21e
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@221
    .line 2058
    move-object/from16 v0, p0

    #@223
    iget v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@225
    if-nez v3, :cond_232

    #@227
    .line 2059
    move-object/from16 v0, p0

    #@229
    iput v9, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@22b
    .line 2060
    move-object/from16 v0, p0

    #@22d
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@22f
    invoke-virtual {v3, v9}, Landroid/telephony/ServiceState;->setRadioTechnology(I)V

    #@232
    .line 2065
    :cond_232
    move-object/from16 v0, v30

    #@234
    array-length v3, v0

    #@235
    const/16 v33, 0xe

    #@237
    move/from16 v0, v33

    #@239
    if-le v3, v0, :cond_257

    #@23b
    .line 2066
    const/16 v3, 0xe

    #@23d
    aget-object v3, v30, v3

    #@23f
    if-eqz v3, :cond_257

    #@241
    const/16 v3, 0xe

    #@243
    aget-object v3, v30, v3

    #@245
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@248
    move-result v3

    #@249
    if-lez v3, :cond_257

    #@24b
    .line 2067
    const/16 v3, 0xe

    #@24d
    aget-object v3, v30, v3

    #@24f
    const/16 v33, 0x10

    #@251
    move/from16 v0, v33

    #@253
    invoke-static {v3, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_256
    .catch Ljava/lang/NumberFormatException; {:try_start_189 .. :try_end_256} :catch_442
    .catch Ljava/lang/RuntimeException; {:try_start_189 .. :try_end_256} :catch_425

    #@256
    move-result v24

    #@257
    .line 2075
    :cond_257
    :goto_257
    :try_start_257
    move-object/from16 v0, p0

    #@259
    move/from16 v1, v26

    #@25b
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->regCodeIsRoaming(I)Z

    #@25e
    move-result v3

    #@25f
    move-object/from16 v0, p0

    #@261
    iput-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmRoaming:Z

    #@263
    .line 2076
    move-object/from16 v0, p0

    #@265
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@267
    move-object/from16 v0, p0

    #@269
    move/from16 v1, v26

    #@26b
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->regCodeToServiceState(I)I

    #@26e
    move-result v33

    #@26f
    move/from16 v0, v33

    #@271
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setState(I)V

    #@274
    .line 2078
    move-object/from16 v0, p0

    #@276
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@278
    move-object/from16 v0, p0

    #@27a
    move/from16 v1, v26

    #@27c
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->regCodesIsSearching(I)Z

    #@27f
    move-result v33

    #@280
    move/from16 v0, v33

    #@282
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setVoiceSearching(Z)V

    #@285
    .line 2084
    const-string v3, "BR"

    #@287
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@28a
    move-result v3

    #@28b
    if-nez v3, :cond_2a5

    #@28d
    const-string v3, "MX"

    #@28f
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@292
    move-result v3

    #@293
    if-nez v3, :cond_2a5

    #@295
    const-string v3, "SCA"

    #@297
    const-string v33, "ro.build.target_region"

    #@299
    invoke-static/range {v33 .. v33}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@29c
    move-result-object v33

    #@29d
    move-object/from16 v0, v33

    #@29f
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a2
    move-result v3

    #@2a3
    if-eqz v3, :cond_2e6

    #@2a5
    .line 2086
    :cond_2a5
    const/4 v3, 0x3

    #@2a6
    move/from16 v0, v26

    #@2a8
    if-eq v0, v3, :cond_2b0

    #@2aa
    const/16 v3, 0xd

    #@2ac
    move/from16 v0, v26

    #@2ae
    if-ne v0, v3, :cond_2e6

    #@2b0
    :cond_2b0
    move-object/from16 v0, v30

    #@2b2
    array-length v3, v0

    #@2b3
    const/16 v33, 0xe

    #@2b5
    move/from16 v0, v33

    #@2b7
    if-lt v3, v0, :cond_2e6

    #@2b9
    .line 2087
    const/16 v3, 0xd

    #@2bb
    aget-object v3, v30, v3

    #@2bd
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2c0
    move-result v27

    #@2c1
    .line 2089
    .local v27, rejCode:I
    const-string v3, "[GsmServiceStateTracker]"

    #@2c3
    new-instance v33, Ljava/lang/StringBuilder;

    #@2c5
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@2c8
    const-string v34, "[PollStateResult_VIV] Registration denied rejcode : "

    #@2ca
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cd
    move-result-object v33

    #@2ce
    move-object/from16 v0, v33

    #@2d0
    move/from16 v1, v27

    #@2d2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d5
    move-result-object v33

    #@2d6
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d9
    move-result-object v33

    #@2da
    move-object/from16 v0, v33

    #@2dc
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2df
    .line 2091
    move-object/from16 v0, p0

    #@2e1
    move/from16 v1, v27

    #@2e3
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->handleNetworkRejectionForVIV(I)V

    #@2e6
    .line 2098
    .end local v27           #rejCode:I
    :cond_2e6
    const/4 v3, 0x3

    #@2e7
    move/from16 v0, v26

    #@2e9
    if-eq v0, v3, :cond_2f1

    #@2eb
    const/16 v3, 0xd

    #@2ed
    move/from16 v0, v26

    #@2ef
    if-ne v0, v3, :cond_358

    #@2f1
    :cond_2f1
    move-object/from16 v0, v30

    #@2f3
    array-length v3, v0
    :try_end_2f4
    .catch Ljava/lang/RuntimeException; {:try_start_257 .. :try_end_2f4} :catch_425

    #@2f4
    const/16 v33, 0xe

    #@2f6
    move/from16 v0, v33

    #@2f8
    if-lt v3, v0, :cond_358

    #@2fa
    .line 2100
    const/16 v3, 0xd

    #@2fc
    :try_start_2fc
    aget-object v3, v30, v3

    #@2fe
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@301
    move-result v27

    #@302
    .line 2102
    .restart local v27       #rejCode:I
    const-string v3, "ATT"

    #@304
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@307
    move-result v3

    #@308
    if-nez v3, :cond_316

    #@30a
    const-string v3, "US"

    #@30c
    const-string v33, "TMO"

    #@30e
    move-object/from16 v0, v33

    #@310
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@313
    move-result v3

    #@314
    if-eqz v3, :cond_339

    #@316
    .line 2103
    :cond_316
    new-instance v3, Ljava/lang/StringBuilder;

    #@318
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31b
    const-string v33, "Rejec Cause = "

    #@31d
    move-object/from16 v0, v33

    #@31f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@322
    move-result-object v3

    #@323
    move/from16 v0, v27

    #@325
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@328
    move-result-object v3

    #@329
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32c
    move-result-object v3

    #@32d
    move-object/from16 v0, p0

    #@32f
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@332
    .line 2104
    move-object/from16 v0, p0

    #@334
    move/from16 v1, v27

    #@336
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->handleNetworkRejection(I)V

    #@339
    .line 2108
    :cond_339
    const/16 v3, 0xa

    #@33b
    move/from16 v0, v27

    #@33d
    if-ne v0, v3, :cond_358

    #@33f
    .line 2109
    const-string v3, " Posting Managed roaming intent "

    #@341
    move-object/from16 v0, p0

    #@343
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@346
    .line 2110
    new-instance v15, Landroid/content/Intent;

    #@348
    const-string v3, "qualcomm.intent.action.ACTION_MANAGED_ROAMING_IND"

    #@34a
    invoke-direct {v15, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@34d
    .line 2112
    .local v15, intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@34f
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@351
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@354
    move-result-object v3

    #@355
    invoke-virtual {v3, v15}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_358
    .catch Ljava/lang/NumberFormatException; {:try_start_2fc .. :try_end_358} :catch_45f
    .catch Ljava/lang/RuntimeException; {:try_start_2fc .. :try_end_358} :catch_425

    #@358
    .line 2120
    .end local v15           #intent:Landroid/content/Intent;
    .end local v27           #rejCode:I
    :cond_358
    :goto_358
    const/4 v3, 0x3

    #@359
    move/from16 v0, v26

    #@35b
    if-eq v0, v3, :cond_369

    #@35d
    const/16 v3, 0xc

    #@35f
    move/from16 v0, v26

    #@361
    if-eq v0, v3, :cond_369

    #@363
    const/16 v3, 0xd

    #@365
    move/from16 v0, v26

    #@367
    if-ne v0, v3, :cond_3fd

    #@369
    :cond_369
    :try_start_369
    move-object/from16 v0, v30

    #@36b
    array-length v3, v0
    :try_end_36c
    .catch Ljava/lang/RuntimeException; {:try_start_369 .. :try_end_36c} :catch_425

    #@36c
    const/16 v33, 0xe

    #@36e
    move/from16 v0, v33

    #@370
    if-lt v3, v0, :cond_3fd

    #@372
    .line 2122
    const/16 v3, 0xd

    #@374
    :try_start_374
    aget-object v3, v30, v3

    #@376
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@379
    move-result v27

    #@37a
    .line 2124
    .restart local v27       #rejCode:I
    const-string v3, "GSM"

    #@37c
    new-instance v33, Ljava/lang/StringBuilder;

    #@37e
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@381
    const-string v34, "[lge_gsmsst_dbg] - 3, 12, 13 Registration denied rejcode : "

    #@383
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@386
    move-result-object v33

    #@387
    move-object/from16 v0, v33

    #@389
    move/from16 v1, v27

    #@38b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38e
    move-result-object v33

    #@38f
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@392
    move-result-object v33

    #@393
    move-object/from16 v0, v33

    #@395
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@398
    .line 2128
    const-string v3, "BR"

    #@39a
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@39d
    move-result v3

    #@39e
    if-nez v3, :cond_3b8

    #@3a0
    const-string v3, "MX"

    #@3a2
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@3a5
    move-result v3

    #@3a6
    if-nez v3, :cond_3b8

    #@3a8
    const-string v3, "SCA"

    #@3aa
    const-string v33, "ro.build.target_region"

    #@3ac
    invoke-static/range {v33 .. v33}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3af
    move-result-object v33

    #@3b0
    move-object/from16 v0, v33

    #@3b2
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b5
    move-result v3

    #@3b6
    if-eqz v3, :cond_3fd

    #@3b8
    :cond_3b8
    const/4 v3, 0x2

    #@3b9
    move/from16 v0, v27

    #@3bb
    if-eq v0, v3, :cond_3c7

    #@3bd
    const/4 v3, 0x3

    #@3be
    move/from16 v0, v27

    #@3c0
    if-eq v0, v3, :cond_3c7

    #@3c2
    const/4 v3, 0x6

    #@3c3
    move/from16 v0, v27

    #@3c5
    if-ne v0, v3, :cond_3fd

    #@3c7
    .line 2130
    :cond_3c7
    const/4 v3, 0x1

    #@3c8
    move-object/from16 v0, p0

    #@3ca
    iput-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSimIsInvalid:Z

    #@3cc
    .line 2131
    const-string v3, "[GsmServiceStateTracker]"

    #@3ce
    new-instance v33, Ljava/lang/StringBuilder;

    #@3d0
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@3d3
    const-string v34, "rejCode : "

    #@3d5
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d8
    move-result-object v33

    #@3d9
    move-object/from16 v0, v33

    #@3db
    move/from16 v1, v27

    #@3dd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e0
    move-result-object v33

    #@3e1
    const-string v34, " mSimIsInvalid : "

    #@3e3
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e6
    move-result-object v33

    #@3e7
    move-object/from16 v0, p0

    #@3e9
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSimIsInvalid:Z

    #@3eb
    move/from16 v34, v0

    #@3ed
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3f0
    move-result-object v33

    #@3f1
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f4
    move-result-object v33

    #@3f5
    move-object/from16 v0, v33

    #@3f7
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3fa
    .line 2132
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V
    :try_end_3fd
    .catch Ljava/lang/NumberFormatException; {:try_start_374 .. :try_end_3fd} :catch_47c
    .catch Ljava/lang/RuntimeException; {:try_start_374 .. :try_end_3fd} :catch_425

    #@3fd
    .line 2141
    .end local v27           #rejCode:I
    :cond_3fd
    :goto_3fd
    const/16 v3, 0xa

    #@3ff
    move/from16 v0, v26

    #@401
    if-eq v0, v3, :cond_415

    #@403
    const/16 v3, 0xc

    #@405
    move/from16 v0, v26

    #@407
    if-eq v0, v3, :cond_415

    #@409
    const/16 v3, 0xd

    #@40b
    move/from16 v0, v26

    #@40d
    if-eq v0, v3, :cond_415

    #@40f
    const/16 v3, 0xe

    #@411
    move/from16 v0, v26

    #@413
    if-ne v0, v3, :cond_49b

    #@415
    .line 2142
    :cond_415
    const/4 v3, 0x1

    #@416
    :try_start_416
    move-object/from16 v0, p0

    #@418
    iput-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@41a
    .line 2168
    :goto_41a
    move-object/from16 v0, p0

    #@41c
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@41e
    move/from16 v0, v24

    #@420
    invoke-virtual {v3, v0}, Landroid/telephony/gsm/GsmCellLocation;->setPsc(I)V
    :try_end_423
    .catch Ljava/lang/RuntimeException; {:try_start_416 .. :try_end_423} :catch_425

    #@423
    goto/16 :goto_6a

    #@425
    .line 2429
    .end local v9           #CStype:I
    .end local v24           #psc:I
    .end local v25           #reasonRegStateDenied:I
    .end local v26           #regState:I
    .end local v30           #states:[Ljava/lang/String;
    :catch_425
    move-exception v14

    #@426
    .line 2430
    .local v14, ex:Ljava/lang/RuntimeException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@428
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@42b
    const-string v33, "Exception while polling service state. Probably malformed RIL response."

    #@42d
    move-object/from16 v0, v33

    #@42f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@432
    move-result-object v3

    #@433
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@436
    move-result-object v3

    #@437
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43a
    move-result-object v3

    #@43b
    move-object/from16 v0, p0

    #@43d
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@440
    goto/16 :goto_6a

    #@442
    .line 2070
    .end local v14           #ex:Ljava/lang/RuntimeException;
    .restart local v9       #CStype:I
    .restart local v24       #psc:I
    .restart local v25       #reasonRegStateDenied:I
    .restart local v26       #regState:I
    .restart local v30       #states:[Ljava/lang/String;
    :catch_442
    move-exception v14

    #@443
    .line 2071
    .local v14, ex:Ljava/lang/NumberFormatException;
    :try_start_443
    new-instance v3, Ljava/lang/StringBuilder;

    #@445
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@448
    const-string v33, "error parsing RegistrationState: "

    #@44a
    move-object/from16 v0, v33

    #@44c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44f
    move-result-object v3

    #@450
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@453
    move-result-object v3

    #@454
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@457
    move-result-object v3

    #@458
    move-object/from16 v0, p0

    #@45a
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@45d
    goto/16 :goto_257

    #@45f
    .line 2114
    .end local v14           #ex:Ljava/lang/NumberFormatException;
    :catch_45f
    move-exception v14

    #@460
    .line 2115
    .restart local v14       #ex:Ljava/lang/NumberFormatException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@462
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@465
    const-string v33, "error parsing regCode: "

    #@467
    move-object/from16 v0, v33

    #@469
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46c
    move-result-object v3

    #@46d
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@470
    move-result-object v3

    #@471
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@474
    move-result-object v3

    #@475
    move-object/from16 v0, p0

    #@477
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@47a
    goto/16 :goto_358

    #@47c
    .line 2135
    .end local v14           #ex:Ljava/lang/NumberFormatException;
    :catch_47c
    move-exception v14

    #@47d
    .line 2136
    .restart local v14       #ex:Ljava/lang/NumberFormatException;
    const-string v3, "GSM"

    #@47f
    new-instance v33, Ljava/lang/StringBuilder;

    #@481
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@484
    const-string v34, "error parsing regCode: "

    #@486
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@489
    move-result-object v33

    #@48a
    move-object/from16 v0, v33

    #@48c
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48f
    move-result-object v33

    #@490
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@493
    move-result-object v33

    #@494
    move-object/from16 v0, v33

    #@496
    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@499
    goto/16 :goto_3fd

    #@49b
    .line 2146
    .end local v14           #ex:Ljava/lang/NumberFormatException;
    :cond_49b
    const-string v3, "KR"

    #@49d
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@4a0
    move-result v3

    #@4a1
    if-eqz v3, :cond_4af

    #@4a3
    const/4 v3, 0x3

    #@4a4
    move/from16 v0, v26

    #@4a6
    if-ne v0, v3, :cond_4af

    #@4a8
    .line 2147
    const/4 v3, 0x1

    #@4a9
    move-object/from16 v0, p0

    #@4ab
    iput-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@4ad
    goto/16 :goto_41a

    #@4af
    .line 2151
    :cond_4af
    const-string v3, "US"

    #@4b1
    const-string v33, "TMO"

    #@4b3
    move-object/from16 v0, v33

    #@4b5
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@4b8
    move-result v3

    #@4b9
    if-eqz v3, :cond_4c7

    #@4bb
    const/4 v3, 0x3

    #@4bc
    move/from16 v0, v26

    #@4be
    if-ne v0, v3, :cond_4c7

    #@4c0
    .line 2152
    const/4 v3, 0x1

    #@4c1
    move-object/from16 v0, p0

    #@4c3
    iput-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@4c5
    goto/16 :goto_41a

    #@4c7
    .line 2155
    :cond_4c7
    const-string v3, "AU"

    #@4c9
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@4cc
    move-result v3

    #@4cd
    if-eqz v3, :cond_4db

    #@4cf
    const/4 v3, 0x3

    #@4d0
    move/from16 v0, v26

    #@4d2
    if-ne v0, v3, :cond_4db

    #@4d4
    .line 2156
    const/4 v3, 0x1

    #@4d5
    move-object/from16 v0, p0

    #@4d7
    iput-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@4d9
    goto/16 :goto_41a

    #@4db
    .line 2159
    :cond_4db
    const/4 v3, 0x0

    #@4dc
    move-object/from16 v0, p0

    #@4de
    iput-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@4e0
    goto/16 :goto_41a

    #@4e2
    .line 2172
    .end local v9           #CStype:I
    .end local v24           #psc:I
    .end local v25           #reasonRegStateDenied:I
    .end local v26           #regState:I
    .end local v30           #states:[Ljava/lang/String;
    :sswitch_4e2
    move-object/from16 v0, p2

    #@4e4
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4e6
    check-cast v3, [Ljava/lang/String;

    #@4e8
    move-object v0, v3

    #@4e9
    check-cast v0, [Ljava/lang/String;

    #@4eb
    move-object/from16 v30, v0

    #@4ed
    .line 2175
    .restart local v30       #states:[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    #@4ef
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4f2
    const-string v33, "handlePollStateResult: EVENT_POLL_STATE_GPRS states.length="

    #@4f4
    move-object/from16 v0, v33

    #@4f6
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f9
    move-result-object v3

    #@4fa
    move-object/from16 v0, v30

    #@4fc
    array-length v0, v0

    #@4fd
    move/from16 v33, v0

    #@4ff
    move/from16 v0, v33

    #@501
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@504
    move-result-object v3

    #@505
    const-string v33, " states="

    #@507
    move-object/from16 v0, v33

    #@509
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50c
    move-result-object v3

    #@50d
    move-object/from16 v0, v30

    #@50f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@512
    move-result-object v3

    #@513
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@516
    move-result-object v3

    #@517
    move-object/from16 v0, p0

    #@519
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@51c
    .line 2179
    const/16 v31, 0x0

    #@51e
    .line 2180
    .local v31, type:I
    const/16 v26, -0x1

    #@520
    .line 2181
    .restart local v26       #regState:I
    const/4 v3, -0x1

    #@521
    move-object/from16 v0, p0

    #@523
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewReasonDataDenied:I

    #@525
    .line 2182
    const/4 v3, 0x1

    #@526
    move-object/from16 v0, p0

    #@528
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewMaxDataCalls:I

    #@52a
    .line 2183
    move-object/from16 v0, v30

    #@52c
    array-length v3, v0
    :try_end_52d
    .catch Ljava/lang/RuntimeException; {:try_start_443 .. :try_end_52d} :catch_425

    #@52d
    if-lez v3, :cond_701

    #@52f
    .line 2185
    const/4 v3, 0x0

    #@530
    :try_start_530
    aget-object v3, v30, v3

    #@532
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@535
    move-result v26

    #@536
    .line 2188
    move-object/from16 v0, v30

    #@538
    array-length v3, v0

    #@539
    const/16 v33, 0x3

    #@53b
    move/from16 v0, v33

    #@53d
    if-lt v3, v0, :cond_55c

    #@53f
    const/4 v3, 0x2

    #@540
    aget-object v3, v30, v3

    #@542
    if-eqz v3, :cond_55c

    #@544
    const/4 v3, 0x2

    #@545
    aget-object v3, v30, v3

    #@547
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@54a
    move-result v3

    #@54b
    if-lez v3, :cond_55c

    #@54d
    .line 2189
    const/4 v3, 0x2

    #@54e
    aget-object v3, v30, v3

    #@550
    const/16 v33, 0x10

    #@552
    move/from16 v0, v33

    #@554
    invoke-static {v3, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@557
    move-result v3

    #@558
    move-object/from16 v0, p0

    #@55a
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCID:I

    #@55c
    .line 2194
    :cond_55c
    move-object/from16 v0, v30

    #@55e
    array-length v3, v0

    #@55f
    const/16 v33, 0x4

    #@561
    move/from16 v0, v33

    #@563
    if-lt v3, v0, :cond_571

    #@565
    const/4 v3, 0x3

    #@566
    aget-object v3, v30, v3

    #@568
    if-eqz v3, :cond_571

    #@56a
    .line 2195
    const/4 v3, 0x3

    #@56b
    aget-object v3, v30, v3

    #@56d
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@570
    move-result v31

    #@571
    .line 2197
    :cond_571
    move-object/from16 v0, v30

    #@573
    array-length v3, v0

    #@574
    const/16 v33, 0x5

    #@576
    move/from16 v0, v33

    #@578
    if-lt v3, v0, :cond_58a

    #@57a
    const/4 v3, 0x3

    #@57b
    move/from16 v0, v26

    #@57d
    if-ne v0, v3, :cond_58a

    #@57f
    .line 2198
    const/4 v3, 0x4

    #@580
    aget-object v3, v30, v3

    #@582
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@585
    move-result v3

    #@586
    move-object/from16 v0, p0

    #@588
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewReasonDataDenied:I

    #@58a
    .line 2202
    :cond_58a
    move-object/from16 v0, v30

    #@58c
    array-length v3, v0

    #@58d
    const/16 v33, 0x5

    #@58f
    move/from16 v0, v33

    #@591
    if-lt v3, v0, :cond_645

    #@593
    const/4 v3, 0x3

    #@594
    move/from16 v0, v26

    #@596
    if-eq v0, v3, :cond_5a4

    #@598
    const/16 v3, 0xc

    #@59a
    move/from16 v0, v26

    #@59c
    if-eq v0, v3, :cond_5a4

    #@59e
    const/16 v3, 0xd

    #@5a0
    move/from16 v0, v26

    #@5a2
    if-ne v0, v3, :cond_645

    #@5a4
    .line 2203
    :cond_5a4
    const/4 v3, 0x4

    #@5a5
    aget-object v3, v30, v3

    #@5a7
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5aa
    move-result v3

    #@5ab
    move-object/from16 v0, p0

    #@5ad
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewReasonDataDenied:I

    #@5af
    .line 2205
    const-string v3, "GSM"

    #@5b1
    new-instance v33, Ljava/lang/StringBuilder;

    #@5b3
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@5b6
    const-string v34, "[lge_gsmsst_dbg] - 3, 12, 13 mNewReasonDataDenied : "

    #@5b8
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5bb
    move-result-object v33

    #@5bc
    move-object/from16 v0, p0

    #@5be
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewReasonDataDenied:I

    #@5c0
    move/from16 v34, v0

    #@5c2
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c5
    move-result-object v33

    #@5c6
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c9
    move-result-object v33

    #@5ca
    move-object/from16 v0, v33

    #@5cc
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5cf
    .line 2208
    const-string v3, "BR"

    #@5d1
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@5d4
    move-result v3

    #@5d5
    if-nez v3, :cond_5ef

    #@5d7
    const-string v3, "MX"

    #@5d9
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@5dc
    move-result v3

    #@5dd
    if-nez v3, :cond_5ef

    #@5df
    const-string v3, "SCA"

    #@5e1
    const-string v33, "ro.build.target_region"

    #@5e3
    invoke-static/range {v33 .. v33}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5e6
    move-result-object v33

    #@5e7
    move-object/from16 v0, v33

    #@5e9
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5ec
    move-result v3

    #@5ed
    if-eqz v3, :cond_645

    #@5ef
    :cond_5ef
    move-object/from16 v0, p0

    #@5f1
    iget-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSimIsInvalid:Z

    #@5f3
    const/16 v33, 0x1

    #@5f5
    move/from16 v0, v33

    #@5f7
    if-eq v3, v0, :cond_645

    #@5f9
    move-object/from16 v0, p0

    #@5fb
    iget v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewReasonDataDenied:I

    #@5fd
    const/16 v33, 0x3

    #@5ff
    move/from16 v0, v33

    #@601
    if-eq v3, v0, :cond_60d

    #@603
    move-object/from16 v0, p0

    #@605
    iget v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewReasonDataDenied:I

    #@607
    const/16 v33, 0x6

    #@609
    move/from16 v0, v33

    #@60b
    if-ne v3, v0, :cond_645

    #@60d
    .line 2210
    :cond_60d
    const/4 v3, 0x1

    #@60e
    move-object/from16 v0, p0

    #@610
    iput-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSimIsInvalid:Z

    #@612
    .line 2211
    const-string v3, "[GsmServiceStateTracker]"

    #@614
    new-instance v33, Ljava/lang/StringBuilder;

    #@616
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@619
    const-string v34, "mNewReasonDataDenied : "

    #@61b
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61e
    move-result-object v33

    #@61f
    move-object/from16 v0, p0

    #@621
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewReasonDataDenied:I

    #@623
    move/from16 v34, v0

    #@625
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@628
    move-result-object v33

    #@629
    const-string v34, " mSimIsInvalid : "

    #@62b
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62e
    move-result-object v33

    #@62f
    move-object/from16 v0, p0

    #@631
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSimIsInvalid:Z

    #@633
    move/from16 v34, v0

    #@635
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@638
    move-result-object v33

    #@639
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63c
    move-result-object v33

    #@63d
    move-object/from16 v0, v33

    #@63f
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@642
    .line 2212
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplay()V

    #@645
    .line 2218
    :cond_645
    move-object/from16 v0, v30

    #@647
    array-length v3, v0

    #@648
    const/16 v33, 0x6

    #@64a
    move/from16 v0, v33

    #@64c
    if-lt v3, v0, :cond_659

    #@64e
    .line 2219
    const/4 v3, 0x5

    #@64f
    aget-object v3, v30, v3

    #@651
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@654
    move-result v3

    #@655
    move-object/from16 v0, p0

    #@657
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewMaxDataCalls:I

    #@659
    .line 2222
    :cond_659
    move-object/from16 v0, v30

    #@65b
    array-length v3, v0

    #@65c
    const/16 v33, 0x7

    #@65e
    move/from16 v0, v33

    #@660
    if-lt v3, v0, :cond_67b

    #@662
    const/4 v3, 0x6

    #@663
    aget-object v3, v30, v3

    #@665
    if-eqz v3, :cond_67b

    #@667
    const/4 v3, 0x6

    #@668
    aget-object v3, v30, v3

    #@66a
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@66d
    move-result v3

    #@66e
    if-lez v3, :cond_67b

    #@670
    .line 2223
    const/4 v3, 0x6

    #@671
    aget-object v3, v30, v3

    #@673
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@676
    move-result v3

    #@677
    move-object/from16 v0, p0

    #@679
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTAC:I
    :try_end_67b
    .catch Ljava/lang/NumberFormatException; {:try_start_530 .. :try_end_67b} :catch_7c8
    .catch Ljava/lang/RuntimeException; {:try_start_530 .. :try_end_67b} :catch_425

    #@67b
    .line 2230
    :cond_67b
    :goto_67b
    :try_start_67b
    move-object/from16 v0, v30

    #@67d
    array-length v3, v0
    :try_end_67e
    .catch Ljava/lang/RuntimeException; {:try_start_67b .. :try_end_67e} :catch_425

    #@67e
    const/16 v33, 0xa

    #@680
    move/from16 v0, v33

    #@682
    if-lt v3, v0, :cond_701

    #@684
    .line 2237
    const/16 v20, 0x0

    #@686
    .line 2240
    .local v20, operatorNumeric:Ljava/lang/String;
    :try_start_686
    move-object/from16 v0, p0

    #@688
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@68a
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@68d
    move-result-object v20

    #@68e
    .line 2241
    const/4 v3, 0x0

    #@68f
    const/16 v33, 0x3

    #@691
    move-object/from16 v0, v20

    #@693
    move/from16 v1, v33

    #@695
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@698
    move-result-object v3

    #@699
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_69c
    .catch Ljava/lang/Exception; {:try_start_686 .. :try_end_69c} :catch_7e5
    .catch Ljava/lang/RuntimeException; {:try_start_686 .. :try_end_69c} :catch_425

    #@69c
    move-result v4

    #@69d
    .line 2254
    .local v4, mcc:I
    :goto_69d
    const/4 v3, 0x3

    #@69e
    :try_start_69e
    move-object/from16 v0, v20

    #@6a0
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@6a3
    move-result-object v3

    #@6a4
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_6a7
    .catch Ljava/lang/Exception; {:try_start_69e .. :try_end_6a7} :catch_82f
    .catch Ljava/lang/RuntimeException; {:try_start_69e .. :try_end_6a7} :catch_425

    #@6a7
    move-result v5

    #@6a8
    .line 2264
    .local v5, mnc:I
    :goto_6a8
    const/4 v3, 0x6

    #@6a9
    :try_start_6a9
    aget-object v3, v30, v3

    #@6ab
    invoke-static {v3}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@6ae
    move-result-object v3

    #@6af
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_6b2
    .catch Ljava/lang/Exception; {:try_start_6a9 .. :try_end_6b2} :catch_85d
    .catch Ljava/lang/RuntimeException; {:try_start_6a9 .. :try_end_6b2} :catch_425

    #@6b2
    move-result v8

    #@6b3
    .line 2271
    .local v8, tac:I
    :goto_6b3
    const/4 v3, 0x7

    #@6b4
    :try_start_6b4
    aget-object v3, v30, v3

    #@6b6
    invoke-static {v3}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@6b9
    move-result-object v3

    #@6ba
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_6bd
    .catch Ljava/lang/Exception; {:try_start_6b4 .. :try_end_6bd} :catch_88f
    .catch Ljava/lang/RuntimeException; {:try_start_6b4 .. :try_end_6bd} :catch_425

    #@6bd
    move-result v7

    #@6be
    .line 2278
    .local v7, pci:I
    :goto_6be
    const/16 v3, 0x8

    #@6c0
    :try_start_6c0
    aget-object v3, v30, v3

    #@6c2
    invoke-static {v3}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@6c5
    move-result-object v3

    #@6c6
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_6c9
    .catch Ljava/lang/Exception; {:try_start_6c0 .. :try_end_6c9} :catch_8c1
    .catch Ljava/lang/RuntimeException; {:try_start_6c0 .. :try_end_6c9} :catch_425

    #@6c9
    move-result v6

    #@6ca
    .line 2285
    .local v6, eci:I
    :goto_6ca
    const/16 v3, 0x9

    #@6cc
    :try_start_6cc
    aget-object v3, v30, v3

    #@6ce
    invoke-static {v3}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@6d1
    move-result-object v3

    #@6d2
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_6d5
    .catch Ljava/lang/Exception; {:try_start_6cc .. :try_end_6d5} :catch_8f3
    .catch Ljava/lang/RuntimeException; {:try_start_6cc .. :try_end_6d5} :catch_425

    #@6d5
    move-result v11

    #@6d6
    .line 2292
    .local v11, csgid:I
    :goto_6d6
    :try_start_6d6
    new-instance v3, Landroid/telephony/CellIdentityLte;

    #@6d8
    invoke-direct/range {v3 .. v8}, Landroid/telephony/CellIdentityLte;-><init>(IIIII)V

    #@6db
    move-object/from16 v0, p0

    #@6dd
    iput-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@6df
    .line 2294
    new-instance v3, Ljava/lang/StringBuilder;

    #@6e1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6e4
    const-string v33, "handlePollStateResultMessage: mNewLteCellIdentity="

    #@6e6
    move-object/from16 v0, v33

    #@6e8
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6eb
    move-result-object v3

    #@6ec
    move-object/from16 v0, p0

    #@6ee
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@6f0
    move-object/from16 v33, v0

    #@6f2
    move-object/from16 v0, v33

    #@6f4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f7
    move-result-object v3

    #@6f8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6fb
    move-result-object v3

    #@6fc
    move-object/from16 v0, p0

    #@6fe
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@701
    .line 2300
    .end local v4           #mcc:I
    .end local v5           #mnc:I
    .end local v6           #eci:I
    .end local v7           #pci:I
    .end local v8           #tac:I
    .end local v11           #csgid:I
    .end local v20           #operatorNumeric:Ljava/lang/String;
    :cond_701
    move-object/from16 v0, p0

    #@703
    move/from16 v1, v26

    #@705
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->regCodeToServiceState(I)I

    #@708
    move-result v3

    #@709
    move-object/from16 v0, p0

    #@70b
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@70d
    .line 2301
    move-object/from16 v0, p0

    #@70f
    move/from16 v1, v26

    #@711
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->regCodeIsRoaming(I)Z

    #@714
    move-result v3

    #@715
    move-object/from16 v0, p0

    #@717
    iput-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mDataRoaming:Z

    #@719
    .line 2303
    move-object/from16 v0, p0

    #@71b
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@71d
    move-object/from16 v0, p0

    #@71f
    move/from16 v1, v26

    #@721
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->regCodesIsSearching(I)Z

    #@724
    move-result v33

    #@725
    move/from16 v0, v33

    #@727
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setDataSearching(Z)V

    #@72a
    .line 2305
    move/from16 v0, v31

    #@72c
    move-object/from16 v1, p0

    #@72e
    iput v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@730
    .line 2306
    move-object/from16 v0, p0

    #@732
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@734
    move/from16 v0, v31

    #@736
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setRadioTechnology(I)V

    #@739
    .line 2307
    move-object/from16 v0, p0

    #@73b
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@73d
    move-object/from16 v0, p0

    #@73f
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@741
    move/from16 v33, v0

    #@743
    move/from16 v0, v33

    #@745
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setDataState(I)V

    #@748
    .line 2309
    const-string v3, "KR"

    #@74a
    const-string v33, "KT"

    #@74c
    move-object/from16 v0, v33

    #@74e
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@751
    move-result v3

    #@752
    if-eqz v3, :cond_6a

    #@754
    move-object/from16 v0, p0

    #@756
    iget v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newGPRSState:I

    #@758
    if-nez v3, :cond_6a

    #@75a
    .line 2310
    const-string v21, ""

    #@75c
    .line 2311
    .local v21, ota_is_running:Ljava/lang/String;
    move-object/from16 v0, p0

    #@75e
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@760
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@763
    move-result-object v3

    #@764
    invoke-static {v3}, Lcom/android/internal/telephony/TelephonyUtils;->isKTOTAMode(Landroid/content/Context;)Z
    :try_end_767
    .catch Ljava/lang/RuntimeException; {:try_start_6d6 .. :try_end_767} :catch_425

    #@767
    move-result v22

    #@768
    .line 2314
    .local v22, ota_mode:Z
    :try_start_768
    const-string v3, "gsm.lge.ota_is_running"

    #@76a
    const-string v33, "unknown"

    #@76c
    move-object/from16 v0, v33

    #@76e
    invoke-static {v3, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_771
    .catch Ljava/lang/IllegalArgumentException; {:try_start_768 .. :try_end_771} :catch_b19
    .catch Ljava/lang/RuntimeException; {:try_start_768 .. :try_end_771} :catch_425

    #@771
    move-result-object v21

    #@772
    .line 2318
    :goto_772
    :try_start_772
    new-instance v3, Ljava/lang/StringBuilder;

    #@774
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@777
    const-string v33, "ota_is_running : "

    #@779
    move-object/from16 v0, v33

    #@77b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77e
    move-result-object v3

    #@77f
    move-object/from16 v0, v21

    #@781
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@784
    move-result-object v3

    #@785
    const-string v33, ", ota_mode : "

    #@787
    move-object/from16 v0, v33

    #@789
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78c
    move-result-object v3

    #@78d
    move/from16 v0, v22

    #@78f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@792
    move-result-object v3

    #@793
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@796
    move-result-object v3

    #@797
    move-object/from16 v0, p0

    #@799
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@79c
    .line 2319
    const-string v3, "false"

    #@79e
    move-object/from16 v0, v21

    #@7a0
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7a3
    move-result v3

    #@7a4
    if-eqz v3, :cond_6a

    #@7a6
    if-eqz v22, :cond_6a

    #@7a8
    .line 2320
    new-instance v10, Landroid/content/Intent;

    #@7aa
    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    #@7ad
    .line 2321
    .local v10, KTota:Landroid/content/Intent;
    const-string v3, "com.lge.ota"

    #@7af
    const-string v33, "com.lge.ota.KTNoUSIMActivityForLockScreen"

    #@7b1
    move-object/from16 v0, v33

    #@7b3
    invoke-virtual {v10, v3, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@7b6
    .line 2322
    const/high16 v3, 0x1000

    #@7b8
    invoke-virtual {v10, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@7bb
    .line 2323
    move-object/from16 v0, p0

    #@7bd
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@7bf
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@7c2
    move-result-object v3

    #@7c3
    invoke-virtual {v3, v10}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@7c6
    goto/16 :goto_6a

    #@7c8
    .line 2226
    .end local v10           #KTota:Landroid/content/Intent;
    .end local v21           #ota_is_running:Ljava/lang/String;
    .end local v22           #ota_mode:Z
    :catch_7c8
    move-exception v14

    #@7c9
    .line 2227
    .restart local v14       #ex:Ljava/lang/NumberFormatException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@7cb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7ce
    const-string v33, "error parsing GprsRegistrationState: "

    #@7d0
    move-object/from16 v0, v33

    #@7d2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d5
    move-result-object v3

    #@7d6
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d9
    move-result-object v3

    #@7da
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7dd
    move-result-object v3

    #@7de
    move-object/from16 v0, p0

    #@7e0
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V
    :try_end_7e3
    .catch Ljava/lang/RuntimeException; {:try_start_772 .. :try_end_7e3} :catch_425

    #@7e3
    goto/16 :goto_67b

    #@7e5
    .line 2242
    .end local v14           #ex:Ljava/lang/NumberFormatException;
    .restart local v20       #operatorNumeric:Ljava/lang/String;
    :catch_7e5
    move-exception v12

    #@7e6
    .line 2244
    .local v12, e:Ljava/lang/Exception;
    :try_start_7e6
    move-object/from16 v0, p0

    #@7e8
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@7ea
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@7ed
    move-result-object v20

    #@7ee
    .line 2245
    const/4 v3, 0x0

    #@7ef
    const/16 v33, 0x3

    #@7f1
    move-object/from16 v0, v20

    #@7f3
    move/from16 v1, v33

    #@7f5
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7f8
    move-result-object v3

    #@7f9
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_7fc
    .catch Ljava/lang/Exception; {:try_start_7e6 .. :try_end_7fc} :catch_7ff
    .catch Ljava/lang/RuntimeException; {:try_start_7e6 .. :try_end_7fc} :catch_425

    #@7fc
    move-result v4

    #@7fd
    .restart local v4       #mcc:I
    goto/16 :goto_69d

    #@7ff
    .line 2246
    .end local v4           #mcc:I
    :catch_7ff
    move-exception v14

    #@800
    .line 2247
    .local v14, ex:Ljava/lang/Exception;
    :try_start_800
    new-instance v3, Ljava/lang/StringBuilder;

    #@802
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@805
    const-string v33, "handlePollStateResultMessage: bad mcc operatorNumeric="

    #@807
    move-object/from16 v0, v33

    #@809
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80c
    move-result-object v3

    #@80d
    move-object/from16 v0, v20

    #@80f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@812
    move-result-object v3

    #@813
    const-string v33, " ex="

    #@815
    move-object/from16 v0, v33

    #@817
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81a
    move-result-object v3

    #@81b
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@81e
    move-result-object v3

    #@81f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@822
    move-result-object v3

    #@823
    move-object/from16 v0, p0

    #@825
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@828
    .line 2249
    const-string v20, ""

    #@82a
    .line 2250
    const v4, 0x7fffffff

    #@82d
    .restart local v4       #mcc:I
    goto/16 :goto_69d

    #@82f
    .line 2255
    .end local v12           #e:Ljava/lang/Exception;
    .end local v14           #ex:Ljava/lang/Exception;
    :catch_82f
    move-exception v12

    #@830
    .line 2256
    .restart local v12       #e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    #@832
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@835
    const-string v33, "handlePollStateResultMessage: bad mnc operatorNumeric="

    #@837
    move-object/from16 v0, v33

    #@839
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83c
    move-result-object v3

    #@83d
    move-object/from16 v0, v20

    #@83f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@842
    move-result-object v3

    #@843
    const-string v33, " e="

    #@845
    move-object/from16 v0, v33

    #@847
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84a
    move-result-object v3

    #@84b
    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@84e
    move-result-object v3

    #@84f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@852
    move-result-object v3

    #@853
    move-object/from16 v0, p0

    #@855
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@858
    .line 2258
    const v5, 0x7fffffff

    #@85b
    .restart local v5       #mnc:I
    goto/16 :goto_6a8

    #@85d
    .line 2265
    .end local v12           #e:Ljava/lang/Exception;
    :catch_85d
    move-exception v12

    #@85e
    .line 2266
    .restart local v12       #e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    #@860
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@863
    const-string v33, "handlePollStateResultMessage: bad tac states[6]="

    #@865
    move-object/from16 v0, v33

    #@867
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86a
    move-result-object v3

    #@86b
    const/16 v33, 0x6

    #@86d
    aget-object v33, v30, v33

    #@86f
    move-object/from16 v0, v33

    #@871
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@874
    move-result-object v3

    #@875
    const-string v33, " e="

    #@877
    move-object/from16 v0, v33

    #@879
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87c
    move-result-object v3

    #@87d
    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@880
    move-result-object v3

    #@881
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@884
    move-result-object v3

    #@885
    move-object/from16 v0, p0

    #@887
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@88a
    .line 2268
    const v8, 0x7fffffff

    #@88d
    .restart local v8       #tac:I
    goto/16 :goto_6b3

    #@88f
    .line 2272
    .end local v12           #e:Ljava/lang/Exception;
    :catch_88f
    move-exception v12

    #@890
    .line 2273
    .restart local v12       #e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    #@892
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@895
    const-string v33, "handlePollStateResultMessage: bad pci states[7]="

    #@897
    move-object/from16 v0, v33

    #@899
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89c
    move-result-object v3

    #@89d
    const/16 v33, 0x7

    #@89f
    aget-object v33, v30, v33

    #@8a1
    move-object/from16 v0, v33

    #@8a3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a6
    move-result-object v3

    #@8a7
    const-string v33, " e="

    #@8a9
    move-object/from16 v0, v33

    #@8ab
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8ae
    move-result-object v3

    #@8af
    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b2
    move-result-object v3

    #@8b3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b6
    move-result-object v3

    #@8b7
    move-object/from16 v0, p0

    #@8b9
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@8bc
    .line 2275
    const v7, 0x7fffffff

    #@8bf
    .restart local v7       #pci:I
    goto/16 :goto_6be

    #@8c1
    .line 2279
    .end local v12           #e:Ljava/lang/Exception;
    :catch_8c1
    move-exception v12

    #@8c2
    .line 2280
    .restart local v12       #e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    #@8c4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8c7
    const-string v33, "handlePollStateResultMessage: bad eci states[8]="

    #@8c9
    move-object/from16 v0, v33

    #@8cb
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8ce
    move-result-object v3

    #@8cf
    const/16 v33, 0x8

    #@8d1
    aget-object v33, v30, v33

    #@8d3
    move-object/from16 v0, v33

    #@8d5
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d8
    move-result-object v3

    #@8d9
    const-string v33, " e="

    #@8db
    move-object/from16 v0, v33

    #@8dd
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e0
    move-result-object v3

    #@8e1
    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8e4
    move-result-object v3

    #@8e5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e8
    move-result-object v3

    #@8e9
    move-object/from16 v0, p0

    #@8eb
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->loge(Ljava/lang/String;)V

    #@8ee
    .line 2282
    const v6, 0x7fffffff

    #@8f1
    .restart local v6       #eci:I
    goto/16 :goto_6ca

    #@8f3
    .line 2286
    .end local v12           #e:Ljava/lang/Exception;
    :catch_8f3
    move-exception v12

    #@8f4
    .line 2290
    .restart local v12       #e:Ljava/lang/Exception;
    const v11, 0x7fffffff

    #@8f7
    .restart local v11       #csgid:I
    goto/16 :goto_6d6

    #@8f9
    .line 2395
    .end local v4           #mcc:I
    .end local v5           #mnc:I
    .end local v6           #eci:I
    .end local v7           #pci:I
    .end local v8           #tac:I
    .end local v11           #csgid:I
    .end local v12           #e:Ljava/lang/Exception;
    .end local v20           #operatorNumeric:Ljava/lang/String;
    .end local v26           #regState:I
    .end local v30           #states:[Ljava/lang/String;
    .end local v31           #type:I
    :sswitch_8f9
    move-object/from16 v0, p2

    #@8fb
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@8fd
    check-cast v3, [Ljava/lang/String;

    #@8ff
    move-object v0, v3

    #@900
    check-cast v0, [Ljava/lang/String;

    #@902
    move-object/from16 v19, v0

    #@904
    .line 2397
    .local v19, opNames:[Ljava/lang/String;
    if-eqz v19, :cond_6a

    #@906
    move-object/from16 v0, v19

    #@908
    array-length v3, v0

    #@909
    const/16 v33, 0x3

    #@90b
    move/from16 v0, v33

    #@90d
    if-lt v3, v0, :cond_6a

    #@90f
    .line 2399
    invoke-static {}, Lcom/android/internal/telephony/PlmnListParser;->getInstance()Lcom/android/internal/telephony/PlmnListParser;

    #@912
    move-result-object v23

    #@913
    .line 2401
    .local v23, plmnParser:Lcom/android/internal/telephony/PlmnListParser;
    const/4 v3, 0x2

    #@914
    aget-object v18, v19, v3

    #@916
    .line 2402
    .local v18, numeric:Ljava/lang/String;
    move-object/from16 v0, v23

    #@918
    move-object/from16 v1, v18

    #@91a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/PlmnListParser;->getLongName(Ljava/lang/String;)Ljava/lang/String;

    #@91d
    move-result-object v17

    #@91e
    .line 2403
    .local v17, longName:Ljava/lang/String;
    move-object/from16 v0, v23

    #@920
    move-object/from16 v1, v18

    #@922
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/PlmnListParser;->getShortName(Ljava/lang/String;)Ljava/lang/String;

    #@925
    move-result-object v29

    #@926
    .line 2404
    .local v29, shortName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@928
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@92a
    const/16 v33, 0x0

    #@92c
    aget-object v33, v19, v33

    #@92e
    invoke-static/range {v33 .. v33}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@931
    move-result v33

    #@932
    if-eqz v33, :cond_951

    #@934
    if-eqz v17, :cond_951

    #@936
    .end local v17           #longName:Ljava/lang/String;
    :goto_936
    const/16 v33, 0x1

    #@938
    aget-object v33, v19, v33

    #@93a
    invoke-static/range {v33 .. v33}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@93d
    move-result v33

    #@93e
    if-eqz v33, :cond_956

    #@940
    if-eqz v29, :cond_956

    #@942
    .end local v29           #shortName:Ljava/lang/String;
    :goto_942
    const/16 v33, 0x2

    #@944
    aget-object v33, v19, v33

    #@946
    move-object/from16 v0, v17

    #@948
    move-object/from16 v1, v29

    #@94a
    move-object/from16 v2, v33

    #@94c
    invoke-virtual {v3, v0, v1, v2}, Landroid/telephony/ServiceState;->setOperatorName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@94f
    goto/16 :goto_6a

    #@951
    .restart local v17       #longName:Ljava/lang/String;
    .restart local v29       #shortName:Ljava/lang/String;
    :cond_951
    const/16 v33, 0x0

    #@953
    aget-object v17, v19, v33

    #@955
    goto :goto_936

    #@956
    .end local v17           #longName:Ljava/lang/String;
    :cond_956
    const/16 v33, 0x1

    #@958
    aget-object v29, v19, v33

    #@95a
    goto :goto_942

    #@95b
    .line 2413
    .end local v18           #numeric:Ljava/lang/String;
    .end local v19           #opNames:[Ljava/lang/String;
    .end local v23           #plmnParser:Lcom/android/internal/telephony/PlmnListParser;
    .end local v29           #shortName:Ljava/lang/String;
    :sswitch_95b
    move-object/from16 v0, p2

    #@95d
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@95f
    check-cast v3, [I

    #@961
    move-object v0, v3

    #@962
    check-cast v0, [I

    #@964
    move-object/from16 v16, v0

    #@966
    .line 2414
    .local v16, ints:[I
    move-object/from16 v0, p0

    #@968
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@96a
    move-object/from16 v33, v0

    #@96c
    const/4 v3, 0x0

    #@96d
    aget v3, v16, v3

    #@96f
    const/16 v34, 0x1

    #@971
    move/from16 v0, v34

    #@973
    if-ne v3, v0, :cond_9a4

    #@975
    const/4 v3, 0x1

    #@976
    :goto_976
    move-object/from16 v0, v33

    #@978
    invoke-virtual {v0, v3}, Landroid/telephony/ServiceState;->setIsManualSelection(Z)V

    #@97b
    .line 2415
    const/4 v3, 0x0

    #@97c
    aget v3, v16, v3

    #@97e
    const/16 v33, 0x1

    #@980
    move/from16 v0, v33

    #@982
    if-ne v3, v0, :cond_6a

    #@984
    move-object/from16 v0, p0

    #@986
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@988
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->isManualNetSelAllowed()Z

    #@98b
    move-result v3

    #@98c
    if-nez v3, :cond_6a

    #@98e
    .line 2421
    move-object/from16 v0, p0

    #@990
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@992
    const/16 v33, 0x0

    #@994
    move-object/from16 v0, v33

    #@996
    invoke-virtual {v3, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->setNetworkSelectionModeAutomatic(Landroid/os/Message;)V

    #@999
    .line 2422
    const-string v3, "GSM"

    #@99b
    const-string v33, " Forcing Automatic Network Selection, manual selection is not allowed"

    #@99d
    move-object/from16 v0, v33

    #@99f
    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9a2
    .catch Ljava/lang/RuntimeException; {:try_start_800 .. :try_end_9a2} :catch_425

    #@9a2
    goto/16 :goto_6a

    #@9a4
    .line 2414
    :cond_9a4
    const/4 v3, 0x0

    #@9a5
    goto :goto_976

    #@9a6
    .line 2452
    .end local v16           #ints:[I
    .restart local v32       #voice_capable:Z
    :cond_9a6
    move-object/from16 v0, p0

    #@9a8
    iget v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->state_of_suppressing_not_registered_ind:I

    #@9aa
    const/16 v33, 0x1

    #@9ac
    move/from16 v0, v33

    #@9ae
    if-ne v3, v0, :cond_e1

    #@9b0
    move-object/from16 v0, p0

    #@9b2
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@9b4
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I

    #@9b7
    move-result v3

    #@9b8
    if-nez v3, :cond_e1

    #@9ba
    .line 2453
    const-string v3, "NONET_SUPP - Registered to a network. Cancel suppressing."

    #@9bc
    move-object/from16 v0, p0

    #@9be
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@9c1
    .line 2455
    const/4 v3, 0x2

    #@9c2
    move-object/from16 v0, p0

    #@9c4
    iput v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->state_of_suppressing_not_registered_ind:I

    #@9c6
    .line 2456
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cancelStopSuppressingAlarm()V

    #@9c9
    goto/16 :goto_e1

    #@9cb
    .line 2469
    :cond_9cb
    const/16 v28, 0x0

    #@9cd
    goto/16 :goto_f9

    #@9cf
    .line 2480
    .restart local v28       #roaming:Z
    :cond_9cf
    move-object/from16 v0, p0

    #@9d1
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@9d3
    move-object/from16 v0, p0

    #@9d5
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLAC:I

    #@9d7
    move/from16 v33, v0

    #@9d9
    move-object/from16 v0, p0

    #@9db
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCID:I

    #@9dd
    move/from16 v34, v0

    #@9df
    move/from16 v0, v33

    #@9e1
    move/from16 v1, v34

    #@9e3
    invoke-virtual {v3, v0, v1}, Landroid/telephony/gsm/GsmCellLocation;->setLacAndCid(II)V

    #@9e6
    goto/16 :goto_158

    #@9e8
    .line 2483
    :cond_9e8
    move-object/from16 v0, p0

    #@9ea
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@9ec
    move-object/from16 v0, p0

    #@9ee
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLAC:I

    #@9f0
    move/from16 v33, v0

    #@9f2
    move-object/from16 v0, p0

    #@9f4
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCID:I

    #@9f6
    move/from16 v34, v0

    #@9f8
    move/from16 v0, v33

    #@9fa
    move/from16 v1, v34

    #@9fc
    invoke-virtual {v3, v0, v1}, Landroid/telephony/gsm/GsmCellLocation;->setLacAndCid(II)V

    #@9ff
    goto/16 :goto_158

    #@a01
    .line 2503
    .end local v28           #roaming:Z
    :cond_a01
    move-object/from16 v0, p0

    #@a03
    iget-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmRoaming:Z

    #@a05
    if-nez v3, :cond_a0d

    #@a07
    move-object/from16 v0, p0

    #@a09
    iget-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mDataRoaming:Z

    #@a0b
    if-eqz v3, :cond_ae5

    #@a0d
    :cond_a0d
    const/16 v28, 0x1

    #@a0f
    .line 2504
    .restart local v28       #roaming:Z
    :goto_a0f
    move-object/from16 v0, p0

    #@a11
    iget-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmRoaming:Z

    #@a13
    if-eqz v3, :cond_a2b

    #@a15
    move-object/from16 v0, p0

    #@a17
    iget-boolean v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmRoaming:Z

    #@a19
    move-object/from16 v0, p0

    #@a1b
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@a1d
    move-object/from16 v33, v0

    #@a1f
    move-object/from16 v0, p0

    #@a21
    move-object/from16 v1, v33

    #@a23
    invoke-direct {v0, v3, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isRoamingBetweenOperators(ZLandroid/telephony/ServiceState;)Z

    #@a26
    move-result v3

    #@a27
    if-nez v3, :cond_a2b

    #@a29
    .line 2505
    const/16 v28, 0x0

    #@a2b
    .line 2508
    :cond_a2b
    const/4 v3, 0x0

    #@a2c
    const-string v33, "KR_RAD_TEST"

    #@a2e
    move-object/from16 v0, v33

    #@a30
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a33
    move-result v3

    #@a34
    if-eqz v3, :cond_a3e

    #@a36
    .line 2509
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isOemRadTestSettingTrue()Z

    #@a39
    move-result v3

    #@a3a
    if-eqz v3, :cond_a3e

    #@a3c
    .line 2512
    const/16 v28, 0x1

    #@a3e
    .line 2518
    :cond_a3e
    const-string v3, "US"

    #@a40
    const-string v33, "ATT"

    #@a42
    move-object/from16 v0, v33

    #@a44
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@a47
    move-result v3

    #@a48
    if-eqz v3, :cond_a54

    #@a4a
    if-eqz v28, :cond_a54

    #@a4c
    .line 2519
    move-object/from16 v0, p0

    #@a4e
    move/from16 v1, v28

    #@a50
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->checkInternationalRoamingForATT(Z)Z

    #@a53
    move-result v28

    #@a54
    .line 2523
    :cond_a54
    move-object/from16 v0, p0

    #@a56
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@a58
    move/from16 v0, v28

    #@a5a
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setRoaming(Z)V

    #@a5d
    .line 2525
    const/4 v3, 0x0

    #@a5e
    const-string v33, "vzw_roaming_state"

    #@a60
    move-object/from16 v0, v33

    #@a62
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a65
    move-result v3

    #@a66
    if-eqz v3, :cond_aa6

    #@a68
    .line 2526
    move-object/from16 v0, p0

    #@a6a
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@a6c
    move-object/from16 v0, p0

    #@a6e
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mGsmRoaming:Z

    #@a70
    move/from16 v33, v0

    #@a72
    move-object/from16 v0, p0

    #@a74
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@a76
    move-object/from16 v34, v0

    #@a78
    move-object/from16 v0, p0

    #@a7a
    move/from16 v1, v33

    #@a7c
    move-object/from16 v2, v34

    #@a7e
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isRoamingBetweenOperators(ZLandroid/telephony/ServiceState;)Z

    #@a81
    move-result v33

    #@a82
    move/from16 v0, v33

    #@a84
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setVoiceRoaming(Z)V

    #@a87
    .line 2527
    move-object/from16 v0, p0

    #@a89
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@a8b
    move-object/from16 v0, p0

    #@a8d
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mDataRoaming:Z

    #@a8f
    move/from16 v33, v0

    #@a91
    move-object/from16 v0, p0

    #@a93
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@a95
    move-object/from16 v34, v0

    #@a97
    move-object/from16 v0, p0

    #@a99
    move/from16 v1, v33

    #@a9b
    move-object/from16 v2, v34

    #@a9d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isRoamingBetweenOperators(ZLandroid/telephony/ServiceState;)Z

    #@aa0
    move-result v33

    #@aa1
    move/from16 v0, v33

    #@aa3
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setDataRoaming(Z)V

    #@aa6
    .line 2530
    :cond_aa6
    move-object/from16 v0, p0

    #@aa8
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@aaa
    move-object/from16 v0, p0

    #@aac
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@aae
    move/from16 v33, v0

    #@ab0
    move/from16 v0, v33

    #@ab2
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setEmergencyOnly(Z)V

    #@ab5
    .line 2533
    move-object/from16 v0, p0

    #@ab7
    iget v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@ab9
    const/16 v33, 0xe

    #@abb
    move/from16 v0, v33

    #@abd
    if-ne v3, v0, :cond_b01

    #@abf
    .line 2534
    move-object/from16 v0, p0

    #@ac1
    iget v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLAC:I

    #@ac3
    const/16 v33, -0x1

    #@ac5
    move/from16 v0, v33

    #@ac7
    if-ne v3, v0, :cond_ae9

    #@ac9
    .line 2535
    move-object/from16 v0, p0

    #@acb
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@acd
    move-object/from16 v0, p0

    #@acf
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mTAC:I

    #@ad1
    move/from16 v33, v0

    #@ad3
    move-object/from16 v0, p0

    #@ad5
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCID:I

    #@ad7
    move/from16 v34, v0

    #@ad9
    move/from16 v0, v33

    #@adb
    move/from16 v1, v34

    #@add
    invoke-virtual {v3, v0, v1}, Landroid/telephony/gsm/GsmCellLocation;->setLacAndCid(II)V

    #@ae0
    .line 2544
    :goto_ae0
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->pollStateDone()V

    #@ae3
    goto/16 :goto_e

    #@ae5
    .line 2503
    .end local v28           #roaming:Z
    :cond_ae5
    const/16 v28, 0x0

    #@ae7
    goto/16 :goto_a0f

    #@ae9
    .line 2537
    .restart local v28       #roaming:Z
    :cond_ae9
    move-object/from16 v0, p0

    #@aeb
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@aed
    move-object/from16 v0, p0

    #@aef
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLAC:I

    #@af1
    move/from16 v33, v0

    #@af3
    move-object/from16 v0, p0

    #@af5
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCID:I

    #@af7
    move/from16 v34, v0

    #@af9
    move/from16 v0, v33

    #@afb
    move/from16 v1, v34

    #@afd
    invoke-virtual {v3, v0, v1}, Landroid/telephony/gsm/GsmCellLocation;->setLacAndCid(II)V

    #@b00
    goto :goto_ae0

    #@b01
    .line 2540
    :cond_b01
    move-object/from16 v0, p0

    #@b03
    iget-object v3, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->newCellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@b05
    move-object/from16 v0, p0

    #@b07
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mLAC:I

    #@b09
    move/from16 v33, v0

    #@b0b
    move-object/from16 v0, p0

    #@b0d
    iget v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCID:I

    #@b0f
    move/from16 v34, v0

    #@b11
    move/from16 v0, v33

    #@b13
    move/from16 v1, v34

    #@b15
    invoke-virtual {v3, v0, v1}, Landroid/telephony/gsm/GsmCellLocation;->setLacAndCid(II)V

    #@b18
    goto :goto_ae0

    #@b19
    .line 2315
    .end local v28           #roaming:Z
    .end local v32           #voice_capable:Z
    .restart local v21       #ota_is_running:Ljava/lang/String;
    .restart local v22       #ota_mode:Z
    .restart local v26       #regState:I
    .restart local v30       #states:[Ljava/lang/String;
    .restart local v31       #type:I
    :catch_b19
    move-exception v3

    #@b1a
    goto/16 :goto_772

    #@b1c
    .line 2020
    :sswitch_data_b1c
    .sparse-switch
        0x4 -> :sswitch_171
        0x5 -> :sswitch_4e2
        0x6 -> :sswitch_8f9
        0xe -> :sswitch_95b
    .end sparse-switch
.end method

.method protected hangupAndPowerOff()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1457
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4
    iget-boolean v0, v0, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@6
    if-nez v0, :cond_13

    #@8
    .line 1458
    const-string v0, "hangupAndPowerOff : hangup is Ignoring."

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@d
    .line 1459
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@f
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setRadioPower(ZLandroid/os/Message;)V

    #@12
    .line 1471
    :goto_12
    return-void

    #@13
    .line 1464
    :cond_13
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@15
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->isInCall()Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_36

    #@1b
    .line 1465
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1d
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@1f
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@21
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->hangupIfAlive()V

    #@24
    .line 1466
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@26
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@28
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2a
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->hangupIfAlive()V

    #@2d
    .line 1467
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2f
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@31
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@33
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->hangupIfAlive()V

    #@36
    .line 1470
    :cond_36
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@38
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setRadioPower(ZLandroid/os/Message;)V

    #@3b
    goto :goto_12
.end method

.method public isConcurrentVoiceAndDataAllowed()Z
    .registers 3

    #@0
    .prologue
    .line 3859
    iget v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@2
    const/4 v1, 0x3

    #@3
    if-lt v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 4382
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[GsmSST] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 4383
    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 4387
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[GsmSST] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 4388
    return-void
.end method

.method protected onSignalStrengthResult(Landroid/os/AsyncResult;Z)Z
    .registers 11
    .parameter "ar"
    .parameter "isGsm"

    #@0
    .prologue
    const/16 v4, 0xe

    #@2
    .line 5380
    iget v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@4
    if-ne v2, v4, :cond_7

    #@6
    .line 5381
    const/4 p2, 0x1

    #@7
    .line 5384
    :cond_7
    invoke-super {p0, p1, p2}, Lcom/android/internal/telephony/ServiceStateTracker;->onSignalStrengthResult(Landroid/os/AsyncResult;Z)Z

    #@a
    move-result v1

    #@b
    .line 5386
    .local v1, ssChanged:Z
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@d
    monitor-enter v3

    #@e
    .line 5387
    :try_start_e
    iget v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@10
    if-ne v2, v4, :cond_32

    #@12
    .line 5388
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@14
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@17
    move-result-wide v4

    #@18
    const-wide/16 v6, 0x3e8

    #@1a
    mul-long/2addr v4, v6

    #@1b
    invoke-virtual {v2, v4, v5}, Landroid/telephony/CellInfoLte;->setTimeStamp(J)V

    #@1e
    .line 5389
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@20
    const/4 v4, 0x4

    #@21
    invoke-virtual {v2, v4}, Landroid/telephony/CellInfoLte;->setTimeStampType(I)V

    #@24
    .line 5390
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@26
    invoke-virtual {v2}, Landroid/telephony/CellInfoLte;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthLte;

    #@29
    move-result-object v2

    #@2a
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@2c
    const v5, 0x7fffffff

    #@2f
    invoke-virtual {v2, v4, v5}, Landroid/telephony/CellSignalStrengthLte;->initialize(Landroid/telephony/SignalStrength;I)V

    #@32
    .line 5393
    :cond_32
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@34
    invoke-virtual {v2}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    #@37
    move-result-object v2

    #@38
    if-eqz v2, :cond_49

    #@3a
    .line 5394
    new-instance v0, Ljava/util/ArrayList;

    #@3c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@3f
    .line 5395
    .local v0, arrayCi:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/CellInfo;>;"
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@41
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@44
    .line 5396
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPhoneBase:Lcom/android/internal/telephony/PhoneBase;

    #@46
    invoke-virtual {v2, v0}, Lcom/android/internal/telephony/PhoneBase;->notifyCellInfo(Ljava/util/List;)V

    #@49
    .line 5398
    .end local v0           #arrayCi:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/CellInfo;>;"
    :cond_49
    monitor-exit v3

    #@4a
    .line 5399
    return v1

    #@4b
    .line 5398
    :catchall_4b
    move-exception v2

    #@4c
    monitor-exit v3
    :try_end_4d
    .catchall {:try_start_e .. :try_end_4d} :catchall_4b

    #@4d
    throw v2
.end method

.method protected onUpdateIccAvailability()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4290
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 4316
    :cond_5
    :goto_5
    return-void

    #@6
    .line 4294
    :cond_6
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getUiccCardApplication()Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    move-result-object v0

    #@a
    .line 4296
    .local v0, newUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c
    if-eq v1, v0, :cond_5

    #@e
    .line 4297
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@10
    if-eqz v1, :cond_29

    #@12
    .line 4298
    const-string v1, "Removing stale icc objects."

    #@14
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@17
    .line 4299
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@19
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForReady(Landroid/os/Handler;)V

    #@1c
    .line 4300
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@1e
    if-eqz v1, :cond_25

    #@20
    .line 4301
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@22
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@25
    .line 4303
    :cond_25
    iput-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@27
    .line 4304
    iput-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@29
    .line 4306
    :cond_29
    if-eqz v0, :cond_5

    #@2b
    .line 4307
    const-string v1, "New card found"

    #@2d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@30
    .line 4308
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@32
    .line 4309
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@34
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@37
    move-result-object v1

    #@38
    iput-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@3a
    .line 4310
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@3c
    const/16 v2, 0x11

    #@3e
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->registerForReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@41
    .line 4311
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@43
    if-eqz v1, :cond_5

    #@45
    .line 4312
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@47
    const/16 v2, 0x10

    #@49
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@4c
    goto :goto_5
.end method

.method public setIMSRegistate(Z)V
    .registers 7
    .parameter "Registate"

    #@0
    .prologue
    .line 4528
    const-string v2, "GSM"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "[IMS_AFW][GsmServiceStateTracker] setIMSRegistate - Registate : "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 4530
    iget-boolean v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->IMSRegiOnOff:Z

    #@1a
    if-eqz v2, :cond_53

    #@1c
    if-nez p1, :cond_53

    #@1e
    .line 4531
    const-string v2, "GSM"

    #@20
    const-string v3, "[GsmServiceStateTracker]IMSRegiOnOff == true && Registate == false"

    #@22
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 4532
    iget-boolean v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->alarmSwitch:Z

    #@27
    const/4 v3, 0x1

    #@28
    if-ne v2, v3, :cond_53

    #@2a
    .line 4533
    const-string v2, "GSM"

    #@2c
    const-string v3, "[GsmServiceStateTracker]alarmSwitch == true"

    #@2e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 4534
    iput-boolean p1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->IMSRegiOnOff:Z

    #@33
    .line 4536
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@35
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@38
    move-result-object v1

    #@39
    .line 4537
    .local v1, mContext:Landroid/content/Context;
    const-string v2, "alarm"

    #@3b
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3e
    move-result-object v0

    #@3f
    check-cast v0, Landroid/app/AlarmManager;

    #@41
    .line 4538
    .local v0, am:Landroid/app/AlarmManager;
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRadioOffIntent:Landroid/app/PendingIntent;

    #@43
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@46
    .line 4539
    const/4 v2, 0x0

    #@47
    iput-boolean v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->alarmSwitch:Z

    #@49
    .line 4541
    const/16 v2, 0x2c

    #@4b
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->sendMessage(Landroid/os/Message;)Z

    #@52
    .line 4546
    .end local v0           #am:Landroid/app/AlarmManager;
    .end local v1           #mContext:Landroid/content/Context;
    :goto_52
    return-void

    #@53
    .line 4545
    :cond_53
    iput-boolean p1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->IMSRegiOnOff:Z

    #@55
    goto :goto_52
.end method

.method public setPdpRejectedNotification(II)V
    .registers 3
    .parameter "notifyType"
    .parameter "mFailCause"

    #@0
    .prologue
    .line 4168
    iput p2, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->failCause:I

    #@2
    .line 4169
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setNotification(I)V

    #@5
    .line 4170
    return-void
.end method

.method protected setPowerStateToDesired()V
    .registers 11

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 1401
    const-string v4, "GSM"

    #@4
    new-instance v5, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v6, "mDesiredPowerState = "

    #@b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v5

    #@f
    iget-boolean v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v5

    #@19
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 1402
    const-string v4, "GSM"

    #@1e
    new-instance v5, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v6, "getRadioState = "

    #@25
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    iget-object v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2b
    invoke-interface {v6}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1403
    const-string v4, "GSM"

    #@3c
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "poweroffdelayneed = "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    iget-boolean v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->poweroffdelayneed:Z

    #@49
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 1404
    const-string v4, "GSM"

    #@56
    new-instance v5, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v6, "alarmSwitch = "

    #@5d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    iget-boolean v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->alarmSwitch:Z

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v5

    #@6b
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 1406
    iget-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->alarmSwitch:Z

    #@70
    if-eqz v4, :cond_8e

    #@72
    .line 1407
    const-string v4, "GSM"

    #@74
    const-string v5, "[GsmServiceStateTracker]alarmSwitch == true"

    #@76
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    .line 1408
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@7b
    invoke-virtual {v4}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@7e
    move-result-object v3

    #@7f
    .line 1409
    .local v3, mContext:Landroid/content/Context;
    const-string v4, "alarm"

    #@81
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@84
    move-result-object v0

    #@85
    check-cast v0, Landroid/app/AlarmManager;

    #@87
    .line 1410
    .local v0, am:Landroid/app/AlarmManager;
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRadioOffIntent:Landroid/app/PendingIntent;

    #@89
    invoke-virtual {v0, v4}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@8c
    .line 1411
    iput-boolean v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->alarmSwitch:Z

    #@8e
    .line 1415
    .end local v0           #am:Landroid/app/AlarmManager;
    .end local v3           #mContext:Landroid/content/Context;
    :cond_8e
    iget-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@90
    if-eqz v4, :cond_a5

    #@92
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@94
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@97
    move-result-object v4

    #@98
    sget-object v5, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_OFF:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@9a
    if-ne v4, v5, :cond_a5

    #@9c
    .line 1417
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@9e
    const/4 v5, 0x0

    #@9f
    invoke-interface {v4, v7, v5}, Lcom/android/internal/telephony/CommandsInterface;->setRadioPower(ZLandroid/os/Message;)V

    #@a2
    .line 1450
    :cond_a2
    :goto_a2
    iput-boolean v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRequestedByPhone:Z

    #@a4
    .line 1452
    return-void

    #@a5
    .line 1418
    :cond_a5
    iget-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@a7
    if-nez v4, :cond_111

    #@a9
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@ab
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@ae
    move-result-object v4

    #@af
    invoke-virtual {v4}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@b2
    move-result v4

    #@b3
    if-eqz v4, :cond_111

    #@b5
    .line 1420
    iget-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->poweroffdelayneed:Z

    #@b7
    if-eqz v4, :cond_109

    #@b9
    .line 1421
    iget-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->IMSRegiOnOff:Z

    #@bb
    if-eqz v4, :cond_101

    #@bd
    iget-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->alarmSwitch:Z

    #@bf
    if-nez v4, :cond_101

    #@c1
    .line 1422
    const-string v4, "GSM"

    #@c3
    const-string v5, "[GsmServiceStateTracker]IMSRegiOnOff == true"

    #@c5
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c8
    .line 1423
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@ca
    invoke-virtual {v4}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@cd
    move-result-object v3

    #@ce
    .line 1424
    .restart local v3       #mContext:Landroid/content/Context;
    const-string v4, "alarm"

    #@d0
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d3
    move-result-object v0

    #@d4
    check-cast v0, Landroid/app/AlarmManager;

    #@d6
    .line 1426
    .restart local v0       #am:Landroid/app/AlarmManager;
    new-instance v2, Landroid/content/Intent;

    #@d8
    const-string v4, "android.intent.action.ACTION_RADIO_OFF"

    #@da
    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@dd
    .line 1427
    .local v2, intent:Landroid/content/Intent;
    invoke-static {v3, v9, v2, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@e0
    move-result-object v4

    #@e1
    iput-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRadioOffIntent:Landroid/app/PendingIntent;

    #@e3
    .line 1429
    iput-boolean v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->alarmSwitch:Z

    #@e5
    .line 1430
    const-string v4, "GSM"

    #@e7
    const-string v5, "[GsmServiceStateTracker]Alarm setting"

    #@e9
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ec
    .line 1431
    const/4 v4, 0x2

    #@ed
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@f0
    move-result-wide v5

    #@f1
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@f3
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@f6
    move-result-object v7

    #@f7
    iget v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@f9
    int-to-long v7, v7

    #@fa
    add-long/2addr v5, v7

    #@fb
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRadioOffIntent:Landroid/app/PendingIntent;

    #@fd
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@100
    goto :goto_a2

    #@101
    .line 1433
    .end local v0           #am:Landroid/app/AlarmManager;
    .end local v2           #intent:Landroid/content/Intent;
    .end local v3           #mContext:Landroid/content/Context;
    :cond_101
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@103
    iget-object v1, v4, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@105
    .line 1434
    .local v1, dcTracker:Lcom/android/internal/telephony/DataConnectionTracker;
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->powerOffRadioSafely(Lcom/android/internal/telephony/DataConnectionTracker;)V

    #@108
    goto :goto_a2

    #@109
    .line 1437
    .end local v1           #dcTracker:Lcom/android/internal/telephony/DataConnectionTracker;
    :cond_109
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@10b
    iget-object v1, v4, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@10d
    .line 1438
    .restart local v1       #dcTracker:Lcom/android/internal/telephony/DataConnectionTracker;
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->powerOffRadioSafely(Lcom/android/internal/telephony/DataConnectionTracker;)V

    #@110
    goto :goto_a2

    #@111
    .line 1443
    .end local v1           #dcTracker:Lcom/android/internal/telephony/DataConnectionTracker;
    :cond_111
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isIPPhoneSupported()Z

    #@114
    move-result v4

    #@115
    if-eqz v4, :cond_a2

    #@117
    .line 1444
    iget-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRequestedByPhone:Z

    #@119
    if-eqz v4, :cond_a2

    #@11b
    iget-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@11d
    if-eqz v4, :cond_a2

    #@11f
    iget-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPrevDesiredPowerState:Z

    #@121
    if-nez v4, :cond_a2

    #@123
    .line 1445
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->isNormalPowerOnReq()Z

    #@126
    move-result v4

    #@127
    if-nez v4, :cond_a2

    #@129
    .line 1446
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@12b
    invoke-static {v4}, Lcom/android/internal/telephony/CallManager;->checkNoCoverageCase(Lcom/android/internal/telephony/CommandsInterface;)V

    #@12e
    goto/16 :goto_a2
.end method

.method public set_internetpdn_ipv6_blocked_by_ip6table(Z)V
    .registers 4
    .parameter "blocked"

    #@0
    .prologue
    .line 4370
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "set_internetpdn_ipv6_blocked_by_ip6table current :"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-boolean v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", requested "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@22
    .line 4371
    sput-boolean p1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@24
    .line 4372
    return-void
.end method

.method public set_internetpdn_ipv6_blocked_iface(Ljava/lang/String;)V
    .registers 4
    .parameter "blocked_iface"

    #@0
    .prologue
    .line 4375
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "set_internetpdn_ipv6_blocked_iface current :"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-object v1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", requested "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@22
    .line 4376
    sput-object p1, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@24
    .line 4377
    return-void
.end method

.method protected updateSpnDisplay()V
    .registers 19

    #@0
    .prologue
    .line 1477
    const-string v14, "KR"

    #@2
    const-string v15, "KT"

    #@4
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v14

    #@8
    if-eqz v14, :cond_e

    #@a
    .line 1478
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplayKT()V

    #@d
    .line 1868
    :cond_d
    :goto_d
    return-void

    #@e
    .line 1480
    :cond_e
    const-string v14, "KR"

    #@10
    const-string v15, "SKT"

    #@12
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@15
    move-result v14

    #@16
    if-eqz v14, :cond_1c

    #@18
    .line 1481
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplaySKT()V

    #@1b
    goto :goto_d

    #@1c
    .line 1483
    :cond_1c
    const-string v14, "KR"

    #@1e
    const-string v15, "LGU"

    #@20
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@23
    move-result v14

    #@24
    if-eqz v14, :cond_2a

    #@26
    .line 1484
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->updateSpnDisplayLGU()V

    #@29
    goto :goto_d

    #@2a
    .line 1507
    :cond_2a
    move-object/from16 v0, p0

    #@2c
    iget-object v7, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@2e
    .line 1508
    .local v7, iccRecords:Lcom/android/internal/telephony/uicc/IccRecords;
    const/4 v9, 0x0

    #@2f
    .line 1509
    .local v9, plmn:Ljava/lang/String;
    const/4 v11, 0x0

    #@30
    .line 1512
    .local v11, showPlmn:Z
    const/4 v1, 0x0

    #@31
    .line 1513
    .local v1, OperatorNumeric:Ljava/lang/String;
    const/4 v3, 0x0

    #@32
    .line 1514
    .local v3, SimNumeric:Ljava/lang/String;
    move-object/from16 v0, p0

    #@34
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@36
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    .line 1515
    if-eqz v7, :cond_40

    #@3c
    .line 1516
    invoke-virtual {v7}, Lcom/android/internal/telephony/uicc/IccRecords;->getOperatorNumeric()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    .line 1518
    :cond_40
    const-string v14, "GSM"

    #@42
    new-instance v15, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v16, " OperatorNumeric Value is  "

    #@49
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v15

    #@4d
    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v15

    #@51
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v15

    #@55
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 1519
    const-string v14, "GSM"

    #@5a
    new-instance v15, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v16, " SimNumeric Value is  "

    #@61
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v15

    #@65
    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v15

    #@69
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v15

    #@6d
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 1520
    const-string v14, "GSM"

    #@72
    new-instance v15, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v16, " ss.getRoaming Value is  "

    #@79
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v15

    #@7d
    move-object/from16 v0, p0

    #@7f
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@81
    move-object/from16 v16, v0

    #@83
    invoke-virtual/range {v16 .. v16}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@86
    move-result v16

    #@87
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v15

    #@8b
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v15

    #@8f
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 1524
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@95
    move-result-object v14

    #@96
    const-string v15, "ATT"

    #@98
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9b
    move-result v14

    #@9c
    if-eqz v14, :cond_123

    #@9e
    .line 1525
    const-string v14, "GSM"

    #@a0
    new-instance v15, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v16, "updateSpnDisplay - mIccRecords = "

    #@a7
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v15

    #@ab
    move-object/from16 v0, p0

    #@ad
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@af
    move-object/from16 v16, v0

    #@b1
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v15

    #@b5
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v15

    #@b9
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    .line 1526
    const-string v14, "GSM"

    #@be
    new-instance v15, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v16, "updateSpnDisplay - isAttImeiLocked = "

    #@c5
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v15

    #@c9
    sget-boolean v16, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isAttImeiLocked:Z

    #@cb
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v15

    #@cf
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v15

    #@d3
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    .line 1528
    sget-boolean v14, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isAttImeiLocked:Z

    #@d8
    const/4 v15, 0x1

    #@d9
    if-ne v14, v15, :cond_123

    #@db
    .line 1530
    const-string v14, "GSM"

    #@dd
    const-string v15, "Display IMEI Lock"

    #@df
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    .line 1531
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@e5
    move-result-object v14

    #@e6
    const v15, 0x1040326

    #@e9
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@ec
    move-result-object v14

    #@ed
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@f0
    move-result-object v9

    #@f1
    .line 1533
    const/4 v13, 0x0

    #@f2
    .line 1534
    .local v13, spn:Ljava/lang/String;
    const/4 v6, 0x0

    #@f3
    .line 1535
    .local v6, att_showSpn:Z
    const/4 v5, 0x1

    #@f4
    .line 1537
    .local v5, att_showPlmn:Z
    new-instance v8, Landroid/content/Intent;

    #@f6
    const-string v14, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@f8
    invoke-direct {v8, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@fb
    .line 1538
    .local v8, intent:Landroid/content/Intent;
    const/high16 v14, 0x2000

    #@fd
    invoke-virtual {v8, v14}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@100
    .line 1539
    const-string v14, "showSpn"

    #@102
    invoke-virtual {v8, v14, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@105
    .line 1540
    const-string v14, "spn"

    #@107
    invoke-virtual {v8, v14, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@10a
    .line 1541
    const-string v14, "showPlmn"

    #@10c
    invoke-virtual {v8, v14, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@10f
    .line 1542
    const-string v14, "plmn"

    #@111
    invoke-virtual {v8, v14, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@114
    .line 1543
    move-object/from16 v0, p0

    #@116
    iget-object v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@118
    invoke-virtual {v14}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@11b
    move-result-object v14

    #@11c
    sget-object v15, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@11e
    invoke-virtual {v14, v8, v15}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@121
    goto/16 :goto_d

    #@123
    .line 1550
    .end local v5           #att_showPlmn:Z
    .end local v6           #att_showSpn:Z
    .end local v8           #intent:Landroid/content/Intent;
    .end local v13           #spn:Ljava/lang/String;
    :cond_123
    const/4 v12, 0x0

    #@124
    .line 1551
    .local v12, showSpn:Z
    if-eqz v7, :cond_196

    #@126
    invoke-virtual {v7}, Lcom/android/internal/telephony/uicc/IccRecords;->getServiceProviderName()Ljava/lang/String;

    #@129
    move-result-object v13

    #@12a
    .line 1552
    .restart local v13       #spn:Ljava/lang/String;
    :goto_12a
    const-string v14, "JP"

    #@12c
    const-string v15, "DCM"

    #@12e
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@131
    move-result v14

    #@132
    if-eqz v14, :cond_199

    #@134
    if-nez v7, :cond_199

    #@136
    .line 1553
    move-object/from16 v0, p0

    #@138
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@13a
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getState()I

    #@13d
    move-result v14

    #@13e
    const/4 v15, 0x1

    #@13f
    if-ne v14, v15, :cond_d

    #@141
    move-object/from16 v0, p0

    #@143
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@145
    invoke-interface {v14}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@148
    move-result-object v14

    #@149
    invoke-virtual {v14}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@14c
    move-result v14

    #@14d
    if-eqz v14, :cond_d

    #@14f
    .line 1554
    const-string v14, "updateSpnDisplay: Sim = No sim, ServiceState = OUT_OF_SERVICE, RadioState = On"

    #@151
    move-object/from16 v0, p0

    #@153
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@156
    .line 1555
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@159
    move-result-object v14

    #@15a
    const v15, 0x104030b

    #@15d
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@160
    move-result-object v14

    #@161
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@164
    move-result-object v9

    #@165
    .line 1557
    const/4 v12, 0x0

    #@166
    .line 1558
    const/4 v11, 0x0

    #@167
    .line 1560
    new-instance v8, Landroid/content/Intent;

    #@169
    const-string v14, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@16b
    invoke-direct {v8, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@16e
    .line 1561
    .restart local v8       #intent:Landroid/content/Intent;
    const/high16 v14, 0x2000

    #@170
    invoke-virtual {v8, v14}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@173
    .line 1562
    const-string v14, "showSpn"

    #@175
    invoke-virtual {v8, v14, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@178
    .line 1563
    const-string v14, "spn"

    #@17a
    invoke-virtual {v8, v14, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@17d
    .line 1564
    const-string v14, "showPlmn"

    #@17f
    invoke-virtual {v8, v14, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@182
    .line 1565
    const-string v14, "plmn"

    #@184
    invoke-virtual {v8, v14, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@187
    .line 1568
    move-object/from16 v0, p0

    #@189
    iget-object v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@18b
    invoke-virtual {v14}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@18e
    move-result-object v14

    #@18f
    sget-object v15, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@191
    invoke-virtual {v14, v8, v15}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@194
    goto/16 :goto_d

    #@196
    .line 1551
    .end local v8           #intent:Landroid/content/Intent;
    .end local v13           #spn:Ljava/lang/String;
    :cond_196
    const-string v13, ""

    #@198
    goto :goto_12a

    #@199
    .line 1574
    .restart local v13       #spn:Ljava/lang/String;
    :cond_199
    if-eqz v7, :cond_567

    #@19b
    move-object/from16 v0, p0

    #@19d
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@19f
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@1a2
    move-result-object v14

    #@1a3
    invoke-virtual {v7, v14}, Lcom/android/internal/telephony/uicc/IccRecords;->getDisplayRule(Ljava/lang/String;)I

    #@1a6
    move-result v10

    #@1a7
    .line 1575
    .local v10, rule:I
    :goto_1a7
    move-object/from16 v0, p0

    #@1a9
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1ab
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getState()I

    #@1ae
    move-result v14

    #@1af
    const/4 v15, 0x1

    #@1b0
    if-eq v14, v15, :cond_1bd

    #@1b2
    move-object/from16 v0, p0

    #@1b4
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1b6
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getState()I

    #@1b9
    move-result v14

    #@1ba
    const/4 v15, 0x2

    #@1bb
    if-ne v14, v15, :cond_644

    #@1bd
    .line 1577
    :cond_1bd
    const/4 v11, 0x1

    #@1be
    .line 1578
    move-object/from16 v0, p0

    #@1c0
    iget-boolean v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@1c2
    if-eqz v14, :cond_57b

    #@1c4
    .line 1581
    const-string v14, "AU"

    #@1c6
    const-string v15, "TEL"

    #@1c8
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1cb
    move-result v14

    #@1cc
    if-eqz v14, :cond_56a

    #@1ce
    move-object/from16 v0, p0

    #@1d0
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1d2
    invoke-interface {v14}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@1d5
    move-result-object v14

    #@1d6
    invoke-virtual {v14}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@1d9
    move-result v14

    #@1da
    if-eqz v14, :cond_56a

    #@1dc
    .line 1582
    new-instance v14, Ljava/lang/StringBuilder;

    #@1de
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@1e1
    const-string v15, "000 "

    #@1e3
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v14

    #@1e7
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@1ea
    move-result-object v15

    #@1eb
    const v16, 0x1040325

    #@1ee
    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@1f1
    move-result-object v15

    #@1f2
    invoke-virtual {v15}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1f5
    move-result-object v15

    #@1f6
    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@1f9
    move-result-object v15

    #@1fa
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fd
    move-result-object v14

    #@1fe
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@201
    move-result-object v9

    #@202
    .line 1591
    :goto_202
    const-string v14, "VZW"

    #@204
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@207
    move-result v14

    #@208
    if-eqz v14, :cond_252

    #@20a
    if-nez v7, :cond_252

    #@20c
    .line 1592
    move-object/from16 v0, p0

    #@20e
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@210
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getState()I

    #@213
    move-result v14

    #@214
    const/4 v15, 0x1

    #@215
    if-ne v14, v15, :cond_252

    #@217
    move-object/from16 v0, p0

    #@219
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@21b
    invoke-interface {v14}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@21e
    move-result-object v14

    #@21f
    invoke-virtual {v14}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@222
    move-result v14

    #@223
    if-eqz v14, :cond_252

    #@225
    .line 1593
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@228
    move-result-object v14

    #@229
    const v15, 0x104030b

    #@22c
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@22f
    move-result-object v14

    #@230
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@233
    move-result-object v9

    #@234
    .line 1595
    new-instance v14, Ljava/lang/StringBuilder;

    #@236
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@239
    const-string v15, "updateSpnDisplay: Sim = No sim, ServiceState = OUT_OF_SERVICE, RadioState = On, set plmn=\'"

    #@23b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23e
    move-result-object v14

    #@23f
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@242
    move-result-object v14

    #@243
    const-string v15, "\'"

    #@245
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@248
    move-result-object v14

    #@249
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24c
    move-result-object v14

    #@24d
    move-object/from16 v0, p0

    #@24f
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@252
    .line 1618
    :cond_252
    :goto_252
    const-string v14, "ATT"

    #@254
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@257
    move-result v14

    #@258
    if-eqz v14, :cond_5d2

    #@25a
    .line 1619
    move-object/from16 v0, p0

    #@25c
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@25e
    invoke-interface {v14}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@261
    move-result-object v14

    #@262
    invoke-virtual {v14}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@265
    move-result v14

    #@266
    if-eqz v14, :cond_26a

    #@268
    .line 1620
    const-string v9, ""

    #@26a
    .line 1638
    :cond_26a
    :goto_26a
    new-instance v14, Ljava/lang/StringBuilder;

    #@26c
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@26f
    const-string v15, "updateSpnDisplay: radio is on but out of service, set plmn=\'"

    #@271
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@274
    move-result-object v14

    #@275
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@278
    move-result-object v14

    #@279
    const-string v15, "\'"

    #@27b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27e
    move-result-object v14

    #@27f
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@282
    move-result-object v14

    #@283
    move-object/from16 v0, p0

    #@285
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@288
    .line 1658
    :goto_288
    move-object/from16 v0, p0

    #@28a
    iget v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isNetworkLoked:I

    #@28c
    packed-switch v14, :pswitch_data_848

    #@28f
    .line 1685
    :goto_28f
    const-string v14, "BR"

    #@291
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@294
    move-result v14

    #@295
    if-nez v14, :cond_2ad

    #@297
    const-string v14, "MX"

    #@299
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@29c
    move-result v14

    #@29d
    if-nez v14, :cond_2ad

    #@29f
    const-string v14, "SCA"

    #@2a1
    const-string v15, "ro.build.target_region"

    #@2a3
    invoke-static {v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2a6
    move-result-object v15

    #@2a7
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2aa
    move-result v14

    #@2ab
    if-eqz v14, :cond_2d2

    #@2ad
    :cond_2ad
    move-object/from16 v0, p0

    #@2af
    iget-boolean v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSimIsInvalid:Z

    #@2b1
    const/4 v15, 0x1

    #@2b2
    if-ne v14, v15, :cond_2d2

    #@2b4
    .line 1686
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@2b7
    move-result-object v14

    #@2b8
    const v15, 0x2090128

    #@2bb
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@2be
    move-result-object v14

    #@2bf
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2c2
    move-result-object v9

    #@2c3
    .line 1687
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@2c6
    move-result-object v14

    #@2c7
    const v15, 0x2090128

    #@2ca
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@2cd
    move-result-object v14

    #@2ce
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2d1
    move-result-object v13

    #@2d2
    .line 1697
    :cond_2d2
    const-string v14, "JP"

    #@2d4
    const-string v15, "DCM"

    #@2d6
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@2d9
    move-result v14

    #@2da
    if-eqz v14, :cond_6d3

    #@2dc
    .line 1699
    move-object/from16 v0, p0

    #@2de
    iget-boolean v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@2e0
    if-eqz v14, :cond_2ec

    #@2e2
    move-object/from16 v0, p0

    #@2e4
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@2e6
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getDataState()I

    #@2e9
    move-result v14

    #@2ea
    if-nez v14, :cond_6d0

    #@2ec
    :cond_2ec
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2ef
    move-result v14

    #@2f0
    if-nez v14, :cond_6d0

    #@2f2
    and-int/lit8 v14, v10, 0x1

    #@2f4
    const/4 v15, 0x1

    #@2f5
    if-ne v14, v15, :cond_6d0

    #@2f7
    move-object/from16 v0, p0

    #@2f9
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@2fb
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getState()I

    #@2fe
    move-result v14

    #@2ff
    if-eqz v14, :cond_30b

    #@301
    move-object/from16 v0, p0

    #@303
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@305
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getDataState()I

    #@308
    move-result v14

    #@309
    if-nez v14, :cond_6d0

    #@30b
    :cond_30b
    const/4 v12, 0x1

    #@30c
    .line 1716
    :goto_30c
    move-object/from16 v0, p0

    #@30e
    iget-boolean v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowPlmn:Z

    #@310
    if-ne v11, v14, :cond_332

    #@312
    move-object/from16 v0, p0

    #@314
    iget-boolean v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowSpn:Z

    #@316
    if-ne v12, v14, :cond_332

    #@318
    move-object/from16 v0, p0

    #@31a
    iget-object v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curSpn:Ljava/lang/String;

    #@31c
    invoke-static {v13, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@31f
    move-result v14

    #@320
    if-eqz v14, :cond_332

    #@322
    move-object/from16 v0, p0

    #@324
    iget-object v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@326
    invoke-static {v9, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@329
    move-result v14

    #@32a
    if-eqz v14, :cond_332

    #@32c
    move-object/from16 v0, p0

    #@32e
    iget-boolean v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mResendEnable:Z

    #@330
    if-eqz v14, :cond_555

    #@332
    .line 1721
    :cond_332
    new-instance v14, Ljava/lang/StringBuilder;

    #@334
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@337
    const-string v15, "updateSpnDisplay: changed sending intent rule="

    #@339
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33c
    move-result-object v14

    #@33d
    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@340
    move-result-object v14

    #@341
    const-string v15, " showPlmn=\'%b\' plmn=\'%s\' showSpn=\'%b\' spn=\'%s\'"

    #@343
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@346
    move-result-object v14

    #@347
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34a
    move-result-object v14

    #@34b
    const/4 v15, 0x4

    #@34c
    new-array v15, v15, [Ljava/lang/Object;

    #@34e
    const/16 v16, 0x0

    #@350
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@353
    move-result-object v17

    #@354
    aput-object v17, v15, v16

    #@356
    const/16 v16, 0x1

    #@358
    aput-object v9, v15, v16

    #@35a
    const/16 v16, 0x2

    #@35c
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@35f
    move-result-object v17

    #@360
    aput-object v17, v15, v16

    #@362
    const/16 v16, 0x3

    #@364
    aput-object v13, v15, v16

    #@366
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@369
    move-result-object v14

    #@36a
    move-object/from16 v0, p0

    #@36c
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@36f
    .line 1728
    const-string v14, "BR"

    #@371
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@374
    move-result v14

    #@375
    if-nez v14, :cond_38d

    #@377
    const-string v14, "MX"

    #@379
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@37c
    move-result v14

    #@37d
    if-nez v14, :cond_38d

    #@37f
    const-string v14, "SCA"

    #@381
    const-string v15, "ro.build.target_region"

    #@383
    invoke-static {v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@386
    move-result-object v15

    #@387
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38a
    move-result v14

    #@38b
    if-eqz v14, :cond_3e0

    #@38d
    :cond_38d
    move-object/from16 v0, p0

    #@38f
    iget-boolean v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mSimIsInvalid:Z

    #@391
    const/4 v15, 0x1

    #@392
    if-ne v14, v15, :cond_3e0

    #@394
    .line 1729
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@397
    move-result-object v14

    #@398
    const v15, 0x2090128

    #@39b
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@39e
    move-result-object v14

    #@39f
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3a2
    move-result-object v9

    #@3a3
    .line 1730
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3a6
    move-result-object v14

    #@3a7
    const v15, 0x2090128

    #@3aa
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@3ad
    move-result-object v14

    #@3ae
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3b1
    move-result-object v13

    #@3b2
    .line 1731
    const/4 v11, 0x1

    #@3b3
    .line 1732
    const/4 v12, 0x0

    #@3b4
    .line 1733
    const-string v14, "[GsmServiceStateTracker]"

    #@3b6
    new-instance v15, Ljava/lang/StringBuilder;

    #@3b8
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@3bb
    const-string v16, "plmn : "

    #@3bd
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c0
    move-result-object v15

    #@3c1
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c4
    move-result-object v15

    #@3c5
    const-string v16, " showPlmn : "

    #@3c7
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ca
    move-result-object v15

    #@3cb
    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3ce
    move-result-object v15

    #@3cf
    const-string v16, " showSpn : "

    #@3d1
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d4
    move-result-object v15

    #@3d5
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3d8
    move-result-object v15

    #@3d9
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3dc
    move-result-object v15

    #@3dd
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e0
    .line 1737
    :cond_3e0
    const-string v14, "SCA"

    #@3e2
    const-string v15, "ro.build.target_region"

    #@3e4
    invoke-static {v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3e7
    move-result-object v15

    #@3e8
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3eb
    move-result v14

    #@3ec
    if-eqz v14, :cond_404

    #@3ee
    move-object/from16 v0, p0

    #@3f0
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@3f2
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getState()I

    #@3f5
    move-result v14

    #@3f6
    if-nez v14, :cond_404

    #@3f8
    .line 1738
    if-eqz v13, :cond_404

    #@3fa
    if-eqz v9, :cond_404

    #@3fc
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3ff
    move-result v14

    #@400
    if-eqz v14, :cond_404

    #@402
    .line 1739
    const/4 v12, 0x1

    #@403
    .line 1740
    const/4 v11, 0x0

    #@404
    .line 1746
    :cond_404
    move-object/from16 v0, p0

    #@406
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@408
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getState()I

    #@40b
    move-result v14

    #@40c
    if-nez v14, :cond_4cc

    #@40e
    .line 1747
    const-string v14, "GSM"

    #@410
    new-instance v15, Ljava/lang/StringBuilder;

    #@412
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@415
    const-string v16, "Before calling changenwname Rule: "

    #@417
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41a
    move-result-object v15

    #@41b
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41e
    move-result-object v15

    #@41f
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@422
    move-result-object v15

    #@423
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@426
    .line 1748
    const-string v14, "GSM"

    #@428
    new-instance v15, Ljava/lang/StringBuilder;

    #@42a
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@42d
    const-string v16, "Before Processing, spn: "

    #@42f
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@432
    move-result-object v15

    #@433
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@436
    move-result-object v15

    #@437
    const-string v16, "plmn: "

    #@439
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43c
    move-result-object v15

    #@43d
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@440
    move-result-object v15

    #@441
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@444
    move-result-object v15

    #@445
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@448
    .line 1749
    const-string v14, "GSM"

    #@44a
    new-instance v15, Ljava/lang/StringBuilder;

    #@44c
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@44f
    const-string v16, "Before Processing showSpn: "

    #@451
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@454
    move-result-object v15

    #@455
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@458
    move-result-object v15

    #@459
    const-string v16, "showPlmn: "

    #@45b
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45e
    move-result-object v15

    #@45f
    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@462
    move-result-object v15

    #@463
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@466
    move-result-object v15

    #@467
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46a
    .line 1751
    if-eqz v13, :cond_472

    #@46c
    .line 1752
    move-object/from16 v0, p0

    #@46e
    invoke-direct {v0, v13}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->ChangeNWname(Ljava/lang/String;)Ljava/lang/String;

    #@471
    move-result-object v13

    #@472
    .line 1754
    :cond_472
    if-eqz v9, :cond_47a

    #@474
    .line 1755
    move-object/from16 v0, p0

    #@476
    invoke-direct {v0, v9}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->ChangeNWname(Ljava/lang/String;)Ljava/lang/String;

    #@479
    move-result-object v9

    #@47a
    .line 1758
    :cond_47a
    if-eqz v1, :cond_4b6

    #@47c
    if-eqz v3, :cond_4b6

    #@47e
    .line 1760
    const-string v14, "52000"

    #@480
    invoke-virtual {v3, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@483
    move-result v14

    #@484
    if-nez v14, :cond_72d

    #@486
    const-string v14, "52099"

    #@488
    invoke-virtual {v1, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@48b
    move-result v14

    #@48c
    if-nez v14, :cond_72d

    #@48e
    .line 1761
    move-object/from16 v0, p0

    #@490
    iget-object v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@492
    invoke-virtual {v14}, Lcom/android/internal/telephony/gsm/GSMPhone;->getSubscriberId()Ljava/lang/String;

    #@495
    move-result-object v4

    #@496
    .line 1762
    .local v4, TRUE_IMSI:Ljava/lang/String;
    if-eqz v4, :cond_707

    #@498
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@49b
    move-result v14

    #@49c
    const/4 v15, 0x7

    #@49d
    if-lt v14, v15, :cond_707

    #@49f
    const-string v14, "5200020"

    #@4a1
    const/4 v15, 0x0

    #@4a2
    const/16 v16, 0x7

    #@4a4
    move/from16 v0, v16

    #@4a6
    invoke-virtual {v4, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4a9
    move-result-object v15

    #@4aa
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4ad
    move-result v14

    #@4ae
    if-eqz v14, :cond_707

    #@4b0
    .line 1764
    const-string v9, "TRUE-H"

    #@4b2
    .line 1765
    const-string v13, "TRUE"

    #@4b4
    .line 1776
    :goto_4b4
    const/4 v11, 0x1

    #@4b5
    .line 1777
    const/4 v12, 0x1

    #@4b6
    .line 1842
    .end local v4           #TRUE_IMSI:Ljava/lang/String;
    :cond_4b6
    :goto_4b6
    if-eqz v9, :cond_4cc

    #@4b8
    if-eqz v13, :cond_4cc

    #@4ba
    move-object/from16 v0, p0

    #@4bc
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@4be
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@4c1
    move-result v14

    #@4c2
    if-nez v14, :cond_4cc

    #@4c4
    .line 1843
    invoke-virtual {v9, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@4c7
    move-result v14

    #@4c8
    if-eqz v14, :cond_4cc

    #@4ca
    .line 1844
    const/4 v11, 0x1

    #@4cb
    .line 1845
    const/4 v12, 0x0

    #@4cc
    .line 1850
    :cond_4cc
    const-string v14, "GSM"

    #@4ce
    new-instance v15, Ljava/lang/StringBuilder;

    #@4d0
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@4d3
    const-string v16, "After calling changenwname Rule: "

    #@4d5
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d8
    move-result-object v15

    #@4d9
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4dc
    move-result-object v15

    #@4dd
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e0
    move-result-object v15

    #@4e1
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e4
    .line 1851
    const-string v14, "GSM"

    #@4e6
    new-instance v15, Ljava/lang/StringBuilder;

    #@4e8
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@4eb
    const-string v16, "After Processing, spn: "

    #@4ed
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f0
    move-result-object v15

    #@4f1
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f4
    move-result-object v15

    #@4f5
    const-string v16, "plmn: "

    #@4f7
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4fa
    move-result-object v15

    #@4fb
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4fe
    move-result-object v15

    #@4ff
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@502
    move-result-object v15

    #@503
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@506
    .line 1852
    const-string v14, "GSM"

    #@508
    new-instance v15, Ljava/lang/StringBuilder;

    #@50a
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@50d
    const-string v16, "After Processing showSpn: "

    #@50f
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@512
    move-result-object v15

    #@513
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@516
    move-result-object v15

    #@517
    const-string v16, "showPlmn: "

    #@519
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51c
    move-result-object v15

    #@51d
    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@520
    move-result-object v15

    #@521
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@524
    move-result-object v15

    #@525
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@528
    .line 1855
    new-instance v8, Landroid/content/Intent;

    #@52a
    const-string v14, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@52c
    invoke-direct {v8, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@52f
    .line 1856
    .restart local v8       #intent:Landroid/content/Intent;
    const/high16 v14, 0x2000

    #@531
    invoke-virtual {v8, v14}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@534
    .line 1857
    const-string v14, "showSpn"

    #@536
    invoke-virtual {v8, v14, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@539
    .line 1858
    const-string v14, "spn"

    #@53b
    invoke-virtual {v8, v14, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@53e
    .line 1859
    const-string v14, "showPlmn"

    #@540
    invoke-virtual {v8, v14, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@543
    .line 1860
    const-string v14, "plmn"

    #@545
    invoke-virtual {v8, v14, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@548
    .line 1861
    move-object/from16 v0, p0

    #@54a
    iget-object v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@54c
    invoke-virtual {v14}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@54f
    move-result-object v14

    #@550
    sget-object v15, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@552
    invoke-virtual {v14, v8, v15}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@555
    .line 1864
    .end local v8           #intent:Landroid/content/Intent;
    :cond_555
    move-object/from16 v0, p0

    #@557
    iput-boolean v12, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowSpn:Z

    #@559
    .line 1865
    move-object/from16 v0, p0

    #@55b
    iput-boolean v11, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curShowPlmn:Z

    #@55d
    .line 1866
    move-object/from16 v0, p0

    #@55f
    iput-object v13, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curSpn:Ljava/lang/String;

    #@561
    .line 1867
    move-object/from16 v0, p0

    #@563
    iput-object v9, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->curPlmn:Ljava/lang/String;

    #@565
    goto/16 :goto_d

    #@567
    .line 1574
    .end local v10           #rule:I
    :cond_567
    const/4 v10, 0x0

    #@568
    goto/16 :goto_1a7

    #@56a
    .line 1587
    .restart local v10       #rule:I
    :cond_56a
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@56d
    move-result-object v14

    #@56e
    const v15, 0x1040325

    #@571
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@574
    move-result-object v14

    #@575
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@578
    move-result-object v9

    #@579
    goto/16 :goto_202

    #@57b
    .line 1601
    :cond_57b
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@57e
    move-result-object v14

    #@57f
    const v15, 0x104030b

    #@582
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@585
    move-result-object v14

    #@586
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@589
    move-result-object v9

    #@58a
    .line 1605
    const-string v14, "VZW"

    #@58c
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@58f
    move-result v14

    #@590
    if-eqz v14, :cond_252

    #@592
    .line 1606
    move-object/from16 v0, p0

    #@594
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@596
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getDataState()I

    #@599
    move-result v14

    #@59a
    if-nez v14, :cond_252

    #@59c
    .line 1608
    move-object/from16 v0, p0

    #@59e
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@5a0
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@5a3
    move-result-object v9

    #@5a4
    .line 1609
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5a7
    move-result v14

    #@5a8
    if-nez v14, :cond_5d0

    #@5aa
    and-int/lit8 v14, v10, 0x2

    #@5ac
    const/4 v15, 0x2

    #@5ad
    if-ne v14, v15, :cond_5d0

    #@5af
    const/4 v11, 0x1

    #@5b0
    .line 1612
    :goto_5b0
    new-instance v14, Ljava/lang/StringBuilder;

    #@5b2
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@5b5
    const-string v15, "updateSpnDisplay: radio is on but PS only state, set plmn=\'"

    #@5b7
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5ba
    move-result-object v14

    #@5bb
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5be
    move-result-object v14

    #@5bf
    const-string v15, "\'"

    #@5c1
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c4
    move-result-object v14

    #@5c5
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c8
    move-result-object v14

    #@5c9
    move-object/from16 v0, p0

    #@5cb
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@5ce
    goto/16 :goto_252

    #@5d0
    .line 1609
    :cond_5d0
    const/4 v11, 0x0

    #@5d1
    goto :goto_5b0

    #@5d2
    .line 1625
    :cond_5d2
    const-string v14, "US"

    #@5d4
    const-string v15, "TMO"

    #@5d6
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@5d9
    move-result v14

    #@5da
    if-eqz v14, :cond_26a

    #@5dc
    .line 1626
    move-object/from16 v0, p0

    #@5de
    iget-boolean v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mPermanentReject:Z

    #@5e0
    if-eqz v14, :cond_616

    #@5e2
    .line 1627
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@5e5
    move-result-object v14

    #@5e6
    const v15, 0x104030b

    #@5e9
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@5ec
    move-result-object v14

    #@5ed
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5f0
    move-result-object v9

    #@5f1
    .line 1628
    const-string v14, "[GsmServiceStateTracker]"

    #@5f3
    new-instance v15, Ljava/lang/StringBuilder;

    #@5f5
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@5f8
    const-string v16, "[CHOSH] updateSpnDisplay: No service and radio is on plmn=\'"

    #@5fa
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5fd
    move-result-object v15

    #@5fe
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@601
    move-result-object v15

    #@602
    const-string v16, "\'"

    #@604
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@607
    move-result-object v15

    #@608
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60b
    move-result-object v15

    #@60c
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60f
    .line 1634
    :goto_60f
    const/4 v14, 0x0

    #@610
    move-object/from16 v0, p0

    #@612
    iput v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mResendCounter:I

    #@614
    goto/16 :goto_26a

    #@616
    .line 1631
    :cond_616
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@619
    move-result-object v14

    #@61a
    const v15, 0x104000c

    #@61d
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@620
    move-result-object v14

    #@621
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@624
    move-result-object v9

    #@625
    .line 1632
    const-string v14, "[GsmServiceStateTracker]"

    #@627
    new-instance v15, Ljava/lang/StringBuilder;

    #@629
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@62c
    const-string v16, "[CHOSH] updateSpnDisplay: Search and radio is on plmn=\'"

    #@62e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@631
    move-result-object v15

    #@632
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@635
    move-result-object v15

    #@636
    const-string v16, "\'"

    #@638
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63b
    move-result-object v15

    #@63c
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63f
    move-result-object v15

    #@640
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@643
    goto :goto_60f

    #@644
    .line 1640
    :cond_644
    move-object/from16 v0, p0

    #@646
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@648
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getState()I

    #@64b
    move-result v14

    #@64c
    if-nez v14, :cond_666

    #@64e
    .line 1642
    move-object/from16 v0, p0

    #@650
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@652
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@655
    move-result-object v9

    #@656
    .line 1643
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@659
    move-result v14

    #@65a
    if-nez v14, :cond_664

    #@65c
    and-int/lit8 v14, v10, 0x2

    #@65e
    const/4 v15, 0x2

    #@65f
    if-ne v14, v15, :cond_664

    #@661
    const/4 v11, 0x1

    #@662
    :goto_662
    goto/16 :goto_288

    #@664
    :cond_664
    const/4 v11, 0x0

    #@665
    goto :goto_662

    #@666
    .line 1648
    :cond_666
    new-instance v14, Ljava/lang/StringBuilder;

    #@668
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@66b
    const-string v15, "updateSpnDisplay: radio is off w/ showPlmn="

    #@66d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@670
    move-result-object v14

    #@671
    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@674
    move-result-object v14

    #@675
    const-string v15, " plmn="

    #@677
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67a
    move-result-object v14

    #@67b
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67e
    move-result-object v14

    #@67f
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@682
    move-result-object v14

    #@683
    move-object/from16 v0, p0

    #@685
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->log(Ljava/lang/String;)V

    #@688
    goto/16 :goto_288

    #@68a
    .line 1660
    :pswitch_68a
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@68d
    move-result-object v14

    #@68e
    const v15, 0x209011b

    #@691
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@694
    move-result-object v9

    #@695
    .line 1661
    const/4 v11, 0x1

    #@696
    .line 1662
    goto/16 :goto_28f

    #@698
    .line 1664
    :pswitch_698
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@69b
    move-result-object v14

    #@69c
    const v15, 0x209011c

    #@69f
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6a2
    move-result-object v9

    #@6a3
    .line 1665
    const/4 v11, 0x1

    #@6a4
    .line 1666
    goto/16 :goto_28f

    #@6a6
    .line 1668
    :pswitch_6a6
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@6a9
    move-result-object v14

    #@6aa
    const v15, 0x209011d

    #@6ad
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6b0
    move-result-object v9

    #@6b1
    .line 1669
    const/4 v11, 0x1

    #@6b2
    .line 1670
    goto/16 :goto_28f

    #@6b4
    .line 1672
    :pswitch_6b4
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@6b7
    move-result-object v14

    #@6b8
    const v15, 0x209011e

    #@6bb
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6be
    move-result-object v9

    #@6bf
    .line 1673
    const/4 v11, 0x1

    #@6c0
    .line 1674
    goto/16 :goto_28f

    #@6c2
    .line 1676
    :pswitch_6c2
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@6c5
    move-result-object v14

    #@6c6
    const v15, 0x209011f

    #@6c9
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6cc
    move-result-object v9

    #@6cd
    .line 1677
    const/4 v11, 0x1

    #@6ce
    goto/16 :goto_28f

    #@6d0
    .line 1699
    :cond_6d0
    const/4 v12, 0x0

    #@6d1
    goto/16 :goto_30c

    #@6d3
    .line 1707
    :cond_6d3
    move-object/from16 v0, p0

    #@6d5
    iget-boolean v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->mEmergencyOnly:Z

    #@6d7
    if-eqz v14, :cond_6e3

    #@6d9
    move-object/from16 v0, p0

    #@6db
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@6dd
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getDataState()I

    #@6e0
    move-result v14

    #@6e1
    if-nez v14, :cond_705

    #@6e3
    :cond_6e3
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6e6
    move-result v14

    #@6e7
    if-nez v14, :cond_705

    #@6e9
    and-int/lit8 v14, v10, 0x1

    #@6eb
    const/4 v15, 0x1

    #@6ec
    if-ne v14, v15, :cond_705

    #@6ee
    move-object/from16 v0, p0

    #@6f0
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@6f2
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getState()I

    #@6f5
    move-result v14

    #@6f6
    if-eqz v14, :cond_702

    #@6f8
    move-object/from16 v0, p0

    #@6fa
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@6fc
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getDataState()I

    #@6ff
    move-result v14

    #@700
    if-nez v14, :cond_705

    #@702
    :cond_702
    const/4 v12, 0x1

    #@703
    :goto_703
    goto/16 :goto_30c

    #@705
    :cond_705
    const/4 v12, 0x0

    #@706
    goto :goto_703

    #@707
    .line 1767
    .restart local v4       #TRUE_IMSI:Ljava/lang/String;
    :cond_707
    if-eqz v4, :cond_727

    #@709
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@70c
    move-result v14

    #@70d
    const/4 v15, 0x7

    #@70e
    if-lt v14, v15, :cond_727

    #@710
    const-string v14, "5200019"

    #@712
    const/4 v15, 0x0

    #@713
    const/16 v16, 0x7

    #@715
    move/from16 v0, v16

    #@717
    invoke-virtual {v4, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@71a
    move-result-object v15

    #@71b
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71e
    move-result v14

    #@71f
    if-eqz v14, :cond_727

    #@721
    .line 1769
    const-string v9, "my"

    #@723
    .line 1770
    const-string v13, "TRUE"

    #@725
    goto/16 :goto_4b4

    #@727
    .line 1773
    :cond_727
    const-string v9, "TRUE"

    #@729
    .line 1774
    const-string v13, "TRUE"

    #@72b
    goto/16 :goto_4b4

    #@72d
    .line 1782
    .end local v4           #TRUE_IMSI:Ljava/lang/String;
    :cond_72d
    const-string v14, "65507"

    #@72f
    invoke-virtual {v3, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@732
    move-result v14

    #@733
    if-nez v14, :cond_7a5

    #@735
    .line 1783
    move-object/from16 v0, p0

    #@737
    iget-object v14, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@739
    invoke-virtual {v14}, Lcom/android/internal/telephony/gsm/GSMPhone;->getSubscriberId()Ljava/lang/String;

    #@73c
    move-result-object v2

    #@73d
    .line 1784
    .local v2, RBull_imsi:Ljava/lang/String;
    const-string v14, "65501"

    #@73f
    invoke-virtual {v1, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@742
    move-result v14

    #@743
    if-nez v14, :cond_751

    #@745
    move-object/from16 v0, p0

    #@747
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@749
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@74c
    move-result v14

    #@74d
    if-eqz v14, :cond_751

    #@74f
    .line 1785
    const/4 v12, 0x1

    #@750
    .line 1786
    const/4 v11, 0x0

    #@751
    .line 1788
    :cond_751
    const-string v14, "GSM "

    #@753
    new-instance v15, Ljava/lang/StringBuilder;

    #@755
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@758
    const-string v16, "[RBull - cellc MVNO] RBull_imsi:  "

    #@75a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75d
    move-result-object v15

    #@75e
    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@761
    move-result-object v15

    #@762
    const-string v16, "plmn: "

    #@764
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@767
    move-result-object v15

    #@768
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76b
    move-result-object v15

    #@76c
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76f
    move-result-object v15

    #@770
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@773
    .line 1790
    if-eqz v2, :cond_4b6

    #@775
    if-eqz v9, :cond_4b6

    #@777
    .line 1791
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@77a
    move-result v14

    #@77b
    const/4 v15, 0x6

    #@77c
    if-lt v14, v15, :cond_4b6

    #@77e
    move-object/from16 v0, p0

    #@780
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@782
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@785
    move-result v14

    #@786
    if-nez v14, :cond_4b6

    #@788
    const-string v14, "Cell C"

    #@78a
    invoke-virtual {v9, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@78d
    move-result v14

    #@78e
    if-eqz v14, :cond_4b6

    #@790
    .line 1793
    const-string v14, "6550713"

    #@792
    const/4 v15, 0x0

    #@793
    const/16 v16, 0x7

    #@795
    move/from16 v0, v16

    #@797
    invoke-virtual {v2, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@79a
    move-result-object v15

    #@79b
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@79e
    move-result v14

    #@79f
    if-eqz v14, :cond_4b6

    #@7a1
    .line 1794
    const-string v9, "Red Bull"

    #@7a3
    goto/16 :goto_4b6

    #@7a5
    .line 1802
    .end local v2           #RBull_imsi:Ljava/lang/String;
    :cond_7a5
    const-string v14, "60400"

    #@7a7
    invoke-virtual {v3, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@7aa
    move-result v14

    #@7ab
    if-nez v14, :cond_7de

    #@7ad
    .line 1803
    const-string v14, "Pro"

    #@7af
    invoke-virtual {v14, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7b2
    move-result v14

    #@7b3
    if-nez v14, :cond_7bf

    #@7b5
    const-string v14, "Perso"

    #@7b7
    invoke-virtual {v14, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7ba
    move-result v14

    #@7bb
    if-nez v14, :cond_7bf

    #@7bd
    .line 1804
    const-string v13, "Meditel"

    #@7bf
    .line 1806
    :cond_7bf
    if-eqz v9, :cond_4b6

    #@7c1
    move-object/from16 v0, p0

    #@7c3
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@7c5
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@7c8
    move-result v14

    #@7c9
    if-nez v14, :cond_4b6

    #@7cb
    .line 1807
    const/4 v14, 0x1

    #@7cc
    if-ne v12, v14, :cond_7da

    #@7ce
    const/4 v14, 0x1

    #@7cf
    if-ne v11, v14, :cond_7da

    #@7d1
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7d4
    move-result v14

    #@7d5
    if-eqz v14, :cond_7da

    #@7d7
    .line 1808
    const/4 v11, 0x0

    #@7d8
    goto/16 :goto_4b6

    #@7da
    .line 1810
    :cond_7da
    const-string v9, "Meditel"

    #@7dc
    goto/16 :goto_4b6

    #@7de
    .line 1815
    :cond_7de
    const-string v14, "62130"

    #@7e0
    invoke-virtual {v3, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@7e3
    move-result v14

    #@7e4
    if-nez v14, :cond_801

    #@7e6
    const-string v14, "62130"

    #@7e8
    invoke-virtual {v1, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@7eb
    move-result v14

    #@7ec
    if-nez v14, :cond_801

    #@7ee
    .line 1816
    const/4 v14, 0x1

    #@7ef
    if-ne v11, v14, :cond_4b6

    #@7f1
    if-nez v12, :cond_4b6

    #@7f3
    move-object/from16 v0, p0

    #@7f5
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@7f7
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@7fa
    move-result v14

    #@7fb
    if-nez v14, :cond_4b6

    #@7fd
    .line 1817
    const-string v9, "MTN-NG"

    #@7ff
    goto/16 :goto_4b6

    #@801
    .line 1823
    :cond_801
    const-string v14, "42601"

    #@803
    invoke-virtual {v3, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@806
    move-result v14

    #@807
    if-nez v14, :cond_81e

    #@809
    .line 1824
    const/4 v14, 0x1

    #@80a
    if-ne v11, v14, :cond_4b6

    #@80c
    if-nez v12, :cond_4b6

    #@80e
    move-object/from16 v0, p0

    #@810
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@812
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@815
    move-result v14

    #@816
    const/4 v15, 0x1

    #@817
    if-ne v14, v15, :cond_4b6

    #@819
    .line 1825
    const-string v13, "Batelco"

    #@81b
    .line 1826
    const/4 v12, 0x1

    #@81c
    goto/16 :goto_4b6

    #@81e
    .line 1833
    :cond_81e
    const-string v14, "405853"

    #@820
    invoke-virtual {v3, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@823
    move-result v14

    #@824
    if-nez v14, :cond_4b6

    #@826
    const-string v14, "40430"

    #@828
    invoke-virtual {v1, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@82b
    move-result v14

    #@82c
    if-nez v14, :cond_4b6

    #@82e
    if-eqz v9, :cond_4b6

    #@830
    const-string v14, "40430"

    #@832
    invoke-virtual {v9, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@835
    move-result v14

    #@836
    if-eqz v14, :cond_4b6

    #@838
    move-object/from16 v0, p0

    #@83a
    iget-object v14, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@83c
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@83f
    move-result v14

    #@840
    const/4 v15, 0x1

    #@841
    if-ne v14, v15, :cond_4b6

    #@843
    .line 1835
    const-string v9, "Vodafone IN"

    #@845
    goto/16 :goto_4b6

    #@847
    .line 1658
    nop

    #@848
    :pswitch_data_848
    .packed-switch 0x1
        :pswitch_68a
        :pswitch_698
        :pswitch_6a6
        :pswitch_6b4
        :pswitch_6c2
    .end packed-switch
.end method
