.class abstract Lcom/android/internal/telephony/sip/SipConnectionBase;
.super Lcom/android/internal/telephony/Connection;
.source "SipConnectionBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/sip/SipConnectionBase$1;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "SIP_CONN"


# instance fields
.field private connectTime:J

.field private connectTimeReal:J

.field private createTime:J

.field private dialString:Ljava/lang/String;

.field private disconnectTime:J

.field private duration:J

.field private holdingStartTime:J

.field private isIncoming:Z

.field private mCause:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field private mSipAudioCall:Landroid/net/sip/SipAudioCall;

.field private nextPostDialChar:I

.field private postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

.field private postDialString:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "dialString"

    #@0
    .prologue
    .line 61
    invoke-direct {p0}, Lcom/android/internal/telephony/Connection;-><init>()V

    #@3
    .line 54
    const-wide/16 v0, -0x1

    #@5
    iput-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->duration:J

    #@7
    .line 58
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->mCause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@b
    .line 59
    sget-object v0, Lcom/android/internal/telephony/Connection$PostDialState;->NOT_STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@d
    iput-object v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@f
    .line 62
    iput-object p1, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->dialString:Ljava/lang/String;

    #@11
    .line 64
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->extractPostDialPortion(Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->postDialString:Ljava/lang/String;

    #@17
    .line 66
    const/4 v0, 0x0

    #@18
    iput-boolean v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->isIncoming:Z

    #@1a
    .line 67
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1d
    move-result-wide v0

    #@1e
    iput-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->createTime:J

    #@20
    .line 68
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 168
    const-string v0, "SIP_CONN"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[SipConn] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 169
    return-void
.end method


# virtual methods
.method public cancelPostDial()V
    .registers 1

    #@0
    .prologue
    .line 151
    return-void
.end method

.method public getBeforeFowardingNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 186
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getConnectTime()J
    .registers 3

    #@0
    .prologue
    .line 95
    iget-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->connectTime:J

    #@2
    return-wide v0
.end method

.method public getCreateTime()J
    .registers 3

    #@0
    .prologue
    .line 90
    iget-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->createTime:J

    #@2
    return-wide v0
.end method

.method public getDisconnectCause()Lcom/android/internal/telephony/Connection$DisconnectCause;
    .registers 2

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->mCause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    return-object v0
.end method

.method public getDisconnectTime()J
    .registers 3

    #@0
    .prologue
    .line 100
    iget-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->disconnectTime:J

    #@2
    return-wide v0
.end method

.method public getDurationMillis()J
    .registers 5

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 105
    iget-wide v2, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->connectTimeReal:J

    #@4
    cmp-long v2, v2, v0

    #@6
    if-nez v2, :cond_9

    #@8
    .line 110
    :goto_8
    return-wide v0

    #@9
    .line 107
    :cond_9
    iget-wide v2, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->duration:J

    #@b
    cmp-long v0, v2, v0

    #@d
    if-gez v0, :cond_17

    #@f
    .line 108
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@12
    move-result-wide v0

    #@13
    iget-wide v2, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->connectTimeReal:J

    #@15
    sub-long/2addr v0, v2

    #@16
    goto :goto_8

    #@17
    .line 110
    :cond_17
    iget-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->duration:J

    #@19
    goto :goto_8
.end method

.method public getHoldDurationMillis()J
    .registers 5

    #@0
    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipConnectionBase;->getState()Lcom/android/internal/telephony/Call$State;

    #@3
    move-result-object v0

    #@4
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@6
    if-eq v0, v1, :cond_b

    #@8
    .line 118
    const-wide/16 v0, 0x0

    #@a
    .line 120
    :goto_a
    return-wide v0

    #@b
    :cond_b
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@e
    move-result-wide v0

    #@f
    iget-wide v2, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->holdingStartTime:J

    #@11
    sub-long/2addr v0, v2

    #@12
    goto :goto_a
.end method

.method public getNumberPresentation()I
    .registers 2

    #@0
    .prologue
    .line 174
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@2
    return v0
.end method

.method protected abstract getPhone()Lcom/android/internal/telephony/Phone;
.end method

.method public getPostDialState()Lcom/android/internal/telephony/Connection$PostDialState;
    .registers 2

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2
    return-object v0
.end method

.method public getRemainingPostDialString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 157
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->CANCELLED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@4
    if-eq v0, v1, :cond_1a

    #@6
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@8
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->COMPLETE:Lcom/android/internal/telephony/Connection$PostDialState;

    #@a
    if-eq v0, v1, :cond_1a

    #@c
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->postDialString:Ljava/lang/String;

    #@e
    if-eqz v0, :cond_1a

    #@10
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->postDialString:Ljava/lang/String;

    #@12
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@15
    move-result v0

    #@16
    iget v1, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->nextPostDialChar:I

    #@18
    if-gt v0, v1, :cond_1d

    #@1a
    .line 161
    :cond_1a
    const-string v0, ""

    #@1c
    .line 164
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    iget-object v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->postDialString:Ljava/lang/String;

    #@1f
    iget v1, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->nextPostDialChar:I

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    goto :goto_1c
.end method

.method public getUUSInfo()Lcom/android/internal/telephony/UUSInfo;
    .registers 2

    #@0
    .prologue
    .line 180
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public proceedAfterWaitChar()V
    .registers 1

    #@0
    .prologue
    .line 141
    return-void
.end method

.method public proceedAfterWildChar(Ljava/lang/String;)V
    .registers 2
    .parameter "str"

    #@0
    .prologue
    .line 146
    return-void
.end method

.method setDisconnectCause(Lcom/android/internal/telephony/Connection$DisconnectCause;)V
    .registers 2
    .parameter "cause"

    #@0
    .prologue
    .line 130
    iput-object p1, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->mCause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    .line 131
    return-void
.end method

.method protected setState(Lcom/android/internal/telephony/Call$State;)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 71
    sget-object v0, Lcom/android/internal/telephony/sip/SipConnectionBase$1;->$SwitchMap$com$android$internal$telephony$Call$State:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_36

    #@b
    .line 86
    :cond_b
    :goto_b
    return-void

    #@c
    .line 73
    :pswitch_c
    iget-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->connectTime:J

    #@e
    const-wide/16 v2, 0x0

    #@10
    cmp-long v0, v0, v2

    #@12
    if-nez v0, :cond_b

    #@14
    .line 74
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@17
    move-result-wide v0

    #@18
    iput-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->connectTimeReal:J

    #@1a
    .line 75
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1d
    move-result-wide v0

    #@1e
    iput-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->connectTime:J

    #@20
    goto :goto_b

    #@21
    .line 79
    :pswitch_21
    invoke-virtual {p0}, Lcom/android/internal/telephony/sip/SipConnectionBase;->getDurationMillis()J

    #@24
    move-result-wide v0

    #@25
    iput-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->duration:J

    #@27
    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@2a
    move-result-wide v0

    #@2b
    iput-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->disconnectTime:J

    #@2d
    goto :goto_b

    #@2e
    .line 83
    :pswitch_2e
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@31
    move-result-wide v0

    #@32
    iput-wide v0, p0, Lcom/android/internal/telephony/sip/SipConnectionBase;->holdingStartTime:J

    #@34
    goto :goto_b

    #@35
    .line 71
    nop

    #@36
    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_c
        :pswitch_21
        :pswitch_2e
    .end packed-switch
.end method
