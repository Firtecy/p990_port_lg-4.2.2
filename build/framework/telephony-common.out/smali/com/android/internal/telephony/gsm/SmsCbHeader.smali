.class Lcom/android/internal/telephony/gsm/SmsCbHeader;
.super Ljava/lang/Object;
.source "SmsCbHeader.java"


# static fields
.field static final FORMAT_ETWS_PRIMARY:I = 0x3

.field static final FORMAT_GSM:I = 0x1

.field static final FORMAT_UMTS:I = 0x2

.field private static final MESSAGE_TYPE_CBS_MESSAGE:I = 0x1

.field static final PDU_HEADER_LENGTH:I = 0x6

.field private static final PDU_LENGTH_ETWS:I = 0x38

.field private static final PDU_LENGTH_GSM:I = 0x58


# instance fields
.field private final dataCodingScheme:I

.field private final format:I

.field private final geographicalScope:I

.field private final mCmasInfo:Landroid/telephony/SmsCbCmasInfo;

.field private final mEtwsInfo:Landroid/telephony/SmsCbEtwsInfo;

.field private final messageIdentifier:I

.field private final nrOfPages:I

.field private final pageIndex:I

.field private final serialNumber:I


# direct methods
.method public constructor <init>([B)V
    .registers 16
    .parameter "pdu"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 93
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 94
    if-eqz p1, :cond_9

    #@5
    array-length v0, p1

    #@6
    const/4 v2, 0x6

    #@7
    if-ge v0, v2, :cond_11

    #@9
    .line 95
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    const-string v2, "Illegal PDU"

    #@d
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 98
    :cond_11
    array-length v0, p1

    #@12
    const/16 v2, 0x58

    #@14
    if-gt v0, v2, :cond_c6

    #@16
    .line 103
    const/4 v0, 0x0

    #@17
    aget-byte v0, p1, v0

    #@19
    and-int/lit16 v0, v0, 0xc0

    #@1b
    ushr-int/lit8 v0, v0, 0x6

    #@1d
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->geographicalScope:I

    #@1f
    .line 104
    const/4 v0, 0x0

    #@20
    aget-byte v0, p1, v0

    #@22
    and-int/lit16 v0, v0, 0xff

    #@24
    shl-int/lit8 v0, v0, 0x8

    #@26
    const/4 v2, 0x1

    #@27
    aget-byte v2, p1, v2

    #@29
    and-int/lit16 v2, v2, 0xff

    #@2b
    or-int/2addr v0, v2

    #@2c
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->serialNumber:I

    #@2e
    .line 105
    const/4 v0, 0x2

    #@2f
    aget-byte v0, p1, v0

    #@31
    and-int/lit16 v0, v0, 0xff

    #@33
    shl-int/lit8 v0, v0, 0x8

    #@35
    const/4 v2, 0x3

    #@36
    aget-byte v2, p1, v2

    #@38
    and-int/lit16 v2, v2, 0xff

    #@3a
    or-int/2addr v0, v2

    #@3b
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@3d
    .line 106
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->isEtwsMessage()Z

    #@40
    move-result v0

    #@41
    if-eqz v0, :cond_86

    #@43
    array-length v0, p1

    #@44
    const/16 v2, 0x38

    #@46
    if-gt v0, v2, :cond_86

    #@48
    .line 107
    const/4 v0, 0x3

    #@49
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->format:I

    #@4b
    .line 108
    const/4 v0, -0x1

    #@4c
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->dataCodingScheme:I

    #@4e
    .line 109
    const/4 v0, -0x1

    #@4f
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->pageIndex:I

    #@51
    .line 110
    const/4 v0, -0x1

    #@52
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->nrOfPages:I

    #@54
    .line 111
    const/4 v0, 0x4

    #@55
    aget-byte v0, p1, v0

    #@57
    and-int/lit8 v0, v0, 0x1

    #@59
    if-eqz v0, :cond_80

    #@5b
    const/4 v8, 0x1

    #@5c
    .line 112
    .local v8, emergencyUserAlert:Z
    :goto_5c
    const/4 v0, 0x5

    #@5d
    aget-byte v0, p1, v0

    #@5f
    and-int/lit16 v0, v0, 0x80

    #@61
    if-eqz v0, :cond_82

    #@63
    const/4 v7, 0x1

    #@64
    .line 113
    .local v7, activatePopup:Z
    :goto_64
    const/4 v0, 0x4

    #@65
    aget-byte v0, p1, v0

    #@67
    and-int/lit16 v0, v0, 0xfe

    #@69
    ushr-int/lit8 v13, v0, 0x1

    #@6b
    .line 116
    .local v13, warningType:I
    array-length v0, p1

    #@6c
    const/4 v2, 0x6

    #@6d
    if-le v0, v2, :cond_84

    #@6f
    .line 117
    const/4 v0, 0x6

    #@70
    array-length v2, p1

    #@71
    invoke-static {p1, v0, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    #@74
    move-result-object v12

    #@75
    .line 121
    .local v12, warningSecurityInfo:[B
    :goto_75
    new-instance v0, Landroid/telephony/SmsCbEtwsInfo;

    #@77
    invoke-direct {v0, v13, v8, v7, v12}, Landroid/telephony/SmsCbEtwsInfo;-><init>(IZZ[B)V

    #@7a
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->mEtwsInfo:Landroid/telephony/SmsCbEtwsInfo;

    #@7c
    .line 123
    const/4 v0, 0x0

    #@7d
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->mCmasInfo:Landroid/telephony/SmsCbCmasInfo;

    #@7f
    .line 183
    .end local v7           #activatePopup:Z
    .end local v8           #emergencyUserAlert:Z
    .end local v12           #warningSecurityInfo:[B
    .end local v13           #warningType:I
    :goto_7f
    return-void

    #@80
    .line 111
    :cond_80
    const/4 v8, 0x0

    #@81
    goto :goto_5c

    #@82
    .line 112
    .restart local v8       #emergencyUserAlert:Z
    :cond_82
    const/4 v7, 0x0

    #@83
    goto :goto_64

    #@84
    .line 119
    .restart local v7       #activatePopup:Z
    .restart local v13       #warningType:I
    :cond_84
    const/4 v12, 0x0

    #@85
    .restart local v12       #warningSecurityInfo:[B
    goto :goto_75

    #@86
    .line 127
    .end local v7           #activatePopup:Z
    .end local v8           #emergencyUserAlert:Z
    .end local v12           #warningSecurityInfo:[B
    .end local v13           #warningType:I
    :cond_86
    const/4 v0, 0x1

    #@87
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->format:I

    #@89
    .line 128
    const/4 v0, 0x4

    #@8a
    aget-byte v0, p1, v0

    #@8c
    and-int/lit16 v0, v0, 0xff

    #@8e
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->dataCodingScheme:I

    #@90
    .line 131
    const/4 v0, 0x5

    #@91
    aget-byte v0, p1, v0

    #@93
    and-int/lit16 v0, v0, 0xf0

    #@95
    ushr-int/lit8 v11, v0, 0x4

    #@97
    .line 132
    .local v11, pageIndex:I
    const/4 v0, 0x5

    #@98
    aget-byte v0, p1, v0

    #@9a
    and-int/lit8 v10, v0, 0xf

    #@9c
    .line 134
    .local v10, nrOfPages:I
    if-eqz v11, :cond_a2

    #@9e
    if-eqz v10, :cond_a2

    #@a0
    if-le v11, v10, :cond_a4

    #@a2
    .line 135
    :cond_a2
    const/4 v11, 0x1

    #@a3
    .line 136
    const/4 v10, 0x1

    #@a4
    .line 139
    :cond_a4
    iput v11, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->pageIndex:I

    #@a6
    .line 140
    iput v10, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->nrOfPages:I

    #@a8
    .line 165
    .end local v10           #nrOfPages:I
    .end local v11           #pageIndex:I
    :goto_a8
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->isEtwsMessage()Z

    #@ab
    move-result v0

    #@ac
    if-eqz v0, :cond_11d

    #@ae
    .line 166
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->isEtwsEmergencyUserAlert()Z

    #@b1
    move-result v8

    #@b2
    .line 167
    .restart local v8       #emergencyUserAlert:Z
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->isEtwsPopupAlert()Z

    #@b5
    move-result v7

    #@b6
    .line 168
    .restart local v7       #activatePopup:Z
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getEtwsWarningType()I

    #@b9
    move-result v13

    #@ba
    .line 169
    .restart local v13       #warningType:I
    new-instance v0, Landroid/telephony/SmsCbEtwsInfo;

    #@bc
    const/4 v2, 0x0

    #@bd
    invoke-direct {v0, v13, v8, v7, v2}, Landroid/telephony/SmsCbEtwsInfo;-><init>(IZZ[B)V

    #@c0
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->mEtwsInfo:Landroid/telephony/SmsCbEtwsInfo;

    #@c2
    .line 170
    const/4 v0, 0x0

    #@c3
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->mCmasInfo:Landroid/telephony/SmsCbCmasInfo;

    #@c5
    goto :goto_7f

    #@c6
    .line 145
    .end local v7           #activatePopup:Z
    .end local v8           #emergencyUserAlert:Z
    .end local v13           #warningType:I
    :cond_c6
    const/4 v0, 0x2

    #@c7
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->format:I

    #@c9
    .line 147
    const/4 v0, 0x0

    #@ca
    aget-byte v9, p1, v0

    #@cc
    .line 149
    .local v9, messageType:I
    const/4 v0, 0x1

    #@cd
    if-eq v9, v0, :cond_e8

    #@cf
    .line 150
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d1
    new-instance v2, Ljava/lang/StringBuilder;

    #@d3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d6
    const-string v3, "Unsupported message type "

    #@d8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v2

    #@dc
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@df
    move-result-object v2

    #@e0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v2

    #@e4
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e7
    throw v0

    #@e8
    .line 153
    :cond_e8
    const/4 v0, 0x1

    #@e9
    aget-byte v0, p1, v0

    #@eb
    and-int/lit16 v0, v0, 0xff

    #@ed
    shl-int/lit8 v0, v0, 0x8

    #@ef
    const/4 v2, 0x2

    #@f0
    aget-byte v2, p1, v2

    #@f2
    and-int/lit16 v2, v2, 0xff

    #@f4
    or-int/2addr v0, v2

    #@f5
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@f7
    .line 154
    const/4 v0, 0x3

    #@f8
    aget-byte v0, p1, v0

    #@fa
    and-int/lit16 v0, v0, 0xc0

    #@fc
    ushr-int/lit8 v0, v0, 0x6

    #@fe
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->geographicalScope:I

    #@100
    .line 155
    const/4 v0, 0x3

    #@101
    aget-byte v0, p1, v0

    #@103
    and-int/lit16 v0, v0, 0xff

    #@105
    shl-int/lit8 v0, v0, 0x8

    #@107
    const/4 v2, 0x4

    #@108
    aget-byte v2, p1, v2

    #@10a
    and-int/lit16 v2, v2, 0xff

    #@10c
    or-int/2addr v0, v2

    #@10d
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->serialNumber:I

    #@10f
    .line 156
    const/4 v0, 0x5

    #@110
    aget-byte v0, p1, v0

    #@112
    and-int/lit16 v0, v0, 0xff

    #@114
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->dataCodingScheme:I

    #@116
    .line 161
    const/4 v0, 0x1

    #@117
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->pageIndex:I

    #@119
    .line 162
    const/4 v0, 0x1

    #@11a
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->nrOfPages:I

    #@11c
    goto :goto_a8

    #@11d
    .line 171
    .end local v9           #messageType:I
    :cond_11d
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->isCmasMessage()Z

    #@120
    move-result v0

    #@121
    if-eqz v0, :cond_141

    #@123
    .line 172
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getCmasMessageClass()I

    #@126
    move-result v1

    #@127
    .line 173
    .local v1, messageClass:I
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getCmasSeverity()I

    #@12a
    move-result v4

    #@12b
    .line 174
    .local v4, severity:I
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getCmasUrgency()I

    #@12e
    move-result v5

    #@12f
    .line 175
    .local v5, urgency:I
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getCmasCertainty()I

    #@132
    move-result v6

    #@133
    .line 176
    .local v6, certainty:I
    const/4 v0, 0x0

    #@134
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->mEtwsInfo:Landroid/telephony/SmsCbEtwsInfo;

    #@136
    .line 177
    new-instance v0, Landroid/telephony/SmsCbCmasInfo;

    #@138
    const/4 v2, -0x1

    #@139
    const/4 v3, -0x1

    #@13a
    invoke-direct/range {v0 .. v6}, Landroid/telephony/SmsCbCmasInfo;-><init>(IIIIII)V

    #@13d
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->mCmasInfo:Landroid/telephony/SmsCbCmasInfo;

    #@13f
    goto/16 :goto_7f

    #@141
    .line 180
    .end local v1           #messageClass:I
    .end local v4           #severity:I
    .end local v5           #urgency:I
    .end local v6           #certainty:I
    :cond_141
    const/4 v0, 0x0

    #@142
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->mEtwsInfo:Landroid/telephony/SmsCbEtwsInfo;

    #@144
    .line 181
    const/4 v0, 0x0

    #@145
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->mCmasInfo:Landroid/telephony/SmsCbCmasInfo;

    #@147
    goto/16 :goto_7f
.end method

.method private getCmasCertainty()I
    .registers 2

    #@0
    .prologue
    .line 423
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@2
    packed-switch v0, :pswitch_data_c

    #@5
    .line 437
    const/4 v0, -0x1

    #@6
    :goto_6
    return v0

    #@7
    .line 428
    :pswitch_7
    const/4 v0, 0x0

    #@8
    goto :goto_6

    #@9
    .line 434
    :pswitch_9
    const/4 v0, 0x1

    #@a
    goto :goto_6

    #@b
    .line 423
    nop

    #@c
    :pswitch_data_c
    .packed-switch 0x1113
        :pswitch_7
        :pswitch_9
        :pswitch_7
        :pswitch_9
        :pswitch_7
        :pswitch_9
        :pswitch_7
        :pswitch_9
    .end packed-switch
.end method

.method private getCmasMessageClass()I
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    const/4 v2, 0x2

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v0, 0x0

    #@4
    const/4 v4, -0x1

    #@5
    .line 297
    const-string v5, "US"

    #@7
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@a
    move-result v5

    #@b
    if-eqz v5, :cond_20

    #@d
    .line 298
    iget v5, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@f
    packed-switch v5, :pswitch_data_34

    #@12
    move v0, v4

    #@13
    .line 361
    :goto_13
    :pswitch_13
    return v0

    #@14
    :pswitch_14
    move v0, v1

    #@15
    .line 304
    goto :goto_13

    #@16
    :pswitch_16
    move v0, v2

    #@17
    .line 313
    goto :goto_13

    #@18
    :pswitch_18
    move v0, v3

    #@19
    .line 316
    goto :goto_13

    #@1a
    .line 319
    :pswitch_1a
    const/4 v0, 0x4

    #@1b
    goto :goto_13

    #@1c
    .line 322
    :pswitch_1c
    const/4 v0, 0x5

    #@1d
    goto :goto_13

    #@1e
    .line 325
    :pswitch_1e
    const/4 v0, 0x6

    #@1f
    goto :goto_13

    #@20
    .line 332
    :cond_20
    iget v5, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@22
    packed-switch v5, :pswitch_data_52

    #@25
    move v0, v4

    #@26
    .line 361
    goto :goto_13

    #@27
    :pswitch_27
    move v0, v1

    #@28
    .line 338
    goto :goto_13

    #@29
    :pswitch_29
    move v0, v2

    #@2a
    .line 346
    goto :goto_13

    #@2b
    :pswitch_2b
    move v0, v3

    #@2c
    .line 349
    goto :goto_13

    #@2d
    .line 352
    :pswitch_2d
    const/4 v0, 0x4

    #@2e
    goto :goto_13

    #@2f
    .line 355
    :pswitch_2f
    const/4 v0, 0x5

    #@30
    goto :goto_13

    #@31
    .line 358
    :pswitch_31
    const/4 v0, 0x6

    #@32
    goto :goto_13

    #@33
    .line 298
    nop

    #@34
    :pswitch_data_34
    .packed-switch 0x1112
        :pswitch_13
        :pswitch_14
        :pswitch_14
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_18
        :pswitch_1a
        :pswitch_1c
        :pswitch_1e
    .end packed-switch

    #@52
    .line 332
    :pswitch_data_52
    .packed-switch 0x1112
        :pswitch_13
        :pswitch_27
        :pswitch_27
        :pswitch_29
        :pswitch_29
        :pswitch_29
        :pswitch_29
        :pswitch_29
        :pswitch_29
        :pswitch_2b
        :pswitch_2d
        :pswitch_2f
        :pswitch_31
    .end packed-switch
.end method

.method private getCmasSeverity()I
    .registers 2

    #@0
    .prologue
    .line 373
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@2
    packed-switch v0, :pswitch_data_c

    #@5
    .line 387
    const/4 v0, -0x1

    #@6
    :goto_6
    return v0

    #@7
    .line 376
    :pswitch_7
    const/4 v0, 0x0

    #@8
    goto :goto_6

    #@9
    .line 384
    :pswitch_9
    const/4 v0, 0x1

    #@a
    goto :goto_6

    #@b
    .line 373
    nop

    #@c
    :pswitch_data_c
    .packed-switch 0x1113
        :pswitch_7
        :pswitch_7
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method private getCmasUrgency()I
    .registers 2

    #@0
    .prologue
    .line 398
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@2
    packed-switch v0, :pswitch_data_c

    #@5
    .line 412
    const/4 v0, -0x1

    #@6
    :goto_6
    return v0

    #@7
    .line 403
    :pswitch_7
    const/4 v0, 0x0

    #@8
    goto :goto_6

    #@9
    .line 409
    :pswitch_9
    const/4 v0, 0x1

    #@a
    goto :goto_6

    #@b
    .line 398
    nop

    #@c
    :pswitch_data_c
    .packed-switch 0x1113
        :pswitch_7
        :pswitch_7
        :pswitch_9
        :pswitch_9
        :pswitch_7
        :pswitch_7
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method private getEtwsWarningType()I
    .registers 2

    #@0
    .prologue
    .line 287
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@2
    add-int/lit16 v0, v0, -0x1100

    #@4
    return v0
.end method

.method private isCmasMessage()Z
    .registers 3

    #@0
    .prologue
    .line 256
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@2
    const/16 v1, 0x1112

    #@4
    if-lt v0, v1, :cond_e

    #@6
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@8
    const/16 v1, 0x112f

    #@a
    if-gt v0, v1, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private isEtwsEmergencyUserAlert()Z
    .registers 2

    #@0
    .prologue
    .line 277
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->serialNumber:I

    #@2
    and-int/lit16 v0, v0, 0x2000

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private isEtwsMessage()Z
    .registers 3

    #@0
    .prologue
    .line 231
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@2
    const v1, 0xfff8

    #@5
    and-int/2addr v0, v1

    #@6
    const/16 v1, 0x1100

    #@8
    if-ne v0, v1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private isEtwsPopupAlert()Z
    .registers 2

    #@0
    .prologue
    .line 267
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->serialNumber:I

    #@2
    and-int/lit16 v0, v0, 0x1000

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method


# virtual methods
.method getCmasInfo()Landroid/telephony/SmsCbCmasInfo;
    .registers 2

    #@0
    .prologue
    .line 214
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->mCmasInfo:Landroid/telephony/SmsCbCmasInfo;

    #@2
    return-object v0
.end method

.method getDataCodingScheme()I
    .registers 2

    #@0
    .prologue
    .line 198
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->dataCodingScheme:I

    #@2
    return v0
.end method

.method getEtwsInfo()Landroid/telephony/SmsCbEtwsInfo;
    .registers 2

    #@0
    .prologue
    .line 210
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->mEtwsInfo:Landroid/telephony/SmsCbEtwsInfo;

    #@2
    return-object v0
.end method

.method getGeographicalScope()I
    .registers 2

    #@0
    .prologue
    .line 186
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->geographicalScope:I

    #@2
    return v0
.end method

.method getNumberOfPages()I
    .registers 2

    #@0
    .prologue
    .line 206
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->nrOfPages:I

    #@2
    return v0
.end method

.method getPageIndex()I
    .registers 2

    #@0
    .prologue
    .line 202
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->pageIndex:I

    #@2
    return v0
.end method

.method getSerialNumber()I
    .registers 2

    #@0
    .prologue
    .line 190
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->serialNumber:I

    #@2
    return v0
.end method

.method getServiceCategory()I
    .registers 2

    #@0
    .prologue
    .line 194
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@2
    return v0
.end method

.method isEmergencyMessage()Z
    .registers 3

    #@0
    .prologue
    .line 222
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@2
    const/16 v1, 0x1100

    #@4
    if-lt v0, v1, :cond_e

    #@6
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@8
    const/16 v1, 0x18ff

    #@a
    if-gt v0, v1, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method isEtwsPrimaryNotification()Z
    .registers 3

    #@0
    .prologue
    .line 240
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->format:I

    #@2
    const/4 v1, 0x3

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method isUmtsFormat()Z
    .registers 3

    #@0
    .prologue
    .line 248
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->format:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 443
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SmsCbHeader{GS="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->geographicalScope:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", serialNumber=0x"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->serialNumber:I

    #@19
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v1, ", messageIdentifier=0x"

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->messageIdentifier:I

    #@29
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    const-string v1, ", DCS=0x"

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->dataCodingScheme:I

    #@39
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, ", page "

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->pageIndex:I

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, " of "

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsCbHeader;->nrOfPages:I

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    const/16 v1, 0x7d

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    return-object v0
.end method
