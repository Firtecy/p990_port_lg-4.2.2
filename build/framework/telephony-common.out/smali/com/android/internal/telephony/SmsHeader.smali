.class public Lcom/android/internal/telephony/SmsHeader;
.super Ljava/lang/Object;
.source "SmsHeader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/SmsHeader$MiscElt;,
        Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;,
        Lcom/android/internal/telephony/SmsHeader$ReplyAddress;,
        Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;,
        Lcom/android/internal/telephony/SmsHeader$ConcatRef;,
        Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    }
.end annotation


# static fields
.field public static final ELT_ID_APPLICATION_PORT_ADDRESSING_16_BIT:I = 0x5

.field public static final ELT_ID_APPLICATION_PORT_ADDRESSING_8_BIT:I = 0x4

.field public static final ELT_ID_CHARACTER_SIZE_WVG_OBJECT:I = 0x19

.field public static final ELT_ID_COMPRESSION_CONTROL:I = 0x16

.field public static final ELT_ID_CONCATENATED_16_BIT_REFERENCE:I = 0x8

.field public static final ELT_ID_CONCATENATED_8_BIT_REFERENCE:I = 0x0

.field public static final ELT_ID_CONFIRMREAD_ELEMENT:I = 0x44

.field public static final ELT_ID_ENHANCED_VOICE_MAIL_INFORMATION:I = 0x23

.field public static final ELT_ID_EXTENDED_OBJECT:I = 0x14

.field public static final ELT_ID_EXTENDED_OBJECT_DATA_REQUEST_CMD:I = 0x1a

.field public static final ELT_ID_HYPERLINK_FORMAT_ELEMENT:I = 0x21

.field public static final ELT_ID_LARGE_ANIMATION:I = 0xe

.field public static final ELT_ID_LARGE_PICTURE:I = 0x10

.field public static final ELT_ID_NATIONAL_LANGUAGE_LOCKING_SHIFT:I = 0x25

.field public static final ELT_ID_NATIONAL_LANGUAGE_SINGLE_SHIFT:I = 0x24

.field public static final ELT_ID_OBJECT_DISTR_INDICATOR:I = 0x17

.field public static final ELT_ID_PREDEFINED_ANIMATION:I = 0xd

.field public static final ELT_ID_PREDEFINED_SOUND:I = 0xb

.field public static final ELT_ID_REPLY_ADDRESS_ELEMENT:I = 0x22

.field public static final ELT_ID_REUSED_EXTENDED_OBJECT:I = 0x15

.field public static final ELT_ID_RFC_822_EMAIL_HEADER:I = 0x20

.field public static final ELT_ID_SMALL_ANIMATION:I = 0xf

.field public static final ELT_ID_SMALL_PICTURE:I = 0x11

.field public static final ELT_ID_SMSC_CONTROL_PARAMS:I = 0x6

.field public static final ELT_ID_SPECIAL_SMS_MESSAGE_INDICATION:I = 0x1

.field public static final ELT_ID_STANDARD_WVG_OBJECT:I = 0x18

.field public static final ELT_ID_TEXT_FORMATTING:I = 0xa

.field public static final ELT_ID_UDH_SOURCE_INDICATION:I = 0x7

.field public static final ELT_ID_USER_DEFINED_SOUND:I = 0xc

.field public static final ELT_ID_USER_PROMPT_INDICATOR:I = 0x13

.field public static final ELT_ID_VARIABLE_PICTURE:I = 0x12

.field public static final ELT_ID_WIRELESS_CTRL_MSG_PROTOCOL:I = 0x9

.field public static final PORT_KTAPPMANAGER:I = 0xc216

.field public static final PORT_KTKAFAPPLICATION:I = 0xc221

.field public static final PORT_KTKAM:I = 0xc210

.field public static final PORT_KTSHOWMEMORY:I = 0xc212

.field public static final PORT_KTSHOWMESSENGER:I = 0xc215

.field public static final PORT_KTSHOWNAVIGATION:I = 0xc213

.field public static final PORT_KTSHOWOPENMAIL:I = 0xc211

.field public static final PORT_KTSHOWVIEDIO:I = 0xc214

.field public static final PORT_LGTMMSNOTIMESSAGE:I = 0xc015

.field public static final PORT_LGTOMAMMSNOTI:I = 0x1004

.field public static final PORT_SKT_COMMON_PUSH:I = 0x425c

.field public static final PORT_SKT_PORT_URL:I = 0x4261

.field public static final PORT_WAP_PUSH:I = 0xb84

.field public static final PORT_WAP_WSP:I = 0x23f0


# instance fields
.field public concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

.field public languageShiftTable:I

.field public languageTable:I

.field public miscEltList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/SmsHeader$MiscElt;",
            ">;"
        }
    .end annotation
.end field

.field public portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

.field public replyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

.field public smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

.field public specialSmsMsgList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 163
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 146
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/SmsHeader;->specialSmsMsgList:Ljava/util/ArrayList;

    #@b
    .line 155
    new-instance v0, Ljava/util/ArrayList;

    #@d
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/internal/telephony/SmsHeader;->miscEltList:Ljava/util/ArrayList;

    #@12
    .line 165
    iput v1, p0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@14
    .line 166
    iput v1, p0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@16
    .line 168
    return-void
.end method

.method public static fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;
    .registers 16
    .parameter "data"

    #@0
    .prologue
    const/4 v14, 0x1

    #@1
    const/4 v13, -0x1

    #@2
    const/4 v12, 0x0

    #@3
    .line 177
    new-instance v2, Ljava/io/ByteArrayInputStream;

    #@5
    invoke-direct {v2, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@8
    .line 178
    .local v2, inStream:Ljava/io/ByteArrayInputStream;
    new-instance v8, Lcom/android/internal/telephony/SmsHeader;

    #@a
    invoke-direct {v8}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@d
    .line 179
    .local v8, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    :cond_d
    :goto_d
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->available()I

    #@10
    move-result v10

    #@11
    if-lez v10, :cond_13d

    #@13
    .line 188
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@16
    move-result v1

    #@17
    .line 189
    .local v1, id:I
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@1a
    move-result v3

    #@1b
    .line 198
    .local v3, length:I
    sparse-switch v1, :sswitch_data_13e

    #@1e
    .line 284
    new-instance v6, Lcom/android/internal/telephony/SmsHeader$MiscElt;

    #@20
    invoke-direct {v6}, Lcom/android/internal/telephony/SmsHeader$MiscElt;-><init>()V

    #@23
    .line 285
    .local v6, miscElt:Lcom/android/internal/telephony/SmsHeader$MiscElt;
    iput v1, v6, Lcom/android/internal/telephony/SmsHeader$MiscElt;->id:I

    #@25
    .line 286
    new-array v10, v3, [B

    #@27
    iput-object v10, v6, Lcom/android/internal/telephony/SmsHeader$MiscElt;->data:[B

    #@29
    .line 287
    iget-object v10, v6, Lcom/android/internal/telephony/SmsHeader$MiscElt;->data:[B

    #@2b
    invoke-virtual {v2, v10, v12, v3}, Ljava/io/ByteArrayInputStream;->read([BII)I

    #@2e
    .line 288
    iget-object v10, v8, Lcom/android/internal/telephony/SmsHeader;->miscEltList:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@33
    goto :goto_d

    #@34
    .line 200
    .end local v6           #miscElt:Lcom/android/internal/telephony/SmsHeader$MiscElt;
    :sswitch_34
    new-instance v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@36
    invoke-direct {v0}, Lcom/android/internal/telephony/SmsHeader$ConcatRef;-><init>()V

    #@39
    .line 201
    .local v0, concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@3c
    move-result v10

    #@3d
    iput v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@3f
    .line 202
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@42
    move-result v10

    #@43
    iput v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@45
    .line 203
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@48
    move-result v10

    #@49
    iput v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@4b
    .line 204
    iput-boolean v14, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->isEightBits:Z

    #@4d
    .line 205
    iget v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@4f
    if-eqz v10, :cond_d

    #@51
    iget v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@53
    if-eqz v10, :cond_d

    #@55
    iget v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@57
    iget v11, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@59
    if-gt v10, v11, :cond_d

    #@5b
    .line 207
    iput-object v0, v8, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@5d
    goto :goto_d

    #@5e
    .line 211
    .end local v0           #concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    :sswitch_5e
    new-instance v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@60
    invoke-direct {v0}, Lcom/android/internal/telephony/SmsHeader$ConcatRef;-><init>()V

    #@63
    .line 212
    .restart local v0       #concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@66
    move-result v10

    #@67
    shl-int/lit8 v10, v10, 0x8

    #@69
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@6c
    move-result v11

    #@6d
    or-int/2addr v10, v11

    #@6e
    iput v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@70
    .line 213
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@73
    move-result v10

    #@74
    iput v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@76
    .line 214
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@79
    move-result v10

    #@7a
    iput v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@7c
    .line 215
    iput-boolean v12, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->isEightBits:Z

    #@7e
    .line 216
    iget v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@80
    if-eqz v10, :cond_d

    #@82
    iget v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@84
    if-eqz v10, :cond_d

    #@86
    iget v10, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@88
    iget v11, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@8a
    if-gt v10, v11, :cond_d

    #@8c
    .line 218
    iput-object v0, v8, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@8e
    goto/16 :goto_d

    #@90
    .line 222
    .end local v0           #concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    :sswitch_90
    new-instance v7, Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@92
    invoke-direct {v7}, Lcom/android/internal/telephony/SmsHeader$PortAddrs;-><init>()V

    #@95
    .line 223
    .local v7, portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@98
    move-result v10

    #@99
    iput v10, v7, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@9b
    .line 224
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@9e
    move-result v10

    #@9f
    iput v10, v7, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->origPort:I

    #@a1
    .line 225
    iput-boolean v14, v7, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->areEightBits:Z

    #@a3
    .line 226
    iput-object v7, v8, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@a5
    goto/16 :goto_d

    #@a7
    .line 229
    .end local v7           #portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    :sswitch_a7
    new-instance v7, Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@a9
    invoke-direct {v7}, Lcom/android/internal/telephony/SmsHeader$PortAddrs;-><init>()V

    #@ac
    .line 230
    .restart local v7       #portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@af
    move-result v10

    #@b0
    shl-int/lit8 v10, v10, 0x8

    #@b2
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@b5
    move-result v11

    #@b6
    or-int/2addr v10, v11

    #@b7
    iput v10, v7, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@b9
    .line 231
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@bc
    move-result v10

    #@bd
    shl-int/lit8 v10, v10, 0x8

    #@bf
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@c2
    move-result v11

    #@c3
    or-int/2addr v10, v11

    #@c4
    iput v10, v7, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->origPort:I

    #@c6
    .line 232
    iput-boolean v12, v7, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->areEightBits:Z

    #@c8
    .line 233
    iput-object v7, v8, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@ca
    goto/16 :goto_d

    #@cc
    .line 236
    .end local v7           #portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    :sswitch_cc
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@cf
    move-result v10

    #@d0
    iput v10, v8, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@d2
    goto/16 :goto_d

    #@d4
    .line 239
    :sswitch_d4
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@d7
    move-result v10

    #@d8
    iput v10, v8, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@da
    goto/16 :goto_d

    #@dc
    .line 243
    :sswitch_dc
    new-instance v5, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@de
    invoke-direct {v5}, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;-><init>()V

    #@e1
    .line 244
    .local v5, localSmsRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;
    iput v3, v5, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->length:I

    #@e3
    .line 245
    if-lez v3, :cond_fc

    #@e5
    .line 246
    new-array v10, v3, [B

    #@e7
    iput-object v10, v5, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@e9
    .line 249
    iget-object v10, v5, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@eb
    invoke-virtual {v2, v10, v12, v3}, Ljava/io/ByteArrayInputStream;->read([BII)I

    #@ee
    move-result v10

    #@ef
    if-ne v13, v10, :cond_f8

    #@f1
    .line 250
    const-string v10, "GSM:SmsHeader"

    #@f3
    const-string v11, "localSmsRead.confirmRead: The end of the stream has been reached"

    #@f5
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f8
    .line 254
    :cond_f8
    iput-object v5, v8, Lcom/android/internal/telephony/SmsHeader;->smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@fa
    goto/16 :goto_d

    #@fc
    .line 256
    :cond_fc
    const-string v10, "GSM"

    #@fe
    const-string v11, "Confirm read length is invalid"

    #@100
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@103
    goto/16 :goto_d

    #@105
    .line 262
    .end local v5           #localSmsRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;
    :sswitch_105
    new-instance v4, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@107
    invoke-direct {v4}, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;-><init>()V

    #@10a
    .line 263
    .local v4, localReplyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    new-array v10, v3, [B

    #@10c
    iput-object v10, v4, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->replyData:[B

    #@10e
    .line 266
    iget-object v10, v4, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->replyData:[B

    #@110
    invoke-virtual {v2, v10, v12, v3}, Ljava/io/ByteArrayInputStream;->read([BII)I

    #@113
    move-result v10

    #@114
    if-ne v13, v10, :cond_11c

    #@116
    .line 267
    iput v12, v4, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->addressLength:I

    #@118
    .line 271
    :goto_118
    iput-object v4, v8, Lcom/android/internal/telephony/SmsHeader;->replyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@11a
    goto/16 :goto_d

    #@11c
    .line 270
    :cond_11c
    iget-object v10, v4, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->replyData:[B

    #@11e
    aget-byte v10, v10, v12

    #@120
    and-int/lit16 v10, v10, 0xff

    #@122
    iput v10, v4, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->addressLength:I

    #@124
    goto :goto_118

    #@125
    .line 277
    .end local v4           #localReplyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    :sswitch_125
    new-instance v9, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;

    #@127
    invoke-direct {v9}, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;-><init>()V

    #@12a
    .line 278
    .local v9, specialSmsMsg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@12d
    move-result v10

    #@12e
    iput v10, v9, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;->msgIndType:I

    #@130
    .line 279
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->read()I

    #@133
    move-result v10

    #@134
    iput v10, v9, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;->msgCount:I

    #@136
    .line 280
    iget-object v10, v8, Lcom/android/internal/telephony/SmsHeader;->specialSmsMsgList:Ljava/util/ArrayList;

    #@138
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@13b
    goto/16 :goto_d

    #@13d
    .line 291
    .end local v1           #id:I
    .end local v3           #length:I
    .end local v9           #specialSmsMsg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    :cond_13d
    return-object v8

    #@13e
    .line 198
    :sswitch_data_13e
    .sparse-switch
        0x0 -> :sswitch_34
        0x1 -> :sswitch_125
        0x4 -> :sswitch_90
        0x5 -> :sswitch_a7
        0x8 -> :sswitch_5e
        0x22 -> :sswitch_105
        0x24 -> :sswitch_cc
        0x25 -> :sswitch_d4
        0x44 -> :sswitch_dc
    .end sparse-switch
.end method

.method public static toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B
    .registers 14
    .parameter "smsHeader"

    #@0
    .prologue
    const/4 v12, 0x2

    #@1
    const/4 v11, 0x4

    #@2
    const/4 v10, 0x0

    #@3
    const/4 v9, 0x1

    #@4
    .line 300
    iget-object v8, p0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@6
    if-nez v8, :cond_3e

    #@8
    iget-object v8, p0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@a
    if-nez v8, :cond_3e

    #@c
    iget-object v8, p0, Lcom/android/internal/telephony/SmsHeader;->specialSmsMsgList:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    #@11
    move-result v8

    #@12
    if-eqz v8, :cond_3e

    #@14
    iget-object v8, p0, Lcom/android/internal/telephony/SmsHeader;->miscEltList:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    #@19
    move-result v8

    #@1a
    if-eqz v8, :cond_3e

    #@1c
    iget v8, p0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@1e
    if-nez v8, :cond_3e

    #@20
    iget v8, p0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@22
    if-nez v8, :cond_3e

    #@24
    iget-object v8, p0, Lcom/android/internal/telephony/SmsHeader;->smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@26
    if-nez v8, :cond_3e

    #@28
    iget-object v8, p0, Lcom/android/internal/telephony/SmsHeader;->replyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@2a
    if-nez v8, :cond_3e

    #@2c
    iget-object v8, p0, Lcom/android/internal/telephony/SmsHeader;->miscEltList:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    #@31
    move-result v8

    #@32
    if-eqz v8, :cond_3e

    #@34
    iget v8, p0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@36
    if-nez v8, :cond_3e

    #@38
    iget v8, p0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@3a
    if-nez v8, :cond_3e

    #@3c
    .line 317
    const/4 v8, 0x0

    #@3d
    .line 400
    :goto_3d
    return-object v8

    #@3e
    .line 320
    :cond_3e
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    #@40
    const/16 v8, 0x8c

    #@42
    invoke-direct {v3, v8}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@45
    .line 322
    .local v3, outStream:Ljava/io/ByteArrayOutputStream;
    iget-object v0, p0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@47
    .line 323
    .local v0, concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    if-eqz v0, :cond_63

    #@49
    .line 324
    iget-boolean v8, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->isEightBits:Z

    #@4b
    if-eqz v8, :cond_f9

    #@4d
    .line 325
    invoke-virtual {v3, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@50
    .line 326
    const/4 v8, 0x3

    #@51
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@54
    .line 327
    iget v8, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@56
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@59
    .line 334
    :goto_59
    iget v8, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@5b
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@5e
    .line 335
    iget v8, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@60
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@63
    .line 337
    :cond_63
    iget-object v4, p0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@65
    .line 338
    .local v4, portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    if-eqz v4, :cond_7b

    #@67
    .line 339
    iget-boolean v8, v4, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->areEightBits:Z

    #@69
    if-eqz v8, :cond_111

    #@6b
    .line 340
    invoke-virtual {v3, v11}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@6e
    .line 341
    invoke-virtual {v3, v12}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@71
    .line 342
    iget v8, v4, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@73
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@76
    .line 343
    iget v8, v4, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->origPort:I

    #@78
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@7b
    .line 354
    :cond_7b
    :goto_7b
    iget-object v6, p0, Lcom/android/internal/telephony/SmsHeader;->smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@7d
    .line 355
    .local v6, smsread:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;
    if-eqz v6, :cond_95

    #@7f
    iget-object v8, v6, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@81
    if-eqz v8, :cond_95

    #@83
    iget-object v8, v6, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@85
    array-length v8, v8

    #@86
    if-eqz v8, :cond_95

    #@88
    .line 356
    const/16 v8, 0x44

    #@8a
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@8d
    .line 357
    invoke-virtual {v3, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@90
    .line 358
    iget-object v8, v6, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@92
    invoke-virtual {v3, v8, v10, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@95
    .line 364
    :cond_95
    iget-object v5, p0, Lcom/android/internal/telephony/SmsHeader;->replyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@97
    .line 365
    .local v5, replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    if-eqz v5, :cond_b0

    #@99
    .line 366
    const/16 v8, 0x22

    #@9b
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@9e
    .line 367
    iget-object v8, v5, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->replyData:[B

    #@a0
    array-length v8, v8

    #@a1
    add-int/lit8 v8, v8, 0x1

    #@a3
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@a6
    .line 368
    iget v8, v5, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->addressLength:I

    #@a8
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@ab
    .line 370
    :try_start_ab
    iget-object v8, v5, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->replyData:[B

    #@ad
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_b0
    .catch Ljava/io/IOException; {:try_start_ab .. :try_end_b0} :catch_162

    #@b0
    .line 377
    :cond_b0
    :goto_b0
    iget v8, p0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@b2
    if-eqz v8, :cond_c1

    #@b4
    .line 378
    const/16 v8, 0x24

    #@b6
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@b9
    .line 379
    invoke-virtual {v3, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@bc
    .line 380
    iget v8, p0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@be
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@c1
    .line 382
    :cond_c1
    iget v8, p0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@c3
    if-eqz v8, :cond_d2

    #@c5
    .line 383
    const/16 v8, 0x25

    #@c7
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@ca
    .line 384
    invoke-virtual {v3, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@cd
    .line 385
    iget v8, p0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@cf
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@d2
    .line 388
    :cond_d2
    iget-object v8, p0, Lcom/android/internal/telephony/SmsHeader;->specialSmsMsgList:Ljava/util/ArrayList;

    #@d4
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@d7
    move-result-object v1

    #@d8
    .local v1, i$:Ljava/util/Iterator;
    :goto_d8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@db
    move-result v8

    #@dc
    if-eqz v8, :cond_136

    #@de
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e1
    move-result-object v7

    #@e2
    check-cast v7, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;

    #@e4
    .line 389
    .local v7, specialSmsMsg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    invoke-virtual {v3, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@e7
    .line 390
    invoke-virtual {v3, v12}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@ea
    .line 391
    iget v8, v7, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;->msgIndType:I

    #@ec
    and-int/lit16 v8, v8, 0xff

    #@ee
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@f1
    .line 392
    iget v8, v7, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;->msgCount:I

    #@f3
    and-int/lit16 v8, v8, 0xff

    #@f5
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@f8
    goto :goto_d8

    #@f9
    .line 329
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v4           #portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    .end local v5           #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    .end local v6           #smsread:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;
    .end local v7           #specialSmsMsg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    :cond_f9
    const/16 v8, 0x8

    #@fb
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@fe
    .line 330
    invoke-virtual {v3, v11}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@101
    .line 331
    iget v8, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@103
    ushr-int/lit8 v8, v8, 0x8

    #@105
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@108
    .line 332
    iget v8, v0, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@10a
    and-int/lit16 v8, v8, 0xff

    #@10c
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@10f
    goto/16 :goto_59

    #@111
    .line 345
    .restart local v4       #portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    :cond_111
    const/4 v8, 0x5

    #@112
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@115
    .line 346
    invoke-virtual {v3, v11}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@118
    .line 347
    iget v8, v4, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@11a
    ushr-int/lit8 v8, v8, 0x8

    #@11c
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@11f
    .line 348
    iget v8, v4, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@121
    and-int/lit16 v8, v8, 0xff

    #@123
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@126
    .line 349
    iget v8, v4, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->origPort:I

    #@128
    ushr-int/lit8 v8, v8, 0x8

    #@12a
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@12d
    .line 350
    iget v8, v4, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->origPort:I

    #@12f
    and-int/lit16 v8, v8, 0xff

    #@131
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@134
    goto/16 :goto_7b

    #@136
    .line 395
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v5       #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    .restart local v6       #smsread:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;
    :cond_136
    iget-object v8, p0, Lcom/android/internal/telephony/SmsHeader;->miscEltList:Ljava/util/ArrayList;

    #@138
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13b
    move-result-object v1

    #@13c
    :goto_13c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@13f
    move-result v8

    #@140
    if-eqz v8, :cond_15c

    #@142
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@145
    move-result-object v2

    #@146
    check-cast v2, Lcom/android/internal/telephony/SmsHeader$MiscElt;

    #@148
    .line 396
    .local v2, miscElt:Lcom/android/internal/telephony/SmsHeader$MiscElt;
    iget v8, v2, Lcom/android/internal/telephony/SmsHeader$MiscElt;->id:I

    #@14a
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@14d
    .line 397
    iget-object v8, v2, Lcom/android/internal/telephony/SmsHeader$MiscElt;->data:[B

    #@14f
    array-length v8, v8

    #@150
    invoke-virtual {v3, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@153
    .line 398
    iget-object v8, v2, Lcom/android/internal/telephony/SmsHeader$MiscElt;->data:[B

    #@155
    iget-object v9, v2, Lcom/android/internal/telephony/SmsHeader$MiscElt;->data:[B

    #@157
    array-length v9, v9

    #@158
    invoke-virtual {v3, v8, v10, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@15b
    goto :goto_13c

    #@15c
    .line 400
    .end local v2           #miscElt:Lcom/android/internal/telephony/SmsHeader$MiscElt;
    :cond_15c
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@15f
    move-result-object v8

    #@160
    goto/16 :goto_3d

    #@162
    .line 371
    .end local v1           #i$:Ljava/util/Iterator;
    :catch_162
    move-exception v8

    #@163
    goto/16 :goto_b0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 405
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 406
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v4, "UserDataHeader "

    #@7
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 407
    const-string v4, "{ ConcatRef "

    #@c
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 408
    iget-object v4, p0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@11
    if-nez v4, :cond_ab

    #@13
    .line 409
    const-string v4, "unset"

    #@15
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 417
    :goto_18
    const-string v4, ", PortAddrs "

    #@1a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 418
    iget-object v4, p0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@1f
    if-nez v4, :cond_11a

    #@21
    .line 419
    const-string v4, "unset"

    #@23
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 426
    :goto_26
    iget v4, p0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@28
    if-eqz v4, :cond_42

    #@2a
    .line 427
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, ", languageShiftTable="

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    iget v5, p0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    .line 429
    :cond_42
    iget v4, p0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@44
    if-eqz v4, :cond_5e

    #@46
    .line 430
    new-instance v4, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v5, ", languageTable="

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    iget v5, p0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@53
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v4

    #@5b
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    .line 432
    :cond_5e
    iget-object v4, p0, Lcom/android/internal/telephony/SmsHeader;->specialSmsMsgList:Ljava/util/ArrayList;

    #@60
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@63
    move-result-object v1

    #@64
    .local v1, i$:Ljava/util/Iterator;
    :goto_64
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@67
    move-result v4

    #@68
    if-eqz v4, :cond_16f

    #@6a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6d
    move-result-object v3

    #@6e
    check-cast v3, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;

    #@70
    .line 433
    .local v3, specialSmsMsg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    const-string v4, ", SpecialSmsMsg "

    #@72
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    .line 434
    new-instance v4, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v5, "{ msgIndType="

    #@7c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    iget v5, v3, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;->msgIndType:I

    #@82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    .line 435
    new-instance v4, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v5, ", msgCount="

    #@94
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v4

    #@98
    iget v5, v3, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;->msgCount:I

    #@9a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v4

    #@9e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v4

    #@a2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    .line 436
    const-string v4, " }"

    #@a7
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    goto :goto_64

    #@ab
    .line 411
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #specialSmsMsg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    :cond_ab
    new-instance v4, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v5, "{ refNumber="

    #@b2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v4

    #@b6
    iget-object v5, p0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@b8
    iget v5, v5, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@ba
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v4

    #@be
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v4

    #@c2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    .line 412
    new-instance v4, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v5, ", msgCount="

    #@cc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v4

    #@d0
    iget-object v5, p0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@d2
    iget v5, v5, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@d4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v4

    #@d8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v4

    #@dc
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    .line 413
    new-instance v4, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v5, ", seqNumber="

    #@e6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v4

    #@ea
    iget-object v5, p0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@ec
    iget v5, v5, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@ee
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v4

    #@f2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f5
    move-result-object v4

    #@f6
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    .line 414
    new-instance v4, Ljava/lang/StringBuilder;

    #@fb
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@fe
    const-string v5, ", isEightBits="

    #@100
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v4

    #@104
    iget-object v5, p0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@106
    iget-boolean v5, v5, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->isEightBits:Z

    #@108
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v4

    #@10c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10f
    move-result-object v4

    #@110
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    .line 415
    const-string v4, " }"

    #@115
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    goto/16 :goto_18

    #@11a
    .line 421
    :cond_11a
    new-instance v4, Ljava/lang/StringBuilder;

    #@11c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11f
    const-string v5, "{ destPort="

    #@121
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v4

    #@125
    iget-object v5, p0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@127
    iget v5, v5, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@129
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v4

    #@12d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v4

    #@131
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    .line 422
    new-instance v4, Ljava/lang/StringBuilder;

    #@136
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@139
    const-string v5, ", origPort="

    #@13b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v4

    #@13f
    iget-object v5, p0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@141
    iget v5, v5, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->origPort:I

    #@143
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@146
    move-result-object v4

    #@147
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14a
    move-result-object v4

    #@14b
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    .line 423
    new-instance v4, Ljava/lang/StringBuilder;

    #@150
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@153
    const-string v5, ", areEightBits="

    #@155
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v4

    #@159
    iget-object v5, p0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@15b
    iget-boolean v5, v5, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->areEightBits:Z

    #@15d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@160
    move-result-object v4

    #@161
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@164
    move-result-object v4

    #@165
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    .line 424
    const-string v4, " }"

    #@16a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    goto/16 :goto_26

    #@16f
    .line 439
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_16f
    iget-object v4, p0, Lcom/android/internal/telephony/SmsHeader;->specialSmsMsgList:Ljava/util/ArrayList;

    #@171
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@174
    move-result-object v1

    #@175
    :goto_175
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@178
    move-result v4

    #@179
    if-eqz v4, :cond_1bc

    #@17b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17e
    move-result-object v3

    #@17f
    check-cast v3, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;

    #@181
    .line 440
    .restart local v3       #specialSmsMsg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    const-string v4, ", SpecialSmsMsg "

    #@183
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@186
    .line 441
    new-instance v4, Ljava/lang/StringBuilder;

    #@188
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@18b
    const-string v5, "{ msgIndType="

    #@18d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@190
    move-result-object v4

    #@191
    iget v5, v3, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;->msgIndType:I

    #@193
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@196
    move-result-object v4

    #@197
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19a
    move-result-object v4

    #@19b
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    .line 442
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a3
    const-string v5, ", msgCount="

    #@1a5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v4

    #@1a9
    iget v5, v3, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;->msgCount:I

    #@1ab
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ae
    move-result-object v4

    #@1af
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b2
    move-result-object v4

    #@1b3
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b6
    .line 443
    const-string v4, " }"

    #@1b8
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    goto :goto_175

    #@1bc
    .line 446
    .end local v3           #specialSmsMsg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    :cond_1bc
    iget-object v4, p0, Lcom/android/internal/telephony/SmsHeader;->miscEltList:Ljava/util/ArrayList;

    #@1be
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1c1
    move-result-object v1

    #@1c2
    :goto_1c2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1c5
    move-result v4

    #@1c6
    if-eqz v4, :cond_226

    #@1c8
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1cb
    move-result-object v2

    #@1cc
    check-cast v2, Lcom/android/internal/telephony/SmsHeader$MiscElt;

    #@1ce
    .line 447
    .local v2, miscElt:Lcom/android/internal/telephony/SmsHeader$MiscElt;
    const-string v4, ", MiscElt "

    #@1d0
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d3
    .line 448
    new-instance v4, Ljava/lang/StringBuilder;

    #@1d5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d8
    const-string v5, "{ id="

    #@1da
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dd
    move-result-object v4

    #@1de
    iget v5, v2, Lcom/android/internal/telephony/SmsHeader$MiscElt;->id:I

    #@1e0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e3
    move-result-object v4

    #@1e4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e7
    move-result-object v4

    #@1e8
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    .line 449
    new-instance v4, Ljava/lang/StringBuilder;

    #@1ed
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f0
    const-string v5, ", length="

    #@1f2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v4

    #@1f6
    iget-object v5, v2, Lcom/android/internal/telephony/SmsHeader$MiscElt;->data:[B

    #@1f8
    array-length v5, v5

    #@1f9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v4

    #@1fd
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@200
    move-result-object v4

    #@201
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@204
    .line 450
    new-instance v4, Ljava/lang/StringBuilder;

    #@206
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@209
    const-string v5, ", data="

    #@20b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20e
    move-result-object v4

    #@20f
    iget-object v5, v2, Lcom/android/internal/telephony/SmsHeader$MiscElt;->data:[B

    #@211
    invoke-static {v5}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@214
    move-result-object v5

    #@215
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@218
    move-result-object v4

    #@219
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21c
    move-result-object v4

    #@21d
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@220
    .line 451
    const-string v4, " }"

    #@222
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@225
    goto :goto_1c2

    #@226
    .line 453
    .end local v2           #miscElt:Lcom/android/internal/telephony/SmsHeader$MiscElt;
    :cond_226
    const-string v4, " }"

    #@228
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22b
    .line 454
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22e
    move-result-object v4

    #@22f
    return-object v4
.end method
