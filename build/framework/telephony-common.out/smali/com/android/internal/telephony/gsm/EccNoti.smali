.class public Lcom/android/internal/telephony/gsm/EccNoti;
.super Ljava/lang/Object;
.source "EccNoti.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gsm/EccNoti$1;,
        Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;
    }
.end annotation


# static fields
.field public static final ACTIVATION_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/activation"

.field public static final ACTIVATION_VALUE:I = 0x1

.field public static final ALL_VALUE:I = 0x0

.field public static final AUTHORITY:Ljava/lang/String; = "ecc_notification"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DATABASE_NAME:Ljava/lang/String; = "ecc_noti.db"

.field public static final DATABASE_VERSION:I = 0x1

#the value of this static final field might be set in the static constructor
.field private static final DBG:Z = false

.field public static final DTT_CODE_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/dtt_code"

.field public static final DTT_CODE_VALUE:I = 0x3

.field public static final DTT_DIGIT_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/dtt_digit"

.field public static final DTT_DIGIT_VALUE:I = 0x4

.field public static final ECCNOTI_TABLE_NAME:Ljava/lang/String; = "ecc_noti"

.field public static final KEY_ACTIVATION:Ljava/lang/String; = "activation"

.field public static final KEY_DTT_CODE:Ljava/lang/String; = "dtt_code"

.field public static final KEY_DTT_DIGIT:Ljava/lang/String; = "dtt_digit"

.field public static final KEY_ID:Ljava/lang/String; = "_id"

.field public static final KEY_MMI:Ljava/lang/String; = "mmi"

.field public static final MMI_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/mmi"

.field public static final MMI_VALUE:I = 0x2

.field private static SENDING_ECN:Z = false

.field private static SENDING_UVR:Z = false

.field private static final TAG:Ljava/lang/String; = "ECC_NOTI"

.field public static final default_activation:Ljava/lang/String; = "ON"

.field public static final default_dtt_code:Ljava/lang/String; = "030708090B0C0E0F24405B5C5D5E5F601C1D1E1F1012131415161718191A"

.field public static final default_dtt_digit:Ljava/lang/String; = "897023415618307564290729486153"

.field public static final default_mmi:Ljava/lang/String; = "#119#"

.field static eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data; = null

.field private static mDecoded:Ljava/lang/String; = null

.field private static mMmiCode:Lcom/android/internal/telephony/gsm/GsmMmiCode; = null

.field private static mPhone:Lcom/android/internal/telephony/gsm/GSMPhone; = null

.field static mResolver:Landroid/content/ContentResolver; = null

.field private static mUiccapp:Lcom/android/internal/telephony/uicc/UiccCardApplication; = null

.field private static misProcessing:Z = false

.field private static notCloseCallScreen:Z = false

.field static final parsingEccNoti:Ljava/lang/String; = ""


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 55
    const-string v0, "ro.build.type"

    #@3
    const-string v2, "user"

    #@5
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    const-string v2, "user"

    #@b
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_25

    #@11
    move v0, v1

    #@12
    :goto_12
    sput-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@14
    .line 82
    const-string v0, "content://ecc_notification/ecc_noti"

    #@16
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@19
    move-result-object v0

    #@1a
    sput-object v0, Lcom/android/internal/telephony/gsm/EccNoti;->CONTENT_URI:Landroid/net/Uri;

    #@1c
    .line 125
    sput-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@1e
    .line 126
    sput-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_UVR:Z

    #@20
    .line 127
    sput-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->misProcessing:Z

    #@22
    .line 128
    sput-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->notCloseCallScreen:Z

    #@24
    return-void

    #@25
    .line 55
    :cond_25
    const/4 v0, 0x1

    #@26
    goto :goto_12
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 108
    return-void
.end method

.method public static CompleteECCNoti()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 419
    sget-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    const-string v0, "ECC_NOTI"

    #@7
    const-string v1, "CompleteECCNoti "

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 421
    :cond_c
    sput-boolean v2, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@e
    .line 422
    sput-boolean v2, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_UVR:Z

    #@10
    .line 423
    sput-boolean v2, Lcom/android/internal/telephony/gsm/EccNoti;->misProcessing:Z

    #@12
    .line 424
    return-void
.end method

.method private static DecodeUNR(Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "data"

    #@0
    .prologue
    const/16 v11, 0x10

    #@2
    const/4 v7, 0x0

    #@3
    .line 507
    new-instance v0, Ljava/lang/String;

    #@5
    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    #@8
    .line 512
    .local v0, Decoded:Ljava/lang/String;
    invoke-static {p0}, Lcom/android/internal/telephony/gsm/EccNoti;->StringToInt(Ljava/lang/String;)[I

    #@b
    move-result-object v2

    #@c
    .line 514
    .local v2, UNRHexa:[I
    const/4 v3, 0x0

    #@d
    .local v3, i:I
    :goto_d
    array-length v8, v2

    #@e
    if-ge v3, v8, :cond_55

    #@10
    .line 515
    sget-boolean v8, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@12
    if-eqz v8, :cond_52

    #@14
    const-string v8, "ECC_NOTI"

    #@16
    new-instance v9, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v10, "DecodeUNR input Charater ( "

    #@1d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v9

    #@21
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@24
    move-result v10

    #@25
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@28
    move-result-object v9

    #@29
    const-string v10, " ) in Integer (0x"

    #@2b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v9

    #@2f
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@32
    move-result v10

    #@33
    invoke-static {v10, v11}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    #@36
    move-result-object v10

    #@37
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v9

    #@3b
    const-string v10, " ) to GSM Code ==>  0x"

    #@3d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    aget v10, v2, v3

    #@43
    invoke-static {v10, v11}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    #@46
    move-result-object v10

    #@47
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v9

    #@4b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v9

    #@4f
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 514
    :cond_52
    add-int/lit8 v3, v3, 0x1

    #@54
    goto :goto_d

    #@55
    .line 522
    :cond_55
    const/4 v6, 0x0

    #@56
    .local v6, n:I
    :goto_56
    const/4 v8, 0x3

    #@57
    if-ge v6, v8, :cond_136

    #@59
    .line 524
    aget v8, v2, v6

    #@5b
    sget-object v9, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->dtt_code:[I

    #@5d
    invoke-static {v8, v9}, Lcom/android/internal/telephony/gsm/EccNoti;->match(I[I)I

    #@60
    move-result v5

    #@61
    .line 525
    .local v5, matchingidx:I
    sget-boolean v8, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@63
    if-eqz v8, :cond_8a

    #@65
    const-string v8, "ECC_NOTI"

    #@67
    new-instance v9, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v10, "Digit Index tmpIdx : "

    #@6e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v9

    #@72
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v9

    #@76
    const-string v10, " max length : "

    #@78
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v9

    #@7c
    sget-object v10, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->dtt_digit:[C

    #@7e
    array-length v10, v10

    #@7f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v9

    #@83
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v9

    #@87
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 528
    :cond_8a
    if-ltz v5, :cond_91

    #@8c
    sget-object v8, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->dtt_digit:[C

    #@8e
    array-length v8, v8

    #@8f
    if-lt v5, v8, :cond_93

    #@91
    :cond_91
    move-object v0, v7

    #@92
    .line 557
    .end local v0           #Decoded:Ljava/lang/String;
    .end local v5           #matchingidx:I
    :cond_92
    :goto_92
    return-object v0

    #@93
    .line 532
    .restart local v0       #Decoded:Ljava/lang/String;
    .restart local v5       #matchingidx:I
    :cond_93
    sget-object v8, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->dtt_digit:[C

    #@95
    aget-char v8, v8, v5

    #@97
    invoke-static {v8}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    #@9a
    move-result-object v8

    #@9b
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@9e
    move-result v1

    #@9f
    .line 533
    .local v1, Digitindex:I
    sget-boolean v8, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@a1
    if-eqz v8, :cond_c6

    #@a3
    const-string v8, "ECC_NOTI"

    #@a5
    new-instance v9, Ljava/lang/StringBuilder;

    #@a7
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@aa
    const-string v10, "Digit Index Digitindex : "

    #@ac
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v9

    #@b0
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v9

    #@b4
    const-string v10, " max length : "

    #@b6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v9

    #@ba
    array-length v10, v2

    #@bb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@be
    move-result-object v9

    #@bf
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v9

    #@c3
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c6
    .line 536
    :cond_c6
    if-ltz v1, :cond_cb

    #@c8
    array-length v8, v2

    #@c9
    if-lt v1, v8, :cond_cd

    #@cb
    :cond_cb
    move-object v0, v7

    #@cc
    .line 537
    goto :goto_92

    #@cd
    .line 540
    :cond_cd
    add-int/lit8 v8, v1, 0x3

    #@cf
    aget v4, v2, v8

    #@d1
    .line 541
    .local v4, machingch:I
    sget-boolean v8, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@d3
    if-eqz v8, :cond_ed

    #@d5
    const-string v8, "ECC_NOTI"

    #@d7
    new-instance v9, Ljava/lang/StringBuilder;

    #@d9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@dc
    const-string v10, "Digit Index USSD Ch machingch : "

    #@de
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v9

    #@e2
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v9

    #@e6
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e9
    move-result-object v9

    #@ea
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ed
    .line 544
    :cond_ed
    sget-object v8, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->dtt_code:[I

    #@ef
    invoke-static {v4, v8}, Lcom/android/internal/telephony/gsm/EccNoti;->match(I[I)I

    #@f2
    move-result v5

    #@f3
    .line 545
    sget-boolean v8, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@f5
    if-eqz v8, :cond_11c

    #@f7
    const-string v8, "ECC_NOTI"

    #@f9
    new-instance v9, Ljava/lang/StringBuilder;

    #@fb
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@fe
    const-string v10, "Digit Index USSD matchingidx : "

    #@100
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v9

    #@104
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@107
    move-result-object v9

    #@108
    const-string v10, " max length : "

    #@10a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v9

    #@10e
    sget-object v10, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->dtt_digit:[C

    #@110
    array-length v10, v10

    #@111
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@114
    move-result-object v9

    #@115
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v9

    #@119
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 548
    :cond_11c
    if-ltz v5, :cond_123

    #@11e
    sget-object v8, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->dtt_digit:[C

    #@120
    array-length v8, v8

    #@121
    if-lt v5, v8, :cond_126

    #@123
    :cond_123
    move-object v0, v7

    #@124
    .line 549
    goto/16 :goto_92

    #@126
    .line 552
    :cond_126
    sget-object v8, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->dtt_digit:[C

    #@128
    aget-char v8, v8, v5

    #@12a
    invoke-static {v8}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    #@12d
    move-result-object v8

    #@12e
    invoke-virtual {v0, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@131
    move-result-object v0

    #@132
    .line 522
    add-int/lit8 v6, v6, 0x1

    #@134
    goto/16 :goto_56

    #@136
    .line 555
    .end local v1           #Digitindex:I
    .end local v4           #machingch:I
    .end local v5           #matchingidx:I
    :cond_136
    sget-boolean v7, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@138
    if-eqz v7, :cond_92

    #@13a
    const-string v7, "ECC_NOTI"

    #@13c
    new-instance v8, Ljava/lang/StringBuilder;

    #@13e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@141
    const-string v9, "Result( DecodeUNR ) ==>   "

    #@143
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v8

    #@147
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v8

    #@14b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14e
    move-result-object v8

    #@14f
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@152
    goto/16 :goto_92
.end method

.method public static On(Ljava/lang/String;)Z
    .registers 8
    .parameter "Operation_Code"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 219
    new-instance v4, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@4
    invoke-direct {v4}, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;-><init>()V

    #@7
    sput-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@9
    .line 226
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@b
    if-eqz v4, :cond_11

    #@d
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->mResolver:Landroid/content/ContentResolver;

    #@f
    if-nez v4, :cond_31

    #@11
    .line 227
    :cond_11
    sget-boolean v2, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@13
    if-eqz v2, :cond_2f

    #@15
    const-string v2, "ECC_NOTI"

    #@17
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "EccNoti Not allocate EccNot_Data resolver "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    sget-object v5, Lcom/android/internal/telephony/gsm/EccNoti;->mResolver:Landroid/content/ContentResolver;

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    :cond_2f
    move v2, v3

    #@30
    .line 263
    :cond_30
    :goto_30
    return v2

    #@31
    .line 232
    :cond_31
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@33
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->mResolver:Landroid/content/ContentResolver;

    #@35
    const-string v5, "activation"

    #@37
    invoke-static {v4, v5}, Lcom/android/internal/telephony/gsm/EccNoti;->read(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    sput-object v4, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->activation:Ljava/lang/String;

    #@3d
    .line 234
    sget-boolean v4, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@3f
    if-eqz v4, :cond_67

    #@41
    const-string v4, "ECC_NOTI"

    #@43
    new-instance v5, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v6, "TMO OPerattion: "

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    const-string v6, "Enable : "

    #@54
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    sget-object v6, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@5a
    sget-object v6, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->activation:Ljava/lang/String;

    #@5c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 236
    :cond_67
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@69
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->activation:Ljava/lang/String;

    #@6b
    const-string v5, "ON"

    #@6d
    invoke-static {v4, v5}, Landroid/telephony/PhoneNumberUtils;->compareStrictly(Ljava/lang/String;Ljava/lang/String;)Z

    #@70
    move-result v4

    #@71
    if-eqz v4, :cond_d1

    #@73
    invoke-static {p0}, Lcom/android/internal/telephony/gsm/EccNoti;->checkOperator_Code(Ljava/lang/String;)Z

    #@76
    move-result v4

    #@77
    if-eqz v4, :cond_d1

    #@79
    .line 242
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@7b
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->mResolver:Landroid/content/ContentResolver;

    #@7d
    const-string v5, "mmi"

    #@7f
    invoke-static {v4, v5}, Lcom/android/internal/telephony/gsm/EccNoti;->read(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@82
    move-result-object v4

    #@83
    sput-object v4, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->mmi:Ljava/lang/String;

    #@85
    .line 243
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->mResolver:Landroid/content/ContentResolver;

    #@87
    const-string v5, "dtt_code"

    #@89
    invoke-static {v4, v5}, Lcom/android/internal/telephony/gsm/EccNoti;->read(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@8c
    move-result-object v0

    #@8d
    .line 244
    .local v0, Temp_DTT_Code:Ljava/lang/String;
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->mResolver:Landroid/content/ContentResolver;

    #@8f
    const-string v5, "dtt_digit"

    #@91
    invoke-static {v4, v5}, Lcom/android/internal/telephony/gsm/EccNoti;->read(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@94
    move-result-object v1

    #@95
    .line 245
    .local v1, Temp_DTT_Digit:Ljava/lang/String;
    if-eqz v0, :cond_a9

    #@97
    if-eqz v1, :cond_a9

    #@99
    .line 246
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@9b
    invoke-static {v0}, Lcom/android/internal/telephony/gsm/EccNoti;->getGsmAlpaArrayFromDTTString(Ljava/lang/String;)[I

    #@9e
    move-result-object v4

    #@9f
    sput-object v4, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->dtt_code:[I

    #@a1
    .line 247
    sget-object v4, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@a3
    invoke-static {v1}, Lcom/android/internal/telephony/gsm/EccNoti;->StringtoChar(Ljava/lang/String;)[C

    #@a6
    move-result-object v4

    #@a7
    sput-object v4, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->dtt_digit:[C

    #@a9
    .line 252
    :cond_a9
    sput-boolean v2, Lcom/android/internal/telephony/gsm/EccNoti;->misProcessing:Z

    #@ab
    .line 254
    sput-boolean v3, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@ad
    .line 255
    sput-boolean v3, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_UVR:Z

    #@af
    .line 257
    sget-boolean v3, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@b1
    if-eqz v3, :cond_30

    #@b3
    const-string v3, "ECC_NOTI"

    #@b5
    new-instance v4, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v5, "ECC Noti [On] "

    #@bc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v4

    #@c0
    sget-object v5, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@c2
    sget-object v5, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->mmi:Ljava/lang/String;

    #@c4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v4

    #@c8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v4

    #@cc
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    goto/16 :goto_30

    #@d1
    .line 262
    .end local v0           #Temp_DTT_Code:Ljava/lang/String;
    .end local v1           #Temp_DTT_Digit:Ljava/lang/String;
    :cond_d1
    sget-boolean v2, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@d3
    if-eqz v2, :cond_dc

    #@d5
    const-string v2, "ECC_NOTI"

    #@d7
    const-string v4, "ECC Noti [OFF] "

    #@d9
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    :cond_dc
    move v2, v3

    #@dd
    .line 263
    goto/16 :goto_30
.end method

.method private static StringToInt(Ljava/lang/String;)[I
    .registers 11
    .parameter "str"

    #@0
    .prologue
    .line 581
    const-string v8, "gsm.eccnoti.dcs"

    #@2
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 582
    .local v1, dcs:Ljava/lang/String;
    const-string v8, "gsm.eccnoti.dcs"

    #@8
    const-string v9, ""

    #@a
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 583
    if-eqz v1, :cond_1f

    #@f
    const-string v8, "0F"

    #@11
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v8

    #@15
    if-nez v8, :cond_1f

    #@17
    const-string v8, ""

    #@19
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v8

    #@1d
    if-eqz v8, :cond_34

    #@1f
    .line 584
    :cond_1f
    invoke-static {p0}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm8BitPacked(Ljava/lang/String;)[B

    #@22
    move-result-object v4

    #@23
    .line 585
    .local v4, gStr:[B
    if-eqz v4, :cond_34

    #@25
    .line 586
    array-length v3, v4

    #@26
    .line 587
    .local v3, gLen:I
    if-lez v3, :cond_34

    #@28
    .line 588
    new-array v2, v3, [I

    #@2a
    .line 589
    .local v2, gHexa:[I
    const/4 v5, 0x0

    #@2b
    .local v5, i:I
    :goto_2b
    if-ge v5, v3, :cond_47

    #@2d
    .line 590
    aget-byte v8, v4, v5

    #@2f
    aput v8, v2, v5

    #@31
    .line 589
    add-int/lit8 v5, v5, 0x1

    #@33
    goto :goto_2b

    #@34
    .line 598
    .end local v2           #gHexa:[I
    .end local v3           #gLen:I
    .end local v4           #gStr:[B
    .end local v5           #i:I
    :cond_34
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@37
    move-result v7

    #@38
    .line 599
    .local v7, len:I
    new-array v0, v7, [I

    #@3a
    .line 601
    .local v0, Hexa:[I
    const/4 v6, 0x0

    #@3b
    .local v6, idx:I
    :goto_3b
    if-ge v6, v7, :cond_46

    #@3d
    .line 602
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    #@40
    move-result v8

    #@41
    aput v8, v0, v6

    #@43
    .line 601
    add-int/lit8 v6, v6, 0x1

    #@45
    goto :goto_3b

    #@46
    :cond_46
    move-object v2, v0

    #@47
    .line 604
    .end local v0           #Hexa:[I
    .end local v6           #idx:I
    .end local v7           #len:I
    :cond_47
    return-object v2
.end method

.method private static StringtoChar(Ljava/lang/String;)[C
    .registers 5
    .parameter "str"

    #@0
    .prologue
    .line 612
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v1

    #@4
    .line 613
    .local v1, len:I
    new-array v2, v1, [C

    #@6
    .line 614
    .local v2, result:[C
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_12

    #@9
    .line 615
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v3

    #@d
    aput-char v3, v2, v0

    #@f
    .line 614
    add-int/lit8 v0, v0, 0x1

    #@11
    goto :goto_7

    #@12
    .line 618
    :cond_12
    return-object v2
.end method

.method private static checkOperator_Code(Ljava/lang/String;)Z
    .registers 7
    .parameter "mcc_mnc"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 649
    const/16 v4, 0x12

    #@4
    new-array v0, v4, [Ljava/lang/String;

    #@6
    const-string v4, "310160"

    #@8
    aput-object v4, v0, v2

    #@a
    const-string v4, "310200"

    #@c
    aput-object v4, v0, v3

    #@e
    const/4 v4, 0x2

    #@f
    const-string v5, "310210"

    #@11
    aput-object v5, v0, v4

    #@13
    const/4 v4, 0x3

    #@14
    const-string v5, "310220"

    #@16
    aput-object v5, v0, v4

    #@18
    const/4 v4, 0x4

    #@19
    const-string v5, "310230"

    #@1b
    aput-object v5, v0, v4

    #@1d
    const/4 v4, 0x5

    #@1e
    const-string v5, "310240"

    #@20
    aput-object v5, v0, v4

    #@22
    const/4 v4, 0x6

    #@23
    const-string v5, "310250"

    #@25
    aput-object v5, v0, v4

    #@27
    const/4 v4, 0x7

    #@28
    const-string v5, "310260"

    #@2a
    aput-object v5, v0, v4

    #@2c
    const/16 v4, 0x8

    #@2e
    const-string v5, "310270"

    #@30
    aput-object v5, v0, v4

    #@32
    const/16 v4, 0x9

    #@34
    const-string v5, "310300"

    #@36
    aput-object v5, v0, v4

    #@38
    const/16 v4, 0xa

    #@3a
    const-string v5, "310310"

    #@3c
    aput-object v5, v0, v4

    #@3e
    const/16 v4, 0xb

    #@40
    const-string v5, "310490"

    #@42
    aput-object v5, v0, v4

    #@44
    const/16 v4, 0xc

    #@46
    const-string v5, "310530"

    #@48
    aput-object v5, v0, v4

    #@4a
    const/16 v4, 0xd

    #@4c
    const-string v5, "310580"

    #@4e
    aput-object v5, v0, v4

    #@50
    const/16 v4, 0xe

    #@52
    const-string v5, "310590"

    #@54
    aput-object v5, v0, v4

    #@56
    const/16 v4, 0xf

    #@58
    const-string v5, "310640"

    #@5a
    aput-object v5, v0, v4

    #@5c
    const/16 v4, 0x10

    #@5e
    const-string v5, "310660"

    #@60
    aput-object v5, v0, v4

    #@62
    const/16 v4, 0x11

    #@64
    const-string v5, "310800"

    #@66
    aput-object v5, v0, v4

    #@68
    .line 656
    .local v0, Operator_Code:[Ljava/lang/String;
    if-nez p0, :cond_6b

    #@6a
    .line 671
    :cond_6a
    :goto_6a
    return v2

    #@6b
    .line 659
    :cond_6b
    const/4 v1, 0x0

    #@6c
    .local v1, index:I
    :goto_6c
    array-length v4, v0

    #@6d
    if-ge v1, v4, :cond_6a

    #@6f
    .line 661
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@72
    move-result v4

    #@73
    aget-object v5, v0, v1

    #@75
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@78
    move-result v5

    #@79
    if-eq v4, v5, :cond_98

    #@7b
    .line 663
    new-instance v4, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v4

    #@84
    const-string v5, "0"

    #@86
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v4

    #@8a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v4

    #@8e
    aget-object v5, v0, v1

    #@90
    invoke-static {v4, v5}, Landroid/telephony/PhoneNumberUtils;->compareStrictly(Ljava/lang/String;Ljava/lang/String;)Z

    #@93
    move-result v4

    #@94
    if-eqz v4, :cond_a2

    #@96
    move v2, v3

    #@97
    .line 664
    goto :goto_6a

    #@98
    .line 666
    :cond_98
    aget-object v4, v0, v1

    #@9a
    invoke-static {p0, v4}, Landroid/telephony/PhoneNumberUtils;->compareStrictly(Ljava/lang/String;Ljava/lang/String;)Z

    #@9d
    move-result v4

    #@9e
    if-eqz v4, :cond_a2

    #@a0
    move v2, v3

    #@a1
    .line 667
    goto :goto_6a

    #@a2
    .line 659
    :cond_a2
    add-int/lit8 v1, v1, 0x1

    #@a4
    goto :goto_6c
.end method

.method private static getGsmAlpaArrayFromDTTString(Ljava/lang/String;)[I
    .registers 8
    .parameter "dtt_table"

    #@0
    .prologue
    .line 627
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    .line 628
    .local v0, Len:I
    div-int/lit8 v5, v0, 0x2

    #@6
    new-array v1, v5, [I

    #@8
    .line 630
    .local v1, TableArray:[I
    const/4 v3, 0x0

    #@9
    .line 631
    .local v3, j:I
    const/4 v2, 0x0

    #@a
    .local v2, i:I
    move v4, v3

    #@b
    .end local v3           #j:I
    .local v4, j:I
    :goto_b
    if-ge v2, v0, :cond_21

    #@d
    .line 632
    add-int/lit8 v3, v4, 0x1

    #@f
    .end local v4           #j:I
    .restart local v3       #j:I
    add-int/lit8 v5, v2, 0x2

    #@11
    invoke-virtual {p0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@14
    move-result-object v5

    #@15
    const/16 v6, 0x10

    #@17
    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@1a
    move-result v5

    #@1b
    aput v5, v1, v4

    #@1d
    .line 631
    add-int/lit8 v2, v2, 0x2

    #@1f
    move v4, v3

    #@20
    .end local v3           #j:I
    .restart local v4       #j:I
    goto :goto_b

    #@21
    .line 635
    :cond_21
    return-object v1
.end method

.method public static getNotCloseCallScreenECCNoti()Z
    .registers 1

    #@0
    .prologue
    .line 486
    sget-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->notCloseCallScreen:Z

    #@2
    return v0
.end method

.method public static getProcessECCNoti()Z
    .registers 1

    #@0
    .prologue
    .line 448
    sget-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->misProcessing:Z

    #@2
    return v0
.end method

.method public static getResolver()Landroid/content/ContentResolver;
    .registers 1

    #@0
    .prologue
    .line 439
    sget-object v0, Lcom/android/internal/telephony/gsm/EccNoti;->mResolver:Landroid/content/ContentResolver;

    #@2
    return-object v0
.end method

.method public static isSENDING_ECN()Z
    .registers 1

    #@0
    .prologue
    .line 467
    sget-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@2
    return v0
.end method

.method public static isSENDING_UVR()Z
    .registers 1

    #@0
    .prologue
    .line 476
    sget-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_UVR:Z

    #@2
    return v0
.end method

.method private static match(I[I)I
    .registers 4
    .parameter "in"
    .parameter "out"

    #@0
    .prologue
    .line 567
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    array-length v1, p1

    #@2
    if-ge v0, v1, :cond_c

    #@4
    .line 568
    aget v1, p1, v0

    #@6
    if-ne p0, v1, :cond_9

    #@8
    .line 571
    .end local v0           #i:I
    :goto_8
    return v0

    #@9
    .line 567
    .restart local v0       #i:I
    :cond_9
    add-int/lit8 v0, v0, 0x1

    #@b
    goto :goto_1

    #@c
    .line 571
    :cond_c
    const/4 v0, -0x1

    #@d
    goto :goto_8
.end method

.method public static processECN()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 338
    sget-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@3
    if-eqz v1, :cond_1f

    #@5
    const-string v1, "ECC_NOTI"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "sendECN MmiCode SENDING_ECN "

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    sget-boolean v3, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 340
    :cond_1f
    sget-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@21
    if-eq v1, v4, :cond_42

    #@23
    .line 341
    sput-boolean v4, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@25
    .line 342
    const/4 v1, 0x0

    #@26
    sput-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_UVR:Z

    #@28
    .line 343
    new-instance v1, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    sget-object v2, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@2f
    sget-object v2, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->mmi:Ljava/lang/String;

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    const-string v2, ""

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    .line 344
    .local v0, ECN:Ljava/lang/String;
    invoke-static {v0}, Lcom/android/internal/telephony/gsm/EccNoti;->sendUSSD(Ljava/lang/String;)V

    #@42
    .line 346
    :cond_42
    return-void
.end method

.method public static processUNR(Lcom/android/internal/telephony/MmiCode;)Z
    .registers 7
    .parameter "mmi"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 355
    if-nez p0, :cond_13

    #@3
    .line 356
    sget-boolean v3, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@5
    if-eqz v3, :cond_e

    #@7
    const-string v3, "ECC_NOTI"

    #@9
    const-string v4, "processUNR fail [ MMI is NULL ]"

    #@b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 357
    :cond_e
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->CompleteECCNoti()V

    #@11
    move v0, v2

    #@12
    .line 394
    :cond_12
    :goto_12
    return v0

    #@13
    .line 361
    :cond_13
    invoke-interface {p0}, Lcom/android/internal/telephony/MmiCode;->getState()Lcom/android/internal/telephony/MmiCode$State;

    #@16
    move-result-object v1

    #@17
    .line 362
    .local v1, state:Lcom/android/internal/telephony/MmiCode$State;
    const/4 v0, 0x0

    #@18
    .line 364
    .local v0, ret:Z
    sget-boolean v3, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@1a
    if-eqz v3, :cond_34

    #@1c
    const-string v3, "ECC_NOTI"

    #@1e
    new-instance v4, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v5, "processEccNoti state : "

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 366
    :cond_34
    sget-object v3, Lcom/android/internal/telephony/gsm/EccNoti$1;->$SwitchMap$com$android$internal$telephony$MmiCode$State:[I

    #@36
    invoke-virtual {v1}, Lcom/android/internal/telephony/MmiCode$State;->ordinal()I

    #@39
    move-result v4

    #@3a
    aget v3, v3, v4

    #@3c
    packed-switch v3, :pswitch_data_6c

    #@3f
    move v0, v2

    #@40
    .line 390
    goto :goto_12

    #@41
    .line 368
    :pswitch_41
    invoke-interface {p0}, Lcom/android/internal/telephony/MmiCode;->getMessage()Ljava/lang/CharSequence;

    #@44
    move-result-object v2

    #@45
    if-eqz v2, :cond_12

    #@47
    sget-boolean v2, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@49
    const/4 v3, 0x1

    #@4a
    if-ne v2, v3, :cond_12

    #@4c
    .line 369
    invoke-interface {p0}, Lcom/android/internal/telephony/MmiCode;->getMessage()Ljava/lang/CharSequence;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/EccNoti;->DecodeUNR(Ljava/lang/String;)Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    sput-object v2, Lcom/android/internal/telephony/gsm/EccNoti;->mDecoded:Ljava/lang/String;

    #@5a
    .line 370
    sget-object v2, Lcom/android/internal/telephony/gsm/EccNoti;->mDecoded:Ljava/lang/String;

    #@5c
    if-nez v2, :cond_60

    #@5e
    .line 371
    const/4 v0, 0x0

    #@5f
    goto :goto_12

    #@60
    .line 373
    :cond_60
    const/4 v0, 0x1

    #@61
    goto :goto_12

    #@62
    .line 380
    :pswitch_62
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->CompleteECCNoti()V

    #@65
    .line 381
    const/4 v0, 0x0

    #@66
    .line 382
    goto :goto_12

    #@67
    .line 385
    :pswitch_67
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->CompleteECCNoti()V

    #@6a
    .line 386
    const/4 v0, 0x0

    #@6b
    .line 387
    goto :goto_12

    #@6c
    .line 366
    :pswitch_data_6c
    .packed-switch 0x1
        :pswitch_41
        :pswitch_62
        :pswitch_67
    .end packed-switch
.end method

.method public static processUVR()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 404
    sget-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@3
    if-eqz v0, :cond_1f

    #@5
    const-string v0, "ECC_NOTI"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "processUVR MmiCode SENDING_UVR "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    sget-boolean v2, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_UVR:Z

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 406
    :cond_1f
    sget-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_UVR:Z

    #@21
    if-eq v0, v3, :cond_2d

    #@23
    .line 407
    const/4 v0, 0x0

    #@24
    sput-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@26
    .line 408
    sput-boolean v3, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_UVR:Z

    #@28
    .line 409
    sget-object v0, Lcom/android/internal/telephony/gsm/EccNoti;->mDecoded:Ljava/lang/String;

    #@2a
    invoke-static {v0}, Lcom/android/internal/telephony/gsm/EccNoti;->sendUSSD(Ljava/lang/String;)V

    #@2d
    .line 411
    :cond_2d
    return-void
.end method

.method public static read(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "resolver"
    .parameter "Key_value"

    #@0
    .prologue
    .line 171
    const/4 v6, 0x0

    #@1
    .line 172
    .local v6, cursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    #@2
    .line 175
    .local v8, retVal:Ljava/lang/String;
    :try_start_2
    sget-object v1, Lcom/android/internal/telephony/gsm/EccNoti;->CONTENT_URI:Landroid/net/Uri;

    #@4
    const/4 v2, 0x0

    #@5
    const/4 v3, 0x0

    #@6
    const/4 v4, 0x0

    #@7
    const/4 v5, 0x0

    #@8
    move-object v0, p0

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@c
    move-result-object v6

    #@d
    .line 177
    if-eqz v6, :cond_22

    #@f
    .line 178
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_aa

    #@15
    .line 179
    const-string v0, "activation"

    #@17
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_5b

    #@1d
    .line 180
    const/4 v0, 0x1

    #@1e
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_21
    .catchall {:try_start_2 .. :try_end_21} :catchall_b3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_21} :catch_9e

    #@21
    move-result-object v8

    #@22
    .line 199
    :cond_22
    :goto_22
    if-eqz v6, :cond_27

    #@24
    .line 200
    :goto_24
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@27
    .line 203
    :cond_27
    sget-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@29
    if-nez v0, :cond_33

    #@2b
    const-string v0, "activation"

    #@2d
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_55

    #@33
    :cond_33
    const-string v0, "ECC_NOTI"

    #@35
    new-instance v1, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v2, "read Key value : "

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    const-string v2, "  read value : "

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 205
    :cond_55
    if-eqz v6, :cond_5a

    #@57
    .line 206
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@5a
    .line 208
    :cond_5a
    return-object v8

    #@5b
    .line 181
    :cond_5b
    :try_start_5b
    const-string v0, "mmi"

    #@5d
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@60
    move-result v0

    #@61
    if-eqz v0, :cond_69

    #@63
    .line 182
    const/4 v0, 0x2

    #@64
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@67
    move-result-object v8

    #@68
    goto :goto_22

    #@69
    .line 183
    :cond_69
    const-string v0, "dtt_code"

    #@6b
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v0

    #@6f
    if-eqz v0, :cond_77

    #@71
    .line 184
    const/4 v0, 0x3

    #@72
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@75
    move-result-object v8

    #@76
    goto :goto_22

    #@77
    .line 185
    :cond_77
    const-string v0, "dtt_digit"

    #@79
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7c
    move-result v0

    #@7d
    if-eqz v0, :cond_85

    #@7f
    .line 186
    const/4 v0, 0x4

    #@80
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@83
    move-result-object v8

    #@84
    goto :goto_22

    #@85
    .line 188
    :cond_85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@87
    new-instance v1, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v2, "ECC Noti Key value : "

    #@8e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v1

    #@92
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v1

    #@96
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v1

    #@9a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9d
    throw v0
    :try_end_9e
    .catchall {:try_start_5b .. :try_end_9e} :catchall_b3
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_9e} :catch_9e

    #@9e
    .line 196
    :catch_9e
    move-exception v7

    #@9f
    .line 197
    .local v7, e:Ljava/lang/Exception;
    :try_start_9f
    const-string v0, "ECC_NOTI"

    #@a1
    const-string v1, "read"

    #@a3
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a6
    .catchall {:try_start_9f .. :try_end_a6} :catchall_b3

    #@a6
    .line 199
    if-eqz v6, :cond_27

    #@a8
    goto/16 :goto_24

    #@aa
    .line 193
    .end local v7           #e:Ljava/lang/Exception;
    :cond_aa
    :try_start_aa
    const-string v0, "ECC_NOTI"

    #@ac
    const-string v1, "cursor cannot move to first"

    #@ae
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b1
    .catchall {:try_start_aa .. :try_end_b1} :catchall_b3
    .catch Ljava/lang/Exception; {:try_start_aa .. :try_end_b1} :catch_9e

    #@b1
    goto/16 :goto_22

    #@b3
    .line 199
    :catchall_b3
    move-exception v0

    #@b4
    if-eqz v6, :cond_b9

    #@b6
    .line 200
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@b9
    .line 199
    :cond_b9
    throw v0
.end method

.method private static sendUSSD(Ljava/lang/String;)V
    .registers 3
    .parameter "mmi"

    #@0
    .prologue
    .line 325
    sget-object v0, Lcom/android/internal/telephony/gsm/EccNoti;->mMmiCode:Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 326
    sget-object v0, Lcom/android/internal/telephony/gsm/EccNoti;->mMmiCode:Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@6
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sendUssd(Ljava/lang/String;)V

    #@9
    .line 329
    :cond_9
    :goto_9
    return-void

    #@a
    .line 328
    :cond_a
    sget-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@c
    if-eqz v0, :cond_9

    #@e
    const-string v0, "ECC_NOTI"

    #@10
    const-string v1, "sendUSSD MmiCode is null"

    #@12
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    goto :goto_9
.end method

.method public static setMMICode(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;
    .registers 6
    .parameter "phone"
    .parameter "app"

    #@0
    .prologue
    .line 278
    sput-object p1, Lcom/android/internal/telephony/gsm/EccNoti;->mUiccapp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    .line 279
    sput-object p0, Lcom/android/internal/telephony/gsm/EccNoti;->mPhone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4
    .line 284
    sget-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@6
    if-nez v1, :cond_6c

    #@8
    sget-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_UVR:Z

    #@a
    if-nez v1, :cond_6c

    #@c
    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    sget-object v2, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@13
    sget-object v2, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->mmi:Ljava/lang/String;

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, ""

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 289
    .local v0, ECN:Ljava/lang/String;
    sget-object v1, Lcom/android/internal/telephony/gsm/EccNoti;->mPhone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@25
    sget-object v2, Lcom/android/internal/telephony/gsm/EccNoti;->mUiccapp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@27
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->newFromUssdUserInput(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@2a
    move-result-object v1

    #@2b
    sput-object v1, Lcom/android/internal/telephony/gsm/EccNoti;->mMmiCode:Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@2d
    .line 292
    sget-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@2f
    if-eqz v1, :cond_69

    #@31
    const-string v1, "ECC_NOTI"

    #@33
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, "setMMICode Setting ECN Format ==>  "

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    sget-object v3, Lcom/android/internal/telephony/gsm/EccNoti;->eccnoti_data:Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;

    #@40
    sget-object v3, Lcom/android/internal/telephony/gsm/EccNoti$EccNoti_Data;->mmi:Ljava/lang/String;

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    const-string v3, " sending "

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    const-string v3, " return : [ "

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    sget-object v3, Lcom/android/internal/telephony/gsm/EccNoti;->mMmiCode:Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@58
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v2

    #@5c
    const-string v3, " ]"

    #@5e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v2

    #@66
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 314
    .end local v0           #ECN:Ljava/lang/String;
    :cond_69
    :goto_69
    sget-object v1, Lcom/android/internal/telephony/gsm/EccNoti;->mMmiCode:Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@6b
    return-object v1

    #@6c
    .line 295
    :cond_6c
    sget-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@6e
    const/4 v2, 0x1

    #@6f
    if-ne v1, v2, :cond_cf

    #@71
    sget-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_UVR:Z

    #@73
    if-nez v1, :cond_cf

    #@75
    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v2, "#"

    #@7c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v1

    #@80
    sget-object v2, Lcom/android/internal/telephony/gsm/EccNoti;->mDecoded:Ljava/lang/String;

    #@82
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v1

    #@86
    const-string v2, "#"

    #@88
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v1

    #@8c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v1

    #@90
    sput-object v1, Lcom/android/internal/telephony/gsm/EccNoti;->mDecoded:Ljava/lang/String;

    #@92
    .line 300
    sget-object v1, Lcom/android/internal/telephony/gsm/EccNoti;->mDecoded:Ljava/lang/String;

    #@94
    sget-object v2, Lcom/android/internal/telephony/gsm/EccNoti;->mPhone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@96
    sget-object v3, Lcom/android/internal/telephony/gsm/EccNoti;->mUiccapp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@98
    invoke-static {v1, v2, v3}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->newFromUssdUserInput(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@9b
    move-result-object v1

    #@9c
    sput-object v1, Lcom/android/internal/telephony/gsm/EccNoti;->mMmiCode:Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@9e
    .line 304
    sget-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@a0
    if-eqz v1, :cond_69

    #@a2
    const-string v1, "ECC_NOTI"

    #@a4
    new-instance v2, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string v3, "setMMICode Setting UVR Format ==>  "

    #@ab
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v2

    #@af
    sget-object v3, Lcom/android/internal/telephony/gsm/EccNoti;->mDecoded:Ljava/lang/String;

    #@b1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v2

    #@b5
    const-string v3, " return : [ "

    #@b7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v2

    #@bb
    sget-object v3, Lcom/android/internal/telephony/gsm/EccNoti;->mMmiCode:Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@bd
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v2

    #@c1
    const-string v3, " ]"

    #@c3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v2

    #@c7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v2

    #@cb
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    goto :goto_69

    #@cf
    .line 309
    :cond_cf
    sget-boolean v1, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@d1
    if-eqz v1, :cond_69

    #@d3
    const-string v1, "ECC_NOTI"

    #@d5
    new-instance v2, Ljava/lang/StringBuilder;

    #@d7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@da
    const-string v3, "setMMICode ECN : "

    #@dc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v2

    #@e0
    sget-boolean v3, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_ECN:Z

    #@e2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v2

    #@e6
    const-string v3, " UVR : "

    #@e8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v2

    #@ec
    sget-boolean v3, Lcom/android/internal/telephony/gsm/EccNoti;->SENDING_UVR:Z

    #@ee
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v2

    #@f2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f5
    move-result-object v2

    #@f6
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f9
    goto/16 :goto_69
.end method

.method public static setNotCloseCallScreenECCNoti(Z)V
    .registers 4
    .parameter "setVal"

    #@0
    .prologue
    .line 495
    sget-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@2
    if-eqz v0, :cond_1e

    #@4
    const-string v0, "ECC_NOTI"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "setNotCloseCallScreenECCNoti notCloseCallScreen : "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    sget-boolean v2, Lcom/android/internal/telephony/gsm/EccNoti;->notCloseCallScreen:Z

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 497
    :cond_1e
    sput-boolean p0, Lcom/android/internal/telephony/gsm/EccNoti;->notCloseCallScreen:Z

    #@20
    .line 498
    return-void
.end method

.method public static setProcessECCNoti(Z)V
    .registers 4
    .parameter "setVal"

    #@0
    .prologue
    .line 457
    sput-boolean p0, Lcom/android/internal/telephony/gsm/EccNoti;->misProcessing:Z

    #@2
    .line 458
    sget-boolean v0, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@4
    if-eqz v0, :cond_20

    #@6
    const-string v0, "ECC_NOTI"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "setProcessECCNoti Procssing : "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    sget-boolean v2, Lcom/android/internal/telephony/gsm/EccNoti;->misProcessing:Z

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 459
    :cond_20
    return-void
.end method

.method public static setResolver(Landroid/content/ContentResolver;)V
    .registers 1
    .parameter "resolver"

    #@0
    .prologue
    .line 431
    sput-object p0, Lcom/android/internal/telephony/gsm/EccNoti;->mResolver:Landroid/content/ContentResolver;

    #@2
    .line 432
    return-void
.end method

.method public static update(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)I
    .registers 9
    .parameter "resolver"
    .parameter "Key_value"
    .parameter "update_value"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 139
    new-instance v1, Landroid/content/ContentValues;

    #@3
    const/4 v2, 0x1

    #@4
    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    #@7
    .line 141
    .local v1, values:Landroid/content/ContentValues;
    sget-boolean v2, Lcom/android/internal/telephony/gsm/EccNoti;->DBG:Z

    #@9
    if-nez v2, :cond_13

    #@b
    const-string v2, "activation"

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_35

    #@13
    :cond_13
    const-string v2, "ECC_NOTI"

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v4, "write key String : "

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    const-string v4, "  update String : "

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 146
    :cond_35
    const-string v2, "dtt_code"

    #@37
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_62

    #@3d
    .line 147
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@40
    move-result v2

    #@41
    const-string v3, "030708090B0C0E0F24405B5C5D5E5F601C1D1E1F1012131415161718191A"

    #@43
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@46
    move-result v3

    #@47
    if-eq v2, v3, :cond_8f

    #@49
    .line 148
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4b
    new-instance v3, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v4, "ECC Noti Code : "

    #@52
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@61
    throw v2

    #@62
    .line 150
    :cond_62
    const-string v2, "dtt_digit"

    #@64
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v2

    #@68
    if-eqz v2, :cond_8f

    #@6a
    .line 151
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@6d
    move-result v2

    #@6e
    const-string v3, "897023415618307564290729486153"

    #@70
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@73
    move-result v3

    #@74
    if-eq v2, v3, :cond_8f

    #@76
    .line 152
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@78
    new-instance v3, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v4, "ECC Noti Digit : "

    #@7f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v3

    #@83
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v3

    #@8b
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@8e
    throw v2

    #@8f
    .line 157
    :cond_8f
    invoke-virtual {v1, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@92
    .line 159
    sget-object v2, Lcom/android/internal/telephony/gsm/EccNoti;->CONTENT_URI:Landroid/net/Uri;

    #@94
    invoke-virtual {p0, v2, v1, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@97
    move-result v0

    #@98
    .line 161
    .local v0, result:I
    return v0
.end method
