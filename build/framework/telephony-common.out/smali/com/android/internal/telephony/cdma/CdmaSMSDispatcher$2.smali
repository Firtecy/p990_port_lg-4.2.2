.class Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$2;
.super Landroid/content/BroadcastReceiver;
.source "CdmaSMSDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2568
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$2;->this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2571
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    const-string v1, "com.lge.ims.action.DOMAIN_NOTIFICATION"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_4c

    #@d
    .line 2572
    new-instance v0, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v1, "cdmaDomainNotificationReceiver, [KDDI][DAN] Received Broadcast(DAN)! : "

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@27
    .line 2573
    new-instance v0, Landroid/content/Intent;

    #@29
    const-string v1, "com.lge.kddi.intent.action.DAN_SENT"

    #@2b
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2e
    invoke-static {p1, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@31
    move-result-object v3

    #@32
    .line 2574
    .local v3, mSentIntent:Landroid/app/PendingIntent;
    new-instance v0, Landroid/content/Intent;

    #@34
    const-string v1, "com.lge.kddi.intent.action.DAN_DELIVERED"

    #@36
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@39
    invoke-static {p1, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@3c
    move-result-object v4

    #@3d
    .line 2576
    .local v4, mdeliveryIntent:Landroid/app/PendingIntent;
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$2;->this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;

    #@3f
    const-string v1, "08059980000"

    #@41
    const-string v2, "08059980000"

    #@43
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$2;->this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;

    #@45
    invoke-static {v5}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->access$100(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)I

    #@48
    move-result v5

    #@49
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->sendDomainNotiMessage(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;I)V

    #@4c
    .line 2578
    .end local v3           #mSentIntent:Landroid/app/PendingIntent;
    .end local v4           #mdeliveryIntent:Landroid/app/PendingIntent;
    :cond_4c
    return-void
.end method
