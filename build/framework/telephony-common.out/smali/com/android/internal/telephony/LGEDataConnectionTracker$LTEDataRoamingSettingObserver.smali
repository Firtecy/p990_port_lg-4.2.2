.class Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;
.super Landroid/database/ContentObserver;
.source "LGEDataConnectionTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGEDataConnectionTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LTEDataRoamingSettingObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/LGEDataConnectionTracker;Landroid/os/Handler;)V
    .registers 3
    .parameter
    .parameter "handler"

    #@0
    .prologue
    .line 2397
    iput-object p1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@2
    .line 2398
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    .line 2399
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 3
    .parameter "selfChange"

    #@0
    .prologue
    .line 2415
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$400(Lcom/android/internal/telephony/LGEDataConnectionTracker;)V

    #@5
    .line 2416
    return-void
.end method

.method public register(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 2402
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    .line 2403
    .local v0, resolver:Landroid/content/ContentResolver;
    const-string v1, "data_lte_roaming"

    #@6
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@9
    move-result-object v1

    #@a
    const/4 v2, 0x0

    #@b
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@e
    .line 2405
    return-void
.end method

.method public unregister(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 2408
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    .line 2409
    .local v0, resolver:Landroid/content/ContentResolver;
    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@7
    .line 2410
    return-void
.end method
