.class public final Lcom/android/internal/telephony/cdma/CdmaInformationRecords;
.super Ljava/lang/Object;
.source "CdmaInformationRecords.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/CdmaInformationRecords$1;,
        Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ReleaseInfoRecKddi;,
        Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53AudioControlInfoRec;,
        Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ClirInfoRec;,
        Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaLineControlInfoRec;,
        Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaRedirectingNumberInfoRec;,
        Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaNumberInfoRec;,
        Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;,
        Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;,
        Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayItemRec;,
        Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaSignalInfoRec;
    }
.end annotation


# static fields
.field public static final RIL_CDMA_CALLED_PARTY_NUMBER_INFO_REC:I = 0x1

.field public static final RIL_CDMA_CALLING_PARTY_NUMBER_INFO_REC:I = 0x2

.field public static final RIL_CDMA_CONNECTED_NUMBER_INFO_REC:I = 0x3

.field public static final RIL_CDMA_DISPLAY_INFO_REC:I = 0x0

.field public static final RIL_CDMA_EXTENDED_DISPLAY_INFO_REC:I = 0x7

.field public static final RIL_CDMA_LINE_CONTROL_INFO_REC:I = 0x6

.field public static final RIL_CDMA_REDIRECTING_NUMBER_INFO_REC:I = 0x5

.field public static final RIL_CDMA_SIGNAL_INFO_REC:I = 0x4

.field public static final RIL_CDMA_T53_AUDIO_CONTROL_INFO_REC:I = 0xa

.field public static final RIL_CDMA_T53_CLIR_INFO_REC:I = 0x8

.field public static final RIL_CDMA_T53_RELEASE_INFO_REC:I = 0x9


# instance fields
.field public record:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 12
    .parameter "p"

    #@0
    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v1

    #@7
    .line 57
    .local v1, id:I
    packed-switch v1, :pswitch_data_e0

    #@a
    .line 104
    :cond_a
    new-instance v0, Ljava/lang/RuntimeException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "RIL_UNSOL_CDMA_INFO_REC: unsupported record. Got "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->idToString(I)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 59
    :pswitch_2d
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@30
    move-result-object v9

    #@31
    .line 60
    .local v9, data:[B
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;

    #@33
    invoke-direct {v0, v1, v9}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;-><init>(I[B)V

    #@36
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@38
    .line 108
    .end local v9           #data:[B
    :goto_38
    return-void

    #@39
    .line 63
    :pswitch_39
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;

    #@3b
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;-><init>(ILjava/lang/String;)V

    #@42
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@44
    goto :goto_38

    #@45
    .line 69
    :pswitch_45
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaNumberInfoRec;

    #@47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4e
    move-result v3

    #@4f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@52
    move-result v4

    #@53
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@56
    move-result v5

    #@57
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5a
    move-result v6

    #@5b
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaNumberInfoRec;-><init>(ILjava/lang/String;IIII)V

    #@5e
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@60
    goto :goto_38

    #@61
    .line 74
    :pswitch_61
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaSignalInfoRec;

    #@63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@66
    move-result v2

    #@67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6a
    move-result v3

    #@6b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6e
    move-result v4

    #@6f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@72
    move-result v5

    #@73
    invoke-direct {v0, v2, v3, v4, v5}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaSignalInfoRec;-><init>(IIII)V

    #@76
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@78
    goto :goto_38

    #@79
    .line 78
    :pswitch_79
    new-instance v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaRedirectingNumberInfoRec;

    #@7b
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7e
    move-result-object v3

    #@7f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@82
    move-result v4

    #@83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@86
    move-result v5

    #@87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8a
    move-result v6

    #@8b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8e
    move-result v7

    #@8f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@92
    move-result v8

    #@93
    invoke-direct/range {v2 .. v8}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaRedirectingNumberInfoRec;-><init>(Ljava/lang/String;IIIII)V

    #@96
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@98
    goto :goto_38

    #@99
    .line 83
    :pswitch_99
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaLineControlInfoRec;

    #@9b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9e
    move-result v2

    #@9f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a2
    move-result v3

    #@a3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a6
    move-result v4

    #@a7
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@aa
    move-result v5

    #@ab
    invoke-direct {v0, v2, v3, v4, v5}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaLineControlInfoRec;-><init>(IIII)V

    #@ae
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@b0
    goto :goto_38

    #@b1
    .line 88
    :pswitch_b1
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ClirInfoRec;

    #@b3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@b6
    move-result v2

    #@b7
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ClirInfoRec;-><init>(I)V

    #@ba
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@bc
    goto/16 :goto_38

    #@be
    .line 92
    :pswitch_be
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53AudioControlInfoRec;

    #@c0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c3
    move-result v2

    #@c4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c7
    move-result v3

    #@c8
    invoke-direct {v0, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53AudioControlInfoRec;-><init>(II)V

    #@cb
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@cd
    goto/16 :goto_38

    #@cf
    .line 98
    :pswitch_cf
    const-string v0, "KDDI"

    #@d1
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@d4
    move-result v0

    #@d5
    if-eqz v0, :cond_a

    #@d7
    .line 99
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ReleaseInfoRecKddi;

    #@d9
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ReleaseInfoRecKddi;-><init>()V

    #@dc
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@de
    goto/16 :goto_38

    #@e0
    .line 57
    :pswitch_data_e0
    .packed-switch 0x0
        :pswitch_39
        :pswitch_45
        :pswitch_45
        :pswitch_45
        :pswitch_61
        :pswitch_79
        :pswitch_99
        :pswitch_2d
        :pswitch_b1
        :pswitch_cf
        :pswitch_be
    .end packed-switch
.end method

.method public static idToString(I)Ljava/lang/String;
    .registers 2
    .parameter "id"

    #@0
    .prologue
    .line 111
    packed-switch p0, :pswitch_data_28

    #@3
    .line 123
    const-string v0, "<unknown record>"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 112
    :pswitch_6
    const-string v0, "RIL_CDMA_DISPLAY_INFO_REC"

    #@8
    goto :goto_5

    #@9
    .line 113
    :pswitch_9
    const-string v0, "RIL_CDMA_CALLED_PARTY_NUMBER_INFO_REC"

    #@b
    goto :goto_5

    #@c
    .line 114
    :pswitch_c
    const-string v0, "RIL_CDMA_CALLING_PARTY_NUMBER_INFO_REC"

    #@e
    goto :goto_5

    #@f
    .line 115
    :pswitch_f
    const-string v0, "RIL_CDMA_CONNECTED_NUMBER_INFO_REC"

    #@11
    goto :goto_5

    #@12
    .line 116
    :pswitch_12
    const-string v0, "RIL_CDMA_SIGNAL_INFO_REC"

    #@14
    goto :goto_5

    #@15
    .line 117
    :pswitch_15
    const-string v0, "RIL_CDMA_REDIRECTING_NUMBER_INFO_REC"

    #@17
    goto :goto_5

    #@18
    .line 118
    :pswitch_18
    const-string v0, "RIL_CDMA_LINE_CONTROL_INFO_REC"

    #@1a
    goto :goto_5

    #@1b
    .line 119
    :pswitch_1b
    const-string v0, "RIL_CDMA_EXTENDED_DISPLAY_INFO_REC"

    #@1d
    goto :goto_5

    #@1e
    .line 120
    :pswitch_1e
    const-string v0, "RIL_CDMA_T53_CLIR_INFO_REC"

    #@20
    goto :goto_5

    #@21
    .line 121
    :pswitch_21
    const-string v0, "RIL_CDMA_T53_RELEASE_INFO_REC"

    #@23
    goto :goto_5

    #@24
    .line 122
    :pswitch_24
    const-string v0, "RIL_CDMA_T53_AUDIO_CONTROL_INFO_REC"

    #@26
    goto :goto_5

    #@27
    .line 111
    nop

    #@28
    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
        :pswitch_24
    .end packed-switch
.end method
