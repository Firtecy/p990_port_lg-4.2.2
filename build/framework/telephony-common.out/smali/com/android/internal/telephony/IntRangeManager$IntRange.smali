.class Lcom/android/internal/telephony/IntRangeManager$IntRange;
.super Ljava/lang/Object;
.source "IntRangeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IntRangeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IntRange"
.end annotation


# instance fields
.field final clients:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/IntRangeManager$ClientRange;",
            ">;"
        }
    .end annotation
.end field

.field endId:I

.field startId:I

.field final synthetic this$0:Lcom/android/internal/telephony/IntRangeManager;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V
    .registers 7
    .parameter
    .parameter "startId"
    .parameter "endId"
    .parameter "client"

    #@0
    .prologue
    .line 67
    iput-object p1, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->this$0:Lcom/android/internal/telephony/IntRangeManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 68
    iput p2, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@7
    .line 69
    iput p3, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@9
    .line 70
    new-instance v0, Ljava/util/ArrayList;

    #@b
    const/4 v1, 0x4

    #@c
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@11
    .line 71
    iget-object v0, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@13
    new-instance v1, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@15
    invoke-direct {v1, p1, p2, p3, p4}, Lcom/android/internal/telephony/IntRangeManager$ClientRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@18
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1b
    .line 72
    return-void
.end method

.method constructor <init>(Lcom/android/internal/telephony/IntRangeManager;Lcom/android/internal/telephony/IntRangeManager$ClientRange;)V
    .registers 5
    .parameter
    .parameter "clientRange"

    #@0
    .prologue
    .line 78
    iput-object p1, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->this$0:Lcom/android/internal/telephony/IntRangeManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 79
    iget v0, p2, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@7
    iput v0, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@9
    .line 80
    iget v0, p2, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@b
    iput v0, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@d
    .line 81
    new-instance v0, Ljava/util/ArrayList;

    #@f
    const/4 v1, 0x4

    #@10
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@13
    iput-object v0, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@15
    .line 82
    iget-object v0, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 83
    return-void
.end method

.method constructor <init>(Lcom/android/internal/telephony/IntRangeManager;Lcom/android/internal/telephony/IntRangeManager$IntRange;I)V
    .registers 7
    .parameter
    .parameter "intRange"
    .parameter "numElements"

    #@0
    .prologue
    .line 95
    iput-object p1, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->this$0:Lcom/android/internal/telephony/IntRangeManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 96
    iget v1, p2, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@7
    iput v1, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@9
    .line 97
    iget v1, p2, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@b
    iput v1, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@d
    .line 98
    new-instance v1, Ljava/util/ArrayList;

    #@f
    iget-object v2, p2, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v2

    #@15
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@18
    iput-object v1, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@1a
    .line 99
    const/4 v0, 0x0

    #@1b
    .local v0, i:I
    :goto_1b
    if-ge v0, p3, :cond_2b

    #@1d
    .line 100
    iget-object v1, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@1f
    iget-object v2, p2, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28
    .line 99
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_1b

    #@2b
    .line 102
    :cond_2b
    return-void
.end method


# virtual methods
.method insert(Lcom/android/internal/telephony/IntRangeManager$ClientRange;)V
    .registers 8
    .parameter "range"

    #@0
    .prologue
    .line 114
    iget-object v4, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    .line 115
    .local v2, len:I
    const/4 v1, -0x1

    #@7
    .line 116
    .local v1, insert:I
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v2, :cond_37

    #@a
    .line 117
    iget-object v4, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@12
    .line 118
    .local v3, nextRange:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    iget v4, p1, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@14
    iget v5, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@16
    if-gt v4, v5, :cond_2e

    #@18
    .line 120
    invoke-virtual {p1, v3}, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v4

    #@1c
    if-nez v4, :cond_36

    #@1e
    .line 122
    iget v4, p1, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@20
    iget v5, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@22
    if-ne v4, v5, :cond_31

    #@24
    iget v4, p1, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@26
    iget v5, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@28
    if-le v4, v5, :cond_31

    #@2a
    .line 123
    add-int/lit8 v1, v0, 0x1

    #@2c
    .line 124
    if-ge v1, v2, :cond_37

    #@2e
    .line 116
    :cond_2e
    add-int/lit8 v0, v0, 0x1

    #@30
    goto :goto_8

    #@31
    .line 131
    :cond_31
    iget-object v4, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@33
    invoke-virtual {v4, v0, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@36
    .line 141
    .end local v3           #nextRange:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    :cond_36
    :goto_36
    return-void

    #@37
    .line 136
    :cond_37
    const/4 v4, -0x1

    #@38
    if-eq v1, v4, :cond_42

    #@3a
    if-ge v1, v2, :cond_42

    #@3c
    .line 137
    iget-object v4, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@3e
    invoke-virtual {v4, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@41
    goto :goto_36

    #@42
    .line 140
    :cond_42
    iget-object v4, p0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@44
    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@47
    goto :goto_36
.end method
