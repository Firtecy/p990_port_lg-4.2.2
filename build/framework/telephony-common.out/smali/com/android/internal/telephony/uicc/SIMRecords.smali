.class public Lcom/android/internal/telephony/uicc/SIMRecords;
.super Lcom/android/internal/telephony/uicc/IccRecords;
.source "SIMRecords.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/uicc/SIMRecords$4;,
        Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;,
        Lcom/android/internal/telephony/uicc/SIMRecords$EfSktImsiPLoaded;,
        Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;
    }
.end annotation


# static fields
.field public static final ALL_ICC_DS_URI:Landroid/net/Uri; = null

.field static final CFF_LINE1_MASK:I = 0xf

.field static final CFF_LINE1_RESET:I = 0xf0

.field static final CFF_UNCONDITIONAL_ACTIVE:I = 0xa

.field static final CFF_UNCONDITIONAL_DEACTIVE:I = 0x5

.field private static final CFIS_ADN_CAPABILITY_ID_OFFSET:I = 0xe

.field private static final CFIS_ADN_EXTENSION_ID_OFFSET:I = 0xf

.field private static final CFIS_BCD_NUMBER_LENGTH_OFFSET:I = 0x2

.field private static final CFIS_TON_NPI_OFFSET:I = 0x3

.field private static final CPHS_SST_MBN_ENABLED:I = 0x30

.field private static final CPHS_SST_MBN_MASK:I = 0x30

.field private static final CRASH_RIL:Z = false

.field protected static final DBG:Z = true

.field private static ENABLE_PRIVACY_LOG:Z = false

.field private static final EVENT_APP_READY:I = 0x1

.field private static final EVENT_AUTO_SELECT_DONE:I = 0x12c

.field private static final EVENT_GET_ACTHPLMN_DONE:I = 0x68

.field protected static final EVENT_GET_AD_DONE:I = 0x9

.field private static final EVENT_GET_ALL_SMS_DONE:I = 0x12

.field private static final EVENT_GET_CFF_DONE:I = 0x18

.field private static final EVENT_GET_CFIS_DONE:I = 0x20

.field private static final EVENT_GET_CPHS_MAILBOX_DONE:I = 0xb

.field private static final EVENT_GET_CSP_CPHS_DONE:I = 0x21

.field private static final EVENT_GET_GID1_DONE:I = 0x25

.field private static final EVENT_GET_HPLMNWACT_DONE:I = 0x6e

.field private static final EVENT_GET_ICCID_DONE:I = 0x4

.field private static final EVENT_GET_IMSIP_DONE_KR:I = 0x29

.field private static final EVENT_GET_IMSI_DONE:I = 0x3

.field private static final EVENT_GET_IMSI_DONE_KR:I = 0x2c

.field private static final EVENT_GET_INFO_CPHS_DONE:I = 0x1a

.field private static final EVENT_GET_MASTER_IMSI_DONE_LGU:I = 0xc8

.field private static final EVENT_GET_MBDN_DONE:I = 0x6

.field private static final EVENT_GET_MBI_DONE:I = 0x5

.field protected static final EVENT_GET_MSISDN_DONE:I = 0xa

.field private static final EVENT_GET_MSISDN_IF_CARD_IS_EMPTY_DONE_KR:I = 0x2b

.field private static final EVENT_GET_MSISDN_WITHOUT_EXT_DONE:I = 0x2a

.field private static final EVENT_GET_MWIS_DONE:I = 0x7

.field private static final EVENT_GET_PNN_DONE:I = 0xf

.field private static final EVENT_GET_SMS_DONE:I = 0x16

.field private static final EVENT_GET_SPDI_DONE:I = 0xd

.field private static final EVENT_GET_SPN_DONE:I = 0xc

.field private static final EVENT_GET_SPON_IMSI1_DONE_LGU:I = 0xc9

.field private static final EVENT_GET_SPON_IMSI2_DONE_LGU:I = 0xca

.field private static final EVENT_GET_SPON_IMSI3_DONE_LGU:I = 0xcb

.field protected static final EVENT_GET_SST_DONE:I = 0x11

.field private static final EVENT_GET_VOICE_MAIL_INDICATOR_CPHS_DONE:I = 0x8

.field private static final EVENT_MARK_SMS_READ_DONE:I = 0x13

.field private static final EVENT_READ_EF_ROAMING:I = 0xcc

.field private static final EVENT_SET_CPHS_MAILBOX_DONE:I = 0x19

.field private static final EVENT_SET_CPHS_MWIS_DONE:I = 0x28

.field private static final EVENT_SET_MBDN_DONE:I = 0x14

.field private static final EVENT_SET_MSISDN_DONE:I = 0x1e

.field private static final EVENT_SET_MWIS_DONE:I = 0x27

.field private static final EVENT_SIM_REFRESH:I = 0x1f

.field public static final EVENT_SMARTCARD_GET_ATR:I = 0x32

.field private static final EVENT_SMS_ON_SIM:I = 0x15

.field private static final EVENT_UPDATE_DONE:I = 0xe

.field public static FDN_NAME_MAX:I = 0x0

.field public static FDN_REC_NUM:I = 0x0

.field private static final FLEX_VSIM_GID:Ljava/lang/String; = "persist.service.vsim.gid"

.field private static final FLEX_VSIM_ICCID:Ljava/lang/String; = "persist.service.vsim.iccid"

.field private static final FLEX_VSIM_IMSI:Ljava/lang/String; = "persist.service.vsim.imsi"

.field private static final FLEX_VSIM_MCC_MNC:Ljava/lang/String; = "persist.service.vsim.mcc-mnc"

.field private static final FLEX_VSIM_SPN:Ljava/lang/String; = "persist.service.vsim.spn"

.field private static final FLEX_VSIM_STATUS:Ljava/lang/String; = "persist.service.vsim.enable"

.field public static final ICC_URI:Landroid/net/Uri; = null

.field public static final INDEX_ON_ICC:Ljava/lang/String; = "index_on_icc"

.field public static final IS_41_EMAIL_NETWORK_ADDRESS:Ljava/lang/String; = "6245"

.field public static final LGE_MSGTYPE_SIM:I = 0x7

.field public static final LGE_SERVICE_MSGTYPE:Ljava/lang/String; = "lgeMsgType"

.field protected static final LOG_TAG:Ljava/lang/String; = "GSM"

.field private static final MCCMNC_CODES_HAVING_3DIGITS_MNC:[Ljava/lang/String; = null

.field private static final MISSING_TEXT:Ljava/lang/String; = "/*missing text*/"

.field private static final MSISDN_NONALPHA_LEN:I = 0xe

.field private static final MSISDN_NUMBER_LEN:I = 0xa

.field public static final NETWORK_SELECTION_KEY:Ljava/lang/String; = "network_selection_key"

.field public static final NETWORK_SELECTION_NAME_KEY:Ljava/lang/String; = "network_selection_name_key"

.field public static final SMS_CONCAT_URI:Landroid/net/Uri; = null

.field private static final SMS_FORMAT_CSIM:I = 0x2

.field private static final SMS_FORMAT_USIM:I = 0x1

.field public static final SMS_INBOX_URI:Landroid/net/Uri; = null

.field public static final SMS_OUTBOX_URI:Landroid/net/Uri; = null

.field public static final SMS_SENT_URI:Landroid/net/Uri; = null

.field public static final SMS_URI:Landroid/net/Uri; = null

.field public static final SPN_RULE_SHOW_PLMN:I = 0x2

.field public static final SPN_RULE_SHOW_SPN:I = 0x1

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final STATUS_PENDING:I = 0x20

.field public static final SUB_ID:Ljava/lang/String; = "sub_id"

.field static final TAG_FULL_NETWORK_NAME:I = 0x43

.field static final TAG_SHORT_NETWORK_NAME:I = 0x45

.field static final TAG_SPDI:I = 0xa3

.field static final TAG_SPDI_PLMN_LIST:I = 0x80


# instance fields
.field private callForwardingEnabled:Z

.field private custCompletedReceiver:Landroid/content/BroadcastReceiver;

.field private dialogProgress:Landroid/app/AlertDialog;

.field efCPHS_MWI:[B

.field private efHplmnwact:Ljava/lang/String;

.field efMWIS:[B

.field filter:Landroid/content/IntentFilter;

.field gid1:Ljava/lang/String;

.field imsip:Ljava/lang/String;

.field private mCphsInfo:[B

.field mCspPlmnEnabled:Z

.field mEfCff:[B

.field mEfCfis:[B

.field mIndexOnIcc:I

.field mSpnOverride:Lcom/android/internal/telephony/uicc/SpnOverride;

.field mUsimServiceTable:Lcom/android/internal/telephony/uicc/UsimServiceTable;

.field mVmConfig:Lcom/android/internal/telephony/uicc/VoiceMailConstants;

.field private msisdnNumber:[B

.field pnnHomeName:Ljava/lang/String;

.field spdiNetworks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field spnDisplayCondition:I

.field private spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

.field private sumNumber:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 123
    const-string v3, "persist.service.privacy.enable"

    #@4
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    const-string v4, "ATT"

    #@a
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_1c

    #@10
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    const-string v4, "TMO"

    #@16
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_37a

    #@1c
    :cond_1c
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    const-string v4, "US"

    #@22
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_37a

    #@28
    move v0, v1

    #@29
    :goto_29
    invoke-static {v3, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@2c
    move-result v0

    #@2d
    sput-boolean v0, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@2f
    .line 281
    const/16 v0, 0x83

    #@31
    new-array v0, v0, [Ljava/lang/String;

    #@33
    const-string v3, "405025"

    #@35
    aput-object v3, v0, v1

    #@37
    const-string v3, "405026"

    #@39
    aput-object v3, v0, v2

    #@3b
    const/4 v2, 0x2

    #@3c
    const-string v3, "405027"

    #@3e
    aput-object v3, v0, v2

    #@40
    const/4 v2, 0x3

    #@41
    const-string v3, "405028"

    #@43
    aput-object v3, v0, v2

    #@45
    const/4 v2, 0x4

    #@46
    const-string v3, "405029"

    #@48
    aput-object v3, v0, v2

    #@4a
    const/4 v2, 0x5

    #@4b
    const-string v3, "405030"

    #@4d
    aput-object v3, v0, v2

    #@4f
    const/4 v2, 0x6

    #@50
    const-string v3, "405031"

    #@52
    aput-object v3, v0, v2

    #@54
    const/4 v2, 0x7

    #@55
    const-string v3, "405032"

    #@57
    aput-object v3, v0, v2

    #@59
    const/16 v2, 0x8

    #@5b
    const-string v3, "405033"

    #@5d
    aput-object v3, v0, v2

    #@5f
    const/16 v2, 0x9

    #@61
    const-string v3, "405034"

    #@63
    aput-object v3, v0, v2

    #@65
    const/16 v2, 0xa

    #@67
    const-string v3, "405035"

    #@69
    aput-object v3, v0, v2

    #@6b
    const/16 v2, 0xb

    #@6d
    const-string v3, "405036"

    #@6f
    aput-object v3, v0, v2

    #@71
    const/16 v2, 0xc

    #@73
    const-string v3, "405037"

    #@75
    aput-object v3, v0, v2

    #@77
    const/16 v2, 0xd

    #@79
    const-string v3, "405038"

    #@7b
    aput-object v3, v0, v2

    #@7d
    const/16 v2, 0xe

    #@7f
    const-string v3, "405039"

    #@81
    aput-object v3, v0, v2

    #@83
    const/16 v2, 0xf

    #@85
    const-string v3, "405040"

    #@87
    aput-object v3, v0, v2

    #@89
    const/16 v2, 0x10

    #@8b
    const-string v3, "405041"

    #@8d
    aput-object v3, v0, v2

    #@8f
    const/16 v2, 0x11

    #@91
    const-string v3, "405042"

    #@93
    aput-object v3, v0, v2

    #@95
    const/16 v2, 0x12

    #@97
    const-string v3, "405043"

    #@99
    aput-object v3, v0, v2

    #@9b
    const/16 v2, 0x13

    #@9d
    const-string v3, "405044"

    #@9f
    aput-object v3, v0, v2

    #@a1
    const/16 v2, 0x14

    #@a3
    const-string v3, "405045"

    #@a5
    aput-object v3, v0, v2

    #@a7
    const/16 v2, 0x15

    #@a9
    const-string v3, "405046"

    #@ab
    aput-object v3, v0, v2

    #@ad
    const/16 v2, 0x16

    #@af
    const-string v3, "405047"

    #@b1
    aput-object v3, v0, v2

    #@b3
    const/16 v2, 0x17

    #@b5
    const-string v3, "405750"

    #@b7
    aput-object v3, v0, v2

    #@b9
    const/16 v2, 0x18

    #@bb
    const-string v3, "405751"

    #@bd
    aput-object v3, v0, v2

    #@bf
    const/16 v2, 0x19

    #@c1
    const-string v3, "405752"

    #@c3
    aput-object v3, v0, v2

    #@c5
    const/16 v2, 0x1a

    #@c7
    const-string v3, "405753"

    #@c9
    aput-object v3, v0, v2

    #@cb
    const/16 v2, 0x1b

    #@cd
    const-string v3, "405754"

    #@cf
    aput-object v3, v0, v2

    #@d1
    const/16 v2, 0x1c

    #@d3
    const-string v3, "405755"

    #@d5
    aput-object v3, v0, v2

    #@d7
    const/16 v2, 0x1d

    #@d9
    const-string v3, "405756"

    #@db
    aput-object v3, v0, v2

    #@dd
    const/16 v2, 0x1e

    #@df
    const-string v3, "405799"

    #@e1
    aput-object v3, v0, v2

    #@e3
    const/16 v2, 0x1f

    #@e5
    const-string v3, "405800"

    #@e7
    aput-object v3, v0, v2

    #@e9
    const/16 v2, 0x20

    #@eb
    const-string v3, "405801"

    #@ed
    aput-object v3, v0, v2

    #@ef
    const/16 v2, 0x21

    #@f1
    const-string v3, "405802"

    #@f3
    aput-object v3, v0, v2

    #@f5
    const/16 v2, 0x22

    #@f7
    const-string v3, "405803"

    #@f9
    aput-object v3, v0, v2

    #@fb
    const/16 v2, 0x23

    #@fd
    const-string v3, "405804"

    #@ff
    aput-object v3, v0, v2

    #@101
    const/16 v2, 0x24

    #@103
    const-string v3, "405805"

    #@105
    aput-object v3, v0, v2

    #@107
    const/16 v2, 0x25

    #@109
    const-string v3, "405806"

    #@10b
    aput-object v3, v0, v2

    #@10d
    const/16 v2, 0x26

    #@10f
    const-string v3, "405807"

    #@111
    aput-object v3, v0, v2

    #@113
    const/16 v2, 0x27

    #@115
    const-string v3, "405808"

    #@117
    aput-object v3, v0, v2

    #@119
    const/16 v2, 0x28

    #@11b
    const-string v3, "405809"

    #@11d
    aput-object v3, v0, v2

    #@11f
    const/16 v2, 0x29

    #@121
    const-string v3, "405810"

    #@123
    aput-object v3, v0, v2

    #@125
    const/16 v2, 0x2a

    #@127
    const-string v3, "405811"

    #@129
    aput-object v3, v0, v2

    #@12b
    const/16 v2, 0x2b

    #@12d
    const-string v3, "405812"

    #@12f
    aput-object v3, v0, v2

    #@131
    const/16 v2, 0x2c

    #@133
    const-string v3, "405813"

    #@135
    aput-object v3, v0, v2

    #@137
    const/16 v2, 0x2d

    #@139
    const-string v3, "405814"

    #@13b
    aput-object v3, v0, v2

    #@13d
    const/16 v2, 0x2e

    #@13f
    const-string v3, "405815"

    #@141
    aput-object v3, v0, v2

    #@143
    const/16 v2, 0x2f

    #@145
    const-string v3, "405816"

    #@147
    aput-object v3, v0, v2

    #@149
    const/16 v2, 0x30

    #@14b
    const-string v3, "405817"

    #@14d
    aput-object v3, v0, v2

    #@14f
    const/16 v2, 0x31

    #@151
    const-string v3, "405818"

    #@153
    aput-object v3, v0, v2

    #@155
    const/16 v2, 0x32

    #@157
    const-string v3, "405819"

    #@159
    aput-object v3, v0, v2

    #@15b
    const/16 v2, 0x33

    #@15d
    const-string v3, "405820"

    #@15f
    aput-object v3, v0, v2

    #@161
    const/16 v2, 0x34

    #@163
    const-string v3, "405821"

    #@165
    aput-object v3, v0, v2

    #@167
    const/16 v2, 0x35

    #@169
    const-string v3, "405822"

    #@16b
    aput-object v3, v0, v2

    #@16d
    const/16 v2, 0x36

    #@16f
    const-string v3, "405823"

    #@171
    aput-object v3, v0, v2

    #@173
    const/16 v2, 0x37

    #@175
    const-string v3, "405824"

    #@177
    aput-object v3, v0, v2

    #@179
    const/16 v2, 0x38

    #@17b
    const-string v3, "405825"

    #@17d
    aput-object v3, v0, v2

    #@17f
    const/16 v2, 0x39

    #@181
    const-string v3, "405826"

    #@183
    aput-object v3, v0, v2

    #@185
    const/16 v2, 0x3a

    #@187
    const-string v3, "405827"

    #@189
    aput-object v3, v0, v2

    #@18b
    const/16 v2, 0x3b

    #@18d
    const-string v3, "405828"

    #@18f
    aput-object v3, v0, v2

    #@191
    const/16 v2, 0x3c

    #@193
    const-string v3, "405829"

    #@195
    aput-object v3, v0, v2

    #@197
    const/16 v2, 0x3d

    #@199
    const-string v3, "405830"

    #@19b
    aput-object v3, v0, v2

    #@19d
    const/16 v2, 0x3e

    #@19f
    const-string v3, "405831"

    #@1a1
    aput-object v3, v0, v2

    #@1a3
    const/16 v2, 0x3f

    #@1a5
    const-string v3, "405832"

    #@1a7
    aput-object v3, v0, v2

    #@1a9
    const/16 v2, 0x40

    #@1ab
    const-string v3, "405833"

    #@1ad
    aput-object v3, v0, v2

    #@1af
    const/16 v2, 0x41

    #@1b1
    const-string v3, "405834"

    #@1b3
    aput-object v3, v0, v2

    #@1b5
    const/16 v2, 0x42

    #@1b7
    const-string v3, "405835"

    #@1b9
    aput-object v3, v0, v2

    #@1bb
    const/16 v2, 0x43

    #@1bd
    const-string v3, "405836"

    #@1bf
    aput-object v3, v0, v2

    #@1c1
    const/16 v2, 0x44

    #@1c3
    const-string v3, "405837"

    #@1c5
    aput-object v3, v0, v2

    #@1c7
    const/16 v2, 0x45

    #@1c9
    const-string v3, "405838"

    #@1cb
    aput-object v3, v0, v2

    #@1cd
    const/16 v2, 0x46

    #@1cf
    const-string v3, "405839"

    #@1d1
    aput-object v3, v0, v2

    #@1d3
    const/16 v2, 0x47

    #@1d5
    const-string v3, "405840"

    #@1d7
    aput-object v3, v0, v2

    #@1d9
    const/16 v2, 0x48

    #@1db
    const-string v3, "405841"

    #@1dd
    aput-object v3, v0, v2

    #@1df
    const/16 v2, 0x49

    #@1e1
    const-string v3, "405842"

    #@1e3
    aput-object v3, v0, v2

    #@1e5
    const/16 v2, 0x4a

    #@1e7
    const-string v3, "405843"

    #@1e9
    aput-object v3, v0, v2

    #@1eb
    const/16 v2, 0x4b

    #@1ed
    const-string v3, "405844"

    #@1ef
    aput-object v3, v0, v2

    #@1f1
    const/16 v2, 0x4c

    #@1f3
    const-string v3, "405845"

    #@1f5
    aput-object v3, v0, v2

    #@1f7
    const/16 v2, 0x4d

    #@1f9
    const-string v3, "405846"

    #@1fb
    aput-object v3, v0, v2

    #@1fd
    const/16 v2, 0x4e

    #@1ff
    const-string v3, "405847"

    #@201
    aput-object v3, v0, v2

    #@203
    const/16 v2, 0x4f

    #@205
    const-string v3, "405848"

    #@207
    aput-object v3, v0, v2

    #@209
    const/16 v2, 0x50

    #@20b
    const-string v3, "405849"

    #@20d
    aput-object v3, v0, v2

    #@20f
    const/16 v2, 0x51

    #@211
    const-string v3, "405850"

    #@213
    aput-object v3, v0, v2

    #@215
    const/16 v2, 0x52

    #@217
    const-string v3, "405851"

    #@219
    aput-object v3, v0, v2

    #@21b
    const/16 v2, 0x53

    #@21d
    const-string v3, "405852"

    #@21f
    aput-object v3, v0, v2

    #@221
    const/16 v2, 0x54

    #@223
    const-string v3, "405853"

    #@225
    aput-object v3, v0, v2

    #@227
    const/16 v2, 0x55

    #@229
    const-string v3, "405875"

    #@22b
    aput-object v3, v0, v2

    #@22d
    const/16 v2, 0x56

    #@22f
    const-string v3, "405876"

    #@231
    aput-object v3, v0, v2

    #@233
    const/16 v2, 0x57

    #@235
    const-string v3, "405877"

    #@237
    aput-object v3, v0, v2

    #@239
    const/16 v2, 0x58

    #@23b
    const-string v3, "405878"

    #@23d
    aput-object v3, v0, v2

    #@23f
    const/16 v2, 0x59

    #@241
    const-string v3, "405879"

    #@243
    aput-object v3, v0, v2

    #@245
    const/16 v2, 0x5a

    #@247
    const-string v3, "405880"

    #@249
    aput-object v3, v0, v2

    #@24b
    const/16 v2, 0x5b

    #@24d
    const-string v3, "405881"

    #@24f
    aput-object v3, v0, v2

    #@251
    const/16 v2, 0x5c

    #@253
    const-string v3, "405882"

    #@255
    aput-object v3, v0, v2

    #@257
    const/16 v2, 0x5d

    #@259
    const-string v3, "405883"

    #@25b
    aput-object v3, v0, v2

    #@25d
    const/16 v2, 0x5e

    #@25f
    const-string v3, "405884"

    #@261
    aput-object v3, v0, v2

    #@263
    const/16 v2, 0x5f

    #@265
    const-string v3, "405885"

    #@267
    aput-object v3, v0, v2

    #@269
    const/16 v2, 0x60

    #@26b
    const-string v3, "405886"

    #@26d
    aput-object v3, v0, v2

    #@26f
    const/16 v2, 0x61

    #@271
    const-string v3, "405908"

    #@273
    aput-object v3, v0, v2

    #@275
    const/16 v2, 0x62

    #@277
    const-string v3, "405909"

    #@279
    aput-object v3, v0, v2

    #@27b
    const/16 v2, 0x63

    #@27d
    const-string v3, "405910"

    #@27f
    aput-object v3, v0, v2

    #@281
    const/16 v2, 0x64

    #@283
    const-string v3, "405911"

    #@285
    aput-object v3, v0, v2

    #@287
    const/16 v2, 0x65

    #@289
    const-string v3, "405912"

    #@28b
    aput-object v3, v0, v2

    #@28d
    const/16 v2, 0x66

    #@28f
    const-string v3, "405913"

    #@291
    aput-object v3, v0, v2

    #@293
    const/16 v2, 0x67

    #@295
    const-string v3, "405914"

    #@297
    aput-object v3, v0, v2

    #@299
    const/16 v2, 0x68

    #@29b
    const-string v3, "405915"

    #@29d
    aput-object v3, v0, v2

    #@29f
    const/16 v2, 0x69

    #@2a1
    const-string v3, "405916"

    #@2a3
    aput-object v3, v0, v2

    #@2a5
    const/16 v2, 0x6a

    #@2a7
    const-string v3, "405917"

    #@2a9
    aput-object v3, v0, v2

    #@2ab
    const/16 v2, 0x6b

    #@2ad
    const-string v3, "405918"

    #@2af
    aput-object v3, v0, v2

    #@2b1
    const/16 v2, 0x6c

    #@2b3
    const-string v3, "405919"

    #@2b5
    aput-object v3, v0, v2

    #@2b7
    const/16 v2, 0x6d

    #@2b9
    const-string v3, "405920"

    #@2bb
    aput-object v3, v0, v2

    #@2bd
    const/16 v2, 0x6e

    #@2bf
    const-string v3, "405921"

    #@2c1
    aput-object v3, v0, v2

    #@2c3
    const/16 v2, 0x6f

    #@2c5
    const-string v3, "405922"

    #@2c7
    aput-object v3, v0, v2

    #@2c9
    const/16 v2, 0x70

    #@2cb
    const-string v3, "405923"

    #@2cd
    aput-object v3, v0, v2

    #@2cf
    const/16 v2, 0x71

    #@2d1
    const-string v3, "405924"

    #@2d3
    aput-object v3, v0, v2

    #@2d5
    const/16 v2, 0x72

    #@2d7
    const-string v3, "405925"

    #@2d9
    aput-object v3, v0, v2

    #@2db
    const/16 v2, 0x73

    #@2dd
    const-string v3, "405926"

    #@2df
    aput-object v3, v0, v2

    #@2e1
    const/16 v2, 0x74

    #@2e3
    const-string v3, "405927"

    #@2e5
    aput-object v3, v0, v2

    #@2e7
    const/16 v2, 0x75

    #@2e9
    const-string v3, "405928"

    #@2eb
    aput-object v3, v0, v2

    #@2ed
    const/16 v2, 0x76

    #@2ef
    const-string v3, "405929"

    #@2f1
    aput-object v3, v0, v2

    #@2f3
    const/16 v2, 0x77

    #@2f5
    const-string v3, "405930"

    #@2f7
    aput-object v3, v0, v2

    #@2f9
    const/16 v2, 0x78

    #@2fb
    const-string v3, "405931"

    #@2fd
    aput-object v3, v0, v2

    #@2ff
    const/16 v2, 0x79

    #@301
    const-string v3, "405932"

    #@303
    aput-object v3, v0, v2

    #@305
    const/16 v2, 0x7a

    #@307
    const-string v3, "310260"

    #@309
    aput-object v3, v0, v2

    #@30b
    const/16 v2, 0x7b

    #@30d
    const-string v3, "302370"

    #@30f
    aput-object v3, v0, v2

    #@311
    const/16 v2, 0x7c

    #@313
    const-string v3, "302720"

    #@315
    aput-object v3, v0, v2

    #@317
    const/16 v2, 0x7d

    #@319
    const-string v3, "502145"

    #@31b
    aput-object v3, v0, v2

    #@31d
    const/16 v2, 0x7e

    #@31f
    const-string v3, "502148"

    #@321
    aput-object v3, v0, v2

    #@323
    const/16 v2, 0x7f

    #@325
    const-string v3, "502143"

    #@327
    aput-object v3, v0, v2

    #@329
    const/16 v2, 0x80

    #@32b
    const-string v3, "502146"

    #@32d
    aput-object v3, v0, v2

    #@32f
    const/16 v2, 0x81

    #@331
    const-string v3, "502147"

    #@333
    aput-object v3, v0, v2

    #@335
    const/16 v2, 0x82

    #@337
    const-string v3, "502142"

    #@339
    aput-object v3, v0, v2

    #@33b
    sput-object v0, Lcom/android/internal/telephony/uicc/SIMRecords;->MCCMNC_CODES_HAVING_3DIGITS_MNC:[Ljava/lang/String;

    #@33d
    .line 313
    const-string v0, "content://sms"

    #@33f
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@342
    move-result-object v0

    #@343
    sput-object v0, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_URI:Landroid/net/Uri;

    #@345
    .line 314
    const-string v0, "content://sms/icc"

    #@347
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@34a
    move-result-object v0

    #@34b
    sput-object v0, Lcom/android/internal/telephony/uicc/SIMRecords;->ICC_URI:Landroid/net/Uri;

    #@34d
    .line 315
    const-string v0, "content://sms/concatpart"

    #@34f
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@352
    move-result-object v0

    #@353
    sput-object v0, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@355
    .line 316
    const-string v0, "content://sms/icc_ds"

    #@357
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@35a
    move-result-object v0

    #@35b
    sput-object v0, Lcom/android/internal/telephony/uicc/SIMRecords;->ALL_ICC_DS_URI:Landroid/net/Uri;

    #@35d
    .line 330
    const-string v0, "content://sms/inbox"

    #@35f
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@362
    move-result-object v0

    #@363
    sput-object v0, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_INBOX_URI:Landroid/net/Uri;

    #@365
    .line 331
    const-string v0, "content://sms/sent"

    #@367
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@36a
    move-result-object v0

    #@36b
    sput-object v0, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_SENT_URI:Landroid/net/Uri;

    #@36d
    .line 332
    const-string v0, "content://sms/outbox"

    #@36f
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@372
    move-result-object v0

    #@373
    sput-object v0, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_OUTBOX_URI:Landroid/net/Uri;

    #@375
    .line 337
    sput v1, Lcom/android/internal/telephony/uicc/SIMRecords;->FDN_NAME_MAX:I

    #@377
    .line 338
    sput v1, Lcom/android/internal/telephony/uicc/SIMRecords;->FDN_REC_NUM:I

    #@379
    return-void

    #@37a
    :cond_37a
    move v0, v2

    #@37b
    .line 123
    goto/16 :goto_29
.end method

.method public constructor <init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 9
    .parameter "app"
    .parameter "c"
    .parameter "ci"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 359
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/uicc/IccRecords;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@6
    .line 147
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mCphsInfo:[B

    #@8
    .line 148
    iput-boolean v4, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mCspPlmnEnabled:Z

    #@a
    .line 150
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@c
    .line 151
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efCPHS_MWI:[B

    #@e
    .line 152
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCff:[B

    #@10
    .line 153
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@12
    .line 158
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spdiNetworks:Ljava/util/ArrayList;

    #@14
    .line 160
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->pnnHomeName:Ljava/lang/String;

    #@16
    .line 241
    iput v3, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->sumNumber:I

    #@18
    .line 324
    const/4 v0, -0x1

    #@19
    iput v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mIndexOnIcc:I

    #@1b
    .line 341
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->dialogProgress:Landroid/app/AlertDialog;

    #@1d
    .line 343
    new-instance v0, Landroid/content/IntentFilter;

    #@1f
    const-string v1, "com.lge.action.CUST_COMPLETE_INFO"

    #@21
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@24
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->filter:Landroid/content/IntentFilter;

    #@26
    .line 345
    new-instance v0, Lcom/android/internal/telephony/uicc/SIMRecords$1;

    #@28
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/uicc/SIMRecords$1;-><init>(Lcom/android/internal/telephony/uicc/SIMRecords;)V

    #@2b
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->custCompletedReceiver:Landroid/content/BroadcastReceiver;

    #@2d
    .line 361
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@2f
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@31
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecordCache;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@34
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->adnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@36
    .line 363
    new-instance v0, Lcom/android/internal/telephony/uicc/VoiceMailConstants;

    #@38
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/VoiceMailConstants;-><init>()V

    #@3b
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mVmConfig:Lcom/android/internal/telephony/uicc/VoiceMailConstants;

    #@3d
    .line 364
    new-instance v0, Lcom/android/internal/telephony/uicc/SpnOverride;

    #@3f
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/SpnOverride;-><init>()V

    #@42
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mSpnOverride:Lcom/android/internal/telephony/uicc/SpnOverride;

    #@44
    .line 366
    iput-boolean v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@46
    .line 369
    iput v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@48
    .line 371
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@4a
    const/16 v1, 0x15

    #@4c
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setOnSmsOnSim(Landroid/os/Handler;ILjava/lang/Object;)V

    #@4f
    .line 372
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@51
    const/16 v1, 0x1f

    #@53
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->registerForIccRefresh(Landroid/os/Handler;ILjava/lang/Object;)V

    #@56
    .line 374
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->resetRecords()V

    #@59
    .line 375
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5b
    invoke-virtual {v0, p0, v4, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->registerForReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5e
    .line 377
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@61
    move-result-object v0

    #@62
    const-string v1, "KT"

    #@64
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v0

    #@68
    if-eqz v0, :cond_75

    #@6a
    .line 378
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@6c
    const/16 v1, 0x32

    #@6e
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@71
    move-result-object v1

    #@72
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->smartCardGetATR(Landroid/os/Message;)V

    #@75
    .line 382
    :cond_75
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/uicc/SIMRecords;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->dialogProgress:Landroid/app/AlertDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$002(Lcom/android/internal/telephony/uicc/SIMRecords;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 116
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->dialogProgress:Landroid/app/AlertDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/uicc/SIMRecords;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/uicc/SIMRecords;->setUiccType(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$400()Z
    .registers 1

    #@0
    .prologue
    .line 116
    sget-boolean v0, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/uicc/SIMRecords;)[B
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->msisdnNumber:[B

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/internal/telephony/uicc/SIMRecords;[B)[B
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 116
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->msisdnNumber:[B

    #@2
    return-object p1
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/uicc/SIMRecords;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->sumNumber:I

    #@2
    return v0
.end method

.method static synthetic access$602(Lcom/android/internal/telephony/uicc/SIMRecords;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 116
    iput p1, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->sumNumber:I

    #@2
    return p1
.end method

.method static synthetic access$612(Lcom/android/internal/telephony/uicc/SIMRecords;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->sumNumber:I

    #@2
    add-int/2addr v0, p1

    #@3
    iput v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->sumNumber:I

    #@5
    return v0
.end method

.method private emptyUSimCache()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2296
    const-string v0, "emptyUSimCache(), start"

    #@3
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@6
    .line 2297
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v1, "emptyUSimCache(), isSimSmsDeleteAll: "

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    sget-boolean v1, Lcom/android/internal/telephony/uicc/SIMRecords;->isSimSmsDeleteAll:Z

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1e
    .line 2298
    sget-boolean v0, Lcom/android/internal/telephony/uicc/SIMRecords;->isSimSmsDeleteAll:Z

    #@20
    if-nez v0, :cond_5c

    #@22
    .line 2299
    const-string v0, "emptyUSimCache(), Delete SIMRecords.ICC_URI"

    #@24
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@27
    .line 2300
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@29
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@2b
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2e
    move-result-object v1

    #@2f
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ICC_URI:Landroid/net/Uri;

    #@31
    invoke-static {v0, v1, v2, v3, v3}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@34
    .line 2301
    const/4 v0, 0x1

    #@35
    sput-boolean v0, Lcom/android/internal/telephony/uicc/SIMRecords;->isSimSmsDeleteAll:Z

    #@37
    .line 2302
    new-instance v0, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v1, "emptyUSimCache(), After isSimSmsDeleteAll: "

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    sget-boolean v1, Lcom/android/internal/telephony/uicc/SIMRecords;->isSimSmsDeleteAll:Z

    #@44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@47
    move-result-object v0

    #@48
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v0

    #@4c
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4f
    .line 2303
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@51
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@53
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@56
    move-result-object v1

    #@57
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@59
    invoke-static {v0, v1, v2, v3, v3}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@5c
    .line 2305
    :cond_5c
    return-void
.end method

.method private extractEmailAddressFromMessageBody(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .registers 15
    .parameter "originatingAddress"
    .parameter "messageBody"

    #@0
    .prologue
    .line 2670
    if-nez p2, :cond_4

    #@2
    .line 2671
    const/4 v6, 0x0

    #@3
    .line 2725
    :goto_3
    return-object v6

    #@4
    .line 2674
    :cond_4
    const-string v10, "( /)|( )"

    #@6
    const/4 v11, 0x2

    #@7
    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@a
    move-result-object v6

    #@b
    .line 2675
    .local v6, parts:[Ljava/lang/String;
    array-length v10, v6

    #@c
    const/4 v11, 0x2

    #@d
    if-ge v10, v11, :cond_11

    #@f
    .line 2676
    const/4 v6, 0x0

    #@10
    goto :goto_3

    #@11
    .line 2679
    :cond_11
    const/4 v10, 0x0

    #@12
    aget-object v1, v6, v10

    #@14
    .line 2680
    .local v1, emailFrom:Ljava/lang/String;
    const/4 v10, 0x1

    #@15
    aget-object v0, v6, v10

    #@17
    .line 2682
    .local v0, emailBody:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@1a
    move-result v10

    #@1b
    const/4 v11, 0x4

    #@1c
    if-ne v10, v11, :cond_2b

    #@1e
    const-string v10, "6245"

    #@20
    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v10

    #@24
    if-eqz v10, :cond_2b

    #@26
    .line 2683
    const/4 v2, 0x1

    #@27
    .line 2692
    .local v2, isEmail:Z
    :goto_27
    if-nez v2, :cond_30

    #@29
    .line 2693
    const/4 v6, 0x0

    #@2a
    goto :goto_3

    #@2b
    .line 2685
    .end local v2           #isEmail:Z
    :cond_2b
    invoke-static {v1}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@2e
    move-result v2

    #@2f
    .restart local v2       #isEmail:Z
    goto :goto_27

    #@30
    .line 2696
    :cond_30
    invoke-static {v1}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@33
    move-result v10

    #@34
    if-nez v10, :cond_61

    #@36
    const-string v10, "("

    #@38
    invoke-virtual {v1, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@3b
    move-result v10

    #@3c
    if-nez v10, :cond_61

    #@3e
    .line 2697
    const-string v10, ")"

    #@40
    invoke-virtual {p2, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@43
    move-result v3

    #@44
    .line 2698
    .local v3, parenthesis:I
    const-string v10, ")"

    #@46
    invoke-virtual {v1, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@49
    move-result v10

    #@4a
    const/4 v11, -0x1

    #@4b
    if-ne v10, v11, :cond_61

    #@4d
    const/4 v10, -0x1

    #@4e
    if-eq v3, v10, :cond_61

    #@50
    .line 2699
    const/4 v10, 0x0

    #@51
    add-int/lit8 v11, v3, 0x1

    #@53
    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    .line 2700
    add-int/lit8 v10, v3, 0x2

    #@59
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@5c
    move-result v11

    #@5d
    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@60
    move-result-object v0

    #@61
    .line 2704
    .end local v3           #parenthesis:I
    :cond_61
    const-string v10, "( /)|( )"

    #@63
    const/4 v11, 0x2

    #@64
    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@67
    move-result-object v7

    #@68
    .line 2705
    .local v7, parts2:[Ljava/lang/String;
    array-length v10, v7

    #@69
    const/4 v11, 0x1

    #@6a
    if-le v10, v11, :cond_9a

    #@6c
    .line 2706
    const/4 v10, 0x0

    #@6d
    aget-object v9, v7, v10

    #@6f
    .line 2707
    .local v9, tempFrom:Ljava/lang/String;
    const/4 v10, 0x1

    #@70
    aget-object v8, v7, v10

    #@72
    .line 2709
    .local v8, tempBody:Ljava/lang/String;
    const-string v10, "("

    #@74
    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@77
    move-result v5

    #@78
    .line 2710
    .local v5, parenthesisStart:I
    const/4 v10, -0x1

    #@79
    if-eq v5, v10, :cond_9a

    #@7b
    .line 2711
    const-string v10, ")"

    #@7d
    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@80
    move-result v4

    #@81
    .line 2713
    .local v4, parenthesisEnd:I
    const/4 v10, -0x1

    #@82
    if-eq v4, v10, :cond_9a

    #@84
    if-le v4, v5, :cond_9a

    #@86
    .line 2714
    add-int/lit8 v10, v5, 0x1

    #@88
    invoke-virtual {v9, v10, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@8b
    move-result-object v9

    #@8c
    .line 2715
    invoke-static {v9}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@8f
    move-result v10

    #@90
    if-nez v10, :cond_98

    #@92
    invoke-static {v9}, Landroid/provider/Telephony$Mms;->isPhoneNumber(Ljava/lang/String;)Z

    #@95
    move-result v10

    #@96
    if-eqz v10, :cond_9a

    #@98
    .line 2716
    :cond_98
    move-object v1, v9

    #@99
    .line 2717
    move-object v0, v8

    #@9a
    .line 2723
    .end local v4           #parenthesisEnd:I
    .end local v5           #parenthesisStart:I
    .end local v8           #tempBody:Ljava/lang/String;
    .end local v9           #tempFrom:Ljava/lang/String;
    :cond_9a
    const/4 v10, 0x0

    #@9b
    aput-object v1, v6, v10

    #@9d
    .line 2724
    const/4 v10, 0x1

    #@9e
    aput-object v0, v6, v10

    #@a0
    goto/16 :goto_3
.end method

.method private getExtFromEf(I)I
    .registers 5
    .parameter "ef"

    #@0
    .prologue
    .line 470
    packed-switch p1, :pswitch_data_16

    #@3
    .line 480
    const/16 v0, 0x6f4a

    #@5
    .line 482
    .local v0, ext:I
    :goto_5
    return v0

    #@6
    .line 473
    .end local v0           #ext:I
    :pswitch_6
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@8
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@b
    move-result-object v1

    #@c
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_USIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@e
    if-ne v1, v2, :cond_13

    #@10
    .line 474
    const/16 v0, 0x6f4e

    #@12
    .restart local v0       #ext:I
    goto :goto_5

    #@13
    .line 476
    .end local v0           #ext:I
    :cond_13
    const/16 v0, 0x6f4a

    #@15
    .line 478
    .restart local v0       #ext:I
    goto :goto_5

    #@16
    .line 470
    :pswitch_data_16
    .packed-switch 0x6f40
        :pswitch_6
    .end packed-switch
.end method

.method private getSpnFsm(ZLandroid/os/AsyncResult;)V
    .registers 11
    .parameter "start"
    .parameter "ar"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/16 v6, 0xc

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 3177
    if-eqz p1, :cond_28

    #@7
    .line 3180
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@9
    sget-object v3, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->READ_SPN_3GPP:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@b
    if-eq v2, v3, :cond_1f

    #@d
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@f
    sget-object v3, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->READ_SPN_CPHS:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@11
    if-eq v2, v3, :cond_1f

    #@13
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@15
    sget-object v3, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->READ_SPN_SHORT_CPHS:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@17
    if-eq v2, v3, :cond_1f

    #@19
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@1b
    sget-object v3, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->INIT:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@1d
    if-ne v2, v3, :cond_24

    #@1f
    .line 3186
    :cond_1f
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->INIT:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@21
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@23
    .line 3277
    :goto_23
    return-void

    #@24
    .line 3189
    :cond_24
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->INIT:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@26
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@28
    .line 3193
    :cond_28
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords$4;->$SwitchMap$com$android$internal$telephony$uicc$SIMRecords$Get_Spn_Fsm_State:[I

    #@2a
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@2c
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->ordinal()I

    #@2f
    move-result v3

    #@30
    aget v2, v2, v3

    #@32
    packed-switch v2, :pswitch_data_18a

    #@35
    .line 3275
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->IDLE:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@37
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@39
    goto :goto_23

    #@3a
    .line 3195
    :pswitch_3a
    iput-object v7, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@3c
    .line 3197
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@3e
    const/16 v3, 0x6f46

    #@40
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@47
    .line 3199
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@49
    add-int/lit8 v2, v2, 0x1

    #@4b
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@4d
    .line 3201
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->READ_SPN_3GPP:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@4f
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@51
    goto :goto_23

    #@52
    .line 3204
    :pswitch_52
    if-eqz p2, :cond_e0

    #@54
    iget-object v2, p2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@56
    if-nez v2, :cond_e0

    #@58
    .line 3205
    iget-object v2, p2, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@5a
    check-cast v2, [B

    #@5c
    move-object v0, v2

    #@5d
    check-cast v0, [B

    #@5f
    .line 3206
    .local v0, data:[B
    aget-byte v2, v0, v4

    #@61
    and-int/lit16 v2, v2, 0xff

    #@63
    iput v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnDisplayCondition:I

    #@65
    .line 3208
    aget-byte v2, v0, v5

    #@67
    and-int/lit16 v2, v2, 0xff

    #@69
    const/16 v3, 0xff

    #@6b
    if-ne v2, v3, :cond_d6

    #@6d
    .line 3209
    iput-object v7, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@6f
    .line 3214
    :goto_6f
    new-instance v2, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v3, "Load EF_SPN: "

    #@76
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v2

    #@7a
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@7c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v2

    #@80
    const-string v3, " spnDisplayCondition: "

    #@82
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v2

    #@86
    iget v3, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnDisplayCondition:I

    #@88
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v2

    #@8c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v2

    #@90
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@93
    .line 3218
    const-string v2, "1"

    #@95
    const-string v3, "persist.service.vsim.enable"

    #@97
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@9a
    move-result-object v3

    #@9b
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9e
    move-result v2

    #@9f
    if-eqz v2, :cond_c9

    #@a1
    .line 3220
    const-string v2, "persist.service.vsim.spn"

    #@a3
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@a6
    move-result-object v1

    #@a7
    .line 3222
    .local v1, vsim_spn:Ljava/lang/String;
    const-string v2, "GSM"

    #@a9
    new-instance v3, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    const-string v4, "[LGE_UICC] VSIM_SPN : "

    #@b0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v3

    #@b4
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v3

    #@b8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v3

    #@bc
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    .line 3224
    if-eqz v1, :cond_c9

    #@c1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@c4
    move-result v2

    #@c5
    if-le v2, v5, :cond_c9

    #@c7
    .line 3225
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@c9
    .line 3229
    .end local v1           #vsim_spn:Ljava/lang/String;
    :cond_c9
    const-string v2, "gsm.sim.operator.alpha"

    #@cb
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@cd
    invoke-direct {p0, v2, v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@d0
    .line 3231
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->IDLE:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@d2
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@d4
    goto/16 :goto_23

    #@d6
    .line 3212
    :cond_d6
    array-length v2, v0

    #@d7
    add-int/lit8 v2, v2, -0x1

    #@d9
    invoke-static {v0, v5, v2}, Lcom/android/internal/telephony/uicc/IccUtils;->adnStringFieldToString([BII)Ljava/lang/String;

    #@dc
    move-result-object v2

    #@dd
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@df
    goto :goto_6f

    #@e0
    .line 3233
    .end local v0           #data:[B
    :cond_e0
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@e2
    const/16 v3, 0x6f14

    #@e4
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@e7
    move-result-object v4

    #@e8
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@eb
    .line 3235
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@ed
    add-int/lit8 v2, v2, 0x1

    #@ef
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@f1
    .line 3237
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->READ_SPN_CPHS:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@f3
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@f5
    .line 3241
    const/4 v2, -0x1

    #@f6
    iput v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnDisplayCondition:I

    #@f8
    goto/16 :goto_23

    #@fa
    .line 3245
    :pswitch_fa
    if-eqz p2, :cond_133

    #@fc
    iget-object v2, p2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@fe
    if-nez v2, :cond_133

    #@100
    .line 3246
    iget-object v2, p2, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@102
    check-cast v2, [B

    #@104
    move-object v0, v2

    #@105
    check-cast v0, [B

    #@107
    .line 3247
    .restart local v0       #data:[B
    array-length v2, v0

    #@108
    invoke-static {v0, v4, v2}, Lcom/android/internal/telephony/uicc/IccUtils;->adnStringFieldToString([BII)Ljava/lang/String;

    #@10b
    move-result-object v2

    #@10c
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@10e
    .line 3249
    new-instance v2, Ljava/lang/StringBuilder;

    #@110
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@113
    const-string v3, "Load EF_SPN_CPHS: "

    #@115
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v2

    #@119
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@11b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v2

    #@11f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@122
    move-result-object v2

    #@123
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@126
    .line 3250
    const-string v2, "gsm.sim.operator.alpha"

    #@128
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@12a
    invoke-direct {p0, v2, v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@12d
    .line 3252
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->IDLE:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@12f
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@131
    goto/16 :goto_23

    #@133
    .line 3254
    .end local v0           #data:[B
    :cond_133
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@135
    const/16 v3, 0x6f18

    #@137
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@13a
    move-result-object v4

    #@13b
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@13e
    .line 3256
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@140
    add-int/lit8 v2, v2, 0x1

    #@142
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@144
    .line 3258
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->READ_SPN_SHORT_CPHS:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@146
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@148
    goto/16 :goto_23

    #@14a
    .line 3262
    :pswitch_14a
    if-eqz p2, :cond_183

    #@14c
    iget-object v2, p2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@14e
    if-nez v2, :cond_183

    #@150
    .line 3263
    iget-object v2, p2, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@152
    check-cast v2, [B

    #@154
    move-object v0, v2

    #@155
    check-cast v0, [B

    #@157
    .line 3264
    .restart local v0       #data:[B
    array-length v2, v0

    #@158
    invoke-static {v0, v4, v2}, Lcom/android/internal/telephony/uicc/IccUtils;->adnStringFieldToString([BII)Ljava/lang/String;

    #@15b
    move-result-object v2

    #@15c
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@15e
    .line 3266
    new-instance v2, Ljava/lang/StringBuilder;

    #@160
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@163
    const-string v3, "Load EF_SPN_SHORT_CPHS: "

    #@165
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v2

    #@169
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@16b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v2

    #@16f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@172
    move-result-object v2

    #@173
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@176
    .line 3267
    const-string v2, "gsm.sim.operator.alpha"

    #@178
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@17a
    invoke-direct {p0, v2, v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@17d
    .line 3272
    .end local v0           #data:[B
    :goto_17d
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;->IDLE:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@17f
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnState:Lcom/android/internal/telephony/uicc/SIMRecords$Get_Spn_Fsm_State;

    #@181
    goto/16 :goto_23

    #@183
    .line 3269
    :cond_183
    const-string v2, "No SPN loaded in either CHPS or 3GPP"

    #@185
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@188
    goto :goto_17d

    #@189
    .line 3193
    nop

    #@18a
    :pswitch_data_18a
    .packed-switch 0x1
        :pswitch_3a
        :pswitch_52
        :pswitch_fa
        :pswitch_14a
    .end packed-switch
.end method

.method private handleEfCspData([B)V
    .registers 10
    .parameter "data"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 3366
    array-length v5, p1

    #@2
    div-int/lit8 v3, v5, 0x2

    #@4
    .line 3368
    .local v3, usedCspGroups:I
    const/16 v4, -0x40

    #@6
    .line 3370
    .local v4, valueAddedServicesGroup:B
    iput-boolean v7, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mCspPlmnEnabled:Z

    #@8
    .line 3371
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v3, :cond_77

    #@b
    .line 3372
    mul-int/lit8 v5, v1, 0x2

    #@d
    aget-byte v5, p1, v5

    #@f
    if-ne v5, v4, :cond_74

    #@11
    .line 3373
    new-instance v5, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v6, "[CSP] found ValueAddedServicesGroup, value "

    #@18
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v5

    #@1c
    mul-int/lit8 v6, v1, 0x2

    #@1e
    add-int/lit8 v6, v6, 0x1

    #@20
    aget-byte v6, p1, v6

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@2d
    .line 3374
    mul-int/lit8 v5, v1, 0x2

    #@2f
    add-int/lit8 v5, v5, 0x1

    #@31
    aget-byte v5, p1, v5

    #@33
    and-int/lit16 v5, v5, 0x80

    #@35
    const/16 v6, 0x80

    #@37
    if-ne v5, v6, :cond_3c

    #@39
    .line 3378
    iput-boolean v7, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mCspPlmnEnabled:Z

    #@3b
    .line 3402
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 3380
    :cond_3c
    const/4 v5, 0x0

    #@3d
    iput-boolean v5, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mCspPlmnEnabled:Z

    #@3f
    .line 3383
    const-string v5, "[CSP] Don\'t set Automatic Network Selection"

    #@41
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@44
    .line 3385
    const-string v5, "ATT"

    #@46
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@49
    move-result v5

    #@4a
    if-eqz v5, :cond_3b

    #@4c
    .line 3386
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->getContext()Landroid/content/Context;

    #@4f
    move-result-object v5

    #@50
    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@53
    move-result-object v2

    #@54
    .line 3387
    .local v2, sp:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@57
    move-result-object v0

    #@58
    .line 3388
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v5, "network_selection_key"

    #@5a
    const-string v6, ""

    #@5c
    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@5f
    .line 3389
    const-string v5, "network_selection_name_key"

    #@61
    const-string v6, ""

    #@63
    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@66
    .line 3391
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@69
    move-result v5

    #@6a
    if-nez v5, :cond_3b

    #@6c
    .line 3392
    const-string v5, "GSM"

    #@6e
    const-string v6, "failed to commit automatic network selection preference"

    #@70
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    goto :goto_3b

    #@74
    .line 3371
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v2           #sp:Landroid/content/SharedPreferences;
    :cond_74
    add-int/lit8 v1, v1, 0x1

    #@76
    goto :goto_9

    #@77
    .line 3401
    :cond_77
    const-string v5, "[CSP] Value Added Service Group (0xC0), not found!"

    #@79
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@7c
    goto :goto_3b
.end method

.method private handleFileUpdate(I)V
    .registers 7
    .parameter "efid"

    #@0
    .prologue
    const/16 v3, 0x6f40

    #@2
    const/4 v4, 0x1

    #@3
    .line 2025
    sparse-switch p1, :sswitch_data_d2

    #@6
    .line 2085
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->adnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->reset()V

    #@b
    .line 2086
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->fetchSimRecords()V

    #@e
    .line 2089
    :goto_e
    return-void

    #@f
    .line 2027
    :sswitch_f
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@11
    add-int/lit8 v0, v0, 0x1

    #@13
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@15
    .line 2028
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@17
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@19
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@1c
    const/16 v1, 0x6fc7

    #@1e
    const/16 v2, 0x6fc8

    #@20
    iget v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mailboxIndex:I

    #@22
    const/4 v4, 0x6

    #@23
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->loadFromEF(IIILandroid/os/Message;)V

    #@2a
    goto :goto_e

    #@2b
    .line 2032
    :sswitch_2b
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@2d
    add-int/lit8 v0, v0, 0x1

    #@2f
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@31
    .line 2033
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@33
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@35
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@38
    const/16 v1, 0x6f17

    #@3a
    const/16 v2, 0x6f4a

    #@3c
    const/16 v3, 0xb

    #@3e
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->loadFromEF(IIILandroid/os/Message;)V

    #@45
    goto :goto_e

    #@46
    .line 2037
    :sswitch_46
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@48
    add-int/lit8 v0, v0, 0x1

    #@4a
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@4c
    .line 2038
    const-string v0, "[CSP] SIM Refresh for EF_CSP_CPHS"

    #@4e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@51
    .line 2039
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@53
    const/16 v1, 0x6f15

    #@55
    const/16 v2, 0x21

    #@57
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@5e
    goto :goto_e

    #@5f
    .line 2043
    :sswitch_5f
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@61
    add-int/lit8 v0, v0, 0x1

    #@63
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@65
    .line 2044
    const-string v0, "GSM"

    #@67
    const-string v1, "SIM Refresh called for EF_MSISDN"

    #@69
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 2048
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@6e
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@70
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@73
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->getExtFromEf(I)I

    #@76
    move-result v1

    #@77
    const/16 v2, 0xa

    #@79
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@7c
    move-result-object v2

    #@7d
    invoke-virtual {v0, v3, v1, v4, v2}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->loadFromEF(IIILandroid/os/Message;)V

    #@80
    goto :goto_e

    #@81
    .line 2053
    :sswitch_81
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@83
    add-int/lit8 v0, v0, 0x1

    #@85
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@87
    .line 2054
    const-string v0, "GSM"

    #@89
    const-string v1, "SIM Refresh called for EF_CFIS"

    #@8b
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 2055
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@90
    const/16 v1, 0x6fcb

    #@92
    const/16 v2, 0x20

    #@94
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@97
    move-result-object v2

    #@98
    invoke-virtual {v0, v1, v4, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V

    #@9b
    goto/16 :goto_e

    #@9d
    .line 2059
    :sswitch_9d
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@9f
    add-int/lit8 v0, v0, 0x1

    #@a1
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@a3
    .line 2060
    const-string v0, "GSM"

    #@a5
    const-string v1, "SIM Refresh called for EF_CFF_CPHS"

    #@a7
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 2061
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@ac
    const/16 v1, 0x6f13

    #@ae
    const/16 v2, 0x18

    #@b0
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@b3
    move-result-object v2

    #@b4
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@b7
    goto/16 :goto_e

    #@b9
    .line 2078
    :sswitch_b9
    new-instance v0, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v1, "[lge] refresh efid: "

    #@c0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v0

    #@c4
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v0

    #@c8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v0

    #@cc
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@cf
    goto/16 :goto_e

    #@d1
    .line 2025
    nop

    #@d2
    :sswitch_data_d2
    .sparse-switch
        0x2fe7 -> :sswitch_b9
        0x2fe8 -> :sswitch_b9
        0x2ff0 -> :sswitch_b9
        0x4f1b -> :sswitch_b9
        0x4f1c -> :sswitch_b9
        0x4f1e -> :sswitch_b9
        0x4f1f -> :sswitch_b9
        0x4f20 -> :sswitch_b9
        0x6f13 -> :sswitch_9d
        0x6f15 -> :sswitch_46
        0x6f17 -> :sswitch_2b
        0x6f1b -> :sswitch_b9
        0x6f40 -> :sswitch_5f
        0x6fc7 -> :sswitch_f
        0x6fcb -> :sswitch_81
        0x9f01 -> :sswitch_b9
        0x9f02 -> :sswitch_b9
        0x9f03 -> :sswitch_b9
        0x9f04 -> :sswitch_b9
    .end sparse-switch
.end method

.method private handleSimRefresh(Lcom/android/internal/telephony/uicc/IccRefreshResponse;)V
    .registers 12
    .parameter "refreshResponse"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x0

    #@2
    .line 2092
    if-nez p1, :cond_a

    #@4
    .line 2093
    const-string v6, "handleSimRefresh received without input"

    #@6
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@9
    .line 2184
    :cond_9
    :goto_9
    return-void

    #@a
    .line 2108
    :cond_a
    iget v6, p1, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->refreshResult:I

    #@c
    packed-switch v6, :pswitch_data_c0

    #@f
    .line 2181
    const-string v6, "handleSimRefresh with unknown operation"

    #@11
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@14
    goto :goto_9

    #@15
    .line 2110
    :pswitch_15
    const-string v6, "handleSimRefresh with SIM_FILE_UPDATED"

    #@17
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1a
    .line 2111
    iget v6, p1, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->efId:I

    #@1c
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->handleFileUpdate(I)V

    #@1f
    .line 2113
    new-instance v1, Landroid/content/Intent;

    #@21
    const-string v6, "android.intent.action.OTA_USIM_REFRESH_FILE_UPDATED"

    #@23
    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@26
    .line 2114
    .local v1, intent_fcn:Landroid/content/Intent;
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@28
    invoke-virtual {v6, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@2b
    .line 2117
    iget v6, p1, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->efId:I

    #@2d
    const/16 v7, 0x2ff0

    #@2f
    if-ne v6, v7, :cond_9

    #@31
    .line 2119
    new-instance v0, Landroid/content/Intent;

    #@33
    const-string v6, "android.intent.action.OTA_USIM_REFRESH_FILE_UPDATED_DCM_LOCK"

    #@35
    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@38
    .line 2120
    .local v0, intent_dcm_lock:Landroid/content/Intent;
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@3a
    invoke-virtual {v6, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@3d
    goto :goto_9

    #@3e
    .line 2125
    .end local v0           #intent_dcm_lock:Landroid/content/Intent;
    .end local v1           #intent_fcn:Landroid/content/Intent;
    :pswitch_3e
    const-string v6, "handleSimRefresh with SIM_REFRESH_INIT"

    #@40
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@43
    .line 2127
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->adnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@45
    invoke-virtual {v6}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->reset()V

    #@48
    .line 2130
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@4a
    invoke-virtual {v6, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForReady(Landroid/os/Handler;)V

    #@4d
    .line 2131
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@4f
    const/4 v7, 0x1

    #@50
    invoke-virtual {v6, p0, v7, v9}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->registerForReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@53
    .line 2133
    sget v6, Lcom/android/internal/telephony/uicc/SIMRecords;->mUiccType:I

    #@55
    const/4 v7, 0x5

    #@56
    if-ne v6, v7, :cond_6f

    #@58
    .line 2134
    sget-boolean v6, Lcom/android/internal/telephony/uicc/SIMRecords;->mWriteEFRoaming:Z

    #@5a
    if-eqz v6, :cond_6f

    #@5c
    .line 2135
    const-string v6, "send intent LGU_ROAMING_SET_FINISHED"

    #@5e
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@61
    .line 2136
    new-instance v3, Landroid/content/Intent;

    #@63
    const-string v6, "android.intent.action.LGU_ROAMING_SET_FINISHED"

    #@65
    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@68
    .line 2137
    .local v3, intent_init:Landroid/content/Intent;
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@6a
    invoke-virtual {v6, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@6d
    .line 2138
    sput-boolean v8, Lcom/android/internal/telephony/uicc/SIMRecords;->mWriteEFRoaming:Z

    #@6f
    .line 2144
    .end local v3           #intent_init:Landroid/content/Intent;
    :cond_6f
    new-instance v2, Landroid/content/Intent;

    #@71
    const-string v6, "android.intent.action.OTA_USIM_REFRESH_INIT"

    #@73
    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@76
    .line 2145
    .local v2, intent_ifcn:Landroid/content/Intent;
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@78
    invoke-virtual {v6, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@7b
    goto :goto_9

    #@7c
    .line 2149
    .end local v2           #intent_ifcn:Landroid/content/Intent;
    :pswitch_7c
    const-string v6, "handleSimRefresh with SIM_REFRESH_RESET"

    #@7e
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@81
    .line 2150
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->powerOffOnSimReset()Z

    #@84
    move-result v6

    #@85
    if-eqz v6, :cond_9a

    #@87
    .line 2151
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@89
    invoke-interface {v6, v8, v9}, Lcom/android/internal/telephony/CommandsInterface;->setRadioPower(ZLandroid/os/Message;)V

    #@8c
    .line 2168
    :cond_8c
    :goto_8c
    new-instance v5, Landroid/content/Intent;

    #@8e
    const-string v6, "android.intent.action.OTA_USIM_REFRESH_TO_RESET"

    #@90
    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@93
    .line 2169
    .local v5, intent_reset:Landroid/content/Intent;
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@95
    invoke-virtual {v6, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@98
    goto/16 :goto_9

    #@9a
    .line 2160
    .end local v5           #intent_reset:Landroid/content/Intent;
    :cond_9a
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9c
    invoke-virtual {v6}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@9f
    move-result-object v6

    #@a0
    sget-object v7, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@a2
    if-ne v6, v7, :cond_8c

    #@a4
    .line 2161
    const-string v6, "GSM"

    #@a6
    const-string v7, "[SIMRecords] APPSTATE_READY "

    #@a8
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    goto :goto_8c

    #@ac
    .line 2174
    :pswitch_ac
    const-string v6, "Send SIM_REFRESH_RESET event to the module to reset the phone by user."

    #@ae
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@b1
    .line 2175
    new-instance v4, Landroid/content/Intent;

    #@b3
    const-string v6, "android.intent.action.OTA_USIM_REFRESH_TO_RESET"

    #@b5
    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b8
    .line 2176
    .local v4, intent_proact:Landroid/content/Intent;
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@ba
    invoke-virtual {v6, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@bd
    goto/16 :goto_9

    #@bf
    .line 2108
    nop

    #@c0
    :pswitch_data_c0
    .packed-switch 0x0
        :pswitch_15
        :pswitch_3e
        :pswitch_7c
        :pswitch_ac
    .end packed-switch
.end method

.method private handleSms([B)V
    .registers 9
    .parameter "ba"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2217
    aget-byte v3, p1, v6

    #@3
    if-eqz v3, :cond_1f

    #@5
    .line 2218
    const-string v3, "ENF"

    #@7
    new-instance v4, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v5, "status : "

    #@e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    aget-byte v5, p1, v6

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 2222
    :cond_1f
    aget-byte v3, p1, v6

    #@21
    const/4 v4, 0x3

    #@22
    if-ne v3, v4, :cond_3b

    #@24
    .line 2223
    array-length v1, p1

    #@25
    .line 2227
    .local v1, n:I
    add-int/lit8 v3, v1, -0x1

    #@27
    new-array v2, v3, [B

    #@29
    .line 2228
    .local v2, pdu:[B
    const/4 v3, 0x1

    #@2a
    add-int/lit8 v4, v1, -0x1

    #@2c
    invoke-static {p1, v3, v2, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2f
    .line 2229
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/SmsMessage;->createFromPdu([B)Lcom/android/internal/telephony/gsm/SmsMessage;

    #@32
    move-result-object v0

    #@33
    .line 2231
    .local v0, message:Lcom/android/internal/telephony/gsm/SmsMessage;
    iget v3, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mIndexOnIcc:I

    #@35
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/SmsMessage;->setIndexOnIcc(I)V

    #@38
    .line 2234
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->dispatchGsmMessage(Lcom/android/internal/telephony/SmsMessageBase;)I

    #@3b
    .line 2236
    .end local v0           #message:Lcom/android/internal/telephony/gsm/SmsMessage;
    .end local v1           #n:I
    .end local v2           #pdu:[B
    :cond_3b
    return-void
.end method

.method private handleSmses(Ljava/util/ArrayList;)V
    .registers 13
    .parameter "messages"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 2240
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 2242
    .local v1, count:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v1, :cond_55

    #@9
    .line 2243
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v6

    #@d
    check-cast v6, [B

    #@f
    move-object v0, v6

    #@10
    check-cast v0, [B

    #@12
    .line 2245
    .local v0, ba:[B
    aget-byte v6, v0, v9

    #@14
    if-eqz v6, :cond_3a

    #@16
    .line 2246
    const-string v6, "ENF"

    #@18
    new-instance v7, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v8, "status "

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v7

    #@27
    const-string v8, ": "

    #@29
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    aget-byte v8, v0, v9

    #@2f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v7

    #@37
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 2251
    :cond_3a
    aget-byte v6, v0, v9

    #@3c
    const/4 v7, 0x3

    #@3d
    if-ne v6, v7, :cond_52

    #@3f
    .line 2252
    array-length v4, v0

    #@40
    .line 2256
    .local v4, n:I
    add-int/lit8 v6, v4, -0x1

    #@42
    new-array v5, v6, [B

    #@44
    .line 2257
    .local v5, pdu:[B
    add-int/lit8 v6, v4, -0x1

    #@46
    invoke-static {v0, v10, v5, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@49
    .line 2258
    invoke-static {v5}, Lcom/android/internal/telephony/gsm/SmsMessage;->createFromPdu([B)Lcom/android/internal/telephony/gsm/SmsMessage;

    #@4c
    move-result-object v3

    #@4d
    .line 2260
    .local v3, message:Lcom/android/internal/telephony/gsm/SmsMessage;
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->dispatchGsmMessage(Lcom/android/internal/telephony/SmsMessageBase;)I

    #@50
    .line 2265
    aput-byte v10, v0, v9

    #@52
    .line 2242
    .end local v3           #message:Lcom/android/internal/telephony/gsm/SmsMessage;
    .end local v4           #n:I
    .end local v5           #pdu:[B
    :cond_52
    add-int/lit8 v2, v2, 0x1

    #@54
    goto :goto_7

    #@55
    .line 2273
    .end local v0           #ba:[B
    :cond_55
    return-void
.end method

.method private handleUSimSmses(Ljava/util/ArrayList;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2277
    .local p1, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    const-string v1, "handleUSimSmses(), start"

    #@2
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 2279
    const/4 v1, 0x0

    #@6
    const-string v2, "uicc_csim"

    #@8
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_11

    #@e
    .line 2280
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->emptyUSimCache()V

    #@11
    .line 2284
    :cond_11
    move-object v0, p1

    #@12
    .line 2285
    .local v0, simSmsMessage:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    new-instance v1, Ljava/lang/Thread;

    #@14
    new-instance v2, Lcom/android/internal/telephony/uicc/SIMRecords$2;

    #@16
    invoke-direct {v2, p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords$2;-><init>(Lcom/android/internal/telephony/uicc/SIMRecords;Ljava/util/ArrayList;)V

    #@19
    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@1c
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    #@1f
    .line 2292
    return-void
.end method

.method private insertSmsDB(ILcom/android/internal/telephony/gsm/SmsMessage;)Landroid/net/Uri;
    .registers 25
    .parameter "statsOnSim"
    .parameter "sms"

    #@0
    .prologue
    .line 2338
    const/4 v6, 0x0

    #@1
    .line 2339
    .local v6, boxType:I
    const/4 v7, 0x0

    #@2
    .line 2340
    .local v7, boxTypeKR:I
    const/4 v5, 0x0

    #@3
    .line 2341
    .local v5, body:Ljava/lang/String;
    const/4 v4, 0x0

    #@4
    .line 2342
    .local v4, address:Ljava/lang/String;
    const-wide/16 v8, -0x1

    #@6
    .line 2343
    .local v8, date:J
    const/4 v15, 0x0

    #@7
    .line 2344
    .local v15, smsBoxUri:Landroid/net/Uri;
    const/16 v16, 0x0

    #@9
    .line 2345
    .local v16, smsInsertUri:Landroid/net/Uri;
    new-instance v19, Landroid/content/ContentValues;

    #@b
    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    #@e
    .line 2351
    .local v19, values:Landroid/content/ContentValues;
    sparse-switch p1, :sswitch_data_35c

    #@11
    .line 2384
    const/4 v6, 0x2

    #@12
    .line 2385
    const/4 v7, 0x2

    #@13
    .line 2386
    :try_start_13
    sget-object v15, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_SENT_URI:Landroid/net/Uri;

    #@15
    .line 2387
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDestinationAddress()Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    .line 2388
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1c
    move-result-wide v8

    #@1d
    .line 2389
    const-string v20, "read"

    #@1f
    const/16 v21, 0x1

    #@21
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v21

    #@25
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@28
    .line 2390
    const-string v20, "seen"

    #@2a
    const/16 v21, 0x1

    #@2c
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f
    move-result-object v21

    #@30
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@33
    .line 2395
    :cond_33
    :goto_33
    const-string v20, "type"

    #@35
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38
    move-result-object v21

    #@39
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3c
    .line 2398
    const/16 v20, 0x0

    #@3e
    const-string v21, "parse_email_on_uicc"

    #@40
    invoke-static/range {v20 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@43
    move-result v20

    #@44
    if-eqz v20, :cond_6e

    #@46
    .line 2399
    if-eqz v4, :cond_6e

    #@48
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@4b
    move-result v20

    #@4c
    const/16 v21, 0x4

    #@4e
    move/from16 v0, v20

    #@50
    move/from16 v1, v21

    #@52
    if-ne v0, v1, :cond_6e

    #@54
    const-string v20, "6245"

    #@56
    move-object/from16 v0, v20

    #@58
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v20

    #@5c
    if-eqz v20, :cond_6e

    #@5e
    .line 2400
    move-object/from16 v0, p0

    #@60
    invoke-direct {v0, v4, v5}, Lcom/android/internal/telephony/uicc/SIMRecords;->extractEmailAddressFromMessageBody(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    #@63
    move-result-object v14

    #@64
    .line 2401
    .local v14, parts:[Ljava/lang/String;
    if-eqz v14, :cond_6e

    #@66
    .line 2402
    const/16 v20, 0x0

    #@68
    aget-object v4, v14, v20

    #@6a
    .line 2403
    const/16 v20, 0x1

    #@6c
    aget-object v5, v14, v20

    #@6e
    .line 2407
    .end local v14           #parts:[Ljava/lang/String;
    :cond_6e
    if-nez v4, :cond_282

    #@70
    .line 2408
    const-string v4, "Unknown"

    #@72
    .line 2423
    :cond_72
    :goto_72
    const/16 v20, 0x0

    #@74
    const-string v21, "sms_separate_usimbox"

    #@76
    invoke-static/range {v20 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@79
    move-result v20

    #@7a
    if-eqz v20, :cond_321

    #@7c
    .line 2428
    const-string v20, "dcs"

    #@7e
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDataCodingScheme()I

    #@81
    move-result v21

    #@82
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@85
    move-result-object v21

    #@86
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@89
    .line 2429
    const/16 v20, 0x0

    #@8b
    const-string v21, "kr_address_spec"

    #@8d
    invoke-static/range {v20 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@90
    move-result v20

    #@91
    if-eqz v20, :cond_a6

    #@93
    .line 2430
    const-string v21, "reply_address"

    #@95
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@98
    move-result-object v20

    #@99
    if-nez v20, :cond_2ee

    #@9b
    const-string v20, ""

    #@9d
    :goto_9d
    move-object/from16 v0, v19

    #@9f
    move-object/from16 v1, v21

    #@a1
    move-object/from16 v2, v20

    #@a3
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a6
    .line 2432
    :cond_a6
    const/16 v20, 0x1

    #@a8
    move/from16 v0, p1

    #@aa
    move/from16 v1, v20

    #@ac
    if-eq v0, v1, :cond_b6

    #@ae
    const/16 v20, 0x3

    #@b0
    move/from16 v0, p1

    #@b2
    move/from16 v1, v20

    #@b4
    if-ne v0, v1, :cond_2fe

    #@b6
    .line 2433
    :cond_b6
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@b9
    move-result-object v20

    #@ba
    if-nez v20, :cond_2f8

    #@bc
    if-nez v4, :cond_2f4

    #@be
    const-string v18, ""

    #@c0
    .line 2434
    .local v18, uiAddress:Ljava/lang/String;
    :goto_c0
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c3
    move-result v20

    #@c4
    if-eqz v20, :cond_d1

    #@c6
    .line 2435
    new-instance v18, Ljava/lang/String;

    #@c8
    .end local v18           #uiAddress:Ljava/lang/String;
    const-string v20, "Unknown"

    #@ca
    move-object/from16 v0, v18

    #@cc
    move-object/from16 v1, v20

    #@ce
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@d1
    .line 2437
    .restart local v18       #uiAddress:Ljava/lang/String;
    :cond_d1
    const-string v20, "address"

    #@d3
    move-object/from16 v0, v19

    #@d5
    move-object/from16 v1, v20

    #@d7
    move-object/from16 v2, v18

    #@d9
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@dc
    .line 2438
    const-string v20, "date"

    #@de
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@e1
    move-result-object v21

    #@e2
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@e5
    .line 2443
    .end local v18           #uiAddress:Ljava/lang/String;
    :goto_e5
    const-string v21, "original_address"

    #@e7
    if-nez v4, :cond_31d

    #@e9
    const-string v20, ""

    #@eb
    :goto_eb
    move-object/from16 v0, v19

    #@ed
    move-object/from16 v1, v21

    #@ef
    move-object/from16 v2, v20

    #@f1
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@f4
    .line 2444
    const-string v20, "msg_boxtype"

    #@f6
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f9
    move-result-object v21

    #@fa
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@fd
    .line 2452
    :goto_fd
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    #@100
    move-result-object v5

    #@101
    .line 2453
    if-nez v5, :cond_105

    #@103
    .line 2454
    const-string v5, "empty contents"

    #@105
    .line 2456
    :cond_105
    const-string v20, "body"

    #@107
    move-object/from16 v0, v19

    #@109
    move-object/from16 v1, v20

    #@10b
    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@10e
    .line 2459
    const-string v20, "protocol"

    #@110
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getProtocolIdentifier()I

    #@113
    move-result v21

    #@114
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@117
    move-result-object v21

    #@118
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@11b
    .line 2462
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getPseudoSubject()Ljava/lang/String;

    #@11e
    move-result-object v20

    #@11f
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    #@122
    move-result v20

    #@123
    if-lez v20, :cond_12e

    #@125
    .line 2463
    const-string v20, "subject"

    #@127
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getPseudoSubject()Ljava/lang/String;

    #@12a
    move-result-object v21

    #@12b
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@12e
    .line 2466
    :cond_12e
    const-string v21, "reply_path_present"

    #@130
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->isReplyPathPresent()Z

    #@133
    move-result v20

    #@134
    if-eqz v20, :cond_335

    #@136
    const/16 v20, 0x1

    #@138
    :goto_138
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13b
    move-result-object v20

    #@13c
    move-object/from16 v0, v19

    #@13e
    move-object/from16 v1, v21

    #@140
    move-object/from16 v2, v20

    #@142
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@145
    .line 2469
    const-string v20, "service_center"

    #@147
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getServiceCenterAddress()Ljava/lang/String;

    #@14a
    move-result-object v21

    #@14b
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@14e
    .line 2472
    const-string v20, "lgeMsgType"

    #@150
    const/16 v21, 0x7

    #@152
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@155
    move-result-object v21

    #@156
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@159
    .line 2475
    const-string v20, "index_on_icc"

    #@15b
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getIndexOnIcc()I

    #@15e
    move-result v21

    #@15f
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@162
    move-result-object v21

    #@163
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@166
    .line 2479
    const/16 v20, 0x0

    #@168
    const-string v21, "uicc_csim"

    #@16a
    invoke-static/range {v20 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@16d
    move-result v20

    #@16e
    if-eqz v20, :cond_180

    #@170
    .line 2480
    const-string v20, "sms_format"

    #@172
    const/16 v21, 0x1

    #@174
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@177
    move-result-object v21

    #@178
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@17b
    .line 2481
    const-string v20, "insertSmsDB(), SMS_FORMAT : SMS_FORMAT_USIM: 1"

    #@17d
    invoke-static/range {v20 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@180
    .line 2486
    :cond_180
    invoke-static {}, Landroid/telephony/MSimSmsManager;->getDefault()Landroid/telephony/MSimSmsManager;

    #@183
    move-result-object v20

    #@184
    invoke-virtual/range {v20 .. v20}, Landroid/telephony/MSimSmsManager;->getPreferredSmsSubscription()I

    #@187
    move-result v17

    #@188
    .line 2487
    .local v17, subId:I
    const-string v20, "sub_id"

    #@18a
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18d
    move-result-object v21

    #@18e
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@191
    .line 2489
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@194
    move-result-object v20

    #@195
    invoke-virtual/range {v20 .. v20}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@198
    move-result v20

    #@199
    if-eqz v20, :cond_1c9

    #@19b
    .line 2490
    const-string v3, "sms_imsi_data"

    #@19d
    .line 2491
    .local v3, SMS_IMSI_DATA:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@1a0
    move-result-object v20

    #@1a1
    move-object/from16 v0, v20

    #@1a3
    move/from16 v1, v17

    #@1a5
    invoke-virtual {v0, v1}, Landroid/telephony/MSimTelephonyManager;->getSimSerialNumber(I)Ljava/lang/String;

    #@1a8
    move-result-object v11

    #@1a9
    .line 2492
    .local v11, iccd:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@1ac
    move-result-object v20

    #@1ad
    move-object/from16 v0, v20

    #@1af
    move/from16 v1, v17

    #@1b1
    invoke-virtual {v0, v1}, Landroid/telephony/MSimTelephonyManager;->getSubscriberId(I)Ljava/lang/String;

    #@1b4
    move-result-object v12

    #@1b5
    .line 2493
    .local v12, imsi:Ljava/lang/String;
    const/4 v13, 0x0

    #@1b6
    .line 2494
    .local v13, mIccImsi:Ljava/lang/String;
    if-nez v11, :cond_33d

    #@1b8
    if-nez v12, :cond_33d

    #@1ba
    .line 2495
    const/16 v20, 0x1

    #@1bc
    move/from16 v0, v17

    #@1be
    move/from16 v1, v20

    #@1c0
    if-ne v0, v1, :cond_339

    #@1c2
    const-string v13, "sim2"

    #@1c4
    .line 2499
    :goto_1c4
    move-object/from16 v0, v19

    #@1c6
    invoke-virtual {v0, v3, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1c9
    .catch Ljava/lang/NullPointerException; {:try_start_13 .. :try_end_1c9} :catch_207

    #@1c9
    .line 2504
    .end local v3           #SMS_IMSI_DATA:Ljava/lang/String;
    .end local v11           #iccd:Ljava/lang/String;
    .end local v12           #imsi:Ljava/lang/String;
    .end local v13           #mIccImsi:Ljava/lang/String;
    :cond_1c9
    :try_start_1c9
    move-object/from16 v0, p0

    #@1cb
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1cd
    move-object/from16 v20, v0

    #@1cf
    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1d2
    move-result-object v20

    #@1d3
    move-object/from16 v0, v20

    #@1d5
    move-object/from16 v1, v19

    #@1d7
    invoke-virtual {v0, v15, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@1da
    move-result-object v16

    #@1db
    .line 2505
    if-nez v16, :cond_1e2

    #@1dd
    .line 2506
    const-string v20, "insertSmsDB(), smsInsertUri is null"

    #@1df
    invoke-static/range {v20 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_1e2
    .catch Landroid/database/SQLException; {:try_start_1c9 .. :try_end_1e2} :catch_354
    .catch Ljava/lang/NullPointerException; {:try_start_1c9 .. :try_end_1e2} :catch_207

    #@1e2
    .line 2514
    .end local v17           #subId:I
    :cond_1e2
    :goto_1e2
    return-object v16

    #@1e3
    .line 2353
    :sswitch_1e3
    const/4 v6, 0x1

    #@1e4
    .line 2354
    const/4 v7, 0x1

    #@1e5
    .line 2355
    :try_start_1e5
    sget-object v15, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_INBOX_URI:Landroid/net/Uri;

    #@1e7
    .line 2356
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    #@1ea
    move-result-object v4

    #@1eb
    .line 2357
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getTimestampMillis()J

    #@1ee
    move-result-wide v8

    #@1ef
    .line 2358
    const-string v20, "read"

    #@1f1
    const/16 v21, 0x1

    #@1f3
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f6
    move-result-object v21

    #@1f7
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1fa
    .line 2359
    const-string v20, "seen"

    #@1fc
    const/16 v21, 0x1

    #@1fe
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@201
    move-result-object v21

    #@202
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_205
    .catch Ljava/lang/NullPointerException; {:try_start_1e5 .. :try_end_205} :catch_207

    #@205
    goto/16 :goto_33

    #@207
    .line 2511
    :catch_207
    move-exception v10

    #@208
    .line 2512
    .local v10, e:Ljava/lang/NullPointerException;
    new-instance v20, Ljava/lang/StringBuilder;

    #@20a
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@20d
    const-string v21, "insertSmsDB(), "

    #@20f
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v20

    #@213
    invoke-virtual {v10}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    #@216
    move-result-object v21

    #@217
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v20

    #@21b
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21e
    move-result-object v20

    #@21f
    invoke-static/range {v20 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@222
    goto :goto_1e2

    #@223
    .line 2362
    .end local v10           #e:Ljava/lang/NullPointerException;
    :sswitch_223
    const/4 v6, 0x1

    #@224
    .line 2363
    const/4 v7, 0x1

    #@225
    .line 2364
    :try_start_225
    sget-object v15, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_INBOX_URI:Landroid/net/Uri;

    #@227
    .line 2365
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    #@22a
    move-result-object v4

    #@22b
    .line 2366
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getTimestampMillis()J

    #@22e
    move-result-wide v8

    #@22f
    .line 2367
    const-string v20, "read"

    #@231
    const/16 v21, 0x0

    #@233
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@236
    move-result-object v21

    #@237
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@23a
    .line 2368
    const-string v20, "seen"

    #@23c
    const/16 v21, 0x0

    #@23e
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@241
    move-result-object v21

    #@242
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@245
    goto/16 :goto_33

    #@247
    .line 2371
    :sswitch_247
    const/4 v6, 0x5

    #@248
    .line 2372
    const/4 v7, 0x0

    #@249
    .line 2373
    sget-object v15, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_OUTBOX_URI:Landroid/net/Uri;

    #@24b
    .line 2374
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDestinationAddress()Ljava/lang/String;

    #@24e
    move-result-object v4

    #@24f
    .line 2375
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@252
    move-result-wide v8

    #@253
    .line 2376
    const-string v20, "read"

    #@255
    const/16 v21, 0x1

    #@257
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25a
    move-result-object v21

    #@25b
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@25e
    .line 2377
    const-string v20, "seen"

    #@260
    const/16 v21, 0x1

    #@262
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@265
    move-result-object v21

    #@266
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@269
    .line 2378
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getStatusReportReq()I

    #@26c
    move-result v20

    #@26d
    const/16 v21, 0x1

    #@26f
    move/from16 v0, v20

    #@271
    move/from16 v1, v21

    #@273
    if-ne v0, v1, :cond_33

    #@275
    .line 2379
    const-string v20, "status"

    #@277
    const/16 v21, 0x20

    #@279
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27c
    move-result-object v21

    #@27d
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@280
    goto/16 :goto_33

    #@282
    .line 2409
    :cond_282
    const/16 v20, 0x0

    #@284
    const-string v21, "dcm_support_sim_load_voice_mail_tp_oa_address_ntt_docomo"

    #@286
    invoke-static/range {v20 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@289
    move-result v20

    #@28a
    if-eqz v20, :cond_29c

    #@28c
    if-eqz v4, :cond_29c

    #@28e
    const-string v20, "(?i).*docomo.*"

    #@290
    move-object/from16 v0, v20

    #@292
    invoke-virtual {v4, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@295
    move-result v20

    #@296
    if-eqz v20, :cond_29c

    #@298
    .line 2411
    const-string v4, "NTT DOCOMO"

    #@29a
    goto/16 :goto_72

    #@29c
    .line 2414
    :cond_29c
    const/16 v20, 0x0

    #@29e
    const-string v21, "hide_privacy_log"

    #@2a0
    invoke-static/range {v20 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@2a3
    move-result v20

    #@2a4
    const/16 v21, 0x1

    #@2a6
    move/from16 v0, v20

    #@2a8
    move/from16 v1, v21

    #@2aa
    if-ne v0, v1, :cond_2d4

    #@2ac
    .line 2415
    const-string v20, "1"

    #@2ae
    const-string v21, "persist.service.privacy.enable"

    #@2b0
    invoke-static/range {v21 .. v21}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2b3
    move-result-object v21

    #@2b4
    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b7
    move-result v20

    #@2b8
    if-eqz v20, :cond_72

    #@2ba
    .line 2416
    new-instance v20, Ljava/lang/StringBuilder;

    #@2bc
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@2bf
    const-string v21, "updateMessagefromUSim(), address is not null ="

    #@2c1
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c4
    move-result-object v20

    #@2c5
    move-object/from16 v0, v20

    #@2c7
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ca
    move-result-object v20

    #@2cb
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ce
    move-result-object v20

    #@2cf
    invoke-static/range {v20 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2d2
    goto/16 :goto_72

    #@2d4
    .line 2419
    :cond_2d4
    new-instance v20, Ljava/lang/StringBuilder;

    #@2d6
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@2d9
    const-string v21, "updateMessagefromUSim(), address is not null ="

    #@2db
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2de
    move-result-object v20

    #@2df
    move-object/from16 v0, v20

    #@2e1
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e4
    move-result-object v20

    #@2e5
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e8
    move-result-object v20

    #@2e9
    invoke-static/range {v20 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2ec
    goto/16 :goto_72

    #@2ee
    .line 2430
    :cond_2ee
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@2f1
    move-result-object v20

    #@2f2
    goto/16 :goto_9d

    #@2f4
    :cond_2f4
    move-object/from16 v18, v4

    #@2f6
    .line 2433
    goto/16 :goto_c0

    #@2f8
    :cond_2f8
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@2fb
    move-result-object v18

    #@2fc
    goto/16 :goto_c0

    #@2fe
    .line 2440
    :cond_2fe
    const-string v21, "address"

    #@300
    if-nez v4, :cond_31a

    #@302
    const-string v20, ""

    #@304
    :goto_304
    move-object/from16 v0, v19

    #@306
    move-object/from16 v1, v21

    #@308
    move-object/from16 v2, v20

    #@30a
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@30d
    .line 2441
    const-string v20, "date"

    #@30f
    const/16 v21, -0x1

    #@311
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@314
    move-result-object v21

    #@315
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@318
    goto/16 :goto_e5

    #@31a
    :cond_31a
    move-object/from16 v20, v4

    #@31c
    .line 2440
    goto :goto_304

    #@31d
    :cond_31d
    move-object/from16 v20, v4

    #@31f
    .line 2443
    goto/16 :goto_eb

    #@321
    .line 2447
    :cond_321
    const-string v20, "address"

    #@323
    move-object/from16 v0, v19

    #@325
    move-object/from16 v1, v20

    #@327
    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@32a
    .line 2448
    const-string v20, "date"

    #@32c
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@32f
    move-result-object v21

    #@330
    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@333
    goto/16 :goto_fd

    #@335
    .line 2466
    :cond_335
    const/16 v20, 0x0

    #@337
    goto/16 :goto_138

    #@339
    .line 2495
    .restart local v3       #SMS_IMSI_DATA:Ljava/lang/String;
    .restart local v11       #iccd:Ljava/lang/String;
    .restart local v12       #imsi:Ljava/lang/String;
    .restart local v13       #mIccImsi:Ljava/lang/String;
    .restart local v17       #subId:I
    :cond_339
    const-string v13, "sim1"

    #@33b
    goto/16 :goto_1c4

    #@33d
    .line 2497
    :cond_33d
    new-instance v20, Ljava/lang/StringBuilder;

    #@33f
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@342
    move-object/from16 v0, v20

    #@344
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@347
    move-result-object v20

    #@348
    move-object/from16 v0, v20

    #@34a
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34d
    move-result-object v20

    #@34e
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@351
    move-result-object v13

    #@352
    goto/16 :goto_1c4

    #@354
    .line 2508
    .end local v3           #SMS_IMSI_DATA:Ljava/lang/String;
    .end local v11           #iccd:Ljava/lang/String;
    .end local v12           #imsi:Ljava/lang/String;
    .end local v13           #mIccImsi:Ljava/lang/String;
    :catch_354
    move-exception v10

    #@355
    .line 2509
    .local v10, e:Landroid/database/SQLException;
    const-string v20, "insertSmsDB(), SQL Insert error"

    #@357
    invoke-static/range {v20 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_35a
    .catch Ljava/lang/NullPointerException; {:try_start_225 .. :try_end_35a} :catch_207

    #@35a
    goto/16 :goto_1e2

    #@35c
    .line 2351
    :sswitch_data_35c
    .sparse-switch
        0x1 -> :sswitch_1e3
        0x3 -> :sswitch_223
        0x7 -> :sswitch_247
    .end sparse-switch
.end method

.method private insertSmsDBForConcat(ILcom/android/internal/telephony/gsm/SmsMessage;)V
    .registers 31
    .parameter "statusOnSim"
    .parameter "sms"

    #@0
    .prologue
    .line 2518
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@3
    move-result-object v22

    #@4
    .line 2519
    .local v22, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    move-object/from16 v0, v22

    #@6
    iget-object v2, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@8
    iget v0, v2, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@a
    move/from16 v16, v0

    #@c
    .line 2520
    .local v16, msgCount:I
    move-object/from16 v0, v22

    #@e
    iget-object v2, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@10
    iget v0, v2, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@12
    move/from16 v21, v0

    #@14
    .line 2521
    .local v21, seqNum:I
    move-object/from16 v0, v22

    #@16
    iget-object v2, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@18
    iget v0, v2, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@1a
    move/from16 v18, v0

    #@1c
    .line 2523
    .local v18, refNum:I
    const/16 v17, 0x0

    #@1e
    .line 2524
    .local v17, numOfQueriedItem:I
    const/16 v19, 0x0

    #@20
    .line 2526
    .local v19, saparate_flag:Z
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "reference = "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    move/from16 v0, v18

    #@2d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    const-string v3, " and count = "

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    move/from16 v0, v16

    #@39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    .line 2527
    .local v5, selection:Ljava/lang/String;
    move-object/from16 v0, p0

    #@43
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@45
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@48
    move-result-object v2

    #@49
    sget-object v3, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@4b
    const/4 v4, 0x0

    #@4c
    const/4 v6, 0x0

    #@4d
    const/4 v7, 0x0

    #@4e
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@51
    move-result-object v9

    #@52
    .line 2529
    .local v9, c:Landroid/database/Cursor;
    if-eqz v9, :cond_208

    #@54
    .line 2531
    :try_start_54
    new-instance v27, Landroid/content/ContentValues;

    #@56
    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    #@59
    .line 2532
    .local v27, values:Landroid/content/ContentValues;
    const/16 v25, 0x0

    #@5b
    .line 2533
    .local v25, smsUri:Landroid/net/Uri;
    const/4 v14, 0x0

    #@5c
    .line 2534
    .local v14, iccSring:Ljava/lang/String;
    const/4 v15, 0x0

    #@5d
    .line 2535
    .local v15, indexOnIcc:I
    const-wide/16 v23, 0x0

    #@5f
    .line 2536
    .local v23, smsId:J
    const/16 v20, 0x0

    #@61
    .line 2538
    .local v20, seq:I
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    #@64
    move-result v2

    #@65
    add-int/lit8 v17, v2, 0x1

    #@67
    .line 2539
    new-instance v2, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v3, "insertSmsDBForConcat(), msgCount: "

    #@6e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v2

    #@72
    move/from16 v0, v16

    #@74
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    const-string v3, ", numOfQueriedItem: "

    #@7a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    move/from16 v0, v17

    #@80
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v2

    #@88
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@8b
    .line 2540
    move/from16 v0, v16

    #@8d
    move/from16 v1, v17

    #@8f
    if-ge v0, v1, :cond_98

    #@91
    .line 2541
    const-string v2, "insertSmsDBForConcat(),  Abnormal case!, separate following cocats"

    #@93
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@96
    .line 2542
    const/16 v19, 0x1

    #@98
    .line 2544
    :cond_98
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    #@9b
    move-result v2

    #@9c
    if-eqz v2, :cond_258

    #@9e
    if-nez v19, :cond_258

    #@a0
    .line 2547
    new-instance v8, Ljava/util/ArrayList;

    #@a2
    move/from16 v0, v16

    #@a4
    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@a7
    .line 2548
    .local v8, body_part:Ljava/util/ArrayList;
    const/4 v13, 0x0

    #@a8
    .local v13, i:I
    :goto_a8
    move/from16 v0, v16

    #@aa
    if-ge v13, v0, :cond_b4

    #@ac
    .line 2549
    const-string v2, "/*missing text*/"

    #@ae
    invoke-virtual {v8, v13, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@b1
    .line 2548
    add-int/lit8 v13, v13, 0x1

    #@b3
    goto :goto_a8

    #@b4
    .line 2551
    :cond_b4
    new-instance v14, Ljava/lang/String;

    #@b6
    .end local v14           #iccSring:Ljava/lang/String;
    invoke-direct {v14}, Ljava/lang/String;-><init>()V

    #@b9
    .line 2552
    .restart local v14       #iccSring:Ljava/lang/String;
    const-string v2, "sms_id"

    #@bb
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@be
    move-result v2

    #@bf
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    #@c2
    move-result-wide v23

    #@c3
    .line 2554
    :cond_c3
    const-string v2, "sequence"

    #@c5
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@c8
    move-result v2

    #@c9
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    #@cc
    move-result v20

    #@cd
    .line 2555
    add-int/lit8 v2, v20, -0x1

    #@cf
    const-string v3, "body"

    #@d1
    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@d4
    move-result v3

    #@d5
    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@d8
    move-result-object v3

    #@d9
    invoke-virtual {v8, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@dc
    .line 2556
    const-string v2, "icc_index"

    #@de
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@e1
    move-result v2

    #@e2
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    #@e5
    move-result v15

    #@e6
    .line 2557
    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@e9
    move-result-object v2

    #@ea
    invoke-virtual {v14, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@ed
    move-result-object v14

    #@ee
    .line 2558
    const-string v2, ","

    #@f0
    invoke-virtual {v14, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@f3
    move-result-object v14

    #@f4
    .line 2559
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@f7
    move-result v2

    #@f8
    if-nez v2, :cond_c3

    #@fa
    .line 2561
    add-int/lit8 v2, v21, -0x1

    #@fc
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    #@ff
    move-result-object v3

    #@100
    invoke-virtual {v8, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@103
    .line 2562
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getIndexOnIcc()I

    #@106
    move-result v2

    #@107
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@10a
    move-result-object v2

    #@10b
    invoke-virtual {v14, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@10e
    move-result-object v14

    #@10f
    .line 2563
    const-string v10, ""

    #@111
    .line 2564
    .local v10, concatBody:Ljava/lang/String;
    const/4 v13, 0x0

    #@112
    :goto_112
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@115
    move-result v2

    #@116
    if-ge v13, v2, :cond_130

    #@118
    .line 2565
    new-instance v2, Ljava/lang/StringBuilder;

    #@11a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11d
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v2

    #@121
    invoke-virtual {v8, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@124
    move-result-object v3

    #@125
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v2

    #@129
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12c
    move-result-object v10

    #@12d
    .line 2564
    add-int/lit8 v13, v13, 0x1

    #@12f
    goto :goto_112

    #@130
    .line 2569
    :cond_130
    const-string v2, "sms_id"

    #@132
    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@135
    move-result-object v3

    #@136
    move-object/from16 v0, v27

    #@138
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@13b
    .line 2570
    const-string v2, "body"

    #@13d
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    #@140
    move-result-object v3

    #@141
    move-object/from16 v0, v27

    #@143
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@146
    .line 2571
    const-string v2, "reference"

    #@148
    move-object/from16 v0, v22

    #@14a
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@14c
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@14e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@151
    move-result-object v3

    #@152
    move-object/from16 v0, v27

    #@154
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@157
    .line 2572
    const-string v2, "count"

    #@159
    move-object/from16 v0, v22

    #@15b
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@15d
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@15f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@162
    move-result-object v3

    #@163
    move-object/from16 v0, v27

    #@165
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@168
    .line 2573
    const-string v2, "sequence"

    #@16a
    move-object/from16 v0, v22

    #@16c
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@16e
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@170
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@173
    move-result-object v3

    #@174
    move-object/from16 v0, v27

    #@176
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@179
    .line 2574
    const-string v2, "icc_index"

    #@17b
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getIndexOnIcc()I

    #@17e
    move-result v3

    #@17f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@182
    move-result-object v3

    #@183
    move-object/from16 v0, v27

    #@185
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_188
    .catchall {:try_start_54 .. :try_end_188} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_54 .. :try_end_188} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_54 .. :try_end_188} :catch_235

    #@188
    .line 2577
    :try_start_188
    move-object/from16 v0, p0

    #@18a
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@18c
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@18f
    move-result-object v2

    #@190
    sget-object v3, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@192
    move-object/from16 v0, v27

    #@194
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@197
    move-result-object v25

    #@198
    .line 2578
    if-nez v25, :cond_19f

    #@19a
    .line 2579
    const-string v2, "insertSmsDBForConcat(), smsUri of SMS_CONCAT_URI is null"

    #@19c
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_19f
    .catchall {:try_start_188 .. :try_end_19f} :catchall_2e4
    .catch Landroid/database/SQLException; {:try_start_188 .. :try_end_19f} :catch_209
    .catch Ljava/lang/NullPointerException; {:try_start_188 .. :try_end_19f} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_188 .. :try_end_19f} :catch_235

    #@19f
    .line 2586
    :cond_19f
    :goto_19f
    :try_start_19f
    invoke-virtual/range {v27 .. v27}, Landroid/content/ContentValues;->clear()V

    #@1a2
    .line 2587
    const-string v2, "body"

    #@1a4
    move-object/from16 v0, v27

    #@1a6
    invoke-virtual {v0, v2, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1a9
    .line 2588
    const-string v2, "index_on_icc"

    #@1ab
    move-object/from16 v0, v27

    #@1ad
    invoke-virtual {v0, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1b0
    .line 2590
    const/4 v2, 0x0

    #@1b1
    const-string v3, "uicc_csim"

    #@1b3
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1b6
    move-result v2

    #@1b7
    if-eqz v2, :cond_1c5

    #@1b9
    .line 2591
    const-string v2, "sms_format"

    #@1bb
    const/4 v3, 0x1

    #@1bc
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1bf
    move-result-object v3

    #@1c0
    move-object/from16 v0, v27

    #@1c2
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_1c5
    .catchall {:try_start_19f .. :try_end_1c5} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_19f .. :try_end_1c5} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_19f .. :try_end_1c5} :catch_235

    #@1c5
    .line 2596
    :cond_1c5
    :try_start_1c5
    move-object/from16 v0, p0

    #@1c7
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1c9
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1cc
    move-result-object v2

    #@1cd
    sget-object v3, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_URI:Landroid/net/Uri;

    #@1cf
    new-instance v4, Ljava/lang/StringBuilder;

    #@1d1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d4
    const-string v6, "_id="

    #@1d6
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v4

    #@1da
    move-wide/from16 v0, v23

    #@1dc
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v4

    #@1e0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e3
    move-result-object v4

    #@1e4
    const/4 v6, 0x0

    #@1e5
    move-object/from16 v0, v27

    #@1e7
    invoke-virtual {v2, v3, v0, v4, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@1ea
    move-result v26

    #@1eb
    .line 2597
    .local v26, updateResult:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@1ed
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f0
    const-string v3, "insertSmsDBForConcat(), updateResult = "

    #@1f2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v2

    #@1f6
    move/from16 v0, v26

    #@1f8
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fb
    move-result-object v2

    #@1fc
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ff
    move-result-object v2

    #@200
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_203
    .catchall {:try_start_1c5 .. :try_end_203} :catchall_2e4
    .catch Landroid/database/SQLException; {:try_start_1c5 .. :try_end_203} :catch_22e
    .catch Ljava/lang/NullPointerException; {:try_start_1c5 .. :try_end_203} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1c5 .. :try_end_203} :catch_235

    #@203
    .line 2629
    .end local v8           #body_part:Ljava/util/ArrayList;
    .end local v10           #concatBody:Ljava/lang/String;
    .end local v13           #i:I
    .end local v26           #updateResult:I
    :cond_203
    :goto_203
    if-eqz v9, :cond_208

    #@205
    .line 2630
    .end local v14           #iccSring:Ljava/lang/String;
    .end local v15           #indexOnIcc:I
    .end local v20           #seq:I
    .end local v23           #smsId:J
    .end local v25           #smsUri:Landroid/net/Uri;
    .end local v27           #values:Landroid/content/ContentValues;
    :goto_205
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@208
    .line 2634
    :cond_208
    return-void

    #@209
    .line 2581
    .restart local v8       #body_part:Ljava/util/ArrayList;
    .restart local v10       #concatBody:Ljava/lang/String;
    .restart local v13       #i:I
    .restart local v14       #iccSring:Ljava/lang/String;
    .restart local v15       #indexOnIcc:I
    .restart local v20       #seq:I
    .restart local v23       #smsId:J
    .restart local v25       #smsUri:Landroid/net/Uri;
    .restart local v27       #values:Landroid/content/ContentValues;
    :catch_209
    move-exception v11

    #@20a
    .line 2582
    .local v11, e:Landroid/database/SQLException;
    :try_start_20a
    const-string v2, "insertSmsDBForConcat(), SQL Insert error"

    #@20c
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_20f
    .catchall {:try_start_20a .. :try_end_20f} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_20a .. :try_end_20f} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_20a .. :try_end_20f} :catch_235

    #@20f
    goto :goto_19f

    #@210
    .line 2621
    .end local v8           #body_part:Ljava/util/ArrayList;
    .end local v10           #concatBody:Ljava/lang/String;
    .end local v11           #e:Landroid/database/SQLException;
    .end local v13           #i:I
    .end local v14           #iccSring:Ljava/lang/String;
    .end local v15           #indexOnIcc:I
    .end local v20           #seq:I
    .end local v23           #smsId:J
    .end local v25           #smsUri:Landroid/net/Uri;
    .end local v27           #values:Landroid/content/ContentValues;
    :catch_210
    move-exception v11

    #@211
    .line 2622
    .local v11, e:Ljava/lang/NullPointerException;
    :try_start_211
    new-instance v2, Ljava/lang/StringBuilder;

    #@213
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@216
    const-string v3, "insertSmsDBForConcat(), "

    #@218
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21b
    move-result-object v2

    #@21c
    invoke-virtual {v11}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    #@21f
    move-result-object v3

    #@220
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v2

    #@224
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@227
    move-result-object v2

    #@228
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_22b
    .catchall {:try_start_211 .. :try_end_22b} :catchall_2e4

    #@22b
    .line 2629
    if-eqz v9, :cond_208

    #@22d
    goto :goto_205

    #@22e
    .line 2598
    .end local v11           #e:Ljava/lang/NullPointerException;
    .restart local v8       #body_part:Ljava/util/ArrayList;
    .restart local v10       #concatBody:Ljava/lang/String;
    .restart local v13       #i:I
    .restart local v14       #iccSring:Ljava/lang/String;
    .restart local v15       #indexOnIcc:I
    .restart local v20       #seq:I
    .restart local v23       #smsId:J
    .restart local v25       #smsUri:Landroid/net/Uri;
    .restart local v27       #values:Landroid/content/ContentValues;
    :catch_22e
    move-exception v11

    #@22f
    .line 2599
    .local v11, e:Landroid/database/SQLException;
    :try_start_22f
    const-string v2, "insertSmsDBForConcat(), SQL update error"

    #@231
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_234
    .catchall {:try_start_22f .. :try_end_234} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_22f .. :try_end_234} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_22f .. :try_end_234} :catch_235

    #@234
    goto :goto_203

    #@235
    .line 2624
    .end local v8           #body_part:Ljava/util/ArrayList;
    .end local v10           #concatBody:Ljava/lang/String;
    .end local v11           #e:Landroid/database/SQLException;
    .end local v13           #i:I
    .end local v14           #iccSring:Ljava/lang/String;
    .end local v15           #indexOnIcc:I
    .end local v20           #seq:I
    .end local v23           #smsId:J
    .end local v25           #smsUri:Landroid/net/Uri;
    .end local v27           #values:Landroid/content/ContentValues;
    :catch_235
    move-exception v11

    #@236
    .line 2625
    .local v11, e:Ljava/lang/IndexOutOfBoundsException;
    :try_start_236
    const-string v2, "insertSmsDBForConcat(), IndexOutOfBoundsException"

    #@238
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@23b
    .line 2626
    new-instance v2, Ljava/lang/StringBuilder;

    #@23d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@240
    const-string v3, "insertSmsDBForConcat()"

    #@242
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@245
    move-result-object v2

    #@246
    invoke-virtual {v11}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    #@249
    move-result-object v3

    #@24a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24d
    move-result-object v2

    #@24e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@251
    move-result-object v2

    #@252
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_255
    .catchall {:try_start_236 .. :try_end_255} :catchall_2e4

    #@255
    .line 2629
    if-eqz v9, :cond_208

    #@257
    goto :goto_205

    #@258
    .line 2603
    .end local v11           #e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v14       #iccSring:Ljava/lang/String;
    .restart local v15       #indexOnIcc:I
    .restart local v20       #seq:I
    .restart local v23       #smsId:J
    .restart local v25       #smsUri:Landroid/net/Uri;
    .restart local v27       #values:Landroid/content/ContentValues;
    :cond_258
    :try_start_258
    invoke-direct/range {p0 .. p2}, Lcom/android/internal/telephony/uicc/SIMRecords;->insertSmsDB(ILcom/android/internal/telephony/gsm/SmsMessage;)Landroid/net/Uri;

    #@25b
    move-result-object v12

    #@25c
    .line 2606
    .local v12, firstInsertUri:Landroid/net/Uri;
    const-string v2, "reference"

    #@25e
    move-object/from16 v0, v22

    #@260
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@262
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@264
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@267
    move-result-object v3

    #@268
    move-object/from16 v0, v27

    #@26a
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@26d
    .line 2607
    const-string v2, "count"

    #@26f
    move-object/from16 v0, v22

    #@271
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@273
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@275
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@278
    move-result-object v3

    #@279
    move-object/from16 v0, v27

    #@27b
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@27e
    .line 2608
    const-string v2, "sequence"

    #@280
    move-object/from16 v0, v22

    #@282
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@284
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@286
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@289
    move-result-object v3

    #@28a
    move-object/from16 v0, v27

    #@28c
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@28f
    .line 2609
    const-string v2, "icc_index"

    #@291
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getIndexOnIcc()I

    #@294
    move-result v3

    #@295
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@298
    move-result-object v3

    #@299
    move-object/from16 v0, v27

    #@29b
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@29e
    .line 2610
    const-string v3, "sms_id"

    #@2a0
    invoke-virtual {v12}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@2a3
    move-result-object v2

    #@2a4
    const/4 v4, 0x0

    #@2a5
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2a8
    move-result-object v2

    #@2a9
    check-cast v2, Ljava/lang/String;

    #@2ab
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@2ae
    move-result-wide v6

    #@2af
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2b2
    move-result-object v2

    #@2b3
    move-object/from16 v0, v27

    #@2b5
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@2b8
    .line 2611
    const-string v2, "body"

    #@2ba
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    #@2bd
    move-result-object v3

    #@2be
    move-object/from16 v0, v27

    #@2c0
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2c3
    .catchall {:try_start_258 .. :try_end_2c3} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_258 .. :try_end_2c3} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_258 .. :try_end_2c3} :catch_235

    #@2c3
    .line 2613
    :try_start_2c3
    move-object/from16 v0, p0

    #@2c5
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@2c7
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2ca
    move-result-object v2

    #@2cb
    sget-object v3, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@2cd
    move-object/from16 v0, v27

    #@2cf
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@2d2
    move-result-object v25

    #@2d3
    .line 2614
    if-nez v25, :cond_203

    #@2d5
    .line 2615
    const-string v2, "insertSmsDBForConcat(), smsUri of SMS_CONCAT_URI is null"

    #@2d7
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_2da
    .catchall {:try_start_2c3 .. :try_end_2da} :catchall_2e4
    .catch Landroid/database/SQLException; {:try_start_2c3 .. :try_end_2da} :catch_2dc
    .catch Ljava/lang/NullPointerException; {:try_start_2c3 .. :try_end_2da} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2c3 .. :try_end_2da} :catch_235

    #@2da
    goto/16 :goto_203

    #@2dc
    .line 2617
    :catch_2dc
    move-exception v11

    #@2dd
    .line 2618
    .local v11, e:Landroid/database/SQLException;
    :try_start_2dd
    const-string v2, "insertSmsDBForConcat(), SQL Insert error"

    #@2df
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_2e2
    .catchall {:try_start_2dd .. :try_end_2e2} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_2dd .. :try_end_2e2} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2dd .. :try_end_2e2} :catch_235

    #@2e2
    goto/16 :goto_203

    #@2e4
    .line 2629
    .end local v11           #e:Landroid/database/SQLException;
    .end local v12           #firstInsertUri:Landroid/net/Uri;
    .end local v14           #iccSring:Ljava/lang/String;
    .end local v15           #indexOnIcc:I
    .end local v20           #seq:I
    .end local v23           #smsId:J
    .end local v25           #smsUri:Landroid/net/Uri;
    .end local v27           #values:Landroid/content/ContentValues;
    :catchall_2e4
    move-exception v2

    #@2e5
    if-eqz v9, :cond_2ea

    #@2e7
    .line 2630
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@2ea
    .line 2629
    :cond_2ea
    throw v2
.end method

.method private isCphsMailboxEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 3324
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mCphsInfo:[B

    #@4
    if-nez v2, :cond_7

    #@6
    .line 3325
    :goto_6
    return v1

    #@7
    :cond_7
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mCphsInfo:[B

    #@9
    aget-byte v2, v2, v0

    #@b
    and-int/lit8 v2, v2, 0x30

    #@d
    const/16 v3, 0x30

    #@f
    if-ne v2, v3, :cond_13

    #@11
    :goto_11
    move v1, v0

    #@12
    goto :goto_6

    #@13
    :cond_13
    move v0, v1

    #@14
    goto :goto_11
.end method

.method private isOnMatchingPlmn(Ljava/lang/String;)Z
    .registers 7
    .parameter "plmn"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 3130
    if-nez p1, :cond_5

    #@4
    .line 3143
    :cond_4
    :goto_4
    return v2

    #@5
    .line 3132
    :cond_5
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->getOperatorNumeric()Ljava/lang/String;

    #@8
    move-result-object v4

    #@9
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_11

    #@f
    move v2, v3

    #@10
    .line 3133
    goto :goto_4

    #@11
    .line 3136
    :cond_11
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spdiNetworks:Ljava/util/ArrayList;

    #@13
    if-eqz v4, :cond_4

    #@15
    .line 3137
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spdiNetworks:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v0

    #@1b
    .local v0, i$:Ljava/util/Iterator;
    :cond_1b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v4

    #@1f
    if-eqz v4, :cond_4

    #@21
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Ljava/lang/String;

    #@27
    .line 3138
    .local v1, spdiNet:Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_1b

    #@2d
    move v2, v3

    #@2e
    .line 3139
    goto :goto_4
.end method

.method private lgeFetchSimRecordsBeforeImsi()V
    .registers 3

    #@0
    .prologue
    .line 3413
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string v1, "SKT"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_18

    #@c
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    const-string v1, "KT"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1b

    #@18
    .line 3414
    :cond_18
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->setUsimType()V

    #@1b
    .line 3416
    :cond_1b
    return-void
.end method

.method private parseEfSpdi([B)V
    .registers 10
    .parameter "data"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 3286
    new-instance v3, Lcom/android/internal/telephony/gsm/SimTlv;

    #@3
    array-length v5, p1

    #@4
    invoke-direct {v3, p1, v7, v5}, Lcom/android/internal/telephony/gsm/SimTlv;-><init>([BII)V

    #@7
    .line 3288
    .local v3, tlv:Lcom/android/internal/telephony/gsm/SimTlv;
    const/4 v2, 0x0

    #@8
    .line 3290
    .local v2, plmnEntries:[B
    :goto_8
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/SimTlv;->isValidObject()Z

    #@b
    move-result v5

    #@c
    if-eqz v5, :cond_31

    #@e
    .line 3292
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/SimTlv;->getTag()I

    #@11
    move-result v5

    #@12
    const/16 v6, 0xa3

    #@14
    if-ne v5, v6, :cond_25

    #@16
    .line 3293
    new-instance v4, Lcom/android/internal/telephony/gsm/SimTlv;

    #@18
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/SimTlv;->getData()[B

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/SimTlv;->getData()[B

    #@1f
    move-result-object v6

    #@20
    array-length v6, v6

    #@21
    invoke-direct {v4, v5, v7, v6}, Lcom/android/internal/telephony/gsm/SimTlv;-><init>([BII)V

    #@24
    .end local v3           #tlv:Lcom/android/internal/telephony/gsm/SimTlv;
    .local v4, tlv:Lcom/android/internal/telephony/gsm/SimTlv;
    move-object v3, v4

    #@25
    .line 3296
    .end local v4           #tlv:Lcom/android/internal/telephony/gsm/SimTlv;
    .restart local v3       #tlv:Lcom/android/internal/telephony/gsm/SimTlv;
    :cond_25
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/SimTlv;->getTag()I

    #@28
    move-result v5

    #@29
    const/16 v6, 0x80

    #@2b
    if-ne v5, v6, :cond_34

    #@2d
    .line 3297
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/SimTlv;->getData()[B

    #@30
    move-result-object v2

    #@31
    .line 3302
    :cond_31
    if-nez v2, :cond_38

    #@33
    .line 3318
    :cond_33
    return-void

    #@34
    .line 3290
    :cond_34
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/SimTlv;->nextObject()Z

    #@37
    goto :goto_8

    #@38
    .line 3306
    :cond_38
    new-instance v5, Ljava/util/ArrayList;

    #@3a
    array-length v6, v2

    #@3b
    div-int/lit8 v6, v6, 0x3

    #@3d
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    #@40
    iput-object v5, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spdiNetworks:Ljava/util/ArrayList;

    #@42
    .line 3308
    const/4 v0, 0x0

    #@43
    .local v0, i:I
    :goto_43
    add-int/lit8 v5, v0, 0x2

    #@45
    array-length v6, v2

    #@46
    if-ge v5, v6, :cond_33

    #@48
    .line 3310
    const/4 v5, 0x3

    #@49
    invoke-static {v2, v0, v5}, Lcom/android/internal/telephony/uicc/IccUtils;->bcdToString([BII)Ljava/lang/String;

    #@4c
    move-result-object v1

    #@4d
    .line 3313
    .local v1, plmnCode:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@50
    move-result v5

    #@51
    const/4 v6, 0x5

    #@52
    if-lt v5, v6, :cond_6f

    #@54
    .line 3314
    new-instance v5, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v6, "EF_SPDI network: "

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v5

    #@63
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@6a
    .line 3315
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spdiNetworks:Ljava/util/ArrayList;

    #@6c
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6f
    .line 3308
    :cond_6f
    add-int/lit8 v0, v0, 0x3

    #@71
    goto :goto_43
.end method

.method private setSpnFromConfig(Ljava/lang/String;)V
    .registers 3
    .parameter "carrier"

    #@0
    .prologue
    .line 2932
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mSpnOverride:Lcom/android/internal/telephony/uicc/SpnOverride;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/uicc/SpnOverride;->containsCarrier(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 2933
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mSpnOverride:Lcom/android/internal/telephony/uicc/SpnOverride;

    #@a
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/uicc/SpnOverride;->getSpn(Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@10
    .line 2935
    :cond_10
    return-void
.end method

.method private setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "key"
    .parameter "val"

    #@0
    .prologue
    .line 3407
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_d

    #@a
    .line 3408
    invoke-static {p1, p2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 3410
    :cond_d
    return-void
.end method

.method private setUiccType(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "param_imsi"
    .parameter "param_type"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    .line 3452
    if-eqz p2, :cond_35

    #@3
    .line 3453
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@5
    if-eqz v2, :cond_29

    #@7
    .line 3455
    const-string v2, "GSM"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "[LGE UICC] setting card type explicitly imsi: "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    const-string v4, "  type: "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 3457
    :cond_29
    const-string v2, "gsm.sim.type"

    #@2b
    invoke-static {v2, p2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    .line 3592
    :cond_2e
    :goto_2e
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->getUsimType()I

    #@31
    move-result v2

    #@32
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mUiccType:I

    #@34
    .line 3593
    return-void

    #@35
    .line 3459
    :cond_35
    const/4 v2, 0x0

    #@36
    invoke-virtual {p1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3d
    move-result v0

    #@3e
    .line 3461
    .local v0, mcc:I
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@40
    if-eqz v2, :cond_47

    #@42
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@44
    const/4 v3, -0x1

    #@45
    if-ne v2, v3, :cond_4d

    #@47
    .line 3462
    :cond_47
    invoke-static {v0}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@4a
    move-result v2

    #@4b
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@4d
    .line 3465
    :cond_4d
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@4f
    if-eqz v2, :cond_75

    #@51
    .line 3467
    const-string v2, "GSM"

    #@53
    new-instance v3, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v4, "[LGE UICC] mcc: "

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v3

    #@5e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    const-string v4, " mncLength: "

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    iget v4, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@6a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v3

    #@72
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 3470
    :cond_75
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@77
    add-int/lit8 v2, v2, 0x3

    #@79
    invoke-virtual {p1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7c
    move-result-object v2

    #@7d
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@80
    move-result v1

    #@81
    .line 3472
    .local v1, mnc:I
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@83
    if-eqz v2, :cond_a7

    #@85
    .line 3474
    const-string v2, "GSM"

    #@87
    new-instance v3, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v4, "[LGE UICC] mcc: "

    #@8e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v3

    #@92
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@95
    move-result-object v3

    #@96
    const-string v4, "  mnc: "

    #@98
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v3

    #@9c
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v3

    #@a0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v3

    #@a4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 3477
    :cond_a7
    sparse-switch v0, :sswitch_data_1ba

    #@aa
    .line 3587
    const-string v2, "gsm.sim.type"

    #@ac
    const-string v3, "unknown"

    #@ae
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@b1
    goto/16 :goto_2e

    #@b3
    .line 3479
    :sswitch_b3
    const-string v2, "gsm.sim.type"

    #@b5
    const-string v3, "test"

    #@b7
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@ba
    goto/16 :goto_2e

    #@bc
    .line 3482
    :sswitch_bc
    packed-switch v1, :pswitch_data_1e0

    #@bf
    .line 3487
    const-string v2, "gsm.sim.type"

    #@c1
    const-string v3, "unknown"

    #@c3
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@c6
    goto/16 :goto_2e

    #@c8
    .line 3484
    :pswitch_c8
    const-string v2, "gsm.sim.type"

    #@ca
    const-string v3, "test"

    #@cc
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@cf
    goto/16 :goto_2e

    #@d1
    .line 3492
    :sswitch_d1
    packed-switch v1, :pswitch_data_1e6

    #@d4
    .line 3513
    :pswitch_d4
    const-string v2, "gsm.sim.type"

    #@d6
    const-string v3, "unknown"

    #@d8
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@db
    goto/16 :goto_2e

    #@dd
    .line 3494
    :pswitch_dd
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@e0
    move-result-object v2

    #@e1
    const-string v3, "SPR"

    #@e3
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e6
    move-result v2

    #@e7
    if-eqz v2, :cond_f2

    #@e9
    .line 3495
    const-string v2, "gsm.sim.type"

    #@eb
    const-string v3, "spr"

    #@ed
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@f0
    goto/16 :goto_2e

    #@f2
    .line 3498
    :cond_f2
    const-string v2, "gsm.sim.type"

    #@f4
    const-string v3, "unknown"

    #@f6
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@f9
    goto/16 :goto_2e

    #@fb
    .line 3503
    :pswitch_fb
    const-string v2, "gsm.sim.type"

    #@fd
    const-string v3, "skt"

    #@ff
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@102
    goto/16 :goto_2e

    #@104
    .line 3507
    :pswitch_104
    const-string v2, "gsm.sim.type"

    #@106
    const-string v3, "kt"

    #@108
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@10b
    goto/16 :goto_2e

    #@10d
    .line 3510
    :pswitch_10d
    const-string v2, "gsm.sim.type"

    #@10f
    const-string v3, "lgu"

    #@111
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@114
    goto/16 :goto_2e

    #@116
    .line 3518
    :sswitch_116
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efHplmnwact:Ljava/lang/String;

    #@118
    if-nez v2, :cond_2e

    #@11a
    .line 3519
    const-string v2, "GSM"

    #@11c
    const-string v3, "[LGE UICC][VZW] HPLMNACT is not read yet"

    #@11e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@121
    .line 3521
    const/4 v2, 0x4

    #@122
    if-ne v1, v2, :cond_2e

    #@124
    .line 3522
    const-string v2, "gsm.sim.type"

    #@126
    const-string v3, "vzw3g"

    #@128
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@12b
    .line 3523
    const-string v2, "GSM"

    #@12d
    const-string v3, "vzw3g sim type set to gsm.sim.type property"

    #@12f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@132
    goto/16 :goto_2e

    #@134
    .line 3528
    :sswitch_134
    sparse-switch v1, :sswitch_data_202

    #@137
    goto/16 :goto_2e

    #@139
    .line 3535
    :sswitch_139
    const-string v2, "gsm.sim.type"

    #@13b
    const-string v3, "usc3g"

    #@13d
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@140
    .line 3536
    const-string v2, "GSM"

    #@142
    const-string v3, "[LGE UICC] gsm.sim.type: usc3g"

    #@144
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@147
    goto/16 :goto_2e

    #@149
    .line 3530
    :sswitch_149
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efHplmnwact:Ljava/lang/String;

    #@14b
    if-nez v2, :cond_2e

    #@14d
    .line 3531
    const-string v2, "gsm.sim.type"

    #@14f
    const-string v3, "vzw4g"

    #@151
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@154
    .line 3532
    const-string v2, "GSM"

    #@156
    const-string v3, "vzw4g sim type set to gsm.sim.type property"

    #@158
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15b
    goto/16 :goto_2e

    #@15d
    .line 3539
    :sswitch_15d
    const-string v2, "gsm.sim.type"

    #@15f
    const-string v3, "usc4g"

    #@161
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@164
    .line 3540
    const-string v2, "GSM"

    #@166
    const-string v3, "[LGE UICC] gsm.sim.type: usc4g"

    #@168
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16b
    goto/16 :goto_2e

    #@16d
    .line 3543
    :sswitch_16d
    const-string v2, "gsm.sim.type"

    #@16f
    const-string v3, "mpcs"

    #@171
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@174
    goto/16 :goto_2e

    #@176
    .line 3552
    :sswitch_176
    const/16 v2, 0x6f

    #@178
    if-ne v0, v2, :cond_187

    #@17a
    const/16 v2, 0xdc

    #@17c
    if-ne v1, v2, :cond_187

    #@17e
    .line 3553
    const-string v2, "gsm.sim.type"

    #@180
    const-string v3, "mpcs"

    #@182
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@185
    goto/16 :goto_2e

    #@187
    .line 3555
    :cond_187
    const/16 v2, 0x104

    #@189
    if-ne v0, v2, :cond_2e

    #@18b
    const/16 v2, 0x320

    #@18d
    if-ne v1, v2, :cond_2e

    #@18f
    .line 3556
    const-string v2, "gsm.sim.type"

    #@191
    const-string v3, "mpcs"

    #@193
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@196
    goto/16 :goto_2e

    #@198
    .line 3567
    :sswitch_198
    const/16 v2, 0x78

    #@19a
    if-ne v1, v2, :cond_2e

    #@19c
    .line 3568
    const-string v2, "gsm.sim.type"

    #@19e
    const-string v3, "spr"

    #@1a0
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1a3
    goto/16 :goto_2e

    #@1a5
    .line 3576
    :sswitch_1a5
    packed-switch v1, :pswitch_data_214

    #@1a8
    .line 3581
    const-string v2, "gsm.sim.type"

    #@1aa
    const-string v3, "unknown"

    #@1ac
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1af
    goto/16 :goto_2e

    #@1b1
    .line 3578
    :pswitch_1b1
    const-string v2, "gsm.sim.type"

    #@1b3
    const-string v3, "docomo"

    #@1b5
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1b8
    goto/16 :goto_2e

    #@1ba
    .line 3477
    :sswitch_data_1ba
    .sparse-switch
        0x1 -> :sswitch_b3
        0x2 -> :sswitch_bc
        0x6f -> :sswitch_176
        0xcc -> :sswitch_116
        0x104 -> :sswitch_176
        0x136 -> :sswitch_198
        0x137 -> :sswitch_134
        0x1b8 -> :sswitch_1a5
        0x1c2 -> :sswitch_d1
    .end sparse-switch

    #@1e0
    .line 3482
    :pswitch_data_1e0
    .packed-switch 0xb
        :pswitch_c8
    .end packed-switch

    #@1e6
    .line 3492
    :pswitch_data_1e6
    .packed-switch 0x0
        :pswitch_dd
        :pswitch_d4
        :pswitch_104
        :pswitch_d4
        :pswitch_d4
        :pswitch_fb
        :pswitch_10d
        :pswitch_d4
        :pswitch_104
        :pswitch_d4
        :pswitch_d4
        :pswitch_fb
    .end packed-switch

    #@202
    .line 3528
    :sswitch_data_202
    .sparse-switch
        0xdc -> :sswitch_139
        0x1e0 -> :sswitch_149
        0x244 -> :sswitch_15d
        0x294 -> :sswitch_16d
    .end sparse-switch

    #@214
    .line 3576
    :pswitch_data_214
    .packed-switch 0xa
        :pswitch_1b1
    .end packed-switch
.end method

.method private setUsimType()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/16 v6, 0x64

    #@3
    .line 3419
    const-string v1, "GSM"

    #@5
    const-string v2, " call setUsimType "

    #@7
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 3422
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@f
    move-result-object v1

    #@10
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_USIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@12
    if-ne v1, v2, :cond_34

    #@14
    .line 3424
    :try_start_14
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@16
    const/16 v2, 0x2f24

    #@18
    const/16 v3, 0x64

    #@1a
    new-instance v4, Lcom/android/internal/telephony/uicc/SIMRecords$EfSktImsiPLoaded;

    #@1c
    const/4 v5, 0x0

    #@1d
    invoke-direct {v4, p0, v5}, Lcom/android/internal/telephony/uicc/SIMRecords$EfSktImsiPLoaded;-><init>(Lcom/android/internal/telephony/uicc/SIMRecords;Lcom/android/internal/telephony/uicc/SIMRecords$1;)V

    #@20
    invoke-virtual {p0, v3, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@27
    .line 3426
    const-string v1, "GSM"

    #@29
    const-string v2, " call setUsimType EF_IMSI_P"

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 3427
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@30
    add-int/lit8 v1, v1, 0x1

    #@32
    iput v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I
    :try_end_34
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_34} :catch_6a

    #@34
    .line 3434
    :cond_34
    :goto_34
    :try_start_34
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@36
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@38
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getAid()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    const/16 v3, 0x2c

    #@3e
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@41
    move-result-object v3

    #@42
    invoke-interface {v1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->getIMSIForApp(Ljava/lang/String;Landroid/os/Message;)V

    #@45
    .line 3435
    const-string v1, "GSM"

    #@47
    const-string v2, " call setUsimType getIMSI"

    #@49
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 3436
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@4e
    add-int/lit8 v1, v1, 0x1

    #@50
    iput v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_52} :catch_86

    #@52
    .line 3440
    :goto_52
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@54
    const/16 v2, 0x6f40

    #@56
    const/4 v3, 0x1

    #@57
    new-instance v4, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;

    #@59
    invoke-direct {v4, p0, v7}, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;-><init>(Lcom/android/internal/telephony/uicc/SIMRecords;Lcom/android/internal/telephony/uicc/SIMRecords$1;)V

    #@5c
    invoke-virtual {p0, v6, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V

    #@63
    .line 3442
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@65
    add-int/lit8 v1, v1, 0x1

    #@67
    iput v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@69
    .line 3443
    return-void

    #@6a
    .line 3428
    :catch_6a
    move-exception v0

    #@6b
    .line 3429
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "GSM"

    #@6d
    new-instance v2, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v3, " setUsimType: mFh is "

    #@74
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@7a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v2

    #@82
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    goto :goto_34

    #@86
    .line 3437
    .end local v0           #e:Ljava/lang/Exception;
    :catch_86
    move-exception v0

    #@87
    .line 3438
    .restart local v0       #e:Ljava/lang/Exception;
    const-string v1, "GSM"

    #@89
    new-instance v2, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v3, " setUsimType: mCi is "

    #@90
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v2

    #@94
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@96
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v2

    #@9a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v2

    #@9e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    goto :goto_52
.end method

.method private setVoiceMailByCountry(Ljava/lang/String;)V
    .registers 3
    .parameter "spn"

    #@0
    .prologue
    .line 2939
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mVmConfig:Lcom/android/internal/telephony/uicc/VoiceMailConstants;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/uicc/VoiceMailConstants;->containsCarrier(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_1b

    #@8
    .line 2940
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->isVoiceMailFixed:Z

    #@b
    .line 2941
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mVmConfig:Lcom/android/internal/telephony/uicc/VoiceMailConstants;

    #@d
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/uicc/VoiceMailConstants;->getVoiceMailNumber(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailNum:Ljava/lang/String;

    #@13
    .line 2942
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mVmConfig:Lcom/android/internal/telephony/uicc/VoiceMailConstants;

    #@15
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/uicc/VoiceMailConstants;->getVoiceMailTag(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailTag:Ljava/lang/String;

    #@1b
    .line 2944
    :cond_1b
    return-void
.end method

.method private updateMessagefromUSim(Lcom/android/internal/telephony/gsm/SmsMessage;)V
    .registers 4
    .parameter "sms"

    #@0
    .prologue
    .line 2637
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@3
    move-result-object v0

    #@4
    .line 2638
    .local v0, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    if-eqz v0, :cond_a

    #@6
    iget-object v1, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@8
    if-nez v1, :cond_12

    #@a
    .line 2639
    :cond_a
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getStatusOnIcc()I

    #@d
    move-result v1

    #@e
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/uicc/SIMRecords;->insertSmsDB(ILcom/android/internal/telephony/gsm/SmsMessage;)Landroid/net/Uri;

    #@11
    .line 2643
    :goto_11
    return-void

    #@12
    .line 2641
    :cond_12
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getStatusOnIcc()I

    #@15
    move-result v1

    #@16
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/uicc/SIMRecords;->insertSmsDBForConcat(ILcom/android/internal/telephony/gsm/SmsMessage;)V

    #@19
    goto :goto_11
.end method

.method private validEfCfis([B)Z
    .registers 6
    .parameter "data"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 654
    if-eqz p1, :cond_e

    #@4
    aget-byte v2, p1, v1

    #@6
    if-lt v2, v0, :cond_e

    #@8
    aget-byte v2, p1, v1

    #@a
    const/4 v3, 0x4

    #@b
    if-gt v2, v3, :cond_e

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    move v0, v1

    #@f
    goto :goto_d
.end method


# virtual methods
.method public compICCID(Ljava/lang/String;)Z
    .registers 7
    .parameter "ICCID"

    #@0
    .prologue
    .line 3654
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@3
    move-result-object v3

    #@4
    const-string v4, "SPR"

    #@6
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_e

    #@c
    .line 3655
    const/4 v2, 0x0

    #@d
    .line 3674
    :cond_d
    :goto_d
    return v2

    #@e
    .line 3657
    :cond_e
    const/4 v2, 0x0

    #@f
    .line 3658
    .local v2, retValue:Z
    const-string v3, "ro.chameleon.SIMLock"

    #@11
    const-string v4, "0"

    #@13
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .line 3659
    .local v0, SIMLock:Ljava/lang/String;
    const-string v3, "ro.chameleon.SIMUICCID"

    #@19
    const-string v4, "0"

    #@1b
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    .line 3661
    .local v1, SIMUICCID:Ljava/lang/String;
    const-string v3, "0"

    #@21
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v3

    #@25
    if-nez v3, :cond_d

    #@27
    const-string v3, "0"

    #@29
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v3

    #@2d
    if-nez v3, :cond_d

    #@2f
    .line 3663
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v3

    #@33
    if-eqz v3, :cond_3e

    #@35
    .line 3665
    const-string v3, "GSM"

    #@37
    const-string v4, "iccid is match with ./SIM/UICCID"

    #@39
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 3666
    const/4 v2, 0x0

    #@3d
    goto :goto_d

    #@3e
    .line 3670
    :cond_3e
    const-string v3, "GSM"

    #@40
    const-string v4, "SIM locked. iccid is mismatch with ./SIM/UICCID"

    #@42
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 3671
    const/4 v2, 0x1

    #@46
    goto :goto_d
.end method

.method protected dispatchGsmMessage(Lcom/android/internal/telephony/SmsMessageBase;)I
    .registers 3
    .parameter "message"

    #@0
    .prologue
    .line 2192
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mNewSmsRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@5
    .line 2193
    const/4 v0, 0x0

    #@6
    return v0
.end method

.method public dispose()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 386
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "Disposing SIMRecords "

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@17
    .line 389
    sput-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->iccidToLoad:Z

    #@19
    .line 390
    sput-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->isSimChanged:Z

    #@1b
    .line 392
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@1d
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForIccRefresh(Landroid/os/Handler;)V

    #@20
    .line 393
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@22
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnSmsOnSim(Landroid/os/Handler;)V

    #@25
    .line 394
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@27
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForReady(Landroid/os/Handler;)V

    #@2a
    .line 395
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->resetRecords()V

    #@2d
    .line 396
    invoke-super {p0}, Lcom/android/internal/telephony/uicc/IccRecords;->dispose()V

    #@30
    .line 397
    return-void
.end method

.method protected fetchSimRecords()V
    .registers 7

    #@0
    .prologue
    const/16 v5, 0x6f40

    #@2
    const/4 v0, 0x0

    #@3
    const/4 v4, 0x0

    #@4
    const/4 v3, 0x1

    #@5
    .line 2955
    sput v0, Lcom/android/internal/telephony/uicc/SIMRecords;->FDN_NAME_MAX:I

    #@7
    .line 2956
    sput v0, Lcom/android/internal/telephony/uicc/SIMRecords;->FDN_REC_NUM:I

    #@9
    .line 2959
    iput-boolean v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@b
    .line 2961
    new-instance v0, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v1, "fetchSimRecords "

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@23
    .line 2964
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->lgeFetchSimRecordsBeforeImsi()V

    #@26
    .line 2967
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@28
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2a
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getAid()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    const/4 v2, 0x3

    #@2f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@32
    move-result-object v2

    #@33
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->getIMSIForApp(Ljava/lang/String;Landroid/os/Message;)V

    #@36
    .line 2968
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@38
    add-int/lit8 v0, v0, 0x1

    #@3a
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@3c
    .line 2971
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    const-string v1, "VZW"

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v0

    #@46
    if-eqz v0, :cond_5b

    #@48
    .line 2973
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@4a
    const/16 v1, 0x6f62

    #@4c
    const/16 v2, 0x6e

    #@4e
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@55
    .line 2974
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@57
    add-int/lit8 v0, v0, 0x1

    #@59
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@5b
    .line 2978
    :cond_5b
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@5d
    const/16 v1, 0x2fe2

    #@5f
    const/4 v2, 0x4

    #@60
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@63
    move-result-object v2

    #@64
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@67
    .line 2979
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@69
    add-int/lit8 v0, v0, 0x1

    #@6b
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@6d
    .line 2982
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@70
    move-result-object v0

    #@71
    const-string v1, "KR"

    #@73
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v0

    #@77
    if-nez v0, :cond_8c

    #@79
    .line 2983
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@7b
    const/16 v1, 0x6f3e

    #@7d
    const/16 v2, 0x25

    #@7f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@82
    move-result-object v2

    #@83
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@86
    .line 2984
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@88
    add-int/lit8 v0, v0, 0x1

    #@8a
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@8c
    .line 2991
    :cond_8c
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@8e
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@90
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@93
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/uicc/SIMRecords;->getExtFromEf(I)I

    #@96
    move-result v1

    #@97
    const/16 v2, 0xa

    #@99
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@9c
    move-result-object v2

    #@9d
    invoke-virtual {v0, v5, v1, v3, v2}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->loadFromEF(IIILandroid/os/Message;)V

    #@a0
    .line 2994
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@a2
    add-int/lit8 v0, v0, 0x1

    #@a4
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@a6
    .line 2997
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@a8
    const/16 v1, 0x6fc9

    #@aa
    const/4 v2, 0x5

    #@ab
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@ae
    move-result-object v2

    #@af
    invoke-virtual {v0, v1, v3, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V

    #@b2
    .line 2998
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@b4
    add-int/lit8 v0, v0, 0x1

    #@b6
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@b8
    .line 3000
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@ba
    const/16 v1, 0x6fad

    #@bc
    const/16 v2, 0x9

    #@be
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@c1
    move-result-object v2

    #@c2
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@c5
    .line 3001
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@c7
    add-int/lit8 v0, v0, 0x1

    #@c9
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@cb
    .line 3006
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@ce
    move-result-object v0

    #@cf
    const-string v1, "SKT"

    #@d1
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d4
    move-result v0

    #@d5
    if-nez v0, :cond_108

    #@d7
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@da
    move-result-object v0

    #@db
    const-string v1, "KT"

    #@dd
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e0
    move-result v0

    #@e1
    if-nez v0, :cond_108

    #@e3
    .line 3008
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@e5
    const/16 v1, 0x6fca

    #@e7
    const/4 v2, 0x7

    #@e8
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@eb
    move-result-object v2

    #@ec
    invoke-virtual {v0, v1, v3, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V

    #@ef
    .line 3009
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@f1
    add-int/lit8 v0, v0, 0x1

    #@f3
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@f5
    .line 3016
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@f7
    const/16 v1, 0x6f11

    #@f9
    const/16 v2, 0x8

    #@fb
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@fe
    move-result-object v2

    #@ff
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@102
    .line 3019
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@104
    add-int/lit8 v0, v0, 0x1

    #@106
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@108
    .line 3027
    :cond_108
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@10b
    move-result-object v0

    #@10c
    const-string v1, "SKT"

    #@10e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@111
    move-result v0

    #@112
    if-nez v0, :cond_127

    #@114
    .line 3030
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@116
    const/16 v1, 0x6fcb

    #@118
    const/16 v2, 0x20

    #@11a
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@11d
    move-result-object v2

    #@11e
    invoke-virtual {v0, v1, v3, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V

    #@121
    .line 3031
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@123
    add-int/lit8 v0, v0, 0x1

    #@125
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@127
    .line 3036
    :cond_127
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@129
    const/16 v1, 0x6f13

    #@12b
    const/16 v2, 0x18

    #@12d
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@130
    move-result-object v2

    #@131
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@134
    .line 3037
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@136
    add-int/lit8 v0, v0, 0x1

    #@138
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@13a
    .line 3040
    invoke-direct {p0, v3, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->getSpnFsm(ZLandroid/os/AsyncResult;)V

    #@13d
    .line 3042
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@13f
    const/16 v1, 0x6fcd

    #@141
    const/16 v2, 0xd

    #@143
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@146
    move-result-object v2

    #@147
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@14a
    .line 3043
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@14c
    add-int/lit8 v0, v0, 0x1

    #@14e
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@150
    .line 3045
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@152
    const/16 v1, 0x6fc5

    #@154
    const/16 v2, 0xf

    #@156
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@159
    move-result-object v2

    #@15a
    invoke-virtual {v0, v1, v3, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V

    #@15d
    .line 3046
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@15f
    add-int/lit8 v0, v0, 0x1

    #@161
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@163
    .line 3048
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@165
    const/16 v1, 0x6f38

    #@167
    const/16 v2, 0x11

    #@169
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@16c
    move-result-object v2

    #@16d
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@170
    .line 3049
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@172
    add-int/lit8 v0, v0, 0x1

    #@174
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@176
    .line 3051
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@178
    const/16 v1, 0x6f16

    #@17a
    const/16 v2, 0x1a

    #@17c
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@17f
    move-result-object v2

    #@180
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@183
    .line 3052
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@185
    add-int/lit8 v0, v0, 0x1

    #@187
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@189
    .line 3054
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@18b
    const/16 v1, 0x6f15

    #@18d
    const/16 v2, 0x21

    #@18f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@192
    move-result-object v2

    #@193
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@196
    .line 3055
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@198
    add-int/lit8 v0, v0, 0x1

    #@19a
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@19c
    .line 3065
    const-string v0, "seperate_processing_sms_uicc"

    #@19e
    invoke-static {v4, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1a1
    move-result v0

    #@1a2
    if-eqz v0, :cond_1c0

    #@1a4
    .line 3066
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1a6
    const/16 v1, 0x6f3c

    #@1a8
    const/16 v2, 0x12

    #@1aa
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@1ad
    move-result-object v2

    #@1ae
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V

    #@1b1
    .line 3068
    const-string v0, "copy_submit_to_uicc"

    #@1b3
    invoke-static {v4, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1b6
    move-result v0

    #@1b7
    if-eqz v0, :cond_1c0

    #@1b9
    .line 3069
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1bc
    move-result-wide v0

    #@1bd
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->setTimeforSMSonUSIM(J)V

    #@1c0
    .line 3075
    :cond_1c0
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1c3
    move-result-object v0

    #@1c4
    const-string v1, "LGU"

    #@1c6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c9
    move-result v0

    #@1ca
    if-eqz v0, :cond_1df

    #@1cc
    .line 3076
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1ce
    const/16 v1, 0x2f50

    #@1d0
    const/16 v2, 0xcc

    #@1d2
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@1d5
    move-result-object v2

    #@1d6
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@1d9
    .line 3077
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@1db
    add-int/lit8 v0, v0, 0x1

    #@1dd
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@1df
    .line 3093
    :cond_1df
    new-instance v0, Ljava/lang/StringBuilder;

    #@1e1
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1e4
    const-string v1, "fetchSimRecords "

    #@1e6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v0

    #@1ea
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@1ec
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ef
    move-result-object v0

    #@1f0
    const-string v1, " requested: "

    #@1f2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v0

    #@1f6
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@1f8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1fb
    move-result-object v0

    #@1fc
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ff
    move-result-object v0

    #@200
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@203
    .line 3094
    return-void
.end method

.method protected finalize()V
    .registers 2

    #@0
    .prologue
    .line 400
    const-string v0, "finalized"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@5
    .line 401
    return-void
.end method

.method public getCountryCode()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 821
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 822
    const/4 v0, 0x0

    #@5
    .line 824
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@8
    const/4 v1, 0x0

    #@9
    const/4 v2, 0x3

    #@a
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    goto :goto_5
.end method

.method public getDisplayRule(Ljava/lang/String;)I
    .registers 5
    .parameter "plmn"

    #@0
    .prologue
    .line 3107
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_d

    #@8
    iget v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnDisplayCondition:I

    #@a
    const/4 v2, -0x1

    #@b
    if-ne v1, v2, :cond_f

    #@d
    .line 3109
    :cond_d
    const/4 v0, 0x2

    #@e
    .line 3123
    .local v0, rule:I
    :cond_e
    :goto_e
    return v0

    #@f
    .line 3110
    .end local v0           #rule:I
    :cond_f
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/SIMRecords;->isOnMatchingPlmn(Ljava/lang/String;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_20

    #@15
    .line 3111
    const/4 v0, 0x1

    #@16
    .line 3112
    .restart local v0       #rule:I
    iget v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnDisplayCondition:I

    #@18
    and-int/lit8 v1, v1, 0x1

    #@1a
    const/4 v2, 0x1

    #@1b
    if-ne v1, v2, :cond_e

    #@1d
    .line 3114
    or-int/lit8 v0, v0, 0x2

    #@1f
    goto :goto_e

    #@20
    .line 3117
    .end local v0           #rule:I
    :cond_20
    const/4 v0, 0x2

    #@21
    .line 3118
    .restart local v0       #rule:I
    iget v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnDisplayCondition:I

    #@23
    and-int/lit8 v1, v1, 0x2

    #@25
    if-nez v1, :cond_e

    #@27
    .line 3120
    or-int/lit8 v0, v0, 0x1

    #@29
    goto :goto_e
.end method

.method public getIMSI()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 451
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getIMSIP()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 456
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->imsip:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMsisdnAlphaTag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 516
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdnTag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMsisdnNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 460
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getOperatorNumeric()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 797
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@4
    if-nez v1, :cond_c

    #@6
    .line 798
    const-string v1, "getOperatorNumeric: IMSI == null"

    #@8
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@b
    .line 814
    :goto_b
    return-object v0

    #@c
    .line 801
    :cond_c
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@e
    const/4 v2, -0x1

    #@f
    if-eq v1, v2, :cond_15

    #@11
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@13
    if-nez v1, :cond_1b

    #@15
    .line 802
    :cond_15
    const-string v1, "getSIMOperatorNumeric: bad mncLength"

    #@17
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1a
    goto :goto_b

    #@1b
    .line 807
    :cond_1b
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    const-string v1, "LGU"

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_39

    #@27
    .line 808
    sget-object v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mMccMnc:Ljava/lang/String;

    #@29
    if-eqz v0, :cond_2e

    #@2b
    sget-object v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mMccMnc:Ljava/lang/String;

    #@2d
    goto :goto_b

    #@2e
    :cond_2e
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@30
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@32
    add-int/lit8 v1, v1, 0x3

    #@34
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    goto :goto_b

    #@39
    .line 814
    :cond_39
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@3b
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@3d
    add-int/lit8 v1, v1, 0x3

    #@3f
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    goto :goto_b
.end method

.method public getUsimServiceTable()Lcom/android/internal/telephony/uicc/UsimServiceTable;
    .registers 2

    #@0
    .prologue
    .line 465
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mUsimServiceTable:Lcom/android/internal/telephony/uicc/UsimServiceTable;

    #@2
    return-object v0
.end method

.method public getVoiceCallForwardingFlag()Z
    .registers 2

    #@0
    .prologue
    .line 701
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->callForwardingEnabled:Z

    #@2
    return v0
.end method

.method public getVoiceMailAlphaTag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 592
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailTag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getVoiceMailNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 520
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailNum:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getVoiceMessageCount()I
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 658
    const/4 v2, 0x0

    #@3
    .line 659
    .local v2, voiceMailWaiting:Z
    const/4 v0, 0x0

    #@4
    .line 660
    .local v0, countVoiceMessages:I
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@6
    if-eqz v5, :cond_37

    #@8
    .line 664
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@a
    aget-byte v5, v5, v4

    #@c
    and-int/lit8 v5, v5, 0x1

    #@e
    if-eqz v5, :cond_35

    #@10
    move v2, v3

    #@11
    .line 665
    :goto_11
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@13
    aget-byte v3, v4, v3

    #@15
    and-int/lit16 v0, v3, 0xff

    #@17
    .line 667
    if-eqz v2, :cond_1c

    #@19
    if-nez v0, :cond_1c

    #@1b
    .line 669
    const/4 v0, -0x1

    #@1c
    .line 671
    :cond_1c
    const-string v3, "GSM"

    #@1e
    new-instance v4, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v5, " VoiceMessageCount from SIM MWIS = "

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 685
    :cond_34
    :goto_34
    return v0

    #@35
    :cond_35
    move v2, v4

    #@36
    .line 664
    goto :goto_11

    #@37
    .line 672
    :cond_37
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efCPHS_MWI:[B

    #@39
    if-eqz v3, :cond_34

    #@3b
    .line 674
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efCPHS_MWI:[B

    #@3d
    aget-byte v3, v3, v4

    #@3f
    and-int/lit8 v1, v3, 0xf

    #@41
    .line 677
    .local v1, indicator:I
    const/16 v3, 0xa

    #@43
    if-ne v1, v3, :cond_5f

    #@45
    .line 679
    const/4 v0, -0x1

    #@46
    .line 683
    :cond_46
    :goto_46
    const-string v3, "GSM"

    #@48
    new-instance v4, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v5, " VoiceMessageCount from SIM CPHS = "

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v4

    #@5b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    goto :goto_34

    #@5f
    .line 680
    :cond_5f
    const/4 v3, 0x5

    #@60
    if-ne v1, v3, :cond_46

    #@62
    .line 681
    const/4 v0, 0x0

    #@63
    goto :goto_46
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 39
    .parameter "msg"

    #@0
    .prologue
    .line 834
    const/16 v20, 0x0

    #@2
    .line 836
    .local v20, isRecordLoadResponse:Z
    move-object/from16 v0, p0

    #@4
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@6
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_41

    #@c
    .line 837
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Received message "

    #@13
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    move-object/from16 v0, p1

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v4, "["

    #@1f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    move-object/from16 v0, p1

    #@25
    iget v4, v0, Landroid/os/Message;->what:I

    #@27
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v4, "] "

    #@2d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    const-string v4, " while being destroyed. Ignoring."

    #@33
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    move-object/from16 v0, p0

    #@3d
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@40
    .line 2022
    :cond_40
    :goto_40
    return-void

    #@41
    .line 842
    :cond_41
    :try_start_41
    move-object/from16 v0, p1

    #@43
    iget v2, v0, Landroid/os/Message;->what:I

    #@45
    sparse-switch v2, :sswitch_data_1ee4

    #@48
    .line 2011
    invoke-super/range {p0 .. p1}, Lcom/android/internal/telephony/uicc/IccRecords;->handleMessage(Landroid/os/Message;)V
    :try_end_4b
    .catchall {:try_start_41 .. :try_end_4b} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_41 .. :try_end_4b} :catch_55

    #@4b
    .line 2018
    :cond_4b
    :goto_4b
    if-eqz v20, :cond_40

    #@4d
    .line 2019
    :goto_4d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->onRecordLoaded()V

    #@50
    goto :goto_40

    #@51
    .line 844
    :sswitch_51
    :try_start_51
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->onReady()V
    :try_end_54
    .catchall {:try_start_51 .. :try_end_54} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_51 .. :try_end_54} :catch_55

    #@54
    goto :goto_4b

    #@55
    .line 2013
    :catch_55
    move-exception v15

    #@56
    .line 2015
    .local v15, exc:Ljava/lang/RuntimeException;
    :try_start_56
    const-string v2, "Exception parsing SIM record"

    #@58
    move-object/from16 v0, p0

    #@5a
    invoke-virtual {v0, v2, v15}, Lcom/android/internal/telephony/uicc/SIMRecords;->logw(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5d
    .catchall {:try_start_56 .. :try_end_5d} :catchall_87

    #@5d
    .line 2018
    if-eqz v20, :cond_40

    #@5f
    goto :goto_4d

    #@60
    .line 849
    .end local v15           #exc:Ljava/lang/RuntimeException;
    :sswitch_60
    const/16 v20, 0x1

    #@62
    .line 851
    :try_start_62
    move-object/from16 v0, p1

    #@64
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@66
    check-cast v9, Landroid/os/AsyncResult;

    #@68
    .line 853
    .local v9, ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@6a
    if-eqz v2, :cond_8e

    #@6c
    .line 854
    new-instance v2, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v4, "Exception querying IMSI, Exception:"

    #@73
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v2

    #@77
    iget-object v4, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@79
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v2

    #@7d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v2

    #@81
    move-object/from16 v0, p0

    #@83
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V
    :try_end_86
    .catchall {:try_start_62 .. :try_end_86} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_62 .. :try_end_86} :catch_55

    #@86
    goto :goto_4b

    #@87
    .line 2018
    .end local v9           #ar:Landroid/os/AsyncResult;
    :catchall_87
    move-exception v2

    #@88
    if-eqz v20, :cond_8d

    #@8a
    .line 2019
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->onRecordLoaded()V

    #@8d
    .line 2018
    :cond_8d
    throw v2

    #@8e
    .line 858
    .restart local v9       #ar:Landroid/os/AsyncResult;
    :cond_8e
    :try_start_8e
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@90
    check-cast v2, Ljava/lang/String;

    #@92
    move-object/from16 v0, p0

    #@94
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@96
    .line 861
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@99
    move-result-object v2

    #@9a
    const-string v4, "KDDI"

    #@9c
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9f
    move-result v2

    #@a0
    if-eqz v2, :cond_b5

    #@a2
    move-object/from16 v0, p0

    #@a4
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;
    :try_end_a6
    .catchall {:try_start_8e .. :try_end_a6} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_8e .. :try_end_a6} :catch_55

    #@a6
    if-eqz v2, :cond_b5

    #@a8
    .line 863
    :try_start_a8
    move-object/from16 v0, p0

    #@aa
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@ac
    const/4 v4, 0x0

    #@ad
    const/4 v5, 0x5

    #@ae
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b1
    move-result-object v2

    #@b2
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_b5
    .catchall {:try_start_a8 .. :try_end_b5} :catchall_87
    .catch Ljava/lang/NumberFormatException; {:try_start_a8 .. :try_end_b5} :catch_255
    .catch Ljava/lang/RuntimeException; {:try_start_a8 .. :try_end_b5} :catch_55

    #@b5
    .line 871
    :cond_b5
    :goto_b5
    :try_start_b5
    const-string v2, "1"

    #@b7
    const-string v4, "persist.service.vsim.enable"

    #@b9
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@bc
    move-result-object v4

    #@bd
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c0
    move-result v2

    #@c1
    if-eqz v2, :cond_f6

    #@c3
    .line 873
    const/16 v35, 0x0

    #@c5
    .line 874
    .local v35, vsim_imsi:Ljava/lang/String;
    const/16 v36, 0x0

    #@c7
    .line 876
    .local v36, vsim_mcc_mnc:Ljava/lang/String;
    const-string v2, "persist.service.vsim.imsi"

    #@c9
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@cc
    move-result-object v35

    #@cd
    .line 877
    const-string v2, "GSM"

    #@cf
    new-instance v4, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v5, "[LGE_UICC] VSIM IMSI : "

    #@d6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v4

    #@da
    move-object/from16 v0, v35

    #@dc
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v4

    #@e0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v4

    #@e4
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e7
    .line 879
    if-eqz v35, :cond_264

    #@e9
    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->length()I

    #@ec
    move-result v2

    #@ed
    const/4 v4, 0x4

    #@ee
    if-le v2, v4, :cond_264

    #@f0
    .line 880
    move-object/from16 v0, v35

    #@f2
    move-object/from16 v1, p0

    #@f4
    iput-object v0, v1, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@f6
    .line 898
    .end local v35           #vsim_imsi:Ljava/lang/String;
    .end local v36           #vsim_mcc_mnc:Ljava/lang/String;
    :cond_f6
    :goto_f6
    move-object/from16 v0, p0

    #@f8
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@fa
    if-eqz v2, :cond_138

    #@fc
    move-object/from16 v0, p0

    #@fe
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@100
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@103
    move-result v2

    #@104
    const/4 v4, 0x6

    #@105
    if-lt v2, v4, :cond_113

    #@107
    move-object/from16 v0, p0

    #@109
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@10b
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@10e
    move-result v2

    #@10f
    const/16 v4, 0xf

    #@111
    if-le v2, v4, :cond_138

    #@113
    .line 899
    :cond_113
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@115
    if-eqz v2, :cond_133

    #@117
    new-instance v2, Ljava/lang/StringBuilder;

    #@119
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11c
    const-string v4, "invalid IMSI "

    #@11e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v2

    #@122
    move-object/from16 v0, p0

    #@124
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@126
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v2

    #@12a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12d
    move-result-object v2

    #@12e
    move-object/from16 v0, p0

    #@130
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@133
    .line 900
    :cond_133
    const/4 v2, 0x0

    #@134
    move-object/from16 v0, p0

    #@136
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@138
    .line 903
    :cond_138
    const-string v2, "IMSI: xxxxxxx"

    #@13a
    move-object/from16 v0, p0

    #@13c
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@13f
    .line 905
    move-object/from16 v0, p0

    #@141
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@143
    if-eqz v2, :cond_14c

    #@145
    move-object/from16 v0, p0

    #@147
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@149
    const/4 v4, 0x2

    #@14a
    if-ne v2, v4, :cond_181

    #@14c
    :cond_14c
    move-object/from16 v0, p0

    #@14e
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@150
    if-eqz v2, :cond_181

    #@152
    move-object/from16 v0, p0

    #@154
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@156
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@159
    move-result v2

    #@15a
    const/4 v4, 0x6

    #@15b
    if-lt v2, v4, :cond_181

    #@15d
    .line 907
    move-object/from16 v0, p0

    #@15f
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@161
    const/4 v4, 0x0

    #@162
    const/4 v5, 0x6

    #@163
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@166
    move-result-object v26

    #@167
    .line 908
    .local v26, mccmncCode:Ljava/lang/String;
    sget-object v10, Lcom/android/internal/telephony/uicc/SIMRecords;->MCCMNC_CODES_HAVING_3DIGITS_MNC:[Ljava/lang/String;

    #@169
    .local v10, arr$:[Ljava/lang/String;
    array-length v0, v10

    #@16a
    move/from16 v23, v0

    #@16c
    .local v23, len$:I
    const/16 v17, 0x0

    #@16e
    .local v17, i$:I
    :goto_16e
    move/from16 v0, v17

    #@170
    move/from16 v1, v23

    #@172
    if-ge v0, v1, :cond_181

    #@174
    aget-object v25, v10, v17

    #@176
    .line 909
    .local v25, mccmnc:Ljava/lang/String;
    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@179
    move-result v2

    #@17a
    if-eqz v2, :cond_2a5

    #@17c
    .line 910
    const/4 v2, 0x3

    #@17d
    move-object/from16 v0, p0

    #@17f
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@181
    .line 915
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_181
    move-object/from16 v0, p0

    #@183
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I
    :try_end_185
    .catchall {:try_start_b5 .. :try_end_185} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_b5 .. :try_end_185} :catch_55

    #@185
    if-nez v2, :cond_19d

    #@187
    .line 919
    :try_start_187
    move-object/from16 v0, p0

    #@189
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@18b
    const/4 v4, 0x0

    #@18c
    const/4 v5, 0x3

    #@18d
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@190
    move-result-object v2

    #@191
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@194
    move-result v24

    #@195
    .line 920
    .local v24, mcc:I
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@198
    move-result v2

    #@199
    move-object/from16 v0, p0

    #@19b
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I
    :try_end_19d
    .catchall {:try_start_187 .. :try_end_19d} :catchall_87
    .catch Ljava/lang/NumberFormatException; {:try_start_187 .. :try_end_19d} :catch_2a9
    .catch Ljava/lang/RuntimeException; {:try_start_187 .. :try_end_19d} :catch_55

    #@19d
    .line 927
    .end local v24           #mcc:I
    :cond_19d
    :goto_19d
    :try_start_19d
    move-object/from16 v0, p0

    #@19f
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1a1
    if-eqz v2, :cond_230

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1a7
    const/4 v4, -0x1

    #@1a8
    if-eq v2, v4, :cond_230

    #@1aa
    .line 930
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1ad
    move-result-object v2

    #@1ae
    const-string v4, "VZW"

    #@1b0
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b3
    move-result v2

    #@1b4
    if-eqz v2, :cond_1fb

    #@1b6
    move-object/from16 v0, p0

    #@1b8
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1ba
    if-eqz v2, :cond_1fb

    #@1bc
    .line 931
    move-object/from16 v0, p0

    #@1be
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1c0
    const/4 v4, 0x0

    #@1c1
    const/4 v5, 0x3

    #@1c2
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c5
    move-result-object v2

    #@1c6
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1c9
    move-result v24

    #@1ca
    .line 932
    .restart local v24       #mcc:I
    const/16 v2, 0xcc

    #@1cc
    move/from16 v0, v24

    #@1ce
    if-ne v0, v2, :cond_1fb

    #@1d0
    move-object/from16 v0, p0

    #@1d2
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1d4
    const/4 v4, 0x3

    #@1d5
    if-ne v2, v4, :cond_1fb

    #@1d7
    .line 934
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@1da
    move-result v2

    #@1db
    move-object/from16 v0, p0

    #@1dd
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1df
    .line 935
    const-string v2, "GSM"

    #@1e1
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e3
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e6
    const-string v5, "[LGE UICC] SIMRecords : mncLength = "

    #@1e8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v4

    #@1ec
    move-object/from16 v0, p0

    #@1ee
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1f0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v4

    #@1f4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f7
    move-result-object v4

    #@1f8
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1fb
    .line 941
    .end local v24           #mcc:I
    :cond_1fb
    const-string v2, "US"

    #@1fd
    const-string v4, "SPR"

    #@1ff
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@202
    move-result v2

    #@203
    if-eqz v2, :cond_2d0

    #@205
    .line 942
    move-object/from16 v0, p0

    #@207
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@209
    const/4 v4, 0x0

    #@20a
    const/4 v5, 0x6

    #@20b
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@20e
    move-result-object v27

    #@20f
    .line 943
    .local v27, numeric:Ljava/lang/String;
    const-string v2, "310120"

    #@211
    move-object/from16 v0, v27

    #@213
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@216
    move-result v2

    #@217
    if-nez v2, :cond_223

    #@219
    const-string v2, "316010"

    #@21b
    move-object/from16 v0, v27

    #@21d
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@220
    move-result v2

    #@221
    if-eqz v2, :cond_2b8

    #@223
    .line 944
    :cond_223
    move-object/from16 v0, p0

    #@225
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@227
    const-string v4, "ro.cdma.home.operator.numeric"

    #@229
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@22c
    move-result-object v4

    #@22d
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@230
    .line 955
    .end local v27           #numeric:Ljava/lang/String;
    :cond_230
    :goto_230
    move-object/from16 v0, p0

    #@232
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@234
    if-eqz v2, :cond_24c

    #@236
    .line 956
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@239
    move-result-object v2

    #@23a
    const-string v4, "LGU"

    #@23c
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23f
    move-result v2

    #@240
    if-nez v2, :cond_2e8

    #@242
    .line 957
    move-object/from16 v0, p0

    #@244
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@246
    const/4 v4, 0x0

    #@247
    move-object/from16 v0, p0

    #@249
    invoke-direct {v0, v2, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->setUiccType(Ljava/lang/String;Ljava/lang/String;)V

    #@24c
    .line 1002
    :cond_24c
    move-object/from16 v0, p0

    #@24e
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsiReadyRegistrants:Landroid/os/RegistrantList;

    #@250
    invoke-virtual {v2}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@253
    goto/16 :goto_4b

    #@255
    .line 864
    :catch_255
    move-exception v13

    #@256
    .line 865
    .local v13, e:Ljava/lang/NumberFormatException;
    const/4 v2, 0x0

    #@257
    move-object/from16 v0, p0

    #@259
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@25b
    .line 866
    const-string v2, "GSM"

    #@25d
    const-string v4, "SIMRecords: Corrupt IMSI!"

    #@25f
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@262
    goto/16 :goto_b5

    #@264
    .line 884
    .end local v13           #e:Ljava/lang/NumberFormatException;
    .restart local v35       #vsim_imsi:Ljava/lang/String;
    .restart local v36       #vsim_mcc_mnc:Ljava/lang/String;
    :cond_264
    const-string v2, "persist.service.vsim.mcc-mnc"

    #@266
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@269
    move-result-object v36

    #@26a
    .line 886
    const-string v2, "GSM"

    #@26c
    new-instance v4, Ljava/lang/StringBuilder;

    #@26e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@271
    const-string v5, "[LGE_UICC] VSIM MCC-MNC : "

    #@273
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@276
    move-result-object v4

    #@277
    move-object/from16 v0, v36

    #@279
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27c
    move-result-object v4

    #@27d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@280
    move-result-object v4

    #@281
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@284
    .line 888
    new-instance v31, Ljava/lang/StringBuilder;

    #@286
    move-object/from16 v0, p0

    #@288
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@28a
    move-object/from16 v0, v31

    #@28c
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@28f
    .line 889
    .local v31, sb:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    #@290
    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->length()I

    #@293
    move-result v4

    #@294
    move-object/from16 v0, v31

    #@296
    move-object/from16 v1, v36

    #@298
    invoke-virtual {v0, v2, v4, v1}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    #@29b
    .line 890
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29e
    move-result-object v2

    #@29f
    move-object/from16 v0, p0

    #@2a1
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@2a3
    goto/16 :goto_f6

    #@2a5
    .line 908
    .end local v31           #sb:Ljava/lang/StringBuilder;
    .end local v35           #vsim_imsi:Ljava/lang/String;
    .end local v36           #vsim_mcc_mnc:Ljava/lang/String;
    .restart local v10       #arr$:[Ljava/lang/String;
    .restart local v17       #i$:I
    .restart local v23       #len$:I
    .restart local v25       #mccmnc:Ljava/lang/String;
    .restart local v26       #mccmncCode:Ljava/lang/String;
    :cond_2a5
    add-int/lit8 v17, v17, 0x1

    #@2a7
    goto/16 :goto_16e

    #@2a9
    .line 921
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :catch_2a9
    move-exception v13

    #@2aa
    .line 922
    .restart local v13       #e:Ljava/lang/NumberFormatException;
    const/4 v2, 0x0

    #@2ab
    move-object/from16 v0, p0

    #@2ad
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@2af
    .line 923
    const-string v2, "Corrupt IMSI!"

    #@2b1
    move-object/from16 v0, p0

    #@2b3
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@2b6
    goto/16 :goto_19d

    #@2b8
    .line 946
    .end local v13           #e:Ljava/lang/NumberFormatException;
    .restart local v27       #numeric:Ljava/lang/String;
    :cond_2b8
    move-object/from16 v0, p0

    #@2ba
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@2bc
    move-object/from16 v0, p0

    #@2be
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@2c0
    const/4 v5, 0x0

    #@2c1
    move-object/from16 v0, p0

    #@2c3
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@2c5
    add-int/lit8 v6, v6, 0x3

    #@2c7
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2ca
    move-result-object v4

    #@2cb
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@2ce
    goto/16 :goto_230

    #@2d0
    .line 950
    .end local v27           #numeric:Ljava/lang/String;
    :cond_2d0
    move-object/from16 v0, p0

    #@2d2
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@2d4
    move-object/from16 v0, p0

    #@2d6
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@2d8
    const/4 v5, 0x0

    #@2d9
    move-object/from16 v0, p0

    #@2db
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@2dd
    add-int/lit8 v6, v6, 0x3

    #@2df
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2e2
    move-result-object v4

    #@2e3
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@2e6
    goto/16 :goto_230

    #@2e8
    .line 959
    :cond_2e8
    move-object/from16 v0, p0

    #@2ea
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@2ec
    const/4 v4, 0x0

    #@2ed
    const/4 v5, 0x5

    #@2ee
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2f1
    move-result-object v2

    #@2f2
    const-string v4, "45006"

    #@2f4
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f7
    move-result v2

    #@2f8
    if-eqz v2, :cond_304

    #@2fa
    .line 960
    const/4 v2, 0x1

    #@2fb
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mIsSponIMSI:I

    #@2fd
    .line 961
    const-string v2, "GSM"

    #@2ff
    const-string v4, "LGE_USIM_IS_HOME_IMSI"

    #@301
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@304
    .line 964
    :cond_304
    move-object/from16 v0, p0

    #@306
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@308
    const/4 v4, 0x0

    #@309
    const/4 v5, 0x6

    #@30a
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@30d
    move-result-object v2

    #@30e
    const-string v4, "450069"

    #@310
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@313
    move-result v2

    #@314
    if-eqz v2, :cond_3ad

    #@316
    .line 965
    move-object/from16 v0, p0

    #@318
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@31a
    const/4 v4, 0x0

    #@31b
    move-object/from16 v0, p0

    #@31d
    invoke-direct {v0, v2, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->setUiccType(Ljava/lang/String;Ljava/lang/String;)V

    #@320
    .line 966
    const/4 v2, 0x2

    #@321
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mIsEmptyUsim:I

    #@323
    .line 975
    :goto_323
    move-object/from16 v0, p0

    #@325
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@327
    const/4 v4, 0x0

    #@328
    const/4 v5, 0x5

    #@329
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@32c
    move-result-object v2

    #@32d
    const-string v4, "45005"

    #@32f
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@332
    move-result v2

    #@333
    if-eqz v2, :cond_3b2

    #@335
    .line 976
    const/4 v2, 0x1

    #@336
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mIsSponIMSI:I

    #@338
    .line 977
    const-string v2, "GSM"

    #@33a
    const-string v4, "[FakeRoaming] 45005: LGE_USIM_IS_HOME_IMSI"

    #@33c
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33f
    .line 989
    :cond_33f
    :goto_33f
    move-object/from16 v0, p0

    #@341
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@343
    const/16 v4, 0x2f41

    #@345
    const/16 v5, 0xc9

    #@347
    move-object/from16 v0, p0

    #@349
    invoke-virtual {v0, v5}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@34c
    move-result-object v5

    #@34d
    invoke-virtual {v2, v4, v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@350
    .line 990
    move-object/from16 v0, p0

    #@352
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@354
    add-int/lit8 v2, v2, 0x1

    #@356
    move-object/from16 v0, p0

    #@358
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@35a
    .line 991
    move-object/from16 v0, p0

    #@35c
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@35e
    const/16 v4, 0x2f42

    #@360
    const/16 v5, 0xca

    #@362
    move-object/from16 v0, p0

    #@364
    invoke-virtual {v0, v5}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@367
    move-result-object v5

    #@368
    invoke-virtual {v2, v4, v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@36b
    .line 992
    move-object/from16 v0, p0

    #@36d
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@36f
    add-int/lit8 v2, v2, 0x1

    #@371
    move-object/from16 v0, p0

    #@373
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@375
    .line 993
    move-object/from16 v0, p0

    #@377
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@379
    const/16 v4, 0x2f43

    #@37b
    const/16 v5, 0xcb

    #@37d
    move-object/from16 v0, p0

    #@37f
    invoke-virtual {v0, v5}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@382
    move-result-object v5

    #@383
    invoke-virtual {v2, v4, v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@386
    .line 994
    move-object/from16 v0, p0

    #@388
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@38a
    add-int/lit8 v2, v2, 0x1

    #@38c
    move-object/from16 v0, p0

    #@38e
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@390
    .line 995
    move-object/from16 v0, p0

    #@392
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@394
    const/16 v4, 0x2f40

    #@396
    const/16 v5, 0xc8

    #@398
    move-object/from16 v0, p0

    #@39a
    invoke-virtual {v0, v5}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@39d
    move-result-object v5

    #@39e
    invoke-virtual {v2, v4, v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@3a1
    .line 996
    move-object/from16 v0, p0

    #@3a3
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@3a5
    add-int/lit8 v2, v2, 0x1

    #@3a7
    move-object/from16 v0, p0

    #@3a9
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@3ab
    goto/16 :goto_4b

    #@3ad
    .line 968
    :cond_3ad
    const/4 v2, 0x1

    #@3ae
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mIsEmptyUsim:I

    #@3b0
    goto/16 :goto_323

    #@3b2
    .line 979
    :cond_3b2
    move-object/from16 v0, p0

    #@3b4
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@3b6
    const/4 v4, 0x0

    #@3b7
    const/4 v5, 0x5

    #@3b8
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3bb
    move-result-object v2

    #@3bc
    const-string v4, "45008"

    #@3be
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c1
    move-result v2

    #@3c2
    if-eqz v2, :cond_3d0

    #@3c4
    .line 980
    const/4 v2, 0x2

    #@3c5
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mIsSponIMSI:I

    #@3c7
    .line 981
    const-string v2, "GSM"

    #@3c9
    const-string v4, "[FakeRoaming] 45008: LGE_USIM_IS_SPON1_IMSI"

    #@3cb
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3ce
    goto/16 :goto_33f

    #@3d0
    .line 983
    :cond_3d0
    move-object/from16 v0, p0

    #@3d2
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@3d4
    const/4 v4, 0x0

    #@3d5
    const/4 v5, 0x5

    #@3d6
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3d9
    move-result-object v2

    #@3da
    const-string v4, "45000"

    #@3dc
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3df
    move-result v2

    #@3e0
    if-eqz v2, :cond_33f

    #@3e2
    .line 984
    const/4 v2, 0x3

    #@3e3
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mIsSponIMSI:I

    #@3e5
    .line 985
    const-string v2, "GSM"

    #@3e7
    const-string v4, "[FakeRoaming] 45000: LGE_USIM_IS_SPON2_IMSI"

    #@3e9
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3ec
    goto/16 :goto_33f

    #@3ee
    .line 1008
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_3ee
    const-string v2, "GSM"

    #@3f0
    const-string v4, "EVENT_GET_MASTER_IMSI_DONE_LGU"

    #@3f2
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f5
    .line 1009
    const/16 v20, 0x1

    #@3f7
    .line 1011
    move-object/from16 v0, p1

    #@3f9
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3fb
    check-cast v9, Landroid/os/AsyncResult;

    #@3fd
    .line 1012
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3ff
    if-eqz v2, :cond_42e

    #@401
    .line 1013
    const-string v2, "GSM"

    #@403
    new-instance v4, Ljava/lang/StringBuilder;

    #@405
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@408
    const-string v5, "Exception querying MF IMSI, Exception:"

    #@40a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40d
    move-result-object v4

    #@40e
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@410
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@413
    move-result-object v4

    #@414
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@417
    move-result-object v4

    #@418
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41b
    .line 1014
    move-object/from16 v0, p0

    #@41d
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@41f
    const/4 v4, 0x0

    #@420
    move-object/from16 v0, p0

    #@422
    invoke-direct {v0, v2, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->setUiccType(Ljava/lang/String;Ljava/lang/String;)V

    #@425
    .line 1015
    move-object/from16 v0, p0

    #@427
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsiReadyRegistrants:Landroid/os/RegistrantList;

    #@429
    invoke-virtual {v2}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@42c
    goto/16 :goto_4b

    #@42e
    .line 1019
    :cond_42e
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@430
    check-cast v2, [B

    #@432
    move-object v0, v2

    #@433
    check-cast v0, [B

    #@435
    move-object v12, v0

    #@436
    .line 1021
    .local v12, data:[B
    const/4 v2, 0x0

    #@437
    aget-byte v2, v12, v2

    #@439
    if-nez v2, :cond_455

    #@43b
    .line 1022
    const-string v2, "GSM"

    #@43d
    const-string v4, "MASTER IMSI IS EMPTY"

    #@43f
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@442
    .line 1023
    move-object/from16 v0, p0

    #@444
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@446
    const/4 v4, 0x0

    #@447
    move-object/from16 v0, p0

    #@449
    invoke-direct {v0, v2, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->setUiccType(Ljava/lang/String;Ljava/lang/String;)V

    #@44c
    .line 1024
    move-object/from16 v0, p0

    #@44e
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsiReadyRegistrants:Landroid/os/RegistrantList;

    #@450
    invoke-virtual {v2}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@453
    goto/16 :goto_4b

    #@455
    .line 1027
    :cond_455
    const-string v2, "GSM"

    #@457
    new-instance v4, Ljava/lang/StringBuilder;

    #@459
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@45c
    const-string v5, "RAW MF IMSI : "

    #@45e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@461
    move-result-object v4

    #@462
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@465
    move-result-object v5

    #@466
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@469
    move-result-object v4

    #@46a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46d
    move-result-object v4

    #@46e
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@471
    .line 1029
    const/4 v2, 0x2

    #@472
    aget-byte v2, v12, v2

    #@474
    and-int/lit16 v2, v2, 0xf0

    #@476
    const/16 v4, 0x40

    #@478
    if-ne v2, v4, :cond_4ae

    #@47a
    const/4 v2, 0x3

    #@47b
    aget-byte v2, v12, v2

    #@47d
    const/4 v4, 0x5

    #@47e
    if-ne v2, v4, :cond_4ae

    #@480
    const/4 v2, 0x4

    #@481
    aget-byte v2, v12, v2

    #@483
    const/16 v4, 0x60

    #@485
    if-eq v2, v4, :cond_48c

    #@487
    const/4 v2, 0x4

    #@488
    aget-byte v2, v12, v2

    #@48a
    if-nez v2, :cond_4ae

    #@48c
    .line 1030
    :cond_48c
    const/4 v2, 0x5

    #@48d
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mUiccType:I

    #@48f
    .line 1031
    const/4 v2, 0x0

    #@490
    array-length v4, v12

    #@491
    invoke-static {v12, v2, v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bcdToString([BII)Ljava/lang/String;

    #@494
    move-result-object v2

    #@495
    const/4 v4, 0x5

    #@496
    const/16 v5, 0x14

    #@498
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@49b
    move-result-object v2

    #@49c
    move-object/from16 v0, p0

    #@49e
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mMFImsi:Ljava/lang/String;

    #@4a0
    .line 1032
    const-string v2, "45006"

    #@4a2
    sput-object v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mMccMnc:Ljava/lang/String;

    #@4a4
    .line 1033
    move-object/from16 v0, p0

    #@4a6
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mMFImsi:Ljava/lang/String;

    #@4a8
    const/4 v4, 0x0

    #@4a9
    move-object/from16 v0, p0

    #@4ab
    invoke-direct {v0, v2, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->setUiccType(Ljava/lang/String;Ljava/lang/String;)V

    #@4ae
    .line 1036
    :cond_4ae
    const-string v2, "GSM"

    #@4b0
    new-instance v4, Ljava/lang/StringBuilder;

    #@4b2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4b5
    const-string v5, "mUiccType : "

    #@4b7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ba
    move-result-object v4

    #@4bb
    sget v5, Lcom/android/internal/telephony/uicc/SIMRecords;->mUiccType:I

    #@4bd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c0
    move-result-object v4

    #@4c1
    const-string v5, "    mMFImsi : "

    #@4c3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c6
    move-result-object v4

    #@4c7
    move-object/from16 v0, p0

    #@4c9
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mMFImsi:Ljava/lang/String;

    #@4cb
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ce
    move-result-object v4

    #@4cf
    const-string v5, "    mMccMnc : "

    #@4d1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d4
    move-result-object v4

    #@4d5
    sget-object v5, Lcom/android/internal/telephony/uicc/SIMRecords;->mMccMnc:Ljava/lang/String;

    #@4d7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4da
    move-result-object v4

    #@4db
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4de
    move-result-object v4

    #@4df
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e2
    .line 1038
    move-object/from16 v0, p0

    #@4e4
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsiReadyRegistrants:Landroid/os/RegistrantList;

    #@4e6
    invoke-virtual {v2}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@4e9
    goto/16 :goto_4b

    #@4eb
    .line 1043
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_4eb
    const-string v2, "GSM"

    #@4ed
    const-string v4, "EVENT_GET_SPON_IMSI1_DONE_LGU"

    #@4ef
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f2
    .line 1044
    const/16 v20, 0x1

    #@4f4
    .line 1046
    move-object/from16 v0, p1

    #@4f6
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4f8
    check-cast v9, Landroid/os/AsyncResult;

    #@4fa
    .line 1047
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4fc
    if-eqz v2, :cond_51a

    #@4fe
    .line 1048
    const-string v2, "GSM"

    #@500
    new-instance v4, Ljava/lang/StringBuilder;

    #@502
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@505
    const-string v5, "Exception querying Spon IMSI1, Exception:"

    #@507
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50a
    move-result-object v4

    #@50b
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@50d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@510
    move-result-object v4

    #@511
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@514
    move-result-object v4

    #@515
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@518
    goto/16 :goto_4b

    #@51a
    .line 1052
    :cond_51a
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@51c
    check-cast v2, [B

    #@51e
    move-object v0, v2

    #@51f
    check-cast v0, [B

    #@521
    move-object v12, v0

    #@522
    .line 1053
    .restart local v12       #data:[B
    const/4 v2, 0x0

    #@523
    aget-byte v2, v12, v2

    #@525
    if-eqz v2, :cond_4b

    #@527
    .line 1054
    const-string v2, "GSM"

    #@529
    new-instance v4, Ljava/lang/StringBuilder;

    #@52b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@52e
    const-string v5, "RAW Spon IMSI1 : "

    #@530
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@533
    move-result-object v4

    #@534
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@537
    move-result-object v5

    #@538
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53b
    move-result-object v4

    #@53c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53f
    move-result-object v4

    #@540
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@543
    .line 1056
    const/4 v2, 0x0

    #@544
    array-length v4, v12

    #@545
    invoke-static {v12, v2, v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bcdToString([BII)Ljava/lang/String;

    #@548
    move-result-object v2

    #@549
    const/4 v4, 0x5

    #@54a
    const/16 v5, 0x14

    #@54c
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@54f
    move-result-object v2

    #@550
    move-object/from16 v0, p0

    #@552
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi1:Ljava/lang/String;

    #@554
    .line 1057
    const-string v2, "GSM"

    #@556
    new-instance v4, Ljava/lang/StringBuilder;

    #@558
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@55b
    const-string v5, "mSponImsi1 : "

    #@55d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@560
    move-result-object v4

    #@561
    move-object/from16 v0, p0

    #@563
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi1:Ljava/lang/String;

    #@565
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@568
    move-result-object v4

    #@569
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56c
    move-result-object v4

    #@56d
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@570
    .line 1058
    move-object/from16 v0, p0

    #@572
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@574
    const/4 v4, 0x0

    #@575
    move-object/from16 v0, p0

    #@577
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@579
    add-int/lit8 v5, v5, 0x3

    #@57b
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@57e
    move-result-object v2

    #@57f
    move-object/from16 v0, p0

    #@581
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi1:Ljava/lang/String;

    #@583
    const/4 v5, 0x0

    #@584
    move-object/from16 v0, p0

    #@586
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@588
    add-int/lit8 v6, v6, 0x3

    #@58a
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@58d
    move-result-object v4

    #@58e
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@591
    move-result v2

    #@592
    if-eqz v2, :cond_4b

    #@594
    .line 1059
    const/4 v2, 0x2

    #@595
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mIsSponIMSI:I

    #@597
    goto/16 :goto_4b

    #@599
    .line 1064
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_599
    const-string v2, "GSM"

    #@59b
    const-string v4, "EVENT_GET_SPON_IMSI2_DONE_LGU"

    #@59d
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5a0
    .line 1065
    const/16 v20, 0x1

    #@5a2
    .line 1067
    move-object/from16 v0, p1

    #@5a4
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5a6
    check-cast v9, Landroid/os/AsyncResult;

    #@5a8
    .line 1068
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5aa
    if-eqz v2, :cond_5c8

    #@5ac
    .line 1069
    const-string v2, "GSM"

    #@5ae
    new-instance v4, Ljava/lang/StringBuilder;

    #@5b0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5b3
    const-string v5, "Exception querying Spon IMSI2, Exception:"

    #@5b5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b8
    move-result-object v4

    #@5b9
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5bb
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5be
    move-result-object v4

    #@5bf
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c2
    move-result-object v4

    #@5c3
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5c6
    goto/16 :goto_4b

    #@5c8
    .line 1073
    :cond_5c8
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@5ca
    check-cast v2, [B

    #@5cc
    move-object v0, v2

    #@5cd
    check-cast v0, [B

    #@5cf
    move-object v12, v0

    #@5d0
    .line 1074
    .restart local v12       #data:[B
    const/4 v2, 0x0

    #@5d1
    aget-byte v2, v12, v2

    #@5d3
    if-eqz v2, :cond_4b

    #@5d5
    .line 1075
    const-string v2, "GSM"

    #@5d7
    new-instance v4, Ljava/lang/StringBuilder;

    #@5d9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5dc
    const-string v5, "RAW Spon IMSI2 : "

    #@5de
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e1
    move-result-object v4

    #@5e2
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@5e5
    move-result-object v5

    #@5e6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e9
    move-result-object v4

    #@5ea
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5ed
    move-result-object v4

    #@5ee
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f1
    .line 1077
    const/4 v2, 0x0

    #@5f2
    array-length v4, v12

    #@5f3
    invoke-static {v12, v2, v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bcdToString([BII)Ljava/lang/String;

    #@5f6
    move-result-object v2

    #@5f7
    const/4 v4, 0x5

    #@5f8
    const/16 v5, 0x14

    #@5fa
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5fd
    move-result-object v2

    #@5fe
    move-object/from16 v0, p0

    #@600
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi2:Ljava/lang/String;

    #@602
    .line 1078
    const-string v2, "GSM"

    #@604
    new-instance v4, Ljava/lang/StringBuilder;

    #@606
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@609
    const-string v5, "mSponImsi2 : "

    #@60b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60e
    move-result-object v4

    #@60f
    move-object/from16 v0, p0

    #@611
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi2:Ljava/lang/String;

    #@613
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@616
    move-result-object v4

    #@617
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61a
    move-result-object v4

    #@61b
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61e
    .line 1079
    move-object/from16 v0, p0

    #@620
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@622
    const/4 v4, 0x0

    #@623
    move-object/from16 v0, p0

    #@625
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@627
    add-int/lit8 v5, v5, 0x3

    #@629
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@62c
    move-result-object v2

    #@62d
    move-object/from16 v0, p0

    #@62f
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi2:Ljava/lang/String;

    #@631
    const/4 v5, 0x0

    #@632
    move-object/from16 v0, p0

    #@634
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@636
    add-int/lit8 v6, v6, 0x3

    #@638
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@63b
    move-result-object v4

    #@63c
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63f
    move-result v2

    #@640
    if-eqz v2, :cond_4b

    #@642
    .line 1080
    const/4 v2, 0x3

    #@643
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mIsSponIMSI:I

    #@645
    goto/16 :goto_4b

    #@647
    .line 1085
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_647
    const-string v2, "GSM"

    #@649
    const-string v4, "EVENT_GET_SPON_IMSI3_DONE_LGU"

    #@64b
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64e
    .line 1086
    const/16 v20, 0x1

    #@650
    .line 1088
    move-object/from16 v0, p1

    #@652
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@654
    check-cast v9, Landroid/os/AsyncResult;

    #@656
    .line 1089
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@658
    if-eqz v2, :cond_676

    #@65a
    .line 1090
    const-string v2, "GSM"

    #@65c
    new-instance v4, Ljava/lang/StringBuilder;

    #@65e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@661
    const-string v5, "Exception querying Spon IMSI3, Exception:"

    #@663
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@666
    move-result-object v4

    #@667
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@669
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66c
    move-result-object v4

    #@66d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@670
    move-result-object v4

    #@671
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@674
    goto/16 :goto_4b

    #@676
    .line 1094
    :cond_676
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@678
    check-cast v2, [B

    #@67a
    move-object v0, v2

    #@67b
    check-cast v0, [B

    #@67d
    move-object v12, v0

    #@67e
    .line 1095
    .restart local v12       #data:[B
    const/4 v2, 0x0

    #@67f
    aget-byte v2, v12, v2

    #@681
    if-eqz v2, :cond_4b

    #@683
    .line 1096
    const-string v2, "GSM"

    #@685
    new-instance v4, Ljava/lang/StringBuilder;

    #@687
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@68a
    const-string v5, "RAW Spon IMSI3 : "

    #@68c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68f
    move-result-object v4

    #@690
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@693
    move-result-object v5

    #@694
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@697
    move-result-object v4

    #@698
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69b
    move-result-object v4

    #@69c
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@69f
    .line 1098
    const/4 v2, 0x0

    #@6a0
    array-length v4, v12

    #@6a1
    invoke-static {v12, v2, v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bcdToString([BII)Ljava/lang/String;

    #@6a4
    move-result-object v2

    #@6a5
    const/4 v4, 0x5

    #@6a6
    const/16 v5, 0x14

    #@6a8
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6ab
    move-result-object v2

    #@6ac
    move-object/from16 v0, p0

    #@6ae
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi3:Ljava/lang/String;

    #@6b0
    .line 1099
    const-string v2, "GSM"

    #@6b2
    new-instance v4, Ljava/lang/StringBuilder;

    #@6b4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6b7
    const-string v5, "mSponImsi3 : "

    #@6b9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6bc
    move-result-object v4

    #@6bd
    move-object/from16 v0, p0

    #@6bf
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi3:Ljava/lang/String;

    #@6c1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c4
    move-result-object v4

    #@6c5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c8
    move-result-object v4

    #@6c9
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6cc
    .line 1100
    move-object/from16 v0, p0

    #@6ce
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@6d0
    const/4 v4, 0x0

    #@6d1
    move-object/from16 v0, p0

    #@6d3
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@6d5
    add-int/lit8 v5, v5, 0x3

    #@6d7
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6da
    move-result-object v2

    #@6db
    move-object/from16 v0, p0

    #@6dd
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mSponImsi3:Ljava/lang/String;

    #@6df
    const/4 v5, 0x0

    #@6e0
    move-object/from16 v0, p0

    #@6e2
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@6e4
    add-int/lit8 v6, v6, 0x3

    #@6e6
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6e9
    move-result-object v4

    #@6ea
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6ed
    move-result v2

    #@6ee
    if-eqz v2, :cond_4b

    #@6f0
    .line 1101
    const/4 v2, 0x4

    #@6f1
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mIsSponIMSI:I

    #@6f3
    goto/16 :goto_4b

    #@6f5
    .line 1108
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_6f5
    const-string v2, "GSM"

    #@6f7
    const-string v4, "EVENT_GET_IMSI_DONE_KR"

    #@6f9
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6fc
    .line 1109
    const/16 v20, 0x1

    #@6fe
    .line 1111
    move-object/from16 v0, p0

    #@700
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->imsip:Ljava/lang/String;

    #@702
    if-nez v2, :cond_4b

    #@704
    .line 1112
    move-object/from16 v0, p1

    #@706
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@708
    check-cast v9, Landroid/os/AsyncResult;

    #@70a
    .line 1114
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@70c
    if-eqz v2, :cond_72a

    #@70e
    .line 1115
    const-string v2, "GSM"

    #@710
    new-instance v4, Ljava/lang/StringBuilder;

    #@712
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@715
    const-string v5, "Exception querying IMSI, Exception:"

    #@717
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71a
    move-result-object v4

    #@71b
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@71d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@720
    move-result-object v4

    #@721
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@724
    move-result-object v4

    #@725
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@728
    goto/16 :goto_4b

    #@72a
    .line 1121
    :cond_72a
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@72c
    check-cast v2, Ljava/lang/String;

    #@72e
    move-object/from16 v0, p0

    #@730
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@732
    .line 1123
    move-object/from16 v0, p0

    #@734
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@736
    if-eqz v2, :cond_74f

    #@738
    move-object/from16 v0, p0

    #@73a
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@73c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@73f
    move-result v2

    #@740
    const/4 v4, 0x6

    #@741
    if-lt v2, v4, :cond_74f

    #@743
    move-object/from16 v0, p0

    #@745
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@747
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@74a
    move-result v2

    #@74b
    const/16 v4, 0xf

    #@74d
    if-le v2, v4, :cond_76d

    #@74f
    .line 1124
    :cond_74f
    const-string v2, "GSM"

    #@751
    new-instance v4, Ljava/lang/StringBuilder;

    #@753
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@756
    const-string v5, "invalid IMSI "

    #@758
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75b
    move-result-object v4

    #@75c
    move-object/from16 v0, p0

    #@75e
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@760
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@763
    move-result-object v4

    #@764
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@767
    move-result-object v4

    #@768
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@76b
    goto/16 :goto_4b

    #@76d
    .line 1130
    :cond_76d
    const-string v2, "GSM"

    #@76f
    new-instance v4, Ljava/lang/StringBuilder;

    #@771
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@774
    const-string v5, "IMSI: "

    #@776
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@779
    move-result-object v4

    #@77a
    move-object/from16 v0, p0

    #@77c
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@77e
    const/4 v6, 0x0

    #@77f
    const/4 v7, 0x6

    #@780
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@783
    move-result-object v5

    #@784
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@787
    move-result-object v4

    #@788
    const-string v5, "xxxxxxxxx"

    #@78a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78d
    move-result-object v4

    #@78e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@791
    move-result-object v4

    #@792
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@795
    .line 1131
    move-object/from16 v0, p0

    #@797
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@799
    const/4 v4, 0x0

    #@79a
    move-object/from16 v0, p0

    #@79c
    invoke-direct {v0, v2, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->setUiccType(Ljava/lang/String;Ljava/lang/String;)V

    #@79f
    goto/16 :goto_4b

    #@7a1
    .line 1136
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_7a1
    const-string v2, "GSM"

    #@7a3
    const-string v4, "EVENT_GET_IMSIP_DONE_KR"

    #@7a5
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a8
    .line 1139
    move-object/from16 v0, p1

    #@7aa
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7ac
    check-cast v9, Landroid/os/AsyncResult;

    #@7ae
    .line 1140
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@7b0
    check-cast v2, [B

    #@7b2
    move-object v0, v2

    #@7b3
    check-cast v0, [B

    #@7b5
    move-object v12, v0

    #@7b6
    .line 1142
    .restart local v12       #data:[B
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@7b8
    if-eqz v2, :cond_7c8

    #@7ba
    .line 1143
    const-string v2, "GSM"

    #@7bc
    const-string v4, "IMSIP: Exception -can\'t read IMSIP correctly, IMSIP: null"

    #@7be
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7c1
    .line 1144
    const/4 v2, 0x0

    #@7c2
    move-object/from16 v0, p0

    #@7c4
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->imsip:Ljava/lang/String;

    #@7c6
    goto/16 :goto_4b

    #@7c8
    .line 1148
    :cond_7c8
    const/4 v2, 0x0

    #@7c9
    array-length v4, v12

    #@7ca
    invoke-static {v12, v2, v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bcdToString([BII)Ljava/lang/String;

    #@7cd
    move-result-object v2

    #@7ce
    move-object/from16 v0, p0

    #@7d0
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->imsip:Ljava/lang/String;

    #@7d2
    .line 1150
    const/4 v2, 0x1

    #@7d3
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mUiccType:I

    #@7d5
    .line 1152
    const-string v2, "gsm.sim.type"

    #@7d7
    sget v4, Lcom/android/internal/telephony/uicc/SIMRecords;->mUiccType:I

    #@7d9
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@7dc
    move-result-object v4

    #@7dd
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7e0
    .line 1154
    const-string v2, "GSM"

    #@7e2
    const-string v4, "[LGE_USIM] mUiccType : SKT"

    #@7e4
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7e7
    .line 1155
    const-string v2, "GSM"

    #@7e9
    new-instance v4, Ljava/lang/StringBuilder;

    #@7eb
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7ee
    const-string v5, "IMSIP: "

    #@7f0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f3
    move-result-object v4

    #@7f4
    move-object/from16 v0, p0

    #@7f6
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->imsip:Ljava/lang/String;

    #@7f8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7fb
    move-result-object v4

    #@7fc
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7ff
    move-result-object v4

    #@800
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@803
    goto/16 :goto_4b

    #@805
    .line 1159
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_805
    const/16 v20, 0x0

    #@807
    .line 1161
    move-object/from16 v0, p1

    #@809
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@80b
    check-cast v9, Landroid/os/AsyncResult;

    #@80d
    .line 1163
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@80f
    if-eqz v2, :cond_81a

    #@811
    .line 1164
    const-string v2, "GSM"

    #@813
    const-string v4, "Finally invalid or missing EF[MSISDN]"

    #@815
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@818
    goto/16 :goto_4b

    #@81a
    .line 1168
    :cond_81a
    iget-object v3, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@81c
    check-cast v3, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@81e
    .line 1170
    .local v3, adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/AdnRecord;->getNumber()Ljava/lang/String;

    #@821
    move-result-object v2

    #@822
    move-object/from16 v0, p0

    #@824
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@826
    .line 1171
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/AdnRecord;->getAlphaTag()Ljava/lang/String;

    #@829
    move-result-object v2

    #@82a
    move-object/from16 v0, p0

    #@82c
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdnTag:Ljava/lang/String;

    #@82e
    .line 1173
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@830
    if-eqz v2, :cond_4b

    #@832
    const-string v2, "GSM"

    #@834
    new-instance v4, Ljava/lang/StringBuilder;

    #@836
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@839
    const-string v5, "MSISDN: "

    #@83b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83e
    move-result-object v4

    #@83f
    move-object/from16 v0, p0

    #@841
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@843
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@846
    move-result-object v4

    #@847
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84a
    move-result-object v4

    #@84b
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84e
    goto/16 :goto_4b

    #@850
    .line 1179
    .end local v3           #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_850
    const/16 v20, 0x1

    #@852
    .line 1181
    move-object/from16 v0, p1

    #@854
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@856
    check-cast v9, Landroid/os/AsyncResult;

    #@858
    .line 1182
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@85a
    check-cast v2, [B

    #@85c
    move-object v0, v2

    #@85d
    check-cast v0, [B

    #@85f
    move-object v12, v0

    #@860
    .line 1184
    .restart local v12       #data:[B
    const/16 v21, 0x0

    #@862
    .line 1185
    .local v21, isValidMbdn:Z
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@864
    if-nez v2, :cond_8a2

    #@866
    .line 1187
    new-instance v2, Ljava/lang/StringBuilder;

    #@868
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@86b
    const-string v4, "EF_MBI: "

    #@86d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@870
    move-result-object v2

    #@871
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@874
    move-result-object v4

    #@875
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@878
    move-result-object v2

    #@879
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87c
    move-result-object v2

    #@87d
    move-object/from16 v0, p0

    #@87f
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@882
    .line 1190
    const/4 v2, 0x0

    #@883
    aget-byte v2, v12, v2

    #@885
    and-int/lit16 v2, v2, 0xff

    #@887
    move-object/from16 v0, p0

    #@889
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mailboxIndex:I

    #@88b
    .line 1193
    move-object/from16 v0, p0

    #@88d
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mailboxIndex:I

    #@88f
    if-eqz v2, :cond_8a2

    #@891
    move-object/from16 v0, p0

    #@893
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mailboxIndex:I

    #@895
    const/16 v4, 0xff

    #@897
    if-eq v2, v4, :cond_8a2

    #@899
    .line 1194
    const-string v2, "Got valid mailbox number for MBDN"

    #@89b
    move-object/from16 v0, p0

    #@89d
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@8a0
    .line 1195
    const/16 v21, 0x1

    #@8a2
    .line 1200
    :cond_8a2
    move-object/from16 v0, p0

    #@8a4
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@8a6
    add-int/lit8 v2, v2, 0x1

    #@8a8
    move-object/from16 v0, p0

    #@8aa
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@8ac
    .line 1202
    if-eqz v21, :cond_8cb

    #@8ae
    .line 1204
    new-instance v2, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@8b0
    move-object/from16 v0, p0

    #@8b2
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@8b4
    invoke-direct {v2, v4}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@8b7
    const/16 v4, 0x6fc7

    #@8b9
    const/16 v5, 0x6fc8

    #@8bb
    move-object/from16 v0, p0

    #@8bd
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mailboxIndex:I

    #@8bf
    const/4 v7, 0x6

    #@8c0
    move-object/from16 v0, p0

    #@8c2
    invoke-virtual {v0, v7}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@8c5
    move-result-object v7

    #@8c6
    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->loadFromEF(IIILandroid/os/Message;)V

    #@8c9
    goto/16 :goto_4b

    #@8cb
    .line 1209
    :cond_8cb
    new-instance v2, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@8cd
    move-object/from16 v0, p0

    #@8cf
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@8d1
    invoke-direct {v2, v4}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@8d4
    const/16 v4, 0x6f17

    #@8d6
    const/16 v5, 0x6f4a

    #@8d8
    const/4 v6, 0x1

    #@8d9
    const/16 v7, 0xb

    #@8db
    move-object/from16 v0, p0

    #@8dd
    invoke-virtual {v0, v7}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@8e0
    move-result-object v7

    #@8e1
    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->loadFromEF(IIILandroid/os/Message;)V

    #@8e4
    goto/16 :goto_4b

    #@8e6
    .line 1222
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    .end local v21           #isValidMbdn:Z
    :sswitch_8e6
    const/4 v2, 0x0

    #@8e7
    move-object/from16 v0, p0

    #@8e9
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailNum:Ljava/lang/String;

    #@8eb
    .line 1223
    const/4 v2, 0x0

    #@8ec
    move-object/from16 v0, p0

    #@8ee
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailTag:Ljava/lang/String;

    #@8f0
    .line 1224
    const/16 v20, 0x1

    #@8f2
    .line 1226
    move-object/from16 v0, p1

    #@8f4
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8f6
    check-cast v9, Landroid/os/AsyncResult;

    #@8f8
    .line 1228
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@8fa
    if-eqz v2, :cond_952

    #@8fc
    .line 1230
    new-instance v2, Ljava/lang/StringBuilder;

    #@8fe
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@901
    const-string v4, "Invalid or missing EF"

    #@903
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@906
    move-result-object v4

    #@907
    move-object/from16 v0, p1

    #@909
    iget v2, v0, Landroid/os/Message;->what:I

    #@90b
    const/16 v5, 0xb

    #@90d
    if-ne v2, v5, :cond_94f

    #@90f
    const-string v2, "[MAILBOX]"

    #@911
    :goto_911
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@914
    move-result-object v2

    #@915
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@918
    move-result-object v2

    #@919
    move-object/from16 v0, p0

    #@91b
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@91e
    .line 1236
    move-object/from16 v0, p1

    #@920
    iget v2, v0, Landroid/os/Message;->what:I

    #@922
    const/4 v4, 0x6

    #@923
    if-ne v2, v4, :cond_4b

    #@925
    .line 1238
    const/4 v2, 0x1

    #@926
    move-object/from16 v0, p0

    #@928
    iput-boolean v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mVmsIsNotInSIM:Z

    #@92a
    .line 1243
    move-object/from16 v0, p0

    #@92c
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@92e
    add-int/lit8 v2, v2, 0x1

    #@930
    move-object/from16 v0, p0

    #@932
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@934
    .line 1244
    new-instance v2, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@936
    move-object/from16 v0, p0

    #@938
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@93a
    invoke-direct {v2, v4}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@93d
    const/16 v4, 0x6f17

    #@93f
    const/16 v5, 0x6f4a

    #@941
    const/4 v6, 0x1

    #@942
    const/16 v7, 0xb

    #@944
    move-object/from16 v0, p0

    #@946
    invoke-virtual {v0, v7}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@949
    move-result-object v7

    #@94a
    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->loadFromEF(IIILandroid/os/Message;)V

    #@94d
    goto/16 :goto_4b

    #@94f
    .line 1230
    :cond_94f
    const-string v2, "[MBDN]"

    #@951
    goto :goto_911

    #@952
    .line 1251
    :cond_952
    iget-object v3, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@954
    check-cast v3, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@956
    .line 1253
    .restart local v3       #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    new-instance v2, Ljava/lang/StringBuilder;

    #@958
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@95b
    const-string v4, "VM: "

    #@95d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@960
    move-result-object v2

    #@961
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@964
    move-result-object v4

    #@965
    move-object/from16 v0, p1

    #@967
    iget v2, v0, Landroid/os/Message;->what:I

    #@969
    const/16 v5, 0xb

    #@96b
    if-ne v2, v5, :cond_9b3

    #@96d
    const-string v2, " EF[MAILBOX]"

    #@96f
    :goto_96f
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@972
    move-result-object v2

    #@973
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@976
    move-result-object v2

    #@977
    move-object/from16 v0, p0

    #@979
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@97c
    .line 1256
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/AdnRecord;->isEmpty()Z

    #@97f
    move-result v2

    #@980
    if-eqz v2, :cond_9b6

    #@982
    move-object/from16 v0, p1

    #@984
    iget v2, v0, Landroid/os/Message;->what:I

    #@986
    const/4 v4, 0x6

    #@987
    if-ne v2, v4, :cond_9b6

    #@989
    .line 1261
    const/4 v2, 0x1

    #@98a
    move-object/from16 v0, p0

    #@98c
    iput-boolean v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mVmsIsNotInSIM:Z

    #@98e
    .line 1263
    move-object/from16 v0, p0

    #@990
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@992
    add-int/lit8 v2, v2, 0x1

    #@994
    move-object/from16 v0, p0

    #@996
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@998
    .line 1264
    new-instance v2, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@99a
    move-object/from16 v0, p0

    #@99c
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@99e
    invoke-direct {v2, v4}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@9a1
    const/16 v4, 0x6f17

    #@9a3
    const/16 v5, 0x6f4a

    #@9a5
    const/4 v6, 0x1

    #@9a6
    const/16 v7, 0xb

    #@9a8
    move-object/from16 v0, p0

    #@9aa
    invoke-virtual {v0, v7}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@9ad
    move-result-object v7

    #@9ae
    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->loadFromEF(IIILandroid/os/Message;)V

    #@9b1
    goto/16 :goto_4b

    #@9b3
    .line 1253
    :cond_9b3
    const-string v2, " EF[MBDN]"

    #@9b5
    goto :goto_96f

    #@9b6
    .line 1271
    :cond_9b6
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/AdnRecord;->getNumber()Ljava/lang/String;

    #@9b9
    move-result-object v2

    #@9ba
    move-object/from16 v0, p0

    #@9bc
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailNum:Ljava/lang/String;

    #@9be
    .line 1272
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/AdnRecord;->getAlphaTag()Ljava/lang/String;

    #@9c1
    move-result-object v2

    #@9c2
    move-object/from16 v0, p0

    #@9c4
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailTag:Ljava/lang/String;

    #@9c6
    goto/16 :goto_4b

    #@9c8
    .line 1276
    .end local v3           #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_9c8
    const/16 v20, 0x1

    #@9ca
    .line 1278
    move-object/from16 v0, p1

    #@9cc
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9ce
    check-cast v9, Landroid/os/AsyncResult;

    #@9d0
    .line 1280
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@9d2
    if-eqz v2, :cond_9fc

    #@9d4
    .line 1281
    const-string v2, "Invalid or missing EF[MSISDN]"

    #@9d6
    move-object/from16 v0, p0

    #@9d8
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@9db
    .line 1284
    const-string v2, "GSM"

    #@9dd
    const-string v4, "and try to read again without an extensional EF"

    #@9df
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9e2
    .line 1285
    new-instance v2, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@9e4
    move-object/from16 v0, p0

    #@9e6
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@9e8
    invoke-direct {v2, v4}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@9eb
    const/16 v4, 0x6f40

    #@9ed
    const/4 v5, 0x0

    #@9ee
    const/4 v6, 0x1

    #@9ef
    const/16 v7, 0x2a

    #@9f1
    move-object/from16 v0, p0

    #@9f3
    invoke-virtual {v0, v7}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@9f6
    move-result-object v7

    #@9f7
    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->loadFromEF(IIILandroid/os/Message;)V

    #@9fa
    goto/16 :goto_4b

    #@9fc
    .line 1292
    :cond_9fc
    iget-object v3, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@9fe
    check-cast v3, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@a00
    .line 1294
    .restart local v3       #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/AdnRecord;->getNumber()Ljava/lang/String;

    #@a03
    move-result-object v2

    #@a04
    move-object/from16 v0, p0

    #@a06
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@a08
    .line 1295
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/AdnRecord;->getAlphaTag()Ljava/lang/String;

    #@a0b
    move-result-object v2

    #@a0c
    move-object/from16 v0, p0

    #@a0e
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdnTag:Ljava/lang/String;

    #@a10
    .line 1297
    const-string v2, "MSISDN: xxxxxxx"

    #@a12
    move-object/from16 v0, p0

    #@a14
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@a17
    .line 1299
    const-string v2, "ro.product.locale.region"

    #@a19
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@a1c
    move-result-object v2

    #@a1d
    const-string v4, "KR"

    #@a1f
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a22
    move-result v2

    #@a23
    if-eqz v2, :cond_4b

    #@a25
    .line 1300
    const-string v2, "ril.msisdn"

    #@a27
    move-object/from16 v0, p0

    #@a29
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@a2b
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@a2e
    goto/16 :goto_4b

    #@a30
    .line 1305
    .end local v3           #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_a30
    const/16 v20, 0x0

    #@a32
    .line 1306
    move-object/from16 v0, p1

    #@a34
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a36
    check-cast v9, Landroid/os/AsyncResult;

    #@a38
    .line 1308
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@a3a
    if-eqz v2, :cond_4b

    #@a3c
    .line 1309
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@a3e
    check-cast v2, Landroid/os/Message;

    #@a40
    invoke-static {v2}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@a43
    move-result-object v2

    #@a44
    iget-object v4, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@a46
    iput-object v4, v2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@a48
    .line 1311
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@a4a
    check-cast v2, Landroid/os/Message;

    #@a4c
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    #@a4f
    goto/16 :goto_4b

    #@a51
    .line 1316
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_a51
    const/16 v20, 0x1

    #@a53
    .line 1318
    move-object/from16 v0, p1

    #@a55
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a57
    check-cast v9, Landroid/os/AsyncResult;

    #@a59
    .line 1319
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@a5b
    check-cast v2, [B

    #@a5d
    move-object v0, v2

    #@a5e
    check-cast v0, [B

    #@a60
    move-object v12, v0

    #@a61
    .line 1322
    .restart local v12       #data:[B
    const/4 v2, 0x0

    #@a62
    const-string v4, "hide_privacy_log"

    #@a64
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a67
    move-result v2

    #@a68
    const/4 v4, 0x1

    #@a69
    if-ne v2, v4, :cond_ab5

    #@a6b
    .line 1323
    const-string v2, "1"

    #@a6d
    const-string v4, "persist.service.privacy.enable"

    #@a6f
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@a72
    move-result-object v4

    #@a73
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a76
    move-result v2

    #@a77
    if-eqz v2, :cond_a95

    #@a79
    .line 1324
    const-string v2, "GSM"

    #@a7b
    new-instance v4, Ljava/lang/StringBuilder;

    #@a7d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a80
    const-string v5, "EF_MWIS : "

    #@a82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a85
    move-result-object v4

    #@a86
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@a89
    move-result-object v5

    #@a8a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8d
    move-result-object v4

    #@a8e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a91
    move-result-object v4

    #@a92
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a95
    .line 1330
    :cond_a95
    :goto_a95
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@a97
    if-eqz v2, :cond_ad2

    #@a99
    .line 1331
    const-string v2, "GSM"

    #@a9b
    new-instance v4, Ljava/lang/StringBuilder;

    #@a9d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@aa0
    const-string v5, "EVENT_GET_MWIS_DONE exception = "

    #@aa2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa5
    move-result-object v4

    #@aa6
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@aa8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@aab
    move-result-object v4

    #@aac
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aaf
    move-result-object v4

    #@ab0
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ab3
    goto/16 :goto_4b

    #@ab5
    .line 1327
    :cond_ab5
    const-string v2, "GSM"

    #@ab7
    new-instance v4, Ljava/lang/StringBuilder;

    #@ab9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@abc
    const-string v5, "EF_MWIS : "

    #@abe
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac1
    move-result-object v4

    #@ac2
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@ac5
    move-result-object v5

    #@ac6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac9
    move-result-object v4

    #@aca
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@acd
    move-result-object v4

    #@ace
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ad1
    goto :goto_a95

    #@ad2
    .line 1336
    :cond_ad2
    const/4 v2, 0x0

    #@ad3
    aget-byte v2, v12, v2

    #@ad5
    and-int/lit16 v2, v2, 0xff

    #@ad7
    const/16 v4, 0xff

    #@ad9
    if-ne v2, v4, :cond_ae4

    #@adb
    .line 1337
    const-string v2, "GSM"

    #@add
    const-string v4, "SIMRecords: Uninitialized record MWIS"

    #@adf
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ae2
    goto/16 :goto_4b

    #@ae4
    .line 1341
    :cond_ae4
    move-object/from16 v0, p0

    #@ae6
    iput-object v12, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@ae8
    goto/16 :goto_4b

    #@aea
    .line 1346
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_aea
    move-object/from16 v0, p1

    #@aec
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@aee
    check-cast v9, Landroid/os/AsyncResult;

    #@af0
    .line 1347
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v0, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@af2
    move-object/from16 v28, v0

    #@af4
    check-cast v28, Landroid/os/Message;

    #@af6
    .line 1348
    .local v28, onComplete:Landroid/os/Message;
    if-eqz v28, :cond_4b

    #@af8
    .line 1351
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@afa
    if-eqz v2, :cond_b0e

    #@afc
    .line 1352
    invoke-static/range {v28 .. v28}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@aff
    move-result-object v2

    #@b00
    new-instance v4, Lcom/android/internal/telephony/uicc/IccVmNotSupportedException;

    #@b02
    const-string v5, "SIM update failed for EF_MWIS/EF_CPHS_MWIS"

    #@b04
    invoke-direct {v4, v5}, Lcom/android/internal/telephony/uicc/IccVmNotSupportedException;-><init>(Ljava/lang/String;)V

    #@b07
    iput-object v4, v2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@b09
    .line 1358
    :goto_b09
    invoke-virtual/range {v28 .. v28}, Landroid/os/Message;->sendToTarget()V

    #@b0c
    goto/16 :goto_4b

    #@b0e
    .line 1356
    :cond_b0e
    invoke-static/range {v28 .. v28}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@b11
    move-result-object v2

    #@b12
    const/4 v4, 0x0

    #@b13
    iput-object v4, v2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@b15
    goto :goto_b09

    #@b16
    .line 1363
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v28           #onComplete:Landroid/os/Message;
    :sswitch_b16
    const/16 v20, 0x1

    #@b18
    .line 1365
    move-object/from16 v0, p1

    #@b1a
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b1c
    check-cast v9, Landroid/os/AsyncResult;

    #@b1e
    .line 1366
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@b20
    check-cast v2, [B

    #@b22
    move-object v0, v2

    #@b23
    check-cast v0, [B

    #@b25
    move-object v12, v0

    #@b26
    .line 1368
    .restart local v12       #data:[B
    const/4 v2, 0x0

    #@b27
    const-string v4, "hide_privacy_log"

    #@b29
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@b2c
    move-result v2

    #@b2d
    const/4 v4, 0x1

    #@b2e
    if-ne v2, v4, :cond_b7a

    #@b30
    .line 1369
    const-string v2, "1"

    #@b32
    const-string v4, "persist.service.privacy.enable"

    #@b34
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@b37
    move-result-object v4

    #@b38
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b3b
    move-result v2

    #@b3c
    if-eqz v2, :cond_b5a

    #@b3e
    .line 1370
    const-string v2, "GSM"

    #@b40
    new-instance v4, Ljava/lang/StringBuilder;

    #@b42
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b45
    const-string v5, "EF_CPHS_MWI: "

    #@b47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4a
    move-result-object v4

    #@b4b
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@b4e
    move-result-object v5

    #@b4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b52
    move-result-object v4

    #@b53
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b56
    move-result-object v4

    #@b57
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b5a
    .line 1376
    :cond_b5a
    :goto_b5a
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@b5c
    if-eqz v2, :cond_b97

    #@b5e
    .line 1377
    const-string v2, "GSM"

    #@b60
    new-instance v4, Ljava/lang/StringBuilder;

    #@b62
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b65
    const-string v5, "EVENT_GET_VOICE_MAIL_INDICATOR_CPHS_DONE exception = "

    #@b67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6a
    move-result-object v4

    #@b6b
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@b6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b70
    move-result-object v4

    #@b71
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b74
    move-result-object v4

    #@b75
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b78
    goto/16 :goto_4b

    #@b7a
    .line 1373
    :cond_b7a
    const-string v2, "GSM"

    #@b7c
    new-instance v4, Ljava/lang/StringBuilder;

    #@b7e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b81
    const-string v5, "EF_CPHS_MWI: "

    #@b83
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b86
    move-result-object v4

    #@b87
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@b8a
    move-result-object v5

    #@b8b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8e
    move-result-object v4

    #@b8f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b92
    move-result-object v4

    #@b93
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b96
    goto :goto_b5a

    #@b97
    .line 1382
    :cond_b97
    move-object/from16 v0, p0

    #@b99
    iput-object v12, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->efCPHS_MWI:[B

    #@b9b
    goto/16 :goto_4b

    #@b9d
    .line 1386
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_b9d
    const/16 v20, 0x1

    #@b9f
    .line 1388
    move-object/from16 v0, p1

    #@ba1
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ba3
    check-cast v9, Landroid/os/AsyncResult;

    #@ba5
    .line 1389
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@ba7
    check-cast v2, [B

    #@ba9
    move-object v0, v2

    #@baa
    check-cast v0, [B

    #@bac
    move-object v12, v0

    #@bad
    .line 1391
    .restart local v12       #data:[B
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@baf
    if-nez v2, :cond_4b

    #@bb1
    .line 1395
    const/4 v2, 0x0

    #@bb2
    array-length v4, v12

    #@bb3
    invoke-static {v12, v2, v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bcdToString([BII)Ljava/lang/String;

    #@bb6
    move-result-object v2

    #@bb7
    move-object/from16 v0, p0

    #@bb9
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@bbb
    .line 1397
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@bbd
    if-eqz v2, :cond_c64

    #@bbf
    .line 1398
    new-instance v2, Ljava/lang/StringBuilder;

    #@bc1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@bc4
    const-string v4, "iccid: "

    #@bc6
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc9
    move-result-object v2

    #@bca
    move-object/from16 v0, p0

    #@bcc
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@bce
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd1
    move-result-object v2

    #@bd2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd5
    move-result-object v2

    #@bd6
    move-object/from16 v0, p0

    #@bd8
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@bdb
    .line 1403
    :goto_bdb
    const-string v2, "1"

    #@bdd
    const-string v4, "persist.service.vsim.enable"

    #@bdf
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@be2
    move-result-object v4

    #@be3
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@be6
    move-result v2

    #@be7
    if-eqz v2, :cond_c11

    #@be9
    .line 1404
    const-string v2, "persist.service.vsim.iccid"

    #@beb
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@bee
    move-result-object v34

    #@bef
    .line 1405
    .local v34, vsim_iccid:Ljava/lang/String;
    const-string v2, "GSM"

    #@bf1
    new-instance v4, Ljava/lang/StringBuilder;

    #@bf3
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@bf6
    const-string v5, "[LGE_UICC] VSIM ICCID : "

    #@bf8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bfb
    move-result-object v4

    #@bfc
    move-object/from16 v0, v34

    #@bfe
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c01
    move-result-object v4

    #@c02
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c05
    move-result-object v4

    #@c06
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c09
    .line 1406
    if-eqz v34, :cond_c11

    #@c0b
    .line 1407
    move-object/from16 v0, v34

    #@c0d
    move-object/from16 v1, p0

    #@c0f
    iput-object v0, v1, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@c11
    .line 1412
    .end local v34           #vsim_iccid:Ljava/lang/String;
    :cond_c11
    move-object/from16 v0, p0

    #@c13
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@c15
    move-object/from16 v0, p0

    #@c17
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->compICCID(Ljava/lang/String;)Z

    #@c1a
    move-result v2

    #@c1b
    if-eqz v2, :cond_c6d

    #@c1d
    .line 1414
    const-string v2, "SIMLock is activated by ICCID. SIM state is changed to ABSENT."

    #@c1f
    move-object/from16 v0, p0

    #@c21
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@c24
    .line 1415
    new-instance v19, Landroid/content/Intent;

    #@c26
    const-string v2, "android.intent.action.SIM_STATE_CHANGED"

    #@c28
    move-object/from16 v0, v19

    #@c2a
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c2d
    .line 1416
    .local v19, intent:Landroid/content/Intent;
    const/high16 v2, 0x2000

    #@c2f
    move-object/from16 v0, v19

    #@c31
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c34
    .line 1417
    const-string v2, "phoneName"

    #@c36
    const-string v4, "Phone"

    #@c38
    move-object/from16 v0, v19

    #@c3a
    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c3d
    .line 1418
    const-string v2, "ss"

    #@c3f
    sget-object v4, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@c41
    invoke-virtual {v4}, Lcom/android/internal/telephony/IccCardConstants$State;->toString()Ljava/lang/String;

    #@c44
    move-result-object v4

    #@c45
    move-object/from16 v0, v19

    #@c47
    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c4a
    .line 1419
    const-string v2, "android.permission.READ_PHONE_STATE"

    #@c4c
    const/4 v4, -0x1

    #@c4d
    move-object/from16 v0, v19

    #@c4f
    invoke-static {v0, v2, v4}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    #@c52
    .line 1421
    const-string v2, "gsm.sim.state"

    #@c54
    sget-object v4, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@c56
    invoke-virtual {v4}, Lcom/android/internal/telephony/IccCardConstants$State;->toString()Ljava/lang/String;

    #@c59
    move-result-object v4

    #@c5a
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@c5d
    .line 1422
    const/4 v2, 0x0

    #@c5e
    move-object/from16 v0, p0

    #@c60
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@c62
    goto/16 :goto_4b

    #@c64
    .line 1400
    .end local v19           #intent:Landroid/content/Intent;
    :cond_c64
    const-string v2, "iccid: xxxxxxxxxxxxxxxxxxx"

    #@c66
    move-object/from16 v0, p0

    #@c68
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@c6b
    goto/16 :goto_bdb

    #@c6d
    .line 1427
    :cond_c6d
    move-object/from16 v0, p0

    #@c6f
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@c71
    if-eqz v2, :cond_4b

    #@c73
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->iccidToLoad:Z

    #@c75
    const/4 v4, 0x1

    #@c76
    if-eq v2, v4, :cond_4b

    #@c78
    .line 1429
    const/4 v2, 0x1

    #@c79
    sput-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->iccidToLoad:Z

    #@c7b
    .line 1431
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->getSimIccid()Ljava/lang/String;

    #@c7e
    move-result-object v30

    #@c7f
    .line 1432
    .local v30, saved_iccid:Ljava/lang/String;
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@c81
    if-eqz v2, :cond_c9d

    #@c83
    .line 1433
    new-instance v2, Ljava/lang/StringBuilder;

    #@c85
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c88
    const-string v4, "[LGE_UICC] saved ICCID: "

    #@c8a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8d
    move-result-object v2

    #@c8e
    move-object/from16 v0, v30

    #@c90
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c93
    move-result-object v2

    #@c94
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c97
    move-result-object v2

    #@c98
    move-object/from16 v0, p0

    #@c9a
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@c9d
    .line 1435
    :cond_c9d
    move-object/from16 v0, p0

    #@c9f
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@ca1
    move-object/from16 v0, p0

    #@ca3
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->setSimIccid(Ljava/lang/String;)V

    #@ca6
    .line 1442
    move-object/from16 v0, p0

    #@ca8
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@caa
    move-object/from16 v0, v30

    #@cac
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@caf
    move-result v2

    #@cb0
    if-eqz v2, :cond_cd9

    #@cb2
    .line 1443
    const-string v2, "persist.radio.iccid-changed"

    #@cb4
    const-string v4, "0"

    #@cb6
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@cb9
    .line 1445
    const-string v2, "1"

    #@cbb
    const-string v4, "persist.service.vsim.enable"

    #@cbd
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@cc0
    move-result-object v4

    #@cc1
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cc4
    move-result v2

    #@cc5
    if-eqz v2, :cond_cd0

    #@cc7
    move-object/from16 v0, p0

    #@cc9
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@ccb
    if-eqz v2, :cond_cd0

    #@ccd
    .line 1446
    const/4 v2, 0x1

    #@cce
    sput-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->isSimChanged:Z

    #@cd0
    .line 1450
    :cond_cd0
    const-string v2, "[LGE_UICC] No SIM changed"

    #@cd2
    move-object/from16 v0, p0

    #@cd4
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@cd7
    goto/16 :goto_4b

    #@cd9
    .line 1453
    :cond_cd9
    if-nez v30, :cond_cee

    #@cdb
    .line 1454
    const-string v2, "persist.radio.iccid-changed"

    #@cdd
    const-string v4, "2"

    #@cdf
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@ce2
    .line 1455
    const-string v2, "[LGE_UICC] SIM first insert"

    #@ce4
    move-object/from16 v0, p0

    #@ce6
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@ce9
    .line 1463
    :goto_ce9
    const/4 v2, 0x1

    #@cea
    sput-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->isSimChanged:Z

    #@cec
    goto/16 :goto_4b

    #@cee
    .line 1458
    :cond_cee
    const-string v2, "persist.radio.iccid-changed"

    #@cf0
    const-string v4, "1"

    #@cf2
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@cf5
    .line 1459
    const-string v2, "[LGE_UICC] SIM Changed"

    #@cf7
    move-object/from16 v0, p0

    #@cf9
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@cfc
    goto :goto_ce9

    #@cfd
    .line 1472
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    .end local v30           #saved_iccid:Ljava/lang/String;
    :sswitch_cfd
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@d00
    move-result-object v2

    #@d01
    const-string v4, "KR"

    #@d03
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d06
    move-result v2

    #@d07
    if-nez v2, :cond_4b

    #@d09
    .line 1473
    const/16 v20, 0x1

    #@d0b
    .line 1474
    move-object/from16 v0, p1

    #@d0d
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d0f
    check-cast v9, Landroid/os/AsyncResult;

    #@d11
    .line 1475
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@d13
    check-cast v2, [B

    #@d15
    move-object v0, v2

    #@d16
    check-cast v0, [B

    #@d18
    move-object v12, v0

    #@d19
    .line 1477
    .restart local v12       #data:[B
    const-string v2, "1"

    #@d1b
    const-string v4, "persist.service.vsim.enable"

    #@d1d
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@d20
    move-result-object v4

    #@d21
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d24
    move-result v2

    #@d25
    if-eqz v2, :cond_d8f

    #@d27
    .line 1478
    const-string v2, "persist.service.vsim.gid"

    #@d29
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@d2c
    move-result-object v33

    #@d2d
    .line 1479
    .local v33, vsim_gid1:Ljava/lang/String;
    const-string v2, "GSM"

    #@d2f
    new-instance v4, Ljava/lang/StringBuilder;

    #@d31
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d34
    const-string v5, "[LGE_UICC] VSIM_GID1 : "

    #@d36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d39
    move-result-object v4

    #@d3a
    move-object/from16 v0, v33

    #@d3c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3f
    move-result-object v4

    #@d40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d43
    move-result-object v4

    #@d44
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d47
    .line 1481
    if-eqz v33, :cond_d56

    #@d49
    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->length()I

    #@d4c
    move-result v2

    #@d4d
    const/4 v4, 0x1

    #@d4e
    if-le v2, v4, :cond_d56

    #@d50
    .line 1482
    move-object/from16 v0, v33

    #@d52
    move-object/from16 v1, p0

    #@d54
    iput-object v0, v1, Lcom/android/internal/telephony/uicc/SIMRecords;->gid1:Ljava/lang/String;

    #@d56
    .line 1497
    .end local v33           #vsim_gid1:Ljava/lang/String;
    :cond_d56
    :goto_d56
    move-object/from16 v0, p0

    #@d58
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->gid1:Ljava/lang/String;

    #@d5a
    if-eqz v2, :cond_4b

    #@d5c
    .line 1498
    move-object/from16 v0, p0

    #@d5e
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->gid1:Ljava/lang/String;

    #@d60
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@d63
    move-result-object v2

    #@d64
    move-object/from16 v0, p0

    #@d66
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->gid1:Ljava/lang/String;

    #@d68
    .line 1499
    const-string v2, "gsm.sim.operator.gid"

    #@d6a
    move-object/from16 v0, p0

    #@d6c
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->gid1:Ljava/lang/String;

    #@d6e
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@d71
    .line 1500
    const-string v2, "GSM"

    #@d73
    new-instance v4, Ljava/lang/StringBuilder;

    #@d75
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d78
    const-string v5, "[UICC] GID1: "

    #@d7a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7d
    move-result-object v4

    #@d7e
    move-object/from16 v0, p0

    #@d80
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->gid1:Ljava/lang/String;

    #@d82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d85
    move-result-object v4

    #@d86
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d89
    move-result-object v4

    #@d8a
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d8d
    goto/16 :goto_4b

    #@d8f
    .line 1487
    :cond_d8f
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@d91
    if-eqz v2, :cond_dbb

    #@d93
    .line 1488
    const-string v2, "GSM"

    #@d95
    new-instance v4, Ljava/lang/StringBuilder;

    #@d97
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d9a
    const-string v5, "EVENT_GET_GID1_DONE exception = "

    #@d9c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9f
    move-result-object v4

    #@da0
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@da2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@da5
    move-result-object v4

    #@da6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da9
    move-result-object v4

    #@daa
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@dad
    .line 1489
    const/4 v2, 0x0

    #@dae
    move-object/from16 v0, p0

    #@db0
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->gid1:Ljava/lang/String;

    #@db2
    .line 1490
    const-string v2, "gsm.sim.operator.gid"

    #@db4
    const-string v4, ""

    #@db6
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@db9
    goto/16 :goto_4b

    #@dbb
    .line 1493
    :cond_dbb
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@dbe
    move-result-object v2

    #@dbf
    move-object/from16 v0, p0

    #@dc1
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->gid1:Ljava/lang/String;

    #@dc3
    .line 1494
    const-string v2, "GSM"

    #@dc5
    new-instance v4, Ljava/lang/StringBuilder;

    #@dc7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@dca
    const-string v5, "[LGE_UICC] SIM_GID1: "

    #@dcc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dcf
    move-result-object v4

    #@dd0
    move-object/from16 v0, p0

    #@dd2
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->gid1:Ljava/lang/String;

    #@dd4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd7
    move-result-object v4

    #@dd8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ddb
    move-result-object v4

    #@ddc
    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ddf
    goto/16 :goto_d56

    #@de1
    .line 1506
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_de1
    const-string v2, "GSM"

    #@de3
    const-string v4, "[pjman] SIMRecords : EVENT_GET_ACTHPLMN_DONE"

    #@de5
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@de8
    .line 1508
    const/16 v20, 0x1

    #@dea
    .line 1510
    move-object/from16 v0, p1

    #@dec
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@dee
    check-cast v9, Landroid/os/AsyncResult;

    #@df0
    .line 1511
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@df2
    if-eqz v2, :cond_e10

    #@df4
    .line 1512
    const-string v2, "GSM"

    #@df6
    new-instance v4, Ljava/lang/StringBuilder;

    #@df8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@dfb
    const-string v5, "Exception querying ACT HPLMN, Exception:"

    #@dfd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e00
    move-result-object v4

    #@e01
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@e03
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e06
    move-result-object v4

    #@e07
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0a
    move-result-object v4

    #@e0b
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e0e
    goto/16 :goto_4b

    #@e10
    .line 1516
    :cond_e10
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@e12
    check-cast v2, [B

    #@e14
    move-object v0, v2

    #@e15
    check-cast v0, [B

    #@e17
    move-object v12, v0

    #@e18
    .line 1517
    .restart local v12       #data:[B
    if-eqz v12, :cond_e1e

    #@e1a
    array-length v2, v12

    #@e1b
    const/4 v4, 0x3

    #@e1c
    if-ge v2, v4, :cond_e27

    #@e1e
    .line 1518
    :cond_e1e
    const-string v2, "GSM"

    #@e20
    const-string v4, "ACT HPLMN length error"

    #@e22
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e25
    goto/16 :goto_4b

    #@e27
    .line 1522
    :cond_e27
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bcdToStringForPlmn([B)Ljava/lang/String;

    #@e2a
    move-result-object v2

    #@e2b
    move-object/from16 v0, p0

    #@e2d
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mActHplmn:Ljava/lang/String;

    #@e2f
    .line 1524
    move-object/from16 v0, p0

    #@e31
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mActHplmn:Ljava/lang/String;

    #@e33
    if-eqz v2, :cond_e57

    #@e35
    move-object/from16 v0, p0

    #@e37
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mActHplmn:Ljava/lang/String;

    #@e39
    const/4 v4, 0x5

    #@e3a
    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    #@e3d
    move-result v2

    #@e3e
    const/16 v4, 0x46

    #@e40
    if-ne v2, v4, :cond_e57

    #@e42
    .line 1525
    const-string v2, "GSM"

    #@e44
    const-string v4, "EF_ACT_HPLMN: Strip MNC3"

    #@e46
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e49
    .line 1526
    move-object/from16 v0, p0

    #@e4b
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mActHplmn:Ljava/lang/String;

    #@e4d
    const/4 v4, 0x0

    #@e4e
    const/4 v5, 0x5

    #@e4f
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@e52
    move-result-object v2

    #@e53
    move-object/from16 v0, p0

    #@e55
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mActHplmn:Ljava/lang/String;

    #@e57
    .line 1529
    :cond_e57
    const-string v2, "GSM"

    #@e59
    new-instance v4, Ljava/lang/StringBuilder;

    #@e5b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e5e
    const-string v5, "[pjman] mActHplmn = "

    #@e60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e63
    move-result-object v4

    #@e64
    move-object/from16 v0, p0

    #@e66
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mActHplmn:Ljava/lang/String;

    #@e68
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6b
    move-result-object v4

    #@e6c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6f
    move-result-object v4

    #@e70
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e73
    .catchall {:try_start_19d .. :try_end_e73} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_19d .. :try_end_e73} :catch_55

    #@e73
    goto/16 :goto_4b

    #@e75
    .line 1536
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_e75
    const/16 v20, 0x1

    #@e77
    .line 1538
    :try_start_e77
    move-object/from16 v0, p1

    #@e79
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e7b
    check-cast v9, Landroid/os/AsyncResult;

    #@e7d
    .line 1539
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@e7f
    check-cast v2, [B

    #@e81
    move-object v0, v2

    #@e82
    check-cast v0, [B

    #@e84
    move-object v12, v0

    #@e85
    .line 1541
    .restart local v12       #data:[B
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;
    :try_end_e87
    .catchall {:try_start_e77 .. :try_end_e87} :catchall_14e8

    #@e87
    if-eqz v2, :cond_fbd

    #@e89
    .line 1569
    :try_start_e89
    move-object/from16 v0, p0

    #@e8b
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@e8d
    const/4 v4, -0x1

    #@e8e
    if-eq v2, v4, :cond_e9d

    #@e90
    move-object/from16 v0, p0

    #@e92
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@e94
    if-eqz v2, :cond_e9d

    #@e96
    move-object/from16 v0, p0

    #@e98
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@e9a
    const/4 v4, 0x2

    #@e9b
    if-ne v2, v4, :cond_ed2

    #@e9d
    :cond_e9d
    move-object/from16 v0, p0

    #@e9f
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@ea1
    if-eqz v2, :cond_ed2

    #@ea3
    move-object/from16 v0, p0

    #@ea5
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@ea7
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@eaa
    move-result v2

    #@eab
    const/4 v4, 0x6

    #@eac
    if-lt v2, v4, :cond_ed2

    #@eae
    .line 1571
    move-object/from16 v0, p0

    #@eb0
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@eb2
    const/4 v4, 0x0

    #@eb3
    const/4 v5, 0x6

    #@eb4
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@eb7
    move-result-object v26

    #@eb8
    .line 1572
    .restart local v26       #mccmncCode:Ljava/lang/String;
    sget-object v10, Lcom/android/internal/telephony/uicc/SIMRecords;->MCCMNC_CODES_HAVING_3DIGITS_MNC:[Ljava/lang/String;

    #@eba
    .restart local v10       #arr$:[Ljava/lang/String;
    array-length v0, v10

    #@ebb
    move/from16 v23, v0

    #@ebd
    .restart local v23       #len$:I
    const/16 v17, 0x0

    #@ebf
    .restart local v17       #i$:I
    :goto_ebf
    move/from16 v0, v17

    #@ec1
    move/from16 v1, v23

    #@ec3
    if-ge v0, v1, :cond_ed2

    #@ec5
    aget-object v25, v10, v17

    #@ec7
    .line 1573
    .restart local v25       #mccmnc:Ljava/lang/String;
    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@eca
    move-result v2

    #@ecb
    if-eqz v2, :cond_1d4e

    #@ecd
    .line 1574
    const/4 v2, 0x3

    #@ece
    move-object/from16 v0, p0

    #@ed0
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@ed2
    .line 1580
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_ed2
    move-object/from16 v0, p0

    #@ed4
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@ed6
    if-eqz v2, :cond_edf

    #@ed8
    move-object/from16 v0, p0

    #@eda
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@edc
    const/4 v4, -0x1

    #@edd
    if-ne v2, v4, :cond_efb

    #@edf
    .line 1581
    :cond_edf
    move-object/from16 v0, p0

    #@ee1
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;
    :try_end_ee3
    .catchall {:try_start_e89 .. :try_end_ee3} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_e89 .. :try_end_ee3} :catch_55

    #@ee3
    if-eqz v2, :cond_1d52

    #@ee5
    .line 1583
    :try_start_ee5
    move-object/from16 v0, p0

    #@ee7
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@ee9
    const/4 v4, 0x0

    #@eea
    const/4 v5, 0x3

    #@eeb
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@eee
    move-result-object v2

    #@eef
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@ef2
    move-result v24

    #@ef3
    .line 1585
    .restart local v24       #mcc:I
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@ef6
    move-result v2

    #@ef7
    move-object/from16 v0, p0

    #@ef9
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I
    :try_end_efb
    .catchall {:try_start_ee5 .. :try_end_efb} :catchall_87
    .catch Ljava/lang/NumberFormatException; {:try_start_ee5 .. :try_end_efb} :catch_1d60
    .catch Ljava/lang/RuntimeException; {:try_start_ee5 .. :try_end_efb} :catch_55

    #@efb
    .line 1597
    .end local v24           #mcc:I
    :cond_efb
    :goto_efb
    :try_start_efb
    move-object/from16 v0, p0

    #@efd
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@eff
    if-eqz v2, :cond_f8d

    #@f01
    move-object/from16 v0, p0

    #@f03
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@f05
    if-eqz v2, :cond_f8d

    #@f07
    .line 1600
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@f0a
    move-result-object v2

    #@f0b
    const-string v4, "VZW"

    #@f0d
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f10
    move-result v2

    #@f11
    if-eqz v2, :cond_f58

    #@f13
    move-object/from16 v0, p0

    #@f15
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@f17
    if-eqz v2, :cond_f58

    #@f19
    .line 1601
    move-object/from16 v0, p0

    #@f1b
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@f1d
    const/4 v4, 0x0

    #@f1e
    const/4 v5, 0x3

    #@f1f
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@f22
    move-result-object v2

    #@f23
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@f26
    move-result v24

    #@f27
    .line 1602
    .restart local v24       #mcc:I
    const/16 v2, 0xcc

    #@f29
    move/from16 v0, v24

    #@f2b
    if-ne v0, v2, :cond_f58

    #@f2d
    move-object/from16 v0, p0

    #@f2f
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@f31
    const/4 v4, 0x3

    #@f32
    if-ne v2, v4, :cond_f58

    #@f34
    .line 1604
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@f37
    move-result v2

    #@f38
    move-object/from16 v0, p0

    #@f3a
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@f3c
    .line 1605
    const-string v2, "GSM"

    #@f3e
    new-instance v4, Ljava/lang/StringBuilder;

    #@f40
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f43
    const-string v5, "[LGE UICC] SIMRecords : mncLength = "

    #@f45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f48
    move-result-object v4

    #@f49
    move-object/from16 v0, p0

    #@f4b
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@f4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f50
    move-result-object v4

    #@f51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f54
    move-result-object v4

    #@f55
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f58
    .line 1611
    .end local v24           #mcc:I
    :cond_f58
    const-string v2, "US"

    #@f5a
    const-string v4, "SPR"

    #@f5c
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@f5f
    move-result v2

    #@f60
    if-eqz v2, :cond_1d6f

    #@f62
    .line 1612
    move-object/from16 v0, p0

    #@f64
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@f66
    const/4 v4, 0x0

    #@f67
    const/4 v5, 0x6

    #@f68
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@f6b
    move-result-object v27

    #@f6c
    .line 1613
    .restart local v27       #numeric:Ljava/lang/String;
    const-string v2, "310120"

    #@f6e
    move-object/from16 v0, v27

    #@f70
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f73
    move-result v2

    #@f74
    if-nez v2, :cond_f80

    #@f76
    const-string v2, "316010"

    #@f78
    move-object/from16 v0, v27

    #@f7a
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f7d
    move-result v2

    #@f7e
    if-eqz v2, :cond_1d87

    #@f80
    .line 1614
    :cond_f80
    move-object/from16 v0, p0

    #@f82
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@f84
    const-string v4, "ro.cdma.home.operator.numeric"

    #@f86
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f89
    move-result-object v4

    #@f8a
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@f8d
    .line 1628
    .end local v27           #numeric:Ljava/lang/String;
    :cond_f8d
    :goto_f8d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f8f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f92
    const-string v4, "IccRecords.mncLenthAd = "

    #@f94
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f97
    move-result-object v2

    #@f98
    sget v4, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I

    #@f9a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9d
    move-result-object v2

    #@f9e
    const-string v4, " SIMRecords.mncLength = "

    #@fa0
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa3
    move-result-object v2

    #@fa4
    move-object/from16 v0, p0

    #@fa6
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@fa8
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fab
    move-result-object v2

    #@fac
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@faf
    move-result-object v2

    #@fb0
    move-object/from16 v0, p0

    #@fb2
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@fb5
    .line 1629
    move-object/from16 v0, p0

    #@fb7
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@fb9
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I
    :try_end_fbb
    .catchall {:try_start_efb .. :try_end_fbb} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_efb .. :try_end_fbb} :catch_55

    #@fbb
    goto/16 :goto_4b

    #@fbd
    .line 1545
    :cond_fbd
    :try_start_fbd
    new-instance v2, Ljava/lang/StringBuilder;

    #@fbf
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@fc2
    const-string v4, "EF_AD: "

    #@fc4
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc7
    move-result-object v2

    #@fc8
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@fcb
    move-result-object v4

    #@fcc
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fcf
    move-result-object v2

    #@fd0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd3
    move-result-object v2

    #@fd4
    move-object/from16 v0, p0

    #@fd6
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@fd9
    .line 1547
    array-length v2, v12

    #@fda
    const/4 v4, 0x3

    #@fdb
    if-ge v2, v4, :cond_1118

    #@fdd
    .line 1548
    const-string v2, "Corrupt AD data on SIM"

    #@fdf
    move-object/from16 v0, p0

    #@fe1
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V
    :try_end_fe4
    .catchall {:try_start_fbd .. :try_end_fe4} :catchall_14e8

    #@fe4
    .line 1569
    :try_start_fe4
    move-object/from16 v0, p0

    #@fe6
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@fe8
    const/4 v4, -0x1

    #@fe9
    if-eq v2, v4, :cond_ff8

    #@feb
    move-object/from16 v0, p0

    #@fed
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@fef
    if-eqz v2, :cond_ff8

    #@ff1
    move-object/from16 v0, p0

    #@ff3
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@ff5
    const/4 v4, 0x2

    #@ff6
    if-ne v2, v4, :cond_102d

    #@ff8
    :cond_ff8
    move-object/from16 v0, p0

    #@ffa
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@ffc
    if-eqz v2, :cond_102d

    #@ffe
    move-object/from16 v0, p0

    #@1000
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1002
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@1005
    move-result v2

    #@1006
    const/4 v4, 0x6

    #@1007
    if-lt v2, v4, :cond_102d

    #@1009
    .line 1571
    move-object/from16 v0, p0

    #@100b
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@100d
    const/4 v4, 0x0

    #@100e
    const/4 v5, 0x6

    #@100f
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1012
    move-result-object v26

    #@1013
    .line 1572
    .restart local v26       #mccmncCode:Ljava/lang/String;
    sget-object v10, Lcom/android/internal/telephony/uicc/SIMRecords;->MCCMNC_CODES_HAVING_3DIGITS_MNC:[Ljava/lang/String;

    #@1015
    .restart local v10       #arr$:[Ljava/lang/String;
    array-length v0, v10

    #@1016
    move/from16 v23, v0

    #@1018
    .restart local v23       #len$:I
    const/16 v17, 0x0

    #@101a
    .restart local v17       #i$:I
    :goto_101a
    move/from16 v0, v17

    #@101c
    move/from16 v1, v23

    #@101e
    if-ge v0, v1, :cond_102d

    #@1020
    aget-object v25, v10, v17

    #@1022
    .line 1573
    .restart local v25       #mccmnc:Ljava/lang/String;
    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1025
    move-result v2

    #@1026
    if-eqz v2, :cond_1d9f

    #@1028
    .line 1574
    const/4 v2, 0x3

    #@1029
    move-object/from16 v0, p0

    #@102b
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@102d
    .line 1580
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_102d
    move-object/from16 v0, p0

    #@102f
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1031
    if-eqz v2, :cond_103a

    #@1033
    move-object/from16 v0, p0

    #@1035
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1037
    const/4 v4, -0x1

    #@1038
    if-ne v2, v4, :cond_1056

    #@103a
    .line 1581
    :cond_103a
    move-object/from16 v0, p0

    #@103c
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;
    :try_end_103e
    .catchall {:try_start_fe4 .. :try_end_103e} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_fe4 .. :try_end_103e} :catch_55

    #@103e
    if-eqz v2, :cond_1da3

    #@1040
    .line 1583
    :try_start_1040
    move-object/from16 v0, p0

    #@1042
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1044
    const/4 v4, 0x0

    #@1045
    const/4 v5, 0x3

    #@1046
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1049
    move-result-object v2

    #@104a
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@104d
    move-result v24

    #@104e
    .line 1585
    .restart local v24       #mcc:I
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@1051
    move-result v2

    #@1052
    move-object/from16 v0, p0

    #@1054
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I
    :try_end_1056
    .catchall {:try_start_1040 .. :try_end_1056} :catchall_87
    .catch Ljava/lang/NumberFormatException; {:try_start_1040 .. :try_end_1056} :catch_1db1
    .catch Ljava/lang/RuntimeException; {:try_start_1040 .. :try_end_1056} :catch_55

    #@1056
    .line 1597
    .end local v24           #mcc:I
    :cond_1056
    :goto_1056
    :try_start_1056
    move-object/from16 v0, p0

    #@1058
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@105a
    if-eqz v2, :cond_10e8

    #@105c
    move-object/from16 v0, p0

    #@105e
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1060
    if-eqz v2, :cond_10e8

    #@1062
    .line 1600
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1065
    move-result-object v2

    #@1066
    const-string v4, "VZW"

    #@1068
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@106b
    move-result v2

    #@106c
    if-eqz v2, :cond_10b3

    #@106e
    move-object/from16 v0, p0

    #@1070
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1072
    if-eqz v2, :cond_10b3

    #@1074
    .line 1601
    move-object/from16 v0, p0

    #@1076
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1078
    const/4 v4, 0x0

    #@1079
    const/4 v5, 0x3

    #@107a
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@107d
    move-result-object v2

    #@107e
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1081
    move-result v24

    #@1082
    .line 1602
    .restart local v24       #mcc:I
    const/16 v2, 0xcc

    #@1084
    move/from16 v0, v24

    #@1086
    if-ne v0, v2, :cond_10b3

    #@1088
    move-object/from16 v0, p0

    #@108a
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@108c
    const/4 v4, 0x3

    #@108d
    if-ne v2, v4, :cond_10b3

    #@108f
    .line 1604
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@1092
    move-result v2

    #@1093
    move-object/from16 v0, p0

    #@1095
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1097
    .line 1605
    const-string v2, "GSM"

    #@1099
    new-instance v4, Ljava/lang/StringBuilder;

    #@109b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@109e
    const-string v5, "[LGE UICC] SIMRecords : mncLength = "

    #@10a0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a3
    move-result-object v4

    #@10a4
    move-object/from16 v0, p0

    #@10a6
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@10a8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10ab
    move-result-object v4

    #@10ac
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10af
    move-result-object v4

    #@10b0
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10b3
    .line 1611
    .end local v24           #mcc:I
    :cond_10b3
    const-string v2, "US"

    #@10b5
    const-string v4, "SPR"

    #@10b7
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@10ba
    move-result v2

    #@10bb
    if-eqz v2, :cond_1dc0

    #@10bd
    .line 1612
    move-object/from16 v0, p0

    #@10bf
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@10c1
    const/4 v4, 0x0

    #@10c2
    const/4 v5, 0x6

    #@10c3
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@10c6
    move-result-object v27

    #@10c7
    .line 1613
    .restart local v27       #numeric:Ljava/lang/String;
    const-string v2, "310120"

    #@10c9
    move-object/from16 v0, v27

    #@10cb
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10ce
    move-result v2

    #@10cf
    if-nez v2, :cond_10db

    #@10d1
    const-string v2, "316010"

    #@10d3
    move-object/from16 v0, v27

    #@10d5
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10d8
    move-result v2

    #@10d9
    if-eqz v2, :cond_1dd8

    #@10db
    .line 1614
    :cond_10db
    move-object/from16 v0, p0

    #@10dd
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@10df
    const-string v4, "ro.cdma.home.operator.numeric"

    #@10e1
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@10e4
    move-result-object v4

    #@10e5
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@10e8
    .line 1628
    .end local v27           #numeric:Ljava/lang/String;
    :cond_10e8
    :goto_10e8
    new-instance v2, Ljava/lang/StringBuilder;

    #@10ea
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10ed
    const-string v4, "IccRecords.mncLenthAd = "

    #@10ef
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f2
    move-result-object v2

    #@10f3
    sget v4, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I

    #@10f5
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10f8
    move-result-object v2

    #@10f9
    const-string v4, " SIMRecords.mncLength = "

    #@10fb
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10fe
    move-result-object v2

    #@10ff
    move-object/from16 v0, p0

    #@1101
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1103
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1106
    move-result-object v2

    #@1107
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110a
    move-result-object v2

    #@110b
    move-object/from16 v0, p0

    #@110d
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1110
    .line 1629
    move-object/from16 v0, p0

    #@1112
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1114
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I
    :try_end_1116
    .catchall {:try_start_1056 .. :try_end_1116} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_1056 .. :try_end_1116} :catch_55

    #@1116
    goto/16 :goto_4b

    #@1118
    .line 1552
    :cond_1118
    :try_start_1118
    array-length v2, v12

    #@1119
    const/4 v4, 0x3

    #@111a
    if-ne v2, v4, :cond_1257

    #@111c
    .line 1553
    const-string v2, "MNC length not present in EF_AD"

    #@111e
    move-object/from16 v0, p0

    #@1120
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V
    :try_end_1123
    .catchall {:try_start_1118 .. :try_end_1123} :catchall_14e8

    #@1123
    .line 1569
    :try_start_1123
    move-object/from16 v0, p0

    #@1125
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1127
    const/4 v4, -0x1

    #@1128
    if-eq v2, v4, :cond_1137

    #@112a
    move-object/from16 v0, p0

    #@112c
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@112e
    if-eqz v2, :cond_1137

    #@1130
    move-object/from16 v0, p0

    #@1132
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1134
    const/4 v4, 0x2

    #@1135
    if-ne v2, v4, :cond_116c

    #@1137
    :cond_1137
    move-object/from16 v0, p0

    #@1139
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@113b
    if-eqz v2, :cond_116c

    #@113d
    move-object/from16 v0, p0

    #@113f
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1141
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@1144
    move-result v2

    #@1145
    const/4 v4, 0x6

    #@1146
    if-lt v2, v4, :cond_116c

    #@1148
    .line 1571
    move-object/from16 v0, p0

    #@114a
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@114c
    const/4 v4, 0x0

    #@114d
    const/4 v5, 0x6

    #@114e
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1151
    move-result-object v26

    #@1152
    .line 1572
    .restart local v26       #mccmncCode:Ljava/lang/String;
    sget-object v10, Lcom/android/internal/telephony/uicc/SIMRecords;->MCCMNC_CODES_HAVING_3DIGITS_MNC:[Ljava/lang/String;

    #@1154
    .restart local v10       #arr$:[Ljava/lang/String;
    array-length v0, v10

    #@1155
    move/from16 v23, v0

    #@1157
    .restart local v23       #len$:I
    const/16 v17, 0x0

    #@1159
    .restart local v17       #i$:I
    :goto_1159
    move/from16 v0, v17

    #@115b
    move/from16 v1, v23

    #@115d
    if-ge v0, v1, :cond_116c

    #@115f
    aget-object v25, v10, v17

    #@1161
    .line 1573
    .restart local v25       #mccmnc:Ljava/lang/String;
    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1164
    move-result v2

    #@1165
    if-eqz v2, :cond_1df0

    #@1167
    .line 1574
    const/4 v2, 0x3

    #@1168
    move-object/from16 v0, p0

    #@116a
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@116c
    .line 1580
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_116c
    move-object/from16 v0, p0

    #@116e
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1170
    if-eqz v2, :cond_1179

    #@1172
    move-object/from16 v0, p0

    #@1174
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1176
    const/4 v4, -0x1

    #@1177
    if-ne v2, v4, :cond_1195

    #@1179
    .line 1581
    :cond_1179
    move-object/from16 v0, p0

    #@117b
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;
    :try_end_117d
    .catchall {:try_start_1123 .. :try_end_117d} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_1123 .. :try_end_117d} :catch_55

    #@117d
    if-eqz v2, :cond_1df4

    #@117f
    .line 1583
    :try_start_117f
    move-object/from16 v0, p0

    #@1181
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1183
    const/4 v4, 0x0

    #@1184
    const/4 v5, 0x3

    #@1185
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1188
    move-result-object v2

    #@1189
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@118c
    move-result v24

    #@118d
    .line 1585
    .restart local v24       #mcc:I
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@1190
    move-result v2

    #@1191
    move-object/from16 v0, p0

    #@1193
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I
    :try_end_1195
    .catchall {:try_start_117f .. :try_end_1195} :catchall_87
    .catch Ljava/lang/NumberFormatException; {:try_start_117f .. :try_end_1195} :catch_1e02
    .catch Ljava/lang/RuntimeException; {:try_start_117f .. :try_end_1195} :catch_55

    #@1195
    .line 1597
    .end local v24           #mcc:I
    :cond_1195
    :goto_1195
    :try_start_1195
    move-object/from16 v0, p0

    #@1197
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1199
    if-eqz v2, :cond_1227

    #@119b
    move-object/from16 v0, p0

    #@119d
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@119f
    if-eqz v2, :cond_1227

    #@11a1
    .line 1600
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@11a4
    move-result-object v2

    #@11a5
    const-string v4, "VZW"

    #@11a7
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11aa
    move-result v2

    #@11ab
    if-eqz v2, :cond_11f2

    #@11ad
    move-object/from16 v0, p0

    #@11af
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@11b1
    if-eqz v2, :cond_11f2

    #@11b3
    .line 1601
    move-object/from16 v0, p0

    #@11b5
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@11b7
    const/4 v4, 0x0

    #@11b8
    const/4 v5, 0x3

    #@11b9
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@11bc
    move-result-object v2

    #@11bd
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@11c0
    move-result v24

    #@11c1
    .line 1602
    .restart local v24       #mcc:I
    const/16 v2, 0xcc

    #@11c3
    move/from16 v0, v24

    #@11c5
    if-ne v0, v2, :cond_11f2

    #@11c7
    move-object/from16 v0, p0

    #@11c9
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@11cb
    const/4 v4, 0x3

    #@11cc
    if-ne v2, v4, :cond_11f2

    #@11ce
    .line 1604
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@11d1
    move-result v2

    #@11d2
    move-object/from16 v0, p0

    #@11d4
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@11d6
    .line 1605
    const-string v2, "GSM"

    #@11d8
    new-instance v4, Ljava/lang/StringBuilder;

    #@11da
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11dd
    const-string v5, "[LGE UICC] SIMRecords : mncLength = "

    #@11df
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e2
    move-result-object v4

    #@11e3
    move-object/from16 v0, p0

    #@11e5
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@11e7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11ea
    move-result-object v4

    #@11eb
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11ee
    move-result-object v4

    #@11ef
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11f2
    .line 1611
    .end local v24           #mcc:I
    :cond_11f2
    const-string v2, "US"

    #@11f4
    const-string v4, "SPR"

    #@11f6
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@11f9
    move-result v2

    #@11fa
    if-eqz v2, :cond_1e11

    #@11fc
    .line 1612
    move-object/from16 v0, p0

    #@11fe
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1200
    const/4 v4, 0x0

    #@1201
    const/4 v5, 0x6

    #@1202
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1205
    move-result-object v27

    #@1206
    .line 1613
    .restart local v27       #numeric:Ljava/lang/String;
    const-string v2, "310120"

    #@1208
    move-object/from16 v0, v27

    #@120a
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@120d
    move-result v2

    #@120e
    if-nez v2, :cond_121a

    #@1210
    const-string v2, "316010"

    #@1212
    move-object/from16 v0, v27

    #@1214
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1217
    move-result v2

    #@1218
    if-eqz v2, :cond_1e29

    #@121a
    .line 1614
    :cond_121a
    move-object/from16 v0, p0

    #@121c
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@121e
    const-string v4, "ro.cdma.home.operator.numeric"

    #@1220
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1223
    move-result-object v4

    #@1224
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1227
    .line 1628
    .end local v27           #numeric:Ljava/lang/String;
    :cond_1227
    :goto_1227
    new-instance v2, Ljava/lang/StringBuilder;

    #@1229
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@122c
    const-string v4, "IccRecords.mncLenthAd = "

    #@122e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1231
    move-result-object v2

    #@1232
    sget v4, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I

    #@1234
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1237
    move-result-object v2

    #@1238
    const-string v4, " SIMRecords.mncLength = "

    #@123a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123d
    move-result-object v2

    #@123e
    move-object/from16 v0, p0

    #@1240
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1242
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1245
    move-result-object v2

    #@1246
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1249
    move-result-object v2

    #@124a
    move-object/from16 v0, p0

    #@124c
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@124f
    .line 1629
    move-object/from16 v0, p0

    #@1251
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1253
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I
    :try_end_1255
    .catchall {:try_start_1195 .. :try_end_1255} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_1195 .. :try_end_1255} :catch_55

    #@1255
    goto/16 :goto_4b

    #@1257
    .line 1557
    :cond_1257
    :try_start_1257
    const-string v2, "1"

    #@1259
    const-string v4, "persist.service.vsim.enable"

    #@125b
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@125e
    move-result-object v4

    #@125f
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1262
    move-result v2

    #@1263
    if-eqz v2, :cond_139e

    #@1265
    .line 1558
    const/4 v2, 0x0

    #@1266
    move-object/from16 v0, p0

    #@1268
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I
    :try_end_126a
    .catchall {:try_start_1257 .. :try_end_126a} :catchall_14e8

    #@126a
    .line 1569
    :try_start_126a
    move-object/from16 v0, p0

    #@126c
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@126e
    const/4 v4, -0x1

    #@126f
    if-eq v2, v4, :cond_127e

    #@1271
    move-object/from16 v0, p0

    #@1273
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1275
    if-eqz v2, :cond_127e

    #@1277
    move-object/from16 v0, p0

    #@1279
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@127b
    const/4 v4, 0x2

    #@127c
    if-ne v2, v4, :cond_12b3

    #@127e
    :cond_127e
    move-object/from16 v0, p0

    #@1280
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1282
    if-eqz v2, :cond_12b3

    #@1284
    move-object/from16 v0, p0

    #@1286
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1288
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@128b
    move-result v2

    #@128c
    const/4 v4, 0x6

    #@128d
    if-lt v2, v4, :cond_12b3

    #@128f
    .line 1571
    move-object/from16 v0, p0

    #@1291
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1293
    const/4 v4, 0x0

    #@1294
    const/4 v5, 0x6

    #@1295
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1298
    move-result-object v26

    #@1299
    .line 1572
    .restart local v26       #mccmncCode:Ljava/lang/String;
    sget-object v10, Lcom/android/internal/telephony/uicc/SIMRecords;->MCCMNC_CODES_HAVING_3DIGITS_MNC:[Ljava/lang/String;

    #@129b
    .restart local v10       #arr$:[Ljava/lang/String;
    array-length v0, v10

    #@129c
    move/from16 v23, v0

    #@129e
    .restart local v23       #len$:I
    const/16 v17, 0x0

    #@12a0
    .restart local v17       #i$:I
    :goto_12a0
    move/from16 v0, v17

    #@12a2
    move/from16 v1, v23

    #@12a4
    if-ge v0, v1, :cond_12b3

    #@12a6
    aget-object v25, v10, v17

    #@12a8
    .line 1573
    .restart local v25       #mccmnc:Ljava/lang/String;
    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12ab
    move-result v2

    #@12ac
    if-eqz v2, :cond_1e41

    #@12ae
    .line 1574
    const/4 v2, 0x3

    #@12af
    move-object/from16 v0, p0

    #@12b1
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@12b3
    .line 1580
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_12b3
    move-object/from16 v0, p0

    #@12b5
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@12b7
    if-eqz v2, :cond_12c0

    #@12b9
    move-object/from16 v0, p0

    #@12bb
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@12bd
    const/4 v4, -0x1

    #@12be
    if-ne v2, v4, :cond_12dc

    #@12c0
    .line 1581
    :cond_12c0
    move-object/from16 v0, p0

    #@12c2
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;
    :try_end_12c4
    .catchall {:try_start_126a .. :try_end_12c4} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_126a .. :try_end_12c4} :catch_55

    #@12c4
    if-eqz v2, :cond_1e45

    #@12c6
    .line 1583
    :try_start_12c6
    move-object/from16 v0, p0

    #@12c8
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@12ca
    const/4 v4, 0x0

    #@12cb
    const/4 v5, 0x3

    #@12cc
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@12cf
    move-result-object v2

    #@12d0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@12d3
    move-result v24

    #@12d4
    .line 1585
    .restart local v24       #mcc:I
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@12d7
    move-result v2

    #@12d8
    move-object/from16 v0, p0

    #@12da
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I
    :try_end_12dc
    .catchall {:try_start_12c6 .. :try_end_12dc} :catchall_87
    .catch Ljava/lang/NumberFormatException; {:try_start_12c6 .. :try_end_12dc} :catch_1e53
    .catch Ljava/lang/RuntimeException; {:try_start_12c6 .. :try_end_12dc} :catch_55

    #@12dc
    .line 1597
    .end local v24           #mcc:I
    :cond_12dc
    :goto_12dc
    :try_start_12dc
    move-object/from16 v0, p0

    #@12de
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@12e0
    if-eqz v2, :cond_136e

    #@12e2
    move-object/from16 v0, p0

    #@12e4
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@12e6
    if-eqz v2, :cond_136e

    #@12e8
    .line 1600
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@12eb
    move-result-object v2

    #@12ec
    const-string v4, "VZW"

    #@12ee
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12f1
    move-result v2

    #@12f2
    if-eqz v2, :cond_1339

    #@12f4
    move-object/from16 v0, p0

    #@12f6
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@12f8
    if-eqz v2, :cond_1339

    #@12fa
    .line 1601
    move-object/from16 v0, p0

    #@12fc
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@12fe
    const/4 v4, 0x0

    #@12ff
    const/4 v5, 0x3

    #@1300
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1303
    move-result-object v2

    #@1304
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1307
    move-result v24

    #@1308
    .line 1602
    .restart local v24       #mcc:I
    const/16 v2, 0xcc

    #@130a
    move/from16 v0, v24

    #@130c
    if-ne v0, v2, :cond_1339

    #@130e
    move-object/from16 v0, p0

    #@1310
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1312
    const/4 v4, 0x3

    #@1313
    if-ne v2, v4, :cond_1339

    #@1315
    .line 1604
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@1318
    move-result v2

    #@1319
    move-object/from16 v0, p0

    #@131b
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@131d
    .line 1605
    const-string v2, "GSM"

    #@131f
    new-instance v4, Ljava/lang/StringBuilder;

    #@1321
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1324
    const-string v5, "[LGE UICC] SIMRecords : mncLength = "

    #@1326
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1329
    move-result-object v4

    #@132a
    move-object/from16 v0, p0

    #@132c
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@132e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1331
    move-result-object v4

    #@1332
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1335
    move-result-object v4

    #@1336
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1339
    .line 1611
    .end local v24           #mcc:I
    :cond_1339
    const-string v2, "US"

    #@133b
    const-string v4, "SPR"

    #@133d
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1340
    move-result v2

    #@1341
    if-eqz v2, :cond_1e62

    #@1343
    .line 1612
    move-object/from16 v0, p0

    #@1345
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1347
    const/4 v4, 0x0

    #@1348
    const/4 v5, 0x6

    #@1349
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@134c
    move-result-object v27

    #@134d
    .line 1613
    .restart local v27       #numeric:Ljava/lang/String;
    const-string v2, "310120"

    #@134f
    move-object/from16 v0, v27

    #@1351
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1354
    move-result v2

    #@1355
    if-nez v2, :cond_1361

    #@1357
    const-string v2, "316010"

    #@1359
    move-object/from16 v0, v27

    #@135b
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@135e
    move-result v2

    #@135f
    if-eqz v2, :cond_1e7a

    #@1361
    .line 1614
    :cond_1361
    move-object/from16 v0, p0

    #@1363
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1365
    const-string v4, "ro.cdma.home.operator.numeric"

    #@1367
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@136a
    move-result-object v4

    #@136b
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@136e
    .line 1628
    .end local v27           #numeric:Ljava/lang/String;
    :cond_136e
    :goto_136e
    new-instance v2, Ljava/lang/StringBuilder;

    #@1370
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1373
    const-string v4, "IccRecords.mncLenthAd = "

    #@1375
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1378
    move-result-object v2

    #@1379
    sget v4, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I

    #@137b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@137e
    move-result-object v2

    #@137f
    const-string v4, " SIMRecords.mncLength = "

    #@1381
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1384
    move-result-object v2

    #@1385
    move-object/from16 v0, p0

    #@1387
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1389
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@138c
    move-result-object v2

    #@138d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1390
    move-result-object v2

    #@1391
    move-object/from16 v0, p0

    #@1393
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1396
    .line 1629
    move-object/from16 v0, p0

    #@1398
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@139a
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I
    :try_end_139c
    .catchall {:try_start_12dc .. :try_end_139c} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_12dc .. :try_end_139c} :catch_55

    #@139c
    goto/16 :goto_4b

    #@139e
    .line 1563
    :cond_139e
    const/4 v2, 0x3

    #@139f
    :try_start_139f
    aget-byte v2, v12, v2

    #@13a1
    and-int/lit8 v2, v2, 0xf

    #@13a3
    move-object/from16 v0, p0

    #@13a5
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@13a7
    .line 1565
    move-object/from16 v0, p0

    #@13a9
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@13ab
    const/16 v4, 0xf

    #@13ad
    if-ne v2, v4, :cond_13b4

    #@13af
    .line 1566
    const/4 v2, 0x0

    #@13b0
    move-object/from16 v0, p0

    #@13b2
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I
    :try_end_13b4
    .catchall {:try_start_139f .. :try_end_13b4} :catchall_14e8

    #@13b4
    .line 1569
    :cond_13b4
    :try_start_13b4
    move-object/from16 v0, p0

    #@13b6
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@13b8
    const/4 v4, -0x1

    #@13b9
    if-eq v2, v4, :cond_13c8

    #@13bb
    move-object/from16 v0, p0

    #@13bd
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@13bf
    if-eqz v2, :cond_13c8

    #@13c1
    move-object/from16 v0, p0

    #@13c3
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@13c5
    const/4 v4, 0x2

    #@13c6
    if-ne v2, v4, :cond_13fd

    #@13c8
    :cond_13c8
    move-object/from16 v0, p0

    #@13ca
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@13cc
    if-eqz v2, :cond_13fd

    #@13ce
    move-object/from16 v0, p0

    #@13d0
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@13d2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@13d5
    move-result v2

    #@13d6
    const/4 v4, 0x6

    #@13d7
    if-lt v2, v4, :cond_13fd

    #@13d9
    .line 1571
    move-object/from16 v0, p0

    #@13db
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@13dd
    const/4 v4, 0x0

    #@13de
    const/4 v5, 0x6

    #@13df
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@13e2
    move-result-object v26

    #@13e3
    .line 1572
    .restart local v26       #mccmncCode:Ljava/lang/String;
    sget-object v10, Lcom/android/internal/telephony/uicc/SIMRecords;->MCCMNC_CODES_HAVING_3DIGITS_MNC:[Ljava/lang/String;

    #@13e5
    .restart local v10       #arr$:[Ljava/lang/String;
    array-length v0, v10

    #@13e6
    move/from16 v23, v0

    #@13e8
    .restart local v23       #len$:I
    const/16 v17, 0x0

    #@13ea
    .restart local v17       #i$:I
    :goto_13ea
    move/from16 v0, v17

    #@13ec
    move/from16 v1, v23

    #@13ee
    if-ge v0, v1, :cond_13fd

    #@13f0
    aget-object v25, v10, v17

    #@13f2
    .line 1573
    .restart local v25       #mccmnc:Ljava/lang/String;
    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13f5
    move-result v2

    #@13f6
    if-eqz v2, :cond_1e92

    #@13f8
    .line 1574
    const/4 v2, 0x3

    #@13f9
    move-object/from16 v0, p0

    #@13fb
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@13fd
    .line 1580
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_13fd
    move-object/from16 v0, p0

    #@13ff
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1401
    if-eqz v2, :cond_140a

    #@1403
    move-object/from16 v0, p0

    #@1405
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1407
    const/4 v4, -0x1

    #@1408
    if-ne v2, v4, :cond_1426

    #@140a
    .line 1581
    :cond_140a
    move-object/from16 v0, p0

    #@140c
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;
    :try_end_140e
    .catchall {:try_start_13b4 .. :try_end_140e} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_13b4 .. :try_end_140e} :catch_55

    #@140e
    if-eqz v2, :cond_1e96

    #@1410
    .line 1583
    :try_start_1410
    move-object/from16 v0, p0

    #@1412
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1414
    const/4 v4, 0x0

    #@1415
    const/4 v5, 0x3

    #@1416
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1419
    move-result-object v2

    #@141a
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@141d
    move-result v24

    #@141e
    .line 1585
    .restart local v24       #mcc:I
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@1421
    move-result v2

    #@1422
    move-object/from16 v0, p0

    #@1424
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I
    :try_end_1426
    .catchall {:try_start_1410 .. :try_end_1426} :catchall_87
    .catch Ljava/lang/NumberFormatException; {:try_start_1410 .. :try_end_1426} :catch_1ea4
    .catch Ljava/lang/RuntimeException; {:try_start_1410 .. :try_end_1426} :catch_55

    #@1426
    .line 1597
    .end local v24           #mcc:I
    :cond_1426
    :goto_1426
    :try_start_1426
    move-object/from16 v0, p0

    #@1428
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@142a
    if-eqz v2, :cond_14b8

    #@142c
    move-object/from16 v0, p0

    #@142e
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1430
    if-eqz v2, :cond_14b8

    #@1432
    .line 1600
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1435
    move-result-object v2

    #@1436
    const-string v4, "VZW"

    #@1438
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@143b
    move-result v2

    #@143c
    if-eqz v2, :cond_1483

    #@143e
    move-object/from16 v0, p0

    #@1440
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1442
    if-eqz v2, :cond_1483

    #@1444
    .line 1601
    move-object/from16 v0, p0

    #@1446
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1448
    const/4 v4, 0x0

    #@1449
    const/4 v5, 0x3

    #@144a
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@144d
    move-result-object v2

    #@144e
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1451
    move-result v24

    #@1452
    .line 1602
    .restart local v24       #mcc:I
    const/16 v2, 0xcc

    #@1454
    move/from16 v0, v24

    #@1456
    if-ne v0, v2, :cond_1483

    #@1458
    move-object/from16 v0, p0

    #@145a
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@145c
    const/4 v4, 0x3

    #@145d
    if-ne v2, v4, :cond_1483

    #@145f
    .line 1604
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@1462
    move-result v2

    #@1463
    move-object/from16 v0, p0

    #@1465
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1467
    .line 1605
    const-string v2, "GSM"

    #@1469
    new-instance v4, Ljava/lang/StringBuilder;

    #@146b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@146e
    const-string v5, "[LGE UICC] SIMRecords : mncLength = "

    #@1470
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1473
    move-result-object v4

    #@1474
    move-object/from16 v0, p0

    #@1476
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1478
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@147b
    move-result-object v4

    #@147c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@147f
    move-result-object v4

    #@1480
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1483
    .line 1611
    .end local v24           #mcc:I
    :cond_1483
    const-string v2, "US"

    #@1485
    const-string v4, "SPR"

    #@1487
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@148a
    move-result v2

    #@148b
    if-eqz v2, :cond_1eb3

    #@148d
    .line 1612
    move-object/from16 v0, p0

    #@148f
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1491
    const/4 v4, 0x0

    #@1492
    const/4 v5, 0x6

    #@1493
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1496
    move-result-object v27

    #@1497
    .line 1613
    .restart local v27       #numeric:Ljava/lang/String;
    const-string v2, "310120"

    #@1499
    move-object/from16 v0, v27

    #@149b
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@149e
    move-result v2

    #@149f
    if-nez v2, :cond_14ab

    #@14a1
    const-string v2, "316010"

    #@14a3
    move-object/from16 v0, v27

    #@14a5
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14a8
    move-result v2

    #@14a9
    if-eqz v2, :cond_1ecb

    #@14ab
    .line 1614
    :cond_14ab
    move-object/from16 v0, p0

    #@14ad
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@14af
    const-string v4, "ro.cdma.home.operator.numeric"

    #@14b1
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@14b4
    move-result-object v4

    #@14b5
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@14b8
    .line 1628
    .end local v27           #numeric:Ljava/lang/String;
    :cond_14b8
    :goto_14b8
    new-instance v2, Ljava/lang/StringBuilder;

    #@14ba
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14bd
    const-string v4, "IccRecords.mncLenthAd = "

    #@14bf
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c2
    move-result-object v2

    #@14c3
    sget v4, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I

    #@14c5
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14c8
    move-result-object v2

    #@14c9
    const-string v4, " SIMRecords.mncLength = "

    #@14cb
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14ce
    move-result-object v2

    #@14cf
    move-object/from16 v0, p0

    #@14d1
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@14d3
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14d6
    move-result-object v2

    #@14d7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14da
    move-result-object v2

    #@14db
    move-object/from16 v0, p0

    #@14dd
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@14e0
    .line 1629
    move-object/from16 v0, p0

    #@14e2
    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@14e4
    sput v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I

    #@14e6
    goto/16 :goto_4b

    #@14e8
    .line 1569
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :catchall_14e8
    move-exception v2

    #@14e9
    move-object/from16 v0, p0

    #@14eb
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@14ed
    const/4 v5, -0x1

    #@14ee
    if-eq v4, v5, :cond_14fd

    #@14f0
    move-object/from16 v0, p0

    #@14f2
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@14f4
    if-eqz v4, :cond_14fd

    #@14f6
    move-object/from16 v0, p0

    #@14f8
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@14fa
    const/4 v5, 0x2

    #@14fb
    if-ne v4, v5, :cond_1532

    #@14fd
    :cond_14fd
    move-object/from16 v0, p0

    #@14ff
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1501
    if-eqz v4, :cond_1532

    #@1503
    move-object/from16 v0, p0

    #@1505
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1507
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@150a
    move-result v4

    #@150b
    const/4 v5, 0x6

    #@150c
    if-lt v4, v5, :cond_1532

    #@150e
    .line 1571
    move-object/from16 v0, p0

    #@1510
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1512
    const/4 v5, 0x0

    #@1513
    const/4 v6, 0x6

    #@1514
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1517
    move-result-object v26

    #@1518
    .line 1572
    .restart local v26       #mccmncCode:Ljava/lang/String;
    sget-object v10, Lcom/android/internal/telephony/uicc/SIMRecords;->MCCMNC_CODES_HAVING_3DIGITS_MNC:[Ljava/lang/String;

    #@151a
    .restart local v10       #arr$:[Ljava/lang/String;
    array-length v0, v10

    #@151b
    move/from16 v23, v0

    #@151d
    .restart local v23       #len$:I
    const/16 v17, 0x0

    #@151f
    .restart local v17       #i$:I
    :goto_151f
    move/from16 v0, v17

    #@1521
    move/from16 v1, v23

    #@1523
    if-ge v0, v1, :cond_1532

    #@1525
    aget-object v25, v10, v17

    #@1527
    .line 1573
    .restart local v25       #mccmnc:Ljava/lang/String;
    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@152a
    move-result v4

    #@152b
    if-eqz v4, :cond_1cfd

    #@152d
    .line 1574
    const/4 v4, 0x3

    #@152e
    move-object/from16 v0, p0

    #@1530
    iput v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1532
    .line 1580
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_1532
    move-object/from16 v0, p0

    #@1534
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1536
    if-eqz v4, :cond_153f

    #@1538
    move-object/from16 v0, p0

    #@153a
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@153c
    const/4 v5, -0x1

    #@153d
    if-ne v4, v5, :cond_155b

    #@153f
    .line 1581
    :cond_153f
    move-object/from16 v0, p0

    #@1541
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;
    :try_end_1543
    .catchall {:try_start_1426 .. :try_end_1543} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_1426 .. :try_end_1543} :catch_55

    #@1543
    if-eqz v4, :cond_1d01

    #@1545
    .line 1583
    :try_start_1545
    move-object/from16 v0, p0

    #@1547
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1549
    const/4 v5, 0x0

    #@154a
    const/4 v6, 0x3

    #@154b
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@154e
    move-result-object v4

    #@154f
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1552
    move-result v24

    #@1553
    .line 1585
    .restart local v24       #mcc:I
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@1556
    move-result v4

    #@1557
    move-object/from16 v0, p0

    #@1559
    iput v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I
    :try_end_155b
    .catchall {:try_start_1545 .. :try_end_155b} :catchall_87
    .catch Ljava/lang/NumberFormatException; {:try_start_1545 .. :try_end_155b} :catch_1d0f
    .catch Ljava/lang/RuntimeException; {:try_start_1545 .. :try_end_155b} :catch_55

    #@155b
    .line 1597
    .end local v24           #mcc:I
    :cond_155b
    :goto_155b
    :try_start_155b
    move-object/from16 v0, p0

    #@155d
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@155f
    if-eqz v4, :cond_15ed

    #@1561
    move-object/from16 v0, p0

    #@1563
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1565
    if-eqz v4, :cond_15ed

    #@1567
    .line 1600
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@156a
    move-result-object v4

    #@156b
    const-string v5, "VZW"

    #@156d
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1570
    move-result v4

    #@1571
    if-eqz v4, :cond_15b8

    #@1573
    move-object/from16 v0, p0

    #@1575
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1577
    if-eqz v4, :cond_15b8

    #@1579
    .line 1601
    move-object/from16 v0, p0

    #@157b
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@157d
    const/4 v5, 0x0

    #@157e
    const/4 v6, 0x3

    #@157f
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1582
    move-result-object v4

    #@1583
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1586
    move-result v24

    #@1587
    .line 1602
    .restart local v24       #mcc:I
    const/16 v4, 0xcc

    #@1589
    move/from16 v0, v24

    #@158b
    if-ne v0, v4, :cond_15b8

    #@158d
    move-object/from16 v0, p0

    #@158f
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1591
    const/4 v5, 0x3

    #@1592
    if-ne v4, v5, :cond_15b8

    #@1594
    .line 1604
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@1597
    move-result v4

    #@1598
    move-object/from16 v0, p0

    #@159a
    iput v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@159c
    .line 1605
    const-string v4, "GSM"

    #@159e
    new-instance v5, Ljava/lang/StringBuilder;

    #@15a0
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@15a3
    const-string v6, "[LGE UICC] SIMRecords : mncLength = "

    #@15a5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a8
    move-result-object v5

    #@15a9
    move-object/from16 v0, p0

    #@15ab
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@15ad
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15b0
    move-result-object v5

    #@15b1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15b4
    move-result-object v5

    #@15b5
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15b8
    .line 1611
    .end local v24           #mcc:I
    :cond_15b8
    const-string v4, "US"

    #@15ba
    const-string v5, "SPR"

    #@15bc
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@15bf
    move-result v4

    #@15c0
    if-eqz v4, :cond_1d1e

    #@15c2
    .line 1612
    move-object/from16 v0, p0

    #@15c4
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@15c6
    const/4 v5, 0x0

    #@15c7
    const/4 v6, 0x6

    #@15c8
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@15cb
    move-result-object v27

    #@15cc
    .line 1613
    .restart local v27       #numeric:Ljava/lang/String;
    const-string v4, "310120"

    #@15ce
    move-object/from16 v0, v27

    #@15d0
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15d3
    move-result v4

    #@15d4
    if-nez v4, :cond_15e0

    #@15d6
    const-string v4, "316010"

    #@15d8
    move-object/from16 v0, v27

    #@15da
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15dd
    move-result v4

    #@15de
    if-eqz v4, :cond_1d36

    #@15e0
    .line 1614
    :cond_15e0
    move-object/from16 v0, p0

    #@15e2
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@15e4
    const-string v5, "ro.cdma.home.operator.numeric"

    #@15e6
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@15e9
    move-result-object v5

    #@15ea
    invoke-static {v4, v5}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@15ed
    .line 1628
    .end local v27           #numeric:Ljava/lang/String;
    :cond_15ed
    :goto_15ed
    new-instance v4, Ljava/lang/StringBuilder;

    #@15ef
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15f2
    const-string v5, "IccRecords.mncLenthAd = "

    #@15f4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f7
    move-result-object v4

    #@15f8
    sget v5, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I

    #@15fa
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15fd
    move-result-object v4

    #@15fe
    const-string v5, " SIMRecords.mncLength = "

    #@1600
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1603
    move-result-object v4

    #@1604
    move-object/from16 v0, p0

    #@1606
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1608
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@160b
    move-result-object v4

    #@160c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160f
    move-result-object v4

    #@1610
    move-object/from16 v0, p0

    #@1612
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1615
    .line 1629
    move-object/from16 v0, p0

    #@1617
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1619
    sput v4, Lcom/android/internal/telephony/uicc/SIMRecords;->mncLengthAd:I

    #@161b
    .line 1569
    throw v2

    #@161c
    .line 1635
    :sswitch_161c
    const/16 v20, 0x1

    #@161e
    .line 1636
    move-object/from16 v0, p1

    #@1620
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1622
    check-cast v9, Landroid/os/AsyncResult;

    #@1624
    .line 1637
    .restart local v9       #ar:Landroid/os/AsyncResult;
    const/4 v2, 0x0

    #@1625
    move-object/from16 v0, p0

    #@1627
    invoke-direct {v0, v2, v9}, Lcom/android/internal/telephony/uicc/SIMRecords;->getSpnFsm(ZLandroid/os/AsyncResult;)V

    #@162a
    goto/16 :goto_4b

    #@162c
    .line 1641
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_162c
    const/16 v20, 0x1

    #@162e
    .line 1643
    move-object/from16 v0, p1

    #@1630
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1632
    check-cast v9, Landroid/os/AsyncResult;

    #@1634
    .line 1644
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1636
    check-cast v2, [B

    #@1638
    move-object v0, v2

    #@1639
    check-cast v0, [B

    #@163b
    move-object v12, v0

    #@163c
    .line 1646
    .restart local v12       #data:[B
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@163e
    if-nez v2, :cond_4b

    #@1640
    .line 1650
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@1642
    if-eqz v2, :cond_1660

    #@1644
    new-instance v2, Ljava/lang/StringBuilder;

    #@1646
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1649
    const-string v4, "EF_CFF_CPHS: "

    #@164b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164e
    move-result-object v2

    #@164f
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@1652
    move-result-object v4

    #@1653
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1656
    move-result-object v2

    #@1657
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@165a
    move-result-object v2

    #@165b
    move-object/from16 v0, p0

    #@165d
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1660
    .line 1651
    :cond_1660
    move-object/from16 v0, p0

    #@1662
    iput-object v12, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCff:[B

    #@1664
    .line 1653
    move-object/from16 v0, p0

    #@1666
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@1668
    move-object/from16 v0, p0

    #@166a
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->validEfCfis([B)Z

    #@166d
    move-result v2

    #@166e
    if-eqz v2, :cond_168e

    #@1670
    .line 1654
    const/4 v2, 0x0

    #@1671
    aget-byte v2, v12, v2

    #@1673
    and-int/lit8 v2, v2, 0xf

    #@1675
    const/16 v4, 0xa

    #@1677
    if-ne v2, v4, :cond_168c

    #@1679
    const/4 v2, 0x1

    #@167a
    :goto_167a
    move-object/from16 v0, p0

    #@167c
    iput-boolean v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->callForwardingEnabled:Z

    #@167e
    .line 1657
    move-object/from16 v0, p0

    #@1680
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mRecordsEventsRegistrants:Landroid/os/RegistrantList;

    #@1682
    const/4 v4, 0x1

    #@1683
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1686
    move-result-object v4

    #@1687
    invoke-virtual {v2, v4}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@168a
    goto/16 :goto_4b

    #@168c
    .line 1654
    :cond_168c
    const/4 v2, 0x0

    #@168d
    goto :goto_167a

    #@168e
    .line 1659
    :cond_168e
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@1690
    if-eqz v2, :cond_4b

    #@1692
    new-instance v2, Ljava/lang/StringBuilder;

    #@1694
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1697
    const-string v4, "EVENT_GET_CFF_DONE: invalid mEfCfis="

    #@1699
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@169c
    move-result-object v2

    #@169d
    move-object/from16 v0, p0

    #@169f
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@16a1
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@16a4
    move-result-object v4

    #@16a5
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a8
    move-result-object v2

    #@16a9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16ac
    move-result-object v2

    #@16ad
    move-object/from16 v0, p0

    #@16af
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@16b2
    goto/16 :goto_4b

    #@16b4
    .line 1665
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_16b4
    const/16 v20, 0x1

    #@16b6
    .line 1667
    move-object/from16 v0, p1

    #@16b8
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16ba
    check-cast v9, Landroid/os/AsyncResult;

    #@16bc
    .line 1668
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@16be
    check-cast v2, [B

    #@16c0
    move-object v0, v2

    #@16c1
    check-cast v0, [B

    #@16c3
    move-object v12, v0

    #@16c4
    .line 1670
    .restart local v12       #data:[B
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@16c6
    if-nez v2, :cond_4b

    #@16c8
    .line 1674
    move-object/from16 v0, p0

    #@16ca
    invoke-direct {v0, v12}, Lcom/android/internal/telephony/uicc/SIMRecords;->parseEfSpdi([B)V

    #@16cd
    goto/16 :goto_4b

    #@16cf
    .line 1678
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_16cf
    move-object/from16 v0, p1

    #@16d1
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16d3
    check-cast v9, Landroid/os/AsyncResult;

    #@16d5
    .line 1679
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@16d7
    if-eqz v2, :cond_4b

    #@16d9
    .line 1680
    const-string v2, "update failed. "

    #@16db
    iget-object v4, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@16dd
    move-object/from16 v0, p0

    #@16df
    invoke-virtual {v0, v2, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->logw(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@16e2
    goto/16 :goto_4b

    #@16e4
    .line 1685
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_16e4
    const/16 v20, 0x1

    #@16e6
    .line 1687
    move-object/from16 v0, p1

    #@16e8
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16ea
    check-cast v9, Landroid/os/AsyncResult;

    #@16ec
    .line 1688
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@16ee
    check-cast v2, [B

    #@16f0
    move-object v0, v2

    #@16f1
    check-cast v0, [B

    #@16f3
    move-object v12, v0

    #@16f4
    .line 1690
    .restart local v12       #data:[B
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@16f6
    if-nez v2, :cond_4b

    #@16f8
    .line 1694
    new-instance v32, Lcom/android/internal/telephony/gsm/SimTlv;

    #@16fa
    const/4 v2, 0x0

    #@16fb
    array-length v4, v12

    #@16fc
    move-object/from16 v0, v32

    #@16fe
    invoke-direct {v0, v12, v2, v4}, Lcom/android/internal/telephony/gsm/SimTlv;-><init>([BII)V

    #@1701
    .line 1696
    .local v32, tlv:Lcom/android/internal/telephony/gsm/SimTlv;
    :goto_1701
    invoke-virtual/range {v32 .. v32}, Lcom/android/internal/telephony/gsm/SimTlv;->isValidObject()Z

    #@1704
    move-result v2

    #@1705
    if-eqz v2, :cond_4b

    #@1707
    .line 1697
    invoke-virtual/range {v32 .. v32}, Lcom/android/internal/telephony/gsm/SimTlv;->getTag()I

    #@170a
    move-result v2

    #@170b
    const/16 v4, 0x43

    #@170d
    if-ne v2, v4, :cond_1723

    #@170f
    .line 1698
    invoke-virtual/range {v32 .. v32}, Lcom/android/internal/telephony/gsm/SimTlv;->getData()[B

    #@1712
    move-result-object v2

    #@1713
    const/4 v4, 0x0

    #@1714
    invoke-virtual/range {v32 .. v32}, Lcom/android/internal/telephony/gsm/SimTlv;->getData()[B

    #@1717
    move-result-object v5

    #@1718
    array-length v5, v5

    #@1719
    invoke-static {v2, v4, v5}, Lcom/android/internal/telephony/uicc/IccUtils;->networkNameToString([BII)Ljava/lang/String;

    #@171c
    move-result-object v2

    #@171d
    move-object/from16 v0, p0

    #@171f
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->pnnHomeName:Ljava/lang/String;

    #@1721
    goto/16 :goto_4b

    #@1723
    .line 1696
    :cond_1723
    invoke-virtual/range {v32 .. v32}, Lcom/android/internal/telephony/gsm/SimTlv;->nextObject()Z

    #@1726
    goto :goto_1701

    #@1727
    .line 1711
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    .end local v32           #tlv:Lcom/android/internal/telephony/gsm/SimTlv;
    :sswitch_1727
    const/16 v20, 0x0

    #@1729
    .line 1713
    move-object/from16 v0, p1

    #@172b
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@172d
    check-cast v9, Landroid/os/AsyncResult;

    #@172f
    .line 1714
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1731
    if-nez v2, :cond_4b

    #@1733
    .line 1718
    const/4 v2, 0x0

    #@1734
    const-string v4, "seperate_processing_sms_uicc"

    #@1736
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1739
    move-result v2

    #@173a
    if-eqz v2, :cond_1747

    #@173c
    .line 1719
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@173e
    check-cast v2, Ljava/util/ArrayList;

    #@1740
    move-object/from16 v0, p0

    #@1742
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->handleUSimSmses(Ljava/util/ArrayList;)V

    #@1745
    goto/16 :goto_4b

    #@1747
    .line 1723
    :cond_1747
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1749
    check-cast v2, Ljava/util/ArrayList;

    #@174b
    move-object/from16 v0, p0

    #@174d
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->handleSmses(Ljava/util/ArrayList;)V

    #@1750
    goto/16 :goto_4b

    #@1752
    .line 1728
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_1752
    const-string v2, "ENF"

    #@1754
    new-instance v4, Ljava/lang/StringBuilder;

    #@1756
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1759
    const-string v5, "marked read: sms "

    #@175b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@175e
    move-result-object v4

    #@175f
    move-object/from16 v0, p1

    #@1761
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@1763
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1766
    move-result-object v4

    #@1767
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@176a
    move-result-object v4

    #@176b
    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@176e
    goto/16 :goto_4b

    #@1770
    .line 1732
    :sswitch_1770
    const/16 v20, 0x0

    #@1772
    .line 1734
    move-object/from16 v0, p1

    #@1774
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1776
    check-cast v9, Landroid/os/AsyncResult;

    #@1778
    .line 1736
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@177a
    check-cast v2, [I

    #@177c
    move-object v0, v2

    #@177d
    check-cast v0, [I

    #@177f
    move-object/from16 v18, v0

    #@1781
    .line 1738
    .local v18, index:[I
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1783
    if-nez v2, :cond_178b

    #@1785
    move-object/from16 v0, v18

    #@1787
    array-length v2, v0

    #@1788
    const/4 v4, 0x1

    #@1789
    if-eq v2, v4, :cond_17b4

    #@178b
    .line 1739
    :cond_178b
    new-instance v2, Ljava/lang/StringBuilder;

    #@178d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1790
    const-string v4, "Error on SMS_ON_SIM with exp "

    #@1792
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1795
    move-result-object v2

    #@1796
    iget-object v4, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1798
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@179b
    move-result-object v2

    #@179c
    const-string v4, " length "

    #@179e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a1
    move-result-object v2

    #@17a2
    move-object/from16 v0, v18

    #@17a4
    array-length v4, v0

    #@17a5
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17a8
    move-result-object v2

    #@17a9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17ac
    move-result-object v2

    #@17ad
    move-object/from16 v0, p0

    #@17af
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@17b2
    goto/16 :goto_4b

    #@17b4
    .line 1742
    :cond_17b4
    new-instance v2, Ljava/lang/StringBuilder;

    #@17b6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17b9
    const-string v4, "READ EF_SMS RECORD index="

    #@17bb
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17be
    move-result-object v2

    #@17bf
    const/4 v4, 0x0

    #@17c0
    aget v4, v18, v4

    #@17c2
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17c5
    move-result-object v2

    #@17c6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17c9
    move-result-object v2

    #@17ca
    move-object/from16 v0, p0

    #@17cc
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@17cf
    .line 1744
    const/4 v2, 0x0

    #@17d0
    const-string v4, "seperate_processing_sms_uicc"

    #@17d2
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@17d5
    move-result v2

    #@17d6
    if-eqz v2, :cond_17fb

    #@17d8
    .line 1745
    const/4 v2, 0x0

    #@17d9
    aget v2, v18, v2

    #@17db
    move-object/from16 v0, p0

    #@17dd
    iput v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mIndexOnIcc:I

    #@17df
    .line 1746
    const-string v2, "GSM"

    #@17e1
    new-instance v4, Ljava/lang/StringBuilder;

    #@17e3
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@17e6
    const-string v5, "[TEL-SMS] EVENT_SMS_ON_SIM - mIndexOnIcc: "

    #@17e8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17eb
    move-result-object v4

    #@17ec
    move-object/from16 v0, p0

    #@17ee
    iget v5, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mIndexOnIcc:I

    #@17f0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17f3
    move-result-object v4

    #@17f4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17f7
    move-result-object v4

    #@17f8
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17fb
    .line 1749
    :cond_17fb
    move-object/from16 v0, p0

    #@17fd
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@17ff
    const/16 v4, 0x6f3c

    #@1801
    const/4 v5, 0x0

    #@1802
    aget v5, v18, v5

    #@1804
    const/16 v6, 0x16

    #@1806
    move-object/from16 v0, p0

    #@1808
    invoke-virtual {v0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@180b
    move-result-object v6

    #@180c
    invoke-virtual {v2, v4, v5, v6}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V

    #@180f
    goto/16 :goto_4b

    #@1811
    .line 1755
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v18           #index:[I
    :sswitch_1811
    const/16 v20, 0x0

    #@1813
    .line 1756
    move-object/from16 v0, p1

    #@1815
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1817
    check-cast v9, Landroid/os/AsyncResult;

    #@1819
    .line 1757
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@181b
    if-nez v2, :cond_182a

    #@181d
    .line 1758
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@181f
    check-cast v2, [B

    #@1821
    check-cast v2, [B

    #@1823
    move-object/from16 v0, p0

    #@1825
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->handleSms([B)V

    #@1828
    goto/16 :goto_4b

    #@182a
    .line 1760
    :cond_182a
    new-instance v2, Ljava/lang/StringBuilder;

    #@182c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@182f
    const-string v4, "Error on GET_SMS with exp "

    #@1831
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1834
    move-result-object v2

    #@1835
    iget-object v4, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1837
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@183a
    move-result-object v2

    #@183b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@183e
    move-result-object v2

    #@183f
    move-object/from16 v0, p0

    #@1841
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@1844
    goto/16 :goto_4b

    #@1846
    .line 1764
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_1846
    const/16 v20, 0x1

    #@1848
    .line 1766
    move-object/from16 v0, p1

    #@184a
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@184c
    check-cast v9, Landroid/os/AsyncResult;

    #@184e
    .line 1767
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1850
    check-cast v2, [B

    #@1852
    move-object v0, v2

    #@1853
    check-cast v0, [B

    #@1855
    move-object v12, v0

    #@1856
    .line 1769
    .restart local v12       #data:[B
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1858
    if-nez v2, :cond_4b

    #@185a
    .line 1773
    new-instance v2, Lcom/android/internal/telephony/uicc/UsimServiceTable;

    #@185c
    invoke-direct {v2, v12}, Lcom/android/internal/telephony/uicc/UsimServiceTable;-><init>([B)V

    #@185f
    move-object/from16 v0, p0

    #@1861
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mUsimServiceTable:Lcom/android/internal/telephony/uicc/UsimServiceTable;

    #@1863
    .line 1774
    new-instance v2, Ljava/lang/StringBuilder;

    #@1865
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1868
    const-string v4, "SST: "

    #@186a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@186d
    move-result-object v2

    #@186e
    move-object/from16 v0, p0

    #@1870
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mUsimServiceTable:Lcom/android/internal/telephony/uicc/UsimServiceTable;

    #@1872
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1875
    move-result-object v2

    #@1876
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1879
    move-result-object v2

    #@187a
    move-object/from16 v0, p0

    #@187c
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@187f
    goto/16 :goto_4b

    #@1881
    .line 1778
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_1881
    const/16 v20, 0x1

    #@1883
    .line 1780
    move-object/from16 v0, p1

    #@1885
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1887
    check-cast v9, Landroid/os/AsyncResult;

    #@1889
    .line 1782
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@188b
    if-nez v2, :cond_4b

    #@188d
    .line 1786
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@188f
    check-cast v2, [B

    #@1891
    check-cast v2, [B

    #@1893
    move-object/from16 v0, p0

    #@1895
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mCphsInfo:[B

    #@1897
    .line 1788
    new-instance v2, Ljava/lang/StringBuilder;

    #@1899
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@189c
    const-string v4, "iCPHS: "

    #@189e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18a1
    move-result-object v2

    #@18a2
    move-object/from16 v0, p0

    #@18a4
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mCphsInfo:[B

    #@18a6
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@18a9
    move-result-object v4

    #@18aa
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18ad
    move-result-object v2

    #@18ae
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18b1
    move-result-object v2

    #@18b2
    move-object/from16 v0, p0

    #@18b4
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@18b7
    goto/16 :goto_4b

    #@18b9
    .line 1792
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_18b9
    const/16 v20, 0x0

    #@18bb
    .line 1793
    move-object/from16 v0, p1

    #@18bd
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@18bf
    check-cast v9, Landroid/os/AsyncResult;

    #@18c1
    .line 1795
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@18c3
    if-nez v2, :cond_18d5

    #@18c5
    .line 1796
    move-object/from16 v0, p0

    #@18c7
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->newVoiceMailNum:Ljava/lang/String;

    #@18c9
    move-object/from16 v0, p0

    #@18cb
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailNum:Ljava/lang/String;

    #@18cd
    .line 1797
    move-object/from16 v0, p0

    #@18cf
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->newVoiceMailTag:Ljava/lang/String;

    #@18d1
    move-object/from16 v0, p0

    #@18d3
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailTag:Ljava/lang/String;

    #@18d5
    .line 1800
    :cond_18d5
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->isCphsMailboxEnabled()Z

    #@18d8
    move-result v2

    #@18d9
    if-eqz v2, :cond_192f

    #@18db
    .line 1801
    new-instance v3, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@18dd
    move-object/from16 v0, p0

    #@18df
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailTag:Ljava/lang/String;

    #@18e1
    move-object/from16 v0, p0

    #@18e3
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailNum:Ljava/lang/String;

    #@18e5
    invoke-direct {v3, v2, v4}, Lcom/android/internal/telephony/uicc/AdnRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@18e8
    .line 1802
    .restart local v3       #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    iget-object v0, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@18ea
    move-object/from16 v29, v0

    #@18ec
    check-cast v29, Landroid/os/Message;

    #@18ee
    .line 1811
    .local v29, onCphsCompleted:Landroid/os/Message;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@18f0
    if-nez v2, :cond_1911

    #@18f2
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@18f4
    if-eqz v2, :cond_1911

    #@18f6
    .line 1812
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@18f8
    check-cast v2, Landroid/os/Message;

    #@18fa
    invoke-static {v2}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@18fd
    move-result-object v2

    #@18fe
    const/4 v4, 0x0

    #@18ff
    iput-object v4, v2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1901
    .line 1814
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@1903
    check-cast v2, Landroid/os/Message;

    #@1905
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    #@1908
    .line 1816
    const-string v2, "Callback with MBDN successful."

    #@190a
    move-object/from16 v0, p0

    #@190c
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@190f
    .line 1818
    const/16 v29, 0x0

    #@1911
    .line 1821
    :cond_1911
    new-instance v2, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@1913
    move-object/from16 v0, p0

    #@1915
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@1917
    invoke-direct {v2, v4}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@191a
    const/16 v4, 0x6f17

    #@191c
    const/16 v5, 0x6f4a

    #@191e
    const/4 v6, 0x1

    #@191f
    const/4 v7, 0x0

    #@1920
    const/16 v8, 0x19

    #@1922
    move-object/from16 v0, p0

    #@1924
    move-object/from16 v1, v29

    #@1926
    invoke-virtual {v0, v8, v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1929
    move-result-object v8

    #@192a
    invoke-virtual/range {v2 .. v8}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->updateEF(Lcom/android/internal/telephony/uicc/AdnRecord;IIILjava/lang/String;Landroid/os/Message;)V

    #@192d
    goto/16 :goto_4b

    #@192f
    .line 1826
    .end local v3           #adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    .end local v29           #onCphsCompleted:Landroid/os/Message;
    :cond_192f
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@1931
    if-eqz v2, :cond_4b

    #@1933
    .line 1827
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@1935
    check-cast v2, Landroid/os/Message;

    #@1937
    invoke-static {v2}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@193a
    move-result-object v2

    #@193b
    iget-object v4, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@193d
    iput-object v4, v2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@193f
    .line 1829
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@1941
    check-cast v2, Landroid/os/Message;

    #@1943
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    #@1946
    goto/16 :goto_4b

    #@1948
    .line 1834
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_1948
    const/16 v20, 0x0

    #@194a
    .line 1835
    move-object/from16 v0, p1

    #@194c
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@194e
    check-cast v9, Landroid/os/AsyncResult;

    #@1950
    .line 1836
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1952
    if-nez v2, :cond_1984

    #@1954
    .line 1837
    move-object/from16 v0, p0

    #@1956
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->newVoiceMailNum:Ljava/lang/String;

    #@1958
    move-object/from16 v0, p0

    #@195a
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailNum:Ljava/lang/String;

    #@195c
    .line 1838
    move-object/from16 v0, p0

    #@195e
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->newVoiceMailTag:Ljava/lang/String;

    #@1960
    move-object/from16 v0, p0

    #@1962
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailTag:Ljava/lang/String;

    #@1964
    .line 1843
    :goto_1964
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@1966
    if-eqz v2, :cond_4b

    #@1968
    .line 1844
    const-string v2, "Callback with CPHS MB successful."

    #@196a
    move-object/from16 v0, p0

    #@196c
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@196f
    .line 1845
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@1971
    check-cast v2, Landroid/os/Message;

    #@1973
    invoke-static {v2}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@1976
    move-result-object v2

    #@1977
    iget-object v4, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1979
    iput-object v4, v2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@197b
    .line 1847
    iget-object v2, v9, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@197d
    check-cast v2, Landroid/os/Message;

    #@197f
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    #@1982
    goto/16 :goto_4b

    #@1984
    .line 1840
    :cond_1984
    new-instance v2, Ljava/lang/StringBuilder;

    #@1986
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1989
    const-string v4, "Set CPHS MailBox with exception: "

    #@198b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198e
    move-result-object v2

    #@198f
    iget-object v4, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1991
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1994
    move-result-object v2

    #@1995
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1998
    move-result-object v2

    #@1999
    move-object/from16 v0, p0

    #@199b
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@199e
    goto :goto_1964

    #@199f
    .line 1851
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_199f
    const/16 v20, 0x0

    #@19a1
    .line 1852
    move-object/from16 v0, p1

    #@19a3
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@19a5
    check-cast v9, Landroid/os/AsyncResult;

    #@19a7
    .line 1853
    .restart local v9       #ar:Landroid/os/AsyncResult;
    new-instance v2, Ljava/lang/StringBuilder;

    #@19a9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19ac
    const-string v4, "Sim REFRESH with exception: "

    #@19ae
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19b1
    move-result-object v2

    #@19b2
    iget-object v4, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@19b4
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19b7
    move-result-object v2

    #@19b8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19bb
    move-result-object v2

    #@19bc
    move-object/from16 v0, p0

    #@19be
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@19c1
    .line 1854
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@19c3
    if-nez v2, :cond_4b

    #@19c5
    .line 1855
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@19c7
    check-cast v2, Lcom/android/internal/telephony/uicc/IccRefreshResponse;

    #@19c9
    move-object/from16 v0, p0

    #@19cb
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->handleSimRefresh(Lcom/android/internal/telephony/uicc/IccRefreshResponse;)V

    #@19ce
    goto/16 :goto_4b

    #@19d0
    .line 1859
    .end local v9           #ar:Landroid/os/AsyncResult;
    :sswitch_19d0
    const/16 v20, 0x1

    #@19d2
    .line 1861
    move-object/from16 v0, p1

    #@19d4
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@19d6
    check-cast v9, Landroid/os/AsyncResult;

    #@19d8
    .line 1862
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@19da
    check-cast v2, [B

    #@19dc
    move-object v0, v2

    #@19dd
    check-cast v0, [B

    #@19df
    move-object v12, v0

    #@19e0
    .line 1864
    .restart local v12       #data:[B
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@19e2
    if-nez v2, :cond_4b

    #@19e4
    .line 1868
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@19e6
    if-eqz v2, :cond_1a04

    #@19e8
    new-instance v2, Ljava/lang/StringBuilder;

    #@19ea
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19ed
    const-string v4, "EF_CFIS: "

    #@19ef
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19f2
    move-result-object v2

    #@19f3
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@19f6
    move-result-object v4

    #@19f7
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19fa
    move-result-object v2

    #@19fb
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19fe
    move-result-object v2

    #@19ff
    move-object/from16 v0, p0

    #@1a01
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1a04
    .line 1870
    :cond_1a04
    move-object/from16 v0, p0

    #@1a06
    invoke-direct {v0, v12}, Lcom/android/internal/telephony/uicc/SIMRecords;->validEfCfis([B)Z

    #@1a09
    move-result v2

    #@1a0a
    if-eqz v2, :cond_1a48

    #@1a0c
    .line 1871
    move-object/from16 v0, p0

    #@1a0e
    iput-object v12, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@1a10
    .line 1874
    const/4 v2, 0x1

    #@1a11
    aget-byte v2, v12, v2

    #@1a13
    and-int/lit8 v2, v2, 0x1

    #@1a15
    if-eqz v2, :cond_1a46

    #@1a17
    const/4 v2, 0x1

    #@1a18
    :goto_1a18
    move-object/from16 v0, p0

    #@1a1a
    iput-boolean v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->callForwardingEnabled:Z

    #@1a1c
    .line 1875
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a1e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a21
    const-string v4, "EF_CFIS: callFordwardingEnabled="

    #@1a23
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a26
    move-result-object v2

    #@1a27
    move-object/from16 v0, p0

    #@1a29
    iget-boolean v4, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->callForwardingEnabled:Z

    #@1a2b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a2e
    move-result-object v2

    #@1a2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a32
    move-result-object v2

    #@1a33
    move-object/from16 v0, p0

    #@1a35
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1a38
    .line 1877
    move-object/from16 v0, p0

    #@1a3a
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mRecordsEventsRegistrants:Landroid/os/RegistrantList;

    #@1a3c
    const/4 v4, 0x1

    #@1a3d
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a40
    move-result-object v4

    #@1a41
    invoke-virtual {v2, v4}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@1a44
    goto/16 :goto_4b

    #@1a46
    .line 1874
    :cond_1a46
    const/4 v2, 0x0

    #@1a47
    goto :goto_1a18

    #@1a48
    .line 1879
    :cond_1a48
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@1a4a
    if-eqz v2, :cond_4b

    #@1a4c
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a4e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a51
    const-string v4, "EF_CFIS: invalid data="

    #@1a53
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a56
    move-result-object v2

    #@1a57
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@1a5a
    move-result-object v4

    #@1a5b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5e
    move-result-object v2

    #@1a5f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a62
    move-result-object v2

    #@1a63
    move-object/from16 v0, p0

    #@1a65
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1a68
    goto/16 :goto_4b

    #@1a6a
    .line 1884
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_1a6a
    const/16 v20, 0x1

    #@1a6c
    .line 1886
    move-object/from16 v0, p1

    #@1a6e
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1a70
    check-cast v9, Landroid/os/AsyncResult;

    #@1a72
    .line 1888
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1a74
    if-eqz v2, :cond_1a92

    #@1a76
    .line 1889
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a78
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a7b
    const-string v4, "Exception in fetching EF_CSP data "

    #@1a7d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a80
    move-result-object v2

    #@1a81
    iget-object v4, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1a83
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a86
    move-result-object v2

    #@1a87
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a8a
    move-result-object v2

    #@1a8b
    move-object/from16 v0, p0

    #@1a8d
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@1a90
    goto/16 :goto_4b

    #@1a92
    .line 1893
    :cond_1a92
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1a94
    check-cast v2, [B

    #@1a96
    move-object v0, v2

    #@1a97
    check-cast v0, [B

    #@1a99
    move-object v12, v0

    #@1a9a
    .line 1895
    .restart local v12       #data:[B
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a9c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a9f
    const-string v4, "EF_CSP: "

    #@1aa1
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa4
    move-result-object v2

    #@1aa5
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@1aa8
    move-result-object v4

    #@1aa9
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aac
    move-result-object v2

    #@1aad
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ab0
    move-result-object v2

    #@1ab1
    move-object/from16 v0, p0

    #@1ab3
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1ab6
    .line 1896
    move-object/from16 v0, p0

    #@1ab8
    invoke-direct {v0, v12}, Lcom/android/internal/telephony/uicc/SIMRecords;->handleEfCspData([B)V

    #@1abb
    goto/16 :goto_4b

    #@1abd
    .line 1901
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    :sswitch_1abd
    const/16 v20, 0x1

    #@1abf
    .line 1902
    move-object/from16 v0, p1

    #@1ac1
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1ac3
    check-cast v9, Landroid/os/AsyncResult;

    #@1ac5
    .line 1904
    .restart local v9       #ar:Landroid/os/AsyncResult;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1ac7
    if-eqz v2, :cond_1aea

    #@1ac9
    .line 1905
    const-string v2, "GSM"

    #@1acb
    new-instance v4, Ljava/lang/StringBuilder;

    #@1acd
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1ad0
    const-string v5, "[LGE UICC] HPLMNWACT: Exception in fetching HPLMNWACT Records "

    #@1ad2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ad5
    move-result-object v4

    #@1ad6
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1ad8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1adb
    move-result-object v4

    #@1adc
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1adf
    move-result-object v4

    #@1ae0
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1ae3
    .line 1906
    const/4 v2, 0x0

    #@1ae4
    move-object/from16 v0, p0

    #@1ae6
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->efHplmnwact:Ljava/lang/String;

    #@1ae8
    goto/16 :goto_4b

    #@1aea
    .line 1910
    :cond_1aea
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1aec
    check-cast v2, [B

    #@1aee
    move-object v0, v2

    #@1aef
    check-cast v0, [B

    #@1af1
    move-object v12, v0

    #@1af2
    .line 1911
    .restart local v12       #data:[B
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@1af5
    move-result-object v2

    #@1af6
    move-object/from16 v0, p0

    #@1af8
    iput-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->efHplmnwact:Ljava/lang/String;

    #@1afa
    .line 1913
    move-object/from16 v0, p0

    #@1afc
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->efHplmnwact:Ljava/lang/String;

    #@1afe
    if-nez v2, :cond_1b09

    #@1b00
    .line 1915
    const-string v2, "GSM"

    #@1b02
    const-string v4, "[LGE UICC] HPLMNWACT is Invalid "

    #@1b04
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b07
    goto/16 :goto_4b

    #@1b09
    .line 1919
    :cond_1b09
    sget-boolean v2, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@1b0b
    if-eqz v2, :cond_1b60

    #@1b0d
    .line 1921
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b0f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b12
    const-string v4, "[LGE UICC] EF_HPLMNWACT = "

    #@1b14
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b17
    move-result-object v2

    #@1b18
    move-object/from16 v0, p0

    #@1b1a
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->efHplmnwact:Ljava/lang/String;

    #@1b1c
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b1f
    move-result-object v2

    #@1b20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b23
    move-result-object v2

    #@1b24
    move-object/from16 v0, p0

    #@1b26
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1b29
    .line 1922
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b2e
    const-string v4, "[LGE UICC] MCC/MNC = "

    #@1b30
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b33
    move-result-object v2

    #@1b34
    move-object/from16 v0, p0

    #@1b36
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->efHplmnwact:Ljava/lang/String;

    #@1b38
    const/4 v5, 0x0

    #@1b39
    const/4 v6, 0x6

    #@1b3a
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1b3d
    move-result-object v4

    #@1b3e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b41
    move-result-object v2

    #@1b42
    const-string v4, "[LGE UICC] E-UTRAN = "

    #@1b44
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b47
    move-result-object v2

    #@1b48
    move-object/from16 v0, p0

    #@1b4a
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->efHplmnwact:Ljava/lang/String;

    #@1b4c
    const/4 v5, 0x6

    #@1b4d
    const/16 v6, 0x8

    #@1b4f
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1b52
    move-result-object v4

    #@1b53
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b56
    move-result-object v2

    #@1b57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b5a
    move-result-object v2

    #@1b5b
    move-object/from16 v0, p0

    #@1b5d
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1b60
    .line 1925
    :cond_1b60
    array-length v0, v12

    #@1b61
    move/from16 v22, v0

    #@1b63
    .line 1927
    .local v22, len:I
    const/16 v2, 0x64

    #@1b65
    move/from16 v0, v22

    #@1b67
    if-le v0, v2, :cond_1b6b

    #@1b69
    .line 1928
    const/16 v22, 0x64

    #@1b6b
    .line 1931
    :cond_1b6b
    const/16 v16, 0x0

    #@1b6d
    .local v16, i:I
    :goto_1b6d
    move/from16 v0, v16

    #@1b6f
    move/from16 v1, v22

    #@1b71
    if-ge v0, v1, :cond_1ba0

    #@1b73
    .line 1933
    aget-byte v2, v12, v16

    #@1b75
    const/4 v4, 0x2

    #@1b76
    if-ne v2, v4, :cond_1bd3

    #@1b78
    add-int/lit8 v2, v16, 0x1

    #@1b7a
    aget-byte v2, v12, v2

    #@1b7c
    const/16 v4, -0xc

    #@1b7e
    if-ne v2, v4, :cond_1bd3

    #@1b80
    add-int/lit8 v2, v16, 0x2

    #@1b82
    aget-byte v2, v12, v2

    #@1b84
    const/16 v4, 0x40

    #@1b86
    if-ne v2, v4, :cond_1bd3

    #@1b88
    .line 1935
    add-int/lit8 v2, v16, 0x3

    #@1b8a
    aget-byte v2, v12, v2

    #@1b8c
    and-int/lit8 v2, v2, 0x40

    #@1b8e
    const/16 v4, 0x40

    #@1b90
    if-eq v2, v4, :cond_1c3b

    #@1b92
    .line 1938
    const-string v2, "gsm.sim.type"

    #@1b94
    const-string v4, "vzw3g"

    #@1b96
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1b99
    .line 1939
    const-string v2, "[LGE UICC] return VZW 3G"

    #@1b9b
    move-object/from16 v0, p0

    #@1b9d
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1ba0
    .line 1965
    :cond_1ba0
    :goto_1ba0
    const-string v2, "GSM"

    #@1ba2
    const-string v4, "Broadcast Received EVENT_HPLMNWACT "

    #@1ba4
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ba7
    .line 1966
    new-instance v19, Landroid/content/Intent;

    #@1ba9
    const-string v2, "android.intent.action.SIM_STATE_CHANGED"

    #@1bab
    move-object/from16 v0, v19

    #@1bad
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1bb0
    .line 1967
    .restart local v19       #intent:Landroid/content/Intent;
    const/high16 v2, 0x2000

    #@1bb2
    move-object/from16 v0, v19

    #@1bb4
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@1bb7
    .line 1968
    const-string v2, "phoneName"

    #@1bb9
    const-string v4, "Phone"

    #@1bbb
    move-object/from16 v0, v19

    #@1bbd
    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1bc0
    .line 1969
    const-string v2, "ss"

    #@1bc2
    const-string v4, "HPLMN_SIMTYPE"

    #@1bc4
    move-object/from16 v0, v19

    #@1bc6
    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1bc9
    .line 1971
    const-string v2, "android.permission.READ_PHONE_STATE"

    #@1bcb
    const/4 v4, -0x1

    #@1bcc
    move-object/from16 v0, v19

    #@1bce
    invoke-static {v0, v2, v4}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    #@1bd1
    goto/16 :goto_4b

    #@1bd3
    .line 1943
    .end local v19           #intent:Landroid/content/Intent;
    :cond_1bd3
    aget-byte v2, v12, v16

    #@1bd5
    const/4 v4, 0x2

    #@1bd6
    if-ne v2, v4, :cond_1be8

    #@1bd8
    add-int/lit8 v2, v16, 0x1

    #@1bda
    aget-byte v2, v12, v2

    #@1bdc
    const/16 v4, -0xc

    #@1bde
    if-ne v2, v4, :cond_1be8

    #@1be0
    add-int/lit8 v2, v16, 0x2

    #@1be2
    aget-byte v2, v12, v2

    #@1be4
    const/16 v4, 0x40

    #@1be6
    if-eq v2, v4, :cond_1bfd

    #@1be8
    :cond_1be8
    aget-byte v2, v12, v16

    #@1bea
    const/16 v4, 0x13

    #@1bec
    if-ne v2, v4, :cond_1c3b

    #@1bee
    add-int/lit8 v2, v16, 0x1

    #@1bf0
    aget-byte v2, v12, v2

    #@1bf2
    const/4 v4, 0x1

    #@1bf3
    if-ne v2, v4, :cond_1c3b

    #@1bf5
    add-int/lit8 v2, v16, 0x2

    #@1bf7
    aget-byte v2, v12, v2

    #@1bf9
    const/16 v4, -0x7c

    #@1bfb
    if-ne v2, v4, :cond_1c3b

    #@1bfd
    .line 1947
    :cond_1bfd
    add-int/lit8 v2, v16, 0x3

    #@1bff
    aget-byte v2, v12, v2

    #@1c01
    and-int/lit8 v2, v2, 0x40

    #@1c03
    const/16 v4, 0x40

    #@1c05
    if-ne v2, v4, :cond_1c3b

    #@1c07
    .line 1950
    move-object/from16 v0, p0

    #@1c09
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1c0b
    const/4 v4, 0x0

    #@1c0c
    const/4 v5, 0x6

    #@1c0d
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c10
    move-result-object v2

    #@1c11
    const-string v4, "311480"

    #@1c13
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c16
    move-result v2

    #@1c17
    if-nez v2, :cond_1c2b

    #@1c19
    move-object/from16 v0, p0

    #@1c1b
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1c1d
    const/4 v4, 0x0

    #@1c1e
    const/4 v5, 0x5

    #@1c1f
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c22
    move-result-object v2

    #@1c23
    const-string v4, "20404"

    #@1c25
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c28
    move-result v2

    #@1c29
    if-eqz v2, :cond_1c3b

    #@1c2b
    .line 1954
    :cond_1c2b
    const-string v2, "gsm.sim.type"

    #@1c2d
    const-string v4, "vzw4g"

    #@1c2f
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1c32
    .line 1955
    const-string v2, "[LGE UICC] return VZW 4G"

    #@1c34
    move-object/from16 v0, p0

    #@1c36
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1c39
    goto/16 :goto_1ba0

    #@1c3b
    .line 1931
    :cond_1c3b
    add-int/lit8 v16, v16, 0x5

    #@1c3d
    goto/16 :goto_1b6d

    #@1c3f
    .line 1977
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    .end local v16           #i:I
    .end local v22           #len:I
    :sswitch_1c3f
    move-object/from16 v0, p1

    #@1c41
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1c43
    check-cast v9, Landroid/os/AsyncResult;

    #@1c45
    .line 1978
    .restart local v9       #ar:Landroid/os/AsyncResult;
    const-string v2, "GSM"

    #@1c47
    const-string v4, "Received EVENT_SMARTCARD_GET_ATR "

    #@1c49
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c4c
    .line 1979
    if-eqz v9, :cond_1c52

    #@1c4e
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1c50
    if-eqz v2, :cond_1c74

    #@1c52
    .line 1980
    :cond_1c52
    if-eqz v9, :cond_4b

    #@1c54
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1c56
    if-eqz v2, :cond_4b

    #@1c58
    .line 1981
    const-string v2, "GSM"

    #@1c5a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c5c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c5f
    const-string v5, "EVENT_SMARTCARD_GET_ATR Error : "

    #@1c61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c64
    move-result-object v4

    #@1c65
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1c67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c6a
    move-result-object v4

    #@1c6b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c6e
    move-result-object v4

    #@1c6f
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c72
    goto/16 :goto_4b

    #@1c74
    .line 1985
    :cond_1c74
    iget-object v11, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1c76
    check-cast v11, Lcom/android/internal/telephony/uicc/SmartCardResult;

    #@1c78
    .line 1986
    .local v11, atr_data:Lcom/android/internal/telephony/uicc/SmartCardResult;
    const-string v2, "GSM"

    #@1c7a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c7c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c7f
    const-string v5, "EVENT_SMARTCARD_GET_ATR "

    #@1c81
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c84
    move-result-object v4

    #@1c85
    invoke-virtual {v11}, Lcom/android/internal/telephony/uicc/SmartCardResult;->toString()Ljava/lang/String;

    #@1c88
    move-result-object v5

    #@1c89
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c8c
    move-result-object v4

    #@1c8d
    const-string v5, "#"

    #@1c8f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c92
    move-result-object v4

    #@1c93
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c96
    move-result-object v4

    #@1c97
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c9a
    .line 1987
    const-string v2, "gsm.sim.atr"

    #@1c9c
    iget-object v4, v11, Lcom/android/internal/telephony/uicc/SmartCardResult;->data:[B

    #@1c9e
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@1ca1
    move-result-object v4

    #@1ca2
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1ca5
    goto/16 :goto_4b

    #@1ca7
    .line 1992
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v11           #atr_data:Lcom/android/internal/telephony/uicc/SmartCardResult;
    :sswitch_1ca7
    const/16 v20, 0x1

    #@1ca9
    .line 1993
    move-object/from16 v0, p1

    #@1cab
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1cad
    check-cast v9, Landroid/os/AsyncResult;

    #@1caf
    .line 1994
    .restart local v9       #ar:Landroid/os/AsyncResult;
    const/4 v14, 0x0

    #@1cb0
    .line 1996
    .local v14, efroaming:Ljava/lang/String;
    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1cb2
    if-eqz v2, :cond_1cd0

    #@1cb4
    .line 1997
    const-string v2, "GSM"

    #@1cb6
    new-instance v4, Ljava/lang/StringBuilder;

    #@1cb8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1cbb
    const-string v5, "Exception in reading EF_ROAMING: "

    #@1cbd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc0
    move-result-object v4

    #@1cc1
    iget-object v5, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1cc3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1cc6
    move-result-object v4

    #@1cc7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cca
    move-result-object v4

    #@1ccb
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1cce
    goto/16 :goto_4b

    #@1cd0
    .line 2001
    :cond_1cd0
    iget-object v2, v9, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1cd2
    check-cast v2, [B

    #@1cd4
    move-object v0, v2

    #@1cd5
    check-cast v0, [B

    #@1cd7
    move-object v12, v0

    #@1cd8
    .line 2002
    .restart local v12       #data:[B
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@1cdb
    move-result-object v14

    #@1cdc
    .line 2004
    const-string v2, "GSM"

    #@1cde
    new-instance v4, Ljava/lang/StringBuilder;

    #@1ce0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1ce3
    const-string v5, "EVENT_READ_EF_ROAMING efroaming :"

    #@1ce5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ce8
    move-result-object v4

    #@1ce9
    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cec
    move-result-object v4

    #@1ced
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cf0
    move-result-object v4

    #@1cf1
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1cf4
    .line 2005
    const-string v2, "gsm.sim.lgu_efroaming"

    #@1cf6
    move-object/from16 v0, p0

    #@1cf8
    invoke-direct {v0, v2, v14}, Lcom/android/internal/telephony/uicc/SIMRecords;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@1cfb
    goto/16 :goto_4b

    #@1cfd
    .line 1572
    .end local v9           #ar:Landroid/os/AsyncResult;
    .end local v12           #data:[B
    .end local v14           #efroaming:Ljava/lang/String;
    .restart local v10       #arr$:[Ljava/lang/String;
    .restart local v17       #i$:I
    .restart local v23       #len$:I
    .restart local v25       #mccmnc:Ljava/lang/String;
    .restart local v26       #mccmncCode:Ljava/lang/String;
    :cond_1cfd
    add-int/lit8 v17, v17, 0x1

    #@1cff
    goto/16 :goto_151f

    #@1d01
    .line 1592
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_1d01
    const/4 v4, 0x0

    #@1d02
    move-object/from16 v0, p0

    #@1d04
    iput v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1d06
    .line 1594
    const-string v4, "MNC length not present in EF_AD"

    #@1d08
    move-object/from16 v0, p0

    #@1d0a
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1d0d
    goto/16 :goto_155b

    #@1d0f
    .line 1586
    :catch_1d0f
    move-exception v13

    #@1d10
    .line 1587
    .restart local v13       #e:Ljava/lang/NumberFormatException;
    const/4 v4, 0x0

    #@1d11
    move-object/from16 v0, p0

    #@1d13
    iput v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1d15
    .line 1588
    const-string v4, "Corrupt IMSI!"

    #@1d17
    move-object/from16 v0, p0

    #@1d19
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@1d1c
    goto/16 :goto_155b

    #@1d1e
    .line 1621
    .end local v13           #e:Ljava/lang/NumberFormatException;
    :cond_1d1e
    move-object/from16 v0, p0

    #@1d20
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1d22
    move-object/from16 v0, p0

    #@1d24
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1d26
    const/4 v6, 0x0

    #@1d27
    move-object/from16 v0, p0

    #@1d29
    iget v7, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1d2b
    add-int/lit8 v7, v7, 0x3

    #@1d2d
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1d30
    move-result-object v5

    #@1d31
    invoke-static {v4, v5}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1d34
    goto/16 :goto_15ed

    #@1d36
    .line 1616
    .restart local v27       #numeric:Ljava/lang/String;
    :cond_1d36
    move-object/from16 v0, p0

    #@1d38
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1d3a
    move-object/from16 v0, p0

    #@1d3c
    iget-object v5, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1d3e
    const/4 v6, 0x0

    #@1d3f
    move-object/from16 v0, p0

    #@1d41
    iget v7, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1d43
    add-int/lit8 v7, v7, 0x3

    #@1d45
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1d48
    move-result-object v5

    #@1d49
    invoke-static {v4, v5}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1d4c
    goto/16 :goto_15ed

    #@1d4e
    .line 1572
    .end local v27           #numeric:Ljava/lang/String;
    .restart local v9       #ar:Landroid/os/AsyncResult;
    .restart local v10       #arr$:[Ljava/lang/String;
    .restart local v12       #data:[B
    .restart local v17       #i$:I
    .restart local v23       #len$:I
    .restart local v25       #mccmnc:Ljava/lang/String;
    .restart local v26       #mccmncCode:Ljava/lang/String;
    :cond_1d4e
    add-int/lit8 v17, v17, 0x1

    #@1d50
    goto/16 :goto_ebf

    #@1d52
    .line 1592
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_1d52
    const/4 v2, 0x0

    #@1d53
    move-object/from16 v0, p0

    #@1d55
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1d57
    .line 1594
    const-string v2, "MNC length not present in EF_AD"

    #@1d59
    move-object/from16 v0, p0

    #@1d5b
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1d5e
    goto/16 :goto_efb

    #@1d60
    .line 1586
    :catch_1d60
    move-exception v13

    #@1d61
    .line 1587
    .restart local v13       #e:Ljava/lang/NumberFormatException;
    const/4 v2, 0x0

    #@1d62
    move-object/from16 v0, p0

    #@1d64
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1d66
    .line 1588
    const-string v2, "Corrupt IMSI!"

    #@1d68
    move-object/from16 v0, p0

    #@1d6a
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@1d6d
    goto/16 :goto_efb

    #@1d6f
    .line 1621
    .end local v13           #e:Ljava/lang/NumberFormatException;
    :cond_1d6f
    move-object/from16 v0, p0

    #@1d71
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1d73
    move-object/from16 v0, p0

    #@1d75
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1d77
    const/4 v5, 0x0

    #@1d78
    move-object/from16 v0, p0

    #@1d7a
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1d7c
    add-int/lit8 v6, v6, 0x3

    #@1d7e
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1d81
    move-result-object v4

    #@1d82
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1d85
    goto/16 :goto_f8d

    #@1d87
    .line 1616
    .restart local v27       #numeric:Ljava/lang/String;
    :cond_1d87
    move-object/from16 v0, p0

    #@1d89
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1d8b
    move-object/from16 v0, p0

    #@1d8d
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1d8f
    const/4 v5, 0x0

    #@1d90
    move-object/from16 v0, p0

    #@1d92
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1d94
    add-int/lit8 v6, v6, 0x3

    #@1d96
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1d99
    move-result-object v4

    #@1d9a
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1d9d
    goto/16 :goto_f8d

    #@1d9f
    .line 1572
    .end local v27           #numeric:Ljava/lang/String;
    .restart local v10       #arr$:[Ljava/lang/String;
    .restart local v17       #i$:I
    .restart local v23       #len$:I
    .restart local v25       #mccmnc:Ljava/lang/String;
    .restart local v26       #mccmncCode:Ljava/lang/String;
    :cond_1d9f
    add-int/lit8 v17, v17, 0x1

    #@1da1
    goto/16 :goto_101a

    #@1da3
    .line 1592
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_1da3
    const/4 v2, 0x0

    #@1da4
    move-object/from16 v0, p0

    #@1da6
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1da8
    .line 1594
    const-string v2, "MNC length not present in EF_AD"

    #@1daa
    move-object/from16 v0, p0

    #@1dac
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1daf
    goto/16 :goto_1056

    #@1db1
    .line 1586
    :catch_1db1
    move-exception v13

    #@1db2
    .line 1587
    .restart local v13       #e:Ljava/lang/NumberFormatException;
    const/4 v2, 0x0

    #@1db3
    move-object/from16 v0, p0

    #@1db5
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1db7
    .line 1588
    const-string v2, "Corrupt IMSI!"

    #@1db9
    move-object/from16 v0, p0

    #@1dbb
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@1dbe
    goto/16 :goto_1056

    #@1dc0
    .line 1621
    .end local v13           #e:Ljava/lang/NumberFormatException;
    :cond_1dc0
    move-object/from16 v0, p0

    #@1dc2
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1dc4
    move-object/from16 v0, p0

    #@1dc6
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1dc8
    const/4 v5, 0x0

    #@1dc9
    move-object/from16 v0, p0

    #@1dcb
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1dcd
    add-int/lit8 v6, v6, 0x3

    #@1dcf
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1dd2
    move-result-object v4

    #@1dd3
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1dd6
    goto/16 :goto_10e8

    #@1dd8
    .line 1616
    .restart local v27       #numeric:Ljava/lang/String;
    :cond_1dd8
    move-object/from16 v0, p0

    #@1dda
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1ddc
    move-object/from16 v0, p0

    #@1dde
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1de0
    const/4 v5, 0x0

    #@1de1
    move-object/from16 v0, p0

    #@1de3
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1de5
    add-int/lit8 v6, v6, 0x3

    #@1de7
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1dea
    move-result-object v4

    #@1deb
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1dee
    goto/16 :goto_10e8

    #@1df0
    .line 1572
    .end local v27           #numeric:Ljava/lang/String;
    .restart local v10       #arr$:[Ljava/lang/String;
    .restart local v17       #i$:I
    .restart local v23       #len$:I
    .restart local v25       #mccmnc:Ljava/lang/String;
    .restart local v26       #mccmncCode:Ljava/lang/String;
    :cond_1df0
    add-int/lit8 v17, v17, 0x1

    #@1df2
    goto/16 :goto_1159

    #@1df4
    .line 1592
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_1df4
    const/4 v2, 0x0

    #@1df5
    move-object/from16 v0, p0

    #@1df7
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1df9
    .line 1594
    const-string v2, "MNC length not present in EF_AD"

    #@1dfb
    move-object/from16 v0, p0

    #@1dfd
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1e00
    goto/16 :goto_1195

    #@1e02
    .line 1586
    :catch_1e02
    move-exception v13

    #@1e03
    .line 1587
    .restart local v13       #e:Ljava/lang/NumberFormatException;
    const/4 v2, 0x0

    #@1e04
    move-object/from16 v0, p0

    #@1e06
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1e08
    .line 1588
    const-string v2, "Corrupt IMSI!"

    #@1e0a
    move-object/from16 v0, p0

    #@1e0c
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@1e0f
    goto/16 :goto_1195

    #@1e11
    .line 1621
    .end local v13           #e:Ljava/lang/NumberFormatException;
    :cond_1e11
    move-object/from16 v0, p0

    #@1e13
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1e15
    move-object/from16 v0, p0

    #@1e17
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1e19
    const/4 v5, 0x0

    #@1e1a
    move-object/from16 v0, p0

    #@1e1c
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1e1e
    add-int/lit8 v6, v6, 0x3

    #@1e20
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1e23
    move-result-object v4

    #@1e24
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1e27
    goto/16 :goto_1227

    #@1e29
    .line 1616
    .restart local v27       #numeric:Ljava/lang/String;
    :cond_1e29
    move-object/from16 v0, p0

    #@1e2b
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1e2d
    move-object/from16 v0, p0

    #@1e2f
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1e31
    const/4 v5, 0x0

    #@1e32
    move-object/from16 v0, p0

    #@1e34
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1e36
    add-int/lit8 v6, v6, 0x3

    #@1e38
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1e3b
    move-result-object v4

    #@1e3c
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1e3f
    goto/16 :goto_1227

    #@1e41
    .line 1572
    .end local v27           #numeric:Ljava/lang/String;
    .restart local v10       #arr$:[Ljava/lang/String;
    .restart local v17       #i$:I
    .restart local v23       #len$:I
    .restart local v25       #mccmnc:Ljava/lang/String;
    .restart local v26       #mccmncCode:Ljava/lang/String;
    :cond_1e41
    add-int/lit8 v17, v17, 0x1

    #@1e43
    goto/16 :goto_12a0

    #@1e45
    .line 1592
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_1e45
    const/4 v2, 0x0

    #@1e46
    move-object/from16 v0, p0

    #@1e48
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1e4a
    .line 1594
    const-string v2, "MNC length not present in EF_AD"

    #@1e4c
    move-object/from16 v0, p0

    #@1e4e
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1e51
    goto/16 :goto_12dc

    #@1e53
    .line 1586
    :catch_1e53
    move-exception v13

    #@1e54
    .line 1587
    .restart local v13       #e:Ljava/lang/NumberFormatException;
    const/4 v2, 0x0

    #@1e55
    move-object/from16 v0, p0

    #@1e57
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1e59
    .line 1588
    const-string v2, "Corrupt IMSI!"

    #@1e5b
    move-object/from16 v0, p0

    #@1e5d
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@1e60
    goto/16 :goto_12dc

    #@1e62
    .line 1621
    .end local v13           #e:Ljava/lang/NumberFormatException;
    :cond_1e62
    move-object/from16 v0, p0

    #@1e64
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1e66
    move-object/from16 v0, p0

    #@1e68
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1e6a
    const/4 v5, 0x0

    #@1e6b
    move-object/from16 v0, p0

    #@1e6d
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1e6f
    add-int/lit8 v6, v6, 0x3

    #@1e71
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1e74
    move-result-object v4

    #@1e75
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1e78
    goto/16 :goto_136e

    #@1e7a
    .line 1616
    .restart local v27       #numeric:Ljava/lang/String;
    :cond_1e7a
    move-object/from16 v0, p0

    #@1e7c
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1e7e
    move-object/from16 v0, p0

    #@1e80
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1e82
    const/4 v5, 0x0

    #@1e83
    move-object/from16 v0, p0

    #@1e85
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1e87
    add-int/lit8 v6, v6, 0x3

    #@1e89
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1e8c
    move-result-object v4

    #@1e8d
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1e90
    goto/16 :goto_136e

    #@1e92
    .line 1572
    .end local v27           #numeric:Ljava/lang/String;
    .restart local v10       #arr$:[Ljava/lang/String;
    .restart local v17       #i$:I
    .restart local v23       #len$:I
    .restart local v25       #mccmnc:Ljava/lang/String;
    .restart local v26       #mccmncCode:Ljava/lang/String;
    :cond_1e92
    add-int/lit8 v17, v17, 0x1

    #@1e94
    goto/16 :goto_13ea

    #@1e96
    .line 1592
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v23           #len$:I
    .end local v25           #mccmnc:Ljava/lang/String;
    .end local v26           #mccmncCode:Ljava/lang/String;
    :cond_1e96
    const/4 v2, 0x0

    #@1e97
    move-object/from16 v0, p0

    #@1e99
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1e9b
    .line 1594
    const-string v2, "MNC length not present in EF_AD"

    #@1e9d
    move-object/from16 v0, p0

    #@1e9f
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@1ea2
    goto/16 :goto_1426

    #@1ea4
    .line 1586
    :catch_1ea4
    move-exception v13

    #@1ea5
    .line 1587
    .restart local v13       #e:Ljava/lang/NumberFormatException;
    const/4 v2, 0x0

    #@1ea6
    move-object/from16 v0, p0

    #@1ea8
    iput v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1eaa
    .line 1588
    const-string v2, "Corrupt IMSI!"

    #@1eac
    move-object/from16 v0, p0

    #@1eae
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@1eb1
    goto/16 :goto_1426

    #@1eb3
    .line 1621
    .end local v13           #e:Ljava/lang/NumberFormatException;
    :cond_1eb3
    move-object/from16 v0, p0

    #@1eb5
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1eb7
    move-object/from16 v0, p0

    #@1eb9
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1ebb
    const/4 v5, 0x0

    #@1ebc
    move-object/from16 v0, p0

    #@1ebe
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1ec0
    add-int/lit8 v6, v6, 0x3

    #@1ec2
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1ec5
    move-result-object v4

    #@1ec6
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@1ec9
    goto/16 :goto_14b8

    #@1ecb
    .line 1616
    .restart local v27       #numeric:Ljava/lang/String;
    :cond_1ecb
    move-object/from16 v0, p0

    #@1ecd
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1ecf
    move-object/from16 v0, p0

    #@1ed1
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@1ed3
    const/4 v5, 0x0

    #@1ed4
    move-object/from16 v0, p0

    #@1ed6
    iget v6, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@1ed8
    add-int/lit8 v6, v6, 0x3

    #@1eda
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1edd
    move-result-object v4

    #@1ede
    invoke-static {v2, v4}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1ee1
    .catchall {:try_start_155b .. :try_end_1ee1} :catchall_87
    .catch Ljava/lang/RuntimeException; {:try_start_155b .. :try_end_1ee1} :catch_55

    #@1ee1
    goto/16 :goto_14b8

    #@1ee3
    .line 842
    nop

    #@1ee4
    :sswitch_data_1ee4
    .sparse-switch
        0x1 -> :sswitch_51
        0x3 -> :sswitch_60
        0x4 -> :sswitch_b9d
        0x5 -> :sswitch_850
        0x6 -> :sswitch_8e6
        0x7 -> :sswitch_a51
        0x8 -> :sswitch_b16
        0x9 -> :sswitch_e75
        0xa -> :sswitch_9c8
        0xb -> :sswitch_8e6
        0xc -> :sswitch_161c
        0xd -> :sswitch_16b4
        0xe -> :sswitch_16cf
        0xf -> :sswitch_16e4
        0x11 -> :sswitch_1846
        0x12 -> :sswitch_1727
        0x13 -> :sswitch_1752
        0x14 -> :sswitch_18b9
        0x15 -> :sswitch_1770
        0x16 -> :sswitch_1811
        0x18 -> :sswitch_162c
        0x19 -> :sswitch_1948
        0x1a -> :sswitch_1881
        0x1e -> :sswitch_a30
        0x1f -> :sswitch_199f
        0x20 -> :sswitch_19d0
        0x21 -> :sswitch_1a6a
        0x25 -> :sswitch_cfd
        0x27 -> :sswitch_aea
        0x28 -> :sswitch_aea
        0x29 -> :sswitch_7a1
        0x2a -> :sswitch_805
        0x2c -> :sswitch_6f5
        0x32 -> :sswitch_1c3f
        0x68 -> :sswitch_de1
        0x6e -> :sswitch_1abd
        0xc8 -> :sswitch_3ee
        0xc9 -> :sswitch_4eb
        0xca -> :sswitch_599
        0xcb -> :sswitch_647
        0xcc -> :sswitch_1ca7
    .end sparse-switch
.end method

.method public handleSmsOnIcc(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2204
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3
    check-cast v1, [I

    #@5
    move-object v0, v1

    #@6
    check-cast v0, [I

    #@8
    .line 2206
    .local v0, index:[I
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@a
    if-nez v1, :cond_10

    #@c
    array-length v1, v0

    #@d
    const/4 v2, 0x1

    #@e
    if-eq v1, v2, :cond_34

    #@10
    .line 2207
    :cond_10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, " Error on SMS_ON_SIM with exp "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, " length "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    array-length v2, v0

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@33
    .line 2214
    :goto_33
    return-void

    #@34
    .line 2210
    :cond_34
    new-instance v1, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v2, "READ EF_SMS RECORD index= "

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    aget v2, v0, v3

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@4c
    .line 2211
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@4e
    const/16 v2, 0x6f3c

    #@50
    aget v3, v0, v3

    #@52
    const/16 v4, 0x16

    #@54
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(I)Landroid/os/Message;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V

    #@5b
    goto :goto_33
.end method

.method public isCallForwardStatusStored()Z
    .registers 2

    #@0
    .prologue
    .line 693
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@2
    if-nez v0, :cond_8

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCff:[B

    #@6
    if-eqz v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isCspPlmnEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 3349
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mCspPlmnEnabled:Z

    #@2
    return v0
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 3329
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[SIMRecords] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 3330
    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 3333
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[SIMRecords] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 3334
    return-void
.end method

.method protected logv(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 3341
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[SIMRecords] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 3342
    return-void
.end method

.method protected logw(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 6
    .parameter "s"
    .parameter "tr"

    #@0
    .prologue
    .line 3337
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[SIMRecords] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    .line 3338
    return-void
.end method

.method protected onAllRecordsLoaded()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 2894
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->getOperatorNumeric()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 2898
    .local v0, operator:Ljava/lang/String;
    sget-boolean v1, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@9
    if-eqz v1, :cond_27

    #@b
    .line 2900
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "SIMRecords: onAllRecordsLoaded set \'gsm.sim.operator.numeric\' to operator=\'"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, "\'"

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@27
    .line 2904
    :cond_27
    const-string v1, "gsm.sim.operator.numeric"

    #@29
    invoke-direct {p0, v1, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 2905
    const-string v1, "gsm.apn.sim.operator.numeric"

    #@2e
    invoke-direct {p0, v1, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 2907
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@33
    if-eqz v1, :cond_70

    #@35
    .line 2908
    const-string v1, "gsm.sim.operator.iso-country"

    #@37
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@39
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@40
    move-result v2

    #@41
    invoke-static {v2}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    invoke-direct {p0, v1, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    .line 2916
    :goto_48
    sget-object v1, Lcom/android/internal/telephony/uicc/SIMRecords;->mMccMnc:Ljava/lang/String;

    #@4a
    if-eqz v1, :cond_5f

    #@4c
    .line 2917
    const-string v1, "gsm.sim.operator.iso-country"

    #@4e
    sget-object v2, Lcom/android/internal/telephony/uicc/SIMRecords;->mMccMnc:Ljava/lang/String;

    #@50
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@57
    move-result v2

    #@58
    invoke-static {v2}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5f
    .line 2922
    :cond_5f
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->setVoiceMailByCountry(Ljava/lang/String;)V

    #@62
    .line 2923
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->setSpnFromConfig(Ljava/lang/String;)V

    #@65
    .line 2925
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsLoadedRegistrants:Landroid/os/RegistrantList;

    #@67
    new-instance v2, Landroid/os/AsyncResult;

    #@69
    invoke-direct {v2, v3, v3, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@6c
    invoke-virtual {v1, v2}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@6f
    .line 2927
    return-void

    #@70
    .line 2912
    :cond_70
    const-string v1, "onAllRecordsLoaded: imsi is NULL!"

    #@72
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@75
    goto :goto_48
.end method

.method public onReady()V
    .registers 1

    #@0
    .prologue
    .line 2948
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->fetchSimRecords()V

    #@3
    .line 2949
    return-void
.end method

.method protected onRecordLoaded()V
    .registers 26

    #@0
    .prologue
    .line 2733
    move-object/from16 v0, p0

    #@2
    iget v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@4
    move/from16 v22, v0

    #@6
    add-int/lit8 v22, v22, -0x1

    #@8
    move/from16 v0, v22

    #@a
    move-object/from16 v1, p0

    #@c
    iput v0, v1, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@e
    .line 2734
    new-instance v22, Ljava/lang/StringBuilder;

    #@10
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v23, "onRecordLoaded "

    #@15
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v22

    #@19
    move-object/from16 v0, p0

    #@1b
    iget v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@1d
    move/from16 v23, v0

    #@1f
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v22

    #@23
    const-string v23, " requested: "

    #@25
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v22

    #@29
    move-object/from16 v0, p0

    #@2b
    iget-boolean v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@2d
    move/from16 v23, v0

    #@2f
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@32
    move-result-object v22

    #@33
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v22

    #@37
    move-object/from16 v0, p0

    #@39
    move-object/from16 v1, v22

    #@3b
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@3e
    .line 2736
    move-object/from16 v0, p0

    #@40
    iget v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@42
    move/from16 v22, v0

    #@44
    if-nez v22, :cond_24f

    #@46
    move-object/from16 v0, p0

    #@48
    iget-boolean v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@4a
    move/from16 v22, v0

    #@4c
    const/16 v23, 0x1

    #@4e
    move/from16 v0, v22

    #@50
    move/from16 v1, v23

    #@52
    if-ne v0, v1, :cond_24f

    #@54
    .line 2737
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->onAllRecordsLoaded()V

    #@57
    .line 2739
    sget-boolean v22, Lcom/android/internal/telephony/uicc/SIMRecords;->iccidToLoad:Z

    #@59
    const/16 v23, 0x1

    #@5b
    move/from16 v0, v22

    #@5d
    move/from16 v1, v23

    #@5f
    if-ne v0, v1, :cond_223

    #@61
    sget-boolean v22, Lcom/android/internal/telephony/uicc/SIMRecords;->isSimChanged:Z

    #@63
    const/16 v23, 0x1

    #@65
    move/from16 v0, v22

    #@67
    move/from16 v1, v23

    #@69
    if-ne v0, v1, :cond_223

    #@6b
    .line 2740
    new-instance v11, Landroid/content/Intent;

    #@6d
    const-string v22, "android.intent.action.SIM_CHANGED_INFO"

    #@6f
    move-object/from16 v0, v22

    #@71
    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@74
    .line 2742
    .local v11, intent_sim_changed:Landroid/content/Intent;
    const-string v22, "persist.radio.iccid-changed"

    #@76
    invoke-static/range {v22 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@79
    move-result-object v22

    #@7a
    const-string v23, "1"

    #@7c
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v22

    #@80
    if-eqz v22, :cond_224

    #@82
    .line 2743
    const-string v22, "reason"

    #@84
    const-string v23, "actual"

    #@86
    move-object/from16 v0, v22

    #@88
    move-object/from16 v1, v23

    #@8a
    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@8d
    .line 2748
    :cond_8d
    :goto_8d
    move-object/from16 v0, p0

    #@8f
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@91
    move-object/from16 v22, v0

    #@93
    move-object/from16 v0, v22

    #@95
    invoke-virtual {v0, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@98
    .line 2751
    const/16 v22, 0x0

    #@9a
    sput-boolean v22, Lcom/android/internal/telephony/uicc/SIMRecords;->isSimChanged:Z

    #@9c
    .line 2755
    const-string v22, "1"

    #@9e
    const-string v23, "ro.build.sbp"

    #@a0
    const-string v24, "0"

    #@a2
    invoke-static/range {v23 .. v24}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a5
    move-result-object v23

    #@a6
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a9
    move-result v22

    #@aa
    if-eqz v22, :cond_223

    #@ac
    .line 2756
    const-string v20, "FFFFF"

    #@ae
    .line 2757
    .local v20, sim_mccmnc:Ljava/lang/String;
    const-string v17, "/cust"

    #@b0
    .line 2758
    .local v17, mapping_rootdir:Ljava/lang/String;
    const-string v22, "ro.lge.capp_cupss.rootdir"

    #@b2
    const-string v23, "/cust"

    #@b4
    invoke-static/range {v22 .. v23}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b7
    move-result-object v8

    #@b8
    .line 2760
    .local v8, cupss_rootdir:Ljava/lang/String;
    const/4 v10, 0x0

    #@b9
    .line 2762
    .local v10, displayUI:Z
    move-object/from16 v0, p0

    #@bb
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@bd
    move-object/from16 v22, v0

    #@bf
    if-eqz v22, :cond_e1

    #@c1
    .line 2765
    const-string v22, "3"

    #@c3
    move-object/from16 v0, p0

    #@c5
    iget v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@c7
    move/from16 v23, v0

    #@c9
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cc
    move-result-object v23

    #@cd
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d0
    move-result v22

    #@d1
    if-eqz v22, :cond_23f

    #@d3
    .line 2766
    move-object/from16 v0, p0

    #@d5
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@d7
    move-object/from16 v22, v0

    #@d9
    const/16 v23, 0x0

    #@db
    const/16 v24, 0x6

    #@dd
    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@e0
    move-result-object v20

    #@e1
    .line 2774
    :cond_e1
    :goto_e1
    const/4 v2, 0x0

    #@e2
    .line 2775
    .local v2, br:Ljava/io/BufferedReader;
    const-string v6, "/cust/cust_path_mapping.cfg"

    #@e4
    .line 2776
    .local v6, configFilePath:Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    #@e6
    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@e9
    .line 2778
    .local v7, cupssPathFile:Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    #@ec
    move-result v22

    #@ed
    if-eqz v22, :cond_139

    #@ef
    .line 2780
    :try_start_ef
    new-instance v3, Ljava/io/BufferedReader;

    #@f1
    new-instance v22, Ljava/io/FileReader;

    #@f3
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@f6
    move-result-object v23

    #@f7
    invoke-direct/range {v22 .. v23}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@fa
    move-object/from16 v0, v22

    #@fc
    invoke-direct {v3, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_ff
    .catch Ljava/io/IOException; {:try_start_ef .. :try_end_ff} :catch_269

    #@ff
    .line 2783
    .end local v2           #br:Ljava/io/BufferedReader;
    .local v3, br:Ljava/io/BufferedReader;
    :cond_ff
    :try_start_ff
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@102
    move-result-object v15

    #@103
    .local v15, line:Ljava/lang/String;
    if-eqz v15, :cond_135

    #@105
    .line 2784
    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@108
    move-result-object v15

    #@109
    .line 2785
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    #@10c
    move-result v22

    #@10d
    if-lez v22, :cond_ff

    #@10f
    .line 2786
    const-string v22, "="

    #@111
    move-object/from16 v0, v22

    #@113
    invoke-virtual {v15, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@116
    move-result-object v12

    #@117
    .line 2787
    .local v12, keyValue:[Ljava/lang/String;
    array-length v0, v12

    #@118
    move/from16 v22, v0

    #@11a
    const/16 v23, 0x1

    #@11c
    move/from16 v0, v22

    #@11e
    move/from16 v1, v23

    #@120
    if-le v0, v1, :cond_ff

    #@122
    const/16 v22, 0x0

    #@124
    aget-object v22, v12, v22

    #@126
    move-object/from16 v0, v20

    #@128
    move-object/from16 v1, v22

    #@12a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12d
    move-result v22

    #@12e
    if-eqz v22, :cond_ff

    #@130
    .line 2788
    const/16 v22, 0x1

    #@132
    aget-object v17, v12, v22

    #@134
    .line 2789
    const/4 v10, 0x1

    #@135
    .line 2794
    .end local v12           #keyValue:[Ljava/lang/String;
    :cond_135
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_138
    .catch Ljava/io/IOException; {:try_start_ff .. :try_end_138} :catch_26c

    #@138
    move-object v2, v3

    #@139
    .line 2801
    .end local v3           #br:Ljava/io/BufferedReader;
    .end local v15           #line:Ljava/lang/String;
    .restart local v2       #br:Ljava/io/BufferedReader;
    :cond_139
    :goto_139
    const-string v22, "persist.sys.cupss.backup-status"

    #@13b
    const-string v23, "F"

    #@13d
    invoke-static/range {v22 .. v23}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@140
    move-result-object v19

    #@141
    .line 2803
    .local v19, restoreMode:Ljava/lang/String;
    const-string v22, "1"

    #@143
    move-object/from16 v0, v22

    #@145
    move-object/from16 v1, v19

    #@147
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14a
    move-result v22

    #@14b
    if-eqz v22, :cond_155

    #@14d
    .line 2804
    const-string v22, "GSM"

    #@14f
    const-string v23, "[LGE][SBP] RESTORE Mode support!!"

    #@151
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@154
    .line 2806
    const/4 v10, 0x0

    #@155
    .line 2810
    :cond_155
    const-string v22, "persist.radio.first-mccmnc"

    #@157
    const-string v23, ""

    #@159
    invoke-static/range {v22 .. v23}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@15c
    move-result-object v22

    #@15d
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    #@160
    move-result v22

    #@161
    const/16 v23, 0x5

    #@163
    move/from16 v0, v22

    #@165
    move/from16 v1, v23

    #@167
    if-lt v0, v1, :cond_171

    #@169
    .line 2811
    const-string v22, "GSM"

    #@16b
    const-string v23, "[LGE][SBP] CUST locked"

    #@16d
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@170
    .line 2813
    const/4 v10, 0x0

    #@171
    .line 2817
    :cond_171
    const/16 v22, 0x1

    #@173
    move/from16 v0, v22

    #@175
    if-ne v10, v0, :cond_223

    #@177
    move-object/from16 v0, v17

    #@179
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17c
    move-result v22

    #@17d
    if-nez v22, :cond_223

    #@17f
    .line 2818
    const-string v22, "GSM"

    #@181
    new-instance v23, Ljava/lang/StringBuilder;

    #@183
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@186
    const-string v24, "[LGE][SBP] CUST setting MCC,MNC:"

    #@188
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v23

    #@18c
    move-object/from16 v0, v23

    #@18e
    move-object/from16 v1, v20

    #@190
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@193
    move-result-object v23

    #@194
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@197
    move-result-object v23

    #@198
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19b
    .line 2821
    move-object/from16 v0, p0

    #@19d
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@19f
    move-object/from16 v22, v0

    #@1a1
    const-string v23, "keyguard"

    #@1a3
    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1a6
    move-result-object v14

    #@1a7
    check-cast v14, Landroid/app/KeyguardManager;

    #@1a9
    .line 2822
    .local v14, km:Landroid/app/KeyguardManager;
    const-string v22, "keyguard"

    #@1ab
    move-object/from16 v0, v22

    #@1ad
    invoke-virtual {v14, v0}, Landroid/app/KeyguardManager;->newKeyguardLock(Ljava/lang/String;)Landroid/app/KeyguardManager$KeyguardLock;

    #@1b0
    move-result-object v13

    #@1b1
    .line 2823
    .local v13, kl:Landroid/app/KeyguardManager$KeyguardLock;
    invoke-virtual {v13}, Landroid/app/KeyguardManager$KeyguardLock;->disableKeyguard()V

    #@1b4
    .line 2826
    move-object/from16 v0, p0

    #@1b6
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1b8
    move-object/from16 v22, v0

    #@1ba
    move-object/from16 v0, p0

    #@1bc
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->custCompletedReceiver:Landroid/content/BroadcastReceiver;

    #@1be
    move-object/from16 v23, v0

    #@1c0
    move-object/from16 v0, p0

    #@1c2
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/SIMRecords;->filter:Landroid/content/IntentFilter;

    #@1c4
    move-object/from16 v24, v0

    #@1c6
    invoke-virtual/range {v22 .. v24}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1c9
    .line 2828
    new-instance v16, Lcom/android/internal/telephony/uicc/SIMRecords$3;

    #@1cb
    move-object/from16 v0, v16

    #@1cd
    move-object/from16 v1, p0

    #@1cf
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/uicc/SIMRecords$3;-><init>(Lcom/android/internal/telephony/uicc/SIMRecords;)V

    #@1d2
    .line 2865
    .local v16, listener:Landroid/content/DialogInterface$OnClickListener;
    const-string v21, "Warning"

    #@1d4
    .line 2866
    .local v21, title:Ljava/lang/String;
    const-string v18, "New SIM Detected. Need to apply new settings, require reboot. Do you want to reboot now?"

    #@1d6
    .line 2867
    .local v18, message:Ljava/lang/String;
    const-string v5, "Ok"

    #@1d8
    .line 2868
    .local v5, buttonOkTxt:Ljava/lang/String;
    const-string v4, "Cancel"

    #@1da
    .line 2870
    .local v4, buttonCancelTxt:Ljava/lang/String;
    new-instance v22, Landroid/app/AlertDialog$Builder;

    #@1dc
    move-object/from16 v0, p0

    #@1de
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1e0
    move-object/from16 v23, v0

    #@1e2
    invoke-direct/range {v22 .. v23}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@1e5
    move-object/from16 v0, v22

    #@1e7
    move-object/from16 v1, v21

    #@1e9
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@1ec
    move-result-object v22

    #@1ed
    move-object/from16 v0, v22

    #@1ef
    move-object/from16 v1, v18

    #@1f1
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@1f4
    move-result-object v22

    #@1f5
    move-object/from16 v0, v22

    #@1f7
    move-object/from16 v1, v16

    #@1f9
    invoke-virtual {v0, v5, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@1fc
    move-result-object v22

    #@1fd
    move-object/from16 v0, v22

    #@1ff
    move-object/from16 v1, v16

    #@201
    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@204
    move-result-object v22

    #@205
    invoke-virtual/range {v22 .. v22}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@208
    move-result-object v9

    #@209
    .line 2877
    .local v9, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v9}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@20c
    move-result-object v22

    #@20d
    const/16 v23, 0x7d3

    #@20f
    invoke-virtual/range {v22 .. v23}, Landroid/view/Window;->setType(I)V

    #@212
    .line 2878
    const/16 v22, 0x0

    #@214
    move/from16 v0, v22

    #@216
    invoke-virtual {v9, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    #@219
    .line 2879
    const/16 v22, 0x0

    #@21b
    move/from16 v0, v22

    #@21d
    invoke-virtual {v9, v0}, Landroid/app/AlertDialog;->setCancelable(Z)V

    #@220
    .line 2880
    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    #@223
    .line 2891
    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v4           #buttonCancelTxt:Ljava/lang/String;
    .end local v5           #buttonOkTxt:Ljava/lang/String;
    .end local v6           #configFilePath:Ljava/lang/String;
    .end local v7           #cupssPathFile:Ljava/io/File;
    .end local v8           #cupss_rootdir:Ljava/lang/String;
    .end local v9           #dialog:Landroid/app/AlertDialog;
    .end local v10           #displayUI:Z
    .end local v11           #intent_sim_changed:Landroid/content/Intent;
    .end local v13           #kl:Landroid/app/KeyguardManager$KeyguardLock;
    .end local v14           #km:Landroid/app/KeyguardManager;
    .end local v16           #listener:Landroid/content/DialogInterface$OnClickListener;
    .end local v17           #mapping_rootdir:Ljava/lang/String;
    .end local v18           #message:Ljava/lang/String;
    .end local v19           #restoreMode:Ljava/lang/String;
    .end local v20           #sim_mccmnc:Ljava/lang/String;
    .end local v21           #title:Ljava/lang/String;
    :cond_223
    :goto_223
    return-void

    #@224
    .line 2744
    .restart local v11       #intent_sim_changed:Landroid/content/Intent;
    :cond_224
    const-string v22, "persist.radio.iccid-changed"

    #@226
    invoke-static/range {v22 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@229
    move-result-object v22

    #@22a
    const-string v23, "2"

    #@22c
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22f
    move-result v22

    #@230
    if-eqz v22, :cond_8d

    #@232
    .line 2745
    const-string v22, "reason"

    #@234
    const-string v23, "first"

    #@236
    move-object/from16 v0, v22

    #@238
    move-object/from16 v1, v23

    #@23a
    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@23d
    goto/16 :goto_8d

    #@23f
    .line 2769
    .restart local v8       #cupss_rootdir:Ljava/lang/String;
    .restart local v10       #displayUI:Z
    .restart local v17       #mapping_rootdir:Ljava/lang/String;
    .restart local v20       #sim_mccmnc:Ljava/lang/String;
    :cond_23f
    move-object/from16 v0, p0

    #@241
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@243
    move-object/from16 v22, v0

    #@245
    const/16 v23, 0x0

    #@247
    const/16 v24, 0x5

    #@249
    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@24c
    move-result-object v20

    #@24d
    goto/16 :goto_e1

    #@24f
    .line 2887
    .end local v8           #cupss_rootdir:Ljava/lang/String;
    .end local v10           #displayUI:Z
    .end local v11           #intent_sim_changed:Landroid/content/Intent;
    .end local v17           #mapping_rootdir:Ljava/lang/String;
    .end local v20           #sim_mccmnc:Ljava/lang/String;
    :cond_24f
    move-object/from16 v0, p0

    #@251
    iget v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@253
    move/from16 v22, v0

    #@255
    if-gez v22, :cond_223

    #@257
    .line 2888
    const-string v22, "recordsToLoad <0, programmer error suspected"

    #@259
    move-object/from16 v0, p0

    #@25b
    move-object/from16 v1, v22

    #@25d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/SIMRecords;->loge(Ljava/lang/String;)V

    #@260
    .line 2889
    const/16 v22, 0x0

    #@262
    move/from16 v0, v22

    #@264
    move-object/from16 v1, p0

    #@266
    iput v0, v1, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@268
    goto :goto_223

    #@269
    .line 2795
    .restart local v2       #br:Ljava/io/BufferedReader;
    .restart local v6       #configFilePath:Ljava/lang/String;
    .restart local v7       #cupssPathFile:Ljava/io/File;
    .restart local v8       #cupss_rootdir:Ljava/lang/String;
    .restart local v10       #displayUI:Z
    .restart local v11       #intent_sim_changed:Landroid/content/Intent;
    .restart local v17       #mapping_rootdir:Ljava/lang/String;
    .restart local v20       #sim_mccmnc:Ljava/lang/String;
    :catch_269
    move-exception v22

    #@26a
    goto/16 :goto_139

    #@26c
    .end local v2           #br:Ljava/io/BufferedReader;
    .restart local v3       #br:Ljava/io/BufferedReader;
    :catch_26c
    move-exception v22

    #@26d
    move-object v2, v3

    #@26e
    .end local v3           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    goto/16 :goto_139
.end method

.method public onRefresh(Z[I)V
    .registers 3
    .parameter "fileChanged"
    .parameter "fileList"

    #@0
    .prologue
    .line 784
    if-eqz p1, :cond_5

    #@2
    .line 788
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->fetchSimRecords()V

    #@5
    .line 790
    :cond_5
    return-void
.end method

.method protected resetRecords()V
    .registers 5

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 404
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@5
    .line 405
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@7
    .line 406
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->voiceMailNum:Ljava/lang/String;

    #@9
    .line 407
    iput v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->countVoiceMessages:I

    #@b
    .line 408
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@d
    .line 409
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@f
    .line 411
    iput v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spnDisplayCondition:I

    #@11
    .line 412
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@13
    .line 413
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efCPHS_MWI:[B

    #@15
    .line 414
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->spdiNetworks:Ljava/util/ArrayList;

    #@17
    .line 415
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->pnnHomeName:Ljava/lang/String;

    #@19
    .line 418
    sput v3, Lcom/android/internal/telephony/uicc/SIMRecords;->FDN_NAME_MAX:I

    #@1b
    .line 419
    sput v3, Lcom/android/internal/telephony/uicc/SIMRecords;->FDN_REC_NUM:I

    #@1d
    .line 423
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    const-string v1, "KR"

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v0

    #@27
    if-nez v0, :cond_30

    #@29
    .line 424
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->gid1:Ljava/lang/String;

    #@2b
    .line 425
    const-string v0, "gsm.sim.operator.gid"

    #@2d
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@30
    .line 428
    :cond_30
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->adnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@32
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->reset()V

    #@35
    .line 430
    const-string v0, "SIMRecords: onRadioOffOrNotAvailable set \'gsm.sim.operator.numeric\' to operator=null"

    #@37
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@3a
    .line 432
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@3d
    move-result-object v0

    #@3e
    const-string v1, "DCM"

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v0

    #@44
    if-nez v0, :cond_50

    #@46
    .line 433
    const-string v0, "gsm.sim.operator.alpha"

    #@48
    invoke-direct {p0, v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@4b
    .line 434
    const-string v0, "gsm.sim.operator.iso-country"

    #@4d
    invoke-direct {p0, v0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@50
    .line 440
    :cond_50
    iput-boolean v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@52
    .line 441
    return-void
.end method

.method public setMsisdnNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 11
    .parameter "alphaTag"
    .parameter "number"
    .parameter "onComplete"

    #@0
    .prologue
    const/16 v2, 0x6f40

    #@2
    .line 503
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@4
    .line 504
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdnTag:Ljava/lang/String;

    #@6
    .line 506
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Set MSISDN: "

    #@d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdnTag:Ljava/lang/String;

    #@13
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v3, " "

    #@19
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v3, "xxxxxxx"

    #@1f
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@2a
    .line 508
    new-instance v1, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@2c
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdnTag:Ljava/lang/String;

    #@2e
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->msisdn:Ljava/lang/String;

    #@30
    invoke-direct {v1, v0, v3}, Lcom/android/internal/telephony/uicc/AdnRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    .line 510
    .local v1, adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@35
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@37
    invoke-direct {v0, v3}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@3a
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->getExtFromEf(I)I

    #@3d
    move-result v3

    #@3e
    const/4 v4, 0x1

    #@3f
    const/4 v5, 0x0

    #@40
    const/16 v6, 0x1e

    #@42
    invoke-virtual {p0, v6, p3}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@45
    move-result-object v6

    #@46
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->updateEF(Lcom/android/internal/telephony/uicc/AdnRecord;IIILjava/lang/String;Landroid/os/Message;)V

    #@49
    .line 513
    return-void
.end method

.method public setVoiceCallForwardingFlag(IZ)V
    .registers 4
    .parameter "line"
    .parameter "enable"

    #@0
    .prologue
    .line 709
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->setVoiceCallForwardingFlag(IZLjava/lang/String;)V

    #@4
    .line 710
    return-void
.end method

.method public setVoiceCallForwardingFlag(IZLjava/lang/String;)V
    .registers 13
    .parameter "line"
    .parameter "enable"
    .parameter "dialNumber"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 719
    if-eq p1, v1, :cond_4

    #@3
    .line 776
    :cond_3
    :goto_3
    return-void

    #@4
    .line 721
    :cond_4
    iput-boolean p2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->callForwardingEnabled:Z

    #@6
    .line 723
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mRecordsEventsRegistrants:Landroid/os/RegistrantList;

    #@8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@f
    .line 726
    :try_start_f
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@11
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->validEfCfis([B)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_e5

    #@17
    .line 728
    if-eqz p2, :cond_d9

    #@19
    .line 729
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@1b
    const/4 v1, 0x1

    #@1c
    aget-byte v2, v0, v1

    #@1e
    or-int/lit8 v2, v2, 0x1

    #@20
    int-to-byte v2, v2

    #@21
    aput-byte v2, v0, v1

    #@23
    .line 734
    :goto_23
    new-instance v0, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v1, "setVoiceCallForwardingFlag: enable="

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    const-string v1, " mEfCfis="

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@3a
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v0

    #@46
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@49
    .line 739
    if-eqz p2, :cond_8e

    #@4b
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4e
    move-result v0

    #@4f
    if-nez v0, :cond_8e

    #@51
    .line 740
    sget-boolean v0, Lcom/android/internal/telephony/uicc/SIMRecords;->ENABLE_PRIVACY_LOG:Z

    #@53
    if-eqz v0, :cond_6d

    #@55
    const-string v0, "GSM"

    #@57
    new-instance v1, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v2, "EF_CFIS: updating cf number, "

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v1

    #@6a
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 741
    :cond_6d
    invoke-static {p3}, Landroid/telephony/PhoneNumberUtils;->numberToCalledPartyBCD(Ljava/lang/String;)[B

    #@70
    move-result-object v6

    #@71
    .line 743
    .local v6, bcdNumber:[B
    const/4 v0, 0x0

    #@72
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@74
    const/4 v2, 0x3

    #@75
    array-length v3, v6

    #@76
    invoke-static {v6, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@79
    .line 745
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@7b
    const/4 v1, 0x2

    #@7c
    array-length v2, v6

    #@7d
    int-to-byte v2, v2

    #@7e
    aput-byte v2, v0, v1

    #@80
    .line 746
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@82
    const/16 v1, 0xe

    #@84
    const/4 v2, -0x1

    #@85
    aput-byte v2, v0, v1

    #@87
    .line 747
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@89
    const/16 v1, 0xf

    #@8b
    const/4 v2, -0x1

    #@8c
    aput-byte v2, v0, v1

    #@8e
    .line 750
    .end local v6           #bcdNumber:[B
    :cond_8e
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@90
    const/16 v1, 0x6fcb

    #@92
    const/4 v2, 0x1

    #@93
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@95
    const/4 v4, 0x0

    #@96
    const/16 v5, 0xe

    #@98
    const/16 v8, 0x6fcb

    #@9a
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9d
    move-result-object v8

    #@9e
    invoke-virtual {p0, v5, v8}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a1
    move-result-object v5

    #@a2
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFLinearFixed(II[BLjava/lang/String;Landroid/os/Message;)V

    #@a5
    .line 758
    :goto_a5
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCff:[B

    #@a7
    if-eqz v0, :cond_3

    #@a9
    .line 759
    if-eqz p2, :cond_10c

    #@ab
    .line 760
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCff:[B

    #@ad
    const/4 v1, 0x0

    #@ae
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCff:[B

    #@b0
    const/4 v3, 0x0

    #@b1
    aget-byte v2, v2, v3

    #@b3
    and-int/lit16 v2, v2, 0xf0

    #@b5
    or-int/lit8 v2, v2, 0xa

    #@b7
    int-to-byte v2, v2

    #@b8
    aput-byte v2, v0, v1

    #@ba
    .line 767
    :goto_ba
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@bc
    const/16 v1, 0x6f13

    #@be
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCff:[B

    #@c0
    const/16 v3, 0xe

    #@c2
    const/16 v4, 0x6f13

    #@c4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c7
    move-result-object v4

    #@c8
    invoke-virtual {p0, v3, v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@cb
    move-result-object v3

    #@cc
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFTransparent(I[BLandroid/os/Message;)V
    :try_end_cf
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_f .. :try_end_cf} :catch_d1

    #@cf
    goto/16 :goto_3

    #@d1
    .line 771
    :catch_d1
    move-exception v7

    #@d2
    .line 772
    .local v7, ex:Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v0, "Error saving call fowarding flag to SIM. Probably malformed SIM record"

    #@d4
    invoke-virtual {p0, v0, v7}, Lcom/android/internal/telephony/uicc/SIMRecords;->logw(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@d7
    goto/16 :goto_3

    #@d9
    .line 731
    .end local v7           #ex:Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_d9
    :try_start_d9
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@db
    const/4 v1, 0x1

    #@dc
    aget-byte v2, v0, v1

    #@de
    and-int/lit16 v2, v2, 0xfe

    #@e0
    int-to-byte v2, v2

    #@e1
    aput-byte v2, v0, v1

    #@e3
    goto/16 :goto_23

    #@e5
    .line 754
    :cond_e5
    new-instance v0, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v1, "setVoiceCallForwardingFlag: ignoring enable="

    #@ec
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v0

    #@f0
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v0

    #@f4
    const-string v1, " invalid mEfCfis="

    #@f6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v0

    #@fa
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCfis:[B

    #@fc
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@ff
    move-result-object v1

    #@100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v0

    #@104
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@107
    move-result-object v0

    #@108
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/SIMRecords;->log(Ljava/lang/String;)V

    #@10b
    goto :goto_a5

    #@10c
    .line 763
    :cond_10c
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCff:[B

    #@10e
    const/4 v1, 0x0

    #@10f
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->mEfCff:[B

    #@111
    const/4 v3, 0x0

    #@112
    aget-byte v2, v2, v3

    #@114
    and-int/lit16 v2, v2, 0xf0

    #@116
    or-int/lit8 v2, v2, 0x5

    #@118
    int-to-byte v2, v2

    #@119
    aput-byte v2, v0, v1
    :try_end_11b
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_d9 .. :try_end_11b} :catch_d1

    #@11b
    goto :goto_ba
.end method

.method public setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 11
    .parameter "alphaTag"
    .parameter "voiceNumber"
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 549
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->isVoiceMailFixed:Z

    #@3
    if-eqz v0, :cond_16

    #@5
    .line 550
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@8
    move-result-object v0

    #@9
    new-instance v2, Lcom/android/internal/telephony/uicc/IccVmFixedException;

    #@b
    const-string v3, "Voicemail number is fixed by operator"

    #@d
    invoke-direct {v2, v3}, Lcom/android/internal/telephony/uicc/IccVmFixedException;-><init>(Ljava/lang/String;)V

    #@10
    iput-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@12
    .line 552
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@15
    .line 588
    :goto_15
    return-void

    #@16
    .line 556
    :cond_16
    const-string v0, "23430"

    #@18
    const-string v2, "gsm.sim.operator.numeric"

    #@1a
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_39

    #@24
    .line 557
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mVmsIsNotInSIM:Z

    #@26
    if-eqz v0, :cond_39

    #@28
    .line 558
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@2b
    move-result-object v0

    #@2c
    new-instance v2, Lcom/android/internal/telephony/uicc/IccVmNotSupportedException;

    #@2e
    const-string v3, "Voicemail number is fixed by operator"

    #@30
    invoke-direct {v2, v3}, Lcom/android/internal/telephony/uicc/IccVmNotSupportedException;-><init>(Ljava/lang/String;)V

    #@33
    iput-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@35
    .line 560
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@38
    goto :goto_15

    #@39
    .line 566
    :cond_39
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->newVoiceMailNum:Ljava/lang/String;

    #@3b
    .line 567
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->newVoiceMailTag:Ljava/lang/String;

    #@3d
    .line 569
    new-instance v1, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@3f
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->newVoiceMailTag:Ljava/lang/String;

    #@41
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->newVoiceMailNum:Ljava/lang/String;

    #@43
    invoke-direct {v1, v0, v2}, Lcom/android/internal/telephony/uicc/AdnRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 571
    .local v1, adn:Lcom/android/internal/telephony/uicc/AdnRecord;
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mailboxIndex:I

    #@48
    if-eqz v0, :cond_67

    #@4a
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mailboxIndex:I

    #@4c
    const/16 v2, 0xff

    #@4e
    if-eq v0, v2, :cond_67

    #@50
    .line 573
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@52
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@54
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@57
    const/16 v2, 0x6fc7

    #@59
    const/16 v3, 0x6fc8

    #@5b
    iget v4, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mailboxIndex:I

    #@5d
    const/16 v6, 0x14

    #@5f
    invoke-virtual {p0, v6, p3}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@62
    move-result-object v6

    #@63
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->updateEF(Lcom/android/internal/telephony/uicc/AdnRecord;IIILjava/lang/String;Landroid/os/Message;)V

    #@66
    goto :goto_15

    #@67
    .line 577
    :cond_67
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/SIMRecords;->isCphsMailboxEnabled()Z

    #@6a
    move-result v0

    #@6b
    if-eqz v0, :cond_83

    #@6d
    .line 579
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordLoader;

    #@6f
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@71
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@74
    const/16 v2, 0x6f17

    #@76
    const/16 v3, 0x6f4a

    #@78
    const/4 v4, 0x1

    #@79
    const/16 v6, 0x19

    #@7b
    invoke-virtual {p0, v6, p3}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@7e
    move-result-object v6

    #@7f
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/uicc/AdnRecordLoader;->updateEF(Lcom/android/internal/telephony/uicc/AdnRecord;IIILjava/lang/String;Landroid/os/Message;)V

    #@82
    goto :goto_15

    #@83
    .line 584
    :cond_83
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@86
    move-result-object v0

    #@87
    new-instance v2, Lcom/android/internal/telephony/uicc/IccVmNotSupportedException;

    #@89
    const-string v3, "Update SIM voice mailbox error"

    #@8b
    invoke-direct {v2, v3}, Lcom/android/internal/telephony/uicc/IccVmNotSupportedException;-><init>(Ljava/lang/String;)V

    #@8e
    iput-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@90
    .line 586
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@93
    goto :goto_15
.end method

.method public setVoiceMessageWaiting(IILandroid/os/Message;)V
    .registers 13
    .parameter "line"
    .parameter "countWaiting"
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 607
    if-eq p1, v1, :cond_5

    #@4
    .line 649
    :goto_4
    return-void

    #@5
    .line 613
    :cond_5
    :try_start_5
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@7
    if-eqz v2, :cond_46

    #@9
    .line 617
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@b
    const/4 v3, 0x0

    #@c
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@e
    const/4 v5, 0x0

    #@f
    aget-byte v4, v4, v5

    #@11
    and-int/lit16 v4, v4, 0xfe

    #@13
    if-nez p2, :cond_3d

    #@15
    :goto_15
    or-int/2addr v0, v4

    #@16
    int-to-byte v0, v0

    #@17
    aput-byte v0, v2, v3

    #@19
    .line 621
    if-gez p2, :cond_3f

    #@1b
    .line 624
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@1d
    const/4 v1, 0x1

    #@1e
    const/4 v2, 0x0

    #@1f
    aput-byte v2, v0, v1

    #@21
    .line 629
    :goto_21
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@23
    const/16 v1, 0x6fca

    #@25
    const/4 v2, 0x1

    #@26
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@28
    const/4 v4, 0x0

    #@29
    const/16 v5, 0x27

    #@2b
    const/16 v7, 0x6fca

    #@2d
    const/4 v8, 0x0

    #@2e
    invoke-virtual {p0, v5, v7, v8, p3}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@31
    move-result-object v5

    #@32
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFLinearFixed(II[BLjava/lang/String;Landroid/os/Message;)V
    :try_end_35
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_5 .. :try_end_35} :catch_36

    #@35
    goto :goto_4

    #@36
    .line 646
    :catch_36
    move-exception v6

    #@37
    .line 647
    .local v6, ex:Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v0, "Error saving voice mail state to SIM. Probably malformed SIM record"

    #@39
    invoke-virtual {p0, v0, v6}, Lcom/android/internal/telephony/uicc/SIMRecords;->logw(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3c
    goto :goto_4

    #@3d
    .end local v6           #ex:Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_3d
    move v0, v1

    #@3e
    .line 617
    goto :goto_15

    #@3f
    .line 626
    :cond_3f
    :try_start_3f
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efMWIS:[B

    #@41
    const/4 v1, 0x1

    #@42
    int-to-byte v2, p2

    #@43
    aput-byte v2, v0, v1

    #@45
    goto :goto_21

    #@46
    .line 632
    :cond_46
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efCPHS_MWI:[B

    #@48
    if-eqz v0, :cond_71

    #@4a
    .line 634
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efCPHS_MWI:[B

    #@4c
    const/4 v2, 0x0

    #@4d
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efCPHS_MWI:[B

    #@4f
    const/4 v3, 0x0

    #@50
    aget-byte v0, v0, v3

    #@52
    and-int/lit16 v3, v0, 0xf0

    #@54
    if-nez p2, :cond_6e

    #@56
    const/4 v0, 0x5

    #@57
    :goto_57
    or-int/2addr v0, v3

    #@58
    int-to-byte v0, v0

    #@59
    aput-byte v0, v1, v2

    #@5b
    .line 636
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@5d
    const/16 v1, 0x6f11

    #@5f
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords;->efCPHS_MWI:[B

    #@61
    const/16 v3, 0x28

    #@63
    const/16 v4, 0x6f11

    #@65
    const/4 v5, 0x0

    #@66
    invoke-virtual {p0, v3, v4, v5, p3}, Lcom/android/internal/telephony/uicc/SIMRecords;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFTransparent(I[BLandroid/os/Message;)V

    #@6d
    goto :goto_4

    #@6e
    .line 634
    :cond_6e
    const/16 v0, 0xa

    #@70
    goto :goto_57

    #@71
    .line 641
    :cond_71
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@74
    move-result-object v0

    #@75
    new-instance v1, Lcom/android/internal/telephony/uicc/IccVmNotSupportedException;

    #@77
    const-string v2, "SIM does not support EF_MWIS & EF_CPHS_MWIS"

    #@79
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/uicc/IccVmNotSupportedException;-><init>(Ljava/lang/String;)V

    #@7c
    iput-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@7e
    .line 644
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V
    :try_end_81
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3f .. :try_end_81} :catch_36

    #@81
    goto :goto_4
.end method

.method public updateCurrentIccUSimMessageProvider(Ljava/util/ArrayList;)Z
    .registers 14
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p1, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    const/4 v7, 0x0

    #@1
    .line 2310
    :try_start_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@4
    move-result v1

    #@5
    .line 2311
    .local v1, count:I
    new-instance v6, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v8, "updateCurrentIccUSimMessageProvider(), messages size= "

    #@c
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v6

    #@18
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1b
    .line 2312
    const/4 v4, 0x0

    #@1c
    .local v4, i:I
    :goto_1c
    if-ge v4, v1, :cond_69

    #@1e
    .line 2313
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, [B

    #@24
    .line 2314
    .local v0, ba:[B
    const/4 v6, 0x0

    #@25
    aget-byte v6, v0, v6

    #@27
    if-eqz v6, :cond_43

    #@29
    .line 2315
    new-instance v2, Lcom/android/internal/telephony/SmsRawData;

    #@2b
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v6

    #@2f
    check-cast v6, [B

    #@31
    invoke-direct {v2, v6}, Lcom/android/internal/telephony/SmsRawData;-><init>([B)V

    #@34
    .line 2316
    .local v2, data:Lcom/android/internal/telephony/SmsRawData;
    add-int/lit8 v6, v4, 0x1

    #@36
    invoke-virtual {v2}, Lcom/android/internal/telephony/SmsRawData;->getBytes()[B

    #@39
    move-result-object v8

    #@3a
    invoke-static {v6, v8}, Lcom/android/internal/telephony/gsm/SmsMessage;->createFromEfRecord(I[B)Lcom/android/internal/telephony/gsm/SmsMessage;

    #@3d
    move-result-object v5

    #@3e
    .line 2317
    .local v5, sms:Lcom/android/internal/telephony/gsm/SmsMessage;
    if-eqz v5, :cond_46

    #@40
    .line 2318
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/uicc/SIMRecords;->updateMessagefromUSim(Lcom/android/internal/telephony/gsm/SmsMessage;)V

    #@43
    .line 2312
    .end local v2           #data:Lcom/android/internal/telephony/SmsRawData;
    .end local v5           #sms:Lcom/android/internal/telephony/gsm/SmsMessage;
    :cond_43
    :goto_43
    add-int/lit8 v4, v4, 0x1

    #@45
    goto :goto_1c

    #@46
    .line 2320
    .restart local v2       #data:Lcom/android/internal/telephony/SmsRawData;
    .restart local v5       #sms:Lcom/android/internal/telephony/gsm/SmsMessage;
    :cond_46
    const-string v6, "updateCurrentIccUSimMessageProvider(), SmsMessage is null. It is created from createFromUSimEfRecord()."

    #@48
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_4b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_4b} :catch_4c
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_4b} :catch_7a

    #@4b
    goto :goto_43

    #@4c
    .line 2328
    .end local v0           #ba:[B
    .end local v1           #count:I
    .end local v2           #data:Lcom/android/internal/telephony/SmsRawData;
    .end local v4           #i:I
    .end local v5           #sms:Lcom/android/internal/telephony/gsm/SmsMessage;
    :catch_4c
    move-exception v3

    #@4d
    .line 2329
    .local v3, e:Ljava/lang/IllegalArgumentException;
    new-instance v6, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v8, "updateCurrentIccUSimMessageProvider(), IllegalArgumentException"

    #@54
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    #@5b
    move-result-object v8

    #@5c
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v6

    #@60
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v6

    #@64
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@67
    .end local v3           #e:Ljava/lang/IllegalArgumentException;
    :goto_67
    move v6, v7

    #@68
    .line 2334
    :goto_68
    return v6

    #@69
    .line 2325
    .restart local v1       #count:I
    .restart local v4       #i:I
    :cond_69
    :try_start_69
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@6b
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@6d
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@70
    move-result-object v8

    #@71
    sget-object v9, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@73
    const/4 v10, 0x0

    #@74
    const/4 v11, 0x0

    #@75
    invoke-static {v6, v8, v9, v10, v11}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_78
    .catch Ljava/lang/IllegalArgumentException; {:try_start_69 .. :try_end_78} :catch_4c
    .catch Landroid/database/SQLException; {:try_start_69 .. :try_end_78} :catch_7a

    #@78
    .line 2326
    const/4 v6, 0x1

    #@79
    goto :goto_68

    #@7a
    .line 2331
    .end local v1           #count:I
    .end local v4           #i:I
    :catch_7a
    move-exception v3

    #@7b
    .line 2332
    .local v3, e:Landroid/database/SQLException;
    const-string v6, "updateCurrentIccUSimMessageProvider(), Can\'t store current SMS in SIM to SQLite DB"

    #@7d
    invoke-static {v6, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@80
    goto :goto_67
.end method
