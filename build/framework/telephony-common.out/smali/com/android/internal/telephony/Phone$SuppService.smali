.class public final enum Lcom/android/internal/telephony/Phone$SuppService;
.super Ljava/lang/Enum;
.source "Phone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/Phone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SuppService"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/Phone$SuppService;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/Phone$SuppService;

.field public static final enum CONFERENCE:Lcom/android/internal/telephony/Phone$SuppService;

.field public static final enum HANGUP:Lcom/android/internal/telephony/Phone$SuppService;

.field public static final enum REJECT:Lcom/android/internal/telephony/Phone$SuppService;

.field public static final enum SEPARATE:Lcom/android/internal/telephony/Phone$SuppService;

.field public static final enum SWITCH:Lcom/android/internal/telephony/Phone$SuppService;

.field public static final enum TRANSFER:Lcom/android/internal/telephony/Phone$SuppService;

.field public static final enum UNKNOWN:Lcom/android/internal/telephony/Phone$SuppService;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 89
    new-instance v0, Lcom/android/internal/telephony/Phone$SuppService;

    #@7
    const-string v1, "UNKNOWN"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/Phone$SuppService;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/Phone$SuppService;->UNKNOWN:Lcom/android/internal/telephony/Phone$SuppService;

    #@e
    new-instance v0, Lcom/android/internal/telephony/Phone$SuppService;

    #@10
    const-string v1, "SWITCH"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/Phone$SuppService;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/Phone$SuppService;->SWITCH:Lcom/android/internal/telephony/Phone$SuppService;

    #@17
    new-instance v0, Lcom/android/internal/telephony/Phone$SuppService;

    #@19
    const-string v1, "SEPARATE"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/Phone$SuppService;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/Phone$SuppService;->SEPARATE:Lcom/android/internal/telephony/Phone$SuppService;

    #@20
    new-instance v0, Lcom/android/internal/telephony/Phone$SuppService;

    #@22
    const-string v1, "TRANSFER"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/Phone$SuppService;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/Phone$SuppService;->TRANSFER:Lcom/android/internal/telephony/Phone$SuppService;

    #@29
    new-instance v0, Lcom/android/internal/telephony/Phone$SuppService;

    #@2b
    const-string v1, "CONFERENCE"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/internal/telephony/Phone$SuppService;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/Phone$SuppService;->CONFERENCE:Lcom/android/internal/telephony/Phone$SuppService;

    #@32
    new-instance v0, Lcom/android/internal/telephony/Phone$SuppService;

    #@34
    const-string v1, "REJECT"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Phone$SuppService;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/internal/telephony/Phone$SuppService;->REJECT:Lcom/android/internal/telephony/Phone$SuppService;

    #@3c
    new-instance v0, Lcom/android/internal/telephony/Phone$SuppService;

    #@3e
    const-string v1, "HANGUP"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Phone$SuppService;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/android/internal/telephony/Phone$SuppService;->HANGUP:Lcom/android/internal/telephony/Phone$SuppService;

    #@46
    .line 88
    const/4 v0, 0x7

    #@47
    new-array v0, v0, [Lcom/android/internal/telephony/Phone$SuppService;

    #@49
    sget-object v1, Lcom/android/internal/telephony/Phone$SuppService;->UNKNOWN:Lcom/android/internal/telephony/Phone$SuppService;

    #@4b
    aput-object v1, v0, v3

    #@4d
    sget-object v1, Lcom/android/internal/telephony/Phone$SuppService;->SWITCH:Lcom/android/internal/telephony/Phone$SuppService;

    #@4f
    aput-object v1, v0, v4

    #@51
    sget-object v1, Lcom/android/internal/telephony/Phone$SuppService;->SEPARATE:Lcom/android/internal/telephony/Phone$SuppService;

    #@53
    aput-object v1, v0, v5

    #@55
    sget-object v1, Lcom/android/internal/telephony/Phone$SuppService;->TRANSFER:Lcom/android/internal/telephony/Phone$SuppService;

    #@57
    aput-object v1, v0, v6

    #@59
    sget-object v1, Lcom/android/internal/telephony/Phone$SuppService;->CONFERENCE:Lcom/android/internal/telephony/Phone$SuppService;

    #@5b
    aput-object v1, v0, v7

    #@5d
    const/4 v1, 0x5

    #@5e
    sget-object v2, Lcom/android/internal/telephony/Phone$SuppService;->REJECT:Lcom/android/internal/telephony/Phone$SuppService;

    #@60
    aput-object v2, v0, v1

    #@62
    const/4 v1, 0x6

    #@63
    sget-object v2, Lcom/android/internal/telephony/Phone$SuppService;->HANGUP:Lcom/android/internal/telephony/Phone$SuppService;

    #@65
    aput-object v2, v0, v1

    #@67
    sput-object v0, Lcom/android/internal/telephony/Phone$SuppService;->$VALUES:[Lcom/android/internal/telephony/Phone$SuppService;

    #@69
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/Phone$SuppService;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 88
    const-class v0, Lcom/android/internal/telephony/Phone$SuppService;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/Phone$SuppService;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/Phone$SuppService;
    .registers 1

    #@0
    .prologue
    .line 88
    sget-object v0, Lcom/android/internal/telephony/Phone$SuppService;->$VALUES:[Lcom/android/internal/telephony/Phone$SuppService;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/Phone$SuppService;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/Phone$SuppService;

    #@8
    return-object v0
.end method
