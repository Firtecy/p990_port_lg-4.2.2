.class public Lcom/android/internal/telephony/ApnSelectionHandler;
.super Landroid/os/Handler;
.source "ApnSelectionHandler.java"


# static fields
.field public static final KT_Domestic_APN:Ljava/lang/String; = "lte150.ktfwing.com"

.field public static final KT_MCCMNC:Ljava/lang/String; = "45008"

.field public static final KT_Roaming_APN:Ljava/lang/String; = "lte150.ktfwing.com"

.field public static final LGT_Domestic_APN:Ljava/lang/String; = "internet.lguplus.co.kr"

.field public static final LGT_MCCMNC:Ljava/lang/String; = "45006"

.field public static final LGT_Roaming_APN:Ljava/lang/String; = "wroaming.lguplus.co.kr"

.field public static final LGT_Roaming_LTE_APN:Ljava/lang/String; = "lte-roaming.lguplus.co.kr"

.field private static final LOG_TAG:Ljava/lang/String; = "[LGE_DATA][ApnSelectionHandler] "

.field public static final REASON_ADDED_APN_FAILED:Ljava/lang/String; = "Added_APN_failed"

.field public static final REASON_SELECT_DEFAULT_APN:Ljava/lang/String; = "Select_default_APN_between_domestic_and_roaming"

.field public static final SKT_Domestic_APN:Ljava/lang/String; = "lte.sktelecom.com"

.field public static final SKT_IMS_APN:Ljava/lang/String; = "ims"

.field public static final SKT_MCCMNC:Ljava/lang/String; = "45005"

.field public static final SKT_MVNO_MCCMNC:Ljava/lang/String; = "45011"

.field public static final SKT_Roaming_3G_APN:Ljava/lang/String; = "roaming.sktelecom.com"

.field public static final SKT_Roaming_LTE_APN:Ljava/lang/String; = "lte-roaming.sktelecom.com"


# instance fields
.field public APN_FAIL_Flag:Z

.field public Domestic_APN_ID:I

.field public IMS_APN_ID:I

.field public Last_default_APN_ID:I

.field public Roaming_3G_APN_ID:I

.field public Roaming_LTE_APN_ID:I

.field public Roaming_check_APN_ID:I

.field featureset:Ljava/lang/String;

.field private mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/DataConnectionTracker;Lcom/android/internal/telephony/PhoneBase;)V
    .registers 5
    .parameter "dct"
    .parameter "p"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 118
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@4
    .line 64
    iput-boolean v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->APN_FAIL_Flag:Z

    #@6
    .line 85
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@8
    .line 87
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->IMS_APN_ID:I

    #@a
    .line 89
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@c
    .line 90
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@e
    .line 91
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_check_APN_ID:I

    #@10
    .line 98
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@12
    .line 119
    const-string v0, "[LGE_DATA][ApnSelectionHandler] "

    #@14
    const-string v1, "ApnSelectionHandler() has created"

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 121
    iput-object p1, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1b
    .line 127
    const-string v0, "ro.afwdata.LGfeatureset"

    #@1d
    const-string v1, "none"

    #@1f
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    iput-object v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->featureset:Ljava/lang/String;

    #@25
    .line 147
    return-void
.end method


# virtual methods
.method public findAllOperatorApnID(Lcom/android/internal/telephony/ApnSetting;)V
    .registers 4
    .parameter "apn"

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->featureset:Ljava/lang/String;

    #@2
    const-string v1, "LGTBASE"

    #@4
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_4f

    #@a
    .line 159
    iget-object v0, p1, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@c
    const-string v1, "internet.lguplus.co.kr"

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_21

    #@14
    iget v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@16
    if-nez v0, :cond_21

    #@18
    .line 162
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@1a
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@1c
    .line 163
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@1e
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@20
    .line 210
    :cond_20
    :goto_20
    return-void

    #@21
    .line 164
    :cond_21
    iget-object v0, p1, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@23
    const-string v1, "wroaming.lguplus.co.kr"

    #@25
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_38

    #@2b
    iget v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@2d
    if-nez v0, :cond_38

    #@2f
    .line 166
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@31
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@33
    .line 167
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@35
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@37
    goto :goto_20

    #@38
    .line 170
    :cond_38
    iget-object v0, p1, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@3a
    const-string v1, "lte-roaming.lguplus.co.kr"

    #@3c
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@3f
    move-result v0

    #@40
    if-eqz v0, :cond_20

    #@42
    iget v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@44
    if-nez v0, :cond_20

    #@46
    .line 172
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@48
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@4a
    .line 173
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@4c
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@4e
    goto :goto_20

    #@4f
    .line 177
    :cond_4f
    iget-object v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->featureset:Ljava/lang/String;

    #@51
    const-string v1, "KTBASE"

    #@53
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@56
    move-result v0

    #@57
    if-eqz v0, :cond_87

    #@59
    .line 179
    iget-object v0, p1, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@5b
    const-string v1, "lte150.ktfwing.com"

    #@5d
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@60
    move-result v0

    #@61
    if-eqz v0, :cond_70

    #@63
    iget v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@65
    if-nez v0, :cond_70

    #@67
    .line 180
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@69
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@6b
    .line 181
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@6d
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@6f
    goto :goto_20

    #@70
    .line 182
    :cond_70
    iget-object v0, p1, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@72
    const-string v1, "lte150.ktfwing.com"

    #@74
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@77
    move-result v0

    #@78
    if-eqz v0, :cond_20

    #@7a
    iget v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@7c
    if-nez v0, :cond_20

    #@7e
    .line 183
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@80
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@82
    .line 184
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@84
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@86
    goto :goto_20

    #@87
    .line 189
    :cond_87
    iget-object v0, p1, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@89
    const-string v1, "lte.sktelecom.com"

    #@8b
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@8e
    move-result v0

    #@8f
    if-eqz v0, :cond_9e

    #@91
    iget v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@93
    if-nez v0, :cond_9e

    #@95
    .line 191
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@97
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@99
    .line 192
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@9b
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@9d
    goto :goto_20

    #@9e
    .line 195
    :cond_9e
    iget-object v0, p1, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@a0
    const-string v1, "ims"

    #@a2
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@a5
    move-result v0

    #@a6
    if-eqz v0, :cond_b6

    #@a8
    iget v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->IMS_APN_ID:I

    #@aa
    if-nez v0, :cond_b6

    #@ac
    .line 196
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@ae
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->IMS_APN_ID:I

    #@b0
    .line 197
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@b2
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@b4
    goto/16 :goto_20

    #@b6
    .line 200
    :cond_b6
    iget-object v0, p1, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@b8
    const-string v1, "roaming.sktelecom.com"

    #@ba
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@bd
    move-result v0

    #@be
    if-eqz v0, :cond_ce

    #@c0
    iget v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@c2
    if-nez v0, :cond_ce

    #@c4
    .line 202
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@c6
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@c8
    .line 203
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@ca
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@cc
    goto/16 :goto_20

    #@ce
    .line 204
    :cond_ce
    iget-object v0, p1, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@d0
    const-string v1, "lte-roaming.sktelecom.com"

    #@d2
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@d5
    move-result v0

    #@d6
    if-eqz v0, :cond_20

    #@d8
    iget v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@da
    if-nez v0, :cond_20

    #@dc
    .line 206
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@de
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@e0
    .line 207
    iget v0, p1, Lcom/android/internal/telephony/DataProfile;->id:I

    #@e2
    iput v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@e4
    goto/16 :goto_20
.end method

.method public selectApn(Ljava/lang/String;)V
    .registers 10
    .parameter "reason"

    #@0
    .prologue
    .line 213
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v5}, Lcom/android/internal/telephony/DataConnectionTracker;->getOperatorNumeric()Ljava/lang/String;

    #@5
    move-result-object v4

    #@6
    .line 214
    .local v4, usim_mcc_mnc:Ljava/lang/String;
    const/4 v2, 0x0

    #@7
    .line 215
    .local v2, changeAPN:Z
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@9
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@e
    move-result-object v5

    #@f
    invoke-virtual {v5}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@12
    move-result v0

    #@13
    .line 217
    .local v0, IsRoaming:Z
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@15
    invoke-virtual {v5}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredNetworkMode()I

    #@18
    move-result v1

    #@19
    .line 219
    .local v1, NWmode:I
    if-nez v4, :cond_2a

    #@1b
    .line 220
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@1d
    const-string v6, "<selectApn()> usim_mcc_mnc is NULL. Just set \'Domestic APN\' to \'Preferred APN\', just in case!!"

    #@1f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 223
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@24
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@26
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredApn(I)V

    #@29
    .line 399
    :goto_29
    return-void

    #@2a
    .line 227
    :cond_2a
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@2c
    new-instance v6, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v7, "<selectApn()> usim_mcc_mnc : "

    #@33
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v6

    #@37
    iget-object v7, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@39
    invoke-virtual {v7}, Lcom/android/internal/telephony/DataConnectionTracker;->getOperatorNumeric()Ljava/lang/String;

    #@3c
    move-result-object v7

    #@3d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v6

    #@45
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 229
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@4a
    new-instance v6, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v7, "<selectApn()> IsRoaming : "

    #@51
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v6

    #@55
    iget-object v7, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@57
    iget-object v7, v7, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@59
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@5c
    move-result-object v7

    #@5d
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@60
    move-result v7

    #@61
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v6

    #@69
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 231
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@6e
    new-instance v6, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v7, "<selectApn()> reason : "

    #@75
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v6

    #@79
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v6

    #@7d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v6

    #@81
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 232
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@86
    new-instance v6, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v7, "<selectApn()> Domestic_APN_ID : "

    #@8d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v6

    #@91
    iget v7, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@93
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@96
    move-result-object v6

    #@97
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v6

    #@9b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    .line 233
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@a0
    new-instance v6, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v7, "<selectApn()> Roaming_3G_APN_ID : "

    #@a7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v6

    #@ab
    iget v7, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@ad
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v6

    #@b1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v6

    #@b5
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    .line 234
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@ba
    new-instance v6, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v7, "<selectApn()> Roaming_LTE_APN_ID : "

    #@c1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v6

    #@c5
    iget v7, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@c7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v6

    #@cb
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v6

    #@cf
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d2
    .line 236
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@d4
    new-instance v6, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v7, "<selectApn()> Last_default_APN_ID : "

    #@db
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v6

    #@df
    iget v7, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@e1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v6

    #@e5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v6

    #@e9
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ec
    .line 240
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@ee
    invoke-virtual {v5}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredApn()Lcom/android/internal/telephony/DataProfile;

    #@f1
    move-result-object v3

    #@f2
    .line 243
    .local v3, mDP:Lcom/android/internal/telephony/DataProfile;
    if-eqz v3, :cond_133

    #@f4
    .line 244
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@f6
    new-instance v6, Ljava/lang/StringBuilder;

    #@f8
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@fb
    const-string v7, "<selectApn()> getPreferredApn().id : "

    #@fd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v6

    #@101
    iget v7, v3, Lcom/android/internal/telephony/DataProfile;->id:I

    #@103
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@106
    move-result-object v6

    #@107
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v6

    #@10b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10e
    .line 246
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@110
    iput-object v3, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@112
    .line 248
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@114
    invoke-virtual {v5}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredApn()Lcom/android/internal/telephony/DataProfile;

    #@117
    move-result-object v3

    #@118
    .line 259
    :goto_118
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@11a
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@11c
    if-eqz v5, :cond_12a

    #@11e
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@120
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@122
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    #@125
    move-result v5

    #@126
    if-nez v5, :cond_12a

    #@128
    if-nez v3, :cond_14d

    #@12a
    .line 264
    :cond_12a
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@12c
    const-string v6, "<selectApn()> mAllApns is NULL. || mAllApns is Empty. || getPreferredApn() is NULL."

    #@12e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@131
    goto/16 :goto_29

    #@133
    .line 250
    :cond_133
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@135
    const-string v6, "<selectApn()> getPreferredApn() is NULL. Just set \'Domestic APN\' to \'Preferred APN\' in case of initializing APN DB."

    #@137
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13a
    .line 252
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@13c
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@13e
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredApn(I)V

    #@141
    .line 254
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@143
    const/4 v6, 0x0

    #@144
    iput-object v6, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@146
    .line 256
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@148
    invoke-virtual {v5}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredApn()Lcom/android/internal/telephony/DataProfile;

    #@14b
    move-result-object v3

    #@14c
    goto :goto_118

    #@14d
    .line 269
    :cond_14d
    const-string v5, "45005"

    #@14f
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@152
    move-result v5

    #@153
    if-nez v5, :cond_16d

    #@155
    const-string v5, "45011"

    #@157
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15a
    move-result v5

    #@15b
    if-nez v5, :cond_16d

    #@15d
    const-string v5, "45006"

    #@15f
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@162
    move-result v5

    #@163
    if-nez v5, :cond_16d

    #@165
    const-string v5, "45008"

    #@167
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16a
    move-result v5

    #@16b
    if-eqz v5, :cond_19d

    #@16d
    .line 282
    :cond_16d
    const-string v5, "Select_default_APN_between_domestic_and_roaming"

    #@16f
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@172
    move-result v5

    #@173
    if-eqz v5, :cond_2ad

    #@175
    .line 283
    if-eqz v3, :cond_19d

    #@177
    iget v5, v3, Lcom/android/internal/telephony/DataProfile;->id:I

    #@179
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@17b
    if-gt v5, v6, :cond_19d

    #@17d
    .line 288
    if-eqz v0, :cond_2a2

    #@17f
    .line 290
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@181
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@183
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@185
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@188
    move-result-object v5

    #@189
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_SKT:Z

    #@18b
    if-eqz v5, :cond_244

    #@18d
    .line 292
    packed-switch v1, :pswitch_data_342

    #@190
    .line 305
    :pswitch_190
    iget v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@192
    iput v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_check_APN_ID:I

    #@194
    .line 326
    :goto_194
    if-eqz v3, :cond_19d

    #@196
    iget v5, v3, Lcom/android/internal/telephony/DataProfile;->id:I

    #@198
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_check_APN_ID:I

    #@19a
    if-eq v5, v6, :cond_19d

    #@19c
    .line 327
    const/4 v2, 0x1

    #@19d
    .line 351
    :cond_19d
    :goto_19d
    if-eqz v2, :cond_202

    #@19f
    .line 352
    if-eqz v0, :cond_339

    #@1a1
    .line 354
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1a3
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1a5
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1a7
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1aa
    move-result-object v5

    #@1ab
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_SKT:Z

    #@1ad
    if-eqz v5, :cond_2d2

    #@1af
    .line 355
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@1b1
    new-instance v6, Ljava/lang/StringBuilder;

    #@1b3
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1b6
    const-string v7, "<selectApn()> net.Is_LTERoaming_allowed : "

    #@1b8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    move-result-object v6

    #@1bc
    const-string v7, "net.Is_LTERoaming_allowed"

    #@1be
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1c1
    move-result-object v7

    #@1c2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v6

    #@1c6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c9
    move-result-object v6

    #@1ca
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1cd
    .line 356
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@1cf
    new-instance v6, Ljava/lang/StringBuilder;

    #@1d1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1d4
    const-string v7, "<selectApn()> NWmode : "

    #@1d6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v6

    #@1da
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1dd
    move-result-object v6

    #@1de
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e1
    move-result-object v6

    #@1e2
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e5
    .line 358
    sparse-switch v1, :sswitch_data_35c

    #@1e8
    .line 368
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1ea
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@1ec
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredApn(I)V

    #@1ef
    .line 392
    :goto_1ef
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1f1
    iget-object v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1f3
    invoke-virtual {v6}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredApn()Lcom/android/internal/telephony/DataProfile;

    #@1f6
    move-result-object v6

    #@1f7
    iput-object v6, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@1f9
    .line 394
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1fb
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1fd
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mLGEDataConnectionTracker:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@1ff
    invoke-virtual {v5}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->sendPdnTable()V

    #@202
    .line 397
    :cond_202
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@204
    new-instance v6, Ljava/lang/StringBuilder;

    #@206
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@209
    const-string v7, "<selectApn()> changeAPN : "

    #@20b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20e
    move-result-object v6

    #@20f
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@212
    move-result-object v6

    #@213
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@216
    move-result-object v6

    #@217
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21a
    .line 398
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@21c
    new-instance v6, Ljava/lang/StringBuilder;

    #@21e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@221
    const-string v7, "<selectApn()> mPreferredApn : "

    #@223
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v6

    #@227
    iget-object v7, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@229
    iget-object v7, v7, Lcom/android/internal/telephony/DataConnectionTracker;->mPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@22b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v6

    #@22f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@232
    move-result-object v6

    #@233
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@236
    goto/16 :goto_29

    #@238
    .line 295
    :pswitch_238
    iget v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@23a
    iput v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_check_APN_ID:I

    #@23c
    goto/16 :goto_194

    #@23e
    .line 301
    :pswitch_23e
    iget v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@240
    iput v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_check_APN_ID:I

    #@242
    goto/16 :goto_194

    #@244
    .line 311
    :cond_244
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@246
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@248
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@24a
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@24d
    move-result-object v5

    #@24e
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_LTE_ROAMING_LGU:Z

    #@250
    if-eqz v5, :cond_29c

    #@252
    .line 312
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@254
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@256
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getLTEDataRoamingEnable()Z

    #@259
    move-result v5

    #@25a
    if-eqz v5, :cond_27c

    #@25c
    .line 313
    iget v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@25e
    iput v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_check_APN_ID:I

    #@260
    .line 314
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@262
    new-instance v6, Ljava/lang/StringBuilder;

    #@264
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@267
    const-string v7, "<selectApn()> IsRoaming: LTE Roaming enabled = "

    #@269
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26c
    move-result-object v6

    #@26d
    iget v7, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_check_APN_ID:I

    #@26f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@272
    move-result-object v6

    #@273
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@276
    move-result-object v6

    #@277
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27a
    goto/16 :goto_194

    #@27c
    .line 316
    :cond_27c
    iget v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@27e
    iput v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_check_APN_ID:I

    #@280
    .line 317
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@282
    new-instance v6, Ljava/lang/StringBuilder;

    #@284
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@287
    const-string v7, "<selectApn()> IsRoaming: W/G Roaming enabled = "

    #@289
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28c
    move-result-object v6

    #@28d
    iget v7, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_check_APN_ID:I

    #@28f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@292
    move-result-object v6

    #@293
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@296
    move-result-object v6

    #@297
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29a
    goto/16 :goto_194

    #@29c
    .line 323
    :cond_29c
    iget v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@29e
    iput v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_check_APN_ID:I

    #@2a0
    goto/16 :goto_194

    #@2a2
    .line 330
    :cond_2a2
    if-eqz v3, :cond_19d

    #@2a4
    iget v5, v3, Lcom/android/internal/telephony/DataProfile;->id:I

    #@2a6
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@2a8
    if-eq v5, v6, :cond_19d

    #@2aa
    .line 331
    const/4 v2, 0x1

    #@2ab
    goto/16 :goto_19d

    #@2ad
    .line 340
    :cond_2ad
    const-string v5, "Added_APN_failed"

    #@2af
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b2
    move-result v5

    #@2b3
    if-eqz v5, :cond_19d

    #@2b5
    .line 341
    if-eqz v3, :cond_19d

    #@2b7
    iget v5, v3, Lcom/android/internal/telephony/DataProfile;->id:I

    #@2b9
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Last_default_APN_ID:I

    #@2bb
    if-le v5, v6, :cond_19d

    #@2bd
    .line 346
    const/4 v2, 0x1

    #@2be
    goto/16 :goto_19d

    #@2c0
    .line 360
    :sswitch_2c0
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2c2
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@2c4
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredApn(I)V

    #@2c7
    goto/16 :goto_1ef

    #@2c9
    .line 364
    :sswitch_2c9
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2cb
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@2cd
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredApn(I)V

    #@2d0
    goto/16 :goto_1ef

    #@2d2
    .line 375
    :cond_2d2
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2d4
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2d6
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2d8
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2db
    move-result-object v5

    #@2dc
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_LTE_ROAMING_LGU:Z

    #@2de
    if-eqz v5, :cond_330

    #@2e0
    .line 376
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2e2
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2e4
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getLTEDataRoamingEnable()Z

    #@2e7
    move-result v5

    #@2e8
    if-eqz v5, :cond_30d

    #@2ea
    .line 377
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2ec
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@2ee
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredApn(I)V

    #@2f1
    .line 378
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@2f3
    new-instance v6, Ljava/lang/StringBuilder;

    #@2f5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f8
    const-string v7, "<selectApn()> changeAPN, LTE Roaming = "

    #@2fa
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fd
    move-result-object v6

    #@2fe
    iget v7, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@300
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@303
    move-result-object v6

    #@304
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@307
    move-result-object v6

    #@308
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30b
    goto/16 :goto_1ef

    #@30d
    .line 380
    :cond_30d
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@30f
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@311
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredApn(I)V

    #@314
    .line 381
    const-string v5, "[LGE_DATA][ApnSelectionHandler] "

    #@316
    new-instance v6, Ljava/lang/StringBuilder;

    #@318
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@31b
    const-string v7, "<selectApn()> changeAPN, W/G Roaming = "

    #@31d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@320
    move-result-object v6

    #@321
    iget v7, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@323
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@326
    move-result-object v6

    #@327
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32a
    move-result-object v6

    #@32b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32e
    goto/16 :goto_1ef

    #@330
    .line 386
    :cond_330
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@332
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@334
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredApn(I)V

    #@337
    goto/16 :goto_1ef

    #@339
    .line 389
    :cond_339
    iget-object v5, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@33b
    iget v6, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@33d
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredApn(I)V

    #@340
    goto/16 :goto_1ef

    #@342
    .line 292
    :pswitch_data_342
    .packed-switch 0x1
        :pswitch_23e
        :pswitch_23e
        :pswitch_23e
        :pswitch_190
        :pswitch_190
        :pswitch_190
        :pswitch_190
        :pswitch_190
        :pswitch_238
        :pswitch_190
        :pswitch_238
    .end packed-switch

    #@35c
    .line 358
    :sswitch_data_35c
    .sparse-switch
        0x3 -> :sswitch_2c9
        0x9 -> :sswitch_2c0
    .end sparse-switch
.end method

.method public selectApnForLteRoamingOfUplus(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 403
    const-string v0, "[LGE_DATA][ApnSelectionHandler] "

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "<selectApnForLTERoamingOfUplus()> enabled = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 405
    if-eqz p1, :cond_62

    #@1a
    .line 406
    iget-object v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1c
    iget v1, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@1e
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredApn(I)V

    #@21
    .line 407
    const-string v0, "[LGE_DATA][ApnSelectionHandler] "

    #@23
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v2, "<selectApnForLTERoamingOfUplus()> changeAPN, LTE Roaming = "

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    iget v2, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 413
    :goto_3b
    iget-object v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3d
    iget-object v1, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3f
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredApn()Lcom/android/internal/telephony/DataProfile;

    #@42
    move-result-object v1

    #@43
    iput-object v1, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@45
    .line 414
    const-string v0, "[LGE_DATA][ApnSelectionHandler] "

    #@47
    new-instance v1, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v2, "<selectApnForLTERoamingOfUplus()> mPreferredApn : "

    #@4e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    iget-object v2, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@54
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->mPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v1

    #@5e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 415
    return-void

    #@62
    .line 409
    :cond_62
    iget-object v0, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->mGsmDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@64
    iget v1, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@66
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredApn(I)V

    #@69
    .line 410
    const-string v0, "[LGE_DATA][ApnSelectionHandler] "

    #@6b
    new-instance v1, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v2, "<selectApnForLTERoamingOfUplus()> changeAPN, W/G Roaming = "

    #@72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v1

    #@76
    iget v2, p0, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v1

    #@7c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v1

    #@80
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    goto :goto_3b
.end method
