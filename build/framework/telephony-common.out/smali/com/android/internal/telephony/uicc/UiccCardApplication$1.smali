.class Lcom/android/internal/telephony/uicc/UiccCardApplication$1;
.super Landroid/os/Handler;
.source "UiccCardApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/UiccCardApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 451
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 456
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$000(Lcom/android/internal/telephony/uicc/UiccCardApplication;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_37

    #@c
    .line 457
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "Received message "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "["

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    iget v3, p1, Landroid/os/Message;->what:I

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, "] while being destroyed. Ignoring."

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$100(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;)V

    #@36
    .line 534
    :goto_36
    return-void

    #@37
    .line 462
    :cond_37
    iget v1, p1, Landroid/os/Message;->what:I

    #@39
    packed-switch v1, :pswitch_data_124

    #@3c
    .line 532
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@3e
    new-instance v2, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v3, "Unknown Event "

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    iget v3, p1, Landroid/os/Message;->what:I

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$100(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;)V

    #@56
    goto :goto_36

    #@57
    .line 469
    :pswitch_57
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@59
    check-cast v0, Landroid/os/AsyncResult;

    #@5b
    .line 471
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5d
    if-nez v1, :cond_7d

    #@5f
    .line 472
    iget v1, p1, Landroid/os/Message;->what:I

    #@61
    if-eq v1, v4, :cond_67

    #@63
    iget v1, p1, Landroid/os/Message;->what:I

    #@65
    if-ne v1, v5, :cond_b5

    #@67
    .line 474
    :cond_67
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@69
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6b
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$200(Lcom/android/internal/telephony/uicc/UiccCardApplication;)Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->setInternalPIN(Ljava/lang/String;)V

    #@72
    .line 475
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@74
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@76
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$400(Lcom/android/internal/telephony/uicc/UiccCardApplication;)I

    #@79
    move-result v2

    #@7a
    invoke-static {v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$302(Lcom/android/internal/telephony/uicc/UiccCardApplication;I)I

    #@7d
    .line 480
    :cond_7d
    :goto_7d
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@7f
    invoke-static {v1, v6}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$202(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;)Ljava/lang/String;

    #@82
    .line 484
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@84
    if-eqz v1, :cond_97

    #@86
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@88
    if-eqz v1, :cond_97

    #@8a
    .line 485
    iget v1, p1, Landroid/os/Message;->what:I

    #@8c
    if-eq v1, v4, :cond_92

    #@8e
    iget v1, p1, Landroid/os/Message;->what:I

    #@90
    if-ne v1, v5, :cond_c1

    #@92
    .line 487
    :cond_92
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@94
    invoke-static {v1, v0, v4}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$700(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/os/AsyncResult;Z)V

    #@97
    .line 493
    :cond_97
    :goto_97
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@99
    invoke-static {v1, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$802(Lcom/android/internal/telephony/uicc/UiccCardApplication;Z)Z

    #@9c
    .line 494
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9e
    invoke-static {v1, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$902(Lcom/android/internal/telephony/uicc/UiccCardApplication;Z)Z

    #@a1
    .line 497
    iget-object v1, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@a3
    check-cast v1, Landroid/os/Message;

    #@a5
    invoke-static {v1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@a8
    move-result-object v1

    #@a9
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@ab
    iput-object v2, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@ad
    .line 499
    iget-object v1, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@af
    check-cast v1, Landroid/os/Message;

    #@b1
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@b4
    goto :goto_36

    #@b5
    .line 477
    :cond_b5
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@b7
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@b9
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$600(Lcom/android/internal/telephony/uicc/UiccCardApplication;)I

    #@bc
    move-result v2

    #@bd
    invoke-static {v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$502(Lcom/android/internal/telephony/uicc/UiccCardApplication;I)I

    #@c0
    goto :goto_7d

    #@c1
    .line 489
    :cond_c1
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c3
    invoke-static {v1, v0, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$700(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/os/AsyncResult;Z)V

    #@c6
    goto :goto_97

    #@c7
    .line 502
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_c7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c9
    check-cast v0, Landroid/os/AsyncResult;

    #@cb
    .line 503
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@cd
    invoke-static {v1, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$1000(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/os/AsyncResult;)V

    #@d0
    goto/16 :goto_36

    #@d2
    .line 506
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_d2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d4
    check-cast v0, Landroid/os/AsyncResult;

    #@d6
    .line 507
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@d8
    invoke-static {v1, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$1100(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/os/AsyncResult;)V

    #@db
    goto/16 :goto_36

    #@dd
    .line 510
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_dd
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@df
    check-cast v0, Landroid/os/AsyncResult;

    #@e1
    .line 511
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@e3
    invoke-static {v1, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$1200(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/os/AsyncResult;)V

    #@e6
    goto/16 :goto_36

    #@e8
    .line 514
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_e8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ea
    check-cast v0, Landroid/os/AsyncResult;

    #@ec
    .line 515
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@ee
    invoke-static {v1, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$1300(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/os/AsyncResult;)V

    #@f1
    goto/16 :goto_36

    #@f3
    .line 519
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_f3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f5
    check-cast v0, Landroid/os/AsyncResult;

    #@f7
    .line 520
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@f9
    if-nez v1, :cond_109

    #@fb
    .line 521
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@fd
    const-string v2, "[LGE] Internal PIN verify OK"

    #@ff
    invoke-static {v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$100(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;)V

    #@102
    .line 528
    :goto_102
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@104
    invoke-static {v1, v6}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$202(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;)Ljava/lang/String;

    #@107
    goto/16 :goto_36

    #@109
    .line 524
    :cond_109
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@10b
    new-instance v2, Ljava/lang/StringBuilder;

    #@10d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@110
    const-string v3, "[LGE] Error in internal PIN verify with exception"

    #@112
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v2

    #@116
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@118
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v2

    #@11c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v2

    #@120
    invoke-static {v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->access$100(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;)V

    #@123
    goto :goto_102

    #@124
    .line 462
    :pswitch_data_124
    .packed-switch 0x1
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_c7
        :pswitch_d2
        :pswitch_dd
        :pswitch_e8
        :pswitch_57
        :pswitch_f3
    .end packed-switch
.end method
