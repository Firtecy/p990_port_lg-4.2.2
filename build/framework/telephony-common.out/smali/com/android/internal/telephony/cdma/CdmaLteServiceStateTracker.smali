.class public Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;
.super Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;
.source "CdmaLteServiceStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker$1;,
        Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker$BlockEHRPDInternetIPv6Task;
    }
.end annotation


# instance fields
.field protected CheckATTACH:Z

.field protected isFixedSync:Z

.field private mBlockEHRPDInternetIPv6Timer:Ljava/util/Timer;

.field private mCdmaLtePhone:Lcom/android/internal/telephony/cdma/CDMALTEPhone;

.field private final mCellInfoLte:Landroid/telephony/CellInfoLte;

.field private mLasteCellIdentityLte:Landroid/telephony/CellIdentityLte;

.field private mLteSS:Landroid/telephony/ServiceState;

.field private mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/cdma/CDMALTEPhone;)V
    .registers 6
    .parameter "phone"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 117
    new-instance v0, Landroid/telephony/CellInfoLte;

    #@4
    invoke-direct {v0}, Landroid/telephony/CellInfoLte;-><init>()V

    #@7
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;Landroid/telephony/CellInfo;)V

    #@a
    .line 94
    new-instance v0, Landroid/telephony/CellIdentityLte;

    #@c
    invoke-direct {v0}, Landroid/telephony/CellIdentityLte;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@11
    .line 95
    new-instance v0, Landroid/telephony/CellIdentityLte;

    #@13
    invoke-direct {v0}, Landroid/telephony/CellIdentityLte;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLasteCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@18
    .line 100
    iput-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->isFixedSync:Z

    #@1a
    .line 118
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mCdmaLtePhone:Lcom/android/internal/telephony/cdma/CDMALTEPhone;

    #@1c
    .line 119
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@1e
    check-cast v0, Landroid/telephony/CellInfoLte;

    #@20
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@22
    .line 121
    new-instance v0, Landroid/telephony/ServiceState;

    #@24
    invoke-direct {v0}, Landroid/telephony/ServiceState;-><init>()V

    #@27
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@29
    .line 122
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@2b
    check-cast v0, Landroid/telephony/CellInfoLte;

    #@2d
    new-instance v1, Landroid/telephony/CellSignalStrengthLte;

    #@2f
    invoke-direct {v1}, Landroid/telephony/CellSignalStrengthLte;-><init>()V

    #@32
    invoke-virtual {v0, v1}, Landroid/telephony/CellInfoLte;->setCellSignalStrength(Landroid/telephony/CellSignalStrengthLte;)V

    #@35
    .line 123
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@37
    check-cast v0, Landroid/telephony/CellInfoLte;

    #@39
    new-instance v1, Landroid/telephony/CellIdentityLte;

    #@3b
    invoke-direct {v1}, Landroid/telephony/CellIdentityLte;-><init>()V

    #@3e
    invoke-virtual {v0, v1}, Landroid/telephony/CellInfoLte;->setCellIdentity(Landroid/telephony/CellIdentityLte;)V

    #@41
    .line 125
    iput-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->CheckATTACH:Z

    #@43
    .line 129
    new-instance v0, Ljava/util/Timer;

    #@45
    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    #@48
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mBlockEHRPDInternetIPv6Timer:Ljava/util/Timer;

    #@4a
    .line 132
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@4c
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@4f
    move-result-object v0

    #@50
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_BLOCK_TETHERINGPDN_ON_GSMROAMING_KDDI:Z

    #@52
    if-ne v0, v3, :cond_5b

    #@54
    .line 134
    const-string v0, "CdmaLteServiceStateTracker Constructors: KDDI isFixedSync is set to True"

    #@56
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@59
    .line 135
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->isFixedSync:Z

    #@5b
    .line 138
    :cond_5b
    const-string v0, "CdmaLteServiceStateTracker Constructors"

    #@5d
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@60
    .line 139
    return-void
.end method

.method private getLinkProperties_defaultAPN()Landroid/net/LinkProperties;
    .registers 3

    #@0
    .prologue
    .line 1219
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2
    iget-object v0, v1, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@4
    .line 1221
    .local v0, myDct:Lcom/android/internal/telephony/DataConnectionTracker;
    if-nez v0, :cond_8

    #@6
    .line 1222
    const/4 v1, 0x0

    #@7
    .line 1224
    :goto_7
    return-object v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->getLinkProperties_defaultAPN()Landroid/net/LinkProperties;

    #@b
    move-result-object v1

    #@c
    goto :goto_7
.end method

.method private isInHomeSidNid(II)Z
    .registers 8
    .parameter "sid"
    .parameter "nid"

    #@0
    .prologue
    const v4, 0xffff

    #@3
    const/4 v1, 0x1

    #@4
    .line 1087
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->isSidsAllZeros()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_b

    #@a
    .line 1104
    :cond_a
    :goto_a
    return v1

    #@b
    .line 1090
    :cond_b
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@d
    array-length v2, v2

    #@e
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeNetworkId:[I

    #@10
    array-length v3, v3

    #@11
    if-ne v2, v3, :cond_a

    #@13
    .line 1092
    if-eqz p1, :cond_a

    #@15
    .line 1094
    const/4 v0, 0x0

    #@16
    .local v0, i:I
    :goto_16
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@18
    array-length v2, v2

    #@19
    if-ge v0, v2, :cond_3a

    #@1b
    .line 1097
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@1d
    aget v2, v2, v0

    #@1f
    if-ne v2, p1, :cond_37

    #@21
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeNetworkId:[I

    #@23
    aget v2, v2, v0

    #@25
    if-eqz v2, :cond_a

    #@27
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeNetworkId:[I

    #@29
    aget v2, v2, v0

    #@2b
    if-eq v2, v4, :cond_a

    #@2d
    if-eqz p2, :cond_a

    #@2f
    if-eq p2, v4, :cond_a

    #@31
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeNetworkId:[I

    #@33
    aget v2, v2, v0

    #@35
    if-eq v2, p2, :cond_a

    #@37
    .line 1094
    :cond_37
    add-int/lit8 v0, v0, 0x1

    #@39
    goto :goto_16

    #@3a
    .line 1104
    :cond_3a
    const/4 v1, 0x0

    #@3b
    goto :goto_a
.end method

.method private onSetEhrpdInfo(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    .line 1125
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2
    if-eqz v2, :cond_a

    #@4
    .line 1126
    const-string v2, "onEhrpdInfoReceived, there is Exception"

    #@6
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@9
    .line 1137
    :goto_9
    return-void

    #@a
    .line 1129
    :cond_a
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c
    check-cast v2, Ljava/lang/String;

    #@e
    move-object v1, v2

    #@f
    check-cast v1, Ljava/lang/String;

    #@11
    .line 1130
    .local v1, result:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "[IMS_AFW] GET EHRPD Info: "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@27
    .line 1132
    const/4 v0, 0x0

    #@28
    .local v0, i:I
    :goto_28
    const-string v2, ":"

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    array-length v2, v2

    #@2f
    if-ge v0, v2, :cond_40

    #@31
    .line 1133
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mEhrpdInfo:[Ljava/lang/String;

    #@33
    const-string v3, ":"

    #@35
    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    aget-object v3, v3, v0

    #@3b
    aput-object v3, v2, v0

    #@3d
    .line 1132
    add-int/lit8 v0, v0, 0x1

    #@3f
    goto :goto_28

    #@40
    .line 1135
    :cond_40
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v3, "[IMS_AFW] Sector ID : "

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mEhrpdInfo:[Ljava/lang/String;

    #@4d
    const/4 v4, 0x0

    #@4e
    aget-object v3, v3, v4

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    const-string v3, ", Subnet length : "

    #@56
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v2

    #@5a
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mEhrpdInfo:[Ljava/lang/String;

    #@5c
    const/4 v4, 0x1

    #@5d
    aget-object v3, v3, v4

    #@5f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v2

    #@63
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@6a
    goto :goto_9
.end method

.method private onSetLteInfo(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    .line 1109
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2
    if-eqz v2, :cond_a

    #@4
    .line 1110
    const-string v2, "onLteInfoReceived, there is Exception"

    #@6
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@9
    .line 1122
    :goto_9
    return-void

    #@a
    .line 1113
    :cond_a
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c
    check-cast v2, Ljava/lang/String;

    #@e
    move-object v1, v2

    #@f
    check-cast v1, Ljava/lang/String;

    #@11
    .line 1114
    .local v1, result:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "[IMS_AFW] GET LTE Info: "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@27
    .line 1116
    const/4 v0, 0x0

    #@28
    .local v0, i:I
    :goto_28
    const-string v2, ","

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    array-length v2, v2

    #@2f
    if-ge v0, v2, :cond_40

    #@31
    .line 1117
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@33
    const-string v3, ","

    #@35
    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    aget-object v3, v3, v0

    #@3b
    aput-object v3, v2, v0

    #@3d
    .line 1116
    add-int/lit8 v0, v0, 0x1

    #@3f
    goto :goto_28

    #@40
    .line 1119
    :cond_40
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v3, "[IMS_AFW] MCC : "

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@4d
    const/4 v4, 0x0

    #@4e
    aget-object v3, v3, v4

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    const-string v3, ", MNC : "

    #@56
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v2

    #@5a
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@5c
    const/4 v4, 0x1

    #@5d
    aget-object v3, v3, v4

    #@5f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v2

    #@63
    const-string v3, ", Cell ID : "

    #@65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@6b
    const/4 v4, 0x2

    #@6c
    aget-object v3, v3, v4

    #@6e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v2

    #@72
    const-string v3, ", TAC : "

    #@74
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@7a
    const/4 v4, 0x3

    #@7b
    aget-object v3, v3, v4

    #@7d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v2

    #@81
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v2

    #@85
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@88
    goto :goto_9
.end method

.method private on_Ehrpd_Internet_Ipv6_block_requested()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 1175
    new-instance v6, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v7, "[EHRPD_IPV6] on_Ehrpd_Internet_Ipv6_block_requested() entry : current state "

    #@8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v6

    #@c
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@e
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@11
    move-result v7

    #@12
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@1d
    .line 1177
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@1f
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@26
    move-result-object v6

    #@27
    const-string v7, "data_ehrpd_internet_ipv6_enabled"

    #@29
    const/4 v8, 0x0

    #@2a
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2d
    move-result v4

    #@2e
    .line 1178
    .local v4, ehrpd_internet_ipv6_enabled:I
    if-ne v4, v9, :cond_47

    #@30
    .line 1180
    new-instance v6, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v7, "[EHRPD_IPV6] ehrpd_internet_ipv6_enabled is "

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@46
    .line 1213
    :cond_46
    :goto_46
    return-void

    #@47
    .line 1182
    :cond_47
    iget-object v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@49
    invoke-virtual {v6}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@4c
    move-result v6

    #@4d
    const/16 v7, 0xd

    #@4f
    if-ne v6, v7, :cond_46

    #@51
    sget-boolean v6, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@53
    if-nez v6, :cond_46

    #@55
    .line 1185
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->getLinkProperties_defaultAPN()Landroid/net/LinkProperties;

    #@58
    move-result-object v2

    #@59
    .line 1186
    .local v2, default_linkProp:Landroid/net/LinkProperties;
    const/4 v1, 0x0

    #@5a
    .line 1188
    .local v1, default_iface:Ljava/lang/String;
    if-eqz v2, :cond_60

    #@5c
    .line 1190
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@5f
    move-result-object v1

    #@60
    .line 1193
    :cond_60
    if-eqz v1, :cond_d4

    #@62
    .line 1195
    const-string v6, "network_management"

    #@64
    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@67
    move-result-object v0

    #@68
    .line 1196
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@6b
    move-result-object v5

    #@6c
    .line 1197
    .local v5, service:Landroid/os/INetworkManagementService;
    new-instance v6, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v7, "[EHRPD_IPV6] Block Interface "

    #@73
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v6

    #@77
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v6

    #@7b
    const-string v7, ", Current Tech is "

    #@7d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v6

    #@81
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@83
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@86
    move-result v7

    #@87
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v6

    #@8b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v6

    #@8f
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@92
    .line 1199
    if-eqz v5, :cond_97

    #@94
    :try_start_94
    invoke-interface {v5, v1}, Landroid/os/INetworkManagementService;->blockIPv6Interface(Ljava/lang/String;)V

    #@97
    .line 1200
    :cond_97
    sput-object v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@99
    .line 1201
    new-instance v6, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    const-string v7, "[EHRPD_IPV6] on_Ehrpd_Internet_Ipv6_block_requested after set ehrpd_ipv6_block_iface : "

    #@a0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v6

    #@a4
    sget-object v7, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@a6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v6

    #@aa
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v6

    #@ae
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@b1
    .line 1202
    const/4 v6, 0x1

    #@b2
    sput-boolean v6, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@b4
    .line 1203
    new-instance v6, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v7, "[EHRPD_IPV6] on_Ehrpd_Internet_Ipv6_block_requested after set is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables : "

    #@bb
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v6

    #@bf
    sget-boolean v7, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@c1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v6

    #@c5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v6

    #@c9
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V
    :try_end_cc
    .catch Landroid/os/RemoteException; {:try_start_94 .. :try_end_cc} :catch_ce

    #@cc
    goto/16 :goto_46

    #@ce
    .line 1204
    :catch_ce
    move-exception v3

    #@cf
    .line 1205
    .local v3, e:Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    #@d2
    goto/16 :goto_46

    #@d4
    .line 1210
    .end local v0           #b:Landroid/os/IBinder;
    .end local v3           #e:Landroid/os/RemoteException;
    .end local v5           #service:Landroid/os/INetworkManagementService;
    :cond_d4
    const-string v6, "[EHRPD_IPV6] default_iface is null"

    #@d6
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@d9
    goto/16 :goto_46
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 1240
    const-string v0, "CdmaLteServiceStateTracker extends:"

    #@2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 1241
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@8
    .line 1242
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, " mCdmaLtePhone="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mCdmaLtePhone:Lcom/android/internal/telephony/cdma/CDMALTEPhone;

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20
    .line 1243
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, " mLteSS="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 1244
    return-void
.end method

.method public getAllCellInfo()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1145
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1147
    .local v0, arrayList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/CellInfo;>;"
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@7
    monitor-enter v2

    #@8
    .line 1148
    :try_start_8
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d
    .line 1149
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_8 .. :try_end_e} :catchall_25

    #@e
    .line 1150
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "getAllCellInfo: arrayList="

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@24
    .line 1151
    return-object v0

    #@25
    .line 1149
    :catchall_25
    move-exception v1

    #@26
    :try_start_26
    monitor-exit v2
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_25

    #@27
    throw v1
.end method

.method protected getNewCdmaDataConnectionTracker(Lcom/android/internal/telephony/cdma/CDMAPhone;)Lcom/android/internal/telephony/DataConnectionTracker;
    .registers 3
    .parameter "phone"

    #@0
    .prologue
    .line 432
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;

    #@2
    invoke-direct {v0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V

    #@5
    return-object v0
.end method

.method protected getNewGsmDataConnectionTracker(Lcom/android/internal/telephony/PhoneBase;)Lcom/android/internal/telephony/DataConnectionTracker;
    .registers 3
    .parameter "phone"

    #@0
    .prologue
    .line 428
    new-instance v0, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    #@2
    invoke-direct {v0, p1}, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;-><init>(Lcom/android/internal/telephony/PhoneBase;)V

    #@5
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x3

    #@3
    .line 147
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@5
    iget-boolean v3, v3, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@7
    if-nez v3, :cond_43

    #@9
    .line 148
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "Received message "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    const-string v4, "["

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    iget v4, p1, Landroid/os/Message;->what:I

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    const-string v4, "]"

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    const-string v4, " while being destroyed. Ignoring."

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->loge(Ljava/lang/String;)V

    #@37
    .line 151
    if-eqz p1, :cond_42

    #@39
    iget v3, p1, Landroid/os/Message;->what:I

    #@3b
    const/16 v4, 0xb

    #@3d
    if-ne v3, v4, :cond_42

    #@3f
    .line 152
    invoke-super {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->handleMessage(Landroid/os/Message;)V

    #@42
    .line 223
    :cond_42
    :goto_42
    return-void

    #@43
    .line 158
    :cond_43
    iget v3, p1, Landroid/os/Message;->what:I

    #@45
    sparse-switch v3, :sswitch_data_136

    #@48
    .line 221
    invoke-super {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->handleMessage(Landroid/os/Message;)V

    #@4b
    goto :goto_42

    #@4c
    .line 160
    :sswitch_4c
    const-string v3, "handleMessage EVENT_POLL_STATE_GPRS"

    #@4e
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@51
    .line 161
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@53
    check-cast v0, Landroid/os/AsyncResult;

    #@55
    .line 162
    .local v0, ar:Landroid/os/AsyncResult;
    iget v3, p1, Landroid/os/Message;->what:I

    #@57
    invoke-virtual {p0, v3, v0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->handlePollStateResult(ILandroid/os/AsyncResult;)V

    #@5a
    goto :goto_42

    #@5b
    .line 165
    .end local v0           #ar:Landroid/os/AsyncResult;
    :sswitch_5b
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@5d
    check-cast v1, Lcom/android/internal/telephony/uicc/RuimRecords;

    #@5f
    .line 166
    .local v1, ruim:Lcom/android/internal/telephony/uicc/RuimRecords;
    if-eqz v1, :cond_10a

    #@61
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->isProvisioned()Z

    #@64
    move-result v3

    #@65
    if-eqz v3, :cond_10a

    #@67
    .line 167
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->getMdn()Ljava/lang/String;

    #@6a
    move-result-object v3

    #@6b
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@6d
    .line 170
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@6f
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@72
    move-result-object v3

    #@73
    const-string v4, "support_assisted_dialing"

    #@75
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@78
    move-result v3

    #@79
    if-eqz v3, :cond_ed

    #@7b
    .line 171
    new-instance v3, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v4, "handleMessage EVENT_RUIM_RECORDS_LOADED : "

    #@82
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@88
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v3

    #@8c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v3

    #@90
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@93
    .line 173
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->isIccIdChanged()Z

    #@96
    move-result v3

    #@97
    if-eqz v3, :cond_ed

    #@99
    .line 174
    const-string v3, "isIccIdChanged = true, Area/Length Update"

    #@9b
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@9e
    .line 175
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@a0
    if-eqz v3, :cond_ed

    #@a2
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@a4
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@a7
    move-result v3

    #@a8
    if-lt v3, v6, :cond_ed

    #@aa
    .line 176
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@ac
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@af
    move-result-object v3

    #@b0
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b3
    move-result-object v3

    #@b4
    const-string v4, "area"

    #@b6
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@b8
    invoke-virtual {v5, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@bb
    move-result-object v5

    #@bc
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@bf
    .line 180
    new-instance v2, Landroid/content/ContentValues;

    #@c1
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@c4
    .line 181
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "area"

    #@c6
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@c8
    invoke-virtual {v4, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@cb
    move-result-object v4

    #@cc
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@cf
    .line 182
    const-string v3, "length"

    #@d1
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@d3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@d6
    move-result v4

    #@d7
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@da
    move-result-object v4

    #@db
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@de
    .line 183
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@e0
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@e3
    move-result-object v3

    #@e4
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e7
    move-result-object v3

    #@e8
    sget-object v4, Landroid/provider/Settings$AssistDial;->CONTENT_URI:Landroid/net/Uri;

    #@ea
    invoke-virtual {v3, v4, v2, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@ed
    .line 191
    .end local v2           #values:Landroid/content/ContentValues;
    :cond_ed
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->getMin()Ljava/lang/String;

    #@f0
    move-result-object v3

    #@f1
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMin:Ljava/lang/String;

    #@f3
    .line 192
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->getSid()Ljava/lang/String;

    #@f6
    move-result-object v3

    #@f7
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->getNid()Ljava/lang/String;

    #@fa
    move-result-object v4

    #@fb
    invoke-virtual {p0, v3, v4}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->parseSidNid(Ljava/lang/String;Ljava/lang/String;)V

    #@fe
    .line 193
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->getPrlVersion()Ljava/lang/String;

    #@101
    move-result-object v3

    #@102
    iput-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mPrlVersion:Ljava/lang/String;

    #@104
    .line 194
    const/4 v3, 0x1

    #@105
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsMinInfoReady:Z

    #@107
    .line 195
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->updateOtaspState()V

    #@10a
    .line 200
    :cond_10a
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->pollState()V

    #@10d
    goto/16 :goto_42

    #@10f
    .line 205
    .end local v1           #ruim:Lcom/android/internal/telephony/uicc/RuimRecords;
    :sswitch_10f
    const-string v3, "[IMS_AFW] EVENT_GET_LTE_INFO_DONE"

    #@111
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@114
    .line 206
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@116
    check-cast v3, Landroid/os/AsyncResult;

    #@118
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->onSetLteInfo(Landroid/os/AsyncResult;)V

    #@11b
    goto/16 :goto_42

    #@11d
    .line 209
    :sswitch_11d
    const-string v3, "[IMS_AFW] EVENT_GET_EHRPD_INFO_DONE"

    #@11f
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@122
    .line 210
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@124
    check-cast v3, Landroid/os/AsyncResult;

    #@126
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->onSetEhrpdInfo(Landroid/os/AsyncResult;)V

    #@129
    goto/16 :goto_42

    #@12b
    .line 216
    :sswitch_12b
    const-string v3, "[EHRPD_IPV6] Got event EVENT_BLOCK_EHRPD_INTERNET_IPV6"

    #@12d
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@130
    .line 217
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->on_Ehrpd_Internet_Ipv6_block_requested()V

    #@133
    goto/16 :goto_42

    #@135
    .line 158
    nop

    #@136
    :sswitch_data_136
    .sparse-switch
        0x5 -> :sswitch_4c
        0x1b -> :sswitch_5b
        0x32 -> :sswitch_12b
        0x64 -> :sswitch_10f
        0x65 -> :sswitch_11d
    .end sparse-switch
.end method

.method protected handlePollStateResultMessage(ILandroid/os/AsyncResult;)V
    .registers 22
    .parameter "what"
    .parameter "ar"

    #@0
    .prologue
    .line 240
    const/4 v2, 0x5

    #@1
    move/from16 v0, p1

    #@3
    if-ne v0, v2, :cond_399

    #@5
    .line 241
    move-object/from16 v0, p2

    #@7
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@9
    check-cast v2, [Ljava/lang/String;

    #@b
    move-object v15, v2

    #@c
    check-cast v15, [Ljava/lang/String;

    #@e
    .line 243
    .local v15, states:[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v18, "handlePollStateResultMessage: EVENT_POLL_STATE_GPRS states.length="

    #@15
    move-object/from16 v0, v18

    #@17
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    array-length v0, v15

    #@1c
    move/from16 v18, v0

    #@1e
    move/from16 v0, v18

    #@20
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    const-string v18, " states="

    #@26
    move-object/from16 v0, v18

    #@28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    move-object/from16 v0, p0

    #@36
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@39
    .line 247
    const/16 v17, 0x0

    #@3b
    .line 248
    .local v17, type:I
    const/4 v13, -0x1

    #@3c
    .line 250
    .local v13, regState:I
    const/4 v14, 0x1

    #@3d
    .line 251
    .local v14, roamingIndicator_data:I
    const/16 v16, 0x0

    #@3f
    .line 252
    .local v16, systemIsInPrl_data:I
    const/4 v9, 0x1

    #@40
    .line 254
    .local v9, defaultRoamingIndicator_data:I
    array-length v2, v15

    #@41
    if-lez v2, :cond_136

    #@43
    .line 256
    const/4 v2, 0x0

    #@44
    :try_start_44
    aget-object v2, v15, v2

    #@46
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@49
    move-result v13

    #@4a
    .line 259
    array-length v2, v15

    #@4b
    const/16 v18, 0x4

    #@4d
    move/from16 v0, v18

    #@4f
    if-lt v2, v0, :cond_5d

    #@51
    const/4 v2, 0x3

    #@52
    aget-object v2, v15, v2

    #@54
    if-eqz v2, :cond_5d

    #@56
    .line 260
    const/4 v2, 0x3

    #@57
    aget-object v2, v15, v2

    #@59
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_5c
    .catch Ljava/lang/NumberFormatException; {:try_start_44 .. :try_end_5c} :catch_250

    #@5c
    move-result v17

    #@5d
    .line 266
    :cond_5d
    :goto_5d
    array-length v2, v15

    #@5e
    const/16 v18, 0xa

    #@60
    move/from16 v0, v18

    #@62
    if-lt v2, v0, :cond_dc

    #@64
    .line 273
    const/4 v12, 0x0

    #@65
    .line 276
    .local v12, operatorNumeric:Ljava/lang/String;
    :try_start_65
    move-object/from16 v0, p0

    #@67
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@69
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@6c
    move-result-object v12

    #@6d
    .line 277
    const/4 v2, 0x0

    #@6e
    const/16 v18, 0x3

    #@70
    move/from16 v0, v18

    #@72
    invoke-virtual {v12, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@75
    move-result-object v2

    #@76
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_79
    .catch Ljava/lang/Exception; {:try_start_65 .. :try_end_79} :catch_26d

    #@79
    move-result v3

    #@7a
    .line 290
    .local v3, mcc:I
    :goto_7a
    const/4 v2, 0x3

    #@7b
    :try_start_7b
    invoke-virtual {v12, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@7e
    move-result-object v2

    #@7f
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_82
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_82} :catch_2b3

    #@82
    move-result v4

    #@83
    .line 300
    .local v4, mnc:I
    :goto_83
    const/4 v2, 0x6

    #@84
    :try_start_84
    aget-object v2, v15, v2

    #@86
    invoke-static {v2}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@89
    move-result-object v2

    #@8a
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_8d
    .catch Ljava/lang/Exception; {:try_start_84 .. :try_end_8d} :catch_2df

    #@8d
    move-result v7

    #@8e
    .line 307
    .local v7, tac:I
    :goto_8e
    const/4 v2, 0x7

    #@8f
    :try_start_8f
    aget-object v2, v15, v2

    #@91
    invoke-static {v2}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@94
    move-result-object v2

    #@95
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_98
    .catch Ljava/lang/Exception; {:try_start_8f .. :try_end_98} :catch_311

    #@98
    move-result v6

    #@99
    .line 314
    .local v6, pci:I
    :goto_99
    const/16 v2, 0x8

    #@9b
    :try_start_9b
    aget-object v2, v15, v2

    #@9d
    invoke-static {v2}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@a0
    move-result-object v2

    #@a1
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_a4
    .catch Ljava/lang/Exception; {:try_start_9b .. :try_end_a4} :catch_343

    #@a4
    move-result v5

    #@a5
    .line 321
    .local v5, eci:I
    :goto_a5
    const/16 v2, 0x9

    #@a7
    :try_start_a7
    aget-object v2, v15, v2

    #@a9
    invoke-static {v2}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@ac
    move-result-object v2

    #@ad
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_b0
    .catch Ljava/lang/Exception; {:try_start_a7 .. :try_end_b0} :catch_375

    #@b0
    move-result v8

    #@b1
    .line 328
    .local v8, csgid:I
    :goto_b1
    new-instance v2, Landroid/telephony/CellIdentityLte;

    #@b3
    invoke-direct/range {v2 .. v7}, Landroid/telephony/CellIdentityLte;-><init>(IIIII)V

    #@b6
    move-object/from16 v0, p0

    #@b8
    iput-object v2, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@ba
    .line 330
    new-instance v2, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v18, "handlePollStateResultMessage: mNewLteCellIdentity="

    #@c1
    move-object/from16 v0, v18

    #@c3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v2

    #@c7
    move-object/from16 v0, p0

    #@c9
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@cb
    move-object/from16 v18, v0

    #@cd
    move-object/from16 v0, v18

    #@cf
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v2

    #@d3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v2

    #@d7
    move-object/from16 v0, p0

    #@d9
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@dc
    .line 335
    .end local v3           #mcc:I
    .end local v4           #mnc:I
    .end local v5           #eci:I
    .end local v6           #pci:I
    .end local v7           #tac:I
    .end local v8           #csgid:I
    .end local v12           #operatorNumeric:Ljava/lang/String;
    :cond_dc
    const/4 v2, 0x0

    #@dd
    const-string v18, "vzw_eri"

    #@df
    move-object/from16 v0, v18

    #@e1
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e4
    move-result v2

    #@e5
    if-eqz v2, :cond_136

    #@e7
    .line 336
    array-length v2, v15

    #@e8
    const/16 v18, 0xe

    #@ea
    move/from16 v0, v18

    #@ec
    if-lt v2, v0, :cond_136

    #@ee
    .line 337
    const/16 v2, 0xb

    #@f0
    aget-object v2, v15, v2

    #@f2
    if-eqz v2, :cond_106

    #@f4
    const/16 v2, 0xb

    #@f6
    aget-object v2, v15, v2

    #@f8
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@fb
    move-result v2

    #@fc
    if-lez v2, :cond_106

    #@fe
    .line 338
    const/16 v2, 0xb

    #@100
    aget-object v2, v15, v2

    #@102
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@105
    move-result v14

    #@106
    .line 340
    :cond_106
    const/16 v2, 0xc

    #@108
    aget-object v2, v15, v2

    #@10a
    if-eqz v2, :cond_11e

    #@10c
    const/16 v2, 0xc

    #@10e
    aget-object v2, v15, v2

    #@110
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@113
    move-result v2

    #@114
    if-lez v2, :cond_11e

    #@116
    .line 341
    const/16 v2, 0xc

    #@118
    aget-object v2, v15, v2

    #@11a
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@11d
    move-result v16

    #@11e
    .line 343
    :cond_11e
    const/16 v2, 0xd

    #@120
    aget-object v2, v15, v2

    #@122
    if-eqz v2, :cond_136

    #@124
    const/16 v2, 0xd

    #@126
    aget-object v2, v15, v2

    #@128
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@12b
    move-result v2

    #@12c
    if-lez v2, :cond_136

    #@12e
    .line 344
    const/16 v2, 0xd

    #@130
    aget-object v2, v15, v2

    #@132
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@135
    move-result v9

    #@136
    .line 351
    :cond_136
    move-object/from16 v0, p0

    #@138
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@13a
    move/from16 v0, v17

    #@13c
    invoke-virtual {v2, v0}, Landroid/telephony/ServiceState;->setRadioTechnology(I)V

    #@13f
    .line 352
    move-object/from16 v0, p0

    #@141
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@143
    move-object/from16 v0, p0

    #@145
    invoke-virtual {v0, v13}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->regCodeToServiceState(I)I

    #@148
    move-result v18

    #@149
    move/from16 v0, v18

    #@14b
    invoke-virtual {v2, v0}, Landroid/telephony/ServiceState;->setState(I)V

    #@14e
    .line 354
    const/4 v2, 0x0

    #@14f
    const-string v18, "vzw_eri"

    #@151
    move-object/from16 v0, v18

    #@153
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@156
    move-result v2

    #@157
    if-eqz v2, :cond_38d

    #@159
    .line 355
    array-length v2, v15

    #@15a
    const/16 v18, 0xc

    #@15c
    move/from16 v0, v18

    #@15e
    if-lt v2, v0, :cond_37e

    #@160
    .line 356
    move-object/from16 v0, p0

    #@162
    invoke-virtual {v0, v13}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->regCodeIsRoaming(I)Z

    #@165
    move-result v2

    #@166
    if-eqz v2, :cond_37b

    #@168
    const/16 v2, 0xb

    #@16a
    aget-object v2, v15, v2

    #@16c
    move-object/from16 v0, p0

    #@16e
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->isRoamIndForHomeSystem_data(Ljava/lang/String;)Z

    #@171
    move-result v2

    #@172
    if-nez v2, :cond_37b

    #@174
    const/4 v2, 0x1

    #@175
    :goto_175
    move-object/from16 v0, p0

    #@177
    iput-boolean v2, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataRoaming:Z

    #@179
    .line 360
    :goto_179
    move-object/from16 v0, p0

    #@17b
    iput v14, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator_data:I

    #@17d
    .line 361
    if-nez v16, :cond_38a

    #@17f
    const/4 v2, 0x0

    #@180
    :goto_180
    move-object/from16 v0, p0

    #@182
    iput-boolean v2, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl_data:Z

    #@184
    .line 362
    move-object/from16 v0, p0

    #@186
    iput v9, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator_data:I

    #@188
    .line 363
    move/from16 v0, v17

    #@18a
    move-object/from16 v1, p0

    #@18c
    iput v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNetworkType_data:I

    #@18e
    .line 365
    sget-boolean v2, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->DBG_CALL:Z

    #@190
    if-eqz v2, :cond_21c

    #@192
    new-instance v2, Ljava/lang/StringBuilder;

    #@194
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@197
    const-string v18, "mDataRoaming="

    #@199
    move-object/from16 v0, v18

    #@19b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v2

    #@19f
    move-object/from16 v0, p0

    #@1a1
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataRoaming:Z

    #@1a3
    move/from16 v18, v0

    #@1a5
    move/from16 v0, v18

    #@1a7
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v2

    #@1ab
    const-string v18, " mRoamingIndicator_data="

    #@1ad
    move-object/from16 v0, v18

    #@1af
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v2

    #@1b3
    move-object/from16 v0, p0

    #@1b5
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator_data:I

    #@1b7
    move/from16 v18, v0

    #@1b9
    move/from16 v0, v18

    #@1bb
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1be
    move-result-object v2

    #@1bf
    const-string v18, " mIsInPrl_data="

    #@1c1
    move-object/from16 v0, v18

    #@1c3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c6
    move-result-object v2

    #@1c7
    move-object/from16 v0, p0

    #@1c9
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl_data:Z

    #@1cb
    move/from16 v18, v0

    #@1cd
    move/from16 v0, v18

    #@1cf
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v2

    #@1d3
    const-string v18, " mDefaultRoamingIndicator_data="

    #@1d5
    move-object/from16 v0, v18

    #@1d7
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v2

    #@1db
    move-object/from16 v0, p0

    #@1dd
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator_data:I

    #@1df
    move/from16 v18, v0

    #@1e1
    move/from16 v0, v18

    #@1e3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v2

    #@1e7
    const-string v18, " mNetworkType_data="

    #@1e9
    move-object/from16 v0, v18

    #@1eb
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v2

    #@1ef
    move-object/from16 v0, p0

    #@1f1
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNetworkType_data:I

    #@1f3
    move/from16 v18, v0

    #@1f5
    move/from16 v0, v18

    #@1f7
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v2

    #@1fb
    const-string v18, " mLteSS.getState()="

    #@1fd
    move-object/from16 v0, v18

    #@1ff
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@202
    move-result-object v2

    #@203
    move-object/from16 v0, p0

    #@205
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@207
    move-object/from16 v18, v0

    #@209
    invoke-virtual/range {v18 .. v18}, Landroid/telephony/ServiceState;->getState()I

    #@20c
    move-result v18

    #@20d
    move/from16 v0, v18

    #@20f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@212
    move-result-object v2

    #@213
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@216
    move-result-object v2

    #@217
    move-object/from16 v0, p0

    #@219
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@21c
    .line 372
    :cond_21c
    :goto_21c
    move-object/from16 v0, p0

    #@21e
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@220
    move-object/from16 v0, p0

    #@222
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataRoaming:Z

    #@224
    move/from16 v18, v0

    #@226
    move/from16 v0, v18

    #@228
    invoke-virtual {v2, v0}, Landroid/telephony/ServiceState;->setRoaming(Z)V

    #@22b
    .line 374
    move-object/from16 v0, p0

    #@22d
    iget-boolean v2, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataRoaming:Z

    #@22f
    if-eqz v2, :cond_23c

    #@231
    move-object/from16 v0, p0

    #@233
    iget-object v2, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@235
    const/16 v18, 0x1

    #@237
    move/from16 v0, v18

    #@239
    invoke-virtual {v2, v0}, Landroid/telephony/ServiceState;->setRoaming(Z)V

    #@23c
    .line 375
    :cond_23c
    move-object/from16 v0, p0

    #@23e
    iget-object v2, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@240
    move-object/from16 v0, p0

    #@242
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@244
    move-object/from16 v18, v0

    #@246
    invoke-virtual/range {v18 .. v18}, Landroid/telephony/ServiceState;->getState()I

    #@249
    move-result v18

    #@24a
    move/from16 v0, v18

    #@24c
    invoke-virtual {v2, v0}, Landroid/telephony/ServiceState;->setDataState(I)V

    #@24f
    .line 379
    .end local v9           #defaultRoamingIndicator_data:I
    .end local v13           #regState:I
    .end local v14           #roamingIndicator_data:I
    .end local v15           #states:[Ljava/lang/String;
    .end local v16           #systemIsInPrl_data:I
    .end local v17           #type:I
    :goto_24f
    return-void

    #@250
    .line 262
    .restart local v9       #defaultRoamingIndicator_data:I
    .restart local v13       #regState:I
    .restart local v14       #roamingIndicator_data:I
    .restart local v15       #states:[Ljava/lang/String;
    .restart local v16       #systemIsInPrl_data:I
    .restart local v17       #type:I
    :catch_250
    move-exception v11

    #@251
    .line 263
    .local v11, ex:Ljava/lang/NumberFormatException;
    new-instance v2, Ljava/lang/StringBuilder;

    #@253
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@256
    const-string v18, "handlePollStateResultMessage: error parsing GprsRegistrationState: "

    #@258
    move-object/from16 v0, v18

    #@25a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25d
    move-result-object v2

    #@25e
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@261
    move-result-object v2

    #@262
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@265
    move-result-object v2

    #@266
    move-object/from16 v0, p0

    #@268
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->loge(Ljava/lang/String;)V

    #@26b
    goto/16 :goto_5d

    #@26d
    .line 278
    .end local v11           #ex:Ljava/lang/NumberFormatException;
    .restart local v12       #operatorNumeric:Ljava/lang/String;
    :catch_26d
    move-exception v10

    #@26e
    .line 280
    .local v10, e:Ljava/lang/Exception;
    :try_start_26e
    move-object/from16 v0, p0

    #@270
    iget-object v2, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@272
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@275
    move-result-object v12

    #@276
    .line 281
    const/4 v2, 0x0

    #@277
    const/16 v18, 0x3

    #@279
    move/from16 v0, v18

    #@27b
    invoke-virtual {v12, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@27e
    move-result-object v2

    #@27f
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_282
    .catch Ljava/lang/Exception; {:try_start_26e .. :try_end_282} :catch_285

    #@282
    move-result v3

    #@283
    .restart local v3       #mcc:I
    goto/16 :goto_7a

    #@285
    .line 282
    .end local v3           #mcc:I
    :catch_285
    move-exception v11

    #@286
    .line 283
    .local v11, ex:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@288
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28b
    const-string v18, "handlePollStateResultMessage: bad mcc operatorNumeric="

    #@28d
    move-object/from16 v0, v18

    #@28f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@292
    move-result-object v2

    #@293
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@296
    move-result-object v2

    #@297
    const-string v18, " ex="

    #@299
    move-object/from16 v0, v18

    #@29b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29e
    move-result-object v2

    #@29f
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a2
    move-result-object v2

    #@2a3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a6
    move-result-object v2

    #@2a7
    move-object/from16 v0, p0

    #@2a9
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->loge(Ljava/lang/String;)V

    #@2ac
    .line 285
    const-string v12, ""

    #@2ae
    .line 286
    const v3, 0x7fffffff

    #@2b1
    .restart local v3       #mcc:I
    goto/16 :goto_7a

    #@2b3
    .line 291
    .end local v10           #e:Ljava/lang/Exception;
    .end local v11           #ex:Ljava/lang/Exception;
    :catch_2b3
    move-exception v10

    #@2b4
    .line 292
    .restart local v10       #e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2b9
    const-string v18, "handlePollStateResultMessage: bad mnc operatorNumeric="

    #@2bb
    move-object/from16 v0, v18

    #@2bd
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c0
    move-result-object v2

    #@2c1
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c4
    move-result-object v2

    #@2c5
    const-string v18, " e="

    #@2c7
    move-object/from16 v0, v18

    #@2c9
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cc
    move-result-object v2

    #@2cd
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d0
    move-result-object v2

    #@2d1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d4
    move-result-object v2

    #@2d5
    move-object/from16 v0, p0

    #@2d7
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->loge(Ljava/lang/String;)V

    #@2da
    .line 294
    const v4, 0x7fffffff

    #@2dd
    .restart local v4       #mnc:I
    goto/16 :goto_83

    #@2df
    .line 301
    .end local v10           #e:Ljava/lang/Exception;
    :catch_2df
    move-exception v10

    #@2e0
    .line 302
    .restart local v10       #e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@2e2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e5
    const-string v18, "handlePollStateResultMessage: bad tac states[6]="

    #@2e7
    move-object/from16 v0, v18

    #@2e9
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ec
    move-result-object v2

    #@2ed
    const/16 v18, 0x6

    #@2ef
    aget-object v18, v15, v18

    #@2f1
    move-object/from16 v0, v18

    #@2f3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f6
    move-result-object v2

    #@2f7
    const-string v18, " e="

    #@2f9
    move-object/from16 v0, v18

    #@2fb
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fe
    move-result-object v2

    #@2ff
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@302
    move-result-object v2

    #@303
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@306
    move-result-object v2

    #@307
    move-object/from16 v0, p0

    #@309
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->loge(Ljava/lang/String;)V

    #@30c
    .line 304
    const v7, 0x7fffffff

    #@30f
    .restart local v7       #tac:I
    goto/16 :goto_8e

    #@311
    .line 308
    .end local v10           #e:Ljava/lang/Exception;
    :catch_311
    move-exception v10

    #@312
    .line 309
    .restart local v10       #e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@314
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@317
    const-string v18, "handlePollStateResultMessage: bad pci states[7]="

    #@319
    move-object/from16 v0, v18

    #@31b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31e
    move-result-object v2

    #@31f
    const/16 v18, 0x7

    #@321
    aget-object v18, v15, v18

    #@323
    move-object/from16 v0, v18

    #@325
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@328
    move-result-object v2

    #@329
    const-string v18, " e="

    #@32b
    move-object/from16 v0, v18

    #@32d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@330
    move-result-object v2

    #@331
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@334
    move-result-object v2

    #@335
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@338
    move-result-object v2

    #@339
    move-object/from16 v0, p0

    #@33b
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->loge(Ljava/lang/String;)V

    #@33e
    .line 311
    const v6, 0x7fffffff

    #@341
    .restart local v6       #pci:I
    goto/16 :goto_99

    #@343
    .line 315
    .end local v10           #e:Ljava/lang/Exception;
    :catch_343
    move-exception v10

    #@344
    .line 316
    .restart local v10       #e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@346
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@349
    const-string v18, "handlePollStateResultMessage: bad eci states[8]="

    #@34b
    move-object/from16 v0, v18

    #@34d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@350
    move-result-object v2

    #@351
    const/16 v18, 0x8

    #@353
    aget-object v18, v15, v18

    #@355
    move-object/from16 v0, v18

    #@357
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35a
    move-result-object v2

    #@35b
    const-string v18, " e="

    #@35d
    move-object/from16 v0, v18

    #@35f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@362
    move-result-object v2

    #@363
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@366
    move-result-object v2

    #@367
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36a
    move-result-object v2

    #@36b
    move-object/from16 v0, p0

    #@36d
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->loge(Ljava/lang/String;)V

    #@370
    .line 318
    const v5, 0x7fffffff

    #@373
    .restart local v5       #eci:I
    goto/16 :goto_a5

    #@375
    .line 322
    .end local v10           #e:Ljava/lang/Exception;
    :catch_375
    move-exception v10

    #@376
    .line 326
    .restart local v10       #e:Ljava/lang/Exception;
    const v8, 0x7fffffff

    #@379
    .restart local v8       #csgid:I
    goto/16 :goto_b1

    #@37b
    .line 356
    .end local v3           #mcc:I
    .end local v4           #mnc:I
    .end local v5           #eci:I
    .end local v6           #pci:I
    .end local v7           #tac:I
    .end local v8           #csgid:I
    .end local v10           #e:Ljava/lang/Exception;
    .end local v12           #operatorNumeric:Ljava/lang/String;
    :cond_37b
    const/4 v2, 0x0

    #@37c
    goto/16 :goto_175

    #@37e
    .line 358
    :cond_37e
    move-object/from16 v0, p0

    #@380
    invoke-virtual {v0, v13}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->regCodeIsRoaming(I)Z

    #@383
    move-result v2

    #@384
    move-object/from16 v0, p0

    #@386
    iput-boolean v2, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataRoaming:Z

    #@388
    goto/16 :goto_179

    #@38a
    .line 361
    :cond_38a
    const/4 v2, 0x1

    #@38b
    goto/16 :goto_180

    #@38d
    .line 371
    :cond_38d
    move-object/from16 v0, p0

    #@38f
    invoke-virtual {v0, v13}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->regCodeIsRoaming(I)Z

    #@392
    move-result v2

    #@393
    move-object/from16 v0, p0

    #@395
    iput-boolean v2, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataRoaming:Z

    #@397
    goto/16 :goto_21c

    #@399
    .line 377
    .end local v9           #defaultRoamingIndicator_data:I
    .end local v13           #regState:I
    .end local v14           #roamingIndicator_data:I
    .end local v15           #states:[Ljava/lang/String;
    .end local v16           #systemIsInPrl_data:I
    .end local v17           #type:I
    :cond_399
    invoke-super/range {p0 .. p2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->handlePollStateResultMessage(ILandroid/os/AsyncResult;)V

    #@39c
    goto/16 :goto_24f
.end method

.method public isConcurrentVoiceAndDataAllowed()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1072
    iget v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@3
    const/16 v2, 0xe

    #@5
    if-ne v1, v2, :cond_8

    #@7
    .line 1075
    :cond_7
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@a
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getCssIndicator()I

    #@d
    move-result v1

    #@e
    if-eq v1, v0, :cond_7

    #@10
    const/4 v0, 0x0

    #@11
    goto :goto_7
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 1230
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[CdmaLteSST] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1231
    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 1235
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[CdmaLteSST] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1236
    return-void
.end method

.method protected onSignalStrengthResult(Landroid/os/AsyncResult;Z)Z
    .registers 11
    .parameter "ar"
    .parameter "isGsm"

    #@0
    .prologue
    const/16 v4, 0xe

    #@2
    .line 1041
    iget v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@4
    if-ne v2, v4, :cond_7

    #@6
    .line 1042
    const/4 p2, 0x1

    #@7
    .line 1046
    :cond_7
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@9
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@c
    move-result-object v2

    #@d
    const-string v3, "support_svlte"

    #@f
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_16

    #@15
    .line 1047
    const/4 p2, 0x0

    #@16
    .line 1051
    :cond_16
    invoke-super {p0, p1, p2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->onSignalStrengthResult(Landroid/os/AsyncResult;Z)Z

    #@19
    move-result v1

    #@1a
    .line 1053
    .local v1, ssChanged:Z
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@1c
    monitor-enter v3

    #@1d
    .line 1054
    :try_start_1d
    iget v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@1f
    if-ne v2, v4, :cond_41

    #@21
    .line 1055
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@23
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@26
    move-result-wide v4

    #@27
    const-wide/16 v6, 0x3e8

    #@29
    mul-long/2addr v4, v6

    #@2a
    invoke-virtual {v2, v4, v5}, Landroid/telephony/CellInfoLte;->setTimeStamp(J)V

    #@2d
    .line 1056
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@2f
    const/4 v4, 0x4

    #@30
    invoke-virtual {v2, v4}, Landroid/telephony/CellInfoLte;->setTimeStampType(I)V

    #@33
    .line 1057
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@35
    invoke-virtual {v2}, Landroid/telephony/CellInfoLte;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthLte;

    #@38
    move-result-object v2

    #@39
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@3b
    const v5, 0x7fffffff

    #@3e
    invoke-virtual {v2, v4, v5}, Landroid/telephony/CellSignalStrengthLte;->initialize(Landroid/telephony/SignalStrength;I)V

    #@41
    .line 1060
    :cond_41
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@43
    invoke-virtual {v2}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    #@46
    move-result-object v2

    #@47
    if-eqz v2, :cond_58

    #@49
    .line 1061
    new-instance v0, Ljava/util/ArrayList;

    #@4b
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@4e
    .line 1062
    .local v0, arrayCi:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/CellInfo;>;"
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mCellInfoLte:Landroid/telephony/CellInfoLte;

    #@50
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@53
    .line 1063
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPhoneBase:Lcom/android/internal/telephony/PhoneBase;

    #@55
    invoke-virtual {v2, v0}, Lcom/android/internal/telephony/PhoneBase;->notifyCellInfo(Ljava/util/List;)V

    #@58
    .line 1065
    .end local v0           #arrayCi:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/CellInfo;>;"
    :cond_58
    monitor-exit v3

    #@59
    .line 1066
    return v1

    #@5a
    .line 1065
    :catchall_5a
    move-exception v2

    #@5b
    monitor-exit v3
    :try_end_5c
    .catchall {:try_start_1d .. :try_end_5c} :catchall_5a

    #@5c
    throw v2
.end method

.method protected pollState()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 383
    const/4 v0, 0x1

    #@2
    new-array v0, v0, [I

    #@4
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@6
    .line 384
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@8
    aput v3, v0, v3

    #@a
    .line 386
    sget-object v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker$1;->$SwitchMap$com$android$internal$telephony$CommandsInterface$RadioState:[I

    #@c
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->ordinal()I

    #@15
    move-result v1

    #@16
    aget v0, v0, v1

    #@18
    packed-switch v0, :pswitch_data_80

    #@1b
    .line 410
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@1d
    aget v1, v0, v3

    #@1f
    add-int/lit8 v1, v1, 0x1

    #@21
    aput v1, v0, v3

    #@23
    .line 412
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@25
    const/16 v1, 0x19

    #@27
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@29
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2c
    move-result-object v1

    #@2d
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getOperator(Landroid/os/Message;)V

    #@30
    .line 414
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@32
    aget v1, v0, v3

    #@34
    add-int/lit8 v1, v1, 0x1

    #@36
    aput v1, v0, v3

    #@38
    .line 416
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@3a
    const/16 v1, 0x18

    #@3c
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@3e
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@41
    move-result-object v1

    #@42
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getVoiceRegistrationState(Landroid/os/Message;)V

    #@45
    .line 419
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@47
    aget v1, v0, v3

    #@49
    add-int/lit8 v1, v1, 0x1

    #@4b
    aput v1, v0, v3

    #@4d
    .line 421
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@4f
    const/4 v1, 0x5

    #@50
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@52
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@55
    move-result-object v1

    #@56
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getDataRegistrationState(Landroid/os/Message;)V

    #@59
    .line 425
    :goto_59
    return-void

    #@5a
    .line 388
    :pswitch_5a
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@5c
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@5f
    .line 389
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@61
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->setStateInvalid()V

    #@64
    .line 390
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->setSignalStrengthDefaultValues()V

    #@67
    .line 391
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@69
    .line 393
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->pollStateDone()V

    #@6c
    goto :goto_59

    #@6d
    .line 397
    :pswitch_6d
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@6f
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->setStateOff()V

    #@72
    .line 398
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@74
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->setStateInvalid()V

    #@77
    .line 399
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->setSignalStrengthDefaultValues()V

    #@7a
    .line 400
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@7c
    .line 402
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->pollStateDone()V

    #@7f
    goto :goto_59

    #@80
    .line 386
    :pswitch_data_80
    .packed-switch 0x1
        :pswitch_5a
        :pswitch_6d
    .end packed-switch
.end method

.method protected pollStateDone()V
    .registers 58

    #@0
    .prologue
    .line 437
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@4
    move-object/from16 v51, v0

    #@6
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getRilRadioTechnology()I

    #@9
    move-result v51

    #@a
    move/from16 v0, v51

    #@c
    move-object/from16 v1, p0

    #@e
    iput v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@10
    .line 438
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@14
    move-object/from16 v51, v0

    #@16
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@19
    move-result v51

    #@1a
    move/from16 v0, v51

    #@1c
    move-object/from16 v1, p0

    #@1e
    iput v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@20
    .line 439
    move-object/from16 v0, p0

    #@22
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@24
    move-object/from16 v51, v0

    #@26
    move-object/from16 v0, p0

    #@28
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@2a
    move/from16 v52, v0

    #@2c
    invoke-virtual/range {v51 .. v52}, Landroid/telephony/ServiceState;->setRadioTechnology(I)V

    #@2f
    .line 440
    new-instance v51, Ljava/lang/StringBuilder;

    #@31
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v52, "pollStateDone CdmaLTEServiceState STATE_IN_SERVICE newNetworkType = "

    #@36
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v51

    #@3a
    move-object/from16 v0, p0

    #@3c
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@3e
    move/from16 v52, v0

    #@40
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v51

    #@44
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v51

    #@48
    move-object/from16 v0, p0

    #@4a
    move-object/from16 v1, v51

    #@4c
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@4f
    .line 452
    move-object/from16 v0, p0

    #@51
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@53
    move-object/from16 v51, v0

    #@55
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@58
    move-result v51

    #@59
    const/16 v52, 0x1

    #@5b
    move/from16 v0, v51

    #@5d
    move/from16 v1, v52

    #@5f
    if-ne v0, v1, :cond_9b

    #@61
    .line 453
    move-object/from16 v0, p0

    #@63
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@65
    move-object/from16 v51, v0

    #@67
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@6a
    move-result-object v51

    #@6b
    invoke-virtual/range {v51 .. v51}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6e
    move-result-object v51

    #@6f
    const-string v52, "preferred_network_mode"

    #@71
    sget v53, Lcom/android/internal/telephony/RILConstants;->PREFERRED_NETWORK_MODE:I

    #@73
    invoke-static/range {v51 .. v53}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@76
    move-result v41

    #@77
    .line 457
    .local v41, networkMode:I
    const/16 v51, 0xb

    #@79
    move/from16 v0, v41

    #@7b
    move/from16 v1, v51

    #@7d
    if-ne v0, v1, :cond_9b

    #@7f
    .line 458
    const-string v51, "pollState: LTE Only mode"

    #@81
    move-object/from16 v0, p0

    #@83
    move-object/from16 v1, v51

    #@85
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@88
    .line 459
    move-object/from16 v0, p0

    #@8a
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@8c
    move-object/from16 v51, v0

    #@8e
    move-object/from16 v0, p0

    #@90
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@92
    move-object/from16 v52, v0

    #@94
    invoke-virtual/range {v52 .. v52}, Landroid/telephony/ServiceState;->getState()I

    #@97
    move-result v52

    #@98
    invoke-virtual/range {v51 .. v52}, Landroid/telephony/ServiceState;->setState(I)V

    #@9b
    .line 463
    .end local v41           #networkMode:I
    :cond_9b
    new-instance v51, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v52, "pollStateDone: oldSS=["

    #@a2
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v51

    #@a6
    move-object/from16 v0, p0

    #@a8
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@aa
    move-object/from16 v52, v0

    #@ac
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v51

    #@b0
    const-string v52, "] newSS=["

    #@b2
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v51

    #@b6
    move-object/from16 v0, p0

    #@b8
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@ba
    move-object/from16 v52, v0

    #@bc
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v51

    #@c0
    const-string v52, "]"

    #@c2
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v51

    #@c6
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v51

    #@ca
    move-object/from16 v0, p0

    #@cc
    move-object/from16 v1, v51

    #@ce
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@d1
    .line 465
    move-object/from16 v0, p0

    #@d3
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@d5
    move-object/from16 v51, v0

    #@d7
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@da
    move-result v51

    #@db
    if-eqz v51, :cond_dc1

    #@dd
    move-object/from16 v0, p0

    #@df
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@e1
    move-object/from16 v51, v0

    #@e3
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@e6
    move-result v51

    #@e7
    if-nez v51, :cond_dc1

    #@e9
    const/16 v33, 0x1

    #@eb
    .line 468
    .local v33, hasRegistered:Z
    :goto_eb
    move-object/from16 v0, p0

    #@ed
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@ef
    move-object/from16 v51, v0

    #@f1
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@f4
    move-result v51

    #@f5
    if-nez v51, :cond_dc5

    #@f7
    move-object/from16 v0, p0

    #@f9
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@fb
    move-object/from16 v51, v0

    #@fd
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@100
    move-result v51

    #@101
    if-eqz v51, :cond_dc5

    #@103
    const/16 v26, 0x1

    #@105
    .line 471
    .local v26, hasDeregistered:Z
    :goto_105
    move-object/from16 v0, p0

    #@107
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@109
    move/from16 v51, v0

    #@10b
    if-eqz v51, :cond_dc9

    #@10d
    move-object/from16 v0, p0

    #@10f
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@111
    move/from16 v51, v0

    #@113
    if-nez v51, :cond_dc9

    #@115
    const/16 v22, 0x1

    #@117
    .line 475
    .local v22, hasCdmaDataConnectionAttached:Z
    :goto_117
    move-object/from16 v0, p0

    #@119
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@11b
    move/from16 v51, v0

    #@11d
    if-nez v51, :cond_dcd

    #@11f
    move-object/from16 v0, p0

    #@121
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@123
    move/from16 v51, v0

    #@125
    if-eqz v51, :cond_dcd

    #@127
    const/16 v24, 0x1

    #@129
    .line 479
    .local v24, hasCdmaDataConnectionDetached:Z
    :goto_129
    move-object/from16 v0, p0

    #@12b
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@12d
    move/from16 v51, v0

    #@12f
    move-object/from16 v0, p0

    #@131
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@133
    move/from16 v52, v0

    #@135
    move/from16 v0, v51

    #@137
    move/from16 v1, v52

    #@139
    if-eq v0, v1, :cond_dd1

    #@13b
    const/16 v23, 0x1

    #@13d
    .line 482
    .local v23, hasCdmaDataConnectionChanged:Z
    :goto_13d
    move-object/from16 v0, p0

    #@13f
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@141
    move/from16 v51, v0

    #@143
    move-object/from16 v0, p0

    #@145
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@147
    move/from16 v52, v0

    #@149
    move/from16 v0, v51

    #@14b
    move/from16 v1, v52

    #@14d
    if-eq v0, v1, :cond_dd5

    #@14f
    const/16 v32, 0x1

    #@151
    .line 484
    .local v32, hasRadioTechnologyChanged:Z
    :goto_151
    move-object/from16 v0, p0

    #@153
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@155
    move-object/from16 v51, v0

    #@157
    move-object/from16 v0, p0

    #@159
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@15b
    move-object/from16 v52, v0

    #@15d
    invoke-virtual/range {v51 .. v52}, Landroid/telephony/ServiceState;->equals(Ljava/lang/Object;)Z

    #@160
    move-result v51

    #@161
    if-nez v51, :cond_dd9

    #@163
    const/16 v25, 0x1

    #@165
    .line 486
    .local v25, hasChanged:Z
    :goto_165
    move-object/from16 v0, p0

    #@167
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@169
    move-object/from16 v51, v0

    #@16b
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@16e
    move-result v51

    #@16f
    if-nez v51, :cond_ddd

    #@171
    move-object/from16 v0, p0

    #@173
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@175
    move-object/from16 v51, v0

    #@177
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@17a
    move-result v51

    #@17b
    if-eqz v51, :cond_ddd

    #@17d
    const/16 v35, 0x1

    #@17f
    .line 488
    .local v35, hasRoamingOn:Z
    :goto_17f
    move-object/from16 v0, p0

    #@181
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@183
    move-object/from16 v51, v0

    #@185
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@188
    move-result v51

    #@189
    if-eqz v51, :cond_de1

    #@18b
    move-object/from16 v0, p0

    #@18d
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@18f
    move-object/from16 v51, v0

    #@191
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@194
    move-result v51

    #@195
    if-nez v51, :cond_de1

    #@197
    const/16 v34, 0x1

    #@199
    .line 490
    .local v34, hasRoamingOff:Z
    :goto_199
    move-object/from16 v0, p0

    #@19b
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@19d
    move-object/from16 v51, v0

    #@19f
    move-object/from16 v0, p0

    #@1a1
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@1a3
    move-object/from16 v52, v0

    #@1a5
    invoke-virtual/range {v51 .. v52}, Landroid/telephony/cdma/CdmaCellLocation;->equals(Ljava/lang/Object;)Z

    #@1a8
    move-result v51

    #@1a9
    if-nez v51, :cond_de5

    #@1ab
    const/16 v29, 0x1

    #@1ad
    .line 492
    .local v29, hasLocationChanged:Z
    :goto_1ad
    move-object/from16 v0, p0

    #@1af
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@1b1
    move/from16 v51, v0

    #@1b3
    if-nez v51, :cond_de9

    #@1b5
    move-object/from16 v0, p0

    #@1b7
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@1b9
    move/from16 v51, v0

    #@1bb
    const/16 v52, 0xe

    #@1bd
    move/from16 v0, v51

    #@1bf
    move/from16 v1, v52

    #@1c1
    if-ne v0, v1, :cond_1d1

    #@1c3
    move-object/from16 v0, p0

    #@1c5
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@1c7
    move/from16 v51, v0

    #@1c9
    const/16 v52, 0xd

    #@1cb
    move/from16 v0, v51

    #@1cd
    move/from16 v1, v52

    #@1cf
    if-eq v0, v1, :cond_1ed

    #@1d1
    :cond_1d1
    move-object/from16 v0, p0

    #@1d3
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@1d5
    move/from16 v51, v0

    #@1d7
    const/16 v52, 0xd

    #@1d9
    move/from16 v0, v51

    #@1db
    move/from16 v1, v52

    #@1dd
    if-ne v0, v1, :cond_de9

    #@1df
    move-object/from16 v0, p0

    #@1e1
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@1e3
    move/from16 v51, v0

    #@1e5
    const/16 v52, 0xe

    #@1e7
    move/from16 v0, v51

    #@1e9
    move/from16 v1, v52

    #@1eb
    if-ne v0, v1, :cond_de9

    #@1ed
    :cond_1ed
    const/16 v21, 0x1

    #@1ef
    .line 499
    .local v21, has4gHandoff:Z
    :goto_1ef
    move-object/from16 v0, p0

    #@1f1
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@1f3
    move/from16 v51, v0

    #@1f5
    const/16 v52, 0xe

    #@1f7
    move/from16 v0, v51

    #@1f9
    move/from16 v1, v52

    #@1fb
    if-eq v0, v1, :cond_20b

    #@1fd
    move-object/from16 v0, p0

    #@1ff
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@201
    move/from16 v51, v0

    #@203
    const/16 v52, 0xd

    #@205
    move/from16 v0, v51

    #@207
    move/from16 v1, v52

    #@209
    if-ne v0, v1, :cond_ded

    #@20b
    :cond_20b
    move-object/from16 v0, p0

    #@20d
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@20f
    move/from16 v51, v0

    #@211
    const/16 v52, 0xe

    #@213
    move/from16 v0, v51

    #@215
    move/from16 v1, v52

    #@217
    if-eq v0, v1, :cond_ded

    #@219
    move-object/from16 v0, p0

    #@21b
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@21d
    move/from16 v51, v0

    #@21f
    const/16 v52, 0xd

    #@221
    move/from16 v0, v51

    #@223
    move/from16 v1, v52

    #@225
    if-eq v0, v1, :cond_ded

    #@227
    const/16 v31, 0x1

    #@229
    .line 505
    .local v31, hasMultiApnSupport:Z
    :goto_229
    move-object/from16 v0, p0

    #@22b
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@22d
    move/from16 v51, v0

    #@22f
    const/16 v52, 0x4

    #@231
    move/from16 v0, v51

    #@233
    move/from16 v1, v52

    #@235
    if-lt v0, v1, :cond_df1

    #@237
    move-object/from16 v0, p0

    #@239
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@23b
    move/from16 v51, v0

    #@23d
    const/16 v52, 0x8

    #@23f
    move/from16 v0, v51

    #@241
    move/from16 v1, v52

    #@243
    if-gt v0, v1, :cond_df1

    #@245
    const/16 v30, 0x1

    #@247
    .line 510
    .local v30, hasLostMultiApnSupport:Z
    :goto_247
    move-object/from16 v0, p0

    #@249
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@24b
    move-object/from16 v51, v0

    #@24d
    invoke-interface/range {v51 .. v51}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@250
    move-result-object v51

    #@251
    move-object/from16 v0, v51

    #@253
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_CREATE_CDMADATACONNECTIONTRACKER:Z

    #@255
    move/from16 v51, v0

    #@257
    if-eqz v51, :cond_2b5

    #@259
    if-eqz v30, :cond_2b5

    #@25b
    .line 512
    const-string v51, "LGTBASE"

    #@25d
    move-object/from16 v0, p0

    #@25f
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@261
    move-object/from16 v52, v0

    #@263
    invoke-interface/range {v52 .. v52}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@266
    move-result-object v52

    #@267
    move-object/from16 v0, v52

    #@269
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@26b
    move-object/from16 v52, v0

    #@26d
    invoke-virtual/range {v51 .. v52}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@270
    move-result v51

    #@271
    if-eqz v51, :cond_df5

    #@273
    move-object/from16 v0, p0

    #@275
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@277
    move-object/from16 v51, v0

    #@279
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@27c
    move-result-object v51

    #@27d
    invoke-virtual/range {v51 .. v51}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@280
    move-result-object v51

    #@281
    const-string v52, "revsetting_hidden"

    #@283
    const/16 v53, 0x0

    #@285
    invoke-static/range {v51 .. v53}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@288
    move-result v51

    #@289
    const/16 v52, 0x1

    #@28b
    move/from16 v0, v51

    #@28d
    move/from16 v1, v52

    #@28f
    if-ne v0, v1, :cond_df5

    #@291
    .line 514
    new-instance v51, Ljava/lang/StringBuilder;

    #@293
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@296
    const-string v52, "[LG_DATA] Create CdmaDataConnectionTracker for LGU+ EVDO (by Hidden Menu - Rev.A == set), so, hasLostMultiApnSupport =  "

    #@298
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29b
    move-result-object v51

    #@29c
    move-object/from16 v0, v51

    #@29e
    move/from16 v1, v30

    #@2a0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a3
    move-result-object v51

    #@2a4
    const-string v52, " : true"

    #@2a6
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a9
    move-result-object v51

    #@2aa
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ad
    move-result-object v51

    #@2ae
    move-object/from16 v0, p0

    #@2b0
    move-object/from16 v1, v51

    #@2b2
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@2b5
    .line 524
    :cond_2b5
    :goto_2b5
    move-object/from16 v0, p0

    #@2b7
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2b9
    move-object/from16 v51, v0

    #@2bb
    invoke-interface/range {v51 .. v51}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2be
    move-result-object v51

    #@2bf
    move-object/from16 v0, v51

    #@2c1
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

    #@2c3
    move/from16 v51, v0

    #@2c5
    if-eqz v51, :cond_2e7

    #@2c7
    .line 526
    move-object/from16 v0, p0

    #@2c9
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@2cb
    move/from16 v51, v0

    #@2cd
    if-eqz v51, :cond_e17

    #@2cf
    move-object/from16 v0, p0

    #@2d1
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@2d3
    move/from16 v51, v0

    #@2d5
    if-nez v51, :cond_e17

    #@2d7
    move-object/from16 v0, p0

    #@2d9
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@2db
    move/from16 v51, v0

    #@2dd
    if-nez v51, :cond_e17

    #@2df
    .line 528
    const/16 v51, 0x1

    #@2e1
    move/from16 v0, v51

    #@2e3
    move-object/from16 v1, p0

    #@2e5
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->CheckATTACH:Z

    #@2e7
    .line 542
    :cond_2e7
    :goto_2e7
    const/16 v51, 0x0

    #@2e9
    const-string v52, "vzw_eri"

    #@2eb
    invoke-static/range {v51 .. v52}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@2ee
    move-result v51

    #@2ef
    if-eqz v51, :cond_384

    #@2f1
    .line 543
    move-object/from16 v0, p0

    #@2f3
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2f5
    move-object/from16 v51, v0

    #@2f7
    move-object/from16 v0, p0

    #@2f9
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@2fb
    move-object/from16 v52, v0

    #@2fd
    invoke-virtual/range {v52 .. v52}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@300
    move-result-object v52

    #@301
    invoke-virtual/range {v51 .. v52}, Landroid/telephony/ServiceState;->setOperatorAlphaLong(Ljava/lang/String;)V

    #@304
    .line 544
    move-object/from16 v0, p0

    #@306
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@308
    move-object/from16 v51, v0

    #@30a
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@30d
    move-result v51

    #@30e
    move-object/from16 v0, p0

    #@310
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@312
    move-object/from16 v52, v0

    #@314
    invoke-virtual/range {v52 .. v52}, Landroid/telephony/ServiceState;->getState()I

    #@317
    move-result v52

    #@318
    move/from16 v0, v51

    #@31a
    move/from16 v1, v52

    #@31c
    if-eq v0, v1, :cond_e53

    #@31e
    const/16 v51, 0x1

    #@320
    :goto_320
    move/from16 v0, v51

    #@322
    move-object/from16 v1, p0

    #@324
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@326
    .line 545
    move-object/from16 v0, p0

    #@328
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@32a
    move-object/from16 v51, v0

    #@32c
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getNetworkId()I

    #@32f
    move-result v51

    #@330
    move-object/from16 v0, p0

    #@332
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@334
    move-object/from16 v52, v0

    #@336
    invoke-virtual/range {v52 .. v52}, Landroid/telephony/ServiceState;->getNetworkId()I

    #@339
    move-result v52

    #@33a
    move/from16 v0, v51

    #@33c
    move/from16 v1, v52

    #@33e
    if-ne v0, v1, :cond_35a

    #@340
    move-object/from16 v0, p0

    #@342
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@344
    move-object/from16 v51, v0

    #@346
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getSystemId()I

    #@349
    move-result v51

    #@34a
    move-object/from16 v0, p0

    #@34c
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@34e
    move-object/from16 v52, v0

    #@350
    invoke-virtual/range {v52 .. v52}, Landroid/telephony/ServiceState;->getSystemId()I

    #@353
    move-result v52

    #@354
    move/from16 v0, v51

    #@356
    move/from16 v1, v52

    #@358
    if-eq v0, v1, :cond_e57

    #@35a
    :cond_35a
    const/16 v51, 0x1

    #@35c
    :goto_35c
    move/from16 v0, v51

    #@35e
    move-object/from16 v1, p0

    #@360
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedSystemIDNetworkID:Z

    #@362
    .line 547
    move-object/from16 v0, p0

    #@364
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@366
    move-object/from16 v51, v0

    #@368
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getCdmaRoamingIndicator()I

    #@36b
    move-result v51

    #@36c
    move-object/from16 v0, p0

    #@36e
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@370
    move-object/from16 v52, v0

    #@372
    invoke-virtual/range {v52 .. v52}, Landroid/telephony/ServiceState;->getCdmaRoamingIndicator()I

    #@375
    move-result v52

    #@376
    move/from16 v0, v51

    #@378
    move/from16 v1, v52

    #@37a
    if-eq v0, v1, :cond_e5b

    #@37c
    const/16 v51, 0x1

    #@37e
    :goto_37e
    move/from16 v0, v51

    #@380
    move-object/from16 v1, p0

    #@382
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedRoamingIndicator:Z

    #@384
    .line 552
    :cond_384
    const-string v51, "KDDI"

    #@386
    invoke-static/range {v51 .. v51}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@389
    move-result v51

    #@38a
    if-eqz v51, :cond_39c

    #@38c
    move-object/from16 v0, p0

    #@38e
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@390
    move/from16 v51, v0

    #@392
    const/16 v52, 0xc

    #@394
    move/from16 v0, v51

    #@396
    move/from16 v1, v52

    #@398
    if-ne v0, v1, :cond_39c

    #@39a
    .line 553
    const/16 v30, 0x1

    #@39c
    .line 558
    :cond_39c
    const-string v51, "ril.current.datatech"

    #@39e
    move-object/from16 v0, p0

    #@3a0
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@3a2
    move-object/from16 v52, v0

    #@3a4
    invoke-virtual/range {v52 .. v52}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@3a7
    move-result v52

    #@3a8
    invoke-static/range {v52 .. v52}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3ab
    move-result-object v52

    #@3ac
    invoke-static/range {v51 .. v52}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@3af
    .line 560
    move-object/from16 v0, p0

    #@3b1
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@3b3
    move-object/from16 v51, v0

    #@3b5
    invoke-interface/range {v51 .. v51}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@3b8
    move-result-object v51

    #@3b9
    move-object/from16 v0, v51

    #@3bb
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_IP_V6_BLOCK_CONFIG_ON_EHRPD_VZW:Z

    #@3bd
    move/from16 v51, v0

    #@3bf
    if-eqz v51, :cond_48f

    #@3c1
    .line 562
    move-object/from16 v0, p0

    #@3c3
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@3c5
    move-object/from16 v51, v0

    #@3c7
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@3ca
    move-result-object v51

    #@3cb
    invoke-virtual/range {v51 .. v51}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3ce
    move-result-object v51

    #@3cf
    const-string v52, "data_ehrpd_internet_ipv6_enabled"

    #@3d1
    const/16 v53, 0x0

    #@3d3
    invoke-static/range {v51 .. v53}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3d6
    move-result v17

    #@3d7
    .line 564
    .local v17, ehrpd_internet_ipv6_enabled:I
    new-instance v51, Ljava/lang/StringBuilder;

    #@3d9
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@3dc
    const-string v52, "[EHRPD_IPV6] newSS Tech is "

    #@3de
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e1
    move-result-object v51

    #@3e2
    move-object/from16 v0, p0

    #@3e4
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@3e6
    move-object/from16 v52, v0

    #@3e8
    invoke-virtual/range {v52 .. v52}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@3eb
    move-result v52

    #@3ec
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3ef
    move-result-object v51

    #@3f0
    const-string v52, ", ehrpd_internet_ipv6_enabled is "

    #@3f2
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f5
    move-result-object v51

    #@3f6
    move-object/from16 v0, v51

    #@3f8
    move/from16 v1, v17

    #@3fa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3fd
    move-result-object v51

    #@3fe
    const-string v52, ", currently blocked "

    #@400
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@403
    move-result-object v51

    #@404
    sget-boolean v52, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@406
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@409
    move-result-object v51

    #@40a
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40d
    move-result-object v51

    #@40e
    move-object/from16 v0, p0

    #@410
    move-object/from16 v1, v51

    #@412
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@415
    .line 565
    const/16 v51, 0x1

    #@417
    move/from16 v0, v17

    #@419
    move/from16 v1, v51

    #@41b
    if-ne v0, v1, :cond_e5f

    #@41d
    .line 567
    new-instance v51, Ljava/lang/StringBuilder;

    #@41f
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@422
    const-string v52, "[EHRPD_IPV6] ehrpd_internet_ipv6_enabled is "

    #@424
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@427
    move-result-object v51

    #@428
    move-object/from16 v0, v51

    #@42a
    move/from16 v1, v17

    #@42c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42f
    move-result-object v51

    #@430
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@433
    move-result-object v51

    #@434
    move-object/from16 v0, p0

    #@436
    move-object/from16 v1, v51

    #@438
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@43b
    .line 649
    :cond_43b
    :goto_43b
    move-object/from16 v0, p0

    #@43d
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@43f
    move-object/from16 v51, v0

    #@441
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@444
    move-result-object v51

    #@445
    invoke-virtual/range {v51 .. v51}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@448
    move-result-object v51

    #@449
    const-string v52, "com.verizon.hardware.telephony.lte"

    #@44b
    invoke-virtual/range {v51 .. v52}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@44e
    move-result v28

    #@44f
    .line 650
    .local v28, hasLTE:Z
    move-object/from16 v0, p0

    #@451
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@453
    move-object/from16 v51, v0

    #@455
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@458
    move-result-object v51

    #@459
    invoke-virtual/range {v51 .. v51}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@45c
    move-result-object v51

    #@45d
    const-string v52, "com.verizon.hardware.telephony.ehrpd"

    #@45f
    invoke-virtual/range {v51 .. v52}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@462
    move-result v27

    #@463
    .line 651
    .local v27, hasEHRPD:Z
    new-instance v51, Ljava/lang/StringBuilder;

    #@465
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@468
    const-string v52, "[Data meter test plan] hasLTE = "

    #@46a
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46d
    move-result-object v51

    #@46e
    move-object/from16 v0, v51

    #@470
    move/from16 v1, v28

    #@472
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@475
    move-result-object v51

    #@476
    const-string v52, " hasEHRPD = "

    #@478
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47b
    move-result-object v51

    #@47c
    move-object/from16 v0, v51

    #@47e
    move/from16 v1, v27

    #@480
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@483
    move-result-object v51

    #@484
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@487
    move-result-object v51

    #@488
    move-object/from16 v0, p0

    #@48a
    move-object/from16 v1, v51

    #@48c
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@48f
    .line 657
    .end local v17           #ehrpd_internet_ipv6_enabled:I
    .end local v27           #hasEHRPD:Z
    .end local v28           #hasLTE:Z
    :cond_48f
    new-instance v51, Ljava/lang/StringBuilder;

    #@491
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@494
    const-string v52, "pollStateDone: hasRegistered="

    #@496
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@499
    move-result-object v51

    #@49a
    move-object/from16 v0, v51

    #@49c
    move/from16 v1, v33

    #@49e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4a1
    move-result-object v51

    #@4a2
    const-string v52, " hasDeegistered="

    #@4a4
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a7
    move-result-object v51

    #@4a8
    move-object/from16 v0, v51

    #@4aa
    move/from16 v1, v26

    #@4ac
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4af
    move-result-object v51

    #@4b0
    const-string v52, " hasCdmaDataConnectionAttached="

    #@4b2
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b5
    move-result-object v51

    #@4b6
    move-object/from16 v0, v51

    #@4b8
    move/from16 v1, v22

    #@4ba
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4bd
    move-result-object v51

    #@4be
    const-string v52, " hasCdmaDataConnectionDetached="

    #@4c0
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c3
    move-result-object v51

    #@4c4
    move-object/from16 v0, v51

    #@4c6
    move/from16 v1, v24

    #@4c8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4cb
    move-result-object v51

    #@4cc
    const-string v52, " hasCdmaDataConnectionChanged="

    #@4ce
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d1
    move-result-object v51

    #@4d2
    move-object/from16 v0, v51

    #@4d4
    move/from16 v1, v23

    #@4d6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4d9
    move-result-object v51

    #@4da
    const-string v52, " hasRadioTechnologyChanged = "

    #@4dc
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4df
    move-result-object v51

    #@4e0
    move-object/from16 v0, v51

    #@4e2
    move/from16 v1, v32

    #@4e4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4e7
    move-result-object v51

    #@4e8
    const-string v52, " hasChanged="

    #@4ea
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ed
    move-result-object v51

    #@4ee
    move-object/from16 v0, v51

    #@4f0
    move/from16 v1, v25

    #@4f2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4f5
    move-result-object v51

    #@4f6
    const-string v52, " hasRoamingOn="

    #@4f8
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4fb
    move-result-object v51

    #@4fc
    move-object/from16 v0, v51

    #@4fe
    move/from16 v1, v35

    #@500
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@503
    move-result-object v51

    #@504
    const-string v52, " hasRoamingOff="

    #@506
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@509
    move-result-object v51

    #@50a
    move-object/from16 v0, v51

    #@50c
    move/from16 v1, v34

    #@50e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@511
    move-result-object v51

    #@512
    const-string v52, " hasLocationChanged="

    #@514
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@517
    move-result-object v51

    #@518
    move-object/from16 v0, v51

    #@51a
    move/from16 v1, v29

    #@51c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@51f
    move-result-object v51

    #@520
    const-string v52, " has4gHandoff = "

    #@522
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@525
    move-result-object v51

    #@526
    move-object/from16 v0, v51

    #@528
    move/from16 v1, v21

    #@52a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@52d
    move-result-object v51

    #@52e
    const-string v52, " hasMultiApnSupport="

    #@530
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@533
    move-result-object v51

    #@534
    move-object/from16 v0, v51

    #@536
    move/from16 v1, v31

    #@538
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@53b
    move-result-object v51

    #@53c
    const-string v52, " hasLostMultiApnSupport="

    #@53e
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@541
    move-result-object v51

    #@542
    move-object/from16 v0, v51

    #@544
    move/from16 v1, v30

    #@546
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@549
    move-result-object v51

    #@54a
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54d
    move-result-object v51

    #@54e
    move-object/from16 v0, p0

    #@550
    move-object/from16 v1, v51

    #@552
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@555
    .line 673
    move-object/from16 v0, p0

    #@557
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@559
    move-object/from16 v51, v0

    #@55b
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@55e
    move-result v51

    #@55f
    move-object/from16 v0, p0

    #@561
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@563
    move-object/from16 v52, v0

    #@565
    invoke-virtual/range {v52 .. v52}, Landroid/telephony/ServiceState;->getState()I

    #@568
    move-result v52

    #@569
    move/from16 v0, v51

    #@56b
    move/from16 v1, v52

    #@56d
    if-ne v0, v1, :cond_581

    #@56f
    move-object/from16 v0, p0

    #@571
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@573
    move/from16 v51, v0

    #@575
    move-object/from16 v0, p0

    #@577
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@579
    move/from16 v52, v0

    #@57b
    move/from16 v0, v51

    #@57d
    move/from16 v1, v52

    #@57f
    if-eq v0, v1, :cond_5cf

    #@581
    .line 675
    :cond_581
    const v51, 0xc3c4

    #@584
    const/16 v52, 0x4

    #@586
    move/from16 v0, v52

    #@588
    new-array v0, v0, [Ljava/lang/Object;

    #@58a
    move-object/from16 v52, v0

    #@58c
    const/16 v53, 0x0

    #@58e
    move-object/from16 v0, p0

    #@590
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@592
    move-object/from16 v54, v0

    #@594
    invoke-virtual/range {v54 .. v54}, Landroid/telephony/ServiceState;->getState()I

    #@597
    move-result v54

    #@598
    invoke-static/range {v54 .. v54}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59b
    move-result-object v54

    #@59c
    aput-object v54, v52, v53

    #@59e
    const/16 v53, 0x1

    #@5a0
    move-object/from16 v0, p0

    #@5a2
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@5a4
    move/from16 v54, v0

    #@5a6
    invoke-static/range {v54 .. v54}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5a9
    move-result-object v54

    #@5aa
    aput-object v54, v52, v53

    #@5ac
    const/16 v53, 0x2

    #@5ae
    move-object/from16 v0, p0

    #@5b0
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@5b2
    move-object/from16 v54, v0

    #@5b4
    invoke-virtual/range {v54 .. v54}, Landroid/telephony/ServiceState;->getState()I

    #@5b7
    move-result v54

    #@5b8
    invoke-static/range {v54 .. v54}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5bb
    move-result-object v54

    #@5bc
    aput-object v54, v52, v53

    #@5be
    const/16 v53, 0x3

    #@5c0
    move-object/from16 v0, p0

    #@5c2
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@5c4
    move/from16 v54, v0

    #@5c6
    invoke-static/range {v54 .. v54}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5c9
    move-result-object v54

    #@5ca
    aput-object v54, v52, v53

    #@5cc
    invoke-static/range {v51 .. v52}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@5cf
    .line 680
    :cond_5cf
    move-object/from16 v0, p0

    #@5d1
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@5d3
    move-object/from16 v51, v0

    #@5d5
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@5d8
    move-result-object v51

    #@5d9
    const-string v52, "support_assisted_dialing"

    #@5db
    invoke-static/range {v51 .. v52}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@5de
    move-result v51

    #@5df
    if-nez v51, :cond_5f3

    #@5e1
    move-object/from16 v0, p0

    #@5e3
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@5e5
    move-object/from16 v51, v0

    #@5e7
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@5ea
    move-result-object v51

    #@5eb
    const-string v52, "support_smart_dialing"

    #@5ed
    invoke-static/range {v51 .. v52}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@5f0
    move-result v51

    #@5f1
    if-eqz v51, :cond_614

    #@5f3
    .line 684
    :cond_5f3
    move-object/from16 v0, p0

    #@5f5
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@5f7
    move-object/from16 v51, v0

    #@5f9
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@5fc
    move-result-object v51

    #@5fd
    invoke-virtual/range {v51 .. v51}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@600
    move-result-object v51

    #@601
    const-string v52, "assist_dial_ota_sid"

    #@603
    move-object/from16 v0, p0

    #@605
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@607
    move-object/from16 v53, v0

    #@609
    invoke-virtual/range {v53 .. v53}, Landroid/telephony/ServiceState;->getSystemId()I

    #@60c
    move-result v53

    #@60d
    invoke-static/range {v53 .. v53}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@610
    move-result-object v53

    #@611
    invoke-static/range {v51 .. v53}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@614
    .line 691
    :cond_614
    move-object/from16 v0, p0

    #@616
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@618
    move-object/from16 v50, v0

    #@61a
    .line 692
    .local v50, tss:Landroid/telephony/ServiceState;
    move-object/from16 v0, p0

    #@61c
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@61e
    move-object/from16 v51, v0

    #@620
    move-object/from16 v0, v51

    #@622
    move-object/from16 v1, p0

    #@624
    iput-object v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@626
    .line 693
    move-object/from16 v0, v50

    #@628
    move-object/from16 v1, p0

    #@62a
    iput-object v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@62c
    .line 695
    move-object/from16 v0, p0

    #@62e
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@630
    move-object/from16 v51, v0

    #@632
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@635
    .line 696
    move-object/from16 v0, p0

    #@637
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLteSS:Landroid/telephony/ServiceState;

    #@639
    move-object/from16 v51, v0

    #@63b
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@63e
    .line 699
    if-eqz v31, :cond_686

    #@640
    move-object/from16 v0, p0

    #@642
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@644
    move-object/from16 v51, v0

    #@646
    move-object/from16 v0, v51

    #@648
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@64a
    move-object/from16 v51, v0

    #@64c
    move-object/from16 v0, v51

    #@64e
    instance-of v0, v0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;

    #@650
    move/from16 v51, v0

    #@652
    if-eqz v51, :cond_686

    #@654
    .line 701
    const-string v51, "GsmDataConnectionTracker Created"

    #@656
    move-object/from16 v0, p0

    #@658
    move-object/from16 v1, v51

    #@65a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@65d
    .line 702
    move-object/from16 v0, p0

    #@65f
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@661
    move-object/from16 v51, v0

    #@663
    move-object/from16 v0, v51

    #@665
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@667
    move-object/from16 v51, v0

    #@669
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/DataConnectionTracker;->dispose()V

    #@66c
    .line 703
    move-object/from16 v0, p0

    #@66e
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@670
    move-object/from16 v51, v0

    #@672
    move-object/from16 v0, p0

    #@674
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@676
    move-object/from16 v52, v0

    #@678
    move-object/from16 v0, p0

    #@67a
    move-object/from16 v1, v52

    #@67c
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->getNewGsmDataConnectionTracker(Lcom/android/internal/telephony/PhoneBase;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@67f
    move-result-object v52

    #@680
    move-object/from16 v0, v52

    #@682
    move-object/from16 v1, v51

    #@684
    iput-object v0, v1, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@686
    .line 706
    :cond_686
    if-eqz v30, :cond_6ce

    #@688
    move-object/from16 v0, p0

    #@68a
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@68c
    move-object/from16 v51, v0

    #@68e
    move-object/from16 v0, v51

    #@690
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@692
    move-object/from16 v51, v0

    #@694
    move-object/from16 v0, v51

    #@696
    instance-of v0, v0, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    #@698
    move/from16 v51, v0

    #@69a
    if-eqz v51, :cond_6ce

    #@69c
    .line 708
    const-string v51, "GsmDataConnectionTracker disposed"

    #@69e
    move-object/from16 v0, p0

    #@6a0
    move-object/from16 v1, v51

    #@6a2
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@6a5
    .line 709
    move-object/from16 v0, p0

    #@6a7
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@6a9
    move-object/from16 v51, v0

    #@6ab
    move-object/from16 v0, v51

    #@6ad
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@6af
    move-object/from16 v51, v0

    #@6b1
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/DataConnectionTracker;->dispose()V

    #@6b4
    .line 710
    move-object/from16 v0, p0

    #@6b6
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@6b8
    move-object/from16 v51, v0

    #@6ba
    move-object/from16 v0, p0

    #@6bc
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@6be
    move-object/from16 v52, v0

    #@6c0
    move-object/from16 v0, p0

    #@6c2
    move-object/from16 v1, v52

    #@6c4
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->getNewCdmaDataConnectionTracker(Lcom/android/internal/telephony/cdma/CDMAPhone;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@6c7
    move-result-object v52

    #@6c8
    move-object/from16 v0, v52

    #@6ca
    move-object/from16 v1, v51

    #@6cc
    iput-object v0, v1, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@6ce
    .line 713
    :cond_6ce
    move-object/from16 v0, p0

    #@6d0
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@6d2
    move-object/from16 v47, v0

    #@6d4
    .line 714
    .local v47, tcl:Landroid/telephony/cdma/CdmaCellLocation;
    move-object/from16 v0, p0

    #@6d6
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@6d8
    move-object/from16 v51, v0

    #@6da
    move-object/from16 v0, v51

    #@6dc
    move-object/from16 v1, p0

    #@6de
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@6e0
    .line 715
    move-object/from16 v0, v47

    #@6e2
    move-object/from16 v1, p0

    #@6e4
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@6e6
    .line 717
    move-object/from16 v0, p0

    #@6e8
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@6ea
    move/from16 v51, v0

    #@6ec
    move/from16 v0, v51

    #@6ee
    move-object/from16 v1, p0

    #@6f0
    iput v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@6f2
    .line 718
    move-object/from16 v0, p0

    #@6f4
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@6f6
    move/from16 v51, v0

    #@6f8
    move/from16 v0, v51

    #@6fa
    move-object/from16 v1, p0

    #@6fc
    iput v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@6fe
    .line 719
    const/16 v51, 0x0

    #@700
    move/from16 v0, v51

    #@702
    move-object/from16 v1, p0

    #@704
    iput v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@706
    .line 721
    move-object/from16 v0, p0

    #@708
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@70a
    move-object/from16 v51, v0

    #@70c
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@70f
    .line 725
    move-object/from16 v0, p0

    #@711
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@713
    move-object/from16 v51, v0

    #@715
    invoke-interface/range {v51 .. v51}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@718
    move-result-object v51

    #@719
    move-object/from16 v0, v51

    #@71b
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@71d
    move/from16 v51, v0

    #@71f
    if-eqz v51, :cond_74d

    #@721
    .line 726
    move-object/from16 v0, p0

    #@723
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@725
    move-object/from16 v51, v0

    #@727
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@72a
    move-result v51

    #@72b
    const/16 v52, 0xe

    #@72d
    move/from16 v0, v51

    #@72f
    move/from16 v1, v52

    #@731
    if-ne v0, v1, :cond_feb

    #@733
    .line 727
    const-string v51, "CDMA"

    #@735
    const-string v52, "[IMS_AFW] Radio Tech is LTE, Get LTE Info from modem"

    #@737
    invoke-static/range {v51 .. v52}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@73a
    .line 728
    move-object/from16 v0, p0

    #@73c
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@73e
    move-object/from16 v51, v0

    #@740
    const/16 v52, 0x64

    #@742
    move-object/from16 v0, p0

    #@744
    move/from16 v1, v52

    #@746
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@749
    move-result-object v52

    #@74a
    invoke-interface/range {v51 .. v52}, Lcom/android/internal/telephony/CommandsInterface;->getLteInfoForIms(Landroid/os/Message;)V

    #@74d
    .line 738
    :cond_74d
    :goto_74d
    if-eqz v32, :cond_88d

    #@74f
    .line 740
    move-object/from16 v0, p0

    #@751
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@753
    move-object/from16 v51, v0

    #@755
    move-object/from16 v0, v51

    #@757
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@759
    move-object/from16 v51, v0

    #@75b
    invoke-interface/range {v51 .. v51}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@75e
    move-result-object v51

    #@75f
    move-object/from16 v0, v51

    #@761
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_RIL_CONN_HISTORY:Z

    #@763
    move/from16 v51, v0

    #@765
    if-eqz v51, :cond_780

    #@767
    .line 742
    move-object/from16 v0, p0

    #@769
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@76b
    move-object/from16 v51, v0

    #@76d
    move-object/from16 v0, v51

    #@76f
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@771
    move-object/from16 v51, v0

    #@773
    invoke-interface/range {v51 .. v51}, Lcom/android/internal/telephony/CommandsInterface;->getMyDebugger()Lcom/android/internal/telephony/MMdebuger;

    #@776
    move-result-object v51

    #@777
    move-object/from16 v0, p0

    #@779
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@77b
    move/from16 v52, v0

    #@77d
    invoke-virtual/range {v51 .. v52}, Lcom/android/internal/telephony/MMdebuger;->savecurrenttech(I)V

    #@780
    .line 747
    :cond_780
    move-object/from16 v0, p0

    #@782
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@784
    move-object/from16 v51, v0

    #@786
    move-object/from16 v0, v51

    #@788
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@78a
    move-object/from16 v51, v0

    #@78c
    invoke-interface/range {v51 .. v51}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@78f
    move-result-object v51

    #@790
    move-object/from16 v0, v51

    #@792
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_GSM_GLOBAL_PREFERED_APN_SPR:Z

    #@794
    move/from16 v51, v0

    #@796
    if-eqz v51, :cond_829

    #@798
    .line 749
    new-instance v51, Ljava/lang/StringBuilder;

    #@79a
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@79d
    const-string v52, "[bycho] TETHERING_PDN_SET(Hidden) "

    #@79f
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a2
    move-result-object v51

    #@7a3
    move-object/from16 v0, p0

    #@7a5
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@7a7
    move-object/from16 v52, v0

    #@7a9
    invoke-virtual/range {v52 .. v52}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@7ac
    move-result-object v52

    #@7ad
    invoke-virtual/range {v52 .. v52}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7b0
    move-result-object v52

    #@7b1
    const-string v53, "tethering_pdn"

    #@7b3
    const/16 v54, 0x1

    #@7b5
    invoke-static/range {v52 .. v54}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@7b8
    move-result v52

    #@7b9
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7bc
    move-result-object v51

    #@7bd
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c0
    move-result-object v51

    #@7c1
    move-object/from16 v0, p0

    #@7c3
    move-object/from16 v1, v51

    #@7c5
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@7c8
    .line 751
    move-object/from16 v0, p0

    #@7ca
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@7cc
    move/from16 v51, v0

    #@7ce
    const/16 v52, 0xe

    #@7d0
    move/from16 v0, v51

    #@7d2
    move/from16 v1, v52

    #@7d4
    if-eq v0, v1, :cond_1019

    #@7d6
    move-object/from16 v0, p0

    #@7d8
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@7da
    move/from16 v51, v0

    #@7dc
    const/16 v52, 0xd

    #@7de
    move/from16 v0, v51

    #@7e0
    move/from16 v1, v52

    #@7e2
    if-eq v0, v1, :cond_1019

    #@7e4
    .line 753
    move-object/from16 v0, p0

    #@7e6
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@7e8
    move-object/from16 v51, v0

    #@7ea
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@7ed
    move-result-object v51

    #@7ee
    invoke-virtual/range {v51 .. v51}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7f1
    move-result-object v51

    #@7f2
    const-string v52, "tether_dun_required"

    #@7f4
    const/16 v53, 0x0

    #@7f6
    invoke-static/range {v51 .. v53}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@7f9
    .line 754
    new-instance v51, Ljava/lang/StringBuilder;

    #@7fb
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@7fe
    const-string v52, "[bycho] User selected Internet PDN for Tethering on GSM NetWork  TETHER_DUN_REQUIRED "

    #@800
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@803
    move-result-object v51

    #@804
    move-object/from16 v0, p0

    #@806
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@808
    move-object/from16 v52, v0

    #@80a
    invoke-virtual/range {v52 .. v52}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@80d
    move-result-object v52

    #@80e
    invoke-virtual/range {v52 .. v52}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@811
    move-result-object v52

    #@812
    const-string v53, "tether_dun_required"

    #@814
    const/16 v54, 0x0

    #@816
    invoke-static/range {v52 .. v54}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@819
    move-result v52

    #@81a
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81d
    move-result-object v51

    #@81e
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@821
    move-result-object v51

    #@822
    move-object/from16 v0, p0

    #@824
    move-object/from16 v1, v51

    #@826
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@829
    .line 773
    :cond_829
    :goto_829
    move-object/from16 v0, p0

    #@82b
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@82d
    move-object/from16 v51, v0

    #@82f
    const-string v52, "gsm.network.type"

    #@831
    move-object/from16 v0, p0

    #@833
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@835
    move/from16 v53, v0

    #@837
    invoke-static/range {v53 .. v53}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@83a
    move-result-object v53

    #@83b
    invoke-virtual/range {v51 .. v53}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@83e
    .line 776
    move-object/from16 v0, p0

    #@840
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@842
    move-object/from16 v51, v0

    #@844
    invoke-interface/range {v51 .. v51}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@847
    move-result-object v51

    #@848
    move-object/from16 v0, v51

    #@84a
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_SPR_MTU_CHANGE:Z

    #@84c
    move/from16 v51, v0

    #@84e
    if-eqz v51, :cond_88d

    #@850
    move-object/from16 v0, p0

    #@852
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@854
    move/from16 v51, v0

    #@856
    if-eqz v51, :cond_88d

    #@858
    .line 777
    move-object/from16 v0, p0

    #@85a
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@85c
    move-object/from16 v51, v0

    #@85e
    const-string v52, "net.rmnet.activenetwork"

    #@860
    move-object/from16 v0, p0

    #@862
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@864
    move/from16 v53, v0

    #@866
    invoke-static/range {v53 .. v53}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@869
    move-result-object v53

    #@86a
    invoke-virtual/range {v51 .. v53}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@86d
    .line 780
    new-instance v51, Ljava/lang/StringBuilder;

    #@86f
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@872
    const-string v52, "[LG DATA] we set activenetwork : "

    #@874
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@877
    move-result-object v51

    #@878
    move-object/from16 v0, p0

    #@87a
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@87c
    move/from16 v52, v0

    #@87e
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@881
    move-result-object v51

    #@882
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@885
    move-result-object v51

    #@886
    move-object/from16 v0, p0

    #@888
    move-object/from16 v1, v51

    #@88a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@88d
    .line 786
    :cond_88d
    if-eqz v33, :cond_898

    #@88f
    .line 787
    move-object/from16 v0, p0

    #@891
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNetworkAttachedRegistrants:Landroid/os/RegistrantList;

    #@893
    move-object/from16 v51, v0

    #@895
    invoke-virtual/range {v51 .. v51}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@898
    .line 790
    :cond_898
    if-eqz v25, :cond_bba

    #@89a
    .line 791
    move-object/from16 v0, p0

    #@89c
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@89e
    move-object/from16 v51, v0

    #@8a0
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->isEriFileLoaded()Z

    #@8a3
    move-result v51

    #@8a4
    if-eqz v51, :cond_a8a

    #@8a6
    .line 795
    move-object/from16 v0, p0

    #@8a8
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@8aa
    move-object/from16 v51, v0

    #@8ac
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@8af
    move-result v51

    #@8b0
    if-eqz v51, :cond_8ba

    #@8b2
    move-object/from16 v0, p0

    #@8b4
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@8b6
    move/from16 v51, v0

    #@8b8
    if-nez v51, :cond_1095

    #@8ba
    .line 797
    :cond_8ba
    move-object/from16 v0, p0

    #@8bc
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@8be
    move-object/from16 v51, v0

    #@8c0
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getCdmaEriText()Ljava/lang/String;

    #@8c3
    move-result-object v18

    #@8c4
    .line 811
    .local v18, eriText:Ljava/lang/String;
    :cond_8c4
    :goto_8c4
    move-object/from16 v0, p0

    #@8c6
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@8c8
    move-object/from16 v51, v0

    #@8ca
    move-object/from16 v0, v51

    #@8cc
    move-object/from16 v1, v18

    #@8ce
    invoke-virtual {v0, v1}, Landroid/telephony/ServiceState;->setOperatorAlphaLong(Ljava/lang/String;)V

    #@8d1
    .line 813
    const/16 v51, 0x0

    #@8d3
    const-string v52, "vzw_eri"

    #@8d5
    invoke-static/range {v51 .. v52}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8d8
    move-result v51

    #@8d9
    if-eqz v51, :cond_a8a

    #@8db
    .line 814
    const/4 v5, -0x1

    #@8dc
    .line 815
    .local v5, alertId:I
    move-object/from16 v0, p0

    #@8de
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@8e0
    move-object/from16 v51, v0

    #@8e2
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@8e5
    move-result v51

    #@8e6
    if-nez v51, :cond_10e1

    #@8e8
    .line 816
    move-object/from16 v0, p0

    #@8ea
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@8ec
    move-object/from16 v51, v0

    #@8ee
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getAlertId()I

    #@8f1
    move-result v5

    #@8f2
    .line 820
    :cond_8f2
    :goto_8f2
    move-object/from16 v0, p0

    #@8f4
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@8f6
    move-object/from16 v51, v0

    #@8f8
    move-object/from16 v0, v51

    #@8fa
    move-object/from16 v1, v18

    #@8fc
    invoke-virtual {v0, v1}, Landroid/telephony/ServiceState;->setOperatorAlphaLong(Ljava/lang/String;)V

    #@8ff
    .line 821
    sget-boolean v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->DBG_CALL:Z

    #@901
    if-eqz v51, :cond_91f

    #@903
    const-string v51, "CDMA"

    #@905
    new-instance v52, Ljava/lang/StringBuilder;

    #@907
    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    #@90a
    const-string v53, "pollStateDone: eriText = "

    #@90c
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90f
    move-result-object v52

    #@910
    move-object/from16 v0, v52

    #@912
    move-object/from16 v1, v18

    #@914
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@917
    move-result-object v52

    #@918
    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91b
    move-result-object v52

    #@91c
    invoke-static/range {v51 .. v52}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@91f
    .line 822
    :cond_91f
    sget-boolean v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->DBG_CALL:Z

    #@921
    if-eqz v51, :cond_959

    #@923
    const-string v51, "CDMA"

    #@925
    new-instance v52, Ljava/lang/StringBuilder;

    #@927
    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    #@92a
    const-string v53, "1. newSS.getNetworkId() = "

    #@92c
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92f
    move-result-object v52

    #@930
    move-object/from16 v0, p0

    #@932
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@934
    move-object/from16 v53, v0

    #@936
    invoke-virtual/range {v53 .. v53}, Landroid/telephony/ServiceState;->getNetworkId()I

    #@939
    move-result v53

    #@93a
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93d
    move-result-object v52

    #@93e
    const-string v53, ", ss.getNetworkId() = "

    #@940
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@943
    move-result-object v52

    #@944
    move-object/from16 v0, p0

    #@946
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@948
    move-object/from16 v53, v0

    #@94a
    invoke-virtual/range {v53 .. v53}, Landroid/telephony/ServiceState;->getNetworkId()I

    #@94d
    move-result v53

    #@94e
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@951
    move-result-object v52

    #@952
    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@955
    move-result-object v52

    #@956
    invoke-static/range {v51 .. v52}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@959
    .line 823
    :cond_959
    sget-boolean v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->DBG_CALL:Z

    #@95b
    if-eqz v51, :cond_993

    #@95d
    const-string v51, "CDMA"

    #@95f
    new-instance v52, Ljava/lang/StringBuilder;

    #@961
    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    #@964
    const-string v53, "2. newSS.getSystemId() = "

    #@966
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@969
    move-result-object v52

    #@96a
    move-object/from16 v0, p0

    #@96c
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@96e
    move-object/from16 v53, v0

    #@970
    invoke-virtual/range {v53 .. v53}, Landroid/telephony/ServiceState;->getSystemId()I

    #@973
    move-result v53

    #@974
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@977
    move-result-object v52

    #@978
    const-string v53, ", ss.getSystemId() = "

    #@97a
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97d
    move-result-object v52

    #@97e
    move-object/from16 v0, p0

    #@980
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@982
    move-object/from16 v53, v0

    #@984
    invoke-virtual/range {v53 .. v53}, Landroid/telephony/ServiceState;->getSystemId()I

    #@987
    move-result v53

    #@988
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@98b
    move-result-object v52

    #@98c
    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98f
    move-result-object v52

    #@990
    invoke-static/range {v51 .. v52}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@993
    .line 824
    :cond_993
    sget-boolean v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->DBG_CALL:Z

    #@995
    if-eqz v51, :cond_9b9

    #@997
    const-string v51, "CDMA"

    #@999
    new-instance v52, Ljava/lang/StringBuilder;

    #@99b
    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    #@99e
    const-string v53, "3. ss.getState() = "

    #@9a0
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a3
    move-result-object v52

    #@9a4
    move-object/from16 v0, p0

    #@9a6
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@9a8
    move-object/from16 v53, v0

    #@9aa
    invoke-virtual/range {v53 .. v53}, Landroid/telephony/ServiceState;->getState()I

    #@9ad
    move-result v53

    #@9ae
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9b1
    move-result-object v52

    #@9b2
    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b5
    move-result-object v52

    #@9b6
    invoke-static/range {v51 .. v52}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9b9
    .line 825
    :cond_9b9
    sget-boolean v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->DBG_CALL:Z

    #@9bb
    if-eqz v51, :cond_9db

    #@9bd
    const-string v51, "CDMA"

    #@9bf
    new-instance v52, Ljava/lang/StringBuilder;

    #@9c1
    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    #@9c4
    const-string v53, "4. hasStateChanged = "

    #@9c6
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c9
    move-result-object v52

    #@9ca
    move-object/from16 v0, p0

    #@9cc
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@9ce
    move/from16 v53, v0

    #@9d0
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9d3
    move-result-object v52

    #@9d4
    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d7
    move-result-object v52

    #@9d8
    invoke-static/range {v51 .. v52}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9db
    .line 826
    :cond_9db
    sget-boolean v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->DBG_CALL:Z

    #@9dd
    if-eqz v51, :cond_9fd

    #@9df
    const-string v51, "CDMA"

    #@9e1
    new-instance v52, Ljava/lang/StringBuilder;

    #@9e3
    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    #@9e6
    const-string v53, "5. isInShutDown = "

    #@9e8
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9eb
    move-result-object v52

    #@9ec
    move-object/from16 v0, p0

    #@9ee
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isInShutDown:Z

    #@9f0
    move/from16 v53, v0

    #@9f2
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9f5
    move-result-object v52

    #@9f6
    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f9
    move-result-object v52

    #@9fa
    invoke-static/range {v51 .. v52}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9fd
    .line 827
    :cond_9fd
    move-object/from16 v0, p0

    #@9ff
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@a01
    move/from16 v51, v0

    #@a03
    if-nez v51, :cond_a0d

    #@a05
    move-object/from16 v0, p0

    #@a07
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedRoamingIndicator:Z

    #@a09
    move/from16 v51, v0

    #@a0b
    if-eqz v51, :cond_a11

    #@a0d
    :cond_a0d
    const/16 v51, -0x1

    #@a0f
    sput v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->prev_alertId:I

    #@a11
    .line 829
    :cond_a11
    move-object/from16 v0, p0

    #@a13
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@a15
    move-object/from16 v51, v0

    #@a17
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@a1a
    move-result v51

    #@a1b
    const/16 v52, 0x1

    #@a1d
    move/from16 v0, v51

    #@a1f
    move/from16 v1, v52

    #@a21
    if-ne v0, v1, :cond_10f7

    #@a23
    move-object/from16 v0, p0

    #@a25
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@a27
    move/from16 v51, v0

    #@a29
    if-eqz v51, :cond_10f7

    #@a2b
    move-object/from16 v0, p0

    #@a2d
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isInShutDown:Z

    #@a2f
    move/from16 v51, v0

    #@a31
    if-nez v51, :cond_10f7

    #@a33
    .line 830
    sget-boolean v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->DBG_CALL:Z

    #@a35
    if-eqz v51, :cond_a65

    #@a37
    const-string v51, "CDMA"

    #@a39
    new-instance v52, Ljava/lang/StringBuilder;

    #@a3b
    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    #@a3e
    const-string v53, "isInShutDown : "

    #@a40
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a43
    move-result-object v52

    #@a44
    move-object/from16 v0, p0

    #@a46
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isInShutDown:Z

    #@a48
    move/from16 v53, v0

    #@a4a
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a4d
    move-result-object v52

    #@a4e
    const-string v53, ", hasStateChanged : "

    #@a50
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a53
    move-result-object v52

    #@a54
    move-object/from16 v0, p0

    #@a56
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@a58
    move/from16 v53, v0

    #@a5a
    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a5d
    move-result-object v52

    #@a5e
    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a61
    move-result-object v52

    #@a62
    invoke-static/range {v51 .. v52}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a65
    .line 831
    :cond_a65
    const/16 v51, 0x3e8

    #@a67
    move-object/from16 v0, p0

    #@a69
    move/from16 v1, v51

    #@a6b
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->PlayVZWERISound(I)V

    #@a6e
    .line 840
    :cond_a6e
    :goto_a6e
    new-instance v19, Landroid/content/Intent;

    #@a70
    const-string v51, "com.android.erichanged"

    #@a72
    move-object/from16 v0, v19

    #@a74
    move-object/from16 v1, v51

    #@a76
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a79
    .line 841
    .local v19, erichanged:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@a7b
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@a7d
    move-object/from16 v51, v0

    #@a7f
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@a82
    move-result-object v51

    #@a83
    move-object/from16 v0, v51

    #@a85
    move-object/from16 v1, v19

    #@a87
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@a8a
    .line 846
    .end local v5           #alertId:I
    .end local v18           #eriText:Ljava/lang/String;
    .end local v19           #erichanged:Landroid/content/Intent;
    :cond_a8a
    move-object/from16 v0, p0

    #@a8c
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@a8e
    move-object/from16 v51, v0

    #@a90
    if-eqz v51, :cond_abd

    #@a92
    move-object/from16 v0, p0

    #@a94
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@a96
    move-object/from16 v51, v0

    #@a98
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@a9b
    move-result-object v51

    #@a9c
    sget-object v52, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@a9e
    move-object/from16 v0, v51

    #@aa0
    move-object/from16 v1, v52

    #@aa2
    if-ne v0, v1, :cond_abd

    #@aa4
    move-object/from16 v0, p0

    #@aa6
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@aa8
    move-object/from16 v51, v0

    #@aaa
    if-eqz v51, :cond_abd

    #@aac
    .line 863
    const/16 v51, 0x0

    #@aae
    const-string v52, "vzw_eri"

    #@ab0
    invoke-static/range {v51 .. v52}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@ab3
    move-result v51

    #@ab4
    if-eqz v51, :cond_1122

    #@ab6
    .line 864
    const-string v51, "CDMA"

    #@ab8
    const-string v52, "do not set the operator name from CSIM record..just set to the ERI text for VZW"

    #@aba
    invoke-static/range {v51 .. v52}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@abd
    .line 881
    :cond_abd
    :goto_abd
    move-object/from16 v0, p0

    #@abf
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@ac1
    move-object/from16 v51, v0

    #@ac3
    const-string v52, "gsm.operator.alpha"

    #@ac5
    move-object/from16 v0, p0

    #@ac7
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@ac9
    move-object/from16 v53, v0

    #@acb
    invoke-virtual/range {v53 .. v53}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@ace
    move-result-object v53

    #@acf
    invoke-virtual/range {v51 .. v53}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@ad2
    .line 884
    const-string v51, "gsm.operator.numeric"

    #@ad4
    const-string v52, ""

    #@ad6
    invoke-static/range {v51 .. v52}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ad9
    move-result-object v43

    #@ada
    .line 886
    .local v43, prevOperatorNumeric:Ljava/lang/String;
    move-object/from16 v0, p0

    #@adc
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@ade
    move-object/from16 v51, v0

    #@ae0
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@ae3
    move-result-object v42

    #@ae4
    .line 887
    .local v42, operatorNumeric:Ljava/lang/String;
    move-object/from16 v0, p0

    #@ae6
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@ae8
    move-object/from16 v51, v0

    #@aea
    const-string v52, "gsm.operator.numeric"

    #@aec
    move-object/from16 v0, v51

    #@aee
    move-object/from16 v1, v52

    #@af0
    move-object/from16 v2, v42

    #@af2
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@af5
    .line 889
    if-nez v42, :cond_117f

    #@af7
    .line 890
    const-string v51, "operatorNumeric is null"

    #@af9
    move-object/from16 v0, p0

    #@afb
    move-object/from16 v1, v51

    #@afd
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@b00
    .line 891
    move-object/from16 v0, p0

    #@b02
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@b04
    move-object/from16 v51, v0

    #@b06
    const-string v52, "gsm.operator.iso-country"

    #@b08
    const-string v53, ""

    #@b0a
    invoke-virtual/range {v51 .. v53}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@b0d
    .line 892
    const/16 v51, 0x0

    #@b0f
    move/from16 v0, v51

    #@b11
    move-object/from16 v1, p0

    #@b13
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@b15
    .line 917
    :cond_b15
    :goto_b15
    move-object/from16 v0, p0

    #@b17
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@b19
    move-object/from16 v51, v0

    #@b1b
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@b1e
    move-result-object v51

    #@b1f
    const-string v52, "support_assisted_dialing"

    #@b21
    invoke-static/range {v51 .. v52}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@b24
    move-result v51

    #@b25
    if-nez v51, :cond_b39

    #@b27
    move-object/from16 v0, p0

    #@b29
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@b2b
    move-object/from16 v51, v0

    #@b2d
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@b30
    move-result-object v51

    #@b31
    const-string v52, "support_smart_dialing"

    #@b33
    invoke-static/range {v51 .. v52}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@b36
    move-result v51

    #@b37
    if-eqz v51, :cond_b89

    #@b39
    .line 921
    :cond_b39
    invoke-static/range {v42 .. v42}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b3c
    move-result v51

    #@b3d
    if-nez v51, :cond_b89

    #@b3f
    .line 924
    const/16 v51, 0x0

    #@b41
    const/16 v52, 0x3

    #@b43
    :try_start_b43
    move-object/from16 v0, v42

    #@b45
    move/from16 v1, v51

    #@b47
    move/from16 v2, v52

    #@b49
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b4c
    move-result-object v7

    #@b4d
    .line 925
    .local v7, assistedDialingMcc:Ljava/lang/String;
    const-string v51, " ***** put System.ASSIST_DIAL_OTA_MCC "

    #@b4f
    move-object/from16 v0, p0

    #@b51
    move-object/from16 v1, v51

    #@b53
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@b56
    .line 926
    move-object/from16 v0, p0

    #@b58
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@b5a
    move-object/from16 v51, v0

    #@b5c
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@b5f
    move-result-object v51

    #@b60
    invoke-virtual/range {v51 .. v51}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b63
    move-result-object v51

    #@b64
    const-string v52, "assist_dial_ota_mcc"

    #@b66
    move-object/from16 v0, v51

    #@b68
    move-object/from16 v1, v52

    #@b6a
    invoke-static {v0, v1, v7}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@b6d
    .line 928
    new-instance v51, Ljava/lang/StringBuilder;

    #@b6f
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@b72
    const-string v52, " ***** MCC  "

    #@b74
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b77
    move-result-object v51

    #@b78
    move-object/from16 v0, v51

    #@b7a
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7d
    move-result-object v51

    #@b7e
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b81
    move-result-object v51

    #@b82
    move-object/from16 v0, p0

    #@b84
    move-object/from16 v1, v51

    #@b86
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V
    :try_end_b89
    .catch Ljava/lang/NumberFormatException; {:try_start_b43 .. :try_end_b89} :catch_1223

    #@b89
    .line 936
    .end local v7           #assistedDialingMcc:Ljava/lang/String;
    :cond_b89
    :goto_b89
    move-object/from16 v0, p0

    #@b8b
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@b8d
    move-object/from16 v52, v0

    #@b8f
    const-string v53, "gsm.operator.isroaming"

    #@b91
    move-object/from16 v0, p0

    #@b93
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@b95
    move-object/from16 v51, v0

    #@b97
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@b9a
    move-result v51

    #@b9b
    if-eqz v51, :cond_122f

    #@b9d
    const-string v51, "true"

    #@b9f
    :goto_b9f
    move-object/from16 v0, v52

    #@ba1
    move-object/from16 v1, v53

    #@ba3
    move-object/from16 v2, v51

    #@ba5
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@ba8
    .line 939
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->updateSpnDisplay()V

    #@bab
    .line 940
    move-object/from16 v0, p0

    #@bad
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@baf
    move-object/from16 v51, v0

    #@bb1
    move-object/from16 v0, p0

    #@bb3
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@bb5
    move-object/from16 v52, v0

    #@bb7
    invoke-virtual/range {v51 .. v52}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyServiceStateChanged(Landroid/telephony/ServiceState;)V

    #@bba
    .line 943
    .end local v42           #operatorNumeric:Ljava/lang/String;
    .end local v43           #prevOperatorNumeric:Ljava/lang/String;
    :cond_bba
    if-nez v22, :cond_bbe

    #@bbc
    if-eqz v21, :cond_1233

    #@bbe
    .line 944
    :cond_bbe
    move-object/from16 v0, p0

    #@bc0
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mAttachedRegistrants:Landroid/os/RegistrantList;

    #@bc2
    move-object/from16 v51, v0

    #@bc4
    invoke-virtual/range {v51 .. v51}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@bc7
    .line 956
    :cond_bc7
    :goto_bc7
    if-eqz v24, :cond_bd2

    #@bc9
    .line 957
    move-object/from16 v0, p0

    #@bcb
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mDetachedRegistrants:Landroid/os/RegistrantList;

    #@bcd
    move-object/from16 v51, v0

    #@bcf
    invoke-virtual/range {v51 .. v51}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@bd2
    .line 960
    :cond_bd2
    if-nez v23, :cond_bd6

    #@bd4
    if-eqz v32, :cond_be1

    #@bd6
    .line 961
    :cond_bd6
    move-object/from16 v0, p0

    #@bd8
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@bda
    move-object/from16 v51, v0

    #@bdc
    const/16 v52, 0x0

    #@bde
    invoke-virtual/range {v51 .. v52}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyDataConnection(Ljava/lang/String;)V

    #@be1
    .line 965
    :cond_be1
    const/16 v51, 0x0

    #@be3
    const-string v52, "lgu_global_roaming"

    #@be5
    invoke-static/range {v51 .. v52}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@be8
    move-result v51

    #@be9
    if-eqz v51, :cond_c3b

    #@beb
    if-eqz v22, :cond_c3b

    #@bed
    move-object/from16 v0, p0

    #@bef
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@bf1
    move/from16 v51, v0

    #@bf3
    const/16 v52, 0xe

    #@bf5
    move/from16 v0, v51

    #@bf7
    move/from16 v1, v52

    #@bf9
    if-ne v0, v1, :cond_c3b

    #@bfb
    .line 968
    const-string v51, "gsm.lge.lte_reject_cause"

    #@bfd
    const-string v52, "0"

    #@bff
    invoke-static/range {v51 .. v52}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@c02
    .line 969
    const-string v51, "LTE data in-service : set no U+ LTE reject cause "

    #@c04
    move-object/from16 v0, p0

    #@c06
    move-object/from16 v1, v51

    #@c08
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@c0b
    .line 971
    new-instance v37, Landroid/content/Intent;

    #@c0d
    const-string v51, "android.intent.action.LTE_EMM_REJECT"

    #@c0f
    move-object/from16 v0, v37

    #@c11
    move-object/from16 v1, v51

    #@c13
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c16
    .line 972
    .local v37, intent:Landroid/content/Intent;
    const-string v51, "rejectCode"

    #@c18
    const/16 v52, 0x0

    #@c1a
    move-object/from16 v0, v37

    #@c1c
    move-object/from16 v1, v51

    #@c1e
    move/from16 v2, v52

    #@c20
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@c23
    .line 973
    move-object/from16 v0, p0

    #@c25
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@c27
    move-object/from16 v51, v0

    #@c29
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@c2c
    move-result-object v51

    #@c2d
    move-object/from16 v0, v51

    #@c2f
    move-object/from16 v1, v37

    #@c31
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@c34
    .line 974
    const-string v51, "persist.radio.last_ltereject"

    #@c36
    const-string v52, "0"

    #@c38
    invoke-static/range {v51 .. v52}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@c3b
    .line 980
    .end local v37           #intent:Landroid/content/Intent;
    :cond_c3b
    if-eqz v35, :cond_cb9

    #@c3d
    .line 981
    move-object/from16 v0, p0

    #@c3f
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOnRegistrants:Landroid/os/RegistrantList;

    #@c41
    move-object/from16 v51, v0

    #@c43
    invoke-virtual/range {v51 .. v51}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@c46
    .line 983
    move-object/from16 v0, p0

    #@c48
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@c4a
    move-object/from16 v51, v0

    #@c4c
    invoke-interface/range {v51 .. v51}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@c4f
    move-result-object v51

    #@c50
    move-object/from16 v0, v51

    #@c52
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SEND_DATA_ROAM_POPUP_INTENT_VZW:Z

    #@c54
    move/from16 v51, v0

    #@c56
    if-eqz v51, :cond_cb9

    #@c58
    .line 985
    move-object/from16 v0, p0

    #@c5a
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@c5c
    move-object/from16 v51, v0

    #@c5e
    move-object/from16 v0, v51

    #@c60
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c62
    move-object/from16 v51, v0

    #@c64
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@c67
    move-result v51

    #@c68
    if-nez v51, :cond_cb9

    #@c6a
    move-object/from16 v0, p0

    #@c6c
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@c6e
    move-object/from16 v51, v0

    #@c70
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@c73
    move-result v51

    #@c74
    if-nez v51, :cond_cb9

    #@c76
    .line 987
    move-object/from16 v0, p0

    #@c78
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@c7a
    move-object/from16 v51, v0

    #@c7c
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getCdmaEriText()Ljava/lang/String;

    #@c7f
    move-result-object v51

    #@c80
    if-eqz v51, :cond_cb9

    #@c82
    move-object/from16 v0, p0

    #@c84
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@c86
    move-object/from16 v51, v0

    #@c88
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getCdmaEriText()Ljava/lang/String;

    #@c8b
    move-result-object v51

    #@c8c
    const-string v52, "Extended Network"

    #@c8e
    invoke-virtual/range {v51 .. v52}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c91
    move-result v51

    #@c92
    if-nez v51, :cond_cb9

    #@c94
    .line 988
    const-string v51, " ***** Send Roam intent  ACTION_DATA_ROAMING_MENU"

    #@c96
    move-object/from16 v0, p0

    #@c98
    move-object/from16 v1, v51

    #@c9a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@c9d
    .line 989
    new-instance v37, Landroid/content/Intent;

    #@c9f
    const-string v51, "com.lge.android.intent.action.ACTION_DATA_ROAMING_MENU"

    #@ca1
    move-object/from16 v0, v37

    #@ca3
    move-object/from16 v1, v51

    #@ca5
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@ca8
    .line 990
    .restart local v37       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@caa
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@cac
    move-object/from16 v51, v0

    #@cae
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@cb1
    move-result-object v51

    #@cb2
    move-object/from16 v0, v51

    #@cb4
    move-object/from16 v1, v37

    #@cb6
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@cb9
    .line 998
    .end local v37           #intent:Landroid/content/Intent;
    :cond_cb9
    if-eqz v34, :cond_cc4

    #@cbb
    .line 999
    move-object/from16 v0, p0

    #@cbd
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOffRegistrants:Landroid/os/RegistrantList;

    #@cbf
    move-object/from16 v51, v0

    #@cc1
    invoke-virtual/range {v51 .. v51}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@cc4
    .line 1002
    :cond_cc4
    if-eqz v29, :cond_ccf

    #@cc6
    .line 1003
    move-object/from16 v0, p0

    #@cc8
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@cca
    move-object/from16 v51, v0

    #@ccc
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyLocationChanged()V

    #@ccf
    .line 1007
    :cond_ccf
    move-object/from16 v0, p0

    #@cd1
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@cd3
    move-object/from16 v51, v0

    #@cd5
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@cd8
    move-result-object v51

    #@cd9
    const-string v52, "vzw_gfit"

    #@cdb
    invoke-static/range {v51 .. v52}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@cde
    move-result v51

    #@cdf
    if-eqz v51, :cond_cec

    #@ce1
    .line 1008
    if-eqz v26, :cond_cec

    #@ce3
    .line 1009
    move-object/from16 v0, p0

    #@ce5
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNoServiceChangedRegistrants:Landroid/os/RegistrantList;

    #@ce7
    move-object/from16 v51, v0

    #@ce9
    invoke-virtual/range {v51 .. v51}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@cec
    .line 1014
    :cond_cec
    new-instance v6, Ljava/util/ArrayList;

    #@cee
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@cf1
    .line 1015
    .local v6, arrayCi:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/CellInfo;>;"
    move-object/from16 v0, p0

    #@cf3
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@cf5
    move-object/from16 v52, v0

    #@cf7
    monitor-enter v52

    #@cf8
    .line 1016
    :try_start_cf8
    move-object/from16 v0, p0

    #@cfa
    iget-object v10, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@cfc
    check-cast v10, Landroid/telephony/CellInfoLte;

    #@cfe
    .line 1018
    .local v10, cil:Landroid/telephony/CellInfoLte;
    move-object/from16 v0, p0

    #@d00
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@d02
    move-object/from16 v51, v0

    #@d04
    move-object/from16 v0, p0

    #@d06
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLasteCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@d08
    move-object/from16 v53, v0

    #@d0a
    move-object/from16 v0, v51

    #@d0c
    move-object/from16 v1, v53

    #@d0e
    invoke-virtual {v0, v1}, Landroid/telephony/CellIdentityLte;->equals(Ljava/lang/Object;)Z

    #@d11
    move-result v51

    #@d12
    if-nez v51, :cond_1292

    #@d14
    const/4 v9, 0x1

    #@d15
    .line 1019
    .local v9, cidChanged:Z
    :goto_d15
    if-nez v33, :cond_d1b

    #@d17
    if-nez v26, :cond_d1b

    #@d19
    if-eqz v9, :cond_db4

    #@d1b
    .line 1021
    :cond_d1b
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@d1e
    move-result-wide v53

    #@d1f
    const-wide/16 v55, 0x3e8

    #@d21
    mul-long v48, v53, v55

    #@d23
    .line 1022
    .local v48, timeStamp:J
    move-object/from16 v0, p0

    #@d25
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@d27
    move-object/from16 v51, v0

    #@d29
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@d2c
    move-result v51

    #@d2d
    if-nez v51, :cond_1295

    #@d2f
    const/16 v44, 0x1

    #@d31
    .line 1023
    .local v44, registered:Z
    :goto_d31
    move-object/from16 v0, p0

    #@d33
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mNewCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@d35
    move-object/from16 v51, v0

    #@d37
    move-object/from16 v0, v51

    #@d39
    move-object/from16 v1, p0

    #@d3b
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLasteCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@d3d
    .line 1025
    move/from16 v0, v44

    #@d3f
    invoke-virtual {v10, v0}, Landroid/telephony/CellInfoLte;->setRegisterd(Z)V

    #@d42
    .line 1026
    move-object/from16 v0, p0

    #@d44
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mLasteCellIdentityLte:Landroid/telephony/CellIdentityLte;

    #@d46
    move-object/from16 v51, v0

    #@d48
    move-object/from16 v0, v51

    #@d4a
    invoke-virtual {v10, v0}, Landroid/telephony/CellInfoLte;->setCellIdentity(Landroid/telephony/CellIdentityLte;)V

    #@d4d
    .line 1028
    new-instance v51, Ljava/lang/StringBuilder;

    #@d4f
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@d52
    const-string v53, "pollStateDone: hasRegistered="

    #@d54
    move-object/from16 v0, v51

    #@d56
    move-object/from16 v1, v53

    #@d58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5b
    move-result-object v51

    #@d5c
    move-object/from16 v0, v51

    #@d5e
    move/from16 v1, v33

    #@d60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d63
    move-result-object v51

    #@d64
    const-string v53, " hasDeregistered="

    #@d66
    move-object/from16 v0, v51

    #@d68
    move-object/from16 v1, v53

    #@d6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6d
    move-result-object v51

    #@d6e
    move-object/from16 v0, v51

    #@d70
    move/from16 v1, v26

    #@d72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d75
    move-result-object v51

    #@d76
    const-string v53, " cidChanged="

    #@d78
    move-object/from16 v0, v51

    #@d7a
    move-object/from16 v1, v53

    #@d7c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7f
    move-result-object v51

    #@d80
    move-object/from16 v0, v51

    #@d82
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d85
    move-result-object v51

    #@d86
    const-string v53, " mCellInfo="

    #@d88
    move-object/from16 v0, v51

    #@d8a
    move-object/from16 v1, v53

    #@d8c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8f
    move-result-object v51

    #@d90
    move-object/from16 v0, p0

    #@d92
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@d94
    move-object/from16 v53, v0

    #@d96
    move-object/from16 v0, v51

    #@d98
    move-object/from16 v1, v53

    #@d9a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d9d
    move-result-object v51

    #@d9e
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da1
    move-result-object v51

    #@da2
    move-object/from16 v0, p0

    #@da4
    move-object/from16 v1, v51

    #@da6
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@da9
    .line 1033
    move-object/from16 v0, p0

    #@dab
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@dad
    move-object/from16 v51, v0

    #@daf
    move-object/from16 v0, v51

    #@db1
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@db4
    .line 1035
    .end local v44           #registered:Z
    .end local v48           #timeStamp:J
    :cond_db4
    move-object/from16 v0, p0

    #@db6
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mPhoneBase:Lcom/android/internal/telephony/PhoneBase;

    #@db8
    move-object/from16 v51, v0

    #@dba
    move-object/from16 v0, v51

    #@dbc
    invoke-virtual {v0, v6}, Lcom/android/internal/telephony/PhoneBase;->notifyCellInfo(Ljava/util/List;)V

    #@dbf
    .line 1036
    monitor-exit v52
    :try_end_dc0
    .catchall {:try_start_cf8 .. :try_end_dc0} :catchall_1299

    #@dc0
    .line 1037
    return-void

    #@dc1
    .line 465
    .end local v6           #arrayCi:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/CellInfo;>;"
    .end local v9           #cidChanged:Z
    .end local v10           #cil:Landroid/telephony/CellInfoLte;
    .end local v21           #has4gHandoff:Z
    .end local v22           #hasCdmaDataConnectionAttached:Z
    .end local v23           #hasCdmaDataConnectionChanged:Z
    .end local v24           #hasCdmaDataConnectionDetached:Z
    .end local v25           #hasChanged:Z
    .end local v26           #hasDeregistered:Z
    .end local v29           #hasLocationChanged:Z
    .end local v30           #hasLostMultiApnSupport:Z
    .end local v31           #hasMultiApnSupport:Z
    .end local v32           #hasRadioTechnologyChanged:Z
    .end local v33           #hasRegistered:Z
    .end local v34           #hasRoamingOff:Z
    .end local v35           #hasRoamingOn:Z
    .end local v47           #tcl:Landroid/telephony/cdma/CdmaCellLocation;
    .end local v50           #tss:Landroid/telephony/ServiceState;
    :cond_dc1
    const/16 v33, 0x0

    #@dc3
    goto/16 :goto_eb

    #@dc5
    .line 468
    .restart local v33       #hasRegistered:Z
    :cond_dc5
    const/16 v26, 0x0

    #@dc7
    goto/16 :goto_105

    #@dc9
    .line 471
    .restart local v26       #hasDeregistered:Z
    :cond_dc9
    const/16 v22, 0x0

    #@dcb
    goto/16 :goto_117

    #@dcd
    .line 475
    .restart local v22       #hasCdmaDataConnectionAttached:Z
    :cond_dcd
    const/16 v24, 0x0

    #@dcf
    goto/16 :goto_129

    #@dd1
    .line 479
    .restart local v24       #hasCdmaDataConnectionDetached:Z
    :cond_dd1
    const/16 v23, 0x0

    #@dd3
    goto/16 :goto_13d

    #@dd5
    .line 482
    .restart local v23       #hasCdmaDataConnectionChanged:Z
    :cond_dd5
    const/16 v32, 0x0

    #@dd7
    goto/16 :goto_151

    #@dd9
    .line 484
    .restart local v32       #hasRadioTechnologyChanged:Z
    :cond_dd9
    const/16 v25, 0x0

    #@ddb
    goto/16 :goto_165

    #@ddd
    .line 486
    .restart local v25       #hasChanged:Z
    :cond_ddd
    const/16 v35, 0x0

    #@ddf
    goto/16 :goto_17f

    #@de1
    .line 488
    .restart local v35       #hasRoamingOn:Z
    :cond_de1
    const/16 v34, 0x0

    #@de3
    goto/16 :goto_199

    #@de5
    .line 490
    .restart local v34       #hasRoamingOff:Z
    :cond_de5
    const/16 v29, 0x0

    #@de7
    goto/16 :goto_1ad

    #@de9
    .line 492
    .restart local v29       #hasLocationChanged:Z
    :cond_de9
    const/16 v21, 0x0

    #@deb
    goto/16 :goto_1ef

    #@ded
    .line 499
    .restart local v21       #has4gHandoff:Z
    :cond_ded
    const/16 v31, 0x0

    #@def
    goto/16 :goto_229

    #@df1
    .line 505
    .restart local v31       #hasMultiApnSupport:Z
    :cond_df1
    const/16 v30, 0x0

    #@df3
    goto/16 :goto_247

    #@df5
    .line 518
    .restart local v30       #hasLostMultiApnSupport:Z
    :cond_df5
    const/16 v30, 0x0

    #@df7
    .line 519
    new-instance v51, Ljava/lang/StringBuilder;

    #@df9
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@dfc
    const-string v52, "[LG_DATA] Don\'t create CdmaDataConnectionTracker, so, hasLostMultiApnSupport = "

    #@dfe
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e01
    move-result-object v51

    #@e02
    move-object/from16 v0, v51

    #@e04
    move/from16 v1, v30

    #@e06
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e09
    move-result-object v51

    #@e0a
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0d
    move-result-object v51

    #@e0e
    move-object/from16 v0, p0

    #@e10
    move-object/from16 v1, v51

    #@e12
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@e15
    goto/16 :goto_2b5

    #@e17
    .line 530
    :cond_e17
    move-object/from16 v0, p0

    #@e19
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@e1b
    move/from16 v51, v0

    #@e1d
    if-nez v51, :cond_e39

    #@e1f
    move-object/from16 v0, p0

    #@e21
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@e23
    move/from16 v51, v0

    #@e25
    if-nez v51, :cond_e39

    #@e27
    move-object/from16 v0, p0

    #@e29
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@e2b
    move/from16 v51, v0

    #@e2d
    if-nez v51, :cond_e39

    #@e2f
    .line 532
    const/16 v51, 0x1

    #@e31
    move/from16 v0, v51

    #@e33
    move-object/from16 v1, p0

    #@e35
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->CheckATTACH:Z

    #@e37
    goto/16 :goto_2e7

    #@e39
    .line 534
    :cond_e39
    move-object/from16 v0, p0

    #@e3b
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@e3d
    move/from16 v51, v0

    #@e3f
    if-nez v51, :cond_2e7

    #@e41
    move-object/from16 v0, p0

    #@e43
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@e45
    move/from16 v51, v0

    #@e47
    if-eqz v51, :cond_2e7

    #@e49
    .line 536
    const/16 v51, 0x0

    #@e4b
    move/from16 v0, v51

    #@e4d
    move-object/from16 v1, p0

    #@e4f
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->CheckATTACH:Z

    #@e51
    goto/16 :goto_2e7

    #@e53
    .line 544
    :cond_e53
    const/16 v51, 0x0

    #@e55
    goto/16 :goto_320

    #@e57
    .line 545
    :cond_e57
    const/16 v51, 0x0

    #@e59
    goto/16 :goto_35c

    #@e5b
    .line 547
    :cond_e5b
    const/16 v51, 0x0

    #@e5d
    goto/16 :goto_37e

    #@e5f
    .line 569
    .restart local v17       #ehrpd_internet_ipv6_enabled:I
    :cond_e5f
    move-object/from16 v0, p0

    #@e61
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@e63
    move-object/from16 v51, v0

    #@e65
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@e68
    move-result v51

    #@e69
    const/16 v52, 0xd

    #@e6b
    move/from16 v0, v51

    #@e6d
    move/from16 v1, v52

    #@e6f
    if-ne v0, v1, :cond_f0e

    #@e71
    sget-boolean v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@e73
    if-nez v51, :cond_f0e

    #@e75
    .line 572
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->getLinkProperties_defaultAPN()Landroid/net/LinkProperties;

    #@e78
    move-result-object v12

    #@e79
    .line 574
    .local v12, default_linkProp:Landroid/net/LinkProperties;
    const-wide/16 v13, 0xbb8

    #@e7b
    .line 575
    .local v13, delay:J
    const/4 v11, 0x0

    #@e7c
    .line 577
    .local v11, default_iface:Ljava/lang/String;
    const-string v51, "[EHRPD_IPV6] block_ipv6 Internet start timer some sec"

    #@e7e
    move-object/from16 v0, p0

    #@e80
    move-object/from16 v1, v51

    #@e82
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@e85
    .line 578
    const-string v51, "debug.ipv6block.delay"

    #@e87
    invoke-static/range {v51 .. v51}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@e8a
    move-result-object v15

    #@e8b
    .line 580
    .local v15, delay_block_ipv6_time:Ljava/lang/String;
    if-eqz v12, :cond_e91

    #@e8d
    .line 582
    invoke-virtual {v12}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@e90
    move-result-object v11

    #@e91
    .line 585
    :cond_e91
    if-eqz v11, :cond_f03

    #@e93
    .line 587
    if-eqz v15, :cond_ebb

    #@e95
    .line 589
    const-string v51, ""

    #@e97
    move-object/from16 v0, v51

    #@e99
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e9c
    move-result v51

    #@e9d
    if-eqz v51, :cond_efb

    #@e9f
    .line 591
    new-instance v51, Ljava/lang/StringBuilder;

    #@ea1
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@ea4
    const-string v52, "[EHRPD_IPV6] cannot parse delay_block_ipv6_time "

    #@ea6
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea9
    move-result-object v51

    #@eaa
    move-object/from16 v0, v51

    #@eac
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eaf
    move-result-object v51

    #@eb0
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb3
    move-result-object v51

    #@eb4
    move-object/from16 v0, p0

    #@eb6
    move-object/from16 v1, v51

    #@eb8
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@ebb
    .line 598
    :cond_ebb
    :goto_ebb
    new-instance v51, Ljava/lang/StringBuilder;

    #@ebd
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@ec0
    const-string v52, "[EHRPD_IPV6] Get Property : "

    #@ec2
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec5
    move-result-object v51

    #@ec6
    move-object/from16 v0, v51

    #@ec8
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ecb
    move-result-object v51

    #@ecc
    const-string v52, ", parse : "

    #@ece
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed1
    move-result-object v51

    #@ed2
    move-object/from16 v0, v51

    #@ed4
    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ed7
    move-result-object v51

    #@ed8
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@edb
    move-result-object v51

    #@edc
    move-object/from16 v0, p0

    #@ede
    move-object/from16 v1, v51

    #@ee0
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@ee3
    .line 599
    move-object/from16 v0, p0

    #@ee5
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mBlockEHRPDInternetIPv6Timer:Ljava/util/Timer;

    #@ee7
    move-object/from16 v51, v0

    #@ee9
    new-instance v52, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker$BlockEHRPDInternetIPv6Task;

    #@eeb
    move-object/from16 v0, v52

    #@eed
    move-object/from16 v1, p0

    #@eef
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker$BlockEHRPDInternetIPv6Task;-><init>(Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;)V

    #@ef2
    move-object/from16 v0, v51

    #@ef4
    move-object/from16 v1, v52

    #@ef6
    invoke-virtual {v0, v1, v13, v14}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    #@ef9
    goto/16 :goto_43b

    #@efb
    .line 595
    :cond_efb
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@efe
    move-result v51

    #@eff
    move/from16 v0, v51

    #@f01
    int-to-long v13, v0

    #@f02
    goto :goto_ebb

    #@f03
    .line 603
    :cond_f03
    const-string v51, "[EHRPD_IPV6] default_iface is null"

    #@f05
    move-object/from16 v0, p0

    #@f07
    move-object/from16 v1, v51

    #@f09
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@f0c
    goto/16 :goto_43b

    #@f0e
    .line 606
    .end local v11           #default_iface:Ljava/lang/String;
    .end local v12           #default_linkProp:Landroid/net/LinkProperties;
    .end local v13           #delay:J
    .end local v15           #delay_block_ipv6_time:Ljava/lang/String;
    :cond_f0e
    move-object/from16 v0, p0

    #@f10
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@f12
    move-object/from16 v51, v0

    #@f14
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@f17
    move-result v51

    #@f18
    const/16 v52, 0xd

    #@f1a
    move/from16 v0, v51

    #@f1c
    move/from16 v1, v52

    #@f1e
    if-eq v0, v1, :cond_43b

    #@f20
    sget-boolean v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@f22
    if-eqz v51, :cond_43b

    #@f24
    .line 609
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->getLinkProperties_defaultAPN()Landroid/net/LinkProperties;

    #@f27
    move-result-object v12

    #@f28
    .line 610
    .restart local v12       #default_linkProp:Landroid/net/LinkProperties;
    const/4 v11, 0x0

    #@f29
    .line 612
    .restart local v11       #default_iface:Ljava/lang/String;
    if-eqz v12, :cond_f2f

    #@f2b
    invoke-virtual {v12}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@f2e
    move-result-object v11

    #@f2f
    .line 614
    :cond_f2f
    if-eqz v11, :cond_f8a

    #@f31
    .line 616
    const-string v51, "network_management"

    #@f33
    invoke-static/range {v51 .. v51}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@f36
    move-result-object v8

    #@f37
    .line 617
    .local v8, b:Landroid/os/IBinder;
    invoke-static {v8}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@f3a
    move-result-object v45

    #@f3b
    .line 618
    .local v45, service:Landroid/os/INetworkManagementService;
    new-instance v51, Ljava/lang/StringBuilder;

    #@f3d
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@f40
    const-string v52, "[EHRPD_IPV6] UnBlock Interface "

    #@f42
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f45
    move-result-object v51

    #@f46
    move-object/from16 v0, v51

    #@f48
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4b
    move-result-object v51

    #@f4c
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4f
    move-result-object v51

    #@f50
    move-object/from16 v0, p0

    #@f52
    move-object/from16 v1, v51

    #@f54
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@f57
    .line 620
    if-eqz v45, :cond_f5e

    #@f59
    :try_start_f59
    move-object/from16 v0, v45

    #@f5b
    invoke-interface {v0, v11}, Landroid/os/INetworkManagementService;->unblockIPv6Interface(Ljava/lang/String;)V

    #@f5e
    .line 621
    :cond_f5e
    const/16 v51, 0x0

    #@f60
    sput-boolean v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@f62
    .line 622
    const/16 v51, 0x0

    #@f64
    sput-object v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@f66
    .line 623
    move-object/from16 v0, p0

    #@f68
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mBlockEHRPDInternetIPv6Timer:Ljava/util/Timer;

    #@f6a
    move-object/from16 v51, v0

    #@f6c
    invoke-virtual/range {v51 .. v51}, Ljava/util/Timer;->cancel()V

    #@f6f
    .line 624
    const/16 v51, 0x0

    #@f71
    move-object/from16 v0, v51

    #@f73
    move-object/from16 v1, p0

    #@f75
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mBlockEHRPDInternetIPv6Timer:Ljava/util/Timer;

    #@f77
    .line 625
    new-instance v51, Ljava/util/Timer;

    #@f79
    invoke-direct/range {v51 .. v51}, Ljava/util/Timer;-><init>()V

    #@f7c
    move-object/from16 v0, v51

    #@f7e
    move-object/from16 v1, p0

    #@f80
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mBlockEHRPDInternetIPv6Timer:Ljava/util/Timer;
    :try_end_f82
    .catch Landroid/os/RemoteException; {:try_start_f59 .. :try_end_f82} :catch_f84

    #@f82
    goto/16 :goto_43b

    #@f84
    .line 626
    :catch_f84
    move-exception v16

    #@f85
    .line 627
    .local v16, e:Landroid/os/RemoteException;
    invoke-virtual/range {v16 .. v16}, Landroid/os/RemoteException;->printStackTrace()V

    #@f88
    goto/16 :goto_43b

    #@f8a
    .line 630
    .end local v8           #b:Landroid/os/IBinder;
    .end local v16           #e:Landroid/os/RemoteException;
    .end local v45           #service:Landroid/os/INetworkManagementService;
    :cond_f8a
    sget-object v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@f8c
    if-eqz v51, :cond_43b

    #@f8e
    .line 632
    const-string v51, "network_management"

    #@f90
    invoke-static/range {v51 .. v51}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@f93
    move-result-object v8

    #@f94
    .line 633
    .restart local v8       #b:Landroid/os/IBinder;
    invoke-static {v8}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@f97
    move-result-object v45

    #@f98
    .line 634
    .restart local v45       #service:Landroid/os/INetworkManagementService;
    new-instance v51, Ljava/lang/StringBuilder;

    #@f9a
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@f9d
    const-string v52, "[EHRPD_IPV6] default_iface is null, But UnBlock Interface "

    #@f9f
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa2
    move-result-object v51

    #@fa3
    sget-object v52, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@fa5
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa8
    move-result-object v51

    #@fa9
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fac
    move-result-object v51

    #@fad
    move-object/from16 v0, p0

    #@faf
    move-object/from16 v1, v51

    #@fb1
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@fb4
    .line 636
    if-eqz v45, :cond_fbf

    #@fb6
    :try_start_fb6
    sget-object v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@fb8
    move-object/from16 v0, v45

    #@fba
    move-object/from16 v1, v51

    #@fbc
    invoke-interface {v0, v1}, Landroid/os/INetworkManagementService;->unblockIPv6Interface(Ljava/lang/String;)V

    #@fbf
    .line 637
    :cond_fbf
    const/16 v51, 0x0

    #@fc1
    sput-boolean v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@fc3
    .line 638
    const/16 v51, 0x0

    #@fc5
    sput-object v51, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@fc7
    .line 639
    move-object/from16 v0, p0

    #@fc9
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mBlockEHRPDInternetIPv6Timer:Ljava/util/Timer;

    #@fcb
    move-object/from16 v51, v0

    #@fcd
    invoke-virtual/range {v51 .. v51}, Ljava/util/Timer;->cancel()V

    #@fd0
    .line 640
    const/16 v51, 0x0

    #@fd2
    move-object/from16 v0, v51

    #@fd4
    move-object/from16 v1, p0

    #@fd6
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mBlockEHRPDInternetIPv6Timer:Ljava/util/Timer;

    #@fd8
    .line 641
    new-instance v51, Ljava/util/Timer;

    #@fda
    invoke-direct/range {v51 .. v51}, Ljava/util/Timer;-><init>()V

    #@fdd
    move-object/from16 v0, v51

    #@fdf
    move-object/from16 v1, p0

    #@fe1
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->mBlockEHRPDInternetIPv6Timer:Ljava/util/Timer;
    :try_end_fe3
    .catch Landroid/os/RemoteException; {:try_start_fb6 .. :try_end_fe3} :catch_fe5

    #@fe3
    goto/16 :goto_43b

    #@fe5
    .line 642
    :catch_fe5
    move-exception v16

    #@fe6
    .line 643
    .restart local v16       #e:Landroid/os/RemoteException;
    invoke-virtual/range {v16 .. v16}, Landroid/os/RemoteException;->printStackTrace()V

    #@fe9
    goto/16 :goto_43b

    #@feb
    .line 730
    .end local v8           #b:Landroid/os/IBinder;
    .end local v11           #default_iface:Ljava/lang/String;
    .end local v12           #default_linkProp:Landroid/net/LinkProperties;
    .end local v16           #e:Landroid/os/RemoteException;
    .end local v17           #ehrpd_internet_ipv6_enabled:I
    .end local v45           #service:Landroid/os/INetworkManagementService;
    .restart local v47       #tcl:Landroid/telephony/cdma/CdmaCellLocation;
    .restart local v50       #tss:Landroid/telephony/ServiceState;
    :cond_feb
    move-object/from16 v0, p0

    #@fed
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@fef
    move-object/from16 v51, v0

    #@ff1
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@ff4
    move-result v51

    #@ff5
    const/16 v52, 0xd

    #@ff7
    move/from16 v0, v51

    #@ff9
    move/from16 v1, v52

    #@ffb
    if-ne v0, v1, :cond_74d

    #@ffd
    .line 731
    const-string v51, "CDMA"

    #@fff
    const-string v52, "[IMS_AFW] Radio Tech is EHRPD, Get CDMA Info from modem"

    #@1001
    invoke-static/range {v51 .. v52}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1004
    .line 732
    move-object/from16 v0, p0

    #@1006
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1008
    move-object/from16 v51, v0

    #@100a
    const/16 v52, 0x65

    #@100c
    move-object/from16 v0, p0

    #@100e
    move/from16 v1, v52

    #@1010
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@1013
    move-result-object v52

    #@1014
    invoke-interface/range {v51 .. v52}, Lcom/android/internal/telephony/CommandsInterface;->getEhrpdInfoForIms(Landroid/os/Message;)V

    #@1017
    goto/16 :goto_74d

    #@1019
    .line 758
    :cond_1019
    move-object/from16 v0, p0

    #@101b
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@101d
    move-object/from16 v51, v0

    #@101f
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@1022
    move-result-object v51

    #@1023
    invoke-virtual/range {v51 .. v51}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1026
    move-result-object v51

    #@1027
    const-string v52, "tethering_pdn"

    #@1029
    const/16 v53, 0x1

    #@102b
    invoke-static/range {v51 .. v53}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@102e
    move-result v39

    #@102f
    .line 760
    .local v39, lg_data_sprint_iot_hidden_menu:I
    new-instance v51, Ljava/lang/StringBuilder;

    #@1031
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@1034
    const-string v52, "[bycho]lg_data_sprint_iot_hidden_menu : "

    #@1036
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1039
    move-result-object v51

    #@103a
    move-object/from16 v0, v51

    #@103c
    move/from16 v1, v39

    #@103e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1041
    move-result-object v51

    #@1042
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1045
    move-result-object v51

    #@1046
    move-object/from16 v0, p0

    #@1048
    move-object/from16 v1, v51

    #@104a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@104d
    .line 761
    const/16 v51, 0x2

    #@104f
    move/from16 v0, v39

    #@1051
    move/from16 v1, v51

    #@1053
    if-eq v0, v1, :cond_1075

    #@1055
    .line 762
    move-object/from16 v0, p0

    #@1057
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@1059
    move-object/from16 v51, v0

    #@105b
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@105e
    move-result-object v51

    #@105f
    invoke-virtual/range {v51 .. v51}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1062
    move-result-object v51

    #@1063
    const-string v52, "tether_dun_required"

    #@1065
    const/16 v53, 0x1

    #@1067
    invoke-static/range {v51 .. v53}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@106a
    .line 763
    const-string v51, "[bycho]There is APN profile which can handles dun type."

    #@106c
    move-object/from16 v0, p0

    #@106e
    move-object/from16 v1, v51

    #@1070
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@1073
    goto/16 :goto_829

    #@1075
    .line 765
    :cond_1075
    move-object/from16 v0, p0

    #@1077
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@1079
    move-object/from16 v51, v0

    #@107b
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@107e
    move-result-object v51

    #@107f
    invoke-virtual/range {v51 .. v51}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1082
    move-result-object v51

    #@1083
    const-string v52, "tether_dun_required"

    #@1085
    const/16 v53, 0x0

    #@1087
    invoke-static/range {v51 .. v53}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@108a
    .line 766
    const-string v51, "[bycho]User selected Internet PDN for Tethering in Hidden Menu...Do not change TEHTER_DUN_REQUIRED value for Sprint IRAT test!!!"

    #@108c
    move-object/from16 v0, p0

    #@108e
    move-object/from16 v1, v51

    #@1090
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@1093
    goto/16 :goto_829

    #@1095
    .line 798
    .end local v39           #lg_data_sprint_iot_hidden_menu:I
    :cond_1095
    move-object/from16 v0, p0

    #@1097
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1099
    move-object/from16 v51, v0

    #@109b
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@109e
    move-result v51

    #@109f
    const/16 v52, 0x3

    #@10a1
    move/from16 v0, v51

    #@10a3
    move/from16 v1, v52

    #@10a5
    if-ne v0, v1, :cond_10ca

    #@10a7
    .line 799
    move-object/from16 v0, p0

    #@10a9
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@10ab
    move-object/from16 v51, v0

    #@10ad
    if-eqz v51, :cond_10c7

    #@10af
    move-object/from16 v0, p0

    #@10b1
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@10b3
    move-object/from16 v51, v0

    #@10b5
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/uicc/IccRecords;->getServiceProviderName()Ljava/lang/String;

    #@10b8
    move-result-object v18

    #@10b9
    .line 800
    .restart local v18       #eriText:Ljava/lang/String;
    :goto_10b9
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@10bc
    move-result v51

    #@10bd
    if-eqz v51, :cond_8c4

    #@10bf
    .line 803
    const-string v51, "ro.cdma.home.operator.alpha"

    #@10c1
    invoke-static/range {v51 .. v51}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@10c4
    move-result-object v18

    #@10c5
    goto/16 :goto_8c4

    #@10c7
    .line 799
    .end local v18           #eriText:Ljava/lang/String;
    :cond_10c7
    const/16 v18, 0x0

    #@10c9
    goto :goto_10b9

    #@10ca
    .line 808
    :cond_10ca
    move-object/from16 v0, p0

    #@10cc
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@10ce
    move-object/from16 v51, v0

    #@10d0
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@10d3
    move-result-object v51

    #@10d4
    const v52, 0x10400d6

    #@10d7
    invoke-virtual/range {v51 .. v52}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@10da
    move-result-object v51

    #@10db
    invoke-virtual/range {v51 .. v51}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10de
    move-result-object v18

    #@10df
    .restart local v18       #eriText:Ljava/lang/String;
    goto/16 :goto_8c4

    #@10e1
    .line 817
    .restart local v5       #alertId:I
    :cond_10e1
    move-object/from16 v0, p0

    #@10e3
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@10e5
    move-object/from16 v51, v0

    #@10e7
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@10ea
    move-result v51

    #@10eb
    const/16 v52, 0x3

    #@10ed
    move/from16 v0, v51

    #@10ef
    move/from16 v1, v52

    #@10f1
    if-ne v0, v1, :cond_8f2

    #@10f3
    .line 818
    const/16 v18, 0x0

    #@10f5
    goto/16 :goto_8f2

    #@10f7
    .line 832
    :cond_10f7
    move-object/from16 v0, p0

    #@10f9
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@10fb
    move-object/from16 v51, v0

    #@10fd
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getState()I

    #@1100
    move-result v51

    #@1101
    if-nez v51, :cond_a6e

    #@1103
    .line 835
    move-object/from16 v0, p0

    #@1105
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedSystemIDNetworkID:Z

    #@1107
    move/from16 v51, v0

    #@1109
    if-nez v51, :cond_111b

    #@110b
    move-object/from16 v0, p0

    #@110d
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedRoamingIndicator:Z

    #@110f
    move/from16 v51, v0

    #@1111
    if-nez v51, :cond_111b

    #@1113
    move-object/from16 v0, p0

    #@1115
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@1117
    move/from16 v51, v0

    #@1119
    if-eqz v51, :cond_a6e

    #@111b
    .line 837
    :cond_111b
    move-object/from16 v0, p0

    #@111d
    invoke-virtual {v0, v5}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->PlayVZWERISound(I)V

    #@1120
    goto/16 :goto_a6e

    #@1122
    .line 866
    .end local v5           #alertId:I
    .end local v18           #eriText:Ljava/lang/String;
    :cond_1122
    move-object/from16 v0, p0

    #@1124
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@1126
    move-object/from16 v51, v0

    #@1128
    check-cast v51, Lcom/android/internal/telephony/uicc/RuimRecords;

    #@112a
    invoke-virtual/range {v51 .. v51}, Lcom/android/internal/telephony/uicc/RuimRecords;->getCsimSpnDisplayCondition()Z

    #@112d
    move-result v46

    #@112e
    .line 868
    .local v46, showSpn:Z
    move-object/from16 v0, p0

    #@1130
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1132
    move-object/from16 v51, v0

    #@1134
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getCdmaEriIconIndex()I

    #@1137
    move-result v36

    #@1138
    .line 870
    .local v36, iconIndex:I
    if-eqz v46, :cond_abd

    #@113a
    const/16 v51, 0x1

    #@113c
    move/from16 v0, v36

    #@113e
    move/from16 v1, v51

    #@1140
    if-ne v0, v1, :cond_abd

    #@1142
    move-object/from16 v0, p0

    #@1144
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1146
    move-object/from16 v51, v0

    #@1148
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getSystemId()I

    #@114b
    move-result v51

    #@114c
    move-object/from16 v0, p0

    #@114e
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1150
    move-object/from16 v52, v0

    #@1152
    invoke-virtual/range {v52 .. v52}, Landroid/telephony/ServiceState;->getNetworkId()I

    #@1155
    move-result v52

    #@1156
    move-object/from16 v0, p0

    #@1158
    move/from16 v1, v51

    #@115a
    move/from16 v2, v52

    #@115c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->isInHomeSidNid(II)Z

    #@115f
    move-result v51

    #@1160
    if-eqz v51, :cond_abd

    #@1162
    move-object/from16 v0, p0

    #@1164
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@1166
    move-object/from16 v51, v0

    #@1168
    if-eqz v51, :cond_abd

    #@116a
    .line 873
    move-object/from16 v0, p0

    #@116c
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@116e
    move-object/from16 v51, v0

    #@1170
    move-object/from16 v0, p0

    #@1172
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@1174
    move-object/from16 v52, v0

    #@1176
    invoke-virtual/range {v52 .. v52}, Lcom/android/internal/telephony/uicc/IccRecords;->getServiceProviderName()Ljava/lang/String;

    #@1179
    move-result-object v52

    #@117a
    invoke-virtual/range {v51 .. v52}, Landroid/telephony/ServiceState;->setOperatorAlphaLong(Ljava/lang/String;)V

    #@117d
    goto/16 :goto_abd

    #@117f
    .line 894
    .end local v36           #iconIndex:I
    .end local v46           #showSpn:Z
    .restart local v42       #operatorNumeric:Ljava/lang/String;
    .restart local v43       #prevOperatorNumeric:Ljava/lang/String;
    :cond_117f
    const-string v38, ""

    #@1181
    .line 895
    .local v38, isoCountryCode:Ljava/lang/String;
    const/16 v51, 0x0

    #@1183
    const/16 v52, 0x3

    #@1185
    move-object/from16 v0, v42

    #@1187
    move/from16 v1, v51

    #@1189
    move/from16 v2, v52

    #@118b
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@118e
    move-result-object v40

    #@118f
    .line 897
    .local v40, mcc:Ljava/lang/String;
    const/16 v51, 0x0

    #@1191
    const/16 v52, 0x3

    #@1193
    :try_start_1193
    move-object/from16 v0, v42

    #@1195
    move/from16 v1, v51

    #@1197
    move/from16 v2, v52

    #@1199
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@119c
    move-result-object v51

    #@119d
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@11a0
    move-result v51

    #@11a1
    invoke-static/range {v51 .. v51}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;
    :try_end_11a4
    .catch Ljava/lang/NumberFormatException; {:try_start_1193 .. :try_end_11a4} :catch_11e3
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1193 .. :try_end_11a4} :catch_1203

    #@11a4
    move-result-object v38

    #@11a5
    .line 905
    :goto_11a5
    move-object/from16 v0, p0

    #@11a7
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@11a9
    move-object/from16 v51, v0

    #@11ab
    const-string v52, "gsm.operator.iso-country"

    #@11ad
    move-object/from16 v0, v51

    #@11af
    move-object/from16 v1, v52

    #@11b1
    move-object/from16 v2, v38

    #@11b3
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@11b6
    .line 907
    const/16 v51, 0x1

    #@11b8
    move/from16 v0, v51

    #@11ba
    move-object/from16 v1, p0

    #@11bc
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@11be
    .line 909
    move-object/from16 v0, p0

    #@11c0
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@11c2
    move-object/from16 v51, v0

    #@11c4
    move-object/from16 v0, p0

    #@11c6
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@11c8
    move/from16 v52, v0

    #@11ca
    move-object/from16 v0, p0

    #@11cc
    move-object/from16 v1, v51

    #@11ce
    move-object/from16 v2, v42

    #@11d0
    move-object/from16 v3, v43

    #@11d2
    move/from16 v4, v52

    #@11d4
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->shouldFixTimeZoneNow(Lcom/android/internal/telephony/PhoneBase;Ljava/lang/String;Ljava/lang/String;Z)Z

    #@11d7
    move-result v51

    #@11d8
    if-eqz v51, :cond_b15

    #@11da
    .line 911
    move-object/from16 v0, p0

    #@11dc
    move-object/from16 v1, v38

    #@11de
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->fixTimeZone(Ljava/lang/String;)V

    #@11e1
    goto/16 :goto_b15

    #@11e3
    .line 899
    :catch_11e3
    move-exception v20

    #@11e4
    .line 900
    .local v20, ex:Ljava/lang/NumberFormatException;
    new-instance v51, Ljava/lang/StringBuilder;

    #@11e6
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@11e9
    const-string v52, "countryCodeForMcc error"

    #@11eb
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11ee
    move-result-object v51

    #@11ef
    move-object/from16 v0, v51

    #@11f1
    move-object/from16 v1, v20

    #@11f3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11f6
    move-result-object v51

    #@11f7
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11fa
    move-result-object v51

    #@11fb
    move-object/from16 v0, p0

    #@11fd
    move-object/from16 v1, v51

    #@11ff
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->loge(Ljava/lang/String;)V

    #@1202
    goto :goto_11a5

    #@1203
    .line 901
    .end local v20           #ex:Ljava/lang/NumberFormatException;
    :catch_1203
    move-exception v20

    #@1204
    .line 902
    .local v20, ex:Ljava/lang/StringIndexOutOfBoundsException;
    new-instance v51, Ljava/lang/StringBuilder;

    #@1206
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@1209
    const-string v52, "countryCodeForMcc error"

    #@120b
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120e
    move-result-object v51

    #@120f
    move-object/from16 v0, v51

    #@1211
    move-object/from16 v1, v20

    #@1213
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1216
    move-result-object v51

    #@1217
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121a
    move-result-object v51

    #@121b
    move-object/from16 v0, p0

    #@121d
    move-object/from16 v1, v51

    #@121f
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->loge(Ljava/lang/String;)V

    #@1222
    goto :goto_11a5

    #@1223
    .line 929
    .end local v20           #ex:Ljava/lang/StringIndexOutOfBoundsException;
    .end local v38           #isoCountryCode:Ljava/lang/String;
    .end local v40           #mcc:Ljava/lang/String;
    :catch_1223
    move-exception v16

    #@1224
    .line 930
    .local v16, e:Ljava/lang/NumberFormatException;
    const-string v51, "NumberFormatException"

    #@1226
    move-object/from16 v0, p0

    #@1228
    move-object/from16 v1, v51

    #@122a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->loge(Ljava/lang/String;)V

    #@122d
    goto/16 :goto_b89

    #@122f
    .line 936
    .end local v16           #e:Ljava/lang/NumberFormatException;
    :cond_122f
    const-string v51, "false"

    #@1231
    goto/16 :goto_b9f

    #@1233
    .line 947
    .end local v42           #operatorNumeric:Ljava/lang/String;
    .end local v43           #prevOperatorNumeric:Ljava/lang/String;
    :cond_1233
    move-object/from16 v0, p0

    #@1235
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1237
    move-object/from16 v51, v0

    #@1239
    invoke-interface/range {v51 .. v51}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@123c
    move-result-object v51

    #@123d
    move-object/from16 v0, v51

    #@123f
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

    #@1241
    move/from16 v51, v0

    #@1243
    if-eqz v51, :cond_bc7

    #@1245
    move-object/from16 v0, p0

    #@1247
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->CheckATTACH:Z

    #@1249
    move/from16 v51, v0

    #@124b
    const/16 v52, 0x1

    #@124d
    move/from16 v0, v51

    #@124f
    move/from16 v1, v52

    #@1251
    if-ne v0, v1, :cond_bc7

    #@1253
    move-object/from16 v0, p0

    #@1255
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1257
    move-object/from16 v51, v0

    #@1259
    invoke-virtual/range {v51 .. v51}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@125c
    move-result v51

    #@125d
    if-eqz v51, :cond_bc7

    #@125f
    .line 950
    const/16 v51, 0x0

    #@1261
    move/from16 v0, v51

    #@1263
    move-object/from16 v1, p0

    #@1265
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->CheckATTACH:Z

    #@1267
    .line 951
    new-instance v51, Ljava/lang/StringBuilder;

    #@1269
    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    #@126c
    const-string v52, "mAttachedRegistrants.notifyRegistrants() CheckATTACH:: "

    #@126e
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1271
    move-result-object v51

    #@1272
    move-object/from16 v0, p0

    #@1274
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->CheckATTACH:Z

    #@1276
    move/from16 v52, v0

    #@1278
    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@127b
    move-result-object v51

    #@127c
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@127f
    move-result-object v51

    #@1280
    move-object/from16 v0, p0

    #@1282
    move-object/from16 v1, v51

    #@1284
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@1287
    .line 952
    move-object/from16 v0, p0

    #@1289
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mAttachedRegistrants:Landroid/os/RegistrantList;

    #@128b
    move-object/from16 v51, v0

    #@128d
    invoke-virtual/range {v51 .. v51}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@1290
    goto/16 :goto_bc7

    #@1292
    .line 1018
    .restart local v6       #arrayCi:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/CellInfo;>;"
    .restart local v10       #cil:Landroid/telephony/CellInfoLte;
    :cond_1292
    const/4 v9, 0x0

    #@1293
    goto/16 :goto_d15

    #@1295
    .line 1022
    .restart local v9       #cidChanged:Z
    .restart local v48       #timeStamp:J
    :cond_1295
    const/16 v44, 0x0

    #@1297
    goto/16 :goto_d31

    #@1299
    .line 1036
    .end local v9           #cidChanged:Z
    .end local v10           #cil:Landroid/telephony/CellInfoLte;
    .end local v48           #timeStamp:J
    :catchall_1299
    move-exception v51

    #@129a
    :try_start_129a
    monitor-exit v52
    :try_end_129b
    .catchall {:try_start_129a .. :try_end_129b} :catchall_1299

    #@129b
    throw v51
.end method

.method protected setCdmaTechnology(I)V
    .registers 3
    .parameter "radioTechnology"

    #@0
    .prologue
    .line 232
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2
    invoke-virtual {v0, p1}, Landroid/telephony/ServiceState;->setRadioTechnology(I)V

    #@5
    .line 233
    return-void
.end method

.method public set_internetpdn_ipv6_blocked_by_ip6table(Z)V
    .registers 4
    .parameter "blocked"

    #@0
    .prologue
    .line 1159
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "set_internetpdn_ipv6_blocked_by_ip6table current :"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-boolean v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", requested "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@22
    .line 1160
    sput-boolean p1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@24
    .line 1161
    return-void
.end method

.method public set_internetpdn_ipv6_blocked_iface(Ljava/lang/String;)V
    .registers 4
    .parameter "blocked_iface"

    #@0
    .prologue
    .line 1167
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "set_internetpdn_ipv6_blocked_iface current :"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", requested "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->log(Ljava/lang/String;)V

    #@22
    .line 1168
    sput-object p1, Lcom/android/internal/telephony/cdma/CdmaLteServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@24
    .line 1169
    return-void
.end method
