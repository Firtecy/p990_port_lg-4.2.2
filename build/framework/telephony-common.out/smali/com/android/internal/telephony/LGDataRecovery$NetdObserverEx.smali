.class Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;
.super Landroid/net/INetworkManagementEventObserverEx$Stub;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGDataRecovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetdObserverEx"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LGDataRecovery;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/LGDataRecovery;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 794
    iput-object p1, p0, Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@2
    invoke-direct {p0}, Landroid/net/INetworkManagementEventObserverEx$Stub;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/LGDataRecovery;Lcom/android/internal/telephony/LGDataRecovery$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 794
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@3
    return-void
.end method


# virtual methods
.method public DnsFailed(Ljava/lang/String;I)V
    .registers 7
    .parameter "host"
    .parameter "errorCode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 805
    new-instance v0, Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;

    #@2
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@4
    invoke-direct {v0, v1, p1, p2}, Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;-><init>(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;I)V

    #@7
    .line 806
    .local v0, failResult:Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@9
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@b
    const v3, 0x42069

    #@e
    invoke-virtual {v2, v3, v0}, Lcom/android/internal/telephony/LGDataRecovery;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/LGDataRecovery;->sendMessage(Landroid/os/Message;)Z

    #@15
    .line 807
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "DNS result Hostname: "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, " code: "

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v2, " desc: "

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@36
    invoke-virtual {v2, p2}, Lcom/android/internal/telephony/LGDataRecovery;->errorCodeToString(I)Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    invoke-static {v1}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@45
    .line 808
    return-void
.end method

.method public interfaceLinkStateChanged(Ljava/lang/String;Z)V
    .registers 5
    .parameter "iface"
    .parameter "up"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 797
    const-string v0, "rmnet"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    const/4 v1, -0x1

    #@7
    if-eq v0, v1, :cond_2e

    #@9
    .line 798
    new-instance v0, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v1, "RMNET interface "

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    const-string v1, " state changed: UP??"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    invoke-static {v0}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@29
    .line 799
    iget-object v0, p0, Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@2b
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/telephony/LGDataRecovery;->updateConnectionInfosList(Ljava/lang/String;Z)V

    #@2e
    .line 801
    :cond_2e
    return-void
.end method

.method public interfaceThrottleStateChanged(Ljava/lang/String;I)V
    .registers 3
    .parameter "iface"
    .parameter "status"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 811
    return-void
.end method
