.class Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;
.super Ljava/lang/Object;
.source "IccPhonebookProviderBackendImp.java"

# interfaces
.implements Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$1;,
        Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;,
        Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;,
        Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final DEBUG_V:Z = false

.field private static final EMPTY_ENTRY:[Ljava/lang/Object; = null

.field private static final EMPTY_GROUP:[Ljava/lang/Object; = null

.field private static final EMPTY_INFO:[Ljava/lang/Object; = null

.field private static final STATUS_NOENTRY:I = 0x2

.field private static final SUPPORT_GROUP:Z = false

.field private static final TAG:Ljava/lang/String; = "IccPhonebookProvider"


# instance fields
.field private mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

.field private mInfo:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 46
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@7
    array-length v0, v0

    #@8
    new-array v0, v0, [Ljava/lang/Object;

    #@a
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@c
    .line 47
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@e
    const/16 v1, 0xf

    #@10
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v2

    #@14
    aput-object v2, v0, v1

    #@16
    .line 48
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@18
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v1

    #@1c
    aput-object v1, v0, v6

    #@1e
    .line 49
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@20
    const-string v1, ""

    #@22
    aput-object v1, v0, v4

    #@24
    .line 50
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@26
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29
    move-result-object v1

    #@2a
    aput-object v1, v0, v5

    #@2c
    .line 51
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@2e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v1

    #@32
    aput-object v1, v0, v7

    #@34
    .line 52
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@36
    const/4 v1, 0x5

    #@37
    const-string v2, ""

    #@39
    aput-object v2, v0, v1

    #@3b
    .line 53
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@3d
    const/4 v1, 0x6

    #@3e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41
    move-result-object v2

    #@42
    aput-object v2, v0, v1

    #@44
    .line 54
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@46
    const/4 v1, 0x7

    #@47
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4a
    move-result-object v2

    #@4b
    aput-object v2, v0, v1

    #@4d
    .line 55
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@4f
    const/16 v1, 0x8

    #@51
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@54
    move-result-object v2

    #@55
    aput-object v2, v0, v1

    #@57
    .line 56
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@59
    const/16 v1, 0x9

    #@5b
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5e
    move-result-object v2

    #@5f
    aput-object v2, v0, v1

    #@61
    .line 57
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@63
    const/16 v1, 0xa

    #@65
    const-string v2, ""

    #@67
    aput-object v2, v0, v1

    #@69
    .line 58
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@6b
    const/16 v1, 0xb

    #@6d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@70
    move-result-object v2

    #@71
    aput-object v2, v0, v1

    #@73
    .line 59
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@75
    const/16 v1, 0xc

    #@77
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7a
    move-result-object v2

    #@7b
    aput-object v2, v0, v1

    #@7d
    .line 60
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@7f
    const/16 v1, 0xd

    #@81
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@84
    move-result-object v2

    #@85
    aput-object v2, v0, v1

    #@87
    .line 61
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@89
    const/16 v1, 0xe

    #@8b
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8e
    move-result-object v2

    #@8f
    aput-object v2, v0, v1

    #@91
    .line 62
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@93
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@96
    move-result-object v1

    #@97
    aput-object v1, v0, v3

    #@99
    .line 67
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@9b
    array-length v0, v0

    #@9c
    new-array v0, v0, [Ljava/lang/Object;

    #@9e
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@a0
    .line 68
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@a2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a5
    move-result-object v1

    #@a6
    aput-object v1, v0, v3

    #@a8
    .line 69
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@aa
    const-string v1, ""

    #@ac
    aput-object v1, v0, v4

    #@ae
    .line 70
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@b0
    const-string v1, ""

    #@b2
    aput-object v1, v0, v5

    #@b4
    .line 71
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@b6
    const-string v1, ""

    #@b8
    aput-object v1, v0, v6

    #@ba
    .line 72
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@bc
    const-string v1, ""

    #@be
    aput-object v1, v0, v7

    #@c0
    .line 73
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@c2
    const/4 v1, 0x5

    #@c3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c6
    move-result-object v2

    #@c7
    aput-object v2, v0, v1

    #@c9
    .line 78
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->GROUP_PROJECTION:[Ljava/lang/String;

    #@cb
    array-length v0, v0

    #@cc
    new-array v0, v0, [Ljava/lang/Object;

    #@ce
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_GROUP:[Ljava/lang/Object;

    #@d0
    .line 79
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_GROUP:[Ljava/lang/Object;

    #@d2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d5
    move-result-object v1

    #@d6
    aput-object v1, v0, v3

    #@d8
    .line 80
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_GROUP:[Ljava/lang/Object;

    #@da
    const-string v1, ""

    #@dc
    aput-object v1, v0, v4

    #@de
    .line 81
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 90
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 85
    new-instance v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@5
    const/4 v1, 0x0

    #@6
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;-><init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$1;)V

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@b
    .line 86
    new-instance v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;

    #@d
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;-><init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;)V

    #@10
    iput-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mInfo:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;

    #@12
    .line 91
    const-string v0, "IccPhonebookProvider"

    #@14
    const-string v1, "*** QCT IccPhonebookProvider ***"

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 92
    const-string v0, "IccPhonebookProvider"

    #@1b
    const-string v1, "| SUPPORT_GROUP=false"

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 93
    return-void
.end method

.method static synthetic access$1500(Ljava/lang/String;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 27
    invoke-static {p0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->detectType(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1600(Ljava/lang/String;IZ)Ljava/lang/String;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 27
    invoke-static {p0, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->convertValidSeparators(Ljava/lang/String;IZ)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1800()[Ljava/lang/Object;
    .registers 1

    #@0
    .prologue
    .line 27
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method private static convertValidSeparators(Ljava/lang/String;IZ)Ljava/lang/String;
    .registers 9
    .parameter "number"
    .parameter "type"
    .parameter "inverse"

    #@0
    .prologue
    const/16 v5, 0x2b

    #@2
    .line 975
    if-nez p0, :cond_7

    #@4
    .line 976
    const-string v4, ""

    #@6
    .line 994
    :goto_6
    return-object v4

    #@7
    .line 978
    :cond_7
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@a
    move-result v2

    #@b
    .line 979
    .local v2, len:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@10
    .line 981
    .local v3, ret:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v2, :cond_2a

    #@13
    .line 982
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@16
    move-result v0

    #@17
    .line 984
    .local v0, c:C
    if-eqz p2, :cond_1f

    #@19
    .line 991
    :cond_19
    :goto_19
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1c
    .line 981
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_11

    #@1f
    .line 987
    :cond_1f
    const/4 v4, 0x1

    #@20
    if-ne p1, v4, :cond_19

    #@22
    if-nez v1, :cond_19

    #@24
    if-eq v0, v5, :cond_19

    #@26
    .line 988
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@29
    goto :goto_19

    #@2a
    .line 994
    .end local v0           #c:C
    :cond_2a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    goto :goto_6
.end method

.method private static detectType(Ljava/lang/String;)I
    .registers 4
    .parameter "num"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 959
    if-eqz p0, :cond_9

    #@3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_a

    #@9
    .line 967
    :cond_9
    :goto_9
    return v0

    #@a
    .line 963
    :cond_a
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@d
    move-result v1

    #@e
    const/16 v2, 0x2b

    #@10
    if-ne v1, v2, :cond_9

    #@12
    .line 964
    const/4 v0, 0x1

    #@13
    goto :goto_9
.end method


# virtual methods
.method public deleteEntry(Landroid/content/Context;I)I
    .registers 4
    .parameter "context"
    .parameter "simIndex"

    #@0
    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@2
    invoke-static {v0, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->access$600(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public deleteGroup(Landroid/content/Context;I)I
    .registers 4
    .parameter "context"
    .parameter "groupIndex"

    #@0
    .prologue
    .line 157
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@2
    invoke-static {v0, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->access$1100(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public insertEntry(Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 5
    .parameter "context"
    .parameter "simIndex"
    .parameter "values"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@2
    invoke-static {v0, p1, p2, p3}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->access$400(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;ILandroid/content/ContentValues;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public insertGroup(Landroid/content/Context;Landroid/content/ContentValues;)I
    .registers 4
    .parameter "context"
    .parameter "values"

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@2
    invoke-static {v0, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->access$900(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;Landroid/content/ContentValues;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public readEntry(Landroid/content/Context;I)Landroid/database/Cursor;
    .registers 5
    .parameter "context"
    .parameter "simIndex"

    #@0
    .prologue
    .line 106
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@2
    invoke-static {v1, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->access$200(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;I)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@5
    move-result-object v0

    #@6
    .line 107
    .local v0, record:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@8
    invoke-static {v1, v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->access$300(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)Landroid/database/Cursor;

    #@b
    move-result-object v1

    #@c
    return-object v1
.end method

.method public readGroup(Landroid/content/Context;I)Landroid/database/Cursor;
    .registers 5
    .parameter "context"
    .parameter "groupIndex"

    #@0
    .prologue
    .line 135
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@2
    invoke-static {v1, p1, p2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->access$700(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;I)Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@5
    move-result-object v0

    #@6
    .line 136
    .local v0, record:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@8
    invoke-static {v1, v0}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->access$800(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)Landroid/database/Cursor;

    #@b
    move-result-object v1

    #@c
    return-object v1
.end method

.method public readInfo(Landroid/content/Context;)Landroid/database/Cursor;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mInfo:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;

    #@2
    invoke-static {v0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->access$100(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;Landroid/content/Context;)Landroid/database/Cursor;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public updateEntry(Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 5
    .parameter "context"
    .parameter "simIndex"
    .parameter "values"

    #@0
    .prologue
    .line 121
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@2
    invoke-static {v0, p1, p2, p3}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->access$500(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;ILandroid/content/ContentValues;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public updateGroup(Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 5
    .parameter "context"
    .parameter "groupIndex"
    .parameter "values"

    #@0
    .prologue
    .line 150
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->mIO:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;

    #@2
    invoke-static {v0, p1, p2, p3}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;->access$1000(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctIO;Landroid/content/Context;ILandroid/content/ContentValues;)I

    #@5
    move-result v0

    #@6
    return v0
.end method
