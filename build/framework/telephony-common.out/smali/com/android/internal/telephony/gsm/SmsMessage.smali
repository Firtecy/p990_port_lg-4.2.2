.class public Lcom/android/internal/telephony/gsm/SmsMessage;
.super Lcom/android/internal/telephony/SmsMessageBase;
.source "SmsMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;,
        Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;,
        Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;,
        Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final ENCODING_16BIT_DCS:I = 0x8

.field public static final ENCODING_7BIT_DCS:I = 0x0

.field public static final ENCODING_8BIT_DCS:I = 0x84

.field public static final ENCODING_UNKNOWN_DCS:I = -0x1

.field static final LOG_TAG:Ljava/lang/String; = "GSM"

.field public static final PID_1ST_SPECIAL_SMS:I = 0x48

.field public static final PID_2ND_SPECIAL_SMS:I = 0x5f

.field public static final PID_KT_CALLBACK_URL:I = 0x4e

.field public static final PID_KT_FOTA:I = 0x7d

.field public static final PID_KT_LBS:I = 0x51

.field public static final PID_KT_PORT_ADDRESS:I = 0x53

.field public static final PID_LGT_PORT_ADDRESS:I = 0x53

.field public static final PID_LMS:I = 0x4f

.field public static final PID_NORMAL_MESSAGE:I = 0x0

.field public static final PID_SKT_CALLBACK_URL:I = 0x91

.field public static final PID_SKT_PORT_ADDRESS:I = 0xa2

.field static final TP_VPF_ABSOLUTE:I = 0x3

.field static final TP_VPF_ENHANCED:I = 0x1

.field static final TP_VPF_NONE:I = 0x0

.field static final TP_VPF_RELATIVE:I = 0x2

.field private static timeSmsOnSim:J


# instance fields
.field private automaticDeletion:Z

.field private dataCodingScheme:I

.field private dischargeTimeMillis:J

.field private forSubmit:Z

.field private isStatusReportMessage:Z

.field private mVoiceMailCount:I

.field private messageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

.field private mti:I

.field private protocolIdentifier:I

.field private recipientAddress:Lcom/android/internal/telephony/gsm/GsmSmsAddress;

.field private replyPathPresent:Z

.field private status:I

.field validtyPeroidFormat:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 188
    const-wide/16 v0, 0x0

    #@2
    sput-wide v0, Lcom/android/internal/telephony/gsm/SmsMessage;->timeSmsOnSim:J

    #@4
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 108
    invoke-direct {p0}, Lcom/android/internal/telephony/SmsMessageBase;-><init>()V

    #@4
    .line 156
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->replyPathPresent:Z

    #@6
    .line 182
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->isStatusReportMessage:Z

    #@8
    .line 185
    const/4 v0, -0x1

    #@9
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->validtyPeroidFormat:I

    #@b
    .line 202
    iput v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->mVoiceMailCount:I

    #@d
    .line 2111
    return-void
.end method

.method public static calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 5
    .parameter "msgBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 2462
    invoke-static {p0, p1}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptetsLossyAuto(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@3
    move-result-object v1

    #@4
    .line 2463
    .local v1, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    if-nez v1, :cond_2d

    #@6
    .line 2464
    new-instance v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@8
    .end local v1           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    invoke-direct {v1}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@b
    .line 2465
    .restart local v1       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@e
    move-result v2

    #@f
    mul-int/lit8 v0, v2, 0x2

    #@11
    .line 2466
    .local v0, octets:I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@14
    move-result v2

    #@15
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@17
    .line 2467
    const/16 v2, 0x8c

    #@19
    if-le v0, v2, :cond_2e

    #@1b
    .line 2468
    add-int/lit16 v2, v0, 0x85

    #@1d
    div-int/lit16 v2, v2, 0x86

    #@1f
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@21
    .line 2470
    iget v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@23
    mul-int/lit16 v2, v2, 0x86

    #@25
    sub-int/2addr v2, v0

    #@26
    div-int/lit8 v2, v2, 0x2

    #@28
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@2a
    .line 2476
    :goto_2a
    const/4 v2, 0x3

    #@2b
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@2d
    .line 2478
    .end local v0           #octets:I
    :cond_2d
    return-object v1

    #@2e
    .line 2473
    .restart local v0       #octets:I
    :cond_2e
    const/4 v2, 0x1

    #@2f
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@31
    .line 2474
    rsub-int v2, v0, 0x8c

    #@33
    div-int/lit8 v2, v2, 0x2

    #@35
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@37
    goto :goto_2a
.end method

.method public static calculateLength(Ljava/lang/CharSequence;I)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 15
    .parameter "msgBody"
    .parameter "dataCodingScheme"

    #@0
    .prologue
    const/16 v12, 0x8c

    #@2
    const/4 v9, 0x0

    #@3
    .line 2602
    new-instance v1, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;

    #@5
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8
    move-result-object v10

    #@9
    invoke-direct {v1, p1, v10}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;-><init>(ILjava/lang/String;)V

    #@c
    .line 2603
    .local v1, dcs:Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getEncodingType()I

    #@f
    move-result v2

    #@10
    .line 2605
    .local v2, encodingType:I
    new-instance v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@12
    invoke-direct {v6}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@15
    .line 2606
    .local v6, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    packed-switch v2, :pswitch_data_128

    #@18
    .line 2659
    :try_start_18
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@1b
    move-result v10

    #@1c
    mul-int/lit8 v4, v10, 0x2

    #@1e
    .line 2660
    .local v4, octets:I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@21
    move-result v10

    #@22
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@24
    .line 2661
    if-le v4, v12, :cond_f8

    #@26
    .line 2662
    div-int/lit16 v10, v4, 0x86

    #@28
    add-int/lit8 v10, v10, 0x1

    #@2a
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@2c
    .line 2663
    rem-int/lit16 v10, v4, 0x86

    #@2e
    rsub-int v10, v10, 0x86

    #@30
    div-int/lit8 v10, v10, 0x2

    #@32
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@34
    .line 2669
    :goto_34
    const/4 v10, 0x3

    #@35
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I
    :try_end_37
    .catch Ljava/lang/RuntimeException; {:try_start_18 .. :try_end_37} :catch_103

    #@37
    .line 2676
    .end local v4           #octets:I
    .end local v6           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :goto_37
    return-object v6

    #@38
    .line 2609
    .restart local v6       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :pswitch_38
    const/4 v10, 0x0

    #@39
    :try_start_39
    invoke-static {p0, v10}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptets(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@3c
    move-result-object v7

    #@3d
    .line 2610
    .local v7, tedGsm7bit:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    if-nez v7, :cond_46

    #@3f
    .line 2611
    const-string v10, "calculateLength(), countGsmSeptets return is null"

    #@41
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@44
    move-object v6, v9

    #@45
    .line 2612
    goto :goto_37

    #@46
    .line 2615
    :cond_46
    iget v5, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@48
    .line 2617
    .local v5, septets:I
    iput v5, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@4a
    .line 2618
    const/16 v10, 0xa0

    #@4c
    if-le v5, v10, :cond_81

    #@4e
    .line 2619
    div-int/lit16 v10, v5, 0x99

    #@50
    add-int/lit8 v10, v10, 0x1

    #@52
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@54
    .line 2620
    rem-int/lit16 v10, v5, 0x99

    #@56
    rsub-int v10, v10, 0x99

    #@58
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@5a
    .line 2626
    :goto_5a
    const/4 v10, 0x1

    #@5b
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I
    :try_end_5d
    .catch Ljava/lang/RuntimeException; {:try_start_39 .. :try_end_5d} :catch_5e

    #@5d
    goto :goto_37

    #@5e
    .line 2628
    .end local v5           #septets:I
    .end local v7           #tedGsm7bit:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :catch_5e
    move-exception v3

    #@5f
    .line 2629
    .local v3, ex:Ljava/lang/RuntimeException;
    new-instance v10, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v11, "calculateLength(), encodingType: "

    #@66
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v10

    #@6a
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v10

    #@6e
    const-string v11, ", "

    #@70
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v10

    #@74
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v10

    #@78
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v10

    #@7c
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@7f
    move-object v6, v9

    #@80
    .line 2630
    goto :goto_37

    #@81
    .line 2623
    .end local v3           #ex:Ljava/lang/RuntimeException;
    .restart local v5       #septets:I
    .restart local v7       #tedGsm7bit:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_81
    const/4 v10, 0x1

    #@82
    :try_start_82
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@84
    .line 2624
    rsub-int v10, v5, 0xa0

    #@86
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I
    :try_end_88
    .catch Ljava/lang/RuntimeException; {:try_start_82 .. :try_end_88} :catch_5e

    #@88
    goto :goto_5a

    #@89
    .line 2635
    .end local v5           #septets:I
    .end local v7           #tedGsm7bit:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :pswitch_89
    :try_start_89
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8c
    move-result-object v10

    #@8d
    const-string v11, "euc-kr"

    #@8f
    invoke-virtual {v10, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@92
    move-result-object v8

    #@93
    .line 2636
    .local v8, textPart:[B
    array-length v0, v8

    #@94
    .line 2638
    .local v0, byteCount:I
    iput v0, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@96
    .line 2639
    if-le v0, v12, :cond_cc

    #@98
    .line 2640
    div-int/lit16 v10, v0, 0x86

    #@9a
    add-int/lit8 v10, v10, 0x1

    #@9c
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@9e
    .line 2641
    rem-int/lit16 v10, v0, 0x86

    #@a0
    rsub-int v10, v10, 0x86

    #@a2
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@a4
    .line 2647
    :goto_a4
    const/4 v10, 0x2

    #@a5
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I
    :try_end_a7
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_89 .. :try_end_a7} :catch_a8
    .catch Ljava/lang/RuntimeException; {:try_start_89 .. :try_end_a7} :catch_d4

    #@a7
    goto :goto_37

    #@a8
    .line 2649
    .end local v0           #byteCount:I
    .end local v8           #textPart:[B
    :catch_a8
    move-exception v3

    #@a9
    .line 2650
    .local v3, ex:Ljava/io/UnsupportedEncodingException;
    new-instance v10, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    const-string v11, "calculateLength(), encodingType: "

    #@b0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v10

    #@b4
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v10

    #@b8
    const-string v11, ", "

    #@ba
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v10

    #@be
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v10

    #@c2
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v10

    #@c6
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@c9
    move-object v6, v9

    #@ca
    .line 2651
    goto/16 :goto_37

    #@cc
    .line 2644
    .end local v3           #ex:Ljava/io/UnsupportedEncodingException;
    .restart local v0       #byteCount:I
    .restart local v8       #textPart:[B
    :cond_cc
    const/4 v10, 0x1

    #@cd
    :try_start_cd
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@cf
    .line 2645
    rsub-int v10, v0, 0x8c

    #@d1
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I
    :try_end_d3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_cd .. :try_end_d3} :catch_a8
    .catch Ljava/lang/RuntimeException; {:try_start_cd .. :try_end_d3} :catch_d4

    #@d3
    goto :goto_a4

    #@d4
    .line 2652
    .end local v0           #byteCount:I
    .end local v8           #textPart:[B
    :catch_d4
    move-exception v3

    #@d5
    .line 2653
    .local v3, ex:Ljava/lang/RuntimeException;
    new-instance v10, Ljava/lang/StringBuilder;

    #@d7
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@da
    const-string v11, "calculateLength(), encodingType: "

    #@dc
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v10

    #@e0
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v10

    #@e4
    const-string v11, ", "

    #@e6
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v10

    #@ea
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v10

    #@ee
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v10

    #@f2
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@f5
    move-object v6, v9

    #@f6
    .line 2654
    goto/16 :goto_37

    #@f8
    .line 2666
    .end local v3           #ex:Ljava/lang/RuntimeException;
    .restart local v4       #octets:I
    :cond_f8
    const/4 v10, 0x1

    #@f9
    :try_start_f9
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@fb
    .line 2667
    rsub-int v10, v4, 0x8c

    #@fd
    div-int/lit8 v10, v10, 0x2

    #@ff
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I
    :try_end_101
    .catch Ljava/lang/RuntimeException; {:try_start_f9 .. :try_end_101} :catch_103

    #@101
    goto/16 :goto_34

    #@103
    .line 2671
    .end local v4           #octets:I
    :catch_103
    move-exception v3

    #@104
    .line 2672
    .restart local v3       #ex:Ljava/lang/RuntimeException;
    new-instance v10, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v11, "calculateLength(), encodingType: "

    #@10b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v10

    #@10f
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@112
    move-result-object v10

    #@113
    const-string v11, ", "

    #@115
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v10

    #@119
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v10

    #@11d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v10

    #@121
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@124
    move-object v6, v9

    #@125
    .line 2673
    goto/16 :goto_37

    #@127
    .line 2606
    nop

    #@128
    :pswitch_data_128
    .packed-switch 0x1
        :pswitch_38
        :pswitch_89
    .end packed-switch
.end method

.method public static calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 5
    .parameter "msgBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 2437
    invoke-static {p0, p1}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptets(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@3
    move-result-object v1

    #@4
    .line 2438
    .local v1, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    if-nez v1, :cond_2d

    #@6
    .line 2439
    new-instance v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@8
    .end local v1           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    invoke-direct {v1}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@b
    .line 2440
    .restart local v1       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@e
    move-result v2

    #@f
    mul-int/lit8 v0, v2, 0x2

    #@11
    .line 2441
    .local v0, octets:I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@14
    move-result v2

    #@15
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@17
    .line 2442
    const/16 v2, 0x8c

    #@19
    if-le v0, v2, :cond_2e

    #@1b
    .line 2443
    add-int/lit16 v2, v0, 0x85

    #@1d
    div-int/lit16 v2, v2, 0x86

    #@1f
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@21
    .line 2445
    iget v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@23
    mul-int/lit16 v2, v2, 0x86

    #@25
    sub-int/2addr v2, v0

    #@26
    div-int/lit8 v2, v2, 0x2

    #@28
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@2a
    .line 2451
    :goto_2a
    const/4 v2, 0x3

    #@2b
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@2d
    .line 2453
    .end local v0           #octets:I
    :cond_2d
    return-object v1

    #@2e
    .line 2448
    .restart local v0       #octets:I
    :cond_2e
    const/4 v2, 0x1

    #@2f
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@31
    .line 2449
    rsub-int v2, v0, 0x8c

    #@33
    div-int/lit8 v2, v2, 0x2

    #@35
    iput v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@37
    goto :goto_2a
.end method

.method public static calculateLength(Ljava/lang/CharSequence;ZZ)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 15
    .parameter "msgBody"
    .parameter "use7bitOnly"
    .parameter "useUserInterface"

    #@0
    .prologue
    .line 2495
    const/4 v10, 0x0

    #@1
    const-string v11, "KREncodingScheme"

    #@3
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v10

    #@7
    const/4 v11, 0x1

    #@8
    if-ne v10, v11, :cond_125

    #@a
    .line 2496
    new-instance v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@c
    invoke-direct {v7}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@f
    .line 2498
    .local v7, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    if-nez p1, :cond_5e

    #@11
    const/4 v10, 0x1

    #@12
    :goto_12
    :try_start_12
    invoke-static {p0, v10}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptetsEx(Ljava/lang/CharSequence;Z)[I

    #@15
    move-result-object v5

    #@16
    .line 2499
    .local v5, params:[I
    const/4 v10, 0x0

    #@17
    aget v6, v5, v10

    #@19
    .line 2502
    .local v6, septets:I
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isCountCharIndexInsteadOfSeptets()Z

    #@1c
    move-result v10

    #@1d
    if-eqz v10, :cond_60

    #@1f
    .line 2503
    const/4 v10, 0x1

    #@20
    aget v0, v5, v10

    #@22
    .line 2504
    .local v0, charindex:I
    sget v10, Landroid/telephony/SmsMessage;->LIMIT_USER_DATA_SEPTETS:I

    #@24
    if-le v6, v10, :cond_63

    #@26
    .line 2505
    new-instance v10, Lcom/android/internal/telephony/EncodeException;

    #@28
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2b
    move-result-object v11

    #@2c
    invoke-direct {v10, v11}, Lcom/android/internal/telephony/EncodeException;-><init>(Ljava/lang/String;)V

    #@2f
    throw v10
    :try_end_30
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_12 .. :try_end_30} :catch_30

    #@30
    .line 2522
    .end local v0           #charindex:I
    .end local v5           #params:[I
    .end local v6           #septets:I
    :catch_30
    move-exception v2

    #@31
    .line 2523
    .local v2, ex:Lcom/android/internal/telephony/EncodeException;
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isKSC5601Encoding()Z

    #@34
    move-result v10

    #@35
    const/4 v11, 0x1

    #@36
    if-ne v10, v11, :cond_b6

    #@38
    .line 2525
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    .line 2527
    .local v3, message:Ljava/lang/String;
    :try_start_3c
    const-string v10, "euc-kr"

    #@3e
    invoke-virtual {v3, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_41
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3c .. :try_end_41} :catch_87
    .catch Ljava/lang/RuntimeException; {:try_start_3c .. :try_end_41} :catch_9a

    #@41
    move-result-object v9

    #@42
    .line 2545
    .local v9, textPart:[B
    array-length v8, v9

    #@43
    .line 2546
    .local v8, textLen:I
    iput v8, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@45
    .line 2548
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@47
    if-le v8, v10, :cond_ad

    #@49
    .line 2549
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@4b
    div-int v10, v8, v10

    #@4d
    add-int/lit8 v10, v10, 0x1

    #@4f
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@51
    .line 2550
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@53
    sget v11, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@55
    rem-int v11, v8, v11

    #@57
    sub-int/2addr v10, v11

    #@58
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@5a
    .line 2555
    :goto_5a
    const/4 v10, 0x2

    #@5b
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@5d
    .line 2588
    .end local v2           #ex:Lcom/android/internal/telephony/EncodeException;
    .end local v3           #message:Ljava/lang/String;
    .end local v7           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .end local v8           #textLen:I
    .end local v9           #textPart:[B
    :goto_5d
    return-object v7

    #@5e
    .line 2498
    .restart local v7       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_5e
    const/4 v10, 0x0

    #@5f
    goto :goto_12

    #@60
    .line 2508
    .restart local v5       #params:[I
    .restart local v6       #septets:I
    :cond_60
    const/4 v10, 0x0

    #@61
    :try_start_61
    aget v0, v5, v10

    #@63
    .line 2512
    .restart local v0       #charindex:I
    :cond_63
    iput v0, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@65
    .line 2513
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_SEPTETS_EX:I

    #@67
    if-le v0, v10, :cond_7e

    #@69
    .line 2514
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_SEPTETS_EX:I

    #@6b
    div-int v10, v0, v10

    #@6d
    add-int/lit8 v10, v10, 0x1

    #@6f
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@71
    .line 2515
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_SEPTETS_EX:I

    #@73
    sget v11, Landroid/telephony/SmsMessage;->MAX_USER_DATA_SEPTETS_EX:I

    #@75
    rem-int v11, v0, v11

    #@77
    sub-int/2addr v10, v11

    #@78
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@7a
    .line 2521
    :goto_7a
    const/4 v10, 0x1

    #@7b
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@7d
    goto :goto_5d

    #@7e
    .line 2518
    :cond_7e
    const/4 v10, 0x1

    #@7f
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@81
    .line 2519
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_SEPTETS_EX:I

    #@83
    sub-int/2addr v10, v0

    #@84
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I
    :try_end_86
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_61 .. :try_end_86} :catch_30

    #@86
    goto :goto_7a

    #@87
    .line 2528
    .end local v0           #charindex:I
    .end local v5           #params:[I
    .end local v6           #septets:I
    .restart local v2       #ex:Lcom/android/internal/telephony/EncodeException;
    .restart local v3       #message:Ljava/lang/String;
    :catch_87
    move-exception v1

    #@88
    .line 2530
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    const-string v10, "calculateLength(), Implausible UnsupportedEncodingException "

    #@8a
    invoke-static {v10, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8d
    .line 2531
    const/4 v10, 0x0

    #@8e
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@90
    .line 2532
    const/4 v10, 0x0

    #@91
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@93
    .line 2533
    const/4 v10, 0x0

    #@94
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@96
    .line 2534
    const/4 v10, 0x1

    #@97
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@99
    goto :goto_5d

    #@9a
    .line 2536
    .end local v1           #e:Ljava/io/UnsupportedEncodingException;
    :catch_9a
    move-exception v1

    #@9b
    .line 2537
    .local v1, e:Ljava/lang/RuntimeException;
    const-string v10, "calculateLength(), Implausible RuntimeException "

    #@9d
    invoke-static {v10, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a0
    .line 2538
    const/4 v10, 0x0

    #@a1
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@a3
    .line 2539
    const/4 v10, 0x0

    #@a4
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@a6
    .line 2540
    const/4 v10, 0x0

    #@a7
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@a9
    .line 2541
    const/4 v10, 0x1

    #@aa
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@ac
    goto :goto_5d

    #@ad
    .line 2552
    .end local v1           #e:Ljava/lang/RuntimeException;
    .restart local v8       #textLen:I
    .restart local v9       #textPart:[B
    :cond_ad
    const/4 v10, 0x1

    #@ae
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@b0
    .line 2553
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@b2
    sub-int/2addr v10, v8

    #@b3
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@b5
    goto :goto_5a

    #@b6
    .line 2557
    .end local v3           #message:Ljava/lang/String;
    .end local v8           #textLen:I
    .end local v9           #textPart:[B
    :cond_b6
    new-instance v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@b8
    .end local v7           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    invoke-direct {v7}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@bb
    .line 2559
    .restart local v7       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    const/4 v10, 0x0

    #@bc
    const-string v11, "countLengthBytes"

    #@be
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c1
    move-result v10

    #@c2
    const/4 v11, 0x1

    #@c3
    if-ne v10, v11, :cond_f6

    #@c5
    .line 2560
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@c8
    move-result v10

    #@c9
    mul-int/lit8 v4, v10, 0x2

    #@cb
    .line 2561
    .local v4, octets:I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@ce
    move-result v10

    #@cf
    mul-int/lit8 v10, v10, 0x2

    #@d1
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@d3
    .line 2563
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@d5
    if-le v4, v10, :cond_ed

    #@d7
    .line 2564
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@d9
    div-int v10, v4, v10

    #@db
    add-int/lit8 v10, v10, 0x1

    #@dd
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@df
    .line 2565
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@e1
    sget v11, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@e3
    rem-int v11, v4, v11

    #@e5
    sub-int/2addr v10, v11

    #@e6
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@e8
    .line 2583
    :goto_e8
    const/4 v10, 0x3

    #@e9
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@eb
    goto/16 :goto_5d

    #@ed
    .line 2567
    :cond_ed
    const/4 v10, 0x1

    #@ee
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@f0
    .line 2568
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@f2
    sub-int/2addr v10, v4

    #@f3
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@f5
    goto :goto_e8

    #@f6
    .line 2572
    .end local v4           #octets:I
    :cond_f6
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@f9
    move-result v10

    #@fa
    mul-int/lit8 v4, v10, 0x2

    #@fc
    .line 2573
    .restart local v4       #octets:I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@ff
    move-result v10

    #@100
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@102
    .line 2575
    const/16 v10, 0x8c

    #@104
    if-le v4, v10, :cond_11a

    #@106
    .line 2576
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@108
    div-int v10, v4, v10

    #@10a
    add-int/lit8 v10, v10, 0x1

    #@10c
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@10e
    .line 2577
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@110
    sget v11, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@112
    rem-int v11, v4, v11

    #@114
    sub-int/2addr v10, v11

    #@115
    div-int/lit8 v10, v10, 0x2

    #@117
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@119
    goto :goto_e8

    #@11a
    .line 2579
    :cond_11a
    const/4 v10, 0x1

    #@11b
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@11d
    .line 2580
    sget v10, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@11f
    sub-int/2addr v10, v4

    #@120
    div-int/lit8 v10, v10, 0x2

    #@122
    iput v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@124
    goto :goto_e8

    #@125
    .line 2588
    .end local v2           #ex:Lcom/android/internal/telephony/EncodeException;
    .end local v4           #octets:I
    .end local v7           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_125
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@128
    move-result-object v7

    #@129
    goto/16 :goto_5d
.end method

.method public static calculateLengthHeaderReplyaddressEx(Ljava/lang/CharSequence;I)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 14
    .parameter "msgBody"
    .parameter "dataCodingScheme"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 2689
    new-instance v1, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;

    #@3
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@6
    move-result-object v10

    #@7
    invoke-direct {v1, p1, v10}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;-><init>(ILjava/lang/String;)V

    #@a
    .line 2690
    .local v1, dcs:Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getEncodingType()I

    #@d
    move-result v2

    #@e
    .line 2692
    .local v2, encodingType:I
    new-instance v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@10
    invoke-direct {v6}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@13
    .line 2693
    .local v6, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    packed-switch v2, :pswitch_data_12a

    #@16
    .line 2747
    :try_start_16
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@19
    move-result v10

    #@1a
    mul-int/lit8 v4, v10, 0x2

    #@1c
    .line 2748
    .local v4, octets:I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@1f
    move-result v10

    #@20
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@22
    .line 2749
    const/16 v10, 0x8c

    #@24
    if-le v4, v10, :cond_fa

    #@26
    .line 2750
    div-int/lit8 v10, v4, 0x7d

    #@28
    add-int/lit8 v10, v10, 0x1

    #@2a
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@2c
    .line 2751
    rem-int/lit8 v10, v4, 0x7d

    #@2e
    rsub-int/lit8 v10, v10, 0x7d

    #@30
    div-int/lit8 v10, v10, 0x2

    #@32
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@34
    .line 2757
    :goto_34
    const/4 v10, 0x3

    #@35
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I
    :try_end_37
    .catch Ljava/lang/RuntimeException; {:try_start_16 .. :try_end_37} :catch_105

    #@37
    .line 2764
    .end local v4           #octets:I
    .end local v6           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :goto_37
    return-object v6

    #@38
    .line 2696
    .restart local v6       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :pswitch_38
    const/4 v10, 0x0

    #@39
    :try_start_39
    invoke-static {p0, v10}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptets(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@3c
    move-result-object v7

    #@3d
    .line 2698
    .local v7, tedGsm7bit:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    if-nez v7, :cond_46

    #@3f
    .line 2699
    const-string v10, "calculateLengthHeaderReplyaddressEx(), countGsmSeptets return is null"

    #@41
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@44
    move-object v6, v9

    #@45
    .line 2700
    goto :goto_37

    #@46
    .line 2703
    :cond_46
    iget v5, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@48
    .line 2705
    .local v5, septets:I
    iput v5, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@4a
    .line 2706
    const/16 v10, 0x8e

    #@4c
    if-le v5, v10, :cond_81

    #@4e
    .line 2707
    div-int/lit16 v10, v5, 0x8e

    #@50
    add-int/lit8 v10, v10, 0x1

    #@52
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@54
    .line 2708
    rem-int/lit16 v10, v5, 0x8e

    #@56
    rsub-int v10, v10, 0x8e

    #@58
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@5a
    .line 2714
    :goto_5a
    const/4 v10, 0x1

    #@5b
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I
    :try_end_5d
    .catch Ljava/lang/RuntimeException; {:try_start_39 .. :try_end_5d} :catch_5e

    #@5d
    goto :goto_37

    #@5e
    .line 2716
    .end local v5           #septets:I
    .end local v7           #tedGsm7bit:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :catch_5e
    move-exception v3

    #@5f
    .line 2717
    .local v3, ex:Ljava/lang/RuntimeException;
    new-instance v10, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v11, "calculateLengthHeaderReplyaddressEx(), encodingType: "

    #@66
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v10

    #@6a
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v10

    #@6e
    const-string v11, ", "

    #@70
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v10

    #@74
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v10

    #@78
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v10

    #@7c
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@7f
    move-object v6, v9

    #@80
    .line 2718
    goto :goto_37

    #@81
    .line 2711
    .end local v3           #ex:Ljava/lang/RuntimeException;
    .restart local v5       #septets:I
    .restart local v7       #tedGsm7bit:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_81
    const/4 v10, 0x1

    #@82
    :try_start_82
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@84
    .line 2712
    rsub-int v10, v5, 0x8e

    #@86
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I
    :try_end_88
    .catch Ljava/lang/RuntimeException; {:try_start_82 .. :try_end_88} :catch_5e

    #@88
    goto :goto_5a

    #@89
    .line 2723
    .end local v5           #septets:I
    .end local v7           #tedGsm7bit:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :pswitch_89
    :try_start_89
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8c
    move-result-object v10

    #@8d
    const-string v11, "euc-kr"

    #@8f
    invoke-virtual {v10, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@92
    move-result-object v8

    #@93
    .line 2724
    .local v8, textPart:[B
    array-length v0, v8

    #@94
    .line 2726
    .local v0, byteCount:I
    iput v0, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@96
    .line 2727
    const/16 v10, 0x7d

    #@98
    if-le v0, v10, :cond_ce

    #@9a
    .line 2728
    div-int/lit8 v10, v0, 0x7d

    #@9c
    add-int/lit8 v10, v10, 0x1

    #@9e
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@a0
    .line 2729
    rem-int/lit8 v10, v0, 0x7d

    #@a2
    rsub-int/lit8 v10, v10, 0x7d

    #@a4
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@a6
    .line 2735
    :goto_a6
    const/4 v10, 0x2

    #@a7
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I
    :try_end_a9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_89 .. :try_end_a9} :catch_aa
    .catch Ljava/lang/RuntimeException; {:try_start_89 .. :try_end_a9} :catch_d6

    #@a9
    goto :goto_37

    #@aa
    .line 2737
    .end local v0           #byteCount:I
    .end local v8           #textPart:[B
    :catch_aa
    move-exception v3

    #@ab
    .line 2738
    .local v3, ex:Ljava/io/UnsupportedEncodingException;
    new-instance v10, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v11, "calculateLengthHeaderReplyaddressEx(), encodingType: "

    #@b2
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v10

    #@b6
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v10

    #@ba
    const-string v11, ", "

    #@bc
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v10

    #@c0
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v10

    #@c4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v10

    #@c8
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@cb
    move-object v6, v9

    #@cc
    .line 2739
    goto/16 :goto_37

    #@ce
    .line 2732
    .end local v3           #ex:Ljava/io/UnsupportedEncodingException;
    .restart local v0       #byteCount:I
    .restart local v8       #textPart:[B
    :cond_ce
    const/4 v10, 0x1

    #@cf
    :try_start_cf
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@d1
    .line 2733
    rsub-int/lit8 v10, v0, 0x7d

    #@d3
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I
    :try_end_d5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_cf .. :try_end_d5} :catch_aa
    .catch Ljava/lang/RuntimeException; {:try_start_cf .. :try_end_d5} :catch_d6

    #@d5
    goto :goto_a6

    #@d6
    .line 2740
    .end local v0           #byteCount:I
    .end local v8           #textPart:[B
    :catch_d6
    move-exception v3

    #@d7
    .line 2741
    .local v3, ex:Ljava/lang/RuntimeException;
    new-instance v10, Ljava/lang/StringBuilder;

    #@d9
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@dc
    const-string v11, "calculateLengthHeaderReplyaddressEx(), encodingType: "

    #@de
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v10

    #@e2
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v10

    #@e6
    const-string v11, ", "

    #@e8
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v10

    #@ec
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v10

    #@f0
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f3
    move-result-object v10

    #@f4
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@f7
    move-object v6, v9

    #@f8
    .line 2742
    goto/16 :goto_37

    #@fa
    .line 2754
    .end local v3           #ex:Ljava/lang/RuntimeException;
    .restart local v4       #octets:I
    :cond_fa
    const/4 v10, 0x1

    #@fb
    :try_start_fb
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@fd
    .line 2755
    rsub-int v10, v4, 0x8c

    #@ff
    div-int/lit8 v10, v10, 0x2

    #@101
    iput v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I
    :try_end_103
    .catch Ljava/lang/RuntimeException; {:try_start_fb .. :try_end_103} :catch_105

    #@103
    goto/16 :goto_34

    #@105
    .line 2759
    .end local v4           #octets:I
    :catch_105
    move-exception v3

    #@106
    .line 2760
    .restart local v3       #ex:Ljava/lang/RuntimeException;
    new-instance v10, Ljava/lang/StringBuilder;

    #@108
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@10b
    const-string v11, "calculateLengthHeaderReplyaddressEx(), encodingType: "

    #@10d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v10

    #@111
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@114
    move-result-object v10

    #@115
    const-string v11, ", "

    #@117
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v10

    #@11b
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v10

    #@11f
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@122
    move-result-object v10

    #@123
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@126
    move-object v6, v9

    #@127
    .line 2761
    goto/16 :goto_37

    #@129
    .line 2693
    nop

    #@12a
    :pswitch_data_12a
    .packed-switch 0x1
        :pswitch_38
        :pswitch_89
    .end packed-switch
.end method

.method public static createDataCodingScheme(Lcom/android/internal/telephony/SmsConstants$MessageClass;ZIIZI)B
    .registers 9
    .parameter "messageclass"
    .parameter "isCompressed"
    .parameter "encodingtype"
    .parameter "msgwatingtype"
    .parameter "msgwaitingactive"
    .parameter "msgwaitingkind"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v1, 0x1

    #@2
    .line 1482
    packed-switch p3, :pswitch_data_12c

    #@5
    .line 1559
    :pswitch_5
    if-ne p3, v1, :cond_11a

    #@7
    .line 1560
    const/16 v0, -0x40

    #@9
    .line 1568
    .local v0, data:B
    :goto_9
    if-ne p4, v1, :cond_126

    #@b
    .line 1569
    or-int/lit8 v1, v0, 0x8

    #@d
    int-to-byte v0, v1

    #@e
    .line 1576
    :goto_e
    int-to-byte v1, p5

    #@f
    and-int/lit8 v1, v1, 0x3

    #@11
    or-int/2addr v1, v0

    #@12
    int-to-byte v0, v1

    #@13
    .line 1579
    :cond_13
    :goto_13
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v2, "createDataCodingScheme(), data = "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@29
    .line 1581
    return v0

    #@2a
    .line 1487
    .end local v0           #data:B
    :pswitch_2a
    if-ne p1, v1, :cond_cc

    #@2c
    .line 1488
    const/16 v0, 0x20

    #@2e
    .line 1493
    .restart local v0       #data:B
    :goto_2e
    new-instance v1, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v2, "createDataCodingScheme(), data = "

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, " for bit 5"

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4a
    .line 1494
    new-instance v1, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v2, "createDataCodingScheme(), encodingtype = "

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v1

    #@59
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v1

    #@5d
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@60
    .line 1497
    sget-object v1, Lcom/android/internal/telephony/SmsConstants$MessageClass;->UNKNOWN:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@62
    if-eq p0, v1, :cond_cf

    #@64
    .line 1498
    or-int/lit8 v1, v0, 0x10

    #@66
    int-to-byte v0, v1

    #@67
    .line 1503
    :goto_67
    new-instance v1, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v2, "createDataCodingScheme(), data = "

    #@6e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v1

    #@72
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v1

    #@76
    const-string v2, " for bit 4"

    #@78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v1

    #@7c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v1

    #@80
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@83
    .line 1506
    if-eqz p2, :cond_8b

    #@85
    .line 1507
    add-int/lit8 v1, p2, -0x1

    #@87
    shl-int/lit8 v1, v1, 0x2

    #@89
    or-int/2addr v1, v0

    #@8a
    int-to-byte v0, v1

    #@8b
    .line 1510
    :cond_8b
    new-instance v1, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v2, "createDataCodingScheme(), data = "

    #@92
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v1

    #@96
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@99
    move-result-object v1

    #@9a
    const-string v2, " for bit 3-2"

    #@9c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v1

    #@a0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v1

    #@a4
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@a7
    .line 1513
    sget-object v1, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_0:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@a9
    if-ne p0, v1, :cond_d3

    #@ab
    .line 1514
    or-int/lit8 v1, v0, 0x0

    #@ad
    int-to-byte v0, v1

    #@ae
    .line 1523
    :cond_ae
    :goto_ae
    new-instance v1, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v2, "createDataCodingScheme(), data = "

    #@b5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v1

    #@b9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v1

    #@bd
    const-string v2, " for bit 1-0"

    #@bf
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v1

    #@c3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v1

    #@c7
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@ca
    goto/16 :goto_13

    #@cc
    .line 1490
    .end local v0           #data:B
    :cond_cc
    const/4 v0, 0x0

    #@cd
    .restart local v0       #data:B
    goto/16 :goto_2e

    #@cf
    .line 1500
    :cond_cf
    or-int/lit8 v1, v0, 0x0

    #@d1
    int-to-byte v0, v1

    #@d2
    goto :goto_67

    #@d3
    .line 1515
    :cond_d3
    sget-object v1, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_1:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@d5
    if-ne p0, v1, :cond_db

    #@d7
    .line 1516
    or-int/lit8 v1, v0, 0x1

    #@d9
    int-to-byte v0, v1

    #@da
    goto :goto_ae

    #@db
    .line 1517
    :cond_db
    sget-object v1, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_2:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@dd
    if-ne p0, v1, :cond_e3

    #@df
    .line 1518
    or-int/lit8 v1, v0, 0x2

    #@e1
    int-to-byte v0, v1

    #@e2
    goto :goto_ae

    #@e3
    .line 1519
    :cond_e3
    sget-object v1, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_3:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@e5
    if-ne p0, v1, :cond_ae

    #@e7
    .line 1520
    or-int/lit8 v1, v0, 0x3

    #@e9
    int-to-byte v0, v1

    #@ea
    goto :goto_ae

    #@eb
    .line 1530
    .end local v0           #data:B
    :pswitch_eb
    const/16 v0, -0x10

    #@ed
    .line 1533
    .restart local v0       #data:B
    if-ne p2, v2, :cond_f2

    #@ef
    .line 1534
    const/16 v1, -0xc

    #@f1
    int-to-byte v0, v1

    #@f2
    .line 1537
    :cond_f2
    sget-object v1, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_0:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@f4
    if-ne p0, v1, :cond_fb

    #@f6
    .line 1538
    or-int/lit8 v1, v0, 0x0

    #@f8
    int-to-byte v0, v1

    #@f9
    goto/16 :goto_13

    #@fb
    .line 1539
    :cond_fb
    sget-object v1, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_1:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@fd
    if-ne p0, v1, :cond_104

    #@ff
    .line 1540
    or-int/lit8 v1, v0, 0x1

    #@101
    int-to-byte v0, v1

    #@102
    goto/16 :goto_13

    #@104
    .line 1541
    :cond_104
    sget-object v1, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_2:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@106
    if-ne p0, v1, :cond_10d

    #@108
    .line 1542
    or-int/lit8 v1, v0, 0x2

    #@10a
    int-to-byte v0, v1

    #@10b
    goto/16 :goto_13

    #@10d
    .line 1543
    :cond_10d
    sget-object v1, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_3:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@10f
    if-ne p0, v1, :cond_13

    #@111
    .line 1544
    or-int/lit8 v1, v0, 0x3

    #@113
    int-to-byte v0, v1

    #@114
    goto/16 :goto_13

    #@116
    .line 1552
    .end local v0           #data:B
    :pswitch_116
    const/16 v0, -0x7c

    #@118
    .line 1554
    .restart local v0       #data:B
    goto/16 :goto_13

    #@11a
    .line 1561
    .end local v0           #data:B
    :cond_11a
    if-ne p3, v2, :cond_122

    #@11c
    if-ne p2, v1, :cond_122

    #@11e
    .line 1562
    const/16 v0, -0x30

    #@120
    .restart local v0       #data:B
    goto/16 :goto_9

    #@122
    .line 1564
    .end local v0           #data:B
    :cond_122
    const/16 v0, -0x20

    #@124
    .restart local v0       #data:B
    goto/16 :goto_9

    #@126
    .line 1571
    :cond_126
    or-int/lit8 v1, v0, 0x0

    #@128
    int-to-byte v0, v1

    #@129
    goto/16 :goto_e

    #@12b
    .line 1482
    nop

    #@12c
    :pswitch_data_12c
    .packed-switch 0x0
        :pswitch_2a
        :pswitch_5
        :pswitch_5
        :pswitch_eb
        :pswitch_116
    .end packed-switch
.end method

.method public static createFromEfRecord(I[B)Lcom/android/internal/telephony/gsm/SmsMessage;
    .registers 9
    .parameter "index"
    .parameter "data"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 584
    :try_start_1
    new-instance v1, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@3
    invoke-direct {v1}, Lcom/android/internal/telephony/gsm/SmsMessage;-><init>()V

    #@6
    .line 586
    .local v1, msg:Lcom/android/internal/telephony/gsm/SmsMessage;
    iput p0, v1, Lcom/android/internal/telephony/SmsMessageBase;->indexOnIcc:I

    #@8
    .line 591
    const/4 v5, 0x0

    #@9
    aget-byte v5, p1, v5

    #@b
    and-int/lit8 v5, v5, 0x1

    #@d
    if-nez v5, :cond_18

    #@f
    .line 592
    const-string v5, "GSM"

    #@11
    const-string v6, "SMS parsing failed: Trying to parse a free record"

    #@13
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    move-object v1, v4

    #@17
    .line 609
    .end local v1           #msg:Lcom/android/internal/telephony/gsm/SmsMessage;
    :goto_17
    return-object v1

    #@18
    .line 596
    .restart local v1       #msg:Lcom/android/internal/telephony/gsm/SmsMessage;
    :cond_18
    const/4 v5, 0x0

    #@19
    aget-byte v5, p1, v5

    #@1b
    and-int/lit8 v5, v5, 0x7

    #@1d
    iput v5, v1, Lcom/android/internal/telephony/SmsMessageBase;->statusOnIcc:I

    #@1f
    .line 599
    array-length v5, p1

    #@20
    add-int/lit8 v3, v5, -0x1

    #@22
    .line 603
    .local v3, size:I
    new-array v2, v3, [B

    #@24
    .line 604
    .local v2, pdu:[B
    const/4 v5, 0x1

    #@25
    const/4 v6, 0x0

    #@26
    invoke-static {p1, v5, v2, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@29
    .line 605
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/gsm/SmsMessage;->parsePdu([B)V
    :try_end_2c
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_2c} :catch_2d

    #@2c
    goto :goto_17

    #@2d
    .line 607
    .end local v1           #msg:Lcom/android/internal/telephony/gsm/SmsMessage;
    .end local v2           #pdu:[B
    .end local v3           #size:I
    :catch_2d
    move-exception v0

    #@2e
    .line 608
    .local v0, ex:Ljava/lang/RuntimeException;
    const-string v5, "GSM"

    #@30
    const-string v6, "SMS PDU parsing failed: "

    #@32
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@35
    move-object v1, v4

    #@36
    .line 609
    goto :goto_17
.end method

.method public static createFromPdu([B)Lcom/android/internal/telephony/gsm/SmsMessage;
    .registers 7
    .parameter "pdu"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 520
    :try_start_1
    new-instance v2, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@3
    invoke-direct {v2}, Lcom/android/internal/telephony/gsm/SmsMessage;-><init>()V

    #@6
    .line 521
    .local v2, msg:Lcom/android/internal/telephony/gsm/SmsMessage;
    invoke-direct {v2, p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->parsePdu([B)V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_9} :catch_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_9} :catch_14

    #@9
    .line 528
    .end local v2           #msg:Lcom/android/internal/telephony/gsm/SmsMessage;
    :goto_9
    return-object v2

    #@a
    .line 523
    :catch_a
    move-exception v1

    #@b
    .line 524
    .local v1, ex:Ljava/lang/RuntimeException;
    const-string v4, "GSM"

    #@d
    const-string v5, "SMS PDU parsing failed: "

    #@f
    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    move-object v2, v3

    #@13
    .line 525
    goto :goto_9

    #@14
    .line 526
    .end local v1           #ex:Ljava/lang/RuntimeException;
    :catch_14
    move-exception v0

    #@15
    .line 527
    .local v0, e:Ljava/lang/OutOfMemoryError;
    const-string v4, "GSM"

    #@17
    const-string v5, "SMS PDU parsing failed with out of memory: "

    #@19
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    move-object v2, v3

    #@1d
    .line 528
    goto :goto_9
.end method

.method private static encodeKR(Ljava/lang/String;[B)[B
    .registers 9
    .parameter "message"
    .parameter "header"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1097
    const-string v3, "euc-kr"

    #@4
    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@7
    move-result-object v1

    #@8
    .line 1100
    .local v1, textPart:[B
    const/4 v3, 0x0

    #@9
    const-string v4, "lgu_gsm_submit_encoding_type"

    #@b
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_23

    #@11
    .line 1101
    new-instance v3, Lcom/android/internal/telephony/uicc/UsimService;

    #@13
    invoke-direct {v3}, Lcom/android/internal/telephony/uicc/UsimService;-><init>()V

    #@16
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/UsimService;->getUsimType()I

    #@19
    move-result v3

    #@1a
    const/4 v4, 0x5

    #@1b
    if-ne v3, v4, :cond_23

    #@1d
    .line 1102
    const-string v3, "ksc5601"

    #@1f
    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@22
    move-result-object v1

    #@23
    .line 1107
    :cond_23
    if-eqz p1, :cond_4b

    #@25
    .line 1109
    array-length v3, p1

    #@26
    array-length v4, v1

    #@27
    add-int/2addr v3, v4

    #@28
    add-int/lit8 v3, v3, 0x1

    #@2a
    new-array v2, v3, [B

    #@2c
    .line 1111
    .local v2, userData:[B
    array-length v3, p1

    #@2d
    int-to-byte v3, v3

    #@2e
    aput-byte v3, v2, v5

    #@30
    .line 1112
    array-length v3, p1

    #@31
    invoke-static {p1, v5, v2, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@34
    .line 1113
    array-length v3, p1

    #@35
    add-int/lit8 v3, v3, 0x1

    #@37
    array-length v4, v1

    #@38
    invoke-static {v1, v5, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3b
    .line 1118
    :goto_3b
    array-length v3, v2

    #@3c
    add-int/lit8 v3, v3, 0x1

    #@3e
    new-array v0, v3, [B

    #@40
    .line 1119
    .local v0, ret:[B
    array-length v3, v2

    #@41
    and-int/lit16 v3, v3, 0xff

    #@43
    int-to-byte v3, v3

    #@44
    aput-byte v3, v0, v5

    #@46
    .line 1120
    array-length v3, v2

    #@47
    invoke-static {v2, v5, v0, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@4a
    .line 1121
    return-object v0

    #@4b
    .line 1116
    .end local v0           #ret:[B
    .end local v2           #userData:[B
    :cond_4b
    move-object v2, v1

    #@4c
    .restart local v2       #userData:[B
    goto :goto_3b
.end method

.method private static encodeUCS2(Ljava/lang/String;[B)[B
    .registers 9
    .parameter "message"
    .parameter "header"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1073
    const-string v3, "utf-16be"

    #@4
    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@7
    move-result-object v1

    #@8
    .line 1075
    .local v1, textPart:[B
    if-eqz p1, :cond_30

    #@a
    .line 1077
    array-length v3, p1

    #@b
    array-length v4, v1

    #@c
    add-int/2addr v3, v4

    #@d
    add-int/lit8 v3, v3, 0x1

    #@f
    new-array v2, v3, [B

    #@11
    .line 1079
    .local v2, userData:[B
    array-length v3, p1

    #@12
    int-to-byte v3, v3

    #@13
    aput-byte v3, v2, v5

    #@15
    .line 1080
    array-length v3, p1

    #@16
    invoke-static {p1, v5, v2, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@19
    .line 1081
    array-length v3, p1

    #@1a
    add-int/lit8 v3, v3, 0x1

    #@1c
    array-length v4, v1

    #@1d
    invoke-static {v1, v5, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@20
    .line 1086
    :goto_20
    array-length v3, v2

    #@21
    add-int/lit8 v3, v3, 0x1

    #@23
    new-array v0, v3, [B

    #@25
    .line 1087
    .local v0, ret:[B
    array-length v3, v2

    #@26
    and-int/lit16 v3, v3, 0xff

    #@28
    int-to-byte v3, v3

    #@29
    aput-byte v3, v0, v5

    #@2b
    .line 1088
    array-length v3, v2

    #@2c
    invoke-static {v2, v5, v0, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2f
    .line 1089
    return-object v0

    #@30
    .line 1084
    .end local v0           #ret:[B
    .end local v2           #userData:[B
    :cond_30
    move-object v2, v1

    #@31
    .restart local v2       #userData:[B
    goto :goto_20
.end method

.method public static getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BI)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    .registers 18
    .parameter "scAddress"
    .parameter "originatorAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "time"
    .parameter "header"
    .parameter "encodingtype"

    #@0
    .prologue
    .line 1671
    const/4 v8, 0x0

    #@1
    const/4 v9, 0x0

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move v3, p3

    #@6
    move-wide v4, p4

    #@7
    move-object/from16 v6, p6

    #@9
    move/from16 v7, p7

    #@b
    invoke-static/range {v0 .. v9}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public static getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    .registers 23
    .parameter "scAddress"
    .parameter "originatorAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "time"
    .parameter "header"
    .parameter "encodingtype"
    .parameter "languageTable"
    .parameter "languageShiftTable"

    #@0
    .prologue
    .line 1688
    if-eqz p2, :cond_4

    #@2
    if-nez p1, :cond_6

    #@4
    .line 1689
    :cond_4
    const/4 v6, 0x0

    #@5
    .line 1809
    :goto_5
    return-object v6

    #@6
    .line 1692
    :cond_6
    new-instance v6, Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@8
    invoke-direct {v6}, Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;-><init>()V

    #@b
    .line 1695
    .local v6, ret:Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    if-eqz p6, :cond_32

    #@d
    const/16 v10, 0x40

    #@f
    :goto_f
    or-int/lit8 v10, v10, 0x0

    #@11
    int-to-byte v5, v10

    #@12
    .line 1697
    .local v5, mtiByte:B
    move/from16 v0, p3

    #@14
    invoke-static {p0, p1, v5, v0, v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPduHead(Ljava/lang/String;Ljava/lang/String;BZLcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;)Ljava/io/ByteArrayOutputStream;

    #@17
    move-result-object v3

    #@18
    .line 1700
    .local v3, bo:Ljava/io/ByteArrayOutputStream;
    const/4 v10, 0x1

    #@19
    move/from16 v0, p7

    #@1b
    if-ne v0, v10, :cond_af

    #@1d
    .line 1703
    :try_start_1d
    move-object/from16 v0, p6

    #@1f
    move/from16 v1, p8

    #@21
    move/from16 v2, p9

    #@23
    invoke-static {p2, v0, v1, v2}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPackedWithHeader(Ljava/lang/String;[BII)[B

    #@26
    move-result-object v9

    #@27
    .line 1705
    .local v9, userData:[B
    const/4 v10, 0x0

    #@28
    aget-byte v10, v9, v10

    #@2a
    and-int/lit16 v10, v10, 0xff

    #@2c
    const/16 v11, 0xa0

    #@2e
    if-le v10, v11, :cond_34

    #@30
    .line 1707
    const/4 v6, 0x0

    #@31
    goto :goto_5

    #@32
    .line 1695
    .end local v3           #bo:Ljava/io/ByteArrayOutputStream;
    .end local v5           #mtiByte:B
    .end local v9           #userData:[B
    :cond_32
    const/4 v10, 0x0

    #@33
    goto :goto_f

    #@34
    .line 1712
    .restart local v3       #bo:Ljava/io/ByteArrayOutputStream;
    .restart local v5       #mtiByte:B
    .restart local v9       #userData:[B
    :cond_34
    const/4 v10, 0x0

    #@35
    invoke-virtual {v3, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@38
    .line 1715
    const/4 v10, 0x0

    #@39
    const-string v11, "dcm_copytosim_localtimezone"

    #@3b
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3e
    move-result v10

    #@3f
    if-eqz v10, :cond_52

    #@41
    .line 1716
    move-wide/from16 v0, p4

    #@43
    invoke-static {v0, v1, v3}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPduSCTS_TimeZone(JLjava/io/ByteArrayOutputStream;)V

    #@46
    .line 1721
    :goto_46
    const/4 v10, 0x0

    #@47
    array-length v11, v9

    #@48
    invoke-virtual {v3, v9, v10, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_4b
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_1d .. :try_end_4b} :catch_58

    #@4b
    .line 1808
    :goto_4b
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@4e
    move-result-object v10

    #@4f
    iput-object v10, v6, Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;->encodedMessage:[B

    #@51
    goto :goto_5

    #@52
    .line 1719
    :cond_52
    :try_start_52
    move-wide/from16 v0, p4

    #@54
    invoke-static {v0, v1, v3}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPduSCTS(JLjava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;
    :try_end_57
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_52 .. :try_end_57} :catch_58

    #@57
    goto :goto_46

    #@58
    .line 1722
    .end local v9           #userData:[B
    :catch_58
    move-exception v4

    #@59
    .line 1728
    .local v4, ex:Lcom/android/internal/telephony/EncodeException;
    :try_start_59
    const-string v10, "utf-16be"

    #@5b
    invoke-virtual {p2, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_5e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_59 .. :try_end_5e} :catch_81

    #@5e
    move-result-object v7

    #@5f
    .line 1734
    .local v7, textPart:[B
    if-eqz p6, :cond_8a

    #@61
    .line 1735
    move-object/from16 v0, p6

    #@63
    array-length v10, v0

    #@64
    array-length v11, v7

    #@65
    add-int/2addr v10, v11

    #@66
    new-array v9, v10, [B

    #@68
    .line 1736
    .restart local v9       #userData:[B
    const/4 v10, 0x0

    #@69
    const/4 v11, 0x0

    #@6a
    move-object/from16 v0, p6

    #@6c
    array-length v12, v0

    #@6d
    move-object/from16 v0, p6

    #@6f
    invoke-static {v0, v10, v9, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@72
    .line 1737
    const/4 v10, 0x0

    #@73
    move-object/from16 v0, p6

    #@75
    array-length v11, v0

    #@76
    array-length v12, v7

    #@77
    invoke-static {v7, v10, v9, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@7a
    .line 1742
    :goto_7a
    array-length v10, v9

    #@7b
    const/16 v11, 0x8c

    #@7d
    if-le v10, v11, :cond_8c

    #@7f
    .line 1744
    const/4 v6, 0x0

    #@80
    goto :goto_5

    #@81
    .line 1729
    .end local v7           #textPart:[B
    .end local v9           #userData:[B
    :catch_81
    move-exception v8

    #@82
    .line 1730
    .local v8, uex:Ljava/io/UnsupportedEncodingException;
    const-string v10, "getDeliverPdu(), Implausible UnsupportedEncodingException "

    #@84
    invoke-static {v10, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@87
    .line 1731
    const/4 v6, 0x0

    #@88
    goto/16 :goto_5

    #@8a
    .line 1739
    .end local v8           #uex:Ljava/io/UnsupportedEncodingException;
    .restart local v7       #textPart:[B
    :cond_8a
    move-object v9, v7

    #@8b
    .restart local v9       #userData:[B
    goto :goto_7a

    #@8c
    .line 1748
    :cond_8c
    const/16 v10, 0x8

    #@8e
    invoke-virtual {v3, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@91
    .line 1751
    move-wide/from16 v0, p4

    #@93
    invoke-static {v0, v1, v3}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPduSCTS(JLjava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;

    #@96
    .line 1754
    if-eqz p6, :cond_aa

    #@98
    .line 1755
    array-length v10, v9

    #@99
    add-int/lit8 v10, v10, 0x1

    #@9b
    invoke-virtual {v3, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@9e
    .line 1756
    move-object/from16 v0, p6

    #@a0
    array-length v10, v0

    #@a1
    invoke-virtual {v3, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@a4
    .line 1760
    :goto_a4
    const/4 v10, 0x0

    #@a5
    array-length v11, v9

    #@a6
    invoke-virtual {v3, v9, v10, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@a9
    goto :goto_4b

    #@aa
    .line 1758
    :cond_aa
    array-length v10, v9

    #@ab
    invoke-virtual {v3, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@ae
    goto :goto_a4

    #@af
    .line 1768
    .end local v4           #ex:Lcom/android/internal/telephony/EncodeException;
    .end local v7           #textPart:[B
    .end local v9           #userData:[B
    :cond_af
    :try_start_af
    const-string v10, "utf-16be"

    #@b1
    invoke-virtual {p2, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_b4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_af .. :try_end_b4} :catch_d8

    #@b4
    move-result-object v7

    #@b5
    .line 1774
    .restart local v7       #textPart:[B
    if-eqz p6, :cond_e1

    #@b7
    .line 1775
    move-object/from16 v0, p6

    #@b9
    array-length v10, v0

    #@ba
    array-length v11, v7

    #@bb
    add-int/2addr v10, v11

    #@bc
    new-array v9, v10, [B

    #@be
    .line 1776
    .restart local v9       #userData:[B
    const/4 v10, 0x0

    #@bf
    const/4 v11, 0x0

    #@c0
    move-object/from16 v0, p6

    #@c2
    array-length v12, v0

    #@c3
    move-object/from16 v0, p6

    #@c5
    invoke-static {v0, v10, v9, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@c8
    .line 1777
    const/4 v10, 0x0

    #@c9
    move-object/from16 v0, p6

    #@cb
    array-length v11, v0

    #@cc
    array-length v12, v7

    #@cd
    invoke-static {v7, v10, v9, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@d0
    .line 1782
    :goto_d0
    array-length v10, v9

    #@d1
    const/16 v11, 0x8c

    #@d3
    if-le v10, v11, :cond_e3

    #@d5
    .line 1784
    const/4 v6, 0x0

    #@d6
    goto/16 :goto_5

    #@d8
    .line 1769
    .end local v7           #textPart:[B
    .end local v9           #userData:[B
    :catch_d8
    move-exception v8

    #@d9
    .line 1770
    .restart local v8       #uex:Ljava/io/UnsupportedEncodingException;
    const-string v10, "getDeliverPdu(), Implausible UnsupportedEncodingException "

    #@db
    invoke-static {v10, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@de
    .line 1771
    const/4 v6, 0x0

    #@df
    goto/16 :goto_5

    #@e1
    .line 1779
    .end local v8           #uex:Ljava/io/UnsupportedEncodingException;
    .restart local v7       #textPart:[B
    :cond_e1
    move-object v9, v7

    #@e2
    .restart local v9       #userData:[B
    goto :goto_d0

    #@e3
    .line 1788
    :cond_e3
    const/16 v10, 0x8

    #@e5
    invoke-virtual {v3, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@e8
    .line 1792
    const/4 v10, 0x0

    #@e9
    const-string v11, "dcm_copytosim_localtimezone"

    #@eb
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@ee
    move-result v10

    #@ef
    if-eqz v10, :cond_10b

    #@f1
    .line 1793
    move-wide/from16 v0, p4

    #@f3
    invoke-static {v0, v1, v3}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPduSCTS_TimeZone(JLjava/io/ByteArrayOutputStream;)V

    #@f6
    .line 1799
    :goto_f6
    if-eqz p6, :cond_111

    #@f8
    .line 1800
    array-length v10, v9

    #@f9
    add-int/lit8 v10, v10, 0x1

    #@fb
    invoke-virtual {v3, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@fe
    .line 1801
    move-object/from16 v0, p6

    #@100
    array-length v10, v0

    #@101
    invoke-virtual {v3, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@104
    .line 1806
    :goto_104
    const/4 v10, 0x0

    #@105
    array-length v11, v9

    #@106
    invoke-virtual {v3, v9, v10, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@109
    goto/16 :goto_4b

    #@10b
    .line 1796
    :cond_10b
    move-wide/from16 v0, p4

    #@10d
    invoke-static {v0, v1, v3}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPduSCTS(JLjava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;

    #@110
    goto :goto_f6

    #@111
    .line 1803
    :cond_111
    array-length v10, v9

    #@112
    invoke-virtual {v3, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@115
    goto :goto_104
.end method

.method private static getDeliverPduHead(Ljava/lang/String;Ljava/lang/String;BZILcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;)Ljava/io/ByteArrayOutputStream;
    .registers 12
    .parameter "scAddress"
    .parameter "originationAddress"
    .parameter "mtiByte"
    .parameter "statusReportRequested"
    .parameter "protocolId"
    .parameter "ret"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2067
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@3
    const/16 v2, 0xb4

    #@5
    invoke-direct {v0, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@8
    .line 2071
    .local v0, bo:Ljava/io/ByteArrayOutputStream;
    if-nez p0, :cond_2c

    #@a
    .line 2072
    const/4 v2, 0x0

    #@b
    iput-object v2, p5, Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;->encodedScAddress:[B

    #@d
    .line 2079
    :goto_d
    if-eqz p3, :cond_17

    #@f
    .line 2081
    or-int/lit8 v2, p2, 0x20

    #@11
    int-to-byte p2, v2

    #@12
    .line 2082
    const-string v2, "getDeliverPduHead(), SMS status report requested"

    #@14
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@17
    .line 2084
    :cond_17
    invoke-virtual {v0, p2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1a
    .line 2088
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->KRsmsnetworkPortionToCalledPartyBCD(Ljava/lang/String;)[B

    #@1d
    move-result-object v1

    #@1e
    .line 2091
    .local v1, daBytes:[B
    if-nez v1, :cond_33

    #@20
    .line 2092
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@23
    .line 2093
    const/16 v2, 0x81

    #@25
    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@28
    .line 2106
    :goto_28
    invoke-virtual {v0, p4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2b
    .line 2107
    return-object v0

    #@2c
    .line 2074
    .end local v1           #daBytes:[B
    :cond_2c
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCDWithLength(Ljava/lang/String;)[B

    #@2f
    move-result-object v2

    #@30
    iput-object v2, p5, Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;->encodedScAddress:[B

    #@32
    goto :goto_d

    #@33
    .line 2098
    .restart local v1       #daBytes:[B
    :cond_33
    array-length v2, v1

    #@34
    add-int/lit8 v2, v2, -0x1

    #@36
    mul-int/lit8 v4, v2, 0x2

    #@38
    array-length v2, v1

    #@39
    add-int/lit8 v2, v2, -0x1

    #@3b
    aget-byte v2, v1, v2

    #@3d
    and-int/lit16 v2, v2, 0xf0

    #@3f
    const/16 v5, 0xf0

    #@41
    if-ne v2, v5, :cond_4e

    #@43
    const/4 v2, 0x1

    #@44
    :goto_44
    sub-int v2, v4, v2

    #@46
    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@49
    .line 2102
    array-length v2, v1

    #@4a
    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@4d
    goto :goto_28

    #@4e
    :cond_4e
    move v2, v3

    #@4f
    .line 2098
    goto :goto_44
.end method

.method private static getDeliverPduHead(Ljava/lang/String;Ljava/lang/String;BZLcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;)Ljava/io/ByteArrayOutputStream;
    .registers 15
    .parameter "scAddress"
    .parameter "originatorAddress"
    .parameter "mtiByte"
    .parameter "statusReportRequested"
    .parameter "ret"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 1995
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@4
    const/16 v7, 0xb4

    #@6
    invoke-direct {v0, v7}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@9
    .line 1998
    .local v0, bo:Ljava/io/ByteArrayOutputStream;
    if-nez p0, :cond_4e

    #@b
    .line 1999
    const/4 v7, 0x0

    #@c
    iput-object v7, p4, Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;->encodedScAddress:[B

    #@e
    .line 2005
    :goto_e
    if-eqz p3, :cond_18

    #@10
    .line 2007
    or-int/lit8 v7, p2, 0x20

    #@12
    int-to-byte p2, v7

    #@13
    .line 2008
    const-string v7, "getDeliverPduHead(), SMS status report requested"

    #@15
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@18
    .line 2010
    :cond_18
    invoke-virtual {v0, p2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1b
    .line 2013
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCD(Ljava/lang/String;)[B

    #@1e
    move-result-object v3

    #@1f
    .line 2016
    .local v3, oaBytes:[B
    if-nez v3, :cond_71

    #@21
    .line 2017
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@24
    move-result v5

    #@25
    if-nez v5, :cond_4a

    #@27
    .line 2020
    const/4 v5, 0x0

    #@28
    const/4 v7, 0x0

    #@29
    :try_start_29
    invoke-static {p1, v5, v7}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPacked(Ljava/lang/String;II)[B

    #@2c
    move-result-object v2

    #@2d
    .line 2022
    .local v2, gsm7BitPackedAddress:[B
    array-length v5, v2

    #@2e
    add-int/lit8 v5, v5, -0x1

    #@30
    new-array v4, v5, [B

    #@32
    .line 2023
    .local v4, readGsm7BitPackedAddress:[B
    const/4 v5, 0x1

    #@33
    const/4 v7, 0x0

    #@34
    array-length v8, v2

    #@35
    add-int/lit8 v8, v8, -0x1

    #@37
    invoke-static {v2, v5, v4, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3a
    .line 2025
    array-length v5, v4

    #@3b
    mul-int/lit8 v5, v5, 0x2

    #@3d
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@40
    .line 2026
    const/16 v5, 0xd0

    #@42
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@45
    .line 2027
    const/4 v5, 0x0

    #@46
    array-length v7, v4

    #@47
    invoke-virtual {v0, v4, v5, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_4a
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_29 .. :try_end_4a} :catch_55

    #@4a
    .line 2043
    .end local v2           #gsm7BitPackedAddress:[B
    .end local v4           #readGsm7BitPackedAddress:[B
    :cond_4a
    :goto_4a
    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@4d
    .line 2044
    return-object v0

    #@4e
    .line 2001
    .end local v3           #oaBytes:[B
    :cond_4e
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCDWithLength(Ljava/lang/String;)[B

    #@51
    move-result-object v7

    #@52
    iput-object v7, p4, Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;->encodedScAddress:[B

    #@54
    goto :goto_e

    #@55
    .line 2028
    .restart local v3       #oaBytes:[B
    :catch_55
    move-exception v1

    #@56
    .line 2029
    .local v1, e:Lcom/android/internal/telephony/EncodeException;
    new-instance v5, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v7, "getDeliverPduHead(), "

    #@5d
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v1}, Lcom/android/internal/telephony/EncodeException;->getMessage()Ljava/lang/String;

    #@64
    move-result-object v7

    #@65
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@70
    goto :goto_4a

    #@71
    .line 2035
    .end local v1           #e:Lcom/android/internal/telephony/EncodeException;
    :cond_71
    array-length v7, v3

    #@72
    add-int/lit8 v7, v7, -0x1

    #@74
    mul-int/lit8 v7, v7, 0x2

    #@76
    array-length v8, v3

    #@77
    add-int/lit8 v8, v8, -0x1

    #@79
    aget-byte v8, v3, v8

    #@7b
    and-int/lit16 v8, v8, 0xf0

    #@7d
    const/16 v9, 0xf0

    #@7f
    if-ne v8, v9, :cond_8b

    #@81
    :goto_81
    sub-int v5, v7, v5

    #@83
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@86
    .line 2039
    array-length v5, v3

    #@87
    invoke-virtual {v0, v3, v6, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@8a
    goto :goto_4a

    #@8b
    :cond_8b
    move v5, v6

    #@8c
    .line 2035
    goto :goto_81
.end method

.method private static getDeliverPduSCTS(JLjava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;
    .registers 14
    .parameter "msgtime"
    .parameter "byteoutput"

    #@0
    .prologue
    .line 1932
    new-instance v5, Landroid/text/format/Time;

    #@2
    const-string v9, "UTC"

    #@4
    invoke-direct {v5, v9}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@7
    .line 1933
    .local v5, sctstime:Landroid/text/format/Time;
    invoke-virtual {v5, p0, p1}, Landroid/text/format/Time;->set(J)V

    #@a
    .line 1935
    iget v9, v5, Landroid/text/format/Time;->year:I

    #@c
    const/16 v10, 0x7d0

    #@e
    if-lt v9, v10, :cond_4f

    #@10
    iget v9, v5, Landroid/text/format/Time;->year:I

    #@12
    add-int/lit16 v7, v9, -0x7d0

    #@14
    .line 1936
    .local v7, year:I
    :goto_14
    iget v9, v5, Landroid/text/format/Time;->month:I

    #@16
    add-int/lit8 v3, v9, 0x1

    #@18
    .line 1940
    .local v3, month:I
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@1b
    move-result v8

    #@1c
    .line 1941
    .local v8, yearByte:B
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@1f
    move-result v4

    #@20
    .line 1942
    .local v4, monthByte:B
    iget v9, v5, Landroid/text/format/Time;->monthDay:I

    #@22
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@25
    move-result v0

    #@26
    .line 1943
    .local v0, dayByte:B
    iget v9, v5, Landroid/text/format/Time;->hour:I

    #@28
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@2b
    move-result v1

    #@2c
    .line 1944
    .local v1, hourByte:B
    iget v9, v5, Landroid/text/format/Time;->minute:I

    #@2e
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@31
    move-result v2

    #@32
    .line 1945
    .local v2, minuteByte:B
    iget v9, v5, Landroid/text/format/Time;->second:I

    #@34
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@37
    move-result v6

    #@38
    .line 1949
    .local v6, secondByte:B
    invoke-virtual {p2, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@3b
    .line 1950
    invoke-virtual {p2, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@3e
    .line 1951
    invoke-virtual {p2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@41
    .line 1952
    invoke-virtual {p2, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@44
    .line 1953
    invoke-virtual {p2, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@47
    .line 1954
    invoke-virtual {p2, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@4a
    .line 1955
    const/4 v9, 0x0

    #@4b
    invoke-virtual {p2, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@4e
    .line 1957
    return-object p2

    #@4f
    .line 1935
    .end local v0           #dayByte:B
    .end local v1           #hourByte:B
    .end local v2           #minuteByte:B
    .end local v3           #month:I
    .end local v4           #monthByte:B
    .end local v6           #secondByte:B
    .end local v7           #year:I
    .end local v8           #yearByte:B
    :cond_4f
    iget v9, v5, Landroid/text/format/Time;->year:I

    #@51
    add-int/lit16 v7, v9, -0x76c

    #@53
    goto :goto_14
.end method

.method private static getDeliverPduSCTS_TimeZone(JLjava/io/ByteArrayOutputStream;)V
    .registers 9
    .parameter "msgtime"
    .parameter "outStream"

    #@0
    .prologue
    .line 1972
    new-instance v2, Landroid/text/format/Time;

    #@2
    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    #@5
    .line 1973
    .local v2, sctstime:Landroid/text/format/Time;
    invoke-virtual {v2, p0, p1}, Landroid/text/format/Time;->set(J)V

    #@8
    .line 1974
    iget-object v4, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    #@a
    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@d
    move-result-object v0

    #@e
    .line 1976
    .local v0, mMyTimeZone:Ljava/util/TimeZone;
    iget v4, v2, Landroid/text/format/Time;->year:I

    #@10
    const/16 v5, 0x7d0

    #@12
    if-lt v4, v5, :cond_60

    #@14
    iget v4, v2, Landroid/text/format/Time;->year:I

    #@16
    add-int/lit16 v3, v4, -0x7d0

    #@18
    .line 1978
    .local v3, year:I
    :goto_18
    iget v4, v2, Landroid/text/format/Time;->month:I

    #@1a
    add-int/lit8 v1, v4, 0x1

    #@1c
    .line 1981
    .local v1, month:I
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@1f
    move-result v4

    #@20
    invoke-virtual {p2, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@23
    .line 1982
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@26
    move-result v4

    #@27
    invoke-virtual {p2, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2a
    .line 1983
    iget v4, v2, Landroid/text/format/Time;->monthDay:I

    #@2c
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@2f
    move-result v4

    #@30
    invoke-virtual {p2, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@33
    .line 1984
    iget v4, v2, Landroid/text/format/Time;->hour:I

    #@35
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@38
    move-result v4

    #@39
    invoke-virtual {p2, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@3c
    .line 1985
    iget v4, v2, Landroid/text/format/Time;->minute:I

    #@3e
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@41
    move-result v4

    #@42
    invoke-virtual {p2, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@45
    .line 1986
    iget v4, v2, Landroid/text/format/Time;->second:I

    #@47
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@4a
    move-result v4

    #@4b
    invoke-virtual {p2, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@4e
    .line 1987
    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    #@51
    move-result v4

    #@52
    const v5, 0x36ee80

    #@55
    div-int/2addr v4, v5

    #@56
    mul-int/lit8 v4, v4, 0x4

    #@58
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@5b
    move-result v4

    #@5c
    invoke-virtual {p2, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@5f
    .line 1988
    return-void

    #@60
    .line 1976
    .end local v1           #month:I
    .end local v3           #year:I
    :cond_60
    iget v4, v2, Landroid/text/format/Time;->year:I

    #@62
    add-int/lit16 v3, v4, -0x76c

    #@64
    goto :goto_18
.end method

.method private static getSCAddressTemp()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1207
    const-string v0, ""

    #@4
    .line 1209
    .local v0, mSCAddress:Ljava/lang/String;
    const-string v4, "addSCAddress"

    #@6
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9
    move-result v4

    #@a
    if-ne v4, v6, :cond_cd

    #@c
    .line 1211
    :try_start_c
    const-string v4, "iusiminfo"

    #@e
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@11
    move-result-object v4

    #@12
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@15
    move-result-object v1

    #@16
    .line 1212
    .local v1, mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;
    if-eqz v1, :cond_1c

    #@18
    .line 1213
    invoke-interface {v1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->getSCAddressTemp()Ljava/lang/String;
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_1b} :catch_d0

    #@1b
    move-result-object v0

    #@1c
    .line 1219
    .end local v1           #mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;
    :cond_1c
    :goto_1c
    if-nez v0, :cond_25

    #@1e
    .line 1220
    const-string v4, "getSCAddressTemp(), scaddr = null"

    #@20
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@23
    move-object v0, v3

    #@24
    .line 1250
    .end local v0           #mSCAddress:Ljava/lang/String;
    :goto_24
    return-object v0

    #@25
    .line 1222
    .restart local v0       #mSCAddress:Ljava/lang/String;
    :cond_25
    const-string v4, ""

    #@27
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_b5

    #@2d
    .line 1223
    const-string v4, "addSCAddress"

    #@2f
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@32
    move-result v4

    #@33
    if-ne v4, v6, :cond_b2

    #@35
    .line 1224
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    .line 1225
    .local v2, msisdn:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v5, "getSCAddressTemp(), msisdn = "

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v4

    #@50
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@53
    .line 1226
    if-eqz v2, :cond_aa

    #@55
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@58
    move-result v4

    #@59
    if-eqz v4, :cond_aa

    #@5b
    .line 1228
    const-string v3, "+"

    #@5d
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@60
    move-result v3

    #@61
    if-ne v3, v6, :cond_92

    #@63
    .line 1230
    new-instance v3, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v4, "+82"

    #@6a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v3

    #@6e
    const/4 v4, 0x2

    #@6f
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@72
    move-result-object v4

    #@73
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v0

    #@7b
    .line 1234
    :goto_7b
    new-instance v3, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v4, "getSCAddressTemp(), msisdn = "

    #@82
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v3

    #@8e
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@91
    goto :goto_24

    #@92
    .line 1233
    :cond_92
    new-instance v3, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    const-string v4, "82"

    #@99
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v3

    #@9d
    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@a0
    move-result-object v4

    #@a1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v0

    #@a9
    goto :goto_7b

    #@aa
    .line 1239
    :cond_aa
    const-string v4, "getSCAddressTemp(), msisdn = null"

    #@ac
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@af
    move-object v0, v3

    #@b0
    .line 1240
    goto/16 :goto_24

    #@b2
    .end local v2           #msisdn:Ljava/lang/String;
    :cond_b2
    move-object v0, v3

    #@b3
    .line 1243
    goto/16 :goto_24

    #@b5
    .line 1246
    :cond_b5
    new-instance v3, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v4, "getSCAddressTemp(), mSCAddress = "

    #@bc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v3

    #@c0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v3

    #@c4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v3

    #@c8
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@cb
    goto/16 :goto_24

    #@cd
    :cond_cd
    move-object v0, v3

    #@ce
    .line 1250
    goto/16 :goto_24

    #@d0
    .line 1215
    :catch_d0
    move-exception v4

    #@d1
    goto/16 :goto_1c
.end method

.method public static getStaticDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJII[B)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    .registers 24
    .parameter "scAddress"
    .parameter "originatorAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "time"
    .parameter "protocolId"
    .parameter "dataCodingScheme"
    .parameter "header"

    #@0
    .prologue
    .line 1821
    const-string v2, "getStaticDeliverPdu(),[getStaticDeliverPdu]"

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@5
    .line 1822
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "getStaticDeliverPdu(),scAddress: "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@1b
    .line 1823
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v3, "getStaticDeliverPdu(),originatorAddress: "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    move-object/from16 v0, p1

    #@28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@33
    .line 1824
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, "getStaticDeliverPdu(),message: "

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    move-object/from16 v0, p2

    #@40
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@4b
    .line 1825
    new-instance v2, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v3, "getStaticDeliverPdu(),time: "

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    move-wide/from16 v0, p4

    #@58
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v2

    #@5c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@63
    .line 1826
    new-instance v2, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v3, "getStaticDeliverPdu(),protocolId: "

    #@6a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v2

    #@6e
    move/from16 v0, p6

    #@70
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v2

    #@78
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@7b
    .line 1827
    new-instance v2, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v3, "getStaticDeliverPdu(),header: "

    #@82
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v2

    #@86
    move-object/from16 v0, p8

    #@88
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v2

    #@8c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v2

    #@90
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@93
    .line 1829
    new-instance v9, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;

    #@95
    move/from16 v0, p7

    #@97
    move-object/from16 v1, p2

    #@99
    invoke-direct {v9, v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;-><init>(ILjava/lang/String;)V

    #@9c
    .line 1830
    .local v9, dcs:Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;
    invoke-virtual {v9}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getEncodingType()I

    #@9f
    move-result v10

    #@a0
    .line 1831
    .local v10, encoding:I
    invoke-virtual {v9}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@a3
    move-result v12

    #@a4
    .line 1833
    .local v12, reCalcDataCodingScheme:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string v3, "getStaticDeliverPdu(),[Dcs: "

    #@ab
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v2

    #@af
    invoke-virtual {v9}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->toString()Ljava/lang/String;

    #@b2
    move-result-object v3

    #@b3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v2

    #@b7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v2

    #@bb
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@be
    .line 1834
    new-instance v2, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v3, "getStaticDeliverPdu(),dataCodingScheme: "

    #@c5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v2

    #@c9
    move/from16 v0, p7

    #@cb
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v2

    #@cf
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v2

    #@d3
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@d6
    .line 1835
    new-instance v2, Ljava/lang/StringBuilder;

    #@d8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@db
    const-string v3, "getStaticDeliverPdu(),encoding: "

    #@dd
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v2

    #@e1
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v2

    #@e5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v2

    #@e9
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@ec
    .line 1836
    new-instance v2, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    const-string v3, "getStaticDeliverPdu(),reCalcDataCodingScheme: "

    #@f3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v2

    #@f7
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v2

    #@fb
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fe
    move-result-object v2

    #@ff
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@102
    .line 1838
    if-eqz p1, :cond_10e

    #@104
    const-string v2, "Unknown"

    #@106
    move-object/from16 v0, p1

    #@108
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10b
    move-result v2

    #@10c
    if-eqz v2, :cond_110

    #@10e
    .line 1839
    :cond_10e
    const-string p1, ""

    #@110
    .line 1842
    :cond_110
    if-nez p2, :cond_114

    #@112
    .line 1843
    const-string p2, ""

    #@114
    .line 1846
    :cond_114
    new-instance v7, Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@116
    invoke-direct {v7}, Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;-><init>()V

    #@119
    .line 1849
    .local v7, ret:Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    if-eqz p8, :cond_14b

    #@11b
    const/16 v2, 0x40

    #@11d
    :goto_11d
    or-int/lit8 v2, v2, 0x0

    #@11f
    int-to-byte v4, v2

    #@120
    .local v4, mtiByte:B
    move-object v2, p0

    #@121
    move-object/from16 v3, p1

    #@123
    move/from16 v5, p3

    #@125
    move/from16 v6, p6

    #@127
    .line 1851
    invoke-static/range {v2 .. v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPduHead(Ljava/lang/String;Ljava/lang/String;BZILcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;)Ljava/io/ByteArrayOutputStream;

    #@12a
    move-result-object v8

    #@12b
    .line 1856
    .local v8, bo:Ljava/io/ByteArrayOutputStream;
    if-nez v10, :cond_14d

    #@12d
    .line 1857
    new-instance v2, Ljava/lang/StringBuilder;

    #@12f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@132
    const-string v3, "getStaticDeliverPdu(), ["

    #@134
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v2

    #@138
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v2

    #@13c
    const-string v3, "]: ENCODING_UNKNOWN"

    #@13e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v2

    #@142
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v2

    #@146
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@149
    .line 1858
    const/4 v7, 0x0

    #@14a
    .line 1917
    .end local v7           #ret:Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    :goto_14a
    return-object v7

    #@14b
    .line 1849
    .end local v4           #mtiByte:B
    .end local v8           #bo:Ljava/io/ByteArrayOutputStream;
    .restart local v7       #ret:Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    :cond_14b
    const/4 v2, 0x0

    #@14c
    goto :goto_11d

    #@14d
    .line 1862
    .restart local v4       #mtiByte:B
    .restart local v8       #bo:Ljava/io/ByteArrayOutputStream;
    :cond_14d
    const/4 v2, 0x1

    #@14e
    if-ne v10, v2, :cond_1a8

    #@150
    .line 1863
    if-nez p8, :cond_199

    #@152
    const/4 v2, 0x0

    #@153
    move v3, v2

    #@154
    :goto_154
    if-nez p8, :cond_1a1

    #@156
    const/4 v2, 0x0

    #@157
    :goto_157
    const/4 v5, 0x0

    #@158
    :try_start_158
    move-object/from16 v0, p2

    #@15a
    move-object/from16 v1, p8

    #@15c
    invoke-static {v0, v1, v3, v2, v5}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPackedWithHeader(Ljava/lang/String;[BIIZ)[B
    :try_end_15f
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_158 .. :try_end_15f} :catch_205

    #@15f
    move-result-object v14

    #@160
    .line 1887
    .local v14, userData:[B
    :goto_160
    const/4 v2, 0x1

    #@161
    if-ne v10, v2, :cond_23e

    #@163
    .line 1888
    const/4 v2, 0x0

    #@164
    aget-byte v2, v14, v2

    #@166
    and-int/lit16 v2, v2, 0xff

    #@168
    const/16 v3, 0xa0

    #@16a
    if-le v2, v3, :cond_229

    #@16c
    .line 1890
    new-instance v2, Ljava/lang/StringBuilder;

    #@16e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@171
    const-string v3, "getStaticDeliverPdu(), ["

    #@173
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v2

    #@177
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v2

    #@17b
    const-string v3, "]: Message too long("

    #@17d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v2

    #@181
    const/4 v3, 0x0

    #@182
    aget-byte v3, v14, v3

    #@184
    and-int/lit16 v3, v3, 0xff

    #@186
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@189
    move-result-object v2

    #@18a
    const-string v3, ")"

    #@18c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v2

    #@190
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@193
    move-result-object v2

    #@194
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@197
    .line 1891
    const/4 v7, 0x0

    #@198
    goto :goto_14a

    #@199
    .line 1863
    .end local v14           #userData:[B
    :cond_199
    :try_start_199
    invoke-static/range {p8 .. p8}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@19c
    move-result-object v2

    #@19d
    iget v2, v2, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@19f
    move v3, v2

    #@1a0
    goto :goto_154

    #@1a1
    :cond_1a1
    invoke-static/range {p8 .. p8}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@1a4
    move-result-object v2

    #@1a5
    iget v2, v2, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I
    :try_end_1a7
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_199 .. :try_end_1a7} :catch_205

    #@1a7
    goto :goto_157

    #@1a8
    .line 1867
    :cond_1a8
    const/4 v2, 0x2

    #@1a9
    if-ne v10, v2, :cond_1d8

    #@1ab
    .line 1869
    :try_start_1ab
    move-object/from16 v0, p2

    #@1ad
    move-object/from16 v1, p8

    #@1af
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeKR(Ljava/lang/String;[B)[B
    :try_end_1b2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1ab .. :try_end_1b2} :catch_1b4
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_1ab .. :try_end_1b2} :catch_205

    #@1b2
    move-result-object v14

    #@1b3
    .restart local v14       #userData:[B
    goto :goto_160

    #@1b4
    .line 1870
    .end local v14           #userData:[B
    :catch_1b4
    move-exception v13

    #@1b5
    .line 1871
    .local v13, uex:Ljava/io/UnsupportedEncodingException;
    :try_start_1b5
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1ba
    const-string v3, "getStaticDeliverPdu(), ["

    #@1bc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v2

    #@1c0
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c3
    move-result-object v2

    #@1c4
    const-string v3, "]: "

    #@1c6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v2

    #@1ca
    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1cd
    move-result-object v2

    #@1ce
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d1
    move-result-object v2

    #@1d2
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_1d5
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_1b5 .. :try_end_1d5} :catch_205

    #@1d5
    .line 1872
    const/4 v7, 0x0

    #@1d6
    goto/16 :goto_14a

    #@1d8
    .line 1876
    .end local v13           #uex:Ljava/io/UnsupportedEncodingException;
    :cond_1d8
    :try_start_1d8
    move-object/from16 v0, p2

    #@1da
    move-object/from16 v1, p8

    #@1dc
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeUCS2(Ljava/lang/String;[B)[B
    :try_end_1df
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1d8 .. :try_end_1df} :catch_1e1
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_1d8 .. :try_end_1df} :catch_205

    #@1df
    move-result-object v14

    #@1e0
    .restart local v14       #userData:[B
    goto :goto_160

    #@1e1
    .line 1877
    .end local v14           #userData:[B
    :catch_1e1
    move-exception v13

    #@1e2
    .line 1878
    .restart local v13       #uex:Ljava/io/UnsupportedEncodingException;
    :try_start_1e2
    new-instance v2, Ljava/lang/StringBuilder;

    #@1e4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e7
    const-string v3, "getStaticDeliverPdu(), ["

    #@1e9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ec
    move-result-object v2

    #@1ed
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f0
    move-result-object v2

    #@1f1
    const-string v3, "]: "

    #@1f3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v2

    #@1f7
    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v2

    #@1fb
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fe
    move-result-object v2

    #@1ff
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_202
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_1e2 .. :try_end_202} :catch_205

    #@202
    .line 1879
    const/4 v7, 0x0

    #@203
    goto/16 :goto_14a

    #@205
    .line 1882
    .end local v13           #uex:Ljava/io/UnsupportedEncodingException;
    :catch_205
    move-exception v11

    #@206
    .line 1883
    .local v11, ex:Lcom/android/internal/telephony/EncodeException;
    new-instance v2, Ljava/lang/StringBuilder;

    #@208
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@20b
    const-string v3, "getStaticDeliverPdu(), ["

    #@20d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v2

    #@211
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@214
    move-result-object v2

    #@215
    const-string v3, "]: "

    #@217
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v2

    #@21b
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21e
    move-result-object v2

    #@21f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@222
    move-result-object v2

    #@223
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@226
    .line 1884
    const/4 v7, 0x0

    #@227
    goto/16 :goto_14a

    #@229
    .line 1893
    .end local v11           #ex:Lcom/android/internal/telephony/EncodeException;
    .restart local v14       #userData:[B
    :cond_229
    invoke-virtual {v8, v12}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@22c
    .line 1912
    :goto_22c
    move-wide/from16 v0, p4

    #@22e
    invoke-static {v0, v1, v8}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPduSCTS_TimeZone(JLjava/io/ByteArrayOutputStream;)V

    #@231
    .line 1915
    const/4 v2, 0x0

    #@232
    array-length v3, v14

    #@233
    invoke-virtual {v8, v14, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@236
    .line 1916
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@239
    move-result-object v2

    #@23a
    iput-object v2, v7, Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;->encodedMessage:[B

    #@23c
    goto/16 :goto_14a

    #@23e
    .line 1894
    :cond_23e
    const/4 v2, 0x2

    #@23f
    if-ne v10, v2, :cond_27c

    #@241
    .line 1895
    const/4 v2, 0x0

    #@242
    aget-byte v2, v14, v2

    #@244
    and-int/lit16 v2, v2, 0xff

    #@246
    const/16 v3, 0x8c

    #@248
    if-le v2, v3, :cond_278

    #@24a
    .line 1897
    new-instance v2, Ljava/lang/StringBuilder;

    #@24c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24f
    const-string v3, "getStaticDeliverPdu(), ["

    #@251
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@254
    move-result-object v2

    #@255
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@258
    move-result-object v2

    #@259
    const-string v3, "]: Message too long("

    #@25b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25e
    move-result-object v2

    #@25f
    const/4 v3, 0x0

    #@260
    aget-byte v3, v14, v3

    #@262
    and-int/lit16 v3, v3, 0xff

    #@264
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@267
    move-result-object v2

    #@268
    const-string v3, ")"

    #@26a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26d
    move-result-object v2

    #@26e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@271
    move-result-object v2

    #@272
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@275
    .line 1898
    const/4 v7, 0x0

    #@276
    goto/16 :goto_14a

    #@278
    .line 1901
    :cond_278
    invoke-virtual {v8, v12}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@27b
    goto :goto_22c

    #@27c
    .line 1903
    :cond_27c
    const/4 v2, 0x0

    #@27d
    aget-byte v2, v14, v2

    #@27f
    and-int/lit16 v2, v2, 0xff

    #@281
    const/16 v3, 0x8c

    #@283
    if-le v2, v3, :cond_2b3

    #@285
    .line 1905
    new-instance v2, Ljava/lang/StringBuilder;

    #@287
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28a
    const-string v3, "getStaticDeliverPdu(), ["

    #@28c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28f
    move-result-object v2

    #@290
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@293
    move-result-object v2

    #@294
    const-string v3, "]: Message too long("

    #@296
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@299
    move-result-object v2

    #@29a
    const/4 v3, 0x0

    #@29b
    aget-byte v3, v14, v3

    #@29d
    and-int/lit16 v3, v3, 0xff

    #@29f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a2
    move-result-object v2

    #@2a3
    const-string v3, ")"

    #@2a5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a8
    move-result-object v2

    #@2a9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ac
    move-result-object v2

    #@2ad
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@2b0
    .line 1906
    const/4 v7, 0x0

    #@2b1
    goto/16 :goto_14a

    #@2b3
    .line 1909
    :cond_2b3
    invoke-virtual {v8, v12}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2b6
    goto/16 :goto_22c
.end method

.method public static getStaticSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .registers 20
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "header"
    .parameter "dataCodingScheme"
    .parameter "protocolId"

    #@0
    .prologue
    .line 955
    new-instance v2, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;

    #@2
    move/from16 v0, p5

    #@4
    invoke-direct {v2, v0, p2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;-><init>(ILjava/lang/String;)V

    #@7
    .line 956
    .local v2, dcs:Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getEncodingType()I

    #@a
    move-result v3

    #@b
    .line 957
    .local v3, encoding:I
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@e
    move-result v6

    #@f
    .line 959
    .local v6, reCalcDataCodingScheme:I
    const-string v10, "getStaticSubmitPdu(),[getStaticSubmitPdu]"

    #@11
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@14
    .line 960
    new-instance v10, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v11, "getStaticSubmitPdu(),[Dcs: "

    #@1b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v10

    #@1f
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->toString()Ljava/lang/String;

    #@22
    move-result-object v11

    #@23
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v10

    #@27
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v10

    #@2b
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@2e
    .line 961
    new-instance v10, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v11, "getStaticSubmitPdu(),scAddress: "

    #@35
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v10

    #@39
    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v10

    #@3d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v10

    #@41
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@44
    .line 962
    new-instance v10, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v11, "getStaticSubmitPdu(),destinationAddress: "

    #@4b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v10

    #@4f
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v10

    #@53
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v10

    #@57
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@5a
    .line 963
    new-instance v10, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v11, "getStaticSubmitPdu(),message: "

    #@61
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v10

    #@65
    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v10

    #@69
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v10

    #@6d
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@70
    .line 964
    new-instance v10, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v11, "getStaticSubmitPdu(),statusReportRequested: "

    #@77
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v10

    #@7b
    move/from16 v0, p3

    #@7d
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@80
    move-result-object v10

    #@81
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v10

    #@85
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@88
    .line 965
    new-instance v10, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v11, "getStaticSubmitPdu(),header: "

    #@8f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v10

    #@93
    move-object/from16 v0, p4

    #@95
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v10

    #@99
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v10

    #@9d
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@a0
    .line 966
    new-instance v10, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v11, "getStaticSubmitPdu(),dataCodingScheme: "

    #@a7
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v10

    #@ab
    move/from16 v0, p5

    #@ad
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v10

    #@b1
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v10

    #@b5
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@b8
    .line 967
    new-instance v10, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v11, "getStaticSubmitPdu(),reCalcDataCodingScheme: "

    #@bf
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v10

    #@c3
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v10

    #@c7
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v10

    #@cb
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@ce
    .line 968
    new-instance v10, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v11, "getStaticSubmitPdu(),encoding: "

    #@d5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v10

    #@d9
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v10

    #@dd
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v10

    #@e1
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@e4
    .line 969
    new-instance v10, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    const-string v11, "getStaticSubmitPdu(),protocolId: "

    #@eb
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v10

    #@ef
    move/from16 v0, p6

    #@f1
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v10

    #@f5
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f8
    move-result-object v10

    #@f9
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@fc
    .line 972
    if-nez p1, :cond_100

    #@fe
    .line 973
    const/4 v7, 0x0

    #@ff
    .line 1060
    :goto_ff
    return-object v7

    #@100
    .line 976
    :cond_100
    const-string v10, "Unknown"

    #@102
    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@105
    move-result v10

    #@106
    const/4 v11, 0x1

    #@107
    if-ne v10, v11, :cond_10b

    #@109
    .line 977
    const-string p1, ""

    #@10b
    .line 980
    :cond_10b
    if-nez p2, :cond_10f

    #@10d
    .line 981
    const-string p2, ""

    #@10f
    .line 984
    :cond_10f
    new-instance v7, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@111
    invoke-direct {v7}, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;-><init>()V

    #@114
    .line 986
    .local v7, ret:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz p4, :cond_141

    #@116
    const/16 v10, 0x40

    #@118
    :goto_118
    or-int/lit8 v10, v10, 0x1

    #@11a
    int-to-byte v5, v10

    #@11b
    .line 987
    .local v5, mtiByte:B
    move/from16 v0, p3

    #@11d
    invoke-static {p0, p1, v5, v0, v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPduHead(Ljava/lang/String;Ljava/lang/String;BZLcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;)Ljava/io/ByteArrayOutputStream;

    #@120
    move-result-object v1

    #@121
    .line 991
    .local v1, bo:Ljava/io/ByteArrayOutputStream;
    if-nez v3, :cond_143

    #@123
    .line 992
    new-instance v10, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v11, "getStaticSubmitPdu(), ["

    #@12a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v10

    #@12e
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@131
    move-result-object v10

    #@132
    const-string v11, "]: ENCODING_UNKNOWN"

    #@134
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v10

    #@138
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13b
    move-result-object v10

    #@13c
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@13f
    .line 993
    const/4 v7, 0x0

    #@140
    goto :goto_ff

    #@141
    .line 986
    .end local v1           #bo:Ljava/io/ByteArrayOutputStream;
    .end local v5           #mtiByte:B
    :cond_141
    const/4 v10, 0x0

    #@142
    goto :goto_118

    #@143
    .line 997
    .restart local v1       #bo:Ljava/io/ByteArrayOutputStream;
    .restart local v5       #mtiByte:B
    :cond_143
    const/4 v10, 0x1

    #@144
    if-ne v3, v10, :cond_19d

    #@146
    .line 998
    if-nez p4, :cond_18e

    #@148
    const/4 v10, 0x0

    #@149
    move v11, v10

    #@14a
    :goto_14a
    if-nez p4, :cond_196

    #@14c
    const/4 v10, 0x0

    #@14d
    :goto_14d
    const/4 v12, 0x0

    #@14e
    :try_start_14e
    move-object/from16 v0, p4

    #@150
    invoke-static {p2, v0, v11, v10, v12}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPackedWithHeader(Ljava/lang/String;[BIIZ)[B
    :try_end_153
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_14e .. :try_end_153} :catch_1f6

    #@153
    move-result-object v9

    #@154
    .line 1022
    .local v9, userData:[B
    :goto_154
    const/4 v10, 0x1

    #@155
    if-ne v3, v10, :cond_22a

    #@157
    .line 1023
    const/4 v10, 0x0

    #@158
    aget-byte v10, v9, v10

    #@15a
    and-int/lit16 v10, v10, 0xff

    #@15c
    const/16 v11, 0xa0

    #@15e
    if-le v10, v11, :cond_21a

    #@160
    .line 1025
    new-instance v10, Ljava/lang/StringBuilder;

    #@162
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@165
    const-string v11, "getStaticSubmitPdu(), ["

    #@167
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v10

    #@16b
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v10

    #@16f
    const-string v11, "]: Message too long("

    #@171
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v10

    #@175
    const/4 v11, 0x0

    #@176
    aget-byte v11, v9, v11

    #@178
    and-int/lit16 v11, v11, 0xff

    #@17a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v10

    #@17e
    const-string v11, ")"

    #@180
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@183
    move-result-object v10

    #@184
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@187
    move-result-object v10

    #@188
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@18b
    .line 1026
    const/4 v7, 0x0

    #@18c
    goto/16 :goto_ff

    #@18e
    .line 998
    .end local v9           #userData:[B
    :cond_18e
    :try_start_18e
    invoke-static/range {p4 .. p4}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@191
    move-result-object v10

    #@192
    iget v10, v10, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@194
    move v11, v10

    #@195
    goto :goto_14a

    #@196
    :cond_196
    invoke-static/range {p4 .. p4}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@199
    move-result-object v10

    #@19a
    iget v10, v10, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I
    :try_end_19c
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_18e .. :try_end_19c} :catch_1f6

    #@19c
    goto :goto_14d

    #@19d
    .line 1002
    :cond_19d
    const/4 v10, 0x2

    #@19e
    if-ne v3, v10, :cond_1cb

    #@1a0
    .line 1004
    :try_start_1a0
    move-object/from16 v0, p4

    #@1a2
    invoke-static {p2, v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeKR(Ljava/lang/String;[B)[B
    :try_end_1a5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1a0 .. :try_end_1a5} :catch_1a7
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_1a0 .. :try_end_1a5} :catch_1f6

    #@1a5
    move-result-object v9

    #@1a6
    .restart local v9       #userData:[B
    goto :goto_154

    #@1a7
    .line 1005
    .end local v9           #userData:[B
    :catch_1a7
    move-exception v8

    #@1a8
    .line 1006
    .local v8, uex:Ljava/io/UnsupportedEncodingException;
    :try_start_1a8
    new-instance v10, Ljava/lang/StringBuilder;

    #@1aa
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1ad
    const-string v11, "getStaticSubmitPdu(), ["

    #@1af
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v10

    #@1b3
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b6
    move-result-object v10

    #@1b7
    const-string v11, "]: "

    #@1b9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v10

    #@1bd
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c0
    move-result-object v10

    #@1c1
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c4
    move-result-object v10

    #@1c5
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_1c8
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_1a8 .. :try_end_1c8} :catch_1f6

    #@1c8
    .line 1007
    const/4 v7, 0x0

    #@1c9
    goto/16 :goto_ff

    #@1cb
    .line 1011
    .end local v8           #uex:Ljava/io/UnsupportedEncodingException;
    :cond_1cb
    :try_start_1cb
    move-object/from16 v0, p4

    #@1cd
    invoke-static {p2, v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeUCS2(Ljava/lang/String;[B)[B
    :try_end_1d0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1cb .. :try_end_1d0} :catch_1d2
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_1cb .. :try_end_1d0} :catch_1f6

    #@1d0
    move-result-object v9

    #@1d1
    .restart local v9       #userData:[B
    goto :goto_154

    #@1d2
    .line 1012
    .end local v9           #userData:[B
    :catch_1d2
    move-exception v8

    #@1d3
    .line 1013
    .restart local v8       #uex:Ljava/io/UnsupportedEncodingException;
    :try_start_1d3
    new-instance v10, Ljava/lang/StringBuilder;

    #@1d5
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1d8
    const-string v11, "getStaticSubmitPdu(), ["

    #@1da
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dd
    move-result-object v10

    #@1de
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e1
    move-result-object v10

    #@1e2
    const-string v11, "]: "

    #@1e4
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e7
    move-result-object v10

    #@1e8
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v10

    #@1ec
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ef
    move-result-object v10

    #@1f0
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_1f3
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_1d3 .. :try_end_1f3} :catch_1f6

    #@1f3
    .line 1014
    const/4 v7, 0x0

    #@1f4
    goto/16 :goto_ff

    #@1f6
    .line 1017
    .end local v8           #uex:Ljava/io/UnsupportedEncodingException;
    :catch_1f6
    move-exception v4

    #@1f7
    .line 1018
    .local v4, ex:Lcom/android/internal/telephony/EncodeException;
    new-instance v10, Ljava/lang/StringBuilder;

    #@1f9
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1fc
    const-string v11, "getStaticSubmitPdu(), ["

    #@1fe
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@201
    move-result-object v10

    #@202
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@205
    move-result-object v10

    #@206
    const-string v11, "]: "

    #@208
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20b
    move-result-object v10

    #@20c
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20f
    move-result-object v10

    #@210
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@213
    move-result-object v10

    #@214
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@217
    .line 1019
    const/4 v7, 0x0

    #@218
    goto/16 :goto_ff

    #@21a
    .line 1036
    .end local v4           #ex:Lcom/android/internal/telephony/EncodeException;
    .restart local v9       #userData:[B
    :cond_21a
    invoke-virtual {v1, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@21d
    .line 1058
    :goto_21d
    const/4 v10, 0x0

    #@21e
    array-length v11, v9

    #@21f
    invoke-virtual {v1, v9, v10, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@222
    .line 1059
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@225
    move-result-object v10

    #@226
    iput-object v10, v7, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@228
    goto/16 :goto_ff

    #@22a
    .line 1037
    :cond_22a
    const/4 v10, 0x2

    #@22b
    if-ne v3, v10, :cond_268

    #@22d
    .line 1038
    const/4 v10, 0x0

    #@22e
    aget-byte v10, v9, v10

    #@230
    and-int/lit16 v10, v10, 0xff

    #@232
    const/16 v11, 0x8c

    #@234
    if-le v10, v11, :cond_264

    #@236
    .line 1040
    new-instance v10, Ljava/lang/StringBuilder;

    #@238
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@23b
    const-string v11, "getStaticSubmitPdu(), ["

    #@23d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@240
    move-result-object v10

    #@241
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@244
    move-result-object v10

    #@245
    const-string v11, "]: Message too long("

    #@247
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24a
    move-result-object v10

    #@24b
    const/4 v11, 0x0

    #@24c
    aget-byte v11, v9, v11

    #@24e
    and-int/lit16 v11, v11, 0xff

    #@250
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@253
    move-result-object v10

    #@254
    const-string v11, ")"

    #@256
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@259
    move-result-object v10

    #@25a
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25d
    move-result-object v10

    #@25e
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@261
    .line 1041
    const/4 v7, 0x0

    #@262
    goto/16 :goto_ff

    #@264
    .line 1045
    :cond_264
    invoke-virtual {v1, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@267
    goto :goto_21d

    #@268
    .line 1047
    :cond_268
    const/4 v10, 0x0

    #@269
    aget-byte v10, v9, v10

    #@26b
    and-int/lit16 v10, v10, 0xff

    #@26d
    const/16 v11, 0x8c

    #@26f
    if-le v10, v11, :cond_29f

    #@271
    .line 1049
    new-instance v10, Ljava/lang/StringBuilder;

    #@273
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@276
    const-string v11, "getStaticSubmitPdu(), ["

    #@278
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27b
    move-result-object v10

    #@27c
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27f
    move-result-object v10

    #@280
    const-string v11, "]: Message too long("

    #@282
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@285
    move-result-object v10

    #@286
    const/4 v11, 0x0

    #@287
    aget-byte v11, v9, v11

    #@289
    and-int/lit16 v11, v11, 0xff

    #@28b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28e
    move-result-object v10

    #@28f
    const-string v11, ")"

    #@291
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@294
    move-result-object v10

    #@295
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@298
    move-result-object v10

    #@299
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@29c
    .line 1050
    const/4 v7, 0x0

    #@29d
    goto/16 :goto_ff

    #@29f
    .line 1054
    :cond_29f
    invoke-virtual {v1, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2a2
    goto/16 :goto_21d
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;I[BZ)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .registers 13
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "destinationPort"
    .parameter "data"
    .parameter "statusReportRequested"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1156
    new-instance v1, Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@3
    invoke-direct {v1}, Lcom/android/internal/telephony/SmsHeader$PortAddrs;-><init>()V

    #@6
    .line 1157
    .local v1, portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    iput p2, v1, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@8
    .line 1158
    iput v7, v1, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->origPort:I

    #@a
    .line 1159
    iput-boolean v7, v1, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->areEightBits:Z

    #@c
    .line 1161
    new-instance v3, Lcom/android/internal/telephony/SmsHeader;

    #@e
    invoke-direct {v3}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@11
    .line 1162
    .local v3, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    iput-object v1, v3, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@13
    .line 1164
    invoke-static {v3}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@16
    move-result-object v4

    #@17
    .line 1166
    .local v4, smsHeaderData:[B
    array-length v5, p3

    #@18
    array-length v6, v4

    #@19
    add-int/2addr v5, v6

    #@1a
    add-int/lit8 v5, v5, 0x1

    #@1c
    const/16 v6, 0x8c

    #@1e
    if-le v5, v6, :cond_45

    #@20
    .line 1167
    const-string v5, "GSM"

    #@22
    new-instance v6, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v7, "SMS data message may only contain "

    #@29
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v6

    #@2d
    array-length v7, v4

    #@2e
    rsub-int v7, v7, 0x8c

    #@30
    add-int/lit8 v7, v7, -0x1

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    const-string v7, " bytes"

    #@38
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v6

    #@40
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 1169
    const/4 v2, 0x0

    #@44
    .line 1200
    :goto_44
    return-object v2

    #@45
    .line 1172
    :cond_45
    new-instance v2, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@47
    invoke-direct {v2}, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;-><init>()V

    #@4a
    .line 1173
    .local v2, ret:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    const/16 v5, 0x41

    #@4c
    invoke-static {p0, p1, v5, p4, v2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPduHead(Ljava/lang/String;Ljava/lang/String;BZLcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;)Ljava/io/ByteArrayOutputStream;

    #@4f
    move-result-object v0

    #@50
    .line 1180
    .local v0, bo:Ljava/io/ByteArrayOutputStream;
    const/4 v5, 0x4

    #@51
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@54
    .line 1184
    sget v5, Lcom/android/internal/telephony/gsm/SmsMessage;->vp:I

    #@56
    if-lez v5, :cond_5d

    #@58
    .line 1185
    sget v5, Lcom/android/internal/telephony/gsm/SmsMessage;->vp:I

    #@5a
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@5d
    .line 1190
    :cond_5d
    array-length v5, p3

    #@5e
    array-length v6, v4

    #@5f
    add-int/2addr v5, v6

    #@60
    add-int/lit8 v5, v5, 0x1

    #@62
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@65
    .line 1193
    array-length v5, v4

    #@66
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@69
    .line 1194
    array-length v5, v4

    #@6a
    invoke-virtual {v0, v4, v7, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@6d
    .line 1197
    array-length v5, p3

    #@6e
    invoke-virtual {v0, p3, v7, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@71
    .line 1199
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@74
    move-result-object v5

    #@75
    iput-object v5, v2, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@77
    goto :goto_44
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .registers 5
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"

    #@0
    .prologue
    .line 1137
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, p3, v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .registers 21
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "header"

    #@0
    .prologue
    .line 637
    invoke-static {}, Lcom/android/internal/telephony/GsmAlphabet;->setEnabledShiftTablesLG()V

    #@3
    .line 638
    invoke-static {}, Lcom/android/internal/telephony/GsmAlphabet;->getEnabledSingleShiftTablesLG()[I

    #@6
    move-result-object v0

    #@7
    array-length v7, v0

    #@8
    .line 642
    .local v7, singleShiftIndex:I
    if-lez v7, :cond_1b

    #@a
    .line 643
    const/4 v5, 0x0

    #@b
    const/4 v6, 0x0

    #@c
    move-object/from16 v0, p0

    #@e
    move-object/from16 v1, p1

    #@10
    move-object/from16 v2, p2

    #@12
    move/from16 v3, p3

    #@14
    move-object/from16 v4, p4

    #@16
    invoke-static/range {v0 .. v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@19
    move-result-object v0

    #@1a
    .line 647
    :goto_1a
    return-object v0

    #@1b
    :cond_1b
    const/4 v13, 0x0

    #@1c
    const/4 v14, 0x0

    #@1d
    const/4 v15, 0x0

    #@1e
    move-object/from16 v8, p0

    #@20
    move-object/from16 v9, p1

    #@22
    move-object/from16 v10, p2

    #@24
    move/from16 v11, p3

    #@26
    move-object/from16 v12, p4

    #@28
    invoke-static/range {v8 .. v15}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@2b
    move-result-object v0

    #@2c
    goto :goto_1a
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .registers 24
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "header"
    .parameter "encoding"
    .parameter "protocolId"

    #@0
    .prologue
    .line 803
    if-nez p1, :cond_4

    #@2
    .line 804
    const/4 v7, 0x0

    #@3
    .line 944
    :goto_3
    return-object v7

    #@4
    .line 807
    :cond_4
    const-string v2, "Unknown"

    #@6
    move-object/from16 v0, p1

    #@8
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v2

    #@c
    const/4 v3, 0x1

    #@d
    if-ne v2, v3, :cond_11

    #@f
    .line 808
    const-string p1, ""

    #@11
    .line 811
    :cond_11
    if-nez p2, :cond_15

    #@13
    .line 812
    const-string p2, ""

    #@15
    .line 815
    :cond_15
    new-instance v7, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@17
    invoke-direct {v7}, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;-><init>()V

    #@1a
    .line 817
    .local v7, ret:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz p4, :cond_dc

    #@1c
    const/16 v2, 0x40

    #@1e
    :goto_1e
    or-int/lit8 v2, v2, 0x1

    #@20
    int-to-byte v4, v2

    #@21
    .local v4, mtiByte:B
    move-object/from16 v2, p0

    #@23
    move-object/from16 v3, p1

    #@25
    move/from16 v5, p3

    #@27
    move/from16 v6, p6

    #@29
    .line 818
    invoke-static/range {v2 .. v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPduHead(Ljava/lang/String;Ljava/lang/String;BZILcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;)Ljava/io/ByteArrayOutputStream;

    #@2c
    move-result-object v8

    #@2d
    .line 823
    .local v8, bo:Ljava/io/ByteArrayOutputStream;
    if-nez p5, :cond_31

    #@2f
    .line 825
    const/16 p5, 0x1

    #@31
    .line 829
    :cond_31
    const/4 v2, 0x0

    #@32
    const-string v3, "lgu_gsm_submit_encoding_type"

    #@34
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@37
    move-result v2

    #@38
    if-eqz v2, :cond_48

    #@3a
    .line 830
    new-instance v2, Lcom/android/internal/telephony/uicc/UsimService;

    #@3c
    invoke-direct {v2}, Lcom/android/internal/telephony/uicc/UsimService;-><init>()V

    #@3f
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UsimService;->getUsimType()I

    #@42
    move-result v2

    #@43
    const/4 v3, 0x5

    #@44
    if-ne v2, v3, :cond_48

    #@46
    .line 831
    const/16 p5, 0x2

    #@48
    .line 837
    :cond_48
    const/4 v2, 0x1

    #@49
    move/from16 v0, p5

    #@4b
    if-ne v0, v2, :cond_ef

    #@4d
    .line 838
    if-eqz p4, :cond_54

    #@4f
    :try_start_4f
    move-object/from16 v0, p4

    #@51
    array-length v2, v0

    #@52
    if-nez v2, :cond_df

    #@54
    :cond_54
    const/4 v13, 0x0

    #@55
    .line 839
    .local v13, tmpLanguageTable:I
    :goto_55
    if-eqz p4, :cond_5c

    #@57
    move-object/from16 v0, p4

    #@59
    array-length v2, v0

    #@5a
    if-nez v2, :cond_e7

    #@5c
    :cond_5c
    const/4 v12, 0x0

    #@5d
    .line 840
    .local v12, tmpLanguageShiftTable:I
    :goto_5d
    move-object/from16 v0, p2

    #@5f
    move-object/from16 v1, p4

    #@61
    invoke-static {v0, v1, v13, v12}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPackedWithHeader(Ljava/lang/String;[BII)[B

    #@64
    move-result-object v16

    #@65
    .line 843
    .local v16, userData:[B
    const/4 v2, 0x0

    #@66
    const-string v3, "KREncodingScheme"

    #@68
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6b
    move-result v2

    #@6c
    const/4 v3, 0x1

    #@6d
    if-ne v2, v3, :cond_9e

    #@6f
    .line 844
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isCountCharIndexInsteadOfSeptets()Z

    #@72
    move-result v2

    #@73
    if-eqz v2, :cond_9e

    #@75
    .line 845
    const/4 v2, 0x1

    #@76
    move-object/from16 v0, p2

    #@78
    invoke-static {v0, v2}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptets(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@7b
    move-result-object v11

    #@7c
    .line 847
    .local v11, tedGsm7bit:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    if-eqz v11, :cond_9e

    #@7e
    .line 848
    iget v10, v11, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@80
    .line 849
    .local v10, septets:I
    sget v2, Landroid/telephony/SmsMessage;->LIMIT_USER_DATA_SEPTETS:I

    #@82
    if-le v10, v2, :cond_9e

    #@84
    .line 850
    new-instance v2, Lcom/android/internal/telephony/EncodeException;

    #@86
    move-object/from16 v0, p2

    #@88
    invoke-direct {v2, v0}, Lcom/android/internal/telephony/EncodeException;-><init>(Ljava/lang/String;)V

    #@8b
    throw v2
    :try_end_8c
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_4f .. :try_end_8c} :catch_8c

    #@8c
    .line 878
    .end local v10           #septets:I
    .end local v11           #tedGsm7bit:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .end local v12           #tmpLanguageShiftTable:I
    .end local v13           #tmpLanguageTable:I
    .end local v16           #userData:[B
    :catch_8c
    move-exception v9

    #@8d
    .line 880
    .local v9, ex:Lcom/android/internal/telephony/EncodeException;
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isKSC5601Encoding()Z

    #@90
    move-result v2

    #@91
    const/4 v3, 0x1

    #@92
    if-ne v2, v3, :cond_13a

    #@94
    .line 882
    :try_start_94
    move-object/from16 v0, p2

    #@96
    move-object/from16 v1, p4

    #@98
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeKR(Ljava/lang/String;[B)[B
    :try_end_9b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_94 .. :try_end_9b} :catch_124

    #@9b
    move-result-object v16

    #@9c
    .line 883
    .restart local v16       #userData:[B
    const/16 p5, 0x2

    #@9e
    .line 906
    .end local v9           #ex:Lcom/android/internal/telephony/EncodeException;
    :cond_9e
    :goto_9e
    const/4 v2, 0x1

    #@9f
    move/from16 v0, p5

    #@a1
    if-ne v0, v2, :cond_164

    #@a3
    .line 907
    const/4 v2, 0x0

    #@a4
    aget-byte v2, v16, v2

    #@a6
    and-int/lit16 v2, v2, 0xff

    #@a8
    const/16 v3, 0xa0

    #@aa
    if-le v2, v3, :cond_14f

    #@ac
    .line 909
    new-instance v2, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v3, "getSubmitPdu(), ["

    #@b3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v2

    #@b7
    move/from16 v0, p5

    #@b9
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v2

    #@bd
    const-string v3, "]: Message too long("

    #@bf
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v2

    #@c3
    const/4 v3, 0x0

    #@c4
    aget-byte v3, v16, v3

    #@c6
    and-int/lit16 v3, v3, 0xff

    #@c8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v2

    #@cc
    const-string v3, ")"

    #@ce
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v2

    #@d2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v2

    #@d6
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@d9
    .line 910
    const/4 v7, 0x0

    #@da
    goto/16 :goto_3

    #@dc
    .line 817
    .end local v4           #mtiByte:B
    .end local v8           #bo:Ljava/io/ByteArrayOutputStream;
    .end local v16           #userData:[B
    :cond_dc
    const/4 v2, 0x0

    #@dd
    goto/16 :goto_1e

    #@df
    .line 838
    .restart local v4       #mtiByte:B
    .restart local v8       #bo:Ljava/io/ByteArrayOutputStream;
    :cond_df
    :try_start_df
    invoke-static/range {p4 .. p4}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@e2
    move-result-object v2

    #@e3
    iget v13, v2, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@e5
    goto/16 :goto_55

    #@e7
    .line 839
    .restart local v13       #tmpLanguageTable:I
    :cond_e7
    invoke-static/range {p4 .. p4}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@ea
    move-result-object v2

    #@eb
    iget v12, v2, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I
    :try_end_ed
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_df .. :try_end_ed} :catch_8c

    #@ed
    goto/16 :goto_5d

    #@ef
    .line 857
    .end local v13           #tmpLanguageTable:I
    :cond_ef
    const/4 v2, 0x2

    #@f0
    move/from16 v0, p5

    #@f2
    if-ne v0, v2, :cond_112

    #@f4
    .line 859
    :try_start_f4
    move-object/from16 v0, p2

    #@f6
    move-object/from16 v1, p4

    #@f8
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeKR(Ljava/lang/String;[B)[B
    :try_end_fb
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_f4 .. :try_end_fb} :catch_fd
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_f4 .. :try_end_fb} :catch_8c

    #@fb
    move-result-object v16

    #@fc
    .restart local v16       #userData:[B
    goto :goto_9e

    #@fd
    .line 860
    .end local v16           #userData:[B
    :catch_fd
    move-exception v15

    #@fe
    .line 862
    .local v15, uex:Ljava/io/UnsupportedEncodingException;
    :try_start_fe
    move-object/from16 v0, p2

    #@100
    move-object/from16 v1, p4

    #@102
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeUCS2(Ljava/lang/String;[B)[B
    :try_end_105
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_fe .. :try_end_105} :catch_109
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_fe .. :try_end_105} :catch_8c

    #@105
    move-result-object v16

    #@106
    .line 863
    .restart local v16       #userData:[B
    const/16 p5, 0x3

    #@108
    goto :goto_9e

    #@109
    .line 864
    .end local v16           #userData:[B
    :catch_109
    move-exception v14

    #@10a
    .line 865
    .local v14, ucs2_uex:Ljava/io/UnsupportedEncodingException;
    :try_start_10a
    const-string v2, "getSubmitPdu(), Implausible UnsupportedEncodingException "

    #@10c
    invoke-static {v2, v14}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_10f
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_10a .. :try_end_10f} :catch_8c

    #@10f
    .line 866
    const/4 v7, 0x0

    #@110
    goto/16 :goto_3

    #@112
    .line 872
    .end local v14           #ucs2_uex:Ljava/io/UnsupportedEncodingException;
    .end local v15           #uex:Ljava/io/UnsupportedEncodingException;
    :cond_112
    :try_start_112
    move-object/from16 v0, p2

    #@114
    move-object/from16 v1, p4

    #@116
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeUCS2(Ljava/lang/String;[B)[B
    :try_end_119
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_112 .. :try_end_119} :catch_11b
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_112 .. :try_end_119} :catch_8c

    #@119
    move-result-object v16

    #@11a
    .restart local v16       #userData:[B
    goto :goto_9e

    #@11b
    .line 873
    .end local v16           #userData:[B
    :catch_11b
    move-exception v15

    #@11c
    .line 874
    .restart local v15       #uex:Ljava/io/UnsupportedEncodingException;
    :try_start_11c
    const-string v2, "getSubmitPdu(), Implausible UnsupportedEncodingException "

    #@11e
    invoke-static {v2, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_121
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_11c .. :try_end_121} :catch_8c

    #@121
    .line 875
    const/4 v7, 0x0

    #@122
    goto/16 :goto_3

    #@124
    .line 884
    .end local v15           #uex:Ljava/io/UnsupportedEncodingException;
    .restart local v9       #ex:Lcom/android/internal/telephony/EncodeException;
    :catch_124
    move-exception v15

    #@125
    .line 886
    .restart local v15       #uex:Ljava/io/UnsupportedEncodingException;
    :try_start_125
    move-object/from16 v0, p2

    #@127
    move-object/from16 v1, p4

    #@129
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeUCS2(Ljava/lang/String;[B)[B
    :try_end_12c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_125 .. :try_end_12c} :catch_131

    #@12c
    move-result-object v16

    #@12d
    .line 887
    .restart local v16       #userData:[B
    const/16 p5, 0x3

    #@12f
    goto/16 :goto_9e

    #@131
    .line 888
    .end local v16           #userData:[B
    :catch_131
    move-exception v14

    #@132
    .line 889
    .restart local v14       #ucs2_uex:Ljava/io/UnsupportedEncodingException;
    const-string v2, "getSubmitPdu(), Implausible UnsupportedEncodingException "

    #@134
    invoke-static {v2, v14}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@137
    .line 890
    const/4 v7, 0x0

    #@138
    goto/16 :goto_3

    #@13a
    .line 897
    .end local v14           #ucs2_uex:Ljava/io/UnsupportedEncodingException;
    .end local v15           #uex:Ljava/io/UnsupportedEncodingException;
    :cond_13a
    :try_start_13a
    move-object/from16 v0, p2

    #@13c
    move-object/from16 v1, p4

    #@13e
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeUCS2(Ljava/lang/String;[B)[B
    :try_end_141
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_13a .. :try_end_141} :catch_146

    #@141
    move-result-object v16

    #@142
    .line 898
    .restart local v16       #userData:[B
    const/16 p5, 0x3

    #@144
    goto/16 :goto_9e

    #@146
    .line 899
    .end local v16           #userData:[B
    :catch_146
    move-exception v14

    #@147
    .line 900
    .restart local v14       #ucs2_uex:Ljava/io/UnsupportedEncodingException;
    const-string v2, "getSubmitPdu(), Implausible UnsupportedEncodingException "

    #@149
    invoke-static {v2, v14}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14c
    .line 901
    const/4 v7, 0x0

    #@14d
    goto/16 :goto_3

    #@14f
    .line 920
    .end local v9           #ex:Lcom/android/internal/telephony/EncodeException;
    .end local v14           #ucs2_uex:Ljava/io/UnsupportedEncodingException;
    .restart local v16       #userData:[B
    :cond_14f
    const/4 v2, 0x0

    #@150
    invoke-virtual {v8, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@153
    .line 942
    :goto_153
    const/4 v2, 0x0

    #@154
    move-object/from16 v0, v16

    #@156
    array-length v3, v0

    #@157
    move-object/from16 v0, v16

    #@159
    invoke-virtual {v8, v0, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@15c
    .line 943
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@15f
    move-result-object v2

    #@160
    iput-object v2, v7, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@162
    goto/16 :goto_3

    #@164
    .line 921
    :cond_164
    const/4 v2, 0x2

    #@165
    move/from16 v0, p5

    #@167
    if-ne v0, v2, :cond_1a8

    #@169
    .line 922
    const/4 v2, 0x0

    #@16a
    aget-byte v2, v16, v2

    #@16c
    and-int/lit16 v2, v2, 0xff

    #@16e
    const/16 v3, 0x8c

    #@170
    if-le v2, v3, :cond_1a2

    #@172
    .line 924
    new-instance v2, Ljava/lang/StringBuilder;

    #@174
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@177
    const-string v3, "getSubmitPdu(), ["

    #@179
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v2

    #@17d
    move/from16 v0, p5

    #@17f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@182
    move-result-object v2

    #@183
    const-string v3, "]: Message too long("

    #@185
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v2

    #@189
    const/4 v3, 0x0

    #@18a
    aget-byte v3, v16, v3

    #@18c
    and-int/lit16 v3, v3, 0xff

    #@18e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@191
    move-result-object v2

    #@192
    const-string v3, ")"

    #@194
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v2

    #@198
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19b
    move-result-object v2

    #@19c
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@19f
    .line 925
    const/4 v7, 0x0

    #@1a0
    goto/16 :goto_3

    #@1a2
    .line 929
    :cond_1a2
    const/16 v2, 0x84

    #@1a4
    invoke-virtual {v8, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1a7
    goto :goto_153

    #@1a8
    .line 931
    :cond_1a8
    const/4 v2, 0x0

    #@1a9
    aget-byte v2, v16, v2

    #@1ab
    and-int/lit16 v2, v2, 0xff

    #@1ad
    const/16 v3, 0x8c

    #@1af
    if-le v2, v3, :cond_1e1

    #@1b1
    .line 933
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b6
    const-string v3, "getSubmitPdu(), ["

    #@1b8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    move-result-object v2

    #@1bc
    move/from16 v0, p5

    #@1be
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v2

    #@1c2
    const-string v3, "]: Message too long("

    #@1c4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v2

    #@1c8
    const/4 v3, 0x0

    #@1c9
    aget-byte v3, v16, v3

    #@1cb
    and-int/lit16 v3, v3, 0xff

    #@1cd
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d0
    move-result-object v2

    #@1d1
    const-string v3, ")"

    #@1d3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v2

    #@1d7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1da
    move-result-object v2

    #@1db
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@1de
    .line 934
    const/4 v7, 0x0

    #@1df
    goto/16 :goto_3

    #@1e1
    .line 938
    :cond_1e1
    const/16 v2, 0x8

    #@1e3
    invoke-virtual {v8, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1e6
    goto/16 :goto_153
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .registers 29
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "header"
    .parameter "encoding"
    .parameter "languageTable"
    .parameter "languageShiftTable"

    #@0
    .prologue
    .line 671
    const/4 v4, 0x0

    #@1
    const-string v5, "addProtocolID"

    #@3
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v4

    #@7
    const/4 v5, 0x1

    #@8
    if-ne v4, v5, :cond_1c

    #@a
    .line 672
    const/4 v10, 0x0

    #@b
    move-object/from16 v4, p0

    #@d
    move-object/from16 v5, p1

    #@f
    move-object/from16 v6, p2

    #@11
    move/from16 v7, p3

    #@13
    move-object/from16 v8, p4

    #@15
    move/from16 v9, p5

    #@17
    invoke-static/range {v4 .. v10}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@1a
    move-result-object v16

    #@1b
    .line 791
    :goto_1b
    return-object v16

    #@1c
    .line 678
    :cond_1c
    if-eqz p2, :cond_20

    #@1e
    if-nez p1, :cond_23

    #@20
    .line 679
    :cond_20
    const/16 v16, 0x0

    #@22
    goto :goto_1b

    #@23
    .line 682
    :cond_23
    if-nez p5, :cond_bd

    #@25
    .line 688
    const/4 v14, 0x0

    #@26
    .line 689
    .local v14, isConvertToGsmAlphabet:Z
    const-string v4, "persist.gsm.sms.forcegsm7"

    #@28
    const-string v5, "1"

    #@2a
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v12

    #@2e
    .line 690
    .local v12, encodingType:Ljava/lang/String;
    const-string v4, "0"

    #@30
    invoke-virtual {v12, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v14

    #@34
    .line 691
    move-object/from16 v0, p2

    #@36
    invoke-static {v0, v14}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@39
    move-result-object v18

    #@3a
    .line 694
    .local v18, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    move-object/from16 v0, v18

    #@3c
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@3e
    move/from16 p5, v0

    #@40
    .line 695
    move-object/from16 v0, v18

    #@42
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@44
    move/from16 p6, v0

    #@46
    .line 696
    move-object/from16 v0, v18

    #@48
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@4a
    move/from16 p7, v0

    #@4c
    .line 698
    const/4 v4, 0x1

    #@4d
    move/from16 v0, p5

    #@4f
    if-ne v0, v4, :cond_bd

    #@51
    if-nez p6, :cond_55

    #@53
    if-eqz p7, :cond_bd

    #@55
    .line 700
    :cond_55
    if-eqz p4, :cond_11b

    #@57
    .line 701
    invoke-static/range {p4 .. p4}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@5a
    move-result-object v17

    #@5b
    .line 702
    .local v17, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    move-object/from16 v0, v17

    #@5d
    iget v4, v0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@5f
    move/from16 v0, p6

    #@61
    if-ne v4, v0, :cond_6b

    #@63
    move-object/from16 v0, v17

    #@65
    iget v4, v0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@67
    move/from16 v0, p7

    #@69
    if-eq v4, v0, :cond_bd

    #@6b
    .line 704
    :cond_6b
    const-string v4, "GSM"

    #@6d
    new-instance v5, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v6, "Updating language table in SMS header: "

    #@74
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    move-object/from16 v0, v17

    #@7a
    iget v6, v0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@7c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    const-string v6, " -> "

    #@82
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v5

    #@86
    move/from16 v0, p6

    #@88
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v5

    #@8c
    const-string v6, ", "

    #@8e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v5

    #@92
    move-object/from16 v0, v17

    #@94
    iget v6, v0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@96
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@99
    move-result-object v5

    #@9a
    const-string v6, " -> "

    #@9c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v5

    #@a0
    move/from16 v0, p7

    #@a2
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v5

    #@a6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v5

    #@aa
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    .line 707
    move/from16 v0, p6

    #@af
    move-object/from16 v1, v17

    #@b1
    iput v0, v1, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@b3
    .line 708
    move/from16 v0, p7

    #@b5
    move-object/from16 v1, v17

    #@b7
    iput v0, v1, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@b9
    .line 709
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@bc
    move-result-object p4

    #@bd
    .line 720
    .end local v12           #encodingType:Ljava/lang/String;
    .end local v14           #isConvertToGsmAlphabet:Z
    .end local v17           #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    .end local v18           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_bd
    :goto_bd
    new-instance v16, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@bf
    invoke-direct/range {v16 .. v16}, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;-><init>()V

    #@c2
    .line 722
    .local v16, ret:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz p4, :cond_131

    #@c4
    const/16 v4, 0x40

    #@c6
    :goto_c6
    or-int/lit8 v4, v4, 0x1

    #@c8
    int-to-byte v15, v4

    #@c9
    .line 723
    .local v15, mtiByte:B
    move-object/from16 v0, p0

    #@cb
    move-object/from16 v1, p1

    #@cd
    move/from16 v2, p3

    #@cf
    move-object/from16 v3, v16

    #@d1
    invoke-static {v0, v1, v15, v2, v3}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPduHead(Ljava/lang/String;Ljava/lang/String;BZLcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;)Ljava/io/ByteArrayOutputStream;

    #@d4
    move-result-object v11

    #@d5
    .line 730
    .local v11, bo:Ljava/io/ByteArrayOutputStream;
    const/4 v4, 0x1

    #@d6
    move/from16 v0, p5

    #@d8
    if-ne v0, v4, :cond_133

    #@da
    .line 731
    :try_start_da
    move-object/from16 v0, p2

    #@dc
    move-object/from16 v1, p4

    #@de
    move/from16 v2, p6

    #@e0
    move/from16 v3, p7

    #@e2
    invoke-static {v0, v1, v2, v3}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPackedWithHeader(Ljava/lang/String;[BII)[B
    :try_end_e5
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_da .. :try_end_e5} :catch_14a

    #@e5
    move-result-object v20

    #@e6
    .line 757
    .local v20, userData:[B
    :goto_e6
    const/4 v4, 0x1

    #@e7
    move/from16 v0, p5

    #@e9
    if-ne v0, v4, :cond_184

    #@eb
    .line 758
    const/4 v4, 0x0

    #@ec
    aget-byte v4, v20, v4

    #@ee
    and-int/lit16 v4, v4, 0xff

    #@f0
    const/16 v5, 0xa0

    #@f2
    if-le v4, v5, :cond_164

    #@f4
    .line 760
    const-string v4, "GSM"

    #@f6
    new-instance v5, Ljava/lang/StringBuilder;

    #@f8
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@fb
    const-string v6, "Message too long ("

    #@fd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v5

    #@101
    const/4 v6, 0x0

    #@102
    aget-byte v6, v20, v6

    #@104
    and-int/lit16 v6, v6, 0xff

    #@106
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@109
    move-result-object v5

    #@10a
    const-string v6, " septets)"

    #@10c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v5

    #@110
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@113
    move-result-object v5

    #@114
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@117
    .line 761
    const/16 v16, 0x0

    #@119
    goto/16 :goto_1b

    #@11b
    .line 712
    .end local v11           #bo:Ljava/io/ByteArrayOutputStream;
    .end local v15           #mtiByte:B
    .end local v16           #ret:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .end local v20           #userData:[B
    .restart local v12       #encodingType:Ljava/lang/String;
    .restart local v14       #isConvertToGsmAlphabet:Z
    .restart local v18       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_11b
    new-instance v17, Lcom/android/internal/telephony/SmsHeader;

    #@11d
    invoke-direct/range {v17 .. v17}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@120
    .line 713
    .restart local v17       #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    move/from16 v0, p6

    #@122
    move-object/from16 v1, v17

    #@124
    iput v0, v1, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@126
    .line 714
    move/from16 v0, p7

    #@128
    move-object/from16 v1, v17

    #@12a
    iput v0, v1, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@12c
    .line 715
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@12f
    move-result-object p4

    #@130
    goto :goto_bd

    #@131
    .line 722
    .end local v12           #encodingType:Ljava/lang/String;
    .end local v14           #isConvertToGsmAlphabet:Z
    .end local v17           #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    .end local v18           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .restart local v16       #ret:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_131
    const/4 v4, 0x0

    #@132
    goto :goto_c6

    #@133
    .line 735
    .restart local v11       #bo:Ljava/io/ByteArrayOutputStream;
    .restart local v15       #mtiByte:B
    :cond_133
    :try_start_133
    move-object/from16 v0, p2

    #@135
    move-object/from16 v1, p4

    #@137
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeUCS2(Ljava/lang/String;[B)[B
    :try_end_13a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_133 .. :try_end_13a} :catch_13c
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_133 .. :try_end_13a} :catch_14a

    #@13a
    move-result-object v20

    #@13b
    .restart local v20       #userData:[B
    goto :goto_e6

    #@13c
    .line 736
    .end local v20           #userData:[B
    :catch_13c
    move-exception v19

    #@13d
    .line 737
    .local v19, uex:Ljava/io/UnsupportedEncodingException;
    :try_start_13d
    const-string v4, "GSM"

    #@13f
    const-string v5, "Implausible UnsupportedEncodingException "

    #@141
    move-object/from16 v0, v19

    #@143
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_146
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_13d .. :try_end_146} :catch_14a

    #@146
    .line 740
    const/16 v16, 0x0

    #@148
    goto/16 :goto_1b

    #@14a
    .line 743
    .end local v19           #uex:Ljava/io/UnsupportedEncodingException;
    :catch_14a
    move-exception v13

    #@14b
    .line 747
    .local v13, ex:Lcom/android/internal/telephony/EncodeException;
    :try_start_14b
    move-object/from16 v0, p2

    #@14d
    move-object/from16 v1, p4

    #@14f
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeUCS2(Ljava/lang/String;[B)[B
    :try_end_152
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_14b .. :try_end_152} :catch_156

    #@152
    move-result-object v20

    #@153
    .line 748
    .restart local v20       #userData:[B
    const/16 p5, 0x3

    #@155
    goto :goto_e6

    #@156
    .line 749
    .end local v20           #userData:[B
    :catch_156
    move-exception v19

    #@157
    .line 750
    .restart local v19       #uex:Ljava/io/UnsupportedEncodingException;
    const-string v4, "GSM"

    #@159
    const-string v5, "Implausible UnsupportedEncodingException "

    #@15b
    move-object/from16 v0, v19

    #@15d
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@160
    .line 753
    const/16 v16, 0x0

    #@162
    goto/16 :goto_1b

    #@164
    .line 771
    .end local v13           #ex:Lcom/android/internal/telephony/EncodeException;
    .end local v19           #uex:Ljava/io/UnsupportedEncodingException;
    .restart local v20       #userData:[B
    :cond_164
    const/4 v4, 0x0

    #@165
    invoke-virtual {v11, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@168
    .line 783
    :goto_168
    sget v4, Lcom/android/internal/telephony/gsm/SmsMessage;->vp:I

    #@16a
    if-lez v4, :cond_171

    #@16c
    .line 784
    sget v4, Lcom/android/internal/telephony/gsm/SmsMessage;->vp:I

    #@16e
    invoke-virtual {v11, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@171
    .line 789
    :cond_171
    const/4 v4, 0x0

    #@172
    move-object/from16 v0, v20

    #@174
    array-length v5, v0

    #@175
    move-object/from16 v0, v20

    #@177
    invoke-virtual {v11, v0, v4, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@17a
    .line 790
    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@17d
    move-result-object v4

    #@17e
    move-object/from16 v0, v16

    #@180
    iput-object v4, v0, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@182
    goto/16 :goto_1b

    #@184
    .line 773
    :cond_184
    const/4 v4, 0x0

    #@185
    aget-byte v4, v20, v4

    #@187
    and-int/lit16 v4, v4, 0xff

    #@189
    const/16 v5, 0x8c

    #@18b
    if-le v4, v5, :cond_1b4

    #@18d
    .line 775
    const-string v4, "GSM"

    #@18f
    new-instance v5, Ljava/lang/StringBuilder;

    #@191
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@194
    const-string v6, "Message too long ("

    #@196
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@199
    move-result-object v5

    #@19a
    const/4 v6, 0x0

    #@19b
    aget-byte v6, v20, v6

    #@19d
    and-int/lit16 v6, v6, 0xff

    #@19f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v5

    #@1a3
    const-string v6, " bytes)"

    #@1a5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v5

    #@1a9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ac
    move-result-object v5

    #@1ad
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b0
    .line 776
    const/16 v16, 0x0

    #@1b2
    goto/16 :goto_1b

    #@1b4
    .line 780
    :cond_1b4
    const/16 v4, 0x8

    #@1b6
    invoke-virtual {v11, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1b9
    goto :goto_168
.end method

.method private static getSubmitPduHead(Ljava/lang/String;Ljava/lang/String;BZILcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;)Ljava/io/ByteArrayOutputStream;
    .registers 14
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "mtiByte"
    .parameter "statusReportRequested"
    .parameter "protocolId"
    .parameter "ret"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 1602
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@5
    const/16 v5, 0xb4

    #@7
    invoke-direct {v0, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@a
    .line 1606
    .local v0, bo:Ljava/io/ByteArrayOutputStream;
    if-nez p0, :cond_71

    #@c
    .line 1609
    const-string v5, "addSCAddress"

    #@e
    invoke-static {v6, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@11
    move-result v5

    #@12
    if-ne v5, v3, :cond_6e

    #@14
    .line 1611
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSCAddressTemp()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    .line 1613
    .local v2, mSCAddress:Ljava/lang/String;
    if-nez v2, :cond_67

    #@1a
    .line 1614
    iput-object v6, p5, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@1c
    .line 1628
    .end local v2           #mSCAddress:Ljava/lang/String;
    :goto_1c
    if-eqz p3, :cond_21

    #@1e
    .line 1630
    or-int/lit8 v5, p2, 0x20

    #@20
    int-to-byte p2, v5

    #@21
    .line 1634
    :cond_21
    sget v5, Lcom/android/internal/telephony/gsm/SmsMessage;->vp:I

    #@23
    if-lez v5, :cond_28

    #@25
    .line 1636
    or-int/lit8 v5, p2, 0x10

    #@27
    int-to-byte p2, v5

    #@28
    .line 1638
    :cond_28
    new-instance v5, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v6, "getSubmitPduHead(), mtiByte = "

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@3e
    .line 1642
    invoke-virtual {v0, p2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@41
    .line 1645
    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@44
    .line 1649
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCD(Ljava/lang/String;)[B

    #@47
    move-result-object v1

    #@48
    .line 1651
    .local v1, daBytes:[B
    if-eqz v1, :cond_63

    #@4a
    .line 1655
    array-length v5, v1

    #@4b
    add-int/lit8 v5, v5, -0x1

    #@4d
    mul-int/lit8 v5, v5, 0x2

    #@4f
    array-length v6, v1

    #@50
    add-int/lit8 v6, v6, -0x1

    #@52
    aget-byte v6, v1, v6

    #@54
    and-int/lit16 v6, v6, 0xf0

    #@56
    const/16 v7, 0xf0

    #@58
    if-ne v6, v7, :cond_78

    #@5a
    :goto_5a
    sub-int v3, v5, v3

    #@5c
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@5f
    .line 1659
    array-length v3, v1

    #@60
    invoke-virtual {v0, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@63
    .line 1662
    :cond_63
    invoke-virtual {v0, p4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@66
    .line 1663
    return-object v0

    #@67
    .line 1617
    .end local v1           #daBytes:[B
    .restart local v2       #mSCAddress:Ljava/lang/String;
    :cond_67
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCDWithLength(Ljava/lang/String;)[B

    #@6a
    move-result-object v5

    #@6b
    iput-object v5, p5, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@6d
    goto :goto_1c

    #@6e
    .line 1621
    .end local v2           #mSCAddress:Ljava/lang/String;
    :cond_6e
    iput-object v6, p5, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@70
    goto :goto_1c

    #@71
    .line 1623
    :cond_71
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCDWithLength(Ljava/lang/String;)[B

    #@74
    move-result-object v5

    #@75
    iput-object v5, p5, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@77
    goto :goto_1c

    #@78
    .restart local v1       #daBytes:[B
    :cond_78
    move v3, v4

    #@79
    .line 1655
    goto :goto_5a
.end method

.method private static getSubmitPduHead(Ljava/lang/String;Ljava/lang/String;BZLcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;)Ljava/io/ByteArrayOutputStream;
    .registers 13
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "mtiByte"
    .parameter "statusReportRequested"
    .parameter "ret"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 1270
    const-string v1, "addProtocolID"

    #@5
    invoke-static {v2, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8
    move-result v1

    #@9
    if-ne v1, v0, :cond_15

    #@b
    move-object v0, p0

    #@c
    move-object v1, p1

    #@d
    move v2, p2

    #@e
    move v3, p3

    #@f
    move-object v5, p4

    #@10
    .line 1271
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPduHead(Ljava/lang/String;Ljava/lang/String;BZILcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;)Ljava/io/ByteArrayOutputStream;

    #@13
    move-result-object v6

    #@14
    .line 1316
    :goto_14
    return-object v6

    #@15
    .line 1275
    :cond_15
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    #@17
    const/16 v1, 0xb4

    #@19
    invoke-direct {v6, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@1c
    .line 1279
    .local v6, bo:Ljava/io/ByteArrayOutputStream;
    if-nez p0, :cond_53

    #@1e
    .line 1280
    iput-object v2, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@20
    .line 1287
    :goto_20
    if-eqz p3, :cond_25

    #@22
    .line 1289
    or-int/lit8 v1, p2, 0x20

    #@24
    int-to-byte p2, v1

    #@25
    .line 1293
    :cond_25
    sget v1, Lcom/android/internal/telephony/gsm/SmsMessage;->vp:I

    #@27
    if-lez v1, :cond_2c

    #@29
    .line 1294
    or-int/lit8 v1, p2, 0x10

    #@2b
    int-to-byte p2, v1

    #@2c
    .line 1297
    :cond_2c
    invoke-virtual {v6, p2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2f
    .line 1300
    invoke-virtual {v6, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@32
    .line 1304
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCD(Ljava/lang/String;)[B

    #@35
    move-result-object v7

    #@36
    .line 1308
    .local v7, daBytes:[B
    array-length v1, v7

    #@37
    add-int/lit8 v1, v1, -0x1

    #@39
    mul-int/lit8 v1, v1, 0x2

    #@3b
    array-length v2, v7

    #@3c
    add-int/lit8 v2, v2, -0x1

    #@3e
    aget-byte v2, v7, v2

    #@40
    and-int/lit16 v2, v2, 0xf0

    #@42
    const/16 v3, 0xf0

    #@44
    if-ne v2, v3, :cond_5a

    #@46
    :goto_46
    sub-int v0, v1, v0

    #@48
    invoke-virtual {v6, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@4b
    .line 1312
    array-length v0, v7

    #@4c
    invoke-virtual {v6, v7, v4, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@4f
    .line 1315
    invoke-virtual {v6, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@52
    goto :goto_14

    #@53
    .line 1282
    .end local v7           #daBytes:[B
    :cond_53
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCDWithLength(Ljava/lang/String;)[B

    #@56
    move-result-object v1

    #@57
    iput-object v1, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@59
    goto :goto_20

    #@5a
    .restart local v7       #daBytes:[B
    :cond_5a
    move v0, v4

    #@5b
    .line 1308
    goto :goto_46
.end method

.method private static getSubmitPduHeadforEmailOverSms(Ljava/lang/String;Ljava/lang/String;BZLcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;)Ljava/io/ByteArrayOutputStream;
    .registers 12
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "mtiByte"
    .parameter "statusReportRequested"
    .parameter "ret"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1323
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@3
    const/16 v3, 0xb4

    #@5
    invoke-direct {v0, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@8
    .line 1327
    .local v0, bo:Ljava/io/ByteArrayOutputStream;
    if-nez p0, :cond_5f

    #@a
    .line 1328
    const/4 v3, 0x0

    #@b
    iput-object v3, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@d
    .line 1335
    :goto_d
    if-eqz p3, :cond_12

    #@f
    .line 1337
    or-int/lit8 v3, p2, 0x20

    #@11
    int-to-byte p2, v3

    #@12
    .line 1343
    :cond_12
    sget v3, Lcom/android/internal/telephony/gsm/SmsMessage;->vp:I

    #@14
    if-lez v3, :cond_19

    #@16
    .line 1344
    or-int/lit8 v3, p2, 0x10

    #@18
    int-to-byte p2, v3

    #@19
    .line 1348
    :cond_19
    invoke-virtual {v0, p2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1c
    .line 1351
    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1f
    .line 1355
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCD(Ljava/lang/String;)[B

    #@22
    move-result-object v1

    #@23
    .line 1360
    .local v1, daBytes:[B
    if-eqz v1, :cond_59

    #@25
    .line 1361
    array-length v3, v1

    #@26
    add-int/lit8 v3, v3, -0x1

    #@28
    mul-int/lit8 v5, v3, 0x2

    #@2a
    array-length v3, v1

    #@2b
    add-int/lit8 v3, v3, -0x1

    #@2d
    aget-byte v3, v1, v3

    #@2f
    and-int/lit16 v3, v3, 0xf0

    #@31
    const/16 v6, 0xf0

    #@33
    if-ne v3, v6, :cond_66

    #@35
    const/4 v3, 0x1

    #@36
    :goto_36
    sub-int v3, v5, v3

    #@38
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@3b
    .line 1364
    array-length v3, v1

    #@3c
    add-int/lit8 v3, v3, -0x1

    #@3e
    aget-byte v3, v1, v3

    #@40
    and-int/lit16 v3, v3, 0xf0

    #@42
    int-to-byte v2, v3

    #@43
    .line 1367
    .local v2, lengthcheck:B
    const/16 v3, -0x80

    #@45
    aput-byte v3, v1, v4

    #@47
    .line 1368
    aget-byte v3, v1, v4

    #@49
    or-int/lit8 v3, v3, 0x20

    #@4b
    int-to-byte v3, v3

    #@4c
    aput-byte v3, v1, v4

    #@4e
    .line 1369
    aget-byte v3, v1, v4

    #@50
    or-int/lit8 v3, v3, 0x1

    #@52
    int-to-byte v3, v3

    #@53
    aput-byte v3, v1, v4

    #@55
    .line 1372
    array-length v3, v1

    #@56
    invoke-virtual {v0, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@59
    .line 1376
    .end local v2           #lengthcheck:B
    :cond_59
    const/16 v3, 0x32

    #@5b
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@5e
    .line 1378
    return-object v0

    #@5f
    .line 1330
    .end local v1           #daBytes:[B
    :cond_5f
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCDWithLength(Ljava/lang/String;)[B

    #@62
    move-result-object v3

    #@63
    iput-object v3, p4, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B

    #@65
    goto :goto_d

    #@66
    .restart local v1       #daBytes:[B
    :cond_66
    move v3, v4

    #@67
    .line 1361
    goto :goto_36
.end method

.method public static getSubmitPduforEmailoverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BI)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .registers 21
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "header"
    .parameter "encoding"

    #@0
    .prologue
    .line 1386
    if-eqz p2, :cond_4

    #@2
    if-nez p1, :cond_6

    #@4
    .line 1387
    :cond_4
    const/4 v12, 0x0

    #@5
    .line 1474
    :goto_5
    return-object v12

    #@6
    .line 1390
    :cond_6
    new-instance v12, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@8
    invoke-direct {v12}, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;-><init>()V

    #@b
    .line 1392
    .local v12, ret:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz p4, :cond_3d

    #@d
    const/16 v2, 0x40

    #@f
    :goto_f
    or-int/lit8 v2, v2, 0x1

    #@11
    int-to-byte v11, v2

    #@12
    .line 1394
    .local v11, mtiByte:B
    move-object/from16 v0, p1

    #@14
    move/from16 v1, p3

    #@16
    invoke-static {p0, v0, v11, v1, v12}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPduHeadforEmailOverSms(Ljava/lang/String;Ljava/lang/String;BZLcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;)Ljava/io/ByteArrayOutputStream;

    #@19
    move-result-object v8

    #@1a
    .line 1404
    .local v8, bo:Ljava/io/ByteArrayOutputStream;
    if-nez p5, :cond_1e

    #@1c
    .line 1406
    const/16 p5, 0x1

    #@1e
    .line 1409
    :cond_1e
    const/4 v2, 0x1

    #@1f
    move/from16 v0, p5

    #@21
    if-ne v0, v2, :cond_3f

    #@23
    .line 1410
    const/4 v2, 0x0

    #@24
    const/4 v3, 0x0

    #@25
    :try_start_25
    move-object/from16 v0, p2

    #@27
    move-object/from16 v1, p4

    #@29
    invoke-static {v0, v1, v2, v3}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPackedWithHeader(Ljava/lang/String;[BII)[B
    :try_end_2c
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_25 .. :try_end_2c} :catch_50

    #@2c
    move-result-object v14

    #@2d
    .line 1431
    .local v14, userData:[B
    :goto_2d
    const/4 v2, 0x1

    #@2e
    move/from16 v0, p5

    #@30
    if-ne v0, v2, :cond_64

    #@32
    .line 1432
    const/4 v2, 0x0

    #@33
    aget-byte v2, v14, v2

    #@35
    and-int/lit16 v2, v2, 0xff

    #@37
    const/16 v3, 0xa0

    #@39
    if-le v2, v3, :cond_6f

    #@3b
    .line 1434
    const/4 v12, 0x0

    #@3c
    goto :goto_5

    #@3d
    .line 1392
    .end local v8           #bo:Ljava/io/ByteArrayOutputStream;
    .end local v11           #mtiByte:B
    .end local v14           #userData:[B
    :cond_3d
    const/4 v2, 0x0

    #@3e
    goto :goto_f

    #@3f
    .line 1413
    .restart local v8       #bo:Ljava/io/ByteArrayOutputStream;
    .restart local v11       #mtiByte:B
    :cond_3f
    :try_start_3f
    move-object/from16 v0, p2

    #@41
    move-object/from16 v1, p4

    #@43
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeUCS2(Ljava/lang/String;[B)[B
    :try_end_46
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3f .. :try_end_46} :catch_48
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_3f .. :try_end_46} :catch_50

    #@46
    move-result-object v14

    #@47
    .restart local v14       #userData:[B
    goto :goto_2d

    #@48
    .line 1414
    .end local v14           #userData:[B
    :catch_48
    move-exception v13

    #@49
    .line 1415
    .local v13, uex:Ljava/io/UnsupportedEncodingException;
    :try_start_49
    const-string v2, "getSubmitPduforEmailoverSms(), Implausible UnsupportedEncodingException "

    #@4b
    invoke-static {v2, v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4e
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_49 .. :try_end_4e} :catch_50

    #@4e
    .line 1416
    const/4 v12, 0x0

    #@4f
    goto :goto_5

    #@50
    .line 1419
    .end local v13           #uex:Ljava/io/UnsupportedEncodingException;
    :catch_50
    move-exception v10

    #@51
    .line 1423
    .local v10, ex:Lcom/android/internal/telephony/EncodeException;
    :try_start_51
    move-object/from16 v0, p2

    #@53
    move-object/from16 v1, p4

    #@55
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->encodeUCS2(Ljava/lang/String;[B)[B
    :try_end_58
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_51 .. :try_end_58} :catch_5c

    #@58
    move-result-object v14

    #@59
    .line 1424
    .restart local v14       #userData:[B
    const/16 p5, 0x3

    #@5b
    goto :goto_2d

    #@5c
    .line 1425
    .end local v14           #userData:[B
    :catch_5c
    move-exception v13

    #@5d
    .line 1426
    .restart local v13       #uex:Ljava/io/UnsupportedEncodingException;
    const-string v2, "getSubmitPduforEmailoverSms(), Implausible UnsupportedEncodingException "

    #@5f
    invoke-static {v2, v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@62
    .line 1427
    const/4 v12, 0x0

    #@63
    goto :goto_5

    #@64
    .line 1447
    .end local v10           #ex:Lcom/android/internal/telephony/EncodeException;
    .end local v13           #uex:Ljava/io/UnsupportedEncodingException;
    .restart local v14       #userData:[B
    :cond_64
    const/4 v2, 0x0

    #@65
    aget-byte v2, v14, v2

    #@67
    and-int/lit16 v2, v2, 0xff

    #@69
    const/16 v3, 0x8c

    #@6b
    if-le v2, v3, :cond_6f

    #@6d
    .line 1449
    const/4 v12, 0x0

    #@6e
    goto :goto_5

    #@6f
    .line 1457
    :cond_6f
    sget-object v2, Lcom/android/internal/telephony/SmsConstants$MessageClass;->UNKNOWN:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@71
    const/4 v3, 0x0

    #@72
    const/4 v5, 0x0

    #@73
    const/4 v6, 0x0

    #@74
    const/4 v7, 0x2

    #@75
    move/from16 v4, p5

    #@77
    invoke-static/range {v2 .. v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->createDataCodingScheme(Lcom/android/internal/telephony/SmsConstants$MessageClass;ZIIZI)B

    #@7a
    move-result v9

    #@7b
    .line 1459
    .local v9, dcs:B
    invoke-virtual {v8, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@7e
    .line 1461
    new-instance v2, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v3, "getSubmitPduforEmailoverSms(), DCS = "

    #@85
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v2

    #@89
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v2

    #@8d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v2

    #@91
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@94
    .line 1465
    sget v2, Lcom/android/internal/telephony/gsm/SmsMessage;->vp:I

    #@96
    if-lez v2, :cond_9d

    #@98
    .line 1466
    sget v2, Lcom/android/internal/telephony/gsm/SmsMessage;->vp:I

    #@9a
    invoke-virtual {v8, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@9d
    .line 1470
    :cond_9d
    const/4 v2, 0x0

    #@9e
    array-length v3, v14

    #@9f
    invoke-virtual {v8, v14, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@a2
    .line 1472
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@a5
    move-result-object v2

    #@a6
    iput-object v2, v12, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@a8
    goto/16 :goto_5
.end method

.method public static getTPLayerLengthForPDU(Ljava/lang/String;)I
    .registers 5
    .parameter "pdu"

    #@0
    .prologue
    .line 618
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v2

    #@4
    div-int/lit8 v0, v2, 0x2

    #@6
    .line 619
    .local v0, len:I
    const/4 v2, 0x0

    #@7
    const/4 v3, 0x2

    #@8
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    const/16 v3, 0x10

    #@e
    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@11
    move-result v1

    #@12
    .line 621
    .local v1, smscLen:I
    sub-int v2, v0, v1

    #@14
    add-int/lit8 v2, v2, -0x1

    #@16
    return v2
.end method

.method public static isCountCharIndexInsteadOfSeptets()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 3573
    const/4 v1, 0x0

    #@2
    const-string v2, "countCharIndexInsteadOfSeptets"

    #@4
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-ne v1, v0, :cond_11

    #@a
    .line 3574
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isReleaseOperator()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_11

    #@10
    .line 3579
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public static isKSC5601Encoding()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 3558
    const/4 v1, 0x0

    #@2
    const-string v2, "KSC5601Encoding"

    #@4
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-ne v1, v0, :cond_11

    #@a
    .line 3559
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isReleaseOperator()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_11

    #@10
    .line 3564
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public static isReleaseOperator()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 3588
    const-string v4, "gsm.sim.operator.numeric"

    #@4
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 3590
    .local v1, numeric:Ljava/lang/String;
    if-nez v1, :cond_b

    #@a
    .line 3608
    :cond_a
    :goto_a
    return v2

    #@b
    .line 3594
    :cond_b
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@e
    move-result v4

    #@f
    if-nez v4, :cond_13

    #@11
    move v2, v3

    #@12
    .line 3595
    goto :goto_a

    #@13
    .line 3598
    :cond_13
    const/4 v4, 0x0

    #@14
    const-string v5, "releaseOperatorMccMnc"

    #@16
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    .line 3600
    .local v0, mccmnc:Ljava/lang/String;
    if-nez v0, :cond_1e

    #@1c
    move v2, v3

    #@1d
    .line 3601
    goto :goto_a

    #@1e
    .line 3604
    :cond_1e
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v4

    #@22
    if-ne v4, v3, :cond_a

    #@24
    move v2, v3

    #@25
    .line 3605
    goto :goto_a
.end method

.method public static makeSmsHeader(ILjava/lang/String;)[B
    .registers 13
    .parameter "readReplyValue"
    .parameter "replyAddress"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x1

    #@3
    .line 3466
    new-instance v3, Lcom/android/internal/telephony/SmsHeader;

    #@5
    invoke-direct {v3}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@8
    .line 3468
    .local v3, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    const-string v5, "replyAddress"

    #@a
    invoke-static {v10, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@d
    move-result v5

    #@e
    if-ne v5, v6, :cond_2f

    #@10
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isReleaseOperator()Z

    #@13
    move-result v5

    #@14
    if-ne v5, v6, :cond_2f

    #@16
    .line 3469
    if-eqz p1, :cond_2f

    #@18
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@1b
    move-result v5

    #@1c
    if-lez v5, :cond_2f

    #@1e
    .line 3471
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->KRsmsnetworkPortionToCalledPartyBCD(Ljava/lang/String;)[B

    #@21
    move-result-object v0

    #@22
    .line 3472
    .local v0, daBytes:[B
    new-instance v1, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@24
    invoke-direct {v1}, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;-><init>()V

    #@27
    .line 3473
    .local v1, replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    iput-object v0, v1, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->replyData:[B

    #@29
    .line 3475
    if-nez v0, :cond_57

    #@2b
    .line 3476
    iput v7, v1, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->addressLength:I

    #@2d
    .line 3480
    :goto_2d
    iput-object v1, v3, Lcom/android/internal/telephony/SmsHeader;->replyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@2f
    .line 3485
    .end local v0           #daBytes:[B
    .end local v1           #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    :cond_2f
    const-string v5, "confirmRead"

    #@31
    invoke-static {v10, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@34
    move-result v5

    #@35
    if-ne v5, v6, :cond_52

    #@37
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isReleaseOperator()Z

    #@3a
    move-result v5

    #@3b
    if-ne v5, v6, :cond_52

    #@3d
    .line 3486
    const/4 v5, -0x1

    #@3e
    if-le p0, v5, :cond_52

    #@40
    .line 3487
    new-instance v2, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@42
    invoke-direct {v2}, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;-><init>()V

    #@45
    .line 3488
    .local v2, smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;
    iput v6, v2, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->length:I

    #@47
    .line 3489
    new-array v5, v6, [B

    #@49
    iput-object v5, v2, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@4b
    .line 3490
    iget-object v5, v2, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@4d
    int-to-byte v6, p0

    #@4e
    aput-byte v6, v5, v7

    #@50
    .line 3491
    iput-object v2, v3, Lcom/android/internal/telephony/SmsHeader;->smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@52
    .line 3496
    .end local v2           #smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;
    :cond_52
    invoke-static {v3}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@55
    move-result-object v4

    #@56
    .line 3497
    .local v4, smsHeaderData:[B
    return-object v4

    #@57
    .line 3479
    .end local v4           #smsHeaderData:[B
    .restart local v0       #daBytes:[B
    .restart local v1       #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    :cond_57
    array-length v5, v0

    #@58
    add-int/lit8 v5, v5, -0x1

    #@5a
    mul-int/lit8 v8, v5, 0x2

    #@5c
    array-length v5, v0

    #@5d
    add-int/lit8 v5, v5, -0x1

    #@5f
    aget-byte v5, v0, v5

    #@61
    and-int/lit16 v5, v5, 0xf0

    #@63
    const/16 v9, 0xf0

    #@65
    if-ne v5, v9, :cond_6d

    #@67
    move v5, v6

    #@68
    :goto_68
    sub-int v5, v8, v5

    #@6a
    iput v5, v1, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->addressLength:I

    #@6c
    goto :goto_2d

    #@6d
    :cond_6d
    move v5, v7

    #@6e
    goto :goto_68
.end method

.method public static newFromCDS(Ljava/lang/String;)Lcom/android/internal/telephony/gsm/SmsMessage;
    .registers 5
    .parameter "line"

    #@0
    .prologue
    .line 563
    :try_start_0
    new-instance v1, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@2
    invoke-direct {v1}, Lcom/android/internal/telephony/gsm/SmsMessage;-><init>()V

    #@5
    .line 564
    .local v1, msg:Lcom/android/internal/telephony/gsm/SmsMessage;
    invoke-static {p0}, Lcom/android/internal/telephony/uicc/IccUtils;->hexStringToBytes(Ljava/lang/String;)[B

    #@8
    move-result-object v2

    #@9
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/gsm/SmsMessage;->parsePdu([B)V
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_c} :catch_d

    #@c
    .line 568
    .end local v1           #msg:Lcom/android/internal/telephony/gsm/SmsMessage;
    :goto_c
    return-object v1

    #@d
    .line 566
    :catch_d
    move-exception v0

    #@e
    .line 567
    .local v0, ex:Ljava/lang/RuntimeException;
    const-string v2, "GSM"

    #@10
    const-string v3, "CDS SMS PDU parsing failed: "

    #@12
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    .line 568
    const/4 v1, 0x0

    #@16
    goto :goto_c
.end method

.method public static newFromCMT([Ljava/lang/String;)Lcom/android/internal/telephony/gsm/SmsMessage;
    .registers 5
    .parameter "lines"

    #@0
    .prologue
    .line 551
    :try_start_0
    new-instance v1, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@2
    invoke-direct {v1}, Lcom/android/internal/telephony/gsm/SmsMessage;-><init>()V

    #@5
    .line 552
    .local v1, msg:Lcom/android/internal/telephony/gsm/SmsMessage;
    const/4 v2, 0x1

    #@6
    aget-object v2, p0, v2

    #@8
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/IccUtils;->hexStringToBytes(Ljava/lang/String;)[B

    #@b
    move-result-object v2

    #@c
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/gsm/SmsMessage;->parsePdu([B)V
    :try_end_f
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_f} :catch_10

    #@f
    .line 556
    .end local v1           #msg:Lcom/android/internal/telephony/gsm/SmsMessage;
    :goto_f
    return-object v1

    #@10
    .line 554
    :catch_10
    move-exception v0

    #@11
    .line 555
    .local v0, ex:Ljava/lang/RuntimeException;
    const-string v2, "GSM"

    #@13
    const-string v3, "SMS PDU parsing failed: "

    #@15
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    .line 556
    const/4 v1, 0x0

    #@19
    goto :goto_f
.end method

.method private parsePdu([B)V
    .registers 6
    .parameter "pdu"

    #@0
    .prologue
    .line 2897
    iput-object p1, p0, Lcom/android/internal/telephony/SmsMessageBase;->mPdu:[B

    #@2
    .line 2901
    new-instance v1, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;

    #@4
    invoke-direct {v1, p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;-><init>([B)V

    #@7
    .line 2903
    .local v1, p:Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getSCAddress()Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    iput-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->scAddress:Ljava/lang/String;

    #@d
    .line 2905
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->scAddress:Ljava/lang/String;

    #@f
    if-eqz v2, :cond_11

    #@11
    .line 2913
    :cond_11
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@14
    move-result v0

    #@15
    .line 2915
    .local v0, firstByte:I
    and-int/lit8 v2, v0, 0x3

    #@17
    iput v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->mti:I

    #@19
    .line 2916
    iget v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->mti:I

    #@1b
    packed-switch v2, :pswitch_data_32

    #@1e
    .line 2934
    new-instance v2, Ljava/lang/RuntimeException;

    #@20
    const-string v3, "Unsupported message type"

    #@22
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v2

    #@26
    .line 2922
    :pswitch_26
    invoke-direct {p0, v1, v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->parseSmsDeliver(Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;I)V

    #@29
    .line 2936
    :goto_29
    return-void

    #@2a
    .line 2925
    :pswitch_2a
    invoke-direct {p0, v1, v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->parseSmsStatusReport(Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;I)V

    #@2d
    goto :goto_29

    #@2e
    .line 2929
    :pswitch_2e
    invoke-direct {p0, v1, v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->parseSmsSubmit(Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;I)V

    #@31
    goto :goto_29

    #@32
    .line 2916
    :pswitch_data_32
    .packed-switch 0x0
        :pswitch_26
        :pswitch_2e
        :pswitch_2a
        :pswitch_26
    .end packed-switch
.end method

.method private parseSmsDeliver(Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;I)V
    .registers 9
    .parameter "p"
    .parameter "firstByte"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 3038
    and-int/lit16 v1, p2, 0x80

    #@4
    const/16 v4, 0x80

    #@6
    if-ne v1, v4, :cond_32

    #@8
    move v1, v2

    #@9
    :goto_9
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->replyPathPresent:Z

    #@b
    .line 3040
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getAddress()Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@11
    .line 3042
    iget-object v1, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@13
    if-eqz v1, :cond_15

    #@15
    .line 3049
    :cond_15
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@18
    move-result v1

    #@19
    iput v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->protocolIdentifier:I

    #@1b
    .line 3053
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@1e
    move-result v1

    #@1f
    iput v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@21
    .line 3060
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getSCTimestampMillis()J

    #@24
    move-result-wide v4

    #@25
    iput-wide v4, p0, Lcom/android/internal/telephony/SmsMessageBase;->scTimeMillis:J

    #@27
    .line 3064
    and-int/lit8 v1, p2, 0x40

    #@29
    const/16 v4, 0x40

    #@2b
    if-ne v1, v4, :cond_34

    #@2d
    move v0, v2

    #@2e
    .line 3066
    .local v0, hasUserDataHeader:Z
    :goto_2e
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->parseUserData(Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;Z)V

    #@31
    .line 3067
    return-void

    #@32
    .end local v0           #hasUserDataHeader:Z
    :cond_32
    move v1, v3

    #@33
    .line 3038
    goto :goto_9

    #@34
    :cond_34
    move v0, v3

    #@35
    .line 3064
    goto :goto_2e
.end method

.method private parseSmsStatusReport(Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;I)V
    .registers 11
    .parameter "p"
    .parameter "firstByte"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 2991
    iput-boolean v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->isStatusReportMessage:Z

    #@4
    .line 2994
    and-int/lit8 v3, p2, 0x20

    #@6
    if-nez v3, :cond_3d

    #@8
    move v3, v4

    #@9
    :goto_9
    iput-boolean v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->forSubmit:Z

    #@b
    .line 2996
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@e
    move-result v3

    #@f
    iput v3, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageRef:I

    #@11
    .line 2998
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getAddress()Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    #@14
    move-result-object v3

    #@15
    iput-object v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->recipientAddress:Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    #@17
    .line 3000
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getSCTimestampMillis()J

    #@1a
    move-result-wide v6

    #@1b
    iput-wide v6, p0, Lcom/android/internal/telephony/SmsMessageBase;->scTimeMillis:J

    #@1d
    .line 3002
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getSCTimestampMillis()J

    #@20
    move-result-wide v6

    #@21
    iput-wide v6, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->dischargeTimeMillis:J

    #@23
    .line 3004
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@26
    move-result v3

    #@27
    iput v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->status:I

    #@29
    .line 3007
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->moreDataPresent()Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_65

    #@2f
    .line 3009
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@32
    move-result v0

    #@33
    .line 3010
    .local v0, extraParams:I
    move v2, v0

    #@34
    .line 3011
    .local v2, moreExtraParams:I
    :goto_34
    and-int/lit16 v3, v2, 0x80

    #@36
    if-eqz v3, :cond_3f

    #@38
    .line 3015
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@3b
    move-result v2

    #@3c
    goto :goto_34

    #@3d
    .end local v0           #extraParams:I
    .end local v2           #moreExtraParams:I
    :cond_3d
    move v3, v5

    #@3e
    .line 2994
    goto :goto_9

    #@3f
    .line 3019
    .restart local v0       #extraParams:I
    .restart local v2       #moreExtraParams:I
    :cond_3f
    and-int/lit8 v3, v0, 0x78

    #@41
    if-nez v3, :cond_65

    #@43
    .line 3021
    and-int/lit8 v3, v0, 0x1

    #@45
    if-eqz v3, :cond_4d

    #@47
    .line 3022
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@4a
    move-result v3

    #@4b
    iput v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->protocolIdentifier:I

    #@4d
    .line 3025
    :cond_4d
    and-int/lit8 v3, v0, 0x2

    #@4f
    if-eqz v3, :cond_57

    #@51
    .line 3026
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@54
    move-result v3

    #@55
    iput v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@57
    .line 3029
    :cond_57
    and-int/lit8 v3, v0, 0x4

    #@59
    if-eqz v3, :cond_65

    #@5b
    .line 3030
    and-int/lit8 v3, p2, 0x40

    #@5d
    const/16 v6, 0x40

    #@5f
    if-ne v3, v6, :cond_66

    #@61
    move v1, v4

    #@62
    .line 3031
    .local v1, hasUserDataHeader:Z
    :goto_62
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->parseUserData(Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;Z)V

    #@65
    .line 3035
    .end local v0           #extraParams:I
    .end local v1           #hasUserDataHeader:Z
    .end local v2           #moreExtraParams:I
    :cond_65
    return-void

    #@66
    .restart local v0       #extraParams:I
    .restart local v2       #moreExtraParams:I
    :cond_66
    move v1, v5

    #@67
    .line 3030
    goto :goto_62
.end method

.method private parseSmsSubmit(Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;I)V
    .registers 10
    .parameter "p"
    .parameter "firstByte"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 2940
    and-int/lit16 v2, p2, 0x80

    #@4
    const/16 v5, 0x80

    #@6
    if-ne v2, v5, :cond_48

    #@8
    move v2, v3

    #@9
    :goto_9
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->replyPathPresent:Z

    #@b
    .line 2942
    shr-int/lit8 v2, p2, 0x5

    #@d
    and-int/lit8 v2, v2, 0x1

    #@f
    iput v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->statusReportReq:I

    #@11
    .line 2944
    shr-int/lit8 v2, p2, 0x3

    #@13
    and-int/lit8 v2, v2, 0x3

    #@15
    iput v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->vpFormatPresent:I

    #@17
    .line 2946
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@1a
    move-result v2

    #@1b
    iput v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageRef:I

    #@1d
    .line 2947
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getAddress()Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    #@20
    move-result-object v2

    #@21
    iput-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->destinationAddress:Lcom/android/internal/telephony/SmsAddress;

    #@23
    .line 2951
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@26
    move-result v2

    #@27
    iput v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->protocolIdentifier:I

    #@29
    .line 2955
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    #@2c
    move-result v2

    #@2d
    iput v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@2f
    .line 2958
    const/4 v1, 0x0

    #@30
    .line 2959
    .local v1, tmp:I
    iget v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->vpFormatPresent:I

    #@32
    const/4 v5, 0x2

    #@33
    if-ne v2, v5, :cond_4a

    #@35
    .line 2960
    invoke-virtual {p1, v3}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->countVpoctets(I)I

    #@38
    move-result v1

    #@39
    .line 2965
    :cond_39
    :goto_39
    sget-wide v5, Lcom/android/internal/telephony/gsm/SmsMessage;->timeSmsOnSim:J

    #@3b
    iput-wide v5, p0, Lcom/android/internal/telephony/SmsMessageBase;->scTimeMillis:J

    #@3d
    .line 2967
    and-int/lit8 v2, p2, 0x40

    #@3f
    const/16 v5, 0x40

    #@41
    if-ne v2, v5, :cond_59

    #@43
    move v0, v3

    #@44
    .line 2969
    .local v0, hasUserDataHeader:Z
    :goto_44
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->parseUserData(Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;Z)V

    #@47
    .line 2970
    return-void

    #@48
    .end local v0           #hasUserDataHeader:Z
    .end local v1           #tmp:I
    :cond_48
    move v2, v4

    #@49
    .line 2940
    goto :goto_9

    #@4a
    .line 2961
    .restart local v1       #tmp:I
    :cond_4a
    iget v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->vpFormatPresent:I

    #@4c
    if-eq v2, v3, :cond_53

    #@4e
    iget v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->vpFormatPresent:I

    #@50
    const/4 v5, 0x3

    #@51
    if-ne v2, v5, :cond_39

    #@53
    .line 2962
    :cond_53
    const/4 v2, 0x7

    #@54
    invoke-virtual {p1, v2}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->countVpoctets(I)I

    #@57
    move-result v1

    #@58
    goto :goto_39

    #@59
    :cond_59
    move v0, v4

    #@5a
    .line 2967
    goto :goto_44
.end method

.method private parseUserData(Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;Z)V
    .registers 21
    .parameter "p"
    .parameter "hasUserDataHeader"

    #@0
    .prologue
    .line 3110
    const/4 v6, 0x0

    #@1
    .line 3111
    .local v6, hasMessageClass:Z
    const/4 v14, 0x0

    #@2
    .line 3113
    .local v14, userDataCompressed:Z
    const/4 v5, 0x0

    #@3
    .line 3116
    .local v5, encodingType:I
    move-object/from16 v0, p0

    #@5
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@7
    and-int/lit16 v15, v15, 0x80

    #@9
    if-nez v15, :cond_14e

    #@b
    .line 3118
    move-object/from16 v0, p0

    #@d
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@f
    and-int/lit8 v15, v15, 0x40

    #@11
    if-eqz v15, :cond_fb

    #@13
    const/4 v15, 0x1

    #@14
    :goto_14
    move-object/from16 v0, p0

    #@16
    iput-boolean v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->automaticDeletion:Z

    #@18
    .line 3119
    move-object/from16 v0, p0

    #@1a
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@1c
    and-int/lit8 v15, v15, 0x20

    #@1e
    if-eqz v15, :cond_fe

    #@20
    const/4 v14, 0x1

    #@21
    .line 3120
    :goto_21
    move-object/from16 v0, p0

    #@23
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@25
    and-int/lit8 v15, v15, 0x10

    #@27
    if-eqz v15, :cond_101

    #@29
    const/4 v6, 0x1

    #@2a
    .line 3122
    :goto_2a
    if-eqz v14, :cond_104

    #@2c
    .line 3123
    const-string v15, "GSM"

    #@2e
    new-instance v16, Ljava/lang/StringBuilder;

    #@30
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v17, "4 - Unsupported SMS data coding scheme (compression) "

    #@35
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v16

    #@39
    move-object/from16 v0, p0

    #@3b
    iget v0, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@3d
    move/from16 v17, v0

    #@3f
    move/from16 v0, v17

    #@41
    and-int/lit16 v0, v0, 0xff

    #@43
    move/from16 v17, v0

    #@45
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v16

    #@49
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v16

    #@4d
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 3251
    :cond_50
    :goto_50
    const/4 v15, 0x1

    #@51
    if-ne v5, v15, :cond_308

    #@53
    const/4 v15, 0x1

    #@54
    :goto_54
    move-object/from16 v0, p1

    #@56
    move/from16 v1, p2

    #@58
    invoke-virtual {v0, v1, v15}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->constructUserData(ZZ)I

    #@5b
    move-result v3

    #@5c
    .line 3253
    .local v3, count:I
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getUserData()[B

    #@5f
    move-result-object v15

    #@60
    move-object/from16 v0, p0

    #@62
    iput-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->userData:[B

    #@64
    .line 3254
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@67
    move-result-object v15

    #@68
    move-object/from16 v0, p0

    #@6a
    iput-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@6c
    .line 3264
    if-eqz p2, :cond_3ac

    #@6e
    move-object/from16 v0, p0

    #@70
    iget-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@72
    iget-object v15, v15, Lcom/android/internal/telephony/SmsHeader;->specialSmsMsgList:Ljava/util/ArrayList;

    #@74
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    #@77
    move-result v15

    #@78
    if-eqz v15, :cond_3ac

    #@7a
    .line 3265
    move-object/from16 v0, p0

    #@7c
    iget-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@7e
    iget-object v15, v15, Lcom/android/internal/telephony/SmsHeader;->specialSmsMsgList:Ljava/util/ArrayList;

    #@80
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@83
    move-result-object v7

    #@84
    .local v7, i$:Ljava/util/Iterator;
    :goto_84
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@87
    move-result v15

    #@88
    if-eqz v15, :cond_3ac

    #@8a
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8d
    move-result-object v11

    #@8e
    check-cast v11, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;

    #@90
    .line 3266
    .local v11, msg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    iget v15, v11, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;->msgIndType:I

    #@92
    and-int/lit16 v12, v15, 0xff

    #@94
    .line 3273
    .local v12, msgInd:I
    if-eqz v12, :cond_9a

    #@96
    const/16 v15, 0x80

    #@98
    if-ne v12, v15, :cond_394

    #@9a
    .line 3274
    :cond_9a
    const/4 v15, 0x1

    #@9b
    move-object/from16 v0, p0

    #@9d
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->isMwi:Z

    #@9f
    .line 3277
    and-int/lit16 v15, v12, 0x80

    #@a1
    if-nez v15, :cond_30b

    #@a3
    const/4 v15, 0x1

    #@a4
    :goto_a4
    move-object/from16 v0, p0

    #@a6
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->mwiDontStore:Z

    #@a8
    .line 3279
    const/16 v15, 0x80

    #@aa
    if-ne v12, v15, :cond_30e

    #@ac
    .line 3281
    const/4 v15, 0x0

    #@ad
    move-object/from16 v0, p0

    #@af
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->mwiDontStore:Z

    #@b1
    .line 3312
    :cond_b1
    :goto_b1
    iget v15, v11, Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;->msgCount:I

    #@b3
    and-int/lit16 v15, v15, 0xff

    #@b5
    move-object/from16 v0, p0

    #@b7
    iput v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->mVoiceMailCount:I

    #@b9
    .line 3320
    move-object/from16 v0, p0

    #@bb
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->mVoiceMailCount:I

    #@bd
    if-lez v15, :cond_38d

    #@bf
    .line 3321
    const/4 v15, 0x1

    #@c0
    move-object/from16 v0, p0

    #@c2
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->mwiSense:Z

    #@c4
    .line 3325
    :goto_c4
    new-instance v15, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v16, "parseUserData(), MWI in TP-UDH for Vmail. Msg Ind = "

    #@cb
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v15

    #@cf
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v15

    #@d3
    const-string v16, " Dont store = "

    #@d5
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v15

    #@d9
    move-object/from16 v0, p0

    #@db
    iget-boolean v0, v0, Lcom/android/internal/telephony/SmsMessageBase;->mwiDontStore:Z

    #@dd
    move/from16 v16, v0

    #@df
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v15

    #@e3
    const-string v16, " Vmail count = "

    #@e5
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v15

    #@e9
    move-object/from16 v0, p0

    #@eb
    iget v0, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->mVoiceMailCount:I

    #@ed
    move/from16 v16, v0

    #@ef
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v15

    #@f3
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f6
    move-result-object v15

    #@f7
    invoke-static {v15}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@fa
    goto :goto_84

    #@fb
    .line 3118
    .end local v3           #count:I
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v11           #msg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    .end local v12           #msgInd:I
    :cond_fb
    const/4 v15, 0x0

    #@fc
    goto/16 :goto_14

    #@fe
    .line 3119
    :cond_fe
    const/4 v14, 0x0

    #@ff
    goto/16 :goto_21

    #@101
    .line 3120
    :cond_101
    const/4 v6, 0x0

    #@102
    goto/16 :goto_2a

    #@104
    .line 3126
    :cond_104
    move-object/from16 v0, p0

    #@106
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@108
    shr-int/lit8 v15, v15, 0x2

    #@10a
    and-int/lit8 v15, v15, 0x3

    #@10c
    packed-switch v15, :pswitch_data_4d8

    #@10f
    goto/16 :goto_50

    #@111
    .line 3128
    :pswitch_111
    const/4 v5, 0x1

    #@112
    .line 3129
    goto/16 :goto_50

    #@114
    .line 3132
    :pswitch_114
    const/4 v5, 0x3

    #@115
    .line 3133
    goto/16 :goto_50

    #@117
    .line 3137
    :pswitch_117
    const/4 v15, 0x0

    #@118
    const-string v16, "KSC5601Decoding"

    #@11a
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@11d
    move-result v15

    #@11e
    const/16 v16, 0x1

    #@120
    move/from16 v0, v16

    #@122
    if-ne v15, v0, :cond_127

    #@124
    .line 3138
    const/4 v5, 0x2

    #@125
    .line 3139
    goto/16 :goto_50

    #@127
    .line 3143
    :cond_127
    :pswitch_127
    const-string v15, "GSM"

    #@129
    new-instance v16, Ljava/lang/StringBuilder;

    #@12b
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@12e
    const-string v17, "1 - Unsupported SMS data coding scheme "

    #@130
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v16

    #@134
    move-object/from16 v0, p0

    #@136
    iget v0, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@138
    move/from16 v17, v0

    #@13a
    move/from16 v0, v17

    #@13c
    and-int/lit16 v0, v0, 0xff

    #@13e
    move/from16 v17, v0

    #@140
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@143
    move-result-object v16

    #@144
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@147
    move-result-object v16

    #@148
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14b
    .line 3145
    const/4 v5, 0x2

    #@14c
    goto/16 :goto_50

    #@14e
    .line 3149
    :cond_14e
    move-object/from16 v0, p0

    #@150
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@152
    and-int/lit16 v15, v15, 0xf0

    #@154
    const/16 v16, 0xf0

    #@156
    move/from16 v0, v16

    #@158
    if-ne v15, v0, :cond_16f

    #@15a
    .line 3150
    const/4 v15, 0x0

    #@15b
    move-object/from16 v0, p0

    #@15d
    iput-boolean v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->automaticDeletion:Z

    #@15f
    .line 3151
    const/4 v6, 0x1

    #@160
    .line 3152
    const/4 v14, 0x0

    #@161
    .line 3154
    move-object/from16 v0, p0

    #@163
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@165
    and-int/lit8 v15, v15, 0x4

    #@167
    if-nez v15, :cond_16c

    #@169
    .line 3156
    const/4 v5, 0x1

    #@16a
    goto/16 :goto_50

    #@16c
    .line 3159
    :cond_16c
    const/4 v5, 0x2

    #@16d
    goto/16 :goto_50

    #@16f
    .line 3161
    :cond_16f
    move-object/from16 v0, p0

    #@171
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@173
    and-int/lit16 v15, v15, 0xf0

    #@175
    const/16 v16, 0xc0

    #@177
    move/from16 v0, v16

    #@179
    if-eq v15, v0, :cond_193

    #@17b
    move-object/from16 v0, p0

    #@17d
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@17f
    and-int/lit16 v15, v15, 0xf0

    #@181
    const/16 v16, 0xd0

    #@183
    move/from16 v0, v16

    #@185
    if-eq v15, v0, :cond_193

    #@187
    move-object/from16 v0, p0

    #@189
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@18b
    and-int/lit16 v15, v15, 0xf0

    #@18d
    const/16 v16, 0xe0

    #@18f
    move/from16 v0, v16

    #@191
    if-ne v15, v0, :cond_266

    #@193
    .line 3170
    :cond_193
    move-object/from16 v0, p0

    #@195
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@197
    and-int/lit16 v15, v15, 0xf0

    #@199
    const/16 v16, 0xe0

    #@19b
    move/from16 v0, v16

    #@19d
    if-ne v15, v0, :cond_1da

    #@19f
    .line 3171
    const/4 v5, 0x3

    #@1a0
    .line 3176
    :goto_1a0
    const/4 v14, 0x0

    #@1a1
    .line 3177
    move-object/from16 v0, p0

    #@1a3
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@1a5
    and-int/lit8 v15, v15, 0x8

    #@1a7
    const/16 v16, 0x8

    #@1a9
    move/from16 v0, v16

    #@1ab
    if-ne v15, v0, :cond_1dc

    #@1ad
    const/4 v2, 0x1

    #@1ae
    .line 3181
    .local v2, active:Z
    :goto_1ae
    move-object/from16 v0, p0

    #@1b0
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@1b2
    and-int/lit8 v15, v15, 0x3

    #@1b4
    if-nez v15, :cond_1e7

    #@1b6
    .line 3182
    const/4 v15, 0x1

    #@1b7
    move-object/from16 v0, p0

    #@1b9
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->isMwi:Z

    #@1bb
    .line 3183
    move-object/from16 v0, p0

    #@1bd
    iput-boolean v2, v0, Lcom/android/internal/telephony/SmsMessageBase;->mwiSense:Z

    #@1bf
    .line 3184
    move-object/from16 v0, p0

    #@1c1
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@1c3
    and-int/lit16 v15, v15, 0xf0

    #@1c5
    const/16 v16, 0xc0

    #@1c7
    move/from16 v0, v16

    #@1c9
    if-ne v15, v0, :cond_1de

    #@1cb
    const/4 v15, 0x1

    #@1cc
    :goto_1cc
    move-object/from16 v0, p0

    #@1ce
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->mwiDontStore:Z

    #@1d0
    .line 3187
    const/4 v15, 0x1

    #@1d1
    if-ne v2, v15, :cond_1e0

    #@1d3
    .line 3188
    const/4 v15, -0x1

    #@1d4
    move-object/from16 v0, p0

    #@1d6
    iput v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->mVoiceMailCount:I

    #@1d8
    goto/16 :goto_50

    #@1da
    .line 3173
    .end local v2           #active:Z
    :cond_1da
    const/4 v5, 0x1

    #@1db
    goto :goto_1a0

    #@1dc
    .line 3177
    :cond_1dc
    const/4 v2, 0x0

    #@1dd
    goto :goto_1ae

    #@1de
    .line 3184
    .restart local v2       #active:Z
    :cond_1de
    const/4 v15, 0x0

    #@1df
    goto :goto_1cc

    #@1e0
    .line 3190
    :cond_1e0
    const/4 v15, 0x0

    #@1e1
    move-object/from16 v0, p0

    #@1e3
    iput v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->mVoiceMailCount:I

    #@1e5
    goto/16 :goto_50

    #@1e7
    .line 3195
    :cond_1e7
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@1ea
    move-result-object v8

    #@1eb
    .line 3197
    .local v8, mSimInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    const/4 v15, 0x0

    #@1ec
    const-string v16, "voicemailWaitingIndication_MEGAFON"

    #@1ee
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1f1
    move-result v15

    #@1f2
    const/16 v16, 0x1

    #@1f4
    move/from16 v0, v16

    #@1f6
    if-ne v15, v0, :cond_23b

    #@1f8
    invoke-virtual {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@1fb
    move-result-object v15

    #@1fc
    if-eqz v15, :cond_23b

    #@1fe
    invoke-virtual {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@201
    move-result-object v15

    #@202
    const-string v16, "250"

    #@204
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@207
    move-result v15

    #@208
    if-eqz v15, :cond_23b

    #@20a
    invoke-virtual {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMnc()Ljava/lang/String;

    #@20d
    move-result-object v15

    #@20e
    if-eqz v15, :cond_23b

    #@210
    invoke-virtual {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMnc()Ljava/lang/String;

    #@213
    move-result-object v15

    #@214
    const-string v16, "02"

    #@216
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@219
    move-result v15

    #@21a
    if-eqz v15, :cond_23b

    #@21c
    .line 3200
    const-string v15, "parseUserData(), MEGAFON RU, parse as voicemail message"

    #@21e
    invoke-static {v15}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@221
    .line 3201
    const/4 v15, 0x1

    #@222
    move-object/from16 v0, p0

    #@224
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->isMwi:Z

    #@226
    .line 3202
    move-object/from16 v0, p0

    #@228
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@22a
    and-int/lit16 v15, v15, 0xf0

    #@22c
    const/16 v16, 0xc0

    #@22e
    move/from16 v0, v16

    #@230
    if-ne v15, v0, :cond_239

    #@232
    const/4 v15, 0x1

    #@233
    :goto_233
    move-object/from16 v0, p0

    #@235
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->mwiDontStore:Z

    #@237
    goto/16 :goto_50

    #@239
    :cond_239
    const/4 v15, 0x0

    #@23a
    goto :goto_233

    #@23b
    .line 3205
    :cond_23b
    const/4 v15, 0x0

    #@23c
    move-object/from16 v0, p0

    #@23e
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->isMwi:Z

    #@240
    .line 3207
    const-string v15, "GSM"

    #@242
    new-instance v16, Ljava/lang/StringBuilder;

    #@244
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@247
    const-string v17, "MWI for fax, email, or other "

    #@249
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24c
    move-result-object v16

    #@24d
    move-object/from16 v0, p0

    #@24f
    iget v0, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@251
    move/from16 v17, v0

    #@253
    move/from16 v0, v17

    #@255
    and-int/lit16 v0, v0, 0xff

    #@257
    move/from16 v17, v0

    #@259
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25c
    move-result-object v16

    #@25d
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@260
    move-result-object v16

    #@261
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@264
    goto/16 :goto_50

    #@266
    .line 3213
    .end local v2           #active:Z
    .end local v8           #mSimInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    :cond_266
    move-object/from16 v0, p0

    #@268
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@26a
    and-int/lit16 v15, v15, 0xc0

    #@26c
    const/16 v16, 0x80

    #@26e
    move/from16 v0, v16

    #@270
    if-ne v15, v0, :cond_2e2

    #@272
    .line 3216
    move-object/from16 v0, p0

    #@274
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@276
    const/16 v16, 0x84

    #@278
    move/from16 v0, v16

    #@27a
    if-ne v15, v0, :cond_2af

    #@27c
    .line 3218
    const/4 v5, 0x4

    #@27d
    .line 3224
    :goto_27d
    const/4 v15, 0x0

    #@27e
    const-string v16, "KSC5601Decoding"

    #@280
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@283
    move-result v15

    #@284
    const/16 v16, 0x1

    #@286
    move/from16 v0, v16

    #@288
    if-ne v15, v0, :cond_50

    #@28a
    .line 3225
    const/4 v15, 0x0

    #@28b
    move-object/from16 v0, p0

    #@28d
    iput-boolean v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->automaticDeletion:Z

    #@28f
    .line 3226
    const/4 v6, 0x0

    #@290
    .line 3227
    const/4 v14, 0x0

    #@291
    .line 3228
    move-object/from16 v0, p0

    #@293
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@295
    and-int/lit16 v4, v15, 0xf0

    #@297
    .line 3229
    .local v4, dataCodingScheme_byte:I
    const/16 v15, 0x80

    #@299
    if-eq v4, v15, :cond_29f

    #@29b
    const/16 v15, 0x90

    #@29d
    if-ne v4, v15, :cond_2d7

    #@29f
    .line 3230
    :cond_29f
    move-object/from16 v0, p0

    #@2a1
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@2a3
    shr-int/lit8 v15, v15, 0x2

    #@2a5
    and-int/lit8 v15, v15, 0x3

    #@2a7
    packed-switch v15, :pswitch_data_4e4

    #@2aa
    goto/16 :goto_50

    #@2ac
    .line 3234
    :pswitch_2ac
    const/4 v5, 0x1

    #@2ad
    .line 3235
    goto/16 :goto_50

    #@2af
    .line 3220
    .end local v4           #dataCodingScheme_byte:I
    :cond_2af
    const-string v15, "GSM"

    #@2b1
    new-instance v16, Ljava/lang/StringBuilder;

    #@2b3
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@2b6
    const-string v17, "5 - Unsupported SMS data coding scheme "

    #@2b8
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v16

    #@2bc
    move-object/from16 v0, p0

    #@2be
    iget v0, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@2c0
    move/from16 v17, v0

    #@2c2
    move/from16 v0, v17

    #@2c4
    and-int/lit16 v0, v0, 0xff

    #@2c6
    move/from16 v17, v0

    #@2c8
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2cb
    move-result-object v16

    #@2cc
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2cf
    move-result-object v16

    #@2d0
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2d3
    goto :goto_27d

    #@2d4
    .line 3237
    .restart local v4       #dataCodingScheme_byte:I
    :pswitch_2d4
    const/4 v5, 0x2

    #@2d5
    goto/16 :goto_50

    #@2d7
    .line 3240
    :cond_2d7
    const/16 v15, 0xa0

    #@2d9
    if-eq v4, v15, :cond_2df

    #@2db
    const/16 v15, 0xb0

    #@2dd
    if-ne v4, v15, :cond_50

    #@2df
    .line 3241
    :cond_2df
    const/4 v5, 0x1

    #@2e0
    goto/16 :goto_50

    #@2e2
    .line 3246
    .end local v4           #dataCodingScheme_byte:I
    :cond_2e2
    const-string v15, "GSM"

    #@2e4
    new-instance v16, Ljava/lang/StringBuilder;

    #@2e6
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@2e9
    const-string v17, "3 - Unsupported SMS data coding scheme "

    #@2eb
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ee
    move-result-object v16

    #@2ef
    move-object/from16 v0, p0

    #@2f1
    iget v0, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@2f3
    move/from16 v17, v0

    #@2f5
    move/from16 v0, v17

    #@2f7
    and-int/lit16 v0, v0, 0xff

    #@2f9
    move/from16 v17, v0

    #@2fb
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2fe
    move-result-object v16

    #@2ff
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@302
    move-result-object v16

    #@303
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@306
    goto/16 :goto_50

    #@308
    .line 3251
    :cond_308
    const/4 v15, 0x0

    #@309
    goto/16 :goto_54

    #@30b
    .line 3277
    .restart local v3       #count:I
    .restart local v7       #i$:Ljava/util/Iterator;
    .restart local v11       #msg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    .restart local v12       #msgInd:I
    :cond_30b
    const/4 v15, 0x0

    #@30c
    goto/16 :goto_a4

    #@30e
    .line 3283
    :cond_30e
    const/4 v15, 0x0

    #@30f
    const-string v16, "KRVMSType"

    #@311
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@314
    move-result v15

    #@315
    const/16 v16, 0x1

    #@317
    move/from16 v0, v16

    #@319
    if-ne v15, v0, :cond_360

    #@31b
    .line 3284
    if-nez v12, :cond_b1

    #@31d
    .line 3285
    const-string v15, "gsm.sim.operator.numeric"

    #@31f
    invoke-static {v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@322
    move-result-object v10

    #@323
    .line 3286
    .local v10, mccmnc:Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    #@325
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@328
    const-string v16, "parseUserData(), [KR_VMS] mccmnc : "

    #@32a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32d
    move-result-object v15

    #@32e
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@331
    move-result-object v15

    #@332
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@335
    move-result-object v15

    #@336
    invoke-static {v15}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@339
    .line 3287
    const-string v15, "45008"

    #@33b
    invoke-virtual {v15, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33e
    move-result v15

    #@33f
    if-nez v15, :cond_359

    #@341
    const-string v15, "45002"

    #@343
    invoke-virtual {v15, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@346
    move-result v15

    #@347
    if-nez v15, :cond_359

    #@349
    const-string v15, "45005"

    #@34b
    invoke-virtual {v15, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34e
    move-result v15

    #@34f
    if-nez v15, :cond_359

    #@351
    const-string v15, ""

    #@353
    invoke-virtual {v15, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@356
    move-result v15

    #@357
    if-eqz v15, :cond_b1

    #@359
    .line 3290
    :cond_359
    const/4 v15, 0x0

    #@35a
    move-object/from16 v0, p0

    #@35c
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->mwiDontStore:Z

    #@35e
    goto/16 :goto_b1

    #@360
    .line 3294
    .end local v10           #mccmnc:Ljava/lang/String;
    :cond_360
    move-object/from16 v0, p0

    #@362
    iget-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->mwiDontStore:Z

    #@364
    if-nez v15, :cond_b1

    #@366
    .line 3302
    move-object/from16 v0, p0

    #@368
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@36a
    and-int/lit16 v15, v15, 0xf0

    #@36c
    const/16 v16, 0xd0

    #@36e
    move/from16 v0, v16

    #@370
    if-eq v15, v0, :cond_37e

    #@372
    move-object/from16 v0, p0

    #@374
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@376
    and-int/lit16 v15, v15, 0xf0

    #@378
    const/16 v16, 0xe0

    #@37a
    move/from16 v0, v16

    #@37c
    if-ne v15, v0, :cond_386

    #@37e
    :cond_37e
    move-object/from16 v0, p0

    #@380
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@382
    and-int/lit8 v15, v15, 0x3

    #@384
    if-eqz v15, :cond_b1

    #@386
    .line 3308
    :cond_386
    const/4 v15, 0x1

    #@387
    move-object/from16 v0, p0

    #@389
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->mwiDontStore:Z

    #@38b
    goto/16 :goto_b1

    #@38d
    .line 3323
    :cond_38d
    const/4 v15, 0x0

    #@38e
    move-object/from16 v0, p0

    #@390
    iput-boolean v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->mwiSense:Z

    #@392
    goto/16 :goto_c4

    #@394
    .line 3335
    :cond_394
    new-instance v15, Ljava/lang/StringBuilder;

    #@396
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@399
    const-string v16, "parseUserData(), TP_UDH fax/email/extended msg/multisubscriber profile. Msg Ind = "

    #@39b
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39e
    move-result-object v15

    #@39f
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a2
    move-result-object v15

    #@3a3
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a6
    move-result-object v15

    #@3a7
    invoke-static {v15}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@3aa
    goto/16 :goto_84

    #@3ac
    .line 3341
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v11           #msg:Lcom/android/internal/telephony/SmsHeader$SpecialSmsMsg;
    .end local v12           #msgInd:I
    :cond_3ac
    packed-switch v5, :pswitch_data_4f0

    #@3af
    .line 3381
    :goto_3af
    move-object/from16 v0, p0

    #@3b1
    iget-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@3b3
    if-eqz v15, :cond_3b8

    #@3b5
    .line 3382
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->parseMessageBody()V

    #@3b8
    .line 3386
    :cond_3b8
    const/4 v15, 0x0

    #@3b9
    const-string v16, "dcm_service_type_ota_dm"

    #@3bb
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3be
    move-result v15

    #@3bf
    const/16 v16, 0x1

    #@3c1
    move/from16 v0, v16

    #@3c3
    if-ne v15, v0, :cond_3d3

    #@3c5
    .line 3387
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getProtocolIdentifier()I

    #@3c8
    move-result v13

    #@3c9
    .line 3388
    .local v13, protocolIdentifier:I
    const/16 v15, 0x7d

    #@3cb
    if-ne v13, v15, :cond_3d3

    #@3cd
    .line 3389
    const-string v15, ""

    #@3cf
    move-object/from16 v0, p0

    #@3d1
    iput-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@3d3
    .line 3395
    .end local v13           #protocolIdentifier:I
    :cond_3d3
    const/4 v15, 0x0

    #@3d4
    const-string v16, "doNotUse_Class2"

    #@3d6
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3d9
    move-result v15

    #@3da
    const/16 v16, 0x1

    #@3dc
    move/from16 v0, v16

    #@3de
    if-ne v15, v0, :cond_4a2

    #@3e0
    .line 3396
    if-nez v6, :cond_49a

    #@3e2
    .line 3397
    sget-object v15, Lcom/android/internal/telephony/SmsConstants$MessageClass;->UNKNOWN:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@3e4
    move-object/from16 v0, p0

    #@3e6
    iput-object v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->messageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@3e8
    .line 3425
    :goto_3e8
    return-void

    #@3e9
    .line 3345
    :pswitch_3e9
    new-instance v15, Ljava/lang/StringBuilder;

    #@3eb
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@3ee
    const-string v16, "parseUserData(), ENCODING_8BIT : messageBody is other binary data format encodingType="

    #@3f0
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f3
    move-result-object v15

    #@3f4
    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f7
    move-result-object v15

    #@3f8
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3fb
    move-result-object v15

    #@3fc
    invoke-static {v15}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@3ff
    .line 3347
    const/4 v9, 0x0

    #@400
    .line 3348
    .local v9, mccCode:Ljava/lang/String;
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@403
    move-result-object v8

    #@404
    .line 3349
    .restart local v8       #mSimInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    invoke-virtual {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@407
    move-result-object v15

    #@408
    if-eqz v15, :cond_43c

    #@40a
    invoke-virtual {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@40d
    move-result-object v15

    #@40e
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@411
    move-result v15

    #@412
    if-nez v15, :cond_43c

    #@414
    invoke-virtual {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@417
    move-result-object v15

    #@418
    const-string v16, "450"

    #@41a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41d
    move-result v15

    #@41e
    if-eqz v15, :cond_43c

    #@420
    .line 3350
    const/4 v15, 0x0

    #@421
    const-string v16, "KSC5601Decoding"

    #@423
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@426
    move-result v15

    #@427
    if-eqz v15, :cond_435

    #@429
    .line 3351
    move-object/from16 v0, p1

    #@42b
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getUserDataKSC5601(I)Ljava/lang/String;

    #@42e
    move-result-object v15

    #@42f
    move-object/from16 v0, p0

    #@431
    iput-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@433
    goto/16 :goto_3af

    #@435
    .line 3353
    :cond_435
    const/4 v15, 0x0

    #@436
    move-object/from16 v0, p0

    #@438
    iput-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@43a
    goto/16 :goto_3af

    #@43c
    .line 3355
    :cond_43c
    const/4 v15, 0x0

    #@43d
    const-string v16, "handle8bit"

    #@43f
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@442
    move-result v15

    #@443
    const/16 v16, 0x1

    #@445
    move/from16 v0, v16

    #@447
    if-ne v15, v0, :cond_455

    #@449
    .line 3356
    move-object/from16 v0, p1

    #@44b
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getUserDataGSM8Bit(I)Ljava/lang/String;

    #@44e
    move-result-object v15

    #@44f
    move-object/from16 v0, p0

    #@451
    iput-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@453
    goto/16 :goto_3af

    #@455
    .line 3360
    :cond_455
    const/4 v15, 0x0

    #@456
    move-object/from16 v0, p0

    #@458
    iput-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@45a
    goto/16 :goto_3af

    #@45c
    .line 3365
    .end local v8           #mSimInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    .end local v9           #mccCode:Ljava/lang/String;
    :pswitch_45c
    if-eqz p2, :cond_47c

    #@45e
    move-object/from16 v0, p0

    #@460
    iget-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@462
    iget v15, v15, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@464
    move/from16 v16, v15

    #@466
    :goto_466
    if-eqz p2, :cond_480

    #@468
    move-object/from16 v0, p0

    #@46a
    iget-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@46c
    iget v15, v15, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@46e
    :goto_46e
    move-object/from16 v0, p1

    #@470
    move/from16 v1, v16

    #@472
    invoke-virtual {v0, v3, v1, v15}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getUserDataGSM7Bit(III)Ljava/lang/String;

    #@475
    move-result-object v15

    #@476
    move-object/from16 v0, p0

    #@478
    iput-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@47a
    goto/16 :goto_3af

    #@47c
    :cond_47c
    const/4 v15, 0x0

    #@47d
    move/from16 v16, v15

    #@47f
    goto :goto_466

    #@480
    :cond_480
    const/4 v15, 0x0

    #@481
    goto :goto_46e

    #@482
    .line 3371
    :pswitch_482
    move-object/from16 v0, p1

    #@484
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getUserDataUCS2(I)Ljava/lang/String;

    #@487
    move-result-object v15

    #@488
    move-object/from16 v0, p0

    #@48a
    iput-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@48c
    goto/16 :goto_3af

    #@48e
    .line 3375
    :pswitch_48e
    move-object/from16 v0, p1

    #@490
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getUserDataKSC5601(I)Ljava/lang/String;

    #@493
    move-result-object v15

    #@494
    move-object/from16 v0, p0

    #@496
    iput-object v15, v0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@498
    goto/16 :goto_3af

    #@49a
    .line 3399
    :cond_49a
    sget-object v15, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_1:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@49c
    move-object/from16 v0, p0

    #@49e
    iput-object v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->messageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4a0
    goto/16 :goto_3e8

    #@4a2
    .line 3403
    :cond_4a2
    if-nez v6, :cond_4ac

    #@4a4
    .line 3404
    sget-object v15, Lcom/android/internal/telephony/SmsConstants$MessageClass;->UNKNOWN:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4a6
    move-object/from16 v0, p0

    #@4a8
    iput-object v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->messageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4aa
    goto/16 :goto_3e8

    #@4ac
    .line 3406
    :cond_4ac
    move-object/from16 v0, p0

    #@4ae
    iget v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@4b0
    and-int/lit8 v15, v15, 0x3

    #@4b2
    packed-switch v15, :pswitch_data_4fe

    #@4b5
    goto/16 :goto_3e8

    #@4b7
    .line 3408
    :pswitch_4b7
    sget-object v15, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_0:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4b9
    move-object/from16 v0, p0

    #@4bb
    iput-object v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->messageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4bd
    goto/16 :goto_3e8

    #@4bf
    .line 3411
    :pswitch_4bf
    sget-object v15, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_1:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4c1
    move-object/from16 v0, p0

    #@4c3
    iput-object v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->messageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4c5
    goto/16 :goto_3e8

    #@4c7
    .line 3414
    :pswitch_4c7
    sget-object v15, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_2:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4c9
    move-object/from16 v0, p0

    #@4cb
    iput-object v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->messageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4cd
    goto/16 :goto_3e8

    #@4cf
    .line 3417
    :pswitch_4cf
    sget-object v15, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_3:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4d1
    move-object/from16 v0, p0

    #@4d3
    iput-object v15, v0, Lcom/android/internal/telephony/gsm/SmsMessage;->messageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4d5
    goto/16 :goto_3e8

    #@4d7
    .line 3126
    nop

    #@4d8
    :pswitch_data_4d8
    .packed-switch 0x0
        :pswitch_111
        :pswitch_117
        :pswitch_114
        :pswitch_127
    .end packed-switch

    #@4e4
    .line 3230
    :pswitch_data_4e4
    .packed-switch 0x0
        :pswitch_2ac
        :pswitch_2d4
        :pswitch_2ac
        :pswitch_2ac
    .end packed-switch

    #@4f0
    .line 3341
    :pswitch_data_4f0
    .packed-switch 0x0
        :pswitch_3e9
        :pswitch_45c
        :pswitch_3e9
        :pswitch_482
        :pswitch_48e
    .end packed-switch

    #@4fe
    .line 3406
    :pswitch_data_4fe
    .packed-switch 0x0
        :pswitch_4b7
        :pswitch_4bf
        :pswitch_4c7
        :pswitch_4cf
    .end packed-switch
.end method

.method public static setTimeforSMSonUSIM(J)V
    .registers 2
    .parameter "timemillisec"

    #@0
    .prologue
    .line 2979
    sput-wide p0, Lcom/android/internal/telephony/gsm/SmsMessage;->timeSmsOnSim:J

    #@2
    .line 2980
    return-void
.end method


# virtual methods
.method public getConfirmReadInfo()Landroid/os/Bundle;
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3093
    const-string v3, "GsmSmsKTConfirmRead"

    #@3
    move-object v1, v2

    #@4
    check-cast v1, [[B

    #@6
    invoke-static {v3, v2, v2, p0, v1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@9
    move-result-object v0

    #@a
    .line 3094
    .local v0, confirmReadMessage:Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    if-nez v0, :cond_d

    #@c
    .line 3098
    :goto_c
    return-object v2

    #@d
    :cond_d
    invoke-interface {v0}, Lcom/android/internal/telephony/SmsOperatorBasicMessage;->getInformation()Landroid/os/Bundle;

    #@10
    move-result-object v2

    #@11
    goto :goto_c
.end method

.method public getDataCodingScheme()I
    .registers 2

    #@0
    .prologue
    .line 2783
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->dataCodingScheme:I

    #@2
    return v0
.end method

.method public getMessageClass()Lcom/android/internal/telephony/SmsConstants$MessageClass;
    .registers 2

    #@0
    .prologue
    .line 3432
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->messageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@2
    return-object v0
.end method

.method public getNumOfVoicemails()I
    .registers 2

    #@0
    .prologue
    .line 3538
    iget-boolean v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->isMwi:Z

    #@2
    if-nez v0, :cond_21

    #@4
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->isCphsMwiMessage()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_21

    #@a
    .line 3539
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@c
    if-eqz v0, :cond_24

    #@e
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@10
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    #@12
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;->isCphsVoiceMessageSet()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_24

    #@18
    .line 3541
    const/16 v0, 0xff

    #@1a
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->mVoiceMailCount:I

    #@1c
    .line 3545
    :goto_1c
    const-string v0, "getNumOfVoicemails(), CPHS voice mail message"

    #@1e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@21
    .line 3547
    :cond_21
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->mVoiceMailCount:I

    #@23
    return v0

    #@24
    .line 3543
    :cond_24
    const/4 v0, 0x0

    #@25
    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->mVoiceMailCount:I

    #@27
    goto :goto_1c
.end method

.method public getOriginalAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3647
    invoke-super {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 3648
    .local v0, originatingAddress:Ljava/lang/String;
    if-nez v0, :cond_8

    #@6
    .line 3649
    const-string v0, ""

    #@8
    .line 3651
    .end local v0           #originatingAddress:Ljava/lang/String;
    :cond_8
    return-object v0
.end method

.method public getOriginatingAddress()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 3621
    const-string v1, "kr_address_spec"

    #@4
    invoke-static {v2, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_3b

    #@a
    .line 3622
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->mti:I

    #@c
    if-eq v1, v3, :cond_15

    #@e
    .line 3623
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getreplyAddress()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 3624
    .local v0, uiAddress:Ljava/lang/String;
    if-eqz v0, :cond_15

    #@14
    .line 3637
    .end local v0           #uiAddress:Ljava/lang/String;
    :goto_14
    return-object v0

    #@15
    .line 3629
    :cond_15
    const-string v1, "lgu_address_spec"

    #@17
    invoke-static {v2, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_36

    #@1d
    .line 3630
    new-instance v1, Lcom/android/internal/telephony/uicc/UsimService;

    #@1f
    invoke-direct {v1}, Lcom/android/internal/telephony/uicc/UsimService;-><init>()V

    #@22
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UsimService;->getUsimType()I

    #@25
    move-result v1

    #@26
    const/4 v2, 0x5

    #@27
    if-ne v1, v2, :cond_36

    #@29
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@30
    move-result v1

    #@31
    if-ne v1, v3, :cond_36

    #@33
    .line 3632
    const-string v0, ""

    #@35
    goto :goto_14

    #@36
    .line 3635
    :cond_36
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getOriginalAddress()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    goto :goto_14

    #@3b
    .line 3637
    :cond_3b
    invoke-super {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    goto :goto_14
.end method

.method public getProtocolIdentifier()I
    .registers 2

    #@0
    .prologue
    .line 2771
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->protocolIdentifier:I

    #@2
    return v0
.end method

.method public getSmsDisplayMode()I
    .registers 2

    #@0
    .prologue
    .line 3455
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public getSpecialMessageInfo()Landroid/os/Bundle;
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3076
    const-string v4, "GsmSmsKRSpecialMessage"

    #@3
    move-object v2, v3

    #@4
    check-cast v2, [[B

    #@6
    invoke-static {v4, v3, v3, p0, v2}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@9
    move-result-object v1

    #@a
    .line 3077
    .local v1, specialMessage:Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    if-nez v1, :cond_d

    #@c
    .line 3082
    :goto_c
    return-object v3

    #@d
    .line 3081
    :cond_d
    invoke-interface {v1}, Lcom/android/internal/telephony/SmsOperatorBasicMessage;->getInformation()Landroid/os/Bundle;

    #@10
    move-result-object v0

    #@11
    .local v0, info:Landroid/os/Bundle;
    move-object v3, v0

    #@12
    .line 3082
    goto :goto_c
.end method

.method public getStatus()I
    .registers 2

    #@0
    .prologue
    .line 2873
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->status:I

    #@2
    return v0
.end method

.method public getreplyAddress()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3507
    const-string v5, "GsmSmsKTReplyAddress"

    #@3
    move-object v3, v4

    #@4
    check-cast v3, [[B

    #@6
    invoke-static {v5, v4, v4, p0, v3}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@9
    move-result-object v1

    #@a
    .line 3508
    .local v1, replyAddress:Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    if-nez v1, :cond_d

    #@c
    .line 3524
    :cond_c
    :goto_c
    return-object v4

    #@d
    .line 3512
    :cond_d
    invoke-interface {v1}, Lcom/android/internal/telephony/SmsOperatorBasicMessage;->getInformation()Landroid/os/Bundle;

    #@10
    move-result-object v0

    #@11
    .line 3513
    .local v0, info:Landroid/os/Bundle;
    if-eqz v0, :cond_c

    #@13
    .line 3517
    const-string v3, "valid"

    #@15
    const/4 v4, 0x0

    #@16
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@19
    move-result v3

    #@1a
    const/4 v4, 0x1

    #@1b
    if-ne v3, v4, :cond_2a

    #@1d
    .line 3518
    const-string v3, "reply_address"

    #@1f
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    .line 3519
    .local v2, tmpReplyAddress:Ljava/lang/String;
    if-nez v2, :cond_28

    #@25
    .line 3520
    const-string v4, ""

    #@27
    goto :goto_c

    #@28
    :cond_28
    move-object v4, v2

    #@29
    .line 3522
    goto :goto_c

    #@2a
    .line 3524
    .end local v2           #tmpReplyAddress:Ljava/lang/String;
    :cond_2a
    const-string v4, ""

    #@2c
    goto :goto_c
.end method

.method public isCphsMwiMessage()Z
    .registers 2

    #@0
    .prologue
    .line 2797
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@2
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;->isCphsVoiceMessageClear()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_14

    #@a
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@c
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;->isCphsVoiceMessageSet()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_16

    #@14
    :cond_14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public isMWIClearMessage()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2804
    iget-boolean v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->isMwi:Z

    #@3
    if-eqz v0, :cond_a

    #@5
    iget-boolean v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->mwiSense:Z

    #@7
    if-nez v0, :cond_a

    #@9
    .line 2808
    :goto_9
    return v1

    #@a
    :cond_a
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@c
    if-eqz v0, :cond_1b

    #@e
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@10
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    #@12
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;->isCphsVoiceMessageClear()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1b

    #@18
    move v0, v1

    #@19
    :goto_19
    move v1, v0

    #@1a
    goto :goto_9

    #@1b
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_19
.end method

.method public isMWISetMessage()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2815
    iget-boolean v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->isMwi:Z

    #@3
    if-eqz v0, :cond_a

    #@5
    iget-boolean v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->mwiSense:Z

    #@7
    if-eqz v0, :cond_a

    #@9
    .line 2819
    :goto_9
    return v1

    #@a
    :cond_a
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@c
    if-eqz v0, :cond_1b

    #@e
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@10
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    #@12
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;->isCphsVoiceMessageSet()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1b

    #@18
    move v0, v1

    #@19
    :goto_19
    move v1, v0

    #@1a
    goto :goto_9

    #@1b
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_19
.end method

.method public isMwiDontStore()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 2826
    iget-boolean v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->isMwi:Z

    #@4
    if-eqz v2, :cond_b

    #@6
    iget-boolean v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->mwiDontStore:Z

    #@8
    if-eqz v2, :cond_b

    #@a
    .line 2855
    :cond_a
    :goto_a
    return v0

    #@b
    .line 2830
    :cond_b
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->isCphsMwiMessage()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_40

    #@11
    .line 2836
    const/4 v2, 0x0

    #@12
    const-string v3, "Canada_voicemail"

    #@14
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_33

    #@1a
    .line 2839
    const-string v2, " "

    #@1c
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_2c

    #@26
    .line 2840
    const-string v1, "isMwiDontStore(), CPHS with no userdata. No Display SMS."

    #@28
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2b
    goto :goto_a

    #@2c
    .line 2844
    :cond_2c
    const-string v0, "isMwiDontStore(), CPHS with userdata. Dispaly SMS."

    #@2e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@31
    move v0, v1

    #@32
    .line 2845
    goto :goto_a

    #@33
    .line 2849
    :cond_33
    const-string v1, " "

    #@35
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v1

    #@3d
    if-eqz v1, :cond_a

    #@3f
    goto :goto_a

    #@40
    :cond_40
    move v0, v1

    #@41
    .line 2855
    goto :goto_a
.end method

.method public isMwiUrgentMessage()Z
    .registers 2

    #@0
    .prologue
    .line 2865
    const-string v0, "isMwiUrgentMessage(), is not supported in GSM mode."

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@5
    .line 2866
    const/4 v0, 0x0

    #@6
    return v0
.end method

.method public isReplace()Z
    .registers 3

    #@0
    .prologue
    .line 2789
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->protocolIdentifier:I

    #@2
    and-int/lit16 v0, v0, 0xc0

    #@4
    const/16 v1, 0x40

    #@6
    if-ne v0, v1, :cond_18

    #@8
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->protocolIdentifier:I

    #@a
    and-int/lit8 v0, v0, 0x3f

    #@c
    if-lez v0, :cond_18

    #@e
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->protocolIdentifier:I

    #@10
    and-int/lit8 v0, v0, 0x3f

    #@12
    const/16 v1, 0x8

    #@14
    if-ge v0, v1, :cond_18

    #@16
    const/4 v0, 0x1

    #@17
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method public isReplyPathPresent()Z
    .registers 2

    #@0
    .prologue
    .line 2885
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->replyPathPresent:Z

    #@2
    return v0
.end method

.method public isStatusReportMessage()Z
    .registers 2

    #@0
    .prologue
    .line 2879
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->isStatusReportMessage:Z

    #@2
    return v0
.end method

.method public isTypeZero()Z
    .registers 3

    #@0
    .prologue
    .line 537
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->protocolIdentifier:I

    #@2
    const/16 v1, 0x40

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method isUsimDataDownload()Z
    .registers 3

    #@0
    .prologue
    .line 3442
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->messageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@2
    sget-object v1, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_2:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@4
    if-ne v0, v1, :cond_14

    #@6
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->protocolIdentifier:I

    #@8
    const/16 v1, 0x7f

    #@a
    if-eq v0, v1, :cond_12

    #@c
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage;->protocolIdentifier:I

    #@e
    const/16 v1, 0x7c

    #@10
    if-ne v0, v1, :cond_14

    #@12
    :cond_12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method
