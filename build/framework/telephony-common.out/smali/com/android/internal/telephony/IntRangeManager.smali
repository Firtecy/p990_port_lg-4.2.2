.class public abstract Lcom/android/internal/telephony/IntRangeManager;
.super Ljava/lang/Object;
.source "IntRangeManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/IntRangeManager$ClientRange;,
        Lcom/android/internal/telephony/IntRangeManager$IntRange;
    }
.end annotation


# static fields
.field private static final INITIAL_CLIENTS_ARRAY_SIZE:I = 0x4


# instance fields
.field private mRanges:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/IntRangeManager$IntRange;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 181
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 179
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@a
    .line 181
    return-void
.end method

.method private populateAllClientRanges()V
    .registers 10

    #@0
    .prologue
    .line 634
    iget-object v6, p0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v3

    #@6
    .line 635
    .local v3, len:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v3, :cond_30

    #@9
    .line 636
    iget-object v6, p0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v5

    #@f
    check-cast v5, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@11
    .line 638
    .local v5, range:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    iget-object v6, v5, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v0

    #@17
    .line 639
    .local v0, clientLen:I
    const/4 v2, 0x0

    #@18
    .local v2, j:I
    :goto_18
    if-ge v2, v0, :cond_2d

    #@1a
    .line 640
    iget-object v6, v5, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v4

    #@20
    check-cast v4, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@22
    .line 641
    .local v4, nextRange:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    iget v6, v4, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@24
    iget v7, v4, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@26
    const/4 v8, 0x1

    #@27
    invoke-virtual {p0, v6, v7, v8}, Lcom/android/internal/telephony/IntRangeManager;->addRange(IIZ)V

    #@2a
    .line 639
    add-int/lit8 v2, v2, 0x1

    #@2c
    goto :goto_18

    #@2d
    .line 635
    .end local v4           #nextRange:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    :cond_2d
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_7

    #@30
    .line 644
    .end local v0           #clientLen:I
    .end local v2           #j:I
    .end local v5           #range:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_30
    return-void
.end method

.method private populateAllRanges()V
    .registers 6

    #@0
    .prologue
    .line 621
    iget-object v2, p0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .line 623
    .local v1, itr:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/internal/telephony/IntRangeManager$IntRange;>;"
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1b

    #@c
    .line 624
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@12
    .line 625
    .local v0, currRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    iget v2, v0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@14
    iget v3, v0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@16
    const/4 v4, 0x1

    #@17
    invoke-virtual {p0, v2, v3, v4}, Lcom/android/internal/telephony/IntRangeManager;->addRange(IIZ)V

    #@1a
    goto :goto_6

    #@1b
    .line 627
    .end local v0           #currRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_1b
    return-void
.end method


# virtual methods
.method protected abstract addRange(IIZ)V
.end method

.method public declared-synchronized disableRange(IILjava/lang/String;)Z
    .registers 23
    .parameter "startId"
    .parameter "endId"
    .parameter "client"

    #@0
    .prologue
    .line 438
    monitor-enter p0

    #@1
    :try_start_1
    move-object/from16 v0, p0

    #@3
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@5
    move-object/from16 v17, v0

    #@7
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v9

    #@b
    .line 440
    .local v9, len:I
    const/4 v7, 0x0

    #@c
    .local v7, i:I
    :goto_c
    if-ge v7, v9, :cond_1d7

    #@e
    .line 441
    move-object/from16 v0, p0

    #@10
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@12
    move-object/from16 v17, v0

    #@14
    move-object/from16 v0, v17

    #@16
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v14

    #@1a
    check-cast v14, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@1c
    .line 442
    .local v14, range:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    iget v0, v14, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@1e
    move/from16 v17, v0
    :try_end_20
    .catchall {:try_start_1 .. :try_end_20} :catchall_17b

    #@20
    move/from16 v0, p1

    #@22
    move/from16 v1, v17

    #@24
    if-ge v0, v1, :cond_2a

    #@26
    .line 443
    const/16 v17, 0x0

    #@28
    .line 576
    .end local v14           #range:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :goto_28
    monitor-exit p0

    #@29
    return v17

    #@2a
    .line 444
    .restart local v14       #range:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_2a
    :try_start_2a
    iget v0, v14, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@2c
    move/from16 v17, v0

    #@2e
    move/from16 v0, p2

    #@30
    move/from16 v1, v17

    #@32
    if-gt v0, v1, :cond_1d3

    #@34
    .line 447
    iget-object v2, v14, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@36
    .line 450
    .local v2, clients:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/IntRangeManager$ClientRange;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@39
    move-result v5

    #@3a
    .line 451
    .local v5, crLength:I
    const/16 v17, 0x1

    #@3c
    move/from16 v0, v17

    #@3e
    if-ne v5, v0, :cond_91

    #@40
    .line 452
    const/16 v17, 0x0

    #@42
    move/from16 v0, v17

    #@44
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@47
    move-result-object v3

    #@48
    check-cast v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@4a
    .line 453
    .local v3, cr:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    iget v0, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@4c
    move/from16 v17, v0

    #@4e
    move/from16 v0, v17

    #@50
    move/from16 v1, p1

    #@52
    if-ne v0, v1, :cond_8e

    #@54
    iget v0, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@56
    move/from16 v17, v0

    #@58
    move/from16 v0, v17

    #@5a
    move/from16 v1, p2

    #@5c
    if-ne v0, v1, :cond_8e

    #@5e
    iget-object v0, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->client:Ljava/lang/String;

    #@60
    move-object/from16 v17, v0

    #@62
    move-object/from16 v0, v17

    #@64
    move-object/from16 v1, p3

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v17

    #@6a
    if-eqz v17, :cond_8e

    #@6c
    .line 456
    move-object/from16 v0, p0

    #@6e
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@70
    move-object/from16 v17, v0

    #@72
    move-object/from16 v0, v17

    #@74
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@77
    .line 457
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/IntRangeManager;->updateRanges()Z

    #@7a
    move-result v17

    #@7b
    if-eqz v17, :cond_80

    #@7d
    .line 458
    const/16 v17, 0x1

    #@7f
    goto :goto_28

    #@80
    .line 461
    :cond_80
    move-object/from16 v0, p0

    #@82
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@84
    move-object/from16 v17, v0

    #@86
    move-object/from16 v0, v17

    #@88
    invoke-virtual {v0, v7, v14}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@8b
    .line 462
    const/16 v17, 0x0

    #@8d
    goto :goto_28

    #@8e
    .line 465
    :cond_8e
    const/16 v17, 0x0

    #@90
    goto :goto_28

    #@91
    .line 475
    .end local v3           #cr:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    :cond_91
    const/high16 v8, -0x8000

    #@93
    .line 476
    .local v8, largestEndId:I
    const/16 v16, 0x0

    #@95
    .line 479
    .local v16, updateStarted:Z
    const/4 v4, 0x0

    #@96
    .local v4, crIndex:I
    :goto_96
    if-ge v4, v5, :cond_1d3

    #@98
    .line 480
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@9b
    move-result-object v3

    #@9c
    check-cast v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@9e
    .line 481
    .restart local v3       #cr:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    iget v0, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@a0
    move/from16 v17, v0

    #@a2
    move/from16 v0, v17

    #@a4
    move/from16 v1, p1

    #@a6
    if-ne v0, v1, :cond_1c5

    #@a8
    iget v0, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@aa
    move/from16 v17, v0

    #@ac
    move/from16 v0, v17

    #@ae
    move/from16 v1, p2

    #@b0
    if-ne v0, v1, :cond_1c5

    #@b2
    iget-object v0, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->client:Ljava/lang/String;

    #@b4
    move-object/from16 v17, v0

    #@b6
    move-object/from16 v0, v17

    #@b8
    move-object/from16 v1, p3

    #@ba
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bd
    move-result v17

    #@be
    if-eqz v17, :cond_1c5

    #@c0
    .line 483
    add-int/lit8 v17, v5, -0x1

    #@c2
    move/from16 v0, v17

    #@c4
    if-ne v4, v0, :cond_f3

    #@c6
    .line 484
    iget v0, v14, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@c8
    move/from16 v17, v0

    #@ca
    move/from16 v0, v17

    #@cc
    if-ne v0, v8, :cond_d5

    #@ce
    .line 487
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@d1
    .line 488
    const/16 v17, 0x1

    #@d3
    goto/16 :goto_28

    #@d5
    .line 491
    :cond_d5
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@d8
    .line 492
    iput v8, v14, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@da
    .line 493
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/IntRangeManager;->updateRanges()Z

    #@dd
    move-result v17

    #@de
    if-eqz v17, :cond_e4

    #@e0
    .line 494
    const/16 v17, 0x1

    #@e2
    goto/16 :goto_28

    #@e4
    .line 496
    :cond_e4
    invoke-virtual {v2, v4, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@e7
    .line 497
    iget v0, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@e9
    move/from16 v17, v0

    #@eb
    move/from16 v0, v17

    #@ed
    iput v0, v14, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@ef
    .line 498
    const/16 v17, 0x0

    #@f1
    goto/16 :goto_28

    #@f3
    .line 506
    :cond_f3
    new-instance v15, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@f5
    move-object/from16 v0, p0

    #@f7
    invoke-direct {v15, v0, v14, v4}, Lcom/android/internal/telephony/IntRangeManager$IntRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;Lcom/android/internal/telephony/IntRangeManager$IntRange;I)V

    #@fa
    .line 508
    .local v15, rangeCopy:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    if-nez v4, :cond_124

    #@fc
    .line 514
    const/16 v17, 0x1

    #@fe
    move/from16 v0, v17

    #@100
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@103
    move-result-object v17

    #@104
    check-cast v17, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@106
    move-object/from16 v0, v17

    #@108
    iget v13, v0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@10a
    .line 515
    .local v13, nextStartId:I
    iget v0, v14, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@10c
    move/from16 v17, v0

    #@10e
    move/from16 v0, v17

    #@110
    if-eq v13, v0, :cond_116

    #@112
    .line 516
    const/16 v16, 0x1

    #@114
    .line 517
    iput v13, v15, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@116
    .line 520
    :cond_116
    const/16 v17, 0x1

    #@118
    move/from16 v0, v17

    #@11a
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11d
    move-result-object v17

    #@11e
    check-cast v17, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@120
    move-object/from16 v0, v17

    #@122
    iget v8, v0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@124
    .line 527
    .end local v13           #nextStartId:I
    :cond_124
    new-instance v10, Ljava/util/ArrayList;

    #@126
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@129
    .line 529
    .local v10, newRanges:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/IntRangeManager$IntRange;>;"
    move-object v6, v15

    #@12a
    .line 530
    .local v6, currentRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    add-int/lit8 v12, v4, 0x1

    #@12c
    .local v12, nextIndex:I
    :goto_12c
    if-ge v12, v5, :cond_17e

    #@12e
    .line 531
    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@131
    move-result-object v11

    #@132
    check-cast v11, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@134
    .line 532
    .local v11, nextCr:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    iget v0, v11, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@136
    move/from16 v17, v0

    #@138
    add-int/lit8 v18, v8, 0x1

    #@13a
    move/from16 v0, v17

    #@13c
    move/from16 v1, v18

    #@13e
    if-le v0, v1, :cond_15b

    #@140
    .line 533
    const/16 v16, 0x1

    #@142
    .line 534
    iput v8, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@144
    .line 535
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@147
    .line 536
    new-instance v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@149
    .end local v6           #currentRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    move-object/from16 v0, p0

    #@14b
    invoke-direct {v6, v0, v11}, Lcom/android/internal/telephony/IntRangeManager$IntRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;Lcom/android/internal/telephony/IntRangeManager$ClientRange;)V

    #@14e
    .line 543
    .restart local v6       #currentRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :goto_14e
    iget v0, v11, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@150
    move/from16 v17, v0

    #@152
    move/from16 v0, v17

    #@154
    if-le v0, v8, :cond_158

    #@156
    .line 544
    iget v8, v11, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@158
    .line 530
    :cond_158
    add-int/lit8 v12, v12, 0x1

    #@15a
    goto :goto_12c

    #@15b
    .line 538
    :cond_15b
    iget v0, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@15d
    move/from16 v17, v0

    #@15f
    iget v0, v11, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@161
    move/from16 v18, v0

    #@163
    move/from16 v0, v17

    #@165
    move/from16 v1, v18

    #@167
    if-ge v0, v1, :cond_171

    #@169
    .line 539
    iget v0, v11, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@16b
    move/from16 v17, v0

    #@16d
    move/from16 v0, v17

    #@16f
    iput v0, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@171
    .line 541
    :cond_171
    iget-object v0, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@173
    move-object/from16 v17, v0

    #@175
    move-object/from16 v0, v17

    #@177
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_17a
    .catchall {:try_start_2a .. :try_end_17a} :catchall_17b

    #@17a
    goto :goto_14e

    #@17b
    .line 438
    .end local v2           #clients:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/IntRangeManager$ClientRange;>;"
    .end local v3           #cr:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    .end local v4           #crIndex:I
    .end local v5           #crLength:I
    .end local v6           #currentRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    .end local v7           #i:I
    .end local v8           #largestEndId:I
    .end local v9           #len:I
    .end local v10           #newRanges:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/IntRangeManager$IntRange;>;"
    .end local v11           #nextCr:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    .end local v12           #nextIndex:I
    .end local v14           #range:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    .end local v15           #rangeCopy:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    .end local v16           #updateStarted:Z
    :catchall_17b
    move-exception v17

    #@17c
    monitor-exit p0

    #@17d
    throw v17

    #@17e
    .line 549
    .restart local v2       #clients:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/IntRangeManager$ClientRange;>;"
    .restart local v3       #cr:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    .restart local v4       #crIndex:I
    .restart local v5       #crLength:I
    .restart local v6       #currentRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    .restart local v7       #i:I
    .restart local v8       #largestEndId:I
    .restart local v9       #len:I
    .restart local v10       #newRanges:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/IntRangeManager$IntRange;>;"
    .restart local v12       #nextIndex:I
    .restart local v14       #range:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    .restart local v15       #rangeCopy:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    .restart local v16       #updateStarted:Z
    :cond_17e
    move/from16 v0, p2

    #@180
    if-ge v8, v0, :cond_186

    #@182
    .line 550
    const/16 v16, 0x1

    #@184
    .line 551
    :try_start_184
    iput v8, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@186
    .line 553
    :cond_186
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@189
    .line 556
    move-object/from16 v0, p0

    #@18b
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@18d
    move-object/from16 v17, v0

    #@18f
    move-object/from16 v0, v17

    #@191
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@194
    .line 557
    move-object/from16 v0, p0

    #@196
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@198
    move-object/from16 v17, v0

    #@19a
    move-object/from16 v0, v17

    #@19c
    invoke-virtual {v0, v7, v10}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    #@19f
    .line 558
    if-eqz v16, :cond_1c1

    #@1a1
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/IntRangeManager;->updateRanges()Z

    #@1a4
    move-result v17

    #@1a5
    if-nez v17, :cond_1c1

    #@1a7
    .line 560
    move-object/from16 v0, p0

    #@1a9
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@1ab
    move-object/from16 v17, v0

    #@1ad
    move-object/from16 v0, v17

    #@1af
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    #@1b2
    .line 561
    move-object/from16 v0, p0

    #@1b4
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@1b6
    move-object/from16 v17, v0

    #@1b8
    move-object/from16 v0, v17

    #@1ba
    invoke-virtual {v0, v7, v14}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@1bd
    .line 562
    const/16 v17, 0x0

    #@1bf
    goto/16 :goto_28

    #@1c1
    .line 565
    :cond_1c1
    const/16 v17, 0x1

    #@1c3
    goto/16 :goto_28

    #@1c5
    .line 568
    .end local v6           #currentRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    .end local v10           #newRanges:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/IntRangeManager$IntRange;>;"
    .end local v12           #nextIndex:I
    .end local v15           #rangeCopy:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_1c5
    iget v0, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@1c7
    move/from16 v17, v0

    #@1c9
    move/from16 v0, v17

    #@1cb
    if-le v0, v8, :cond_1cf

    #@1cd
    .line 569
    iget v8, v3, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I
    :try_end_1cf
    .catchall {:try_start_184 .. :try_end_1cf} :catchall_17b

    #@1cf
    .line 479
    :cond_1cf
    add-int/lit8 v4, v4, 0x1

    #@1d1
    goto/16 :goto_96

    #@1d3
    .line 440
    .end local v2           #clients:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/IntRangeManager$ClientRange;>;"
    .end local v3           #cr:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    .end local v4           #crIndex:I
    .end local v5           #crLength:I
    .end local v8           #largestEndId:I
    .end local v16           #updateStarted:Z
    :cond_1d3
    add-int/lit8 v7, v7, 0x1

    #@1d5
    goto/16 :goto_c

    #@1d7
    .line 576
    .end local v14           #range:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_1d7
    const/16 v17, 0x0

    #@1d9
    goto/16 :goto_28
.end method

.method public declared-synchronized enableRange(IILjava/lang/String;)Z
    .registers 24
    .parameter "startId"
    .parameter "endId"
    .parameter "client"

    #@0
    .prologue
    .line 194
    monitor-enter p0

    #@1
    :try_start_1
    move-object/from16 v0, p0

    #@3
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@5
    move-object/from16 v17, v0

    #@7
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v10

    #@b
    .line 197
    .local v10, len:I
    if-nez v10, :cond_3c

    #@d
    .line 198
    const/16 v17, 0x1

    #@f
    move-object/from16 v0, p0

    #@11
    move/from16 v1, p1

    #@13
    move/from16 v2, p2

    #@15
    move/from16 v3, v17

    #@17
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/IntRangeManager;->tryAddRanges(IIZ)Z

    #@1a
    move-result v17

    #@1b
    if-eqz v17, :cond_39

    #@1d
    .line 199
    move-object/from16 v0, p0

    #@1f
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@21
    move-object/from16 v17, v0

    #@23
    new-instance v18, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@25
    move-object/from16 v0, v18

    #@27
    move-object/from16 v1, p0

    #@29
    move/from16 v2, p1

    #@2b
    move/from16 v3, p2

    #@2d
    move-object/from16 v4, p3

    #@2f
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/IntRangeManager$IntRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@32
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_35
    .catchall {:try_start_1 .. :try_end_35} :catchall_42a

    #@35
    .line 200
    const/16 v17, 0x1

    #@37
    .line 423
    :goto_37
    monitor-exit p0

    #@38
    return v17

    #@39
    .line 202
    :cond_39
    const/16 v17, 0x0

    #@3b
    goto :goto_37

    #@3c
    .line 206
    :cond_3c
    const/4 v14, 0x0

    #@3d
    .local v14, startIndex:I
    :goto_3d
    if-ge v14, v10, :cond_3fa

    #@3f
    .line 207
    :try_start_3f
    move-object/from16 v0, p0

    #@41
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@43
    move-object/from16 v17, v0

    #@45
    move-object/from16 v0, v17

    #@47
    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4a
    move-result-object v13

    #@4b
    check-cast v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@4d
    .line 208
    .local v13, range:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@4f
    move/from16 v17, v0

    #@51
    move/from16 v0, p1

    #@53
    move/from16 v1, v17

    #@55
    if-lt v0, v1, :cond_78

    #@57
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@59
    move/from16 v17, v0

    #@5b
    move/from16 v0, p2

    #@5d
    move/from16 v1, v17

    #@5f
    if-gt v0, v1, :cond_78

    #@61
    .line 213
    new-instance v17, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@63
    move-object/from16 v0, v17

    #@65
    move-object/from16 v1, p0

    #@67
    move/from16 v2, p1

    #@69
    move/from16 v3, p2

    #@6b
    move-object/from16 v4, p3

    #@6d
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/IntRangeManager$ClientRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@70
    move-object/from16 v0, v17

    #@72
    invoke-virtual {v13, v0}, Lcom/android/internal/telephony/IntRangeManager$IntRange;->insert(Lcom/android/internal/telephony/IntRangeManager$ClientRange;)V

    #@75
    .line 214
    const/16 v17, 0x1

    #@77
    goto :goto_37

    #@78
    .line 215
    :cond_78
    add-int/lit8 v17, p1, -0x1

    #@7a
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@7c
    move/from16 v18, v0

    #@7e
    move/from16 v0, v17

    #@80
    move/from16 v1, v18

    #@82
    if-ne v0, v1, :cond_115

    #@84
    .line 218
    move/from16 v11, p2

    #@86
    .line 219
    .local v11, newRangeEndId:I
    const/4 v12, 0x0

    #@87
    .line 220
    .local v12, nextRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    add-int/lit8 v17, v14, 0x1

    #@89
    move/from16 v0, v17

    #@8b
    if-ge v0, v10, :cond_b7

    #@8d
    .line 221
    move-object/from16 v0, p0

    #@8f
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@91
    move-object/from16 v17, v0

    #@93
    add-int/lit8 v18, v14, 0x1

    #@95
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@98
    move-result-object v12

    #@99
    .end local v12           #nextRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    check-cast v12, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@9b
    .line 222
    .restart local v12       #nextRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    iget v0, v12, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@9d
    move/from16 v17, v0

    #@9f
    add-int/lit8 v17, v17, -0x1

    #@a1
    move/from16 v0, v17

    #@a3
    move/from16 v1, p2

    #@a5
    if-gt v0, v1, :cond_10f

    #@a7
    .line 224
    iget v0, v12, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@a9
    move/from16 v17, v0

    #@ab
    move/from16 v0, p2

    #@ad
    move/from16 v1, v17

    #@af
    if-gt v0, v1, :cond_b7

    #@b1
    .line 226
    iget v0, v12, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@b3
    move/from16 v17, v0

    #@b5
    add-int/lit8 v11, v17, -0x1

    #@b7
    .line 233
    :cond_b7
    :goto_b7
    const/16 v17, 0x1

    #@b9
    move-object/from16 v0, p0

    #@bb
    move/from16 v1, p1

    #@bd
    move/from16 v2, v17

    #@bf
    invoke-virtual {v0, v1, v11, v2}, Lcom/android/internal/telephony/IntRangeManager;->tryAddRanges(IIZ)Z

    #@c2
    move-result v17

    #@c3
    if-eqz v17, :cond_111

    #@c5
    .line 234
    move/from16 v0, p2

    #@c7
    iput v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@c9
    .line 235
    new-instance v17, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@cb
    move-object/from16 v0, v17

    #@cd
    move-object/from16 v1, p0

    #@cf
    move/from16 v2, p1

    #@d1
    move/from16 v3, p2

    #@d3
    move-object/from16 v4, p3

    #@d5
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/IntRangeManager$ClientRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@d8
    move-object/from16 v0, v17

    #@da
    invoke-virtual {v13, v0}, Lcom/android/internal/telephony/IntRangeManager$IntRange;->insert(Lcom/android/internal/telephony/IntRangeManager$ClientRange;)V

    #@dd
    .line 238
    if-eqz v12, :cond_10b

    #@df
    .line 239
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@e1
    move/from16 v17, v0

    #@e3
    iget v0, v12, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@e5
    move/from16 v18, v0

    #@e7
    move/from16 v0, v17

    #@e9
    move/from16 v1, v18

    #@eb
    if-ge v0, v1, :cond_f5

    #@ed
    .line 241
    iget v0, v12, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@ef
    move/from16 v17, v0

    #@f1
    move/from16 v0, v17

    #@f3
    iput v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@f5
    .line 243
    :cond_f5
    iget-object v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@f7
    move-object/from16 v17, v0

    #@f9
    iget-object v0, v12, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@fb
    move-object/from16 v18, v0

    #@fd
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@100
    .line 244
    move-object/from16 v0, p0

    #@102
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@104
    move-object/from16 v17, v0

    #@106
    move-object/from16 v0, v17

    #@108
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@10b
    .line 246
    :cond_10b
    const/16 v17, 0x1

    #@10d
    goto/16 :goto_37

    #@10f
    .line 230
    :cond_10f
    const/4 v12, 0x0

    #@110
    goto :goto_b7

    #@111
    .line 248
    :cond_111
    const/16 v17, 0x0

    #@113
    goto/16 :goto_37

    #@115
    .line 250
    .end local v11           #newRangeEndId:I
    .end local v12           #nextRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_115
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@117
    move/from16 v17, v0

    #@119
    move/from16 v0, p1

    #@11b
    move/from16 v1, v17

    #@11d
    if-ge v0, v1, :cond_2dd

    #@11f
    .line 254
    add-int/lit8 v17, p2, 0x1

    #@121
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@123
    move/from16 v18, v0

    #@125
    move/from16 v0, v17

    #@127
    move/from16 v1, v18

    #@129
    if-ge v0, v1, :cond_15f

    #@12b
    .line 257
    const/16 v17, 0x1

    #@12d
    move-object/from16 v0, p0

    #@12f
    move/from16 v1, p1

    #@131
    move/from16 v2, p2

    #@133
    move/from16 v3, v17

    #@135
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/IntRangeManager;->tryAddRanges(IIZ)Z

    #@138
    move-result v17

    #@139
    if-eqz v17, :cond_15b

    #@13b
    .line 258
    move-object/from16 v0, p0

    #@13d
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@13f
    move-object/from16 v17, v0

    #@141
    new-instance v18, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@143
    move-object/from16 v0, v18

    #@145
    move-object/from16 v1, p0

    #@147
    move/from16 v2, p1

    #@149
    move/from16 v3, p2

    #@14b
    move-object/from16 v4, p3

    #@14d
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/IntRangeManager$IntRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@150
    move-object/from16 v0, v17

    #@152
    move-object/from16 v1, v18

    #@154
    invoke-virtual {v0, v14, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@157
    .line 259
    const/16 v17, 0x1

    #@159
    goto/16 :goto_37

    #@15b
    .line 261
    :cond_15b
    const/16 v17, 0x0

    #@15d
    goto/16 :goto_37

    #@15f
    .line 263
    :cond_15f
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@161
    move/from16 v17, v0

    #@163
    move/from16 v0, p2

    #@165
    move/from16 v1, v17

    #@167
    if-gt v0, v1, :cond_199

    #@169
    .line 266
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@16b
    move/from16 v17, v0

    #@16d
    add-int/lit8 v17, v17, -0x1

    #@16f
    const/16 v18, 0x1

    #@171
    move-object/from16 v0, p0

    #@173
    move/from16 v1, p1

    #@175
    move/from16 v2, v17

    #@177
    move/from16 v3, v18

    #@179
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/IntRangeManager;->tryAddRanges(IIZ)Z

    #@17c
    move-result v17

    #@17d
    if-eqz v17, :cond_195

    #@17f
    .line 267
    move/from16 v0, p1

    #@181
    iput v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@183
    .line 268
    iget-object v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@185
    move-object/from16 v17, v0

    #@187
    const/16 v18, 0x0

    #@189
    new-instance v19, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@18b
    invoke-direct/range {v19 .. v23}, Lcom/android/internal/telephony/IntRangeManager$ClientRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@18e
    invoke-virtual/range {v17 .. v19}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@191
    .line 269
    const/16 v17, 0x1

    #@193
    goto/16 :goto_37

    #@195
    .line 271
    :cond_195
    const/16 v17, 0x0

    #@197
    goto/16 :goto_37

    #@199
    .line 275
    :cond_199
    add-int/lit8 v5, v14, 0x1

    #@19b
    .local v5, endIndex:I
    :goto_19b
    if-ge v5, v10, :cond_283

    #@19d
    .line 276
    move-object/from16 v0, p0

    #@19f
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@1a1
    move-object/from16 v17, v0

    #@1a3
    move-object/from16 v0, v17

    #@1a5
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a8
    move-result-object v6

    #@1a9
    check-cast v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@1ab
    .line 277
    .local v6, endRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    add-int/lit8 v17, p2, 0x1

    #@1ad
    iget v0, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@1af
    move/from16 v18, v0

    #@1b1
    move/from16 v0, v17

    #@1b3
    move/from16 v1, v18

    #@1b5
    if-ge v0, v1, :cond_211

    #@1b7
    .line 280
    const/16 v17, 0x1

    #@1b9
    move-object/from16 v0, p0

    #@1bb
    move/from16 v1, p1

    #@1bd
    move/from16 v2, p2

    #@1bf
    move/from16 v3, v17

    #@1c1
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/IntRangeManager;->tryAddRanges(IIZ)Z

    #@1c4
    move-result v17

    #@1c5
    if-eqz v17, :cond_20d

    #@1c7
    .line 281
    move/from16 v0, p1

    #@1c9
    iput v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@1cb
    .line 282
    move/from16 v0, p2

    #@1cd
    iput v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@1cf
    .line 284
    iget-object v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@1d1
    move-object/from16 v17, v0

    #@1d3
    const/16 v18, 0x0

    #@1d5
    new-instance v19, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@1d7
    invoke-direct/range {v19 .. v23}, Lcom/android/internal/telephony/IntRangeManager$ClientRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@1da
    invoke-virtual/range {v17 .. v19}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@1dd
    .line 290
    add-int/lit8 v8, v14, 0x1

    #@1df
    .line 291
    .local v8, joinIndex:I
    move v7, v8

    #@1e0
    .local v7, i:I
    :goto_1e0
    if-ge v7, v5, :cond_209

    #@1e2
    .line 293
    move-object/from16 v0, p0

    #@1e4
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@1e6
    move-object/from16 v17, v0

    #@1e8
    move-object/from16 v0, v17

    #@1ea
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1ed
    move-result-object v9

    #@1ee
    check-cast v9, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@1f0
    .line 294
    .local v9, joinRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    iget-object v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@1f2
    move-object/from16 v17, v0

    #@1f4
    iget-object v0, v9, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@1f6
    move-object/from16 v18, v0

    #@1f8
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@1fb
    .line 295
    move-object/from16 v0, p0

    #@1fd
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@1ff
    move-object/from16 v17, v0

    #@201
    move-object/from16 v0, v17

    #@203
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@206
    .line 291
    add-int/lit8 v7, v7, 0x1

    #@208
    goto :goto_1e0

    #@209
    .line 297
    .end local v9           #joinRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_209
    const/16 v17, 0x1

    #@20b
    goto/16 :goto_37

    #@20d
    .line 299
    .end local v7           #i:I
    .end local v8           #joinIndex:I
    :cond_20d
    const/16 v17, 0x0

    #@20f
    goto/16 :goto_37

    #@211
    .line 301
    :cond_211
    iget v0, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@213
    move/from16 v17, v0

    #@215
    move/from16 v0, p2

    #@217
    move/from16 v1, v17

    #@219
    if-gt v0, v1, :cond_27f

    #@21b
    .line 305
    iget v0, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@21d
    move/from16 v17, v0

    #@21f
    add-int/lit8 v17, v17, -0x1

    #@221
    const/16 v18, 0x1

    #@223
    move-object/from16 v0, p0

    #@225
    move/from16 v1, p1

    #@227
    move/from16 v2, v17

    #@229
    move/from16 v3, v18

    #@22b
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/IntRangeManager;->tryAddRanges(IIZ)Z

    #@22e
    move-result v17

    #@22f
    if-eqz v17, :cond_27b

    #@231
    .line 306
    move/from16 v0, p1

    #@233
    iput v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@235
    .line 307
    iget v0, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@237
    move/from16 v17, v0

    #@239
    move/from16 v0, v17

    #@23b
    iput v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@23d
    .line 309
    iget-object v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@23f
    move-object/from16 v17, v0

    #@241
    const/16 v18, 0x0

    #@243
    new-instance v19, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@245
    invoke-direct/range {v19 .. v23}, Lcom/android/internal/telephony/IntRangeManager$ClientRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@248
    invoke-virtual/range {v17 .. v19}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@24b
    .line 315
    add-int/lit8 v8, v14, 0x1

    #@24d
    .line 316
    .restart local v8       #joinIndex:I
    move v7, v8

    #@24e
    .restart local v7       #i:I
    :goto_24e
    if-gt v7, v5, :cond_277

    #@250
    .line 317
    move-object/from16 v0, p0

    #@252
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@254
    move-object/from16 v17, v0

    #@256
    move-object/from16 v0, v17

    #@258
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25b
    move-result-object v9

    #@25c
    check-cast v9, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@25e
    .line 318
    .restart local v9       #joinRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    iget-object v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@260
    move-object/from16 v17, v0

    #@262
    iget-object v0, v9, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@264
    move-object/from16 v18, v0

    #@266
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@269
    .line 319
    move-object/from16 v0, p0

    #@26b
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@26d
    move-object/from16 v17, v0

    #@26f
    move-object/from16 v0, v17

    #@271
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@274
    .line 316
    add-int/lit8 v7, v7, 0x1

    #@276
    goto :goto_24e

    #@277
    .line 321
    .end local v9           #joinRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_277
    const/16 v17, 0x1

    #@279
    goto/16 :goto_37

    #@27b
    .line 323
    .end local v7           #i:I
    .end local v8           #joinIndex:I
    :cond_27b
    const/16 v17, 0x0

    #@27d
    goto/16 :goto_37

    #@27f
    .line 275
    :cond_27f
    add-int/lit8 v5, v5, 0x1

    #@281
    goto/16 :goto_19b

    #@283
    .line 330
    .end local v6           #endRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_283
    const/16 v17, 0x1

    #@285
    move-object/from16 v0, p0

    #@287
    move/from16 v1, p1

    #@289
    move/from16 v2, p2

    #@28b
    move/from16 v3, v17

    #@28d
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/IntRangeManager;->tryAddRanges(IIZ)Z

    #@290
    move-result v17

    #@291
    if-eqz v17, :cond_2d9

    #@293
    .line 331
    move/from16 v0, p1

    #@295
    iput v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@297
    .line 332
    move/from16 v0, p2

    #@299
    iput v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@29b
    .line 334
    iget-object v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@29d
    move-object/from16 v17, v0

    #@29f
    const/16 v18, 0x0

    #@2a1
    new-instance v19, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@2a3
    invoke-direct/range {v19 .. v23}, Lcom/android/internal/telephony/IntRangeManager$ClientRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@2a6
    invoke-virtual/range {v17 .. v19}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@2a9
    .line 340
    add-int/lit8 v8, v14, 0x1

    #@2ab
    .line 341
    .restart local v8       #joinIndex:I
    move v7, v8

    #@2ac
    .restart local v7       #i:I
    :goto_2ac
    if-ge v7, v10, :cond_2d5

    #@2ae
    .line 343
    move-object/from16 v0, p0

    #@2b0
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@2b2
    move-object/from16 v17, v0

    #@2b4
    move-object/from16 v0, v17

    #@2b6
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2b9
    move-result-object v9

    #@2ba
    check-cast v9, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@2bc
    .line 344
    .restart local v9       #joinRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    iget-object v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@2be
    move-object/from16 v17, v0

    #@2c0
    iget-object v0, v9, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@2c2
    move-object/from16 v18, v0

    #@2c4
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@2c7
    .line 345
    move-object/from16 v0, p0

    #@2c9
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@2cb
    move-object/from16 v17, v0

    #@2cd
    move-object/from16 v0, v17

    #@2cf
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@2d2
    .line 341
    add-int/lit8 v7, v7, 0x1

    #@2d4
    goto :goto_2ac

    #@2d5
    .line 347
    .end local v9           #joinRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_2d5
    const/16 v17, 0x1

    #@2d7
    goto/16 :goto_37

    #@2d9
    .line 349
    .end local v7           #i:I
    .end local v8           #joinIndex:I
    :cond_2d9
    const/16 v17, 0x0

    #@2db
    goto/16 :goto_37

    #@2dd
    .line 352
    .end local v5           #endIndex:I
    :cond_2dd
    add-int/lit8 v17, p1, 0x1

    #@2df
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@2e1
    move/from16 v18, v0

    #@2e3
    move/from16 v0, v17

    #@2e5
    move/from16 v1, v18

    #@2e7
    if-gt v0, v1, :cond_3f6

    #@2e9
    .line 354
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@2eb
    move/from16 v17, v0

    #@2ed
    move/from16 v0, p2

    #@2ef
    move/from16 v1, v17

    #@2f1
    if-gt v0, v1, :cond_30b

    #@2f3
    .line 357
    new-instance v17, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@2f5
    move-object/from16 v0, v17

    #@2f7
    move-object/from16 v1, p0

    #@2f9
    move/from16 v2, p1

    #@2fb
    move/from16 v3, p2

    #@2fd
    move-object/from16 v4, p3

    #@2ff
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/IntRangeManager$ClientRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@302
    move-object/from16 v0, v17

    #@304
    invoke-virtual {v13, v0}, Lcom/android/internal/telephony/IntRangeManager$IntRange;->insert(Lcom/android/internal/telephony/IntRangeManager$ClientRange;)V

    #@307
    .line 358
    const/16 v17, 0x1

    #@309
    goto/16 :goto_37

    #@30b
    .line 362
    :cond_30b
    move v5, v14

    #@30c
    .line 363
    .restart local v5       #endIndex:I
    add-int/lit8 v15, v14, 0x1

    #@30e
    .local v15, testIndex:I
    :goto_30e
    if-ge v15, v10, :cond_32c

    #@310
    .line 364
    move-object/from16 v0, p0

    #@312
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@314
    move-object/from16 v17, v0

    #@316
    move-object/from16 v0, v17

    #@318
    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@31b
    move-result-object v16

    #@31c
    check-cast v16, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@31e
    .line 365
    .local v16, testRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    add-int/lit8 v17, p2, 0x1

    #@320
    move-object/from16 v0, v16

    #@322
    iget v0, v0, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@324
    move/from16 v18, v0

    #@326
    move/from16 v0, v17

    #@328
    move/from16 v1, v18

    #@32a
    if-ge v0, v1, :cond_360

    #@32c
    .line 372
    .end local v16           #testRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_32c
    if-ne v5, v14, :cond_368

    #@32e
    .line 376
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@330
    move/from16 v17, v0

    #@332
    add-int/lit8 v17, v17, 0x1

    #@334
    const/16 v18, 0x1

    #@336
    move-object/from16 v0, p0

    #@338
    move/from16 v1, v17

    #@33a
    move/from16 v2, p2

    #@33c
    move/from16 v3, v18

    #@33e
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/IntRangeManager;->tryAddRanges(IIZ)Z

    #@341
    move-result v17

    #@342
    if-eqz v17, :cond_364

    #@344
    .line 377
    move/from16 v0, p2

    #@346
    iput v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@348
    .line 378
    new-instance v17, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@34a
    move-object/from16 v0, v17

    #@34c
    move-object/from16 v1, p0

    #@34e
    move/from16 v2, p1

    #@350
    move/from16 v3, p2

    #@352
    move-object/from16 v4, p3

    #@354
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/IntRangeManager$ClientRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@357
    move-object/from16 v0, v17

    #@359
    invoke-virtual {v13, v0}, Lcom/android/internal/telephony/IntRangeManager$IntRange;->insert(Lcom/android/internal/telephony/IntRangeManager$ClientRange;)V

    #@35c
    .line 379
    const/16 v17, 0x1

    #@35e
    goto/16 :goto_37

    #@360
    .line 368
    .restart local v16       #testRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_360
    move v5, v15

    #@361
    .line 363
    add-int/lit8 v15, v15, 0x1

    #@363
    goto :goto_30e

    #@364
    .line 381
    .end local v16           #testRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_364
    const/16 v17, 0x0

    #@366
    goto/16 :goto_37

    #@368
    .line 385
    :cond_368
    move-object/from16 v0, p0

    #@36a
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@36c
    move-object/from16 v17, v0

    #@36e
    move-object/from16 v0, v17

    #@370
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@373
    move-result-object v6

    #@374
    check-cast v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@376
    .line 390
    .restart local v6       #endRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    iget v0, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@378
    move/from16 v17, v0

    #@37a
    move/from16 v0, p2

    #@37c
    move/from16 v1, v17

    #@37e
    if-gt v0, v1, :cond_3e8

    #@380
    iget v0, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->startId:I

    #@382
    move/from16 v17, v0

    #@384
    add-int/lit8 v11, v17, -0x1

    #@386
    .line 393
    .restart local v11       #newRangeEndId:I
    :goto_386
    iget v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@388
    move/from16 v17, v0

    #@38a
    add-int/lit8 v17, v17, 0x1

    #@38c
    const/16 v18, 0x1

    #@38e
    move-object/from16 v0, p0

    #@390
    move/from16 v1, v17

    #@392
    move/from16 v2, v18

    #@394
    invoke-virtual {v0, v1, v11, v2}, Lcom/android/internal/telephony/IntRangeManager;->tryAddRanges(IIZ)Z

    #@397
    move-result v17

    #@398
    if-eqz v17, :cond_3f2

    #@39a
    .line 394
    iget v0, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@39c
    move/from16 v17, v0

    #@39e
    move/from16 v0, p2

    #@3a0
    move/from16 v1, v17

    #@3a2
    if-gt v0, v1, :cond_3eb

    #@3a4
    iget v11, v6, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@3a6
    .line 395
    :goto_3a6
    iput v11, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->endId:I

    #@3a8
    .line 397
    new-instance v17, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@3aa
    move-object/from16 v0, v17

    #@3ac
    move-object/from16 v1, p0

    #@3ae
    move/from16 v2, p1

    #@3b0
    move/from16 v3, p2

    #@3b2
    move-object/from16 v4, p3

    #@3b4
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/IntRangeManager$ClientRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@3b7
    move-object/from16 v0, v17

    #@3b9
    invoke-virtual {v13, v0}, Lcom/android/internal/telephony/IntRangeManager$IntRange;->insert(Lcom/android/internal/telephony/IntRangeManager$ClientRange;)V

    #@3bc
    .line 403
    add-int/lit8 v8, v14, 0x1

    #@3be
    .line 404
    .restart local v8       #joinIndex:I
    move v7, v8

    #@3bf
    .restart local v7       #i:I
    :goto_3bf
    if-gt v7, v5, :cond_3ee

    #@3c1
    .line 405
    move-object/from16 v0, p0

    #@3c3
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@3c5
    move-object/from16 v17, v0

    #@3c7
    move-object/from16 v0, v17

    #@3c9
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3cc
    move-result-object v9

    #@3cd
    check-cast v9, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@3cf
    .line 406
    .restart local v9       #joinRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    iget-object v0, v13, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@3d1
    move-object/from16 v17, v0

    #@3d3
    iget-object v0, v9, Lcom/android/internal/telephony/IntRangeManager$IntRange;->clients:Ljava/util/ArrayList;

    #@3d5
    move-object/from16 v18, v0

    #@3d7
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@3da
    .line 407
    move-object/from16 v0, p0

    #@3dc
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@3de
    move-object/from16 v17, v0

    #@3e0
    move-object/from16 v0, v17

    #@3e2
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@3e5
    .line 404
    add-int/lit8 v7, v7, 0x1

    #@3e7
    goto :goto_3bf

    #@3e8
    .end local v7           #i:I
    .end local v8           #joinIndex:I
    .end local v9           #joinRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    .end local v11           #newRangeEndId:I
    :cond_3e8
    move/from16 v11, p2

    #@3ea
    .line 390
    goto :goto_386

    #@3eb
    .restart local v11       #newRangeEndId:I
    :cond_3eb
    move/from16 v11, p2

    #@3ed
    .line 394
    goto :goto_3a6

    #@3ee
    .line 409
    .restart local v7       #i:I
    .restart local v8       #joinIndex:I
    :cond_3ee
    const/16 v17, 0x1

    #@3f0
    goto/16 :goto_37

    #@3f2
    .line 411
    .end local v7           #i:I
    .end local v8           #joinIndex:I
    :cond_3f2
    const/16 v17, 0x0

    #@3f4
    goto/16 :goto_37

    #@3f6
    .line 206
    .end local v5           #endIndex:I
    .end local v6           #endRange:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    .end local v11           #newRangeEndId:I
    .end local v15           #testIndex:I
    :cond_3f6
    add-int/lit8 v14, v14, 0x1

    #@3f8
    goto/16 :goto_3d

    #@3fa
    .line 419
    .end local v13           #range:Lcom/android/internal/telephony/IntRangeManager$IntRange;
    :cond_3fa
    const/16 v17, 0x1

    #@3fc
    move-object/from16 v0, p0

    #@3fe
    move/from16 v1, p1

    #@400
    move/from16 v2, p2

    #@402
    move/from16 v3, v17

    #@404
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/IntRangeManager;->tryAddRanges(IIZ)Z

    #@407
    move-result v17

    #@408
    if-eqz v17, :cond_426

    #@40a
    .line 420
    move-object/from16 v0, p0

    #@40c
    iget-object v0, v0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@40e
    move-object/from16 v17, v0

    #@410
    new-instance v18, Lcom/android/internal/telephony/IntRangeManager$IntRange;

    #@412
    move-object/from16 v0, v18

    #@414
    move-object/from16 v1, p0

    #@416
    move/from16 v2, p1

    #@418
    move/from16 v3, p2

    #@41a
    move-object/from16 v4, p3

    #@41c
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/IntRangeManager$IntRange;-><init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V

    #@41f
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_422
    .catchall {:try_start_3f .. :try_end_422} :catchall_42a

    #@422
    .line 421
    const/16 v17, 0x1

    #@424
    goto/16 :goto_37

    #@426
    .line 423
    :cond_426
    const/16 v17, 0x0

    #@428
    goto/16 :goto_37

    #@42a
    .line 194
    .end local v10           #len:I
    .end local v14           #startIndex:I
    :catchall_42a
    move-exception v17

    #@42b
    monitor-exit p0

    #@42c
    throw v17
.end method

.method protected abstract finishUpdate()Z
.end method

.method public isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 613
    iget-object v0, p0, Lcom/android/internal/telephony/IntRangeManager;->mRanges:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected abstract startUpdate()V
.end method

.method protected tryAddRanges(IIZ)Z
    .registers 5
    .parameter "startId"
    .parameter "endId"
    .parameter "selected"

    #@0
    .prologue
    .line 601
    invoke-virtual {p0}, Lcom/android/internal/telephony/IntRangeManager;->startUpdate()V

    #@3
    .line 602
    invoke-direct {p0}, Lcom/android/internal/telephony/IntRangeManager;->populateAllRanges()V

    #@6
    .line 604
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/telephony/IntRangeManager;->addRange(IIZ)V

    #@9
    .line 605
    invoke-virtual {p0}, Lcom/android/internal/telephony/IntRangeManager;->finishUpdate()Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public updateRanges()Z
    .registers 2

    #@0
    .prologue
    .line 586
    invoke-virtual {p0}, Lcom/android/internal/telephony/IntRangeManager;->startUpdate()V

    #@3
    .line 588
    invoke-direct {p0}, Lcom/android/internal/telephony/IntRangeManager;->populateAllRanges()V

    #@6
    .line 589
    invoke-virtual {p0}, Lcom/android/internal/telephony/IntRangeManager;->finishUpdate()Z

    #@9
    move-result v0

    #@a
    return v0
.end method
