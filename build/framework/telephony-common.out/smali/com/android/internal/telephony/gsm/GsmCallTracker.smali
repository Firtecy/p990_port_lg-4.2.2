.class public final Lcom/android/internal/telephony/gsm/GsmCallTracker;
.super Lcom/android/internal/telephony/CallTracker;
.source "GsmCallTracker.java"


# static fields
#the value of this static final field might be set in the static constructor
.field private static final CIQ_EXTENSION:Z = false

#the value of this static final field might be set in the static constructor
.field private static final DBG_CIQ:Z = false

.field private static final DBG_POLL:Z = false

.field static final LOG_TAG:Ljava/lang/String; = "GSM"

.field static final MAX_CONNECTIONS:I = 0x7

.field static final MAX_CONNECTIONS_PER_CALL:I = 0x5

.field private static final REPEAT_POLLING:Z

.field public static mFakeCall:Lcom/android/internal/telephony/gsm/GsmCall;

.field private static mIsUsingIPPhone:Z


# instance fields
.field backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

.field callSwitchPending:Z

.field connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

.field desiredMute:Z

.field droppedDuringPoll:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/gsm/GsmConnection;",
            ">;"
        }
    .end annotation
.end field

.field fakeCall:Lcom/android/internal/telephony/gsm/GsmCall;

.field foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

.field hangupPendingMO:Z

.field private mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

.field private mCallStateBroadcasterLock:Lcom/android/internal/telephony/CallStateBroadcaster$InstanceLock;

.field private final mContext:Landroid/content/Context;

.field private mIsDoubleCheckGetCurCall:I

.field private mIsEcmTimerCanceled:Z

.field private mPollTimeout:I

.field private mPollTimeoutCount:I

.field private mPollTimeoutExpired:Z

.field pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

.field phone:Lcom/android/internal/telephony/gsm/GSMPhone;

.field ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

.field state:Lcom/android/internal/telephony/PhoneConstants$State;

.field voiceCallEndedRegistrants:Landroid/os/RegistrantList;

.field voiceCallStartedRegistrants:Landroid/os/RegistrantList;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 91
    const-string v0, "persist.lgiqc.ext"

    #@4
    const-string v3, "0"

    #@6
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    const-string v3, "1"

    #@c
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_30

    #@12
    move v0, v1

    #@13
    :goto_13
    sput-boolean v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->CIQ_EXTENSION:Z

    #@15
    .line 92
    const-string v0, "ro.debuggable"

    #@17
    const-string v3, "0"

    #@19
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    const-string v3, "1"

    #@1f
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_32

    #@25
    :goto_25
    sput-boolean v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->DBG_CIQ:Z

    #@27
    .line 116
    sput-boolean v2, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsUsingIPPhone:Z

    #@29
    .line 119
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isIPPhoneSupported()Z

    #@2c
    move-result v0

    #@2d
    sput-boolean v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsUsingIPPhone:Z

    #@2f
    .line 120
    return-void

    #@30
    :cond_30
    move v0, v2

    #@31
    .line 91
    goto :goto_13

    #@32
    :cond_32
    move v1, v2

    #@33
    .line 92
    goto :goto_25
.end method

.method constructor <init>(Lcom/android/internal/telephony/gsm/GSMPhone;)V
    .registers 8
    .parameter "phone"

    #@0
    .prologue
    const/4 v5, 0x7

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    .line 162
    invoke-direct {p0}, Lcom/android/internal/telephony/CallTracker;-><init>()V

    #@7
    .line 104
    new-array v1, v5, [Lcom/android/internal/telephony/gsm/GsmConnection;

    #@9
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@b
    .line 105
    new-instance v1, Landroid/os/RegistrantList;

    #@d
    invoke-direct {v1}, Landroid/os/RegistrantList;-><init>()V

    #@10
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->voiceCallEndedRegistrants:Landroid/os/RegistrantList;

    #@12
    .line 106
    new-instance v1, Landroid/os/RegistrantList;

    #@14
    invoke-direct {v1}, Landroid/os/RegistrantList;-><init>()V

    #@17
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->voiceCallStartedRegistrants:Landroid/os/RegistrantList;

    #@19
    .line 110
    new-instance v1, Ljava/util/ArrayList;

    #@1b
    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    #@1e
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@20
    .line 115
    new-instance v1, Lcom/android/internal/telephony/gsm/GsmCall;

    #@22
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/gsm/GsmCall;-><init>(Lcom/android/internal/telephony/gsm/GsmCallTracker;)V

    #@25
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->fakeCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@27
    .line 122
    new-instance v1, Lcom/android/internal/telephony/gsm/GsmCall;

    #@29
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/gsm/GsmCall;-><init>(Lcom/android/internal/telephony/gsm/GsmCallTracker;)V

    #@2c
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2e
    .line 124
    new-instance v1, Lcom/android/internal/telephony/gsm/GsmCall;

    #@30
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/gsm/GsmCall;-><init>(Lcom/android/internal/telephony/gsm/GsmCallTracker;)V

    #@33
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@35
    .line 125
    new-instance v1, Lcom/android/internal/telephony/gsm/GsmCall;

    #@37
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/gsm/GsmCall;-><init>(Lcom/android/internal/telephony/gsm/GsmCallTracker;)V

    #@3a
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@3c
    .line 130
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->callSwitchPending:Z

    #@3e
    .line 134
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->desiredMute:Z

    #@40
    .line 136
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@42
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@44
    .line 139
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsEcmTimerCanceled:Z

    #@46
    .line 142
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsDoubleCheckGetCurCall:I

    #@48
    .line 143
    const/16 v1, 0x1388

    #@4a
    iput v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeout:I

    #@4c
    .line 144
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutExpired:Z

    #@4e
    .line 145
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutCount:I

    #@50
    .line 149
    new-instance v1, Landroid/telephony/AssistDialPhoneNumberUtils;

    #@52
    invoke-direct {v1}, Landroid/telephony/AssistDialPhoneNumberUtils;-><init>()V

    #@55
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@57
    .line 163
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@59
    .line 164
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5b
    iput-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@5d
    .line 166
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@5f
    const/4 v2, 0x2

    #@60
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@63
    .line 168
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@65
    const/16 v2, 0x9

    #@67
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6a
    .line 169
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@6c
    const/16 v2, 0xa

    #@6e
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForNotAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@71
    .line 171
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@74
    move-result-object v1

    #@75
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mContext:Landroid/content/Context;

    #@77
    .line 176
    sget-boolean v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->CIQ_EXTENSION:Z

    #@79
    if-eqz v1, :cond_91

    #@7b
    .line 177
    new-instance v1, Lcom/android/internal/telephony/CallStateBroadcaster$InstanceLock;

    #@7d
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@80
    move-result-object v2

    #@81
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/CallStateBroadcaster$InstanceLock;-><init>(Landroid/content/Context;)V

    #@84
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mCallStateBroadcasterLock:Lcom/android/internal/telephony/CallStateBroadcaster$InstanceLock;

    #@86
    .line 178
    sget-boolean v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->DBG_CIQ:Z

    #@88
    if-eqz v1, :cond_91

    #@8a
    const-string v1, "GSM"

    #@8c
    const-string v2, "EXTENSION Start.!"

    #@8e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 183
    :cond_91
    sget-boolean v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsUsingIPPhone:Z

    #@93
    if-eqz v1, :cond_b0

    #@95
    .line 184
    new-instance v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@97
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@9a
    move-result-object v1

    #@9b
    const-string v2, "911"

    #@9d
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->fakeCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@9f
    invoke-direct {v0, v1, v2, p0, v3}, Lcom/android/internal/telephony/gsm/GsmConnection;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/internal/telephony/gsm/GsmCallTracker;Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@a2
    .line 185
    .local v0, conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    if-eqz v0, :cond_a7

    #@a4
    .line 186
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/gsm/GsmConnection;->setAsFake(Z)V

    #@a7
    .line 188
    :cond_a7
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->fakeCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@a9
    invoke-virtual {v1, v4}, Lcom/android/internal/telephony/gsm/GsmCall;->setAsFake(Z)V

    #@ac
    .line 189
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->fakeCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@ae
    sput-object v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mFakeCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@b0
    .line 192
    .end local v0           #conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :cond_b0
    return-void
.end method

.method private dumpState()V
    .registers 7

    #@0
    .prologue
    .line 1126
    const-string v3, "GSM"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "Phone State:"

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1128
    const-string v3, "GSM"

    #@1c
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "Ringing call: "

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@29
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmCall;->toString()Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 1130
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@3a
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmCall;->getConnections()Ljava/util/List;

    #@3d
    move-result-object v1

    #@3e
    .line 1131
    .local v1, l:Ljava/util/List;
    const/4 v0, 0x0

    #@3f
    .local v0, i:I
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@42
    move-result v2

    #@43
    .local v2, s:I
    :goto_43
    if-ge v0, v2, :cond_55

    #@45
    .line 1132
    const-string v3, "GSM"

    #@47
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1131
    add-int/lit8 v0, v0, 0x1

    #@54
    goto :goto_43

    #@55
    .line 1135
    :cond_55
    const-string v3, "GSM"

    #@57
    new-instance v4, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v5, "Foreground call: "

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@64
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmCall;->toString()Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v4

    #@6c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v4

    #@70
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 1137
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@75
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmCall;->getConnections()Ljava/util/List;

    #@78
    move-result-object v1

    #@79
    .line 1138
    const/4 v0, 0x0

    #@7a
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@7d
    move-result v2

    #@7e
    :goto_7e
    if-ge v0, v2, :cond_90

    #@80
    .line 1139
    const-string v3, "GSM"

    #@82
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    .line 1138
    add-int/lit8 v0, v0, 0x1

    #@8f
    goto :goto_7e

    #@90
    .line 1142
    :cond_90
    const-string v3, "GSM"

    #@92
    new-instance v4, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    const-string v5, "Background call: "

    #@99
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v4

    #@9d
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@9f
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmCall;->toString()Ljava/lang/String;

    #@a2
    move-result-object v5

    #@a3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v4

    #@a7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v4

    #@ab
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 1144
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@b0
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmCall;->getConnections()Ljava/util/List;

    #@b3
    move-result-object v1

    #@b4
    .line 1145
    const/4 v0, 0x0

    #@b5
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@b8
    move-result v2

    #@b9
    :goto_b9
    if-ge v0, v2, :cond_cb

    #@bb
    .line 1146
    const-string v3, "GSM"

    #@bd
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@c0
    move-result-object v4

    #@c1
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@c4
    move-result-object v4

    #@c5
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c8
    .line 1145
    add-int/lit8 v0, v0, 0x1

    #@ca
    goto :goto_b9

    #@cb
    .line 1149
    :cond_cb
    return-void
.end method

.method private fakeHoldForegroundBeforeDial()V
    .registers 6

    #@0
    .prologue
    .line 264
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2
    iget-object v4, v4, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v4}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Ljava/util/List;

    #@a
    .line 266
    .local v1, connCopy:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/Connection;>;"
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@e
    move-result v3

    #@f
    .local v3, s:I
    :goto_f
    if-ge v2, v3, :cond_1d

    #@11
    .line 267
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@17
    .line 269
    .local v0, conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->fakeHoldBeforeDial()V

    #@1a
    .line 266
    add-int/lit8 v2, v2, 0x1

    #@1c
    goto :goto_f

    #@1d
    .line 271
    .end local v0           #conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :cond_1d
    return-void
.end method

.method private getFailedService(I)Lcom/android/internal/telephony/Phone$SuppService;
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 1440
    packed-switch p1, :pswitch_data_12

    #@3
    .line 1450
    :pswitch_3
    sget-object v0, Lcom/android/internal/telephony/Phone$SuppService;->UNKNOWN:Lcom/android/internal/telephony/Phone$SuppService;

    #@5
    :goto_5
    return-object v0

    #@6
    .line 1442
    :pswitch_6
    sget-object v0, Lcom/android/internal/telephony/Phone$SuppService;->SWITCH:Lcom/android/internal/telephony/Phone$SuppService;

    #@8
    goto :goto_5

    #@9
    .line 1444
    :pswitch_9
    sget-object v0, Lcom/android/internal/telephony/Phone$SuppService;->CONFERENCE:Lcom/android/internal/telephony/Phone$SuppService;

    #@b
    goto :goto_5

    #@c
    .line 1446
    :pswitch_c
    sget-object v0, Lcom/android/internal/telephony/Phone$SuppService;->SEPARATE:Lcom/android/internal/telephony/Phone$SuppService;

    #@e
    goto :goto_5

    #@f
    .line 1448
    :pswitch_f
    sget-object v0, Lcom/android/internal/telephony/Phone$SuppService;->TRANSFER:Lcom/android/internal/telephony/Phone$SuppService;

    #@11
    goto :goto_5

    #@12
    .line 1440
    :pswitch_data_12
    .packed-switch 0x8
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_9
        :pswitch_c
        :pswitch_f
    .end packed-switch
.end method

.method private handleEcmTimer(I)V
    .registers 5
    .parameter "action"

    #@0
    .prologue
    .line 1674
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleTimerInEmergencyCallbackMode(I)V

    #@5
    .line 1675
    packed-switch p1, :pswitch_data_2a

    #@8
    .line 1679
    const-string v0, "GSM"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "handleEcmTimer, unsupported action "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1681
    :goto_20
    return-void

    #@21
    .line 1676
    :pswitch_21
    const/4 v0, 0x1

    #@22
    iput-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsEcmTimerCanceled:Z

    #@24
    goto :goto_20

    #@25
    .line 1677
    :pswitch_25
    const/4 v0, 0x0

    #@26
    iput-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsEcmTimerCanceled:Z

    #@28
    goto :goto_20

    #@29
    .line 1675
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x0
        :pswitch_25
        :pswitch_21
    .end packed-switch
.end method

.method private handleRadioNotAvailable()V
    .registers 1

    #@0
    .prologue
    .line 1119
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pollCallsWhenSafe()V

    #@3
    .line 1120
    return-void
.end method

.method private internalClearDisconnected()V
    .registers 2

    #@0
    .prologue
    .line 685
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->clearDisconnected()V

    #@5
    .line 686
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@7
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->clearDisconnected()V

    #@a
    .line 687
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->clearDisconnected()V

    #@f
    .line 688
    return-void
.end method

.method private obtainCompleteMessage()Landroid/os/Message;
    .registers 2

    #@0
    .prologue
    .line 696
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage(I)Landroid/os/Message;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private obtainCompleteMessage(I)Landroid/os/Message;
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 705
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@6
    .line 706
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@9
    .line 707
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Lcom/android/internal/telephony/CallTracker;->needsPoll:Z

    #@c
    .line 712
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method private obtainCompleteMessage(II)Landroid/os/Message;
    .registers 4
    .parameter "what"
    .parameter "clirMode"

    #@0
    .prologue
    .line 718
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@6
    .line 719
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@9
    .line 720
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Lcom/android/internal/telephony/CallTracker;->needsPoll:Z

    #@c
    .line 725
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    return-object v0
.end method

.method private okUpdateCall(Lcom/android/internal/telephony/gsm/GsmConnection;Lcom/android/internal/telephony/DriverCall;)Z
    .registers 7
    .parameter "conn"
    .parameter "dc"

    #@0
    .prologue
    .line 1481
    const/4 v0, 0x1

    #@1
    .line 1483
    .local v0, needNotify:Z
    if-eqz p1, :cond_5

    #@3
    if-nez p2, :cond_e

    #@5
    .line 1484
    :cond_5
    const-string v2, "GSM"

    #@7
    const-string v3, "exceptcallstate --> Call instance is null "

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    move v1, v0

    #@d
    .line 1495
    .end local v0           #needNotify:Z
    .local v1, needNotify:I
    :goto_d
    return v1

    #@e
    .line 1488
    .end local v1           #needNotify:I
    .restart local v0       #needNotify:Z
    :cond_e
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->getState()Lcom/android/internal/telephony/Call$State;

    #@11
    move-result-object v2

    #@12
    sget-object v3, Lcom/android/internal/telephony/Call$State;->DISCONNECTING:Lcom/android/internal/telephony/Call$State;

    #@14
    if-eq v2, v3, :cond_1e

    #@16
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->getState()Lcom/android/internal/telephony/Call$State;

    #@19
    move-result-object v2

    #@1a
    sget-object v3, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@1c
    if-ne v2, v3, :cond_38

    #@1e
    :cond_1e
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@20
    sget-object v3, Lcom/android/internal/telephony/DriverCall$State;->DIALING:Lcom/android/internal/telephony/DriverCall$State;

    #@22
    if-eq v2, v3, :cond_30

    #@24
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@26
    sget-object v3, Lcom/android/internal/telephony/DriverCall$State;->INCOMING:Lcom/android/internal/telephony/DriverCall$State;

    #@28
    if-eq v2, v3, :cond_30

    #@2a
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@2c
    sget-object v3, Lcom/android/internal/telephony/DriverCall$State;->WAITING:Lcom/android/internal/telephony/DriverCall$State;

    #@2e
    if-ne v2, v3, :cond_38

    #@30
    .line 1491
    :cond_30
    const/4 v0, 0x0

    #@31
    .line 1492
    const-string v2, "GSM"

    #@33
    const-string v3, "exceptcallstate --> It does not need to be updated"

    #@35
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    :cond_38
    move v1, v0

    #@39
    .line 1495
    .restart local v1       #needNotify:I
    goto :goto_d
.end method

.method private operationComplete()V
    .registers 3

    #@0
    .prologue
    .line 731
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@6
    .line 736
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@8
    if-nez v0, :cond_24

    #@a
    iget-boolean v0, p0, Lcom/android/internal/telephony/CallTracker;->needsPoll:Z

    #@c
    if-eqz v0, :cond_24

    #@e
    .line 737
    const/4 v0, 0x1

    #@f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainMessage(I)Landroid/os/Message;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@15
    .line 738
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@19
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getCurrentCalls(Landroid/os/Message;)V

    #@1c
    .line 745
    :cond_1c
    :goto_1c
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@1e
    if-nez v0, :cond_23

    #@20
    .line 746
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->stopOpDonePoll()V

    #@23
    .line 749
    :cond_23
    return-void

    #@24
    .line 739
    :cond_24
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@26
    if-gez v0, :cond_1c

    #@28
    .line 741
    const-string v0, "GSM"

    #@2a
    const-string v1, "GsmCallTracker.pendingOperations < 0"

    #@2c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 742
    const/4 v0, 0x0

    #@30
    iput v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@32
    goto :goto_1c
.end method

.method private startOpDonePoll()V
    .registers 5

    #@0
    .prologue
    const/16 v2, 0x18

    #@2
    const/4 v3, 0x0

    #@3
    .line 1455
    const-string v0, "KR"

    #@5
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_40

    #@b
    .line 1456
    new-instance v0, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v1, "startOpDonePoll mPollTimeout="

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeout:I

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    const-string v1, ", mPollTimeoutExpired="

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutExpired:Z

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@2f
    .line 1457
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->removeMessages(I)V

    #@32
    .line 1458
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainMessage(I)Landroid/os/Message;

    #@35
    move-result-object v0

    #@36
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeout:I

    #@38
    int-to-long v1, v1

    #@39
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@3c
    .line 1459
    iput-boolean v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutExpired:Z

    #@3e
    .line 1460
    iput v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutCount:I

    #@40
    .line 1462
    :cond_40
    return-void
.end method

.method private stopOpDonePoll()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1465
    const-string v0, "KR"

    #@3
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_36

    #@9
    .line 1466
    new-instance v0, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v1, "stopOpDonePoll mPollTimeout="

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeout:I

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    const-string v1, ", mPollTimeoutExpired="

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutExpired:Z

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@2d
    .line 1467
    const/16 v0, 0x18

    #@2f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->removeMessages(I)V

    #@32
    .line 1468
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutExpired:Z

    #@34
    .line 1469
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutCount:I

    #@36
    .line 1471
    :cond_36
    return-void
.end method

.method private updatePhoneState()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 753
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@4
    .line 755
    .local v0, oldState:Lcom/android/internal/telephony/PhoneConstants$State;
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->isRinging()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_73

    #@c
    .line 756
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->RINGING:Lcom/android/internal/telephony/PhoneConstants$State;

    #@e
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@10
    .line 764
    :goto_10
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@12
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@14
    if-ne v1, v2, :cond_91

    #@16
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@18
    if-eq v0, v1, :cond_91

    #@1a
    .line 765
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->voiceCallEndedRegistrants:Landroid/os/RegistrantList;

    #@1c
    new-instance v2, Landroid/os/AsyncResult;

    #@1e
    invoke-direct {v2, v3, v3, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@21
    invoke-virtual {v1, v2}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@24
    .line 772
    :cond_24
    :goto_24
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@26
    if-eq v1, v0, :cond_72

    #@28
    .line 775
    sget-boolean v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsUsingIPPhone:Z

    #@2a
    if-eqz v1, :cond_a4

    #@2c
    sget-boolean v1, Lcom/android/internal/telephony/CallManager;->mIgnoreDisconnect:Z

    #@2e
    if-nez v1, :cond_3a

    #@30
    sget-boolean v1, Lcom/android/internal/telephony/CallManager;->mIgnoreOutOfServiceArea:Z

    #@32
    if-eqz v1, :cond_a4

    #@34
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@36
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@38
    if-ne v1, v2, :cond_a4

    #@3a
    .line 776
    :cond_3a
    sput-boolean v4, Lcom/android/internal/telephony/CallManager;->mIgnoreDisconnect:Z

    #@3c
    .line 777
    sput-boolean v4, Lcom/android/internal/telephony/CallManager;->mIgnoreOutOfServiceArea:Z

    #@3e
    .line 778
    sget-boolean v1, Lcom/android/internal/telephony/CallManager;->mIgnoreDisconnect:Z

    #@40
    if-nez v1, :cond_45

    #@42
    .line 779
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->setKeepingFakeCall()V

    #@45
    .line 787
    :cond_45
    :goto_45
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@47
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->isInEcm()Z

    #@4a
    move-result v1

    #@4b
    if-eqz v1, :cond_60

    #@4d
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@4f
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@51
    if-ne v1, v2, :cond_60

    #@53
    .line 788
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@55
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->isInEcmExitDelay()Z

    #@58
    move-result v1

    #@59
    if-eqz v1, :cond_60

    #@5b
    .line 789
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@5d
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->exitEmergencyCallbackMode()V

    #@60
    .line 795
    :cond_60
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@62
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@64
    if-ne v1, v2, :cond_72

    #@66
    .line 797
    const-string v1, "isInEmergencyCall set to false updatePhoneState "

    #@68
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@6b
    .line 798
    const-string v1, "ril.cdma.emergencyCall"

    #@6d
    const-string v2, "false"

    #@6f
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@72
    .line 802
    :cond_72
    return-void

    #@73
    .line 757
    :cond_73
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@75
    if-nez v1, :cond_87

    #@77
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@79
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->isIdle()Z

    #@7c
    move-result v1

    #@7d
    if-eqz v1, :cond_87

    #@7f
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@81
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->isIdle()Z

    #@84
    move-result v1

    #@85
    if-nez v1, :cond_8c

    #@87
    .line 759
    :cond_87
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@89
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@8b
    goto :goto_10

    #@8c
    .line 761
    :cond_8c
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@8e
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@90
    goto :goto_10

    #@91
    .line 767
    :cond_91
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@93
    if-ne v0, v1, :cond_24

    #@95
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@97
    if-eq v0, v1, :cond_24

    #@99
    .line 768
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->voiceCallStartedRegistrants:Landroid/os/RegistrantList;

    #@9b
    new-instance v2, Landroid/os/AsyncResult;

    #@9d
    invoke-direct {v2, v3, v3, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@a0
    invoke-virtual {v1, v2}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@a3
    goto :goto_24

    #@a4
    .line 782
    :cond_a4
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@a6
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPhoneStateChanged()V

    #@a9
    goto :goto_45
.end method


# virtual methods
.method acceptCall()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 547
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@3
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@6
    move-result-object v0

    #@7
    sget-object v1, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    #@9
    if-ne v0, v1, :cond_1f

    #@b
    .line 548
    const-string v0, "phone"

    #@d
    const-string v1, "acceptCall: incoming..."

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 550
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->setMute(Z)V

    #@15
    .line 551
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@1a
    move-result-object v1

    #@1b
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->acceptCall(Landroid/os/Message;)V

    #@1e
    .line 558
    :goto_1e
    return-void

    #@1f
    .line 552
    :cond_1f
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@21
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@24
    move-result-object v0

    #@25
    sget-object v1, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    #@27
    if-ne v0, v1, :cond_30

    #@29
    .line 553
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->setMute(Z)V

    #@2c
    .line 554
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->switchWaitingOrHoldingAndActive()V

    #@2f
    goto :goto_1e

    #@30
    .line 556
    :cond_30
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@32
    const-string v1, "phone not ringing"

    #@34
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@37
    throw v0
.end method

.method canConference()Z
    .registers 3

    #@0
    .prologue
    .line 648
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@5
    move-result-object v0

    #@6
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@8
    if-ne v0, v1, :cond_26

    #@a
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@f
    move-result-object v0

    #@10
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@12
    if-ne v0, v1, :cond_26

    #@14
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@16
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->isFull()Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_26

    #@1c
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@1e
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->isFull()Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_26

    #@24
    const/4 v0, 0x1

    #@25
    :goto_25
    return v0

    #@26
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_25
.end method

.method canDial()Z
    .registers 6

    #@0
    .prologue
    .line 657
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I

    #@9
    move-result v2

    #@a
    .line 658
    .local v2, serviceState:I
    const-string v3, "ro.telephony.disable-call"

    #@c
    const-string v4, "false"

    #@e
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 661
    .local v0, disableCall:Ljava/lang/String;
    const/4 v3, 0x3

    #@13
    if-eq v2, v3, :cond_43

    #@15
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@17
    if-nez v3, :cond_43

    #@19
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@1b
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmCall;->isRinging()Z

    #@1e
    move-result v3

    #@1f
    if-nez v3, :cond_43

    #@21
    const-string v3, "true"

    #@23
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v3

    #@27
    if-nez v3, :cond_43

    #@29
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2b
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@32
    move-result v3

    #@33
    if-eqz v3, :cond_41

    #@35
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@37
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@3e
    move-result v3

    #@3f
    if-nez v3, :cond_43

    #@41
    :cond_41
    const/4 v1, 0x1

    #@42
    .line 668
    .local v1, ret:Z
    :goto_42
    return v1

    #@43
    .line 661
    .end local v1           #ret:Z
    :cond_43
    const/4 v1, 0x0

    #@44
    goto :goto_42
.end method

.method canTransfer()Z
    .registers 3

    #@0
    .prologue
    .line 674
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@5
    move-result-object v0

    #@6
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@8
    if-eq v0, v1, :cond_1e

    #@a
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@f
    move-result-object v0

    #@10
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ALERTING:Lcom/android/internal/telephony/Call$State;

    #@12
    if-eq v0, v1, :cond_1e

    #@14
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@16
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@19
    move-result-object v0

    #@1a
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@1c
    if-ne v0, v1, :cond_2a

    #@1e
    :cond_1e
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@20
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@23
    move-result-object v0

    #@24
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@26
    if-ne v0, v1, :cond_2a

    #@28
    const/4 v0, 0x1

    #@29
    :goto_29
    return v0

    #@2a
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_29
.end method

.method clearDisconnected()V
    .registers 2

    #@0
    .prologue
    .line 640
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->internalClearDisconnected()V

    #@3
    .line 642
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->updatePhoneState()V

    #@6
    .line 643
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPreciseCallStateChanged()V

    #@b
    .line 644
    return-void
.end method

.method conference()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 599
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/16 v1, 0xb

    #@4
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage(I)Landroid/os/Message;

    #@7
    move-result-object v1

    #@8
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->conference(Landroid/os/Message;)V

    #@b
    .line 600
    return-void
.end method

.method dial(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    .registers 4
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 495
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, p1, v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;ILcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method dial(Ljava/lang/String;I)Lcom/android/internal/telephony/Connection;
    .registers 4
    .parameter "dialString"
    .parameter "clirMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 505
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;ILcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method declared-synchronized dial(Ljava/lang/String;ILcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;
    .registers 13
    .parameter "dialString"
    .parameter "clirMode"
    .parameter "uusInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 279
    monitor-enter p0

    #@2
    :try_start_2
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->clearDisconnected()V

    #@5
    .line 282
    const/4 v2, 0x0

    #@6
    .line 284
    .local v2, isDialRequestPending:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->canDial()Z

    #@9
    move-result v5

    #@a
    if-nez v5, :cond_17

    #@c
    .line 285
    new-instance v5, Lcom/android/internal/telephony/CallStateException;

    #@e
    const-string v6, "cannot dial in current state"

    #@10
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v5
    :try_end_14
    .catchall {:try_start_2 .. :try_end_14} :catchall_14

    #@14
    .line 279
    .end local v2           #isDialRequestPending:Z
    :catchall_14
    move-exception v5

    #@15
    monitor-exit p0

    #@16
    throw v5

    #@17
    .line 291
    .restart local v2       #isDialRequestPending:Z
    :cond_17
    :try_start_17
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@19
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@1c
    move-result-object v5

    #@1d
    sget-object v6, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@1f
    if-ne v5, v6, :cond_28

    #@21
    .line 296
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->switchWaitingOrHoldingAndActive(I)V

    #@24
    .line 302
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->fakeHoldForegroundBeforeDial()V

    #@27
    .line 303
    const/4 v2, 0x1

    #@28
    .line 306
    :cond_28
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2a
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@2d
    move-result-object v5

    #@2e
    sget-object v6, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@30
    if-eq v5, v6, :cond_3a

    #@32
    .line 308
    new-instance v5, Lcom/android/internal/telephony/CallStateException;

    #@34
    const-string v6, "cannot dial in current state"

    #@36
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@39
    throw v5

    #@3a
    .line 312
    :cond_3a
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@3c
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@3f
    move-result-object v5

    #@40
    const-string v6, "support_assisted_dialing"

    #@42
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@45
    move-result v5

    #@46
    if-eqz v5, :cond_9b

    #@48
    .line 313
    move-object v0, p1

    #@49
    .line 314
    .local v0, assistedDialingBeforeNumber:Ljava/lang/String;
    const-string v5, "GSMCallTracker "

    #@4b
    new-instance v6, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v7, " ********** Dial() before Assisted Dial Number: "

    #@52
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v6

    #@56
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v6

    #@5a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v6

    #@5e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 316
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@63
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@65
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v5, p1, v6}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAssitedDialFinalNumberForGSM(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    #@6c
    move-result-object p1

    #@6d
    .line 318
    const-string v5, "GSMCallTracker "

    #@6f
    new-instance v6, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v7, "******** Dial() after Assisted Dial Number: "

    #@76
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v6

    #@7e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v6

    #@82
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    .line 320
    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@88
    move-result v5

    #@89
    if-eqz v5, :cond_180

    #@8b
    .line 321
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@8d
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@94
    move-result-object v5

    #@95
    const-string v6, "assist_dial_new_number_check"

    #@97
    const/4 v7, 0x0

    #@98
    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@9b
    .line 331
    .end local v0           #assistedDialingBeforeNumber:Ljava/lang/String;
    :cond_9b
    :goto_9b
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@9d
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@a0
    move-result-object v5

    #@a1
    const-string v6, "support_smart_dialing"

    #@a3
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a6
    move-result v5

    #@a7
    if-eqz v5, :cond_d0

    #@a9
    .line 332
    if-eqz p1, :cond_d0

    #@ab
    .line 333
    invoke-static {p1}, Landroid/telephony/AssistDialPhoneNumberUtils;->isNanpSimplified(Ljava/lang/String;)Z

    #@ae
    move-result v5

    #@af
    if-eqz v5, :cond_d0

    #@b1
    .line 334
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isVoiceMailNumber(Ljava/lang/String;)Z

    #@b4
    move-result v5

    #@b5
    if-eqz v5, :cond_192

    #@b7
    .line 335
    new-instance v5, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v6, "+1"

    #@be
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v5

    #@c2
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@c4
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/GSMPhone;->getLine1Number()Ljava/lang/String;

    #@c7
    move-result-object v6

    #@c8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v5

    #@cc
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cf
    move-result-object p1

    #@d0
    .line 345
    :cond_d0
    :goto_d0
    const/4 v5, 0x0

    #@d1
    const-string v6, "support_network_change_auto_retry"

    #@d3
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@d6
    move-result v5

    #@d7
    if-eqz v5, :cond_109

    #@d9
    .line 346
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@db
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@de
    move-result-object v5

    #@df
    invoke-static {p1, v5}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z

    #@e2
    move-result v3

    #@e3
    .line 347
    .local v3, isEmergencyCall:Z
    if-ne v3, v8, :cond_109

    #@e5
    .line 348
    const-string v5, "dial() Emergency Number True!!"

    #@e7
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@ea
    .line 349
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@ec
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@ef
    move-result-object v5

    #@f0
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f3
    move-result-object v5

    #@f4
    const-string v6, "network_change_auto_retry"

    #@f6
    const/4 v7, 0x1

    #@f7
    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@fa
    .line 352
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@fc
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@ff
    move-result-object v5

    #@100
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@103
    move-result-object v5

    #@104
    const-string v6, "network_change_auto_retry_number"

    #@106
    invoke-static {v5, v6, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@109
    .line 360
    .end local v3           #isEmergencyCall:Z
    :cond_109
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@10b
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@10e
    move-result-object v5

    #@10f
    const-string v6, "support_emergency_callback_mode_for_gsm"

    #@111
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@114
    move-result v5

    #@115
    if-eqz v5, :cond_139

    #@117
    .line 361
    const-string v5, "ril.cdma.inecmmode"

    #@119
    const-string v6, "false"

    #@11b
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11e
    move-result-object v1

    #@11f
    .line 362
    .local v1, inEcm:Ljava/lang/String;
    const-string v5, "true"

    #@121
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@124
    move-result v4

    #@125
    .line 363
    .local v4, isPhoneInEcmMode:Z
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@127
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@12a
    move-result-object v5

    #@12b
    invoke-static {p1, v5}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z

    #@12e
    move-result v3

    #@12f
    .line 366
    .restart local v3       #isEmergencyCall:Z
    if-eqz v4, :cond_139

    #@131
    if-eqz v3, :cond_139

    #@133
    .line 367
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@135
    const/4 v5, 0x1

    #@136
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->handleEcmTimer(I)V

    #@139
    .line 372
    .end local v1           #inEcm:Ljava/lang/String;
    .end local v3           #isEmergencyCall:Z
    .end local v4           #isPhoneInEcmMode:Z
    :cond_139
    new-instance v5, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@13b
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@13d
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@140
    move-result-object v6

    #@141
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->checkForTestEmergencyNumber(Ljava/lang/String;)Ljava/lang/String;

    #@144
    move-result-object v7

    #@145
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@147
    invoke-direct {v5, v6, v7, p0, v8}, Lcom/android/internal/telephony/gsm/GsmConnection;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/internal/telephony/gsm/GsmCallTracker;Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@14a
    iput-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@14c
    .line 374
    const/4 v5, 0x0

    #@14d
    iput-boolean v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupPendingMO:Z

    #@14f
    .line 376
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@151
    iget-object v5, v5, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@153
    if-eqz v5, :cond_16b

    #@155
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@157
    iget-object v5, v5, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@159
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@15c
    move-result v5

    #@15d
    if-eqz v5, :cond_16b

    #@15f
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@161
    iget-object v5, v5, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@163
    const/16 v6, 0x4e

    #@165
    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    #@168
    move-result v5

    #@169
    if-ltz v5, :cond_1a0

    #@16b
    .line 380
    :cond_16b
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@16d
    sget-object v6, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@16f
    iput-object v6, v5, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@171
    .line 384
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pollCallsWhenSafe()V

    #@174
    .line 410
    :cond_174
    :goto_174
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->updatePhoneState()V

    #@177
    .line 411
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@179
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPreciseCallStateChanged()V

    #@17c
    .line 413
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;
    :try_end_17e
    .catchall {:try_start_17 .. :try_end_17e} :catchall_14

    #@17e
    monitor-exit p0

    #@17f
    return-object v5

    #@180
    .line 324
    .restart local v0       #assistedDialingBeforeNumber:Ljava/lang/String;
    :cond_180
    :try_start_180
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@182
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@185
    move-result-object v5

    #@186
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@189
    move-result-object v5

    #@18a
    const-string v6, "assist_dial_new_number_check"

    #@18c
    const/4 v7, 0x1

    #@18d
    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@190
    goto/16 :goto_9b

    #@192
    .line 337
    .end local v0           #assistedDialingBeforeNumber:Ljava/lang/String;
    :cond_192
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@194
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@196
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@199
    move-result-object v6

    #@19a
    invoke-virtual {v5, p1, v6}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAssitedDialFinalNumberForGSM(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    #@19d
    move-result-object p1

    #@19e
    goto/16 :goto_d0

    #@1a0
    .line 389
    :cond_1a0
    if-nez v2, :cond_174

    #@1a2
    .line 392
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1a4
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@1a7
    move-result-object v5

    #@1a8
    invoke-static {p1, v5}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z

    #@1ab
    move-result v5

    #@1ac
    if-eqz v5, :cond_1cf

    #@1ae
    .line 394
    const-string v5, "isInEmergencyCall set to true "

    #@1b0
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@1b3
    .line 395
    const-string v5, "ril.cdma.emergencyCall"

    #@1b5
    const-string v6, "true"

    #@1b7
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1ba
    .line 402
    :goto_1ba
    const/4 v5, 0x0

    #@1bb
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->setMute(Z)V

    #@1be
    .line 404
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->startOpDonePoll()V

    #@1c1
    .line 406
    iget-object v5, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1c3
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@1c5
    iget-object v6, v6, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@1c7
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@1ca
    move-result-object v7

    #@1cb
    invoke-interface {v5, v6, p2, p3, v7}, Lcom/android/internal/telephony/CommandsInterface;->dial(Ljava/lang/String;ILcom/android/internal/telephony/UUSInfo;Landroid/os/Message;)V

    #@1ce
    goto :goto_174

    #@1cf
    .line 397
    :cond_1cf
    const-string v5, "ril.cdma.emergencyCall"

    #@1d1
    const-string v6, "false"

    #@1d3
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1d6
    .catchall {:try_start_180 .. :try_end_1d6} :catchall_14

    #@1d6
    goto :goto_1ba
.end method

.method dial(Ljava/lang/String;IZ)Lcom/android/internal/telephony/Connection;
    .registers 5
    .parameter "dialString"
    .parameter "clirMode"
    .parameter "subaddress"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 517
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;IZLcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method declared-synchronized dial(Ljava/lang/String;IZLcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;
    .registers 10
    .parameter "dialString"
    .parameter "clirMode"
    .parameter "subaddress"
    .parameter "uusInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 423
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->clearDisconnected()V

    #@4
    .line 426
    const/4 v0, 0x0

    #@5
    .line 428
    .local v0, isDialRequestPending:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->canDial()Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_16

    #@b
    .line 429
    new-instance v1, Lcom/android/internal/telephony/CallStateException;

    #@d
    const-string v2, "cannot dial in current state"

    #@f
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@12
    throw v1
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_13

    #@13
    .line 423
    .end local v0           #isDialRequestPending:Z
    :catchall_13
    move-exception v1

    #@14
    monitor-exit p0

    #@15
    throw v1

    #@16
    .line 433
    .restart local v0       #isDialRequestPending:Z
    :cond_16
    :try_start_16
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_26

    #@1c
    .line 434
    const-string v1, "GSM"

    #@1e
    const-string v2, " Dialing Number is Null cannot dialing "

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_23
    .catchall {:try_start_16 .. :try_end_23} :catchall_13

    #@23
    .line 435
    const/4 v1, 0x0

    #@24
    .line 489
    :goto_24
    monitor-exit p0

    #@25
    return-object v1

    #@26
    .line 442
    :cond_26
    :try_start_26
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@28
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@2b
    move-result-object v1

    #@2c
    sget-object v2, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@2e
    if-ne v1, v2, :cond_37

    #@30
    .line 447
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->switchWaitingOrHoldingAndActive(I)V

    #@33
    .line 453
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->fakeHoldForegroundBeforeDial()V

    #@36
    .line 454
    const/4 v0, 0x1

    #@37
    .line 457
    :cond_37
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@39
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@3c
    move-result-object v1

    #@3d
    sget-object v2, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@3f
    if-eq v1, v2, :cond_49

    #@41
    .line 459
    new-instance v1, Lcom/android/internal/telephony/CallStateException;

    #@43
    const-string v2, "cannot dial in current state"

    #@45
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@48
    throw v1

    #@49
    .line 462
    :cond_49
    new-instance v1, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@4b
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4d
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->checkForTestEmergencyNumber(Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@57
    invoke-direct {v1, v2, v3, p0, v4}, Lcom/android/internal/telephony/gsm/GsmConnection;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/internal/telephony/gsm/GsmCallTracker;Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@5a
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@5c
    .line 464
    const/4 v1, 0x0

    #@5d
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupPendingMO:Z

    #@5f
    .line 466
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@61
    iget-object v1, v1, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@63
    if-eqz v1, :cond_7b

    #@65
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@67
    iget-object v1, v1, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@69
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@6c
    move-result v1

    #@6d
    if-eqz v1, :cond_7b

    #@6f
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@71
    iget-object v1, v1, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@73
    const/16 v2, 0x4e

    #@75
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    #@78
    move-result v1

    #@79
    if-ltz v1, :cond_8f

    #@7b
    .line 470
    :cond_7b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@7d
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@7f
    iput-object v2, v1, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@81
    .line 474
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pollCallsWhenSafe()V

    #@84
    .line 486
    :cond_84
    :goto_84
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->updatePhoneState()V

    #@87
    .line 487
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@89
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPreciseCallStateChanged()V

    #@8c
    .line 489
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@8e
    goto :goto_24

    #@8f
    .line 479
    :cond_8f
    if-nez v0, :cond_84

    #@91
    .line 481
    const/4 v1, 0x0

    #@92
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->setMute(Z)V

    #@95
    .line 482
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@97
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@99
    iget-object v2, v2, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@9b
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@9e
    move-result-object v3

    #@9f
    invoke-interface {v1, v2, p2, p3, v3}, Lcom/android/internal/telephony/CommandsInterface;->dial(Ljava/lang/String;IZLandroid/os/Message;)V
    :try_end_a2
    .catchall {:try_start_26 .. :try_end_a2} :catchall_13

    #@a2
    goto :goto_84
.end method

.method dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;
    .registers 4
    .parameter "dialString"
    .parameter "uusInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 500
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;ILcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method dial(Ljava/lang/String;Z)Lcom/android/internal/telephony/Connection;
    .registers 5
    .parameter "dialString"
    .parameter "subaddress"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 511
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;IZLcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method dialPendingCall(I)V
    .registers 5
    .parameter "clirMode"

    #@0
    .prologue
    .line 523
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@4
    if-eqz v0, :cond_1c

    #@6
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@8
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@a
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_1c

    #@10
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@12
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@14
    const/16 v1, 0x4e

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    #@19
    move-result v0

    #@1a
    if-ltz v0, :cond_2e

    #@1c
    .line 526
    :cond_1c
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@1e
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@20
    iput-object v1, v0, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@22
    .line 530
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pollCallsWhenSafe()V

    #@25
    .line 537
    :goto_25
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->updatePhoneState()V

    #@28
    .line 538
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2a
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPreciseCallStateChanged()V

    #@2d
    .line 539
    return-void

    #@2e
    .line 533
    :cond_2e
    const/4 v0, 0x0

    #@2f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->setMute(Z)V

    #@32
    .line 535
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@34
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@36
    iget-object v1, v1, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@38
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@3b
    move-result-object v2

    #@3c
    invoke-interface {v0, v1, p1, v2}, Lcom/android/internal/telephony/CommandsInterface;->dial(Ljava/lang/String;ILandroid/os/Message;)V

    #@3f
    goto :goto_25
.end method

.method public dispose()V
    .registers 8

    #@0
    .prologue
    .line 197
    sget-boolean v5, Lcom/android/internal/telephony/gsm/GsmCallTracker;->CIQ_EXTENSION:Z

    #@2
    if-eqz v5, :cond_9

    #@4
    .line 198
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mCallStateBroadcasterLock:Lcom/android/internal/telephony/CallStateBroadcaster$InstanceLock;

    #@6
    invoke-virtual {v5}, Lcom/android/internal/telephony/CallStateBroadcaster$InstanceLock;->unlock()V

    #@9
    .line 202
    :cond_9
    iget-object v5, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@b
    invoke-interface {v5, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForCallStateChanged(Landroid/os/Handler;)V

    #@e
    .line 203
    iget-object v5, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    invoke-interface {v5, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOn(Landroid/os/Handler;)V

    #@13
    .line 204
    iget-object v5, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@15
    invoke-interface {v5, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForNotAvailable(Landroid/os/Handler;)V

    #@18
    .line 206
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@1a
    .local v0, arr$:[Lcom/android/internal/telephony/gsm/GsmConnection;
    array-length v4, v0

    #@1b
    .local v4, len$:I
    const/4 v3, 0x0

    #@1c
    .local v3, i$:I
    :goto_1c
    if-ge v3, v4, :cond_3d

    #@1e
    aget-object v1, v0, v3

    #@20
    .line 208
    .local v1, c:Lcom/android/internal/telephony/gsm/GsmConnection;
    if-eqz v1, :cond_31

    #@22
    .line 209
    :try_start_22
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmConnection;)V

    #@25
    .line 212
    const-string v5, "GSM"

    #@27
    const-string v6, "Posting connection disconnect due to LOST_SIGNAL"

    #@29
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 213
    sget-object v5, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOST_SIGNAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2e
    invoke-virtual {v1, v5}, Lcom/android/internal/telephony/gsm/GsmConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V
    :try_end_31
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_22 .. :try_end_31} :catch_34

    #@31
    .line 206
    :cond_31
    :goto_31
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_1c

    #@34
    .line 215
    :catch_34
    move-exception v2

    #@35
    .line 216
    .local v2, ex:Lcom/android/internal/telephony/CallStateException;
    const-string v5, "GSM"

    #@37
    const-string v6, "unexpected error on hangup during dispose"

    #@39
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    goto :goto_31

    #@3d
    .line 221
    .end local v1           #c:Lcom/android/internal/telephony/gsm/GsmConnection;
    .end local v2           #ex:Lcom/android/internal/telephony/CallStateException;
    :cond_3d
    :try_start_3d
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@3f
    if-eqz v5, :cond_54

    #@41
    .line 222
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@43
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmConnection;)V

    #@46
    .line 223
    const-string v5, "GSM"

    #@48
    const-string v6, "Posting disconnect to pendingMO due to LOST_SIGNAL"

    #@4a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 224
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@4f
    sget-object v6, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOST_SIGNAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@51
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/gsm/GsmConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V
    :try_end_54
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_3d .. :try_end_54} :catch_58

    #@54
    .line 230
    :cond_54
    :goto_54
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->clearDisconnected()V

    #@57
    .line 231
    return-void

    #@58
    .line 226
    :catch_58
    move-exception v2

    #@59
    .line 227
    .restart local v2       #ex:Lcom/android/internal/telephony/CallStateException;
    const-string v5, "GSM"

    #@5b
    const-string v6, "unexpected error on hangup during dispose"

    #@5d
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    goto :goto_54
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 11
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 1690
    const-string v1, "GsmCallTracker extends:"

    #@5
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8
    .line 1691
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/telephony/CallTracker;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@b
    .line 1692
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "connections: length="

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@18
    array-length v2, v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@24
    .line 1693
    const/4 v0, 0x0

    #@25
    .local v0, i:I
    :goto_25
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@27
    array-length v1, v1

    #@28
    if-ge v0, v1, :cond_40

    #@2a
    .line 1694
    const-string v1, "  connections[%d]=%s\n"

    #@2c
    new-array v2, v6, [Ljava/lang/Object;

    #@2e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v3

    #@32
    aput-object v3, v2, v4

    #@34
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@36
    aget-object v3, v3, v0

    #@38
    aput-object v3, v2, v5

    #@3a
    invoke-virtual {p2, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@3d
    .line 1693
    add-int/lit8 v0, v0, 0x1

    #@3f
    goto :goto_25

    #@40
    .line 1696
    :cond_40
    new-instance v1, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v2, " voiceCallEndedRegistrants="

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->voiceCallEndedRegistrants:Landroid/os/RegistrantList;

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@58
    .line 1697
    new-instance v1, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v2, " voiceCallStartedRegistrants="

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->voiceCallStartedRegistrants:Landroid/os/RegistrantList;

    #@65
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v1

    #@69
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v1

    #@6d
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@70
    .line 1698
    new-instance v1, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v2, " droppedDuringPoll: size="

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@7d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@80
    move-result v2

    #@81
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@84
    move-result-object v1

    #@85
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v1

    #@89
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8c
    .line 1699
    const/4 v0, 0x0

    #@8d
    :goto_8d
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@8f
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@92
    move-result v1

    #@93
    if-ge v0, v1, :cond_ad

    #@95
    .line 1700
    const-string v1, "  droppedDuringPoll[%d]=%s\n"

    #@97
    new-array v2, v6, [Ljava/lang/Object;

    #@99
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9c
    move-result-object v3

    #@9d
    aput-object v3, v2, v4

    #@9f
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@a1
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a4
    move-result-object v3

    #@a5
    aput-object v3, v2, v5

    #@a7
    invoke-virtual {p2, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@aa
    .line 1699
    add-int/lit8 v0, v0, 0x1

    #@ac
    goto :goto_8d

    #@ad
    .line 1702
    :cond_ad
    new-instance v1, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v2, " ringingCall="

    #@b4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v1

    #@b8
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@ba
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v1

    #@be
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v1

    #@c2
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c5
    .line 1703
    new-instance v1, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v2, " foregroundCall="

    #@cc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v1

    #@d0
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@d2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v1

    #@d6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v1

    #@da
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@dd
    .line 1704
    new-instance v1, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    const-string v2, " backgroundCall="

    #@e4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v1

    #@e8
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@ea
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v1

    #@ee
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v1

    #@f2
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f5
    .line 1705
    new-instance v1, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v2, " pendingMO="

    #@fc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v1

    #@100
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@102
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v1

    #@106
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v1

    #@10a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@10d
    .line 1706
    new-instance v1, Ljava/lang/StringBuilder;

    #@10f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@112
    const-string v2, " hangupPendingMO="

    #@114
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v1

    #@118
    iget-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupPendingMO:Z

    #@11a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v1

    #@11e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121
    move-result-object v1

    #@122
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@125
    .line 1707
    new-instance v1, Ljava/lang/StringBuilder;

    #@127
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12a
    const-string v2, " phone="

    #@12c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v1

    #@130
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@132
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v1

    #@136
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v1

    #@13a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@13d
    .line 1708
    new-instance v1, Ljava/lang/StringBuilder;

    #@13f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@142
    const-string v2, " desiredMute="

    #@144
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v1

    #@148
    iget-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->desiredMute:Z

    #@14a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v1

    #@14e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@151
    move-result-object v1

    #@152
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@155
    .line 1709
    new-instance v1, Ljava/lang/StringBuilder;

    #@157
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15a
    const-string v2, " state="

    #@15c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v1

    #@160
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@162
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v1

    #@166
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@169
    move-result-object v1

    #@16a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@16d
    .line 1710
    return-void
.end method

.method explicitCallTransfer()V
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v7, 0x9

    #@2
    .line 605
    const-string v5, "1"

    #@4
    const-string v6, "service.iq.active"

    #@6
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v6

    #@a
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_46

    #@10
    .line 606
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS02Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@13
    move-result-object v4

    #@14
    .line 608
    .local v4, gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    if-eqz v4, :cond_52

    #@16
    .line 609
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@18
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmCall;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@1e
    .line 610
    .local v1, backgroundConn:Lcom/android/internal/telephony/gsm/GsmConnection;
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@20
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmCall;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    #@23
    move-result-object v3

    #@24
    check-cast v3, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@26
    .line 612
    .local v3, foregroundConn:Lcom/android/internal/telephony/gsm/GsmConnection;
    const/4 v0, 0x0

    #@27
    .line 613
    .local v0, backCiqId:I
    const/4 v2, 0x0

    #@28
    .line 615
    .local v2, foreCiqId:I
    if-eqz v1, :cond_2e

    #@2a
    .line 616
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmConnection;->getcqID()I

    #@2d
    move-result v0

    #@2e
    .line 619
    :cond_2e
    if-eqz v3, :cond_34

    #@30
    .line 620
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmConnection;->getcqID()I

    #@33
    move-result v2

    #@34
    .line 623
    :cond_34
    iput v0, v4, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->dwCallId:I

    #@36
    .line 624
    iput-byte v7, v4, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@38
    .line 625
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mContext:Landroid/content/Context;

    #@3a
    invoke-static {v5, v4}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@3d
    .line 627
    iput v2, v4, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->dwCallId:I

    #@3f
    .line 628
    iput-byte v7, v4, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@41
    .line 629
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mContext:Landroid/content/Context;

    #@43
    invoke-static {v5, v4}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@46
    .line 635
    .end local v0           #backCiqId:I
    .end local v1           #backgroundConn:Lcom/android/internal/telephony/gsm/GsmConnection;
    .end local v2           #foreCiqId:I
    .end local v3           #foregroundConn:Lcom/android/internal/telephony/gsm/GsmConnection;
    .end local v4           #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_46
    :goto_46
    iget-object v5, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@48
    const/16 v6, 0xd

    #@4a
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage(I)Landroid/os/Message;

    #@4d
    move-result-object v6

    #@4e
    invoke-interface {v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->explicitCallTransfer(Landroid/os/Message;)V

    #@51
    .line 636
    return-void

    #@52
    .line 631
    .restart local v4       #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_52
    const-string v5, "LGDDM-CSC"

    #@54
    const-string v6, "CSC State Transition metric instance is null!"

    #@56
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    goto :goto_46
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 234
    const-string v0, "GSM"

    #@2
    const-string v1, "GsmCallTracker finalized"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 235
    return-void
.end method

.method getConnectionByIndex(Lcom/android/internal/telephony/gsm/GsmCall;I)Lcom/android/internal/telephony/gsm/GsmConnection;
    .registers 7
    .parameter "call"
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1428
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 1429
    .local v1, count:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v1, :cond_1b

    #@9
    .line 1430
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@11
    .line 1431
    .local v0, cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->getGSMIndex()I

    #@14
    move-result v3

    #@15
    if-ne v3, p2, :cond_18

    #@17
    .line 1436
    .end local v0           #cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :goto_17
    return-object v0

    #@18
    .line 1429
    .restart local v0       #cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :cond_18
    add-int/lit8 v2, v2, 0x1

    #@1a
    goto :goto_7

    #@1b
    .line 1436
    .end local v0           #cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_17
.end method

.method getMute()Z
    .registers 2

    #@0
    .prologue
    .line 1207
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->desiredMute:Z

    #@2
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 15
    .parameter "msg"

    #@0
    .prologue
    .line 1505
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2
    iget-boolean v9, v9, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@4
    if-nez v9, :cond_31

    #@6
    .line 1506
    const-string v9, "GSM"

    #@8
    new-instance v10, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v11, "Received message "

    #@f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v10

    #@13
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v10

    #@17
    const-string v11, "["

    #@19
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v10

    #@1d
    iget v11, p1, Landroid/os/Message;->what:I

    #@1f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v10

    #@23
    const-string v11, "] while being destroyed. Ignoring."

    #@25
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v10

    #@29
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v10

    #@2d
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 1667
    :cond_30
    :goto_30
    return-void

    #@31
    .line 1510
    :cond_31
    iget v9, p1, Landroid/os/Message;->what:I

    #@33
    packed-switch v9, :pswitch_data_258

    #@36
    :pswitch_36
    goto :goto_30

    #@37
    .line 1512
    :pswitch_37
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@39
    check-cast v0, Landroid/os/AsyncResult;

    #@3b
    .line 1514
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v9, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@3d
    if-ne p1, v9, :cond_30

    #@3f
    .line 1517
    const/4 v9, 0x0

    #@40
    iput-boolean v9, p0, Lcom/android/internal/telephony/CallTracker;->needsPoll:Z

    #@42
    .line 1518
    const/4 v9, 0x0

    #@43
    iput-object v9, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@45
    .line 1519
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@47
    check-cast v9, Landroid/os/AsyncResult;

    #@49
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->handlePollCalls(Landroid/os/AsyncResult;)V

    #@4c
    goto :goto_30

    #@4d
    .line 1524
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_4d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4f
    check-cast v0, Landroid/os/AsyncResult;

    #@51
    .line 1526
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@53
    .line 1528
    .local v3, e:Ljava/lang/Throwable;
    if-eqz v3, :cond_7a

    #@55
    instance-of v9, v3, Lcom/android/internal/telephony/CommandException;

    #@57
    if-eqz v9, :cond_7a

    #@59
    check-cast v3, Lcom/android/internal/telephony/CommandException;

    #@5b
    .end local v3           #e:Ljava/lang/Throwable;
    invoke-virtual {v3}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    #@5e
    move-result-object v9

    #@5f
    sget-object v10, Lcom/android/internal/telephony/CommandException$Error;->DIAL_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@61
    if-ne v9, v10, :cond_7a

    #@63
    .line 1530
    const-string v9, "GSM"

    #@65
    const-string v10, "VOICE_STK_CC_MODIFIED - send broadcast"

    #@67
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 1533
    new-instance v5, Landroid/content/Intent;

    #@6c
    const-string v9, "com.lge.android.intent.action.voice_stk_cc_modified"

    #@6e
    invoke-direct {v5, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@71
    .line 1535
    .local v5, intent:Landroid/content/Intent;
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@73
    invoke-virtual {v9}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@76
    move-result-object v9

    #@77
    invoke-virtual {v9, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@7a
    .line 1538
    .end local v5           #intent:Landroid/content/Intent;
    :cond_7a
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->operationComplete()V

    #@7d
    goto :goto_30

    #@7e
    .line 1547
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_7e
    const/4 v9, 0x0

    #@7f
    iput-boolean v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->callSwitchPending:Z

    #@81
    .line 1548
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@83
    check-cast v0, Landroid/os/AsyncResult;

    #@85
    .line 1549
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v9, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@87
    if-eqz v9, :cond_c3

    #@89
    .line 1550
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@8b
    iget v10, p1, Landroid/os/Message;->what:I

    #@8d
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->getFailedService(I)Lcom/android/internal/telephony/Phone$SuppService;

    #@90
    move-result-object v10

    #@91
    invoke-virtual {v9, v10}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V

    #@94
    .line 1555
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@96
    if-eqz v9, :cond_be

    #@98
    .line 1556
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@9a
    invoke-virtual {v9}, Lcom/android/internal/telephony/gsm/GsmConnection;->getAddress()Ljava/lang/String;

    #@9d
    move-result-object v9

    #@9e
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@a0
    invoke-virtual {v10}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@a3
    move-result-object v10

    #@a4
    invoke-static {v9, v10}, Landroid/telephony/PhoneNumberUtils;->isExactOrPotentialLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z

    #@a7
    move-result v6

    #@a8
    .line 1559
    .local v6, isEmergencyNumber:Z
    if-eqz v6, :cond_be

    #@aa
    iget-object v9, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@ac
    if-eqz v9, :cond_be

    #@ae
    .line 1560
    const-string v9, "Call Hold/Switch failed, proceed with e911 dialing"

    #@b0
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@b3
    .line 1561
    iget-object v9, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@b5
    check-cast v9, Ljava/lang/Integer;

    #@b7
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    #@ba
    move-result v9

    #@bb
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dialPendingCall(I)V

    #@be
    .line 1569
    .end local v6           #isEmergencyNumber:Z
    :cond_be
    :goto_be
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->operationComplete()V

    #@c1
    goto/16 :goto_30

    #@c3
    .line 1565
    :cond_c3
    iget-object v9, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@c5
    if-eqz v9, :cond_be

    #@c7
    .line 1566
    iget-object v9, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@c9
    check-cast v9, Ljava/lang/Integer;

    #@cb
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    #@ce
    move-result v9

    #@cf
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dialPendingCall(I)V

    #@d2
    goto :goto_be

    #@d3
    .line 1575
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_d3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d5
    check-cast v0, Landroid/os/AsyncResult;

    #@d7
    .line 1576
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v9, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@d9
    if-eqz v9, :cond_e6

    #@db
    .line 1577
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@dd
    iget v10, p1, Landroid/os/Message;->what:I

    #@df
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->getFailedService(I)Lcom/android/internal/telephony/Phone$SuppService;

    #@e2
    move-result-object v10

    #@e3
    invoke-virtual {v9, v10}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V

    #@e6
    .line 1579
    :cond_e6
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->operationComplete()V

    #@e9
    goto/16 :goto_30

    #@eb
    .line 1584
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_eb
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ed
    check-cast v0, Landroid/os/AsyncResult;

    #@ef
    .line 1586
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->operationComplete()V

    #@f2
    .line 1588
    iget-object v9, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@f4
    if-eqz v9, :cond_167

    #@f6
    .line 1591
    const/16 v1, 0x10

    #@f8
    .line 1592
    .local v1, causeCode:I
    const-string v9, "GSM"

    #@fa
    const-string v10, "Exception during getLastCallFailCause, assuming normal disconnect"

    #@fc
    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ff
    .line 1598
    :goto_ff
    const/16 v9, 0x22

    #@101
    if-eq v1, v9, :cond_11c

    #@103
    const/16 v9, 0x29

    #@105
    if-eq v1, v9, :cond_11c

    #@107
    const/16 v9, 0x2a

    #@109
    if-eq v1, v9, :cond_11c

    #@10b
    const/16 v9, 0x2c

    #@10d
    if-eq v1, v9, :cond_11c

    #@10f
    const/16 v9, 0x31

    #@111
    if-eq v1, v9, :cond_11c

    #@113
    const/16 v9, 0x3a

    #@115
    if-eq v1, v9, :cond_11c

    #@117
    const v9, 0xffff

    #@11a
    if-ne v1, v9, :cond_150

    #@11c
    .line 1605
    :cond_11c
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@11e
    invoke-virtual {v9}, Lcom/android/internal/telephony/gsm/GSMPhone;->getCellLocation()Landroid/telephony/CellLocation;

    #@121
    move-result-object v7

    #@122
    check-cast v7, Landroid/telephony/gsm/GsmCellLocation;

    #@124
    .line 1606
    .local v7, loc:Landroid/telephony/gsm/GsmCellLocation;
    const v10, 0xc3ba

    #@127
    const/4 v9, 0x3

    #@128
    new-array v11, v9, [Ljava/lang/Object;

    #@12a
    const/4 v9, 0x0

    #@12b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12e
    move-result-object v12

    #@12f
    aput-object v12, v11, v9

    #@131
    const/4 v12, 0x1

    #@132
    if-eqz v7, :cond_171

    #@134
    invoke-virtual {v7}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    #@137
    move-result v9

    #@138
    :goto_138
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13b
    move-result-object v9

    #@13c
    aput-object v9, v11, v12

    #@13e
    const/4 v9, 0x2

    #@13f
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@142
    move-result-object v12

    #@143
    invoke-virtual {v12}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@146
    move-result v12

    #@147
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14a
    move-result-object v12

    #@14b
    aput-object v12, v11, v9

    #@14d
    invoke-static {v10, v11}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@150
    .line 1611
    .end local v7           #loc:Landroid/telephony/gsm/GsmCellLocation;
    :cond_150
    const/4 v4, 0x0

    #@151
    .local v4, i:I
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@153
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@156
    move-result v8

    #@157
    .line 1612
    .local v8, s:I
    :goto_157
    if-ge v4, v8, :cond_173

    #@159
    .line 1614
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@15b
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15e
    move-result-object v2

    #@15f
    check-cast v2, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@161
    .line 1616
    .local v2, conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    invoke-virtual {v2, v1}, Lcom/android/internal/telephony/gsm/GsmConnection;->onRemoteDisconnect(I)V

    #@164
    .line 1612
    add-int/lit8 v4, v4, 0x1

    #@166
    goto :goto_157

    #@167
    .line 1595
    .end local v1           #causeCode:I
    .end local v2           #conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    .end local v4           #i:I
    .end local v8           #s:I
    :cond_167
    iget-object v9, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@169
    check-cast v9, [I

    #@16b
    check-cast v9, [I

    #@16d
    const/4 v10, 0x0

    #@16e
    aget v1, v9, v10

    #@170
    .restart local v1       #causeCode:I
    goto :goto_ff

    #@171
    .line 1606
    .restart local v7       #loc:Landroid/telephony/gsm/GsmCellLocation;
    :cond_171
    const/4 v9, -0x1

    #@172
    goto :goto_138

    #@173
    .line 1619
    .end local v7           #loc:Landroid/telephony/gsm/GsmCellLocation;
    .restart local v4       #i:I
    .restart local v8       #s:I
    :cond_173
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->updatePhoneState()V

    #@176
    .line 1621
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@178
    invoke-virtual {v9}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPreciseCallStateChanged()V

    #@17b
    .line 1622
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@17d
    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    #@180
    goto/16 :goto_30

    #@182
    .line 1627
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v1           #causeCode:I
    .end local v4           #i:I
    .end local v8           #s:I
    :pswitch_182
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pollCallsWhenSafe()V

    #@185
    .line 1629
    iget v9, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@187
    if-lez v9, :cond_30

    #@189
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@18b
    if-eqz v9, :cond_30

    #@18d
    iget v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutCount:I

    #@18f
    if-lez v9, :cond_30

    #@191
    .line 1630
    new-instance v9, Ljava/lang/StringBuilder;

    #@193
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@196
    const-string v10, "msg.what="

    #@198
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v9

    #@19c
    iget v10, p1, Landroid/os/Message;->what:I

    #@19e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v9

    #@1a2
    const-string v10, ", foregroundCall.getState()"

    #@1a4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v9

    #@1a8
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@1aa
    invoke-virtual {v10}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@1ad
    move-result-object v10

    #@1ae
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b1
    move-result-object v9

    #@1b2
    const-string v10, ", pendingOperations"

    #@1b4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v9

    #@1b8
    iget v10, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@1ba
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1bd
    move-result-object v9

    #@1be
    const-string v10, ", mPollTimeoutCount="

    #@1c0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c3
    move-result-object v9

    #@1c4
    iget v10, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutCount:I

    #@1c6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v9

    #@1ca
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cd
    move-result-object v9

    #@1ce
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@1d1
    .line 1632
    const/4 v9, 0x2

    #@1d2
    iput v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsDoubleCheckGetCurCall:I

    #@1d4
    .line 1633
    const/4 v9, 0x1

    #@1d5
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainMessage(I)Landroid/os/Message;

    #@1d8
    move-result-object v9

    #@1d9
    iput-object v9, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@1db
    .line 1634
    iget-object v9, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1dd
    iget-object v10, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@1df
    invoke-interface {v9, v10}, Lcom/android/internal/telephony/CommandsInterface;->getCurrentCalls(Landroid/os/Message;)V

    #@1e2
    goto/16 :goto_30

    #@1e4
    .line 1640
    :pswitch_1e4
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->handleRadioAvailable()V

    #@1e7
    goto/16 :goto_30

    #@1e9
    .line 1644
    :pswitch_1e9
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->handleRadioNotAvailable()V

    #@1ec
    goto/16 :goto_30

    #@1ee
    .line 1648
    :pswitch_1ee
    new-instance v9, Ljava/lang/StringBuilder;

    #@1f0
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1f3
    const-string v10, "EVENT_POLL_DIAL_HANGUP_DONE: mPollTimeoutCount="

    #@1f5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v9

    #@1f9
    iget v10, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutCount:I

    #@1fb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v9

    #@1ff
    const-string v10, ", fgCall.getState()="

    #@201
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@204
    move-result-object v9

    #@205
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@207
    invoke-virtual {v10}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@20a
    move-result-object v10

    #@20b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20e
    move-result-object v9

    #@20f
    const-string v10, ", pendingOperations="

    #@211
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@214
    move-result-object v9

    #@215
    iget v10, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@217
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v9

    #@21b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21e
    move-result-object v9

    #@21f
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@222
    .line 1651
    iget v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutCount:I

    #@224
    add-int/lit8 v9, v9, 0x1

    #@226
    iput v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutCount:I

    #@228
    .line 1653
    iget v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutCount:I

    #@22a
    const/4 v10, 0x6

    #@22b
    if-ge v9, v10, :cond_23b

    #@22d
    .line 1654
    const/16 v9, 0x18

    #@22f
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainMessage(I)Landroid/os/Message;

    #@232
    move-result-object v9

    #@233
    iget v10, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeout:I

    #@235
    int-to-long v10, v10

    #@236
    invoke-virtual {p0, v9, v10, v11}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@239
    goto/16 :goto_30

    #@23b
    .line 1657
    :cond_23b
    const/4 v9, 0x2

    #@23c
    iput v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsDoubleCheckGetCurCall:I

    #@23e
    .line 1658
    const/4 v9, 0x1

    #@23f
    iput-boolean v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutExpired:Z

    #@241
    .line 1659
    const/4 v9, 0x0

    #@242
    iput v9, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@244
    .line 1660
    const/4 v9, 0x1

    #@245
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainMessage(I)Landroid/os/Message;

    #@248
    move-result-object v9

    #@249
    iput-object v9, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@24b
    .line 1661
    iget-object v9, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@24d
    iget-object v10, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@24f
    invoke-interface {v9, v10}, Lcom/android/internal/telephony/CommandsInterface;->getCurrentCalls(Landroid/os/Message;)V

    #@252
    .line 1662
    const/4 v9, 0x0

    #@253
    iput v9, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutCount:I

    #@255
    goto/16 :goto_30

    #@257
    .line 1510
    nop

    #@258
    :pswitch_data_258
    .packed-switch 0x1
        :pswitch_37
        :pswitch_182
        :pswitch_182
        :pswitch_4d
        :pswitch_eb
        :pswitch_36
        :pswitch_36
        :pswitch_7e
        :pswitch_1e4
        :pswitch_1e9
        :pswitch_d3
        :pswitch_d3
        :pswitch_d3
        :pswitch_36
        :pswitch_36
        :pswitch_36
        :pswitch_36
        :pswitch_36
        :pswitch_36
        :pswitch_36
        :pswitch_36
        :pswitch_36
        :pswitch_36
        :pswitch_1ee
    .end packed-switch
.end method

.method protected declared-synchronized handlePollCalls(Landroid/os/AsyncResult;)V
    .registers 23
    .parameter "ar"

    #@0
    .prologue
    .line 808
    monitor-enter p0

    #@1
    :try_start_1
    move-object/from16 v0, p1

    #@3
    iget-object v0, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5
    move-object/from16 v17, v0

    #@7
    if-nez v17, :cond_f3

    #@9
    .line 809
    move-object/from16 v0, p1

    #@b
    iget-object v15, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@d
    check-cast v15, Ljava/util/List;

    #@f
    .line 821
    .local v15, polledCalls:Ljava/util/List;
    :goto_f
    const/4 v14, 0x0

    #@10
    .line 822
    .local v14, newRinging:Lcom/android/internal/telephony/Connection;
    const/4 v11, 0x0

    #@11
    .line 824
    .local v11, hasNonHangupStateChanged:Z
    const/4 v13, 0x0

    #@12
    .line 825
    .local v13, needsPollDelay:Z
    const/16 v16, 0x0

    #@14
    .line 827
    .local v16, unknownConnectionAppeared:Z
    const/4 v12, 0x0

    #@15
    .local v12, i:I
    const/4 v6, 0x0

    #@16
    .local v6, curDC:I
    invoke-interface {v15}, Ljava/util/List;->size()I

    #@19
    move-result v8

    #@1a
    .line 828
    .local v8, dcSize:I
    :goto_1a
    move-object/from16 v0, p0

    #@1c
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@1e
    move-object/from16 v17, v0

    #@20
    move-object/from16 v0, v17

    #@22
    array-length v0, v0

    #@23
    move/from16 v17, v0

    #@25
    move/from16 v0, v17

    #@27
    if-ge v12, v0, :cond_32e

    #@29
    .line 829
    move-object/from16 v0, p0

    #@2b
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@2d
    move-object/from16 v17, v0

    #@2f
    aget-object v5, v17, v12

    #@31
    .line 830
    .local v5, conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    const/4 v7, 0x0

    #@32
    .line 833
    .local v7, dc:Lcom/android/internal/telephony/DriverCall;
    if-ge v6, v8, :cond_48

    #@34
    .line 834
    invoke-interface {v15, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@37
    move-result-object v7

    #@38
    .end local v7           #dc:Lcom/android/internal/telephony/DriverCall;
    check-cast v7, Lcom/android/internal/telephony/DriverCall;

    #@3a
    .line 836
    .restart local v7       #dc:Lcom/android/internal/telephony/DriverCall;
    iget v0, v7, Lcom/android/internal/telephony/DriverCall;->index:I

    #@3c
    move/from16 v17, v0

    #@3e
    add-int/lit8 v18, v12, 0x1

    #@40
    move/from16 v0, v17

    #@42
    move/from16 v1, v18

    #@44
    if-ne v0, v1, :cond_111

    #@46
    .line 837
    add-int/lit8 v6, v6, 0x1

    #@48
    .line 846
    :cond_48
    :goto_48
    if-nez v5, :cond_1c6

    #@4a
    if-eqz v7, :cond_1c6

    #@4c
    .line 848
    move-object/from16 v0, p0

    #@4e
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@50
    move-object/from16 v17, v0

    #@52
    if-eqz v17, :cond_11d

    #@54
    move-object/from16 v0, p0

    #@56
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@58
    move-object/from16 v17, v0

    #@5a
    move-object/from16 v0, v17

    #@5c
    invoke-virtual {v0, v7}, Lcom/android/internal/telephony/gsm/GsmConnection;->compareTo(Lcom/android/internal/telephony/DriverCall;)Z

    #@5f
    move-result v17

    #@60
    if-eqz v17, :cond_11d

    #@62
    .line 853
    move-object/from16 v0, p0

    #@64
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@66
    move-object/from16 v17, v0

    #@68
    move-object/from16 v0, p0

    #@6a
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@6c
    move-object/from16 v18, v0

    #@6e
    aput-object v18, v17, v12

    #@70
    .line 854
    move-object/from16 v0, p0

    #@72
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@74
    move-object/from16 v17, v0

    #@76
    move-object/from16 v0, v17

    #@78
    iput v12, v0, Lcom/android/internal/telephony/gsm/GsmConnection;->index:I

    #@7a
    .line 855
    move-object/from16 v0, p0

    #@7c
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@7e
    move-object/from16 v17, v0

    #@80
    move-object/from16 v0, v17

    #@82
    invoke-virtual {v0, v7}, Lcom/android/internal/telephony/gsm/GsmConnection;->update(Lcom/android/internal/telephony/DriverCall;)Z

    #@85
    .line 856
    const/16 v17, 0x0

    #@87
    move-object/from16 v0, v17

    #@89
    move-object/from16 v1, p0

    #@8b
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@8d
    .line 859
    move-object/from16 v0, p0

    #@8f
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupPendingMO:Z

    #@91
    move/from16 v17, v0

    #@93
    if-eqz v17, :cond_16a

    #@95
    .line 860
    const/16 v17, 0x0

    #@97
    move/from16 v0, v17

    #@99
    move-object/from16 v1, p0

    #@9b
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupPendingMO:Z

    #@9d
    .line 863
    move-object/from16 v0, p0

    #@9f
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@a1
    move-object/from16 v17, v0

    #@a3
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@a6
    move-result-object v17

    #@a7
    const-string v18, "support_emergency_callback_mode_for_gsm"

    #@a9
    invoke-static/range {v17 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@ac
    move-result v17

    #@ad
    if-eqz v17, :cond_c6

    #@af
    .line 865
    move-object/from16 v0, p0

    #@b1
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsEcmTimerCanceled:Z

    #@b3
    move/from16 v17, v0

    #@b5
    if-eqz v17, :cond_c6

    #@b7
    .line 866
    move-object/from16 v0, p0

    #@b9
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@bb
    move-object/from16 v17, v0

    #@bd
    const/16 v17, 0x0

    #@bf
    move-object/from16 v0, p0

    #@c1
    move/from16 v1, v17

    #@c3
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->handleEcmTimer(I)V
    :try_end_c6
    .catchall {:try_start_1 .. :try_end_c6} :catchall_10e

    #@c6
    .line 872
    :cond_c6
    :try_start_c6
    new-instance v17, Ljava/lang/StringBuilder;

    #@c8
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@cb
    const-string v18, "poll: hangupPendingMO, hangup conn "

    #@cd
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v17

    #@d1
    move-object/from16 v0, v17

    #@d3
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v17

    #@d7
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v17

    #@db
    move-object/from16 v0, p0

    #@dd
    move-object/from16 v1, v17

    #@df
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@e2
    .line 874
    move-object/from16 v0, p0

    #@e4
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@e6
    move-object/from16 v17, v0

    #@e8
    aget-object v17, v17, v12

    #@ea
    move-object/from16 v0, p0

    #@ec
    move-object/from16 v1, v17

    #@ee
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmConnection;)V
    :try_end_f1
    .catchall {:try_start_c6 .. :try_end_f1} :catchall_10e
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_c6 .. :try_end_f1} :catch_114

    #@f1
    .line 1112
    .end local v5           #conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    .end local v6           #curDC:I
    .end local v7           #dc:Lcom/android/internal/telephony/DriverCall;
    .end local v8           #dcSize:I
    .end local v11           #hasNonHangupStateChanged:Z
    .end local v12           #i:I
    .end local v13           #needsPollDelay:Z
    .end local v14           #newRinging:Lcom/android/internal/telephony/Connection;
    .end local v15           #polledCalls:Ljava/util/List;
    .end local v16           #unknownConnectionAppeared:Z
    :cond_f1
    :goto_f1
    monitor-exit p0

    #@f2
    return-void

    #@f3
    .line 810
    :cond_f3
    :try_start_f3
    move-object/from16 v0, p1

    #@f5
    iget-object v0, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@f7
    move-object/from16 v17, v0

    #@f9
    move-object/from16 v0, p0

    #@fb
    move-object/from16 v1, v17

    #@fd
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->isCommandExceptionRadioNotAvailable(Ljava/lang/Throwable;)Z

    #@100
    move-result v17

    #@101
    if-eqz v17, :cond_10a

    #@103
    .line 813
    new-instance v15, Ljava/util/ArrayList;

    #@105
    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    #@108
    .restart local v15       #polledCalls:Ljava/util/List;
    goto/16 :goto_f

    #@10a
    .line 817
    .end local v15           #polledCalls:Ljava/util/List;
    :cond_10a
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pollCallsAfterDelay()V
    :try_end_10d
    .catchall {:try_start_f3 .. :try_end_10d} :catchall_10e

    #@10d
    goto :goto_f1

    #@10e
    .line 808
    :catchall_10e
    move-exception v17

    #@10f
    monitor-exit p0

    #@110
    throw v17

    #@111
    .line 839
    .restart local v5       #conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    .restart local v6       #curDC:I
    .restart local v7       #dc:Lcom/android/internal/telephony/DriverCall;
    .restart local v8       #dcSize:I
    .restart local v11       #hasNonHangupStateChanged:Z
    .restart local v12       #i:I
    .restart local v13       #needsPollDelay:Z
    .restart local v14       #newRinging:Lcom/android/internal/telephony/Connection;
    .restart local v15       #polledCalls:Ljava/util/List;
    .restart local v16       #unknownConnectionAppeared:Z
    :cond_111
    const/4 v7, 0x0

    #@112
    goto/16 :goto_48

    #@114
    .line 875
    :catch_114
    move-exception v9

    #@115
    .line 876
    .local v9, ex:Lcom/android/internal/telephony/CallStateException;
    :try_start_115
    const-string v17, "GSM"

    #@117
    const-string v18, "unexpected error on hangup"

    #@119
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    goto :goto_f1

    #@11d
    .line 884
    .end local v9           #ex:Lcom/android/internal/telephony/CallStateException;
    :cond_11d
    move-object/from16 v0, p0

    #@11f
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@121
    move-object/from16 v17, v0

    #@123
    new-instance v18, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@125
    move-object/from16 v0, p0

    #@127
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@129
    move-object/from16 v19, v0

    #@12b
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@12e
    move-result-object v19

    #@12f
    move-object/from16 v0, v18

    #@131
    move-object/from16 v1, v19

    #@133
    move-object/from16 v2, p0

    #@135
    invoke-direct {v0, v1, v7, v2, v12}, Lcom/android/internal/telephony/gsm/GsmConnection;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/DriverCall;Lcom/android/internal/telephony/gsm/GsmCallTracker;I)V

    #@138
    aput-object v18, v17, v12

    #@13a
    .line 887
    const-string v17, "isInEmergencyCall set to false handlePollCalls 1 MT "

    #@13c
    move-object/from16 v0, p0

    #@13e
    move-object/from16 v1, v17

    #@140
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@143
    .line 888
    const-string v17, "ril.cdma.emergencyCall"

    #@145
    const-string v18, "false"

    #@147
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@14a
    .line 892
    move-object/from16 v0, p0

    #@14c
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@14e
    move-object/from16 v17, v0

    #@150
    aget-object v17, v17, v12

    #@152
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/gsm/GsmConnection;->getCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@155
    move-result-object v17

    #@156
    move-object/from16 v0, p0

    #@158
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@15a
    move-object/from16 v18, v0

    #@15c
    move-object/from16 v0, v17

    #@15e
    move-object/from16 v1, v18

    #@160
    if-ne v0, v1, :cond_16f

    #@162
    .line 893
    move-object/from16 v0, p0

    #@164
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@166
    move-object/from16 v17, v0

    #@168
    aget-object v14, v17, v12

    #@16a
    .line 920
    :cond_16a
    :goto_16a
    const/4 v11, 0x1

    #@16b
    .line 828
    :cond_16b
    :goto_16b
    add-int/lit8 v12, v12, 0x1

    #@16d
    goto/16 :goto_1a

    #@16f
    .line 900
    :cond_16f
    const-string v17, "GSM"

    #@171
    new-instance v18, Ljava/lang/StringBuilder;

    #@173
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@176
    const-string v19, "Phantom call appeared "

    #@178
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v18

    #@17c
    move-object/from16 v0, v18

    #@17e
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v18

    #@182
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@185
    move-result-object v18

    #@186
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@189
    .line 905
    iget-object v0, v7, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@18b
    move-object/from16 v17, v0

    #@18d
    sget-object v18, Lcom/android/internal/telephony/DriverCall$State;->ALERTING:Lcom/android/internal/telephony/DriverCall$State;

    #@18f
    move-object/from16 v0, v17

    #@191
    move-object/from16 v1, v18

    #@193
    if-eq v0, v1, :cond_1c3

    #@195
    iget-object v0, v7, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@197
    move-object/from16 v17, v0

    #@199
    sget-object v18, Lcom/android/internal/telephony/DriverCall$State;->DIALING:Lcom/android/internal/telephony/DriverCall$State;

    #@19b
    move-object/from16 v0, v17

    #@19d
    move-object/from16 v1, v18

    #@19f
    if-eq v0, v1, :cond_1c3

    #@1a1
    .line 908
    move-object/from16 v0, p0

    #@1a3
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@1a5
    move-object/from16 v17, v0

    #@1a7
    aget-object v17, v17, v12

    #@1a9
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/gsm/GsmConnection;->onConnectedSRVCC()V

    #@1ac
    .line 911
    iget-object v0, v7, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@1ae
    move-object/from16 v17, v0

    #@1b0
    sget-object v18, Lcom/android/internal/telephony/DriverCall$State;->HOLDING:Lcom/android/internal/telephony/DriverCall$State;

    #@1b2
    move-object/from16 v0, v17

    #@1b4
    move-object/from16 v1, v18

    #@1b6
    if-ne v0, v1, :cond_1c3

    #@1b8
    .line 913
    move-object/from16 v0, p0

    #@1ba
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@1bc
    move-object/from16 v17, v0

    #@1be
    aget-object v17, v17, v12

    #@1c0
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/gsm/GsmConnection;->onStartedHolding()V

    #@1c3
    .line 917
    :cond_1c3
    const/16 v16, 0x1

    #@1c5
    goto :goto_16a

    #@1c6
    .line 921
    :cond_1c6
    if-eqz v5, :cond_21a

    #@1c8
    if-nez v7, :cond_21a

    #@1ca
    .line 924
    move-object/from16 v0, p0

    #@1cc
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@1ce
    move-object/from16 v17, v0

    #@1d0
    move-object/from16 v0, v17

    #@1d2
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d5
    .line 927
    move-object/from16 v0, p0

    #@1d7
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1d9
    move-object/from16 v17, v0

    #@1db
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@1de
    move-result-object v17

    #@1df
    const-string v18, "support_emergency_callback_mode_for_gsm"

    #@1e1
    invoke-static/range {v17 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1e4
    move-result v17

    #@1e5
    if-eqz v17, :cond_1fe

    #@1e7
    .line 929
    move-object/from16 v0, p0

    #@1e9
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mIsEcmTimerCanceled:Z

    #@1eb
    move/from16 v17, v0

    #@1ed
    if-eqz v17, :cond_1fe

    #@1ef
    .line 930
    move-object/from16 v0, p0

    #@1f1
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@1f3
    move-object/from16 v17, v0

    #@1f5
    const/16 v17, 0x0

    #@1f7
    move-object/from16 v0, p0

    #@1f9
    move/from16 v1, v17

    #@1fb
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->handleEcmTimer(I)V

    #@1fe
    .line 936
    :cond_1fe
    const-string v17, "isInEmergencyCall set to false handlePollCalls 2 call end"

    #@200
    move-object/from16 v0, p0

    #@202
    move-object/from16 v1, v17

    #@204
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@207
    .line 937
    const-string v17, "ril.cdma.emergencyCall"

    #@209
    const-string v18, "false"

    #@20b
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@20e
    .line 942
    move-object/from16 v0, p0

    #@210
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@212
    move-object/from16 v17, v0

    #@214
    const/16 v18, 0x0

    #@216
    aput-object v18, v17, v12

    #@218
    goto/16 :goto_16b

    #@21a
    .line 943
    :cond_21a
    if-eqz v5, :cond_28b

    #@21c
    if-eqz v7, :cond_28b

    #@21e
    invoke-virtual {v5, v7}, Lcom/android/internal/telephony/gsm/GsmConnection;->compareTo(Lcom/android/internal/telephony/DriverCall;)Z

    #@221
    move-result v17

    #@222
    if-nez v17, :cond_28b

    #@224
    .line 947
    move-object/from16 v0, p0

    #@226
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@228
    move-object/from16 v17, v0

    #@22a
    move-object/from16 v0, v17

    #@22c
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@22f
    .line 948
    move-object/from16 v0, p0

    #@231
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@233
    move-object/from16 v17, v0

    #@235
    new-instance v18, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@237
    move-object/from16 v0, p0

    #@239
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@23b
    move-object/from16 v19, v0

    #@23d
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@240
    move-result-object v19

    #@241
    move-object/from16 v0, v18

    #@243
    move-object/from16 v1, v19

    #@245
    move-object/from16 v2, p0

    #@247
    invoke-direct {v0, v1, v7, v2, v12}, Lcom/android/internal/telephony/gsm/GsmConnection;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/DriverCall;Lcom/android/internal/telephony/gsm/GsmCallTracker;I)V

    #@24a
    aput-object v18, v17, v12

    #@24c
    .line 950
    move-object/from16 v0, p0

    #@24e
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@250
    move-object/from16 v17, v0

    #@252
    aget-object v17, v17, v12

    #@254
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/gsm/GsmConnection;->getCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@257
    move-result-object v17

    #@258
    move-object/from16 v0, p0

    #@25a
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@25c
    move-object/from16 v18, v0

    #@25e
    move-object/from16 v0, v17

    #@260
    move-object/from16 v1, v18

    #@262
    if-ne v0, v1, :cond_27c

    #@264
    .line 953
    const-string v17, "isInEmergencyCall set to false handlePollCalls 3 MT "

    #@266
    move-object/from16 v0, p0

    #@268
    move-object/from16 v1, v17

    #@26a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@26d
    .line 954
    const-string v17, "ril.cdma.emergencyCall"

    #@26f
    const-string v18, "false"

    #@271
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@274
    .line 957
    move-object/from16 v0, p0

    #@276
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@278
    move-object/from16 v17, v0

    #@27a
    aget-object v14, v17, v12

    #@27c
    .line 965
    :cond_27c
    if-nez v14, :cond_286

    #@27e
    move-object/from16 v0, p0

    #@280
    invoke-direct {v0, v5, v7}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->okUpdateCall(Lcom/android/internal/telephony/gsm/GsmConnection;Lcom/android/internal/telephony/DriverCall;)Z

    #@283
    move-result v17

    #@284
    if-eqz v17, :cond_289

    #@286
    :cond_286
    const/4 v11, 0x1

    #@287
    :goto_287
    goto/16 :goto_16b

    #@289
    :cond_289
    const/4 v11, 0x0

    #@28a
    goto :goto_287

    #@28b
    .line 967
    :cond_28b
    if-eqz v5, :cond_16b

    #@28d
    if-eqz v7, :cond_16b

    #@28f
    .line 973
    const/4 v4, 0x0

    #@290
    .line 974
    .local v4, changed:Z
    move-object/from16 v0, p0

    #@292
    invoke-direct {v0, v5, v7}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->okUpdateCall(Lcom/android/internal/telephony/gsm/GsmConnection;Lcom/android/internal/telephony/DriverCall;)Z

    #@295
    move-result v17

    #@296
    if-eqz v17, :cond_31d

    #@298
    .line 976
    sget-boolean v17, Lcom/android/internal/telephony/gsm/GsmCallTracker;->CIQ_EXTENSION:Z

    #@29a
    if-eqz v17, :cond_2d4

    #@29c
    iget-object v0, v7, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@29e
    move-object/from16 v17, v0

    #@2a0
    sget-object v18, Lcom/android/internal/telephony/DriverCall$State;->INCOMING:Lcom/android/internal/telephony/DriverCall$State;

    #@2a2
    move-object/from16 v0, v17

    #@2a4
    move-object/from16 v1, v18

    #@2a6
    if-ne v0, v1, :cond_2d4

    #@2a8
    .line 977
    sget-boolean v17, Lcom/android/internal/telephony/gsm/GsmCallTracker;->DBG_CIQ:Z

    #@2aa
    if-eqz v17, :cond_2b5

    #@2ac
    const-string v17, "[CIQ_EXTENSION] update() handlePollCalls() RINGING call "

    #@2ae
    move-object/from16 v0, p0

    #@2b0
    move-object/from16 v1, v17

    #@2b2
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@2b5
    .line 978
    :cond_2b5
    move-object/from16 v0, p0

    #@2b7
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@2b9
    move-object/from16 v17, v0

    #@2bb
    aget-object v17, v17, v12

    #@2bd
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/gsm/GsmConnection;->getCIQIdForTMUS()I

    #@2c0
    move-result v17

    #@2c1
    move-object/from16 v0, p0

    #@2c3
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->connections:[Lcom/android/internal/telephony/gsm/GsmConnection;

    #@2c5
    move-object/from16 v18, v0

    #@2c7
    aget-object v18, v18, v12

    #@2c9
    move-object/from16 v0, v18

    #@2cb
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@2cd
    move-object/from16 v18, v0

    #@2cf
    sget-object v19, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    #@2d1
    invoke-static/range {v17 .. v19}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallStatus(ILjava/lang/String;Lcom/android/internal/telephony/Call$State;)V

    #@2d4
    .line 982
    :cond_2d4
    const-string v17, "1"

    #@2d6
    const-string v18, "service.iq.active"

    #@2d8
    invoke-static/range {v18 .. v18}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2db
    move-result-object v18

    #@2dc
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2df
    move-result v17

    #@2e0
    if-eqz v17, :cond_319

    #@2e2
    .line 983
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmConnection;->getState()Lcom/android/internal/telephony/Call$State;

    #@2e5
    move-result-object v17

    #@2e6
    sget-object v18, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@2e8
    move-object/from16 v0, v17

    #@2ea
    move-object/from16 v1, v18

    #@2ec
    if-ne v0, v1, :cond_319

    #@2ee
    iget-object v0, v7, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@2f0
    move-object/from16 v17, v0

    #@2f2
    sget-object v18, Lcom/android/internal/telephony/DriverCall$State;->ACTIVE:Lcom/android/internal/telephony/DriverCall$State;

    #@2f4
    move-object/from16 v0, v17

    #@2f6
    move-object/from16 v1, v18

    #@2f8
    if-ne v0, v1, :cond_319

    #@2fa
    .line 984
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS02Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@2fd
    move-result-object v10

    #@2fe
    .line 986
    .local v10, gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    if-eqz v10, :cond_324

    #@300
    .line 987
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmConnection;->getcqID()I

    #@303
    move-result v17

    #@304
    move/from16 v0, v17

    #@306
    iput v0, v10, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->dwCallId:I

    #@308
    .line 988
    const/16 v17, 0x4

    #@30a
    move/from16 v0, v17

    #@30c
    iput-byte v0, v10, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@30e
    .line 989
    move-object/from16 v0, p0

    #@310
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mContext:Landroid/content/Context;

    #@312
    move-object/from16 v17, v0

    #@314
    move-object/from16 v0, v17

    #@316
    invoke-static {v0, v10}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@319
    .line 996
    .end local v10           #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_319
    :goto_319
    invoke-virtual {v5, v7}, Lcom/android/internal/telephony/gsm/GsmConnection;->update(Lcom/android/internal/telephony/DriverCall;)Z

    #@31c
    move-result v4

    #@31d
    .line 998
    :cond_31d
    if-nez v11, :cond_321

    #@31f
    if-eqz v4, :cond_32c

    #@321
    :cond_321
    const/4 v11, 0x1

    #@322
    :goto_322
    goto/16 :goto_16b

    #@324
    .line 991
    .restart local v10       #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_324
    const-string v17, "LGDDM-CSC"

    #@326
    const-string v18, "CSC State Transition metric instance is null!"

    #@328
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32b
    goto :goto_319

    #@32c
    .line 998
    .end local v10           #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_32c
    const/4 v11, 0x0

    #@32d
    goto :goto_322

    #@32e
    .line 1025
    .end local v4           #changed:Z
    .end local v5           #conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    .end local v7           #dc:Lcom/android/internal/telephony/DriverCall;
    :cond_32e
    move-object/from16 v0, p0

    #@330
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@332
    move-object/from16 v17, v0

    #@334
    if-eqz v17, :cond_377

    #@336
    .line 1026
    const-string v17, "GSM"

    #@338
    new-instance v18, Ljava/lang/StringBuilder;

    #@33a
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@33d
    const-string v19, "Pending MO dropped before poll fg state:"

    #@33f
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@342
    move-result-object v18

    #@343
    move-object/from16 v0, p0

    #@345
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@347
    move-object/from16 v19, v0

    #@349
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@34c
    move-result-object v19

    #@34d
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@350
    move-result-object v18

    #@351
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@354
    move-result-object v18

    #@355
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@358
    .line 1029
    move-object/from16 v0, p0

    #@35a
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@35c
    move-object/from16 v17, v0

    #@35e
    move-object/from16 v0, p0

    #@360
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@362
    move-object/from16 v18, v0

    #@364
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@367
    .line 1030
    const/16 v17, 0x0

    #@369
    move-object/from16 v0, v17

    #@36b
    move-object/from16 v1, p0

    #@36d
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@36f
    .line 1031
    const/16 v17, 0x0

    #@371
    move/from16 v0, v17

    #@373
    move-object/from16 v1, p0

    #@375
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupPendingMO:Z

    #@377
    .line 1034
    :cond_377
    if-eqz v14, :cond_384

    #@379
    .line 1035
    move-object/from16 v0, p0

    #@37b
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@37d
    move-object/from16 v17, v0

    #@37f
    move-object/from16 v0, v17

    #@381
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyNewRingingConnection(Lcom/android/internal/telephony/Connection;)V

    #@384
    .line 1041
    :cond_384
    move-object/from16 v0, p0

    #@386
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@388
    move-object/from16 v17, v0

    #@38a
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@38d
    move-result v17

    #@38e
    add-int/lit8 v12, v17, -0x1

    #@390
    :goto_390
    if-ltz v12, :cond_494

    #@392
    .line 1042
    move-object/from16 v0, p0

    #@394
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@396
    move-object/from16 v17, v0

    #@398
    move-object/from16 v0, v17

    #@39a
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@39d
    move-result-object v5

    #@39e
    check-cast v5, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@3a0
    .line 1044
    .restart local v5       #conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmConnection;->isIncoming()Z

    #@3a3
    move-result v17

    #@3a4
    if-eqz v17, :cond_40c

    #@3a6
    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmConnection;->getConnectTime()J

    #@3a9
    move-result-wide v17

    #@3aa
    const-wide/16 v19, 0x0

    #@3ac
    cmp-long v17, v17, v19

    #@3ae
    if-nez v17, :cond_40c

    #@3b0
    .line 1047
    iget-object v0, v5, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3b2
    move-object/from16 v17, v0

    #@3b4
    sget-object v18, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3b6
    move-object/from16 v0, v17

    #@3b8
    move-object/from16 v1, v18

    #@3ba
    if-ne v0, v1, :cond_409

    #@3bc
    .line 1048
    sget-object v3, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3be
    .line 1054
    .local v3, cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    :goto_3be
    new-instance v17, Ljava/lang/StringBuilder;

    #@3c0
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@3c3
    const-string v18, "missed/rejected call, conn.cause="

    #@3c5
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c8
    move-result-object v17

    #@3c9
    iget-object v0, v5, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3cb
    move-object/from16 v18, v0

    #@3cd
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d0
    move-result-object v17

    #@3d1
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d4
    move-result-object v17

    #@3d5
    move-object/from16 v0, p0

    #@3d7
    move-object/from16 v1, v17

    #@3d9
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@3dc
    .line 1055
    new-instance v17, Ljava/lang/StringBuilder;

    #@3de
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@3e1
    const-string v18, "setting cause to "

    #@3e3
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e6
    move-result-object v17

    #@3e7
    move-object/from16 v0, v17

    #@3e9
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3ec
    move-result-object v17

    #@3ed
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f0
    move-result-object v17

    #@3f1
    move-object/from16 v0, p0

    #@3f3
    move-object/from16 v1, v17

    #@3f5
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@3f8
    .line 1057
    move-object/from16 v0, p0

    #@3fa
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@3fc
    move-object/from16 v17, v0

    #@3fe
    move-object/from16 v0, v17

    #@400
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@403
    .line 1058
    invoke-virtual {v5, v3}, Lcom/android/internal/telephony/gsm/GsmConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V

    #@406
    .line 1041
    .end local v3           #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    :cond_406
    :goto_406
    add-int/lit8 v12, v12, -0x1

    #@408
    goto :goto_390

    #@409
    .line 1050
    :cond_409
    sget-object v3, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_MISSED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@40b
    .restart local v3       #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    goto :goto_3be

    #@40c
    .line 1059
    .end local v3           #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    :cond_40c
    iget-object v0, v5, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@40e
    move-object/from16 v17, v0

    #@410
    sget-object v18, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@412
    move-object/from16 v0, v17

    #@414
    move-object/from16 v1, v18

    #@416
    if-ne v0, v1, :cond_42b

    #@418
    .line 1061
    move-object/from16 v0, p0

    #@41a
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@41c
    move-object/from16 v17, v0

    #@41e
    move-object/from16 v0, v17

    #@420
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@423
    .line 1062
    sget-object v17, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@425
    move-object/from16 v0, v17

    #@427
    invoke-virtual {v5, v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V

    #@42a
    goto :goto_406

    #@42b
    .line 1063
    :cond_42b
    iget-object v0, v5, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@42d
    move-object/from16 v17, v0

    #@42f
    sget-object v18, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@431
    move-object/from16 v0, v17

    #@433
    move-object/from16 v1, v18

    #@435
    if-ne v0, v1, :cond_44a

    #@437
    .line 1065
    move-object/from16 v0, p0

    #@439
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@43b
    move-object/from16 v17, v0

    #@43d
    move-object/from16 v0, v17

    #@43f
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@442
    .line 1066
    sget-object v17, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@444
    move-object/from16 v0, v17

    #@446
    invoke-virtual {v5, v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V

    #@449
    goto :goto_406

    #@44a
    .line 1069
    :cond_44a
    move-object/from16 v0, p0

    #@44c
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutExpired:Z

    #@44e
    move/from16 v17, v0

    #@450
    if-eqz v17, :cond_406

    #@452
    .line 1070
    new-instance v17, Ljava/lang/StringBuilder;

    #@454
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@457
    const-string v18, "mPollTimeoutExpired is trigger "

    #@459
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45c
    move-result-object v17

    #@45d
    move-object/from16 v0, p0

    #@45f
    iget-boolean v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutExpired:Z

    #@461
    move/from16 v18, v0

    #@463
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@466
    move-result-object v17

    #@467
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46a
    move-result-object v17

    #@46b
    move-object/from16 v0, p0

    #@46d
    move-object/from16 v1, v17

    #@46f
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@472
    .line 1071
    sget-object v17, Lcom/android/internal/telephony/Connection$DisconnectCause;->TIMED_OUT:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@474
    move-object/from16 v0, v17

    #@476
    iput-object v0, v5, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@478
    .line 1072
    move-object/from16 v0, p0

    #@47a
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@47c
    move-object/from16 v17, v0

    #@47e
    move-object/from16 v0, v17

    #@480
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@483
    .line 1073
    sget-object v17, Lcom/android/internal/telephony/Connection$DisconnectCause;->TIMED_OUT:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@485
    move-object/from16 v0, v17

    #@487
    invoke-virtual {v5, v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V

    #@48a
    .line 1074
    const/16 v17, 0x0

    #@48c
    move/from16 v0, v17

    #@48e
    move-object/from16 v1, p0

    #@490
    iput-boolean v0, v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mPollTimeoutExpired:Z

    #@492
    goto/16 :goto_406

    #@494
    .line 1080
    .end local v5           #conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :cond_494
    move-object/from16 v0, p0

    #@496
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@498
    move-object/from16 v17, v0

    #@49a
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@49d
    move-result v17

    #@49e
    if-lez v17, :cond_4b6

    #@4a0
    .line 1081
    move-object/from16 v0, p0

    #@4a2
    iget-object v0, v0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@4a4
    move-object/from16 v17, v0

    #@4a6
    const/16 v18, 0x5

    #@4a8
    move-object/from16 v0, p0

    #@4aa
    move/from16 v1, v18

    #@4ac
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainNoPollCompleteMessage(I)Landroid/os/Message;

    #@4af
    move-result-object v18

    #@4b0
    invoke-interface/range {v17 .. v18}, Lcom/android/internal/telephony/CommandsInterface;->getLastCallFailCause(Landroid/os/Message;)V

    #@4b3
    .line 1084
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->startOpDonePoll()V

    #@4b6
    .line 1088
    :cond_4b6
    if-eqz v13, :cond_4bb

    #@4b8
    .line 1089
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pollCallsAfterDelay()V

    #@4bb
    .line 1097
    :cond_4bb
    if-nez v14, :cond_4bf

    #@4bd
    if-eqz v11, :cond_4c2

    #@4bf
    .line 1098
    :cond_4bf
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->internalClearDisconnected()V

    #@4c2
    .line 1101
    :cond_4c2
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->updatePhoneState()V

    #@4c5
    .line 1103
    if-eqz v16, :cond_4d0

    #@4c7
    .line 1104
    move-object/from16 v0, p0

    #@4c9
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4cb
    move-object/from16 v17, v0

    #@4cd
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyUnknownConnection()V

    #@4d0
    .line 1107
    :cond_4d0
    if-nez v11, :cond_4d4

    #@4d2
    if-eqz v14, :cond_f1

    #@4d4
    .line 1108
    :cond_4d4
    move-object/from16 v0, p0

    #@4d6
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4d8
    move-object/from16 v17, v0

    #@4da
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPreciseCallStateChanged()V
    :try_end_4dd
    .catchall {:try_start_115 .. :try_end_4dd} :catchall_10e

    #@4dd
    goto/16 :goto_f1
.end method

.method hangup(Lcom/android/internal/telephony/gsm/GsmCall;)V
    .registers 5
    .parameter "call"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1215
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->getConnections()Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 1216
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "no connections in call"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 1219
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@14
    if-ne p1, v0, :cond_2d

    #@16
    .line 1220
    const-string v0, "(ringing) hangup waiting or background"

    #@18
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@1b
    .line 1221
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1d
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@20
    move-result-object v1

    #@21
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->hangupWaitingOrBackground(Landroid/os/Message;)V

    #@24
    .line 1245
    :goto_24
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->onHangupLocal()V

    #@27
    .line 1246
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@29
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPreciseCallStateChanged()V

    #@2c
    .line 1247
    return-void

    #@2d
    .line 1222
    :cond_2d
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2f
    if-ne p1, v0, :cond_51

    #@31
    .line 1223
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->isDialingOrAlerting()Z

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_4d

    #@37
    .line 1225
    const-string v0, "(foregnd) hangup dialing or alerting..."

    #@39
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@3c
    .line 1227
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->getConnections()Ljava/util/List;

    #@3f
    move-result-object v0

    #@40
    const/4 v1, 0x0

    #@41
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@44
    move-result-object v0

    #@45
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@47
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@49
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmConnection;)V

    #@4c
    goto :goto_24

    #@4d
    .line 1229
    :cond_4d
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupForegroundResumeBackground()V

    #@50
    goto :goto_24

    #@51
    .line 1231
    :cond_51
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@53
    if-ne p1, v0, :cond_6a

    #@55
    .line 1232
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@57
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->isRinging()Z

    #@5a
    move-result v0

    #@5b
    if-eqz v0, :cond_66

    #@5d
    .line 1234
    const-string v0, "hangup all conns in background call"

    #@5f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@62
    .line 1236
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupAllConnections(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@65
    goto :goto_24

    #@66
    .line 1238
    :cond_66
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupWaitingOrBackground()V

    #@69
    goto :goto_24

    #@6a
    .line 1241
    :cond_6a
    new-instance v0, Ljava/lang/RuntimeException;

    #@6c
    new-instance v1, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v2, "GsmCall "

    #@73
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v1

    #@77
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    const-string v2, "does not belong to GsmCallTracker "

    #@7d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v1

    #@81
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v1

    #@85
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v1

    #@89
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@8c
    throw v0
.end method

.method hangup(Lcom/android/internal/telephony/gsm/GsmCall;Z)V
    .registers 6
    .parameter "call"
    .parameter "answerWaiting"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1349
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->getConnections()Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 1350
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "no connections in call"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 1353
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@14
    if-ne p1, v0, :cond_2d

    #@16
    .line 1354
    const-string v0, "(ringing) hangup waiting or background"

    #@18
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@1b
    .line 1355
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1d
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@20
    move-result-object v1

    #@21
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->hangupWaitingOrBackground(Landroid/os/Message;)V

    #@24
    .line 1382
    :goto_24
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->onHangupLocal()V

    #@27
    .line 1383
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@29
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPreciseCallStateChanged()V

    #@2c
    .line 1384
    return-void

    #@2d
    .line 1356
    :cond_2d
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2f
    if-ne p1, v0, :cond_57

    #@31
    .line 1357
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->isDialingOrAlerting()Z

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_4d

    #@37
    .line 1359
    const-string v0, "(foregnd) hangup dialing or alerting..."

    #@39
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@3c
    .line 1361
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->getConnections()Ljava/util/List;

    #@3f
    move-result-object v0

    #@40
    const/4 v1, 0x0

    #@41
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@44
    move-result-object v0

    #@45
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@47
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@49
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmConnection;)V

    #@4c
    goto :goto_24

    #@4d
    .line 1363
    :cond_4d
    if-eqz p2, :cond_53

    #@4f
    .line 1364
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupForegroundResumeBackground()V

    #@52
    goto :goto_24

    #@53
    .line 1366
    :cond_53
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupAllConnections(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@56
    goto :goto_24

    #@57
    .line 1368
    :cond_57
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@59
    if-ne p1, v0, :cond_70

    #@5b
    .line 1369
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@5d
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->isRinging()Z

    #@60
    move-result v0

    #@61
    if-eqz v0, :cond_6c

    #@63
    .line 1371
    const-string v0, "hangup all conns in background call"

    #@65
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@68
    .line 1373
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupAllConnections(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@6b
    goto :goto_24

    #@6c
    .line 1375
    :cond_6c
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupWaitingOrBackground()V

    #@6f
    goto :goto_24

    #@70
    .line 1378
    :cond_70
    new-instance v0, Ljava/lang/RuntimeException;

    #@72
    new-instance v1, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v2, "GsmCall "

    #@79
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v1

    #@7d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v1

    #@81
    const-string v2, "does not belong to GsmCallTracker "

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v1

    #@8b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v1

    #@8f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@92
    throw v0
.end method

.method hangup(Lcom/android/internal/telephony/gsm/GsmConnection;)V
    .registers 6
    .parameter "conn"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1155
    iget-object v1, p1, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    if-eq v1, p0, :cond_27

    #@4
    .line 1156
    new-instance v1, Lcom/android/internal/telephony/CallStateException;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "GsmConnection "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "does not belong to GsmCallTracker "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 1160
    :cond_27
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->pendingMO:Lcom/android/internal/telephony/gsm/GsmConnection;

    #@29
    if-ne p1, v1, :cond_37

    #@2b
    .line 1164
    const-string v1, "hangup: set hangupPendingMO to true"

    #@2d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@30
    .line 1165
    const/4 v1, 0x1

    #@31
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupPendingMO:Z

    #@33
    .line 1177
    :goto_33
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->onHangupLocal()V

    #@36
    .line 1178
    return-void

    #@37
    .line 1168
    :cond_37
    :try_start_37
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@39
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->getGSMIndex()I

    #@3c
    move-result v2

    #@3d
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@40
    move-result-object v3

    #@41
    invoke-interface {v1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->hangupConnection(ILandroid/os/Message;)V
    :try_end_44
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_37 .. :try_end_44} :catch_45

    #@44
    goto :goto_33

    #@45
    .line 1169
    :catch_45
    move-exception v0

    #@46
    .line 1172
    .local v0, ex:Lcom/android/internal/telephony/CallStateException;
    const-string v1, "GSM"

    #@48
    new-instance v2, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v3, "GsmCallTracker WARN: hangup() on absent connection "

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    goto :goto_33
.end method

.method hangupAllCalls()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1315
    const/4 v0, 0x0

    #@1
    .line 1316
    .local v0, hungUp:Z
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@3
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->isIdle()Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_27

    #@9
    .line 1319
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@b
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@e
    move-result-object v1

    #@f
    sget-object v2, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    #@11
    if-eq v1, v2, :cond_27

    #@13
    .line 1320
    const-string v1, "hangupAllCalls: hang up ringing call"

    #@15
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@18
    .line 1321
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1a
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@1d
    move-result-object v2

    #@1e
    invoke-interface {v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->hangupWaitingOrBackground(Landroid/os/Message;)V

    #@21
    .line 1322
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@23
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->onHangupLocal()V

    #@26
    .line 1323
    const/4 v0, 0x1

    #@27
    .line 1326
    :cond_27
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@29
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->isIdle()Z

    #@2c
    move-result v1

    #@2d
    if-nez v1, :cond_3f

    #@2f
    .line 1327
    const-string v1, "hangupAllCalls: hang up active call"

    #@31
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@34
    .line 1328
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@36
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupAllConnections(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@39
    .line 1329
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@3b
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->onHangupLocal()V

    #@3e
    .line 1330
    const/4 v0, 0x1

    #@3f
    .line 1332
    :cond_3f
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@41
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->isIdle()Z

    #@44
    move-result v1

    #@45
    if-nez v1, :cond_57

    #@47
    .line 1333
    const-string v1, "hangupAllCalls: hang up held call"

    #@49
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@4c
    .line 1334
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@4e
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupAllConnections(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@51
    .line 1335
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@53
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmCall;->onHangupLocal()V

    #@56
    .line 1336
    const/4 v0, 0x1

    #@57
    .line 1338
    :cond_57
    if-eqz v0, :cond_5f

    #@59
    .line 1339
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@5b
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPreciseCallStateChanged()V

    #@5e
    .line 1343
    return-void

    #@5f
    .line 1341
    :cond_5f
    new-instance v1, Lcom/android/internal/telephony/CallStateException;

    #@61
    const-string v2, "no active connections to hangup"

    #@63
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@66
    throw v1
.end method

.method hangupAllConnections(Lcom/android/internal/telephony/gsm/GsmCall;)V
    .registers 9
    .parameter "call"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1415
    :try_start_0
    iget-object v4, p1, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 1416
    .local v1, count:I
    const/4 v3, 0x0

    #@7
    .local v3, i:I
    :goto_7
    if-ge v3, v1, :cond_3a

    #@9
    .line 1417
    iget-object v4, p1, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@11
    .line 1418
    .local v0, cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    iget-object v4, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@13
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->getGSMIndex()I

    #@16
    move-result v5

    #@17
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@1a
    move-result-object v6

    #@1b
    invoke-interface {v4, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->hangupConnection(ILandroid/os/Message;)V
    :try_end_1e
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_1e} :catch_21

    #@1e
    .line 1416
    add-int/lit8 v3, v3, 0x1

    #@20
    goto :goto_7

    #@21
    .line 1420
    .end local v0           #cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    .end local v1           #count:I
    .end local v3           #i:I
    :catch_21
    move-exception v2

    #@22
    .line 1421
    .local v2, ex:Lcom/android/internal/telephony/CallStateException;
    const-string v4, "GSM"

    #@24
    new-instance v5, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v6, "hangupConnectionByIndex caught "

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1423
    .end local v2           #ex:Lcom/android/internal/telephony/CallStateException;
    :cond_3a
    return-void
.end method

.method hangupConnectionByIndex(Lcom/android/internal/telephony/gsm/GsmCall;I)V
    .registers 8
    .parameter "call"
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1401
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 1402
    .local v1, count:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v1, :cond_24

    #@9
    .line 1403
    iget-object v3, p1, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@11
    .line 1404
    .local v0, cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->getGSMIndex()I

    #@14
    move-result v3

    #@15
    if-ne v3, p2, :cond_21

    #@17
    .line 1405
    iget-object v3, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@19
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@1c
    move-result-object v4

    #@1d
    invoke-interface {v3, p2, v4}, Lcom/android/internal/telephony/CommandsInterface;->hangupConnection(ILandroid/os/Message;)V

    #@20
    .line 1406
    return-void

    #@21
    .line 1402
    :cond_21
    add-int/lit8 v2, v2, 0x1

    #@23
    goto :goto_7

    #@24
    .line 1410
    .end local v0           #cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :cond_24
    new-instance v3, Lcom/android/internal/telephony/CallStateException;

    #@26
    const-string v4, "no gsm index found"

    #@28
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v3
.end method

.method hangupForVoIP(Lcom/android/internal/telephony/gsm/GsmCall;)V
    .registers 5
    .parameter "call"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1278
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->getConnections()Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 1279
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "no connections in call"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 1282
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@14
    if-ne p1, v0, :cond_28

    #@16
    .line 1283
    const-string v0, "(ringing) hangup waiting or background"

    #@18
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@1b
    .line 1284
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1d
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@20
    move-result-object v1

    #@21
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->hangupWaitingOrBackground(Landroid/os/Message;)V

    #@24
    .line 1308
    :goto_24
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->onHangupLocalMissed()V

    #@27
    .line 1309
    return-void

    #@28
    .line 1285
    :cond_28
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2a
    if-ne p1, v0, :cond_4c

    #@2c
    .line 1286
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->isDialingOrAlerting()Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_48

    #@32
    .line 1288
    const-string v0, "(foregnd) hangup dialing or alerting..."

    #@34
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@37
    .line 1290
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->getConnections()Ljava/util/List;

    #@3a
    move-result-object v0

    #@3b
    const/4 v1, 0x0

    #@3c
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3f
    move-result-object v0

    #@40
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@42
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@44
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmConnection;)V

    #@47
    goto :goto_24

    #@48
    .line 1292
    :cond_48
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupForegroundResumeBackground()V

    #@4b
    goto :goto_24

    #@4c
    .line 1294
    :cond_4c
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@4e
    if-ne p1, v0, :cond_65

    #@50
    .line 1295
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@52
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->isRinging()Z

    #@55
    move-result v0

    #@56
    if-eqz v0, :cond_61

    #@58
    .line 1297
    const-string v0, "hangup all conns in background call"

    #@5a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@5d
    .line 1299
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupAllConnections(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@60
    goto :goto_24

    #@61
    .line 1301
    :cond_61
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupWaitingOrBackground()V

    #@64
    goto :goto_24

    #@65
    .line 1304
    :cond_65
    new-instance v0, Ljava/lang/RuntimeException;

    #@67
    new-instance v1, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v2, "GsmCall "

    #@6e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v1

    #@72
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v1

    #@76
    const-string v2, "does not belong to GsmCallTracker "

    #@78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v1

    #@7c
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v1

    #@80
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v1

    #@84
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@87
    throw v0
.end method

.method hangupForegroundResumeBackground()V
    .registers 3

    #@0
    .prologue
    .line 1395
    const-string v0, "hangupForegroundResumeBackground"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@5
    .line 1396
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@a
    move-result-object v1

    #@b
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->hangupForegroundResumeBackground(Landroid/os/Message;)V

    #@e
    .line 1397
    return-void
.end method

.method hangupNotResume(Lcom/android/internal/telephony/gsm/GsmCall;)V
    .registers 5
    .parameter "call"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1251
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->getConnections()Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 1252
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "no connections in call"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 1255
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@14
    if-ne p1, v0, :cond_3e

    #@16
    .line 1256
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->isDialingOrAlerting()Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_3a

    #@1c
    .line 1258
    const-string v0, "(foregnd) hangupNotResume dialing or alerting..."

    #@1e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@21
    .line 1260
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->getConnections()Ljava/util/List;

    #@24
    move-result-object v0

    #@25
    const/4 v1, 0x0

    #@26
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@29
    move-result-object v0

    #@2a
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@2c
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@2e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmConnection;)V

    #@31
    .line 1269
    :goto_31
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmCall;->onHangupLocal()V

    #@34
    .line 1270
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@36
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPreciseCallStateChanged()V

    #@39
    .line 1271
    return-void

    #@3a
    .line 1262
    :cond_3a
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupAllConnections(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@3d
    goto :goto_31

    #@3e
    .line 1265
    :cond_3e
    new-instance v0, Ljava/lang/RuntimeException;

    #@40
    new-instance v1, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v2, "GsmCall "

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    const-string v2, "does not belong to GsmCallTracker "

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v1

    #@59
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v1

    #@5d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@60
    throw v0
.end method

.method hangupWaitingOrBackground()V
    .registers 3

    #@0
    .prologue
    .line 1389
    const-string v0, "hangupWaitingOrBackground"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->log(Ljava/lang/String;)V

    #@5
    .line 1390
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@a
    move-result-object v1

    #@b
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->hangupWaitingOrBackground(Landroid/os/Message;)V

    #@e
    .line 1391
    return-void
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 1685
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[GsmCallTracker] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1686
    return-void
.end method

.method public registerForVoiceCallEnded(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 250
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 251
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->voiceCallEndedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 252
    return-void
.end method

.method public registerForVoiceCallStarted(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 241
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 242
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->voiceCallStartedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 243
    return-void
.end method

.method rejectCall()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 564
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->isRinging()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_16

    #@c
    .line 565
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@11
    move-result-object v1

    #@12
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->rejectCall(Landroid/os/Message;)V

    #@15
    .line 569
    return-void

    #@16
    .line 567
    :cond_16
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@18
    const-string v1, "phone not ringing"

    #@1a
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0
.end method

.method separate(Lcom/android/internal/telephony/gsm/GsmConnection;)V
    .registers 6
    .parameter "conn"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1182
    iget-object v1, p1, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    if-eq v1, p0, :cond_27

    #@4
    .line 1183
    new-instance v1, Lcom/android/internal/telephony/CallStateException;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "GsmConnection "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "does not belong to GsmCallTracker "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 1187
    :cond_27
    :try_start_27
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@29
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->getGSMIndex()I

    #@2c
    move-result v2

    #@2d
    const/16 v3, 0xc

    #@2f
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage(I)Landroid/os/Message;

    #@32
    move-result-object v3

    #@33
    invoke-interface {v1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->separateConnection(ILandroid/os/Message;)V
    :try_end_36
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_27 .. :try_end_36} :catch_37

    #@36
    .line 1195
    :goto_36
    return-void

    #@37
    .line 1189
    :catch_37
    move-exception v0

    #@38
    .line 1192
    .local v0, ex:Lcom/android/internal/telephony/CallStateException;
    const-string v1, "GSM"

    #@3a
    new-instance v2, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v3, "GsmCallTracker WARN: separate() on absent connection "

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    goto :goto_36
.end method

.method setMute(Z)V
    .registers 5
    .parameter "mute"

    #@0
    .prologue
    .line 1201
    iput-boolean p1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->desiredMute:Z

    #@2
    .line 1202
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@4
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->desiredMute:Z

    #@6
    const/4 v2, 0x0

    #@7
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setMute(ZLandroid/os/Message;)V

    #@a
    .line 1203
    return-void
.end method

.method switchWaitingOrHoldingAndActive()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 574
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@5
    move-result-object v0

    #@6
    sget-object v1, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    #@8
    if-ne v0, v1, :cond_12

    #@a
    .line 575
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "cannot be in the incoming state"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 576
    :cond_12
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->callSwitchPending:Z

    #@14
    if-nez v0, :cond_25

    #@16
    .line 577
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@18
    const/16 v1, 0x8

    #@1a
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage(I)Landroid/os/Message;

    #@1d
    move-result-object v1

    #@1e
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->switchWaitingOrHoldingAndActive(Landroid/os/Message;)V

    #@21
    .line 579
    const/4 v0, 0x1

    #@22
    iput-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->callSwitchPending:Z

    #@24
    .line 583
    :goto_24
    return-void

    #@25
    .line 581
    :cond_25
    const-string v0, "GSM"

    #@27
    const-string v1, "Call Switch request ignored due to pending response"

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_24
.end method

.method switchWaitingOrHoldingAndActive(I)V
    .registers 4
    .parameter "clirMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 587
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@5
    move-result-object v0

    #@6
    sget-object v1, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    #@8
    if-ne v0, v1, :cond_12

    #@a
    .line 588
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "cannot be in the incoming state"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 590
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@14
    const/16 v1, 0x8

    #@16
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->obtainCompleteMessage(II)Landroid/os/Message;

    #@19
    move-result-object v1

    #@1a
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->switchWaitingOrHoldingAndActive(Landroid/os/Message;)V

    #@1d
    .line 592
    const/4 v0, 0x1

    #@1e
    iput-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->callSwitchPending:Z

    #@20
    .line 594
    return-void
.end method

.method public unregisterForVoiceCallEnded(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 255
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->voiceCallEndedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 256
    return-void
.end method

.method public unregisterForVoiceCallStarted(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 246
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->voiceCallStartedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 247
    return-void
.end method
