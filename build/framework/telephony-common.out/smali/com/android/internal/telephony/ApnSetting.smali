.class public Lcom/android/internal/telephony/ApnSetting;
.super Lcom/android/internal/telephony/DataProfile;
.source "ApnSetting.java"


# static fields
.field static final V2_FORMAT_REGEX:Ljava/lang/String; = "^\\[ApnSettingV2\\]\\s*"


# instance fields
.field public final carrier:Ljava/lang/String;

.field public final carrierEnabled:Z

.field public mTetheredCallOn:Z

.field public final mmsPort:Ljava/lang/String;

.field public final mmsProxy:Ljava/lang/String;

.field public final mmsc:Ljava/lang/String;

.field public final port:Ljava/lang/String;

.field public final proxy:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
    .registers 30
    .parameter "id"
    .parameter "numeric"
    .parameter "carrier"
    .parameter "apn"
    .parameter "proxy"
    .parameter "port"
    .parameter "mmsc"
    .parameter "mmsProxy"
    .parameter "mmsPort"
    .parameter "user"
    .parameter "password"
    .parameter "authType"
    .parameter "types"
    .parameter "protocol"
    .parameter "roamingProtocol"
    .parameter "carrierEnabled"
    .parameter "bearer"

    #@0
    .prologue
    .line 48
    move-object v1, p0

    #@1
    move v2, p1

    #@2
    move-object v3, p2

    #@3
    move-object/from16 v4, p4

    #@5
    move-object/from16 v5, p10

    #@7
    move-object/from16 v6, p11

    #@9
    move/from16 v7, p12

    #@b
    move-object/from16 v8, p13

    #@d
    move-object/from16 v9, p14

    #@f
    move-object/from16 v10, p15

    #@11
    move/from16 v11, p17

    #@13
    invoke-direct/range {v1 .. v11}, Lcom/android/internal/telephony/DataProfile;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    #@16
    .line 41
    const/4 v1, 0x0

    #@17
    iput-boolean v1, p0, Lcom/android/internal/telephony/ApnSetting;->mTetheredCallOn:Z

    #@19
    .line 51
    iput-object p3, p0, Lcom/android/internal/telephony/ApnSetting;->carrier:Ljava/lang/String;

    #@1b
    .line 52
    move-object/from16 v0, p5

    #@1d
    iput-object v0, p0, Lcom/android/internal/telephony/ApnSetting;->proxy:Ljava/lang/String;

    #@1f
    .line 53
    move-object/from16 v0, p6

    #@21
    iput-object v0, p0, Lcom/android/internal/telephony/ApnSetting;->port:Ljava/lang/String;

    #@23
    .line 54
    move-object/from16 v0, p7

    #@25
    iput-object v0, p0, Lcom/android/internal/telephony/ApnSetting;->mmsc:Ljava/lang/String;

    #@27
    .line 55
    move-object/from16 v0, p8

    #@29
    iput-object v0, p0, Lcom/android/internal/telephony/ApnSetting;->mmsProxy:Ljava/lang/String;

    #@2b
    .line 56
    move-object/from16 v0, p9

    #@2d
    iput-object v0, p0, Lcom/android/internal/telephony/ApnSetting;->mmsPort:Ljava/lang/String;

    #@2f
    .line 57
    move/from16 v0, p16

    #@31
    iput-boolean v0, p0, Lcom/android/internal/telephony/ApnSetting;->carrierEnabled:Z

    #@33
    .line 58
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/android/internal/telephony/ApnSetting;
    .registers 23
    .parameter "data"

    #@0
    .prologue
    .line 84
    if-nez p0, :cond_4

    #@2
    const/4 v1, 0x0

    #@3
    .line 133
    :goto_3
    return-object v1

    #@4
    .line 88
    :cond_4
    const-string v1, "^\\[ApnSettingV2\\]\\s*.*"

    #@6
    move-object/from16 v0, p0

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_2b

    #@e
    .line 89
    const/16 v21, 0x2

    #@10
    .line 90
    .local v21, version:I
    const-string v1, "^\\[ApnSettingV2\\]\\s*"

    #@12
    const-string v2, ""

    #@14
    move-object/from16 v0, p0

    #@16
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object p0

    #@1a
    .line 95
    :goto_1a
    const-string v1, "\\s*,\\s*"

    #@1c
    move-object/from16 v0, p0

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@21
    move-result-object v19

    #@22
    .line 96
    .local v19, a:[Ljava/lang/String;
    move-object/from16 v0, v19

    #@24
    array-length v1, v0

    #@25
    const/16 v2, 0xe

    #@27
    if-ge v1, v2, :cond_2e

    #@29
    .line 97
    const/4 v1, 0x0

    #@2a
    goto :goto_3

    #@2b
    .line 92
    .end local v19           #a:[Ljava/lang/String;
    .end local v21           #version:I
    :cond_2b
    const/16 v21, 0x1

    #@2d
    .restart local v21       #version:I
    goto :goto_1a

    #@2e
    .line 102
    .restart local v19       #a:[Ljava/lang/String;
    :cond_2e
    const/16 v1, 0xc

    #@30
    :try_start_30
    aget-object v1, v19, v1

    #@32
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_35} :catch_95

    #@35
    move-result v13

    #@36
    .line 111
    .local v13, authType:I
    :goto_36
    const/4 v1, 0x1

    #@37
    move/from16 v0, v21

    #@39
    if-ne v0, v1, :cond_98

    #@3b
    .line 112
    move-object/from16 v0, v19

    #@3d
    array-length v1, v0

    #@3e
    add-int/lit8 v1, v1, -0xd

    #@40
    new-array v14, v1, [Ljava/lang/String;

    #@42
    .line 113
    .local v14, typeArray:[Ljava/lang/String;
    const/16 v1, 0xd

    #@44
    const/4 v2, 0x0

    #@45
    move-object/from16 v0, v19

    #@47
    array-length v3, v0

    #@48
    add-int/lit8 v3, v3, -0xd

    #@4a
    move-object/from16 v0, v19

    #@4c
    invoke-static {v0, v1, v14, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@4f
    .line 114
    const-string v15, "IP"

    #@51
    .line 115
    .local v15, protocol:Ljava/lang/String;
    const-string v16, "IP"

    #@53
    .line 116
    .local v16, roamingProtocol:Ljava/lang/String;
    const/16 v17, 0x1

    #@55
    .line 117
    .local v17, carrierEnabled:Z
    const/16 v18, 0x0

    #@57
    .line 133
    .local v18, bearer:I
    :goto_57
    new-instance v1, Lcom/android/internal/telephony/ApnSetting;

    #@59
    const/4 v2, -0x1

    #@5a
    new-instance v3, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const/16 v4, 0xa

    #@61
    aget-object v4, v19, v4

    #@63
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v3

    #@67
    const/16 v4, 0xb

    #@69
    aget-object v4, v19, v4

    #@6b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    const/4 v4, 0x0

    #@74
    aget-object v4, v19, v4

    #@76
    const/4 v5, 0x1

    #@77
    aget-object v5, v19, v5

    #@79
    const/4 v6, 0x2

    #@7a
    aget-object v6, v19, v6

    #@7c
    const/4 v7, 0x3

    #@7d
    aget-object v7, v19, v7

    #@7f
    const/4 v8, 0x7

    #@80
    aget-object v8, v19, v8

    #@82
    const/16 v9, 0x8

    #@84
    aget-object v9, v19, v9

    #@86
    const/16 v10, 0x9

    #@88
    aget-object v10, v19, v10

    #@8a
    const/4 v11, 0x4

    #@8b
    aget-object v11, v19, v11

    #@8d
    const/4 v12, 0x5

    #@8e
    aget-object v12, v19, v12

    #@90
    invoke-direct/range {v1 .. v18}, Lcom/android/internal/telephony/ApnSetting;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    #@93
    goto/16 :goto_3

    #@95
    .line 103
    .end local v13           #authType:I
    .end local v14           #typeArray:[Ljava/lang/String;
    .end local v15           #protocol:Ljava/lang/String;
    .end local v16           #roamingProtocol:Ljava/lang/String;
    .end local v17           #carrierEnabled:Z
    .end local v18           #bearer:I
    :catch_95
    move-exception v20

    #@96
    .line 104
    .local v20, e:Ljava/lang/Exception;
    const/4 v13, 0x0

    #@97
    .restart local v13       #authType:I
    goto :goto_36

    #@98
    .line 119
    .end local v20           #e:Ljava/lang/Exception;
    :cond_98
    move-object/from16 v0, v19

    #@9a
    array-length v1, v0

    #@9b
    const/16 v2, 0x12

    #@9d
    if-ge v1, v2, :cond_a2

    #@9f
    .line 120
    const/4 v1, 0x0

    #@a0
    goto/16 :goto_3

    #@a2
    .line 122
    :cond_a2
    const/16 v1, 0xd

    #@a4
    aget-object v1, v19, v1

    #@a6
    const-string v2, "\\s*\\|\\s*"

    #@a8
    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@ab
    move-result-object v14

    #@ac
    .line 123
    .restart local v14       #typeArray:[Ljava/lang/String;
    const/16 v1, 0xe

    #@ae
    aget-object v15, v19, v1

    #@b0
    .line 124
    .restart local v15       #protocol:Ljava/lang/String;
    const/16 v1, 0xf

    #@b2
    aget-object v16, v19, v1

    #@b4
    .line 126
    .restart local v16       #roamingProtocol:Ljava/lang/String;
    const/16 v1, 0x10

    #@b6
    :try_start_b6
    aget-object v1, v19, v1

    #@b8
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z
    :try_end_bb
    .catch Ljava/lang/Exception; {:try_start_b6 .. :try_end_bb} :catch_c5

    #@bb
    move-result v17

    #@bc
    .line 130
    .restart local v17       #carrierEnabled:Z
    :goto_bc
    const/16 v1, 0x11

    #@be
    aget-object v1, v19, v1

    #@c0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@c3
    move-result v18

    #@c4
    .restart local v18       #bearer:I
    goto :goto_57

    #@c5
    .line 127
    .end local v17           #carrierEnabled:Z
    .end local v18           #bearer:I
    :catch_c5
    move-exception v20

    #@c6
    .line 128
    .restart local v20       #e:Ljava/lang/Exception;
    const/16 v17, 0x1

    #@c8
    .restart local v17       #carrierEnabled:Z
    goto :goto_bc
.end method


# virtual methods
.method public canHandleType(Ljava/lang/String;)Z
    .registers 11
    .parameter "type"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 181
    const-string v7, "VZW"

    #@4
    const-string v8, "ro.build.target_operator"

    #@6
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v8

    #@a
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v7

    #@e
    if-eqz v7, :cond_7a

    #@10
    const-string v7, "supl"

    #@12
    invoke-virtual {p1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@15
    move-result v7

    #@16
    if-eqz v7, :cond_7a

    #@18
    move v2, v5

    #@19
    .line 183
    .local v2, isSupportSuplTypeOnDefault:Z
    :goto_19
    iget-object v0, p0, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@1b
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@1c
    .local v3, len$:I
    const/4 v1, 0x0

    #@1d
    .local v1, i$:I
    :goto_1d
    if-ge v1, v3, :cond_7f

    #@1f
    aget-object v4, v0, v1

    #@21
    .line 185
    .local v4, t:Ljava/lang/String;
    invoke-virtual {v4, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@24
    move-result v7

    #@25
    if-nez v7, :cond_79

    #@27
    const-string v7, "*"

    #@29
    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2c
    move-result v7

    #@2d
    if-eqz v7, :cond_37

    #@2f
    const-string v7, "entitlement"

    #@31
    invoke-virtual {p1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@34
    move-result v7

    #@35
    if-eqz v7, :cond_79

    #@37
    :cond_37
    const-string v7, "*"

    #@39
    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3c
    move-result v7

    #@3d
    if-eqz v7, :cond_47

    #@3f
    const-string v7, "rcs"

    #@41
    invoke-virtual {p1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@44
    move-result v7

    #@45
    if-eqz v7, :cond_79

    #@47
    :cond_47
    const-string v7, "*"

    #@49
    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@4c
    move-result v7

    #@4d
    if-eqz v7, :cond_57

    #@4f
    const-string v7, "bip"

    #@51
    invoke-virtual {p1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@54
    move-result v7

    #@55
    if-eqz v7, :cond_79

    #@57
    :cond_57
    const-string v7, "default"

    #@59
    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c
    move-result v7

    #@5d
    if-eqz v7, :cond_67

    #@5f
    const-string v7, "supl"

    #@61
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v7

    #@65
    if-nez v7, :cond_79

    #@67
    :cond_67
    const-string v7, "default"

    #@69
    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@6c
    move-result v7

    #@6d
    if-eqz v7, :cond_7c

    #@6f
    const-string v7, "hipri"

    #@71
    invoke-virtual {p1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@74
    move-result v7

    #@75
    if-nez v7, :cond_79

    #@77
    if-eqz v2, :cond_7c

    #@79
    .line 204
    .end local v4           #t:Ljava/lang/String;
    :cond_79
    :goto_79
    return v5

    #@7a
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #isSupportSuplTypeOnDefault:Z
    .end local v3           #len$:I
    :cond_7a
    move v2, v6

    #@7b
    .line 181
    goto :goto_19

    #@7c
    .line 183
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #i$:I
    .restart local v2       #isSupportSuplTypeOnDefault:Z
    .restart local v3       #len$:I
    .restart local v4       #t:Ljava/lang/String;
    :cond_7c
    add-int/lit8 v1, v1, 0x1

    #@7e
    goto :goto_1d

    #@7f
    .end local v4           #t:Ljava/lang/String;
    :cond_7f
    move v5, v6

    #@80
    .line 204
    goto :goto_79
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "o"

    #@0
    .prologue
    .line 210
    instance-of v0, p1, Lcom/android/internal/telephony/ApnSetting;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 211
    :goto_5
    return v0

    #@6
    :cond_6
    invoke-virtual {p0}, Lcom/android/internal/telephony/ApnSetting;->toString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    goto :goto_5
.end method

.method public getDataProfileType()Lcom/android/internal/telephony/DataProfile$DataProfileType;
    .registers 2

    #@0
    .prologue
    .line 169
    sget-object v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_APN:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@2
    return-object v0
.end method

.method public getProfileId()I
    .registers 2

    #@0
    .prologue
    .line 174
    iget v0, p0, Lcom/android/internal/telephony/DataProfile;->id:I

    #@2
    return v0
.end method

.method public setProfileId(I)V
    .registers 2
    .parameter "profileId"

    #@0
    .prologue
    .line 227
    return-void
.end method

.method public toHash()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/android/internal/telephony/ApnSetting;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public toShortString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 216
    const-string v0, "ApnSetting"

    #@2
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 138
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 139
    .local v1, sb:Ljava/lang/StringBuilder;
    const-string v2, "[ApnSettingV2] "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    iget-object v3, p0, Lcom/android/internal/telephony/ApnSetting;->carrier:Ljava/lang/String;

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, ", "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    iget v3, p0, Lcom/android/internal/telephony/DataProfile;->id:I

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, ", "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    iget-object v3, p0, Lcom/android/internal/telephony/DataProfile;->numeric:Ljava/lang/String;

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, ", "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    iget-object v3, p0, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    const-string v3, ", "

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    iget-object v3, p0, Lcom/android/internal/telephony/ApnSetting;->proxy:Ljava/lang/String;

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    const-string v3, ", "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    iget-object v3, p0, Lcom/android/internal/telephony/ApnSetting;->mmsc:Ljava/lang/String;

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    const-string v3, ", "

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    iget-object v3, p0, Lcom/android/internal/telephony/ApnSetting;->mmsProxy:Ljava/lang/String;

    #@55
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    const-string v3, ", "

    #@5b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v2

    #@5f
    iget-object v3, p0, Lcom/android/internal/telephony/ApnSetting;->mmsPort:Ljava/lang/String;

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    const-string v3, ", "

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    iget-object v3, p0, Lcom/android/internal/telephony/ApnSetting;->port:Ljava/lang/String;

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    const-string v3, ", "

    #@73
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v2

    #@77
    iget-object v3, p0, Lcom/android/internal/telephony/DataProfile;->user:Ljava/lang/String;

    #@79
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v2

    #@7d
    const-string v3, ", "

    #@7f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v2

    #@83
    iget-object v3, p0, Lcom/android/internal/telephony/DataProfile;->password:Ljava/lang/String;

    #@85
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v2

    #@89
    const-string v3, ", "

    #@8b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v2

    #@8f
    iget v3, p0, Lcom/android/internal/telephony/DataProfile;->authType:I

    #@91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v2

    #@95
    const-string v3, ", "

    #@97
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    .line 154
    const/4 v0, 0x0

    #@9b
    .local v0, i:I
    :goto_9b
    iget-object v2, p0, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@9d
    array-length v2, v2

    #@9e
    if-ge v0, v2, :cond_b6

    #@a0
    .line 155
    iget-object v2, p0, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@a2
    aget-object v2, v2, v0

    #@a4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    .line 156
    iget-object v2, p0, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@a9
    array-length v2, v2

    #@aa
    add-int/lit8 v2, v2, -0x1

    #@ac
    if-ge v0, v2, :cond_b3

    #@ae
    .line 157
    const-string v2, " | "

    #@b0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    .line 154
    :cond_b3
    add-int/lit8 v0, v0, 0x1

    #@b5
    goto :goto_9b

    #@b6
    .line 160
    :cond_b6
    const-string v2, ", "

    #@b8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v2

    #@bc
    iget-object v3, p0, Lcom/android/internal/telephony/DataProfile;->protocol:Ljava/lang/String;

    #@be
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    .line 161
    const-string v2, ", "

    #@c3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v2

    #@c7
    iget-object v3, p0, Lcom/android/internal/telephony/DataProfile;->roamingProtocol:Ljava/lang/String;

    #@c9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    .line 162
    const-string v2, ", "

    #@ce
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v2

    #@d2
    iget-boolean v3, p0, Lcom/android/internal/telephony/ApnSetting;->carrierEnabled:Z

    #@d4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d7
    .line 163
    const-string v2, ", "

    #@d9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v2

    #@dd
    iget v3, p0, Lcom/android/internal/telephony/DataProfile;->bearer:I

    #@df
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e2
    .line 164
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v2

    #@e6
    return-object v2
.end method
