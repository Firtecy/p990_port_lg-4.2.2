.class public abstract Lcom/android/internal/telephony/PhoneBase;
.super Landroid/os/Handler;
.source "PhoneBase.java"

# interfaces
.implements Lcom/android/internal/telephony/Phone;


# static fields
.field public static final ALS_STATE_CHANGED_ACTION:Ljava/lang/String; = "android.intent.action.ALS_STATE_CHANGED"

.field public static final CLIR_KEY:Ljava/lang/String; = "clir_key"

.field public static final DATA_DISABLED_ON_BOOT_KEY:Ljava/lang/String; = "disabled_on_boot_key"

.field public static final DNS_SERVER_CHECK_DISABLED_KEY:Ljava/lang/String; = "dns_server_check_disabled_key"

.field protected static final EVENT_AKA_AUTHENTICATE_DONE:I = 0x26

.field protected static final EVENT_CALL_RING:I = 0xe

.field protected static final EVENT_CALL_RING_CONTINUE:I = 0xf

.field protected static final EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED:I = 0x1b

.field protected static final EVENT_DEACTIVATE_APPLICATION_DONE:I = 0x28

.field protected static final EVENT_EGSTK_SETUP_OTASP_CALL:I = 0x35

.field protected static final EVENT_EMERGENCY_CALLBACK_MODE_ENTER:I = 0x19

.field protected static final EVENT_EXIT_EMERGENCY_CALLBACK_RESPONSE:I = 0x1a

.field protected static final EVENT_GBA_AUTHENTICATE_BOOTSTRAP_DONE:I = 0x29

.field protected static final EVENT_GBA_AUTHENTICATE_NAF_DONE:I = 0x2a

.field protected static final EVENT_GET_BASEBAND_VERSION_DONE:I = 0x6

.field protected static final EVENT_GET_CALL_FORWARD_DONE:I = 0xd

.field protected static final EVENT_GET_DEVICE_IDENTITY_DONE:I = 0x15

.field protected static final EVENT_GET_IMEISV_DONE:I = 0xa

.field protected static final EVENT_GET_IMEI_DONE:I = 0x9

.field protected static final EVENT_GET_MDN_DONE:I = 0x21

.field protected static final EVENT_GET_SIM_STATUS_DONE:I = 0xb

.field protected static final EVENT_HDR_ROAMING_INDICATOR:I = 0x47

.field protected static final EVENT_ICC_CHANGED:I = 0x1e

.field protected static final EVENT_ICC_RECORD_EVENTS:I = 0x1d

.field protected static final EVENT_LTE_EHRPD_FORCED_CHANGED:I = 0x46

.field protected static final EVENT_MMI_DONE:I = 0x4

.field protected static final EVENT_NV_READY:I = 0x17

.field protected static final EVENT_RADIO_AVAILABLE:I = 0x1

.field protected static final EVENT_RADIO_OFF_OR_NOT_AVAILABLE:I = 0x8

.field protected static final EVENT_RADIO_ON:I = 0x5

.field protected static final EVENT_REGISTERED_TO_NETWORK:I = 0x13

.field protected static final EVENT_RETRY_GET_DEVICE_IDENTITY:I = 0x38

.field protected static final EVENT_RUIM_RECORDS_LOADED:I = 0x16

.field protected static final EVENT_SELECT_APPLICATION_DONE:I = 0x27

.field protected static final EVENT_SET_CALL_FORWARD_DONE:I = 0xc

.field protected static final EVENT_SET_CLIR_COMPLETE:I = 0x12

.field protected static final EVENT_SET_ENHANCED_VP:I = 0x18

.field protected static final EVENT_SET_ERI_VERSION:I = 0x2e

.field protected static final EVENT_SET_NETWORK_AUTOMATIC:I = 0x1c

.field protected static final EVENT_SET_NETWORK_AUTOMATIC_COMPLETE:I = 0x11

.field protected static final EVENT_SET_NETWORK_MANUAL_COMPLETE:I = 0x10

.field protected static final EVENT_SET_VM_NUMBER_DONE:I = 0x14

.field protected static final EVENT_SIM_RECORDS_LOADED:I = 0x3

.field protected static final EVENT_SS:I = 0x1f

.field protected static final EVENT_SSN:I = 0x2

.field protected static final EVENT_USSD:I = 0x7

.field public static final INTERNAL_PIN_VALUE:Ljava/lang/String; = "internal_pin_value_key"

.field private static final LOCAL_DEBUG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "PHONE"

.field public static final NETWORK_SELECTION_KEY:Ljava/lang/String; = "network_selection_key"

.field public static final NETWORK_SELECTION_NAME_KEY:Ljava/lang/String; = "network_selection_name_key"

.field public static final PROPERTY_MULTIMODE_CDMA:Ljava/lang/String; = "ro.config.multimode_cdma"

.field public static final VM_COUNT:Ljava/lang/String; = "vm_count_key"

.field public static final VM_ID:Ljava/lang/String; = "vm_id_key"

.field public static final VM_PRIORITY:Ljava/lang/String; = "vm_priority_key"

.field static ecmExitTime:J


# instance fields
.field protected final mAvpUpgradeFailureRegistrants:Landroid/os/RegistrantList;

.field public mCM:Lcom/android/internal/telephony/CommandsInterface;

.field protected final mCallModifyRegistrants:Landroid/os/RegistrantList;

.field mCallRingContinueToken:I

.field mCallRingDelay:I

.field protected final mContext:Landroid/content/Context;

.field public mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

.field protected final mDisconnectRegistrants:Landroid/os/RegistrantList;

.field mDnsCheckDisabled:Z

.field mDoesRilSendMultipleCallRing:Z

.field public mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/android/internal/telephony/uicc/IccRecords;",
            ">;"
        }
    .end annotation
.end field

.field protected final mIncomingRingRegistrants:Landroid/os/RegistrantList;

.field public mIsTheCurrentActivePhone:Z

.field private mIsUpdateRoamingCountry:Z

.field mIsVoiceCapable:Z

.field public mLGEDataConnectionTracker:Lcom/android/internal/telephony/LGEDataConnectionTracker;

.field protected mLooper:Landroid/os/Looper;

.field protected final mMmiCompleteRegistrants:Landroid/os/RegistrantList;

.field protected final mMmiRegistrants:Landroid/os/RegistrantList;

.field protected final mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

.field protected mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

.field protected mOosIsDisconnect:Z

.field protected final mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

.field protected mRD:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

.field protected final mReferredNetworkTypeRegistrants:Landroid/os/RegistrantList;

.field protected final mServiceStateRegistrants:Landroid/os/RegistrantList;

.field protected final mSimRecordsLoadedRegistrants:Landroid/os/RegistrantList;

.field protected mSimulatedRadioControl:Lcom/android/internal/telephony/test/SimulatedRadioControl;

.field public mSmsStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

.field public mSmsUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

.field protected final mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

.field protected mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/android/internal/telephony/uicc/UiccCardApplication;",
            ">;"
        }
    .end annotation
.end field

.field protected mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

.field mUnitTestMode:Z

.field protected final mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

.field private mVmCount:I

.field private mVmUrgent:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 241
    const-wide/16 v0, 0x0

    #@2
    sput-wide v0, Lcom/android/internal/telephony/PhoneBase;->ecmExitTime:J

    #@4
    return-void
.end method

.method protected constructor <init>(Lcom/android/internal/telephony/PhoneNotifier;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 5
    .parameter "notifier"
    .parameter "context"
    .parameter "ci"

    #@0
    .prologue
    .line 326
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/internal/telephony/PhoneBase;-><init>(Lcom/android/internal/telephony/PhoneNotifier;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Z)V

    #@4
    .line 327
    return-void
.end method

.method protected constructor <init>(Lcom/android/internal/telephony/PhoneNotifier;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Z)V
    .registers 23
    .parameter "notifier"
    .parameter "context"
    .parameter "ci"
    .parameter "unitTestMode"

    #@0
    .prologue
    .line 340
    invoke-direct/range {p0 .. p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 204
    const/4 v14, 0x0

    #@4
    move-object/from16 v0, p0

    #@6
    iput v14, v0, Lcom/android/internal/telephony/PhoneBase;->mVmCount:I

    #@8
    .line 213
    const/4 v14, 0x1

    #@9
    move-object/from16 v0, p0

    #@b
    iput-boolean v14, v0, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@d
    .line 214
    const/4 v14, 0x1

    #@e
    move-object/from16 v0, p0

    #@10
    iput-boolean v14, v0, Lcom/android/internal/telephony/PhoneBase;->mIsVoiceCapable:Z

    #@12
    .line 215
    const/4 v14, 0x0

    #@13
    move-object/from16 v0, p0

    #@15
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@17
    .line 216
    new-instance v14, Ljava/util/concurrent/atomic/AtomicReference;

    #@19
    invoke-direct {v14}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    #@1c
    move-object/from16 v0, p0

    #@1e
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@20
    .line 219
    new-instance v14, Ljava/util/concurrent/atomic/AtomicReference;

    #@22
    invoke-direct {v14}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    #@25
    move-object/from16 v0, p0

    #@27
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@29
    .line 222
    const/4 v14, 0x0

    #@2a
    move-object/from16 v0, p0

    #@2c
    iput-boolean v14, v0, Lcom/android/internal/telephony/PhoneBase;->mVmUrgent:Z

    #@2e
    .line 229
    const-string v14, "persist.telephony.oosisdc"

    #@30
    const/4 v15, 0x0

    #@31
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@34
    move-result v14

    #@35
    move-object/from16 v0, p0

    #@37
    iput-boolean v14, v0, Lcom/android/internal/telephony/PhoneBase;->mOosIsDisconnect:Z

    #@39
    .line 260
    new-instance v14, Landroid/os/RegistrantList;

    #@3b
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@3e
    move-object/from16 v0, p0

    #@40
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

    #@42
    .line 263
    new-instance v14, Landroid/os/RegistrantList;

    #@44
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@47
    move-object/from16 v0, p0

    #@49
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

    #@4b
    .line 266
    new-instance v14, Landroid/os/RegistrantList;

    #@4d
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@50
    move-object/from16 v0, p0

    #@52
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mIncomingRingRegistrants:Landroid/os/RegistrantList;

    #@54
    .line 269
    new-instance v14, Landroid/os/RegistrantList;

    #@56
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@59
    move-object/from16 v0, p0

    #@5b
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@5d
    .line 272
    new-instance v14, Landroid/os/RegistrantList;

    #@5f
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@62
    move-object/from16 v0, p0

    #@64
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mServiceStateRegistrants:Landroid/os/RegistrantList;

    #@66
    .line 275
    new-instance v14, Landroid/os/RegistrantList;

    #@68
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@6b
    move-object/from16 v0, p0

    #@6d
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@6f
    .line 278
    new-instance v14, Landroid/os/RegistrantList;

    #@71
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@74
    move-object/from16 v0, p0

    #@76
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@78
    .line 281
    new-instance v14, Landroid/os/RegistrantList;

    #@7a
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@7d
    move-object/from16 v0, p0

    #@7f
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@81
    .line 284
    new-instance v14, Landroid/os/RegistrantList;

    #@83
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@86
    move-object/from16 v0, p0

    #@88
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

    #@8a
    .line 287
    new-instance v14, Landroid/os/RegistrantList;

    #@8c
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@8f
    move-object/from16 v0, p0

    #@91
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mCallModifyRegistrants:Landroid/os/RegistrantList;

    #@93
    .line 290
    new-instance v14, Landroid/os/RegistrantList;

    #@95
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@98
    move-object/from16 v0, p0

    #@9a
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mAvpUpgradeFailureRegistrants:Landroid/os/RegistrantList;

    #@9c
    .line 295
    new-instance v14, Landroid/os/RegistrantList;

    #@9e
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@a1
    move-object/from16 v0, p0

    #@a3
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mReferredNetworkTypeRegistrants:Landroid/os/RegistrantList;

    #@a5
    .line 299
    new-instance v14, Landroid/os/RegistrantList;

    #@a7
    invoke-direct {v14}, Landroid/os/RegistrantList;-><init>()V

    #@aa
    move-object/from16 v0, p0

    #@ac
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mSimRecordsLoadedRegistrants:Landroid/os/RegistrantList;

    #@ae
    .line 341
    move-object/from16 v0, p1

    #@b0
    move-object/from16 v1, p0

    #@b2
    iput-object v0, v1, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@b4
    .line 342
    move-object/from16 v0, p2

    #@b6
    move-object/from16 v1, p0

    #@b8
    iput-object v0, v1, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@ba
    .line 343
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@bd
    move-result-object v14

    #@be
    move-object/from16 v0, p0

    #@c0
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mLooper:Landroid/os/Looper;

    #@c2
    .line 344
    move-object/from16 v0, p3

    #@c4
    move-object/from16 v1, p0

    #@c6
    iput-object v0, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@c8
    .line 346
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/PhoneBase;->setPropertiesByCarrier()V

    #@cb
    .line 348
    move-object/from16 v0, p0

    #@cd
    move/from16 v1, p4

    #@cf
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/PhoneBase;->setUnitTestMode(Z)V

    #@d2
    .line 350
    invoke-static/range {p2 .. p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@d5
    move-result-object v13

    #@d6
    .line 351
    .local v13, sp:Landroid/content/SharedPreferences;
    const-string v14, "dns_server_check_disabled_key"

    #@d8
    const/4 v15, 0x0

    #@d9
    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    #@dc
    move-result v14

    #@dd
    move-object/from16 v0, p0

    #@df
    iput-boolean v14, v0, Lcom/android/internal/telephony/PhoneBase;->mDnsCheckDisabled:Z

    #@e1
    .line 352
    move-object/from16 v0, p0

    #@e3
    iget-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e5
    const/16 v15, 0xe

    #@e7
    const/16 v16, 0x0

    #@e9
    move-object/from16 v0, p0

    #@eb
    move-object/from16 v1, v16

    #@ed
    invoke-interface {v14, v0, v15, v1}, Lcom/android/internal/telephony/CommandsInterface;->setOnCallRing(Landroid/os/Handler;ILjava/lang/Object;)V

    #@f0
    .line 360
    move-object/from16 v0, p0

    #@f2
    iget-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@f4
    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f7
    move-result-object v14

    #@f8
    const v15, 0x1110030

    #@fb
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@fe
    move-result v14

    #@ff
    move-object/from16 v0, p0

    #@101
    iput-boolean v14, v0, Lcom/android/internal/telephony/PhoneBase;->mIsVoiceCapable:Z

    #@103
    .line 373
    const-string v14, "ro.telephony.call_ring.multiple"

    #@105
    const/4 v15, 0x1

    #@106
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@109
    move-result v14

    #@10a
    move-object/from16 v0, p0

    #@10c
    iput-boolean v14, v0, Lcom/android/internal/telephony/PhoneBase;->mDoesRilSendMultipleCallRing:Z

    #@10e
    .line 375
    const-string v14, "PHONE"

    #@110
    new-instance v15, Ljava/lang/StringBuilder;

    #@112
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string v16, "mDoesRilSendMultipleCallRing="

    #@117
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v15

    #@11b
    move-object/from16 v0, p0

    #@11d
    iget-boolean v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDoesRilSendMultipleCallRing:Z

    #@11f
    move/from16 v16, v0

    #@121
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@124
    move-result-object v15

    #@125
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@128
    move-result-object v15

    #@129
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12c
    .line 377
    const-string v14, "ro.telephony.call_ring.delay"

    #@12e
    const/16 v15, 0xbb8

    #@130
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@133
    move-result v14

    #@134
    move-object/from16 v0, p0

    #@136
    iput v14, v0, Lcom/android/internal/telephony/PhoneBase;->mCallRingDelay:I

    #@138
    .line 379
    const-string v14, "PHONE"

    #@13a
    new-instance v15, Ljava/lang/StringBuilder;

    #@13c
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@13f
    const-string v16, "mCallRingDelay="

    #@141
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v15

    #@145
    move-object/from16 v0, p0

    #@147
    iget v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCallRingDelay:I

    #@149
    move/from16 v16, v0

    #@14b
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v15

    #@14f
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@152
    move-result-object v15

    #@153
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@156
    .line 382
    new-instance v14, Lcom/android/internal/telephony/SmsStorageMonitor;

    #@158
    move-object/from16 v0, p0

    #@15a
    invoke-direct {v14, v0}, Lcom/android/internal/telephony/SmsStorageMonitor;-><init>(Lcom/android/internal/telephony/PhoneBase;)V

    #@15d
    move-object/from16 v0, p0

    #@15f
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mSmsStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@161
    .line 383
    new-instance v14, Lcom/android/internal/telephony/SmsUsageMonitor;

    #@163
    move-object/from16 v0, p2

    #@165
    invoke-direct {v14, v0}, Lcom/android/internal/telephony/SmsUsageMonitor;-><init>(Landroid/content/Context;)V

    #@168
    move-object/from16 v0, p0

    #@16a
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mSmsUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@16c
    .line 384
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@16f
    move-result-object v14

    #@170
    move-object/from16 v0, p0

    #@172
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@174
    .line 385
    move-object/from16 v0, p0

    #@176
    iget-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@178
    const/16 v15, 0x1e

    #@17a
    const/16 v16, 0x0

    #@17c
    move-object/from16 v0, p0

    #@17e
    move-object/from16 v1, v16

    #@180
    invoke-virtual {v14, v0, v15, v1}, Lcom/android/internal/telephony/uicc/UiccController;->registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@183
    .line 386
    const-string v14, "PHONE"

    #@185
    new-instance v15, Ljava/lang/StringBuilder;

    #@187
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@18a
    const-string v16, "mOosIsDisconnect="

    #@18c
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v15

    #@190
    move-object/from16 v0, p0

    #@192
    iget-boolean v0, v0, Lcom/android/internal/telephony/PhoneBase;->mOosIsDisconnect:Z

    #@194
    move/from16 v16, v0

    #@196
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@199
    move-result-object v15

    #@19a
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19d
    move-result-object v15

    #@19e
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a1
    .line 388
    const-string v14, "KR"

    #@1a3
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@1a6
    move-result v14

    #@1a7
    if-eqz v14, :cond_1ac

    #@1a9
    .line 389
    invoke-static/range {p2 .. p2}, Lcom/android/internal/telephony/TelephonyUtils;->parseKrTelephonyStringXML(Landroid/content/Context;)V

    #@1ac
    .line 394
    :cond_1ac
    const-string v14, "VDF"

    #@1ae
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@1b1
    move-result v14

    #@1b2
    if-nez v14, :cond_1bc

    #@1b4
    const-string v14, "OPEN"

    #@1b6
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@1b9
    move-result v14

    #@1ba
    if-eqz v14, :cond_297

    #@1bc
    .line 395
    :cond_1bc
    const-string v6, ""

    #@1be
    .line 396
    .local v6, language:Ljava/lang/String;
    const-string v4, ""

    #@1c0
    .line 397
    .local v4, country:Ljava/lang/String;
    const-string v14, "persist.sys.sim-changed"

    #@1c2
    const-string v15, "F"

    #@1c4
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1c7
    move-result-object v11

    #@1c8
    .line 400
    .local v11, sim_changed:Ljava/lang/String;
    const-string v14, "PHONE"

    #@1ca
    new-instance v15, Ljava/lang/StringBuilder;

    #@1cc
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1cf
    const-string v16, "[LGE][SBP] SIM Changed status: "

    #@1d1
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v15

    #@1d5
    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v15

    #@1d9
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dc
    move-result-object v15

    #@1dd
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e0
    .line 402
    const-string v14, "1"

    #@1e2
    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e5
    move-result v14

    #@1e6
    if-eqz v14, :cond_385

    #@1e8
    .line 403
    const-string v14, "persist.sys.iccid-mcc"

    #@1ea
    const-string v15, "FFF"

    #@1ec
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1ef
    move-result-object v12

    #@1f0
    .line 405
    .local v12, sim_mcc:Ljava/lang/String;
    const-string v14, "PHONE"

    #@1f2
    new-instance v15, Ljava/lang/StringBuilder;

    #@1f4
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1f7
    const-string v16, "[LGE][SBP] SIM ICCID-MCC: "

    #@1f9
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v15

    #@1fd
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@200
    move-result-object v15

    #@201
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@204
    move-result-object v15

    #@205
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@208
    .line 407
    const-string v14, "FFF"

    #@20a
    invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20d
    move-result v14

    #@20e
    if-nez v14, :cond_297

    #@210
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@213
    move-result v14

    #@214
    const/4 v15, 0x3

    #@215
    if-ne v14, v15, :cond_297

    #@217
    .line 409
    const/4 v14, 0x0

    #@218
    const/4 v15, 0x3

    #@219
    invoke-virtual {v12, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@21c
    move-result-object v14

    #@21d
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@220
    move-result v14

    #@221
    invoke-static {v14}, Lcom/android/internal/telephony/MccTable;->defaultLanguageForMcc(I)Ljava/lang/String;

    #@224
    move-result-object v6

    #@225
    .line 410
    const/4 v14, 0x0

    #@226
    const/4 v15, 0x3

    #@227
    invoke-virtual {v12, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@22a
    move-result-object v14

    #@22b
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@22e
    move-result v14

    #@22f
    invoke-static {v14}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;

    #@232
    move-result-object v4

    #@233
    .line 411
    if-nez v6, :cond_24e

    #@235
    .line 412
    const-string v14, "PHONE"

    #@237
    new-instance v15, Ljava/lang/StringBuilder;

    #@239
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@23c
    const-string v16, "[LGE][SBP] No match language: "

    #@23e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@241
    move-result-object v15

    #@242
    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@245
    move-result-object v15

    #@246
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@249
    move-result-object v15

    #@24a
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24d
    .line 541
    .end local v4           #country:Ljava/lang/String;
    .end local v6           #language:Ljava/lang/String;
    .end local v11           #sim_changed:Ljava/lang/String;
    .end local v12           #sim_mcc:Ljava/lang/String;
    :cond_24d
    :goto_24d
    return-void

    #@24e
    .line 417
    .restart local v4       #country:Ljava/lang/String;
    .restart local v6       #language:Ljava/lang/String;
    .restart local v11       #sim_changed:Ljava/lang/String;
    .restart local v12       #sim_mcc:Ljava/lang/String;
    :cond_24e
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@251
    move-result-object v6

    #@252
    .line 419
    if-nez v4, :cond_256

    #@254
    .line 420
    const-string v4, ""

    #@256
    .line 423
    :cond_256
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@259
    move-result-object v4

    #@25a
    .line 426
    :try_start_25a
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@25d
    move-result-object v2

    #@25e
    .line 427
    .local v2, am:Landroid/app/IActivityManager;
    invoke-interface {v2}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    #@261
    move-result-object v3

    #@262
    .line 428
    .local v3, config:Landroid/content/res/Configuration;
    new-instance v14, Ljava/util/Locale;

    #@264
    invoke-direct {v14, v6, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@267
    iput-object v14, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@269
    .line 429
    const/4 v14, 0x1

    #@26a
    iput-boolean v14, v3, Landroid/content/res/Configuration;->userSetLocale:Z

    #@26c
    .line 430
    invoke-interface {v2, v3}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V

    #@26f
    .line 432
    const-string v14, "PHONE"

    #@271
    new-instance v15, Ljava/lang/StringBuilder;

    #@273
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@276
    const-string v16, "[LGE][SBP] PhoneBase locale set to "

    #@278
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27b
    move-result-object v15

    #@27c
    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27f
    move-result-object v15

    #@280
    const-string v16, "_"

    #@282
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@285
    move-result-object v15

    #@286
    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@289
    move-result-object v15

    #@28a
    const-string v16, " base SIM"

    #@28c
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28f
    move-result-object v15

    #@290
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@293
    move-result-object v15

    #@294
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_297
    .catch Ljava/lang/Exception; {:try_start_25a .. :try_end_297} :catch_37b

    #@297
    .line 492
    .end local v2           #am:Landroid/app/IActivityManager;
    .end local v3           #config:Landroid/content/res/Configuration;
    .end local v4           #country:Ljava/lang/String;
    .end local v6           #language:Ljava/lang/String;
    .end local v11           #sim_changed:Ljava/lang/String;
    .end local v12           #sim_mcc:Ljava/lang/String;
    :cond_297
    :goto_297
    const-string v14, "KR"

    #@299
    const-string v15, "LGU"

    #@29b
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@29e
    move-result v14

    #@29f
    if-eqz v14, :cond_30e

    #@2a1
    .line 497
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@2a4
    move-result-object v14

    #@2a5
    const-string v15, "mcc_change"

    #@2a7
    const/16 v16, 0x0

    #@2a9
    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@2ac
    move-result-object v8

    #@2ad
    .line 498
    .local v8, prefs:Landroid/content/SharedPreferences;
    const-string v14, "mcc"

    #@2af
    const-string v15, ""

    #@2b1
    invoke-interface {v8, v14, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2b4
    move-result-object v9

    #@2b5
    .line 499
    .local v9, savedMcc:Ljava/lang/String;
    const-string v14, "PHONE"

    #@2b7
    new-instance v15, Ljava/lang/StringBuilder;

    #@2b9
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@2bc
    const-string v16, "SharedPreference(mcc_change) saved mcc = "

    #@2be
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v15

    #@2c2
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c5
    move-result-object v15

    #@2c6
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c9
    move-result-object v15

    #@2ca
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2cd
    .line 500
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2d0
    move-result v14

    #@2d1
    if-nez v14, :cond_2db

    #@2d3
    const-string v14, "000"

    #@2d5
    invoke-virtual {v14, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d8
    move-result v14

    #@2d9
    if-eqz v14, :cond_30e

    #@2db
    .line 504
    :cond_2db
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@2de
    move-result-object v14

    #@2df
    const-string v15, "mcc"

    #@2e1
    const-string v16, "450"

    #@2e3
    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@2e6
    move-result-object v14

    #@2e7
    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@2ea
    .line 505
    const-string v14, "PHONE"

    #@2ec
    new-instance v15, Ljava/lang/StringBuilder;

    #@2ee
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@2f1
    const-string v16, "Init SharedPreference(mcc_change) new mcc = "

    #@2f3
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f6
    move-result-object v15

    #@2f7
    const-string v16, "mcc"

    #@2f9
    const-string v17, ""

    #@2fb
    move-object/from16 v0, v16

    #@2fd
    move-object/from16 v1, v17

    #@2ff
    invoke-interface {v8, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@302
    move-result-object v16

    #@303
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@306
    move-result-object v15

    #@307
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30a
    move-result-object v15

    #@30b
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30e
    .line 509
    .end local v8           #prefs:Landroid/content/SharedPreferences;
    .end local v9           #savedMcc:Ljava/lang/String;
    :cond_30e
    const-string v14, "KR"

    #@310
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@313
    move-result v14

    #@314
    if-eqz v14, :cond_34d

    #@316
    .line 511
    const-string v14, "persist.radio.camped_mccmnc"

    #@318
    const-string v15, ""

    #@31a
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@31d
    move-result-object v10

    #@31e
    .line 513
    .local v10, savedPropertyCamped_MccMnc:Ljava/lang/String;
    const-string v14, "PHONE"

    #@320
    new-instance v15, Ljava/lang/StringBuilder;

    #@322
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@325
    const-string v16, "savedPropertyCamped_MccMnc = "

    #@327
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32a
    move-result-object v15

    #@32b
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32e
    move-result-object v15

    #@32f
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@332
    move-result-object v15

    #@333
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@336
    .line 515
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@339
    move-result v14

    #@33a
    if-eqz v14, :cond_34d

    #@33c
    .line 516
    const-string v14, "KR"

    #@33e
    const-string v15, "SKT"

    #@340
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@343
    move-result v14

    #@344
    if-eqz v14, :cond_48b

    #@346
    .line 517
    const-string v14, "persist.radio.camped_mccmnc"

    #@348
    const-string v15, "45005"

    #@34a
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@34d
    .line 531
    .end local v10           #savedPropertyCamped_MccMnc:Ljava/lang/String;
    :cond_34d
    :goto_34d
    const/4 v14, 0x0

    #@34e
    const-string v15, "lgu_global_roaming"

    #@350
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@353
    move-result v14

    #@354
    if-eqz v14, :cond_366

    #@356
    .line 532
    move-object/from16 v0, p0

    #@358
    iget-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@35a
    move-object/from16 v0, p0

    #@35c
    iget-object v15, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@35e
    invoke-static {v14, v15}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->getUPlusRILEventDispatcher(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@361
    move-result-object v14

    #@362
    move-object/from16 v0, p0

    #@364
    iput-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mRD:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@366
    .line 537
    :cond_366
    const-string v14, "KR"

    #@368
    invoke-static {v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@36b
    move-result v14

    #@36c
    if-eqz v14, :cond_24d

    #@36e
    .line 538
    move-object/from16 v0, p0

    #@370
    iget-object v14, v0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@372
    move-object/from16 v0, p0

    #@374
    iget-object v15, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@376
    invoke-static {v14, v15}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->getLgeRoamingEventDispatcher(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/LgeRoamingEventDispatcher;

    #@379
    goto/16 :goto_24d

    #@37b
    .line 434
    .restart local v4       #country:Ljava/lang/String;
    .restart local v6       #language:Ljava/lang/String;
    .restart local v11       #sim_changed:Ljava/lang/String;
    .restart local v12       #sim_mcc:Ljava/lang/String;
    :catch_37b
    move-exception v5

    #@37c
    .line 435
    .local v5, e:Ljava/lang/Exception;
    const-string v14, "PHONE"

    #@37e
    const-string v15, "[LGE][SBP] Can\'t update system language base on SIM-ICCID MCC"

    #@380
    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@383
    goto/16 :goto_297

    #@385
    .line 440
    .end local v5           #e:Ljava/lang/Exception;
    .end local v12           #sim_mcc:Ljava/lang/String;
    :cond_385
    const-string v14, "F"

    #@387
    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38a
    move-result v14

    #@38b
    if-nez v14, :cond_395

    #@38d
    const-string v14, "2"

    #@38f
    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@392
    move-result v14

    #@393
    if-eqz v14, :cond_482

    #@395
    .line 442
    :cond_395
    const-string v14, "persist.radio.first-set"

    #@397
    const-string v15, "0"

    #@399
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@39c
    move-result-object v14

    #@39d
    const-string v15, "1"

    #@39f
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a2
    move-result v14

    #@3a3
    if-eqz v14, :cond_3b5

    #@3a5
    const-string v14, "persist.sys.mcc-list-changed"

    #@3a7
    const-string v15, "0"

    #@3a9
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3ac
    move-result-object v14

    #@3ad
    const-string v15, "1"

    #@3af
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b2
    move-result v14

    #@3b3
    if-eqz v14, :cond_297

    #@3b5
    .line 444
    :cond_3b5
    const-string v14, "persist.sys.mcc-list"

    #@3b7
    const-string v15, "F"

    #@3b9
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3bc
    move-result-object v7

    #@3bd
    .line 445
    .local v7, ntcode_mcc:Ljava/lang/String;
    const-string v14, "PHONE"

    #@3bf
    new-instance v15, Ljava/lang/StringBuilder;

    #@3c1
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@3c4
    const-string v16, "[LGE][SBP] NT-CODE MCC: "

    #@3c6
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c9
    move-result-object v15

    #@3ca
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3cd
    move-result-object v15

    #@3ce
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d1
    move-result-object v15

    #@3d2
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d5
    .line 447
    const-string v14, "FFF"

    #@3d7
    invoke-virtual {v14, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3da
    move-result v14

    #@3db
    if-nez v14, :cond_3e4

    #@3dd
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@3e0
    move-result v14

    #@3e1
    const/4 v15, 0x3

    #@3e2
    if-eq v14, v15, :cond_3ec

    #@3e4
    :cond_3e4
    const-string v14, "208"

    #@3e6
    invoke-virtual {v7, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@3e9
    move-result v14

    #@3ea
    if-eqz v14, :cond_297

    #@3ec
    .line 451
    :cond_3ec
    const/4 v14, 0x0

    #@3ed
    const/4 v15, 0x3

    #@3ee
    invoke-virtual {v7, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3f1
    move-result-object v7

    #@3f2
    .line 452
    const/4 v14, 0x0

    #@3f3
    const/4 v15, 0x3

    #@3f4
    invoke-virtual {v7, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3f7
    move-result-object v14

    #@3f8
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3fb
    move-result v14

    #@3fc
    invoke-static {v14}, Lcom/android/internal/telephony/MccTable;->defaultLanguageForMcc(I)Ljava/lang/String;

    #@3ff
    move-result-object v6

    #@400
    .line 453
    const/4 v14, 0x0

    #@401
    const/4 v15, 0x3

    #@402
    invoke-virtual {v7, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@405
    move-result-object v14

    #@406
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@409
    move-result v14

    #@40a
    invoke-static {v14}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;

    #@40d
    move-result-object v4

    #@40e
    .line 455
    if-nez v6, :cond_42a

    #@410
    .line 456
    const-string v14, "PHONE"

    #@412
    new-instance v15, Ljava/lang/StringBuilder;

    #@414
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@417
    const-string v16, "[LGE][SBP] No match language: "

    #@419
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41c
    move-result-object v15

    #@41d
    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@420
    move-result-object v15

    #@421
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@424
    move-result-object v15

    #@425
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@428
    goto/16 :goto_24d

    #@42a
    .line 461
    :cond_42a
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@42d
    move-result-object v6

    #@42e
    .line 463
    if-nez v4, :cond_432

    #@430
    .line 464
    const-string v4, ""

    #@432
    .line 468
    :cond_432
    :try_start_432
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@435
    move-result-object v2

    #@436
    .line 469
    .restart local v2       #am:Landroid/app/IActivityManager;
    invoke-interface {v2}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    #@439
    move-result-object v3

    #@43a
    .line 470
    .restart local v3       #config:Landroid/content/res/Configuration;
    new-instance v14, Ljava/util/Locale;

    #@43c
    invoke-direct {v14, v6, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@43f
    iput-object v14, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@441
    .line 471
    const/4 v14, 0x1

    #@442
    iput-boolean v14, v3, Landroid/content/res/Configuration;->userSetLocale:Z

    #@444
    .line 472
    invoke-interface {v2, v3}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V

    #@447
    .line 474
    const-string v14, "persist.radio.first-set"

    #@449
    const-string v15, "1"

    #@44b
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@44e
    .line 476
    const-string v14, "PHONE"

    #@450
    new-instance v15, Ljava/lang/StringBuilder;

    #@452
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@455
    const-string v16, "[LGE][SBP] Just 1-time update to "

    #@457
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45a
    move-result-object v15

    #@45b
    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45e
    move-result-object v15

    #@45f
    const-string v16, "_"

    #@461
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@464
    move-result-object v15

    #@465
    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@468
    move-result-object v15

    #@469
    const-string v16, " base on NT-Code"

    #@46b
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46e
    move-result-object v15

    #@46f
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@472
    move-result-object v15

    #@473
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_476
    .catch Ljava/lang/Exception; {:try_start_432 .. :try_end_476} :catch_478

    #@476
    goto/16 :goto_297

    #@478
    .line 478
    .end local v2           #am:Landroid/app/IActivityManager;
    .end local v3           #config:Landroid/content/res/Configuration;
    :catch_478
    move-exception v5

    #@479
    .line 479
    .restart local v5       #e:Ljava/lang/Exception;
    const-string v14, "PHONE"

    #@47b
    const-string v15, "[LGE][SBP] Can\'t update system lang base on NT-Code MCC"

    #@47d
    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@480
    goto/16 :goto_297

    #@482
    .line 485
    .end local v5           #e:Ljava/lang/Exception;
    .end local v7           #ntcode_mcc:Ljava/lang/String;
    :cond_482
    const-string v14, "PHONE"

    #@484
    const-string v15, "[LGE][SBP] The Inserted SIM is same!!"

    #@486
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@489
    goto/16 :goto_297

    #@48b
    .line 518
    .end local v4           #country:Ljava/lang/String;
    .end local v6           #language:Ljava/lang/String;
    .end local v11           #sim_changed:Ljava/lang/String;
    .restart local v10       #savedPropertyCamped_MccMnc:Ljava/lang/String;
    :cond_48b
    const-string v14, "KR"

    #@48d
    const-string v15, "KT"

    #@48f
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@492
    move-result v14

    #@493
    if-eqz v14, :cond_49e

    #@495
    .line 519
    const-string v14, "persist.radio.camped_mccmnc"

    #@497
    const-string v15, "45008"

    #@499
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@49c
    goto/16 :goto_34d

    #@49e
    .line 520
    :cond_49e
    const-string v14, "KR"

    #@4a0
    const-string v15, "LGU"

    #@4a2
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@4a5
    move-result v14

    #@4a6
    if-eqz v14, :cond_4b1

    #@4a8
    .line 521
    const-string v14, "persist.radio.camped_mccmnc"

    #@4aa
    const-string v15, "45006"

    #@4ac
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@4af
    goto/16 :goto_34d

    #@4b1
    .line 523
    :cond_4b1
    const-string v14, "persist.radio.camped_mccmnc"

    #@4b3
    const-string v15, "45006"

    #@4b5
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@4b8
    goto/16 :goto_34d
.end method

.method private checkCorrectThread(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 903
    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mLooper:Landroid/os/Looper;

    #@6
    if-eq v0, v1, :cond_10

    #@8
    .line 904
    new-instance v0, Ljava/lang/RuntimeException;

    #@a
    const-string v1, "com.android.internal.telephony.Phone must be used from within one thread"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 907
    :cond_10
    return-void
.end method

.method public static getEndTimeForEcm()J
    .registers 8

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 1776
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@5
    move-result-wide v0

    #@6
    .line 1777
    .local v0, currentTime:J
    sget-wide v6, Lcom/android/internal/telephony/PhoneBase;->ecmExitTime:J

    #@8
    sub-long v2, v6, v0

    #@a
    .line 1779
    .local v2, relatvieEcmExitTime:J
    cmp-long v6, v2, v4

    #@c
    if-lez v6, :cond_27

    #@e
    .line 1780
    const-string v4, "PHONE"

    #@10
    new-instance v5, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v6, "getEndTimeForEcm() : relative ecmExitTime = "

    #@17
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 1784
    .end local v2           #relatvieEcmExitTime:J
    :goto_26
    return-wide v2

    #@27
    .line 1783
    .restart local v2       #relatvieEcmExitTime:J
    :cond_27
    const-string v6, "PHONE"

    #@29
    const-string v7, "getEndTimeForEcm() : wrong case"

    #@2b
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    move-wide v2, v4

    #@2f
    .line 1784
    goto :goto_26
.end method

.method private getSavedNetworkSelection()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 799
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@7
    move-result-object v0

    #@8
    .line 800
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "network_selection_key"

    #@a
    const-string v2, ""

    #@c
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    return-object v1
.end method

.method private static logUnexpectedCdmaMethodCall(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 1511
    const-string v0, "PHONE"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Error! "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "() in PhoneBase should not be "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "called, CDMAPhone inactive."

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1513
    return-void
.end method

.method private static logUnexpectedGsmMethodCall(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 1523
    const-string v0, "PHONE"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Error! "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "() in PhoneBase should not be "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "called, GSMPhone inactive."

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1525
    return-void
.end method

.method private notifyIncomingRing()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1454
    iget-boolean v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIsVoiceCapable:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 1458
    :goto_5
    return-void

    #@6
    .line 1456
    :cond_6
    new-instance v0, Landroid/os/AsyncResult;

    #@8
    invoke-direct {v0, v2, p0, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@b
    .line 1457
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIncomingRingRegistrants:Landroid/os/RegistrantList;

    #@d
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@10
    goto :goto_5
.end method

.method public static resetEcmExitTime()V
    .registers 2

    #@0
    .prologue
    .line 1788
    const-string v0, "PHONE"

    #@2
    const-string v1, "resetEcmExitTime()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1789
    const-wide/16 v0, 0x0

    #@9
    sput-wide v0, Lcom/android/internal/telephony/PhoneBase;->ecmExitTime:J

    #@b
    .line 1790
    return-void
.end method

.method private sendIncomingCallRingNotification(I)V
    .registers 5
    .parameter "token"

    #@0
    .prologue
    .line 1464
    iget-boolean v0, p0, Lcom/android/internal/telephony/PhoneBase;->mIsVoiceCapable:Z

    #@2
    if-eqz v0, :cond_24

    #@4
    iget-boolean v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDoesRilSendMultipleCallRing:Z

    #@6
    if-nez v0, :cond_24

    #@8
    iget v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCallRingContinueToken:I

    #@a
    if-ne p1, v0, :cond_24

    #@c
    .line 1466
    const-string v0, "PHONE"

    #@e
    const-string v1, "Sending notifyIncomingRing"

    #@10
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 1467
    invoke-direct {p0}, Lcom/android/internal/telephony/PhoneBase;->notifyIncomingRing()V

    #@16
    .line 1468
    const/16 v0, 0xf

    #@18
    const/4 v1, 0x0

    #@19
    invoke-virtual {p0, v0, p1, v1}, Lcom/android/internal/telephony/PhoneBase;->obtainMessage(III)Landroid/os/Message;

    #@1c
    move-result-object v0

    #@1d
    iget v1, p0, Lcom/android/internal/telephony/PhoneBase;->mCallRingDelay:I

    #@1f
    int-to-long v1, v1

    #@20
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/PhoneBase;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@23
    .line 1477
    :goto_23
    return-void

    #@24
    .line 1471
    :cond_24
    const-string v0, "PHONE"

    #@26
    new-instance v1, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v2, "Ignoring ring notification request, mDoesRilSendMultipleCallRing="

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    iget-boolean v2, p0, Lcom/android/internal/telephony/PhoneBase;->mDoesRilSendMultipleCallRing:Z

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    const-string v2, " token="

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    const-string v2, " mCallRingContinueToken="

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    iget v2, p0, Lcom/android/internal/telephony/PhoneBase;->mCallRingContinueToken:I

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    const-string v2, " mIsVoiceCapable="

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    iget-boolean v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIsVoiceCapable:Z

    #@55
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@58
    move-result-object v1

    #@59
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v1

    #@5d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    goto :goto_23
.end method

.method public static setCurrentTimeForEcm(J)V
    .registers 6
    .parameter "delayMillis"

    #@0
    .prologue
    .line 1769
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v0

    #@4
    add-long/2addr v0, p0

    #@5
    sput-wide v0, Lcom/android/internal/telephony/PhoneBase;->ecmExitTime:J

    #@7
    .line 1770
    const-string v0, "PHONE"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "setCurrentTimeForEcm() : ecmExitTime = "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    sget-wide v2, Lcom/android/internal/telephony/PhoneBase;->ecmExitTime:J

    #@16
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 1771
    return-void
.end method

.method private setPropertiesByCarrier()V
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x5

    #@1
    const/4 v11, 0x0

    #@2
    .line 914
    const-string v9, "ro.carrier"

    #@4
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 916
    .local v1, carrier:Ljava/lang/String;
    if-eqz v1, :cond_18

    #@a
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@d
    move-result v9

    #@e
    if-eqz v9, :cond_18

    #@10
    const-string v9, "unknown"

    #@12
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v9

    #@16
    if-eqz v9, :cond_19

    #@18
    .line 949
    :cond_18
    :goto_18
    return-void

    #@19
    .line 920
    :cond_19
    iget-object v9, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1e
    move-result-object v9

    #@1f
    const v10, 0x1070040

    #@22
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    #@25
    move-result-object v2

    #@26
    .line 923
    .local v2, carrierLocales:[Ljava/lang/CharSequence;
    const/4 v5, 0x0

    #@27
    .local v5, i:I
    :goto_27
    array-length v9, v2

    #@28
    if-ge v5, v9, :cond_18

    #@2a
    .line 924
    aget-object v9, v2, v5

    #@2c
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    .line 925
    .local v0, c:Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v9

    #@34
    if-eqz v9, :cond_76

    #@36
    .line 926
    add-int/lit8 v9, v5, 0x1

    #@38
    aget-object v9, v2, v9

    #@3a
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3d
    move-result-object v6

    #@3e
    .line 928
    .local v6, l:Ljava/lang/String;
    const/4 v9, 0x2

    #@3f
    invoke-virtual {v6, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@42
    move-result-object v7

    #@43
    .line 929
    .local v7, language:Ljava/lang/String;
    const-string v3, ""

    #@45
    .line 930
    .local v3, country:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@48
    move-result v9

    #@49
    if-lt v9, v12, :cond_50

    #@4b
    .line 931
    const/4 v9, 0x3

    #@4c
    invoke-virtual {v6, v9, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    .line 933
    :cond_50
    iget-object v9, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@52
    invoke-static {v9, v7, v3}, Lcom/android/internal/telephony/MccTable;->setSystemLocale(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@55
    .line 935
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@58
    move-result v9

    #@59
    if-nez v9, :cond_18

    #@5b
    .line 937
    :try_start_5b
    iget-object v9, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@5d
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@60
    move-result-object v9

    #@61
    const-string v10, "wifi_country_code"

    #@63
    invoke-static {v9, v10}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_66
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_5b .. :try_end_66} :catch_67

    #@66
    goto :goto_18

    #@67
    .line 939
    :catch_67
    move-exception v4

    #@68
    .line 941
    .local v4, e:Landroid/provider/Settings$SettingNotFoundException;
    iget-object v9, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@6a
    const-string v10, "wifi"

    #@6c
    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6f
    move-result-object v8

    #@70
    check-cast v8, Landroid/net/wifi/WifiManager;

    #@72
    .line 943
    .local v8, wM:Landroid/net/wifi/WifiManager;
    invoke-virtual {v8, v3, v11}, Landroid/net/wifi/WifiManager;->setCountryCode(Ljava/lang/String;Z)V

    #@75
    goto :goto_18

    #@76
    .line 923
    .end local v3           #country:Ljava/lang/String;
    .end local v4           #e:Landroid/provider/Settings$SettingNotFoundException;
    .end local v6           #l:Ljava/lang/String;
    .end local v7           #language:Ljava/lang/String;
    .end local v8           #wM:Landroid/net/wifi/WifiManager;
    :cond_76
    add-int/lit8 v5, v5, 0x3

    #@78
    goto :goto_27
.end method


# virtual methods
.method public PlayVZWERISound()V
    .registers 2

    #@0
    .prologue
    .line 1801
    const-string v0, "PlayVZWERISound"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1802
    return-void
.end method

.method public StopVZWERISound()V
    .registers 2

    #@0
    .prologue
    .line 1806
    const-string v0, "StopVZWERISound"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1807
    return-void
.end method

.method public acceptCall(I)V
    .registers 5
    .parameter "callType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1649
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Accept with CallType is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public acceptConnectionTypeChange(Lcom/android/internal/telephony/Connection;Ljava/util/Map;)V
    .registers 6
    .parameter "conn"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/Connection;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1704
    .local p2, newExtras:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "acceptConnectionTypeChange is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public changeConnectionType(Landroid/os/Message;Lcom/android/internal/telephony/Connection;ILjava/util/Map;)V
    .registers 8
    .parameter "msg"
    .parameter "conn"
    .parameter "newCallType"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Message;",
            "Lcom/android/internal/telephony/Connection;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1699
    .local p4, newExtras:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "changeConnectionType is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public checkDataProfileEx(II)Z
    .registers 4
    .parameter "type"
    .parameter "Q_IPv"

    #@0
    .prologue
    .line 1997
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public closeImsPdn(I)V
    .registers 3
    .parameter "reason"

    #@0
    .prologue
    .line 1584
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->closeImsPdn(I)V

    #@5
    .line 1585
    return-void
.end method

.method public dial(Ljava/lang/String;I[Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    .registers 7
    .parameter "dialString"
    .parameter "CallType"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1662
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Dial with CallDetails is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public dial(Ljava/lang/String;Z)Lcom/android/internal/telephony/Connection;
    .registers 6
    .parameter "dialString"
    .parameter "subaddress"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1669
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Dial with CallDetails is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public disableApnType(Ljava/lang/String;)I
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 1426
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->disableApnType(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public disableDnsCheck(Z)V
    .registers 5
    .parameter "b"

    #@0
    .prologue
    .line 635
    iput-boolean p1, p0, Lcom/android/internal/telephony/PhoneBase;->mDnsCheckDisabled:Z

    #@2
    .line 636
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v2

    #@6
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@9
    move-result-object v1

    #@a
    .line 637
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@d
    move-result-object v0

    #@e
    .line 638
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "dns_server_check_disabled_key"

    #@10
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    #@13
    .line 639
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@16
    .line 640
    return-void
.end method

.method public dispose()V
    .registers 5

    #@0
    .prologue
    .line 544
    sget-object v1, Lcom/android/internal/telephony/PhoneProxy;->lockForRadioTechnologyChange:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 545
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnCallRing(Landroid/os/Handler;)V

    #@8
    .line 547
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@a
    const/4 v2, 0x0

    #@b
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@e
    .line 548
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@11
    .line 550
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mSmsStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@13
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsStorageMonitor;->dispose()V

    #@16
    .line 551
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mSmsUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@18
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsUsageMonitor;->dispose()V

    #@1b
    .line 552
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@1d
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccController;->unregisterForIccChanged(Landroid/os/Handler;)V

    #@20
    .line 554
    const/4 v0, 0x0

    #@21
    const-string v2, "lgu_global_roaming"

    #@23
    invoke-static {v0, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_4c

    #@29
    .line 555
    const-string v0, "PHONE"

    #@2b
    new-instance v2, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v3, "mRD = "

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mRD:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 556
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mRD:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@45
    if-eqz v0, :cond_4c

    #@47
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mRD:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@49
    invoke-virtual {v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->dispose()V

    #@4c
    .line 560
    :cond_4c
    monitor-exit v1

    #@4d
    .line 561
    return-void

    #@4e
    .line 560
    :catchall_4e
    move-exception v0

    #@4f
    monitor-exit v1
    :try_end_50
    .catchall {:try_start_3 .. :try_end_50} :catchall_4e

    #@50
    throw v0
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 1610
    const-string v0, "PhoneBase:"

    #@2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 1611
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v1, " mCM="

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d
    .line 1612
    new-instance v0, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v1, " mDnsCheckDisabled="

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    iget-boolean v1, p0, Lcom/android/internal/telephony/PhoneBase;->mDnsCheckDisabled:Z

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@35
    .line 1613
    new-instance v0, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v1, " mDataConnectionTracker="

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4d
    .line 1614
    new-instance v0, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v1, " mDoesRilSendMultipleCallRing="

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v0

    #@58
    iget-boolean v1, p0, Lcom/android/internal/telephony/PhoneBase;->mDoesRilSendMultipleCallRing:Z

    #@5a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v0

    #@5e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v0

    #@62
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@65
    .line 1615
    new-instance v0, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v1, " mCallRingContinueToken="

    #@6c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v0

    #@70
    iget v1, p0, Lcom/android/internal/telephony/PhoneBase;->mCallRingContinueToken:I

    #@72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v0

    #@76
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v0

    #@7a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7d
    .line 1616
    new-instance v0, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v1, " mCallRingDelay="

    #@84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v0

    #@88
    iget v1, p0, Lcom/android/internal/telephony/PhoneBase;->mCallRingDelay:I

    #@8a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v0

    #@8e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v0

    #@92
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@95
    .line 1617
    new-instance v0, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v1, " mIsTheCurrentActivePhone="

    #@9c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v0

    #@a0
    iget-boolean v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@a2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v0

    #@a6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v0

    #@aa
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ad
    .line 1618
    new-instance v0, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v1, " mIsVoiceCapable="

    #@b4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v0

    #@b8
    iget-boolean v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIsVoiceCapable:Z

    #@ba
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v0

    #@be
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v0

    #@c2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c5
    .line 1619
    new-instance v0, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v1, " mIccRecords="

    #@cc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v0

    #@d0
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@d2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@d5
    move-result-object v1

    #@d6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v0

    #@da
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v0

    #@de
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e1
    .line 1620
    new-instance v0, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v1, " mUiccApplication="

    #@e8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v0

    #@ec
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@ee
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@f1
    move-result-object v1

    #@f2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v0

    #@f6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v0

    #@fa
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@fd
    .line 1621
    new-instance v0, Ljava/lang/StringBuilder;

    #@ff
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@102
    const-string v1, " mSmsStorageMonitor="

    #@104
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v0

    #@108
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mSmsStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@10a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v0

    #@10e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@111
    move-result-object v0

    #@112
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@115
    .line 1622
    new-instance v0, Ljava/lang/StringBuilder;

    #@117
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@11a
    const-string v1, " mSmsUsageMonitor="

    #@11c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v0

    #@120
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mSmsUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@122
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v0

    #@126
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@129
    move-result-object v0

    #@12a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@12d
    .line 1623
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@130
    .line 1624
    new-instance v0, Ljava/lang/StringBuilder;

    #@132
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@135
    const-string v1, " mLooper="

    #@137
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v0

    #@13b
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mLooper:Landroid/os/Looper;

    #@13d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v0

    #@141
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@144
    move-result-object v0

    #@145
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@148
    .line 1625
    new-instance v0, Ljava/lang/StringBuilder;

    #@14a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@14d
    const-string v1, " mContext="

    #@14f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v0

    #@153
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@155
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v0

    #@159
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15c
    move-result-object v0

    #@15d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@160
    .line 1626
    new-instance v0, Ljava/lang/StringBuilder;

    #@162
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@165
    const-string v1, " mNotifier="

    #@167
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v0

    #@16b
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@16d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v0

    #@171
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@174
    move-result-object v0

    #@175
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@178
    .line 1627
    new-instance v0, Ljava/lang/StringBuilder;

    #@17a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@17d
    const-string v1, " mSimulatedRadioControl="

    #@17f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v0

    #@183
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mSimulatedRadioControl:Lcom/android/internal/telephony/test/SimulatedRadioControl;

    #@185
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v0

    #@189
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18c
    move-result-object v0

    #@18d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@190
    .line 1628
    new-instance v0, Ljava/lang/StringBuilder;

    #@192
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@195
    const-string v1, " mUnitTestMode="

    #@197
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v0

    #@19b
    iget-boolean v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUnitTestMode:Z

    #@19d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v0

    #@1a1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a4
    move-result-object v0

    #@1a5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a8
    .line 1629
    new-instance v0, Ljava/lang/StringBuilder;

    #@1aa
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1ad
    const-string v1, " isDnsCheckDisabled()="

    #@1af
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v0

    #@1b3
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->isDnsCheckDisabled()Z

    #@1b6
    move-result v1

    #@1b7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1ba
    move-result-object v0

    #@1bb
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1be
    move-result-object v0

    #@1bf
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1c2
    .line 1630
    new-instance v0, Ljava/lang/StringBuilder;

    #@1c4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1c7
    const-string v1, " getUnitTestMode()="

    #@1c9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v0

    #@1cd
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getUnitTestMode()Z

    #@1d0
    move-result v1

    #@1d1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v0

    #@1d5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d8
    move-result-object v0

    #@1d9
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1dc
    .line 1631
    new-instance v0, Ljava/lang/StringBuilder;

    #@1de
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1e1
    const-string v1, " getState()="

    #@1e3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v0

    #@1e7
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@1ea
    move-result-object v1

    #@1eb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v0

    #@1ef
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f2
    move-result-object v0

    #@1f3
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1f6
    .line 1632
    new-instance v0, Ljava/lang/StringBuilder;

    #@1f8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1fb
    const-string v1, " getIccSerialNumber()="

    #@1fd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@200
    move-result-object v0

    #@201
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getIccSerialNumber()Ljava/lang/String;

    #@204
    move-result-object v1

    #@205
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@208
    move-result-object v0

    #@209
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20c
    move-result-object v0

    #@20d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@210
    .line 1633
    new-instance v0, Ljava/lang/StringBuilder;

    #@212
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@215
    const-string v1, " getIccRecordsLoaded()="

    #@217
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v0

    #@21b
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getIccRecordsLoaded()Z

    #@21e
    move-result v1

    #@21f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@222
    move-result-object v0

    #@223
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@226
    move-result-object v0

    #@227
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@22a
    .line 1634
    new-instance v0, Ljava/lang/StringBuilder;

    #@22c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@22f
    const-string v1, " getMessageWaitingIndicator()="

    #@231
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@234
    move-result-object v0

    #@235
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getMessageWaitingIndicator()Z

    #@238
    move-result v1

    #@239
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@23c
    move-result-object v0

    #@23d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@240
    move-result-object v0

    #@241
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@244
    .line 1635
    new-instance v0, Ljava/lang/StringBuilder;

    #@246
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@249
    const-string v1, " getCallForwardingIndicator()="

    #@24b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24e
    move-result-object v0

    #@24f
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getCallForwardingIndicator()Z

    #@252
    move-result v1

    #@253
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@256
    move-result-object v0

    #@257
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25a
    move-result-object v0

    #@25b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@25e
    .line 1636
    new-instance v0, Ljava/lang/StringBuilder;

    #@260
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@263
    const-string v1, " isInEmergencyCall()="

    #@265
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@268
    move-result-object v0

    #@269
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->isInEmergencyCall()Z

    #@26c
    move-result v1

    #@26d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@270
    move-result-object v0

    #@271
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@274
    move-result-object v0

    #@275
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@278
    .line 1637
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@27b
    .line 1638
    new-instance v0, Ljava/lang/StringBuilder;

    #@27d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@280
    const-string v1, " isInEcm()="

    #@282
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@285
    move-result-object v0

    #@286
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->isInEcm()Z

    #@289
    move-result v1

    #@28a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@28d
    move-result-object v0

    #@28e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@291
    move-result-object v0

    #@292
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@295
    .line 1639
    new-instance v0, Ljava/lang/StringBuilder;

    #@297
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@29a
    const-string v1, " getPhoneName()="

    #@29c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29f
    move-result-object v0

    #@2a0
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getPhoneName()Ljava/lang/String;

    #@2a3
    move-result-object v1

    #@2a4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a7
    move-result-object v0

    #@2a8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ab
    move-result-object v0

    #@2ac
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2af
    .line 1640
    new-instance v0, Ljava/lang/StringBuilder;

    #@2b1
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2b4
    const-string v1, " getPhoneType()="

    #@2b6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b9
    move-result-object v0

    #@2ba
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@2bd
    move-result v1

    #@2be
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v0

    #@2c2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c5
    move-result-object v0

    #@2c6
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2c9
    .line 1641
    new-instance v0, Ljava/lang/StringBuilder;

    #@2cb
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2ce
    const-string v1, " getVoiceMessageCount()="

    #@2d0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d3
    move-result-object v0

    #@2d4
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getVoiceMessageCount()I

    #@2d7
    move-result v1

    #@2d8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2db
    move-result-object v0

    #@2dc
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2df
    move-result-object v0

    #@2e0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2e3
    .line 1642
    new-instance v0, Ljava/lang/StringBuilder;

    #@2e5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2e8
    const-string v1, " getActiveApnTypes()="

    #@2ea
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ed
    move-result-object v0

    #@2ee
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getActiveApnTypes()[Ljava/lang/String;

    #@2f1
    move-result-object v1

    #@2f2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f5
    move-result-object v0

    #@2f6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f9
    move-result-object v0

    #@2fa
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2fd
    .line 1643
    new-instance v0, Ljava/lang/StringBuilder;

    #@2ff
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@302
    const-string v1, " isDataConnectivityPossible()="

    #@304
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@307
    move-result-object v0

    #@308
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->isDataConnectivityPossible()Z

    #@30b
    move-result v1

    #@30c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@30f
    move-result-object v0

    #@310
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@313
    move-result-object v0

    #@314
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@317
    .line 1644
    new-instance v0, Ljava/lang/StringBuilder;

    #@319
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@31c
    const-string v1, " needsOtaServiceProvisioning="

    #@31e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@321
    move-result-object v0

    #@322
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->needsOtaServiceProvisioning()Z

    #@325
    move-result v1

    #@326
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@329
    move-result-object v0

    #@32a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32d
    move-result-object v0

    #@32e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@331
    .line 1645
    return-void
.end method

.method public enableApnType(Ljava/lang/String;)I
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 1422
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->enableApnType(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public enableEnhancedVoicePrivacy(ZLandroid/os/Message;)V
    .registers 4
    .parameter "enable"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1111
    const-string v0, "enableEnhancedVoicePrivacy"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1112
    return-void
.end method

.method public exitEmergencyCallbackMode()V
    .registers 2

    #@0
    .prologue
    .line 1280
    const-string v0, "exitEmergencyCallbackMode"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1281
    return-void
.end method

.method public getAPNList()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2010
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getActiveApnHost(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 1410
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->getActiveApnString(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getActiveApnTypes()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1406
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->getActiveApnTypes()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAlertId()I
    .registers 2

    #@0
    .prologue
    .line 1811
    const-string v0, "getAlertId"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1812
    const/4 v0, 0x0

    #@6
    return v0
.end method

.method public getAllCellInfo()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1020
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/ServiceStateTracker;->getAllCellInfo()Ljava/util/List;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getCLIRSettingValue()I
    .registers 3

    #@0
    .prologue
    .line 1887
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getCLIRSettingValue"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1888
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public getCallDomain(Lcom/android/internal/telephony/Call;)I
    .registers 5
    .parameter "call"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1657
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getCallDomain is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public getCallForwardingIndicator()Z
    .registers 3

    #@0
    .prologue
    .line 1031
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 1032
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_f

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getVoiceCallForwardingFlag()Z

    #@d
    move-result v1

    #@e
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public getCallTracker()Lcom/android/internal/telephony/CallTracker;
    .registers 2

    #@0
    .prologue
    .line 983
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getCallType(Lcom/android/internal/telephony/Call;)I
    .registers 5
    .parameter "call"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1653
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getCallType is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public getCdmaEriHomeSystems()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1823
    const-string v0, "getCdmaEriHomeSystems"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1824
    const/4 v0, 0x0

    #@6
    return-object v0
.end method

.method public getCdmaEriIconIndex()I
    .registers 2

    #@0
    .prologue
    .line 1233
    const-string v0, "getCdmaEriIconIndex"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1234
    const/4 v0, -0x1

    #@6
    return v0
.end method

.method public getCdmaEriIconMode()I
    .registers 2

    #@0
    .prologue
    .line 1243
    const-string v0, "getCdmaEriIconMode"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1244
    const/4 v0, -0x1

    #@6
    return v0
.end method

.method public getCdmaEriText()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1251
    const-string v0, "getCdmaEriText"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1252
    const-string v0, "GSM nw, no ERI"

    #@7
    return-object v0
.end method

.method public getCdmaInfo()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1598
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->getCdmaInfo()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCdmaLteEhrpdForced()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1987
    const-string v0, "PHONE"

    #@2
    const-string v1, "Error! getCdmaLteEhrpdForced() in PhoneBase should not be called, GSMPhone & CDMAPhone inactive."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1989
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getCdmaMin()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1257
    const-string v0, "getCdmaMin"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1258
    const/4 v0, 0x0

    #@6
    return-object v0
.end method

.method public getCdmaPrlVersion()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1269
    const-string v0, "getCdmaPrlVersion"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1270
    const/4 v0, 0x0

    #@6
    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 623
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getCurrentLine()I
    .registers 3

    #@0
    .prologue
    .line 1904
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getCurrentLine : 2"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1905
    const/4 v0, 0x2

    #@8
    return v0
.end method

.method public getCurrentUiccAppType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;
    .registers 3

    #@0
    .prologue
    .line 987
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@8
    .line 988
    .local v0, currentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eqz v0, :cond_f

    #@a
    .line 989
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@d
    move-result-object v1

    #@e
    .line 991
    :goto_e
    return-object v1

    #@f
    :cond_f
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_UNKNOWN:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@11
    goto :goto_e
.end method

.method public getCurrentVoiceClass()I
    .registers 3

    #@0
    .prologue
    .line 1924
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getCurrentVoiceClass"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1925
    const/4 v0, 0x1

    #@8
    return v0
.end method

.method public getDataConnectionState()Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 2

    #@0
    .prologue
    .line 1516
    const-string v0, "default"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/PhoneBase;->getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDebugInfo(II)[I
    .registers 4
    .parameter "type"
    .parameter "num"

    #@0
    .prologue
    .line 2003
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->getDebugInfo(II)[I

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getEmodeInfoPage(I)Ljava/lang/String;
    .registers 3
    .parameter "EngIndex"

    #@0
    .prologue
    .line 1754
    const-string v0, "Dummy"

    #@2
    return-object v0
.end method

.method public getEngineeringModeInfo(ILandroid/os/Message;)I
    .registers 4
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 1751
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getEnhancedVoicePrivacy(Landroid/os/Message;)V
    .registers 3
    .parameter "onComplete"

    #@0
    .prologue
    .line 1116
    const-string v0, "getEnhancedVoicePrivacy"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1117
    return-void
.end method

.method public getEriFileVersion()I
    .registers 2

    #@0
    .prologue
    .line 1817
    const-string v0, "getEriFileVersion"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1818
    const/4 v0, 0x0

    #@6
    return v0
.end method

.method public getHDRRoamingIndicator()I
    .registers 3

    #@0
    .prologue
    .line 1977
    const-string v0, "PHONE"

    #@2
    const-string v1, "Error! getHDRRoamingIndicator() in PhoneBase should not be called, GSMPhone & CDMAPhone inactive."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1979
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public getHandler()Landroid/os/Handler;
    .registers 1

    #@0
    .prologue
    .line 969
    return-object p0
.end method

.method public getIPPhoneState()Z
    .registers 2

    #@0
    .prologue
    .line 2063
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->getIPPhoneState()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getIccCard()Lcom/android/internal/telephony/IccCard;
    .registers 2

    #@0
    .prologue
    .line 996
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;
    .registers 3

    #@0
    .prologue
    .line 960
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@8
    .line 961
    .local v0, uiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-nez v0, :cond_c

    #@a
    const/4 v1, 0x0

    #@b
    .line 962
    :goto_b
    return-object v1

    #@c
    :cond_c
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@f
    move-result-object v1

    #@10
    goto :goto_b
.end method

.method public getIccRecordsLoaded()Z
    .registers 3

    #@0
    .prologue
    .line 1011
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 1012
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_f

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getRecordsLoaded()Z

    #@d
    move-result v1

    #@e
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public getIccSerialNumber()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1002
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 1005
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_d

    #@a
    iget-object v1, v0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@c
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method public getIsUpdateRoamingCountry()Z
    .registers 2

    #@0
    .prologue
    .line 1559
    iget-boolean v0, p0, Lcom/android/internal/telephony/PhoneBase;->mIsUpdateRoamingCountry:Z

    #@2
    return v0
.end method

.method public getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;
    .registers 3

    #@0
    .prologue
    .line 1493
    const-string v0, "PHONE"

    #@2
    const-string v1, "getIsimRecords() is only supported on LTE devices"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1494
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getLTEDataRoamingEnable()Z
    .registers 2

    #@0
    .prologue
    .line 2051
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mLGEDataConnectionTracker:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->getLTEDataRoamingEnable()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getLinkCapabilities(Ljava/lang/String;)Landroid/net/LinkCapabilities;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 1418
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->getLinkCapabilities(Ljava/lang/String;)Landroid/net/LinkCapabilities;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLinkProperties(Ljava/lang/String;)Landroid/net/LinkProperties;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 1414
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->getLinkProperties(Ljava/lang/String;)Landroid/net/LinkProperties;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLteInfo()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1594
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->getLteInfo()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLteOnCdmaMode()I
    .registers 2

    #@0
    .prologue
    .line 1542
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLteOnCdmaMode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMessageWaitingIndicator()Z
    .registers 2

    #@0
    .prologue
    .line 1026
    iget v0, p0, Lcom/android/internal/telephony/PhoneBase;->mVmCount:I

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public getMipErrorCode(Landroid/os/Message;)V
    .registers 4
    .parameter "result"

    #@0
    .prologue
    .line 2015
    const-string v0, "PHONE"

    #@2
    const-string v1, "Error! This function should never be executed, inactive CDMAPhone."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2016
    return-void
.end method

.method public getModemIntegerItem(ILandroid/os/Message;)V
    .registers 6
    .parameter "item_index"
    .parameter "response"

    #@0
    .prologue
    .line 1855
    const-string v0, "PHONE"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "PhoneBase getModemIntegerItem item_index = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1856
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1a
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->getModemIntegerItem(ILandroid/os/Message;)V

    #@1d
    .line 1857
    return-void
.end method

.method public getModemStringItem(ILandroid/os/Message;)V
    .registers 6
    .parameter "item_index"
    .parameter "response"

    #@0
    .prologue
    .line 1867
    const-string v0, "PHONE"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "PhoneBase getModemStringItem item_index = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1868
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1a
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->getModemStringItem(ILandroid/os/Message;)V

    #@1d
    .line 1869
    return-void
.end method

.method public getMsisdn()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1502
    const-string v0, "getMsisdn"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedGsmMethodCall(Ljava/lang/String;)V

    #@5
    .line 1503
    const/4 v0, 0x0

    #@6
    return-object v0
.end method

.method public getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;
    .registers 3
    .parameter "ipv"

    #@0
    .prologue
    .line 1580
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPcscfAddress(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .registers 4
    .parameter "ipv"
    .parameter "apnType"

    #@0
    .prologue
    .line 1575
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/telephony/DataConnectionTracker;->getPcscfAddress(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public abstract getPhoneName()Ljava/lang/String;
.end method

.method public abstract getPhoneType()I
.end method

.method public getPreferredNetworkType(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1090
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->getPreferredNetworkType(Landroid/os/Message;)V

    #@5
    .line 1091
    return-void
.end method

.method public getProposedConnectionType(Lcom/android/internal/telephony/Connection;)I
    .registers 5
    .parameter "conn"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1714
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getProposedConnectionType is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public getServiceLine(Landroid/os/Message;)V
    .registers 4
    .parameter "onComplete"

    #@0
    .prologue
    .line 1892
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getServiceLine"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1893
    return-void
.end method

.method public getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;
    .registers 2

    #@0
    .prologue
    .line 976
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getSignalStrength()Landroid/telephony/SignalStrength;
    .registers 3

    #@0
    .prologue
    .line 1047
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@3
    move-result-object v0

    #@4
    .line 1048
    .local v0, sst:Lcom/android/internal/telephony/ServiceStateTracker;
    if-nez v0, :cond_c

    #@6
    .line 1049
    new-instance v1, Landroid/telephony/SignalStrength;

    #@8
    invoke-direct {v1}, Landroid/telephony/SignalStrength;-><init>()V

    #@b
    .line 1051
    :goto_b
    return-object v1

    #@c
    :cond_c
    invoke-virtual {v0}, Lcom/android/internal/telephony/ServiceStateTracker;->getSignalStrength()Landroid/telephony/SignalStrength;

    #@f
    move-result-object v1

    #@10
    goto :goto_b
.end method

.method public getSimulatedRadioControl()Lcom/android/internal/telephony/test/SimulatedRadioControl;
    .registers 2

    #@0
    .prologue
    .line 891
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mSimulatedRadioControl:Lcom/android/internal/telephony/test/SimulatedRadioControl;

    #@2
    return-object v0
.end method

.method public getSmscAddress(Landroid/os/Message;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 1094
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->getSmscAddress(Landroid/os/Message;)V

    #@5
    .line 1095
    return-void
.end method

.method public abstract getState()Lcom/android/internal/telephony/PhoneConstants$State;
.end method

.method public getStatusId()I
    .registers 2

    #@0
    .prologue
    .line 1963
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getSubscription()I
    .registers 2

    #@0
    .prologue
    .line 1958
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getUnitTestMode()Z
    .registers 2

    #@0
    .prologue
    .line 827
    iget-boolean v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUnitTestMode:Z

    #@2
    return v0
.end method

.method public getUsimServiceTable()Lcom/android/internal/telephony/uicc/UsimServiceTable;
    .registers 3

    #@0
    .prologue
    .line 1569
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 1570
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_f

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getUsimServiceTable()Lcom/android/internal/telephony/uicc/UsimServiceTable;

    #@d
    move-result-object v1

    #@e
    :goto_e
    return-object v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public getVoiceCallForwardingFlag()Z
    .registers 3

    #@0
    .prologue
    .line 1909
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getVoiceCallForwardingFlag"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1910
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public getVoiceCallForwardingFlagLine2()Z
    .registers 3

    #@0
    .prologue
    .line 1914
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getVoiceCallForwardingFlagLine2"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1915
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public getVoiceMailAlphaTagForALS(I)Ljava/lang/String;
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 1939
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getVoiceMailAlphaTagForALS"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1940
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getVoiceMailNumberForALS(I)Ljava/lang/String;
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 1934
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getVoiceMailNumberForALS"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1935
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getVoiceMessageCount()I
    .registers 2

    #@0
    .prologue
    .line 1205
    iget v0, p0, Lcom/android/internal/telephony/PhoneBase;->mVmCount:I

    #@2
    return v0
.end method

.method public getVoiceMessageUrgent()Z
    .registers 2

    #@0
    .prologue
    .line 1225
    iget-boolean v0, p0, Lcom/android/internal/telephony/PhoneBase;->mVmUrgent:Z

    #@2
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 583
    iget-boolean v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@2
    if-nez v2, :cond_2f

    #@4
    .line 584
    const-string v2, "PHONE"

    #@6
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "Received message "

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    const-string v4, "["

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    iget v4, p1, Landroid/os/Message;->what:I

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    const-string v4, "] while being destroyed. Ignoring."

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 619
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 588
    :cond_2f
    iget v2, p1, Landroid/os/Message;->what:I

    #@31
    sparse-switch v2, :sswitch_data_ae

    #@34
    .line 617
    new-instance v2, Ljava/lang/RuntimeException;

    #@36
    const-string v3, "unexpected event not handled"

    #@38
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3b
    throw v2

    #@3c
    .line 590
    :sswitch_3c
    const-string v2, "PHONE"

    #@3e
    new-instance v3, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v4, "Event EVENT_CALL_RING Received state="

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 591
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5a
    check-cast v0, Landroid/os/AsyncResult;

    #@5c
    .line 592
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5e
    if-nez v2, :cond_2e

    #@60
    .line 593
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@63
    move-result-object v1

    #@64
    .line 594
    .local v1, state:Lcom/android/internal/telephony/PhoneConstants$State;
    iget-boolean v2, p0, Lcom/android/internal/telephony/PhoneBase;->mDoesRilSendMultipleCallRing:Z

    #@66
    if-nez v2, :cond_7c

    #@68
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->RINGING:Lcom/android/internal/telephony/PhoneConstants$State;

    #@6a
    if-eq v1, v2, :cond_70

    #@6c
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@6e
    if-ne v1, v2, :cond_7c

    #@70
    .line 597
    :cond_70
    iget v2, p0, Lcom/android/internal/telephony/PhoneBase;->mCallRingContinueToken:I

    #@72
    add-int/lit8 v2, v2, 0x1

    #@74
    iput v2, p0, Lcom/android/internal/telephony/PhoneBase;->mCallRingContinueToken:I

    #@76
    .line 598
    iget v2, p0, Lcom/android/internal/telephony/PhoneBase;->mCallRingContinueToken:I

    #@78
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/PhoneBase;->sendIncomingCallRingNotification(I)V

    #@7b
    goto :goto_2e

    #@7c
    .line 600
    :cond_7c
    invoke-direct {p0}, Lcom/android/internal/telephony/PhoneBase;->notifyIncomingRing()V

    #@7f
    goto :goto_2e

    #@80
    .line 606
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v1           #state:Lcom/android/internal/telephony/PhoneConstants$State;
    :sswitch_80
    const-string v2, "PHONE"

    #@82
    new-instance v3, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v4, "Event EVENT_CALL_RING_CONTINUE Received stat="

    #@89
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v3

    #@8d
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v3

    #@95
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v3

    #@99
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 607
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@9f
    move-result-object v2

    #@a0
    sget-object v3, Lcom/android/internal/telephony/PhoneConstants$State;->RINGING:Lcom/android/internal/telephony/PhoneConstants$State;

    #@a2
    if-ne v2, v3, :cond_2e

    #@a4
    .line 608
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@a6
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/PhoneBase;->sendIncomingCallRingNotification(I)V

    #@a9
    goto :goto_2e

    #@aa
    .line 613
    :sswitch_aa
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->onUpdateIccAvailability()V

    #@ad
    goto :goto_2e

    #@ae
    .line 588
    :sswitch_data_ae
    .sparse-switch
        0xe -> :sswitch_3c
        0xf -> :sswitch_80
        0x1e -> :sswitch_aa
    .end sparse-switch
.end method

.method public invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .registers 4
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1128
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    #@5
    .line 1129
    return-void
.end method

.method public invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "strings"
    .parameter "response"

    #@0
    .prologue
    .line 1132
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    #@5
    .line 1133
    return-void
.end method

.method public isCspPlmnEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1488
    const-string v0, "isCspPlmnEnabled"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedGsmMethodCall(Ljava/lang/String;)V

    #@5
    .line 1489
    const/4 v0, 0x0

    #@6
    return v0
.end method

.method public isDataConnectivityPossible()Z
    .registers 2

    #@0
    .prologue
    .line 1430
    const-string v0, "default"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/PhoneBase;->isDataConnectivityPossible(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isDataConnectivityPossible(Ljava/lang/String;)Z
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 1434
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->isDataPossible(Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isDnsCheckDisabled()Z
    .registers 2

    #@0
    .prologue
    .line 646
    iget-boolean v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDnsCheckDisabled:Z

    #@2
    return v0
.end method

.method public isEmergencyAttachSupportedOnLte()Z
    .registers 2

    #@0
    .prologue
    .line 2028
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->isEmergencyAttachSupportedOnLte()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isEmergencyCallSupportedOnLte()Z
    .registers 2

    #@0
    .prologue
    .line 2024
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->isEmergencyCallSupportedOnLte()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isInEcm()Z
    .registers 2

    #@0
    .prologue
    .line 1195
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isInEmergencyCall()Z
    .registers 2

    #@0
    .prologue
    .line 1186
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isInternetPDNconnected()Z
    .registers 2

    #@0
    .prologue
    .line 2038
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->isInternetPDNconnected()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isLTEDataRoamingAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 2043
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mLGEDataConnectionTracker:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->isLTEDataRoamingAvailable()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isManualNetSelAllowed()Z
    .registers 2

    #@0
    .prologue
    .line 1482
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isMinInfoReady()Z
    .registers 2

    #@0
    .prologue
    .line 1263
    const-string v0, "isMinInfoReady"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1264
    const/4 v0, 0x0

    #@6
    return v0
.end method

.method public isOtaAttachedOnLte()Z
    .registers 2

    #@0
    .prologue
    .line 2033
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->isOtaAttachedOnLte()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isOtaSpNumber(Ljava/lang/String;)Z
    .registers 3
    .parameter "dialStr"

    #@0
    .prologue
    .line 1316
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isTwoLineSupported()Z
    .registers 3

    #@0
    .prologue
    .line 1929
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! isTwoLineSupported"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1930
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public isVoiceCallSupprotedOnLte()Z
    .registers 2

    #@0
    .prologue
    .line 2020
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->isVoiceCallSupprotedOnLte()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public needsOtaServiceProvisioning()Z
    .registers 2

    #@0
    .prologue
    .line 1308
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public notifyCallForwardingIndicator()V
    .registers 3

    #@0
    .prologue
    .line 1530
    const-string v0, "PHONE"

    #@2
    const-string v1, "Error! This function should never be executed, inactive CDMAPhone."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1531
    return-void
.end method

.method public notifyCellInfo(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1179
    .local p1, cellInfo:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0, p1}, Lcom/android/internal/telephony/PhoneNotifier;->notifyCellInfo(Lcom/android/internal/telephony/Phone;Ljava/util/List;)V

    #@5
    .line 1180
    return-void
.end method

.method public notifyDataActivity()V
    .registers 2

    #@0
    .prologue
    .line 1136
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/PhoneNotifier;->notifyDataActivity(Lcom/android/internal/telephony/Phone;)V

    #@5
    .line 1137
    return-void
.end method

.method public notifyDataConnection(Ljava/lang/String;)V
    .registers 9
    .parameter "reason"

    #@0
    .prologue
    .line 1164
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getActiveApnTypes()[Ljava/lang/String;

    #@3
    move-result-object v4

    #@4
    .line 1165
    .local v4, types:[Ljava/lang/String;
    move-object v1, v4

    #@5
    .local v1, arr$:[Ljava/lang/String;
    array-length v3, v1

    #@6
    .local v3, len$:I
    const/4 v2, 0x0

    #@7
    .local v2, i$:I
    :goto_7
    if-ge v2, v3, :cond_17

    #@9
    aget-object v0, v1, v2

    #@b
    .line 1166
    .local v0, apnType:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@d
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/PhoneBase;->getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@10
    move-result-object v6

    #@11
    invoke-interface {v5, p0, p1, v0, v6}, Lcom/android/internal/telephony/PhoneNotifier;->notifyDataConnection(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;)V

    #@14
    .line 1165
    add-int/lit8 v2, v2, 0x1

    #@16
    goto :goto_7

    #@17
    .line 1168
    .end local v0           #apnType:Ljava/lang/String;
    :cond_17
    return-void
.end method

.method public notifyDataConnection(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "reason"
    .parameter "apnType"

    #@0
    .prologue
    .line 1154
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/PhoneBase;->getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@5
    move-result-object v1

    #@6
    invoke-interface {v0, p0, p1, p2, v1}, Lcom/android/internal/telephony/PhoneNotifier;->notifyDataConnection(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;)V

    #@9
    .line 1155
    return-void
.end method

.method public notifyDataConnection(Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/DataConnection$FailCause;)V
    .registers 10
    .parameter "reason"
    .parameter "apnType"
    .parameter "cause"

    #@0
    .prologue
    .line 1159
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/PhoneBase;->getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@5
    move-result-object v4

    #@6
    move-object v1, p0

    #@7
    move-object v2, p1

    #@8
    move-object v3, p2

    #@9
    move-object v5, p3

    #@a
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/PhoneNotifier;->notifyDataConnection(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@d
    .line 1160
    return-void
.end method

.method public notifyDataConnection(Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;)V
    .registers 5
    .parameter "reason"
    .parameter "apnType"
    .parameter "state"

    #@0
    .prologue
    .line 1150
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/PhoneNotifier;->notifyDataConnection(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;)V

    #@5
    .line 1151
    return-void
.end method

.method public notifyDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "reason"
    .parameter "apnType"

    #@0
    .prologue
    .line 1534
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0, p1, p2}, Lcom/android/internal/telephony/PhoneNotifier;->notifyDataConnectionFailed(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 1535
    return-void
.end method

.method protected notifyDisconnectP(Lcom/android/internal/telephony/Connection;)V
    .registers 4
    .parameter "cn"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 837
    new-instance v0, Landroid/os/AsyncResult;

    #@3
    invoke-direct {v0, v1, p1, v1}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@6
    .line 838
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@8
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@b
    .line 839
    return-void
.end method

.method public notifyMessageWaitingIndicator()V
    .registers 2

    #@0
    .prologue
    .line 1141
    iget-boolean v0, p0, Lcom/android/internal/telephony/PhoneBase;->mIsVoiceCapable:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1146
    :goto_4
    return-void

    #@5
    .line 1145
    :cond_5
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@7
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/PhoneNotifier;->notifyMessageWaitingChanged(Lcom/android/internal/telephony/Phone;)V

    #@a
    goto :goto_4
.end method

.method protected notifyNewRingingConnectionP(Lcom/android/internal/telephony/Connection;)V
    .registers 5
    .parameter "cn"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1444
    iget-boolean v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIsVoiceCapable:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 1448
    :goto_5
    return-void

    #@6
    .line 1446
    :cond_6
    new-instance v0, Landroid/os/AsyncResult;

    #@8
    invoke-direct {v0, v2, p1, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@b
    .line 1447
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

    #@d
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@10
    goto :goto_5
.end method

.method public notifyOtaspChanged(I)V
    .registers 3
    .parameter "otaspMode"

    #@0
    .prologue
    .line 1171
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0, p1}, Lcom/android/internal/telephony/PhoneNotifier;->notifyOtaspChanged(Lcom/android/internal/telephony/Phone;I)V

    #@5
    .line 1172
    return-void
.end method

.method protected notifyPreciseCallStateChangedP()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 666
    new-instance v0, Landroid/os/AsyncResult;

    #@3
    invoke-direct {v0, v1, p0, v1}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@6
    .line 667
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

    #@8
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@b
    .line 668
    return-void
.end method

.method protected notifyServiceStateChangedP(Landroid/telephony/ServiceState;)V
    .registers 4
    .parameter "ss"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 883
    new-instance v0, Landroid/os/AsyncResult;

    #@3
    invoke-direct {v0, v1, p1, v1}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@6
    .line 884
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mServiceStateRegistrants:Landroid/os/RegistrantList;

    #@8
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@b
    .line 886
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@d
    invoke-interface {v1, p0}, Lcom/android/internal/telephony/PhoneNotifier;->notifyServiceState(Lcom/android/internal/telephony/Phone;)V

    #@10
    .line 887
    return-void
.end method

.method public notifySignalStrength()V
    .registers 2

    #@0
    .prologue
    .line 1175
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/PhoneNotifier;->notifySignalStrength(Lcom/android/internal/telephony/Phone;)V

    #@5
    .line 1176
    return-void
.end method

.method protected abstract onUpdateIccAvailability()V
.end method

.method public prepareEri()V
    .registers 2

    #@0
    .prologue
    .line 1796
    const-string v0, "prepareEri"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1797
    return-void
.end method

.method public processECCNotiRep(Lcom/android/internal/telephony/MmiCode;)Z
    .registers 3
    .parameter "mmi"

    #@0
    .prologue
    .line 1763
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public processECCNotiUSSD(Ljava/lang/String;)V
    .registers 2
    .parameter "dialString"

    #@0
    .prologue
    .line 1760
    return-void
.end method

.method public queryAvailableBandMode(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1124
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->queryAvailableBandMode(Landroid/os/Message;)V

    #@5
    .line 1125
    return-void
.end method

.method public queryCdmaRoamingPreference(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1039
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->queryCdmaRoamingPreference(Landroid/os/Message;)V

    #@5
    .line 1040
    return-void
.end method

.method public queryTTYMode(Landroid/os/Message;)V
    .registers 3
    .parameter "onComplete"

    #@0
    .prologue
    .line 1106
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->queryTTYMode(Landroid/os/Message;)V

    #@5
    .line 1107
    return-void
.end method

.method public registerFoT53ClirlInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1380
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerFoT53ClirlInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1381
    return-void
.end method

.method public registerForAvpUpgradeFailure(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 7
    .parameter "h"
    .parameter "what"
    .parameter "obj"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1687
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "registerForAvpUpgradeFailure is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public registerForCallWaiting(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1321
    const-string v0, "registerForCallWaiting"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1322
    return-void
.end method

.method public registerForCdmaOtaStatusChange(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1285
    const-string v0, "registerForCdmaOtaStatusChange"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1286
    return-void
.end method

.method public registerForDisconnect(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 730
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->checkCorrectThread(Landroid/os/Handler;)V

    #@3
    .line 732
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 733
    return-void
.end method

.method public registerForDisplayInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1348
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerForDisplayInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1349
    return-void
.end method

.method public registerForEcmTimerReset(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1331
    const-string v0, "registerForEcmTimerReset"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1332
    return-void
.end method

.method public registerForInCallVoicePrivacyOff(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 707
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerForInCallVoicePrivacyOff(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 708
    return-void
.end method

.method public registerForInCallVoicePrivacyOn(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 697
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerForInCallVoicePrivacyOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 698
    return-void
.end method

.method public registerForIncomingRing(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 718
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->checkCorrectThread(Landroid/os/Handler;)V

    #@3
    .line 720
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mIncomingRingRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 721
    return-void
.end method

.method public registerForLineControlInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1372
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerForLineControlInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1373
    return-void
.end method

.method public registerForMmiComplete(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 766
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->checkCorrectThread(Landroid/os/Handler;)V

    #@3
    .line 768
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 769
    return-void
.end method

.method public registerForMmiInitiate(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 754
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->checkCorrectThread(Landroid/os/Handler;)V

    #@3
    .line 756
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 757
    return-void
.end method

.method public registerForModifyCallRequest(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 7
    .parameter "h"
    .parameter "what"
    .parameter "obj"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1676
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "registerForModifyCallRequest is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public registerForNewRingingConnection(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 685
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->checkCorrectThread(Landroid/os/Handler;)V

    #@3
    .line 687
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 688
    return-void
.end method

.method public registerForNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1356
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerForNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1357
    return-void
.end method

.method public registerForPreciseCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 651
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->checkCorrectThread(Landroid/os/Handler;)V

    #@3
    .line 653
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 654
    return-void
.end method

.method public registerForRedirectedNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1364
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerForRedirectedNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1365
    return-void
.end method

.method public registerForResendIncallMute(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 866
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerForResendIncallMute(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 867
    return-void
.end method

.method public registerForRingbackTone(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 856
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerForRingbackTone(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 857
    return-void
.end method

.method public registerForServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 844
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->checkCorrectThread(Landroid/os/Handler;)V

    #@3
    .line 846
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mServiceStateRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->add(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 847
    return-void
.end method

.method public registerForSetPreferredNetworkType(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 780
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mReferredNetworkTypeRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 781
    return-void
.end method

.method public registerForSignalInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1340
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerForSignalInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1341
    return-void
.end method

.method public registerForSimRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 4
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 789
    return-void
.end method

.method public registerForSubscriptionInfoReady(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1295
    const-string v0, "registerForSubscriptionInfoReady"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1296
    return-void
.end method

.method public registerForSuppServiceFailed(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 742
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->checkCorrectThread(Landroid/os/Handler;)V

    #@3
    .line 744
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 745
    return-void
.end method

.method public registerForT53AudioControlInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1388
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerForT53AudioControlInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1389
    return-void
.end method

.method public registerForUnknownConnection(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 672
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->checkCorrectThread(Landroid/os/Handler;)V

    #@3
    .line 674
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 675
    return-void
.end method

.method public registerLGECipheringInd(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1741
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerLGECipheringInd(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1742
    return-void
.end method

.method public registerLGEUnsol(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1836
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerLGEUnsol(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1837
    return-void
.end method

.method public rejectConnectionTypeChange(Lcom/android/internal/telephony/Connection;)V
    .registers 5
    .parameter "conn"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1709
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "rejectConnectionTypeChange is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public removeReferences()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 564
    iput-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mSmsStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@3
    .line 565
    iput-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mSmsUsageMonitor:Lcom/android/internal/telephony/SmsUsageMonitor;

    #@5
    .line 566
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@7
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@a
    .line 567
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@c
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@f
    .line 568
    iput-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@11
    .line 569
    iput-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@13
    .line 570
    return-void
.end method

.method public requestIsimAuthentication(Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "nonce"
    .parameter "result"

    #@0
    .prologue
    .line 1498
    const-string v0, "PHONE"

    #@2
    const-string v1, "requestIsimAuthentication() is only supported on LTE devices"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1499
    return-void
.end method

.method public resetVoiceMessageCount()V
    .registers 2

    #@0
    .prologue
    .line 1970
    const-string v0, "resetVoiceMessageCount"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1971
    return-void
.end method

.method public restoreSavedNetworkSelection(Landroid/os/Message;)V
    .registers 4
    .parameter "response"

    #@0
    .prologue
    .line 810
    invoke-direct {p0}, Lcom/android/internal/telephony/PhoneBase;->getSavedNetworkSelection()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 813
    .local v0, networkSelection:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_10

    #@a
    .line 814
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@c
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/CommandsInterface;->setNetworkSelectionModeAutomatic(Landroid/os/Message;)V

    #@f
    .line 818
    :goto_f
    return-void

    #@10
    .line 816
    :cond_10
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@12
    invoke-interface {v1, v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->setNetworkSelectionModeManual(Ljava/lang/String;Landroid/os/Message;)V

    #@15
    goto :goto_f
.end method

.method public sendBurstDtmf(Ljava/lang/String;IILandroid/os/Message;)V
    .registers 6
    .parameter "dtmfString"
    .parameter "on"
    .parameter "off"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1275
    const-string v0, "sendBurstDtmf"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1276
    return-void
.end method

.method public sendDefaultAttachProfile(I)V
    .registers 4
    .parameter "profileId"

    #@0
    .prologue
    .line 1605
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-interface {v0, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->sendDefaultAttachProfile(ILandroid/os/Message;)V

    #@6
    .line 1606
    return-void
.end method

.method public setBandMode(ILandroid/os/Message;)V
    .registers 4
    .parameter "bandMode"
    .parameter "response"

    #@0
    .prologue
    .line 1120
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setBandMode(ILandroid/os/Message;)V

    #@5
    .line 1121
    return-void
.end method

.method public setCSGSelectionManual(ILandroid/os/Message;)V
    .registers 5
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1085
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setCSGSelectionManual(ILandroid/os/Message;)V

    #@5
    .line 1086
    const-string v0, "PHONE"

    #@7
    const-string v1, "setCSGSelectionManualt "

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 1087
    return-void
.end method

.method public setCdmaEriVersion(ILandroid/os/Message;)V
    .registers 4
    .parameter "value"
    .parameter "result"

    #@0
    .prologue
    .line 1829
    const-string v0, "setCdmaEriVersion"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1830
    return-void
.end method

.method public setCdmaFactoryReset(Landroid/os/Message;)V
    .registers 4
    .parameter "result"

    #@0
    .prologue
    .line 1876
    const-string v0, "PHONE"

    #@2
    const-string v1, "Error! This function should never be executed, inactive CDMAPhone."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1877
    return-void
.end method

.method public setCdmaRoamingPreference(ILandroid/os/Message;)V
    .registers 4
    .parameter "cdmaRoamingType"
    .parameter "response"

    #@0
    .prologue
    .line 1059
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setCdmaRoamingPreference(ILandroid/os/Message;)V

    #@5
    .line 1060
    return-void
.end method

.method public setCdmaSubscription(ILandroid/os/Message;)V
    .registers 4
    .parameter "cdmaSubscriptionType"
    .parameter "response"

    #@0
    .prologue
    .line 1066
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setCdmaSubscriptionSource(ILandroid/os/Message;)V

    #@5
    .line 1067
    return-void
.end method

.method public setDataConnection(Z)V
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 1950
    const-string v0, "PHONE"

    #@2
    const-string v1, "Error! This function should never be executed, inactive CDMAPhone."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1951
    return-void
.end method

.method public setEchoSuppressionEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 876
    return-void
.end method

.method public setIMSRegistate(Z)V
    .registers 3
    .parameter "Registate"

    #@0
    .prologue
    .line 1589
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->setIMSRegistate(Z)V

    #@5
    .line 1590
    return-void
.end method

.method public setIPPhoneState(Z)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 2069
    invoke-static {p1}, Lcom/android/internal/telephony/TelephonyUtils;->setIPPhoneState(Z)V

    #@3
    .line 2070
    return-void
.end method

.method public setImsStatusForDan(ILandroid/os/Message;)V
    .registers 4
    .parameter "ims_status"
    .parameter "result"

    #@0
    .prologue
    .line 2075
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setImsStatusForDan(ILandroid/os/Message;)V

    #@5
    .line 2076
    return-void
.end method

.method public setIsUpdateRoamingCountry(Z)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 1555
    iput-boolean p1, p0, Lcom/android/internal/telephony/PhoneBase;->mIsUpdateRoamingCountry:Z

    #@2
    .line 1556
    return-void
.end method

.method public setLTEDataRoamingEnable(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 2047
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mLGEDataConnectionTracker:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->setLTEDataRoamingEnable(Z)V

    #@5
    .line 2048
    return-void
.end method

.method public setModemIntegerItem(IILandroid/os/Message;)V
    .registers 7
    .parameter "item_index"
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1849
    const-string v0, "PHONE"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "PhoneBase setModemIntegerItem data = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1850
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1a
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->setModemIntegerItem(IILandroid/os/Message;)V

    #@1d
    .line 1851
    return-void
.end method

.method public setModemStringItem(ILjava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "item_index"
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1861
    const-string v0, "PHONE"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "PhoneBase setModemStringItem data = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1862
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1a
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->setModemStringItem(ILjava/lang/String;Landroid/os/Message;)V

    #@1d
    .line 1863
    return-void
.end method

.method public setOnEcbModeExitResponse(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1397
    const-string v0, "setOnEcbModeExitResponse"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1398
    return-void
.end method

.method public setPreferredNetworkType(ILandroid/os/Message;)V
    .registers 5
    .parameter "networkType"
    .parameter "response"

    #@0
    .prologue
    .line 1073
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@5
    .line 1076
    const/4 v0, 0x0

    #@6
    const-string v1, "vzw_gfit"

    #@8
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 1077
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mReferredNetworkTypeRegistrants:Landroid/os/RegistrantList;

    #@10
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@13
    .line 1080
    :cond_13
    return-void
.end method

.method public setRmnetAutoconnect(ILandroid/os/Message;)V
    .registers 4
    .parameter "param"
    .parameter "result"

    #@0
    .prologue
    .line 2057
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setRmnetAutoconnect(ILandroid/os/Message;)V

    #@5
    .line 2058
    return-void
.end method

.method public setServiceLine(ILandroid/os/Message;)V
    .registers 5
    .parameter "line"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1896
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! setServiceLine"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1897
    return-void
.end method

.method public setSmscAddress(Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "address"
    .parameter "result"

    #@0
    .prologue
    .line 1098
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setSmscAddress(Ljava/lang/String;Landroid/os/Message;)V

    #@5
    .line 1099
    return-void
.end method

.method public setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "property"
    .parameter "value"

    #@0
    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->getUnitTestMode()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_7

    #@6
    .line 257
    :goto_6
    return-void

    #@7
    .line 256
    :cond_7
    invoke-static {p1, p2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    goto :goto_6
.end method

.method public setTTYMode(ILandroid/os/Message;)V
    .registers 4
    .parameter "ttyMode"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1102
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setTTYMode(ILandroid/os/Message;)V

    #@5
    .line 1103
    return-void
.end method

.method public setUnitTestMode(Z)V
    .registers 2
    .parameter "f"

    #@0
    .prologue
    .line 822
    iput-boolean p1, p0, Lcom/android/internal/telephony/PhoneBase;->mUnitTestMode:Z

    #@2
    .line 823
    return-void
.end method

.method public setVoiceMailNumberForALS(ILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "line"
    .parameter "alphaTag"
    .parameter "voiceMailNumber"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1945
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! setVoiceMailNumberForALS"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1946
    return-void
.end method

.method public setVoiceMessageCount(I)V
    .registers 2
    .parameter "countWaiting"

    #@0
    .prologue
    .line 1210
    iput p1, p0, Lcom/android/internal/telephony/PhoneBase;->mVmCount:I

    #@2
    .line 1212
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneBase;->notifyMessageWaitingIndicator()V

    #@5
    .line 1213
    return-void
.end method

.method public setVoiceMessageUrgent(Z)V
    .registers 2
    .parameter "urgent"

    #@0
    .prologue
    .line 1219
    iput-boolean p1, p0, Lcom/android/internal/telephony/PhoneBase;->mVmUrgent:Z

    #@2
    .line 1220
    return-void
.end method

.method public storeALSSettingValue(I)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 1900
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! storeALSSettingValue"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1901
    return-void
.end method

.method public storeCLIRSettingValue(I)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 1883
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! storeCLIRSettingValue"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1884
    return-void
.end method

.method public toggleCurrentLine()I
    .registers 3

    #@0
    .prologue
    .line 1919
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! toggleCurrentLine"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1920
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public uknightEventSet([BLandroid/os/Message;)V
    .registers 4
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1723
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->uknightEventSet([BLandroid/os/Message;)V

    #@5
    .line 1724
    return-void
.end method

.method public uknightGetData(ILandroid/os/Message;)V
    .registers 4
    .parameter "buf_num"
    .parameter "response"

    #@0
    .prologue
    .line 1732
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->uknightGetData(ILandroid/os/Message;)V

    #@5
    .line 1733
    return-void
.end method

.method public uknightLogSet([BLandroid/os/Message;)V
    .registers 4
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1720
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->uknightLogSet([BLandroid/os/Message;)V

    #@5
    .line 1721
    return-void
.end method

.method public uknightMemCheck(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1735
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->uknightMemCheck(Landroid/os/Message;)V

    #@5
    .line 1736
    return-void
.end method

.method public uknightMemSet(ILandroid/os/Message;)V
    .registers 4
    .parameter "percent"
    .parameter "response"

    #@0
    .prologue
    .line 1729
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->uknightMemSet(ILandroid/os/Message;)V

    #@5
    .line 1730
    return-void
.end method

.method public uknightStateChangeSet(ILandroid/os/Message;)V
    .registers 4
    .parameter "event"
    .parameter "response"

    #@0
    .prologue
    .line 1726
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->uknightStateChangeSet(ILandroid/os/Message;)V

    #@5
    .line 1727
    return-void
.end method

.method public unregisterForAvpUpgradeFailure(Landroid/os/Handler;)V
    .registers 5
    .parameter "h"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1692
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "unregisterForAvpUpgradeFailure is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public unregisterForCallWaiting(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1326
    const-string v0, "unregisterForCallWaiting"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1327
    return-void
.end method

.method public unregisterForCdmaOtaStatusChange(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1290
    const-string v0, "unregisterForCdmaOtaStatusChange"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1291
    return-void
.end method

.method public unregisterForDisconnect(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 737
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 738
    return-void
.end method

.method public unregisterForDisplayInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1352
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForDisplayInfo(Landroid/os/Handler;)V

    #@5
    .line 1353
    return-void
.end method

.method public unregisterForEcmTimerReset(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1336
    const-string v0, "unregisterForEcmTimerReset"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1337
    return-void
.end method

.method public unregisterForInCallVoicePrivacyOff(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 712
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForInCallVoicePrivacyOff(Landroid/os/Handler;)V

    #@5
    .line 713
    return-void
.end method

.method public unregisterForInCallVoicePrivacyOn(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 702
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForInCallVoicePrivacyOn(Landroid/os/Handler;)V

    #@5
    .line 703
    return-void
.end method

.method public unregisterForIncomingRing(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 725
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mIncomingRingRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 726
    return-void
.end method

.method public unregisterForLineControlInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1376
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForLineControlInfo(Landroid/os/Handler;)V

    #@5
    .line 1377
    return-void
.end method

.method public unregisterForMmiComplete(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 773
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->checkCorrectThread(Landroid/os/Handler;)V

    #@3
    .line 775
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 776
    return-void
.end method

.method public unregisterForMmiInitiate(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 761
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 762
    return-void
.end method

.method public unregisterForModifyCallRequest(Landroid/os/Handler;)V
    .registers 5
    .parameter "h"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1681
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "unregisterForModifyCallRequest is not supported in this phone "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method public unregisterForNewRingingConnection(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 692
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 693
    return-void
.end method

.method public unregisterForNumberInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1360
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForNumberInfo(Landroid/os/Handler;)V

    #@5
    .line 1361
    return-void
.end method

.method public unregisterForPreciseCallStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 658
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 659
    return-void
.end method

.method public unregisterForRedirectedNumberInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1368
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForRedirectedNumberInfo(Landroid/os/Handler;)V

    #@5
    .line 1369
    return-void
.end method

.method public unregisterForResendIncallMute(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 871
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForResendIncallMute(Landroid/os/Handler;)V

    #@5
    .line 872
    return-void
.end method

.method public unregisterForRingbackTone(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 861
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForRingbackTone(Landroid/os/Handler;)V

    #@5
    .line 862
    return-void
.end method

.method public unregisterForServiceStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 851
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mServiceStateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 852
    return-void
.end method

.method public unregisterForSetPreferredNetworkType(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 784
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mReferredNetworkTypeRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 785
    return-void
.end method

.method public unregisterForSignalInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1344
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForSignalInfo(Landroid/os/Handler;)V

    #@5
    .line 1345
    return-void
.end method

.method public unregisterForSimRecordsLoaded(Landroid/os/Handler;)V
    .registers 2
    .parameter "h"

    #@0
    .prologue
    .line 792
    return-void
.end method

.method public unregisterForSubscriptionInfoReady(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1300
    const-string v0, "unregisterForSubscriptionInfoReady"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1301
    return-void
.end method

.method public unregisterForSuppServiceFailed(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 749
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 750
    return-void
.end method

.method public unregisterForT53AudioControlInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1392
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForT53AudioControlInfo(Landroid/os/Handler;)V

    #@5
    .line 1393
    return-void
.end method

.method public unregisterForT53ClirInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1384
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForT53ClirInfo(Landroid/os/Handler;)V

    #@5
    .line 1385
    return-void
.end method

.method public unregisterForUnknownConnection(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 679
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 680
    return-void
.end method

.method public unregisterLGECipheringInd(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1745
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterLGECipheringInd(Landroid/os/Handler;)V

    #@5
    .line 1746
    return-void
.end method

.method public unregisterLGEUnsol(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1843
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterLGEUnsol(Landroid/os/Handler;)V

    #@5
    .line 1844
    return-void
.end method

.method public unsetOnEcbModeExitResponse(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1402
    const-string v0, "unsetOnEcbModeExitResponse"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneBase;->logUnexpectedCdmaMethodCall(Ljava/lang/String;)V

    #@5
    .line 1403
    return-void
.end method
