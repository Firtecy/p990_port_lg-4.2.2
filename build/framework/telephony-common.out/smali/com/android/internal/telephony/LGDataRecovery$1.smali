.class Lcom/android/internal/telephony/LGDataRecovery$1;
.super Landroid/content/BroadcastReceiver;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGDataRecovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LGDataRecovery;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/LGDataRecovery;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 190
    iput-object p1, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 16
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 195
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 197
    .local v0, action:Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v11, "onReceive: action="

    #@b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v10

    #@f
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v10

    #@13
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v10

    #@17
    invoke-static {v10}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@1a
    .line 200
    const-string v10, "android.intent.action.SCREEN_ON"

    #@1c
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v10

    #@20
    if-eqz v10, :cond_36

    #@22
    .line 201
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@24
    const/4 v11, 0x1

    #@25
    iput-boolean v11, v10, Lcom/android/internal/telephony/LGDataRecovery;->isScreenOn:Z

    #@27
    .line 202
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@29
    iget-object v11, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@2b
    const v12, 0x42065

    #@2e
    invoke-virtual {v11, v12}, Lcom/android/internal/telephony/LGDataRecovery;->obtainMessage(I)Landroid/os/Message;

    #@31
    move-result-object v11

    #@32
    invoke-virtual {v10, v11}, Lcom/android/internal/telephony/LGDataRecovery;->sendMessage(Landroid/os/Message;)Z

    #@35
    .line 265
    :cond_35
    :goto_35
    return-void

    #@36
    .line 204
    :cond_36
    const-string v10, "android.intent.action.SCREEN_OFF"

    #@38
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v10

    #@3c
    if-eqz v10, :cond_52

    #@3e
    .line 205
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@40
    const/4 v11, 0x0

    #@41
    iput-boolean v11, v10, Lcom/android/internal/telephony/LGDataRecovery;->isScreenOn:Z

    #@43
    .line 206
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@45
    iget-object v11, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@47
    const v12, 0x42066

    #@4a
    invoke-virtual {v11, v12}, Lcom/android/internal/telephony/LGDataRecovery;->obtainMessage(I)Landroid/os/Message;

    #@4d
    move-result-object v11

    #@4e
    invoke-virtual {v10, v11}, Lcom/android/internal/telephony/LGDataRecovery;->sendMessage(Landroid/os/Message;)Z

    #@51
    goto :goto_35

    #@52
    .line 208
    :cond_52
    const-string v10, "android.intent.action.ANY_DATA_STATE"

    #@54
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v10

    #@58
    if-eqz v10, :cond_8d

    #@5a
    .line 209
    const-class v10, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@5c
    const-string v11, "state"

    #@5e
    invoke-virtual {p2, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@61
    move-result-object v11

    #@62
    invoke-static {v10, v11}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@65
    move-result-object v9

    #@66
    check-cast v9, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@68
    .line 210
    .local v9, state:Lcom/android/internal/telephony/PhoneConstants$DataState;
    sget-object v10, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@6a
    if-ne v9, v10, :cond_7a

    #@6c
    .line 211
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@6e
    iget-object v11, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@70
    const v12, 0x42068

    #@73
    invoke-virtual {v11, v12, p2}, Lcom/android/internal/telephony/LGDataRecovery;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@76
    move-result-object v11

    #@77
    invoke-virtual {v10, v11}, Lcom/android/internal/telephony/LGDataRecovery;->sendMessage(Landroid/os/Message;)Z

    #@7a
    .line 213
    :cond_7a
    sget-object v10, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@7c
    if-ne v9, v10, :cond_35

    #@7e
    .line 214
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@80
    iget-object v11, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@82
    const v12, 0x42067

    #@85
    invoke-virtual {v11, v12, p2}, Lcom/android/internal/telephony/LGDataRecovery;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@88
    move-result-object v11

    #@89
    invoke-virtual {v10, v11}, Lcom/android/internal/telephony/LGDataRecovery;->sendMessage(Landroid/os/Message;)Z

    #@8c
    goto :goto_35

    #@8d
    .line 216
    .end local v9           #state:Lcom/android/internal/telephony/PhoneConstants$DataState;
    :cond_8d
    const-string v10, "com.lge.internal.telephony.lge-data-stall-alarm"

    #@8f
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@92
    move-result v10

    #@93
    if-eqz v10, :cond_b1

    #@95
    .line 217
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@97
    const v11, 0x4206a

    #@9a
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@9d
    move-result-object v12

    #@9e
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/telephony/LGDataRecovery;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a1
    move-result-object v7

    #@a2
    .line 218
    .local v7, msg:Landroid/os/Message;
    const-string v10, "data.stall.alram.tag"

    #@a4
    const/4 v11, 0x0

    #@a5
    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@a8
    move-result v10

    #@a9
    iput v10, v7, Landroid/os/Message;->arg1:I

    #@ab
    .line 219
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@ad
    invoke-virtual {v10, v7}, Lcom/android/internal/telephony/LGDataRecovery;->sendMessage(Landroid/os/Message;)Z

    #@b0
    goto :goto_35

    #@b1
    .line 220
    .end local v7           #msg:Landroid/os/Message;
    :cond_b1
    const-string v10, "com.lge.internal.telephony.lge-data-conn-check-alarm"

    #@b3
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b6
    move-result v10

    #@b7
    if-eqz v10, :cond_dc

    #@b9
    .line 221
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@bb
    const v11, 0x4206b

    #@be
    invoke-virtual {v10, v11}, Lcom/android/internal/telephony/LGDataRecovery;->obtainMessage(I)Landroid/os/Message;

    #@c1
    move-result-object v7

    #@c2
    .line 222
    .restart local v7       #msg:Landroid/os/Message;
    const-string v10, "conn.check.alram.tag"

    #@c4
    invoke-virtual {p2, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@c7
    move-result-object v5

    #@c8
    .line 223
    .local v5, iface:Ljava/lang/String;
    new-instance v2, Landroid/os/Bundle;

    #@ca
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@cd
    .line 224
    .local v2, data:Landroid/os/Bundle;
    const-string v10, "iface"

    #@cf
    invoke-virtual {v2, v10, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@d2
    .line 225
    invoke-virtual {v7, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@d5
    .line 226
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@d7
    invoke-virtual {v10, v7}, Lcom/android/internal/telephony/LGDataRecovery;->sendMessage(Landroid/os/Message;)Z

    #@da
    goto/16 :goto_35

    #@dc
    .line 227
    .end local v2           #data:Landroid/os/Bundle;
    .end local v5           #iface:Ljava/lang/String;
    .end local v7           #msg:Landroid/os/Message;
    :cond_dc
    const-string v10, "android.net.wifi.STATE_CHANGE"

    #@de
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e1
    move-result v10

    #@e2
    if-eqz v10, :cond_fe

    #@e4
    .line 228
    const-string v10, "networkInfo"

    #@e6
    invoke-virtual {p2, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@e9
    move-result-object v8

    #@ea
    check-cast v8, Landroid/net/NetworkInfo;

    #@ec
    .line 230
    .local v8, networkInfo:Landroid/net/NetworkInfo;
    iget-object v11, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@ee
    if-eqz v8, :cond_fc

    #@f0
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->isConnected()Z

    #@f3
    move-result v10

    #@f4
    if-eqz v10, :cond_fc

    #@f6
    const/4 v10, 0x1

    #@f7
    :goto_f7
    invoke-static {v11, v10}, Lcom/android/internal/telephony/LGDataRecovery;->access$002(Lcom/android/internal/telephony/LGDataRecovery;Z)Z

    #@fa
    goto/16 :goto_35

    #@fc
    :cond_fc
    const/4 v10, 0x0

    #@fd
    goto :goto_f7

    #@fe
    .line 231
    .end local v8           #networkInfo:Landroid/net/NetworkInfo;
    :cond_fe
    const-string v10, "android.net.wifi.WIFI_STATE_CHANGED"

    #@100
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@103
    move-result v10

    #@104
    if-eqz v10, :cond_11d

    #@106
    .line 232
    const-string v10, "wifi_state"

    #@108
    const/4 v11, 0x4

    #@109
    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@10c
    move-result v10

    #@10d
    const/4 v11, 0x3

    #@10e
    if-ne v10, v11, :cond_11b

    #@110
    const/4 v4, 0x1

    #@111
    .line 235
    .local v4, enabled:Z
    :goto_111
    if-nez v4, :cond_35

    #@113
    .line 238
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@115
    const/4 v11, 0x0

    #@116
    invoke-static {v10, v11}, Lcom/android/internal/telephony/LGDataRecovery;->access$002(Lcom/android/internal/telephony/LGDataRecovery;Z)Z

    #@119
    goto/16 :goto_35

    #@11b
    .line 232
    .end local v4           #enabled:Z
    :cond_11b
    const/4 v4, 0x0

    #@11c
    goto :goto_111

    #@11d
    .line 240
    :cond_11d
    const-string v10, "com.lge.ims.action.CALL_STATE"

    #@11f
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@122
    move-result v10

    #@123
    if-eqz v10, :cond_15b

    #@125
    .line 241
    const-string v10, "state"

    #@127
    const/4 v11, 0x0

    #@128
    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@12b
    move-result v6

    #@12c
    .line 242
    .local v6, imscallstate:I
    new-instance v10, Ljava/lang/StringBuilder;

    #@12e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@131
    const-string v11, "IMS_CALL_STATE intent received: "

    #@133
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v10

    #@137
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v10

    #@13b
    const-string v11, " [1:active / 0:inactive]"

    #@13d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v10

    #@141
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@144
    move-result-object v10

    #@145
    invoke-static {v10}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@148
    .line 243
    const/4 v10, 0x1

    #@149
    if-ne v6, v10, :cond_153

    #@14b
    .line 244
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@14d
    const/4 v11, 0x1

    #@14e
    invoke-static {v10, v11}, Lcom/android/internal/telephony/LGDataRecovery;->access$102(Lcom/android/internal/telephony/LGDataRecovery;Z)Z

    #@151
    goto/16 :goto_35

    #@153
    .line 246
    :cond_153
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@155
    const/4 v11, 0x0

    #@156
    invoke-static {v10, v11}, Lcom/android/internal/telephony/LGDataRecovery;->access$102(Lcom/android/internal/telephony/LGDataRecovery;Z)Z

    #@159
    goto/16 :goto_35

    #@15b
    .line 248
    .end local v6           #imscallstate:I
    :cond_15b
    const-string v10, "lg-data-pulllog"

    #@15d
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@160
    move-result v10

    #@161
    if-eqz v10, :cond_35

    #@163
    .line 250
    const-string v10, "LG Data pull log"

    #@165
    invoke-static {v10}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@168
    .line 252
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@16a
    invoke-static {v10}, Lcom/android/internal/telephony/LGDataRecovery;->access$200(Lcom/android/internal/telephony/LGDataRecovery;)Landroid/os/INetworkManagementService;

    #@16d
    move-result-object v10

    #@16e
    if-nez v10, :cond_17f

    #@170
    .line 253
    const-string v10, "network_management"

    #@172
    invoke-static {v10}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@175
    move-result-object v1

    #@176
    .line 254
    .local v1, b:Landroid/os/IBinder;
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@178
    invoke-static {v1}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@17b
    move-result-object v11

    #@17c
    invoke-static {v10, v11}, Lcom/android/internal/telephony/LGDataRecovery;->access$202(Lcom/android/internal/telephony/LGDataRecovery;Landroid/os/INetworkManagementService;)Landroid/os/INetworkManagementService;

    #@17f
    .line 258
    .end local v1           #b:Landroid/os/IBinder;
    :cond_17f
    :try_start_17f
    iget-object v10, p0, Lcom/android/internal/telephony/LGDataRecovery$1;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@181
    invoke-static {v10}, Lcom/android/internal/telephony/LGDataRecovery;->access$200(Lcom/android/internal/telephony/LGDataRecovery;)Landroid/os/INetworkManagementService;

    #@184
    move-result-object v10

    #@185
    const-string v11, "lgdata_pulllog"

    #@187
    invoke-interface {v10, v11}, Landroid/os/INetworkManagementService;->runShellCommand(Ljava/lang/String;)V
    :try_end_18a
    .catch Ljava/lang/Exception; {:try_start_17f .. :try_end_18a} :catch_18c

    #@18a
    goto/16 :goto_35

    #@18c
    .line 259
    :catch_18c
    move-exception v3

    #@18d
    .line 260
    .local v3, e:Ljava/lang/Exception;
    const-string v10, "Error in pulling LGData log"

    #@18f
    invoke-static {v10}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@192
    goto/16 :goto_35
.end method
