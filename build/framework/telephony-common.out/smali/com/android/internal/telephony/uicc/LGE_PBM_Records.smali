.class public Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
.super Ljava/lang/Object;
.source "LGE_PBM_Records.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/telephony/uicc/LGE_PBM_Records;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public ad_type:I

.field public additional_number:Ljava/lang/String;

.field public additional_number_a:Ljava/lang/String;

.field public additional_number_b:Ljava/lang/String;

.field public device:I

.field public email_address:Ljava/lang/String;

.field public gas_id:I

.field public index:I

.field public name:Ljava/lang/String;

.field public number:Ljava/lang/String;

.field public sync_cnt:I

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 23
    new-instance v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/LGE_PBM_Records$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 38
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 2
    .parameter "source"

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->readFromParcel(Landroid/os/Parcel;)V

    #@6
    .line 42
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/internal/telephony/uicc/LGE_PBM_Records$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 7
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 75
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->device:I

    #@6
    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@c
    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->type:I

    #@12
    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@18
    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->number:Ljava/lang/String;

    #@1e
    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@24
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number:Ljava/lang/String;

    #@2a
    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_a:Ljava/lang/String;

    #@30
    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_b:Ljava/lang/String;

    #@36
    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->email_address:Ljava/lang/String;

    #@3c
    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3f
    move-result v0

    #@40
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->gas_id:I

    #@42
    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v0

    #@46
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->sync_cnt:I

    #@48
    .line 72
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, " Device  "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->device:I

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, " index: "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    const-string v1, " type: "

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->type:I

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    const-string v1, " ad_type: "

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    const-string v1, " number: "

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->number:Ljava/lang/String;

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    const-string v1, " name: "

    #@4b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    const-string v1, " additional_number: "

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number:Ljava/lang/String;

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    const-string v1, " additional_number_a: "

    #@63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v0

    #@67
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_a:Ljava/lang/String;

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v0

    #@6d
    const-string v1, " additional_number_b: "

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_b:Ljava/lang/String;

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    const-string v1, " email_address: "

    #@7b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v0

    #@7f
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->email_address:Ljava/lang/String;

    #@81
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v0

    #@85
    const-string v1, " gas_id: "

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->gas_id:I

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    const-string v1, " sync_cnt: "

    #@93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v0

    #@97
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->sync_cnt:I

    #@99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v0

    #@9d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v0

    #@a1
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 45
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->device:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 46
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->index:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 47
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->type:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 48
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->ad_type:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 49
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->number:Ljava/lang/String;

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 50
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->name:Ljava/lang/String;

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1e
    .line 51
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number:Ljava/lang/String;

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 52
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_a:Ljava/lang/String;

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@28
    .line 53
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->additional_number_b:Ljava/lang/String;

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    .line 54
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->email_address:Ljava/lang/String;

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@32
    .line 55
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->gas_id:I

    #@34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    .line 56
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->sync_cnt:I

    #@39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3c
    .line 57
    return-void
.end method
