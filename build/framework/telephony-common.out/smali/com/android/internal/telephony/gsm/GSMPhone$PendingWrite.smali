.class Lcom/android/internal/telephony/gsm/GSMPhone$PendingWrite;
.super Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;
.source "GSMPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gsm/GSMPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PendingWrite"
.end annotation


# instance fields
.field private mData:[B

.field private mEfid:I

.field final synthetic this$0:Lcom/android/internal/telephony/gsm/GSMPhone;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/gsm/GSMPhone;I[BLandroid/os/Message;)V
    .registers 5
    .parameter
    .parameter "efid"
    .parameter "data"
    .parameter "onComplete"

    #@0
    .prologue
    .line 3273
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingWrite;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2
    .line 3274
    invoke-direct {p0, p1, p4}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Landroid/os/Message;)V

    #@5
    .line 3275
    iput-object p3, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingWrite;->mData:[B

    #@7
    .line 3276
    iput p2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingWrite;->mEfid:I

    #@9
    .line 3277
    return-void
.end method


# virtual methods
.method public onSessionStarted(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .registers 8
    .parameter "res"
    .parameter "e"

    #@0
    .prologue
    .line 3281
    if-eqz p2, :cond_6

    #@2
    .line 3282
    invoke-super {p0, p1, p2}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;->onSessionStarted(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@5
    .line 3299
    :goto_5
    return-void

    #@6
    .line 3292
    :cond_6
    :try_start_6
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingWrite;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@8
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$1700(Lcom/android/internal/telephony/gsm/GSMPhone;)Lcom/android/internal/telephony/uicc/UiccController;

    #@b
    move-result-object v2

    #@c
    const/4 v3, 0x3

    #@d
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@14
    move-result-object v0

    #@15
    .line 3293
    .local v0, iccFh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    iget v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingWrite;->mEfid:I

    #@17
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingWrite;->mData:[B

    #@19
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;->mOnComplete:Landroid/os/Message;

    #@1b
    invoke-virtual {v0, v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFTransparent(I[BLandroid/os/Message;)V
    :try_end_1e
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_1e} :catch_1f

    #@1e
    goto :goto_5

    #@1f
    .line 3294
    .end local v0           #iccFh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    :catch_1f
    move-exception v1

    #@20
    .line 3295
    .local v1, ne:Ljava/lang/NullPointerException;
    const-string v2, "GSM"

    #@22
    const-string v3, "NullPointerException occur - mUiccController.getUiccCardApplication(UiccController.APP_FAM_IMS).getIccFileHandler()"

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    goto :goto_5
.end method
