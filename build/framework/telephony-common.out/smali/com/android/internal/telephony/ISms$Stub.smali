.class public abstract Lcom/android/internal/telephony/ISms$Stub;
.super Landroid/os/Binder;
.source "ISms.java"

# interfaces
.implements Lcom/android/internal/telephony/ISms;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/ISms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/ISms$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.ISms"

.field static final TRANSACTION_SendMultipartText:I = 0xc

.field static final TRANSACTION_SendText:I = 0x9

.field static final TRANSACTION_copyMessageToIccEf:I = 0x5

.field static final TRANSACTION_copySmsToIccEf:I = 0x2a

.field static final TRANSACTION_copySmsToIccEfPrivate:I = 0x19

.field static final TRANSACTION_disableCdmaBroadcast:I = 0x15

.field static final TRANSACTION_disableCdmaBroadcastRange:I = 0x20

.field static final TRANSACTION_disableCellBroadcast:I = 0x13

.field static final TRANSACTION_disableCellBroadcastRange:I = 0x17

.field static final TRANSACTION_enableAutoDCDisconnect:I = 0x7

.field static final TRANSACTION_enableCdmaBroadcast:I = 0x14

.field static final TRANSACTION_enableCdmaBroadcastRange:I = 0x1f

.field static final TRANSACTION_enableCellBroadcast:I = 0x12

.field static final TRANSACTION_enableCellBroadcastRange:I = 0x16

.field static final TRANSACTION_getAllMessagesFromIccEf:I = 0x1

.field static final TRANSACTION_getAllMessagesFromIccEf3GPP:I = 0x1a

.field static final TRANSACTION_getAllMessagesFromIccEf3GPP2:I = 0x1b

.field static final TRANSACTION_getImsSmsFormat:I = 0x24

.field static final TRANSACTION_getMaxEfSms:I = 0x28

.field static final TRANSACTION_getPremiumSmsPermission:I = 0x21

.field static final TRANSACTION_getReadItemMessagesFromIccEf:I = 0x2

.field static final TRANSACTION_getSmscenterAddress:I = 0x26

.field static final TRANSACTION_getUiccType:I = 0x1d

.field static final TRANSACTION_isFdnEnabled:I = 0x2b

.field static final TRANSACTION_isGsmMo:I = 0x18

.field static final TRANSACTION_isImsSmsSupported:I = 0x23

.field static final TRANSACTION_sendData:I = 0x6

.field static final TRANSACTION_sendEmailoverMultipartText:I = 0xd

.field static final TRANSACTION_sendEmailoverText:I = 0xa

.field static final TRANSACTION_sendMultipartText:I = 0xb

.field static final TRANSACTION_sendMultipartTextLge:I = 0xf

.field static final TRANSACTION_sendMultipartTextMoreLge:I = 0x10

.field static final TRANSACTION_sendText:I = 0x8

.field static final TRANSACTION_sendTextLge:I = 0xe

.field static final TRANSACTION_sendTextMoreLge:I = 0x11

.field static final TRANSACTION_setMultipartTextValidityPeriod:I = 0x25

.field static final TRANSACTION_setPremiumSmsPermission:I = 0x22

.field static final TRANSACTION_setSmsIsRoaming:I = 0x2c

.field static final TRANSACTION_setSmsPriority:I = 0x1e

.field static final TRANSACTION_setSmscenterAddress:I = 0x27

.field static final TRANSACTION_setUiccType:I = 0x1c

.field static final TRANSACTION_startTestCase:I = 0x2d

.field static final TRANSACTION_updateMessageOnIccEf:I = 0x3

.field static final TRANSACTION_updateMessageOnIccEfMultiMode:I = 0x4

.field static final TRANSACTION_updateSmsOnSimReadStatus:I = 0x29


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 27
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 28
    const-string v0, "com.android.internal.telephony.ISms"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/ISms$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 29
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 36
    if-nez p0, :cond_4

    #@2
    .line 37
    const/4 v0, 0x0

    #@3
    .line 43
    :goto_3
    return-object v0

    #@4
    .line 39
    :cond_4
    const-string v1, "com.android.internal.telephony.ISms"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 40
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/ISms;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 41
    check-cast v0, Lcom/android/internal/telephony/ISms;

    #@12
    goto :goto_3

    #@13
    .line 43
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/ISms$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/ISms$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 47
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 32
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 51
    sparse-switch p1, :sswitch_data_706

    #@3
    .line 695
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v2

    #@7
    :goto_7
    return v2

    #@8
    .line 55
    :sswitch_8
    const-string v2, "com.android.internal.telephony.ISms"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 56
    const/4 v2, 0x1

    #@10
    goto :goto_7

    #@11
    .line 60
    :sswitch_11
    const-string v2, "com.android.internal.telephony.ISms"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 61
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/ISms$Stub;->getAllMessagesFromIccEf()Ljava/util/List;

    #@1b
    move-result-object v26

    #@1c
    .line 62
    .local v26, _result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f
    .line 63
    move-object/from16 v0, p3

    #@21
    move-object/from16 v1, v26

    #@23
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@26
    .line 64
    const/4 v2, 0x1

    #@27
    goto :goto_7

    #@28
    .line 68
    .end local v26           #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    :sswitch_28
    const-string v2, "com.android.internal.telephony.ISms"

    #@2a
    move-object/from16 v0, p2

    #@2c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2f
    .line 70
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@32
    move-result v3

    #@33
    .line 71
    .local v3, _arg0:I
    move-object/from16 v0, p0

    #@35
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->getReadItemMessagesFromIccEf(I)Ljava/util/List;

    #@38
    move-result-object v26

    #@39
    .line 72
    .restart local v26       #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c
    .line 73
    move-object/from16 v0, p3

    #@3e
    move-object/from16 v1, v26

    #@40
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@43
    .line 74
    const/4 v2, 0x1

    #@44
    goto :goto_7

    #@45
    .line 78
    .end local v3           #_arg0:I
    .end local v26           #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    :sswitch_45
    const-string v2, "com.android.internal.telephony.ISms"

    #@47
    move-object/from16 v0, p2

    #@49
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4c
    .line 80
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4f
    move-result v3

    #@50
    .line 82
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@53
    move-result v4

    #@54
    .line 84
    .local v4, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@57
    move-result-object v5

    #@58
    .line 85
    .local v5, _arg2:[B
    move-object/from16 v0, p0

    #@5a
    invoke-virtual {v0, v3, v4, v5}, Lcom/android/internal/telephony/ISms$Stub;->updateMessageOnIccEf(II[B)Z

    #@5d
    move-result v25

    #@5e
    .line 86
    .local v25, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@61
    .line 87
    if-eqz v25, :cond_6b

    #@63
    const/4 v2, 0x1

    #@64
    :goto_64
    move-object/from16 v0, p3

    #@66
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@69
    .line 88
    const/4 v2, 0x1

    #@6a
    goto :goto_7

    #@6b
    .line 87
    :cond_6b
    const/4 v2, 0x0

    #@6c
    goto :goto_64

    #@6d
    .line 92
    .end local v3           #_arg0:I
    .end local v4           #_arg1:I
    .end local v5           #_arg2:[B
    .end local v25           #_result:Z
    :sswitch_6d
    const-string v2, "com.android.internal.telephony.ISms"

    #@6f
    move-object/from16 v0, p2

    #@71
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@74
    .line 94
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@77
    move-result v3

    #@78
    .line 96
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7b
    move-result v4

    #@7c
    .line 98
    .restart local v4       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@7f
    move-result-object v5

    #@80
    .line 100
    .restart local v5       #_arg2:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@83
    move-result v6

    #@84
    .line 101
    .local v6, _arg3:I
    move-object/from16 v0, p0

    #@86
    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/android/internal/telephony/ISms$Stub;->updateMessageOnIccEfMultiMode(II[BI)Z

    #@89
    move-result v25

    #@8a
    .line 102
    .restart local v25       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8d
    .line 103
    if-eqz v25, :cond_98

    #@8f
    const/4 v2, 0x1

    #@90
    :goto_90
    move-object/from16 v0, p3

    #@92
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@95
    .line 104
    const/4 v2, 0x1

    #@96
    goto/16 :goto_7

    #@98
    .line 103
    :cond_98
    const/4 v2, 0x0

    #@99
    goto :goto_90

    #@9a
    .line 108
    .end local v3           #_arg0:I
    .end local v4           #_arg1:I
    .end local v5           #_arg2:[B
    .end local v6           #_arg3:I
    .end local v25           #_result:Z
    :sswitch_9a
    const-string v2, "com.android.internal.telephony.ISms"

    #@9c
    move-object/from16 v0, p2

    #@9e
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a1
    .line 110
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a4
    move-result v3

    #@a5
    .line 112
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@a8
    move-result-object v4

    #@a9
    .line 114
    .local v4, _arg1:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@ac
    move-result-object v5

    #@ad
    .line 115
    .restart local v5       #_arg2:[B
    move-object/from16 v0, p0

    #@af
    invoke-virtual {v0, v3, v4, v5}, Lcom/android/internal/telephony/ISms$Stub;->copyMessageToIccEf(I[B[B)Z

    #@b2
    move-result v25

    #@b3
    .line 116
    .restart local v25       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b6
    .line 117
    if-eqz v25, :cond_c1

    #@b8
    const/4 v2, 0x1

    #@b9
    :goto_b9
    move-object/from16 v0, p3

    #@bb
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@be
    .line 118
    const/4 v2, 0x1

    #@bf
    goto/16 :goto_7

    #@c1
    .line 117
    :cond_c1
    const/4 v2, 0x0

    #@c2
    goto :goto_b9

    #@c3
    .line 122
    .end local v3           #_arg0:I
    .end local v4           #_arg1:[B
    .end local v5           #_arg2:[B
    .end local v25           #_result:Z
    :sswitch_c3
    const-string v2, "com.android.internal.telephony.ISms"

    #@c5
    move-object/from16 v0, p2

    #@c7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ca
    .line 124
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@cd
    move-result-object v3

    #@ce
    .line 126
    .local v3, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d1
    move-result-object v4

    #@d2
    .line 128
    .local v4, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d5
    move-result v5

    #@d6
    .line 130
    .local v5, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@d9
    move-result-object v6

    #@da
    .line 132
    .local v6, _arg3:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@dd
    move-result v2

    #@de
    if-eqz v2, :cond_105

    #@e0
    .line 133
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e2
    move-object/from16 v0, p2

    #@e4
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e7
    move-result-object v7

    #@e8
    check-cast v7, Landroid/app/PendingIntent;

    #@ea
    .line 139
    .local v7, _arg4:Landroid/app/PendingIntent;
    :goto_ea
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ed
    move-result v2

    #@ee
    if-eqz v2, :cond_107

    #@f0
    .line 140
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f2
    move-object/from16 v0, p2

    #@f4
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f7
    move-result-object v8

    #@f8
    check-cast v8, Landroid/app/PendingIntent;

    #@fa
    .local v8, _arg5:Landroid/app/PendingIntent;
    :goto_fa
    move-object/from16 v2, p0

    #@fc
    .line 145
    invoke-virtual/range {v2 .. v8}, Lcom/android/internal/telephony/ISms$Stub;->sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@ff
    .line 146
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@102
    .line 147
    const/4 v2, 0x1

    #@103
    goto/16 :goto_7

    #@105
    .line 136
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    .end local v8           #_arg5:Landroid/app/PendingIntent;
    :cond_105
    const/4 v7, 0x0

    #@106
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    goto :goto_ea

    #@107
    .line 143
    :cond_107
    const/4 v8, 0x0

    #@108
    .restart local v8       #_arg5:Landroid/app/PendingIntent;
    goto :goto_fa

    #@109
    .line 151
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v5           #_arg2:I
    .end local v6           #_arg3:[B
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    .end local v8           #_arg5:Landroid/app/PendingIntent;
    :sswitch_109
    const-string v2, "com.android.internal.telephony.ISms"

    #@10b
    move-object/from16 v0, p2

    #@10d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@110
    .line 153
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@113
    move-result v3

    #@114
    .line 154
    .local v3, _arg0:I
    move-object/from16 v0, p0

    #@116
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->enableAutoDCDisconnect(I)V

    #@119
    .line 155
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@11c
    .line 156
    const/4 v2, 0x1

    #@11d
    goto/16 :goto_7

    #@11f
    .line 160
    .end local v3           #_arg0:I
    :sswitch_11f
    const-string v2, "com.android.internal.telephony.ISms"

    #@121
    move-object/from16 v0, p2

    #@123
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@126
    .line 162
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@129
    move-result-object v3

    #@12a
    .line 164
    .local v3, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12d
    move-result-object v4

    #@12e
    .line 166
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@131
    move-result-object v5

    #@132
    .line 168
    .local v5, _arg2:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@135
    move-result v2

    #@136
    if-eqz v2, :cond_15d

    #@138
    .line 169
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@13a
    move-object/from16 v0, p2

    #@13c
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@13f
    move-result-object v6

    #@140
    check-cast v6, Landroid/app/PendingIntent;

    #@142
    .line 175
    .local v6, _arg3:Landroid/app/PendingIntent;
    :goto_142
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@145
    move-result v2

    #@146
    if-eqz v2, :cond_15f

    #@148
    .line 176
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@14a
    move-object/from16 v0, p2

    #@14c
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@14f
    move-result-object v7

    #@150
    check-cast v7, Landroid/app/PendingIntent;

    #@152
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    :goto_152
    move-object/from16 v2, p0

    #@154
    .line 181
    invoke-virtual/range {v2 .. v7}, Lcom/android/internal/telephony/ISms$Stub;->sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@157
    .line 182
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@15a
    .line 183
    const/4 v2, 0x1

    #@15b
    goto/16 :goto_7

    #@15d
    .line 172
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    :cond_15d
    const/4 v6, 0x0

    #@15e
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    goto :goto_142

    #@15f
    .line 179
    :cond_15f
    const/4 v7, 0x0

    #@160
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    goto :goto_152

    #@161
    .line 187
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v5           #_arg2:Ljava/lang/String;
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    :sswitch_161
    const-string v2, "com.android.internal.telephony.ISms"

    #@163
    move-object/from16 v0, p2

    #@165
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@168
    .line 189
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@16b
    move-result-object v3

    #@16c
    .line 191
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@16f
    move-result-object v4

    #@170
    .line 193
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@173
    move-result-object v5

    #@174
    .line 195
    .restart local v5       #_arg2:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@177
    move-result v2

    #@178
    if-eqz v2, :cond_1a3

    #@17a
    .line 196
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@17c
    move-object/from16 v0, p2

    #@17e
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@181
    move-result-object v6

    #@182
    check-cast v6, Landroid/app/PendingIntent;

    #@184
    .line 202
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    :goto_184
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@187
    move-result v2

    #@188
    if-eqz v2, :cond_1a5

    #@18a
    .line 203
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@18c
    move-object/from16 v0, p2

    #@18e
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@191
    move-result-object v7

    #@192
    check-cast v7, Landroid/app/PendingIntent;

    #@194
    .line 209
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    :goto_194
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@197
    move-result-object v8

    #@198
    .local v8, _arg5:Ljava/lang/String;
    move-object/from16 v2, p0

    #@19a
    .line 210
    invoke-virtual/range {v2 .. v8}, Lcom/android/internal/telephony/ISms$Stub;->SendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V

    #@19d
    .line 211
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a0
    .line 212
    const/4 v2, 0x1

    #@1a1
    goto/16 :goto_7

    #@1a3
    .line 199
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    .end local v8           #_arg5:Ljava/lang/String;
    :cond_1a3
    const/4 v6, 0x0

    #@1a4
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    goto :goto_184

    #@1a5
    .line 206
    :cond_1a5
    const/4 v7, 0x0

    #@1a6
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    goto :goto_194

    #@1a7
    .line 216
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v5           #_arg2:Ljava/lang/String;
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    :sswitch_1a7
    const-string v2, "com.android.internal.telephony.ISms"

    #@1a9
    move-object/from16 v0, p2

    #@1ab
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ae
    .line 218
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b1
    move-result-object v3

    #@1b2
    .line 220
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b5
    move-result-object v4

    #@1b6
    .line 222
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b9
    move-result-object v5

    #@1ba
    .line 224
    .restart local v5       #_arg2:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1bd
    move-result v2

    #@1be
    if-eqz v2, :cond_1e5

    #@1c0
    .line 225
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c2
    move-object/from16 v0, p2

    #@1c4
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1c7
    move-result-object v6

    #@1c8
    check-cast v6, Landroid/app/PendingIntent;

    #@1ca
    .line 231
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    :goto_1ca
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1cd
    move-result v2

    #@1ce
    if-eqz v2, :cond_1e7

    #@1d0
    .line 232
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d2
    move-object/from16 v0, p2

    #@1d4
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1d7
    move-result-object v7

    #@1d8
    check-cast v7, Landroid/app/PendingIntent;

    #@1da
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    :goto_1da
    move-object/from16 v2, p0

    #@1dc
    .line 237
    invoke-virtual/range {v2 .. v7}, Lcom/android/internal/telephony/ISms$Stub;->sendEmailoverText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@1df
    .line 238
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e2
    .line 239
    const/4 v2, 0x1

    #@1e3
    goto/16 :goto_7

    #@1e5
    .line 228
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    :cond_1e5
    const/4 v6, 0x0

    #@1e6
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    goto :goto_1ca

    #@1e7
    .line 235
    :cond_1e7
    const/4 v7, 0x0

    #@1e8
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    goto :goto_1da

    #@1e9
    .line 243
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v5           #_arg2:Ljava/lang/String;
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    :sswitch_1e9
    const-string v2, "com.android.internal.telephony.ISms"

    #@1eb
    move-object/from16 v0, p2

    #@1ed
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1f0
    .line 245
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1f3
    move-result-object v3

    #@1f4
    .line 247
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1f7
    move-result-object v4

    #@1f8
    .line 249
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    #@1fb
    move-result-object v12

    #@1fc
    .line 251
    .local v12, _arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1fe
    move-object/from16 v0, p2

    #@200
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@203
    move-result-object v13

    #@204
    .line 253
    .local v13, _arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@206
    move-object/from16 v0, p2

    #@208
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@20b
    move-result-object v14

    #@20c
    .local v14, _arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    move-object/from16 v9, p0

    #@20e
    move-object v10, v3

    #@20f
    move-object v11, v4

    #@210
    .line 254
    invoke-virtual/range {v9 .. v14}, Lcom/android/internal/telephony/ISms$Stub;->sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    #@213
    .line 255
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@216
    .line 256
    const/4 v2, 0x1

    #@217
    goto/16 :goto_7

    #@219
    .line 260
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v12           #_arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v13           #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .end local v14           #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    :sswitch_219
    const-string v2, "com.android.internal.telephony.ISms"

    #@21b
    move-object/from16 v0, p2

    #@21d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@220
    .line 262
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@223
    move-result-object v3

    #@224
    .line 264
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@227
    move-result-object v4

    #@228
    .line 266
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    #@22b
    move-result-object v12

    #@22c
    .line 268
    .restart local v12       #_arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@22e
    move-object/from16 v0, p2

    #@230
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@233
    move-result-object v13

    #@234
    .line 270
    .restart local v13       #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@236
    move-object/from16 v0, p2

    #@238
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@23b
    move-result-object v14

    #@23c
    .line 272
    .restart local v14       #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@23f
    move-result-object v8

    #@240
    .restart local v8       #_arg5:Ljava/lang/String;
    move-object/from16 v9, p0

    #@242
    move-object v10, v3

    #@243
    move-object v11, v4

    #@244
    move-object v15, v8

    #@245
    .line 273
    invoke-virtual/range {v9 .. v15}, Lcom/android/internal/telephony/ISms$Stub;->SendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    #@248
    .line 274
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@24b
    .line 275
    const/4 v2, 0x1

    #@24c
    goto/16 :goto_7

    #@24e
    .line 279
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v8           #_arg5:Ljava/lang/String;
    .end local v12           #_arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v13           #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .end local v14           #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    :sswitch_24e
    const-string v2, "com.android.internal.telephony.ISms"

    #@250
    move-object/from16 v0, p2

    #@252
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@255
    .line 281
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@258
    move-result-object v3

    #@259
    .line 283
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@25c
    move-result-object v4

    #@25d
    .line 285
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    #@260
    move-result-object v12

    #@261
    .line 287
    .restart local v12       #_arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@263
    move-object/from16 v0, p2

    #@265
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@268
    move-result-object v13

    #@269
    .line 289
    .restart local v13       #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@26b
    move-object/from16 v0, p2

    #@26d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@270
    move-result-object v14

    #@271
    .restart local v14       #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    move-object/from16 v9, p0

    #@273
    move-object v10, v3

    #@274
    move-object v11, v4

    #@275
    .line 290
    invoke-virtual/range {v9 .. v14}, Lcom/android/internal/telephony/ISms$Stub;->sendEmailoverMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    #@278
    .line 291
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@27b
    .line 292
    const/4 v2, 0x1

    #@27c
    goto/16 :goto_7

    #@27e
    .line 296
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v12           #_arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v13           #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .end local v14           #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    :sswitch_27e
    const-string v2, "com.android.internal.telephony.ISms"

    #@280
    move-object/from16 v0, p2

    #@282
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@285
    .line 298
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@288
    move-result-object v3

    #@289
    .line 300
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@28c
    move-result-object v4

    #@28d
    .line 302
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@290
    move-result-object v5

    #@291
    .line 304
    .restart local v5       #_arg2:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@294
    move-result v2

    #@295
    if-eqz v2, :cond_2cc

    #@297
    .line 305
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@299
    move-object/from16 v0, p2

    #@29b
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@29e
    move-result-object v6

    #@29f
    check-cast v6, Landroid/app/PendingIntent;

    #@2a1
    .line 311
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    :goto_2a1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2a4
    move-result v2

    #@2a5
    if-eqz v2, :cond_2ce

    #@2a7
    .line 312
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2a9
    move-object/from16 v0, p2

    #@2ab
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2ae
    move-result-object v7

    #@2af
    check-cast v7, Landroid/app/PendingIntent;

    #@2b1
    .line 318
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    :goto_2b1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2b4
    move-result-object v8

    #@2b5
    .line 320
    .restart local v8       #_arg5:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2b8
    move-result v9

    #@2b9
    .line 322
    .local v9, _arg6:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2bc
    move-result v10

    #@2bd
    .line 324
    .local v10, _arg7:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2c0
    move-result v11

    #@2c1
    .local v11, _arg8:I
    move-object/from16 v2, p0

    #@2c3
    .line 325
    invoke-virtual/range {v2 .. v11}, Lcom/android/internal/telephony/ISms$Stub;->sendTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@2c6
    .line 326
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c9
    .line 327
    const/4 v2, 0x1

    #@2ca
    goto/16 :goto_7

    #@2cc
    .line 308
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    .end local v8           #_arg5:Ljava/lang/String;
    .end local v9           #_arg6:I
    .end local v10           #_arg7:I
    .end local v11           #_arg8:I
    :cond_2cc
    const/4 v6, 0x0

    #@2cd
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    goto :goto_2a1

    #@2ce
    .line 315
    :cond_2ce
    const/4 v7, 0x0

    #@2cf
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    goto :goto_2b1

    #@2d0
    .line 331
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v5           #_arg2:Ljava/lang/String;
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    :sswitch_2d0
    const-string v2, "com.android.internal.telephony.ISms"

    #@2d2
    move-object/from16 v0, p2

    #@2d4
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d7
    .line 333
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2da
    move-result-object v3

    #@2db
    .line 335
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2de
    move-result-object v4

    #@2df
    .line 337
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    #@2e2
    move-result-object v12

    #@2e3
    .line 339
    .restart local v12       #_arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2e5
    move-object/from16 v0, p2

    #@2e7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@2ea
    move-result-object v13

    #@2eb
    .line 341
    .restart local v13       #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2ed
    move-object/from16 v0, p2

    #@2ef
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@2f2
    move-result-object v14

    #@2f3
    .line 343
    .restart local v14       #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2f6
    move-result-object v8

    #@2f7
    .line 345
    .restart local v8       #_arg5:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2fa
    move-result v9

    #@2fb
    .line 347
    .restart local v9       #_arg6:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2fe
    move-result v10

    #@2ff
    .line 349
    .restart local v10       #_arg7:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@302
    move-result v11

    #@303
    .restart local v11       #_arg8:I
    move-object/from16 v15, p0

    #@305
    move-object/from16 v16, v3

    #@307
    move-object/from16 v17, v4

    #@309
    move-object/from16 v18, v12

    #@30b
    move-object/from16 v19, v13

    #@30d
    move-object/from16 v20, v14

    #@30f
    move-object/from16 v21, v8

    #@311
    move/from16 v22, v9

    #@313
    move/from16 v23, v10

    #@315
    move/from16 v24, v11

    #@317
    .line 350
    invoke-virtual/range {v15 .. v24}, Lcom/android/internal/telephony/ISms$Stub;->sendMultipartTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;III)V

    #@31a
    .line 351
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@31d
    .line 352
    const/4 v2, 0x1

    #@31e
    goto/16 :goto_7

    #@320
    .line 356
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v8           #_arg5:Ljava/lang/String;
    .end local v9           #_arg6:I
    .end local v10           #_arg7:I
    .end local v11           #_arg8:I
    .end local v12           #_arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v13           #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .end local v14           #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    :sswitch_320
    const-string v2, "com.android.internal.telephony.ISms"

    #@322
    move-object/from16 v0, p2

    #@324
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@327
    .line 358
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@32a
    move-result-object v3

    #@32b
    .line 360
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@32e
    move-result-object v4

    #@32f
    .line 362
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    #@332
    move-result-object v12

    #@333
    .line 364
    .restart local v12       #_arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@335
    move-object/from16 v0, p2

    #@337
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@33a
    move-result-object v13

    #@33b
    .line 366
    .restart local v13       #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@33d
    move-object/from16 v0, p2

    #@33f
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@342
    move-result-object v14

    #@343
    .line 368
    .restart local v14       #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@346
    move-result-object v8

    #@347
    .line 370
    .restart local v8       #_arg5:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@34a
    move-result v9

    #@34b
    .line 372
    .restart local v9       #_arg6:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@34e
    move-result v10

    #@34f
    .line 374
    .restart local v10       #_arg7:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@352
    move-result v11

    #@353
    .restart local v11       #_arg8:I
    move-object/from16 v15, p0

    #@355
    move-object/from16 v16, v3

    #@357
    move-object/from16 v17, v4

    #@359
    move-object/from16 v18, v12

    #@35b
    move-object/from16 v19, v13

    #@35d
    move-object/from16 v20, v14

    #@35f
    move-object/from16 v21, v8

    #@361
    move/from16 v22, v9

    #@363
    move/from16 v23, v10

    #@365
    move/from16 v24, v11

    #@367
    .line 375
    invoke-virtual/range {v15 .. v24}, Lcom/android/internal/telephony/ISms$Stub;->sendMultipartTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;III)V

    #@36a
    .line 376
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@36d
    .line 377
    const/4 v2, 0x1

    #@36e
    goto/16 :goto_7

    #@370
    .line 381
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v8           #_arg5:Ljava/lang/String;
    .end local v9           #_arg6:I
    .end local v10           #_arg7:I
    .end local v11           #_arg8:I
    .end local v12           #_arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v13           #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .end local v14           #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    :sswitch_370
    const-string v2, "com.android.internal.telephony.ISms"

    #@372
    move-object/from16 v0, p2

    #@374
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@377
    .line 383
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@37a
    move-result-object v3

    #@37b
    .line 385
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@37e
    move-result-object v4

    #@37f
    .line 387
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@382
    move-result-object v5

    #@383
    .line 389
    .restart local v5       #_arg2:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@386
    move-result v2

    #@387
    if-eqz v2, :cond_3be

    #@389
    .line 390
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@38b
    move-object/from16 v0, p2

    #@38d
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@390
    move-result-object v6

    #@391
    check-cast v6, Landroid/app/PendingIntent;

    #@393
    .line 396
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    :goto_393
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@396
    move-result v2

    #@397
    if-eqz v2, :cond_3c0

    #@399
    .line 397
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@39b
    move-object/from16 v0, p2

    #@39d
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3a0
    move-result-object v7

    #@3a1
    check-cast v7, Landroid/app/PendingIntent;

    #@3a3
    .line 403
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    :goto_3a3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3a6
    move-result-object v8

    #@3a7
    .line 405
    .restart local v8       #_arg5:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3aa
    move-result v9

    #@3ab
    .line 407
    .restart local v9       #_arg6:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3ae
    move-result v10

    #@3af
    .line 409
    .restart local v10       #_arg7:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3b2
    move-result v11

    #@3b3
    .restart local v11       #_arg8:I
    move-object/from16 v2, p0

    #@3b5
    .line 410
    invoke-virtual/range {v2 .. v11}, Lcom/android/internal/telephony/ISms$Stub;->sendTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@3b8
    .line 411
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3bb
    .line 412
    const/4 v2, 0x1

    #@3bc
    goto/16 :goto_7

    #@3be
    .line 393
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    .end local v8           #_arg5:Ljava/lang/String;
    .end local v9           #_arg6:I
    .end local v10           #_arg7:I
    .end local v11           #_arg8:I
    :cond_3be
    const/4 v6, 0x0

    #@3bf
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    goto :goto_393

    #@3c0
    .line 400
    :cond_3c0
    const/4 v7, 0x0

    #@3c1
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    goto :goto_3a3

    #@3c2
    .line 416
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v5           #_arg2:Ljava/lang/String;
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    :sswitch_3c2
    const-string v2, "com.android.internal.telephony.ISms"

    #@3c4
    move-object/from16 v0, p2

    #@3c6
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3c9
    .line 418
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3cc
    move-result v3

    #@3cd
    .line 419
    .local v3, _arg0:I
    move-object/from16 v0, p0

    #@3cf
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->enableCellBroadcast(I)Z

    #@3d2
    move-result v25

    #@3d3
    .line 420
    .restart local v25       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3d6
    .line 421
    if-eqz v25, :cond_3e1

    #@3d8
    const/4 v2, 0x1

    #@3d9
    :goto_3d9
    move-object/from16 v0, p3

    #@3db
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@3de
    .line 422
    const/4 v2, 0x1

    #@3df
    goto/16 :goto_7

    #@3e1
    .line 421
    :cond_3e1
    const/4 v2, 0x0

    #@3e2
    goto :goto_3d9

    #@3e3
    .line 426
    .end local v3           #_arg0:I
    .end local v25           #_result:Z
    :sswitch_3e3
    const-string v2, "com.android.internal.telephony.ISms"

    #@3e5
    move-object/from16 v0, p2

    #@3e7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ea
    .line 428
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3ed
    move-result v3

    #@3ee
    .line 429
    .restart local v3       #_arg0:I
    move-object/from16 v0, p0

    #@3f0
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->disableCellBroadcast(I)Z

    #@3f3
    move-result v25

    #@3f4
    .line 430
    .restart local v25       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f7
    .line 431
    if-eqz v25, :cond_402

    #@3f9
    const/4 v2, 0x1

    #@3fa
    :goto_3fa
    move-object/from16 v0, p3

    #@3fc
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@3ff
    .line 432
    const/4 v2, 0x1

    #@400
    goto/16 :goto_7

    #@402
    .line 431
    :cond_402
    const/4 v2, 0x0

    #@403
    goto :goto_3fa

    #@404
    .line 436
    .end local v3           #_arg0:I
    .end local v25           #_result:Z
    :sswitch_404
    const-string v2, "com.android.internal.telephony.ISms"

    #@406
    move-object/from16 v0, p2

    #@408
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40b
    .line 438
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@40e
    move-result v3

    #@40f
    .line 439
    .restart local v3       #_arg0:I
    move-object/from16 v0, p0

    #@411
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->enableCdmaBroadcast(I)Z

    #@414
    move-result v25

    #@415
    .line 440
    .restart local v25       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@418
    .line 441
    if-eqz v25, :cond_423

    #@41a
    const/4 v2, 0x1

    #@41b
    :goto_41b
    move-object/from16 v0, p3

    #@41d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@420
    .line 442
    const/4 v2, 0x1

    #@421
    goto/16 :goto_7

    #@423
    .line 441
    :cond_423
    const/4 v2, 0x0

    #@424
    goto :goto_41b

    #@425
    .line 446
    .end local v3           #_arg0:I
    .end local v25           #_result:Z
    :sswitch_425
    const-string v2, "com.android.internal.telephony.ISms"

    #@427
    move-object/from16 v0, p2

    #@429
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@42c
    .line 448
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@42f
    move-result v3

    #@430
    .line 449
    .restart local v3       #_arg0:I
    move-object/from16 v0, p0

    #@432
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->disableCdmaBroadcast(I)Z

    #@435
    move-result v25

    #@436
    .line 450
    .restart local v25       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@439
    .line 451
    if-eqz v25, :cond_444

    #@43b
    const/4 v2, 0x1

    #@43c
    :goto_43c
    move-object/from16 v0, p3

    #@43e
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@441
    .line 452
    const/4 v2, 0x1

    #@442
    goto/16 :goto_7

    #@444
    .line 451
    :cond_444
    const/4 v2, 0x0

    #@445
    goto :goto_43c

    #@446
    .line 456
    .end local v3           #_arg0:I
    .end local v25           #_result:Z
    :sswitch_446
    const-string v2, "com.android.internal.telephony.ISms"

    #@448
    move-object/from16 v0, p2

    #@44a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@44d
    .line 458
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@450
    move-result v3

    #@451
    .line 460
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@454
    move-result v4

    #@455
    .line 461
    .local v4, _arg1:I
    move-object/from16 v0, p0

    #@457
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/ISms$Stub;->enableCellBroadcastRange(II)Z

    #@45a
    move-result v25

    #@45b
    .line 462
    .restart local v25       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@45e
    .line 463
    if-eqz v25, :cond_469

    #@460
    const/4 v2, 0x1

    #@461
    :goto_461
    move-object/from16 v0, p3

    #@463
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@466
    .line 464
    const/4 v2, 0x1

    #@467
    goto/16 :goto_7

    #@469
    .line 463
    :cond_469
    const/4 v2, 0x0

    #@46a
    goto :goto_461

    #@46b
    .line 468
    .end local v3           #_arg0:I
    .end local v4           #_arg1:I
    .end local v25           #_result:Z
    :sswitch_46b
    const-string v2, "com.android.internal.telephony.ISms"

    #@46d
    move-object/from16 v0, p2

    #@46f
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@472
    .line 470
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@475
    move-result v3

    #@476
    .line 472
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@479
    move-result v4

    #@47a
    .line 473
    .restart local v4       #_arg1:I
    move-object/from16 v0, p0

    #@47c
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/ISms$Stub;->disableCellBroadcastRange(II)Z

    #@47f
    move-result v25

    #@480
    .line 474
    .restart local v25       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@483
    .line 475
    if-eqz v25, :cond_48e

    #@485
    const/4 v2, 0x1

    #@486
    :goto_486
    move-object/from16 v0, p3

    #@488
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@48b
    .line 476
    const/4 v2, 0x1

    #@48c
    goto/16 :goto_7

    #@48e
    .line 475
    :cond_48e
    const/4 v2, 0x0

    #@48f
    goto :goto_486

    #@490
    .line 480
    .end local v3           #_arg0:I
    .end local v4           #_arg1:I
    .end local v25           #_result:Z
    :sswitch_490
    const-string v2, "com.android.internal.telephony.ISms"

    #@492
    move-object/from16 v0, p2

    #@494
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@497
    .line 481
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/ISms$Stub;->isGsmMo()Z

    #@49a
    move-result v25

    #@49b
    .line 482
    .restart local v25       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@49e
    .line 483
    if-eqz v25, :cond_4a9

    #@4a0
    const/4 v2, 0x1

    #@4a1
    :goto_4a1
    move-object/from16 v0, p3

    #@4a3
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@4a6
    .line 484
    const/4 v2, 0x1

    #@4a7
    goto/16 :goto_7

    #@4a9
    .line 483
    :cond_4a9
    const/4 v2, 0x0

    #@4aa
    goto :goto_4a1

    #@4ab
    .line 488
    .end local v25           #_result:Z
    :sswitch_4ab
    const-string v2, "com.android.internal.telephony.ISms"

    #@4ad
    move-object/from16 v0, p2

    #@4af
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4b2
    .line 490
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4b5
    move-result v3

    #@4b6
    .line 492
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@4b9
    move-result-object v4

    #@4ba
    .line 494
    .local v4, _arg1:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@4bd
    move-result-object v5

    #@4be
    .line 496
    .local v5, _arg2:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4c1
    move-result v6

    #@4c2
    .line 497
    .local v6, _arg3:I
    move-object/from16 v0, p0

    #@4c4
    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/android/internal/telephony/ISms$Stub;->copySmsToIccEfPrivate(I[B[BI)I

    #@4c7
    move-result v25

    #@4c8
    .line 498
    .local v25, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4cb
    .line 499
    move-object/from16 v0, p3

    #@4cd
    move/from16 v1, v25

    #@4cf
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@4d2
    .line 500
    const/4 v2, 0x1

    #@4d3
    goto/16 :goto_7

    #@4d5
    .line 504
    .end local v3           #_arg0:I
    .end local v4           #_arg1:[B
    .end local v5           #_arg2:[B
    .end local v6           #_arg3:I
    .end local v25           #_result:I
    :sswitch_4d5
    const-string v2, "com.android.internal.telephony.ISms"

    #@4d7
    move-object/from16 v0, p2

    #@4d9
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4dc
    .line 505
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/ISms$Stub;->getAllMessagesFromIccEf3GPP()Ljava/util/List;

    #@4df
    move-result-object v26

    #@4e0
    .line 506
    .restart local v26       #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e3
    .line 507
    move-object/from16 v0, p3

    #@4e5
    move-object/from16 v1, v26

    #@4e7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@4ea
    .line 508
    const/4 v2, 0x1

    #@4eb
    goto/16 :goto_7

    #@4ed
    .line 512
    .end local v26           #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    :sswitch_4ed
    const-string v2, "com.android.internal.telephony.ISms"

    #@4ef
    move-object/from16 v0, p2

    #@4f1
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4f4
    .line 513
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/ISms$Stub;->getAllMessagesFromIccEf3GPP2()Ljava/util/List;

    #@4f7
    move-result-object v26

    #@4f8
    .line 514
    .restart local v26       #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4fb
    .line 515
    move-object/from16 v0, p3

    #@4fd
    move-object/from16 v1, v26

    #@4ff
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@502
    .line 516
    const/4 v2, 0x1

    #@503
    goto/16 :goto_7

    #@505
    .line 520
    .end local v26           #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    :sswitch_505
    const-string v2, "com.android.internal.telephony.ISms"

    #@507
    move-object/from16 v0, p2

    #@509
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@50c
    .line 522
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@50f
    move-result v3

    #@510
    .line 523
    .restart local v3       #_arg0:I
    move-object/from16 v0, p0

    #@512
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->setUiccType(I)V

    #@515
    .line 524
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@518
    .line 525
    const/4 v2, 0x1

    #@519
    goto/16 :goto_7

    #@51b
    .line 529
    .end local v3           #_arg0:I
    :sswitch_51b
    const-string v2, "com.android.internal.telephony.ISms"

    #@51d
    move-object/from16 v0, p2

    #@51f
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@522
    .line 530
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/ISms$Stub;->getUiccType()I

    #@525
    move-result v25

    #@526
    .line 531
    .restart local v25       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@529
    .line 532
    move-object/from16 v0, p3

    #@52b
    move/from16 v1, v25

    #@52d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@530
    .line 533
    const/4 v2, 0x1

    #@531
    goto/16 :goto_7

    #@533
    .line 537
    .end local v25           #_result:I
    :sswitch_533
    const-string v2, "com.android.internal.telephony.ISms"

    #@535
    move-object/from16 v0, p2

    #@537
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@53a
    .line 539
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@53d
    move-result v3

    #@53e
    .line 540
    .restart local v3       #_arg0:I
    move-object/from16 v0, p0

    #@540
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->setSmsPriority(I)V

    #@543
    .line 541
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@546
    .line 542
    const/4 v2, 0x1

    #@547
    goto/16 :goto_7

    #@549
    .line 546
    .end local v3           #_arg0:I
    :sswitch_549
    const-string v2, "com.android.internal.telephony.ISms"

    #@54b
    move-object/from16 v0, p2

    #@54d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@550
    .line 548
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@553
    move-result v3

    #@554
    .line 550
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@557
    move-result v4

    #@558
    .line 551
    .local v4, _arg1:I
    move-object/from16 v0, p0

    #@55a
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/ISms$Stub;->enableCdmaBroadcastRange(II)Z

    #@55d
    move-result v25

    #@55e
    .line 552
    .local v25, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@561
    .line 553
    if-eqz v25, :cond_56c

    #@563
    const/4 v2, 0x1

    #@564
    :goto_564
    move-object/from16 v0, p3

    #@566
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@569
    .line 554
    const/4 v2, 0x1

    #@56a
    goto/16 :goto_7

    #@56c
    .line 553
    :cond_56c
    const/4 v2, 0x0

    #@56d
    goto :goto_564

    #@56e
    .line 558
    .end local v3           #_arg0:I
    .end local v4           #_arg1:I
    .end local v25           #_result:Z
    :sswitch_56e
    const-string v2, "com.android.internal.telephony.ISms"

    #@570
    move-object/from16 v0, p2

    #@572
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@575
    .line 560
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@578
    move-result v3

    #@579
    .line 562
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@57c
    move-result v4

    #@57d
    .line 563
    .restart local v4       #_arg1:I
    move-object/from16 v0, p0

    #@57f
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/ISms$Stub;->disableCdmaBroadcastRange(II)Z

    #@582
    move-result v25

    #@583
    .line 564
    .restart local v25       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@586
    .line 565
    if-eqz v25, :cond_591

    #@588
    const/4 v2, 0x1

    #@589
    :goto_589
    move-object/from16 v0, p3

    #@58b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@58e
    .line 566
    const/4 v2, 0x1

    #@58f
    goto/16 :goto_7

    #@591
    .line 565
    :cond_591
    const/4 v2, 0x0

    #@592
    goto :goto_589

    #@593
    .line 570
    .end local v3           #_arg0:I
    .end local v4           #_arg1:I
    .end local v25           #_result:Z
    :sswitch_593
    const-string v2, "com.android.internal.telephony.ISms"

    #@595
    move-object/from16 v0, p2

    #@597
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@59a
    .line 572
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@59d
    move-result-object v3

    #@59e
    .line 573
    .local v3, _arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@5a0
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->getPremiumSmsPermission(Ljava/lang/String;)I

    #@5a3
    move-result v25

    #@5a4
    .line 574
    .local v25, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5a7
    .line 575
    move-object/from16 v0, p3

    #@5a9
    move/from16 v1, v25

    #@5ab
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5ae
    .line 576
    const/4 v2, 0x1

    #@5af
    goto/16 :goto_7

    #@5b1
    .line 580
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v25           #_result:I
    :sswitch_5b1
    const-string v2, "com.android.internal.telephony.ISms"

    #@5b3
    move-object/from16 v0, p2

    #@5b5
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5b8
    .line 582
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5bb
    move-result-object v3

    #@5bc
    .line 584
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5bf
    move-result v4

    #@5c0
    .line 585
    .restart local v4       #_arg1:I
    move-object/from16 v0, p0

    #@5c2
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/ISms$Stub;->setPremiumSmsPermission(Ljava/lang/String;I)V

    #@5c5
    .line 586
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c8
    .line 587
    const/4 v2, 0x1

    #@5c9
    goto/16 :goto_7

    #@5cb
    .line 591
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:I
    :sswitch_5cb
    const-string v2, "com.android.internal.telephony.ISms"

    #@5cd
    move-object/from16 v0, p2

    #@5cf
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5d2
    .line 592
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/ISms$Stub;->isImsSmsSupported()Z

    #@5d5
    move-result v25

    #@5d6
    .line 593
    .local v25, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5d9
    .line 594
    if-eqz v25, :cond_5e4

    #@5db
    const/4 v2, 0x1

    #@5dc
    :goto_5dc
    move-object/from16 v0, p3

    #@5de
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@5e1
    .line 595
    const/4 v2, 0x1

    #@5e2
    goto/16 :goto_7

    #@5e4
    .line 594
    :cond_5e4
    const/4 v2, 0x0

    #@5e5
    goto :goto_5dc

    #@5e6
    .line 599
    .end local v25           #_result:Z
    :sswitch_5e6
    const-string v2, "com.android.internal.telephony.ISms"

    #@5e8
    move-object/from16 v0, p2

    #@5ea
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5ed
    .line 600
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/ISms$Stub;->getImsSmsFormat()Ljava/lang/String;

    #@5f0
    move-result-object v25

    #@5f1
    .line 601
    .local v25, _result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5f4
    .line 602
    move-object/from16 v0, p3

    #@5f6
    move-object/from16 v1, v25

    #@5f8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5fb
    .line 603
    const/4 v2, 0x1

    #@5fc
    goto/16 :goto_7

    #@5fe
    .line 607
    .end local v25           #_result:Ljava/lang/String;
    :sswitch_5fe
    const-string v2, "com.android.internal.telephony.ISms"

    #@600
    move-object/from16 v0, p2

    #@602
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@605
    .line 609
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@608
    move-result v3

    #@609
    .line 610
    .local v3, _arg0:I
    move-object/from16 v0, p0

    #@60b
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->setMultipartTextValidityPeriod(I)V

    #@60e
    .line 611
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@611
    .line 612
    const/4 v2, 0x1

    #@612
    goto/16 :goto_7

    #@614
    .line 616
    .end local v3           #_arg0:I
    :sswitch_614
    const-string v2, "com.android.internal.telephony.ISms"

    #@616
    move-object/from16 v0, p2

    #@618
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@61b
    .line 617
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/ISms$Stub;->getSmscenterAddress()Ljava/lang/String;

    #@61e
    move-result-object v25

    #@61f
    .line 618
    .restart local v25       #_result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@622
    .line 619
    move-object/from16 v0, p3

    #@624
    move-object/from16 v1, v25

    #@626
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@629
    .line 620
    const/4 v2, 0x1

    #@62a
    goto/16 :goto_7

    #@62c
    .line 624
    .end local v25           #_result:Ljava/lang/String;
    :sswitch_62c
    const-string v2, "com.android.internal.telephony.ISms"

    #@62e
    move-object/from16 v0, p2

    #@630
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@633
    .line 626
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@636
    move-result-object v3

    #@637
    .line 627
    .local v3, _arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@639
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->setSmscenterAddress(Ljava/lang/String;)Z

    #@63c
    move-result v25

    #@63d
    .line 628
    .local v25, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@640
    .line 629
    if-eqz v25, :cond_64b

    #@642
    const/4 v2, 0x1

    #@643
    :goto_643
    move-object/from16 v0, p3

    #@645
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@648
    .line 630
    const/4 v2, 0x1

    #@649
    goto/16 :goto_7

    #@64b
    .line 629
    :cond_64b
    const/4 v2, 0x0

    #@64c
    goto :goto_643

    #@64d
    .line 634
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v25           #_result:Z
    :sswitch_64d
    const-string v2, "com.android.internal.telephony.ISms"

    #@64f
    move-object/from16 v0, p2

    #@651
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@654
    .line 635
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/ISms$Stub;->getMaxEfSms()I

    #@657
    move-result v25

    #@658
    .line 636
    .local v25, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@65b
    .line 637
    move-object/from16 v0, p3

    #@65d
    move/from16 v1, v25

    #@65f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@662
    .line 638
    const/4 v2, 0x1

    #@663
    goto/16 :goto_7

    #@665
    .line 642
    .end local v25           #_result:I
    :sswitch_665
    const-string v2, "com.android.internal.telephony.ISms"

    #@667
    move-object/from16 v0, p2

    #@669
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@66c
    .line 644
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@66f
    move-result v3

    #@670
    .line 646
    .local v3, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@673
    move-result v2

    #@674
    if-eqz v2, :cond_68b

    #@676
    const/4 v4, 0x1

    #@677
    .line 647
    .local v4, _arg1:Z
    :goto_677
    move-object/from16 v0, p0

    #@679
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/ISms$Stub;->updateSmsOnSimReadStatus(IZ)Z

    #@67c
    move-result v25

    #@67d
    .line 648
    .local v25, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@680
    .line 649
    if-eqz v25, :cond_68d

    #@682
    const/4 v2, 0x1

    #@683
    :goto_683
    move-object/from16 v0, p3

    #@685
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@688
    .line 650
    const/4 v2, 0x1

    #@689
    goto/16 :goto_7

    #@68b
    .line 646
    .end local v4           #_arg1:Z
    .end local v25           #_result:Z
    :cond_68b
    const/4 v4, 0x0

    #@68c
    goto :goto_677

    #@68d
    .line 649
    .restart local v4       #_arg1:Z
    .restart local v25       #_result:Z
    :cond_68d
    const/4 v2, 0x0

    #@68e
    goto :goto_683

    #@68f
    .line 654
    .end local v3           #_arg0:I
    .end local v4           #_arg1:Z
    .end local v25           #_result:Z
    :sswitch_68f
    const-string v2, "com.android.internal.telephony.ISms"

    #@691
    move-object/from16 v0, p2

    #@693
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@696
    .line 656
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@699
    move-result v3

    #@69a
    .line 658
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@69d
    move-result-object v4

    #@69e
    .line 660
    .local v4, _arg1:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@6a1
    move-result-object v5

    #@6a2
    .line 661
    .restart local v5       #_arg2:[B
    move-object/from16 v0, p0

    #@6a4
    invoke-virtual {v0, v3, v4, v5}, Lcom/android/internal/telephony/ISms$Stub;->copySmsToIccEf(I[B[B)I

    #@6a7
    move-result v25

    #@6a8
    .line 662
    .local v25, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6ab
    .line 663
    move-object/from16 v0, p3

    #@6ad
    move/from16 v1, v25

    #@6af
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6b2
    .line 664
    const/4 v2, 0x1

    #@6b3
    goto/16 :goto_7

    #@6b5
    .line 668
    .end local v3           #_arg0:I
    .end local v4           #_arg1:[B
    .end local v5           #_arg2:[B
    .end local v25           #_result:I
    :sswitch_6b5
    const-string v2, "com.android.internal.telephony.ISms"

    #@6b7
    move-object/from16 v0, p2

    #@6b9
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6bc
    .line 669
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/ISms$Stub;->isFdnEnabled()Z

    #@6bf
    move-result v25

    #@6c0
    .line 670
    .local v25, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6c3
    .line 671
    if-eqz v25, :cond_6ce

    #@6c5
    const/4 v2, 0x1

    #@6c6
    :goto_6c6
    move-object/from16 v0, p3

    #@6c8
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@6cb
    .line 672
    const/4 v2, 0x1

    #@6cc
    goto/16 :goto_7

    #@6ce
    .line 671
    :cond_6ce
    const/4 v2, 0x0

    #@6cf
    goto :goto_6c6

    #@6d0
    .line 676
    .end local v25           #_result:Z
    :sswitch_6d0
    const-string v2, "com.android.internal.telephony.ISms"

    #@6d2
    move-object/from16 v0, p2

    #@6d4
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6d7
    .line 678
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6da
    move-result v2

    #@6db
    if-eqz v2, :cond_6e9

    #@6dd
    const/4 v3, 0x1

    #@6de
    .line 679
    .local v3, _arg0:Z
    :goto_6de
    move-object/from16 v0, p0

    #@6e0
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ISms$Stub;->setSmsIsRoaming(Z)V

    #@6e3
    .line 680
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e6
    .line 681
    const/4 v2, 0x1

    #@6e7
    goto/16 :goto_7

    #@6e9
    .line 678
    .end local v3           #_arg0:Z
    :cond_6e9
    const/4 v3, 0x0

    #@6ea
    goto :goto_6de

    #@6eb
    .line 685
    :sswitch_6eb
    const-string v2, "com.android.internal.telephony.ISms"

    #@6ed
    move-object/from16 v0, p2

    #@6ef
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6f2
    .line 687
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6f5
    move-result-object v3

    #@6f6
    .line 689
    .local v3, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6f9
    move-result v4

    #@6fa
    .line 690
    .local v4, _arg1:I
    move-object/from16 v0, p0

    #@6fc
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/ISms$Stub;->startTestCase(Ljava/lang/String;I)V

    #@6ff
    .line 691
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@702
    .line 692
    const/4 v2, 0x1

    #@703
    goto/16 :goto_7

    #@705
    .line 51
    nop

    #@706
    :sswitch_data_706
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_28
        0x3 -> :sswitch_45
        0x4 -> :sswitch_6d
        0x5 -> :sswitch_9a
        0x6 -> :sswitch_c3
        0x7 -> :sswitch_109
        0x8 -> :sswitch_11f
        0x9 -> :sswitch_161
        0xa -> :sswitch_1a7
        0xb -> :sswitch_1e9
        0xc -> :sswitch_219
        0xd -> :sswitch_24e
        0xe -> :sswitch_27e
        0xf -> :sswitch_2d0
        0x10 -> :sswitch_320
        0x11 -> :sswitch_370
        0x12 -> :sswitch_3c2
        0x13 -> :sswitch_3e3
        0x14 -> :sswitch_404
        0x15 -> :sswitch_425
        0x16 -> :sswitch_446
        0x17 -> :sswitch_46b
        0x18 -> :sswitch_490
        0x19 -> :sswitch_4ab
        0x1a -> :sswitch_4d5
        0x1b -> :sswitch_4ed
        0x1c -> :sswitch_505
        0x1d -> :sswitch_51b
        0x1e -> :sswitch_533
        0x1f -> :sswitch_549
        0x20 -> :sswitch_56e
        0x21 -> :sswitch_593
        0x22 -> :sswitch_5b1
        0x23 -> :sswitch_5cb
        0x24 -> :sswitch_5e6
        0x25 -> :sswitch_5fe
        0x26 -> :sswitch_614
        0x27 -> :sswitch_62c
        0x28 -> :sswitch_64d
        0x29 -> :sswitch_665
        0x2a -> :sswitch_68f
        0x2b -> :sswitch_6b5
        0x2c -> :sswitch_6d0
        0x2d -> :sswitch_6eb
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
