.class public Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;
.super Ljava/lang/Object;
.source "SecurityManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/SecurityManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SIMLockInformtion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;
    }
.end annotation


# instance fields
.field public corporate:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

.field public country:I

.field public network:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

.field public networksubset:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

.field public operator:I

.field public serviceprovider:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

.field public sim:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

.field final synthetic this$0:Lcom/android/internal/telephony/SecurityManager;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/SecurityManager;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 74
    iput-object p1, p0, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->this$0:Lcom/android/internal/telephony/SecurityManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 86
    new-instance v0, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@7
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;-><init>(Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;)V

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->network:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@c
    .line 87
    new-instance v0, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@e
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;-><init>(Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;)V

    #@11
    iput-object v0, p0, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->networksubset:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@13
    .line 88
    new-instance v0, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@15
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;-><init>(Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;)V

    #@18
    iput-object v0, p0, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->serviceprovider:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@1a
    .line 89
    new-instance v0, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@1c
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;-><init>(Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;)V

    #@1f
    iput-object v0, p0, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->corporate:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@21
    .line 90
    new-instance v0, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@23
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;-><init>(Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;)V

    #@26
    iput-object v0, p0, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->sim:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@28
    return-void
.end method
