.class public final Lcom/android/internal/telephony/uicc/SIMFileHandler;
.super Lcom/android/internal/telephony/uicc/IccFileHandler;
.source "SIMFileHandler.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccConstants;


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "GSM"


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 4
    .parameter "app"
    .parameter "aid"
    .parameter "ci"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/uicc/IccFileHandler;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;Lcom/android/internal/telephony/CommandsInterface;)V

    #@3
    .line 36
    return-void
.end method


# virtual methods
.method protected getEFPath(I)Ljava/lang/String;
    .registers 5
    .parameter "efid"

    #@0
    .prologue
    .line 44
    sparse-switch p1, :sswitch_data_24

    #@3
    .line 77
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/SIMFileHandler;->getCommonIccEFPath(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 78
    .local v0, path:Ljava/lang/String;
    if-nez v0, :cond_19

    #@9
    .line 79
    const-string v1, "GSM"

    #@b
    const-string v2, "Error: EF Path being returned in null"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 81
    const-string v1, "GSM"

    #@12
    const-string v2, "[LGE_UICC] EF Path sets default path as MF_SIM for GSM Card"

    #@14
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 82
    const-string v0, "3F00"

    #@19
    .line 85
    .end local v0           #path:Ljava/lang/String;
    :cond_19
    :goto_19
    return-object v0

    #@1a
    .line 50
    :sswitch_1a
    const-string v0, "3F007F10"

    #@1c
    goto :goto_19

    #@1d
    .line 65
    :sswitch_1d
    const-string v0, "3F007F20"

    #@1f
    goto :goto_19

    #@20
    .line 75
    :sswitch_20
    const-string v0, "3F007F20"

    #@22
    goto :goto_19

    #@23
    .line 44
    nop

    #@24
    :sswitch_data_24
    .sparse-switch
        0x6f07 -> :sswitch_1d
        0x6f11 -> :sswitch_20
        0x6f13 -> :sswitch_20
        0x6f14 -> :sswitch_20
        0x6f15 -> :sswitch_20
        0x6f16 -> :sswitch_20
        0x6f17 -> :sswitch_20
        0x6f18 -> :sswitch_20
        0x6f38 -> :sswitch_1d
        0x6f3b -> :sswitch_1a
        0x6f3c -> :sswitch_1a
        0x6f3e -> :sswitch_20
        0x6f40 -> :sswitch_1a
        0x6f42 -> :sswitch_1a
        0x6f46 -> :sswitch_1d
        0x6f49 -> :sswitch_1a
        0x6f60 -> :sswitch_1d
        0x6fad -> :sswitch_1d
        0x6fc5 -> :sswitch_1d
        0x6fc6 -> :sswitch_1d
        0x6fc7 -> :sswitch_1d
        0x6fc8 -> :sswitch_1d
        0x6fc9 -> :sswitch_1d
        0x6fca -> :sswitch_1d
        0x6fcb -> :sswitch_1d
        0x6fcd -> :sswitch_1d
    .end sparse-switch
.end method

.method protected logd(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 90
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[SIMFileHandler] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 91
    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 95
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[SIMFileHandler] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 96
    return-void
.end method
