.class Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;
.super Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;
.source "LgeVoiceProtectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LgeVoiceProtectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActiveState"
.end annotation


# instance fields
.field mFDTriggered:Z

.field mIdleDuration:I

.field final synthetic this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)V
    .registers 4
    .parameter

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 348
    iput-object p1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@3
    const/4 v0, 0x0

    #@4
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;-><init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;)V

    #@7
    .line 350
    iput v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->mIdleDuration:I

    #@9
    .line 351
    iput-boolean v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->mFDTriggered:Z

    #@b
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 348
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;-><init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)V

    #@3
    return-void
.end method

.method private triggerDormant()Z
    .registers 9

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 355
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@4
    invoke-virtual {v4}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->isUMTS()Z

    #@7
    move-result v4

    #@8
    if-nez v4, :cond_12

    #@a
    .line 356
    iget-object v3, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@c
    const-string v4, "triggerDormant(): skip triggering since the attached network is non-3G"

    #@e
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@11
    .line 373
    :goto_11
    return v2

    #@12
    .line 360
    :cond_12
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@14
    invoke-static {v4}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$700(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)Ljava/lang/Object;

    #@17
    move-result-object v4

    #@18
    if-eqz v4, :cond_22

    #@1a
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@1c
    invoke-static {v4}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$800(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)Ljava/lang/reflect/Method;

    #@1f
    move-result-object v4

    #@20
    if-nez v4, :cond_2a

    #@22
    .line 361
    :cond_22
    iget-object v3, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@24
    const-string v4, "triggerDormant(): skip triggering since mQcRilHookObject or mQcRilGoDormantFunc is null"

    #@26
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@29
    goto :goto_11

    #@2a
    .line 365
    :cond_2a
    iget-object v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@2c
    const-string v4, "triggerDormant() using qcRilGoDormant with GO_DORMANT_INTERFACE_ALL"

    #@2e
    invoke-static {v2, v4}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@31
    .line 368
    :try_start_31
    iget-object v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@33
    invoke-static {v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$800(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)Ljava/lang/reflect/Method;

    #@36
    move-result-object v2

    #@37
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@39
    invoke-static {v4}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$700(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)Ljava/lang/Object;

    #@3c
    move-result-object v4

    #@3d
    const/4 v5, 0x1

    #@3e
    new-array v5, v5, [Ljava/lang/Object;

    #@40
    const/4 v6, 0x0

    #@41
    const-string v7, ""

    #@43
    aput-object v7, v5, v6

    #@45
    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@48
    move-result-object v1

    #@49
    check-cast v1, Ljava/lang/Boolean;

    #@4b
    .line 369
    .local v1, ret:Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_4e
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_4e} :catch_50

    #@4e
    move-result v2

    #@4f
    goto :goto_11

    #@50
    .line 370
    .end local v1           #ret:Ljava/lang/Boolean;
    :catch_50
    move-exception v0

    #@51
    .line 371
    .local v0, e:Ljava/lang/Exception;
    iget-object v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@53
    new-instance v4, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v5, "triggerDormant(): Failed to call qcRilGoDormant() : "

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v4

    #@66
    invoke-static {v2, v4}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$900(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@69
    move v2, v3

    #@6a
    .line 373
    goto :goto_11
.end method


# virtual methods
.method public enter()V
    .registers 5

    #@0
    .prologue
    .line 384
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@2
    const-string v2, "Enter to Active State"

    #@4
    invoke-static {v1, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@7
    .line 386
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@9
    invoke-virtual {v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->isVpAllowed()Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_1a

    #@f
    .line 387
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@11
    iget-object v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@13
    invoke-static {v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$1000(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$1100(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/util/IState;)V

    #@1a
    .line 390
    :cond_1a
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@1c
    invoke-static {v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$1200(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)I

    #@1f
    move-result v1

    #@20
    and-int/lit8 v1, v1, 0x4

    #@22
    if-eqz v1, :cond_27

    #@24
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->triggerDormant()Z

    #@27
    .line 393
    :cond_27
    :try_start_27
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@29
    const-string v2, "Add Reject Target rule to vp_OUTPUT Chain."

    #@2b
    invoke-static {v1, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@2e
    .line 394
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@30
    iget-object v1, v1, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mNetManageService:Landroid/os/INetworkManagementService;

    #@32
    if-eqz v1, :cond_3c

    #@34
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@36
    iget-object v1, v1, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mNetManageService:Landroid/os/INetworkManagementService;

    #@38
    const/4 v2, 0x1

    #@39
    invoke-interface {v1, v2}, Landroid/os/INetworkManagementService;->setVoiceProtectionEnabled(Z)V
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_3c} :catch_3d

    #@3c
    .line 400
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .line 396
    :catch_3d
    move-exception v0

    #@3e
    .line 397
    .local v0, e:Ljava/lang/Exception;
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@40
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v3, "service.dropPacket exception = "

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$900(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@56
    .line 398
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@58
    iget-object v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@5a
    invoke-static {v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$1000(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;

    #@5d
    move-result-object v2

    #@5e
    invoke-static {v1, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$1300(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/util/IState;)V

    #@61
    goto :goto_3c
.end method

.method public exit()V
    .registers 5

    #@0
    .prologue
    .line 406
    :try_start_0
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@2
    const-string v2, "Add Reject Target rule from vp_OUTPUT Chain."

    #@4
    invoke-static {v1, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@7
    .line 407
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@9
    iget-object v1, v1, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mNetManageService:Landroid/os/INetworkManagementService;

    #@b
    if-eqz v1, :cond_15

    #@d
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@f
    iget-object v1, v1, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mNetManageService:Landroid/os/INetworkManagementService;

    #@11
    const/4 v2, 0x0

    #@12
    invoke-interface {v1, v2}, Landroid/os/INetworkManagementService;->setVoiceProtectionEnabled(Z)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_15} :catch_1d

    #@15
    .line 414
    :cond_15
    :goto_15
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@17
    const-string v2, "Exit from Active State"

    #@19
    invoke-static {v1, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@1c
    .line 415
    return-void

    #@1d
    .line 409
    :catch_1d
    move-exception v0

    #@1e
    .line 410
    .local v0, e:Ljava/lang/Exception;
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "service.dropPacket exception = "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v1, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$900(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@36
    .line 411
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@38
    iget-object v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@3a
    invoke-static {v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$1000(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;

    #@3d
    move-result-object v2

    #@3e
    invoke-static {v1, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$1400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/util/IState;)V

    #@41
    goto :goto_15
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 422
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_1c

    #@5
    .line 430
    invoke-super {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;->processMessage(Landroid/os/Message;)Z

    #@8
    .line 435
    :goto_8
    const/4 v0, 0x1

    #@9
    return v0

    #@a
    .line 425
    :pswitch_a
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@c
    const-string v1, "Go dormant response received"

    #@e
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$300(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@11
    .line 426
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@13
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@15
    check-cast v0, Landroid/os/AsyncResult;

    #@17
    invoke-virtual {v1, v0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->onGoDormantReponse(Landroid/os/AsyncResult;)V

    #@1a
    goto :goto_8

    #@1b
    .line 422
    nop

    #@1c
    :pswitch_data_1c
    .packed-switch 0x25cec
        :pswitch_a
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 379
    const-string v0, "[Active] "

    #@2
    return-object v0
.end method
