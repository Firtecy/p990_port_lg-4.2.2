.class Lcom/android/internal/telephony/test/CallInfo;
.super Ljava/lang/Object;
.source "SimulatedGsmCallState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/test/CallInfo$State;
    }
.end annotation


# instance fields
.field TOA:I

.field isMT:Z

.field isMpty:Z

.field number:Ljava/lang/String;

.field state:Lcom/android/internal/telephony/test/CallInfo$State;


# direct methods
.method constructor <init>(ZLcom/android/internal/telephony/test/CallInfo$State;ZLjava/lang/String;)V
    .registers 7
    .parameter "isMT"
    .parameter "state"
    .parameter "isMpty"
    .parameter "number"

    #@0
    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    iput-boolean p1, p0, Lcom/android/internal/telephony/test/CallInfo;->isMT:Z

    #@5
    .line 53
    iput-object p2, p0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@7
    .line 54
    iput-boolean p3, p0, Lcom/android/internal/telephony/test/CallInfo;->isMpty:Z

    #@9
    .line 55
    iput-object p4, p0, Lcom/android/internal/telephony/test/CallInfo;->number:Ljava/lang/String;

    #@b
    .line 57
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    #@e
    move-result v0

    #@f
    if-lez v0, :cond_1f

    #@11
    const/4 v0, 0x0

    #@12
    invoke-virtual {p4, v0}, Ljava/lang/String;->charAt(I)C

    #@15
    move-result v0

    #@16
    const/16 v1, 0x2b

    #@18
    if-ne v0, v1, :cond_1f

    #@1a
    .line 58
    const/16 v0, 0x91

    #@1c
    iput v0, p0, Lcom/android/internal/telephony/test/CallInfo;->TOA:I

    #@1e
    .line 62
    :goto_1e
    return-void

    #@1f
    .line 60
    :cond_1f
    const/16 v0, 0x81

    #@21
    iput v0, p0, Lcom/android/internal/telephony/test/CallInfo;->TOA:I

    #@23
    goto :goto_1e
.end method

.method static createIncomingCall(Ljava/lang/String;)Lcom/android/internal/telephony/test/CallInfo;
    .registers 5
    .parameter "number"

    #@0
    .prologue
    .line 71
    new-instance v0, Lcom/android/internal/telephony/test/CallInfo;

    #@2
    const/4 v1, 0x1

    #@3
    sget-object v2, Lcom/android/internal/telephony/test/CallInfo$State;->INCOMING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@5
    const/4 v3, 0x0

    #@6
    invoke-direct {v0, v1, v2, v3, p0}, Lcom/android/internal/telephony/test/CallInfo;-><init>(ZLcom/android/internal/telephony/test/CallInfo$State;ZLjava/lang/String;)V

    #@9
    return-object v0
.end method

.method static createOutgoingCall(Ljava/lang/String;)Lcom/android/internal/telephony/test/CallInfo;
    .registers 4
    .parameter "number"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 66
    new-instance v0, Lcom/android/internal/telephony/test/CallInfo;

    #@3
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->DIALING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@5
    invoke-direct {v0, v2, v1, v2, p0}, Lcom/android/internal/telephony/test/CallInfo;-><init>(ZLcom/android/internal/telephony/test/CallInfo$State;ZLjava/lang/String;)V

    #@8
    return-object v0
.end method


# virtual methods
.method isActiveOrHeld()Z
    .registers 3

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@2
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@4
    if-eq v0, v1, :cond_c

    #@6
    iget-object v0, p0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@8
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@a
    if-ne v0, v1, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method isConnecting()Z
    .registers 3

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@2
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->DIALING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@4
    if-eq v0, v1, :cond_c

    #@6
    iget-object v0, p0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@8
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->ALERTING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@a
    if-ne v0, v1, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method isRinging()Z
    .registers 3

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@2
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->INCOMING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@4
    if-eq v0, v1, :cond_c

    #@6
    iget-object v0, p0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@8
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->WAITING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@a
    if-ne v0, v1, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method toCLCCLine(I)Ljava/lang/String;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "+CLCC: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, ","

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget-boolean v0, p0, Lcom/android/internal/telephony/test/CallInfo;->isMT:Z

    #@17
    if-eqz v0, :cond_5c

    #@19
    const-string v0, "1"

    #@1b
    :goto_1b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    const-string v1, ","

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    iget-object v1, p0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@27
    invoke-virtual {v1}, Lcom/android/internal/telephony/test/CallInfo$State;->value()I

    #@2a
    move-result v1

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, ",0,"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    iget-boolean v0, p0, Lcom/android/internal/telephony/test/CallInfo;->isMpty:Z

    #@37
    if-eqz v0, :cond_5f

    #@39
    const-string v0, "1"

    #@3b
    :goto_3b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, ",\""

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    iget-object v1, p0, Lcom/android/internal/telephony/test/CallInfo;->number:Ljava/lang/String;

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    const-string v1, "\","

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    iget v1, p0, Lcom/android/internal/telephony/test/CallInfo;->TOA:I

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    return-object v0

    #@5c
    :cond_5c
    const-string v0, "0"

    #@5e
    goto :goto_1b

    #@5f
    :cond_5f
    const-string v0, "0"

    #@61
    goto :goto_3b
.end method

.method toDriverCall(I)Lcom/android/internal/telephony/DriverCall;
    .registers 6
    .parameter "index"

    #@0
    .prologue
    .line 87
    new-instance v1, Lcom/android/internal/telephony/DriverCall;

    #@2
    invoke-direct {v1}, Lcom/android/internal/telephony/DriverCall;-><init>()V

    #@5
    .line 89
    .local v1, ret:Lcom/android/internal/telephony/DriverCall;
    iput p1, v1, Lcom/android/internal/telephony/DriverCall;->index:I

    #@7
    .line 90
    iget-boolean v2, p0, Lcom/android/internal/telephony/test/CallInfo;->isMT:Z

    #@9
    iput-boolean v2, v1, Lcom/android/internal/telephony/DriverCall;->isMT:Z

    #@b
    .line 93
    :try_start_b
    iget-object v2, p0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@d
    invoke-virtual {v2}, Lcom/android/internal/telephony/test/CallInfo$State;->value()I

    #@10
    move-result v2

    #@11
    invoke-static {v2}, Lcom/android/internal/telephony/DriverCall;->stateFromCLCC(I)Lcom/android/internal/telephony/DriverCall$State;

    #@14
    move-result-object v2

    #@15
    iput-object v2, v1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;
    :try_end_17
    .catch Lcom/android/internal/telephony/ATParseEx; {:try_start_b .. :try_end_17} :catch_2a

    #@17
    .line 98
    iget-boolean v2, p0, Lcom/android/internal/telephony/test/CallInfo;->isMpty:Z

    #@19
    iput-boolean v2, v1, Lcom/android/internal/telephony/DriverCall;->isMpty:Z

    #@1b
    .line 99
    iget-object v2, p0, Lcom/android/internal/telephony/test/CallInfo;->number:Ljava/lang/String;

    #@1d
    iput-object v2, v1, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@1f
    .line 100
    iget v2, p0, Lcom/android/internal/telephony/test/CallInfo;->TOA:I

    #@21
    iput v2, v1, Lcom/android/internal/telephony/DriverCall;->TOA:I

    #@23
    .line 101
    const/4 v2, 0x1

    #@24
    iput-boolean v2, v1, Lcom/android/internal/telephony/DriverCall;->isVoice:Z

    #@26
    .line 102
    const/4 v2, 0x0

    #@27
    iput v2, v1, Lcom/android/internal/telephony/DriverCall;->als:I

    #@29
    .line 104
    return-object v1

    #@2a
    .line 94
    :catch_2a
    move-exception v0

    #@2b
    .line 95
    .local v0, ex:Lcom/android/internal/telephony/ATParseEx;
    new-instance v2, Ljava/lang/RuntimeException;

    #@2d
    const-string v3, "should never happen"

    #@2f
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@32
    throw v2
.end method
