.class public Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;
.super Ljava/lang/Object;
.source "PDPContextStateBroadcaster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/PDPContextStateBroadcaster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InstanceLock"
.end annotation


# static fields
.field private static sLockCount:I

.field private static sMutex:Ljava/lang/Object;


# instance fields
.field locked:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 97
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->sLockCount:I

    #@3
    .line 98
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    sput-object v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->sMutex:Ljava/lang/Object;

    #@a
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 70
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 100
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->locked:Z

    #@6
    .line 71
    sget-object v1, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->sMutex:Ljava/lang/Object;

    #@8
    monitor-enter v1

    #@9
    .line 72
    :try_start_9
    sget v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->sLockCount:I

    #@b
    if-nez v0, :cond_16

    #@d
    .line 73
    new-instance v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;

    #@f
    const/4 v2, 0x0

    #@10
    invoke-direct {v0, p1, v2}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;)V

    #@13
    invoke-static {v0}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->access$002(Lcom/android/internal/telephony/PDPContextStateBroadcaster;)Lcom/android/internal/telephony/PDPContextStateBroadcaster;

    #@16
    .line 75
    :cond_16
    const/4 v0, 0x1

    #@17
    iput-boolean v0, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->locked:Z

    #@19
    .line 76
    sget v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->sLockCount:I

    #@1b
    add-int/lit8 v0, v0, 0x1

    #@1d
    sput v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->sLockCount:I

    #@1f
    .line 77
    monitor-exit v1

    #@20
    .line 78
    return-void

    #@21
    .line 77
    :catchall_21
    move-exception v0

    #@22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_9 .. :try_end_23} :catchall_21

    #@23
    throw v0
.end method


# virtual methods
.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->unlock()V

    #@3
    .line 95
    return-void
.end method

.method public unlock()V
    .registers 3

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->locked:Z

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 83
    sget-object v1, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->sMutex:Ljava/lang/Object;

    #@6
    monitor-enter v1

    #@7
    .line 84
    :try_start_7
    sget v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->sLockCount:I

    #@9
    add-int/lit8 v0, v0, -0x1

    #@b
    sput v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->sLockCount:I

    #@d
    if-nez v0, :cond_13

    #@f
    .line 85
    const/4 v0, 0x0

    #@10
    invoke-static {v0}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->access$002(Lcom/android/internal/telephony/PDPContextStateBroadcaster;)Lcom/android/internal/telephony/PDPContextStateBroadcaster;

    #@13
    .line 87
    :cond_13
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;->locked:Z

    #@16
    .line 88
    monitor-exit v1

    #@17
    .line 90
    :cond_17
    return-void

    #@18
    .line 88
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_7 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method
