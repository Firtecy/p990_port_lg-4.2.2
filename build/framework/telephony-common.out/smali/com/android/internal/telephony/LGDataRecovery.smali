.class public Lcom/android/internal/telephony/LGDataRecovery;
.super Landroid/os/Handler;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;,
        Lcom/android/internal/telephony/LGDataRecovery$ThreadIPtable;,
        Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;,
        Lcom/android/internal/telephony/LGDataRecovery$DataLogFormatter;,
        Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;,
        Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;,
        Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;,
        Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;,
        Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;,
        Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;,
        Lcom/android/internal/telephony/LGDataRecovery$LinkState;,
        Lcom/android/internal/telephony/LGDataRecovery$RecoverySignType;,
        Lcom/android/internal/telephony/LGDataRecovery$DataStallSympton;,
        Lcom/android/internal/telephony/LGDataRecovery$RecoveryRoutine;
    }
.end annotation


# static fields
.field private static final AGGRESSIVE_DELAY_IN_MS:I = 0xea60

.field private static final CONNECTION_COMPLETE_CHECK_TIMER:I = 0x2710

.field protected static final CONN_COMPLETE_CHECK_ALARM_TAG_EXTRA:Ljava/lang/String; = "conn.check.alram.tag"

.field protected static final DATA_STALL_ALARM_TAG_EXTRA:Ljava/lang/String; = "data.stall.alram.tag"

.field protected static final DEFAULT_DUMP:Ljava/lang/String; = "lgdatadump.log"

.field protected static final DNSFAIL_DUMP:Ljava/lang/String; = "dnsfailhistory.log"

.field protected static final DNS_PROPERTY:Ljava/lang/String; = "net.dns1"

.field private static final EAI_ADDRFAMILY:I = 0x1

.field private static final EAI_AGAIN:I = 0x2

.field private static final EAI_BADFLAGS:I = 0x3

.field private static final EAI_BADHINTS:I = 0xc

.field private static final EAI_FAIL:I = 0x4

.field private static final EAI_FAMILY:I = 0x5

.field private static final EAI_MEMORY:I = 0x6

.field private static final EAI_NODATA:I = 0x7

.field private static final EAI_NONAME:I = 0x8

.field private static final EAI_OVERFLOW:I = 0xe

.field private static final EAI_PROTOCOL:I = 0xd

.field private static final EAI_SERVICE:I = 0x9

.field private static final EAI_SOCKTYPE:I = 0xa

.field private static final EAI_SYSTEM:I = 0xb

.field private static final EVENT_BASE:I = 0x42064

.field private static final EVENT_CONN_CHECK_ALARM_EXPIRED:I = 0x4206b

.field private static final EVENT_DATA_CONNECTION_ATTACHED:I = 0x4206e

.field private static final EVENT_DATA_CONNECTION_DETACHED:I = 0x4206f

.field private static final EVENT_DATA_SATE_CONNECTED:I = 0x42067

.field private static final EVENT_DATA_SATE_DISCONNECTED:I = 0x42068

.field private static final EVENT_DNS_FAIL_OBSERVED:I = 0x42069

.field private static final EVENT_GET_MODEM_PACKET_COUNT_DONE:I = 0x42072

.field private static final EVENT_PS_RESTRICT_DISABLED:I = 0x42071

.field private static final EVENT_PS_RESTRICT_ENABLED:I = 0x42070

.field private static final EVENT_SCREEN_OFF:I = 0x42066

.field private static final EVENT_SCREEN_ON:I = 0x42065

.field private static final EVENT_STALL_ALARM_EXPIRED:I = 0x4206a

.field private static final EVENT_VOICE_CALL_ENDED:I = 0x4206c

.field private static final EVENT_VOICE_CALL_STARTED:I = 0x4206d

.field private static final INTENT_IMS_CALL_STATE:Ljava/lang/String; = "com.lge.ims.action.CALL_STATE"

.field private static final INTENT_LGDATA_CONN_COMPLETE_CHECK_ALARM:Ljava/lang/String; = "com.lge.internal.telephony.lge-data-conn-check-alarm"

.field private static final INTENT_LGDATA_PULL_LOG:Ljava/lang/String; = "lg-data-pulllog"

.field private static final INTENT_LGDATA_STALL_ALARM:Ljava/lang/String; = "com.lge.internal.telephony.lge-data-stall-alarm"

.field protected static final IPTABLES_DUMP:Ljava/lang/String; = "iptablesinfo.log"

.field protected static final LGDR_LOG:Ljava/lang/String; = "LGDataRecoveryLog.log"

.field private static final LOG_TAG:Ljava/lang/String; = "GSM"

.field private static final MIN_TIME_DNS_STATCHECK:I = 0x6

.field protected static final NETINFO_DUMP:Ljava/lang/String; = "netinfo.log"

.field protected static final NETSTAT_LOG:Ljava/lang/String; = "netstat.log"

.field private static final NON_AGGRESSIVE_DELAY_IN_MS:I = 0x57e40

.field private static final NUMBER_SENT_PACKETS_OF_HANG:I = 0xa

.field protected static final ROUTE_INFO_URI:Ljava/lang/String; = "/proc/net/route"

.field protected static final STALL_INFO_FILE:Ljava/lang/String; = "datastallinfo.log"

.field protected static final SUSPECTED_SYMPTON_FOR_DATA_STALL:Ljava/lang/String; = "setting.lge-data-suspected-sympton"

.field protected static final V6ROUTE_INFO_URI:Ljava/lang/String; = "/proc/net/ipv6_route"

.field protected static final WIFI_INTERFACE:Ljava/lang/String; = "wlan0"

.field private static mCP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

.field private static mCpResponseFailCount:I

.field private static mCpResponseFlag:Z

.field private static sStatsService:Landroid/net/INetworkStatsService;


# instance fields
.field private DATA_STALL_NOT_SUSPECTED:Z

.field private final DBG:Z

.field private final DBG2:Z

.field isScreenOn:Z

.field private lastStatForDnsFail:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

.field private mConnectionInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCpResponseHandler:Landroid/os/Handler;

.field private mCpResponseHandlerThread:Landroid/os/HandlerThread;

.field private mDataStallAlarmIntent:Landroid/app/PendingIntent;

.field protected mDataStallAlarmTag:I

.field private mIMScall:Z

.field protected mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsPsRestricted:Z

.field private mIsWifiConnected:Z

.field private mNMService:Landroid/os/INetworkManagementService;

.field private mNetdObserverEx:Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;

.field private mNumOfDnsFailNoConn:I

.field mPhone:Lcom/android/internal/telephony/PhoneBase;

.field private mTime1stDnsfailNoConn:J

.field private mVoicecall:Z

.field private modemVendor:Ljava/lang/String;

.field private supportCPtrafficstat:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 67
    sput-object v1, Lcom/android/internal/telephony/LGDataRecovery;->sStatsService:Landroid/net/INetworkStatsService;

    #@4
    .line 82
    sput-object v1, Lcom/android/internal/telephony/LGDataRecovery;->mCP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@6
    .line 83
    sput-boolean v0, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseFlag:Z

    #@8
    .line 84
    sput v0, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseFailCount:I

    #@a
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/PhoneBase;)V
    .registers 8
    .parameter "phone"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 825
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@6
    .line 65
    iput-boolean v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->DBG:Z

    #@8
    .line 66
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->DBG2:Z

    #@a
    .line 69
    iput-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNetdObserverEx:Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;

    #@c
    .line 71
    new-instance v1, Ljava/util/ArrayList;

    #@e
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@11
    iput-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@13
    .line 72
    iput-boolean v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->supportCPtrafficstat:Z

    #@15
    .line 73
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIsWifiConnected:Z

    #@17
    .line 74
    iput-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->lastStatForDnsFail:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@19
    .line 75
    iput v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNumOfDnsFailNoConn:I

    #@1b
    .line 76
    const-wide/16 v1, 0x0

    #@1d
    iput-wide v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mTime1stDnsfailNoConn:J

    #@1f
    .line 77
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIsPsRestricted:Z

    #@21
    .line 78
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->mVoicecall:Z

    #@23
    .line 79
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIMScall:Z

    #@25
    .line 80
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->DATA_STALL_NOT_SUSPECTED:Z

    #@27
    .line 87
    iput-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@29
    .line 88
    iput-boolean v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->isScreenOn:Z

    #@2b
    .line 90
    iput-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@2d
    .line 91
    const-string v1, "QCT"

    #@2f
    iput-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->modemVendor:Ljava/lang/String;

    #@31
    .line 173
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@34
    move-result-wide v1

    #@35
    long-to-int v1, v1

    #@36
    iput v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmTag:I

    #@38
    .line 190
    new-instance v1, Lcom/android/internal/telephony/LGDataRecovery$1;

    #@3a
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/LGDataRecovery$1;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@3d
    iput-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@3f
    .line 827
    iput-object p1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@41
    .line 830
    new-instance v1, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@43
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@46
    sput-object v1, Lcom/android/internal/telephony/LGDataRecovery;->mCP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@48
    .line 832
    new-instance v1, Landroid/os/HandlerThread;

    #@4a
    const-string v2, "mCpResponseHandlerThread"

    #@4c
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@4f
    iput-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseHandlerThread:Landroid/os/HandlerThread;

    #@51
    .line 833
    iget-boolean v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->supportCPtrafficstat:Z

    #@53
    if-eqz v1, :cond_5a

    #@55
    .line 834
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseHandlerThread:Landroid/os/HandlerThread;

    #@57
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@5a
    .line 836
    :cond_5a
    new-instance v1, Lcom/android/internal/telephony/LGDataRecovery$2;

    #@5c
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseHandlerThread:Landroid/os/HandlerThread;

    #@5e
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@61
    move-result-object v2

    #@62
    invoke-direct {v1, p0, v2}, Lcom/android/internal/telephony/LGDataRecovery$2;-><init>(Lcom/android/internal/telephony/LGDataRecovery;Landroid/os/Looper;)V

    #@65
    iput-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseHandler:Landroid/os/Handler;

    #@67
    .line 855
    new-instance v0, Landroid/content/IntentFilter;

    #@69
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@6c
    .line 856
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.ANY_DATA_STATE"

    #@6e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@71
    .line 857
    const-string v1, "android.intent.action.SCREEN_ON"

    #@73
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@76
    .line 858
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@78
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@7b
    .line 859
    const-string v1, "android.net.wifi.STATE_CHANGE"

    #@7d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@80
    .line 860
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    #@82
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@85
    .line 861
    const-string v1, "com.lge.internal.telephony.lge-data-stall-alarm"

    #@87
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@8a
    .line 862
    const-string v1, "com.lge.internal.telephony.lge-data-conn-check-alarm"

    #@8c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@8f
    .line 863
    const-string v1, "lg-data-pulllog"

    #@91
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@94
    .line 864
    const-string v1, "com.lge.ims.action.CALL_STATE"

    #@96
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@99
    .line 865
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@9b
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@9e
    move-result-object v1

    #@9f
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@a1
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@a4
    .line 867
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a6
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@a9
    move-result-object v1

    #@aa
    if-eqz v1, :cond_c4

    #@ac
    .line 869
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ae
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@b1
    move-result-object v1

    #@b2
    const v2, 0x4206c

    #@b5
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/telephony/CallTracker;->registerForVoiceCallEnded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@b8
    .line 870
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ba
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@bd
    move-result-object v1

    #@be
    const v2, 0x4206d

    #@c1
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/telephony/CallTracker;->registerForVoiceCallStarted(Landroid/os/Handler;ILjava/lang/Object;)V

    #@c4
    .line 873
    :cond_c4
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@c6
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@c9
    move-result-object v1

    #@ca
    if-eqz v1, :cond_fc

    #@cc
    .line 875
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ce
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@d1
    move-result-object v1

    #@d2
    const v2, 0x4206e

    #@d5
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/telephony/ServiceStateTracker;->registerForDataConnectionAttached(Landroid/os/Handler;ILjava/lang/Object;)V

    #@d8
    .line 877
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@da
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@dd
    move-result-object v1

    #@de
    const v2, 0x4206f

    #@e1
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/telephony/ServiceStateTracker;->registerForDataConnectionDetached(Landroid/os/Handler;ILjava/lang/Object;)V

    #@e4
    .line 879
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@e6
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@e9
    move-result-object v1

    #@ea
    const v2, 0x42070

    #@ed
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/telephony/ServiceStateTracker;->registerForPsRestrictedEnabled(Landroid/os/Handler;ILjava/lang/Object;)V

    #@f0
    .line 881
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f2
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@f5
    move-result-object v1

    #@f6
    const v2, 0x42071

    #@f9
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/telephony/ServiceStateTracker;->registerForPsRestrictedDisabled(Landroid/os/Handler;ILjava/lang/Object;)V

    #@fc
    .line 885
    :cond_fc
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->startNetdListener()V

    #@ff
    .line 887
    const-string v1, "LGDataRecovery has created."

    #@101
    invoke-static {v1}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@104
    .line 889
    return-void
.end method

.method private Dumpinfo(JI)V
    .registers 14
    .parameter "Id"
    .parameter "symton"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 1613
    new-instance v5, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v6, "ID: "

    #@8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v5

    #@c
    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@f
    move-result-object v5

    #@10
    const-string v6, " Time: "

    #@12
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v5

    #@16
    const-string v6, "yyyy-MM-dd kk:mm:ss"

    #@18
    new-instance v7, Ljava/util/Date;

    #@1a
    invoke-direct {v7, p1, p2}, Ljava/util/Date;-><init>(J)V

    #@1d
    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    #@20
    move-result-wide v7

    #@21
    invoke-static {v6, v7, v8}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    const-string v6, " Symton: "

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    const-string v6, "lgdatadump.log"

    #@39
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 1615
    const-string v5, "<-- ConnectionInfos -->"

    #@3e
    const-string v6, "lgdatadump.log"

    #@40
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 1616
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@45
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@48
    move-result-object v3

    #@49
    .local v3, i$:Ljava/util/Iterator;
    :goto_49
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@4c
    move-result v5

    #@4d
    if-eqz v5, :cond_5f

    #@4f
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@52
    move-result-object v1

    #@53
    check-cast v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@55
    .line 1617
    .local v1, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    invoke-virtual {v1}, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->toString()Ljava/lang/String;

    #@58
    move-result-object v5

    #@59
    const-string v6, "lgdatadump.log"

    #@5b
    invoke-static {v5, v6, v9}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;Z)V

    #@5e
    goto :goto_49

    #@5f
    .line 1620
    .end local v1           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    :cond_5f
    const-string v5, "<-- ip rule show -->"

    #@61
    const-string v6, "lgdatadump.log"

    #@63
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@66
    .line 1621
    const-string v5, "ip rule show"

    #@68
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/LGDataRecovery;->getShellCmdResult(Ljava/lang/String;)Ljava/util/ArrayList;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@6f
    move-result-object v3

    #@70
    :goto_70
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@73
    move-result v5

    #@74
    if-eqz v5, :cond_82

    #@76
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@79
    move-result-object v4

    #@7a
    check-cast v4, Ljava/lang/String;

    #@7c
    .line 1622
    .local v4, line:Ljava/lang/String;
    const-string v5, "lgdatadump.log"

    #@7e
    invoke-static {v4, v5, v9}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;Z)V

    #@81
    goto :goto_70

    #@82
    .line 1625
    .end local v4           #line:Ljava/lang/String;
    :cond_82
    const-string v5, "<-- netcfg -->"

    #@84
    const-string v6, "lgdatadump.log"

    #@86
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@89
    .line 1626
    const-string v5, "netcfg"

    #@8b
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/LGDataRecovery;->getShellCmdResult(Ljava/lang/String;)Ljava/util/ArrayList;

    #@8e
    move-result-object v5

    #@8f
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@92
    move-result-object v3

    #@93
    :goto_93
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@96
    move-result v5

    #@97
    if-eqz v5, :cond_a5

    #@99
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@9c
    move-result-object v4

    #@9d
    check-cast v4, Ljava/lang/String;

    #@9f
    .line 1627
    .restart local v4       #line:Ljava/lang/String;
    const-string v5, "lgdatadump.log"

    #@a1
    invoke-static {v4, v5, v9}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;Z)V

    #@a4
    goto :goto_93

    #@a5
    .line 1630
    .end local v4           #line:Ljava/lang/String;
    :cond_a5
    const-string v5, "<-- proc/net/route -->"

    #@a7
    const-string v6, "lgdatadump.log"

    #@a9
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@ac
    .line 1631
    const-string v5, "proc/net/route"

    #@ae
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LGDataRecovery;->getFileSystemInfo(Ljava/lang/String;)Ljava/util/ArrayList;

    #@b1
    move-result-object v5

    #@b2
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@b5
    move-result-object v3

    #@b6
    :goto_b6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@b9
    move-result v5

    #@ba
    if-eqz v5, :cond_c8

    #@bc
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@bf
    move-result-object v4

    #@c0
    check-cast v4, Ljava/lang/String;

    #@c2
    .line 1632
    .restart local v4       #line:Ljava/lang/String;
    const-string v5, "lgdatadump.log"

    #@c4
    invoke-static {v4, v5, v9}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;Z)V

    #@c7
    goto :goto_b6

    #@c8
    .line 1635
    .end local v4           #line:Ljava/lang/String;
    :cond_c8
    const-string v5, "<-- proc/net/route by API -->"

    #@ca
    const-string v6, "lgdatadump.log"

    #@cc
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@cf
    .line 1636
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->loggingRouteInfos()V

    #@d2
    .line 1638
    const-string v5, "<-- proc/net/dev -->"

    #@d4
    const-string v6, "lgdatadump.log"

    #@d6
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@d9
    .line 1639
    const-string v5, "proc/net/dev"

    #@db
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LGDataRecovery;->getFileSystemInfo(Ljava/lang/String;)Ljava/util/ArrayList;

    #@de
    move-result-object v5

    #@df
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@e2
    move-result-object v3

    #@e3
    :goto_e3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@e6
    move-result v5

    #@e7
    if-eqz v5, :cond_f5

    #@e9
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@ec
    move-result-object v4

    #@ed
    check-cast v4, Ljava/lang/String;

    #@ef
    .line 1640
    .restart local v4       #line:Ljava/lang/String;
    const-string v5, "lgdatadump.log"

    #@f1
    invoke-static {v4, v5, v9}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;Z)V

    #@f4
    goto :goto_e3

    #@f5
    .line 1642
    .end local v4           #line:Ljava/lang/String;
    :cond_f5
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->findDefaultConnInfo()Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@f8
    move-result-object v1

    #@f9
    .line 1643
    .restart local v1       #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    if-eqz v1, :cond_14a

    #@fb
    .line 1644
    new-instance v5, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v6, "<-- proc/net/xt_quota/"

    #@102
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v5

    #@106
    iget-object v6, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@108
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v5

    #@10c
    const-string v6, " -->"

    #@10e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v5

    #@112
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@115
    move-result-object v5

    #@116
    const-string v6, "lgdatadump.log"

    #@118
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@11b
    .line 1645
    new-instance v5, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    const-string v6, "proc/net/xt_quota/"

    #@122
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v5

    #@126
    iget-object v6, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@128
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v5

    #@12c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12f
    move-result-object v5

    #@130
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LGDataRecovery;->getFileSystemInfo(Ljava/lang/String;)Ljava/util/ArrayList;

    #@133
    move-result-object v5

    #@134
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@137
    move-result-object v3

    #@138
    :goto_138
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@13b
    move-result v5

    #@13c
    if-eqz v5, :cond_14a

    #@13e
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@141
    move-result-object v4

    #@142
    check-cast v4, Ljava/lang/String;

    #@144
    .line 1647
    .restart local v4       #line:Ljava/lang/String;
    const-string v5, "lgdatadump.log"

    #@146
    invoke-static {v4, v5, v9}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;Z)V

    #@149
    goto :goto_138

    #@14a
    .line 1657
    .end local v4           #line:Ljava/lang/String;
    :cond_14a
    const/4 v5, 0x2

    #@14b
    if-ne p3, v5, :cond_1d8

    #@14d
    .line 1658
    new-instance v5, Ljava/lang/StringBuilder;

    #@14f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@152
    const-string v6, "ID: "

    #@154
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@157
    move-result-object v5

    #@158
    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v5

    #@15c
    const-string v6, " Time: "

    #@15e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v5

    #@162
    const-string v6, "yyyy-MM-dd kk:mm:ss"

    #@164
    new-instance v7, Ljava/util/Date;

    #@166
    invoke-direct {v7, p1, p2}, Ljava/util/Date;-><init>(J)V

    #@169
    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    #@16c
    move-result-wide v7

    #@16d
    invoke-static {v6, v7, v8}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    #@170
    move-result-object v6

    #@171
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v5

    #@175
    const-string v6, " Symton: "

    #@177
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v5

    #@17b
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v5

    #@17f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@182
    move-result-object v5

    #@183
    const-string v6, "iptablesinfo.log"

    #@185
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@188
    .line 1660
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@18a
    if-nez v5, :cond_198

    #@18c
    .line 1661
    const-string v5, "network_management"

    #@18e
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@191
    move-result-object v0

    #@192
    .line 1662
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@195
    move-result-object v5

    #@196
    iput-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@198
    .line 1666
    .end local v0           #b:Landroid/os/IBinder;
    :cond_198
    :try_start_198
    const-string v5, "\n"

    #@19a
    const-string v6, "iptablesinfo.log"

    #@19c
    const/4 v7, 0x0

    #@19d
    invoke-static {v5, v6, v7}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;Z)V

    #@1a0
    .line 1667
    const-string v5, "<-- iptables -L -->"

    #@1a2
    const-string v6, "iptablesinfo.log"

    #@1a4
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@1a7
    .line 1668
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@1a9
    const-string v6, "iptables -L"

    #@1ab
    invoke-interface {v5, v6}, Landroid/os/INetworkManagementService;->runShellCommand(Ljava/lang/String;)V

    #@1ae
    .line 1669
    const-string v5, "<-- iptables -t nat -L -->"

    #@1b0
    const-string v6, "iptablesinfo.log"

    #@1b2
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@1b5
    .line 1670
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@1b7
    const-string v6, "iptables -t nat -L"

    #@1b9
    invoke-interface {v5, v6}, Landroid/os/INetworkManagementService;->runShellCommand(Ljava/lang/String;)V

    #@1bc
    .line 1671
    const-string v5, "<-- iptables -t mangle -L -->"

    #@1be
    const-string v6, "iptablesinfo.log"

    #@1c0
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@1c3
    .line 1672
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@1c5
    const-string v6, "iptables -t mangle -L"

    #@1c7
    invoke-interface {v5, v6}, Landroid/os/INetworkManagementService;->runShellCommand(Ljava/lang/String;)V

    #@1ca
    .line 1673
    const-string v5, "<-- iptables -t raw -L -->"

    #@1cc
    const-string v6, "iptablesinfo.log"

    #@1ce
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@1d1
    .line 1674
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@1d3
    const-string v6, "iptables -t raw -L"

    #@1d5
    invoke-interface {v5, v6}, Landroid/os/INetworkManagementService;->runShellCommand(Ljava/lang/String;)V
    :try_end_1d8
    .catch Ljava/lang/Exception; {:try_start_198 .. :try_end_1d8} :catch_1d9

    #@1d8
    .line 1680
    :cond_1d8
    :goto_1d8
    return-void

    #@1d9
    .line 1676
    :catch_1d9
    move-exception v2

    #@1da
    .line 1677
    .local v2, e:Ljava/lang/Exception;
    const-string v5, "failed for logging iptables"

    #@1dc
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@1df
    goto :goto_1d8
.end method

.method static synthetic access$002(Lcom/android/internal/telephony/LGDataRecovery;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIsWifiConnected:Z

    #@2
    return p1
.end method

.method static synthetic access$102(Lcom/android/internal/telephony/LGDataRecovery;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIMScall:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/LGDataRecovery;)Landroid/os/INetworkManagementService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/android/internal/telephony/LGDataRecovery;Landroid/os/INetworkManagementService;)Landroid/os/INetworkManagementService;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@2
    return-object p1
.end method

.method static synthetic access$400(Ljava/lang/String;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    invoke-static {p0, p1}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/LGDataRecovery;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->loggingRouteInfos()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LGDataRecovery;->getShellCmdResult(Ljava/lang/String;)Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$700(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 62
    invoke-static {p0, p1, p2}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;Z)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LGDataRecovery;->isActive(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private doRecovery()V
    .registers 3

    #@0
    .prologue
    .line 735
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->getRecoveryAction()I

    #@3
    move-result v0

    #@4
    .line 736
    .local v0, action:I
    const/4 v1, 0x5

    #@5
    if-ge v0, v1, :cond_c

    #@7
    .line 737
    add-int/lit8 v1, v0, 0x1

    #@9
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LGDataRecovery;->putRecoveryAction(I)V

    #@c
    .line 739
    :cond_c
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/LGDataRecovery;->doRecovery(I)V

    #@f
    .line 740
    return-void
.end method

.method private doRecovery(I)V
    .registers 8
    .parameter "action"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    .line 743
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "doRecovery action = "

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@17
    .line 744
    packed-switch p1, :pswitch_data_134

    #@1a
    .line 788
    new-instance v2, Ljava/lang/RuntimeException;

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v4, "doRecovery: Invalid recoveryAction="

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@32
    throw v2

    #@33
    .line 746
    :pswitch_33
    const-string v2, "RECOVERY_ACTION:: PING_CHECK"

    #@35
    const-string v3, "datastallinfo.log"

    #@37
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 747
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3f
    move-result-object v1

    #@40
    .local v1, i$:Ljava/util/Iterator;
    :cond_40
    :goto_40
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@43
    move-result v2

    #@44
    if-eqz v2, :cond_10c

    #@46
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@49
    move-result-object v0

    #@4a
    check-cast v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@4c
    .line 748
    .local v0, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@4e
    sget-object v3, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@50
    if-ne v2, v3, :cond_40

    #@52
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@54
    sget-object v3, Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;->CONNECTED:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@56
    if-ne v2, v3, :cond_40

    #@58
    .line 750
    iget v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->recoverySign:I

    #@5a
    if-ne v2, v5, :cond_40

    #@5c
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@5e
    if-eqz v2, :cond_40

    #@60
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@62
    const-string v3, "default"

    #@64
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@67
    move-result v2

    #@68
    if-eqz v2, :cond_40

    #@6a
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@6c
    if-eqz v2, :cond_40

    #@6e
    .line 751
    new-instance v2, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v3, " RECOVERY_ACTION:: PING_CHECK with APN : "

    #@75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    iget-object v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@7b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@86
    .line 752
    new-instance v2, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;

    #@88
    iget-object v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@8a
    invoke-direct {v2, p0, v3}, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;-><init>(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;)V

    #@8d
    invoke-virtual {v2}, Lcom/android/internal/telephony/LGDataRecovery$ThreadPingToDnsServer;->start()V

    #@90
    goto :goto_40

    #@91
    .line 758
    .end local v0           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    .end local v1           #i$:Ljava/util/Iterator;
    :pswitch_91
    const-string v2, "RECOVERY_ACTION:: CLEANUP_CONN"

    #@93
    const-string v3, "datastallinfo.log"

    #@95
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@98
    .line 759
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@9a
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@9d
    move-result-object v1

    #@9e
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_9e
    :goto_9e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@a1
    move-result v2

    #@a2
    if-eqz v2, :cond_10c

    #@a4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a7
    move-result-object v0

    #@a8
    check-cast v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@aa
    .line 760
    .restart local v0       #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@ac
    sget-object v3, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@ae
    if-ne v2, v3, :cond_9e

    #@b0
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@b2
    sget-object v3, Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;->CONNECTED:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@b4
    if-ne v2, v3, :cond_9e

    #@b6
    .line 762
    iget v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->recoverySign:I

    #@b8
    if-ne v2, v5, :cond_9e

    #@ba
    .line 763
    new-instance v2, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v3, " RECOVERY_ACTION:: CLEANUP_CONN with APN : "

    #@c1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v2

    #@c5
    iget-object v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@c7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v2

    #@cb
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v2

    #@cf
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@d2
    .line 764
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d4
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@d6
    iget-object v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@d8
    const/4 v4, 0x1

    #@d9
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->cleanUpConnection(Ljava/lang/String;Z)V

    #@dc
    goto :goto_9e

    #@dd
    .line 770
    .end local v0           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    .end local v1           #i$:Ljava/util/Iterator;
    :pswitch_dd
    iget-boolean v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mVoicecall:Z

    #@df
    if-nez v2, :cond_e5

    #@e1
    iget-boolean v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIMScall:Z

    #@e3
    if-eqz v2, :cond_10d

    #@e5
    .line 771
    :cond_e5
    new-instance v2, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v3, "voice or mIMS call exist, Do not RADIO OFF mVoicecall:"

    #@ec
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v2

    #@f0
    iget-boolean v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mVoicecall:Z

    #@f2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v2

    #@f6
    const-string v3, " /mIMScall:"

    #@f8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v2

    #@fc
    iget-boolean v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIMScall:Z

    #@fe
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@101
    move-result-object v2

    #@102
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v2

    #@106
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@109
    .line 772
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/LGDataRecovery;->putRecoveryAction(I)V

    #@10c
    .line 792
    :cond_10c
    :goto_10c
    return-void

    #@10d
    .line 774
    :cond_10d
    const-string v2, "RECOVERY_ACTION:: RADIO_OFFON"

    #@10f
    const-string v3, "datastallinfo.log"

    #@111
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@114
    .line 775
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@116
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@118
    invoke-virtual {v2}, Lcom/android/internal/telephony/DataConnectionTracker;->restartRadio()V

    #@11b
    goto :goto_10c

    #@11c
    .line 779
    :pswitch_11c
    const-string v2, "RECOVERY_ACTION:: DCT_RESTART"

    #@11e
    const-string v3, "datastallinfo.log"

    #@120
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@123
    goto :goto_10c

    #@124
    .line 782
    :pswitch_124
    const-string v2, "RECOVERY_ACTION:: RILD_RESTART"

    #@126
    const-string v3, "datastallinfo.log"

    #@128
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@12b
    goto :goto_10c

    #@12c
    .line 785
    :pswitch_12c
    const-string v2, "RECOVERY_ACTION:: MODEM_RESTART"

    #@12e
    const-string v3, "datastallinfo.log"

    #@130
    invoke-static {v2, v3}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@133
    goto :goto_10c

    #@134
    .line 744
    :pswitch_data_134
    .packed-switch 0x0
        :pswitch_33
        :pswitch_91
        :pswitch_dd
        :pswitch_11c
        :pswitch_124
        :pswitch_12c
    .end packed-switch
.end method

.method private doStallCheckwithPktCnt()V
    .registers 14

    #@0
    .prologue
    .line 640
    const/4 v2, 0x0

    #@1
    .line 641
    .local v2, stallsympton:I
    iget-object v9, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@6
    move-result-object v1

    #@7
    .local v1, i$:Ljava/util/Iterator;
    :cond_7
    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@a
    move-result v9

    #@b
    if-eqz v9, :cond_246

    #@d
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@13
    .line 642
    .local v0, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    iget-object v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@15
    sget-object v10, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@17
    if-ne v9, v10, :cond_211

    #@19
    iget-object v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@1b
    sget-object v10, Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;->CONNECTED:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@1d
    if-ne v9, v10, :cond_211

    #@1f
    .line 643
    new-instance v9, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v10, " StallCheck iface = "

    #@26
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v9

    #@2a
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@2c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v9

    #@30
    const-string v10, " APN = "

    #@32
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v9

    #@36
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@38
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v9

    #@3c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v9

    #@40
    invoke-static {v9}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@43
    .line 644
    iget-object v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@45
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/LGDataRecovery;->isActive(Ljava/lang/String;)Z

    #@48
    move-result v9

    #@49
    if-eqz v9, :cond_1ea

    #@4b
    .line 645
    const/4 v9, 0x1

    #@4c
    iput v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->recoverySign:I

    #@4e
    .line 646
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LGDataRecovery;->updateTrafficStatforAP(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V

    #@51
    .line 647
    iget-boolean v9, p0, Lcom/android/internal/telephony/LGDataRecovery;->supportCPtrafficstat:Z

    #@53
    if-eqz v9, :cond_58

    #@55
    .line 648
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LGDataRecovery;->updateTrafficStatforCP(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V

    #@58
    .line 650
    :cond_58
    iget-object v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@5a
    iget-wide v9, v9, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@5c
    iget-object v11, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@5e
    iget-wide v11, v11, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@60
    sub-long/2addr v9, v11

    #@61
    const-wide/16 v11, 0x3e8

    #@63
    div-long v3, v9, v11

    #@65
    .line 651
    .local v3, timegap:J
    const-wide/16 v9, 0x32

    #@67
    cmp-long v9, v3, v9

    #@69
    if-lez v9, :cond_7

    #@6b
    .line 652
    iget-object v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@6d
    iget-wide v9, v9, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@6f
    iget-object v11, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@71
    iget-wide v11, v11, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@73
    sub-long v5, v9, v11

    #@75
    .line 653
    .local v5, txincreaseAP:J
    iget-object v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@77
    iget-wide v9, v9, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@79
    iget-object v11, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@7b
    iget-wide v11, v11, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@7d
    sub-long v7, v9, v11

    #@7f
    .line 654
    .local v7, txincreaseCP:J
    new-instance v9, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v10, "["

    #@86
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v9

    #@8a
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@8c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v9

    #@90
    const-string v10, "] txincreaseAP : "

    #@92
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v9

    #@96
    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@99
    move-result-object v9

    #@9a
    const-string v10, "    txincreaseCP : "

    #@9c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v9

    #@a0
    invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v9

    #@a4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v9

    #@a8
    invoke-static {v9}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@ab
    .line 655
    iget v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->sentSinceLastRecv:I

    #@ad
    if-eqz v9, :cond_7

    #@af
    .line 658
    iget v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->sentSinceLastRecv:I

    #@b1
    const/16 v10, 0xa

    #@b3
    if-le v9, v10, :cond_7

    #@b5
    .line 659
    iget-boolean v9, p0, Lcom/android/internal/telephony/LGDataRecovery;->supportCPtrafficstat:Z

    #@b7
    if-eqz v9, :cond_1b8

    #@b9
    const-wide/16 v9, 0x0

    #@bb
    cmp-long v9, v7, v9

    #@bd
    if-ltz v9, :cond_1b8

    #@bf
    .line 660
    const-wide/16 v9, 0x0

    #@c1
    cmp-long v9, v5, v9

    #@c3
    if-lez v9, :cond_10c

    #@c5
    const-wide/16 v9, 0x0

    #@c7
    cmp-long v9, v7, v9

    #@c9
    if-lez v9, :cond_10c

    #@cb
    .line 662
    const-string v9, "TX_INCREASE_BOTH_AP_CP but no Rx. It can be a problem of network or radio. QXDM log required."

    #@cd
    invoke-static {v9}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@d0
    .line 663
    iget-object v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@d2
    const-string v10, "default"

    #@d4
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@d7
    move-result v9

    #@d8
    if-eqz v9, :cond_7

    #@da
    .line 664
    new-instance v9, Ljava/lang/StringBuilder;

    #@dc
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@df
    const-string v10, "STALL_SYPTOM "

    #@e1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v9

    #@e5
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@e7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v9

    #@eb
    const-string v10, " / "

    #@ed
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v9

    #@f1
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@f3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v9

    #@f7
    const-string v10, " :: TX_INCREASE_BOTH_AP_CP but no Rx QXDM log required"

    #@f9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v9

    #@fd
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@100
    move-result-object v9

    #@101
    const-string v10, "datastallinfo.log"

    #@103
    invoke-static {v9, v10}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@106
    .line 665
    const/4 v2, 0x7

    #@107
    .line 666
    const/4 v9, 0x2

    #@108
    iput v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->recoverySign:I

    #@10a
    goto/16 :goto_7

    #@10c
    .line 670
    :cond_10c
    const-wide/16 v9, 0x0

    #@10e
    cmp-long v9, v5, v9

    #@110
    if-lez v9, :cond_14d

    #@112
    const-wide/16 v9, 0x0

    #@114
    cmp-long v9, v7, v9

    #@116
    if-nez v9, :cond_14d

    #@118
    .line 672
    const/4 v9, 0x4

    #@119
    if-ge v2, v9, :cond_11c

    #@11b
    .line 673
    const/4 v2, 0x4

    #@11c
    .line 675
    :cond_11c
    const/4 v9, 0x2

    #@11d
    iput v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->recoverySign:I

    #@11f
    .line 676
    new-instance v9, Ljava/lang/StringBuilder;

    #@121
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@124
    const-string v10, "STALL_SYPTOM "

    #@126
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v9

    #@12a
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@12c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v9

    #@130
    const-string v10, " / "

    #@132
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v9

    #@136
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@138
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v9

    #@13c
    const-string v10, " :: ONLY_AP_TX_INCREASE"

    #@13e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v9

    #@142
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v9

    #@146
    const-string v10, "datastallinfo.log"

    #@148
    invoke-static {v9, v10}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@14b
    goto/16 :goto_7

    #@14d
    .line 679
    :cond_14d
    const-wide/16 v9, 0x0

    #@14f
    cmp-long v9, v7, v9

    #@151
    if-nez v9, :cond_192

    #@153
    .line 680
    iget-object v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@155
    const-string v10, "ims"

    #@157
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@15a
    move-result v9

    #@15b
    if-nez v9, :cond_7

    #@15d
    .line 681
    const/4 v9, 0x2

    #@15e
    if-ge v2, v9, :cond_161

    #@160
    .line 682
    const/4 v2, 0x2

    #@161
    .line 684
    :cond_161
    const/4 v9, 0x2

    #@162
    iput v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->recoverySign:I

    #@164
    .line 685
    new-instance v9, Ljava/lang/StringBuilder;

    #@166
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@169
    const-string v10, "STALL_SYPTOM "

    #@16b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v9

    #@16f
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@171
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v9

    #@175
    const-string v10, " / "

    #@177
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v9

    #@17b
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@17d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v9

    #@181
    const-string v10, " :: NO_AP_TX_INCREASE"

    #@183
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@186
    move-result-object v9

    #@187
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18a
    move-result-object v9

    #@18b
    const-string v10, "datastallinfo.log"

    #@18d
    invoke-static {v9, v10}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@190
    goto/16 :goto_7

    #@192
    .line 688
    :cond_192
    new-instance v9, Ljava/lang/StringBuilder;

    #@194
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@197
    const-string v10, " only CP Tx Cnt increase. QCT APP may work do not recovery"

    #@199
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v9

    #@19d
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@19f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v9

    #@1a3
    const-string v10, " / "

    #@1a5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v9

    #@1a9
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@1ab
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ae
    move-result-object v9

    #@1af
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b2
    move-result-object v9

    #@1b3
    invoke-static {v9}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@1b6
    goto/16 :goto_7

    #@1b8
    .line 692
    :cond_1b8
    const/4 v9, 0x3

    #@1b9
    if-ge v2, v9, :cond_1bc

    #@1bb
    .line 693
    const/4 v2, 0x3

    #@1bc
    .line 695
    :cond_1bc
    new-instance v9, Ljava/lang/StringBuilder;

    #@1be
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1c1
    const-string v10, "STALL_SYPTOM "

    #@1c3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c6
    move-result-object v9

    #@1c7
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@1c9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v9

    #@1cd
    const-string v10, " / "

    #@1cf
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v9

    #@1d3
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@1d5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v9

    #@1d9
    const-string v10, " :: ONLY_TX_INCREASE"

    #@1db
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1de
    move-result-object v9

    #@1df
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e2
    move-result-object v9

    #@1e3
    const-string v10, "datastallinfo.log"

    #@1e5
    invoke-static {v9, v10}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@1e8
    goto/16 :goto_7

    #@1ea
    .line 701
    .end local v3           #timegap:J
    .end local v5           #txincreaseAP:J
    .end local v7           #txincreaseCP:J
    :cond_1ea
    new-instance v9, Ljava/lang/StringBuilder;

    #@1ec
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1ef
    const-string v10, " conn with "

    #@1f1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v9

    #@1f5
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@1f7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v9

    #@1fb
    const-string v10, " is not Active change LState to DOWN WARNING!!"

    #@1fd
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@200
    move-result-object v9

    #@201
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@204
    move-result-object v9

    #@205
    invoke-static {v9}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@208
    .line 702
    sget-object v9, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKDOWN:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@20a
    iput-object v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@20c
    .line 703
    const/4 v9, 0x1

    #@20d
    iput v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->recoverySign:I

    #@20f
    goto/16 :goto_7

    #@211
    .line 706
    :cond_211
    new-instance v9, Ljava/lang/StringBuilder;

    #@213
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@216
    const-string v10, " ConnInfo for "

    #@218
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21b
    move-result-object v9

    #@21c
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@21e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@221
    move-result-object v9

    #@222
    const-string v10, "  Lstate : "

    #@224
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@227
    move-result-object v9

    #@228
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@22a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v9

    #@22e
    const-string v10, "  FState :"

    #@230
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@233
    move-result-object v9

    #@234
    iget-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@236
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@239
    move-result-object v9

    #@23a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23d
    move-result-object v9

    #@23e
    invoke-static {v9}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@241
    .line 707
    const/4 v9, 0x1

    #@242
    iput v9, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->recoverySign:I

    #@244
    goto/16 :goto_7

    #@246
    .line 710
    .end local v0           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    :cond_246
    new-instance v9, Ljava/lang/StringBuilder;

    #@248
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@24b
    const-string v10, "doStallCheckwithPktCnt stallsympton = "

    #@24d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@250
    move-result-object v9

    #@251
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@254
    move-result-object v9

    #@255
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@258
    move-result-object v9

    #@259
    invoke-static {v9}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@25c
    .line 711
    if-nez v2, :cond_263

    #@25e
    .line 712
    const/4 v9, 0x0

    #@25f
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/LGDataRecovery;->putRecoveryAction(I)V

    #@262
    .line 719
    :cond_262
    :goto_262
    return-void

    #@263
    .line 713
    :cond_263
    const/4 v9, 0x2

    #@264
    if-lt v2, v9, :cond_262

    #@266
    .line 714
    const-string v9, "recovery needed"

    #@268
    invoke-static {v9}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@26b
    .line 715
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->doRecovery()V

    #@26e
    goto :goto_262
.end method

.method private getRecoveryAction()I
    .registers 5

    #@0
    .prologue
    .line 722
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v1

    #@a
    const-string v2, "radio.data.stall.recovery.action"

    #@c
    const/4 v3, 0x1

    #@d
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10
    move-result v0

    #@11
    .line 724
    .local v0, action:I
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "getRecoveryAction: "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-static {v1}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@27
    .line 725
    return v0
.end method

.method private getShellCmdResult(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 14
    .parameter "cmd"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1883
    new-instance v7, Ljava/util/ArrayList;

    #@2
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1884
    .local v7, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@8
    move-result-object v9

    #@9
    .line 1887
    .local v9, runtime:Ljava/lang/Runtime;
    const/4 v3, 0x0

    #@a
    .line 1888
    .local v3, is:Ljava/io/InputStream;
    const/4 v4, 0x0

    #@b
    .line 1889
    .local v4, isr:Ljava/io/InputStreamReader;
    const/4 v0, 0x0

    #@c
    .line 1892
    .local v0, br:Ljava/io/BufferedReader;
    :try_start_c
    invoke-virtual {v9, p1}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    #@f
    move-result-object v8

    #@10
    .line 1893
    .local v8, process:Ljava/lang/Process;
    invoke-virtual {v8}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    #@13
    move-result-object v3

    #@14
    .line 1894
    new-instance v5, Ljava/io/InputStreamReader;

    #@16
    invoke-direct {v5, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_19
    .catchall {:try_start_c .. :try_end_19} :catchall_63
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_19} :catch_8d

    #@19
    .line 1895
    .end local v4           #isr:Ljava/io/InputStreamReader;
    .local v5, isr:Ljava/io/InputStreamReader;
    :try_start_19
    new-instance v1, Ljava/io/BufferedReader;

    #@1b
    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1e
    .catchall {:try_start_19 .. :try_end_1e} :catchall_86
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_1e} :catch_8f

    #@1e
    .line 1898
    .end local v0           #br:Ljava/io/BufferedReader;
    .local v1, br:Ljava/io/BufferedReader;
    :goto_1e
    :try_start_1e
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@21
    move-result-object v6

    #@22
    .local v6, line:Ljava/lang/String;
    if-eqz v6, :cond_51

    #@24
    .line 1899
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_27
    .catchall {:try_start_1e .. :try_end_27} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_27} :catch_28

    #@27
    goto :goto_1e

    #@28
    .line 1904
    .end local v6           #line:Ljava/lang/String;
    :catch_28
    move-exception v2

    #@29
    move-object v0, v1

    #@2a
    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v0       #br:Ljava/io/BufferedReader;
    move-object v4, v5

    #@2b
    .line 1905
    .end local v5           #isr:Ljava/io/InputStreamReader;
    .end local v8           #process:Ljava/lang/Process;
    .local v2, e:Ljava/lang/Exception;
    .restart local v4       #isr:Ljava/io/InputStreamReader;
    :goto_2b
    :try_start_2b
    new-instance v10, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v11, "Error getting i/o stream. "

    #@32
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v10

    #@36
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v10

    #@3a
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v10

    #@3e
    invoke-static {v10}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V
    :try_end_41
    .catchall {:try_start_2b .. :try_end_41} :catchall_63

    #@41
    .line 1907
    if-eqz v0, :cond_46

    #@43
    .line 1909
    :try_start_43
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_46
    .catch Ljava/lang/Exception; {:try_start_43 .. :try_end_46} :catch_7a

    #@46
    .line 1914
    :cond_46
    :goto_46
    if-eqz v4, :cond_4b

    #@48
    .line 1916
    :try_start_48
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V
    :try_end_4b
    .catch Ljava/lang/Exception; {:try_start_48 .. :try_end_4b} :catch_7c

    #@4b
    .line 1921
    :cond_4b
    :goto_4b
    if-eqz v3, :cond_50

    #@4d
    .line 1923
    :try_start_4d
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_50
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_50} :catch_7e

    #@50
    .line 1930
    .end local v2           #e:Ljava/lang/Exception;
    :cond_50
    :goto_50
    return-object v7

    #@51
    .line 1907
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v4           #isr:Ljava/io/InputStreamReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    .restart local v5       #isr:Ljava/io/InputStreamReader;
    .restart local v6       #line:Ljava/lang/String;
    .restart local v8       #process:Ljava/lang/Process;
    :cond_51
    if-eqz v1, :cond_56

    #@53
    .line 1909
    :try_start_53
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_56
    .catch Ljava/lang/Exception; {:try_start_53 .. :try_end_56} :catch_80

    #@56
    .line 1914
    :cond_56
    :goto_56
    if-eqz v5, :cond_5b

    #@58
    .line 1916
    :try_start_58
    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V
    :try_end_5b
    .catch Ljava/lang/Exception; {:try_start_58 .. :try_end_5b} :catch_82

    #@5b
    .line 1921
    :cond_5b
    :goto_5b
    if-eqz v3, :cond_60

    #@5d
    .line 1923
    :try_start_5d
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_60
    .catch Ljava/lang/Exception; {:try_start_5d .. :try_end_60} :catch_84

    #@60
    :cond_60
    :goto_60
    move-object v0, v1

    #@61
    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v0       #br:Ljava/io/BufferedReader;
    move-object v4, v5

    #@62
    .line 1928
    .end local v5           #isr:Ljava/io/InputStreamReader;
    .restart local v4       #isr:Ljava/io/InputStreamReader;
    goto :goto_50

    #@63
    .line 1907
    .end local v6           #line:Ljava/lang/String;
    .end local v8           #process:Ljava/lang/Process;
    :catchall_63
    move-exception v10

    #@64
    :goto_64
    if-eqz v0, :cond_69

    #@66
    .line 1909
    :try_start_66
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_69
    .catch Ljava/lang/Exception; {:try_start_66 .. :try_end_69} :catch_74

    #@69
    .line 1914
    :cond_69
    :goto_69
    if-eqz v4, :cond_6e

    #@6b
    .line 1916
    :try_start_6b
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V
    :try_end_6e
    .catch Ljava/lang/Exception; {:try_start_6b .. :try_end_6e} :catch_76

    #@6e
    .line 1921
    :cond_6e
    :goto_6e
    if-eqz v3, :cond_73

    #@70
    .line 1923
    :try_start_70
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_73
    .catch Ljava/lang/Exception; {:try_start_70 .. :try_end_73} :catch_78

    #@73
    .line 1907
    :cond_73
    :goto_73
    throw v10

    #@74
    .line 1910
    :catch_74
    move-exception v11

    #@75
    goto :goto_69

    #@76
    .line 1917
    :catch_76
    move-exception v11

    #@77
    goto :goto_6e

    #@78
    .line 1924
    :catch_78
    move-exception v11

    #@79
    goto :goto_73

    #@7a
    .line 1910
    .restart local v2       #e:Ljava/lang/Exception;
    :catch_7a
    move-exception v10

    #@7b
    goto :goto_46

    #@7c
    .line 1917
    :catch_7c
    move-exception v10

    #@7d
    goto :goto_4b

    #@7e
    .line 1924
    :catch_7e
    move-exception v10

    #@7f
    goto :goto_50

    #@80
    .line 1910
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v2           #e:Ljava/lang/Exception;
    .end local v4           #isr:Ljava/io/InputStreamReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    .restart local v5       #isr:Ljava/io/InputStreamReader;
    .restart local v6       #line:Ljava/lang/String;
    .restart local v8       #process:Ljava/lang/Process;
    :catch_80
    move-exception v10

    #@81
    goto :goto_56

    #@82
    .line 1917
    :catch_82
    move-exception v10

    #@83
    goto :goto_5b

    #@84
    .line 1924
    :catch_84
    move-exception v10

    #@85
    goto :goto_60

    #@86
    .line 1907
    .end local v1           #br:Ljava/io/BufferedReader;
    .end local v6           #line:Ljava/lang/String;
    .restart local v0       #br:Ljava/io/BufferedReader;
    :catchall_86
    move-exception v10

    #@87
    move-object v4, v5

    #@88
    .end local v5           #isr:Ljava/io/InputStreamReader;
    .restart local v4       #isr:Ljava/io/InputStreamReader;
    goto :goto_64

    #@89
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v4           #isr:Ljava/io/InputStreamReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    .restart local v5       #isr:Ljava/io/InputStreamReader;
    :catchall_89
    move-exception v10

    #@8a
    move-object v0, v1

    #@8b
    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v0       #br:Ljava/io/BufferedReader;
    move-object v4, v5

    #@8c
    .end local v5           #isr:Ljava/io/InputStreamReader;
    .restart local v4       #isr:Ljava/io/InputStreamReader;
    goto :goto_64

    #@8d
    .line 1904
    .end local v8           #process:Ljava/lang/Process;
    :catch_8d
    move-exception v2

    #@8e
    goto :goto_2b

    #@8f
    .end local v4           #isr:Ljava/io/InputStreamReader;
    .restart local v5       #isr:Ljava/io/InputStreamReader;
    .restart local v8       #process:Ljava/lang/Process;
    :catch_8f
    move-exception v2

    #@90
    move-object v4, v5

    #@91
    .end local v5           #isr:Ljava/io/InputStreamReader;
    .restart local v4       #isr:Ljava/io/InputStreamReader;
    goto :goto_2b
.end method

.method private hasDefaultRouteInfo(Ljava/lang/String;)Z
    .registers 12
    .parameter "iface"

    #@0
    .prologue
    .line 1483
    iget-object v8, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@2
    if-nez v8, :cond_10

    #@4
    .line 1484
    const-string v8, "network_management"

    #@6
    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v1

    #@a
    .line 1485
    .local v1, b:Landroid/os/IBinder;
    invoke-static {v1}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@d
    move-result-object v8

    #@e
    iput-object v8, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@10
    .line 1489
    .end local v1           #b:Landroid/os/IBinder;
    :cond_10
    :try_start_10
    iget-object v8, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@12
    invoke-interface {v8, p1}, Landroid/os/INetworkManagementService;->getRoutes(Ljava/lang/String;)[Landroid/net/RouteInfo;

    #@15
    move-result-object v7

    #@16
    .line 1491
    .local v7, routeInfos:[Landroid/net/RouteInfo;
    move-object v0, v7

    #@17
    .local v0, arr$:[Landroid/net/RouteInfo;
    array-length v5, v0

    #@18
    .local v5, len$:I
    const/4 v4, 0x0

    #@19
    .local v4, i$:I
    :goto_19
    if-ge v4, v5, :cond_8a

    #@1b
    aget-object v6, v0, v4

    #@1d
    .line 1493
    .local v6, ri:Landroid/net/RouteInfo;
    invoke-virtual {v6}, Landroid/net/RouteInfo;->getDestination()Landroid/net/LinkAddress;

    #@20
    move-result-object v8

    #@21
    invoke-virtual {v8}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@24
    move-result-object v2

    #@25
    .line 1495
    .local v2, dstAddr:Ljava/net/InetAddress;
    new-instance v8, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v9, "hasDefaultRouteInfo iface: "

    #@2c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v8

    #@34
    const-string v9, " ,dstAddr: "

    #@36
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v8

    #@3a
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v8

    #@3e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v8

    #@42
    invoke-static {v8}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@45
    .line 1496
    new-instance v8, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v9, "hasDefaultRouteInfo iface: "

    #@4c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v8

    #@50
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v8

    #@54
    const-string v9, " ,dstAddr: "

    #@56
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v8

    #@5a
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v8

    #@5e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v8

    #@62
    const-string v9, "lgdatadump.log"

    #@64
    invoke-static {v8, v9}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@67
    .line 1498
    const-string v8, "0.0.0.0"

    #@69
    invoke-static {v8}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@6c
    move-result-object v8

    #@6d
    invoke-virtual {v2, v8}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@70
    move-result v8

    #@71
    if-nez v8, :cond_7f

    #@73
    const-string v8, "::"

    #@75
    invoke-static {v8}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@78
    move-result-object v8

    #@79
    invoke-virtual {v2, v8}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z
    :try_end_7c
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_7c} :catch_84

    #@7c
    move-result v8

    #@7d
    if-eqz v8, :cond_81

    #@7f
    .line 1500
    :cond_7f
    const/4 v8, 0x1

    #@80
    .line 1507
    .end local v0           #arr$:[Landroid/net/RouteInfo;
    .end local v2           #dstAddr:Ljava/net/InetAddress;
    .end local v4           #i$:I
    .end local v5           #len$:I
    .end local v6           #ri:Landroid/net/RouteInfo;
    .end local v7           #routeInfos:[Landroid/net/RouteInfo;
    :goto_80
    return v8

    #@81
    .line 1491
    .restart local v0       #arr$:[Landroid/net/RouteInfo;
    .restart local v2       #dstAddr:Ljava/net/InetAddress;
    .restart local v4       #i$:I
    .restart local v5       #len$:I
    .restart local v6       #ri:Landroid/net/RouteInfo;
    .restart local v7       #routeInfos:[Landroid/net/RouteInfo;
    :cond_81
    add-int/lit8 v4, v4, 0x1

    #@83
    goto :goto_19

    #@84
    .line 1503
    .end local v0           #arr$:[Landroid/net/RouteInfo;
    .end local v2           #dstAddr:Ljava/net/InetAddress;
    .end local v4           #i$:I
    .end local v5           #len$:I
    .end local v6           #ri:Landroid/net/RouteInfo;
    .end local v7           #routeInfos:[Landroid/net/RouteInfo;
    :catch_84
    move-exception v3

    #@85
    .line 1504
    .local v3, e:Ljava/lang/Exception;
    const-string v8, "fail to get routeInfo."

    #@87
    invoke-static {v8}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@8a
    .line 1506
    .end local v3           #e:Ljava/lang/Exception;
    :cond_8a
    new-instance v8, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v9, "hasDefaultRouteInfo iface: "

    #@91
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v8

    #@95
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v8

    #@99
    const-string v9, " , return false "

    #@9b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v8

    #@9f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v8

    #@a3
    invoke-static {v8}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@a6
    .line 1507
    const/4 v8, 0x0

    #@a7
    goto :goto_80
.end method

.method private isActive(Ljava/lang/String;)Z
    .registers 7
    .parameter "iface"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1512
    const/4 v1, 0x0

    #@2
    .line 1513
    .local v1, netInterface:Ljava/net/NetworkInterface;
    if-nez p1, :cond_a

    #@4
    .line 1515
    const-string v3, "iface is null"

    #@6
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@9
    .line 1531
    :goto_9
    return v2

    #@a
    .line 1520
    :cond_a
    :try_start_a
    invoke-static {p1}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    #@d
    move-result-object v1

    #@e
    .line 1521
    if-eqz v1, :cond_4b

    #@10
    invoke-virtual {v1}, Ljava/net/NetworkInterface;->isUp()Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_4b

    #@16
    .line 1523
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "iface ("

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, ") is Active"

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V
    :try_end_32
    .catch Ljava/net/SocketException; {:try_start_a .. :try_end_32} :catch_34

    #@32
    .line 1524
    const/4 v2, 0x1

    #@33
    goto :goto_9

    #@34
    .line 1526
    :catch_34
    move-exception v0

    #@35
    .line 1527
    .local v0, ex:Ljava/net/SocketException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v4, "Could not obtain network interface "

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@4b
    .line 1530
    .end local v0           #ex:Ljava/net/SocketException;
    :cond_4b
    new-instance v3, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v4, "iface ("

    #@52
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    const-string v4, ") is not Active"

    #@5c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@67
    goto :goto_9
.end method

.method private isDataStallAlarmNeeded()Z
    .registers 5

    #@0
    .prologue
    .line 456
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_31

    #@4
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v2

    #@a
    if-lez v2, :cond_31

    #@c
    .line 457
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v1

    #@12
    .local v1, i$:Ljava/util/Iterator;
    :cond_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_31

    #@18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@1e
    .line 458
    .local v0, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@20
    sget-object v3, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@22
    if-ne v2, v3, :cond_12

    #@24
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@26
    sget-object v3, Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;->CONNECTED:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@28
    if-ne v2, v3, :cond_12

    #@2a
    .line 459
    const-string v2, "There is at least one connInfo connected."

    #@2c
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@2f
    .line 460
    const/4 v2, 0x1

    #@30
    .line 464
    .end local v0           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    .end local v1           #i$:Ljava/util/Iterator;
    :goto_30
    return v2

    #@31
    :cond_31
    const/4 v2, 0x0

    #@32
    goto :goto_30
.end method

.method protected static log(Ljava/lang/String;)V
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 1833
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[DataMonitoring] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1834
    const-string v0, "LGDataRecoveryLog.log"

    #@1a
    invoke-static {p0, v0}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1835
    return-void
.end method

.method private static logF(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "msg"
    .parameter "fileName"

    #@0
    .prologue
    .line 1838
    const/4 v0, 0x1

    #@1
    invoke-static {p0, p1, v0}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;Z)V

    #@4
    .line 1839
    return-void
.end method

.method private static declared-synchronized logF(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 11
    .parameter "msg"
    .parameter "fileName"
    .parameter "timestamp"

    #@0
    .prologue
    .line 1842
    const-class v5, Lcom/android/internal/telephony/LGDataRecovery;

    #@2
    monitor-enter v5

    #@3
    const/4 v1, 0x0

    #@4
    .line 1846
    .local v1, fh:Ljava/util/logging/FileHandler;
    :try_start_4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v6, "/data/data/com.android.phone/"

    #@b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_16
    .catchall {:try_start_4 .. :try_end_16} :catchall_4f

    #@16
    move-result-object v3

    #@17
    .line 1849
    .local v3, name:Ljava/lang/String;
    :try_start_17
    new-instance v2, Ljava/util/logging/FileHandler;

    #@19
    const v4, 0x1e8480

    #@1c
    const/4 v6, 0x4

    #@1d
    const/4 v7, 0x1

    #@1e
    invoke-direct {v2, v3, v4, v6, v7}, Ljava/util/logging/FileHandler;-><init>(Ljava/lang/String;IIZ)V
    :try_end_21
    .catchall {:try_start_17 .. :try_end_21} :catchall_52
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_21} :catch_3e

    #@21
    .line 1850
    .end local v1           #fh:Ljava/util/logging/FileHandler;
    .local v2, fh:Ljava/util/logging/FileHandler;
    :try_start_21
    new-instance v4, Lcom/android/internal/telephony/LGDataRecovery$DataLogFormatter;

    #@23
    invoke-direct {v4, p2}, Lcom/android/internal/telephony/LGDataRecovery$DataLogFormatter;-><init>(Z)V

    #@26
    invoke-virtual {v2, v4}, Ljava/util/logging/FileHandler;->setFormatter(Ljava/util/logging/Formatter;)V

    #@29
    .line 1851
    new-instance v4, Ljava/util/logging/LogRecord;

    #@2b
    sget-object v6, Ljava/util/logging/Level;->ALL:Ljava/util/logging/Level;

    #@2d
    invoke-direct {v4, v6, p0}, Ljava/util/logging/LogRecord;-><init>(Ljava/util/logging/Level;Ljava/lang/String;)V

    #@30
    invoke-virtual {v2, v4}, Ljava/util/logging/FileHandler;->publish(Ljava/util/logging/LogRecord;)V
    :try_end_33
    .catchall {:try_start_21 .. :try_end_33} :catchall_5c
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_33} :catch_5f

    #@33
    .line 1860
    if-eqz v2, :cond_3b

    #@35
    .line 1861
    :try_start_35
    invoke-virtual {v2}, Ljava/util/logging/FileHandler;->flush()V

    #@38
    .line 1862
    invoke-virtual {v2}, Ljava/util/logging/FileHandler;->close()V
    :try_end_3b
    .catchall {:try_start_35 .. :try_end_3b} :catchall_62

    #@3b
    :cond_3b
    move-object v1, v2

    #@3c
    .line 1865
    .end local v2           #fh:Ljava/util/logging/FileHandler;
    .restart local v1       #fh:Ljava/util/logging/FileHandler;
    :cond_3c
    :goto_3c
    monitor-exit v5

    #@3d
    return-void

    #@3e
    .line 1853
    :catch_3e
    move-exception v0

    #@3f
    .line 1854
    .local v0, e:Ljava/lang/Exception;
    :goto_3f
    :try_start_3f
    const-string v4, "GSM"

    #@41
    const-string v6, "[DataMonitoring] Can not open log file."

    #@43
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_46
    .catchall {:try_start_3f .. :try_end_46} :catchall_52

    #@46
    .line 1860
    if-eqz v1, :cond_3c

    #@48
    .line 1861
    :try_start_48
    invoke-virtual {v1}, Ljava/util/logging/FileHandler;->flush()V

    #@4b
    .line 1862
    invoke-virtual {v1}, Ljava/util/logging/FileHandler;->close()V
    :try_end_4e
    .catchall {:try_start_48 .. :try_end_4e} :catchall_4f

    #@4e
    goto :goto_3c

    #@4f
    .line 1842
    .end local v0           #e:Ljava/lang/Exception;
    .end local v3           #name:Ljava/lang/String;
    :catchall_4f
    move-exception v4

    #@50
    :goto_50
    monitor-exit v5

    #@51
    throw v4

    #@52
    .line 1860
    .restart local v3       #name:Ljava/lang/String;
    :catchall_52
    move-exception v4

    #@53
    :goto_53
    if-eqz v1, :cond_5b

    #@55
    .line 1861
    :try_start_55
    invoke-virtual {v1}, Ljava/util/logging/FileHandler;->flush()V

    #@58
    .line 1862
    invoke-virtual {v1}, Ljava/util/logging/FileHandler;->close()V

    #@5b
    .line 1860
    :cond_5b
    throw v4
    :try_end_5c
    .catchall {:try_start_55 .. :try_end_5c} :catchall_4f

    #@5c
    .end local v1           #fh:Ljava/util/logging/FileHandler;
    .restart local v2       #fh:Ljava/util/logging/FileHandler;
    :catchall_5c
    move-exception v4

    #@5d
    move-object v1, v2

    #@5e
    .end local v2           #fh:Ljava/util/logging/FileHandler;
    .restart local v1       #fh:Ljava/util/logging/FileHandler;
    goto :goto_53

    #@5f
    .line 1853
    .end local v1           #fh:Ljava/util/logging/FileHandler;
    .restart local v2       #fh:Ljava/util/logging/FileHandler;
    :catch_5f
    move-exception v0

    #@60
    move-object v1, v2

    #@61
    .end local v2           #fh:Ljava/util/logging/FileHandler;
    .restart local v1       #fh:Ljava/util/logging/FileHandler;
    goto :goto_3f

    #@62
    .line 1842
    .end local v1           #fh:Ljava/util/logging/FileHandler;
    .restart local v2       #fh:Ljava/util/logging/FileHandler;
    :catchall_62
    move-exception v4

    #@63
    move-object v1, v2

    #@64
    .end local v2           #fh:Ljava/util/logging/FileHandler;
    .restart local v1       #fh:Ljava/util/logging/FileHandler;
    goto :goto_50
.end method

.method private loggingRouteInfos()V
    .registers 7

    #@0
    .prologue
    .line 1869
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1871
    .local v2, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->getAllRouteInfos()Ljava/util/ArrayList;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v0

    #@d
    .local v0, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_29

    #@13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;

    #@19
    .line 1872
    .local v1, ri:Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;
    invoke-virtual {v1}, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-static {}, Ljava/lang/System;->lineSeparator()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    goto :goto_d

    #@29
    .line 1875
    .end local v1           #ri:Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;
    :cond_29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    #@2c
    move-result v3

    #@2d
    const/4 v4, 0x1

    #@2e
    if-le v3, v4, :cond_43

    #@30
    .line 1876
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    #@33
    move-result v3

    #@34
    add-int/lit8 v3, v3, -0x1

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    #@39
    .line 1877
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, "netinfo.log"

    #@3f
    const/4 v5, 0x0

    #@40
    invoke-static {v3, v4, v5}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;Z)V

    #@43
    .line 1879
    :cond_43
    return-void
.end method

.method private onConnCompletedCheckAlarmExpired(Ljava/lang/String;)V
    .registers 9
    .parameter "iface"

    #@0
    .prologue
    .line 498
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/LGDataRecovery;->findConnInfowithIface(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@3
    move-result-object v0

    #@4
    .line 499
    .local v0, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "onConnCompletedCheckAlarmExpired for "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@1a
    .line 500
    if-nez v0, :cond_39

    #@1c
    .line 501
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v4, "there is no Conn for ConnCompleteCheck for this iface: "

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    const-string v4, " WARNING"

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@38
    .line 541
    :goto_38
    return-void

    #@39
    .line 504
    :cond_39
    const/4 v3, 0x0

    #@3a
    iput-object v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->mConnCheckAlarmIntent:Landroid/app/PendingIntent;

    #@3c
    .line 505
    iget-object v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@3e
    sget-object v4, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKDOWN:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@40
    if-ne v3, v4, :cond_5f

    #@42
    .line 506
    new-instance v3, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v4, "there is no need to do ConnCompleteCheck for this iface: "

    #@49
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    const-string v4, "WARNING"

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v3

    #@5b
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@5e
    goto :goto_38

    #@5f
    .line 509
    :cond_5f
    iget-object v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@61
    sget-object v4, Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;->DISCONNECTED:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@63
    if-ne v3, v4, :cond_19b

    #@65
    .line 510
    new-instance v3, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v4, "iface "

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v3

    #@74
    const-string v4, " didn\'t receive connected event from telephony framework"

    #@76
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v3

    #@7a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v3

    #@7e
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@81
    .line 511
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->modemVendor:Ljava/lang/String;

    #@83
    const-string v4, "QCT"

    #@85
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@88
    move-result v3

    #@89
    if-eqz v3, :cond_151

    #@8b
    .line 512
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LGDataRecovery;->updateTrafficStatforAP(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V

    #@8e
    .line 513
    iget-object v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@90
    iget-wide v3, v3, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@92
    iget-object v5, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@94
    iget-wide v5, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@96
    sub-long v1, v3, v5

    #@98
    .line 514
    .local v1, txincrease:J
    new-instance v3, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v4, "onConnCompletedCheckAlarmExpired txincrease = "

    #@9f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v3

    #@a3
    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v3

    #@a7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v3

    #@ab
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@ae
    .line 515
    iget v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->sentSinceLastRecv:I

    #@b0
    if-lez v3, :cond_e5

    #@b2
    .line 516
    const-string v3, "IPC data path block is suspicious : ONLY_AP_TX_INCREASE_ON_DHCP"

    #@b4
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@b7
    .line 517
    new-instance v3, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v4, "STALL_SYPTOM "

    #@be
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v3

    #@c2
    iget-object v4, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@c4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v3

    #@c8
    const-string v4, " / "

    #@ca
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v3

    #@ce
    iget-object v4, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@d0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v3

    #@d4
    const-string v4, " :: CONN COMPLETE FAIL, ONLY_AP_TX_INCREASE_ON_DHCP"

    #@d6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v3

    #@da
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v3

    #@de
    const-string v4, "datastallinfo.log"

    #@e0
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@e3
    goto/16 :goto_38

    #@e5
    .line 521
    :cond_e5
    const-wide/16 v3, 0x0

    #@e7
    cmp-long v3, v1, v3

    #@e9
    if-nez v3, :cond_11e

    #@eb
    .line 522
    const-string v3, "IPC data path block is suspicious : NO_AP_TX_INCREASE_ON_DHCP"

    #@ed
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@f0
    .line 523
    new-instance v3, Ljava/lang/StringBuilder;

    #@f2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f5
    const-string v4, "STALL_SYPTOM "

    #@f7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v3

    #@fb
    iget-object v4, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@fd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v3

    #@101
    const-string v4, " / "

    #@103
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v3

    #@107
    iget-object v4, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@109
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v3

    #@10d
    const-string v4, " :: CONN COMPLETE FAIL, NO_AP_TX_INCREASE_ON_DHCP"

    #@10f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v3

    #@113
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v3

    #@117
    const-string v4, "datastallinfo.log"

    #@119
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@11c
    goto/16 :goto_38

    #@11e
    .line 528
    :cond_11e
    const-string v3, "QCRIL problem is suspicions"

    #@120
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@123
    .line 529
    new-instance v3, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v4, "STALL_SYPTOM "

    #@12a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v3

    #@12e
    iget-object v4, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@130
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v3

    #@134
    const-string v4, " / "

    #@136
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v3

    #@13a
    iget-object v4, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@13c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v3

    #@140
    const-string v4, " :: CONN COMPLETE FAIL, QCRIL error suspicious"

    #@142
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v3

    #@146
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@149
    move-result-object v3

    #@14a
    const-string v4, "datastallinfo.log"

    #@14c
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@14f
    goto/16 :goto_38

    #@151
    .line 534
    .end local v1           #txincrease:J
    :cond_151
    new-instance v3, Ljava/lang/StringBuilder;

    #@153
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@156
    const-string v4, "Connection for "

    #@158
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v3

    #@15c
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v3

    #@160
    const-string v4, "does not complete"

    #@162
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v3

    #@166
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@169
    move-result-object v3

    #@16a
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@16d
    .line 535
    new-instance v3, Ljava/lang/StringBuilder;

    #@16f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@172
    const-string v4, "STALL_SYPTOM "

    #@174
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@177
    move-result-object v3

    #@178
    iget-object v4, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@17a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v3

    #@17e
    const-string v4, " / "

    #@180
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@183
    move-result-object v3

    #@184
    iget-object v4, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@186
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v3

    #@18a
    const-string v4, " :: CONN COMPLETE FAIL "

    #@18c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v3

    #@190
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@193
    move-result-object v3

    #@194
    const-string v4, "datastallinfo.log"

    #@196
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@199
    goto/16 :goto_38

    #@19b
    .line 539
    :cond_19b
    const-string v3, "FState is CONNECTED but ConnCompletedCheckAlarmExpired WARNING"

    #@19d
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@1a0
    goto/16 :goto_38
.end method

.method private onDataStallAlarmExpired(I)V
    .registers 6
    .parameter "tag"

    #@0
    .prologue
    .line 468
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->dumpNetInfo()V

    #@3
    .line 469
    iget v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmTag:I

    #@5
    if-eq v2, p1, :cond_2a

    #@7
    .line 471
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "onLGDataStallAlarm: ignore, tag="

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, " expecting "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    iget v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmTag:I

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@29
    .line 495
    :goto_29
    return-void

    #@2a
    .line 475
    :cond_2a
    const/4 v2, 0x0

    #@2b
    iput-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@2d
    .line 476
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->isDataStallAlarmNeeded()Z

    #@30
    move-result v2

    #@31
    if-nez v2, :cond_39

    #@33
    .line 477
    const-string v2, "onLGDataStallAlarm: There is no Conn that needs traffic check."

    #@35
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@38
    goto :goto_29

    #@39
    .line 481
    :cond_39
    iget-boolean v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mVoicecall:Z

    #@3b
    if-eqz v2, :cond_57

    #@3d
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3f
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@42
    move-result-object v2

    #@43
    if-eqz v2, :cond_57

    #@45
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@47
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Lcom/android/internal/telephony/ServiceStateTracker;->isConcurrentVoiceAndDataAllowed()Z

    #@4e
    move-result v2

    #@4f
    if-nez v2, :cond_57

    #@51
    .line 484
    const-string v2, "onLGDataStallAlarm: Do not stall check during 2G voice call"

    #@53
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@56
    goto :goto_29

    #@57
    .line 488
    :cond_57
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@59
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5c
    move-result-object v1

    #@5d
    .local v1, i$:Ljava/util/Iterator;
    :goto_5d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@60
    move-result v2

    #@61
    if-eqz v2, :cond_84

    #@63
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@66
    move-result-object v0

    #@67
    check-cast v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@69
    .line 489
    .local v0, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    new-instance v2, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v3, "NETCONNINFO "

    #@70
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v0}, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->toString()Ljava/lang/String;

    #@77
    move-result-object v3

    #@78
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v2

    #@7c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v2

    #@80
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@83
    goto :goto_5d

    #@84
    .line 492
    .end local v0           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    :cond_84
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->doStallCheckwithPktCnt()V

    #@87
    .line 494
    const/4 v2, 0x0

    #@88
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/LGDataRecovery;->startDataStallAlarm(Z)V

    #@8b
    goto :goto_29
.end method

.method private onDataStateConnected(Landroid/content/Intent;)V
    .registers 9
    .parameter "intent"

    #@0
    .prologue
    .line 553
    const-string v5, "onDataStateConnected"

    #@2
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@5
    .line 555
    const-string v5, "apn"

    #@7
    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 556
    .local v0, apnName:Ljava/lang/String;
    const-string v5, "apnType"

    #@d
    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    .line 557
    .local v1, apnType:Ljava/lang/String;
    const-string v5, "iface"

    #@13
    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    .line 562
    .local v3, iface:Ljava/lang/String;
    if-nez v3, :cond_1f

    #@19
    .line 563
    const-string v5, "iface is null ignore this connected intent"

    #@1b
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@1e
    .line 604
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 566
    :cond_1f
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/LGDataRecovery;->findConnInfowithIface(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@22
    move-result-object v2

    #@23
    .line 568
    .local v2, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/LGDataRecovery;->isActive(Ljava/lang/String;)Z

    #@26
    move-result v5

    #@27
    if-nez v5, :cond_74

    #@29
    .line 569
    new-instance v5, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v6, "iface ("

    #@30
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    const-string v6, ") is not linked up data block"

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@45
    .line 570
    new-instance v5, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v6, "STALL_SYPTOM "

    #@4c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v5

    #@54
    const-string v6, " / "

    #@56
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v5

    #@5e
    const-string v6, " :: CONNECTED_BUT_IFACE_LINK_DOWN"

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    const-string v6, "datastallinfo.log"

    #@6a
    invoke-static {v5, v6}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    .line 571
    if-eqz v2, :cond_1e

    #@6f
    .line 572
    sget-object v5, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKDOWN:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@71
    iput-object v5, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@73
    goto :goto_1e

    #@74
    .line 578
    :cond_74
    if-nez v2, :cond_a3

    #@76
    .line 579
    new-instance v5, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v6, "can\'t find conninfo with this active iface ("

    #@7d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v5

    #@81
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v5

    #@85
    const-string v6, ") create NewConnInfo WARNING"

    #@87
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v5

    #@8f
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@92
    .line 580
    new-instance v4, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@94
    invoke-direct {v4, p0}, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@97
    .line 581
    .local v4, newConn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    iput-object v3, v4, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@99
    .line 582
    sget-object v5, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@9b
    iput-object v5, v4, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@9d
    .line 583
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@9f
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a2
    .line 584
    move-object v2, v4

    #@a3
    .line 587
    .end local v4           #newConn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    :cond_a3
    if-eqz v2, :cond_e4

    #@a5
    iget-object v5, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@a7
    sget-object v6, Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;->DISCONNECTED:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@a9
    if-ne v5, v6, :cond_e4

    #@ab
    .line 588
    new-instance v5, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v6, "This conn("

    #@b2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v5

    #@b6
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v5

    #@ba
    const-string v6, ") receives first connected notification"

    #@bc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v5

    #@c0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v5

    #@c4
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@c7
    .line 589
    iput-object v0, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@c9
    .line 590
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/LGDataRecovery;->stopConnectionCompleteCheckTimer(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V

    #@cc
    .line 591
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/LGDataRecovery;->updateTrafficStatforAP(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V

    #@cf
    .line 592
    iget-boolean v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->supportCPtrafficstat:Z

    #@d1
    if-eqz v5, :cond_d6

    #@d3
    .line 593
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/LGDataRecovery;->updateTrafficStatforCP(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V

    #@d6
    .line 595
    :cond_d6
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->isDataStallAlarmNeeded()Z

    #@d9
    move-result v5

    #@da
    if-nez v5, :cond_e0

    #@dc
    .line 596
    const/4 v5, 0x0

    #@dd
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LGDataRecovery;->startDataStallAlarm(Z)V

    #@e0
    .line 598
    :cond_e0
    sget-object v5, Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;->CONNECTED:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@e2
    iput-object v5, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@e4
    .line 601
    :cond_e4
    if-eqz v2, :cond_1e

    #@e6
    iget-object v5, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@e8
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@eb
    move-result v5

    #@ec
    if-nez v5, :cond_1e

    #@ee
    .line 602
    iget-object v5, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@f0
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f3
    goto/16 :goto_1e
.end method

.method private onDataStateDisconnected(Landroid/content/Intent;)V
    .registers 8
    .parameter "intent"

    #@0
    .prologue
    .line 607
    const-string v4, "apn"

    #@2
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 608
    .local v0, apnName:Ljava/lang/String;
    const-string v4, "apnType"

    #@8
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 609
    .local v1, apnType:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "onDataStateDisconnected for "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, "("

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    const-string v5, ")"

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    invoke-static {v4}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@32
    .line 610
    iget-object v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@37
    move-result-object v3

    #@38
    .local v3, i$:Ljava/util/Iterator;
    :cond_38
    :goto_38
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@3b
    move-result v4

    #@3c
    if-eqz v4, :cond_92

    #@3e
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@41
    move-result-object v2

    #@42
    check-cast v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@44
    .line 611
    .local v2, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    iget-object v4, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@46
    if-eqz v4, :cond_38

    #@48
    iget-object v4, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@4a
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@4d
    move-result v4

    #@4e
    if-eqz v4, :cond_38

    #@50
    .line 612
    new-instance v4, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v5, "conn for "

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    iget-object v5, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    const-string v5, " has apn type "

    #@63
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    const-string v5, " ::remove it"

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    invoke-static {v4}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@78
    .line 613
    iget-object v4, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@7a
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@7d
    .line 614
    iget-object v4, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@7f
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@82
    move-result v4

    #@83
    if-nez v4, :cond_38

    #@85
    .line 615
    const-string v4, " This conn has no apntype which is connected update FState to DISCONNECTED"

    #@87
    invoke-static {v4}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@8a
    .line 616
    sget-object v4, Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;->DISCONNECTED:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@8c
    iput-object v4, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@8e
    .line 617
    const/4 v4, 0x0

    #@8f
    iput-object v4, v2, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@91
    goto :goto_38

    #@92
    .line 621
    .end local v2           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    :cond_92
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->isDataStallAlarmNeeded()Z

    #@95
    move-result v4

    #@96
    if-nez v4, :cond_9b

    #@98
    .line 622
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->stopDataStallAlarm()V

    #@9b
    .line 624
    :cond_9b
    return-void
.end method

.method private onDnsFailObserved(Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;)V
    .registers 23
    .parameter "result"

    #@0
    .prologue
    .line 354
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/LGDataRecovery;->dumpDnsFail(Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;)V

    #@3
    .line 355
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/LGDataRecovery;->findIfaceforNonPidDns()Ljava/lang/String;

    #@6
    move-result-object v4

    #@7
    .line 356
    .local v4, iface:Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    #@9
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v18, "onDnsFailObserved on iface ::"

    #@e
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v17

    #@12
    move-object/from16 v0, v17

    #@14
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v17

    #@18
    const-string v18, " hostname:: "

    #@1a
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v17

    #@1e
    move-object/from16 v0, p1

    #@20
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;->hostName:Ljava/lang/String;

    #@22
    move-object/from16 v18, v0

    #@24
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v17

    #@28
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v17

    #@2c
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@2f
    .line 357
    move-object/from16 v0, p0

    #@31
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->mIsWifiConnected:Z

    #@33
    move/from16 v17, v0

    #@35
    if-eqz v17, :cond_55

    #@37
    .line 358
    if-nez v4, :cond_3f

    #@39
    .line 359
    const-string v17, "onDnsFailObserved Wi-Fi enabled but no route info for DNS"

    #@3b
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@3e
    .line 453
    :cond_3e
    :goto_3e
    return-void

    #@3f
    .line 361
    :cond_3f
    const-string v17, "wlan0"

    #@41
    move-object/from16 v0, v17

    #@43
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v17

    #@47
    if-eqz v17, :cond_4f

    #@49
    .line 362
    const-string v17, "onDnsFailObserved DNS fail on Wi-Fi connection"

    #@4b
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@4e
    goto :goto_3e

    #@4f
    .line 364
    :cond_4f
    const-string v17, "onDnsFailObserved Wi-Fi enabled but wlan0 does not have route info for DNS"

    #@51
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@54
    goto :goto_3e

    #@55
    .line 367
    :cond_55
    if-eqz v4, :cond_61

    #@57
    const-string v17, ""

    #@59
    move-object/from16 v0, v17

    #@5b
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v17

    #@5f
    if-eqz v17, :cond_105

    #@61
    .line 368
    :cond_61
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/LGDataRecovery;->findDefaultConnInfo()Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@64
    move-result-object v3

    #@65
    .line 369
    .local v3, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    if-nez v3, :cond_d3

    #@67
    .line 370
    move-object/from16 v0, p0

    #@69
    iget v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->mNumOfDnsFailNoConn:I

    #@6b
    move/from16 v17, v0

    #@6d
    if-nez v17, :cond_a4

    #@6f
    .line 371
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@72
    move-result-wide v17

    #@73
    move-wide/from16 v0, v17

    #@75
    move-object/from16 v2, p0

    #@77
    iput-wide v0, v2, Lcom/android/internal/telephony/LGDataRecovery;->mTime1stDnsfailNoConn:J

    #@79
    .line 372
    new-instance v17, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v18, "onDnsFailObserved First DNS fail No Conn::"

    #@80
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v17

    #@84
    move-object/from16 v0, p0

    #@86
    iget-wide v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->mTime1stDnsfailNoConn:J

    #@88
    move-wide/from16 v18, v0

    #@8a
    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v17

    #@8e
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v17

    #@92
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@95
    .line 378
    :goto_95
    move-object/from16 v0, p0

    #@97
    iget v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->mNumOfDnsFailNoConn:I

    #@99
    move/from16 v17, v0

    #@9b
    add-int/lit8 v17, v17, 0x1

    #@9d
    move/from16 v0, v17

    #@9f
    move-object/from16 v1, p0

    #@a1
    iput v0, v1, Lcom/android/internal/telephony/LGDataRecovery;->mNumOfDnsFailNoConn:I

    #@a3
    goto :goto_3e

    #@a4
    .line 374
    :cond_a4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@a7
    move-result-wide v13

    #@a8
    .line 375
    .local v13, timeCurrDNSFail:J
    move-object/from16 v0, p0

    #@aa
    iget-wide v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->mTime1stDnsfailNoConn:J

    #@ac
    move-wide/from16 v17, v0

    #@ae
    sub-long v17, v13, v17

    #@b0
    const-wide/16 v19, 0x3e8

    #@b2
    div-long v11, v17, v19

    #@b4
    .line 376
    .local v11, time:J
    new-instance v17, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v18, "onDnsFailObserved DNS fail time during no Conn::"

    #@bb
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v17

    #@bf
    move-object/from16 v0, v17

    #@c1
    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v17

    #@c5
    const-string v18, "sec"

    #@c7
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v17

    #@cb
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v17

    #@cf
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@d2
    goto :goto_95

    #@d3
    .line 380
    .end local v11           #time:J
    .end local v13           #timeCurrDNSFail:J
    :cond_d3
    new-instance v17, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v18, "STALL_SYPTOM "

    #@da
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v17

    #@de
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@e0
    move-object/from16 v18, v0

    #@e2
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v17

    #@e6
    const-string v18, " / "

    #@e8
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v17

    #@ec
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@ee
    move-object/from16 v18, v0

    #@f0
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v17

    #@f4
    const-string v18, " :: NO_ROUTE_INFO"

    #@f6
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v17

    #@fa
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v17

    #@fe
    const-string v18, "datastallinfo.log"

    #@100
    invoke-static/range {v17 .. v18}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@103
    goto/16 :goto_3e

    #@105
    .line 382
    .end local v3           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    :cond_105
    const-string v17, "rmnet"

    #@107
    move-object/from16 v0, v17

    #@109
    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@10c
    move-result v17

    #@10d
    const/16 v18, -0x1

    #@10f
    move/from16 v0, v17

    #@111
    move/from16 v1, v18

    #@113
    if-eq v0, v1, :cond_3e

    #@115
    .line 383
    new-instance v17, Ljava/lang/StringBuilder;

    #@117
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@11a
    const-string v18, "onDnsFailObserved iface "

    #@11c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v17

    #@120
    move-object/from16 v0, v17

    #@122
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v17

    #@126
    const-string v18, " has route for default DNS"

    #@128
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v17

    #@12c
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12f
    move-result-object v17

    #@130
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@133
    .line 384
    const/16 v17, 0x0

    #@135
    move/from16 v0, v17

    #@137
    move-object/from16 v1, p0

    #@139
    iput v0, v1, Lcom/android/internal/telephony/LGDataRecovery;->mNumOfDnsFailNoConn:I

    #@13b
    .line 385
    move-object/from16 v0, p0

    #@13d
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@13f
    move-object/from16 v17, v0

    #@141
    if-nez v17, :cond_151

    #@143
    .line 386
    const-string v17, "onDnsFailObserved stall alarm is not working ignore DNS fail report"

    #@145
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@148
    .line 387
    const-string v17, "     stall alarm is not working ignore DNS fail report"

    #@14a
    const-string v18, "dnsfailhistory.log"

    #@14c
    invoke-static/range {v17 .. v18}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@14f
    goto/16 :goto_3e

    #@151
    .line 390
    :cond_151
    move-object/from16 v0, p0

    #@153
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/LGDataRecovery;->findConnInfowithIface(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@156
    move-result-object v3

    #@157
    .line 391
    .restart local v3       #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    if-nez v3, :cond_192

    #@159
    .line 392
    new-instance v17, Ljava/lang/StringBuilder;

    #@15b
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@15e
    const-string v18, "can\'t find conninfo with this active iface ("

    #@160
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v17

    #@164
    move-object/from16 v0, v17

    #@166
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@169
    move-result-object v17

    #@16a
    const-string v18, ") create NewConnInfo WARNING"

    #@16c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v17

    #@170
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@173
    move-result-object v17

    #@174
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@177
    .line 393
    new-instance v9, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@179
    move-object/from16 v0, p0

    #@17b
    invoke-direct {v9, v0}, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@17e
    .line 394
    .local v9, newConn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    iput-object v4, v9, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@180
    .line 395
    sget-object v17, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@182
    move-object/from16 v0, v17

    #@184
    iput-object v0, v9, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@186
    .line 396
    move-object/from16 v0, p0

    #@188
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@18a
    move-object/from16 v17, v0

    #@18c
    move-object/from16 v0, v17

    #@18e
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@191
    .line 397
    move-object v3, v9

    #@192
    .line 399
    .end local v9           #newConn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    :cond_192
    if-eqz v3, :cond_33f

    #@194
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@196
    move-object/from16 v17, v0

    #@198
    if-eqz v17, :cond_33f

    #@19a
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@19c
    move-object/from16 v17, v0

    #@19e
    const-string v18, "default"

    #@1a0
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@1a3
    move-result v17

    #@1a4
    if-eqz v17, :cond_33f

    #@1a6
    .line 400
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@1a8
    move-object/from16 v17, v0

    #@1aa
    move-object/from16 v0, p0

    #@1ac
    move-object/from16 v1, v17

    #@1ae
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/LGDataRecovery;->hasDefaultRouteInfo(Ljava/lang/String;)Z

    #@1b1
    move-result v17

    #@1b2
    if-nez v17, :cond_20b

    #@1b4
    .line 401
    const-string v17, "Default connection doesn\'t have Default Route"

    #@1b6
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@1b9
    .line 402
    new-instance v17, Ljava/lang/StringBuilder;

    #@1bb
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@1be
    const-string v18, "STALL_SYPTOM "

    #@1c0
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c3
    move-result-object v17

    #@1c4
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@1c6
    move-object/from16 v18, v0

    #@1c8
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v17

    #@1cc
    const-string v18, " / "

    #@1ce
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v17

    #@1d2
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@1d4
    move-object/from16 v18, v0

    #@1d6
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v17

    #@1da
    const-string v18, " :: NO_ROUTE_INFO"

    #@1dc
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v17

    #@1e0
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e3
    move-result-object v17

    #@1e4
    const-string v18, "datastallinfo.log"

    #@1e6
    invoke-static/range {v17 .. v18}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@1e9
    .line 403
    new-instance v17, Ljava/lang/StringBuilder;

    #@1eb
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@1ee
    const-string v18, "     connection("

    #@1f0
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v17

    #@1f4
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@1f6
    move-object/from16 v18, v0

    #@1f8
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fb
    move-result-object v17

    #@1fc
    const-string v18, ") for Default DNS doesn\'t have Default Route"

    #@1fe
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@201
    move-result-object v17

    #@202
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@205
    move-result-object v17

    #@206
    const-string v18, "dnsfailhistory.log"

    #@208
    invoke-static/range {v17 .. v18}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@20b
    .line 406
    :cond_20b
    move-object/from16 v0, p0

    #@20d
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/LGDataRecovery;->getAPTrafficStat(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@210
    move-result-object v10

    #@211
    .line 408
    .local v10, stat:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;
    if-nez v10, :cond_21a

    #@213
    .line 409
    const-string v17, "onDnsFailObserved()... Failure to get getAPTrafficStat()"

    #@215
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@218
    goto/16 :goto_3e

    #@21a
    .line 413
    :cond_21a
    move-object/from16 v0, p0

    #@21c
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->lastStatForDnsFail:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@21e
    move-object/from16 v17, v0

    #@220
    if-nez v17, :cond_22c

    #@222
    .line 414
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@224
    move-object/from16 v17, v0

    #@226
    move-object/from16 v0, v17

    #@228
    move-object/from16 v1, p0

    #@22a
    iput-object v0, v1, Lcom/android/internal/telephony/LGDataRecovery;->lastStatForDnsFail:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@22c
    .line 416
    :cond_22c
    iget-wide v0, v10, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@22e
    move-wide/from16 v17, v0

    #@230
    move-object/from16 v0, p0

    #@232
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->lastStatForDnsFail:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@234
    move-object/from16 v19, v0

    #@236
    move-object/from16 v0, v19

    #@238
    iget-wide v0, v0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@23a
    move-wide/from16 v19, v0

    #@23c
    sub-long v17, v17, v19

    #@23e
    const-wide/16 v19, 0x3e8

    #@240
    div-long v15, v17, v19

    #@242
    .line 417
    .local v15, timefromlastDnsFail:J
    const-wide/16 v17, 0x6

    #@244
    cmp-long v17, v15, v17

    #@246
    if-lez v17, :cond_3e

    #@248
    .line 418
    iget-wide v0, v10, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@24a
    move-wide/from16 v17, v0

    #@24c
    move-object/from16 v0, p0

    #@24e
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->lastStatForDnsFail:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@250
    move-object/from16 v19, v0

    #@252
    move-object/from16 v0, v19

    #@254
    iget-wide v0, v0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@256
    move-wide/from16 v19, v0

    #@258
    sub-long v7, v17, v19

    #@25a
    .line 419
    .local v7, increasedTxcnt:J
    iget-wide v0, v10, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@25c
    move-wide/from16 v17, v0

    #@25e
    move-object/from16 v0, p0

    #@260
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->lastStatForDnsFail:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@262
    move-object/from16 v19, v0

    #@264
    move-object/from16 v0, v19

    #@266
    iget-wide v0, v0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@268
    move-wide/from16 v19, v0

    #@26a
    sub-long v5, v17, v19

    #@26c
    .line 420
    .local v5, increasedRxcnt:J
    new-instance v17, Ljava/lang/StringBuilder;

    #@26e
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@271
    const-string v18, "increasedTxcnt : "

    #@273
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@276
    move-result-object v17

    #@277
    move-object/from16 v0, v17

    #@279
    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@27c
    move-result-object v17

    #@27d
    const-string v18, "   increasedRxcnt : "

    #@27f
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@282
    move-result-object v17

    #@283
    move-object/from16 v0, v17

    #@285
    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@288
    move-result-object v17

    #@289
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28c
    move-result-object v17

    #@28d
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@290
    .line 421
    const-wide/16 v17, 0x1

    #@292
    cmp-long v17, v7, v17

    #@294
    if-gez v17, :cond_2db

    #@296
    .line 422
    const-string v17, "onDnsFailObserved no Tx increase & route info exist"

    #@298
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@29b
    .line 423
    new-instance v17, Ljava/lang/StringBuilder;

    #@29d
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@2a0
    const-string v18, "STALL_SYPTOM "

    #@2a2
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a5
    move-result-object v17

    #@2a6
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@2a8
    move-object/from16 v18, v0

    #@2aa
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ad
    move-result-object v17

    #@2ae
    const-string v18, " / "

    #@2b0
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b3
    move-result-object v17

    #@2b4
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@2b6
    move-object/from16 v18, v0

    #@2b8
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v17

    #@2bc
    const-string v18, " :: NO_TX_INCREASE_ON_DNSFAIL"

    #@2be
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v17

    #@2c2
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c5
    move-result-object v17

    #@2c6
    const-string v18, "datastallinfo.log"

    #@2c8
    invoke-static/range {v17 .. v18}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@2cb
    .line 424
    const-string v17, "     NO_TX_INCREASE from LAST DNS FAIL"

    #@2cd
    const-string v18, "dnsfailhistory.log"

    #@2cf
    invoke-static/range {v17 .. v18}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@2d2
    .line 426
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/LGDataRecovery;->dumpIptableInfo()V

    #@2d5
    .line 445
    :cond_2d5
    :goto_2d5
    move-object/from16 v0, p0

    #@2d7
    iput-object v10, v0, Lcom/android/internal/telephony/LGDataRecovery;->lastStatForDnsFail:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@2d9
    goto/16 :goto_3e

    #@2db
    .line 430
    :cond_2db
    const-wide/16 v17, 0x0

    #@2dd
    cmp-long v17, v7, v17

    #@2df
    if-lez v17, :cond_326

    #@2e1
    const-wide/16 v17, 0x1

    #@2e3
    cmp-long v17, v5, v17

    #@2e5
    if-gez v17, :cond_326

    #@2e7
    .line 431
    const-string v17, "onDnsFailObserved only Tx increase & route info exist"

    #@2e9
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@2ec
    .line 432
    const-string v17, "     ONLY TX_INCREASE from LAST DNS FAIL"

    #@2ee
    const-string v18, "dnsfailhistory.log"

    #@2f0
    invoke-static/range {v17 .. v18}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@2f3
    .line 434
    iget-wide v0, v10, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@2f5
    move-wide/from16 v17, v0

    #@2f7
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@2f9
    move-object/from16 v19, v0

    #@2fb
    move-object/from16 v0, v19

    #@2fd
    iget-wide v0, v0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@2ff
    move-wide/from16 v19, v0

    #@301
    sub-long v17, v17, v19

    #@303
    const-wide/16 v19, 0x3e8

    #@305
    div-long v11, v17, v19

    #@307
    .line 435
    .restart local v11       #time:J
    const-wide/16 v17, 0x3c

    #@309
    cmp-long v17, v11, v17

    #@30b
    if-lez v17, :cond_2d5

    #@30d
    .line 436
    move-object/from16 v0, p0

    #@30f
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@311
    move-object/from16 v17, v0

    #@313
    if-eqz v17, :cond_2d5

    #@315
    .line 437
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/LGDataRecovery;->stopDataStallAlarm()V

    #@318
    .line 438
    move-object/from16 v0, p0

    #@31a
    iget v0, v0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmTag:I

    #@31c
    move/from16 v17, v0

    #@31e
    move-object/from16 v0, p0

    #@320
    move/from16 v1, v17

    #@322
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/LGDataRecovery;->onDataStallAlarmExpired(I)V

    #@325
    goto :goto_2d5

    #@326
    .line 441
    .end local v11           #time:J
    :cond_326
    const-wide/16 v17, 0x0

    #@328
    cmp-long v17, v7, v17

    #@32a
    if-lez v17, :cond_2d5

    #@32c
    const-wide/16 v17, 0x0

    #@32e
    cmp-long v17, v5, v17

    #@330
    if-lez v17, :cond_2d5

    #@332
    .line 442
    const-string v17, "onDnsFailObserved both Tx Rx increase ignore DNS fail report"

    #@334
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@337
    .line 443
    const-string v17, "     TX_RX_INCREASE from LAST DNS FAIL "

    #@339
    const-string v18, "dnsfailhistory.log"

    #@33b
    invoke-static/range {v17 .. v18}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@33e
    goto :goto_2d5

    #@33f
    .line 448
    .end local v5           #increasedRxcnt:J
    .end local v7           #increasedTxcnt:J
    .end local v10           #stat:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;
    .end local v15           #timefromlastDnsFail:J
    :cond_33f
    const-string v17, "onDnsFailObserved Non Default connection has main DNS route info Don\'t care this case"

    #@341
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@344
    goto/16 :goto_3e
.end method

.method private onScreenOFF()V
    .registers 2

    #@0
    .prologue
    .line 628
    const-string v0, "onScreenOFF"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@5
    .line 629
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->reStartDataStallAlarm()V

    #@8
    .line 631
    return-void
.end method

.method private onScreenON()V
    .registers 2

    #@0
    .prologue
    .line 635
    const-string v0, "onScreenON"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@5
    .line 636
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->reStartDataStallAlarm()V

    #@8
    .line 638
    return-void
.end method


# virtual methods
.method public dispose()V
    .registers 6

    #@0
    .prologue
    .line 892
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseHandlerThread:Landroid/os/HandlerThread;

    #@2
    invoke-virtual {v2}, Landroid/os/HandlerThread;->isAlive()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_d

    #@8
    .line 893
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseHandlerThread:Landroid/os/HandlerThread;

    #@a
    invoke-virtual {v2}, Landroid/os/HandlerThread;->quit()Z

    #@d
    .line 896
    :cond_d
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@12
    move-result-object v2

    #@13
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@15
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@18
    .line 897
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1a
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@1d
    move-result-object v2

    #@1e
    if-eqz v2, :cond_32

    #@20
    .line 899
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@22
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/CallTracker;->unregisterForVoiceCallEnded(Landroid/os/Handler;)V

    #@29
    .line 900
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2b
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/CallTracker;->unregisterForVoiceCallStarted(Landroid/os/Handler;)V

    #@32
    .line 903
    :cond_32
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@34
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@37
    move-result-object v2

    #@38
    if-eqz v2, :cond_5e

    #@3a
    .line 905
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3c
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/ServiceStateTracker;->unregisterForDataConnectionAttached(Landroid/os/Handler;)V

    #@43
    .line 906
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@45
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/ServiceStateTracker;->unregisterForDataConnectionDetached(Landroid/os/Handler;)V

    #@4c
    .line 907
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4e
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/ServiceStateTracker;->unregisterForPsRestrictedEnabled(Landroid/os/Handler;)V

    #@55
    .line 908
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@57
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/ServiceStateTracker;->unregisterForPsRestrictedDisabled(Landroid/os/Handler;)V

    #@5e
    .line 911
    :cond_5e
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@60
    if-nez v2, :cond_6e

    #@62
    .line 912
    const-string v2, "network_management"

    #@64
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@67
    move-result-object v0

    #@68
    .line 913
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@6b
    move-result-object v2

    #@6c
    iput-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@6e
    .line 917
    .end local v0           #b:Landroid/os/IBinder;
    :cond_6e
    :try_start_6e
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNetdObserverEx:Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;

    #@70
    if-eqz v2, :cond_79

    #@72
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@74
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNetdObserverEx:Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;

    #@76
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->unregisterObserverEx(Landroid/net/INetworkManagementEventObserverEx;)V
    :try_end_79
    .catch Ljava/lang/Exception; {:try_start_6e .. :try_end_79} :catch_7f

    #@79
    .line 922
    :cond_79
    :goto_79
    const-string v2, "LGDataRecovery has disposed."

    #@7b
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@7e
    .line 923
    return-void

    #@7f
    .line 918
    :catch_7f
    move-exception v1

    #@80
    .line 919
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "GSM"

    #@82
    new-instance v3, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v4, "Could not unregister InterfaceObserver "

    #@89
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v3

    #@8d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v3

    #@91
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v3

    #@95
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    goto :goto_79
.end method

.method protected dumpDnsFail(Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;)V
    .registers 5
    .parameter "info"

    #@0
    .prologue
    .line 1683
    const-string v1, "net.dns1"

    #@2
    const-string v2, ""

    #@4
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 1685
    .local v0, dnsAddr:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "hostname: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget-object v2, p1, Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;->hostName:Ljava/lang/String;

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " DNS server addr: "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, " iface: "

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->findIfaceforNonPidDns()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    const-string v2, " isWifiConnected: "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    iget-boolean v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIsWifiConnected:Z

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    const-string v2, "dnsfailhistory.log"

    #@43
    invoke-static {v1, v2}, Lcom/android/internal/telephony/LGDataRecovery;->logF(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 1687
    return-void
.end method

.method protected dumpIptableInfo()V
    .registers 1

    #@0
    .prologue
    .line 1759
    return-void
.end method

.method protected dumpNetInfo()V
    .registers 2

    #@0
    .prologue
    .line 1691
    new-instance v0, Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;

    #@2
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@5
    .line 1692
    .local v0, thread:Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;
    invoke-virtual {v0}, Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;->start()V

    #@8
    .line 1730
    return-void
.end method

.method public errorCodeToString(I)Ljava/lang/String;
    .registers 3
    .parameter "errorCode"

    #@0
    .prologue
    .line 1535
    const/4 v0, 0x0

    #@1
    .line 1537
    .local v0, result:Ljava/lang/String;
    packed-switch p1, :pswitch_data_32

    #@4
    .line 1595
    const-string v0, "unknown error code."

    #@6
    .line 1599
    :goto_6
    return-object v0

    #@7
    .line 1539
    :pswitch_7
    const-string v0, "address family for hostname not supported."

    #@9
    .line 1540
    goto :goto_6

    #@a
    .line 1543
    :pswitch_a
    const-string v0, "temporary failure in name resolution."

    #@c
    .line 1544
    goto :goto_6

    #@d
    .line 1547
    :pswitch_d
    const-string v0, "invalid value for ai_flags."

    #@f
    .line 1548
    goto :goto_6

    #@10
    .line 1551
    :pswitch_10
    const-string v0, "non-recoverable failure in name resolution."

    #@12
    .line 1552
    goto :goto_6

    #@13
    .line 1555
    :pswitch_13
    const-string v0, "ai_family not supported."

    #@15
    .line 1556
    goto :goto_6

    #@16
    .line 1559
    :pswitch_16
    const-string v0, "memory allocation failure."

    #@18
    .line 1560
    goto :goto_6

    #@19
    .line 1563
    :pswitch_19
    const-string v0, "no address associated with hostname."

    #@1b
    .line 1564
    goto :goto_6

    #@1c
    .line 1567
    :pswitch_1c
    const-string v0, "hostname nor servname provided, or not known."

    #@1e
    .line 1568
    goto :goto_6

    #@1f
    .line 1571
    :pswitch_1f
    const-string v0, "servname not supported for ai_socktype."

    #@21
    .line 1572
    goto :goto_6

    #@22
    .line 1575
    :pswitch_22
    const-string v0, "ai_socktype not supported."

    #@24
    .line 1576
    goto :goto_6

    #@25
    .line 1579
    :pswitch_25
    const-string v0, "system error returned in errno."

    #@27
    .line 1580
    goto :goto_6

    #@28
    .line 1583
    :pswitch_28
    const-string v0, "invalid value for hints."

    #@2a
    .line 1584
    goto :goto_6

    #@2b
    .line 1587
    :pswitch_2b
    const-string v0, "resolved protocol is unknown."

    #@2d
    .line 1588
    goto :goto_6

    #@2e
    .line 1591
    :pswitch_2e
    const-string v0, "argument buffer overflow."

    #@30
    .line 1592
    goto :goto_6

    #@31
    .line 1537
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_7
        :pswitch_a
        :pswitch_d
        :pswitch_10
        :pswitch_13
        :pswitch_16
        :pswitch_19
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_2b
        :pswitch_2e
    .end packed-switch
.end method

.method public findConnInfowithIface(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    .registers 6
    .parameter "iface"

    #@0
    .prologue
    .line 1098
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "findConnInfowithIface "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@16
    .line 1099
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@18
    if-eqz v2, :cond_5a

    #@1a
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v2

    #@20
    if-lez v2, :cond_5a

    #@22
    .line 1100
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@27
    move-result-object v1

    #@28
    .local v1, i$:Ljava/util/Iterator;
    :cond_28
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@2b
    move-result v2

    #@2c
    if-eqz v2, :cond_55

    #@2e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@31
    move-result-object v0

    #@32
    check-cast v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@34
    .line 1101
    .local v0, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@36
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v2

    #@3a
    if-eqz v2, :cond_28

    #@3c
    .line 1102
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, " find conn iface = "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    iget-object v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@54
    .line 1109
    .end local v0           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    .end local v1           #i$:Ljava/util/Iterator;
    :goto_54
    return-object v0

    #@55
    .line 1106
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_55
    const-string v2, "find nothing"

    #@57
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@5a
    .line 1108
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_5a
    const-string v2, "mConnectionInfos has no conn List"

    #@5c
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@5f
    .line 1109
    const/4 v0, 0x0

    #@60
    goto :goto_54
.end method

.method public findDefaultConnInfo()Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    .registers 5

    #@0
    .prologue
    .line 1113
    const-string v2, "findDefaultConnInfo"

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@5
    .line 1114
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@7
    if-eqz v2, :cond_5d

    #@9
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v2

    #@f
    if-lez v2, :cond_5d

    #@11
    .line 1115
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v1

    #@17
    .local v1, i$:Ljava/util/Iterator;
    :cond_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_56

    #@1d
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@23
    .line 1116
    .local v0, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@25
    if-eqz v2, :cond_17

    #@27
    iget-object v2, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@29
    const-string v3, "default"

    #@2b
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@2e
    move-result v2

    #@2f
    if-eqz v2, :cond_17

    #@31
    .line 1117
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v3, " find default conn  iface :: "

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    iget-object v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    const-string v3, " APN:: "

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    iget-object v3, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@55
    .line 1125
    .end local v0           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    .end local v1           #i$:Ljava/util/Iterator;
    :goto_55
    return-object v0

    #@56
    .line 1121
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_56
    const-string v2, "find nothing"

    #@58
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@5b
    .line 1125
    .end local v1           #i$:Ljava/util/Iterator;
    :goto_5b
    const/4 v0, 0x0

    #@5c
    goto :goto_55

    #@5d
    .line 1123
    :cond_5d
    const-string v2, "mConnectionInfos has no conn List"

    #@5f
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@62
    goto :goto_5b
.end method

.method public findIfaceforNonPidDns()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 1458
    const-string v4, "net.dns1"

    #@2
    const-string v5, ""

    #@4
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 1459
    .local v1, dnsAddr:Ljava/lang/String;
    const/4 v0, 0x0

    #@9
    .line 1461
    .local v0, defaultIface:Ljava/lang/String;
    const-string v4, ""

    #@b
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_14

    #@11
    .line 1462
    const-string v4, ""

    #@13
    .line 1477
    :goto_13
    return-object v4

    #@14
    .line 1465
    :cond_14
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->getAllRouteInfos()Ljava/util/ArrayList;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1b
    move-result-object v2

    #@1c
    .local v2, i$:Ljava/util/Iterator;
    :cond_1c
    :goto_1c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_66

    #@22
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    check-cast v3, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;

    #@28
    .line 1466
    .local v3, ri:Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;
    iget-object v4, v3, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->dstAddr:Ljava/lang/String;

    #@2a
    invoke-static {v4}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@2d
    move-result-object v4

    #@2e
    invoke-static {v1}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v4, v5}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v4

    #@36
    if-eqz v4, :cond_3b

    #@38
    .line 1467
    iget-object v4, v3, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->iface:Ljava/lang/String;

    #@3a
    goto :goto_13

    #@3b
    .line 1468
    :cond_3b
    iget-object v4, v3, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->dstAddr:Ljava/lang/String;

    #@3d
    invoke-static {v4}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@40
    move-result-object v4

    #@41
    const-string v5, "0.0.0.0"

    #@43
    invoke-static {v5}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v4, v5}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v4

    #@4b
    if-eqz v4, :cond_4f

    #@4d
    if-eqz v0, :cond_63

    #@4f
    :cond_4f
    iget-object v4, v3, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->dstAddr:Ljava/lang/String;

    #@51
    invoke-static {v4}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@54
    move-result-object v4

    #@55
    const-string v5, "::"

    #@57
    invoke-static {v5}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v4, v5}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v4

    #@5f
    if-eqz v4, :cond_1c

    #@61
    if-nez v0, :cond_1c

    #@63
    .line 1470
    :cond_63
    iget-object v0, v3, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->iface:Ljava/lang/String;

    #@65
    goto :goto_1c

    #@66
    .line 1474
    .end local v3           #ri:Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;
    :cond_66
    if-eqz v0, :cond_6a

    #@68
    move-object v4, v0

    #@69
    .line 1475
    goto :goto_13

    #@6a
    .line 1477
    :cond_6a
    const-string v4, ""

    #@6c
    goto :goto_13
.end method

.method public getAPTrafficStat(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;
    .registers 9
    .parameter "iface"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1189
    new-instance v1, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@3
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@6
    .line 1193
    .local v1, resultCnt:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;
    :try_start_6
    sget-object v3, Lcom/android/internal/telephony/LGDataRecovery;->sStatsService:Landroid/net/INetworkStatsService;

    #@8
    if-nez v3, :cond_16

    #@a
    .line 1194
    const-string v3, "netstats"

    #@c
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@f
    move-result-object v3

    #@10
    invoke-static {v3}, Landroid/net/INetworkStatsService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkStatsService;

    #@13
    move-result-object v3

    #@14
    sput-object v3, Lcom/android/internal/telephony/LGDataRecovery;->sStatsService:Landroid/net/INetworkStatsService;

    #@16
    .line 1198
    :cond_16
    sget-object v3, Lcom/android/internal/telephony/LGDataRecovery;->sStatsService:Landroid/net/INetworkStatsService;

    #@18
    if-eqz v3, :cond_35

    #@1a
    .line 1199
    if-eqz p1, :cond_2e

    #@1c
    .line 1200
    iget-wide v3, v1, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@1e
    invoke-static {p1}, Landroid/net/TrafficStats;->getTxPackets(Ljava/lang/String;)J

    #@21
    move-result-wide v5

    #@22
    add-long/2addr v3, v5

    #@23
    iput-wide v3, v1, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@25
    .line 1201
    iget-wide v3, v1, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@27
    invoke-static {p1}, Landroid/net/TrafficStats;->getRxPackets(Ljava/lang/String;)J

    #@2a
    move-result-wide v5

    #@2b
    add-long/2addr v3, v5

    #@2c
    iput-wide v3, v1, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_2e} :catch_3c

    #@2e
    .line 1214
    :cond_2e
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@31
    move-result-wide v2

    #@32
    iput-wide v2, v1, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@34
    .line 1215
    .end local v1           #resultCnt:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;
    :goto_34
    return-object v1

    #@35
    .line 1205
    .restart local v1       #resultCnt:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;
    :cond_35
    :try_start_35
    const-string v3, "Stats Service is null"

    #@37
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_3a} :catch_3c

    #@3a
    move-object v1, v2

    #@3b
    .line 1206
    goto :goto_34

    #@3c
    .line 1209
    :catch_3c
    move-exception v0

    #@3d
    .line 1211
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "fail to get Stats Service"

    #@3f
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@42
    move-object v1, v2

    #@43
    .line 1212
    goto :goto_34
.end method

.method public getAllRouteInfos()Ljava/util/ArrayList;
    .registers 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1373
    const-string v24, "/proc/net/route"

    #@2
    move-object/from16 v0, p0

    #@4
    move-object/from16 v1, v24

    #@6
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/LGDataRecovery;->getFileSystemInfo(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v15

    #@a
    .line 1374
    .local v15, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v22, Ljava/util/ArrayList;

    #@c
    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    #@f
    .line 1376
    .local v22, routes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v24

    #@13
    if-lez v24, :cond_1c

    #@15
    .line 1377
    const/16 v24, 0x0

    #@17
    move/from16 v0, v24

    #@19
    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1c
    .line 1384
    :cond_1c
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1f
    move-result-object v12

    #@20
    .local v12, i$:Ljava/util/Iterator;
    :cond_20
    :goto_20
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@23
    move-result v24

    #@24
    if-eqz v24, :cond_db

    #@26
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@29
    move-result-object v23

    #@2a
    check-cast v23, Ljava/lang/String;

    #@2c
    .line 1385
    .local v23, s:Ljava/lang/String;
    const-string v24, "\t"

    #@2e
    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@31
    move-result-object v7

    #@32
    .line 1387
    .local v7, fields:[Ljava/lang/String;
    array-length v0, v7

    #@33
    move/from16 v24, v0

    #@35
    const/16 v25, 0x8

    #@37
    move/from16 v0, v24

    #@39
    move/from16 v1, v25

    #@3b
    if-le v0, v1, :cond_20

    #@3d
    .line 1388
    const/16 v24, 0x0

    #@3f
    aget-object v13, v7, v24

    #@41
    .line 1389
    .local v13, iface:Ljava/lang/String;
    const/16 v24, 0x1

    #@43
    aget-object v4, v7, v24

    #@45
    .line 1390
    .local v4, dest:Ljava/lang/String;
    const/16 v24, 0x2

    #@47
    aget-object v9, v7, v24

    #@49
    .line 1391
    .local v9, gate:Ljava/lang/String;
    const/16 v24, 0x3

    #@4b
    aget-object v8, v7, v24

    #@4d
    .line 1392
    .local v8, flags:Ljava/lang/String;
    const/16 v24, 0x7

    #@4f
    aget-object v16, v7, v24

    #@51
    .line 1394
    .local v16, mask:Ljava/lang/String;
    const/16 v24, 0x8

    #@53
    :try_start_53
    aget-object v24, v7, v24

    #@55
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@58
    move-result v17

    #@59
    .line 1396
    .local v17, mtu:I
    const/16 v24, 0x10

    #@5b
    move/from16 v0, v24

    #@5d
    invoke-static {v4, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@60
    move-result-wide v24

    #@61
    move-wide/from16 v0, v24

    #@63
    long-to-int v0, v0

    #@64
    move/from16 v24, v0

    #@66
    invoke-static/range {v24 .. v24}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    #@69
    move-result-object v5

    #@6a
    .line 1398
    .local v5, destAddr:Ljava/net/InetAddress;
    const/16 v24, 0x10

    #@6c
    move-object/from16 v0, v16

    #@6e
    move/from16 v1, v24

    #@70
    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@73
    move-result-wide v24

    #@74
    move-wide/from16 v0, v24

    #@76
    long-to-int v0, v0

    #@77
    move/from16 v24, v0

    #@79
    invoke-static/range {v24 .. v24}, Landroid/net/NetworkUtils;->netmaskIntToPrefixLength(I)I

    #@7c
    move-result v19

    #@7d
    .line 1401
    .local v19, prefixLength:I
    new-instance v14, Landroid/net/LinkAddress;

    #@7f
    move/from16 v0, v19

    #@81
    invoke-direct {v14, v5, v0}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@84
    .line 1404
    .local v14, linkAddress:Landroid/net/LinkAddress;
    const/16 v24, 0x10

    #@86
    move/from16 v0, v24

    #@88
    invoke-static {v9, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@8b
    move-result-wide v24

    #@8c
    move-wide/from16 v0, v24

    #@8e
    long-to-int v0, v0

    #@8f
    move/from16 v24, v0

    #@91
    invoke-static/range {v24 .. v24}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    #@94
    move-result-object v11

    #@95
    .line 1407
    .local v11, gatewayAddr:Ljava/net/InetAddress;
    new-instance v20, Landroid/net/RouteInfo;

    #@97
    move-object/from16 v0, v20

    #@99
    invoke-direct {v0, v14, v11}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    #@9c
    .line 1408
    .local v20, route:Landroid/net/RouteInfo;
    new-instance v21, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;

    #@9e
    move-object/from16 v0, v21

    #@a0
    move-object/from16 v1, p0

    #@a2
    move-object/from16 v2, v20

    #@a4
    move/from16 v3, v17

    #@a6
    invoke-direct {v0, v1, v13, v2, v3}, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;-><init>(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;Landroid/net/RouteInfo;I)V

    #@a9
    .line 1410
    .local v21, routeWithIface:Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;
    move-object/from16 v0, v22

    #@ab
    move-object/from16 v1, v21

    #@ad
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_b0
    .catch Ljava/lang/Exception; {:try_start_53 .. :try_end_b0} :catch_b2

    #@b0
    goto/16 :goto_20

    #@b2
    .line 1411
    .end local v5           #destAddr:Ljava/net/InetAddress;
    .end local v11           #gatewayAddr:Ljava/net/InetAddress;
    .end local v14           #linkAddress:Landroid/net/LinkAddress;
    .end local v17           #mtu:I
    .end local v19           #prefixLength:I
    .end local v20           #route:Landroid/net/RouteInfo;
    .end local v21           #routeWithIface:Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;
    :catch_b2
    move-exception v6

    #@b3
    .line 1412
    .local v6, e:Ljava/lang/Exception;
    new-instance v24, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v25, "Error parsing route "

    #@ba
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v24

    #@be
    move-object/from16 v0, v24

    #@c0
    move-object/from16 v1, v23

    #@c2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v24

    #@c6
    const-string v25, " : "

    #@c8
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v24

    #@cc
    move-object/from16 v0, v24

    #@ce
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v24

    #@d2
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v24

    #@d6
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@d9
    goto/16 :goto_20

    #@db
    .line 1419
    .end local v4           #dest:Ljava/lang/String;
    .end local v6           #e:Ljava/lang/Exception;
    .end local v7           #fields:[Ljava/lang/String;
    .end local v8           #flags:Ljava/lang/String;
    .end local v9           #gate:Ljava/lang/String;
    .end local v13           #iface:Ljava/lang/String;
    .end local v16           #mask:Ljava/lang/String;
    .end local v23           #s:Ljava/lang/String;
    :cond_db
    const-string v24, "/proc/net/ipv6_route"

    #@dd
    move-object/from16 v0, p0

    #@df
    move-object/from16 v1, v24

    #@e1
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/LGDataRecovery;->getFileSystemInfo(Ljava/lang/String;)Ljava/util/ArrayList;

    #@e4
    move-result-object v15

    #@e5
    .line 1421
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@e8
    move-result-object v12

    #@e9
    :cond_e9
    :goto_e9
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@ec
    move-result v24

    #@ed
    if-eqz v24, :cond_17a

    #@ef
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f2
    move-result-object v23

    #@f3
    check-cast v23, Ljava/lang/String;

    #@f5
    .line 1422
    .restart local v23       #s:Ljava/lang/String;
    const-string v24, "\\s+"

    #@f7
    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@fa
    move-result-object v7

    #@fb
    .line 1423
    .restart local v7       #fields:[Ljava/lang/String;
    array-length v0, v7

    #@fc
    move/from16 v24, v0

    #@fe
    const/16 v25, 0x9

    #@100
    move/from16 v0, v24

    #@102
    move/from16 v1, v25

    #@104
    if-le v0, v1, :cond_e9

    #@106
    .line 1424
    const/16 v24, 0x9

    #@108
    aget-object v24, v7, v24

    #@10a
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@10d
    move-result-object v13

    #@10e
    .line 1426
    .restart local v13       #iface:Ljava/lang/String;
    const/16 v24, 0x0

    #@110
    aget-object v4, v7, v24

    #@112
    .line 1427
    .restart local v4       #dest:Ljava/lang/String;
    const/16 v24, 0x1

    #@114
    aget-object v18, v7, v24

    #@116
    .line 1428
    .local v18, prefix:Ljava/lang/String;
    const/16 v24, 0x4

    #@118
    aget-object v9, v7, v24

    #@11a
    .line 1432
    .restart local v9       #gate:Ljava/lang/String;
    const/16 v24, 0x10

    #@11c
    :try_start_11c
    move-object/from16 v0, v18

    #@11e
    move/from16 v1, v24

    #@120
    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@123
    move-result v19

    #@124
    .line 1436
    .restart local v19       #prefixLength:I
    invoke-static {v4}, Landroid/net/NetworkUtils;->hexToInet6Address(Ljava/lang/String;)Ljava/net/InetAddress;

    #@127
    move-result-object v5

    #@128
    .line 1437
    .restart local v5       #destAddr:Ljava/net/InetAddress;
    new-instance v14, Landroid/net/LinkAddress;

    #@12a
    move/from16 v0, v19

    #@12c
    invoke-direct {v14, v5, v0}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@12f
    .line 1439
    .restart local v14       #linkAddress:Landroid/net/LinkAddress;
    invoke-static {v9}, Landroid/net/NetworkUtils;->hexToInet6Address(Ljava/lang/String;)Ljava/net/InetAddress;

    #@132
    move-result-object v10

    #@133
    .line 1441
    .local v10, gateAddr:Ljava/net/InetAddress;
    new-instance v20, Landroid/net/RouteInfo;

    #@135
    move-object/from16 v0, v20

    #@137
    invoke-direct {v0, v14, v10}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    #@13a
    .line 1442
    .restart local v20       #route:Landroid/net/RouteInfo;
    new-instance v21, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;

    #@13c
    const/16 v24, 0x0

    #@13e
    move-object/from16 v0, v21

    #@140
    move-object/from16 v1, p0

    #@142
    move-object/from16 v2, v20

    #@144
    move/from16 v3, v24

    #@146
    invoke-direct {v0, v1, v13, v2, v3}, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;-><init>(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;Landroid/net/RouteInfo;I)V

    #@149
    .line 1444
    .restart local v21       #routeWithIface:Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;
    move-object/from16 v0, v22

    #@14b
    move-object/from16 v1, v21

    #@14d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_150
    .catch Ljava/lang/Exception; {:try_start_11c .. :try_end_150} :catch_151

    #@150
    goto :goto_e9

    #@151
    .line 1445
    .end local v5           #destAddr:Ljava/net/InetAddress;
    .end local v10           #gateAddr:Ljava/net/InetAddress;
    .end local v14           #linkAddress:Landroid/net/LinkAddress;
    .end local v19           #prefixLength:I
    .end local v20           #route:Landroid/net/RouteInfo;
    .end local v21           #routeWithIface:Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;
    :catch_151
    move-exception v6

    #@152
    .line 1446
    .restart local v6       #e:Ljava/lang/Exception;
    new-instance v24, Ljava/lang/StringBuilder;

    #@154
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@157
    const-string v25, "Error parsing route "

    #@159
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v24

    #@15d
    move-object/from16 v0, v24

    #@15f
    move-object/from16 v1, v23

    #@161
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v24

    #@165
    const-string v25, " : "

    #@167
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v24

    #@16b
    move-object/from16 v0, v24

    #@16d
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v24

    #@171
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@174
    move-result-object v24

    #@175
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@178
    goto/16 :goto_e9

    #@17a
    .line 1453
    .end local v4           #dest:Ljava/lang/String;
    .end local v6           #e:Ljava/lang/Exception;
    .end local v7           #fields:[Ljava/lang/String;
    .end local v9           #gate:Ljava/lang/String;
    .end local v13           #iface:Ljava/lang/String;
    .end local v18           #prefix:Ljava/lang/String;
    .end local v23           #s:Ljava/lang/String;
    :cond_17a
    return-object v22
.end method

.method public getCPTrafficStat(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;
    .registers 7
    .parameter "APN"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1220
    :try_start_1
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_2} :catch_3f

    #@2
    .line 1221
    const/4 v2, 0x0

    #@3
    :try_start_3
    sput-boolean v2, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseFlag:Z

    #@5
    .line 1223
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseHandler:Landroid/os/Handler;

    #@7
    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    #@a
    move-result-object v1

    #@b
    .line 1225
    .local v1, msg:Landroid/os/Message;
    const v2, 0x42072

    #@e
    iput v2, v1, Landroid/os/Message;->what:I

    #@10
    .line 1226
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@12
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@14
    invoke-static {v2}, Lcom/android/internal/telephony/LgeRIL;->getLgeRIL(Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/LgeRIL;

    #@17
    move-result-object v2

    #@18
    if-eqz v2, :cond_25

    #@1a
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1c
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1e
    invoke-static {v2}, Lcom/android/internal/telephony/LgeRIL;->getLgeRIL(Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/LgeRIL;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2, p1, v1}, Lcom/android/internal/telephony/LgeRIL;->getModemPacketCount(Ljava/lang/String;Landroid/os/Message;)V

    #@25
    .line 1227
    :cond_25
    const-string v2, "sent getModemPacketCount request"

    #@27
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@2a
    .line 1229
    const-string v2, "wait for notify from EVENT_GET_MODEM_PACKET_COUNT_DONE event"

    #@2c
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@2f
    .line 1230
    const-wide/16 v2, 0x3e8

    #@31
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V

    #@34
    .line 1231
    monitor-exit p0
    :try_end_35
    .catchall {:try_start_3 .. :try_end_35} :catchall_3c

    #@35
    .line 1236
    .end local v1           #msg:Landroid/os/Message;
    :goto_35
    sget-boolean v2, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseFlag:Z

    #@37
    if-eqz v2, :cond_44

    #@39
    .line 1238
    sget-object v2, Lcom/android/internal/telephony/LGDataRecovery;->mCP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@3b
    .line 1249
    :goto_3b
    return-object v2

    #@3c
    .line 1231
    :catchall_3c
    move-exception v2

    #@3d
    :try_start_3d
    monitor-exit p0
    :try_end_3e
    .catchall {:try_start_3d .. :try_end_3e} :catchall_3c

    #@3e
    :try_start_3e
    throw v2
    :try_end_3f
    .catch Ljava/lang/InterruptedException; {:try_start_3e .. :try_end_3f} :catch_3f

    #@3f
    .line 1232
    :catch_3f
    move-exception v0

    #@40
    .line 1233
    .local v0, e:Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    #@43
    goto :goto_35

    #@44
    .line 1240
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_44
    sget v2, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseFailCount:I

    #@46
    add-int/lit8 v2, v2, 0x1

    #@48
    sput v2, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseFailCount:I

    #@4a
    .line 1241
    new-instance v2, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v3, "timeout to get modem packet count. cnt= "

    #@51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    sget v3, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseFailCount:I

    #@57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@62
    .line 1242
    sget v2, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseFailCount:I

    #@64
    const/4 v3, 0x2

    #@65
    if-lt v2, v3, :cond_69

    #@67
    .line 1246
    sput v4, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseFailCount:I

    #@69
    .line 1249
    :cond_69
    new-instance v2, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@6b
    invoke-direct {v2, p0}, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@6e
    goto :goto_3b
.end method

.method public getFileSystemInfo(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 11
    .parameter "path"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1342
    const/4 v2, 0x0

    #@1
    .line 1343
    .local v2, fstream:Ljava/io/FileInputStream;
    new-instance v5, Ljava/util/ArrayList;

    #@3
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@6
    .line 1346
    .local v5, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_6
    new-instance v3, Ljava/io/FileInputStream;

    #@8
    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_6 .. :try_end_b} :catchall_4f
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_b} :catch_5f

    #@b
    .line 1347
    .end local v2           #fstream:Ljava/io/FileInputStream;
    .local v3, fstream:Ljava/io/FileInputStream;
    :try_start_b
    new-instance v4, Ljava/io/DataInputStream;

    #@d
    invoke-direct {v4, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@10
    .line 1348
    .local v4, in:Ljava/io/DataInputStream;
    new-instance v0, Ljava/io/BufferedReader;

    #@12
    new-instance v7, Ljava/io/InputStreamReader;

    #@14
    invoke-direct {v7, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@17
    invoke-direct {v0, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@1a
    .line 1353
    .local v0, br:Ljava/io/BufferedReader;
    :goto_1a
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@1d
    move-result-object v6

    #@1e
    .local v6, s:Ljava/lang/String;
    if-eqz v6, :cond_48

    #@20
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@23
    move-result v7

    #@24
    if-eqz v7, :cond_48

    #@26
    .line 1354
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_29
    .catchall {:try_start_b .. :try_end_29} :catchall_5c
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_29} :catch_2a

    #@29
    goto :goto_1a

    #@2a
    .line 1356
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v4           #in:Ljava/io/DataInputStream;
    .end local v6           #s:Ljava/lang/String;
    :catch_2a
    move-exception v1

    #@2b
    move-object v2, v3

    #@2c
    .line 1358
    .end local v3           #fstream:Ljava/io/FileInputStream;
    .local v1, ex:Ljava/io/IOException;
    .restart local v2       #fstream:Ljava/io/FileInputStream;
    :goto_2c
    :try_start_2c
    new-instance v7, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v8, "Error getting i/o stream. "

    #@33
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v7

    #@37
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v7

    #@3f
    invoke-static {v7}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V
    :try_end_42
    .catchall {:try_start_2c .. :try_end_42} :catchall_4f

    #@42
    .line 1360
    if-eqz v2, :cond_47

    #@44
    .line 1362
    :try_start_44
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_47
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_47} :catch_58

    #@47
    .line 1368
    .end local v1           #ex:Ljava/io/IOException;
    :cond_47
    :goto_47
    return-object v5

    #@48
    .line 1360
    .end local v2           #fstream:Ljava/io/FileInputStream;
    .restart local v0       #br:Ljava/io/BufferedReader;
    .restart local v3       #fstream:Ljava/io/FileInputStream;
    .restart local v4       #in:Ljava/io/DataInputStream;
    .restart local v6       #s:Ljava/lang/String;
    :cond_48
    if-eqz v3, :cond_4d

    #@4a
    .line 1362
    :try_start_4a
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4d} :catch_5a

    #@4d
    :cond_4d
    :goto_4d
    move-object v2, v3

    #@4e
    .line 1367
    .end local v3           #fstream:Ljava/io/FileInputStream;
    .restart local v2       #fstream:Ljava/io/FileInputStream;
    goto :goto_47

    #@4f
    .line 1360
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v4           #in:Ljava/io/DataInputStream;
    .end local v6           #s:Ljava/lang/String;
    :catchall_4f
    move-exception v7

    #@50
    :goto_50
    if-eqz v2, :cond_55

    #@52
    .line 1362
    :try_start_52
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_55
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_55} :catch_56

    #@55
    .line 1360
    :cond_55
    :goto_55
    throw v7

    #@56
    .line 1363
    :catch_56
    move-exception v8

    #@57
    goto :goto_55

    #@58
    .restart local v1       #ex:Ljava/io/IOException;
    :catch_58
    move-exception v7

    #@59
    goto :goto_47

    #@5a
    .end local v1           #ex:Ljava/io/IOException;
    .end local v2           #fstream:Ljava/io/FileInputStream;
    .restart local v0       #br:Ljava/io/BufferedReader;
    .restart local v3       #fstream:Ljava/io/FileInputStream;
    .restart local v4       #in:Ljava/io/DataInputStream;
    .restart local v6       #s:Ljava/lang/String;
    :catch_5a
    move-exception v7

    #@5b
    goto :goto_4d

    #@5c
    .line 1360
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v4           #in:Ljava/io/DataInputStream;
    .end local v6           #s:Ljava/lang/String;
    :catchall_5c
    move-exception v7

    #@5d
    move-object v2, v3

    #@5e
    .end local v3           #fstream:Ljava/io/FileInputStream;
    .restart local v2       #fstream:Ljava/io/FileInputStream;
    goto :goto_50

    #@5f
    .line 1356
    :catch_5f
    move-exception v1

    #@60
    goto :goto_2c
.end method

.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 269
    monitor-enter p0

    #@1
    :try_start_1
    iget v2, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v2, :pswitch_data_da

    #@6
    .line 347
    :pswitch_6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Undefine Message: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    iget v3, p1, Landroid/os/Message;->what:I

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@1e
    .line 348
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_27

    #@21
    .line 351
    :cond_21
    :goto_21
    monitor-exit p0

    #@22
    return-void

    #@23
    .line 271
    :pswitch_23
    :try_start_23
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->onScreenON()V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_27

    #@26
    goto :goto_21

    #@27
    .line 269
    :catchall_27
    move-exception v2

    #@28
    monitor-exit p0

    #@29
    throw v2

    #@2a
    .line 275
    :pswitch_2a
    :try_start_2a
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->onScreenOFF()V

    #@2d
    goto :goto_21

    #@2e
    .line 279
    :pswitch_2e
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@30
    instance-of v2, v2, Landroid/content/Intent;

    #@32
    if-eqz v2, :cond_3c

    #@34
    .line 280
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@36
    check-cast v2, Landroid/content/Intent;

    #@38
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/LGDataRecovery;->onDataStateConnected(Landroid/content/Intent;)V

    #@3b
    goto :goto_21

    #@3c
    .line 282
    :cond_3c
    const-string v2, "Data state changed w/o intent"

    #@3e
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@41
    goto :goto_21

    #@42
    .line 287
    :pswitch_42
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@44
    instance-of v2, v2, Landroid/content/Intent;

    #@46
    if-eqz v2, :cond_50

    #@48
    .line 288
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4a
    check-cast v2, Landroid/content/Intent;

    #@4c
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/LGDataRecovery;->onDataStateDisconnected(Landroid/content/Intent;)V

    #@4f
    goto :goto_21

    #@50
    .line 290
    :cond_50
    const-string v2, "Data state changed w/o intent"

    #@52
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@55
    goto :goto_21

    #@56
    .line 295
    :pswitch_56
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@58
    instance-of v2, v2, Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;

    #@5a
    if-eqz v2, :cond_64

    #@5c
    .line 296
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5e
    check-cast v2, Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;

    #@60
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/LGDataRecovery;->onDnsFailObserved(Lcom/android/internal/telephony/LGDataRecovery$DnsFailInfo;)V

    #@63
    goto :goto_21

    #@64
    .line 298
    :cond_64
    const-string v2, "DNS fail observed w/o result"

    #@66
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@69
    goto :goto_21

    #@6a
    .line 303
    :pswitch_6a
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@6c
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/LGDataRecovery;->onDataStallAlarmExpired(I)V

    #@6f
    goto :goto_21

    #@70
    .line 307
    :pswitch_70
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@73
    move-result-object v2

    #@74
    const-string v3, "iface"

    #@76
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@79
    move-result-object v1

    #@7a
    .line 308
    .local v1, iface:Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LGDataRecovery;->onConnCompletedCheckAlarmExpired(Ljava/lang/String;)V

    #@7d
    goto :goto_21

    #@7e
    .line 312
    .end local v1           #iface:Ljava/lang/String;
    :pswitch_7e
    const/4 v2, 0x1

    #@7f
    iput-boolean v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mVoicecall:Z

    #@81
    .line 313
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@83
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@86
    move-result-object v2

    #@87
    if-eqz v2, :cond_21

    #@89
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8b
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@8e
    move-result-object v2

    #@8f
    invoke-virtual {v2}, Lcom/android/internal/telephony/ServiceStateTracker;->isConcurrentVoiceAndDataAllowed()Z

    #@92
    move-result v2

    #@93
    if-nez v2, :cond_21

    #@95
    .line 315
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->stopDataStallAlarm()V

    #@98
    goto :goto_21

    #@99
    .line 320
    :pswitch_99
    const/4 v2, 0x0

    #@9a
    iput-boolean v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mVoicecall:Z

    #@9c
    .line 321
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->isDataStallAlarmNeeded()Z

    #@9f
    move-result v2

    #@a0
    if-eqz v2, :cond_21

    #@a2
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@a4
    if-nez v2, :cond_21

    #@a6
    .line 322
    const/4 v2, 0x0

    #@a7
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/LGDataRecovery;->startDataStallAlarm(Z)V

    #@aa
    goto/16 :goto_21

    #@ac
    .line 327
    :pswitch_ac
    const-string v2, "EVENT_PS_RESTRICT_ENABLED "

    #@ae
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@b1
    .line 328
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->stopDataStallAlarm()V

    #@b4
    .line 329
    const/4 v2, 0x1

    #@b5
    iput-boolean v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIsPsRestricted:Z

    #@b7
    goto/16 :goto_21

    #@b9
    .line 333
    :pswitch_b9
    const-string v2, "EVENT_PS_RESTRICT_DISABLED "

    #@bb
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@be
    .line 334
    const/4 v2, 0x0

    #@bf
    iput-boolean v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mIsPsRestricted:Z

    #@c1
    .line 335
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->isDataStallAlarmNeeded()Z

    #@c4
    move-result v2

    #@c5
    if-eqz v2, :cond_21

    #@c7
    .line 336
    iget-boolean v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->DATA_STALL_NOT_SUSPECTED:Z

    #@c9
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/LGDataRecovery;->startDataStallAlarm(Z)V

    #@cc
    goto/16 :goto_21

    #@ce
    .line 342
    :pswitch_ce
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d0
    check-cast v0, Landroid/os/AsyncResult;

    #@d2
    .line 343
    .local v0, ar:Landroid/os/AsyncResult;
    iget v2, p1, Landroid/os/Message;->what:I

    #@d4
    invoke-virtual {p0, v2, v0}, Lcom/android/internal/telephony/LGDataRecovery;->onGetModemPacketCountDone(ILandroid/os/AsyncResult;)V
    :try_end_d7
    .catchall {:try_start_2a .. :try_end_d7} :catchall_27

    #@d7
    goto/16 :goto_21

    #@d9
    .line 269
    nop

    #@da
    :pswitch_data_da
    .packed-switch 0x42065
        :pswitch_23
        :pswitch_2a
        :pswitch_2e
        :pswitch_42
        :pswitch_56
        :pswitch_6a
        :pswitch_70
        :pswitch_99
        :pswitch_7e
        :pswitch_6
        :pswitch_6
        :pswitch_ac
        :pswitch_b9
        :pswitch_ce
    .end packed-switch
.end method

.method protected onGetModemPacketCountDone(ILandroid/os/AsyncResult;)V
    .registers 9
    .parameter "what"
    .parameter "ar"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const-wide/16 v3, -0x1

    #@3
    .line 1762
    const-string v2, "onGetModemPacketCountDone entry"

    #@5
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@8
    .line 1763
    if-nez p2, :cond_25

    #@a
    .line 1764
    const-string v2, "onGetModemPacketCountDone AsyncResult is null"

    #@c
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@f
    .line 1765
    sget-object v2, Lcom/android/internal/telephony/LGDataRecovery;->mCP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@11
    iput-wide v3, v2, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@13
    .line 1766
    sget-object v2, Lcom/android/internal/telephony/LGDataRecovery;->mCP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@15
    iput-wide v3, v2, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@17
    .line 1787
    :goto_17
    const-string v2, "onGetModemPacketCountDone complete. Now notify."

    #@19
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@1c
    .line 1788
    monitor-enter p0

    #@1d
    .line 1789
    const/4 v2, 0x1

    #@1e
    :try_start_1e
    sput-boolean v2, Lcom/android/internal/telephony/LGDataRecovery;->mCpResponseFlag:Z

    #@20
    .line 1790
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    #@23
    .line 1791
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_1e .. :try_end_24} :catchall_89

    #@24
    .line 1792
    return-void

    #@25
    .line 1767
    :cond_25
    iget-object v2, p2, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@27
    if-nez v2, :cond_37

    #@29
    .line 1768
    const-string v2, "onGetModemPacketCountDone AsyncResult.result is null"

    #@2b
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@2e
    .line 1769
    sget-object v2, Lcom/android/internal/telephony/LGDataRecovery;->mCP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@30
    iput-wide v3, v2, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@32
    .line 1770
    sget-object v2, Lcom/android/internal/telephony/LGDataRecovery;->mCP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@34
    iput-wide v3, v2, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@36
    goto :goto_17

    #@37
    .line 1773
    :cond_37
    iget-object v2, p2, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@39
    check-cast v2, [I

    #@3b
    move-object v1, v2

    #@3c
    check-cast v1, [I

    #@3e
    .line 1775
    .local v1, result:[I
    const/4 v0, 0x0

    #@3f
    .local v0, i:I
    :goto_3f
    array-length v2, v1

    #@40
    if-ge v0, v2, :cond_67

    #@42
    .line 1776
    new-instance v2, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v3, "getModemPacketCountDone: result["

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    const-string v3, "]: "

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    aget v3, v1, v0

    #@59
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v2

    #@61
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@64
    .line 1775
    add-int/lit8 v0, v0, 0x1

    #@66
    goto :goto_3f

    #@67
    .line 1779
    :cond_67
    array-length v2, v1

    #@68
    const/4 v3, 0x2

    #@69
    if-ne v2, v3, :cond_83

    #@6b
    .line 1780
    sget-object v2, Lcom/android/internal/telephony/LGDataRecovery;->mCP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@6d
    const/4 v3, 0x0

    #@6e
    aget v3, v1, v3

    #@70
    int-to-long v3, v3

    #@71
    iput-wide v3, v2, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@73
    .line 1781
    sget-object v2, Lcom/android/internal/telephony/LGDataRecovery;->mCP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@75
    aget v3, v1, v5

    #@77
    int-to-long v3, v3

    #@78
    iput-wide v3, v2, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@7a
    .line 1782
    sget-object v2, Lcom/android/internal/telephony/LGDataRecovery;->mCP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@7c
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@7f
    move-result-wide v3

    #@80
    iput-wide v3, v2, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@82
    goto :goto_17

    #@83
    .line 1784
    :cond_83
    const-string v2, "getModemPacketCountDone return valuse is abnormal"

    #@85
    invoke-static {v2}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@88
    goto :goto_17

    #@89
    .line 1791
    .end local v0           #i:I
    .end local v1           #result:[I
    :catchall_89
    move-exception v2

    #@8a
    :try_start_8a
    monitor-exit p0
    :try_end_8b
    .catchall {:try_start_8a .. :try_end_8b} :catchall_89

    #@8b
    throw v2
.end method

.method public putRecoveryAction(I)V
    .registers 4
    .parameter "action"

    #@0
    .prologue
    .line 729
    iget-object v0, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    const-string v1, "radio.data.stall.recovery.action"

    #@c
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@f
    .line 731
    new-instance v0, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v1, "putRecoveryAction: "

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    invoke-static {v0}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@25
    .line 732
    return-void
.end method

.method public reStartDataStallAlarm()V
    .registers 2

    #@0
    .prologue
    .line 1313
    const-string v0, "reStartDataBlockObserver"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@5
    .line 1315
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->stopDataStallAlarm()V

    #@8
    .line 1316
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->isDataStallAlarmNeeded()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_12

    #@e
    .line 1317
    const/4 v0, 0x0

    #@f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LGDataRecovery;->startDataStallAlarm(Z)V

    #@12
    .line 1319
    :cond_12
    return-void
.end method

.method public startConnectionCompleteCheckAlarm(Ljava/lang/String;)V
    .registers 11
    .parameter "iface"

    #@0
    .prologue
    .line 1068
    const/16 v2, 0x2710

    #@2
    .line 1070
    .local v2, delayInMs:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "startConnectionCompleteCheckAlarm: iface="

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    const-string v5, " delay= "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    const/16 v5, 0xa

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    const-string v5, "seconds"

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    invoke-static {v4}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@2a
    .line 1072
    iget-object v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2c
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@2f
    move-result-object v4

    #@30
    const-string v5, "alarm"

    #@32
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@35
    move-result-object v0

    #@36
    check-cast v0, Landroid/app/AlarmManager;

    #@38
    .line 1075
    .local v0, am:Landroid/app/AlarmManager;
    new-instance v3, Landroid/content/Intent;

    #@3a
    const-string v4, "com.lge.internal.telephony.lge-data-conn-check-alarm"

    #@3c
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3f
    .line 1076
    .local v3, intent:Landroid/content/Intent;
    const-string v4, "conn.check.alram.tag"

    #@41
    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@44
    .line 1077
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/LGDataRecovery;->findConnInfowithIface(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@47
    move-result-object v1

    #@48
    .line 1078
    .local v1, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    if-eqz v1, :cond_65

    #@4a
    .line 1079
    iget-object v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4c
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@4f
    move-result-object v4

    #@50
    const/4 v5, 0x0

    #@51
    const/high16 v6, 0x800

    #@53
    invoke-static {v4, v5, v3, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@56
    move-result-object v4

    #@57
    iput-object v4, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->mConnCheckAlarmIntent:Landroid/app/PendingIntent;

    #@59
    .line 1081
    const/4 v4, 0x2

    #@5a
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@5d
    move-result-wide v5

    #@5e
    int-to-long v7, v2

    #@5f
    add-long/2addr v5, v7

    #@60
    iget-object v7, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->mConnCheckAlarmIntent:Landroid/app/PendingIntent;

    #@62
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@65
    .line 1084
    :cond_65
    return-void
.end method

.method public startDataStallAlarm(Z)V
    .registers 10
    .parameter "suspectedBlock"

    #@0
    .prologue
    .line 1272
    if-nez p1, :cond_6

    #@2
    iget-boolean v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->isScreenOn:Z

    #@4
    if-eqz v3, :cond_75

    #@6
    .line 1273
    :cond_6
    const v1, 0xea60

    #@9
    .line 1277
    .local v1, delayInMs:I
    :goto_9
    iget v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmTag:I

    #@b
    add-int/lit8 v3, v3, 0x1

    #@d
    iput v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmTag:I

    #@f
    .line 1278
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "startDataStallAlarm: tag="

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    iget v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmTag:I

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, " delay="

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    div-int/lit16 v4, v1, 0x3e8

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    const-string v4, "s suspectedFlag: "

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@3d
    .line 1281
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3f
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@42
    move-result-object v3

    #@43
    const-string v4, "alarm"

    #@45
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@48
    move-result-object v0

    #@49
    check-cast v0, Landroid/app/AlarmManager;

    #@4b
    .line 1284
    .local v0, am:Landroid/app/AlarmManager;
    new-instance v2, Landroid/content/Intent;

    #@4d
    const-string v3, "com.lge.internal.telephony.lge-data-stall-alarm"

    #@4f
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@52
    .line 1285
    .local v2, intent:Landroid/content/Intent;
    const-string v3, "data.stall.alram.tag"

    #@54
    iget v4, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmTag:I

    #@56
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@59
    .line 1286
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5b
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5e
    move-result-object v3

    #@5f
    const/4 v4, 0x0

    #@60
    const/high16 v5, 0x800

    #@62
    invoke-static {v3, v4, v2, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@65
    move-result-object v3

    #@66
    iput-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@68
    .line 1293
    const/4 v3, 0x3

    #@69
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@6c
    move-result-wide v4

    #@6d
    int-to-long v6, v1

    #@6e
    add-long/2addr v4, v6

    #@6f
    iget-object v6, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@71
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@74
    .line 1296
    return-void

    #@75
    .line 1275
    .end local v0           #am:Landroid/app/AlarmManager;
    .end local v1           #delayInMs:I
    .end local v2           #intent:Landroid/content/Intent;
    :cond_75
    const v1, 0x57e40

    #@78
    .restart local v1       #delayInMs:I
    goto :goto_9
.end method

.method public startNetdListener()V
    .registers 6

    #@0
    .prologue
    .line 1254
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@2
    if-nez v2, :cond_10

    #@4
    .line 1255
    const-string v2, "network_management"

    #@6
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    .line 1256
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@d
    move-result-object v2

    #@e
    iput-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@10
    .line 1259
    .end local v0           #b:Landroid/os/IBinder;
    :cond_10
    new-instance v2, Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;

    #@12
    const/4 v3, 0x0

    #@13
    invoke-direct {v2, p0, v3}, Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;-><init>(Lcom/android/internal/telephony/LGDataRecovery;Lcom/android/internal/telephony/LGDataRecovery$1;)V

    #@16
    iput-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNetdObserverEx:Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;

    #@18
    .line 1262
    :try_start_18
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNMService:Landroid/os/INetworkManagementService;

    #@1a
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery;->mNetdObserverEx:Lcom/android/internal/telephony/LGDataRecovery$NetdObserverEx;

    #@1c
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->registerObserverEx(Landroid/net/INetworkManagementEventObserverEx;)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_1f} :catch_20

    #@1f
    .line 1266
    :goto_1f
    return-void

    #@20
    .line 1263
    :catch_20
    move-exception v1

    #@21
    .line 1264
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "GSM"

    #@23
    new-instance v3, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v4, "Could not register InterfaceObserver "

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_1f
.end method

.method public stopConnectionCompleteCheckTimer(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V
    .registers 5
    .parameter "conn"

    #@0
    .prologue
    .line 1086
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "stopConnectionCompleteCheckTimer: iface"

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    iget-object v2, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " APN: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget-object v2, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, " mConnCheckAlarmIntent="

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    iget-object v2, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->mConnCheckAlarmIntent:Landroid/app/PendingIntent;

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-static {v1}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@30
    .line 1088
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@32
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@35
    move-result-object v1

    #@36
    const-string v2, "alarm"

    #@38
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3b
    move-result-object v0

    #@3c
    check-cast v0, Landroid/app/AlarmManager;

    #@3e
    .line 1091
    .local v0, am:Landroid/app/AlarmManager;
    iget-object v1, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->mConnCheckAlarmIntent:Landroid/app/PendingIntent;

    #@40
    if-eqz v1, :cond_4a

    #@42
    .line 1092
    iget-object v1, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->mConnCheckAlarmIntent:Landroid/app/PendingIntent;

    #@44
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@47
    .line 1093
    const/4 v1, 0x0

    #@48
    iput-object v1, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->mConnCheckAlarmIntent:Landroid/app/PendingIntent;

    #@4a
    .line 1095
    :cond_4a
    return-void
.end method

.method public stopDataStallAlarm()V
    .registers 4

    #@0
    .prologue
    .line 1299
    iget v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmTag:I

    #@2
    add-int/lit8 v1, v1, 0x1

    #@4
    iput v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmTag:I

    #@6
    .line 1300
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "stopDataStallAlarm: current tag="

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    iget v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmTag:I

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " mDataStallAlarmIntent="

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    iget-object v2, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v1}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@2a
    .line 1302
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2c
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@2f
    move-result-object v1

    #@30
    const-string v2, "alarm"

    #@32
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@35
    move-result-object v0

    #@36
    check-cast v0, Landroid/app/AlarmManager;

    #@38
    .line 1305
    .local v0, am:Landroid/app/AlarmManager;
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@3a
    if-eqz v1, :cond_44

    #@3c
    .line 1306
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@3e
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@41
    .line 1307
    const/4 v1, 0x0

    #@42
    iput-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@44
    .line 1309
    :cond_44
    return-void
.end method

.method public declared-synchronized updateConnectionInfosList(Ljava/lang/String;Z)V
    .registers 10
    .parameter "iface"
    .parameter "up"

    #@0
    .prologue
    .line 999
    monitor-enter p0

    #@1
    :try_start_1
    const-string v5, " UpdateConnectionInfos "

    #@3
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@6
    .line 1000
    const/4 v0, 0x0

    #@7
    .line 1001
    .local v0, alreadyexist:Z
    const/4 v4, 0x0

    #@8
    .line 1002
    .local v4, numofList:I
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@a
    if-eqz v5, :cond_97

    #@c
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_dc

    #@11
    move-result v5

    #@12
    if-lez v5, :cond_97

    #@14
    .line 1004
    :try_start_14
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@19
    move-result-object v3

    #@1a
    .local v3, i$:Ljava/util/Iterator;
    :cond_1a
    :goto_1a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@1d
    move-result v5

    #@1e
    if-eqz v5, :cond_97

    #@20
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@26
    .line 1005
    .local v1, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    iget-object v5, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@28
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v5

    #@2c
    if-eqz v5, :cond_1a

    #@2e
    .line 1006
    new-instance v5, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v6, " Found conn with iface "

    #@35
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    iget-object v6, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@46
    .line 1007
    if-nez p2, :cond_d0

    #@48
    .line 1008
    if-nez v4, :cond_71

    #@4a
    .line 1009
    new-instance v5, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v6, "update LState to LINKDOWN fot this iface : "

    #@51
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v5

    #@5d
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@60
    .line 1010
    sget-object v5, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKDOWN:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@62
    iput-object v5, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@64
    .line 1011
    iget-object v5, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->mConnCheckAlarmIntent:Landroid/app/PendingIntent;

    #@66
    if-eqz v5, :cond_6b

    #@68
    .line 1012
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LGDataRecovery;->stopConnectionCompleteCheckTimer(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V

    #@6b
    .line 1015
    :cond_6b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LGDataRecovery;->updateTrafficStatforAP(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V

    #@6e
    .line 1016
    add-int/lit8 v4, v4, 0x1

    #@70
    goto :goto_1a

    #@71
    .line 1019
    :cond_71
    new-instance v5, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v6, "there are more than 2 connInfos for "

    #@78
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v5

    #@7c
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    const-string v6, " remove it WARNING"

    #@82
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v5

    #@86
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v5

    #@8a
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@8d
    .line 1020
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@8f
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_92
    .catchall {:try_start_14 .. :try_end_92} :catchall_dc
    .catch Ljava/util/ConcurrentModificationException; {:try_start_14 .. :try_end_92} :catch_93

    #@92
    goto :goto_1a

    #@93
    .line 1033
    .end local v1           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    .end local v3           #i$:Ljava/util/Iterator;
    :catch_93
    move-exception v2

    #@94
    .line 1034
    .local v2, e:Ljava/util/ConcurrentModificationException;
    :try_start_94
    invoke-virtual {v2}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    #@97
    .line 1037
    .end local v2           #e:Ljava/util/ConcurrentModificationException;
    :cond_97
    const/4 v5, 0x1

    #@98
    if-eq v0, v5, :cond_b3

    #@9a
    if-eqz p2, :cond_b3

    #@9c
    .line 1038
    new-instance v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@9e
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@a1
    .line 1039
    .restart local v1       #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    iput-object p1, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@a3
    .line 1040
    const/4 v5, 0x0

    #@a4
    iput-object v5, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@a6
    .line 1041
    sget-object v5, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@a8
    iput-object v5, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@aa
    .line 1042
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@ac
    if-eqz v5, :cond_b3

    #@ae
    .line 1043
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->mConnectionInfos:Ljava/util/ArrayList;

    #@b0
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b3
    .line 1046
    .end local v1           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    :cond_b3
    if-eqz p2, :cond_c0

    #@b5
    .line 1047
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/LGDataRecovery;->findConnInfowithIface(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@b8
    move-result-object v1

    #@b9
    .line 1048
    .restart local v1       #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    if-nez v1, :cond_108

    #@bb
    .line 1049
    const-string v5, "conn is null"

    #@bd
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@c0
    .line 1060
    .end local v1           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    :cond_c0
    :goto_c0
    invoke-direct {p0}, Lcom/android/internal/telephony/LGDataRecovery;->isDataStallAlarmNeeded()Z

    #@c3
    move-result v5

    #@c4
    if-nez v5, :cond_ce

    #@c6
    .line 1061
    const-string v5, "There is no ConnectionInfo Connected stopstallAlarm."

    #@c8
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@cb
    .line 1062
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDataRecovery;->stopDataStallAlarm()V
    :try_end_ce
    .catchall {:try_start_94 .. :try_end_ce} :catchall_dc

    #@ce
    .line 1064
    :cond_ce
    :goto_ce
    monitor-exit p0

    #@cf
    return-void

    #@d0
    .line 1023
    .restart local v1       #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    .restart local v3       #i$:Ljava/util/Iterator;
    :cond_d0
    :try_start_d0
    iget-object v5, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@d2
    sget-object v6, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@d4
    if-ne v5, v6, :cond_df

    #@d6
    .line 1024
    const-string v5, "same notification occured ignore this one."

    #@d8
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V
    :try_end_db
    .catchall {:try_start_d0 .. :try_end_db} :catchall_dc
    .catch Ljava/util/ConcurrentModificationException; {:try_start_d0 .. :try_end_db} :catch_93

    #@db
    goto :goto_ce

    #@dc
    .line 999
    .end local v0           #alreadyexist:Z
    .end local v1           #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #numofList:I
    :catchall_dc
    move-exception v5

    #@dd
    monitor-exit p0

    #@de
    throw v5

    #@df
    .line 1027
    .restart local v0       #alreadyexist:Z
    .restart local v1       #conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v4       #numofList:I
    :cond_df
    :try_start_df
    new-instance v5, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v6, "iface"

    #@e6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v5

    #@ea
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v5

    #@ee
    const-string v6, " has conninfo update LState to LINKUP / FState = "

    #@f0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v5

    #@f4
    iget-object v6, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@f6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v5

    #@fa
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v5

    #@fe
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@101
    .line 1028
    const/4 v0, 0x1

    #@102
    .line 1029
    sget-object v5, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@104
    iput-object v5, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;
    :try_end_106
    .catchall {:try_start_df .. :try_end_106} :catchall_dc
    .catch Ljava/util/ConcurrentModificationException; {:try_start_df .. :try_end_106} :catch_93

    #@106
    goto/16 :goto_1a

    #@108
    .line 1050
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_108
    :try_start_108
    iget-object v5, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@10a
    sget-object v6, Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;->DISCONNECTED:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@10c
    if-ne v5, v6, :cond_112

    #@10e
    .line 1051
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/LGDataRecovery;->startConnectionCompleteCheckAlarm(Ljava/lang/String;)V

    #@111
    goto :goto_c0

    #@112
    .line 1053
    :cond_112
    new-instance v5, Ljava/lang/StringBuilder;

    #@114
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@117
    const-string v6, "FState of conn for "

    #@119
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v5

    #@11d
    iget-object v6, v1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@11f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v5

    #@123
    const-string v6, " is NOT DISCONNECTED. WARNING "

    #@125
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v5

    #@129
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12c
    move-result-object v5

    #@12d
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@130
    .line 1054
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LGDataRecovery;->updateTrafficStatforAP(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V

    #@133
    .line 1055
    iget-boolean v5, p0, Lcom/android/internal/telephony/LGDataRecovery;->supportCPtrafficstat:Z

    #@135
    if-eqz v5, :cond_c0

    #@137
    .line 1056
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LGDataRecovery;->updateTrafficStatforCP(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V
    :try_end_13a
    .catchall {:try_start_108 .. :try_end_13a} :catchall_dc

    #@13a
    goto :goto_c0
.end method

.method public updateTrafficStatforAP(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V
    .registers 11
    .parameter "conn"

    #@0
    .prologue
    .line 1129
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@2
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LGDataRecovery;->getAPTrafficStat(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@5
    move-result-object v4

    #@6
    .line 1131
    .local v4, resultCnt:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;
    if-nez v4, :cond_e

    #@8
    .line 1132
    const-string v5, "updateTrafficStatforAP()... Failure to get getAPTrafficStat()"

    #@a
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@d
    .line 1152
    :goto_d
    return-void

    #@e
    .line 1136
    :cond_e
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@10
    if-nez v5, :cond_d4

    #@12
    .line 1137
    new-instance v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@14
    invoke-direct {v5, p0}, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@17
    iput-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@19
    .line 1141
    :goto_19
    iput-object v4, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@1b
    .line 1142
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@1d
    iget-wide v5, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@1f
    iget-object v7, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@21
    iget-wide v7, v7, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@23
    sub-long v2, v5, v7

    #@25
    .line 1143
    .local v2, increasedTxcnt:J
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@27
    iget-wide v5, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@29
    iget-object v7, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@2b
    iget-wide v7, v7, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@2d
    sub-long v0, v5, v7

    #@2f
    .line 1144
    .local v0, increasedRxcnt:J
    const-wide/16 v5, 0x0

    #@31
    cmp-long v5, v0, v5

    #@33
    if-lez v5, :cond_da

    #@35
    .line 1145
    const/4 v5, 0x0

    #@36
    iput v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->sentSinceLastRecv:I

    #@38
    .line 1149
    :goto_38
    new-instance v5, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v6, "Prev stat for "

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    const-string v6, " :: txCnt= "

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@51
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@53
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    const-string v6, " rxCnt= "

    #@59
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@5f
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@61
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v5

    #@69
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@6c
    .line 1150
    new-instance v5, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v6, "Curr stat for "

    #@73
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@79
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v5

    #@7d
    const-string v6, " :: txCnt= "

    #@7f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v5

    #@83
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@85
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@87
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    const-string v6, " rxCnt= "

    #@8d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@93
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@95
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@98
    move-result-object v5

    #@99
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v5

    #@9d
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@a0
    .line 1151
    new-instance v5, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v6, "sentSinceLastRecv for "

    #@a7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v5

    #@ab
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@ad
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v5

    #@b1
    const-string v6, " :: "

    #@b3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v5

    #@b7
    iget v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->sentSinceLastRecv:I

    #@b9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v5

    #@bd
    const-string v6, " systemtime: "

    #@bf
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v5

    #@c3
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@c5
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@c7
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v5

    #@cb
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v5

    #@cf
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@d2
    goto/16 :goto_d

    #@d4
    .line 1139
    .end local v0           #increasedRxcnt:J
    .end local v2           #increasedTxcnt:J
    :cond_d4
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@d6
    iput-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@d8
    goto/16 :goto_19

    #@da
    .line 1147
    .restart local v0       #increasedRxcnt:J
    .restart local v2       #increasedTxcnt:J
    :cond_da
    iget v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->sentSinceLastRecv:I

    #@dc
    int-to-long v5, v5

    #@dd
    add-long/2addr v5, v2

    #@de
    long-to-int v5, v5

    #@df
    iput v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->sentSinceLastRecv:I

    #@e1
    goto/16 :goto_38
.end method

.method public updateTrafficStatforCP(Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;)V
    .registers 13
    .parameter "conn"

    #@0
    .prologue
    const-wide/16 v9, 0x0

    #@2
    .line 1155
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@4
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/LGDataRecovery;->getCPTrafficStat(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@7
    move-result-object v4

    #@8
    .line 1157
    .local v4, resultCnt:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@a
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@c
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@e
    iput-wide v6, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@10
    .line 1158
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@12
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@14
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@16
    iput-wide v6, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@18
    .line 1159
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@1a
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@1c
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@1e
    iput-wide v6, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@20
    .line 1161
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@22
    iget-wide v6, v4, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@24
    iput-wide v6, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@26
    .line 1162
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@28
    iget-wide v6, v4, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@2a
    iput-wide v6, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@2c
    .line 1163
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@2e
    iget-wide v6, v4, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@30
    iput-wide v6, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@32
    .line 1165
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@34
    iget-wide v5, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@36
    iget-object v7, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@38
    iget-wide v7, v7, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@3a
    sub-long v2, v5, v7

    #@3c
    .line 1166
    .local v2, increasedTxcnt:J
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@3e
    iget-wide v5, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@40
    iget-object v7, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@42
    iget-wide v7, v7, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@44
    sub-long v0, v5, v7

    #@46
    .line 1169
    .local v0, increasedRxcnt:J
    cmp-long v5, v2, v9

    #@48
    if-ltz v5, :cond_4e

    #@4a
    cmp-long v5, v0, v9

    #@4c
    if-gez v5, :cond_5a

    #@4e
    .line 1170
    :cond_4e
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@50
    iget-wide v5, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@52
    sub-long v2, v5, v9

    #@54
    .line 1171
    iget-object v5, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@56
    iget-wide v5, v5, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@58
    sub-long v0, v5, v9

    #@5a
    .line 1182
    :cond_5a
    new-instance v5, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v6, "Modem Prev stat for "

    #@61
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@67
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v5

    #@6b
    const-string v6, " :: txCnt= "

    #@6d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@73
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@75
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@78
    move-result-object v5

    #@79
    const-string v6, " rxCnt= "

    #@7b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v5

    #@7f
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@81
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@83
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@86
    move-result-object v5

    #@87
    const-string v6, " time= "

    #@89
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v5

    #@8d
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@8f
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@91
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@94
    move-result-object v5

    #@95
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v5

    #@99
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@9c
    .line 1183
    new-instance v5, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v6, "Modem Curr stat for "

    #@a3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v5

    #@a7
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@a9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v5

    #@ad
    const-string v6, " :: txCnt= "

    #@af
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v5

    #@b3
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@b5
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@b7
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v5

    #@bb
    const-string v6, " rxCnt= "

    #@bd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v5

    #@c1
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@c3
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@c5
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v5

    #@c9
    const-string v6, " time= "

    #@cb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v5

    #@cf
    iget-object v6, p1, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@d1
    iget-wide v6, v6, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@d3
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v5

    #@d7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v5

    #@db
    invoke-static {v5}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@de
    .line 1185
    return-void
.end method
