.class Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;
.super Ljava/lang/Object;
.source "IsimUiccRecords.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/IsimUiccRecords;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EfIsimGbabpLoaded"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 659
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Lcom/android/internal/telephony/uicc/IsimUiccRecords$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 659
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;-><init>(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)V

    #@3
    return-void
.end method


# virtual methods
.method public getEfName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 661
    const-string v0, "EF_ISIM_GBABP"

    #@2
    return-object v0
.end method

.method public onRecordLoaded(Landroid/os/AsyncResult;)V
    .registers 11
    .parameter "ar"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 664
    iget-object v5, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3
    check-cast v5, [B

    #@5
    move-object v0, v5

    #@6
    check-cast v0, [B

    #@8
    .line 665
    .local v0, data:[B
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@a
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@d
    move-result-object v6

    #@e
    invoke-static {v5, v6}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1502(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Ljava/lang/String;)Ljava/lang/String;

    #@11
    .line 666
    invoke-static {}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$600()Z

    #@14
    move-result v5

    #@15
    if-eqz v5, :cond_35

    #@17
    .line 668
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@19
    new-instance v6, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v7, "EF_GBABP="

    #@20
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v6

    #@24
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@26
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1500(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)Ljava/lang/String;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@35
    .line 672
    :cond_35
    const/4 v2, 0x0

    #@36
    .line 673
    .local v2, i:I
    add-int/lit8 v3, v2, 0x1

    #@38
    .end local v2           #i:I
    .local v3, i:I
    :try_start_38
    aget-byte v4, v0, v2

    #@3a
    .line 675
    .local v4, l:I
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@3c
    new-array v6, v4, [B

    #@3e
    invoke-static {v5, v6}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1202(Lcom/android/internal/telephony/uicc/IsimUiccRecords;[B)[B

    #@41
    .line 676
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@43
    invoke-static {v5}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1200(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)[B

    #@46
    move-result-object v5

    #@47
    const/4 v6, 0x0

    #@48
    invoke-static {v0, v3, v5, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@4b
    .line 678
    add-int/lit8 v2, v4, 0x1

    #@4d
    .line 679
    .end local v3           #i:I
    .restart local v2       #i:I
    add-int/lit8 v3, v2, 0x1

    #@4f
    .end local v2           #i:I
    .restart local v3       #i:I
    aget-byte v4, v0, v2

    #@51
    .line 680
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@53
    new-instance v6, Ljava/lang/String;

    #@55
    const-string v7, "UTF-8"

    #@57
    invoke-direct {v6, v0, v3, v4, v7}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@5a
    invoke-static {v5, v6}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1302(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Ljava/lang/String;)Ljava/lang/String;

    #@5d
    .line 682
    add-int v2, v3, v4

    #@5f
    .line 683
    .end local v3           #i:I
    .restart local v2       #i:I
    add-int/lit8 v3, v2, 0x1

    #@61
    .end local v2           #i:I
    .restart local v3       #i:I
    aget-byte v4, v0, v2

    #@63
    .line 684
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@65
    new-instance v6, Ljava/lang/String;

    #@67
    const-string v7, "UTF-8"

    #@69
    invoke-direct {v6, v0, v3, v4, v7}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@6c
    invoke-static {v5, v6}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1402(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Ljava/lang/String;)Ljava/lang/String;

    #@6f
    .line 686
    invoke-static {}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$600()Z

    #@72
    move-result v5

    #@73
    if-eqz v5, :cond_cf

    #@75
    .line 688
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@77
    new-instance v6, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v7, "ISIM records (GBABP): mRand = "

    #@7e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v6

    #@82
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@84
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1200(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)[B

    #@87
    move-result-object v7

    #@88
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v6

    #@8c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v6

    #@90
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@93
    .line 689
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@95
    new-instance v6, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v7, "ISIM records (GBABP): mBtid = "

    #@9c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v6

    #@a0
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@a2
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1300(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)Ljava/lang/String;

    #@a5
    move-result-object v7

    #@a6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v6

    #@aa
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v6

    #@ae
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@b1
    .line 690
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@b3
    new-instance v6, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v7, "ISIM records (GBABP): mKeyLifetime = "

    #@ba
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v6

    #@be
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@c0
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1400(Lcom/android/internal/telephony/uicc/IsimUiccRecords;)Ljava/lang/String;

    #@c3
    move-result-object v7

    #@c4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v6

    #@c8
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v6

    #@cc
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V
    :try_end_cf
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_cf} :catch_d0

    #@cf
    .line 699
    .end local v3           #i:I
    .end local v4           #l:I
    :cond_cf
    :goto_cf
    return-void

    #@d0
    .line 692
    .restart local v3       #i:I
    :catch_d0
    move-exception v1

    #@d1
    move v2, v3

    #@d2
    .line 693
    .end local v3           #i:I
    .local v1, e:Ljava/lang/Exception;
    .restart local v2       #i:I
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@d4
    new-instance v6, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v7, "Failed to parse GBABP contents: "

    #@db
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v6

    #@df
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v6

    #@e3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v6

    #@e7
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->log(Ljava/lang/String;)V

    #@ea
    .line 694
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@ec
    invoke-static {v5, v8}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1202(Lcom/android/internal/telephony/uicc/IsimUiccRecords;[B)[B

    #@ef
    .line 695
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@f1
    invoke-static {v5, v8}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1302(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Ljava/lang/String;)Ljava/lang/String;

    #@f4
    .line 696
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IsimUiccRecords$EfIsimGbabpLoaded;->this$0:Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@f6
    invoke-static {v5, v8}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->access$1402(Lcom/android/internal/telephony/uicc/IsimUiccRecords;Ljava/lang/String;)Ljava/lang/String;

    #@f9
    goto :goto_cf
.end method
