.class Lcom/android/internal/telephony/IccSmsInterfaceManager$1;
.super Landroid/os/Handler;
.source "IccSmsInterfaceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccSmsInterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/IccSmsInterfaceManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 124
    iput-object p1, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 129
    iget v6, p1, Landroid/os/Message;->what:I

    #@4
    sparse-switch v6, :sswitch_data_2a6

    #@7
    .line 273
    :goto_7
    return-void

    #@8
    .line 131
    :sswitch_8
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    check-cast v1, Landroid/os/AsyncResult;

    #@c
    .line 132
    .local v1, ar:Landroid/os/AsyncResult;
    iget-object v6, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@e
    invoke-static {v6}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@11
    move-result-object v6

    #@12
    monitor-enter v6

    #@13
    .line 133
    :try_start_13
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@15
    iget-object v8, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@17
    if-nez v8, :cond_2a

    #@19
    :goto_19
    invoke-static {v7, v5}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$102(Lcom/android/internal/telephony/IccSmsInterfaceManager;Z)Z

    #@1c
    .line 134
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@1e
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    #@25
    .line 135
    monitor-exit v6

    #@26
    goto :goto_7

    #@27
    :catchall_27
    move-exception v4

    #@28
    monitor-exit v6
    :try_end_29
    .catchall {:try_start_13 .. :try_end_29} :catchall_27

    #@29
    throw v4

    #@2a
    :cond_2a
    move v5, v4

    #@2b
    .line 133
    goto :goto_19

    #@2c
    .line 138
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_2c
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2e
    check-cast v1, Landroid/os/AsyncResult;

    #@30
    .line 139
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@32
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@35
    move-result-object v5

    #@36
    monitor-enter v5

    #@37
    .line 140
    :try_start_37
    iget-object v4, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@39
    if-nez v4, :cond_61

    #@3b
    .line 141
    iget-object v6, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@3d
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@3f
    iget-object v4, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@41
    check-cast v4, Ljava/util/ArrayList;

    #@43
    invoke-virtual {v7, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->buildValidRawData(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    #@46
    move-result-object v4

    #@47
    invoke-static {v6, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$202(Lcom/android/internal/telephony/IccSmsInterfaceManager;Ljava/util/List;)Ljava/util/List;

    #@4a
    .line 144
    iget-object v6, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@4c
    iget-object v4, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4e
    check-cast v4, Ljava/util/ArrayList;

    #@50
    invoke-static {v6, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$300(Lcom/android/internal/telephony/IccSmsInterfaceManager;Ljava/util/ArrayList;)V

    #@53
    .line 150
    :cond_53
    :goto_53
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@55
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    #@5c
    .line 151
    monitor-exit v5

    #@5d
    goto :goto_7

    #@5e
    :catchall_5e
    move-exception v4

    #@5f
    monitor-exit v5
    :try_end_60
    .catchall {:try_start_37 .. :try_end_60} :catchall_5e

    #@60
    throw v4

    #@61
    .line 146
    :cond_61
    :try_start_61
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@63
    const-string v6, "Cannot load Sms records"

    #@65
    invoke-virtual {v4, v6}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@68
    .line 147
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@6a
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$200(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/util/List;

    #@6d
    move-result-object v4

    #@6e
    if-eqz v4, :cond_53

    #@70
    .line 148
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@72
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$200(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/util/List;

    #@75
    move-result-object v4

    #@76
    invoke-interface {v4}, Ljava/util/List;->clear()V
    :try_end_79
    .catchall {:try_start_61 .. :try_end_79} :catchall_5e

    #@79
    goto :goto_53

    #@7a
    .line 155
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_7a
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7c
    check-cast v1, Landroid/os/AsyncResult;

    #@7e
    .line 156
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v6, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@80
    invoke-static {v6}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@83
    move-result-object v6

    #@84
    monitor-enter v6

    #@85
    .line 157
    :try_start_85
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@87
    iget-object v8, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@89
    if-nez v8, :cond_9d

    #@8b
    :goto_8b
    invoke-static {v7, v5}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$102(Lcom/android/internal/telephony/IccSmsInterfaceManager;Z)Z

    #@8e
    .line 158
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@90
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@93
    move-result-object v4

    #@94
    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    #@97
    .line 159
    monitor-exit v6

    #@98
    goto/16 :goto_7

    #@9a
    :catchall_9a
    move-exception v4

    #@9b
    monitor-exit v6
    :try_end_9c
    .catchall {:try_start_85 .. :try_end_9c} :catchall_9a

    #@9c
    throw v4

    #@9d
    :cond_9d
    move v5, v4

    #@9e
    .line 157
    goto :goto_8b

    #@9f
    .line 164
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_9f
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a1
    check-cast v1, Landroid/os/AsyncResult;

    #@a3
    .line 165
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@a5
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@a8
    move-result-object v5

    #@a9
    monitor-enter v5

    #@aa
    .line 166
    :try_start_aa
    iget-object v4, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@ac
    if-nez v4, :cond_db

    #@ae
    .line 167
    iget-object v6, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@b0
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@b2
    iget-object v4, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@b4
    check-cast v4, Ljava/util/ArrayList;

    #@b6
    invoke-virtual {v7, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->buildValidRawData(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    #@b9
    move-result-object v4

    #@ba
    invoke-static {v6, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$202(Lcom/android/internal/telephony/IccSmsInterfaceManager;Ljava/util/List;)Ljava/util/List;

    #@bd
    .line 169
    iget-object v6, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@bf
    iget-object v4, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c1
    check-cast v4, Ljava/util/ArrayList;

    #@c3
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@c5
    invoke-static {v7}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$400(Lcom/android/internal/telephony/IccSmsInterfaceManager;)I

    #@c8
    move-result v7

    #@c9
    invoke-static {v6, v4, v7}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$500(Lcom/android/internal/telephony/IccSmsInterfaceManager;Ljava/util/ArrayList;I)V

    #@cc
    .line 176
    :cond_cc
    :goto_cc
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@ce
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@d1
    move-result-object v4

    #@d2
    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    #@d5
    .line 177
    monitor-exit v5

    #@d6
    goto/16 :goto_7

    #@d8
    :catchall_d8
    move-exception v4

    #@d9
    monitor-exit v5
    :try_end_da
    .catchall {:try_start_aa .. :try_end_da} :catchall_d8

    #@da
    throw v4

    #@db
    .line 172
    :cond_db
    :try_start_db
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@dd
    const-string v6, "Cannot load Sms records"

    #@df
    invoke-virtual {v4, v6}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@e2
    .line 173
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@e4
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$200(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/util/List;

    #@e7
    move-result-object v4

    #@e8
    if-eqz v4, :cond_cc

    #@ea
    .line 174
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@ec
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$200(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/util/List;

    #@ef
    move-result-object v4

    #@f0
    invoke-interface {v4}, Ljava/util/List;->clear()V
    :try_end_f3
    .catchall {:try_start_db .. :try_end_f3} :catchall_d8

    #@f3
    goto :goto_cc

    #@f4
    .line 183
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_f4
    const-string v4, "handleMessage():EVENT_GET_SMSCADDRESS, getSmscAddress"

    #@f6
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@f9
    .line 184
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@fb
    check-cast v1, Landroid/os/AsyncResult;

    #@fd
    .line 186
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@ff
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@102
    move-result-object v6

    #@103
    monitor-enter v6

    #@104
    .line 187
    :try_start_104
    iget-object v4, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;
    :try_end_106
    .catchall {:try_start_104 .. :try_end_106} :catchall_153

    #@106
    if-nez v4, :cond_175

    #@108
    .line 189
    :try_start_108
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@10a
    iget-object v4, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@10c
    check-cast v4, Ljava/lang/String;

    #@10e
    invoke-static {v7, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$602(Lcom/android/internal/telephony/IccSmsInterfaceManager;Ljava/lang/String;)Ljava/lang/String;

    #@111
    .line 191
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@113
    iget-object v4, v4, Lcom/android/internal/telephony/IccSmsInterfaceManager;->mContext:Landroid/content/Context;

    #@115
    const-string v7, "hide_privacy_log"

    #@117
    invoke-static {v4, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@11a
    move-result v4

    #@11b
    if-ne v4, v5, :cond_156

    #@11d
    .line 192
    const-string v4, "1"

    #@11f
    const-string v5, "persist.service.privacy.enable"

    #@121
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@124
    move-result-object v5

    #@125
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@128
    move-result v4

    #@129
    if-eqz v4, :cond_147

    #@12b
    .line 193
    new-instance v4, Ljava/lang/StringBuilder;

    #@12d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@130
    const-string v5, "handleMessage():EVENT_GET_SMSCADDRESS, smsc address = "

    #@132
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v4

    #@136
    iget-object v5, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@138
    invoke-static {v5}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$600(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/String;

    #@13b
    move-result-object v5

    #@13c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v4

    #@140
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@143
    move-result-object v4

    #@144
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I
    :try_end_147
    .catchall {:try_start_108 .. :try_end_147} :catchall_153
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_108 .. :try_end_147} :catch_173

    #@147
    .line 207
    :cond_147
    :goto_147
    :try_start_147
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@149
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@14c
    move-result-object v4

    #@14d
    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    #@150
    .line 208
    monitor-exit v6

    #@151
    goto/16 :goto_7

    #@153
    :catchall_153
    move-exception v4

    #@154
    monitor-exit v6
    :try_end_155
    .catchall {:try_start_147 .. :try_end_155} :catchall_153

    #@155
    throw v4

    #@156
    .line 196
    :cond_156
    :try_start_156
    new-instance v4, Ljava/lang/StringBuilder;

    #@158
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15b
    const-string v5, "handleMessage():EVENT_GET_SMSCADDRESS, smsc address = "

    #@15d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v4

    #@161
    iget-object v5, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@163
    invoke-static {v5}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$600(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/String;

    #@166
    move-result-object v5

    #@167
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v4

    #@16b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16e
    move-result-object v4

    #@16f
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I
    :try_end_172
    .catchall {:try_start_156 .. :try_end_172} :catchall_153
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_156 .. :try_end_172} :catch_173

    #@172
    goto :goto_147

    #@173
    .line 200
    :catch_173
    move-exception v4

    #@174
    goto :goto_147

    #@175
    .line 204
    :cond_175
    :try_start_175
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@177
    const-string v5, "Cannot read template"

    #@179
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@17c
    .line 205
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@17e
    const-string v5, ""

    #@180
    invoke-static {v4, v5}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$602(Lcom/android/internal/telephony/IccSmsInterfaceManager;Ljava/lang/String;)Ljava/lang/String;
    :try_end_183
    .catchall {:try_start_175 .. :try_end_183} :catchall_153

    #@183
    goto :goto_147

    #@184
    .line 214
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_184
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@186
    check-cast v1, Landroid/os/AsyncResult;

    #@188
    .line 215
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@18a
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@18d
    move-result-object v6

    #@18e
    monitor-enter v6

    #@18f
    .line 216
    :try_start_18f
    iget-object v4, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@191
    if-nez v4, :cond_1aa

    #@193
    .line 217
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@195
    iget-object v4, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@197
    check-cast v4, [B

    #@199
    check-cast v4, [B

    #@19b
    iget v8, p1, Landroid/os/Message;->arg1:I

    #@19d
    iget-object v5, v1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@19f
    check-cast v5, Ljava/lang/Boolean;

    #@1a1
    invoke-static {v7, v4, v8, v5}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$700(Lcom/android/internal/telephony/IccSmsInterfaceManager;[BILjava/lang/Boolean;)V

    #@1a4
    .line 222
    :goto_1a4
    monitor-exit v6

    #@1a5
    goto/16 :goto_7

    #@1a7
    :catchall_1a7
    move-exception v4

    #@1a8
    monitor-exit v6
    :try_end_1a9
    .catchall {:try_start_18f .. :try_end_1a9} :catchall_1a7

    #@1a9
    throw v4

    #@1aa
    .line 219
    :cond_1aa
    :try_start_1aa
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@1ac
    const/4 v5, 0x0

    #@1ad
    invoke-static {v4, v5}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$102(Lcom/android/internal/telephony/IccSmsInterfaceManager;Z)Z

    #@1b0
    .line 220
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@1b2
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@1b5
    move-result-object v4

    #@1b6
    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_1b9
    .catchall {:try_start_1aa .. :try_end_1b9} :catchall_1a7

    #@1b9
    goto :goto_1a4

    #@1ba
    .line 226
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_1ba
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1bc
    check-cast v1, Landroid/os/AsyncResult;

    #@1be
    .line 227
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v6, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@1c0
    invoke-static {v6}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@1c3
    move-result-object v6

    #@1c4
    monitor-enter v6

    #@1c5
    .line 228
    :try_start_1c5
    iget-object v7, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@1c7
    iget-object v8, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1c9
    if-nez v8, :cond_1cc

    #@1cb
    move v4, v5

    #@1cc
    :cond_1cc
    invoke-static {v7, v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$102(Lcom/android/internal/telephony/IccSmsInterfaceManager;Z)Z

    #@1cf
    .line 229
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@1d1
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@1d4
    move-result-object v4

    #@1d5
    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    #@1d8
    .line 230
    monitor-exit v6

    #@1d9
    goto/16 :goto_7

    #@1db
    :catchall_1db
    move-exception v4

    #@1dc
    monitor-exit v6
    :try_end_1dd
    .catchall {:try_start_1c5 .. :try_end_1dd} :catchall_1db

    #@1dd
    throw v4

    #@1de
    .line 233
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_1de
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1e0
    check-cast v1, Landroid/os/AsyncResult;

    #@1e2
    .line 234
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@1e4
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@1e7
    move-result-object v5

    #@1e8
    monitor-enter v5

    #@1e9
    .line 235
    :try_start_1e9
    iget-object v4, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1eb
    if-nez v4, :cond_241

    #@1ed
    .line 236
    iget-object v4, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1ef
    check-cast v4, [I

    #@1f1
    move-object v0, v4

    #@1f2
    check-cast v0, [I

    #@1f4
    move-object v2, v0

    #@1f5
    .line 237
    .local v2, recordSizeArray:[I
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@1f7
    const/4 v6, 0x2

    #@1f8
    aget v6, v2, v6

    #@1fa
    invoke-static {v4, v6}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$802(Lcom/android/internal/telephony/IccSmsInterfaceManager;I)I

    #@1fd
    .line 241
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@1ff
    new-instance v6, Ljava/lang/StringBuilder;

    #@201
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@204
    const-string v7, "[RED] GET_RECORD_SIZE Size "

    #@206
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v6

    #@20a
    const/4 v7, 0x0

    #@20b
    aget v7, v2, v7

    #@20d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@210
    move-result-object v6

    #@211
    const-string v7, " total "

    #@213
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@216
    move-result-object v6

    #@217
    const/4 v7, 0x1

    #@218
    aget v7, v2, v7

    #@21a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21d
    move-result-object v6

    #@21e
    const-string v7, " #record "

    #@220
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v6

    #@224
    const/4 v7, 0x2

    #@225
    aget v7, v2, v7

    #@227
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v6

    #@22b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22e
    move-result-object v6

    #@22f
    invoke-virtual {v4, v6}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->log(Ljava/lang/String;)V

    #@232
    .line 248
    .end local v2           #recordSizeArray:[I
    :goto_232
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@234
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@237
    move-result-object v4

    #@238
    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    #@23b
    .line 249
    monitor-exit v5

    #@23c
    goto/16 :goto_7

    #@23e
    :catchall_23e
    move-exception v4

    #@23f
    monitor-exit v5
    :try_end_240
    .catchall {:try_start_1e9 .. :try_end_240} :catchall_23e

    #@240
    throw v4

    #@241
    .line 246
    :cond_241
    :try_start_241
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@243
    const/4 v6, -0x1

    #@244
    invoke-static {v4, v6}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$802(Lcom/android/internal/telephony/IccSmsInterfaceManager;I)I
    :try_end_247
    .catchall {:try_start_241 .. :try_end_247} :catchall_23e

    #@247
    goto :goto_232

    #@248
    .line 252
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_248
    const-string v4, "handleMessage():EVENT_COPY_SMS_DONE"

    #@24a
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@24d
    .line 253
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@24f
    check-cast v1, Landroid/os/AsyncResult;

    #@251
    .line 254
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@253
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@256
    move-result-object v5

    #@257
    monitor-enter v5

    #@258
    .line 255
    :try_start_258
    iget-object v4, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;
    :try_end_25a
    .catchall {:try_start_258 .. :try_end_25a} :catchall_294

    #@25a
    if-nez v4, :cond_297

    #@25c
    .line 257
    :try_start_25c
    iget-object v4, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@25e
    check-cast v4, [I

    #@260
    move-object v0, v4

    #@261
    check-cast v0, [I

    #@263
    move-object v3, v0

    #@264
    .line 258
    .local v3, result:[I
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@266
    const/4 v6, 0x0

    #@267
    aget v6, v3, v6

    #@269
    invoke-static {v4, v6}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$902(Lcom/android/internal/telephony/IccSmsInterfaceManager;I)I

    #@26c
    .line 259
    new-instance v4, Ljava/lang/StringBuilder;

    #@26e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@271
    const-string v6, "handleMessage():EVENT_COPY_SMS_DONE, After copy SMS to SIM IndexOnIcc: "

    #@273
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@276
    move-result-object v4

    #@277
    iget-object v6, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@279
    invoke-static {v6}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$900(Lcom/android/internal/telephony/IccSmsInterfaceManager;)I

    #@27c
    move-result v6

    #@27d
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@280
    move-result-object v4

    #@281
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@284
    move-result-object v4

    #@285
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_288
    .catchall {:try_start_25c .. :try_end_288} :catchall_294
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_25c .. :try_end_288} :catch_2a3

    #@288
    .line 266
    .end local v3           #result:[I
    :goto_288
    :try_start_288
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@28a
    invoke-static {v4}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$000(Lcom/android/internal/telephony/IccSmsInterfaceManager;)Ljava/lang/Object;

    #@28d
    move-result-object v4

    #@28e
    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    #@291
    .line 267
    monitor-exit v5

    #@292
    goto/16 :goto_7

    #@294
    :catchall_294
    move-exception v4

    #@295
    monitor-exit v5
    :try_end_296
    .catchall {:try_start_288 .. :try_end_296} :catchall_294

    #@296
    throw v4

    #@297
    .line 263
    :cond_297
    :try_start_297
    const-string v4, "handleMessage():EVENT_COPY_SMS_DONE, Cannot copy sms to sim"

    #@299
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@29c
    .line 264
    iget-object v4, p0, Lcom/android/internal/telephony/IccSmsInterfaceManager$1;->this$0:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@29e
    const/4 v6, -0x1

    #@29f
    invoke-static {v4, v6}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->access$902(Lcom/android/internal/telephony/IccSmsInterfaceManager;I)I
    :try_end_2a2
    .catchall {:try_start_297 .. :try_end_2a2} :catchall_294

    #@2a2
    goto :goto_288

    #@2a3
    .line 260
    :catch_2a3
    move-exception v4

    #@2a4
    goto :goto_288

    #@2a5
    .line 129
    nop

    #@2a6
    :sswitch_data_2a6
    .sparse-switch
        0x1 -> :sswitch_2c
        0x2 -> :sswitch_8
        0x3 -> :sswitch_7a
        0x4 -> :sswitch_7a
        0x5 -> :sswitch_f4
        0x6 -> :sswitch_248
        0x7d -> :sswitch_1de
        0x7e -> :sswitch_184
        0x7f -> :sswitch_1ba
        0x80 -> :sswitch_9f
    .end sparse-switch
.end method
