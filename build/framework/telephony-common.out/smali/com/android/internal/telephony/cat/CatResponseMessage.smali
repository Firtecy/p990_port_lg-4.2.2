.class public Lcom/android/internal/telephony/cat/CatResponseMessage;
.super Ljava/lang/Object;
.source "CatResponseMessage.java"


# instance fields
.field addedInfo:[B

.field additionalInfo:I

.field cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

.field eventValue:I

.field includeAdditionalInfo:Z

.field resCode:Lcom/android/internal/telephony/cat/ResultCode;

.field usersConfirm:Z

.field usersInput:Ljava/lang/String;

.field usersMenuSelection:I

.field usersYesNoSelection:Z


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/cat/CatCmdMessage;)V
    .registers 5
    .parameter "cmdMsg"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 20
    iput-object v2, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@7
    .line 21
    sget-object v0, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->resCode:Lcom/android/internal/telephony/cat/ResultCode;

    #@b
    .line 22
    iput v1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersMenuSelection:I

    #@d
    .line 23
    iput-object v2, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersInput:Ljava/lang/String;

    #@f
    .line 24
    iput-boolean v1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersYesNoSelection:Z

    #@11
    .line 25
    iput-boolean v1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersConfirm:Z

    #@13
    .line 26
    const/4 v0, -0x1

    #@14
    iput v0, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->eventValue:I

    #@16
    .line 27
    iput-object v2, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->addedInfo:[B

    #@18
    .line 28
    iput-boolean v1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->includeAdditionalInfo:Z

    #@1a
    .line 29
    iput v1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->additionalInfo:I

    #@1c
    .line 32
    iget-object v0, p1, Lcom/android/internal/telephony/cat/CatCmdMessage;->mCmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@1e
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@20
    .line 33
    return-void
.end method


# virtual methods
.method getCmdDetails()Lcom/android/internal/telephony/cat/CommandDetails;
    .registers 2

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@2
    return-object v0
.end method

.method public setAdditionalInfo(ZI)V
    .registers 3
    .parameter "includeAdditionalInfo"
    .parameter "additionalInfo"

    #@0
    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->includeAdditionalInfo:Z

    #@2
    .line 62
    iput p2, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->additionalInfo:I

    #@4
    .line 63
    return-void
.end method

.method public setConfirmation(Z)V
    .registers 2
    .parameter "confirm"

    #@0
    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersConfirm:Z

    #@2
    .line 58
    return-void
.end method

.method public setEventDownload(I[B)V
    .registers 3
    .parameter "event"
    .parameter "addedInfo"

    #@0
    .prologue
    .line 48
    iput p1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->eventValue:I

    #@2
    .line 49
    iput-object p2, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->addedInfo:[B

    #@4
    .line 50
    return-void
.end method

.method public setInput(Ljava/lang/String;)V
    .registers 2
    .parameter "input"

    #@0
    .prologue
    .line 44
    iput-object p1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersInput:Ljava/lang/String;

    #@2
    .line 45
    return-void
.end method

.method public setMenuSelection(I)V
    .registers 2
    .parameter "selection"

    #@0
    .prologue
    .line 40
    iput p1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersMenuSelection:I

    #@2
    .line 41
    return-void
.end method

.method public setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V
    .registers 2
    .parameter "resCode"

    #@0
    .prologue
    .line 36
    iput-object p1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->resCode:Lcom/android/internal/telephony/cat/ResultCode;

    #@2
    .line 37
    return-void
.end method

.method public setYesNo(Z)V
    .registers 2
    .parameter "yesNo"

    #@0
    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersYesNoSelection:Z

    #@2
    .line 54
    return-void
.end method
