.class public Lcom/android/internal/telephony/IccPhonebookProvider;
.super Landroid/content/ContentProvider;
.source "IccPhonebookProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;,
        Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;,
        Lcom/android/internal/telephony/IccPhonebookProvider$Contract;
    }
.end annotation


# static fields
.field private static final ADN_GROUP_INDEX:I = 0x1

.field private static final ADN_INDEX:I = 0x0

.field private static final ADN_INFO:I = 0x2

.field private static final ALLOWED_PACKAGE:[Ljava/lang/String; = null

.field private static final DEBUG:Z = false

.field private static final DEBUG_V:Z = false

.field private static final TAG:Ljava/lang/String; = "IccPhonebookProvider"

.field private static final URL_MATCHER:Landroid/content/UriMatcher;

.field private static final sBackend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 235
    new-instance v0, Landroid/content/UriMatcher;

    #@5
    const/4 v1, -0x1

    #@6
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@9
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@b
    .line 237
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@d
    const-string v1, "icc-phonebook"

    #@f
    const-string v2, "adn/#"

    #@11
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@14
    .line 238
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@16
    const-string v1, "icc-phonebook"

    #@18
    const-string v2, "adn/info"

    #@1a
    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@1d
    .line 239
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@1f
    const-string v1, "icc-phonebook"

    #@21
    const-string v2, "adn/group/#"

    #@23
    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@26
    .line 244
    invoke-static {}, Lcom/android/internal/telephony/IccPhonebookProvider;->loadBackend()Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@29
    move-result-object v0

    #@2a
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider;->sBackend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@2c
    .line 460
    new-array v0, v5, [Ljava/lang/String;

    #@2e
    const-string v1, "com.android.contacts"

    #@30
    aput-object v1, v0, v3

    #@32
    const-string v1, "com.lge.contacts.sim.test"

    #@34
    aput-object v1, v0, v4

    #@36
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider;->ALLOWED_PACKAGE:[Ljava/lang/String;

    #@38
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@3
    .line 487
    return-void
.end method

.method private static dumpCallerPackage(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    #@0
    .prologue
    .line 477
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@3
    move-result-object v5

    #@4
    .line 478
    .local v5, pm:Landroid/content/pm/PackageManager;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@7
    move-result v6

    #@8
    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    .line 479
    .local v2, callerPackages:[Ljava/lang/String;
    move-object v0, v2

    #@d
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@e
    .local v4, len$:I
    const/4 v3, 0x0

    #@f
    .local v3, i$:I
    :goto_f
    if-ge v3, v4, :cond_2e

    #@11
    aget-object v1, v0, v3

    #@13
    .line 480
    .local v1, callerPackage:Ljava/lang/String;
    const-string v6, "IccPhonebookProvider"

    #@15
    new-instance v7, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v8, "caller info| "

    #@1c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v7

    #@24
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v7

    #@28
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 479
    add-int/lit8 v3, v3, 0x1

    #@2d
    goto :goto_f

    #@2e
    .line 482
    .end local v1           #callerPackage:Ljava/lang/String;
    :cond_2e
    return-void
.end method

.method private getIndexFromUrl(Landroid/net/Uri;)I
    .registers 7
    .parameter "url"

    #@0
    .prologue
    .line 449
    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 450
    .local v1, index:Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 454
    .end local v1           #index:Ljava/lang/String;
    :goto_8
    return v2

    #@9
    .line 451
    :catch_9
    move-exception v0

    #@a
    .line 452
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "IccPhonebookProvider"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "can\'t extract index from url="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@22
    .line 454
    const/4 v2, 0x0

    #@23
    goto :goto_8
.end method

.method private static isAllowedCallerPackage(Landroid/content/Context;)Z
    .registers 9
    .parameter "context"

    #@0
    .prologue
    .line 465
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@3
    move-result-object v6

    #@4
    .line 466
    .local v6, pm:Landroid/content/pm/PackageManager;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@7
    move-result v7

    #@8
    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    .line 467
    .local v2, callerPackages:[Ljava/lang/String;
    move-object v0, v2

    #@d
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@e
    .local v5, len$:I
    const/4 v3, 0x0

    #@f
    .local v3, i$:I
    :goto_f
    if-ge v3, v5, :cond_2b

    #@11
    aget-object v1, v0, v3

    #@13
    .line 468
    .local v1, callerPackage:Ljava/lang/String;
    const/4 v4, 0x0

    #@14
    .local v4, index:I
    :goto_14
    sget-object v7, Lcom/android/internal/telephony/IccPhonebookProvider;->ALLOWED_PACKAGE:[Ljava/lang/String;

    #@16
    array-length v7, v7

    #@17
    if-ge v4, v7, :cond_28

    #@19
    .line 469
    sget-object v7, Lcom/android/internal/telephony/IccPhonebookProvider;->ALLOWED_PACKAGE:[Ljava/lang/String;

    #@1b
    aget-object v7, v7, v4

    #@1d
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v7

    #@21
    if-eqz v7, :cond_25

    #@23
    .line 470
    const/4 v7, 0x1

    #@24
    .line 473
    .end local v1           #callerPackage:Ljava/lang/String;
    .end local v4           #index:I
    :goto_24
    return v7

    #@25
    .line 468
    .restart local v1       #callerPackage:Ljava/lang/String;
    .restart local v4       #index:I
    :cond_25
    add-int/lit8 v4, v4, 0x1

    #@27
    goto :goto_14

    #@28
    .line 467
    :cond_28
    add-int/lit8 v3, v3, 0x1

    #@2a
    goto :goto_f

    #@2b
    .line 473
    .end local v1           #callerPackage:Ljava/lang/String;
    .end local v4           #index:I
    :cond_2b
    const/4 v7, 0x0

    #@2c
    goto :goto_24
.end method

.method private static loadBackend()Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;
    .registers 7

    #@0
    .prologue
    .line 249
    const/4 v2, 0x0

    #@1
    .line 251
    .local v2, clz:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :try_start_1
    const-string v5, "com.android.internal.telephony.IccPhonebookProviderBackendImp"

    #@3
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_6
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_6} :catch_2c

    #@6
    move-result-object v2

    #@7
    .line 256
    :goto_7
    const/4 v1, 0x0

    #@8
    .line 257
    .local v1, backend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;
    if-eqz v2, :cond_12

    #@a
    .line 259
    :try_start_a
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@d
    move-result-object v5

    #@e
    move-object v0, v5

    #@f
    check-cast v0, Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@11
    move-object v1, v0
    :try_end_12
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_12} :catch_1a
    .catch Ljava/lang/InstantiationException; {:try_start_a .. :try_end_12} :catch_23

    #@12
    .line 268
    :cond_12
    :goto_12
    if-nez v1, :cond_19

    #@14
    .line 269
    new-instance v1, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;

    #@16
    .end local v1           #backend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;
    invoke-direct {v1}, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;-><init>()V

    #@19
    .line 271
    .restart local v1       #backend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;
    :cond_19
    return-object v1

    #@1a
    .line 260
    :catch_1a
    move-exception v3

    #@1b
    .line 261
    .local v3, e1:Ljava/lang/IllegalAccessException;
    const-string v5, "IccPhonebookProvider"

    #@1d
    const-string v6, "illegal access"

    #@1f
    invoke-static {v5, v6, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@22
    goto :goto_12

    #@23
    .line 262
    .end local v3           #e1:Ljava/lang/IllegalAccessException;
    :catch_23
    move-exception v4

    #@24
    .line 263
    .local v4, e2:Ljava/lang/InstantiationException;
    const-string v5, "IccPhonebookProvider"

    #@26
    const-string v6, "InstantiationException"

    #@28
    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2b
    goto :goto_12

    #@2c
    .line 252
    .end local v1           #backend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;
    .end local v4           #e2:Ljava/lang/InstantiationException;
    :catch_2c
    move-exception v5

    #@2d
    goto :goto_7
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 10
    .parameter "url"
    .parameter "where"
    .parameter "whereArgs"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 374
    sget-object v4, Lcom/android/internal/telephony/IccPhonebookProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@4
    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@7
    move-result v4

    #@8
    packed-switch v4, :pswitch_data_cc

    #@b
    .line 402
    :goto_b
    return v2

    #@c
    .line 376
    :pswitch_c
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProvider;->getIndexFromUrl(Landroid/net/Uri;)I

    #@f
    move-result v1

    #@10
    .line 377
    .local v1, simIndex:I
    if-gtz v1, :cond_31

    #@12
    .line 378
    const-string v3, "IccPhonebookProvider"

    #@14
    new-instance v4, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v5, "invalid delete simIndex (url="

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    const-string v5, ")"

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_b

    #@31
    .line 381
    :cond_31
    sget-object v4, Lcom/android/internal/telephony/IccPhonebookProvider;->sBackend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@33
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccPhonebookProvider;->getContext()Landroid/content/Context;

    #@36
    move-result-object v5

    #@37
    invoke-interface {v4, v5, v1}, Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;->deleteEntry(Landroid/content/Context;I)I

    #@3a
    move-result v4

    #@3b
    if-eq v1, v4, :cond_5c

    #@3d
    .line 382
    const-string v3, "IccPhonebookProvider"

    #@3f
    new-instance v4, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v5, "requested delete simIndex("

    #@46
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v4

    #@4e
    const-string v5, ") is not matched to backend result"

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v4

    #@58
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    goto :goto_b

    #@5c
    :cond_5c
    move v2, v3

    #@5d
    .line 385
    goto :goto_b

    #@5e
    .line 388
    .end local v1           #simIndex:I
    :pswitch_5e
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProvider;->getIndexFromUrl(Landroid/net/Uri;)I

    #@61
    move-result v0

    #@62
    .line 389
    .local v0, groupIndex:I
    if-gtz v0, :cond_83

    #@64
    .line 390
    const-string v3, "IccPhonebookProvider"

    #@66
    new-instance v4, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v5, "invalid delete groupIndex (url="

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v4

    #@75
    const-string v5, ")"

    #@77
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v4

    #@7b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v4

    #@7f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    goto :goto_b

    #@83
    .line 393
    :cond_83
    sget-object v4, Lcom/android/internal/telephony/IccPhonebookProvider;->sBackend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@85
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccPhonebookProvider;->getContext()Landroid/content/Context;

    #@88
    move-result-object v5

    #@89
    invoke-interface {v4, v5, v0}, Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;->deleteGroup(Landroid/content/Context;I)I

    #@8c
    move-result v4

    #@8d
    if-eq v0, v4, :cond_af

    #@8f
    .line 394
    const-string v3, "IccPhonebookProvider"

    #@91
    new-instance v4, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v5, "requested delete groupIndex("

    #@98
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v4

    #@9c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v4

    #@a0
    const-string v5, ") is not matched to backend result"

    #@a2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v4

    #@a6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v4

    #@aa
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    goto/16 :goto_b

    #@af
    :cond_af
    move v2, v3

    #@b0
    .line 397
    goto/16 :goto_b

    #@b2
    .line 400
    .end local v0           #groupIndex:I
    :pswitch_b2
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@b4
    new-instance v3, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v4, "Cannot delete URL: "

    #@bb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v3

    #@bf
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v3

    #@c3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v3

    #@c7
    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@ca
    throw v2

    #@cb
    .line 374
    nop

    #@cc
    :pswitch_data_cc
    .packed-switch 0x0
        :pswitch_c
        :pswitch_5e
        :pswitch_b2
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "uri"

    #@0
    .prologue
    .line 278
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_26

    #@9
    .line 285
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Unknown URL "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 282
    :pswitch_22
    const-string v0, "vnd.android.cursor.dir/sim-contact"

    #@24
    return-object v0

    #@25
    .line 278
    nop

    #@26
    :pswitch_data_26
    .packed-switch 0x0
        :pswitch_22
        :pswitch_22
        :pswitch_22
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 10
    .parameter "url"
    .parameter "initialValues"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 333
    sget-object v4, Lcom/android/internal/telephony/IccPhonebookProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v4

    #@7
    packed-switch v4, :pswitch_data_c8

    #@a
    .line 362
    :goto_a
    return-object v3

    #@b
    .line 335
    :pswitch_b
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProvider;->getIndexFromUrl(Landroid/net/Uri;)I

    #@e
    move-result v2

    #@f
    .line 336
    .local v2, simIndex:I
    if-gtz v2, :cond_30

    #@11
    .line 337
    const-string v4, "IccPhonebookProvider"

    #@13
    new-instance v5, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v6, "invalid insert simIndex (url="

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    const-string v6, ")"

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_a

    #@30
    .line 340
    :cond_30
    sget-object v4, Lcom/android/internal/telephony/IccPhonebookProvider;->sBackend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@32
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccPhonebookProvider;->getContext()Landroid/content/Context;

    #@35
    move-result-object v5

    #@36
    invoke-interface {v4, v5, v2, p2}, Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;->insertEntry(Landroid/content/Context;ILandroid/content/ContentValues;)I

    #@39
    move-result v4

    #@3a
    if-eq v2, v4, :cond_5b

    #@3c
    .line 341
    const-string v4, "IccPhonebookProvider"

    #@3e
    new-instance v5, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v6, "requested insert simIndex("

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    const-string v6, ") is not matched to backend result"

    #@4f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    goto :goto_a

    #@5b
    .line 345
    :cond_5b
    new-instance v0, Ljava/lang/StringBuilder;

    #@5d
    const-string v3, "content://icc/adn/"

    #@5f
    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@62
    .line 346
    .local v0, buf:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    .line 347
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v3

    #@69
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@6c
    move-result-object v3

    #@6d
    goto :goto_a

    #@6e
    .line 350
    .end local v0           #buf:Ljava/lang/StringBuilder;
    .end local v2           #simIndex:I
    :pswitch_6e
    sget-object v4, Lcom/android/internal/telephony/IccPhonebookProvider;->sBackend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@70
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccPhonebookProvider;->getContext()Landroid/content/Context;

    #@73
    move-result-object v5

    #@74
    invoke-interface {v4, v5, p2}, Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;->insertGroup(Landroid/content/Context;Landroid/content/ContentValues;)I

    #@77
    move-result v1

    #@78
    .line 351
    .local v1, groupIndex:I
    if-gtz v1, :cond_9a

    #@7a
    .line 352
    const-string v4, "IccPhonebookProvider"

    #@7c
    new-instance v5, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v6, "requested insert groupIndex("

    #@83
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v5

    #@87
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    const-string v6, ") is not matched to backend result"

    #@8d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v5

    #@95
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    goto/16 :goto_a

    #@9a
    .line 355
    :cond_9a
    new-instance v0, Ljava/lang/StringBuilder;

    #@9c
    const-string v3, "content://icc/adn/group/"

    #@9e
    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@a1
    .line 356
    .restart local v0       #buf:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a4
    .line 357
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v3

    #@a8
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@ab
    move-result-object v3

    #@ac
    goto/16 :goto_a

    #@ae
    .line 360
    .end local v0           #buf:Ljava/lang/StringBuilder;
    .end local v1           #groupIndex:I
    :pswitch_ae
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    #@b0
    new-instance v4, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v5, "Cannot insert into URL: "

    #@b7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v4

    #@bb
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v4

    #@bf
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v4

    #@c3
    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@c6
    throw v3

    #@c7
    .line 333
    nop

    #@c8
    :pswitch_data_c8
    .packed-switch 0x0
        :pswitch_b
        :pswitch_6e
        :pswitch_ae
    .end packed-switch
.end method

.method public onCreate()Z
    .registers 2

    #@0
    .prologue
    .line 293
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "url"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sort"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 299
    sget-object v3, Lcom/android/internal/telephony/IccPhonebookProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v3

    #@7
    packed-switch v3, :pswitch_data_76

    #@a
    .line 320
    :goto_a
    return-object v2

    #@b
    .line 301
    :pswitch_b
    sget-object v2, Lcom/android/internal/telephony/IccPhonebookProvider;->sBackend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@d
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccPhonebookProvider;->getContext()Landroid/content/Context;

    #@10
    move-result-object v3

    #@11
    invoke-interface {v2, v3}, Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;->readInfo(Landroid/content/Context;)Landroid/database/Cursor;

    #@14
    move-result-object v2

    #@15
    goto :goto_a

    #@16
    .line 304
    :pswitch_16
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProvider;->getIndexFromUrl(Landroid/net/Uri;)I

    #@19
    move-result v1

    #@1a
    .line 305
    .local v1, simIndex:I
    if-gtz v1, :cond_3b

    #@1c
    .line 306
    const-string v3, "IccPhonebookProvider"

    #@1e
    new-instance v4, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v5, "invali query simIndex (url="

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    const-string v5, ")"

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_a

    #@3b
    .line 309
    :cond_3b
    sget-object v2, Lcom/android/internal/telephony/IccPhonebookProvider;->sBackend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@3d
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccPhonebookProvider;->getContext()Landroid/content/Context;

    #@40
    move-result-object v3

    #@41
    invoke-interface {v2, v3, v1}, Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;->readEntry(Landroid/content/Context;I)Landroid/database/Cursor;

    #@44
    move-result-object v2

    #@45
    goto :goto_a

    #@46
    .line 312
    .end local v1           #simIndex:I
    :pswitch_46
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProvider;->getIndexFromUrl(Landroid/net/Uri;)I

    #@49
    move-result v0

    #@4a
    .line 313
    .local v0, groupIndex:I
    if-gtz v0, :cond_6b

    #@4c
    .line 314
    const-string v3, "IccPhonebookProvider"

    #@4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v5, "invalid query groupIndex (url="

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    const-string v5, ")"

    #@5f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v4

    #@63
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v4

    #@67
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    goto :goto_a

    #@6b
    .line 317
    :cond_6b
    sget-object v2, Lcom/android/internal/telephony/IccPhonebookProvider;->sBackend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@6d
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccPhonebookProvider;->getContext()Landroid/content/Context;

    #@70
    move-result-object v3

    #@71
    invoke-interface {v2, v3, v0}, Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;->readGroup(Landroid/content/Context;I)Landroid/database/Cursor;

    #@74
    move-result-object v2

    #@75
    goto :goto_a

    #@76
    .line 299
    :pswitch_data_76
    .packed-switch 0x0
        :pswitch_16
        :pswitch_46
        :pswitch_b
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 11
    .parameter "url"
    .parameter "values"
    .parameter "where"
    .parameter "whereArgs"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 414
    sget-object v4, Lcom/android/internal/telephony/IccPhonebookProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@4
    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@7
    move-result v4

    #@8
    packed-switch v4, :pswitch_data_cc

    #@b
    .line 442
    :goto_b
    return v2

    #@c
    .line 416
    :pswitch_c
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProvider;->getIndexFromUrl(Landroid/net/Uri;)I

    #@f
    move-result v1

    #@10
    .line 417
    .local v1, simIndex:I
    if-gtz v1, :cond_31

    #@12
    .line 418
    const-string v3, "IccPhonebookProvider"

    #@14
    new-instance v4, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v5, "invalid update simIndex (url="

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    const-string v5, ")"

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_b

    #@31
    .line 421
    :cond_31
    sget-object v4, Lcom/android/internal/telephony/IccPhonebookProvider;->sBackend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@33
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccPhonebookProvider;->getContext()Landroid/content/Context;

    #@36
    move-result-object v5

    #@37
    invoke-interface {v4, v5, v1, p2}, Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;->updateEntry(Landroid/content/Context;ILandroid/content/ContentValues;)I

    #@3a
    move-result v4

    #@3b
    if-eq v1, v4, :cond_5c

    #@3d
    .line 422
    const-string v3, "IccPhonebookProvider"

    #@3f
    new-instance v4, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v5, "requested update simIndex("

    #@46
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v4

    #@4e
    const-string v5, ") is not match to backend result"

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v4

    #@58
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    goto :goto_b

    #@5c
    :cond_5c
    move v2, v3

    #@5d
    .line 425
    goto :goto_b

    #@5e
    .line 428
    .end local v1           #simIndex:I
    :pswitch_5e
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProvider;->getIndexFromUrl(Landroid/net/Uri;)I

    #@61
    move-result v0

    #@62
    .line 429
    .local v0, groupIndex:I
    if-gtz v0, :cond_83

    #@64
    .line 430
    const-string v3, "IccPhonebookProvider"

    #@66
    new-instance v4, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v5, "invalid update groupIndex (url="

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v4

    #@75
    const-string v5, ")"

    #@77
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v4

    #@7b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v4

    #@7f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    goto :goto_b

    #@83
    .line 433
    :cond_83
    sget-object v4, Lcom/android/internal/telephony/IccPhonebookProvider;->sBackend:Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;

    #@85
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccPhonebookProvider;->getContext()Landroid/content/Context;

    #@88
    move-result-object v5

    #@89
    invoke-interface {v4, v5, v0, p2}, Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;->updateGroup(Landroid/content/Context;ILandroid/content/ContentValues;)I

    #@8c
    move-result v4

    #@8d
    if-eq v0, v4, :cond_af

    #@8f
    .line 434
    const-string v3, "IccPhonebookProvider"

    #@91
    new-instance v4, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v5, "requested update groupIndex("

    #@98
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v4

    #@9c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v4

    #@a0
    const-string v5, ") is not matched to backend result"

    #@a2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v4

    #@a6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v4

    #@aa
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    goto/16 :goto_b

    #@af
    :cond_af
    move v2, v3

    #@b0
    .line 437
    goto/16 :goto_b

    #@b2
    .line 440
    .end local v0           #groupIndex:I
    :pswitch_b2
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@b4
    new-instance v3, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v4, "Cannot update URL: "

    #@bb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v3

    #@bf
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v3

    #@c3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v3

    #@c7
    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@ca
    throw v2

    #@cb
    .line 414
    nop

    #@cc
    :pswitch_data_cc
    .packed-switch 0x0
        :pswitch_c
        :pswitch_5e
        :pswitch_b2
    .end packed-switch
.end method
