.class Lcom/android/internal/telephony/test/SimulatedGsmCallState;
.super Landroid/os/Handler;
.source "SimulatedGsmCallState.java"


# static fields
.field static final CONNECTING_PAUSE_MSEC:I = 0x1f4

.field static final EVENT_PROGRESS_CALL_STATE:I = 0x1

.field static final MAX_CALLS:I = 0x7


# instance fields
.field private autoProgressConnecting:Z

.field calls:[Lcom/android/internal/telephony/test/CallInfo;

.field private nextDialFailImmediately:Z


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .registers 3
    .parameter "looper"

    #@0
    .prologue
    .line 155
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@3
    .line 135
    const/4 v0, 0x7

    #@4
    new-array v0, v0, [Lcom/android/internal/telephony/test/CallInfo;

    #@6
    iput-object v0, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@8
    .line 137
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->autoProgressConnecting:Z

    #@b
    .line 156
    return-void
.end method

.method private countActiveLines()I
    .registers 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/test/InvalidStateEx;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 766
    const/4 v4, 0x0

    #@3
    .line 767
    .local v4, hasMpty:Z
    const/4 v3, 0x0

    #@4
    .line 768
    .local v3, hasHeld:Z
    const/4 v1, 0x0

    #@5
    .line 769
    .local v1, hasActive:Z
    const/4 v2, 0x0

    #@6
    .line 770
    .local v2, hasConnecting:Z
    const/4 v5, 0x0

    #@7
    .line 771
    .local v5, hasRinging:Z
    const/4 v7, 0x0

    #@8
    .line 773
    .local v7, mptyIsHeld:Z
    const/4 v6, 0x0

    #@9
    .local v6, i:I
    :goto_9
    iget-object v11, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@b
    array-length v11, v11

    #@c
    if-ge v6, v11, :cond_7b

    #@e
    .line 774
    iget-object v11, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@10
    aget-object v0, v11, v6

    #@12
    .line 776
    .local v0, call:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_3e

    #@14
    .line 777
    if-nez v4, :cond_43

    #@16
    iget-boolean v11, v0, Lcom/android/internal/telephony/test/CallInfo;->isMpty:Z

    #@18
    if-eqz v11, :cond_43

    #@1a
    .line 778
    iget-object v11, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@1c
    sget-object v12, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@1e
    if-ne v11, v12, :cond_41

    #@20
    move v7, v9

    #@21
    .line 791
    :cond_21
    :goto_21
    iget-boolean v11, v0, Lcom/android/internal/telephony/test/CallInfo;->isMpty:Z

    #@23
    or-int/2addr v4, v11

    #@24
    .line 792
    iget-object v11, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@26
    sget-object v12, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@28
    if-ne v11, v12, :cond_77

    #@2a
    move v11, v9

    #@2b
    :goto_2b
    or-int/2addr v3, v11

    #@2c
    .line 793
    iget-object v11, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@2e
    sget-object v12, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@30
    if-ne v11, v12, :cond_79

    #@32
    move v11, v9

    #@33
    :goto_33
    or-int/2addr v1, v11

    #@34
    .line 794
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/CallInfo;->isConnecting()Z

    #@37
    move-result v11

    #@38
    or-int/2addr v2, v11

    #@39
    .line 795
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/CallInfo;->isRinging()Z

    #@3c
    move-result v11

    #@3d
    or-int/2addr v5, v11

    #@3e
    .line 773
    :cond_3e
    add-int/lit8 v6, v6, 0x1

    #@40
    goto :goto_9

    #@41
    :cond_41
    move v7, v10

    #@42
    .line 778
    goto :goto_21

    #@43
    .line 779
    :cond_43
    iget-boolean v11, v0, Lcom/android/internal/telephony/test/CallInfo;->isMpty:Z

    #@45
    if-eqz v11, :cond_5c

    #@47
    if-eqz v7, :cond_5c

    #@49
    iget-object v11, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@4b
    sget-object v12, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@4d
    if-ne v11, v12, :cond_5c

    #@4f
    .line 782
    const-string v9, "ModelInterpreter"

    #@51
    const-string v10, "Invalid state"

    #@53
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 783
    new-instance v9, Lcom/android/internal/telephony/test/InvalidStateEx;

    #@58
    invoke-direct {v9}, Lcom/android/internal/telephony/test/InvalidStateEx;-><init>()V

    #@5b
    throw v9

    #@5c
    .line 784
    :cond_5c
    iget-boolean v11, v0, Lcom/android/internal/telephony/test/CallInfo;->isMpty:Z

    #@5e
    if-nez v11, :cond_21

    #@60
    if-eqz v4, :cond_21

    #@62
    if-eqz v7, :cond_21

    #@64
    iget-object v11, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@66
    sget-object v12, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@68
    if-ne v11, v12, :cond_21

    #@6a
    .line 787
    const-string v9, "ModelInterpreter"

    #@6c
    const-string v10, "Invalid state"

    #@6e
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 788
    new-instance v9, Lcom/android/internal/telephony/test/InvalidStateEx;

    #@73
    invoke-direct {v9}, Lcom/android/internal/telephony/test/InvalidStateEx;-><init>()V

    #@76
    throw v9

    #@77
    :cond_77
    move v11, v10

    #@78
    .line 792
    goto :goto_2b

    #@79
    :cond_79
    move v11, v10

    #@7a
    .line 793
    goto :goto_33

    #@7b
    .line 799
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_7b
    const/4 v8, 0x0

    #@7c
    .line 801
    .local v8, ret:I
    if-eqz v3, :cond_80

    #@7e
    add-int/lit8 v8, v8, 0x1

    #@80
    .line 802
    :cond_80
    if-eqz v1, :cond_84

    #@82
    add-int/lit8 v8, v8, 0x1

    #@84
    .line 803
    :cond_84
    if-eqz v2, :cond_88

    #@86
    add-int/lit8 v8, v8, 0x1

    #@88
    .line 804
    :cond_88
    if-eqz v5, :cond_8c

    #@8a
    add-int/lit8 v8, v8, 0x1

    #@8c
    .line 806
    :cond_8c
    return v8
.end method


# virtual methods
.method public conference()Z
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 607
    const/4 v1, 0x0

    #@2
    .line 610
    .local v1, countCalls:I
    const/4 v2, 0x0

    #@3
    .local v2, i:I
    :goto_3
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@5
    array-length v4, v4

    #@6
    if-ge v2, v4, :cond_1b

    #@8
    .line 611
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@a
    aget-object v0, v4, v2

    #@c
    .line 613
    .local v0, c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_18

    #@e
    .line 614
    add-int/lit8 v1, v1, 0x1

    #@10
    .line 616
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/CallInfo;->isConnecting()Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_18

    #@16
    .line 617
    const/4 v3, 0x0

    #@17
    .line 632
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_17
    return v3

    #@18
    .line 610
    .restart local v0       #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_18
    add-int/lit8 v2, v2, 0x1

    #@1a
    goto :goto_3

    #@1b
    .line 621
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_1b
    const/4 v2, 0x0

    #@1c
    :goto_1c
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@1e
    array-length v4, v4

    #@1f
    if-ge v2, v4, :cond_17

    #@21
    .line 622
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@23
    aget-object v0, v4, v2

    #@25
    .line 624
    .restart local v0       #c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_2f

    #@27
    .line 625
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@29
    iput-object v4, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@2b
    .line 626
    if-lez v1, :cond_2f

    #@2d
    .line 627
    iput-boolean v3, v0, Lcom/android/internal/telephony/test/CallInfo;->isMpty:Z

    #@2f
    .line 621
    :cond_2f
    add-int/lit8 v2, v2, 0x1

    #@31
    goto :goto_1c
.end method

.method public explicitCallTransfer()Z
    .registers 5

    #@0
    .prologue
    .line 637
    const/4 v1, 0x0

    #@1
    .line 640
    .local v1, countCalls:I
    const/4 v2, 0x0

    #@2
    .local v2, i:I
    :goto_2
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@4
    array-length v3, v3

    #@5
    if-ge v2, v3, :cond_1a

    #@7
    .line 641
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@9
    aget-object v0, v3, v2

    #@b
    .line 643
    .local v0, c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_17

    #@d
    .line 644
    add-int/lit8 v1, v1, 0x1

    #@f
    .line 646
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/CallInfo;->isConnecting()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_17

    #@15
    .line 647
    const/4 v3, 0x0

    #@16
    .line 653
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :goto_16
    return v3

    #@17
    .line 640
    .restart local v0       #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_17
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_2

    #@1a
    .line 653
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_1a
    invoke-virtual {p0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->triggerHangupAll()Z

    #@1d
    move-result v3

    #@1e
    goto :goto_16
.end method

.method public getClccLines()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 751
    new-instance v2, Ljava/util/ArrayList;

    #@2
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@4
    array-length v3, v3

    #@5
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    #@8
    .line 753
    .local v2, ret:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@b
    array-length v3, v3

    #@c
    if-ge v1, v3, :cond_20

    #@e
    .line 754
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@10
    aget-object v0, v3, v1

    #@12
    .line 756
    .local v0, c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_1d

    #@14
    .line 757
    add-int/lit8 v3, v1, 0x1

    #@16
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/test/CallInfo;->toCLCCLine(I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    .line 753
    :cond_1d
    add-int/lit8 v1, v1, 0x1

    #@1f
    goto :goto_9

    #@20
    .line 761
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_20
    return-object v2
.end method

.method public getDriverCalls()Ljava/util/List;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/DriverCall;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 731
    new-instance v3, Ljava/util/ArrayList;

    #@2
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@4
    array-length v4, v4

    #@5
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    #@8
    .line 733
    .local v3, ret:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DriverCall;>;"
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@b
    array-length v4, v4

    #@c
    if-ge v2, v4, :cond_20

    #@e
    .line 734
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@10
    aget-object v0, v4, v2

    #@12
    .line 736
    .local v0, c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_1d

    #@14
    .line 739
    add-int/lit8 v4, v2, 0x1

    #@16
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/test/CallInfo;->toDriverCall(I)Lcom/android/internal/telephony/DriverCall;

    #@19
    move-result-object v1

    #@1a
    .line 740
    .local v1, dc:Lcom/android/internal/telephony/DriverCall;
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    .line 733
    .end local v1           #dc:Lcom/android/internal/telephony/DriverCall;
    :cond_1d
    add-int/lit8 v2, v2, 0x1

    #@1f
    goto :goto_9

    #@20
    .line 744
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_20
    const-string v4, "GSM"

    #@22
    new-instance v5, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v6, "SC< getDriverCalls "

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 746
    return-object v3
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 160
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v0, :pswitch_data_10

    #@6
    .line 167
    :goto_6
    monitor-exit p0

    #@7
    .line 168
    return-void

    #@8
    .line 165
    :pswitch_8
    invoke-virtual {p0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->progressConnectingCallState()V

    #@b
    goto :goto_6

    #@c
    .line 167
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0

    #@f
    .line 160
    nop

    #@10
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch
.end method

.method public onAnswer()Z
    .registers 5

    #@0
    .prologue
    .line 358
    monitor-enter p0

    #@1
    .line 359
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    :try_start_2
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@4
    array-length v2, v2

    #@5
    if-ge v1, v2, :cond_22

    #@7
    .line 360
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@9
    aget-object v0, v2, v1

    #@b
    .line 362
    .local v0, call:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_1f

    #@d
    iget-object v2, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@f
    sget-object v3, Lcom/android/internal/telephony/test/CallInfo$State;->INCOMING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@11
    if-eq v2, v3, :cond_19

    #@13
    iget-object v2, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@15
    sget-object v3, Lcom/android/internal/telephony/test/CallInfo$State;->WAITING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@17
    if-ne v2, v3, :cond_1f

    #@19
    .line 366
    :cond_19
    invoke-virtual {p0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->switchActiveAndHeldOrWaiting()Z

    #@1c
    move-result v2

    #@1d
    monitor-exit p0

    #@1e
    .line 371
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :goto_1e
    return v2

    #@1f
    .line 359
    .restart local v0       #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_2

    #@22
    .line 369
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_22
    monitor-exit p0

    #@23
    .line 371
    const/4 v2, 0x0

    #@24
    goto :goto_1e

    #@25
    .line 369
    :catchall_25
    move-exception v2

    #@26
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_2 .. :try_end_27} :catchall_25

    #@27
    throw v2
.end method

.method public onChld(CC)Z
    .registers 7
    .parameter "c0"
    .parameter "c1"

    #@0
    .prologue
    .line 393
    const/4 v0, 0x0

    #@1
    .line 395
    .local v0, callIndex:I
    if-eqz p2, :cond_e

    #@3
    .line 396
    add-int/lit8 v0, p2, -0x31

    #@5
    .line 398
    if-ltz v0, :cond_c

    #@7
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@9
    array-length v2, v2

    #@a
    if-lt v0, v2, :cond_e

    #@c
    .line 399
    :cond_c
    const/4 v1, 0x0

    #@d
    .line 443
    :goto_d
    return v1

    #@e
    .line 403
    :cond_e
    packed-switch p1, :pswitch_data_46

    #@11
    .line 439
    const/4 v1, 0x0

    #@12
    .local v1, ret:Z
    goto :goto_d

    #@13
    .line 405
    .end local v1           #ret:Z
    :pswitch_13
    invoke-virtual {p0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->releaseHeldOrUDUB()Z

    #@16
    move-result v1

    #@17
    .line 406
    .restart local v1       #ret:Z
    goto :goto_d

    #@18
    .line 408
    .end local v1           #ret:Z
    :pswitch_18
    if-gtz p2, :cond_1f

    #@1a
    .line 409
    invoke-virtual {p0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->releaseActiveAcceptHeldOrWaiting()Z

    #@1d
    move-result v1

    #@1e
    .restart local v1       #ret:Z
    goto :goto_d

    #@1f
    .line 411
    .end local v1           #ret:Z
    :cond_1f
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@21
    aget-object v2, v2, v0

    #@23
    if-nez v2, :cond_27

    #@25
    .line 412
    const/4 v1, 0x0

    #@26
    .restart local v1       #ret:Z
    goto :goto_d

    #@27
    .line 414
    .end local v1           #ret:Z
    :cond_27
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@29
    const/4 v3, 0x0

    #@2a
    aput-object v3, v2, v0

    #@2c
    .line 415
    const/4 v1, 0x1

    #@2d
    .line 418
    .restart local v1       #ret:Z
    goto :goto_d

    #@2e
    .line 420
    .end local v1           #ret:Z
    :pswitch_2e
    if-gtz p2, :cond_35

    #@30
    .line 421
    invoke-virtual {p0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->switchActiveAndHeldOrWaiting()Z

    #@33
    move-result v1

    #@34
    .restart local v1       #ret:Z
    goto :goto_d

    #@35
    .line 423
    .end local v1           #ret:Z
    :cond_35
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->separateCall(I)Z

    #@38
    move-result v1

    #@39
    .line 425
    .restart local v1       #ret:Z
    goto :goto_d

    #@3a
    .line 427
    .end local v1           #ret:Z
    :pswitch_3a
    invoke-virtual {p0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->conference()Z

    #@3d
    move-result v1

    #@3e
    .line 428
    .restart local v1       #ret:Z
    goto :goto_d

    #@3f
    .line 430
    .end local v1           #ret:Z
    :pswitch_3f
    invoke-virtual {p0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->explicitCallTransfer()Z

    #@42
    move-result v1

    #@43
    .line 431
    .restart local v1       #ret:Z
    goto :goto_d

    #@44
    .line 435
    .end local v1           #ret:Z
    :pswitch_44
    const/4 v1, 0x0

    #@45
    .line 437
    .restart local v1       #ret:Z
    goto :goto_d

    #@46
    .line 403
    :pswitch_data_46
    .packed-switch 0x30
        :pswitch_13
        :pswitch_18
        :pswitch_2e
        :pswitch_3a
        :pswitch_3f
        :pswitch_44
    .end packed-switch
.end method

.method public onDial(Ljava/lang/String;)Z
    .registers 11
    .parameter "address"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 659
    const/4 v1, -0x1

    #@3
    .line 661
    .local v1, freeSlot:I
    const-string v6, "GSM"

    #@5
    new-instance v7, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v8, "SC> dial \'"

    #@c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v7

    #@10
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v7

    #@14
    const-string v8, "\'"

    #@16
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v7

    #@1a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 663
    iget-boolean v6, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->nextDialFailImmediately:Z

    #@23
    if-eqz v6, :cond_2f

    #@25
    .line 664
    iput-boolean v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->nextDialFailImmediately:Z

    #@27
    .line 666
    const-string v5, "GSM"

    #@29
    const-string v6, "SC< dial fail (per request)"

    #@2b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 726
    :goto_2e
    return v4

    #@2f
    .line 670
    :cond_2f
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    .line 672
    .local v3, phNum:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@36
    move-result v6

    #@37
    if-nez v6, :cond_41

    #@39
    .line 673
    const-string v5, "GSM"

    #@3b
    const-string v6, "SC< dial fail (invalid ph num)"

    #@3d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_2e

    #@41
    .line 678
    :cond_41
    const-string v6, "*99"

    #@43
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@46
    move-result v6

    #@47
    if-eqz v6, :cond_5a

    #@49
    const-string v6, "#"

    #@4b
    invoke-virtual {v3, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@4e
    move-result v6

    #@4f
    if-eqz v6, :cond_5a

    #@51
    .line 679
    const-string v4, "GSM"

    #@53
    const-string v6, "SC< dial ignored (gprs)"

    #@55
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    move v4, v5

    #@59
    .line 680
    goto :goto_2e

    #@5a
    .line 686
    :cond_5a
    :try_start_5a
    invoke-direct {p0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->countActiveLines()I

    #@5d
    move-result v6

    #@5e
    if-le v6, v5, :cond_71

    #@60
    .line 687
    const-string v5, "GSM"

    #@62
    const-string v6, "SC< dial fail (invalid call state)"

    #@64
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_67
    .catch Lcom/android/internal/telephony/test/InvalidStateEx; {:try_start_5a .. :try_end_67} :catch_68

    #@67
    goto :goto_2e

    #@68
    .line 690
    :catch_68
    move-exception v0

    #@69
    .line 691
    .local v0, ex:Lcom/android/internal/telephony/test/InvalidStateEx;
    const-string v5, "GSM"

    #@6b
    const-string v6, "SC< dial fail (invalid call state)"

    #@6d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    goto :goto_2e

    #@71
    .line 695
    .end local v0           #ex:Lcom/android/internal/telephony/test/InvalidStateEx;
    :cond_71
    const/4 v2, 0x0

    #@72
    .local v2, i:I
    :goto_72
    iget-object v6, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@74
    array-length v6, v6

    #@75
    if-ge v2, v6, :cond_b3

    #@77
    .line 696
    if-gez v1, :cond_80

    #@79
    iget-object v6, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@7b
    aget-object v6, v6, v2

    #@7d
    if-nez v6, :cond_80

    #@7f
    .line 697
    move v1, v2

    #@80
    .line 700
    :cond_80
    iget-object v6, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@82
    aget-object v6, v6, v2

    #@84
    if-eqz v6, :cond_98

    #@86
    iget-object v6, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@88
    aget-object v6, v6, v2

    #@8a
    invoke-virtual {v6}, Lcom/android/internal/telephony/test/CallInfo;->isActiveOrHeld()Z

    #@8d
    move-result v6

    #@8e
    if-nez v6, :cond_98

    #@90
    .line 703
    const-string v5, "GSM"

    #@92
    const-string v6, "SC< dial fail (invalid call state)"

    #@94
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    goto :goto_2e

    #@98
    .line 705
    :cond_98
    iget-object v6, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@9a
    aget-object v6, v6, v2

    #@9c
    if-eqz v6, :cond_b0

    #@9e
    iget-object v6, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@a0
    aget-object v6, v6, v2

    #@a2
    iget-object v6, v6, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@a4
    sget-object v7, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@a6
    if-ne v6, v7, :cond_b0

    #@a8
    .line 707
    iget-object v6, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@aa
    aget-object v6, v6, v2

    #@ac
    sget-object v7, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@ae
    iput-object v7, v6, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@b0
    .line 695
    :cond_b0
    add-int/lit8 v2, v2, 0x1

    #@b2
    goto :goto_72

    #@b3
    .line 711
    :cond_b3
    if-gez v1, :cond_be

    #@b5
    .line 712
    const-string v5, "GSM"

    #@b7
    const-string v6, "SC< dial fail (invalid call state)"

    #@b9
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    goto/16 :goto_2e

    #@be
    .line 716
    :cond_be
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@c0
    invoke-static {v3}, Lcom/android/internal/telephony/test/CallInfo;->createOutgoingCall(Ljava/lang/String;)Lcom/android/internal/telephony/test/CallInfo;

    #@c3
    move-result-object v6

    #@c4
    aput-object v6, v4, v1

    #@c6
    .line 718
    iget-boolean v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->autoProgressConnecting:Z

    #@c8
    if-eqz v4, :cond_d7

    #@ca
    .line 719
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@cc
    aget-object v4, v4, v1

    #@ce
    invoke-virtual {p0, v5, v4}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@d1
    move-result-object v4

    #@d2
    const-wide/16 v6, 0x1f4

    #@d4
    invoke-virtual {p0, v4, v6, v7}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@d7
    .line 724
    :cond_d7
    const-string v4, "GSM"

    #@d9
    new-instance v6, Ljava/lang/StringBuilder;

    #@db
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@de
    const-string v7, "SC< dial (slot = "

    #@e0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v6

    #@e4
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v6

    #@e8
    const-string v7, ")"

    #@ea
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v6

    #@ee
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v6

    #@f2
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f5
    move v4, v5

    #@f6
    .line 726
    goto/16 :goto_2e
.end method

.method public onHangup()Z
    .registers 6

    #@0
    .prologue
    .line 376
    const/4 v1, 0x0

    #@1
    .line 378
    .local v1, found:Z
    const/4 v2, 0x0

    #@2
    .local v2, i:I
    :goto_2
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@4
    array-length v3, v3

    #@5
    if-ge v2, v3, :cond_1c

    #@7
    .line 379
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@9
    aget-object v0, v3, v2

    #@b
    .line 381
    .local v0, call:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_19

    #@d
    iget-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@f
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->WAITING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@11
    if-eq v3, v4, :cond_19

    #@13
    .line 382
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@15
    const/4 v4, 0x0

    #@16
    aput-object v4, v3, v2

    #@18
    .line 383
    const/4 v1, 0x1

    #@19
    .line 378
    :cond_19
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_2

    #@1c
    .line 387
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_1c
    return v1
.end method

.method public progressConnectingCallState()V
    .registers 6

    #@0
    .prologue
    .line 219
    monitor-enter p0

    #@1
    .line 220
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    :try_start_2
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@4
    array-length v2, v2

    #@5
    if-ge v1, v2, :cond_25

    #@7
    .line 221
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@9
    aget-object v0, v2, v1

    #@b
    .line 223
    .local v0, call:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_27

    #@d
    iget-object v2, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@f
    sget-object v3, Lcom/android/internal/telephony/test/CallInfo$State;->DIALING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@11
    if-ne v2, v3, :cond_27

    #@13
    .line 224
    sget-object v2, Lcom/android/internal/telephony/test/CallInfo$State;->ALERTING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@15
    iput-object v2, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@17
    .line 226
    iget-boolean v2, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->autoProgressConnecting:Z

    #@19
    if-eqz v2, :cond_25

    #@1b
    .line 227
    const/4 v2, 0x1

    #@1c
    invoke-virtual {p0, v2, v0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1f
    move-result-object v2

    #@20
    const-wide/16 v3, 0x1f4

    #@22
    invoke-virtual {p0, v2, v3, v4}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@25
    .line 239
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_25
    :goto_25
    monitor-exit p0

    #@26
    .line 240
    return-void

    #@27
    .line 232
    .restart local v0       #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_27
    if-eqz v0, :cond_37

    #@29
    iget-object v2, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@2b
    sget-object v3, Lcom/android/internal/telephony/test/CallInfo$State;->ALERTING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@2d
    if-ne v2, v3, :cond_37

    #@2f
    .line 235
    sget-object v2, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@31
    iput-object v2, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@33
    goto :goto_25

    #@34
    .line 239
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :catchall_34
    move-exception v2

    #@35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_2 .. :try_end_36} :catchall_34

    #@36
    throw v2

    #@37
    .line 220
    .restart local v0       #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_37
    add-int/lit8 v1, v1, 0x1

    #@39
    goto :goto_2
.end method

.method public progressConnectingToActive()V
    .registers 5

    #@0
    .prologue
    .line 245
    monitor-enter p0

    #@1
    .line 246
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    :try_start_2
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@4
    array-length v2, v2

    #@5
    if-ge v1, v2, :cond_1d

    #@7
    .line 247
    iget-object v2, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@9
    aget-object v0, v2, v1

    #@b
    .line 249
    .local v0, call:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_1f

    #@d
    iget-object v2, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@f
    sget-object v3, Lcom/android/internal/telephony/test/CallInfo$State;->DIALING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@11
    if-eq v2, v3, :cond_19

    #@13
    iget-object v2, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@15
    sget-object v3, Lcom/android/internal/telephony/test/CallInfo$State;->ALERTING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@17
    if-ne v2, v3, :cond_1f

    #@19
    .line 252
    :cond_19
    sget-object v2, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@1b
    iput-object v2, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@1d
    .line 256
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_1d
    monitor-exit p0

    #@1e
    .line 257
    return-void

    #@1f
    .line 246
    .restart local v0       #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_2

    #@22
    .line 256
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :catchall_22
    move-exception v2

    #@23
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_2 .. :try_end_24} :catchall_22

    #@24
    throw v2
.end method

.method public releaseActiveAcceptHeldOrWaiting()Z
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 478
    const/4 v2, 0x0

    #@3
    .line 479
    .local v2, foundHeld:Z
    const/4 v1, 0x0

    #@4
    .line 481
    .local v1, foundActive:Z
    const/4 v3, 0x0

    #@5
    .local v3, i:I
    :goto_5
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@7
    array-length v4, v4

    #@8
    if-ge v3, v4, :cond_1e

    #@a
    .line 482
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@c
    aget-object v0, v4, v3

    #@e
    .line 484
    .local v0, c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_1b

    #@10
    iget-object v4, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@12
    sget-object v5, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@14
    if-ne v4, v5, :cond_1b

    #@16
    .line 485
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@18
    aput-object v7, v4, v3

    #@1a
    .line 486
    const/4 v1, 0x1

    #@1b
    .line 481
    :cond_1b
    add-int/lit8 v3, v3, 0x1

    #@1d
    goto :goto_5

    #@1e
    .line 490
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_1e
    if-nez v1, :cond_40

    #@20
    .line 493
    const/4 v3, 0x0

    #@21
    :goto_21
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@23
    array-length v4, v4

    #@24
    if-ge v3, v4, :cond_40

    #@26
    .line 494
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@28
    aget-object v0, v4, v3

    #@2a
    .line 496
    .restart local v0       #c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_3d

    #@2c
    iget-object v4, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@2e
    sget-object v5, Lcom/android/internal/telephony/test/CallInfo$State;->DIALING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@30
    if-eq v4, v5, :cond_38

    #@32
    iget-object v4, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@34
    sget-object v5, Lcom/android/internal/telephony/test/CallInfo$State;->ALERTING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@36
    if-ne v4, v5, :cond_3d

    #@38
    .line 500
    :cond_38
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@3a
    aput-object v7, v4, v3

    #@3c
    .line 501
    const/4 v1, 0x1

    #@3d
    .line 493
    :cond_3d
    add-int/lit8 v3, v3, 0x1

    #@3f
    goto :goto_21

    #@40
    .line 506
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_40
    const/4 v3, 0x0

    #@41
    :goto_41
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@43
    array-length v4, v4

    #@44
    if-ge v3, v4, :cond_5a

    #@46
    .line 507
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@48
    aget-object v0, v4, v3

    #@4a
    .line 509
    .restart local v0       #c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_57

    #@4c
    iget-object v4, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@4e
    sget-object v5, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@50
    if-ne v4, v5, :cond_57

    #@52
    .line 510
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@54
    iput-object v4, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@56
    .line 511
    const/4 v2, 0x1

    #@57
    .line 506
    :cond_57
    add-int/lit8 v3, v3, 0x1

    #@59
    goto :goto_41

    #@5a
    .line 515
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_5a
    if-eqz v2, :cond_5d

    #@5c
    .line 528
    :cond_5c
    :goto_5c
    return v6

    #@5d
    .line 519
    :cond_5d
    const/4 v3, 0x0

    #@5e
    :goto_5e
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@60
    array-length v4, v4

    #@61
    if-ge v3, v4, :cond_5c

    #@63
    .line 520
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@65
    aget-object v0, v4, v3

    #@67
    .line 522
    .restart local v0       #c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_74

    #@69
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/CallInfo;->isRinging()Z

    #@6c
    move-result v4

    #@6d
    if-eqz v4, :cond_74

    #@6f
    .line 523
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@71
    iput-object v4, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@73
    goto :goto_5c

    #@74
    .line 519
    :cond_74
    add-int/lit8 v3, v3, 0x1

    #@76
    goto :goto_5e
.end method

.method public releaseHeldOrUDUB()Z
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 448
    const/4 v1, 0x0

    #@2
    .line 450
    .local v1, found:Z
    const/4 v2, 0x0

    #@3
    .local v2, i:I
    :goto_3
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@5
    array-length v3, v3

    #@6
    if-ge v2, v3, :cond_19

    #@8
    .line 451
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@a
    aget-object v0, v3, v2

    #@c
    .line 453
    .local v0, c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_35

    #@e
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/CallInfo;->isRinging()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_35

    #@14
    .line 454
    const/4 v1, 0x1

    #@15
    .line 455
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@17
    aput-object v5, v3, v2

    #@19
    .line 460
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_19
    if-nez v1, :cond_38

    #@1b
    .line 461
    const/4 v2, 0x0

    #@1c
    :goto_1c
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@1e
    array-length v3, v3

    #@1f
    if-ge v2, v3, :cond_38

    #@21
    .line 462
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@23
    aget-object v0, v3, v2

    #@25
    .line 464
    .restart local v0       #c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_32

    #@27
    iget-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@29
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@2b
    if-ne v3, v4, :cond_32

    #@2d
    .line 465
    const/4 v1, 0x1

    #@2e
    .line 466
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@30
    aput-object v5, v3, v2

    #@32
    .line 461
    :cond_32
    add-int/lit8 v2, v2, 0x1

    #@34
    goto :goto_1c

    #@35
    .line 450
    :cond_35
    add-int/lit8 v2, v2, 0x1

    #@37
    goto :goto_3

    #@38
    .line 472
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_38
    const/4 v3, 0x1

    #@39
    return v3
.end method

.method public separateCall(I)Z
    .registers 12
    .parameter "index"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 569
    :try_start_2
    iget-object v8, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@4
    aget-object v0, v8, p1

    #@6
    .line 571
    .local v0, c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_14

    #@8
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/CallInfo;->isConnecting()Z

    #@b
    move-result v8

    #@c
    if-nez v8, :cond_14

    #@e
    invoke-direct {p0}, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->countActiveLines()I

    #@11
    move-result v8

    #@12
    if-eq v8, v6, :cond_16

    #@14
    :cond_14
    move v6, v7

    #@15
    .line 599
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_15
    :goto_15
    return v6

    #@16
    .line 575
    .restart local v0       #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_16
    sget-object v8, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@18
    iput-object v8, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@1a
    .line 576
    const/4 v8, 0x0

    #@1b
    iput-boolean v8, v0, Lcom/android/internal/telephony/test/CallInfo;->isMpty:Z

    #@1d
    .line 578
    const/4 v4, 0x0

    #@1e
    .local v4, i:I
    :goto_1e
    iget-object v8, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@20
    array-length v8, v8

    #@21
    if-ge v4, v8, :cond_15

    #@23
    .line 579
    const/4 v2, 0x0

    #@24
    .local v2, countHeld:I
    const/4 v5, 0x0

    #@25
    .line 581
    .local v5, lastHeld:I
    if-eq v4, p1, :cond_3a

    #@27
    .line 582
    iget-object v8, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@29
    aget-object v1, v8, v4

    #@2b
    .line 584
    .local v1, cb:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v1, :cond_3a

    #@2d
    iget-object v8, v1, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@2f
    sget-object v9, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@31
    if-ne v8, v9, :cond_3a

    #@33
    .line 585
    sget-object v8, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@35
    iput-object v8, v1, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@37
    .line 586
    add-int/lit8 v2, v2, 0x1

    #@39
    .line 587
    move v5, v4

    #@3a
    .line 591
    .end local v1           #cb:Lcom/android/internal/telephony/test/CallInfo;
    :cond_3a
    if-ne v2, v6, :cond_43

    #@3c
    .line 593
    iget-object v8, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@3e
    aget-object v8, v8, v5

    #@40
    const/4 v9, 0x0

    #@41
    iput-boolean v9, v8, Lcom/android/internal/telephony/test/CallInfo;->isMpty:Z
    :try_end_43
    .catch Lcom/android/internal/telephony/test/InvalidStateEx; {:try_start_2 .. :try_end_43} :catch_46

    #@43
    .line 578
    :cond_43
    add-int/lit8 v4, v4, 0x1

    #@45
    goto :goto_1e

    #@46
    .line 598
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    .end local v2           #countHeld:I
    .end local v4           #i:I
    .end local v5           #lastHeld:I
    :catch_46
    move-exception v3

    #@47
    .local v3, ex:Lcom/android/internal/telephony/test/InvalidStateEx;
    move v6, v7

    #@48
    .line 599
    goto :goto_15
.end method

.method public setAutoProgressConnectingCall(Z)V
    .registers 2
    .parameter "b"

    #@0
    .prologue
    .line 264
    iput-boolean p1, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->autoProgressConnecting:Z

    #@2
    .line 265
    return-void
.end method

.method public setNextDialFailImmediately(Z)V
    .registers 2
    .parameter "b"

    #@0
    .prologue
    .line 269
    iput-boolean p1, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->nextDialFailImmediately:Z

    #@2
    .line 270
    return-void
.end method

.method public switchActiveAndHeldOrWaiting()Z
    .registers 6

    #@0
    .prologue
    .line 533
    const/4 v1, 0x0

    #@1
    .line 536
    .local v1, hasHeld:Z
    const/4 v2, 0x0

    #@2
    .local v2, i:I
    :goto_2
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@4
    array-length v3, v3

    #@5
    if-ge v2, v3, :cond_14

    #@7
    .line 537
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@9
    aget-object v0, v3, v2

    #@b
    .line 539
    .local v0, c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_2d

    #@d
    iget-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@f
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@11
    if-ne v3, v4, :cond_2d

    #@13
    .line 540
    const/4 v1, 0x1

    #@14
    .line 546
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_14
    const/4 v2, 0x0

    #@15
    :goto_15
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@17
    array-length v3, v3

    #@18
    if-ge v2, v3, :cond_48

    #@1a
    .line 547
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@1c
    aget-object v0, v3, v2

    #@1e
    .line 549
    .restart local v0       #c:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_2a

    #@20
    .line 550
    iget-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@22
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@24
    if-ne v3, v4, :cond_30

    #@26
    .line 551
    sget-object v3, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@28
    iput-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@2a
    .line 546
    :cond_2a
    :goto_2a
    add-int/lit8 v2, v2, 0x1

    #@2c
    goto :goto_15

    #@2d
    .line 536
    :cond_2d
    add-int/lit8 v2, v2, 0x1

    #@2f
    goto :goto_2

    #@30
    .line 552
    :cond_30
    iget-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@32
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@34
    if-ne v3, v4, :cond_3b

    #@36
    .line 553
    sget-object v3, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@38
    iput-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@3a
    goto :goto_2a

    #@3b
    .line 554
    :cond_3b
    if-nez v1, :cond_2a

    #@3d
    invoke-virtual {v0}, Lcom/android/internal/telephony/test/CallInfo;->isRinging()Z

    #@40
    move-result v3

    #@41
    if-eqz v3, :cond_2a

    #@43
    .line 555
    sget-object v3, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@45
    iput-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@47
    goto :goto_2a

    #@48
    .line 560
    .end local v0           #c:Lcom/android/internal/telephony/test/CallInfo;
    :cond_48
    const/4 v3, 0x1

    #@49
    return v3
.end method

.method public triggerHangupAll()Z
    .registers 6

    #@0
    .prologue
    .line 339
    monitor-enter p0

    #@1
    .line 340
    const/4 v1, 0x0

    #@2
    .line 342
    .local v1, found:Z
    const/4 v2, 0x0

    #@3
    .local v2, i:I
    :goto_3
    :try_start_3
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@5
    array-length v3, v3

    #@6
    if-ge v2, v3, :cond_1b

    #@8
    .line 343
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@a
    aget-object v0, v3, v2

    #@c
    .line 345
    .local v0, call:Lcom/android/internal/telephony/test/CallInfo;
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@e
    aget-object v3, v3, v2

    #@10
    if-eqz v3, :cond_13

    #@12
    .line 346
    const/4 v1, 0x1

    #@13
    .line 349
    :cond_13
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@15
    const/4 v4, 0x0

    #@16
    aput-object v4, v3, v2

    #@18
    .line 342
    add-int/lit8 v2, v2, 0x1

    #@1a
    goto :goto_3

    #@1b
    .line 352
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_1b
    monitor-exit p0

    #@1c
    return v1

    #@1d
    .line 353
    :catchall_1d
    move-exception v3

    #@1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    #@1f
    throw v3
.end method

.method public triggerHangupBackground()Z
    .registers 6

    #@0
    .prologue
    .line 317
    monitor-enter p0

    #@1
    .line 318
    const/4 v1, 0x0

    #@2
    .line 320
    .local v1, found:Z
    const/4 v2, 0x0

    #@3
    .local v2, i:I
    :goto_3
    :try_start_3
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@5
    array-length v3, v3

    #@6
    if-ge v2, v3, :cond_1d

    #@8
    .line 321
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@a
    aget-object v0, v3, v2

    #@c
    .line 323
    .local v0, call:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_1a

    #@e
    iget-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@10
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@12
    if-ne v3, v4, :cond_1a

    #@14
    .line 324
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@16
    const/4 v4, 0x0

    #@17
    aput-object v4, v3, v2

    #@19
    .line 325
    const/4 v1, 0x1

    #@1a
    .line 320
    :cond_1a
    add-int/lit8 v2, v2, 0x1

    #@1c
    goto :goto_3

    #@1d
    .line 329
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_1d
    monitor-exit p0

    #@1e
    return v1

    #@1f
    .line 330
    :catchall_1f
    move-exception v3

    #@20
    monitor-exit p0
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    #@21
    throw v3
.end method

.method public triggerHangupForeground()Z
    .registers 6

    #@0
    .prologue
    .line 278
    monitor-enter p0

    #@1
    .line 281
    const/4 v1, 0x0

    #@2
    .line 283
    .local v1, found:Z
    const/4 v2, 0x0

    #@3
    .local v2, i:I
    :goto_3
    :try_start_3
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@5
    array-length v3, v3

    #@6
    if-ge v2, v3, :cond_23

    #@8
    .line 284
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@a
    aget-object v0, v3, v2

    #@c
    .line 286
    .local v0, call:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_20

    #@e
    iget-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@10
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->INCOMING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@12
    if-eq v3, v4, :cond_1a

    #@14
    iget-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@16
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->WAITING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@18
    if-ne v3, v4, :cond_20

    #@1a
    .line 290
    :cond_1a
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@1c
    const/4 v4, 0x0

    #@1d
    aput-object v4, v3, v2

    #@1f
    .line 291
    const/4 v1, 0x1

    #@20
    .line 283
    :cond_20
    add-int/lit8 v2, v2, 0x1

    #@22
    goto :goto_3

    #@23
    .line 295
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_23
    const/4 v2, 0x0

    #@24
    :goto_24
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@26
    array-length v3, v3

    #@27
    if-ge v2, v3, :cond_4a

    #@29
    .line 296
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@2b
    aget-object v0, v3, v2

    #@2d
    .line 298
    .restart local v0       #call:Lcom/android/internal/telephony/test/CallInfo;
    if-eqz v0, :cond_47

    #@2f
    iget-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@31
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->DIALING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@33
    if-eq v3, v4, :cond_41

    #@35
    iget-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@37
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@39
    if-eq v3, v4, :cond_41

    #@3b
    iget-object v3, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@3d
    sget-object v4, Lcom/android/internal/telephony/test/CallInfo$State;->ALERTING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@3f
    if-ne v3, v4, :cond_47

    #@41
    .line 303
    :cond_41
    iget-object v3, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@43
    const/4 v4, 0x0

    #@44
    aput-object v4, v3, v2

    #@46
    .line 304
    const/4 v1, 0x1

    #@47
    .line 295
    :cond_47
    add-int/lit8 v2, v2, 0x1

    #@49
    goto :goto_24

    #@4a
    .line 307
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_4a
    monitor-exit p0

    #@4b
    return v1

    #@4c
    .line 308
    :catchall_4c
    move-exception v3

    #@4d
    monitor-exit p0
    :try_end_4e
    .catchall {:try_start_3 .. :try_end_4e} :catchall_4c

    #@4e
    throw v3
.end method

.method public triggerRing(Ljava/lang/String;)Z
    .registers 9
    .parameter "number"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 178
    monitor-enter p0

    #@2
    .line 179
    const/4 v1, -0x1

    #@3
    .line 180
    .local v1, empty:I
    const/4 v3, 0x0

    #@4
    .line 183
    .local v3, isCallWaiting:Z
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    :try_start_5
    iget-object v5, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@7
    array-length v5, v5

    #@8
    if-ge v2, v5, :cond_31

    #@a
    .line 184
    iget-object v5, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@c
    aget-object v0, v5, v2

    #@e
    .line 186
    .local v0, call:Lcom/android/internal/telephony/test/CallInfo;
    if-nez v0, :cond_16

    #@10
    if-gez v1, :cond_16

    #@12
    .line 187
    move v1, v2

    #@13
    .line 183
    :cond_13
    :goto_13
    add-int/lit8 v2, v2, 0x1

    #@15
    goto :goto_5

    #@16
    .line 188
    :cond_16
    if-eqz v0, :cond_2d

    #@18
    iget-object v5, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@1a
    sget-object v6, Lcom/android/internal/telephony/test/CallInfo$State;->INCOMING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@1c
    if-eq v5, v6, :cond_24

    #@1e
    iget-object v5, v0, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@20
    sget-object v6, Lcom/android/internal/telephony/test/CallInfo$State;->WAITING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@22
    if-ne v5, v6, :cond_2d

    #@24
    .line 192
    :cond_24
    const-string v5, "ModelInterpreter"

    #@26
    const-string v6, "triggerRing failed; phone already ringing"

    #@28
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 194
    monitor-exit p0

    #@2c
    .line 213
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :goto_2c
    return v4

    #@2d
    .line 195
    .restart local v0       #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_2d
    if-eqz v0, :cond_13

    #@2f
    .line 196
    const/4 v3, 0x1

    #@30
    goto :goto_13

    #@31
    .line 200
    .end local v0           #call:Lcom/android/internal/telephony/test/CallInfo;
    :cond_31
    if-gez v1, :cond_3f

    #@33
    .line 201
    const-string v5, "ModelInterpreter"

    #@35
    const-string v6, "triggerRing failed; all full"

    #@37
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 202
    monitor-exit p0

    #@3b
    goto :goto_2c

    #@3c
    .line 212
    :catchall_3c
    move-exception v4

    #@3d
    monitor-exit p0
    :try_end_3e
    .catchall {:try_start_5 .. :try_end_3e} :catchall_3c

    #@3e
    throw v4

    #@3f
    .line 205
    :cond_3f
    :try_start_3f
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@41
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    invoke-static {v5}, Lcom/android/internal/telephony/test/CallInfo;->createIncomingCall(Ljava/lang/String;)Lcom/android/internal/telephony/test/CallInfo;

    #@48
    move-result-object v5

    #@49
    aput-object v5, v4, v1

    #@4b
    .line 208
    if-eqz v3, :cond_55

    #@4d
    .line 209
    iget-object v4, p0, Lcom/android/internal/telephony/test/SimulatedGsmCallState;->calls:[Lcom/android/internal/telephony/test/CallInfo;

    #@4f
    aget-object v4, v4, v1

    #@51
    sget-object v5, Lcom/android/internal/telephony/test/CallInfo$State;->WAITING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@53
    iput-object v5, v4, Lcom/android/internal/telephony/test/CallInfo;->state:Lcom/android/internal/telephony/test/CallInfo$State;

    #@55
    .line 212
    :cond_55
    monitor-exit p0
    :try_end_56
    .catchall {:try_start_3f .. :try_end_56} :catchall_3c

    #@56
    .line 213
    const/4 v4, 0x1

    #@57
    goto :goto_2c
.end method
