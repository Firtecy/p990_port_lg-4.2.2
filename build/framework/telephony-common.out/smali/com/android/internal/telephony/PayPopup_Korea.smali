.class public Lcom/android/internal/telephony/PayPopup_Korea;
.super Landroid/os/Handler;
.source "PayPopup_Korea.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/PayPopup_Korea$2;,
        Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;
    }
.end annotation


# static fields
.field public static final ALL_DATA_BLOCKED_SKT:I = 0x3

.field public static final CURRENT_MCC:Ljava/lang/String; = "current_mcc"

.field public static final DATA_DISABLE_WIFI_TO_3G_TRANSITION:I = 0xa

.field public static final DATA_ENABLE_WIFI_TO_3G_TRANSITION:I = 0x9

.field public static final DATA_NETWORK_USER_PAYPOPUP_RESPONSE:Ljava/lang/String; = "data_network_user_paypopup_response"

.field public static final DATA_NETWORK_USER_PAYPOPUP_TRANSITION_FROM_WIFI_TO_MOBILE:Ljava/lang/String; = "data_network_user_paypopup_transition_from_wifi_to_mobile"

.field public static final DATA_NETWORK_WAIT_FOR_PAYPOPUP_RESPONSE:Ljava/lang/String; = "data_network_wait_for_paypopup_response"

.field public static final DOMESTIC_DIALOG_LGT:I = 0x67

.field public static final DOMESTIC_DIALOG_SKT:I = 0x64

.field public static final DOMESTIC_ROAMING_DIALOG_KT:I = 0x66

.field private static final EVENT_DELAYED_TOAST_KT:I = 0x258

.field private static final EVENT_RESTART_FOR_FAILSETUP_BOOT:I = 0xc9

.field private static final EVENT_START_CHARGING_POPUP:I = 0xc8

.field private static final EVENT_START_CHARGING_POPUP_ROAM:I = 0xca

.field private static final LOG_TAG:Ljava/lang/String; = "[LGE_DATA][PayPopUp_ko] "

.field public static final MOBILE_DATA_ALLOWED_LGT:I = 0x7

.field public static final MOBILE_DATA_ALLOWED_SKT:I = 0x1

.field public static final MOBILE_DATA_BLOCKED_LGT:I = 0x8

.field public static final MOBILE_DATA_BLOCKED_SKT:I = 0x2

.field public static final MOBILE_DATA_SET_BLOCKED_MMS_SKT:I = 0x6

.field private static final NETWORKOPEN_DELAY_TIMER:I = 0x3e8

.field public static final OLD_MCC:Ljava/lang/String; = "intent_old_mcc"

.field private static final PAY_POPUP_IN_CASE_OF_BOOTING:Ljava/lang/String; = "booting"

.field private static final PAY_POPUP_IN_CASE_OF_NO_DISPLAY_POPUP:Ljava/lang/String; = "no_display_popup"

.field private static final PAY_POPUP_IN_CASE_OF_OTHERS:Ljava/lang/String; = "others"

.field private static final PAY_POPUP_IN_CASE_OF_WIFI_OFF:Ljava/lang/String; = "Wifi_off"

.field private static final PAY_POPUP_NOT_ALLOWED:I = 0x12e

.field private static final PAY_POPUP_OKAY:I = 0x12f

.field private static final PAY_POPUP_WAITING_FOR_USER_RESPONSE:I = 0x12d

.field public static final PREFERRED_DATA_NETWORK_MODE:Ljava/lang/String; = "preferred_data_network_mode"

.field private static final RETRY_DOMESTIC_DIALOG_KT:I = 0x191

.field private static final RETRY_DOMESTIC_DIALOG_LGU:I = 0x192

.field private static final RETRY_DOMESTIC_DIALOG_SKT:I = 0x190

.field private static final RETRY_POPUP_SHOW_DELAY:I = 0x1f4

.field public static final ROAMING_DIALOG_LGT:I = 0x68

.field public static final ROAMING_DIALOG_SKT:I = 0x65

.field public static final ROAM_MOBILE_DATA_ALLOWED_SKT:I = 0x4

.field public static final ROAM_MOBILE_DATA_BLOCKED_SKT:I = 0x5

.field public static UiccIsEmpty:I

.field public static airplane_mode:I


# instance fields
.field featureset:Ljava/lang/String;

.field private global_new_mcc:Ljava/lang/String;

.field private global_old_mcc:Ljava/lang/String;

.field private intent_reset:Z

.field private mActiveDomesticPopup:Z

.field private mActiveRoamingPopup:Z

.field private mConnMgr:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

.field private mDct:Lcom/android/internal/telephony/DataConnectionTracker;

.field mIntentReceiver:Landroid/content/BroadcastReceiver;

.field public mIsok_bypass:Z

.field private mMobileEnabled:Z

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mPhoneMgr:Lcom/android/internal/telephony/ITelephony;

.field private mResolver:Landroid/content/ContentResolver;

.field mStatus:Z

.field mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

.field private mbooting_phone:Z

.field public retryStartActivityForPopup:I

.field private roam_to_domestic_popup_need:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 117
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/android/internal/telephony/PayPopup_Korea;->UiccIsEmpty:I

    #@3
    .line 120
    const/4 v0, -0x1

    #@4
    sput v0, Lcom/android/internal/telephony/PayPopup_Korea;->airplane_mode:I

    #@6
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/DataConnectionTracker;Lcom/android/internal/telephony/PhoneBase;)V
    .registers 7
    .parameter "dct"
    .parameter "p"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 533
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@4
    .line 111
    const-string v1, "000"

    #@6
    iput-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->global_new_mcc:Ljava/lang/String;

    #@8
    .line 112
    const-string v1, "000"

    #@a
    iput-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->global_old_mcc:Ljava/lang/String;

    #@c
    .line 113
    iput-boolean v2, p0, Lcom/android/internal/telephony/PayPopup_Korea;->roam_to_domestic_popup_need:Z

    #@e
    .line 124
    const/4 v1, 0x5

    #@f
    iput v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->retryStartActivityForPopup:I

    #@11
    .line 205
    const/4 v1, 0x1

    #@12
    iput-boolean v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@14
    .line 207
    iput-boolean v2, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mIsok_bypass:Z

    #@16
    .line 209
    iput-boolean v2, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mActiveDomesticPopup:Z

    #@18
    .line 210
    iput-boolean v2, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mActiveRoamingPopup:Z

    #@1a
    .line 213
    iput-boolean v2, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mStatus:Z

    #@1c
    .line 214
    iput-boolean v2, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mMobileEnabled:Z

    #@1e
    .line 218
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiServiceExtIface()Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@21
    move-result-object v1

    #@22
    iput-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@24
    .line 222
    iput-boolean v2, p0, Lcom/android/internal/telephony/PayPopup_Korea;->intent_reset:Z

    #@26
    .line 225
    new-instance v1, Lcom/android/internal/telephony/PayPopup_Korea$1;

    #@28
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/PayPopup_Korea$1;-><init>(Lcom/android/internal/telephony/PayPopup_Korea;)V

    #@2b
    iput-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@2d
    .line 534
    const-string v1, "[LGE_DATA][PayPopUp_ko] "

    #@2f
    const-string v2, "PayPopup_Korea() has created"

    #@31
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 536
    const-string v1, "ro.afwdata.LGfeatureset"

    #@36
    const-string v2, "none"

    #@38
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    iput-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@3e
    .line 537
    iput-object p1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@40
    .line 538
    iput-object p2, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@42
    .line 539
    const-string v1, "phone"

    #@44
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@47
    move-result-object v1

    #@48
    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@4b
    move-result-object v1

    #@4c
    iput-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhoneMgr:Lcom/android/internal/telephony/ITelephony;

    #@4e
    .line 540
    iget-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@50
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@53
    move-result-object v1

    #@54
    const-string v2, "connectivity"

    #@56
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@59
    move-result-object v1

    #@5a
    check-cast v1, Landroid/net/ConnectivityManager;

    #@5c
    iput-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@5e
    .line 542
    iget-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@60
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@63
    move-result-object v1

    #@64
    iget-object v2, p2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@66
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@69
    move-result-object v2

    #@6a
    invoke-static {v1, v2}, Lcom/android/internal/telephony/DataConnectionManager;->getInstance(Landroid/content/Context;Lcom/android/internal/telephony/LGfeature;)Lcom/android/internal/telephony/DataConnectionManager;

    #@6d
    move-result-object v1

    #@6e
    iput-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@70
    .line 544
    iget-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@72
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@75
    move-result-object v1

    #@76
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@79
    move-result-object v1

    #@7a
    iput-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mResolver:Landroid/content/ContentResolver;

    #@7c
    .line 546
    new-instance v0, Landroid/content/IntentFilter;

    #@7e
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@81
    .line 549
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "com.lge.DataEnabledSettingBootableSKT"

    #@83
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@86
    .line 550
    const-string v1, "com.lge.DataNetworkModePayPopupKT"

    #@88
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@8b
    .line 551
    const-string v1, "com.lge.DataNetworkModePayPopupLGT"

    #@8d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@90
    .line 555
    const-string v1, "android.net.conn.STARTING_IN_DATA_SETTING_DISABLE"

    #@92
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@95
    .line 556
    const-string v1, "android.net.conn.STARTING_IN_ROAM_SETTING_DISABLE"

    #@97
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@9a
    .line 557
    const-string v1, "android.net.conn.STARTING_IN_DATA_SETTING_DISABLE_3GONLY"

    #@9c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@9f
    .line 558
    const-string v1, "android.net.conn.DATA_DATA_BLOCK_IN_MMS"

    #@a1
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a4
    .line 562
    const-string v1, "lge.intent.action.LGE_WIFI_3G_TRANSITION"

    #@a6
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a9
    .line 566
    iget-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@ab
    const-string v2, "LGTBASE"

    #@ad
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@b0
    move-result v1

    #@b1
    if-nez v1, :cond_bd

    #@b3
    iget-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@b5
    const-string v2, "SKTBASE"

    #@b7
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@ba
    move-result v1

    #@bb
    if-eqz v1, :cond_c7

    #@bd
    .line 569
    :cond_bd
    const-string v1, "com.lge.intent.action.LGE_CAMPED_MCC_CHANGE"

    #@bf
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c2
    .line 571
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    #@c4
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c7
    .line 577
    :cond_c7
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@c9
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@cc
    .line 578
    const-string v1, "android.intent.action.OTA_USIM_REFRESH_TO_RESET"

    #@ce
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d1
    .line 581
    iget-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@d3
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@d6
    move-result-object v1

    #@d7
    iget-object v2, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@d9
    const/4 v3, 0x0

    #@da
    invoke-virtual {v1, v2, v0, v3, p0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@dd
    .line 583
    return-void
.end method

.method private PayPopupforFeature(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;Ljava/lang/String;Ljava/lang/String;)I
    .registers 21
    .parameter "funcName"
    .parameter "reason"
    .parameter "apntype"

    #@0
    .prologue
    .line 902
    const/4 v7, 0x0

    #@1
    .line 904
    .local v7, in_prog_bypass:Z
    move-object/from16 v0, p0

    #@3
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@5
    iget-object v12, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@c
    move-result-object v12

    #@d
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_KR:Z

    #@f
    if-eqz v12, :cond_10d

    #@11
    move-object/from16 v0, p0

    #@13
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@15
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@18
    move-result-object v12

    #@19
    invoke-virtual {v12}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@1c
    move-result v12

    #@1d
    if-nez v12, :cond_10d

    #@1f
    .line 905
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@21
    new-instance v13, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v14, "LGP_DATA_IMS_KR TYPE type :: "

    #@28
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v13

    #@2c
    move-object/from16 v0, p3

    #@2e
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v13

    #@32
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v13

    #@36
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 906
    move-object/from16 v0, p0

    #@3b
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3d
    iget-object v12, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@3f
    move-object/from16 v0, p0

    #@41
    iget-object v13, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@43
    const/4 v14, 0x5

    #@44
    invoke-virtual {v13, v14}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@47
    move-result-object v13

    #@48
    invoke-virtual {v12, v13}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4b
    move-result-object v6

    #@4c
    check-cast v6, Lcom/android/internal/telephony/ApnContext;

    #@4e
    .line 907
    .local v6, ims_type:Lcom/android/internal/telephony/ApnContext;
    if-eqz p3, :cond_7b

    #@50
    const-string v12, "ims"

    #@52
    move-object/from16 v0, p3

    #@54
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v12

    #@58
    if-eqz v12, :cond_7b

    #@5a
    if-eqz v6, :cond_7b

    #@5c
    invoke-virtual {v6}, Lcom/android/internal/telephony/ApnContext;->isEnabled()Z

    #@5f
    move-result v12

    #@60
    if-eqz v12, :cond_7b

    #@62
    .line 909
    const/4 v7, 0x1

    #@63
    .line 910
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@65
    new-instance v13, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v14, "MPDN (IMS) TYPE :: "

    #@6c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v13

    #@70
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@73
    move-result-object v13

    #@74
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v13

    #@78
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 913
    :cond_7b
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->PayPopupforLGT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@7d
    move-object/from16 v0, p1

    #@7f
    if-ne v0, v12, :cond_c4

    #@81
    .line 914
    move-object/from16 v0, p0

    #@83
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@85
    iget-object v12, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@87
    move-object/from16 v0, p0

    #@89
    iget-object v13, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@8b
    const/16 v14, 0xc

    #@8d
    invoke-virtual {v13, v14}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@90
    move-result-object v13

    #@91
    invoke-virtual {v12, v13}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@94
    move-result-object v10

    #@95
    check-cast v10, Lcom/android/internal/telephony/ApnContext;

    #@97
    .line 915
    .local v10, tethering_type:Lcom/android/internal/telephony/ApnContext;
    if-eqz p3, :cond_c4

    #@99
    const-string v12, "tethering"

    #@9b
    move-object/from16 v0, p3

    #@9d
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a0
    move-result v12

    #@a1
    if-eqz v12, :cond_c4

    #@a3
    if-eqz v10, :cond_c4

    #@a5
    invoke-virtual {v10}, Lcom/android/internal/telephony/ApnContext;->isEnabled()Z

    #@a8
    move-result v12

    #@a9
    if-eqz v12, :cond_c4

    #@ab
    .line 917
    const/4 v7, 0x1

    #@ac
    .line 918
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@ae
    new-instance v13, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v14, "MPDN U+ (TETHERING) TYPE :: "

    #@b5
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v13

    #@b9
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v13

    #@bd
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v13

    #@c1
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c4
    .line 923
    .end local v10           #tethering_type:Lcom/android/internal/telephony/ApnContext;
    :cond_c4
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->PayPopupforLGT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@c6
    move-object/from16 v0, p1

    #@c8
    if-ne v0, v12, :cond_10d

    #@ca
    .line 924
    move-object/from16 v0, p0

    #@cc
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@ce
    iget-object v12, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@d0
    move-object/from16 v0, p0

    #@d2
    iget-object v13, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@d4
    const/16 v14, 0x10

    #@d6
    invoke-virtual {v13, v14}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@d9
    move-result-object v13

    #@da
    invoke-virtual {v12, v13}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@dd
    move-result-object v4

    #@de
    check-cast v4, Lcom/android/internal/telephony/ApnContext;

    #@e0
    .line 925
    .local v4, emergency_type:Lcom/android/internal/telephony/ApnContext;
    if-eqz p3, :cond_10d

    #@e2
    const-string v12, "emergency"

    #@e4
    move-object/from16 v0, p3

    #@e6
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e9
    move-result v12

    #@ea
    if-eqz v12, :cond_10d

    #@ec
    if-eqz v4, :cond_10d

    #@ee
    invoke-virtual {v4}, Lcom/android/internal/telephony/ApnContext;->isEnabled()Z

    #@f1
    move-result v12

    #@f2
    if-eqz v12, :cond_10d

    #@f4
    .line 927
    const/4 v7, 0x1

    #@f5
    .line 928
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@f7
    new-instance v13, Ljava/lang/StringBuilder;

    #@f9
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@fc
    const-string v14, "MPDN U+ (EMERGENCY) TYPE :: "

    #@fe
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v13

    #@102
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@105
    move-result-object v13

    #@106
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v13

    #@10a
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10d
    .line 936
    .end local v4           #emergency_type:Lcom/android/internal/telephony/ApnContext;
    .end local v6           #ims_type:Lcom/android/internal/telephony/ApnContext;
    :cond_10d
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$2;->$SwitchMap$com$android$internal$telephony$PayPopup_Korea$PayPopupFunction:[I

    #@10f
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->ordinal()I

    #@112
    move-result v13

    #@113
    aget v12, v12, v13

    #@115
    packed-switch v12, :pswitch_data_a00

    #@118
    .line 1327
    const/4 v12, 0x0

    #@119
    :goto_119
    return v12

    #@11a
    .line 939
    :pswitch_11a
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@11c
    const-string v13, "PayPopupforSKT()"

    #@11e
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@121
    .line 940
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@123
    new-instance v13, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v14, "in_prog_bypass :: "

    #@12a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v13

    #@12e
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@131
    move-result-object v13

    #@132
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@135
    move-result-object v13

    #@136
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@139
    .line 942
    move-object/from16 v0, p0

    #@13b
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@13d
    iget-object v12, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@13f
    move-object/from16 v0, p0

    #@141
    iget-object v13, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@143
    const/4 v14, 0x1

    #@144
    invoke-virtual {v13, v14}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@147
    move-result-object v13

    #@148
    invoke-virtual {v12, v13}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14b
    move-result-object v2

    #@14c
    check-cast v2, Lcom/android/internal/telephony/ApnContext;

    #@14e
    .line 944
    .local v2, apnContext:Lcom/android/internal/telephony/ApnContext;
    if-eqz v2, :cond_183

    #@150
    .line 946
    move-object/from16 v0, p0

    #@152
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@154
    sget-object v13, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@156
    const-string v14, ""

    #@158
    const/4 v15, 0x0

    #@159
    invoke-virtual {v12, v13, v14, v15}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@15c
    move-result v12

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget-object v13, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@161
    const/4 v13, 0x2

    #@162
    if-ne v12, v13, :cond_183

    #@164
    invoke-virtual {v2}, Lcom/android/internal/telephony/ApnContext;->isEnabled()Z

    #@167
    move-result v12

    #@168
    if-eqz v12, :cond_183

    #@16a
    .line 948
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@16c
    new-instance v13, Ljava/lang/StringBuilder;

    #@16e
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@171
    const-string v14, "SINGLE SKT (MMS) TYPE  :: "

    #@173
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v13

    #@177
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v13

    #@17b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17e
    move-result-object v13

    #@17f
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@182
    .line 949
    const/4 v7, 0x1

    #@183
    .line 953
    :cond_183
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@185
    new-instance v13, Ljava/lang/StringBuilder;

    #@187
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@18a
    const-string v14, "in_prog_bypass :: "

    #@18c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v13

    #@190
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@193
    move-result-object v13

    #@194
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@197
    move-result-object v13

    #@198
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19b
    .line 955
    if-nez v7, :cond_261

    #@19d
    .line 956
    move-object/from16 v0, p0

    #@19f
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@1a1
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@1a4
    move-result-object v12

    #@1a5
    invoke-virtual {v12}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@1a8
    move-result v12

    #@1a9
    if-eqz v12, :cond_265

    #@1ab
    .line 957
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@1ad
    new-instance v13, Ljava/lang/StringBuilder;

    #@1af
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1b2
    const-string v14, "<PayPopupforSKT()> roaming = "

    #@1b4
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v13

    #@1b8
    move-object/from16 v0, p0

    #@1ba
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@1bc
    invoke-interface {v14}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@1bf
    move-result-object v14

    #@1c0
    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@1c3
    move-result v14

    #@1c4
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v13

    #@1c8
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cb
    move-result-object v13

    #@1cc
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1cf
    .line 958
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@1d1
    new-instance v13, Ljava/lang/StringBuilder;

    #@1d3
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1d6
    const-string v14, " mbooting_phone = "

    #@1d8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v13

    #@1dc
    move-object/from16 v0, p0

    #@1de
    iget-boolean v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@1e0
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e3
    move-result-object v13

    #@1e4
    const-string v14, ", airplane_mode = "

    #@1e6
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v13

    #@1ea
    sget v14, Lcom/android/internal/telephony/PayPopup_Korea;->airplane_mode:I

    #@1ec
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ef
    move-result-object v13

    #@1f0
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f3
    move-result-object v13

    #@1f4
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f7
    .line 960
    move-object/from16 v0, p0

    #@1f9
    iget-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@1fb
    const/4 v13, 0x1

    #@1fc
    if-eq v12, v13, :cond_202

    #@1fe
    sget v12, Lcom/android/internal/telephony/PayPopup_Korea;->airplane_mode:I

    #@200
    if-nez v12, :cond_221

    #@202
    .line 961
    :cond_202
    const-string v12, "net.Is_phone_booted"

    #@204
    const-string v13, "false"

    #@206
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@209
    .line 962
    const/4 v12, -0x1

    #@20a
    sput v12, Lcom/android/internal/telephony/PayPopup_Korea;->airplane_mode:I

    #@20c
    .line 963
    move-object/from16 v0, p0

    #@20e
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@210
    const/4 v13, 0x0

    #@211
    invoke-virtual {v12, v13}, Lcom/android/internal/telephony/DataConnectionTracker;->DataOnRoamingEnabled_OnlySel(Z)V

    #@214
    .line 964
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showDialog:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@216
    const/16 v13, 0x65

    #@218
    move-object/from16 v0, p0

    #@21a
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@21d
    .line 965
    const/16 v12, 0x12d

    #@21f
    goto/16 :goto_119

    #@221
    .line 971
    :cond_221
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@223
    new-instance v13, Ljava/lang/StringBuilder;

    #@225
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@228
    const-string v14, "<PayPopupforSKT()>\tallows as roam toast : : reason =  "

    #@22a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v13

    #@22e
    move-object/from16 v0, p2

    #@230
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@233
    move-result-object v13

    #@234
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@237
    move-result-object v13

    #@238
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23b
    .line 974
    const-string v12, "roamingOn"

    #@23d
    move-object/from16 v0, p2

    #@23f
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@242
    move-result v12

    #@243
    if-nez v12, :cond_259

    #@245
    const-string v12, "apnChanged"

    #@247
    move-object/from16 v0, p2

    #@249
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24c
    move-result v12

    #@24d
    if-nez v12, :cond_259

    #@24f
    const-string v12, "dataEnabled"

    #@251
    move-object/from16 v0, p2

    #@253
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@256
    move-result v12

    #@257
    if-eqz v12, :cond_261

    #@259
    .line 977
    :cond_259
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@25b
    const/4 v13, 0x4

    #@25c
    move-object/from16 v0, p0

    #@25e
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@261
    .line 1056
    :cond_261
    :goto_261
    const/16 v12, 0x12f

    #@263
    goto/16 :goto_119

    #@265
    .line 981
    :cond_265
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@267
    new-instance v13, Ljava/lang/StringBuilder;

    #@269
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@26c
    const-string v14, "<PayPopupforSKT()>\tmbooting_phone = "

    #@26e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@271
    move-result-object v13

    #@272
    move-object/from16 v0, p0

    #@274
    iget-boolean v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@276
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@279
    move-result-object v13

    #@27a
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27d
    move-result-object v13

    #@27e
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@281
    .line 984
    move-object/from16 v0, p0

    #@283
    iget-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@285
    const/4 v13, 0x1

    #@286
    if-ne v12, v13, :cond_36d

    #@288
    .line 985
    const-string v12, "net.Is_phone_booted"

    #@28a
    const-string v13, "false"

    #@28c
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@28f
    .line 987
    move-object/from16 v0, p0

    #@291
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@293
    sget-object v13, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@295
    const-string v14, ""

    #@297
    const/4 v15, 0x1

    #@298
    invoke-virtual {v12, v13, v14, v15}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@29b
    move-result v12

    #@29c
    move-object/from16 v0, p0

    #@29e
    iget-object v13, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@2a0
    const/4 v13, 0x3

    #@2a1
    if-ne v12, v13, :cond_314

    #@2a3
    .line 990
    const-string v12, "sys.factory.qem"

    #@2a5
    const/4 v13, 0x0

    #@2a6
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@2a9
    move-result v5

    #@2aa
    .line 991
    .local v5, factory_qem:Z
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@2ac
    new-instance v13, Ljava/lang/StringBuilder;

    #@2ae
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@2b1
    const-string v14, "[LGE_DATA] factory_qem["

    #@2b3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b6
    move-result-object v13

    #@2b7
    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2ba
    move-result-object v13

    #@2bb
    const-string v14, "]"

    #@2bd
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c0
    move-result-object v13

    #@2c1
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c4
    move-result-object v13

    #@2c5
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c8
    .line 992
    const/4 v12, 0x1

    #@2c9
    if-ne v5, v12, :cond_2e7

    #@2cb
    move-object/from16 v0, p0

    #@2cd
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2cf
    iget-object v12, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2d1
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2d3
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2d6
    move-result-object v12

    #@2d7
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BLOCK_PAYPOPUP_BUT_TRYSETUP:Z

    #@2d9
    const/4 v13, 0x1

    #@2da
    if-ne v12, v13, :cond_2e7

    #@2dc
    .line 993
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@2de
    const-string v13, "[LGE_DATA] QEM mode, blocking data call and don\'t show charging popup"

    #@2e0
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e3
    .line 994
    const/16 v12, 0x12f

    #@2e5
    goto/16 :goto_119

    #@2e7
    .line 997
    :cond_2e7
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@2e9
    const-string v13, "<PayPopupforSKT()> DCM_MOBILE_NETWORK_IS_NEED_POPUP : mUserDataEnabled = false / MOBILE_DATA = false."

    #@2eb
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2ee
    .line 999
    move-object/from16 v0, p0

    #@2f0
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2f2
    const/4 v13, 0x0

    #@2f3
    iput-boolean v13, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@2f5
    .line 1000
    move-object/from16 v0, p0

    #@2f7
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@2f9
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@2fc
    move-result-object v12

    #@2fd
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@300
    move-result-object v12

    #@301
    const-string v13, "mobile_data"

    #@303
    const/4 v14, 0x0

    #@304
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@307
    .line 1002
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showDialog:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@309
    const/16 v13, 0x64

    #@30b
    move-object/from16 v0, p0

    #@30d
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@310
    .line 1004
    const/16 v12, 0x12d

    #@312
    goto/16 :goto_119

    #@314
    .line 1007
    .end local v5           #factory_qem:Z
    :cond_314
    move-object/from16 v0, p0

    #@316
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@318
    move-object/from16 v0, p0

    #@31a
    iget-object v13, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@31c
    invoke-virtual {v13}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@31f
    move-result v13

    #@320
    invoke-virtual {v12, v13}, Lcom/android/internal/telephony/DataConnectionTracker;->changePreferrredNetworkMode(Z)V

    #@323
    .line 1008
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@325
    new-instance v13, Ljava/lang/StringBuilder;

    #@327
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@32a
    const-string v14, "<PayPopupforSKT()> changePreferrredNetworkMode is called : mbooting_phone is TRUE / mConnMgr.getMobileDataEnabled() = "

    #@32c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32f
    move-result-object v13

    #@330
    move-object/from16 v0, p0

    #@332
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@334
    invoke-virtual {v14}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@337
    move-result v14

    #@338
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@33b
    move-result-object v13

    #@33c
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33f
    move-result-object v13

    #@340
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@343
    .line 1009
    move-object/from16 v0, p0

    #@345
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@347
    sget-object v13, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@349
    const-string v14, ""

    #@34b
    const/4 v15, 0x1

    #@34c
    invoke-virtual {v12, v13, v14, v15}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@34f
    move-result v12

    #@350
    move-object/from16 v0, p0

    #@352
    iget-object v13, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@354
    const/4 v13, 0x1

    #@355
    if-ne v12, v13, :cond_361

    #@357
    .line 1010
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@359
    const/4 v13, 0x1

    #@35a
    move-object/from16 v0, p0

    #@35c
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@35f
    goto/16 :goto_261

    #@361
    .line 1012
    :cond_361
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@363
    const/4 v13, 0x3

    #@364
    move-object/from16 v0, p0

    #@366
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@369
    .line 1014
    const/16 v12, 0x12e

    #@36b
    goto/16 :goto_119

    #@36d
    .line 1018
    :cond_36d
    move-object/from16 v0, p0

    #@36f
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@371
    sget-object v13, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@373
    const-string v14, ""

    #@375
    const/4 v15, 0x0

    #@376
    invoke-virtual {v12, v13, v14, v15}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@379
    move-result v12

    #@37a
    move-object/from16 v0, p0

    #@37c
    iget-object v13, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@37e
    const/4 v13, 0x1

    #@37f
    if-ne v12, v13, :cond_438

    #@381
    .line 1019
    const/4 v1, 0x0

    #@382
    .line 1022
    .local v1, IsMMS:Z
    move-object/from16 v0, p0

    #@384
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@386
    invoke-virtual {v12}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredNetworkMode()I

    #@389
    move-result v12

    #@38a
    const/4 v13, 0x3

    #@38b
    if-eq v12, v13, :cond_397

    #@38d
    move-object/from16 v0, p0

    #@38f
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@391
    invoke-virtual {v12}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredNetworkMode()I

    #@394
    move-result v12

    #@395
    if-nez v12, :cond_3c6

    #@397
    .line 1025
    :cond_397
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@399
    new-instance v13, Ljava/lang/StringBuilder;

    #@39b
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@39e
    const-string v14, "<PayPopupforSKT()> changePreferrredNetworkMode is called : mbooting_phone is FALSE / mConnMgr.getMobileDataEnabled() = "

    #@3a0
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a3
    move-result-object v13

    #@3a4
    move-object/from16 v0, p0

    #@3a6
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@3a8
    invoke-virtual {v14}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@3ab
    move-result v14

    #@3ac
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3af
    move-result-object v13

    #@3b0
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b3
    move-result-object v13

    #@3b4
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b7
    .line 1026
    move-object/from16 v0, p0

    #@3b9
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3bb
    move-object/from16 v0, p0

    #@3bd
    iget-object v13, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@3bf
    invoke-virtual {v13}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@3c2
    move-result v13

    #@3c3
    invoke-virtual {v12, v13}, Lcom/android/internal/telephony/DataConnectionTracker;->changePreferrredNetworkMode(Z)V

    #@3c6
    .line 1030
    :cond_3c6
    move-object/from16 v0, p0

    #@3c8
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3ca
    iget-object v12, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@3cc
    move-object/from16 v0, p0

    #@3ce
    iget-object v13, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3d0
    const/4 v14, 0x1

    #@3d1
    invoke-virtual {v13, v14}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@3d4
    move-result-object v13

    #@3d5
    invoke-virtual {v12, v13}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d8
    move-result-object v2

    #@3d9
    .end local v2           #apnContext:Lcom/android/internal/telephony/ApnContext;
    check-cast v2, Lcom/android/internal/telephony/ApnContext;

    #@3db
    .line 1032
    .restart local v2       #apnContext:Lcom/android/internal/telephony/ApnContext;
    if-eqz v2, :cond_3e4

    #@3dd
    .line 1033
    invoke-virtual {v2}, Lcom/android/internal/telephony/ApnContext;->isEnabled()Z

    #@3e0
    move-result v12

    #@3e1
    if-eqz v12, :cond_3e4

    #@3e3
    .line 1034
    const/4 v1, 0x1

    #@3e4
    .line 1038
    :cond_3e4
    if-nez v1, :cond_261

    #@3e6
    .line 1039
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@3e8
    new-instance v13, Ljava/lang/StringBuilder;

    #@3ea
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@3ed
    const-string v14, "<PayPopupforSKT()> reason = "

    #@3ef
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f2
    move-result-object v13

    #@3f3
    move-object/from16 v0, p2

    #@3f5
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f8
    move-result-object v13

    #@3f9
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3fc
    move-result-object v13

    #@3fd
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@400
    .line 1041
    const-string v12, "dataEnabled"

    #@402
    move-object/from16 v0, p2

    #@404
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@407
    move-result v12

    #@408
    if-nez v12, :cond_414

    #@40a
    const-string v12, "apnChanged"

    #@40c
    move-object/from16 v0, p2

    #@40e
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@411
    move-result v12

    #@412
    if-eqz v12, :cond_261

    #@414
    .line 1043
    :cond_414
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@416
    new-instance v13, Ljava/lang/StringBuilder;

    #@418
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@41b
    const-string v14, "<PayPopupforSKT()> show_allow_toast_pupop  :: "

    #@41d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@420
    move-result-object v13

    #@421
    move-object/from16 v0, p2

    #@423
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@426
    move-result-object v13

    #@427
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42a
    move-result-object v13

    #@42b
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42e
    .line 1045
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@430
    const/4 v13, 0x1

    #@431
    move-object/from16 v0, p0

    #@433
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@436
    goto/16 :goto_261

    #@438
    .line 1050
    .end local v1           #IsMMS:Z
    :cond_438
    const/16 v12, 0x12e

    #@43a
    goto/16 :goto_119

    #@43c
    .line 1062
    .end local v2           #apnContext:Lcom/android/internal/telephony/ApnContext;
    :pswitch_43c
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@43e
    const-string v13, "PayPopupforKT()"

    #@440
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@443
    .line 1063
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@445
    new-instance v13, Ljava/lang/StringBuilder;

    #@447
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@44a
    const-string v14, "in_prog_bypass :: "

    #@44c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44f
    move-result-object v13

    #@450
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@453
    move-result-object v13

    #@454
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@457
    move-result-object v13

    #@458
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45b
    .line 1065
    if-nez v7, :cond_64b

    #@45d
    .line 1067
    move-object/from16 v0, p0

    #@45f
    iget-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@461
    const/4 v13, 0x1

    #@462
    if-ne v12, v13, :cond_488

    #@464
    .line 1069
    move-object/from16 v0, p0

    #@466
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@468
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@46b
    move-result-object v12

    #@46c
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@46f
    move-result-object v12

    #@470
    const-string v13, "data_network_wait_for_paypopup_response"

    #@472
    const/4 v14, 0x0

    #@473
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@476
    .line 1070
    move-object/from16 v0, p0

    #@478
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@47a
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@47d
    move-result-object v12

    #@47e
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@481
    move-result-object v12

    #@482
    const-string v13, "data_network_user_paypopup_response"

    #@484
    const/4 v14, 0x0

    #@485
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@488
    .line 1073
    :cond_488
    move-object/from16 v0, p0

    #@48a
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@48c
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@48f
    move-result-object v12

    #@490
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@493
    move-result-object v12

    #@494
    const-string v13, "data_network_wait_for_paypopup_response"

    #@496
    const/4 v14, 0x0

    #@497
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@49a
    move-result v8

    #@49b
    .line 1074
    .local v8, is_waiting:I
    move-object/from16 v0, p0

    #@49d
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@49f
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@4a2
    move-result-object v12

    #@4a3
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4a6
    move-result-object v12

    #@4a7
    const-string v13, "data_network_user_paypopup_response"

    #@4a9
    const/4 v14, 0x0

    #@4aa
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@4ad
    move-result v11

    #@4ae
    .line 1075
    .local v11, user_response:I
    move-object/from16 v0, p0

    #@4b0
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@4b2
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@4b5
    move-result-object v12

    #@4b6
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4b9
    move-result-object v12

    #@4ba
    const-string v13, "preferred_data_network_mode"

    #@4bc
    const/4 v14, 0x1

    #@4bd
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@4c0
    move-result v3

    #@4c1
    .line 1077
    .local v3, ask_at_boot:I
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@4c3
    new-instance v13, Ljava/lang/StringBuilder;

    #@4c5
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@4c8
    const-string v14, "trySetupData with reason = "

    #@4ca
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4cd
    move-result-object v13

    #@4ce
    move-object/from16 v0, p2

    #@4d0
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d3
    move-result-object v13

    #@4d4
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d7
    move-result-object v13

    #@4d8
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4db
    .line 1078
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@4dd
    new-instance v13, Ljava/lang/StringBuilder;

    #@4df
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@4e2
    const-string v14, "trySetupData with is_waiting = "

    #@4e4
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e7
    move-result-object v13

    #@4e8
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4eb
    move-result-object v13

    #@4ec
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4ef
    move-result-object v13

    #@4f0
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f3
    .line 1079
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@4f5
    new-instance v13, Ljava/lang/StringBuilder;

    #@4f7
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@4fa
    const-string v14, "trySetupData with user_choice = "

    #@4fc
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ff
    move-result-object v13

    #@500
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@503
    move-result-object v13

    #@504
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@507
    move-result-object v13

    #@508
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50b
    .line 1080
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@50d
    new-instance v13, Ljava/lang/StringBuilder;

    #@50f
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@512
    const-string v14, "trySetupData with ask at boot ="

    #@514
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@517
    move-result-object v13

    #@518
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51b
    move-result-object v13

    #@51c
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51f
    move-result-object v13

    #@520
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@523
    .line 1082
    const/4 v12, 0x1

    #@524
    if-ne v8, v12, :cond_565

    #@526
    .line 1083
    if-nez v11, :cond_533

    #@528
    .line 1084
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@52a
    const-string v13, "PayPopupforKT is waiting for user response"

    #@52c
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52f
    .line 1086
    const/16 v12, 0x12d

    #@531
    goto/16 :goto_119

    #@533
    .line 1087
    :cond_533
    const/4 v12, 0x2

    #@534
    if-ne v11, v12, :cond_565

    #@536
    .line 1089
    move-object/from16 v0, p0

    #@538
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@53a
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@53d
    move-result-object v12

    #@53e
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@541
    move-result-object v12

    #@542
    const-string v13, "data_network_wait_for_paypopup_response"

    #@544
    const/4 v14, 0x0

    #@545
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@548
    .line 1090
    move-object/from16 v0, p0

    #@54a
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@54c
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@54f
    move-result-object v12

    #@550
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@553
    move-result-object v12

    #@554
    const-string v13, "data_network_user_paypopup_response"

    #@556
    const/4 v14, 0x0

    #@557
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@55a
    .line 1092
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@55c
    const-string v13, "PayPopupforKT is accpeted by user"

    #@55e
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@561
    .line 1094
    const/16 v12, 0x12f

    #@563
    goto/16 :goto_119

    #@565
    .line 1103
    :cond_565
    move-object/from16 v0, p0

    #@567
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@569
    invoke-virtual {v12}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@56c
    move-result v12

    #@56d
    const/4 v13, 0x1

    #@56e
    if-ne v12, v13, :cond_636

    #@570
    const/4 v12, 0x1

    #@571
    if-ne v3, v12, :cond_636

    #@573
    const-string v12, "booting"

    #@575
    move-object/from16 v0, p2

    #@577
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57a
    move-result v12

    #@57b
    if-nez v12, :cond_587

    #@57d
    const-string v12, "Wifi_off"

    #@57f
    move-object/from16 v0, p2

    #@581
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@584
    move-result v12

    #@585
    if-eqz v12, :cond_636

    #@587
    .line 1106
    :cond_587
    const-string v12, "sys.factory.qem"

    #@589
    const/4 v13, 0x0

    #@58a
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@58d
    move-result v5

    #@58e
    .line 1107
    .restart local v5       #factory_qem:Z
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@590
    new-instance v13, Ljava/lang/StringBuilder;

    #@592
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@595
    const-string v14, "[LGE_DATA] factory_qem["

    #@597
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59a
    move-result-object v13

    #@59b
    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@59e
    move-result-object v13

    #@59f
    const-string v14, "]"

    #@5a1
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a4
    move-result-object v13

    #@5a5
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a8
    move-result-object v13

    #@5a9
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5ac
    .line 1108
    const/4 v12, 0x1

    #@5ad
    if-ne v5, v12, :cond_5cb

    #@5af
    move-object/from16 v0, p0

    #@5b1
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@5b3
    iget-object v12, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5b5
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5b7
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@5ba
    move-result-object v12

    #@5bb
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BLOCK_PAYPOPUP_BUT_TRYSETUP:Z

    #@5bd
    const/4 v13, 0x1

    #@5be
    if-ne v12, v13, :cond_5cb

    #@5c0
    .line 1109
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@5c2
    const-string v13, "[LGE_DATA] QEM mode, blocking data call and don\'t show charging popup"

    #@5c4
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5c7
    .line 1110
    const/16 v12, 0x12f

    #@5c9
    goto/16 :goto_119

    #@5cb
    .line 1114
    :cond_5cb
    move-object/from16 v0, p0

    #@5cd
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@5cf
    const/4 v13, 0x0

    #@5d0
    iput-boolean v13, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@5d2
    .line 1115
    move-object/from16 v0, p0

    #@5d4
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@5d6
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@5d9
    move-result-object v12

    #@5da
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5dd
    move-result-object v12

    #@5de
    const-string v13, "mobile_data"

    #@5e0
    const/4 v14, 0x0

    #@5e1
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@5e4
    .line 1118
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showDialog:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@5e6
    const/16 v13, 0x66

    #@5e8
    move-object/from16 v0, p0

    #@5ea
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@5ed
    .line 1120
    move-object/from16 v0, p0

    #@5ef
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@5f1
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@5f4
    move-result-object v12

    #@5f5
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5f8
    move-result-object v12

    #@5f9
    const-string v13, "data_network_wait_for_paypopup_response"

    #@5fb
    const/4 v14, 0x1

    #@5fc
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@5ff
    .line 1122
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@601
    new-instance v13, Ljava/lang/StringBuilder;

    #@603
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@606
    const-string v14, "DATA_NETWORK_WAIT_FOR_PAYPOPUP_RESPONSE : "

    #@608
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60b
    move-result-object v13

    #@60c
    move-object/from16 v0, p0

    #@60e
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@610
    invoke-interface {v14}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@613
    move-result-object v14

    #@614
    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@617
    move-result-object v14

    #@618
    const-string v15, "data_network_wait_for_paypopup_response"

    #@61a
    const/16 v16, 0x0

    #@61c
    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@61f
    move-result v14

    #@620
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@623
    move-result-object v13

    #@624
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@627
    move-result-object v13

    #@628
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@62b
    .line 1124
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@62d
    const-string v13, "PayPopupforKT is asking for the response from use"

    #@62f
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@632
    .line 1126
    const/16 v12, 0x12d

    #@634
    goto/16 :goto_119

    #@636
    .line 1127
    .end local v5           #factory_qem:Z
    :cond_636
    move-object/from16 v0, p0

    #@638
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@63a
    invoke-virtual {v12}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@63d
    move-result v12

    #@63e
    if-nez v12, :cond_644

    #@640
    .line 1129
    const/16 v12, 0x12e

    #@642
    goto/16 :goto_119

    #@644
    .line 1131
    :cond_644
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@646
    const-string v13, " PayPopup is just pass not asking "

    #@648
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64b
    .line 1133
    .end local v3           #ask_at_boot:I
    .end local v8           #is_waiting:I
    .end local v11           #user_response:I
    :cond_64b
    const/16 v12, 0x12f

    #@64d
    goto/16 :goto_119

    #@64f
    .line 1139
    :pswitch_64f
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@651
    const-string v13, "PayPopupforLGT()"

    #@653
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@656
    .line 1141
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@658
    new-instance v13, Ljava/lang/StringBuilder;

    #@65a
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@65d
    const-string v14, "in_prog_bypass :: "

    #@65f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@662
    move-result-object v13

    #@663
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@666
    move-result-object v13

    #@667
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66a
    move-result-object v13

    #@66b
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66e
    .line 1143
    move-object/from16 v0, p0

    #@670
    iget-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@672
    const/4 v13, 0x1

    #@673
    if-ne v12, v13, :cond_6a0

    #@675
    .line 1144
    move-object/from16 v0, p0

    #@677
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@679
    sget-object v13, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@67b
    const-string v14, ""

    #@67d
    const/4 v15, 0x1

    #@67e
    invoke-virtual {v12, v13, v14, v15}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@681
    move-result v9

    #@682
    .line 1145
    .local v9, mode:I
    move-object/from16 v0, p0

    #@684
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@686
    const/4 v12, 0x2

    #@687
    if-ne v9, v12, :cond_6a0

    #@689
    .line 1146
    const-string v12, "net.Is_phone_booted"

    #@68b
    const-string v13, "false"

    #@68d
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@690
    .line 1147
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@692
    const-string v13, "1st boot case, and just showing data blocked toast"

    #@694
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@697
    .line 1149
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@699
    const/16 v13, 0x8

    #@69b
    move-object/from16 v0, p0

    #@69d
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@6a0
    .line 1153
    .end local v9           #mode:I
    :cond_6a0
    if-nez v7, :cond_9fb

    #@6a2
    .line 1155
    move-object/from16 v0, p0

    #@6a4
    iget-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@6a6
    const/4 v13, 0x1

    #@6a7
    if-ne v12, v13, :cond_6cd

    #@6a9
    .line 1157
    move-object/from16 v0, p0

    #@6ab
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@6ad
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@6b0
    move-result-object v12

    #@6b1
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6b4
    move-result-object v12

    #@6b5
    const-string v13, "data_network_wait_for_paypopup_response"

    #@6b7
    const/4 v14, 0x0

    #@6b8
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@6bb
    .line 1158
    move-object/from16 v0, p0

    #@6bd
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@6bf
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@6c2
    move-result-object v12

    #@6c3
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6c6
    move-result-object v12

    #@6c7
    const-string v13, "data_network_user_paypopup_response"

    #@6c9
    const/4 v14, 0x0

    #@6ca
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@6cd
    .line 1165
    :cond_6cd
    move-object/from16 v0, p0

    #@6cf
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@6d1
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@6d4
    move-result-object v12

    #@6d5
    invoke-virtual {v12}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@6d8
    move-result v12

    #@6d9
    if-eqz v12, :cond_7ea

    #@6db
    .line 1166
    move-object/from16 v0, p0

    #@6dd
    iget-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mActiveDomesticPopup:Z

    #@6df
    const/4 v13, 0x1

    #@6e0
    if-ne v12, v13, :cond_70b

    #@6e2
    .line 1167
    move-object/from16 v0, p0

    #@6e4
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@6e6
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@6e9
    move-result-object v12

    #@6ea
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6ed
    move-result-object v12

    #@6ee
    const-string v13, "data_network_wait_for_paypopup_response"

    #@6f0
    const/4 v14, 0x0

    #@6f1
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@6f4
    .line 1168
    move-object/from16 v0, p0

    #@6f6
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@6f8
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@6fb
    move-result-object v12

    #@6fc
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6ff
    move-result-object v12

    #@700
    const-string v13, "data_network_user_paypopup_response"

    #@702
    const/4 v14, 0x0

    #@703
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@706
    .line 1169
    const/4 v12, 0x0

    #@707
    move-object/from16 v0, p0

    #@709
    iput-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mActiveDomesticPopup:Z

    #@70b
    .line 1182
    :cond_70b
    :goto_70b
    move-object/from16 v0, p0

    #@70d
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@70f
    sget-object v13, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@711
    const-string v14, ""

    #@713
    const/4 v15, 0x1

    #@714
    invoke-virtual {v12, v13, v14, v15}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@717
    move-result v9

    #@718
    .line 1183
    .restart local v9       #mode:I
    move-object/from16 v0, p0

    #@71a
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@71c
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@71f
    move-result-object v12

    #@720
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@723
    move-result-object v12

    #@724
    const-string v13, "data_network_wait_for_paypopup_response"

    #@726
    const/4 v14, 0x0

    #@727
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@72a
    move-result v8

    #@72b
    .line 1184
    .restart local v8       #is_waiting:I
    move-object/from16 v0, p0

    #@72d
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@72f
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@732
    move-result-object v12

    #@733
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@736
    move-result-object v12

    #@737
    const-string v13, "data_network_user_paypopup_response"

    #@739
    const/4 v14, 0x0

    #@73a
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@73d
    move-result v11

    #@73e
    .line 1186
    .restart local v11       #user_response:I
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@740
    new-instance v13, Ljava/lang/StringBuilder;

    #@742
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@745
    const-string v14, "trySetupData with mode="

    #@747
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74a
    move-result-object v13

    #@74b
    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@74e
    move-result-object v13

    #@74f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@752
    move-result-object v13

    #@753
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@756
    .line 1187
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@758
    new-instance v13, Ljava/lang/StringBuilder;

    #@75a
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@75d
    const-string v14, "trySetupData with is_waiting="

    #@75f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@762
    move-result-object v13

    #@763
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@766
    move-result-object v13

    #@767
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76a
    move-result-object v13

    #@76b
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@76e
    .line 1188
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@770
    new-instance v13, Ljava/lang/StringBuilder;

    #@772
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@775
    const-string v14, "trySetupData with user_choice="

    #@777
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77a
    move-result-object v13

    #@77b
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77e
    move-result-object v13

    #@77f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@782
    move-result-object v13

    #@783
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@786
    .line 1190
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@788
    new-instance v13, Ljava/lang/StringBuilder;

    #@78a
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@78d
    const-string v14, "trySetupData with booting="

    #@78f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@792
    move-result-object v13

    #@793
    move-object/from16 v0, p0

    #@795
    iget-boolean v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@797
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@79a
    move-result-object v13

    #@79b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79e
    move-result-object v13

    #@79f
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a2
    .line 1192
    const/4 v12, 0x1

    #@7a3
    if-ne v8, v12, :cond_85b

    #@7a5
    .line 1193
    move-object/from16 v0, p0

    #@7a7
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@7a9
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@7ac
    move-result-object v12

    #@7ad
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7b0
    move-result-object v12

    #@7b1
    const-string v13, "mobile_data"

    #@7b3
    const/4 v14, 0x0

    #@7b4
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@7b7
    move-result v12

    #@7b8
    const/4 v13, 0x1

    #@7b9
    if-ne v12, v13, :cond_81c

    #@7bb
    .line 1194
    move-object/from16 v0, p0

    #@7bd
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@7bf
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@7c2
    move-result-object v12

    #@7c3
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7c6
    move-result-object v12

    #@7c7
    const-string v13, "data_network_wait_for_paypopup_response"

    #@7c9
    const/4 v14, 0x0

    #@7ca
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@7cd
    .line 1195
    move-object/from16 v0, p0

    #@7cf
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@7d1
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@7d4
    move-result-object v12

    #@7d5
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7d8
    move-result-object v12

    #@7d9
    const-string v13, "data_network_user_paypopup_response"

    #@7db
    const/4 v14, 0x0

    #@7dc
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@7df
    .line 1197
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@7e1
    const-string v13, "When is_wating == 1 and hide paypopup so connect force!!"

    #@7e3
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7e6
    .line 1198
    const/16 v12, 0x12f

    #@7e8
    goto/16 :goto_119

    #@7ea
    .line 1172
    .end local v8           #is_waiting:I
    .end local v9           #mode:I
    .end local v11           #user_response:I
    :cond_7ea
    move-object/from16 v0, p0

    #@7ec
    iget-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mActiveRoamingPopup:Z

    #@7ee
    const/4 v13, 0x1

    #@7ef
    if-ne v12, v13, :cond_70b

    #@7f1
    .line 1173
    move-object/from16 v0, p0

    #@7f3
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@7f5
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@7f8
    move-result-object v12

    #@7f9
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7fc
    move-result-object v12

    #@7fd
    const-string v13, "data_network_wait_for_paypopup_response"

    #@7ff
    const/4 v14, 0x0

    #@800
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@803
    .line 1174
    move-object/from16 v0, p0

    #@805
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@807
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@80a
    move-result-object v12

    #@80b
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@80e
    move-result-object v12

    #@80f
    const-string v13, "data_network_user_paypopup_response"

    #@811
    const/4 v14, 0x0

    #@812
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@815
    .line 1175
    const/4 v12, 0x0

    #@816
    move-object/from16 v0, p0

    #@818
    iput-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mActiveRoamingPopup:Z

    #@81a
    goto/16 :goto_70b

    #@81c
    .line 1200
    .restart local v8       #is_waiting:I
    .restart local v9       #mode:I
    .restart local v11       #user_response:I
    :cond_81c
    if-nez v11, :cond_829

    #@81e
    .line 1201
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@820
    const-string v13, "PayPopupforLGT is waiting for user response"

    #@822
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@825
    .line 1202
    const/16 v12, 0x12d

    #@827
    goto/16 :goto_119

    #@829
    .line 1203
    :cond_829
    const/4 v12, 0x2

    #@82a
    if-ne v11, v12, :cond_85b

    #@82c
    .line 1204
    move-object/from16 v0, p0

    #@82e
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@830
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@833
    move-result-object v12

    #@834
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@837
    move-result-object v12

    #@838
    const-string v13, "data_network_wait_for_paypopup_response"

    #@83a
    const/4 v14, 0x0

    #@83b
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@83e
    .line 1205
    move-object/from16 v0, p0

    #@840
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@842
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@845
    move-result-object v12

    #@846
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@849
    move-result-object v12

    #@84a
    const-string v13, "data_network_user_paypopup_response"

    #@84c
    const/4 v14, 0x0

    #@84d
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@850
    .line 1207
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@852
    const-string v13, "PayPopupforLGT is accpeted by user"

    #@854
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@857
    .line 1208
    const/16 v12, 0x12f

    #@859
    goto/16 :goto_119

    #@85b
    .line 1216
    :cond_85b
    move-object/from16 v0, p0

    #@85d
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@85f
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@862
    move-result-object v12

    #@863
    invoke-virtual {v12}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@866
    move-result v12

    #@867
    if-eqz v12, :cond_8c0

    #@869
    .line 1217
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@86b
    const-string v13, "abnormal case, it\'s ROAMING state"

    #@86d
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@870
    .line 1219
    sget v12, Lcom/android/internal/telephony/PayPopup_Korea;->airplane_mode:I

    #@872
    if-eqz v12, :cond_87b

    #@874
    move-object/from16 v0, p0

    #@876
    iget-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@878
    const/4 v13, 0x1

    #@879
    if-ne v12, v13, :cond_8ae

    #@87b
    .line 1223
    :cond_87b
    const-string v12, "net.Is_phone_booted"

    #@87d
    const-string v13, "false"

    #@87f
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@882
    .line 1224
    const/4 v12, -0x1

    #@883
    sput v12, Lcom/android/internal/telephony/PayPopup_Korea;->airplane_mode:I

    #@885
    .line 1225
    move-object/from16 v0, p0

    #@887
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@889
    const/4 v13, 0x0

    #@88a
    invoke-interface {v12, v13}, Lcom/android/internal/telephony/Phone;->setDataRoamingEnabled(Z)V

    #@88d
    .line 1228
    move-object/from16 v0, p0

    #@88f
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@891
    const/4 v13, 0x0

    #@892
    invoke-interface {v12, v13}, Lcom/android/internal/telephony/Phone;->setLTEDataRoamingEnable(Z)V

    #@895
    .line 1231
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showDialog:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@897
    const/16 v13, 0x68

    #@899
    move-object/from16 v0, p0

    #@89b
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@89e
    .line 1232
    const/4 v12, 0x1

    #@89f
    move-object/from16 v0, p0

    #@8a1
    iput-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mActiveRoamingPopup:Z

    #@8a3
    .line 1236
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@8a5
    const-string v13, "starting.. roaming query popup"

    #@8a7
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8aa
    .line 1238
    const/16 v12, 0x12d

    #@8ac
    goto/16 :goto_119

    #@8ae
    .line 1240
    :cond_8ae
    const-string v12, "net.Is_phone_booted"

    #@8b0
    const-string v13, "false"

    #@8b2
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@8b5
    .line 1242
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@8b7
    const-string v13, "RoamingPopup is not booting or ask at boot"

    #@8b9
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8bc
    .line 1244
    const/16 v12, 0x12f

    #@8be
    goto/16 :goto_119

    #@8c0
    .line 1257
    :cond_8c0
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@8c2
    const-string v13, "normal case, it\'s not roaming state"

    #@8c4
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8c7
    .line 1259
    move-object/from16 v0, p0

    #@8c9
    iget-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@8cb
    const/4 v13, 0x1

    #@8cc
    if-ne v12, v13, :cond_997

    #@8ce
    .line 1260
    move-object/from16 v0, p0

    #@8d0
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@8d2
    const/4 v12, 0x3

    #@8d3
    if-ne v9, v12, :cond_965

    #@8d5
    .line 1261
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@8d7
    const-string v13, "1st boot case, and need to show pay popup !!!"

    #@8d9
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8dc
    .line 1263
    const-string v12, "net.Is_phone_booted"

    #@8de
    const-string v13, "false"

    #@8e0
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@8e3
    .line 1267
    const-string v12, "sys.factory.qem"

    #@8e5
    const/4 v13, 0x0

    #@8e6
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@8e9
    move-result v5

    #@8ea
    .line 1268
    .restart local v5       #factory_qem:Z
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@8ec
    new-instance v13, Ljava/lang/StringBuilder;

    #@8ee
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@8f1
    const-string v14, "[LGE_DATA] factory_qem = "

    #@8f3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f6
    move-result-object v13

    #@8f7
    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8fa
    move-result-object v13

    #@8fb
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8fe
    move-result-object v13

    #@8ff
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@902
    .line 1269
    const/4 v12, 0x1

    #@903
    if-ne v5, v12, :cond_921

    #@905
    move-object/from16 v0, p0

    #@907
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@909
    iget-object v12, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@90b
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@90d
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@910
    move-result-object v12

    #@911
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BLOCK_PAYPOPUP_BUT_TRYSETUP:Z

    #@913
    const/4 v13, 0x1

    #@914
    if-ne v12, v13, :cond_921

    #@916
    .line 1270
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@918
    const-string v13, "[LGE_DATA] QEM mode, blocking data call and don\'t show charging popup"

    #@91a
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@91d
    .line 1271
    const/16 v12, 0x12f

    #@91f
    goto/16 :goto_119

    #@921
    .line 1276
    :cond_921
    move-object/from16 v0, p0

    #@923
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@925
    const/4 v13, 0x0

    #@926
    iput-boolean v13, v12, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@928
    .line 1277
    move-object/from16 v0, p0

    #@92a
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@92c
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@92f
    move-result-object v12

    #@930
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@933
    move-result-object v12

    #@934
    const-string v13, "mobile_data"

    #@936
    const/4 v14, 0x0

    #@937
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@93a
    .line 1279
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showDialog:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@93c
    const/16 v13, 0x67

    #@93e
    move-object/from16 v0, p0

    #@940
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@943
    .line 1280
    const/4 v12, 0x1

    #@944
    move-object/from16 v0, p0

    #@946
    iput-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mActiveDomesticPopup:Z

    #@948
    .line 1282
    move-object/from16 v0, p0

    #@94a
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@94c
    invoke-interface {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@94f
    move-result-object v12

    #@950
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@953
    move-result-object v12

    #@954
    const-string v13, "data_network_wait_for_paypopup_response"

    #@956
    const/4 v14, 0x1

    #@957
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@95a
    .line 1284
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@95c
    const-string v13, "PayPopupforLGT is asking for the response from use"

    #@95e
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@961
    .line 1285
    const/16 v12, 0x12d

    #@963
    goto/16 :goto_119

    #@965
    .line 1288
    .end local v5           #factory_qem:Z
    :cond_965
    const-string v12, "net.Is_phone_booted"

    #@967
    const-string v13, "false"

    #@969
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@96c
    .line 1290
    move-object/from16 v0, p0

    #@96e
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDcMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@970
    const/4 v12, 0x1

    #@971
    if-ne v9, v12, :cond_986

    #@973
    .line 1291
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@975
    const-string v13, "1st boot case, and just showing data allowed toast"

    #@977
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@97a
    .line 1293
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@97c
    const/4 v13, 0x7

    #@97d
    move-object/from16 v0, p0

    #@97f
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@982
    .line 1300
    :goto_982
    const/16 v12, 0x12f

    #@984
    goto/16 :goto_119

    #@986
    .line 1296
    :cond_986
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@988
    const-string v13, "1st boot case, and just showing data blocked toast"

    #@98a
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@98d
    .line 1298
    sget-object v12, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@98f
    const/16 v13, 0x8

    #@991
    move-object/from16 v0, p0

    #@993
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@996
    goto :goto_982

    #@997
    .line 1304
    :cond_997
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@999
    new-instance v13, Ljava/lang/StringBuilder;

    #@99b
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@99e
    const-string v14, "[LGE_DATA_ROAMING] global_new_mcc="

    #@9a0
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a3
    move-result-object v13

    #@9a4
    move-object/from16 v0, p0

    #@9a6
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->global_new_mcc:Ljava/lang/String;

    #@9a8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9ab
    move-result-object v13

    #@9ac
    const-string v14, ", roam_to_domestic_popup_need="

    #@9ae
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b1
    move-result-object v13

    #@9b2
    move-object/from16 v0, p0

    #@9b4
    iget-boolean v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->roam_to_domestic_popup_need:Z

    #@9b6
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9b9
    move-result-object v13

    #@9ba
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9bd
    move-result-object v13

    #@9be
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9c1
    .line 1305
    move-object/from16 v0, p0

    #@9c3
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->global_new_mcc:Ljava/lang/String;

    #@9c5
    const-string v13, "450"

    #@9c7
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9ca
    move-result v12

    #@9cb
    if-nez v12, :cond_9e9

    #@9cd
    move-object/from16 v0, p0

    #@9cf
    iget-object v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->global_new_mcc:Ljava/lang/String;

    #@9d1
    const-string v13, "000"

    #@9d3
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9d6
    move-result v12

    #@9d7
    if-nez v12, :cond_9e9

    #@9d9
    .line 1306
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@9db
    const-string v13, "[LGE_DATA_ROAMING] payPopupforLGT is not booting, PAY_POPUP_NOT_ALLOWED before mcc_change from roam to domestic"

    #@9dd
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9e0
    .line 1307
    const/4 v12, 0x1

    #@9e1
    move-object/from16 v0, p0

    #@9e3
    iput-boolean v12, v0, Lcom/android/internal/telephony/PayPopup_Korea;->roam_to_domestic_popup_need:Z

    #@9e5
    .line 1308
    const/16 v12, 0x12e

    #@9e7
    goto/16 :goto_119

    #@9e9
    .line 1311
    :cond_9e9
    const-string v12, "net.Is_phone_booted"

    #@9eb
    const-string v13, "false"

    #@9ed
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9f0
    .line 1313
    const-string v12, "[LGE_DATA][PayPopUp_ko] "

    #@9f2
    const-string v13, "PayPopupforLGT is not booting or ask at boot"

    #@9f4
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9f7
    .line 1314
    const/16 v12, 0x12f

    #@9f9
    goto/16 :goto_119

    #@9fb
    .line 1320
    .end local v8           #is_waiting:I
    .end local v9           #mode:I
    .end local v11           #user_response:I
    :cond_9fb
    const/16 v12, 0x12f

    #@9fd
    goto/16 :goto_119

    #@9ff
    .line 936
    nop

    #@a00
    :pswitch_data_a00
    .packed-switch 0x3
        :pswitch_11a
        :pswitch_43c
        :pswitch_64f
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/DataConnectionTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/android/internal/telephony/PayPopup_Korea;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 79
    iput-object p1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->global_new_mcc:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$302(Lcom/android/internal/telephony/PayPopup_Korea;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 79
    iput-object p1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->global_old_mcc:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/PayPopup_Korea;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/android/internal/telephony/PayPopup_Korea;->roam_to_domestic_popup_need:Z

    #@2
    return v0
.end method

.method static synthetic access$402(Lcom/android/internal/telephony/PayPopup_Korea;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->roam_to_domestic_popup_need:Z

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/PayPopup_Korea;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mMobileEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/android/internal/telephony/PayPopup_Korea;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mMobileEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/PayPopup_Korea;)Landroid/net/ConnectivityManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@2
    return-object v0
.end method

.method static synthetic access$702(Lcom/android/internal/telephony/PayPopup_Korea;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->intent_reset:Z

    #@2
    return p1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 427
    const-string v3, "[LGE_DATA][PayPopUp_ko] "

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "[LGE_DATA] handleMessage msg="

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 429
    iget v3, p1, Landroid/os/Message;->what:I

    #@1c
    sparse-switch v3, :sswitch_data_118

    #@1f
    .line 470
    const-string v3, "[LGE_DATA][PayPopUp_ko] "

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "[LGE_DATA] invalud handleMessage["

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    const-string v5, "]"

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 474
    :cond_3d
    :goto_3d
    return-void

    #@3e
    .line 431
    :sswitch_3e
    const/16 v3, 0x64

    #@40
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/PayPopup_Korea;->retryRemovedPayPopup(I)V

    #@43
    goto :goto_3d

    #@44
    .line 434
    :sswitch_44
    const/16 v3, 0x66

    #@46
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/PayPopup_Korea;->retryRemovedPayPopup(I)V

    #@49
    goto :goto_3d

    #@4a
    .line 437
    :sswitch_4a
    const/16 v3, 0x67

    #@4c
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/PayPopup_Korea;->retryRemovedPayPopup(I)V

    #@4f
    goto :goto_3d

    #@50
    .line 441
    :sswitch_50
    const-string v3, "[LGE_DATA][PayPopUp_ko] "

    #@52
    const-string v4, "[LGE_DATA] EVENT_DELAYED_TOAST_KT "

    #@54
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 442
    const-string v3, "[LGE_DATA][PayPopUp_ko] "

    #@59
    new-instance v4, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v5, "[LGE_DATA] mStatus "

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    iget-boolean v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mStatus:Z

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v4

    #@6e
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 443
    iget-boolean v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mMobileEnabled:Z

    #@73
    if-ne v3, v6, :cond_3d

    #@75
    iget-object v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@77
    const-string v4, "KTBASE"

    #@79
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7c
    move-result v3

    #@7d
    if-eqz v3, :cond_3d

    #@7f
    iget-boolean v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mStatus:Z

    #@81
    if-eq v3, v6, :cond_3d

    #@83
    .line 444
    const/4 v0, 0x0

    #@84
    .line 445
    .local v0, currentSubType:I
    iget-object v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@86
    if-eqz v3, :cond_9a

    #@88
    iget-object v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@8a
    invoke-virtual {v3, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@8d
    move-result-object v3

    #@8e
    if-eqz v3, :cond_9a

    #@90
    .line 446
    iget-object v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@92
    invoke-virtual {v3, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@95
    move-result-object v3

    #@96
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getSubtype()I

    #@99
    move-result v0

    #@9a
    .line 450
    :cond_9a
    if-eq v0, v6, :cond_9f

    #@9c
    const/4 v3, 0x2

    #@9d
    if-ne v0, v3, :cond_ef

    #@9f
    .line 451
    :cond_9f
    iget-object v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@a1
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@a4
    move-result-object v3

    #@a5
    const v4, 0x209027b

    #@a8
    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@ab
    move-result-object v3

    #@ac
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@af
    move-result-object v2

    #@b0
    .line 458
    .local v2, str_value:Ljava/lang/String;
    :goto_b0
    iget-object v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@b2
    invoke-interface {v3}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getShowKTPayPopup()Z

    #@b5
    move-result v3

    #@b6
    if-ne v3, v6, :cond_eb

    #@b8
    .line 459
    new-instance v1, Landroid/content/Intent;

    #@ba
    const-string v3, "lge.intent.action.toast"

    #@bc
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@bf
    .line 460
    .local v1, intent_kr:Landroid/content/Intent;
    const-string v3, "text"

    #@c1
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c4
    .line 461
    const-string v3, "[LGE_DATA][PayPopUp_ko] "

    #@c6
    new-instance v4, Ljava/lang/StringBuilder;

    #@c8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@cb
    const-string v5, "[LGE_DATA] mWifiServiceExt.getShowKTPayPopup() = "

    #@cd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v4

    #@d1
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@d3
    invoke-interface {v5}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getShowKTPayPopup()Z

    #@d6
    move-result v5

    #@d7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@da
    move-result-object v4

    #@db
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v4

    #@df
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    .line 462
    iget-object v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@e4
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@e7
    move-result-object v3

    #@e8
    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@eb
    .line 464
    .end local v1           #intent_kr:Landroid/content/Intent;
    :cond_eb
    iput-boolean v6, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mStatus:Z

    #@ed
    goto/16 :goto_3d

    #@ef
    .line 452
    .end local v2           #str_value:Ljava/lang/String;
    :cond_ef
    const/16 v3, 0xd

    #@f1
    if-ne v0, v3, :cond_105

    #@f3
    .line 453
    iget-object v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@f5
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@f8
    move-result-object v3

    #@f9
    const v4, 0x209027c

    #@fc
    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@ff
    move-result-object v3

    #@100
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@103
    move-result-object v2

    #@104
    .restart local v2       #str_value:Ljava/lang/String;
    goto :goto_b0

    #@105
    .line 455
    .end local v2           #str_value:Ljava/lang/String;
    :cond_105
    iget-object v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@107
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@10a
    move-result-object v3

    #@10b
    const v4, 0x209027a

    #@10e
    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@111
    move-result-object v3

    #@112
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@115
    move-result-object v2

    #@116
    .restart local v2       #str_value:Ljava/lang/String;
    goto :goto_b0

    #@117
    .line 429
    nop

    #@118
    :sswitch_data_118
    .sparse-switch
        0x190 -> :sswitch_3e
        0x191 -> :sswitch_44
        0x192 -> :sswitch_4a
        0x258 -> :sswitch_50
    .end sparse-switch
.end method

.method public retryRemovedPayPopup(I)V
    .registers 9
    .parameter "popup_name"

    #@0
    .prologue
    const/16 v6, 0x64

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    .line 484
    if-lt p1, v6, :cond_a

    #@6
    const/16 v1, 0x68

    #@8
    if-le p1, v1, :cond_29

    #@a
    .line 485
    :cond_a
    const-string v1, "[LGE_DATA][PayPopUp_ko] "

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "[LGE_DATA] retryRemovedPayPopup, Invalid popup_name["

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, "]"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 530
    :goto_28
    return-void

    #@29
    .line 490
    :cond_29
    iget v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->retryStartActivityForPopup:I

    #@2b
    add-int/lit8 v1, v1, -0x1

    #@2d
    iput v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->retryStartActivityForPopup:I

    #@2f
    .line 492
    const-string v1, "[LGE_DATA][PayPopUp_ko] "

    #@31
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v3, "[LGE_DATA] Popup is removed! ["

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    const-string v3, "], retryStartActivityForPopup["

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    iget v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->retryStartActivityForPopup:I

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    const-string v3, "]"

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 494
    iget v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->retryStartActivityForPopup:I

    #@5b
    if-lez v1, :cond_9c

    #@5d
    .line 495
    const-string v1, "[LGE_DATA][PayPopUp_ko] "

    #@5f
    const-string v2, "[LGE_DATA] Restart Popup !"

    #@61
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 497
    packed-switch p1, :pswitch_data_12a

    #@67
    .line 509
    :pswitch_67
    const-string v1, "[LGE_DATA][PayPopUp_ko] "

    #@69
    new-instance v2, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v3, "[LGE_DATA][jk.soh] Invalid popup["

    #@70
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    const-string v3, "]"

    #@7a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v2

    #@82
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    goto :goto_28

    #@86
    .line 500
    :pswitch_86
    sget-object v1, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showDialog:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@88
    invoke-virtual {p0, v1, v6}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@8b
    goto :goto_28

    #@8c
    .line 503
    :pswitch_8c
    sget-object v1, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showDialog:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@8e
    const/16 v2, 0x66

    #@90
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@93
    goto :goto_28

    #@94
    .line 506
    :pswitch_94
    sget-object v1, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showDialog:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@96
    const/16 v2, 0x67

    #@98
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@9b
    goto :goto_28

    #@9c
    .line 515
    :cond_9c
    const/4 v1, 0x5

    #@9d
    iput v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->retryStartActivityForPopup:I

    #@9f
    .line 518
    iget-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@a1
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@a4
    move-result-object v1

    #@a5
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a8
    move-result-object v1

    #@a9
    const-string v2, "data_network_user_data_disable_setting"

    #@ab
    invoke-static {v1, v2, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@ae
    move-result v0

    #@af
    .line 519
    .local v0, user_setting:I
    const-string v1, "[LGE_DATA][PayPopUp_ko] "

    #@b1
    new-instance v2, Ljava/lang/StringBuilder;

    #@b3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b6
    const-string v3, "[LGE_DATA] To show paypopup is failed. Restore user_setting value["

    #@b8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v2

    #@bc
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v2

    #@c0
    const-string v3, "] roam : "

    #@c2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v2

    #@c6
    iget-object v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@c8
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@cb
    move-result-object v3

    #@cc
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@cf
    move-result v3

    #@d0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v2

    #@d4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v2

    #@d8
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@db
    .line 520
    iget-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@dd
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@df
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e1
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@e4
    move-result-object v1

    #@e5
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MAINTAIN_USER_DATA_SETTING:Z

    #@e7
    if-eqz v1, :cond_100

    #@e9
    iget-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@eb
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@ee
    move-result-object v1

    #@ef
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@f2
    move-result v1

    #@f3
    if-nez v1, :cond_100

    #@f5
    if-ne v0, v4, :cond_100

    #@f7
    iget-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@f9
    if-eqz v1, :cond_100

    #@fb
    .line 525
    iget-object v1, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@fd
    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    #@100
    .line 527
    :cond_100
    const-string v1, "[LGE_DATA][PayPopUp_ko] "

    #@102
    new-instance v2, Ljava/lang/StringBuilder;

    #@104
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@107
    const-string v3, "[LGE_DATA] <retryRemovedPopup()> MOBILE_SET : "

    #@109
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v2

    #@10d
    iget-object v3, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@10f
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@112
    move-result-object v3

    #@113
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@116
    move-result-object v3

    #@117
    const-string v4, "mobile_data"

    #@119
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11c
    move-result v3

    #@11d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@120
    move-result-object v2

    #@121
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v2

    #@125
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@128
    goto/16 :goto_28

    #@12a
    .line 497
    :pswitch_data_12a
    .packed-switch 0x64
        :pswitch_86
        :pswitch_67
        :pswitch_8c
        :pswitch_94
    .end packed-switch
.end method

.method public showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V
    .registers 12
    .parameter "funcName"
    .parameter "reason"

    #@0
    .prologue
    const v8, 0x2090282

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v6, 0x0

    #@5
    .line 595
    sget-object v4, Lcom/android/internal/telephony/PayPopup_Korea$2;->$SwitchMap$com$android$internal$telephony$PayPopup_Korea$PayPopupFunction:[I

    #@7
    invoke-virtual {p1}, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->ordinal()I

    #@a
    move-result v7

    #@b
    aget v4, v4, v7

    #@d
    packed-switch v4, :pswitch_data_292

    #@10
    .line 739
    :goto_10
    return-void

    #@11
    .line 598
    :pswitch_11
    const-string v4, "[LGE_DATA][PayPopUp_ko] "

    #@13
    const-string v5, "showToast()"

    #@15
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 599
    new-instance v2, Landroid/content/Intent;

    #@1a
    const-string v4, "lge.intent.action.toast"

    #@1c
    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1f
    .line 601
    .local v2, intent:Landroid/content/Intent;
    packed-switch p2, :pswitch_data_29a

    #@22
    .line 671
    const-string v4, "[LGE_DATA][PayPopUp_ko] "

    #@24
    new-instance v5, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v6, "The toast doesn\'t exist for this reason : "

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_10

    #@3b
    .line 603
    :pswitch_3b
    const-string v4, "text"

    #@3d
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@3f
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@42
    move-result-object v5

    #@43
    const v6, 0x2090287

    #@46
    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@51
    .line 675
    :goto_51
    iget-object v4, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@53
    invoke-interface {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@5a
    goto :goto_10

    #@5b
    .line 607
    :pswitch_5b
    const-string v4, "text"

    #@5d
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@5f
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@62
    move-result-object v5

    #@63
    const v6, 0x209028b

    #@66
    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@6d
    move-result-object v5

    #@6e
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@71
    goto :goto_51

    #@72
    .line 611
    :pswitch_72
    const-string v4, "text"

    #@74
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@76
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@79
    move-result-object v5

    #@7a
    const v6, 0x209028a

    #@7d
    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@80
    move-result-object v5

    #@81
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@84
    move-result-object v5

    #@85
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@88
    goto :goto_51

    #@89
    .line 617
    :pswitch_89
    const-string v4, "[LGE_DATA][PayPopUp_ko] "

    #@8b
    const-string v5, "[LGE_DATA] Roaming Toast"

    #@8d
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    .line 618
    iget-object v4, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@92
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@94
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@96
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@99
    move-result-object v4

    #@9a
    iget-boolean v4, v4, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_SKT:Z

    #@9c
    if-eqz v4, :cond_e6

    #@9e
    .line 620
    const-string v4, "ril.gsm.reject_cause"

    #@a0
    invoke-static {v4, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@a3
    move-result v0

    #@a4
    .line 622
    .local v0, data_rejCode:I
    sparse-switch v0, :sswitch_data_2b2

    #@a7
    .line 633
    const-string v4, "[LGE_DATA][PayPopUp_ko] "

    #@a9
    new-instance v5, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    const-string v6, "[LGE_DATA] PayPopup_Korea, reject_cause= "

    #@b0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v5

    #@b4
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v5

    #@b8
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v5

    #@bc
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    .line 634
    const-string v4, "text"

    #@c1
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@c3
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@c6
    move-result-object v5

    #@c7
    invoke-virtual {v5, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@ca
    move-result-object v5

    #@cb
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@ce
    move-result-object v5

    #@cf
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@d2
    goto/16 :goto_51

    #@d4
    .line 625
    :sswitch_d4
    const-string v4, "[LGE_DATA][PayPopUp_ko] "

    #@d6
    const-string v5, "[LGE_DATA] PayPopup_Korea, reject_cause GPRS services not allowed "

    #@d8
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@db
    goto/16 :goto_51

    #@dd
    .line 629
    :sswitch_dd
    const-string v4, "[LGE_DATA][PayPopUp_ko] "

    #@df
    const-string v5, "[LGE_DATA] PayPopup_Korea, reject_cause GPRS services not allowed in this PLMN "

    #@e1
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e4
    goto/16 :goto_51

    #@e6
    .line 640
    .end local v0           #data_rejCode:I
    :cond_e6
    const-string v4, "text"

    #@e8
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@ea
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@ed
    move-result-object v5

    #@ee
    invoke-virtual {v5, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@f1
    move-result-object v5

    #@f2
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@f5
    move-result-object v5

    #@f6
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@f9
    goto/16 :goto_51

    #@fb
    .line 647
    :pswitch_fb
    const-string v4, "text"

    #@fd
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@ff
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@102
    move-result-object v5

    #@103
    const v6, 0x209028d

    #@106
    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@109
    move-result-object v5

    #@10a
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10d
    move-result-object v5

    #@10e
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@111
    goto/16 :goto_51

    #@113
    .line 651
    :pswitch_113
    const-string v4, "text"

    #@115
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@117
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@11a
    move-result-object v5

    #@11b
    const v6, 0x2090291

    #@11e
    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@121
    move-result-object v5

    #@122
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@125
    move-result-object v5

    #@126
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@129
    goto/16 :goto_51

    #@12b
    .line 655
    :pswitch_12b
    const-string v4, "text"

    #@12d
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@12f
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@132
    move-result-object v5

    #@133
    const v6, 0x209029a

    #@136
    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@139
    move-result-object v5

    #@13a
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13d
    move-result-object v5

    #@13e
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@141
    goto/16 :goto_51

    #@143
    .line 659
    :pswitch_143
    const-string v4, "text"

    #@145
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@147
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@14a
    move-result-object v5

    #@14b
    const v6, 0x209029b

    #@14e
    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@151
    move-result-object v5

    #@152
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@155
    move-result-object v5

    #@156
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@159
    goto/16 :goto_51

    #@15b
    .line 663
    :pswitch_15b
    const-string v4, "text"

    #@15d
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@15f
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@162
    move-result-object v5

    #@163
    const v6, 0x2090298

    #@166
    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@169
    move-result-object v5

    #@16a
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@16d
    move-result-object v5

    #@16e
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@171
    goto/16 :goto_51

    #@173
    .line 667
    :pswitch_173
    const-string v4, "text"

    #@175
    iget-object v5, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@177
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@17a
    move-result-object v5

    #@17b
    const v6, 0x2090299

    #@17e
    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@181
    move-result-object v5

    #@182
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@185
    move-result-object v5

    #@186
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@189
    goto/16 :goto_51

    #@18b
    .line 683
    .end local v2           #intent:Landroid/content/Intent;
    :pswitch_18b
    const-string v4, "[LGE_DATA][PayPopUp_ko] "

    #@18d
    const-string v7, "showDialog()"

    #@18f
    invoke-static {v4, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@192
    .line 684
    new-instance v2, Landroid/content/Intent;

    #@194
    const-string v4, "android.intent.action.MAIN"

    #@196
    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@199
    .line 686
    .restart local v2       #intent:Landroid/content/Intent;
    packed-switch p2, :pswitch_data_2bc

    #@19c
    .line 709
    const-string v4, "[LGE_DATA][PayPopUp_ko] "

    #@19e
    new-instance v5, Ljava/lang/StringBuilder;

    #@1a0
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1a3
    const-string v6, "The dialog doesn\'t exist for this reason : "

    #@1a5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v5

    #@1a9
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v5

    #@1ad
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b0
    move-result-object v5

    #@1b1
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b4
    goto/16 :goto_10

    #@1b6
    .line 688
    :pswitch_1b6
    const-string v4, "com.android.settings"

    #@1b8
    const-string v7, "com.android.settings.lgesetting.wireless.DataEnabledSettingBootableSKT"

    #@1ba
    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1bd
    .line 713
    :goto_1bd
    const/high16 v4, 0x1000

    #@1bf
    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@1c2
    .line 717
    :try_start_1c2
    iget-object v4, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@1c4
    invoke-interface {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@1c7
    move-result-object v4

    #@1c8
    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1cb
    .catch Ljava/lang/Exception; {:try_start_1c2 .. :try_end_1cb} :catch_1cd

    #@1cb
    goto/16 :goto_10

    #@1cd
    .line 719
    :catch_1cd
    move-exception v1

    #@1ce
    .line 721
    .local v1, e:Ljava/lang/Exception;
    iget-object v4, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@1d0
    invoke-interface {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@1d3
    move-result-object v4

    #@1d4
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1d7
    move-result-object v4

    #@1d8
    const-string v7, "data_network_user_data_disable_setting"

    #@1da
    invoke-static {v4, v7, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1dd
    move-result v3

    #@1de
    .line 722
    .local v3, user_setting:I
    const-string v4, "[LGE_DATA][PayPopUp_ko] "

    #@1e0
    new-instance v7, Ljava/lang/StringBuilder;

    #@1e2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1e5
    const-string v8, "[LGE_DATA_EXCEPT] Exception user_setting : "

    #@1e7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ea
    move-result-object v7

    #@1eb
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v7

    #@1ef
    const-string v8, " roam : "

    #@1f1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v7

    #@1f5
    iget-object v8, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@1f7
    invoke-interface {v8}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@1fa
    move-result-object v8

    #@1fb
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@1fe
    move-result v8

    #@1ff
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@202
    move-result-object v7

    #@203
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@206
    move-result-object v7

    #@207
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20a
    .line 723
    iget-object v4, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@20c
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@20e
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@210
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@213
    move-result-object v4

    #@214
    iget-boolean v4, v4, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MAINTAIN_USER_DATA_SETTING:Z

    #@216
    if-eqz v4, :cond_22f

    #@218
    iget-object v4, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@21a
    invoke-interface {v4}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@21d
    move-result-object v4

    #@21e
    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@221
    move-result v4

    #@222
    if-nez v4, :cond_22f

    #@224
    if-ne v3, v5, :cond_22f

    #@226
    iget-object v4, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@228
    if-eqz v4, :cond_22f

    #@22a
    .line 728
    iget-object v4, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mConnMgr:Landroid/net/ConnectivityManager;

    #@22c
    invoke-virtual {v4, v5}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    #@22f
    .line 730
    :cond_22f
    const-string v4, "[LGE_DATA][PayPopUp_ko] "

    #@231
    new-instance v5, Ljava/lang/StringBuilder;

    #@233
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@236
    const-string v7, "[LGE_DATA] <onDataConnectionAttached()> MOBILE_SET : "

    #@238
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23b
    move-result-object v5

    #@23c
    iget-object v7, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@23e
    invoke-interface {v7}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@241
    move-result-object v7

    #@242
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@245
    move-result-object v7

    #@246
    const-string v8, "mobile_data"

    #@248
    invoke-static {v7, v8, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@24b
    move-result v6

    #@24c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24f
    move-result-object v5

    #@250
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@253
    move-result-object v5

    #@254
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@257
    goto/16 :goto_10

    #@259
    .line 692
    .end local v1           #e:Ljava/lang/Exception;
    .end local v3           #user_setting:I
    :pswitch_259
    const-string v4, "com.android.settings"

    #@25b
    const-string v7, "com.android.settings.lgesetting.wireless.DataRoamingSettingsBootableSKT"

    #@25d
    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@260
    goto/16 :goto_1bd

    #@262
    .line 696
    :pswitch_262
    const-string v4, "com.android.settings"

    #@264
    const-string v7, "com.android.settings.lgesetting.wireless.DataNetworkModePayPopupKT"

    #@266
    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@269
    .line 697
    const-string v7, "isRoaming"

    #@26b
    iget-object v4, p0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@26d
    invoke-interface {v4}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@270
    move-result-object v4

    #@271
    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@274
    move-result v4

    #@275
    if-ne v4, v5, :cond_27d

    #@277
    move v4, v5

    #@278
    :goto_278
    invoke-virtual {v2, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@27b
    goto/16 :goto_1bd

    #@27d
    :cond_27d
    move v4, v6

    #@27e
    goto :goto_278

    #@27f
    .line 701
    :pswitch_27f
    const-string v4, "com.android.settings"

    #@281
    const-string v7, "com.android.settings.lgesetting.wireless.DataNetworkModePayPopupLGT"

    #@283
    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@286
    goto/16 :goto_1bd

    #@288
    .line 705
    :pswitch_288
    const-string v4, "com.android.settings"

    #@28a
    const-string v7, "com.android.settings.lgesetting.wireless.DataNetworkModeRoamingQueryPopupLGT"

    #@28c
    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@28f
    goto/16 :goto_1bd

    #@291
    .line 595
    nop

    #@292
    :pswitch_data_292
    .packed-switch 0x1
        :pswitch_11
        :pswitch_18b
    .end packed-switch

    #@29a
    .line 601
    :pswitch_data_29a
    .packed-switch 0x1
        :pswitch_3b
        :pswitch_5b
        :pswitch_72
        :pswitch_89
        :pswitch_fb
        :pswitch_113
        :pswitch_12b
        :pswitch_143
        :pswitch_15b
        :pswitch_173
    .end packed-switch

    #@2b2
    .line 622
    :sswitch_data_2b2
    .sparse-switch
        0x7 -> :sswitch_d4
        0xe -> :sswitch_dd
    .end sparse-switch

    #@2bc
    .line 686
    :pswitch_data_2bc
    .packed-switch 0x64
        :pswitch_1b6
        :pswitch_259
        :pswitch_262
        :pswitch_27f
        :pswitch_288
    .end packed-switch
.end method

.method public startPayPopup(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 22
    .parameter "reason"
    .parameter "apn_type"

    #@0
    .prologue
    .line 742
    const/4 v11, 0x0

    #@1
    .line 744
    .local v11, result:I
    const-string v5, ""

    #@3
    .line 748
    .local v5, WhichCase:Ljava/lang/String;
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@5
    new-instance v15, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v16, "net.Is_phone_booted : "

    #@c
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v15

    #@10
    const-string v16, "net.Is_phone_booted"

    #@12
    invoke-static/range {v16 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v16

    #@16
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v15

    #@1a
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v15

    #@1e
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 750
    const-string v14, "net.Is_phone_booted"

    #@23
    invoke-static {v14}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v14

    #@27
    const-string v15, "true"

    #@29
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v14

    #@2d
    move-object/from16 v0, p0

    #@2f
    iput-boolean v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@31
    .line 752
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@33
    new-instance v15, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v16, "mbooting_phone : "

    #@3a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v15

    #@3e
    move-object/from16 v0, p0

    #@40
    iget-boolean v0, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@42
    move/from16 v16, v0

    #@44
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@47
    move-result-object v15

    #@48
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v15

    #@4c
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 754
    move-object/from16 v0, p0

    #@51
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@53
    const-string v15, "SKTBASE"

    #@55
    invoke-static {v14, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@58
    move-result v14

    #@59
    if-nez v14, :cond_73

    #@5b
    move-object/from16 v0, p0

    #@5d
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@5f
    const-string v15, "KTBASE"

    #@61
    invoke-static {v14, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@64
    move-result v14

    #@65
    if-nez v14, :cond_73

    #@67
    move-object/from16 v0, p0

    #@69
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@6b
    const-string v15, "LGTBASE"

    #@6d
    invoke-static {v14, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@70
    move-result v14

    #@71
    if-eqz v14, :cond_311

    #@73
    .line 757
    :cond_73
    const/16 v4, 0x20

    #@75
    .line 759
    .local v4, LGE_EXCEPTION_NEED_OPENNING:I
    const/4 v9, 0x0

    #@76
    .line 761
    .local v9, isBlockNetConn:Z
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@78
    new-instance v15, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v16, "isBlockNetConn() : gsm.lge.ota_is_running = "

    #@7f
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v15

    #@83
    const-string v16, "gsm.lge.ota_is_running"

    #@85
    invoke-static/range {v16 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@88
    move-result-object v16

    #@89
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v15

    #@8d
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v15

    #@91
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 763
    const-string v14, "gsm.lge.ota_is_running"

    #@96
    invoke-static {v14}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@99
    move-result-object v14

    #@9a
    const-string v15, "true"

    #@9c
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9f
    move-result v14

    #@a0
    if-eqz v14, :cond_a3

    #@a2
    .line 764
    const/4 v9, 0x1

    #@a3
    .line 767
    :cond_a3
    move-object/from16 v0, p0

    #@a5
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@a7
    const-string v15, "SKTBASE"

    #@a9
    invoke-static {v14, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@ac
    move-result v14

    #@ad
    if-eqz v14, :cond_f5

    #@af
    .line 769
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@b1
    new-instance v15, Ljava/lang/StringBuilder;

    #@b3
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@b6
    const-string v16, "SKT_OTA_USIM_DOWNLOAD = "

    #@b8
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v15

    #@bc
    move-object/from16 v0, p0

    #@be
    iget-object v0, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@c0
    move-object/from16 v16, v0

    #@c2
    invoke-interface/range {v16 .. v16}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@c5
    move-result-object v16

    #@c6
    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c9
    move-result-object v16

    #@ca
    const-string v17, "skt_ota_usim_download"

    #@cc
    const/16 v18, 0x0

    #@ce
    invoke-static/range {v16 .. v18}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d1
    move-result v16

    #@d2
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v15

    #@d6
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v15

    #@da
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dd
    .line 771
    move-object/from16 v0, p0

    #@df
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@e1
    invoke-interface {v14}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@e4
    move-result-object v14

    #@e5
    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e8
    move-result-object v14

    #@e9
    const-string v15, "skt_ota_usim_download"

    #@eb
    const/16 v16, 0x0

    #@ed
    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@f0
    move-result v14

    #@f1
    const/4 v15, 0x1

    #@f2
    if-ne v14, v15, :cond_f5

    #@f4
    .line 772
    const/4 v9, 0x1

    #@f5
    .line 777
    :cond_f5
    move-object/from16 v0, p0

    #@f7
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@f9
    const-string v15, "LGTBASE"

    #@fb
    invoke-static {v14, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@fe
    move-result v14

    #@ff
    if-nez v14, :cond_10d

    #@101
    move-object/from16 v0, p0

    #@103
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@105
    const-string v15, "KTBASE"

    #@107
    invoke-static {v14, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@10a
    move-result v14

    #@10b
    if-eqz v14, :cond_182

    #@10d
    .line 778
    :cond_10d
    const-string v14, "gsm.sim.type"

    #@10f
    invoke-static {v14}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@112
    move-result-object v12

    #@113
    .line 780
    .local v12, simType:Ljava/lang/String;
    new-instance v13, Lcom/android/internal/telephony/uicc/UsimService;

    #@115
    invoke-direct {v13}, Lcom/android/internal/telephony/uicc/UsimService;-><init>()V

    #@118
    .line 781
    .local v13, usimService:Lcom/android/internal/telephony/uicc/UsimService;
    if-eqz v13, :cond_13a

    #@11a
    .line 782
    invoke-virtual {v13}, Lcom/android/internal/telephony/uicc/UsimService;->getUsimIsEmpty()I

    #@11d
    move-result v14

    #@11e
    sput v14, Lcom/android/internal/telephony/PayPopup_Korea;->UiccIsEmpty:I

    #@120
    .line 783
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@122
    new-instance v15, Ljava/lang/StringBuilder;

    #@124
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@127
    const-string v16, "[LGE_DATA] DATA_BLOCK_BY_UNSIGNED_UICC / UiccIsEmpty = "

    #@129
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v15

    #@12d
    sget v16, Lcom/android/internal/telephony/PayPopup_Korea;->UiccIsEmpty:I

    #@12f
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@132
    move-result-object v15

    #@133
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@136
    move-result-object v15

    #@137
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13a
    .line 787
    :cond_13a
    sget v14, Lcom/android/internal/telephony/PayPopup_Korea;->UiccIsEmpty:I

    #@13c
    const/4 v15, 0x2

    #@13d
    if-ne v14, v15, :cond_15f

    #@13f
    const-string v14, "skt"

    #@141
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@144
    move-result v14

    #@145
    if-nez v14, :cond_157

    #@147
    const-string v14, "kt"

    #@149
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14c
    move-result v14

    #@14d
    if-nez v14, :cond_157

    #@14f
    const-string v14, "lgu"

    #@151
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@154
    move-result v14

    #@155
    if-eqz v14, :cond_15f

    #@157
    .line 788
    :cond_157
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@159
    const-string v15, "[LGE_DATA] normal case  empty sim"

    #@15b
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15e
    .line 789
    const/4 v9, 0x1

    #@15f
    .line 792
    :cond_15f
    sget v14, Lcom/android/internal/telephony/PayPopup_Korea;->UiccIsEmpty:I

    #@161
    const/4 v15, 0x1

    #@162
    if-ne v14, v15, :cond_182

    #@164
    const-string v14, "lgu"

    #@166
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@169
    move-result v14

    #@16a
    if-eqz v14, :cond_182

    #@16c
    const-string v14, "gsm.lge.ota_is_running"

    #@16e
    invoke-static {v14}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@171
    move-result-object v14

    #@172
    const-string v15, "true"

    #@174
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@177
    move-result v14

    #@178
    if-eqz v14, :cond_182

    #@17a
    .line 793
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@17c
    const-string v15, "[LGE_DATA] this case is phone number change on HiddenMenu"

    #@17e
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@181
    .line 794
    const/4 v9, 0x0

    #@182
    .line 800
    .end local v12           #simType:Ljava/lang/String;
    .end local v13           #usimService:Lcom/android/internal/telephony/uicc/UsimService;
    :cond_182
    move-object/from16 v0, p0

    #@184
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@186
    const-string v15, "KTBASE"

    #@188
    invoke-static {v14, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@18b
    move-result v14

    #@18c
    if-eqz v14, :cond_19c

    #@18e
    move-object/from16 v0, p0

    #@190
    iget-boolean v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->intent_reset:Z

    #@192
    if-eqz v14, :cond_19c

    #@194
    .line 801
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@196
    const-string v15, "[LGE_DATA] this case is reset"

    #@198
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19b
    .line 802
    const/4 v9, 0x1

    #@19c
    .line 807
    :cond_19c
    if-eqz v9, :cond_1a7

    #@19e
    .line 808
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@1a0
    const-string v15, "isBlockNetConn = OTA"

    #@1a2
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a5
    .line 809
    const/4 v14, 0x0

    #@1a6
    .line 897
    .end local v4           #LGE_EXCEPTION_NEED_OPENNING:I
    .end local v9           #isBlockNetConn:Z
    :goto_1a6
    return v14

    #@1a7
    .line 814
    .restart local v4       #LGE_EXCEPTION_NEED_OPENNING:I
    .restart local v9       #isBlockNetConn:Z
    :cond_1a7
    move-object/from16 v0, p0

    #@1a9
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@1ab
    invoke-interface {v14}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@1ae
    move-result-object v14

    #@1af
    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b2
    move-result-object v14

    #@1b3
    const-string v15, "data_network_user_paypopup_transition_from_wifi_to_mobile"

    #@1b5
    const/16 v16, 0x0

    #@1b7
    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1ba
    move-result v7

    #@1bb
    .line 816
    .local v7, from_wifi_to_mobile:I
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@1bd
    new-instance v15, Ljava/lang/StringBuilder;

    #@1bf
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1c2
    const-string v16, "isWhatKindofReason() : mbooting_phone = "

    #@1c4
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v15

    #@1c8
    move-object/from16 v0, p0

    #@1ca
    iget-boolean v0, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@1cc
    move/from16 v16, v0

    #@1ce
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v15

    #@1d2
    const-string v16, "/ from_wifi_to_mobile = "

    #@1d4
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v15

    #@1d8
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v15

    #@1dc
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1df
    move-result-object v15

    #@1e0
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e3
    .line 819
    const/4 v3, 0x0

    #@1e4
    .line 821
    .local v3, IsThereUsingNetwork:Z
    move-object/from16 v0, p0

    #@1e6
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1e8
    iget-object v14, v14, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@1ea
    invoke-virtual {v14}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    #@1ed
    move-result-object v14

    #@1ee
    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1f1
    move-result-object v8

    #@1f2
    .local v8, i$:Ljava/util/Iterator;
    :cond_1f2
    :goto_1f2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@1f5
    move-result v14

    #@1f6
    if-eqz v14, :cond_242

    #@1f8
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1fb
    move-result-object v6

    #@1fc
    check-cast v6, Lcom/android/internal/telephony/ApnContext;

    #@1fe
    .line 823
    .local v6, apnContext:Lcom/android/internal/telephony/ApnContext;
    invoke-virtual {v6}, Lcom/android/internal/telephony/ApnContext;->getState()Lcom/android/internal/telephony/DctConstants$State;

    #@201
    move-result-object v14

    #@202
    sget-object v15, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@204
    if-ne v14, v15, :cond_1f2

    #@206
    .line 826
    move-object/from16 v0, p0

    #@208
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@20a
    iget-object v14, v14, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@20c
    iget-object v14, v14, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@20e
    invoke-interface {v14}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@211
    move-result-object v14

    #@212
    iget-boolean v14, v14, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_KR:Z

    #@214
    if-eqz v14, :cond_240

    #@216
    .line 828
    invoke-virtual {v6}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@219
    move-result-object v14

    #@21a
    const-string v15, "ims"

    #@21c
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21f
    move-result v14

    #@220
    if-nez v14, :cond_1f2

    #@222
    .line 830
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@224
    new-instance v15, Ljava/lang/StringBuilder;

    #@226
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@229
    const-string v16, "APN_TYPE"

    #@22b
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v15

    #@22f
    invoke-virtual {v6}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@232
    move-result-object v16

    #@233
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@236
    move-result-object v15

    #@237
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23a
    move-result-object v15

    #@23b
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23e
    .line 832
    const/4 v3, 0x1

    #@23f
    goto :goto_1f2

    #@240
    .line 837
    :cond_240
    const/4 v3, 0x1

    #@241
    goto :goto_1f2

    #@242
    .line 843
    .end local v6           #apnContext:Lcom/android/internal/telephony/ApnContext;
    :cond_242
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@244
    new-instance v15, Ljava/lang/StringBuilder;

    #@246
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@249
    const-string v16, "IsThereTypeInUse() :"

    #@24b
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24e
    move-result-object v15

    #@24f
    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@252
    move-result-object v15

    #@253
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@256
    move-result-object v15

    #@257
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25a
    .line 847
    const-string v14, "dataAttached"

    #@25c
    move-object/from16 v0, p1

    #@25e
    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@261
    move-result v14

    #@262
    if-nez v14, :cond_278

    #@264
    const-string v14, "simLoaded"

    #@266
    move-object/from16 v0, p1

    #@268
    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26b
    move-result v14

    #@26c
    if-nez v14, :cond_278

    #@26e
    const-string v14, "roamingOn"

    #@270
    move-object/from16 v0, p1

    #@272
    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@275
    move-result v14

    #@276
    if-eqz v14, :cond_2ec

    #@278
    :cond_278
    move-object/from16 v0, p0

    #@27a
    iget-boolean v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@27c
    const/4 v15, 0x1

    #@27d
    if-ne v14, v15, :cond_2ec

    #@27f
    .line 848
    const-string v10, "booting"

    #@281
    .line 862
    .local v10, isWhatKindofReason:Ljava/lang/String;
    :goto_281
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@283
    new-instance v15, Ljava/lang/StringBuilder;

    #@285
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@288
    const-string v16, "Original reason = "

    #@28a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28d
    move-result-object v15

    #@28e
    move-object/from16 v0, p1

    #@290
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@293
    move-result-object v15

    #@294
    const-string v16, " , LGE reason = "

    #@296
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@299
    move-result-object v15

    #@29a
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29d
    move-result-object v15

    #@29e
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a1
    move-result-object v15

    #@2a2
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a5
    .line 864
    move-object v5, v10

    #@2a6
    .line 866
    move-object/from16 v0, p0

    #@2a8
    iget-boolean v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@2aa
    const/4 v15, 0x1

    #@2ab
    if-ne v14, v15, :cond_311

    #@2ad
    move-object/from16 v0, p0

    #@2af
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@2b1
    const-string v15, "LGTBASE"

    #@2b3
    invoke-static {v14, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@2b6
    move-result v14

    #@2b7
    if-nez v14, :cond_311

    #@2b9
    .line 867
    const-string v14, "booting"

    #@2bb
    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2be
    move-result v14

    #@2bf
    if-nez v14, :cond_311

    #@2c1
    .line 868
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@2c3
    new-instance v15, Ljava/lang/StringBuilder;

    #@2c5
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@2c8
    const-string v16, "check the booting case, mbooting_phone = "

    #@2ca
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cd
    move-result-object v15

    #@2ce
    move-object/from16 v0, p0

    #@2d0
    iget-boolean v0, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mbooting_phone:Z

    #@2d2
    move/from16 v16, v0

    #@2d4
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2d7
    move-result-object v15

    #@2d8
    const-string v16, " WhichCase = "

    #@2da
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2dd
    move-result-object v15

    #@2de
    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e1
    move-result-object v15

    #@2e2
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e5
    move-result-object v15

    #@2e6
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e9
    .line 869
    const/4 v14, 0x0

    #@2ea
    goto/16 :goto_1a6

    #@2ec
    .line 849
    .end local v10           #isWhatKindofReason:Ljava/lang/String;
    :cond_2ec
    const/4 v14, 0x1

    #@2ed
    if-ne v7, v14, :cond_306

    #@2ef
    .line 851
    const-string v10, "Wifi_off"

    #@2f1
    .line 853
    .restart local v10       #isWhatKindofReason:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2f3
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mPhone:Lcom/android/internal/telephony/Phone;

    #@2f5
    invoke-interface {v14}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@2f8
    move-result-object v14

    #@2f9
    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2fc
    move-result-object v14

    #@2fd
    const-string v15, "data_network_user_paypopup_transition_from_wifi_to_mobile"

    #@2ff
    const/16 v16, 0x0

    #@301
    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@304
    goto/16 :goto_281

    #@306
    .line 854
    .end local v10           #isWhatKindofReason:Ljava/lang/String;
    :cond_306
    const/4 v14, 0x1

    #@307
    if-ne v3, v14, :cond_30d

    #@309
    .line 857
    const-string v10, "no_display_popup"

    #@30b
    .restart local v10       #isWhatKindofReason:Ljava/lang/String;
    goto/16 :goto_281

    #@30d
    .line 859
    .end local v10           #isWhatKindofReason:Ljava/lang/String;
    :cond_30d
    const-string v10, "others"

    #@30f
    .restart local v10       #isWhatKindofReason:Ljava/lang/String;
    goto/16 :goto_281

    #@311
    .line 875
    .end local v3           #IsThereUsingNetwork:Z
    .end local v4           #LGE_EXCEPTION_NEED_OPENNING:I
    .end local v7           #from_wifi_to_mobile:I
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v9           #isBlockNetConn:Z
    .end local v10           #isWhatKindofReason:Ljava/lang/String;
    :cond_311
    move-object/from16 v0, p0

    #@313
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@315
    iget-object v14, v14, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@317
    iget-object v14, v14, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@319
    invoke-interface {v14}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@31c
    move-result-object v14

    #@31d
    iget-boolean v14, v14, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_SKT:Z

    #@31f
    if-eqz v14, :cond_35c

    #@321
    .line 876
    const-string v14, "no_display_popup"

    #@323
    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@326
    move-result v14

    #@327
    const/4 v15, 0x1

    #@328
    if-ne v14, v15, :cond_345

    #@32a
    .line 877
    const-string v14, "[LGE_DATA][PayPopUp_ko] "

    #@32c
    new-instance v15, Ljava/lang/StringBuilder;

    #@32e
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@331
    const-string v16, "##Return## Already connection WhichCase = "

    #@333
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@336
    move-result-object v15

    #@337
    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33a
    move-result-object v15

    #@33b
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33e
    move-result-object v15

    #@33f
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@342
    .line 878
    const/4 v14, 0x1

    #@343
    goto/16 :goto_1a6

    #@345
    .line 880
    :cond_345
    sget-object v14, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->PayPopupforSKT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@347
    move-object/from16 v0, p0

    #@349
    move-object/from16 v1, p1

    #@34b
    move-object/from16 v2, p2

    #@34d
    invoke-direct {v0, v14, v1, v2}, Lcom/android/internal/telephony/PayPopup_Korea;->PayPopupforFeature(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;Ljava/lang/String;Ljava/lang/String;)I

    #@350
    move-result v11

    #@351
    .line 893
    :cond_351
    :goto_351
    const/16 v14, 0x12d

    #@353
    if-eq v11, v14, :cond_359

    #@355
    const/16 v14, 0x12e

    #@357
    if-ne v11, v14, :cond_393

    #@359
    .line 894
    :cond_359
    const/4 v14, 0x0

    #@35a
    goto/16 :goto_1a6

    #@35c
    .line 884
    :cond_35c
    move-object/from16 v0, p0

    #@35e
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@360
    const-string v15, "KTBASE"

    #@362
    invoke-static {v14, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@365
    move-result v14

    #@366
    if-eqz v14, :cond_37a

    #@368
    .line 885
    sget-object v14, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->PayPopupforKT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@36a
    move-object/from16 v0, p0

    #@36c
    move-object/from16 v1, p2

    #@36e
    invoke-direct {v0, v14, v5, v1}, Lcom/android/internal/telephony/PayPopup_Korea;->PayPopupforFeature(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;Ljava/lang/String;Ljava/lang/String;)I

    #@371
    move-result v11

    #@372
    .line 887
    const-string v14, "net.Is_phone_booted"

    #@374
    const-string v15, "false"

    #@376
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@379
    goto :goto_351

    #@37a
    .line 888
    :cond_37a
    move-object/from16 v0, p0

    #@37c
    iget-object v14, v0, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@37e
    const-string v15, "LGTBASE"

    #@380
    invoke-static {v14, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@383
    move-result v14

    #@384
    if-eqz v14, :cond_351

    #@386
    .line 890
    sget-object v14, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->PayPopupforLGT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@388
    move-object/from16 v0, p0

    #@38a
    move-object/from16 v1, p1

    #@38c
    move-object/from16 v2, p2

    #@38e
    invoke-direct {v0, v14, v1, v2}, Lcom/android/internal/telephony/PayPopup_Korea;->PayPopupforFeature(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;Ljava/lang/String;Ljava/lang/String;)I

    #@391
    move-result v11

    #@392
    goto :goto_351

    #@393
    .line 897
    :cond_393
    const/4 v14, 0x1

    #@394
    goto/16 :goto_1a6
.end method
