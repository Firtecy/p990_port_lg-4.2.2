.class Lcom/android/internal/telephony/DataConnection$DcInactiveState;
.super Lcom/android/internal/util/State;
.source "DataConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DcInactiveState"
.end annotation


# instance fields
.field private mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

.field private mDisconnectParams:Lcom/android/internal/telephony/DataConnection$DisconnectParams;

.field private mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

.field final synthetic this$0:Lcom/android/internal/telephony/DataConnection;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/DataConnection;)V
    .registers 3
    .parameter

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1252
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@6
    .line 1253
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@8
    .line 1254
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@a
    .line 1255
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mDisconnectParams:Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@c
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1252
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/DataConnection$DcInactiveState;-><init>(Lcom/android/internal/telephony/DataConnection;)V

    #@3
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 4

    #@0
    .prologue
    .line 1272
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2
    iget v1, v0, Lcom/android/internal/telephony/DataConnection;->mTag:I

    #@4
    add-int/lit8 v1, v1, 0x1

    #@6
    iput v1, v0, Lcom/android/internal/telephony/DataConnection;->mTag:I

    #@8
    .line 1281
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@a
    if-eqz v0, :cond_30

    #@c
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@e
    if-eqz v0, :cond_30

    #@10
    .line 1283
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@12
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@14
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@16
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/DataConnection;->access$800(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@19
    .line 1285
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@1b
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@1d
    if-eqz v0, :cond_30

    #@1f
    invoke-static {}, Lcom/android/internal/telephony/DataConnection;->access$1200()Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_30

    #@25
    .line 1287
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@27
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@29
    iget v0, v0, Lcom/android/internal/telephony/DataProfile;->id:I

    #@2b
    const-string v1, "apnFailed"

    #@2d
    invoke-static {v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sendDisconnected(ILjava/lang/String;)V

    #@30
    .line 1291
    :cond_30
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mDisconnectParams:Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@32
    if-eqz v0, :cond_3c

    #@34
    .line 1293
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@36
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mDisconnectParams:Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@38
    const/4 v2, 0x1

    #@39
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/DataConnection;->access$1300(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$DisconnectParams;Z)V

    #@3c
    .line 1295
    :cond_3c
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3e
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnection;->clearSettings()V

    #@41
    .line 1296
    return-void
.end method

.method public exit()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1301
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@3
    .line 1302
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@5
    .line 1303
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mDisconnectParams:Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@7
    .line 1304
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1310
    iget v2, p1, Landroid/os/Message;->what:I

    #@3
    sparse-switch v2, :sswitch_data_b4

    #@6
    .line 1372
    const/4 v1, 0x0

    #@7
    .line 1375
    .local v1, retVal:Z
    :goto_7
    return v1

    #@8
    .line 1313
    .end local v1           #retVal:Z
    :sswitch_8
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@a
    const-string v3, "DcInactiveState: msg.what=RSP_RESET, ignore we\'re already reset"

    #@c
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@f
    .line 1315
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@11
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@13
    const v3, 0x4100f

    #@16
    invoke-virtual {v2, p1, v3}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;I)V

    #@19
    .line 1316
    const/4 v1, 0x1

    #@1a
    .line 1317
    .restart local v1       #retVal:Z
    goto :goto_7

    #@1b
    .line 1320
    .end local v1           #retVal:Z
    :sswitch_1b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1d
    check-cast v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@1f
    .line 1321
    .local v0, cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@21
    iget v2, v2, Lcom/android/internal/telephony/DataConnection;->mTag:I

    #@23
    iput v2, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->tag:I

    #@25
    .line 1323
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@27
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v4, "DcInactiveState msg.what=EVENT_CONNECT.RefCount = "

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@34
    iget v4, v4, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@41
    .line 1326
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@43
    const/4 v3, 0x1

    #@44
    iput v3, v2, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@46
    .line 1327
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@48
    invoke-virtual {v2, v0}, Lcom/android/internal/telephony/DataConnection;->onConnect(Lcom/android/internal/telephony/DataConnection$ConnectionParams;)V

    #@4b
    .line 1330
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@4d
    if-eqz v2, :cond_60

    #@4f
    invoke-static {}, Lcom/android/internal/telephony/DataConnection;->access$1200()Z

    #@52
    move-result v2

    #@53
    if-eqz v2, :cond_60

    #@55
    .line 1332
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@57
    iget v2, v2, Lcom/android/internal/telephony/DataProfile;->id:I

    #@59
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@5b
    iget-object v3, v3, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@5d
    invoke-static {v2, v3}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sendRequested(ILjava/lang/String;)V

    #@60
    .line 1336
    :cond_60
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@62
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@64
    invoke-static {v3}, Lcom/android/internal/telephony/DataConnection;->access$1400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcActivatingState;

    #@67
    move-result-object v3

    #@68
    invoke-static {v2, v3}, Lcom/android/internal/telephony/DataConnection;->access$1500(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@6b
    .line 1337
    const/4 v1, 0x1

    #@6c
    .line 1338
    .restart local v1       #retVal:Z
    goto :goto_7

    #@6d
    .line 1341
    .end local v0           #cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    .end local v1           #retVal:Z
    :sswitch_6d
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@6f
    const-string v3, "DcInactiveState: msg.what=EVENT_DISCONNECT"

    #@71
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@74
    .line 1342
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@76
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@78
    check-cast v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@7a
    invoke-static {v3, v2, v4}, Lcom/android/internal/telephony/DataConnection;->access$1300(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$DisconnectParams;Z)V

    #@7d
    .line 1343
    const/4 v1, 0x1

    #@7e
    .line 1344
    .restart local v1       #retVal:Z
    goto :goto_7

    #@7f
    .line 1347
    .end local v1           #retVal:Z
    :sswitch_7f
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@81
    const-string v3, "DcInactiveState: msg.what=EVENT_DISCONNECT_ALL"

    #@83
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@86
    .line 1348
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@88
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8a
    check-cast v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@8c
    invoke-static {v3, v2, v4}, Lcom/android/internal/telephony/DataConnection;->access$1300(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$DisconnectParams;Z)V

    #@8f
    .line 1349
    const/4 v1, 0x1

    #@90
    .line 1350
    .restart local v1       #retVal:Z
    goto/16 :goto_7

    #@92
    .line 1353
    .end local v1           #retVal:Z
    :sswitch_92
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@94
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@96
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@98
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@9b
    move-result-object v2

    #@9c
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@9e
    if-eqz v2, :cond_aa

    #@a0
    .line 1355
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@a2
    const-string v3, "DcInactiveState: msg.what=EVENT_SETUP_DATA_CONNECTION_DONE"

    #@a4
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@a7
    .line 1356
    const/4 v1, 0x1

    #@a8
    .restart local v1       #retVal:Z
    goto/16 :goto_7

    #@aa
    .line 1361
    .end local v1           #retVal:Z
    :cond_aa
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@ac
    const-string v3, "DcInactiveState: msg.what=EVENT_SETUP_DATA_CONNECTION_DONE Error!!!!"

    #@ae
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@b1
    .line 1362
    const/4 v1, 0x0

    #@b2
    .line 1365
    .restart local v1       #retVal:Z
    goto/16 :goto_7

    #@b4
    .line 1310
    :sswitch_data_b4
    .sparse-switch
        0x40000 -> :sswitch_1b
        0x40001 -> :sswitch_92
        0x40004 -> :sswitch_6d
        0x40006 -> :sswitch_7f
        0x4100e -> :sswitch_8
    .end sparse-switch
.end method

.method public setEnterNotificationParams(Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;I)V
    .registers 5
    .parameter "cp"
    .parameter "cause"
    .parameter "retryOverride"

    #@0
    .prologue
    .line 1260
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@2
    .line 1261
    iput-object p2, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@4
    .line 1262
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@6
    iput p3, v0, Lcom/android/internal/telephony/DataConnection;->mRetryOverride:I

    #@8
    .line 1263
    return-void
.end method

.method public setEnterNotificationParams(Lcom/android/internal/telephony/DataConnection$DisconnectParams;)V
    .registers 2
    .parameter "dp"

    #@0
    .prologue
    .line 1267
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->mDisconnectParams:Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@2
    .line 1268
    return-void
.end method
