.class Lcom/android/internal/telephony/PayPopup_Korea$1;
.super Landroid/content/BroadcastReceiver;
.source "PayPopup_Korea.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/PayPopup_Korea;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/PayPopup_Korea;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/PayPopup_Korea;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 225
    iput-object p1, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 17
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 228
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 230
    .local v1, action:Ljava/lang/String;
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@7
    new-instance v11, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v12, "intent received :"

    #@e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v11

    #@12
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v11

    #@16
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v11

    #@1a
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 234
    const-string v10, "android.net.conn.STARTING_IN_DATA_SETTING_DISABLE"

    #@1f
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v10

    #@23
    if-eqz v10, :cond_36

    #@25
    .line 235
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@27
    const-string v11, "ACTION_STARTING_IN_DATA_SETTING_DISABLE"

    #@29
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 236
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@2e
    sget-object v11, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@30
    const/4 v12, 0x3

    #@31
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V
    :try_end_34
    .catchall {:try_start_1 .. :try_end_34} :catchall_4e

    #@34
    .line 422
    :cond_34
    :goto_34
    monitor-exit p0

    #@35
    return-void

    #@36
    .line 238
    :cond_36
    :try_start_36
    const-string v10, "android.net.conn.STARTING_IN_ROAM_SETTING_DISABLE"

    #@38
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v10

    #@3c
    if-eqz v10, :cond_51

    #@3e
    .line 239
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@40
    const-string v11, "ACTION_STARTING_IN_ROAM_SETTING_DISABLE"

    #@42
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 240
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@47
    sget-object v11, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@49
    const/4 v12, 0x5

    #@4a
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V
    :try_end_4d
    .catchall {:try_start_36 .. :try_end_4d} :catchall_4e

    #@4d
    goto :goto_34

    #@4e
    .line 228
    .end local v1           #action:Ljava/lang/String;
    :catchall_4e
    move-exception v10

    #@4f
    monitor-exit p0

    #@50
    throw v10

    #@51
    .line 242
    .restart local v1       #action:Ljava/lang/String;
    :cond_51
    :try_start_51
    const-string v10, "android.net.conn.STARTING_IN_DATA_SETTING_DISABLE_3GONLY"

    #@53
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v10

    #@57
    if-eqz v10, :cond_69

    #@59
    .line 243
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@5b
    const-string v11, "ACTION_STARTING_IN_DATA_SETTING_DISABLE_3GONLY"

    #@5d
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 244
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@62
    sget-object v11, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@64
    const/4 v12, 0x2

    #@65
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@68
    goto :goto_34

    #@69
    .line 246
    :cond_69
    const-string v10, "android.net.conn.DATA_DATA_BLOCK_IN_MMS"

    #@6b
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v10

    #@6f
    if-eqz v10, :cond_81

    #@71
    .line 247
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@73
    const-string v11, "ACTION_DATA_BLOCK_IN_MMS "

    #@75
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 248
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@7a
    sget-object v11, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@7c
    const/4 v12, 0x6

    #@7d
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@80
    goto :goto_34

    #@81
    .line 252
    :cond_81
    const-string v10, "lge.intent.action.LGE_WIFI_3G_TRANSITION"

    #@83
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@86
    move-result v10

    #@87
    if-eqz v10, :cond_e1

    #@89
    .line 253
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@8b
    const-string v11, "lge.intent.action.LGE_WIFI_3G_TRANSITION"

    #@8d
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    .line 256
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@92
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;

    #@95
    move-result-object v10

    #@96
    invoke-interface {v10}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@99
    move-result-object v10

    #@9a
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9d
    move-result-object v10

    #@9e
    const-string v11, "mobile_data"

    #@a0
    const/4 v12, 0x1

    #@a1
    invoke-static {v10, v11, v12}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@a4
    move-result v10

    #@a5
    const/4 v11, 0x1

    #@a6
    if-ne v10, v11, :cond_b8

    #@a8
    .line 257
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@aa
    const-string v11, "[LGE_DATA] WiFi Off / data enable"

    #@ac
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@af
    .line 258
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@b1
    sget-object v11, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@b3
    const/16 v12, 0x9

    #@b5
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@b8
    .line 261
    :cond_b8
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@ba
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;

    #@bd
    move-result-object v10

    #@be
    invoke-interface {v10}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@c1
    move-result-object v10

    #@c2
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c5
    move-result-object v10

    #@c6
    const-string v11, "mobile_data"

    #@c8
    const/4 v12, 0x1

    #@c9
    invoke-static {v10, v11, v12}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@cc
    move-result v10

    #@cd
    if-nez v10, :cond_34

    #@cf
    .line 262
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@d1
    const-string v11, "[LGE_DATA] WiFi Off / data disable"

    #@d3
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    .line 263
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@d8
    sget-object v11, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@da
    const/16 v12, 0xa

    #@dc
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/telephony/PayPopup_Korea;->showToastAndDialog(Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;I)V

    #@df
    goto/16 :goto_34

    #@e1
    .line 268
    :cond_e1
    const-string v10, "com.lge.intent.action.LGE_CAMPED_MCC_CHANGE"

    #@e3
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e6
    move-result v10

    #@e7
    if-eqz v10, :cond_2e7

    #@e9
    .line 270
    const-string v7, "000"

    #@eb
    .line 271
    .local v7, new_mcc:Ljava/lang/String;
    const-string v8, "000"

    #@ed
    .line 273
    .local v8, old_mcc:Ljava/lang/String;
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@ef
    const-string v11, "[LGE_DATA_ROAMING] Got TelephonyIntents.ACTION_CAMPED_MCC_CHANGE "

    #@f1
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f4
    .line 275
    const-string v10, "newmcc"

    #@f6
    move-object/from16 v0, p2

    #@f8
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@fb
    move-result-object v7

    #@fc
    .line 276
    const-string v10, "oldmcc"

    #@fe
    move-object/from16 v0, p2

    #@100
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@103
    move-result-object v8

    #@104
    .line 278
    if-nez v8, :cond_108

    #@106
    .line 279
    const-string v8, "000"

    #@108
    .line 281
    :cond_108
    if-nez v7, :cond_10c

    #@10a
    .line 282
    const-string v7, "000"

    #@10c
    .line 286
    :cond_10c
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@10e
    iget-object v10, v10, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@110
    const-string v11, "SKTBASE"

    #@112
    invoke-static {v10, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@115
    move-result v10

    #@116
    if-eqz v10, :cond_16c

    #@118
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@11a
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$100(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@11d
    move-result-object v10

    #@11e
    iget-object v10, v10, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@120
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@122
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@125
    move-result-object v10

    #@126
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_SKT:Z

    #@128
    if-nez v10, :cond_16c

    #@12a
    const-string v10, "450"

    #@12c
    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12f
    move-result v10

    #@130
    if-eqz v10, :cond_16c

    #@132
    const-string v10, "450"

    #@134
    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@137
    move-result v10

    #@138
    if-nez v10, :cond_16c

    #@13a
    .line 287
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@13c
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;

    #@13f
    move-result-object v10

    #@140
    invoke-interface {v10}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@143
    move-result-object v10

    #@144
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@147
    move-result-object v10

    #@148
    const-string v11, "mobile_data"

    #@14a
    const/4 v12, 0x0

    #@14b
    invoke-static {v10, v11, v12}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@14e
    move-result v10

    #@14f
    if-nez v10, :cond_16c

    #@151
    .line 288
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@153
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;

    #@156
    move-result-object v10

    #@157
    invoke-interface {v10}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@15a
    move-result-object v10

    #@15b
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@15e
    move-result-object v10

    #@15f
    const-string v11, "data_network_user_data_disable_setting"

    #@161
    const/4 v12, 0x1

    #@162
    invoke-static {v10, v11, v12}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@165
    .line 289
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@167
    const-string v11, "[LGE_DATA] <onDataConnectionAttached()> DATA_NETWORK_USER_DATA_DISABLE_SETTING : 1"

    #@169
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16c
    .line 294
    :cond_16c
    const-string v10, "000"

    #@16e
    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@171
    move-result v10

    #@172
    if-eqz v10, :cond_187

    #@174
    .line 295
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@176
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;

    #@179
    move-result-object v10

    #@17a
    invoke-interface {v10}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@17d
    move-result-object v10

    #@17e
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@181
    move-result-object v10

    #@182
    const-string v11, "intent_old_mcc"

    #@184
    invoke-static {v10, v11, v8}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@187
    .line 297
    :cond_187
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@189
    iget-object v10, v10, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@18b
    const-string v11, "KTBASE"

    #@18d
    invoke-static {v10, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@190
    move-result v10

    #@191
    if-nez v10, :cond_1b1

    #@193
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@195
    iget-object v10, v10, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@197
    const-string v11, "SKTBASE"

    #@199
    invoke-static {v10, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@19c
    move-result v10

    #@19d
    if-eqz v10, :cond_1b3

    #@19f
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@1a1
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$100(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@1a4
    move-result-object v10

    #@1a5
    iget-object v10, v10, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1a7
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1a9
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1ac
    move-result-object v10

    #@1ad
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_SKT:Z

    #@1af
    if-nez v10, :cond_1b3

    #@1b1
    .line 298
    :cond_1b1
    const-string v7, "000"

    #@1b3
    .line 301
    :cond_1b3
    const-string v10, "000"

    #@1b5
    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b8
    move-result v10

    #@1b9
    if-nez v10, :cond_2de

    #@1bb
    .line 303
    const-string v10, "000"

    #@1bd
    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c0
    move-result v10

    #@1c1
    if-eqz v10, :cond_1d7

    #@1c3
    .line 304
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@1c5
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;

    #@1c8
    move-result-object v10

    #@1c9
    invoke-interface {v10}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@1cc
    move-result-object v10

    #@1cd
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1d0
    move-result-object v10

    #@1d1
    const-string v11, "intent_old_mcc"

    #@1d3
    invoke-static {v10, v11}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@1d6
    move-result-object v8

    #@1d7
    .line 307
    :cond_1d7
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@1d9
    new-instance v11, Ljava/lang/StringBuilder;

    #@1db
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1de
    const-string v12, "[LGE_DATA_ROAMING] old_mcc:  "

    #@1e0
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e3
    move-result-object v11

    #@1e4
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e7
    move-result-object v11

    #@1e8
    const-string v12, ", new_mcc:"

    #@1ea
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ed
    move-result-object v11

    #@1ee
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f1
    move-result-object v11

    #@1f2
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f5
    move-result-object v11

    #@1f6
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f9
    .line 309
    if-eqz v8, :cond_1fd

    #@1fb
    if-nez v7, :cond_221

    #@1fd
    .line 310
    :cond_1fd
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@1ff
    new-instance v11, Ljava/lang/StringBuilder;

    #@201
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@204
    const-string v12, "old_mcc:  "

    #@206
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v11

    #@20a
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20d
    move-result-object v11

    #@20e
    const-string v12, ", new_mcc:"

    #@210
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@213
    move-result-object v11

    #@214
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@217
    move-result-object v11

    #@218
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21b
    move-result-object v11

    #@21c
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21f
    goto/16 :goto_34

    #@221
    .line 311
    :cond_221
    const-string v10, "450"

    #@223
    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@226
    move-result v10

    #@227
    if-eqz v10, :cond_24b

    #@229
    const-string v10, "450"

    #@22b
    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22e
    move-result v10

    #@22f
    if-nez v10, :cond_24b

    #@231
    .line 312
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@233
    const-string v11, "[LGE_DATA_ROAMING] MCC Change Roaming popup OK"

    #@235
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@238
    .line 314
    const-string v10, "net.Is_phone_booted"

    #@23a
    const-string v11, "true"

    #@23c
    invoke-static {v10, v11}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@23f
    .line 316
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@241
    invoke-static {v10, v7}, Lcom/android/internal/telephony/PayPopup_Korea;->access$202(Lcom/android/internal/telephony/PayPopup_Korea;Ljava/lang/String;)Ljava/lang/String;

    #@244
    .line 317
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@246
    invoke-static {v10, v8}, Lcom/android/internal/telephony/PayPopup_Korea;->access$302(Lcom/android/internal/telephony/PayPopup_Korea;Ljava/lang/String;)Ljava/lang/String;

    #@249
    goto/16 :goto_34

    #@24b
    .line 318
    :cond_24b
    const-string v10, "450"

    #@24d
    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@250
    move-result v10

    #@251
    if-nez v10, :cond_34

    #@253
    const-string v10, "450"

    #@255
    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@258
    move-result v10

    #@259
    if-eqz v10, :cond_34

    #@25b
    .line 319
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@25d
    const-string v11, "[LGE_DATA_ROAMING] Domestic popup OK"

    #@25f
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@262
    .line 321
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@264
    iget-object v10, v10, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@266
    const-string v11, "LGTBASE"

    #@268
    invoke-static {v10, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@26b
    move-result v10

    #@26c
    if-eqz v10, :cond_296

    #@26e
    .line 322
    const-string v10, "net.Is_phone_booted"

    #@270
    const-string v11, "true"

    #@272
    invoke-static {v10, v11}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@275
    .line 325
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@277
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;

    #@27a
    move-result-object v10

    #@27b
    invoke-interface {v10}, Lcom/android/internal/telephony/Phone;->getLTEDataRoamingEnable()Z

    #@27e
    move-result v10

    #@27f
    if-eqz v10, :cond_296

    #@281
    .line 326
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@283
    const-string v11, "[LGE_DATA_ROAMING] when changed roaming to domestic and data_lte_roaming is 1, forcely set to 0"

    #@285
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@288
    .line 327
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@28a
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$100(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@28d
    move-result-object v10

    #@28e
    iget-object v10, v10, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@290
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mLGEDataConnectionTracker:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@292
    const/4 v11, 0x0

    #@293
    invoke-virtual {v10, v11}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->setLTEDataRoamingEnableNotApplyObserver(Z)V

    #@296
    .line 332
    :cond_296
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@298
    invoke-static {v10, v7}, Lcom/android/internal/telephony/PayPopup_Korea;->access$202(Lcom/android/internal/telephony/PayPopup_Korea;Ljava/lang/String;)Ljava/lang/String;

    #@29b
    .line 333
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@29d
    invoke-static {v10, v8}, Lcom/android/internal/telephony/PayPopup_Korea;->access$302(Lcom/android/internal/telephony/PayPopup_Korea;Ljava/lang/String;)Ljava/lang/String;

    #@2a0
    .line 335
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@2a2
    new-instance v11, Ljava/lang/StringBuilder;

    #@2a4
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@2a7
    const-string v12, "[LGE_DATA_ROAMING] roam_to_domestic_popup_need="

    #@2a9
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ac
    move-result-object v11

    #@2ad
    iget-object v12, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@2af
    invoke-static {v12}, Lcom/android/internal/telephony/PayPopup_Korea;->access$400(Lcom/android/internal/telephony/PayPopup_Korea;)Z

    #@2b2
    move-result v12

    #@2b3
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2b6
    move-result-object v11

    #@2b7
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ba
    move-result-object v11

    #@2bb
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2be
    .line 336
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@2c0
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$400(Lcom/android/internal/telephony/PayPopup_Korea;)Z

    #@2c3
    move-result v10

    #@2c4
    if-eqz v10, :cond_34

    #@2c6
    .line 337
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@2c8
    const-string v11, "roam_to_domesic : mcc_change"

    #@2ca
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2cd
    .line 338
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@2cf
    const-string v11, "roam_to_domesic"

    #@2d1
    const-string v12, "default"

    #@2d3
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/telephony/PayPopup_Korea;->startPayPopup(Ljava/lang/String;Ljava/lang/String;)Z

    #@2d6
    .line 339
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@2d8
    const/4 v11, 0x0

    #@2d9
    invoke-static {v10, v11}, Lcom/android/internal/telephony/PayPopup_Korea;->access$402(Lcom/android/internal/telephony/PayPopup_Korea;Z)Z

    #@2dc
    goto/16 :goto_34

    #@2de
    .line 343
    :cond_2de
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@2e0
    const-string v11, "[LGE_DATA_ROAMING] incorrect intet!!"

    #@2e2
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e5
    goto/16 :goto_34

    #@2e7
    .line 349
    .end local v7           #new_mcc:Ljava/lang/String;
    .end local v8           #old_mcc:Ljava/lang/String;
    :cond_2e7
    const-string v10, "android.intent.action.AIRPLANE_MODE"

    #@2e9
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2ec
    move-result v10

    #@2ed
    if-eqz v10, :cond_31b

    #@2ef
    .line 350
    const-string v10, "state"

    #@2f1
    const/4 v11, 0x0

    #@2f2
    move-object/from16 v0, p2

    #@2f4
    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@2f7
    move-result v3

    #@2f8
    .line 351
    .local v3, enabled:Z
    if-eqz v3, :cond_319

    #@2fa
    const/4 v10, 0x1

    #@2fb
    :goto_2fb
    sput v10, Lcom/android/internal/telephony/PayPopup_Korea;->airplane_mode:I

    #@2fd
    .line 352
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@2ff
    new-instance v11, Ljava/lang/StringBuilder;

    #@301
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@304
    const-string v12, "[LGE_DATA_ROAMING] Airplane_mode popup airplane: "

    #@306
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@309
    move-result-object v11

    #@30a
    sget v12, Lcom/android/internal/telephony/PayPopup_Korea;->airplane_mode:I

    #@30c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30f
    move-result-object v11

    #@310
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@313
    move-result-object v11

    #@314
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@317
    goto/16 :goto_34

    #@319
    .line 351
    :cond_319
    const/4 v10, 0x0

    #@31a
    goto :goto_2fb

    #@31b
    .line 356
    .end local v3           #enabled:Z
    :cond_31b
    const-string v10, "com.lge.DataEnabledSettingBootableSKT"

    #@31d
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@320
    move-result v10

    #@321
    if-eqz v10, :cond_334

    #@323
    .line 357
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@325
    iget-object v11, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@327
    const/16 v12, 0x190

    #@329
    invoke-virtual {v11, v12}, Lcom/android/internal/telephony/PayPopup_Korea;->obtainMessage(I)Landroid/os/Message;

    #@32c
    move-result-object v11

    #@32d
    const-wide/16 v12, 0x1f4

    #@32f
    invoke-virtual {v10, v11, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@332
    goto/16 :goto_34

    #@334
    .line 359
    :cond_334
    const-string v10, "com.lge.DataNetworkModePayPopupKT"

    #@336
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@339
    move-result v10

    #@33a
    if-eqz v10, :cond_34d

    #@33c
    .line 360
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@33e
    iget-object v11, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@340
    const/16 v12, 0x191

    #@342
    invoke-virtual {v11, v12}, Lcom/android/internal/telephony/PayPopup_Korea;->obtainMessage(I)Landroid/os/Message;

    #@345
    move-result-object v11

    #@346
    const-wide/16 v12, 0x1f4

    #@348
    invoke-virtual {v10, v11, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@34b
    goto/16 :goto_34

    #@34d
    .line 362
    :cond_34d
    const-string v10, "com.lge.DataNetworkModePayPopupLGT"

    #@34f
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@352
    move-result v10

    #@353
    if-eqz v10, :cond_366

    #@355
    .line 363
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@357
    iget-object v11, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@359
    const/16 v12, 0x192

    #@35b
    invoke-virtual {v11, v12}, Lcom/android/internal/telephony/PayPopup_Korea;->obtainMessage(I)Landroid/os/Message;

    #@35e
    move-result-object v11

    #@35f
    const-wide/16 v12, 0x1f4

    #@361
    invoke-virtual {v10, v11, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@364
    goto/16 :goto_34

    #@366
    .line 366
    :cond_366
    const-string v10, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@368
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36b
    move-result v10

    #@36c
    if-nez v10, :cond_376

    #@36e
    const-string v10, "android.net.conn.INET_CONDITION_ACTION"

    #@370
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@373
    move-result v10

    #@374
    if-eqz v10, :cond_524

    #@376
    :cond_376
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@378
    iget-object v10, v10, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@37a
    const-string v11, "KTBASE"

    #@37c
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37f
    move-result v10

    #@380
    if-eqz v10, :cond_524

    #@382
    .line 368
    const-string v10, "networkInfo"

    #@384
    move-object/from16 v0, p2

    #@386
    invoke-virtual {v0, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@389
    move-result v10

    #@38a
    if-eqz v10, :cond_34

    #@38c
    .line 369
    const-string v10, "networkInfo"

    #@38e
    move-object/from16 v0, p2

    #@390
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@393
    move-result-object v10

    #@394
    check-cast v10, Landroid/net/NetworkInfo;

    #@396
    move-object v0, v10

    #@397
    check-cast v0, Landroid/net/NetworkInfo;

    #@399
    move-object v4, v0

    #@39a
    .line 370
    .local v4, info:Landroid/net/NetworkInfo;
    if-eqz v4, :cond_34

    #@39c
    .line 371
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    #@39f
    move-result v10

    #@3a0
    packed-switch v10, :pswitch_data_540

    #@3a3
    goto/16 :goto_34

    #@3a5
    .line 373
    :pswitch_3a5
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@3a7
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    #@3aa
    move-result v11

    #@3ab
    invoke-static {v10, v11}, Lcom/android/internal/telephony/PayPopup_Korea;->access$502(Lcom/android/internal/telephony/PayPopup_Korea;Z)Z

    #@3ae
    .line 374
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@3b0
    new-instance v11, Ljava/lang/StringBuilder;

    #@3b2
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@3b5
    const-string v12, "TYPE_MOBILE is Connected = "

    #@3b7
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ba
    move-result-object v11

    #@3bb
    iget-object v12, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@3bd
    invoke-static {v12}, Lcom/android/internal/telephony/PayPopup_Korea;->access$500(Lcom/android/internal/telephony/PayPopup_Korea;)Z

    #@3c0
    move-result v12

    #@3c1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3c4
    move-result-object v11

    #@3c5
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c8
    move-result-object v11

    #@3c9
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3cc
    .line 376
    const/4 v2, 0x0

    #@3cd
    .line 377
    .local v2, currentSubType:I
    const/4 v6, 0x0

    #@3ce
    .line 378
    .local v6, isWifiConnected:Z
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@3d0
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$600(Lcom/android/internal/telephony/PayPopup_Korea;)Landroid/net/ConnectivityManager;

    #@3d3
    move-result-object v10

    #@3d4
    if-eqz v10, :cond_3f2

    #@3d6
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@3d8
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$600(Lcom/android/internal/telephony/PayPopup_Korea;)Landroid/net/ConnectivityManager;

    #@3db
    move-result-object v10

    #@3dc
    const/4 v11, 0x0

    #@3dd
    invoke-virtual {v10, v11}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@3e0
    move-result-object v10

    #@3e1
    if-eqz v10, :cond_3f2

    #@3e3
    .line 379
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@3e5
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$600(Lcom/android/internal/telephony/PayPopup_Korea;)Landroid/net/ConnectivityManager;

    #@3e8
    move-result-object v10

    #@3e9
    const/4 v11, 0x0

    #@3ea
    invoke-virtual {v10, v11}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@3ed
    move-result-object v10

    #@3ee
    invoke-virtual {v10}, Landroid/net/NetworkInfo;->getSubtype()I

    #@3f1
    move-result v2

    #@3f2
    .line 382
    :cond_3f2
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@3f4
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$600(Lcom/android/internal/telephony/PayPopup_Korea;)Landroid/net/ConnectivityManager;

    #@3f7
    move-result-object v10

    #@3f8
    if-eqz v10, :cond_416

    #@3fa
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@3fc
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$600(Lcom/android/internal/telephony/PayPopup_Korea;)Landroid/net/ConnectivityManager;

    #@3ff
    move-result-object v10

    #@400
    const/4 v11, 0x1

    #@401
    invoke-virtual {v10, v11}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@404
    move-result-object v10

    #@405
    if-eqz v10, :cond_416

    #@407
    .line 383
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@409
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$600(Lcom/android/internal/telephony/PayPopup_Korea;)Landroid/net/ConnectivityManager;

    #@40c
    move-result-object v10

    #@40d
    const/4 v11, 0x1

    #@40e
    invoke-virtual {v10, v11}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@411
    move-result-object v10

    #@412
    invoke-virtual {v10}, Landroid/net/NetworkInfo;->isConnected()Z

    #@415
    move-result v6

    #@416
    .line 386
    :cond_416
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@418
    new-instance v11, Ljava/lang/StringBuilder;

    #@41a
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@41d
    const-string v12, "[LGE_DATA] mStatus : "

    #@41f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@422
    move-result-object v11

    #@423
    iget-object v12, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@425
    iget-boolean v12, v12, Lcom/android/internal/telephony/PayPopup_Korea;->mStatus:Z

    #@427
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@42a
    move-result-object v11

    #@42b
    const-string v12, " subtype : "

    #@42d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@430
    move-result-object v11

    #@431
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@434
    move-result-object v11

    #@435
    const-string v12, " wifi state : "

    #@437
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43a
    move-result-object v11

    #@43b
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@43e
    move-result-object v11

    #@43f
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@442
    move-result-object v11

    #@443
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@446
    .line 388
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@448
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$500(Lcom/android/internal/telephony/PayPopup_Korea;)Z

    #@44b
    move-result v10

    #@44c
    const/4 v11, 0x1

    #@44d
    if-ne v10, v11, :cond_4c3

    #@44f
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@451
    iget-object v10, v10, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@453
    const-string v11, "KTBASE"

    #@455
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@458
    move-result v10

    #@459
    if-eqz v10, :cond_4c3

    #@45b
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@45d
    iget-boolean v10, v10, Lcom/android/internal/telephony/PayPopup_Korea;->mStatus:Z

    #@45f
    const/4 v11, 0x1

    #@460
    if-eq v10, v11, :cond_4c3

    #@462
    .line 390
    const/4 v10, 0x1

    #@463
    if-eq v2, v10, :cond_468

    #@465
    const/4 v10, 0x2

    #@466
    if-ne v2, v10, :cond_4cf

    #@468
    .line 391
    :cond_468
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@46a
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;

    #@46d
    move-result-object v10

    #@46e
    invoke-interface {v10}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@471
    move-result-object v10

    #@472
    const v11, 0x209027b

    #@475
    invoke-virtual {v10, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@478
    move-result-object v10

    #@479
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@47c
    move-result-object v9

    #@47d
    .line 403
    .local v9, str_value:Ljava/lang/String;
    :goto_47d
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@47f
    iget-object v10, v10, Lcom/android/internal/telephony/PayPopup_Korea;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@481
    invoke-interface {v10}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getShowKTPayPopup()Z

    #@484
    move-result v10

    #@485
    const/4 v11, 0x1

    #@486
    if-ne v10, v11, :cond_4c3

    #@488
    if-nez v6, :cond_4c3

    #@48a
    .line 404
    new-instance v5, Landroid/content/Intent;

    #@48c
    const-string v10, "lge.intent.action.toast"

    #@48e
    invoke-direct {v5, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@491
    .line 405
    .local v5, intent_kr:Landroid/content/Intent;
    const-string v10, "text"

    #@493
    invoke-virtual {v5, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@496
    .line 406
    const-string v10, "[LGE_DATA][PayPopUp_ko] "

    #@498
    new-instance v11, Ljava/lang/StringBuilder;

    #@49a
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@49d
    const-string v12, "[LGE_DATA] mWifiServiceExt.getShowKTPayPopup() = "

    #@49f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a2
    move-result-object v11

    #@4a3
    iget-object v12, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@4a5
    iget-object v12, v12, Lcom/android/internal/telephony/PayPopup_Korea;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@4a7
    invoke-interface {v12}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getShowKTPayPopup()Z

    #@4aa
    move-result v12

    #@4ab
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4ae
    move-result-object v11

    #@4af
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b2
    move-result-object v11

    #@4b3
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b6
    .line 407
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@4b8
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;

    #@4bb
    move-result-object v10

    #@4bc
    invoke-interface {v10}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@4bf
    move-result-object v10

    #@4c0
    invoke-virtual {v10, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@4c3
    .line 411
    .end local v5           #intent_kr:Landroid/content/Intent;
    .end local v9           #str_value:Ljava/lang/String;
    :cond_4c3
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@4c5
    iget-object v11, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@4c7
    invoke-static {v11}, Lcom/android/internal/telephony/PayPopup_Korea;->access$500(Lcom/android/internal/telephony/PayPopup_Korea;)Z

    #@4ca
    move-result v11

    #@4cb
    iput-boolean v11, v10, Lcom/android/internal/telephony/PayPopup_Korea;->mStatus:Z

    #@4cd
    goto/16 :goto_34

    #@4cf
    .line 392
    :cond_4cf
    const/16 v10, 0xd

    #@4d1
    if-ne v2, v10, :cond_4e9

    #@4d3
    .line 393
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@4d5
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;

    #@4d8
    move-result-object v10

    #@4d9
    invoke-interface {v10}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@4dc
    move-result-object v10

    #@4dd
    const v11, 0x209027c

    #@4e0
    invoke-virtual {v10, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@4e3
    move-result-object v10

    #@4e4
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4e7
    move-result-object v9

    #@4e8
    .restart local v9       #str_value:Ljava/lang/String;
    goto :goto_47d

    #@4e9
    .line 394
    .end local v9           #str_value:Ljava/lang/String;
    :cond_4e9
    const/16 v10, 0xf

    #@4eb
    if-eq v2, v10, :cond_4fc

    #@4ed
    const/16 v10, 0xa

    #@4ef
    if-eq v2, v10, :cond_4fc

    #@4f1
    const/16 v10, 0x9

    #@4f3
    if-eq v2, v10, :cond_4fc

    #@4f5
    const/16 v10, 0x8

    #@4f7
    if-eq v2, v10, :cond_4fc

    #@4f9
    const/4 v10, 0x3

    #@4fa
    if-ne v2, v10, :cond_513

    #@4fc
    .line 397
    :cond_4fc
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@4fe
    invoke-static {v10}, Lcom/android/internal/telephony/PayPopup_Korea;->access$000(Lcom/android/internal/telephony/PayPopup_Korea;)Lcom/android/internal/telephony/Phone;

    #@501
    move-result-object v10

    #@502
    invoke-interface {v10}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@505
    move-result-object v10

    #@506
    const v11, 0x209027a

    #@509
    invoke-virtual {v10, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@50c
    move-result-object v10

    #@50d
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@510
    move-result-object v9

    #@511
    .restart local v9       #str_value:Ljava/lang/String;
    goto/16 :goto_47d

    #@513
    .line 399
    .end local v9           #str_value:Ljava/lang/String;
    :cond_513
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@515
    iget-object v11, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@517
    const/16 v12, 0x258

    #@519
    invoke-virtual {v11, v12}, Lcom/android/internal/telephony/PayPopup_Korea;->obtainMessage(I)Landroid/os/Message;

    #@51c
    move-result-object v11

    #@51d
    const-wide/16 v12, 0x1f4

    #@51f
    invoke-virtual {v10, v11, v12, v13}, Lcom/android/internal/telephony/PayPopup_Korea;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@522
    goto/16 :goto_34

    #@524
    .line 416
    .end local v2           #currentSubType:I
    .end local v4           #info:Landroid/net/NetworkInfo;
    .end local v6           #isWifiConnected:Z
    :cond_524
    const-string v10, "android.intent.action.OTA_USIM_REFRESH_TO_RESET"

    #@526
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@529
    move-result v10

    #@52a
    if-eqz v10, :cond_34

    #@52c
    .line 417
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@52e
    iget-object v10, v10, Lcom/android/internal/telephony/PayPopup_Korea;->featureset:Ljava/lang/String;

    #@530
    const-string v11, "KTBASE"

    #@532
    invoke-static {v10, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@535
    move-result v10

    #@536
    if-eqz v10, :cond_34

    #@538
    .line 418
    iget-object v10, p0, Lcom/android/internal/telephony/PayPopup_Korea$1;->this$0:Lcom/android/internal/telephony/PayPopup_Korea;

    #@53a
    const/4 v11, 0x1

    #@53b
    invoke-static {v10, v11}, Lcom/android/internal/telephony/PayPopup_Korea;->access$702(Lcom/android/internal/telephony/PayPopup_Korea;Z)Z
    :try_end_53e
    .catchall {:try_start_51 .. :try_end_53e} :catchall_4e

    #@53e
    goto/16 :goto_34

    #@540
    .line 371
    :pswitch_data_540
    .packed-switch 0x0
        :pswitch_3a5
    .end packed-switch
.end method
