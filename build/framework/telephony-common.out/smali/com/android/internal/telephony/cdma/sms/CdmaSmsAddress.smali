.class public Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
.super Lcom/android/internal/telephony/SmsAddress;
.source "CdmaSmsAddress.java"


# static fields
.field public static final DIGIT_MODE_4BIT_DTMF:I = 0x0

.field public static final DIGIT_MODE_8BIT_CHAR:I = 0x1

.field public static final NUMBERING_PLAN_ISDN_TELEPHONY:I = 0x1

.field public static final NUMBERING_PLAN_UNKNOWN:I = 0x0

.field public static final NUMBER_MODE_DATA_NETWORK:I = 0x1

.field public static final NUMBER_MODE_NOT_DATA_NETWORK:I = 0x0

.field public static final SMS_ADDRESS_MAX:I = 0x24

.field public static final SMS_SUBADDRESS_MAX:I = 0x24

.field public static final TON_ABBREVIATED:I = 0x6

.field public static final TON_ALPHANUMERIC:I = 0x5

.field public static final TON_INTERNATIONAL_OR_IP:I = 0x1

.field public static final TON_NATIONAL_OR_EMAIL:I = 0x2

.field public static final TON_NETWORK:I = 0x3

.field public static final TON_RESERVED:I = 0x7

.field public static final TON_SUBSCRIBER:I = 0x4

.field public static final TON_UNKNOWN:I

.field private static final numericCharDialableMap:Landroid/util/SparseBooleanArray;

.field private static final numericCharsDialable:[C

.field private static final numericCharsSugar:[C


# instance fields
.field public digitMode:I

.field public numberMode:I

.field public numberOfDigits:I

.field public numberPlan:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 139
    const/16 v1, 0xc

    #@2
    new-array v1, v1, [C

    #@4
    fill-array-data v1, :array_48

    #@7
    sput-object v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharsDialable:[C

    #@9
    .line 143
    const/16 v1, 0x8

    #@b
    new-array v1, v1, [C

    #@d
    fill-array-data v1, :array_58

    #@10
    sput-object v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharsSugar:[C

    #@12
    .line 147
    new-instance v1, Landroid/util/SparseBooleanArray;

    #@14
    sget-object v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharsDialable:[C

    #@16
    array-length v2, v2

    #@17
    sget-object v3, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharsSugar:[C

    #@19
    array-length v3, v3

    #@1a
    add-int/2addr v2, v3

    #@1b
    invoke-direct {v1, v2}, Landroid/util/SparseBooleanArray;-><init>(I)V

    #@1e
    sput-object v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharDialableMap:Landroid/util/SparseBooleanArray;

    #@20
    .line 150
    const/4 v0, 0x0

    #@21
    .local v0, i:I
    :goto_21
    sget-object v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharsDialable:[C

    #@23
    array-length v1, v1

    #@24
    if-ge v0, v1, :cond_33

    #@26
    .line 151
    sget-object v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharDialableMap:Landroid/util/SparseBooleanArray;

    #@28
    sget-object v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharsDialable:[C

    #@2a
    aget-char v2, v2, v0

    #@2c
    const/4 v3, 0x1

    #@2d
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@30
    .line 150
    add-int/lit8 v0, v0, 0x1

    #@32
    goto :goto_21

    #@33
    .line 153
    :cond_33
    const/4 v0, 0x0

    #@34
    :goto_34
    sget-object v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharsSugar:[C

    #@36
    array-length v1, v1

    #@37
    if-ge v0, v1, :cond_46

    #@39
    .line 154
    sget-object v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharDialableMap:Landroid/util/SparseBooleanArray;

    #@3b
    sget-object v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharsSugar:[C

    #@3d
    aget-char v2, v2, v0

    #@3f
    const/4 v3, 0x0

    #@40
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@43
    .line 153
    add-int/lit8 v0, v0, 0x1

    #@45
    goto :goto_34

    #@46
    .line 156
    :cond_46
    return-void

    #@47
    .line 139
    nop

    #@48
    :array_48
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
        0x2at 0x0t
        0x23t 0x0t
    .end array-data

    #@58
    .line 143
    :array_58
    .array-data 0x2
        0x28t 0x0t
        0x29t 0x0t
        0x20t 0x0t
        0x2dt 0x0t
        0x2bt 0x0t
        0x2et 0x0t
        0x2ft 0x0t
        0x5ct 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 99
    invoke-direct {p0}, Lcom/android/internal/telephony/SmsAddress;-><init>()V

    #@3
    .line 100
    return-void
.end method

.method private static filterNumericSugar(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "address"

    #@0
    .prologue
    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 166
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@8
    move-result v3

    #@9
    .line 167
    .local v3, len:I
    const/4 v2, 0x0

    #@a
    .local v2, i:I
    :goto_a
    if-ge v2, v3, :cond_29

    #@c
    .line 168
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v1

    #@10
    .line 169
    .local v1, c:C
    sget-object v5, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharDialableMap:Landroid/util/SparseBooleanArray;

    #@12
    invoke-virtual {v5, v1}, Landroid/util/SparseBooleanArray;->indexOfKey(I)I

    #@15
    move-result v4

    #@16
    .line 170
    .local v4, mapIndex:I
    if-gez v4, :cond_1a

    #@18
    const/4 v5, 0x0

    #@19
    .line 174
    .end local v1           #c:C
    .end local v4           #mapIndex:I
    :goto_19
    return-object v5

    #@1a
    .line 171
    .restart local v1       #c:C
    .restart local v4       #mapIndex:I
    :cond_1a
    sget-object v5, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numericCharDialableMap:Landroid/util/SparseBooleanArray;

    #@1c
    invoke-virtual {v5, v4}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@1f
    move-result v5

    #@20
    if-nez v5, :cond_25

    #@22
    .line 167
    :goto_22
    add-int/lit8 v2, v2, 0x1

    #@24
    goto :goto_a

    #@25
    .line 172
    :cond_25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@28
    goto :goto_22

    #@29
    .line 174
    .end local v1           #c:C
    .end local v4           #mapIndex:I
    :cond_29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    goto :goto_19
.end method

.method private static filterWhitespace(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "address"

    #@0
    .prologue
    .line 182
    if-nez p0, :cond_4

    #@2
    .line 183
    const/4 v4, 0x0

    #@3
    .line 192
    :goto_3
    return-object v4

    #@4
    .line 185
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    .line 186
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@c
    move-result v3

    #@d
    .line 187
    .local v3, len:I
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    if-ge v2, v3, :cond_2b

    #@10
    .line 188
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@13
    move-result v1

    #@14
    .line 189
    .local v1, c:C
    const/16 v4, 0x20

    #@16
    if-eq v1, v4, :cond_24

    #@18
    const/16 v4, 0xd

    #@1a
    if-eq v1, v4, :cond_24

    #@1c
    const/16 v4, 0xa

    #@1e
    if-eq v1, v4, :cond_24

    #@20
    const/16 v4, 0x9

    #@22
    if-ne v1, v4, :cond_27

    #@24
    .line 187
    :cond_24
    :goto_24
    add-int/lit8 v2, v2, 0x1

    #@26
    goto :goto_e

    #@27
    .line 190
    :cond_27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2a
    goto :goto_24

    #@2b
    .line 192
    .end local v1           #c:C
    :cond_2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    goto :goto_3
.end method

.method public static parse(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    .registers 10
    .parameter "address"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v8, -0x1

    #@2
    const/4 v7, 0x0

    #@3
    const/4 v6, 0x1

    #@4
    .line 207
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isUserMode()Z

    #@7
    move-result v4

    #@8
    if-nez v4, :cond_20

    #@a
    .line 208
    new-instance v4, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v5, "parse(), address = "

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@20
    .line 212
    :cond_20
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@22
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;-><init>()V

    #@25
    .line 216
    .local v0, addr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    iput v7, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@27
    .line 217
    iput v7, v0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@29
    .line 218
    iput v7, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@2b
    .line 219
    iput v7, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@2d
    .line 220
    const/4 v2, 0x0

    #@2e
    .line 222
    .local v2, origBytes:[B
    if-nez p0, :cond_32

    #@30
    move-object v0, v3

    #@31
    .line 293
    .end local v0           #addr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    :cond_31
    :goto_31
    return-object v0

    #@32
    .line 226
    .restart local v0       #addr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    :cond_32
    const/16 v4, 0x2b

    #@34
    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    #@37
    move-result v4

    #@38
    if-eq v4, v8, :cond_42

    #@3a
    .line 228
    iput v6, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@3c
    .line 229
    iput v6, v0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@3e
    .line 230
    iput v7, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@40
    .line 231
    iput v6, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@42
    .line 233
    :cond_42
    const/16 v4, 0x40

    #@44
    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    #@47
    move-result v4

    #@48
    if-eq v4, v8, :cond_51

    #@4a
    .line 235
    iput v6, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@4c
    .line 236
    const/4 v4, 0x2

    #@4d
    iput v4, v0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@4f
    .line 237
    iput v6, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@51
    .line 241
    :cond_51
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->filterNumericSugar(Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    .line 244
    .local v1, filteredAddr:Ljava/lang/String;
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isUserMode()Z

    #@58
    move-result v4

    #@59
    if-nez v4, :cond_71

    #@5b
    .line 245
    new-instance v4, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v5, "parse(), filteredAddr = "

    #@62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v4

    #@6e
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@71
    .line 249
    :cond_71
    iget v4, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@73
    if-nez v4, :cond_a0

    #@75
    .line 250
    const-string v4, "parse(), DIGIT_MODE_4BIT_DTMF"

    #@77
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@7a
    .line 251
    if-eqz v1, :cond_80

    #@7c
    .line 252
    invoke-static {v1}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->parseToDtmf(Ljava/lang/String;)[B

    #@7f
    move-result-object v2

    #@80
    .line 256
    :cond_80
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isUserMode()Z

    #@83
    move-result v4

    #@84
    if-nez v4, :cond_9c

    #@86
    .line 257
    new-instance v4, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v5, "parse(), filteredAddr = "

    #@8d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v4

    #@99
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@9c
    .line 261
    :cond_9c
    if-nez v2, :cond_a0

    #@9e
    .line 263
    iput v6, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@a0
    .line 267
    :cond_a0
    iget v4, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@a2
    if-ne v4, v6, :cond_bc

    #@a4
    .line 268
    const-string v4, "parse(), DIGIT_MODE_8BIT_CHAR"

    #@a6
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@a9
    .line 269
    invoke-static {v1}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->filterWhitespace(Ljava/lang/String;)Ljava/lang/String;

    #@ac
    move-result-object v1

    #@ad
    .line 270
    if-nez v1, :cond_b3

    #@af
    .line 271
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->filterWhitespace(Ljava/lang/String;)Ljava/lang/String;

    #@b2
    move-result-object v1

    #@b3
    .line 273
    :cond_b3
    invoke-static {v1}, Lcom/android/internal/telephony/cdma/sms/UserData;->stringToAscii(Ljava/lang/String;)[B

    #@b6
    move-result-object v2

    #@b7
    .line 274
    if-nez v2, :cond_bc

    #@b9
    move-object v0, v3

    #@ba
    .line 275
    goto/16 :goto_31

    #@bc
    .line 279
    :cond_bc
    iput-object v1, v0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@be
    .line 281
    iput-object v2, v0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@c0
    .line 282
    array-length v3, v2

    #@c1
    iput v3, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@c3
    .line 285
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isUserMode()Z

    #@c6
    move-result v3

    #@c7
    if-nez v3, :cond_31

    #@c9
    .line 287
    new-instance v3, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    const-string v4, "parse(), addr.address = "

    #@d0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v3

    #@d4
    iget-object v4, v0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@d6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v3

    #@da
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v3

    #@de
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@e1
    .line 289
    new-instance v3, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v4, "parse(), addr.numberOfDigits = "

    #@e8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v3

    #@ec
    iget v4, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@ee
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v3

    #@f2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f5
    move-result-object v3

    #@f6
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@f9
    goto/16 :goto_31
.end method

.method private static parseToDtmf(Ljava/lang/String;)[B
    .registers 7
    .parameter "address"

    #@0
    .prologue
    .line 124
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v1

    #@4
    .line 125
    .local v1, digits:I
    new-array v3, v1, [B

    #@6
    .line 126
    .local v3, result:[B
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v1, :cond_34

    #@9
    .line 127
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v0

    #@d
    .line 128
    .local v0, c:C
    const/4 v4, 0x0

    #@e
    .line 129
    .local v4, val:I
    const/16 v5, 0x31

    #@10
    if-lt v0, v5, :cond_1e

    #@12
    const/16 v5, 0x39

    #@14
    if-gt v0, v5, :cond_1e

    #@16
    add-int/lit8 v4, v0, -0x30

    #@18
    .line 134
    :goto_18
    int-to-byte v5, v4

    #@19
    aput-byte v5, v3, v2

    #@1b
    .line 126
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_7

    #@1e
    .line 130
    :cond_1e
    const/16 v5, 0x30

    #@20
    if-ne v0, v5, :cond_25

    #@22
    const/16 v4, 0xa

    #@24
    goto :goto_18

    #@25
    .line 131
    :cond_25
    const/16 v5, 0x2a

    #@27
    if-ne v0, v5, :cond_2c

    #@29
    const/16 v4, 0xb

    #@2b
    goto :goto_18

    #@2c
    .line 132
    :cond_2c
    const/16 v5, 0x23

    #@2e
    if-ne v0, v5, :cond_33

    #@30
    const/16 v4, 0xc

    #@32
    goto :goto_18

    #@33
    .line 133
    :cond_33
    const/4 v3, 0x0

    #@34
    .line 136
    .end local v0           #c:C
    .end local v3           #result:[B
    .end local v4           #val:I
    :cond_34
    return-object v3
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 105
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "CdmaSmsAddress "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 106
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "{ digitMode="

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v2, ", numberMode="

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    .line 108
    new-instance v1, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v2, ", numberPlan="

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    .line 109
    new-instance v1, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v2, ", numberOfDigits="

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    iget v2, p0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v2, ", ton="

    #@71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    iget v2, p0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v1

    #@7f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v2, ", address=\""

    #@89
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v1

    #@8d
    iget-object v2, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@8f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v1

    #@93
    const-string v2, "\""

    #@95
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v1

    #@99
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v1

    #@9d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v2, ", origBytes="

    #@a7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v1

    #@ab
    iget-object v2, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@ad
    invoke-static {v2}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@b0
    move-result-object v2

    #@b1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v1

    #@b5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v1

    #@b9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    .line 113
    const-string v1, " }"

    #@be
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    .line 114
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v1

    #@c5
    return-object v1
.end method
