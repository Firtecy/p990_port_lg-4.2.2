.class public Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;
.super Ljava/lang/Object;
.source "LgeBluetoothSimManager.java"


# static fields
.field private static final ACTION_SAP_REQUEST:Ljava/lang/String; = "com.lge.bluetooth.sap.ACTION_SAP_REQUEST"

.field private static final ACTION_SAP_RESPONSE:Ljava/lang/String; = "com.lge.bluetooth.sap.ACTION_SAP_RESPONSE"

.field private static final BTSC_CARD_STATUS_INSERTED:I = 0x4

.field private static final CARD_STATUS_NOT_ACCESSIBLE:I = 0x2

.field private static final CARD_STATUS_RECOVERED:I = 0x5

.field private static final CARD_STATUS_REMOVED:I = 0x3

.field private static final CARD_STATUS_RESET:I = 0x1

.field private static final CARD_STATUS_UNKNOWN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final DBG:Z = false

.field private static final QCT_SAP_CONNECTION_CHECK_STATUS:I = 0x2

.field private static final QCT_SAP_CONNECTION_CONNECT:I = 0x1

.field private static final QCT_SAP_CONNECTION_DISCONNECT:I = 0x0

.field private static final QCT_SAP_REQUEST_OP_GET_ATR:I = 0x0

.field private static final QCT_SAP_REQUEST_OP_POWER_SIM_OFF:I = 0x2

.field private static final QCT_SAP_REQUEST_OP_POWER_SIM_ON:I = 0x3

.field private static final QCT_SAP_REQUEST_OP_READER_STATUS:I = 0x5

.field private static final QCT_SAP_REQUEST_OP_RESET_SIM:I = 0x4

.field private static final QCT_SAP_REQUEST_OP_SEND_APDU:I = 0x1

.field private static final RESULT_OK:I = 0x0

.field private static final RIL_FOR_2CHIP:Z = false

.field private static final RIL_REQ_SAP_CONNECTION_DONE:I = 0xc8

.field private static final RIL_REQ_SAP_DONE:I = 0xc9

.field private static final SAP_DISABLE_EVT:I = 0xb

.field private static final SAP_ENABLE_EVT:I = 0xa

.field private static final SIM_APDU_RESP:I = 0x5

.field private static final SIM_ATR_RESP:I = 0x4

.field private static final SIM_CARD_READER_STATUS_EVT:I = 0x7

.field private static final SIM_CARD_READER_STATUS_RESP:I = 0x7

.field private static final SIM_CARD_STATUS_RESP:I = 0x6

.field private static final SIM_CLOSE_EVT:I = 0x1

.field private static final SIM_CLOSE_RESP:I = 0xb

.field private static final SIM_DISCONNECT_RESP:I = 0x0

.field private static final SIM_OPEN_EVT:I = 0x0

.field private static final SIM_OPEN_RESP:I = 0xa

.field private static final SIM_POWER_OFF_EVT:I = 0x4

.field private static final SIM_POWER_OFF_RESP:I = 0x2

.field private static final SIM_POWER_ON_EVT:I = 0x3

.field private static final SIM_POWER_ON_RESP:I = 0x1

.field private static final SIM_RESET_EVT:I = 0x2

.field private static final SIM_RESET_RESP:I = 0x3

.field private static final SIM_TRANSFER_APDU_EVT:I = 0x6

.field private static final SIM_TRANSFER_ATR_EVT:I = 0x5

.field private static final TAG:Ljava/lang/String; = "LgeBluetoothSimManager"

.field private static mContext:Landroid/content/Context;

.field private static mIntentFilter:Landroid/content/IntentFilter;


# instance fields
.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCM:Lcom/android/internal/telephony/CommandsInterface;

.field private mHandler:Landroid/os/Handler;

.field private mReqType:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 32
    const-string v0, "persist.service.sap.debug"

    #@2
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    const-string v1, "1"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    sput-boolean v0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->DBG:Z

    #@e
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/gsm/GSMPhone;)V
    .registers 6
    .parameter "context"
    .parameter "phone"

    #@0
    .prologue
    .line 94
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 104
    new-instance v0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;-><init>(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@a
    .line 333
    new-instance v0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$2;

    #@c
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$2;-><init>(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)V

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mHandler:Landroid/os/Handler;

    #@11
    .line 95
    sput-object p1, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mContext:Landroid/content/Context;

    #@13
    .line 96
    iget-object v0, p2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@15
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    .line 98
    new-instance v0, Landroid/content/IntentFilter;

    #@19
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@1c
    sput-object v0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mIntentFilter:Landroid/content/IntentFilter;

    #@1e
    .line 99
    sget-object v0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mIntentFilter:Landroid/content/IntentFilter;

    #@20
    const-string v1, "com.lge.bluetooth.sap.ACTION_SAP_REQUEST"

    #@22
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@25
    .line 100
    sget-object v0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mContext:Landroid/content/Context;

    #@27
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@29
    sget-object v2, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mIntentFilter:Landroid/content/IntentFilter;

    #@2b
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2e
    .line 101
    return-void
.end method

.method private static ByteArrayToString([BI)Ljava/lang/String;
    .registers 6
    .parameter "b"
    .parameter "start"

    #@0
    .prologue
    .line 222
    new-instance v1, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 223
    .local v1, s:Ljava/lang/StringBuffer;
    move v0, p1

    #@6
    .local v0, i:I
    :goto_6
    array-length v2, p0

    #@7
    if-ge v0, v2, :cond_1e

    #@9
    .line 224
    aget-byte v2, p0, v0

    #@b
    and-int/lit16 v2, v2, 0xff

    #@d
    add-int/lit16 v2, v2, 0x100

    #@f
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    const/4 v3, 0x1

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1b
    .line 223
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_6

    #@1e
    .line 226
    :cond_1e
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    return-object v2
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget v0, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mReqType:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    iput p1, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mReqType:I

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Lcom/android/internal/telephony/CommandsInterface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;[BILandroid/os/Message;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->sendSapRequestToSim([BILandroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$400()Z
    .registers 1

    #@0
    .prologue
    .line 30
    sget-boolean v0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->DBG:Z

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->onGetRilResponse(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method private onGetRilResponse(Landroid/os/Message;)V
    .registers 14
    .parameter "msg"

    #@0
    .prologue
    const/4 v11, 0x7

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    .line 248
    const/4 v10, 0x0

    #@5
    .line 250
    .local v10, ril_resp:Ljava/lang/String;
    iget v0, p1, Landroid/os/Message;->what:I

    #@7
    packed-switch v0, :pswitch_data_180

    #@a
    .line 320
    :goto_a
    return-void

    #@b
    .line 253
    :pswitch_b
    iget v0, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mReqType:I

    #@d
    if-nez v0, :cond_1f

    #@f
    .line 254
    const-string v0, "LgeBluetoothSimManager"

    #@11
    const-string v1, "[BTUI] ### SIM_OPEN_RESP"

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 255
    const/16 v1, 0xa

    #@18
    move-object v0, p0

    #@19
    move v3, v2

    #@1a
    move v5, v2

    #@1b
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->onSapResponseToManager(IIILjava/lang/String;I)V

    #@1e
    goto :goto_a

    #@1f
    .line 257
    :cond_1f
    const-string v0, "LgeBluetoothSimManager"

    #@21
    const-string v1, "[BTUI] ### SIM_CLOSE_RESP"

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 258
    const/16 v1, 0xb

    #@28
    move-object v0, p0

    #@29
    move v3, v2

    #@2a
    move v5, v2

    #@2b
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->onSapResponseToManager(IIILjava/lang/String;I)V

    #@2e
    goto :goto_a

    #@2f
    .line 263
    :pswitch_2f
    const-string v0, "LgeBluetoothSimManager"

    #@31
    const-string v3, "[BTUI] RIL_REQ_SAP_DONE"

    #@33
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 264
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@38
    check-cast v7, Landroid/os/AsyncResult;

    #@3a
    .line 265
    .local v7, ar:Landroid/os/AsyncResult;
    iget-object v0, v7, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3c
    if-eqz v0, :cond_5d

    #@3e
    .line 266
    const-string v0, "LgeBluetoothSimManager"

    #@40
    new-instance v1, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v2, "[BTUI] exception happens : "

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    iget-object v2, v7, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4d
    invoke-virtual {v2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    goto :goto_a

    #@5d
    .line 267
    :cond_5d
    iget-object v0, v7, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@5f
    if-nez v0, :cond_69

    #@61
    .line 268
    const-string v0, "LgeBluetoothSimManager"

    #@63
    const-string v1, "[BTUI] result is vacant"

    #@65
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    goto :goto_a

    #@69
    .line 270
    :cond_69
    iget-object v0, v7, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@6b
    check-cast v0, [Ljava/lang/String;

    #@6d
    move-object v6, v0

    #@6e
    check-cast v6, [Ljava/lang/String;

    #@70
    .line 271
    .local v6, apdu_rsp:[Ljava/lang/String;
    aget-object v0, v6, v2

    #@72
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@75
    move-result v8

    #@76
    .line 272
    .local v8, op_type:I
    aget-object v9, v6, v1

    #@78
    .line 273
    .local v9, payload:Ljava/lang/String;
    iget v0, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mReqType:I

    #@7a
    packed-switch v0, :pswitch_data_188

    #@7d
    goto :goto_a

    #@7e
    .line 275
    :pswitch_7e
    const-string v0, "LgeBluetoothSimManager"

    #@80
    const-string v1, "[BTUI] ### SIM_RESET_RESP"

    #@82
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    .line 276
    const/4 v1, 0x3

    #@86
    move-object v0, p0

    #@87
    move v3, v2

    #@88
    move v5, v2

    #@89
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->onSapResponseToManager(IIILjava/lang/String;I)V

    #@8c
    goto/16 :goto_a

    #@8e
    .line 279
    :pswitch_8e
    const-string v0, "LgeBluetoothSimManager"

    #@90
    const-string v3, "[BTUI] ### SIM_POWER_ON_RESP"

    #@92
    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    move-object v0, p0

    #@96
    move v3, v2

    #@97
    move v5, v2

    #@98
    .line 280
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->onSapResponseToManager(IIILjava/lang/String;I)V

    #@9b
    goto/16 :goto_a

    #@9d
    .line 283
    :pswitch_9d
    const-string v0, "LgeBluetoothSimManager"

    #@9f
    const-string v1, "[BTUI] ### SIM_POWER_OFF_RESP"

    #@a1
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 284
    const/4 v1, 0x2

    #@a5
    move-object v0, p0

    #@a6
    move v3, v2

    #@a7
    move v5, v2

    #@a8
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->onSapResponseToManager(IIILjava/lang/String;I)V

    #@ab
    goto/16 :goto_a

    #@ad
    .line 287
    :pswitch_ad
    if-eqz v9, :cond_e8

    #@af
    .line 288
    const-string v0, "LgeBluetoothSimManager"

    #@b1
    new-instance v1, Ljava/lang/StringBuilder;

    #@b3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b6
    const-string v3, "[BTUI] ### SIM_ATR_RESP = "

    #@b8
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v1

    #@bc
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v1

    #@c0
    const-string v3, " (len: "

    #@c2
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v1

    #@c6
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@c9
    move-result v3

    #@ca
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v1

    #@ce
    const-string v3, ")"

    #@d0
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v1

    #@d4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v1

    #@d8
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@db
    .line 289
    const/4 v1, 0x4

    #@dc
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@df
    move-result v5

    #@e0
    move-object v0, p0

    #@e1
    move v3, v2

    #@e2
    move-object v4, v9

    #@e3
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->onSapResponseToManager(IIILjava/lang/String;I)V

    #@e6
    goto/16 :goto_a

    #@e8
    .line 291
    :cond_e8
    const-string v0, "LgeBluetoothSimManager"

    #@ea
    const-string v1, "[BTUI] ### SIM_ATR_RESP : playload is null"

    #@ec
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    goto/16 :goto_a

    #@f1
    .line 295
    :pswitch_f1
    if-eqz v9, :cond_12c

    #@f3
    .line 296
    const-string v0, "LgeBluetoothSimManager"

    #@f5
    new-instance v1, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v3, "[BTUI] ### SIM_APDU_RESP = "

    #@fc
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v1

    #@100
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v1

    #@104
    const-string v3, " (len: "

    #@106
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v1

    #@10a
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@10d
    move-result v3

    #@10e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@111
    move-result-object v1

    #@112
    const-string v3, ")"

    #@114
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v1

    #@118
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11b
    move-result-object v1

    #@11c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11f
    .line 297
    const/4 v1, 0x5

    #@120
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@123
    move-result v5

    #@124
    move-object v0, p0

    #@125
    move v3, v2

    #@126
    move-object v4, v9

    #@127
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->onSapResponseToManager(IIILjava/lang/String;I)V

    #@12a
    goto/16 :goto_a

    #@12c
    .line 299
    :cond_12c
    const-string v0, "LgeBluetoothSimManager"

    #@12e
    const-string v1, "[BTUI] ### SIM_APDU_RESP : playload is null"

    #@130
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    goto/16 :goto_a

    #@135
    .line 303
    :pswitch_135
    if-eqz v9, :cond_170

    #@137
    .line 304
    const-string v0, "LgeBluetoothSimManager"

    #@139
    new-instance v1, Ljava/lang/StringBuilder;

    #@13b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13e
    const-string v3, "[BTUI] ### SIM_CARD_READER_STATUS_RESP = "

    #@140
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v1

    #@144
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v1

    #@148
    const-string v3, " (len: "

    #@14a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v1

    #@14e
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@151
    move-result v3

    #@152
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@155
    move-result-object v1

    #@156
    const-string v3, ")"

    #@158
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v1

    #@15c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15f
    move-result-object v1

    #@160
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@163
    .line 305
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@166
    move-result v5

    #@167
    move-object v0, p0

    #@168
    move v1, v11

    #@169
    move v3, v2

    #@16a
    move-object v4, v9

    #@16b
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->onSapResponseToManager(IIILjava/lang/String;I)V

    #@16e
    goto/16 :goto_a

    #@170
    .line 307
    :cond_170
    const-string v0, "LgeBluetoothSimManager"

    #@172
    const-string v1, "[BTUI] ### SIM_CARD_READER_STATUS_RESP : playload is null"

    #@174
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@177
    move-object v0, p0

    #@178
    move v1, v11

    #@179
    move v3, v2

    #@17a
    move v5, v2

    #@17b
    .line 308
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->onSapResponseToManager(IIILjava/lang/String;I)V

    #@17e
    goto/16 :goto_a

    #@180
    .line 250
    :pswitch_data_180
    .packed-switch 0xc8
        :pswitch_b
        :pswitch_2f
    .end packed-switch

    #@188
    .line 273
    :pswitch_data_188
    .packed-switch 0x2
        :pswitch_7e
        :pswitch_8e
        :pswitch_9d
        :pswitch_ad
        :pswitch_f1
        :pswitch_135
    .end packed-switch
.end method

.method private static parseChannelNumber(B)I
    .registers 3
    .parameter "cla"

    #@0
    .prologue
    .line 231
    and-int/lit8 v1, p0, 0x40

    #@2
    if-nez v1, :cond_a

    #@4
    const/4 v0, 0x1

    #@5
    .line 233
    .local v0, isFirstInterindustryClassByteCoding:Z
    :goto_5
    if-eqz v0, :cond_c

    #@7
    .line 236
    and-int/lit8 v1, p0, 0x3

    #@9
    .line 240
    :goto_9
    return v1

    #@a
    .line 231
    .end local v0           #isFirstInterindustryClassByteCoding:Z
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_5

    #@c
    .line 240
    .restart local v0       #isFirstInterindustryClassByteCoding:Z
    :cond_c
    and-int/lit8 v1, p0, 0xf

    #@e
    add-int/lit8 v1, v1, 0x4

    #@10
    goto :goto_9
.end method

.method private sendSapRequestToSim([BILandroid/os/Message;)V
    .registers 8
    .parameter "req_byte"
    .parameter "op_type"
    .parameter "msg"

    #@0
    .prologue
    .line 201
    const-string v1, "LgeBluetoothSimManager"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "[BTUI] sendSapRequestToSim: op_type = "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 203
    const/4 v1, 0x1

    #@19
    if-ne p2, v1, :cond_54

    #@1b
    .line 204
    const/4 v0, 0x0

    #@1c
    .line 205
    .local v0, input_data:Ljava/lang/String;
    if-eqz p1, :cond_4c

    #@1e
    array-length v1, p1

    #@1f
    if-lez v1, :cond_4c

    #@21
    .line 206
    const/4 v1, 0x0

    #@22
    invoke-static {p1, v1}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->ByteArrayToString([BI)Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    .line 207
    const-string v1, "LgeBluetoothSimManager"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "[BTUI] sendSapRequestToSim: input_data = "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 208
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@40
    if-eqz v1, :cond_4b

    #@42
    if-eqz p3, :cond_4b

    #@44
    if-eqz v0, :cond_4b

    #@46
    .line 209
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@48
    invoke-interface {v1, p2, v0, p3}, Lcom/android/internal/telephony/CommandsInterface;->SAPrequest(ILjava/lang/String;Landroid/os/Message;)V

    #@4b
    .line 219
    .end local v0           #input_data:Ljava/lang/String;
    :cond_4b
    :goto_4b
    return-void

    #@4c
    .line 212
    .restart local v0       #input_data:Ljava/lang/String;
    :cond_4c
    const-string v1, "LgeBluetoothSimManager"

    #@4e
    const-string v2, "[BTUI] sendSapRequestToSim: req_byte is null or length is 0"

    #@50
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    goto :goto_4b

    #@54
    .line 215
    .end local v0           #input_data:Ljava/lang/String;
    :cond_54
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@56
    if-eqz v1, :cond_4b

    #@58
    if-eqz p3, :cond_4b

    #@5a
    .line 216
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5c
    const/4 v2, 0x0

    #@5d
    invoke-interface {v1, p2, v2, p3}, Lcom/android/internal/telephony/CommandsInterface;->SAPrequest(ILjava/lang/String;Landroid/os/Message;)V

    #@60
    goto :goto_4b
.end method


# virtual methods
.method public onSapResponseToManager(IIILjava/lang/String;I)V
    .registers 8
    .parameter "resp_type"
    .parameter "result_code"
    .parameter "is_apdu_7816"
    .parameter "resp_data"
    .parameter "resp_len"

    #@0
    .prologue
    .line 324
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "com.lge.bluetooth.sap.ACTION_SAP_RESPONSE"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 325
    .local v0, sapRespIntent:Landroid/content/Intent;
    const-string v1, "resp_type"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@c
    .line 326
    const-string v1, "result_code"

    #@e
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@11
    .line 327
    const-string v1, "is_apdu_7816"

    #@13
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@16
    .line 328
    const-string v1, "resp_data"

    #@18
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1b
    .line 329
    const-string v1, "resp_len"

    #@1d
    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@20
    .line 330
    sget-object v1, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->mContext:Landroid/content/Context;

    #@22
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@25
    .line 331
    return-void
.end method
