.class Lcom/android/internal/telephony/LGEDataConnectionTracker$1;
.super Landroid/content/BroadcastReceiver;
.source "LGEDataConnectionTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGEDataConnectionTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/LGEDataConnectionTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 246
    iput-object p1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 30
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 250
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v5

    #@4
    .line 251
    .local v5, action:Ljava/lang/String;
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@6
    new-instance v24, Ljava/lang/StringBuilder;

    #@8
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v25, "onReceive: action="

    #@d
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v24

    #@11
    move-object/from16 v0, v24

    #@13
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v24

    #@17
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v24

    #@1b
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 252
    const-string v23, "android.intent.action.SCREEN_ON"

    #@20
    move-object/from16 v0, v23

    #@22
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v23

    #@26
    if-eqz v23, :cond_29

    #@28
    .line 634
    :cond_28
    :goto_28
    :pswitch_28
    return-void

    #@29
    .line 255
    :cond_29
    const-string v23, "lge.test.limit_data_usage"

    #@2b
    move-object/from16 v0, v23

    #@2d
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v23

    #@31
    if-eqz v23, :cond_80

    #@33
    .line 256
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@35
    const-string v24, "[LGE_DATA] lge.test.limit_data_usage"

    #@37
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 258
    const-string v23, "cause"

    #@3c
    move-object/from16 v0, p2

    #@3e
    move-object/from16 v1, v23

    #@40
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@43
    move-result-object v23

    #@44
    const-string v24, "2"

    #@46
    invoke-virtual/range {v23 .. v24}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v23

    #@4a
    if-eqz v23, :cond_66

    #@4c
    .line 259
    move-object/from16 v0, p0

    #@4e
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@50
    move-object/from16 v23, v0

    #@52
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@55
    move-result-object v23

    #@56
    move-object/from16 v0, v23

    #@58
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@5a
    move-object/from16 v23, v0

    #@5c
    sget-object v24, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->handleSKT_QA:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@5e
    const-string v25, ""

    #@60
    const/16 v26, 0x2

    #@62
    invoke-virtual/range {v23 .. v26}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@65
    goto :goto_28

    #@66
    .line 261
    :cond_66
    move-object/from16 v0, p0

    #@68
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@6a
    move-object/from16 v23, v0

    #@6c
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@6f
    move-result-object v23

    #@70
    move-object/from16 v0, v23

    #@72
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@74
    move-object/from16 v23, v0

    #@76
    sget-object v24, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->handleSKT_QA:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@78
    const-string v25, ""

    #@7a
    const/16 v26, 0x5

    #@7c
    invoke-virtual/range {v23 .. v26}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@7f
    goto :goto_28

    #@80
    .line 265
    :cond_80
    const-string v23, "com.skt.CALL_PROTECTION_STATUS_CHANGED"

    #@82
    move-object/from16 v0, v23

    #@84
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@87
    move-result v23

    #@88
    if-eqz v23, :cond_121

    #@8a
    .line 267
    move-object/from16 v0, p0

    #@8c
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@8e
    move-object/from16 v23, v0

    #@90
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@93
    move-result-object v23

    #@94
    move-object/from16 v0, v23

    #@96
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@98
    move-object/from16 v23, v0

    #@9a
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@9d
    move-result-object v23

    #@9e
    move-object/from16 v0, v23

    #@a0
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@a2
    move/from16 v23, v0

    #@a4
    const/16 v24, 0x1

    #@a6
    move/from16 v0, v23

    #@a8
    move/from16 v1, v24

    #@aa
    if-ne v0, v1, :cond_28

    #@ac
    .line 269
    const-string v23, "on_off"

    #@ae
    const/16 v24, 0x1

    #@b0
    move-object/from16 v0, p2

    #@b2
    move-object/from16 v1, v23

    #@b4
    move/from16 v2, v24

    #@b6
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@b9
    move-result v12

    #@ba
    .line 271
    .local v12, enabled:Z
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@bc
    new-instance v24, Ljava/lang/StringBuilder;

    #@be
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v25, "[LGE_DATA] CALL_PROTECTION_STATUS_CHANGED ::"

    #@c3
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v24

    #@c7
    move-object/from16 v0, v24

    #@c9
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v24

    #@cd
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v24

    #@d1
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    .line 273
    if-nez v12, :cond_106

    #@d6
    .line 275
    move-object/from16 v0, p0

    #@d8
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@da
    move-object/from16 v23, v0

    #@dc
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@df
    move-result-object v23

    #@e0
    move-object/from16 v0, v23

    #@e2
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@e4
    move-object/from16 v23, v0

    #@e6
    sget-object v24, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@e8
    const-string v25, ""

    #@ea
    const/16 v26, 0x0

    #@ec
    invoke-virtual/range {v23 .. v26}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@ef
    .line 277
    move-object/from16 v0, p0

    #@f1
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@f3
    move-object/from16 v23, v0

    #@f5
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@f8
    move-result-object v23

    #@f9
    move-object/from16 v0, v23

    #@fb
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@fd
    move-object/from16 v23, v0

    #@ff
    const/16 v24, 0x0

    #@101
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@104
    goto/16 :goto_28

    #@106
    .line 280
    :cond_106
    move-object/from16 v0, p0

    #@108
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@10a
    move-object/from16 v23, v0

    #@10c
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@10f
    move-result-object v23

    #@110
    move-object/from16 v0, v23

    #@112
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@114
    move-object/from16 v23, v0

    #@116
    sget-object v24, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@118
    const-string v25, ""

    #@11a
    const/16 v26, 0x1

    #@11c
    invoke-virtual/range {v23 .. v26}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@11f
    goto/16 :goto_28

    #@121
    .line 283
    .end local v12           #enabled:Z
    :cond_121
    const-string v23, "com.skt.CALL_PROTECTION_MENU_OFF"

    #@123
    move-object/from16 v0, v23

    #@125
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@128
    move-result v23

    #@129
    if-eqz v23, :cond_184

    #@12b
    .line 287
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@12d
    const-string v24, "[LGE_DATA] com.skt.CALL_PROTECTION_MENU_OFF"

    #@12f
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@132
    .line 289
    move-object/from16 v0, p0

    #@134
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@136
    move-object/from16 v23, v0

    #@138
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@13b
    move-result-object v23

    #@13c
    move-object/from16 v0, v23

    #@13e
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@140
    move-object/from16 v23, v0

    #@142
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@145
    move-result-object v23

    #@146
    move-object/from16 v0, v23

    #@148
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@14a
    move/from16 v23, v0

    #@14c
    const/16 v24, 0x1

    #@14e
    move/from16 v0, v23

    #@150
    move/from16 v1, v24

    #@152
    if-ne v0, v1, :cond_28

    #@154
    .line 291
    move-object/from16 v0, p0

    #@156
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@158
    move-object/from16 v23, v0

    #@15a
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@15d
    move-result-object v23

    #@15e
    move-object/from16 v0, v23

    #@160
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@162
    move-object/from16 v23, v0

    #@164
    sget-object v24, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@166
    const-string v25, ""

    #@168
    const/16 v26, 0x1

    #@16a
    invoke-virtual/range {v23 .. v26}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@16d
    .line 292
    move-object/from16 v0, p0

    #@16f
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@171
    move-object/from16 v23, v0

    #@173
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@176
    move-result-object v23

    #@177
    move-object/from16 v0, v23

    #@179
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@17b
    move-object/from16 v23, v0

    #@17d
    const/16 v24, 0x0

    #@17f
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@182
    goto/16 :goto_28

    #@184
    .line 295
    :cond_184
    const-string v23, "com.skt.CALL_PROTECTION_MENU_ON"

    #@186
    move-object/from16 v0, v23

    #@188
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18b
    move-result v23

    #@18c
    if-eqz v23, :cond_1d2

    #@18e
    .line 299
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@190
    const-string v24, "[LGE_DATA] com.skt.CALL_PROTECTION_MENU_ON"

    #@192
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@195
    .line 301
    move-object/from16 v0, p0

    #@197
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@199
    move-object/from16 v23, v0

    #@19b
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@19e
    move-result-object v23

    #@19f
    move-object/from16 v0, v23

    #@1a1
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1a3
    move-object/from16 v23, v0

    #@1a5
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1a8
    move-result-object v23

    #@1a9
    move-object/from16 v0, v23

    #@1ab
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@1ad
    move/from16 v23, v0

    #@1af
    const/16 v24, 0x1

    #@1b1
    move/from16 v0, v23

    #@1b3
    move/from16 v1, v24

    #@1b5
    if-ne v0, v1, :cond_28

    #@1b7
    .line 303
    move-object/from16 v0, p0

    #@1b9
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@1bb
    move-object/from16 v23, v0

    #@1bd
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@1c0
    move-result-object v23

    #@1c1
    move-object/from16 v0, v23

    #@1c3
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@1c5
    move-object/from16 v23, v0

    #@1c7
    sget-object v24, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@1c9
    const-string v25, ""

    #@1cb
    const/16 v26, 0x0

    #@1cd
    invoke-virtual/range {v23 .. v26}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@1d0
    goto/16 :goto_28

    #@1d2
    .line 308
    :cond_1d2
    const-string v23, "com.skt.test_intent"

    #@1d4
    move-object/from16 v0, v23

    #@1d6
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d9
    move-result v23

    #@1da
    if-eqz v23, :cond_1fe

    #@1dc
    .line 311
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@1de
    const-string v24, "[LGE_DATA] com.skt.test_intent"

    #@1e0
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e3
    .line 312
    move-object/from16 v0, p0

    #@1e5
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@1e7
    move-object/from16 v23, v0

    #@1e9
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@1ec
    move-result-object v23

    #@1ed
    move-object/from16 v0, v23

    #@1ef
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@1f1
    move-object/from16 v23, v0

    #@1f3
    sget-object v24, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->functionForPacketList:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@1f5
    const-string v25, ""

    #@1f7
    const/16 v26, 0x0

    #@1f9
    invoke-virtual/range {v23 .. v26}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@1fc
    goto/16 :goto_28

    #@1fe
    .line 316
    :cond_1fe
    const-string v23, "com.kt.CALL_PROTECTION_CALL_START"

    #@200
    move-object/from16 v0, v23

    #@202
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@205
    move-result v23

    #@206
    if-eqz v23, :cond_396

    #@208
    .line 317
    move-object/from16 v0, p0

    #@20a
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@20c
    move-object/from16 v23, v0

    #@20e
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@211
    move-result-object v23

    #@212
    move-object/from16 v0, v23

    #@214
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@216
    move-object/from16 v23, v0

    #@218
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@21b
    move-result-object v23

    #@21c
    move-object/from16 v0, v23

    #@21e
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MULTIRAB_KT:Z

    #@220
    move/from16 v23, v0

    #@222
    const/16 v24, 0x1

    #@224
    move/from16 v0, v23

    #@226
    move/from16 v1, v24

    #@228
    if-ne v0, v1, :cond_28

    #@22a
    .line 318
    move-object/from16 v0, p0

    #@22c
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@22e
    move-object/from16 v23, v0

    #@230
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@233
    move-result-object v23

    #@234
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@237
    move-result-object v23

    #@238
    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@23b
    move-result-object v23

    #@23c
    const-string v24, "multi_rab_setting"

    #@23e
    const/16 v25, 0x0

    #@240
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@243
    move-result v23

    #@244
    const/16 v24, 0x1

    #@246
    move/from16 v0, v23

    #@248
    move/from16 v1, v24

    #@24a
    if-ne v0, v1, :cond_359

    #@24c
    const/4 v9, 0x1

    #@24d
    .line 319
    .local v9, blockPacketMenuFlag:Z
    :goto_24d
    move-object/from16 v0, p0

    #@24f
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@251
    move-object/from16 v23, v0

    #@253
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@256
    move-result-object v23

    #@257
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@25a
    move-result-object v23

    #@25b
    sget-object v24, Lcom/android/internal/telephony/PhoneConstants$State;->RINGING:Lcom/android/internal/telephony/PhoneConstants$State;

    #@25d
    move-object/from16 v0, v23

    #@25f
    move-object/from16 v1, v24

    #@261
    if-eq v0, v1, :cond_279

    #@263
    move-object/from16 v0, p0

    #@265
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@267
    move-object/from16 v23, v0

    #@269
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@26c
    move-result-object v23

    #@26d
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@270
    move-result-object v23

    #@271
    sget-object v24, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@273
    move-object/from16 v0, v23

    #@275
    move-object/from16 v1, v24

    #@277
    if-ne v0, v1, :cond_35c

    #@279
    :cond_279
    const/16 v22, 0x1

    #@27b
    .line 321
    .local v22, validCallState:Z
    :goto_27b
    const-string v23, "LGE_DATA"

    #@27d
    new-instance v24, Ljava/lang/StringBuilder;

    #@27f
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@282
    const-string v25, "com.kt.CALL_PROTECTION_CALL_START settingFlag : "

    #@284
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@287
    move-result-object v24

    #@288
    move-object/from16 v0, v24

    #@28a
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@28d
    move-result-object v24

    #@28e
    const-string v25, " callState : "

    #@290
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@293
    move-result-object v24

    #@294
    move-object/from16 v0, v24

    #@296
    move/from16 v1, v22

    #@298
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@29b
    move-result-object v24

    #@29c
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29f
    move-result-object v24

    #@2a0
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a3
    .line 323
    if-eqz v22, :cond_28

    #@2a5
    if-eqz v9, :cond_28

    #@2a7
    move-object/from16 v0, p0

    #@2a9
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@2ab
    move-object/from16 v23, v0

    #@2ad
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@2b0
    move-result-object v23

    #@2b1
    move-object/from16 v0, v23

    #@2b3
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2b5
    move-object/from16 v23, v0

    #@2b7
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2ba
    move-result-object v23

    #@2bb
    move-object/from16 v0, v23

    #@2bd
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@2bf
    move-object/from16 v23, v0

    #@2c1
    const-string v24, "KTBASE"

    #@2c3
    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c6
    move-result v23

    #@2c7
    if-eqz v23, :cond_28

    #@2c9
    move-object/from16 v0, p0

    #@2cb
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@2cd
    move-object/from16 v23, v0

    #@2cf
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@2d2
    move-result-object v23

    #@2d3
    move-object/from16 v0, v23

    #@2d5
    iget-boolean v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@2d7
    move/from16 v23, v0

    #@2d9
    if-nez v23, :cond_28

    #@2db
    .line 324
    const-string v23, "LGE_DATA"

    #@2dd
    const-string v24, "com.kt.CALL_PROTECTION_CALL_START"

    #@2df
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e2
    .line 326
    move-object/from16 v0, p0

    #@2e4
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@2e6
    move-object/from16 v23, v0

    #@2e8
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@2eb
    move-result-object v23

    #@2ec
    const-string v24, "mms"

    #@2ee
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@2f1
    move-result v23

    #@2f2
    if-nez v23, :cond_360

    #@2f4
    move-object/from16 v0, p0

    #@2f6
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@2f8
    move-object/from16 v23, v0

    #@2fa
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@2fd
    move-result-object v23

    #@2fe
    const-string v24, "ims"

    #@300
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@303
    move-result v23

    #@304
    if-nez v23, :cond_360

    #@306
    move-object/from16 v0, p0

    #@308
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@30a
    move-object/from16 v23, v0

    #@30c
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@30f
    move-result-object v23

    #@310
    const-string v24, "supl"

    #@312
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@315
    move-result v23

    #@316
    if-nez v23, :cond_360

    #@318
    move-object/from16 v0, p0

    #@31a
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@31c
    move-object/from16 v23, v0

    #@31e
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@321
    move-result-object v23

    #@322
    const-string v24, "ktmultirab1"

    #@324
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@327
    move-result v23

    #@328
    if-nez v23, :cond_360

    #@32a
    .line 331
    move-object/from16 v0, p0

    #@32c
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@32e
    move-object/from16 v23, v0

    #@330
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@333
    move-result-object v23

    #@334
    move-object/from16 v0, v23

    #@336
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@338
    move-object/from16 v23, v0

    #@33a
    const/16 v24, 0x1

    #@33c
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@33f
    .line 332
    move-object/from16 v0, p0

    #@341
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@343
    move-object/from16 v23, v0

    #@345
    move-object/from16 v0, p0

    #@347
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@349
    move-object/from16 v24, v0

    #@34b
    const v25, 0x42035

    #@34e
    invoke-virtual/range {v24 .. v25}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@351
    move-result-object v24

    #@352
    const-wide/16 v25, 0xbb8

    #@354
    invoke-virtual/range {v23 .. v26}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@357
    goto/16 :goto_28

    #@359
    .line 318
    .end local v9           #blockPacketMenuFlag:Z
    .end local v22           #validCallState:Z
    :cond_359
    const/4 v9, 0x0

    #@35a
    goto/16 :goto_24d

    #@35c
    .line 319
    .restart local v9       #blockPacketMenuFlag:Z
    :cond_35c
    const/16 v22, 0x0

    #@35e
    goto/16 :goto_27b

    #@360
    .line 335
    .restart local v22       #validCallState:Z
    :cond_360
    const-string v23, "LGE_DATA"

    #@362
    const-string v24, "mFDTimeoutMaxCount 10sec"

    #@364
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@367
    .line 336
    new-instance v17, Landroid/content/Intent;

    #@369
    const-string v23, "com.lge.ACTION_FD_TRIGGER_TIME_VAL_CHANGED"

    #@36b
    move-object/from16 v0, v17

    #@36d
    move-object/from16 v1, v23

    #@36f
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@372
    .line 337
    .local v17, sendIntent:Landroid/content/Intent;
    const-string v23, "value"

    #@374
    const/16 v24, 0x1

    #@376
    move-object/from16 v0, v17

    #@378
    move-object/from16 v1, v23

    #@37a
    move/from16 v2, v24

    #@37c
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@37f
    .line 338
    move-object/from16 v0, p0

    #@381
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@383
    move-object/from16 v23, v0

    #@385
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@388
    move-result-object v23

    #@389
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@38c
    move-result-object v23

    #@38d
    move-object/from16 v0, v23

    #@38f
    move-object/from16 v1, v17

    #@391
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@394
    goto/16 :goto_28

    #@396
    .line 343
    .end local v9           #blockPacketMenuFlag:Z
    .end local v17           #sendIntent:Landroid/content/Intent;
    .end local v22           #validCallState:Z
    :cond_396
    const-string v23, "com.kt.CALL_PROTECTION_MENU_OFF"

    #@398
    move-object/from16 v0, v23

    #@39a
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39d
    move-result v23

    #@39e
    if-eqz v23, :cond_434

    #@3a0
    .line 344
    move-object/from16 v0, p0

    #@3a2
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@3a4
    move-object/from16 v23, v0

    #@3a6
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@3a9
    move-result-object v23

    #@3aa
    move-object/from16 v0, v23

    #@3ac
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3ae
    move-object/from16 v23, v0

    #@3b0
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@3b3
    move-result-object v23

    #@3b4
    move-object/from16 v0, v23

    #@3b6
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MULTIRAB_KT:Z

    #@3b8
    move/from16 v23, v0

    #@3ba
    const/16 v24, 0x1

    #@3bc
    move/from16 v0, v23

    #@3be
    move/from16 v1, v24

    #@3c0
    if-ne v0, v1, :cond_28

    #@3c2
    .line 345
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3c5
    move-result-object v23

    #@3c6
    const-string v24, "multi_rab_setting"

    #@3c8
    const/16 v25, 0x0

    #@3ca
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3cd
    move-result v23

    #@3ce
    const/16 v24, 0x1

    #@3d0
    move/from16 v0, v23

    #@3d2
    move/from16 v1, v24

    #@3d4
    if-ne v0, v1, :cond_432

    #@3d6
    const/4 v9, 0x1

    #@3d7
    .line 346
    .restart local v9       #blockPacketMenuFlag:Z
    :goto_3d7
    if-eqz v9, :cond_28

    #@3d9
    move-object/from16 v0, p0

    #@3db
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@3dd
    move-object/from16 v23, v0

    #@3df
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@3e2
    move-result-object v23

    #@3e3
    move-object/from16 v0, v23

    #@3e5
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3e7
    move-object/from16 v23, v0

    #@3e9
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@3ec
    move-result-object v23

    #@3ed
    move-object/from16 v0, v23

    #@3ef
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@3f1
    move-object/from16 v23, v0

    #@3f3
    const-string v24, "KTBASE"

    #@3f5
    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f8
    move-result v23

    #@3f9
    if-eqz v23, :cond_28

    #@3fb
    .line 347
    const-string v23, "LGE_DATA"

    #@3fd
    const-string v24, "com.kt.CALL_PROTECTION_MENU_OFF"

    #@3ff
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@402
    .line 348
    move-object/from16 v0, p0

    #@404
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@406
    move-object/from16 v23, v0

    #@408
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@40b
    move-result-object v23

    #@40c
    move-object/from16 v0, v23

    #@40e
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@410
    move-object/from16 v23, v0

    #@412
    const/16 v24, 0x0

    #@414
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@417
    .line 350
    move-object/from16 v0, p0

    #@419
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@41b
    move-object/from16 v23, v0

    #@41d
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@420
    move-result-object v23

    #@421
    move-object/from16 v0, v23

    #@423
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@425
    move-object/from16 v23, v0

    #@427
    sget-object v24, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@429
    const-string v25, ""

    #@42b
    const/16 v26, 0x1

    #@42d
    invoke-virtual/range {v23 .. v26}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@430
    goto/16 :goto_28

    #@432
    .line 345
    .end local v9           #blockPacketMenuFlag:Z
    :cond_432
    const/4 v9, 0x0

    #@433
    goto :goto_3d7

    #@434
    .line 354
    :cond_434
    const-string v23, "com.kt.CALL_PROTECTION_MENU_ON"

    #@436
    move-object/from16 v0, v23

    #@438
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43b
    move-result v23

    #@43c
    if-eqz v23, :cond_5d8

    #@43e
    .line 355
    move-object/from16 v0, p0

    #@440
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@442
    move-object/from16 v23, v0

    #@444
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@447
    move-result-object v23

    #@448
    move-object/from16 v0, v23

    #@44a
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@44c
    move-object/from16 v23, v0

    #@44e
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@451
    move-result-object v23

    #@452
    move-object/from16 v0, v23

    #@454
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MULTIRAB_KT:Z

    #@456
    move/from16 v23, v0

    #@458
    const/16 v24, 0x1

    #@45a
    move/from16 v0, v23

    #@45c
    move/from16 v1, v24

    #@45e
    if-ne v0, v1, :cond_28

    #@460
    .line 356
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@463
    move-result-object v23

    #@464
    const-string v24, "multi_rab_setting"

    #@466
    const/16 v25, 0x0

    #@468
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@46b
    move-result v23

    #@46c
    const/16 v24, 0x1

    #@46e
    move/from16 v0, v23

    #@470
    move/from16 v1, v24

    #@472
    if-ne v0, v1, :cond_588

    #@474
    const/4 v9, 0x1

    #@475
    .line 357
    .restart local v9       #blockPacketMenuFlag:Z
    :goto_475
    if-eqz v9, :cond_28

    #@477
    move-object/from16 v0, p0

    #@479
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@47b
    move-object/from16 v23, v0

    #@47d
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@480
    move-result-object v23

    #@481
    move-object/from16 v0, v23

    #@483
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@485
    move-object/from16 v23, v0

    #@487
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@48a
    move-result-object v23

    #@48b
    move-object/from16 v0, v23

    #@48d
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@48f
    move-object/from16 v23, v0

    #@491
    const-string v24, "KTBASE"

    #@493
    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@496
    move-result v23

    #@497
    if-eqz v23, :cond_28

    #@499
    .line 358
    const-string v23, "LGE_DATA"

    #@49b
    const-string v24, "com.kt.CALL_PROTECTION_MENU_ON"

    #@49d
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a0
    .line 360
    move-object/from16 v0, p0

    #@4a2
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@4a4
    move-object/from16 v23, v0

    #@4a6
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@4a9
    move-result-object v23

    #@4aa
    move-object/from16 v0, v23

    #@4ac
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@4ae
    move-object/from16 v23, v0

    #@4b0
    sget-object v24, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@4b2
    const-string v25, ""

    #@4b4
    const/16 v26, 0x0

    #@4b6
    invoke-virtual/range {v23 .. v26}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@4b9
    .line 361
    move-object/from16 v0, p0

    #@4bb
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@4bd
    move-object/from16 v23, v0

    #@4bf
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@4c2
    move-result-object v23

    #@4c3
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@4c6
    move-result-object v23

    #@4c7
    const-string v24, "activity"

    #@4c9
    invoke-virtual/range {v23 .. v24}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@4cc
    move-result-object v6

    #@4cd
    check-cast v6, Landroid/app/ActivityManager;

    #@4cf
    .line 362
    .local v6, am:Landroid/app/ActivityManager;
    const/16 v23, 0x1

    #@4d1
    move/from16 v0, v23

    #@4d3
    invoke-virtual {v6, v0}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    #@4d6
    move-result-object v19

    #@4d7
    .line 363
    .local v19, taskInfo:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v19, :cond_28

    #@4d9
    .line 365
    const/16 v23, 0x0

    #@4db
    move-object/from16 v0, v19

    #@4dd
    move/from16 v1, v23

    #@4df
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@4e2
    move-result-object v23

    #@4e3
    check-cast v23, Landroid/app/ActivityManager$RunningTaskInfo;

    #@4e5
    move-object/from16 v0, v23

    #@4e7
    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    #@4e9
    move-object/from16 v20, v0

    #@4eb
    .line 366
    .local v20, topActivity:Landroid/content/ComponentName;
    invoke-virtual/range {v20 .. v20}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@4ee
    move-result-object v14

    #@4ef
    .line 367
    .local v14, name:Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@4f2
    move-result-object v21

    #@4f3
    .line 368
    .local v21, topclassname:Ljava/lang/String;
    const-string v23, "LGE_DATA"

    #@4f5
    new-instance v24, Ljava/lang/StringBuilder;

    #@4f7
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@4fa
    const-string v25, "topActivity.getPackageName(); = "

    #@4fc
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ff
    move-result-object v24

    #@500
    move-object/from16 v0, v24

    #@502
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@505
    move-result-object v24

    #@506
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@509
    move-result-object v24

    #@50a
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50d
    .line 369
    const-string v23, "LGE_DATA"

    #@50f
    new-instance v24, Ljava/lang/StringBuilder;

    #@511
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@514
    const-string v25, "topActivity.getClassName(); = "

    #@516
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@519
    move-result-object v24

    #@51a
    move-object/from16 v0, v24

    #@51c
    move-object/from16 v1, v21

    #@51e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@521
    move-result-object v24

    #@522
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@525
    move-result-object v24

    #@526
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@529
    .line 370
    const-string v23, "com.android.phone.InCallScreen"

    #@52b
    move-object/from16 v0, v21

    #@52d
    move-object/from16 v1, v23

    #@52f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@532
    move-result v23

    #@533
    if-eqz v23, :cond_5c1

    #@535
    .line 372
    move-object/from16 v0, p0

    #@537
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@539
    move-object/from16 v23, v0

    #@53b
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@53e
    move-result-object v23

    #@53f
    const-string v24, "mms"

    #@541
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@544
    move-result v23

    #@545
    if-nez v23, :cond_58b

    #@547
    move-object/from16 v0, p0

    #@549
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@54b
    move-object/from16 v23, v0

    #@54d
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@550
    move-result-object v23

    #@551
    const-string v24, "ktmultirab1"

    #@553
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@556
    move-result v23

    #@557
    if-nez v23, :cond_58b

    #@559
    .line 373
    move-object/from16 v0, p0

    #@55b
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@55d
    move-object/from16 v23, v0

    #@55f
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@562
    move-result-object v23

    #@563
    move-object/from16 v0, v23

    #@565
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@567
    move-object/from16 v23, v0

    #@569
    const/16 v24, 0x1

    #@56b
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@56e
    .line 374
    move-object/from16 v0, p0

    #@570
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@572
    move-object/from16 v23, v0

    #@574
    move-object/from16 v0, p0

    #@576
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@578
    move-object/from16 v24, v0

    #@57a
    const v25, 0x42035

    #@57d
    invoke-virtual/range {v24 .. v25}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@580
    move-result-object v24

    #@581
    const-wide/16 v25, 0xbb8

    #@583
    invoke-virtual/range {v23 .. v26}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@586
    goto/16 :goto_28

    #@588
    .line 356
    .end local v6           #am:Landroid/app/ActivityManager;
    .end local v9           #blockPacketMenuFlag:Z
    .end local v14           #name:Ljava/lang/String;
    .end local v19           #taskInfo:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v20           #topActivity:Landroid/content/ComponentName;
    .end local v21           #topclassname:Ljava/lang/String;
    :cond_588
    const/4 v9, 0x0

    #@589
    goto/16 :goto_475

    #@58b
    .line 377
    .restart local v6       #am:Landroid/app/ActivityManager;
    .restart local v9       #blockPacketMenuFlag:Z
    .restart local v14       #name:Ljava/lang/String;
    .restart local v19       #taskInfo:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .restart local v20       #topActivity:Landroid/content/ComponentName;
    .restart local v21       #topclassname:Ljava/lang/String;
    :cond_58b
    const-string v23, "LGE_DATA"

    #@58d
    const-string v24, "mFDTimeoutMaxCount 10sec"

    #@58f
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@592
    .line 378
    new-instance v17, Landroid/content/Intent;

    #@594
    const-string v23, "com.lge.ACTION_FD_TRIGGER_TIME_VAL_CHANGED"

    #@596
    move-object/from16 v0, v17

    #@598
    move-object/from16 v1, v23

    #@59a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@59d
    .line 379
    .restart local v17       #sendIntent:Landroid/content/Intent;
    const-string v23, "value"

    #@59f
    const/16 v24, 0x1

    #@5a1
    move-object/from16 v0, v17

    #@5a3
    move-object/from16 v1, v23

    #@5a5
    move/from16 v2, v24

    #@5a7
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@5aa
    .line 380
    move-object/from16 v0, p0

    #@5ac
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@5ae
    move-object/from16 v23, v0

    #@5b0
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@5b3
    move-result-object v23

    #@5b4
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5b7
    move-result-object v23

    #@5b8
    move-object/from16 v0, v23

    #@5ba
    move-object/from16 v1, v17

    #@5bc
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@5bf
    goto/16 :goto_28

    #@5c1
    .line 384
    .end local v17           #sendIntent:Landroid/content/Intent;
    :cond_5c1
    move-object/from16 v0, p0

    #@5c3
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@5c5
    move-object/from16 v23, v0

    #@5c7
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@5ca
    move-result-object v23

    #@5cb
    move-object/from16 v0, v23

    #@5cd
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@5cf
    move-object/from16 v23, v0

    #@5d1
    const/16 v24, 0x0

    #@5d3
    invoke-virtual/range {v23 .. v24}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@5d6
    goto/16 :goto_28

    #@5d8
    .line 392
    .end local v6           #am:Landroid/app/ActivityManager;
    .end local v9           #blockPacketMenuFlag:Z
    .end local v14           #name:Ljava/lang/String;
    .end local v19           #taskInfo:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v20           #topActivity:Landroid/content/ComponentName;
    .end local v21           #topclassname:Ljava/lang/String;
    :cond_5d8
    const-string v23, "com.lge.GprsAttachedIsTrue"

    #@5da
    move-object/from16 v0, v23

    #@5dc
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5df
    move-result v23

    #@5e0
    if-eqz v23, :cond_680

    #@5e2
    .line 394
    move-object/from16 v0, p0

    #@5e4
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@5e6
    move-object/from16 v23, v0

    #@5e8
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@5eb
    move-result-object v23

    #@5ec
    move-object/from16 v0, v23

    #@5ee
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@5f0
    move-object/from16 v23, v0

    #@5f2
    if-eqz v23, :cond_28

    #@5f4
    move-object/from16 v0, p0

    #@5f6
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@5f8
    move-object/from16 v23, v0

    #@5fa
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@5fd
    move-result-object v23

    #@5fe
    if-eqz v23, :cond_28

    #@600
    .line 396
    move-object/from16 v0, p0

    #@602
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@604
    move-object/from16 v23, v0

    #@606
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@609
    move-result-object v23

    #@60a
    move-object/from16 v0, v23

    #@60c
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@60e
    move-object/from16 v23, v0

    #@610
    const-string v24, "default"

    #@612
    invoke-virtual/range {v23 .. v24}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@615
    move-result-object v11

    #@616
    check-cast v11, Lcom/android/internal/telephony/ApnContext;

    #@618
    .line 398
    .local v11, defaultContext:Lcom/android/internal/telephony/ApnContext;
    const/4 v13, -0x1

    #@619
    .line 399
    .local v13, gprsState:I
    move-object/from16 v0, p0

    #@61b
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@61d
    move-object/from16 v23, v0

    #@61f
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@622
    move-result-object v23

    #@623
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@626
    move-result-object v23

    #@627
    if-eqz v23, :cond_63b

    #@629
    .line 400
    move-object/from16 v0, p0

    #@62b
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@62d
    move-object/from16 v23, v0

    #@62f
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@632
    move-result-object v23

    #@633
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@636
    move-result-object v23

    #@637
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/ServiceStateTracker;->getCurrentDataConnectionState()I

    #@63a
    move-result v13

    #@63b
    .line 404
    :cond_63b
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@63d
    new-instance v24, Ljava/lang/StringBuilder;

    #@63f
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@642
    const-string v25, "[LGE_DATA] com.lge.GprsAttachedIsTrue / default : "

    #@644
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@647
    move-result-object v24

    #@648
    move-object/from16 v0, v24

    #@64a
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@64d
    move-result-object v24

    #@64e
    const-string v25, " / gprsState : "

    #@650
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@653
    move-result-object v24

    #@654
    move-object/from16 v0, v24

    #@656
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@659
    move-result-object v24

    #@65a
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65d
    move-result-object v24

    #@65e
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@661
    .line 406
    if-eqz v11, :cond_28

    #@663
    invoke-virtual {v11}, Lcom/android/internal/telephony/ApnContext;->getState()Lcom/android/internal/telephony/DctConstants$State;

    #@666
    move-result-object v23

    #@667
    sget-object v24, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@669
    move-object/from16 v0, v23

    #@66b
    move-object/from16 v1, v24

    #@66d
    if-eq v0, v1, :cond_28

    #@66f
    if-nez v13, :cond_28

    #@671
    .line 410
    move-object/from16 v0, p0

    #@673
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@675
    move-object/from16 v23, v0

    #@677
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@67a
    move-result-object v23

    #@67b
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/DataConnectionTracker;->onDataConnectionAttached()V

    #@67e
    goto/16 :goto_28

    #@680
    .line 416
    .end local v11           #defaultContext:Lcom/android/internal/telephony/ApnContext;
    .end local v13           #gprsState:I
    :cond_680
    const-string v23, "android.intent.action.ANY_DATA_STATE"

    #@682
    move-object/from16 v0, v23

    #@684
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@687
    move-result v23

    #@688
    if-eqz v23, :cond_bbd

    #@68a
    .line 418
    const-string v23, "apnType"

    #@68c
    move-object/from16 v0, p2

    #@68e
    move-object/from16 v1, v23

    #@690
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@693
    move-result-object v8

    #@694
    .line 419
    .local v8, apnType:Ljava/lang/String;
    const-class v23, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@696
    const-string v24, "state"

    #@698
    move-object/from16 v0, p2

    #@69a
    move-object/from16 v1, v24

    #@69c
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@69f
    move-result-object v24

    #@6a0
    invoke-static/range {v23 .. v24}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@6a3
    move-result-object v18

    #@6a4
    check-cast v18, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@6a6
    .line 423
    .local v18, state:Lcom/android/internal/telephony/PhoneConstants$DataState;
    if-nez v8, :cond_6b1

    #@6a8
    .line 424
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@6aa
    const-string v24, "[LG_DATA] onReceive() ACTION_ANY_DATA_CONNECTION_STATE_CHANGED apnType is NULL"

    #@6ac
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6af
    .line 425
    const-string v8, ""

    #@6b1
    .line 429
    :cond_6b1
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@6b3
    new-instance v24, Ljava/lang/StringBuilder;

    #@6b5
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@6b8
    const-string v25, "[LGE_DATA] ACTION_ANY_DATA_CONNECTION_STATE_CHANGED : "

    #@6ba
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6bd
    move-result-object v24

    #@6be
    move-object/from16 v0, v24

    #@6c0
    move-object/from16 v1, v18

    #@6c2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6c5
    move-result-object v24

    #@6c6
    const-string v25, "  type "

    #@6c8
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6cb
    move-result-object v24

    #@6cc
    move-object/from16 v0, v24

    #@6ce
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d1
    move-result-object v24

    #@6d2
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d5
    move-result-object v24

    #@6d6
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6d9
    .line 430
    sget-object v23, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$PhoneConstants$DataState:[I

    #@6db
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/PhoneConstants$DataState;->ordinal()I

    #@6de
    move-result v24

    #@6df
    aget v23, v23, v24

    #@6e1
    packed-switch v23, :pswitch_data_c86

    #@6e4
    goto/16 :goto_28

    #@6e6
    .line 432
    :pswitch_6e6
    invoke-static {}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$200()[Z

    #@6e9
    move-result-object v23

    #@6ea
    move-object/from16 v0, p0

    #@6ec
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@6ee
    move-object/from16 v24, v0

    #@6f0
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@6f3
    move-result-object v24

    #@6f4
    move-object/from16 v0, v24

    #@6f6
    invoke-virtual {v0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@6f9
    move-result v24

    #@6fa
    aget-boolean v23, v23, v24

    #@6fc
    if-eqz v23, :cond_716

    #@6fe
    .line 433
    invoke-static {}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$200()[Z

    #@701
    move-result-object v23

    #@702
    move-object/from16 v0, p0

    #@704
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@706
    move-object/from16 v24, v0

    #@708
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@70b
    move-result-object v24

    #@70c
    move-object/from16 v0, v24

    #@70e
    invoke-virtual {v0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@711
    move-result v24

    #@712
    const/16 v25, 0x0

    #@714
    aput-boolean v25, v23, v24

    #@716
    .line 435
    :cond_716
    const-string v23, "wifi"

    #@718
    move-object/from16 v0, v23

    #@71a
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71d
    move-result v23

    #@71e
    if-eqz v23, :cond_739

    #@720
    .line 436
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@722
    const-string v24, "handleConnectionFailure: mIsWifiConnected = false"

    #@724
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@727
    .line 437
    move-object/from16 v0, p0

    #@729
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@72b
    move-object/from16 v23, v0

    #@72d
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@730
    move-result-object v23

    #@731
    const/16 v24, 0x0

    #@733
    move/from16 v0, v24

    #@735
    move-object/from16 v1, v23

    #@737
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@739
    .line 440
    :cond_739
    const-string v23, "wifi"

    #@73b
    move-object/from16 v0, v23

    #@73d
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@740
    move-result v23

    #@741
    if-eqz v23, :cond_74e

    #@743
    .line 442
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@745
    const-string v24, "[LGE_DATA] handleDisconnect() : mJustIsWifiConnected = false"

    #@747
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@74a
    .line 443
    const/16 v23, 0x0

    #@74c
    sput-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsWifiConnected:Z

    #@74e
    .line 447
    :cond_74e
    const-string v23, "bluetooth"

    #@750
    move-object/from16 v0, v23

    #@752
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@755
    move-result v23

    #@756
    if-eqz v23, :cond_763

    #@758
    .line 449
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@75a
    const-string v24, "[LGE_DATA] handleDisconnect() : mJustIsBTConnected = false"

    #@75c
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@75f
    .line 450
    const/16 v23, 0x0

    #@761
    sput-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsBTConnected:Z

    #@763
    .line 455
    :cond_763
    move-object/from16 v0, p0

    #@765
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@767
    move-object/from16 v23, v0

    #@769
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@76c
    move-result-object v23

    #@76d
    move-object/from16 v0, v23

    #@76f
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@771
    move-object/from16 v23, v0

    #@773
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@776
    move-result-object v23

    #@777
    move-object/from16 v0, v23

    #@779
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MMS_ON_DATA_DISABLED_SKT:Z

    #@77b
    move/from16 v23, v0

    #@77d
    if-eqz v23, :cond_7f2

    #@77f
    .line 458
    move-object/from16 v0, p0

    #@781
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@783
    move-object/from16 v23, v0

    #@785
    move-object/from16 v0, v23

    #@787
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@789
    move-object/from16 v23, v0

    #@78b
    move-object/from16 v0, p0

    #@78d
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@78f
    move-object/from16 v24, v0

    #@791
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@794
    const/16 v24, 0x1

    #@796
    invoke-virtual/range {v23 .. v24}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@799
    move-result-object v10

    #@79a
    .line 459
    .local v10, checkInfo:Landroid/net/NetworkInfo;
    move-object/from16 v0, p0

    #@79c
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@79e
    move-object/from16 v23, v0

    #@7a0
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@7a3
    move-result-object v23

    #@7a4
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@7a7
    move-result-object v23

    #@7a8
    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7ab
    move-result-object v23

    #@7ac
    const-string v24, "mobile_data"

    #@7ae
    const/16 v25, 0x1

    #@7b0
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@7b3
    move-result v23

    #@7b4
    if-nez v23, :cond_7f2

    #@7b6
    if-eqz v10, :cond_7f2

    #@7b8
    invoke-virtual {v10}, Landroid/net/NetworkInfo;->isConnected()Z

    #@7bb
    move-result v23

    #@7bc
    const/16 v24, 0x1

    #@7be
    move/from16 v0, v23

    #@7c0
    move/from16 v1, v24

    #@7c2
    if-ne v0, v1, :cond_7f2

    #@7c4
    .line 462
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@7c6
    const-string v24, "[LGE_DATA] Remove default DNS not to allow other apps. to do DNS query Except for MMS."

    #@7c8
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7cb
    .line 464
    sget-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsWifiConnected:Z

    #@7cd
    if-nez v23, :cond_7f2

    #@7cf
    move-object/from16 v0, p0

    #@7d1
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@7d3
    move-object/from16 v23, v0

    #@7d5
    move-object/from16 v0, p0

    #@7d7
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@7d9
    move-object/from16 v24, v0

    #@7db
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@7de
    move-result-object v24

    #@7df
    move-object/from16 v0, v24

    #@7e1
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7e3
    move-object/from16 v24, v0

    #@7e5
    invoke-interface/range {v24 .. v24}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@7e8
    move-result-object v24

    #@7e9
    move-object/from16 v0, v24

    #@7eb
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@7ed
    move/from16 v24, v0

    #@7ef
    invoke-static/range {v23 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$300(Lcom/android/internal/telephony/LGEDataConnectionTracker;Z)V

    #@7f2
    .line 470
    .end local v10           #checkInfo:Landroid/net/NetworkInfo;
    :cond_7f2
    move-object/from16 v0, p0

    #@7f4
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@7f6
    move-object/from16 v23, v0

    #@7f8
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@7fb
    move-result-object v23

    #@7fc
    move-object/from16 v0, v23

    #@7fe
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@800
    move-object/from16 v23, v0

    #@802
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@805
    move-result-object v23

    #@806
    move-object/from16 v0, v23

    #@808
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@80a
    move/from16 v23, v0

    #@80c
    if-eqz v23, :cond_881

    #@80e
    .line 473
    move-object/from16 v0, p0

    #@810
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@812
    move-object/from16 v23, v0

    #@814
    move-object/from16 v0, v23

    #@816
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@818
    move-object/from16 v23, v0

    #@81a
    move-object/from16 v0, p0

    #@81c
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@81e
    move-object/from16 v24, v0

    #@820
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@823
    const/16 v24, 0x17

    #@825
    invoke-virtual/range {v23 .. v24}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@828
    move-result-object v10

    #@829
    .line 474
    .restart local v10       #checkInfo:Landroid/net/NetworkInfo;
    move-object/from16 v0, p0

    #@82b
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@82d
    move-object/from16 v23, v0

    #@82f
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@832
    move-result-object v23

    #@833
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@836
    move-result-object v23

    #@837
    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@83a
    move-result-object v23

    #@83b
    const-string v24, "mobile_data"

    #@83d
    const/16 v25, 0x1

    #@83f
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@842
    move-result v23

    #@843
    if-nez v23, :cond_881

    #@845
    if-eqz v10, :cond_881

    #@847
    invoke-virtual {v10}, Landroid/net/NetworkInfo;->isConnected()Z

    #@84a
    move-result v23

    #@84b
    const/16 v24, 0x1

    #@84d
    move/from16 v0, v23

    #@84f
    move/from16 v1, v24

    #@851
    if-ne v0, v1, :cond_881

    #@853
    .line 477
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@855
    const-string v24, "[LGE_DATA] Remove default DNS not to allow other apps. to do DNS query Except for Emergency."

    #@857
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@85a
    .line 479
    sget-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsWifiConnected:Z

    #@85c
    if-nez v23, :cond_881

    #@85e
    move-object/from16 v0, p0

    #@860
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@862
    move-object/from16 v23, v0

    #@864
    move-object/from16 v0, p0

    #@866
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@868
    move-object/from16 v24, v0

    #@86a
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@86d
    move-result-object v24

    #@86e
    move-object/from16 v0, v24

    #@870
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@872
    move-object/from16 v24, v0

    #@874
    invoke-interface/range {v24 .. v24}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@877
    move-result-object v24

    #@878
    move-object/from16 v0, v24

    #@87a
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@87c
    move/from16 v24, v0

    #@87e
    invoke-static/range {v23 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$300(Lcom/android/internal/telephony/LGEDataConnectionTracker;Z)V

    #@881
    .line 484
    .end local v10           #checkInfo:Landroid/net/NetworkInfo;
    :cond_881
    move-object/from16 v0, p0

    #@883
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@885
    move-object/from16 v23, v0

    #@887
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@88a
    move-result-object v23

    #@88b
    move-object/from16 v0, v23

    #@88d
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@88f
    move-object/from16 v23, v0

    #@891
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@894
    move-result-object v23

    #@895
    move-object/from16 v0, v23

    #@897
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@899
    move/from16 v23, v0

    #@89b
    if-eqz v23, :cond_8eb

    #@89d
    .line 486
    move-object/from16 v0, p0

    #@89f
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@8a1
    move-object/from16 v23, v0

    #@8a3
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@8a6
    move-result-object v23

    #@8a7
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@8aa
    move-result-object v23

    #@8ab
    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8ae
    move-result-object v23

    #@8af
    const-string v24, "mobile_data"

    #@8b1
    const/16 v25, 0x1

    #@8b3
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@8b6
    move-result v23

    #@8b7
    if-nez v23, :cond_8eb

    #@8b9
    .line 488
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@8bb
    const-string v24, "[LGE_DATA_DNS] Remove default DNS ."

    #@8bd
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8c0
    .line 489
    sget-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsWifiConnected:Z

    #@8c2
    if-nez v23, :cond_8eb

    #@8c4
    sget-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsBTConnected:Z

    #@8c6
    if-nez v23, :cond_8eb

    #@8c8
    .line 494
    move-object/from16 v0, p0

    #@8ca
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@8cc
    move-object/from16 v23, v0

    #@8ce
    move-object/from16 v0, p0

    #@8d0
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@8d2
    move-object/from16 v24, v0

    #@8d4
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@8d7
    move-result-object v24

    #@8d8
    move-object/from16 v0, v24

    #@8da
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@8dc
    move-object/from16 v24, v0

    #@8de
    invoke-interface/range {v24 .. v24}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@8e1
    move-result-object v24

    #@8e2
    move-object/from16 v0, v24

    #@8e4
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@8e6
    move/from16 v24, v0

    #@8e8
    invoke-static/range {v23 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$300(Lcom/android/internal/telephony/LGEDataConnectionTracker;Z)V

    #@8eb
    .line 498
    :cond_8eb
    move-object/from16 v0, p0

    #@8ed
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@8ef
    move-object/from16 v23, v0

    #@8f1
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@8f4
    move-result-object v23

    #@8f5
    move-object/from16 v0, v23

    #@8f7
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@8f9
    move-object/from16 v23, v0

    #@8fb
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@8fe
    move-result-object v23

    #@8ff
    move-object/from16 v0, v23

    #@901
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_UPLUS_INIT:Z

    #@903
    move/from16 v23, v0

    #@905
    if-eqz v23, :cond_28

    #@907
    move-object/from16 v0, p0

    #@909
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@90b
    move-object/from16 v23, v0

    #@90d
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@910
    move-result-object v23

    #@911
    move-object/from16 v0, v23

    #@913
    invoke-virtual {v0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@916
    move-result v23

    #@917
    const/16 v24, 0xc

    #@919
    move/from16 v0, v23

    #@91b
    move/from16 v1, v24

    #@91d
    if-ne v0, v1, :cond_28

    #@91f
    .line 501
    const-string v3, "8.8.8.8"

    #@921
    .line 502
    .local v3, DNS_DEFAULT_SERVER1:Ljava/lang/String;
    const-string v4, "8.8.4.4"

    #@923
    .line 504
    .local v4, DNS_DEFAULT_SERVER2:Ljava/lang/String;
    move-object/from16 v0, p0

    #@925
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@927
    move-object/from16 v23, v0

    #@929
    move-object/from16 v0, v23

    #@92b
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@92d
    move-object/from16 v23, v0

    #@92f
    const/16 v24, 0x12

    #@931
    invoke-virtual/range {v23 .. v24}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@934
    move-result-object v15

    #@935
    .line 507
    .local v15, newLp:Landroid/net/LinkProperties;
    if-eqz v15, :cond_28

    #@937
    .line 508
    invoke-static {v3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@93a
    move-result-object v23

    #@93b
    move-object/from16 v0, v23

    #@93d
    invoke-virtual {v15, v0}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@940
    .line 509
    invoke-static {v4}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@943
    move-result-object v23

    #@944
    move-object/from16 v0, v23

    #@946
    invoke-virtual {v15, v0}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@949
    goto/16 :goto_28

    #@94b
    .line 525
    .end local v3           #DNS_DEFAULT_SERVER1:Ljava/lang/String;
    .end local v4           #DNS_DEFAULT_SERVER2:Ljava/lang/String;
    .end local v15           #newLp:Landroid/net/LinkProperties;
    :pswitch_94b
    move-object/from16 v0, p0

    #@94d
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@94f
    move-object/from16 v23, v0

    #@951
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@954
    move-result-object v23

    #@955
    move-object/from16 v0, v23

    #@957
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@959
    move-object/from16 v23, v0

    #@95b
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@95e
    move-result-object v23

    #@95f
    move-object/from16 v0, v23

    #@961
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_TOAST_ON_WIFI_OFF_UPLUS:Z

    #@963
    move/from16 v23, v0

    #@965
    const/16 v24, 0x1

    #@967
    move/from16 v0, v23

    #@969
    move/from16 v1, v24

    #@96b
    if-eq v0, v1, :cond_98f

    #@96d
    move-object/from16 v0, p0

    #@96f
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@971
    move-object/from16 v23, v0

    #@973
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@976
    move-result-object v23

    #@977
    move-object/from16 v0, v23

    #@979
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@97b
    move-object/from16 v23, v0

    #@97d
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@980
    move-result-object v23

    #@981
    move-object/from16 v0, v23

    #@983
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_KT:Z

    #@985
    move/from16 v23, v0

    #@987
    const/16 v24, 0x1

    #@989
    move/from16 v0, v23

    #@98b
    move/from16 v1, v24

    #@98d
    if-ne v0, v1, :cond_9b2

    #@98f
    .line 526
    :cond_98f
    const-string v23, "wifi"

    #@991
    move-object/from16 v0, v23

    #@993
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@996
    move-result v23

    #@997
    if-eqz v23, :cond_9b2

    #@999
    .line 527
    move-object/from16 v0, p0

    #@99b
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@99d
    move-object/from16 v23, v0

    #@99f
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@9a2
    move-result-object v23

    #@9a3
    const/16 v24, 0x1

    #@9a5
    move/from16 v0, v24

    #@9a7
    move-object/from16 v1, v23

    #@9a9
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@9ab
    .line 528
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@9ad
    const-string v24, "[LG_DATA]WIFI IS CONNECTED"

    #@9af
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9b2
    .line 533
    :cond_9b2
    const-string v23, "wifi"

    #@9b4
    move-object/from16 v0, v23

    #@9b6
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9b9
    move-result v23

    #@9ba
    if-eqz v23, :cond_9c7

    #@9bc
    .line 535
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@9be
    const-string v24, "[LGE_DATA] handleConnect() : mJustIsWifiConnected = true"

    #@9c0
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9c3
    .line 536
    const/16 v23, 0x1

    #@9c5
    sput-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsWifiConnected:Z

    #@9c7
    .line 539
    :cond_9c7
    const-string v23, "bluetooth"

    #@9c9
    move-object/from16 v0, v23

    #@9cb
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9ce
    move-result v23

    #@9cf
    if-eqz v23, :cond_9dc

    #@9d1
    .line 541
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@9d3
    const-string v24, "[LGE_DATA] handleconnect() : mJustIsBTConnected = true"

    #@9d5
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9d8
    .line 542
    const/16 v23, 0x1

    #@9da
    sput-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsBTConnected:Z

    #@9dc
    .line 547
    :cond_9dc
    move-object/from16 v0, p0

    #@9de
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@9e0
    move-object/from16 v23, v0

    #@9e2
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@9e5
    move-result-object v23

    #@9e6
    move-object/from16 v0, v23

    #@9e8
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9ea
    move-object/from16 v23, v0

    #@9ec
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@9ef
    move-result-object v23

    #@9f0
    move-object/from16 v0, v23

    #@9f2
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MMS_ON_DATA_DISABLED_SKT:Z

    #@9f4
    move/from16 v23, v0

    #@9f6
    if-eqz v23, :cond_a6b

    #@9f8
    .line 550
    move-object/from16 v0, p0

    #@9fa
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@9fc
    move-object/from16 v23, v0

    #@9fe
    move-object/from16 v0, v23

    #@a00
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@a02
    move-object/from16 v23, v0

    #@a04
    move-object/from16 v0, p0

    #@a06
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@a08
    move-object/from16 v24, v0

    #@a0a
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@a0d
    const/16 v24, 0x1

    #@a0f
    invoke-virtual/range {v23 .. v24}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@a12
    move-result-object v10

    #@a13
    .line 551
    .restart local v10       #checkInfo:Landroid/net/NetworkInfo;
    move-object/from16 v0, p0

    #@a15
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@a17
    move-object/from16 v23, v0

    #@a19
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@a1c
    move-result-object v23

    #@a1d
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@a20
    move-result-object v23

    #@a21
    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a24
    move-result-object v23

    #@a25
    const-string v24, "mobile_data"

    #@a27
    const/16 v25, 0x1

    #@a29
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@a2c
    move-result v23

    #@a2d
    if-nez v23, :cond_a6b

    #@a2f
    if-eqz v10, :cond_a6b

    #@a31
    invoke-virtual {v10}, Landroid/net/NetworkInfo;->isConnected()Z

    #@a34
    move-result v23

    #@a35
    const/16 v24, 0x1

    #@a37
    move/from16 v0, v23

    #@a39
    move/from16 v1, v24

    #@a3b
    if-ne v0, v1, :cond_a6b

    #@a3d
    .line 554
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@a3f
    const-string v24, "[LGE_DATA] Remove default DNS not to allow other apps. to do DNS query Except for MMS."

    #@a41
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a44
    .line 556
    sget-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsWifiConnected:Z

    #@a46
    if-nez v23, :cond_a6b

    #@a48
    move-object/from16 v0, p0

    #@a4a
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@a4c
    move-object/from16 v23, v0

    #@a4e
    move-object/from16 v0, p0

    #@a50
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@a52
    move-object/from16 v24, v0

    #@a54
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@a57
    move-result-object v24

    #@a58
    move-object/from16 v0, v24

    #@a5a
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@a5c
    move-object/from16 v24, v0

    #@a5e
    invoke-interface/range {v24 .. v24}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@a61
    move-result-object v24

    #@a62
    move-object/from16 v0, v24

    #@a64
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@a66
    move/from16 v24, v0

    #@a68
    invoke-static/range {v23 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$300(Lcom/android/internal/telephony/LGEDataConnectionTracker;Z)V

    #@a6b
    .line 562
    .end local v10           #checkInfo:Landroid/net/NetworkInfo;
    :cond_a6b
    move-object/from16 v0, p0

    #@a6d
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@a6f
    move-object/from16 v23, v0

    #@a71
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@a74
    move-result-object v23

    #@a75
    move-object/from16 v0, v23

    #@a77
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@a79
    move-object/from16 v23, v0

    #@a7b
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@a7e
    move-result-object v23

    #@a7f
    move-object/from16 v0, v23

    #@a81
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@a83
    move/from16 v23, v0

    #@a85
    if-eqz v23, :cond_afa

    #@a87
    .line 565
    move-object/from16 v0, p0

    #@a89
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@a8b
    move-object/from16 v23, v0

    #@a8d
    move-object/from16 v0, v23

    #@a8f
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@a91
    move-object/from16 v23, v0

    #@a93
    move-object/from16 v0, p0

    #@a95
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@a97
    move-object/from16 v24, v0

    #@a99
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@a9c
    const/16 v24, 0x17

    #@a9e
    invoke-virtual/range {v23 .. v24}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@aa1
    move-result-object v10

    #@aa2
    .line 566
    .restart local v10       #checkInfo:Landroid/net/NetworkInfo;
    move-object/from16 v0, p0

    #@aa4
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@aa6
    move-object/from16 v23, v0

    #@aa8
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@aab
    move-result-object v23

    #@aac
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@aaf
    move-result-object v23

    #@ab0
    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@ab3
    move-result-object v23

    #@ab4
    const-string v24, "mobile_data"

    #@ab6
    const/16 v25, 0x1

    #@ab8
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@abb
    move-result v23

    #@abc
    if-nez v23, :cond_afa

    #@abe
    if-eqz v10, :cond_afa

    #@ac0
    invoke-virtual {v10}, Landroid/net/NetworkInfo;->isConnected()Z

    #@ac3
    move-result v23

    #@ac4
    const/16 v24, 0x1

    #@ac6
    move/from16 v0, v23

    #@ac8
    move/from16 v1, v24

    #@aca
    if-ne v0, v1, :cond_afa

    #@acc
    .line 569
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@ace
    const-string v24, "[LGE_DATA] Remove default DNS not to allow other apps. to do DNS query Except for Emergency."

    #@ad0
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ad3
    .line 571
    sget-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsWifiConnected:Z

    #@ad5
    if-nez v23, :cond_afa

    #@ad7
    move-object/from16 v0, p0

    #@ad9
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@adb
    move-object/from16 v23, v0

    #@add
    move-object/from16 v0, p0

    #@adf
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@ae1
    move-object/from16 v24, v0

    #@ae3
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@ae6
    move-result-object v24

    #@ae7
    move-object/from16 v0, v24

    #@ae9
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@aeb
    move-object/from16 v24, v0

    #@aed
    invoke-interface/range {v24 .. v24}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@af0
    move-result-object v24

    #@af1
    move-object/from16 v0, v24

    #@af3
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@af5
    move/from16 v24, v0

    #@af7
    invoke-static/range {v23 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$300(Lcom/android/internal/telephony/LGEDataConnectionTracker;Z)V

    #@afa
    .line 576
    .end local v10           #checkInfo:Landroid/net/NetworkInfo;
    :cond_afa
    move-object/from16 v0, p0

    #@afc
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@afe
    move-object/from16 v23, v0

    #@b00
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@b03
    move-result-object v23

    #@b04
    move-object/from16 v0, v23

    #@b06
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@b08
    move-object/from16 v23, v0

    #@b0a
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@b0d
    move-result-object v23

    #@b0e
    move-object/from16 v0, v23

    #@b10
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@b12
    move/from16 v23, v0

    #@b14
    if-eqz v23, :cond_b5d

    #@b16
    .line 578
    move-object/from16 v0, p0

    #@b18
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@b1a
    move-object/from16 v23, v0

    #@b1c
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@b1f
    move-result-object v23

    #@b20
    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@b23
    move-result-object v23

    #@b24
    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b27
    move-result-object v23

    #@b28
    const-string v24, "mobile_data"

    #@b2a
    const/16 v25, 0x1

    #@b2c
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@b2f
    move-result v23

    #@b30
    if-nez v23, :cond_b5d

    #@b32
    .line 580
    sget-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsWifiConnected:Z

    #@b34
    if-nez v23, :cond_b5d

    #@b36
    sget-boolean v23, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsBTConnected:Z

    #@b38
    if-nez v23, :cond_b5d

    #@b3a
    .line 586
    move-object/from16 v0, p0

    #@b3c
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@b3e
    move-object/from16 v23, v0

    #@b40
    move-object/from16 v0, p0

    #@b42
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@b44
    move-object/from16 v24, v0

    #@b46
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@b49
    move-result-object v24

    #@b4a
    move-object/from16 v0, v24

    #@b4c
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@b4e
    move-object/from16 v24, v0

    #@b50
    invoke-interface/range {v24 .. v24}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@b53
    move-result-object v24

    #@b54
    move-object/from16 v0, v24

    #@b56
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@b58
    move/from16 v24, v0

    #@b5a
    invoke-static/range {v23 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$300(Lcom/android/internal/telephony/LGEDataConnectionTracker;Z)V

    #@b5d
    .line 590
    :cond_b5d
    move-object/from16 v0, p0

    #@b5f
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@b61
    move-object/from16 v23, v0

    #@b63
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;

    #@b66
    move-result-object v23

    #@b67
    move-object/from16 v0, v23

    #@b69
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@b6b
    move-object/from16 v23, v0

    #@b6d
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@b70
    move-result-object v23

    #@b71
    move-object/from16 v0, v23

    #@b73
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_UPLUS_INIT:Z

    #@b75
    move/from16 v23, v0

    #@b77
    if-eqz v23, :cond_28

    #@b79
    move-object/from16 v0, p0

    #@b7b
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@b7d
    move-object/from16 v23, v0

    #@b7f
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@b82
    move-result-object v23

    #@b83
    move-object/from16 v0, v23

    #@b85
    invoke-virtual {v0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@b88
    move-result v23

    #@b89
    const/16 v24, 0xc

    #@b8b
    move/from16 v0, v23

    #@b8d
    move/from16 v1, v24

    #@b8f
    if-ne v0, v1, :cond_28

    #@b91
    .line 593
    const-string v3, "8.8.8.8"

    #@b93
    .line 594
    .restart local v3       #DNS_DEFAULT_SERVER1:Ljava/lang/String;
    const-string v4, "8.8.4.4"

    #@b95
    .line 596
    .restart local v4       #DNS_DEFAULT_SERVER2:Ljava/lang/String;
    move-object/from16 v0, p0

    #@b97
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@b99
    move-object/from16 v23, v0

    #@b9b
    move-object/from16 v0, v23

    #@b9d
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@b9f
    move-object/from16 v23, v0

    #@ba1
    const/16 v24, 0x12

    #@ba3
    invoke-virtual/range {v23 .. v24}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@ba6
    move-result-object v15

    #@ba7
    .line 599
    .restart local v15       #newLp:Landroid/net/LinkProperties;
    if-eqz v15, :cond_28

    #@ba9
    .line 600
    invoke-static {v3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@bac
    move-result-object v23

    #@bad
    move-object/from16 v0, v23

    #@baf
    invoke-virtual {v15, v0}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@bb2
    .line 601
    invoke-static {v4}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@bb5
    move-result-object v23

    #@bb6
    move-object/from16 v0, v23

    #@bb8
    invoke-virtual {v15, v0}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@bbb
    goto/16 :goto_28

    #@bbd
    .line 612
    .end local v3           #DNS_DEFAULT_SERVER1:Ljava/lang/String;
    .end local v4           #DNS_DEFAULT_SERVER2:Ljava/lang/String;
    .end local v8           #apnType:Ljava/lang/String;
    .end local v15           #newLp:Landroid/net/LinkProperties;
    .end local v18           #state:Lcom/android/internal/telephony/PhoneConstants$DataState;
    :cond_bbd
    const-string v23, "android.intent.action.DATA_CONNECTION_FAILED"

    #@bbf
    move-object/from16 v0, v23

    #@bc1
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bc4
    move-result v23

    #@bc5
    if-eqz v23, :cond_c3c

    #@bc7
    .line 614
    const-string v23, "apnType"

    #@bc9
    move-object/from16 v0, p2

    #@bcb
    move-object/from16 v1, v23

    #@bcd
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@bd0
    move-result-object v8

    #@bd1
    .line 615
    .restart local v8       #apnType:Ljava/lang/String;
    const-string v23, "reason"

    #@bd3
    move-object/from16 v0, p2

    #@bd5
    move-object/from16 v1, v23

    #@bd7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@bda
    move-result-object v16

    #@bdb
    .line 616
    .local v16, reason:Ljava/lang/String;
    const-string v23, "apn"

    #@bdd
    move-object/from16 v0, p2

    #@bdf
    move-object/from16 v1, v23

    #@be1
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@be4
    move-result-object v7

    #@be5
    .line 617
    .local v7, apnName:Ljava/lang/String;
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@be7
    new-instance v24, Ljava/lang/StringBuilder;

    #@be9
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@bec
    const-string v25, "[LGE_DATA] ACTION_DATA_CONNECTION_FAILED (type) : "

    #@bee
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf1
    move-result-object v24

    #@bf2
    move-object/from16 v0, v24

    #@bf4
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf7
    move-result-object v24

    #@bf8
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bfb
    move-result-object v24

    #@bfc
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bff
    .line 618
    invoke-static {}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$200()[Z

    #@c02
    move-result-object v23

    #@c03
    move-object/from16 v0, p0

    #@c05
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@c07
    move-object/from16 v24, v0

    #@c09
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@c0c
    move-result-object v24

    #@c0d
    move-object/from16 v0, v24

    #@c0f
    invoke-virtual {v0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@c12
    move-result v24

    #@c13
    const/16 v25, 0x0

    #@c15
    aput-boolean v25, v23, v24

    #@c17
    .line 621
    const-string v23, "wifi"

    #@c19
    move-object/from16 v0, v23

    #@c1b
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c1e
    move-result v23

    #@c1f
    if-eqz v23, :cond_28

    #@c21
    .line 622
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@c23
    const-string v24, "handleConnectionFailure: mIsWifiConnected = false"

    #@c25
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c28
    .line 623
    move-object/from16 v0, p0

    #@c2a
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@c2c
    move-object/from16 v23, v0

    #@c2e
    invoke-static/range {v23 .. v23}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;

    #@c31
    move-result-object v23

    #@c32
    const/16 v24, 0x0

    #@c34
    move/from16 v0, v24

    #@c36
    move-object/from16 v1, v23

    #@c38
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@c3a
    goto/16 :goto_28

    #@c3c
    .line 628
    .end local v7           #apnName:Ljava/lang/String;
    .end local v8           #apnType:Ljava/lang/String;
    .end local v16           #reason:Ljava/lang/String;
    :cond_c3c
    const-string v23, "lge.intent.action.LGU_LTE_ROAMING_IS_AVAILABLE"

    #@c3e
    move-object/from16 v0, v23

    #@c40
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c43
    move-result v23

    #@c44
    if-eqz v23, :cond_28

    #@c46
    .line 629
    move-object/from16 v0, p0

    #@c48
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@c4a
    move-object/from16 v23, v0

    #@c4c
    const-string v24, "Lte_Roaming_Is_Available"

    #@c4e
    const/16 v25, 0x0

    #@c50
    move-object/from16 v0, p2

    #@c52
    move-object/from16 v1, v24

    #@c54
    move/from16 v2, v25

    #@c56
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@c59
    move-result v24

    #@c5a
    move/from16 v0, v24

    #@c5c
    move-object/from16 v1, v23

    #@c5e
    iput-boolean v0, v1, Lcom/android/internal/telephony/LGEDataConnectionTracker;->isAvailableLTEDataRoaming:Z

    #@c60
    .line 630
    const-string v23, "[LGE_DATA][LGEDCT] "

    #@c62
    new-instance v24, Ljava/lang/StringBuilder;

    #@c64
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@c67
    const-string v25, "ACTION_LTE_DATAROAMING_AVAILABLE: isAvailableLTEDataRoaming="

    #@c69
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6c
    move-result-object v24

    #@c6d
    move-object/from16 v0, p0

    #@c6f
    iget-object v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;->this$0:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@c71
    move-object/from16 v25, v0

    #@c73
    move-object/from16 v0, v25

    #@c75
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->isAvailableLTEDataRoaming:Z

    #@c77
    move/from16 v25, v0

    #@c79
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c7c
    move-result-object v24

    #@c7d
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c80
    move-result-object v24

    #@c81
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c84
    goto/16 :goto_28

    #@c86
    .line 430
    :pswitch_data_c86
    .packed-switch 0x1
        :pswitch_6e6
        :pswitch_28
        :pswitch_28
        :pswitch_94b
    .end packed-switch
.end method
