.class public abstract Lcom/android/internal/telephony/DataConnectionTracker;
.super Landroid/os/Handler;
.source "DataConnectionTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/DataConnectionTracker$5;,
        Lcom/android/internal/telephony/DataConnectionTracker$RecoveryAction;,
        Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;,
        Lcom/android/internal/telephony/DataConnectionTracker$DataRoamingSettingObserver;,
        Lcom/android/internal/telephony/DataConnectionTracker$ApnChangeObserver;
    }
.end annotation


# static fields
.field private static final ACTION_DELAY_MODE_CHANGE_FOR_IMS:Ljava/lang/String; = "android.intent.action.ACTION_DELAY_MODE_CHANGE_FOR_IMS"

.field protected static final APN_ADMIN_ID:I = 0x8

.field protected static final APN_BIP_ID:I = 0xf

.field protected static final APN_CBS_ID:I = 0x7

.field protected static final APN_DEFAULT_ID:I = 0x0

#the value of this static final field might be set in the static constructor
.field protected static final APN_DELAY_MILLIS:I = 0x0

.field protected static final APN_DUN_ID:I = 0x3

.field protected static final APN_EMERGENCY_ID:I = 0x17

.field protected static final APN_ENTITLEMENT_ID:I = 0xa

.field protected static final APN_FOTA_ID:I = 0x6

.field protected static final APN_HIPRI_ID:I = 0x4

.field protected static final APN_IMS_ID:I = 0x5

.field protected static final APN_INVALID_ID:I = -0x1

.field protected static final APN_KTMULTIRAB1_ID:I = 0xd

.field protected static final APN_KTMULTIRAB2_ID:I = 0xe

.field protected static final APN_MMS_ID:I = 0x1

.field protected static final APN_NUM_TYPES:I = 0x18

.field protected static final APN_RCS_ID:I = 0x10

.field protected static final APN_RESTORE_DELAY_PROP_NAME:Ljava/lang/String; = "android.telephony.apn-restore"

.field protected static final APN_SUPL_ID:I = 0x2

.field protected static final APN_TETHERING_ID:I = 0xc

.field protected static final APN_VZW800_ID:I = 0xb

.field protected static final APN_VZWAPP_ID:I = 0x9

.field protected static final DATA_CONNECTION_ACTIVE_PH_LINK_DOWN:I = 0x1

.field protected static final DATA_CONNECTION_ACTIVE_PH_LINK_INACTIVE:I = 0x0

.field protected static final DATA_CONNECTION_ACTIVE_PH_LINK_UP:I = 0x2

.field protected static final DATA_STALL_ALARM_AGGRESSIVE_DELAY_IN_MS_DEFAULT:I = 0xea60

.field protected static final DATA_STALL_ALARM_NON_AGGRESSIVE_DELAY_IN_MS_DEFAULT:I = 0x57e40

.field protected static final DATA_STALL_ALARM_TAG_EXTRA:Ljava/lang/String; = "data.stall.alram.tag"

.field protected static final DATA_STALL_NOT_SUSPECTED:Z = false

.field protected static final DATA_STALL_NO_RECV_POLL_LIMIT:I = 0x1

.field protected static final DATA_STALL_SUSPECTED:Z = true

.field protected static final DBG:Z = true

.field protected static final DEFALUT_DATA_ON_BOOT_PROP:Ljava/lang/String; = "net.def_data_on_boot"

.field protected static final DEFAULT_DATA_RETRY_CONFIG:Ljava/lang/String; = "default_randomization=2000,5000,10000,20000,40000,80000:5000,160000:5000,320000:5000,640000:5000,1280000:5000,1800000:5000"

.field protected static final DEFAULT_MAX_PDP_RESET_FAIL:I = 0x3

.field protected static final DOMESTIC_DATA_RETRY_CONFIG:Ljava/lang/String; = "default_randomization=0,max_retries=infinite,5000,10000,20000,40000,80000:100,160000:100,320000:100,640000:100,1280000:100,1800000:100"

.field protected static final EMERGENCY_DATA_RETRY_CONFIG:Ljava/lang/String; = "max_retries=4, 2000, 2000, 2000, 2000"

.field protected static final EVENT_CPA_PACKAGE_CHECK:I = 0x400

.field protected static final FAIL_DATA_SETUP_COUNTER:Ljava/lang/String; = "fail_data_setup_counter"

.field protected static final FAIL_DATA_SETUP_FAIL_CAUSE:Ljava/lang/String; = "fail_data_setup_fail_cause"

.field public static final HIGH_PRIO_FIRST:I = 0x0

.field protected static final INTENT_RECONNECT_ALARM_EXTRA_REASON:Ljava/lang/String; = "reconnect_alarm_extra_reason"

.field protected static final INTENT_SET_FAIL_DATA_SETUP_COUNTER:Ljava/lang/String; = "com.android.internal.telephony.dataconnectiontracker.intent_set_fail_data_setup_counter"

.field protected static final IP_VERSION_SUPPORT_TYPE_DUAL:I = 0x3

.field protected static final IP_VERSION_SUPPORT_TYPE_IPV4:I = 0x0

.field protected static final IP_VERSION_SUPPORT_TYPE_IPV6:I = 0x2

.field protected static final IP_VERSION_SUPPORT_TYPE_NOT_AVAILABLE:I = -0x1

.field public static final LOW_PRIO_FIRST:I = 0x1

.field protected static final NO_RECV_POLL_LIMIT:I = 0x18

.field protected static final NT_MODE_INVALID:I = -0x1

.field protected static final NULL_IP:Ljava/lang/String; = "0.0.0.0"

.field protected static final NUMBER_SENT_PACKETS_OF_HANG:I = 0xa

.field protected static final POLL_LONGEST_RTT:I = 0x1d4c0

.field protected static final POLL_NETSTAT_MILLIS:I = 0x3e8

.field protected static final POLL_NETSTAT_SCREEN_OFF_MILLIS:I = 0x927c0

.field protected static final POLL_NETSTAT_SLOW_MILLIS:I = 0x1388

.field protected static final RADIO_TESTS:Z = false

.field protected static final RESTORE_DEFAULT_APN_DELAY:I = 0xea60

.field protected static final SECONDARY_DATA_RETRY_CONFIG:Ljava/lang/String; = "max_retries=3, 5000, 5000, 5000"

.field protected static final VDBG:Z = true

.field protected static final VZW_DATA_RETRY_CONFIG:Ljava/lang/String; = "default_randomization=0,max_retries=infinite,5000,10000,20000:100,40000:100,80000:100,120000:100,180000:100,240000:100"

.field private static final mApnPrioComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/android/internal/telephony/ApnContext;",
            ">;"
        }
    .end annotation
.end field

.field static mApnPriorities:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected static sPolicyDataEnabled:Z

.field private static voice_call_ing:Z


# instance fields
.field protected ACTION_ENABLE_DATA_IN_HPLMN:Ljava/lang/String;

.field protected ACTION_ENABLE_DATA_IN_HPLMN_RESPONSE:Ljava/lang/String;

.field protected ACTION_MOBILE_DATA_ROAMING_OPTION_CANCEL:Ljava/lang/String;

.field protected ACTION_MOBILE_DATA_ROAMING_OPTION_REQUEST:Ljava/lang/String;

.field protected ACTION_MOBILE_DATA_ROAMING_STATE_CHANGE_REQUEST:Ljava/lang/String;

.field public OldisDefaultCount:I

.field protected RADIO_RESET_PROPERTY:Ljava/lang/String;

.field protected REQUEST_ROAMING_OPTION:Ljava/lang/String;

.field protected REQUEST_STATE:Ljava/lang/String;

.field public SPR_GsmGlobalAPN:Ljava/lang/String;

.field public SPR_GsmGlobalAPN_TEST:Ljava/lang/String;

.field protected SUPPORT_LG_DATA_RECOVERY:Z

.field public apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

.field public blockForDun:Z

.field public cpa_PackageName:Ljava/lang/String;

.field public cpa_apn:Ljava/lang/String;

.field public cpa_authType:I

.field public cpa_dns1:Ljava/lang/String;

.field public cpa_dns2:Ljava/lang/String;

.field public cpa_enable:Z

.field public cpa_password:Ljava/lang/String;

.field public cpa_send_result:Z

.field public cpa_user:Ljava/lang/String;

.field protected dataEnabled:[Z

.field public dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

.field public datablockbyadmin:Z

.field private enabledCount:I

.field public hasProfileDbChanged:Z

.field public imsRegiState:Z

.field public internetPDNconnected:Z

.field public isAPNDataBlock:Z

.field protected isDataonPopupShown:Z

.field protected mActiveApn:Lcom/android/internal/telephony/DataProfile;

.field protected mActivity:Lcom/android/internal/telephony/DctConstants$Activity;

.field public mAllApns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/DataProfile;",
            ">;"
        }
    .end annotation
.end field

.field protected mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/internal/telephony/ApnContext;",
            ">;"
        }
    .end annotation
.end field

.field private mApnObserver:Lcom/android/internal/telephony/DataConnectionTracker$ApnChangeObserver;

.field protected mApnToDataConnectionId:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mAutoAttachOnCreation:Z

.field protected mCidActive:I

.field protected mDataConnectRegistrants:Landroid/os/RegistrantList;

.field protected mDataConnectionAsyncChannels:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/internal/telephony/DataConnectionAc;",
            ">;"
        }
    .end annotation
.end field

.field protected mDataConnectionTracker:Landroid/os/Handler;

.field protected mDataConnections:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/internal/telephony/DataConnection;",
            ">;"
        }
    .end annotation
.end field

.field protected mDataEnabledLock:Ljava/lang/Object;

.field protected mDataProfile:I

.field private final mDataRoamingSettingObserver:Lcom/android/internal/telephony/DataConnectionTracker$DataRoamingSettingObserver;

.field protected mDataStallAlarmIntent:Landroid/app/PendingIntent;

.field protected mDataStallAlarmTag:I

.field protected mDataStallTxRxSum:Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

.field protected mDelayModeChangeforIms:Landroid/app/PendingIntent;

.field public mDomesticPreferredApn:Lcom/android/internal/telephony/DataProfile;

.field public mEPDNBarring:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

.field public mEPDNSupport:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

.field public mEmcFailCause:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

.field public mEmerAttachSupport:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

.field public mEmerCampedCID:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

.field public mEmerCampedTAC:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

.field protected mFailDataSetupCounter:I

.field protected mFailDataSetupFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

.field protected mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/android/internal/telephony/uicc/IccRecords;",
            ">;"
        }
    .end annotation
.end field

.field protected mIntentReceiver:Landroid/content/BroadcastReceiver;

.field protected mInternalDataEnabled:Z

.field protected mIsDataProfileActive:I

.field protected mIsDisposed:Z

.field protected mIsOmaDmLock:I

.field protected mIsOmaDmSession:I

.field protected mIsPsRestricted:Z

.field protected mIsScreenOn:Z

.field protected mIsWifiConnected:Z

.field public mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

.field protected mNetStatPollEnabled:Z

.field protected mNetStatPollPeriod:I

.field protected mNoRecvPollCount:I

.field protected mPhone:Lcom/android/internal/telephony/PhoneBase;

.field private mPollNetStat:Ljava/lang/Runnable;

.field public mPreferredApn:Lcom/android/internal/telephony/DataProfile;

.field protected mReconnectIntent:Landroid/app/PendingIntent;

.field public mReconnectIntentForDefaultType:Landroid/content/Intent;

.field protected mRequestedApnType:Ljava/lang/String;

.field protected mResolver:Landroid/content/ContentResolver;

.field public mRomaingPreferredApn:Lcom/android/internal/telephony/DataProfile;

.field protected mRxPkts:J

.field protected mSentSinceLastRecv:J

.field protected mState:Lcom/android/internal/telephony/DctConstants$State;

.field protected mTxPkts:J

.field protected mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

.field protected mUniqueIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

.field protected mUserDataEnabled:Z

.field public mVolteSupport:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

.field protected modeChangeAlarmState:Z

.field public payPopUp_kr:Lcom/android/internal/telephony/PayPopup_Korea;

.field protected roamingOnforResponse:Z

.field public spr_roamingBearer:I

.field tx_onlycount:J


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 199
    const-string v0, "persist.radio.apn_delay"

    #@2
    const/16 v1, 0x1388

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    sput v0, Lcom/android/internal/telephony/DataConnectionTracker;->APN_DELAY_MILLIS:I

    #@a
    .line 214
    const/4 v0, 0x1

    #@b
    sput-boolean v0, Lcom/android/internal/telephony/DataConnectionTracker;->sPolicyDataEnabled:Z

    #@d
    .line 359
    const/4 v0, 0x0

    #@e
    sput-boolean v0, Lcom/android/internal/telephony/DataConnectionTracker;->voice_call_ing:Z

    #@10
    .line 399
    new-instance v0, Lcom/android/internal/telephony/DataConnectionTracker$1;

    #@12
    invoke-direct {v0}, Lcom/android/internal/telephony/DataConnectionTracker$1;-><init>()V

    #@15
    sput-object v0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnPriorities:Ljava/util/LinkedHashMap;

    #@17
    .line 2302
    new-instance v0, Lcom/android/internal/telephony/DataConnectionTracker$4;

    #@19
    invoke-direct {v0}, Lcom/android/internal/telephony/DataConnectionTracker$4;-><init>()V

    #@1c
    sput-object v0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnPrioComparator:Ljava/util/Comparator;

    #@1e
    return-void
.end method

.method protected constructor <init>(Lcom/android/internal/telephony/PhoneBase;)V
    .registers 13
    .parameter "phone"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    const/4 v8, 0x1

    #@3
    const/4 v10, 0x0

    #@4
    const/4 v9, 0x0

    #@5
    .line 1026
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@8
    .line 202
    new-instance v0, Ljava/lang/Object;

    #@a
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataEnabledLock:Ljava/lang/Object;

    #@f
    .line 206
    iput-boolean v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mInternalDataEnabled:Z

    #@11
    .line 211
    iput-boolean v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@13
    .line 216
    const/16 v0, 0x12

    #@15
    new-array v0, v0, [Z

    #@17
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@19
    .line 218
    iput v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@1b
    .line 221
    const-string v0, "default"

    #@1d
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRequestedApnType:Ljava/lang/String;

    #@1f
    .line 289
    const-string v0, "gsm.radioreset"

    #@21
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->RADIO_RESET_PROPERTY:Ljava/lang/String;

    #@23
    .line 308
    iput v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupCounter:I

    #@25
    .line 310
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->ERROR_UNSPECIFIED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@27
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@29
    .line 315
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->modeChangeAlarmState:Z

    #@2b
    .line 316
    iput-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDelayModeChangeforIms:Landroid/app/PendingIntent;

    #@2d
    .line 318
    new-instance v0, Landroid/os/RegistrantList;

    #@2f
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@32
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectRegistrants:Landroid/os/RegistrantList;

    #@34
    .line 324
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    #@36
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    #@39
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@3b
    .line 325
    sget-object v0, Lcom/android/internal/telephony/DctConstants$Activity;->NONE:Lcom/android/internal/telephony/DctConstants$Activity;

    #@3d
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActivity:Lcom/android/internal/telephony/DctConstants$Activity;

    #@3f
    .line 326
    sget-object v0, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@41
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@43
    .line 328
    iput-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionTracker:Landroid/os/Handler;

    #@45
    .line 334
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollEnabled:Z

    #@47
    .line 336
    new-instance v0, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

    #@49
    move-object v1, p0

    #@4a
    move-wide v4, v2

    #@4b
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;-><init>(Lcom/android/internal/telephony/DataConnectionTracker;JJ)V

    #@4e
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallTxRxSum:Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

    #@50
    .line 338
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@53
    move-result-wide v0

    #@54
    long-to-int v0, v0

    #@55
    iput v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmTag:I

    #@57
    .line 340
    iput-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@59
    .line 344
    iput v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mNoRecvPollCount:I

    #@5b
    .line 347
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@5d
    .line 350
    iput-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntent:Landroid/app/PendingIntent;

    #@5f
    .line 356
    iput-wide v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->tx_onlycount:J

    #@61
    .line 371
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAutoAttachOnCreation:Z

    #@63
    .line 376
    iput-boolean v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsScreenOn:Z

    #@65
    .line 379
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@67
    invoke-direct {v0, v9}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@6a
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUniqueIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

    #@6c
    .line 382
    new-instance v0, Ljava/util/HashMap;

    #@6e
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@71
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@73
    .line 386
    new-instance v0, Ljava/util/HashMap;

    #@75
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@78
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionAsyncChannels:Ljava/util/HashMap;

    #@7a
    .line 390
    new-instance v0, Ljava/util/HashMap;

    #@7c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@7f
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnToDataConnectionId:Ljava/util/HashMap;

    #@81
    .line 394
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    #@83
    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    #@86
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@88
    .line 439
    const-string v0, "android.intent.action.MOBILE_DATA_ROAMING_OPTION_REQUEST"

    #@8a
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->ACTION_MOBILE_DATA_ROAMING_OPTION_REQUEST:Ljava/lang/String;

    #@8c
    .line 440
    const-string v0, "android.intent.action.MOBILE_DATA_ROAMING_OPTION_CANCEL"

    #@8e
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->ACTION_MOBILE_DATA_ROAMING_OPTION_CANCEL:Ljava/lang/String;

    #@90
    .line 441
    const-string v0, "requestRoamingOption"

    #@92
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->REQUEST_ROAMING_OPTION:Ljava/lang/String;

    #@94
    .line 443
    const-string v0, "android.intent.action.MOBILE_DATA_ROAMING_STATE_CHANGE_REQUEST"

    #@96
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->ACTION_MOBILE_DATA_ROAMING_STATE_CHANGE_REQUEST:Ljava/lang/String;

    #@98
    .line 444
    const-string v0, "requestState"

    #@9a
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->REQUEST_STATE:Ljava/lang/String;

    #@9c
    .line 445
    const-string v0, "android.intent.action.ENABLE_DATA_IN_HPLMN"

    #@9e
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->ACTION_ENABLE_DATA_IN_HPLMN:Ljava/lang/String;

    #@a0
    .line 446
    const-string v0, "android.intent.action.MOBILE_DATA_IS_ON_RESPONSE"

    #@a2
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->ACTION_ENABLE_DATA_IN_HPLMN_RESPONSE:Ljava/lang/String;

    #@a4
    .line 447
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->roamingOnforResponse:Z

    #@a6
    .line 448
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->isDataonPopupShown:Z

    #@a8
    .line 456
    iput-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@aa
    .line 462
    iput-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@ac
    .line 466
    const-string v0, "cinet.spcs"

    #@ae
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->SPR_GsmGlobalAPN:Ljava/lang/String;

    #@b0
    .line 467
    const-string v0, "gsmtest"

    #@b2
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->SPR_GsmGlobalAPN_TEST:Ljava/lang/String;

    #@b4
    .line 468
    iput-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDomesticPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@b6
    .line 469
    iput-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRomaingPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@b8
    .line 474
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsPsRestricted:Z

    #@ba
    .line 477
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsDisposed:Z

    #@bc
    .line 485
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->isAPNDataBlock:Z

    #@be
    .line 486
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->datablockbyadmin:Z

    #@c0
    .line 489
    iput v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->OldisDefaultCount:I

    #@c2
    .line 493
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->blockForDun:Z

    #@c4
    .line 503
    iput v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmLock:I

    #@c6
    .line 504
    iput v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsDataProfileActive:I

    #@c8
    .line 505
    iput v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataProfile:I

    #@ca
    .line 506
    iput v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmSession:I

    #@cc
    .line 510
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_send_result:Z

    #@ce
    .line 511
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@d0
    .line 522
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->imsRegiState:Z

    #@d2
    .line 527
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;->NONE:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@d4
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mVolteSupport:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@d6
    .line 528
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;->NONE:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@d8
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mEPDNSupport:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@da
    .line 529
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->NONE:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@dc
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mEmerAttachSupport:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@de
    .line 530
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->NONE:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@e0
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mEPDNBarring:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@e2
    .line 532
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->NONE:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@e4
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mEmerCampedCID:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@e6
    .line 533
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->NONE:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@e8
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mEmerCampedTAC:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@ea
    .line 535
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->NONE:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@ec
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mEmcFailCause:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@ee
    .line 536
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->NONE:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@f0
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@f2
    .line 541
    iput-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntentForDefaultType:Landroid/content/Intent;

    #@f4
    .line 545
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->internetPDNconnected:Z

    #@f6
    .line 552
    iput-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->SUPPORT_LG_DATA_RECOVERY:Z

    #@f8
    .line 575
    new-instance v0, Lcom/android/internal/telephony/DataConnectionTracker$2;

    #@fa
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/DataConnectionTracker$2;-><init>(Lcom/android/internal/telephony/DataConnectionTracker;)V

    #@fd
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@ff
    .line 900
    new-instance v0, Lcom/android/internal/telephony/DataConnectionTracker$3;

    #@101
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/DataConnectionTracker$3;-><init>(Lcom/android/internal/telephony/DataConnectionTracker;)V

    #@104
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPollNetStat:Ljava/lang/Runnable;

    #@106
    .line 1027
    const-string v0, "DCT.constructor"

    #@108
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@10b
    .line 1028
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@10d
    .line 1029
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@110
    move-result-object v0

    #@111
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@113
    .line 1030
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@115
    const v1, 0x42021

    #@118
    invoke-virtual {v0, p0, v1, v10}, Lcom/android/internal/telephony/uicc/UiccController;->registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@11b
    .line 1032
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@11d
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@11f
    const v1, 0x42022

    #@122
    invoke-interface {v0, p0, v1, v10}, Lcom/android/internal/telephony/CommandsInterface;->registerForTetheredModeStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@125
    .line 1035
    new-instance v0, Lcom/android/internal/telephony/PayPopup_Korea;

    #@127
    invoke-direct {v0, p0, p1}, Lcom/android/internal/telephony/PayPopup_Korea;-><init>(Lcom/android/internal/telephony/DataConnectionTracker;Lcom/android/internal/telephony/PhoneBase;)V

    #@12a
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->payPopUp_kr:Lcom/android/internal/telephony/PayPopup_Korea;

    #@12c
    .line 1036
    new-instance v0, Lcom/android/internal/telephony/ApnSelectionHandler;

    #@12e
    invoke-direct {v0, p0, p1}, Lcom/android/internal/telephony/ApnSelectionHandler;-><init>(Lcom/android/internal/telephony/DataConnectionTracker;Lcom/android/internal/telephony/PhoneBase;)V

    #@131
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@133
    .line 1040
    const-string v0, "persist.telephony.datarecovery"

    #@135
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@137
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@139
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@13c
    move-result-object v1

    #@13d
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@13f
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@142
    move-result v0

    #@143
    iput-boolean v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->SUPPORT_LG_DATA_RECOVERY:Z

    #@145
    .line 1042
    new-instance v0, Ljava/lang/StringBuilder;

    #@147
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@14a
    const-string v1, "SUPPORT_LG_DATA_RECOVERY = "

    #@14c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v0

    #@150
    iget-boolean v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->SUPPORT_LG_DATA_RECOVERY:Z

    #@152
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@155
    move-result-object v0

    #@156
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@159
    move-result-object v0

    #@15a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@15d
    .line 1045
    new-instance v6, Landroid/content/IntentFilter;

    #@15f
    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    #@162
    .line 1046
    .local v6, filter:Landroid/content/IntentFilter;
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getActionIntentReconnectAlarm()Ljava/lang/String;

    #@165
    move-result-object v0

    #@166
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@169
    .line 1047
    const-string v0, "android.intent.action.SCREEN_ON"

    #@16b
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@16e
    .line 1048
    const-string v0, "android.intent.action.SCREEN_OFF"

    #@170
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@173
    .line 1049
    const-string v0, "android.net.wifi.STATE_CHANGE"

    #@175
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@178
    .line 1050
    const-string v0, "android.net.wifi.WIFI_STATE_CHANGED"

    #@17a
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17d
    .line 1051
    const-string v0, "com.android.internal.telephony.dataconnectiontracker.intent_set_fail_data_setup_counter"

    #@17f
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@182
    .line 1053
    iget-boolean v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->SUPPORT_LG_DATA_RECOVERY:Z

    #@184
    if-nez v0, :cond_18d

    #@186
    .line 1055
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getActionIntentDataStallAlarm()Ljava/lang/String;

    #@189
    move-result-object v0

    #@18a
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@18d
    .line 1057
    :cond_18d
    const-string v0, "com.lge.callingsetmobile"

    #@18f
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@192
    .line 1060
    const-string v0, "android.intent.action.ACTION_DELAY_MODE_CHANGE_FOR_IMS"

    #@194
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@197
    .line 1063
    const-string v0, "android.intent.action.IPV6_STATUS"

    #@199
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@19c
    .line 1066
    const-string v0, "lge.intent.action.LTE_NETWORK_SUPPORTED_INFO"

    #@19e
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1a1
    .line 1067
    const-string v0, "lge.intent.action.LTE_NETWORK_SIB_INFO"

    #@1a3
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1a6
    .line 1068
    const-string v0, "lge.intent.action.DATA_EMERGENCY_FAILED"

    #@1a8
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1ab
    .line 1069
    const-string v0, "lge.intent.action.LTE_STATE_INFO"

    #@1ad
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1b0
    .line 1073
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1b2
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1b4
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1b7
    move-result-object v0

    #@1b8
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_ROAMING_POPUP_TMUS:Z

    #@1ba
    if-eqz v0, :cond_1c6

    #@1bc
    .line 1074
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->ACTION_MOBILE_DATA_ROAMING_STATE_CHANGE_REQUEST:Ljava/lang/String;

    #@1be
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c1
    .line 1075
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->ACTION_ENABLE_DATA_IN_HPLMN_RESPONSE:Ljava/lang/String;

    #@1c3
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c6
    .line 1082
    :cond_1c6
    sget-boolean v0, Lcom/android/internal/telephony/LGfeature;->omadmDataBlock:Z

    #@1c8
    if-ne v0, v8, :cond_1ed

    #@1ca
    .line 1083
    const-string v0, "android.intent.action.OMADM_DEVICE_LOCK_MSG"

    #@1cc
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1cf
    .line 1084
    const-string v0, "android.intent.action.DEVICE_UNLOCKED_MSG"

    #@1d1
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d4
    .line 1086
    const-string v0, "android.intent.action.REQUEST_START_OMADM_SESSION_MSG"

    #@1d6
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d9
    .line 1087
    const-string v0, "android.intent.action.REQUEST_END_OMADM_SESSION_MSG"

    #@1db
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1de
    .line 1088
    const-string v0, "android.intent.action.REQUEST_FOR_OMADM_DATA_SETUP"

    #@1e0
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e3
    .line 1089
    const-string v0, "android.intent.action.REQUEST_FOR_OMADM_DATA_DISCONNECT"

    #@1e5
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e8
    .line 1090
    const-string v0, "android.intent.action.REQUEST_FOR_OMADM_DATA_CONNECT"

    #@1ea
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1ed
    .line 1097
    :cond_1ed
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1ef
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1f2
    move-result-object v0

    #@1f3
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1f5
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1f7
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1fa
    move-result-object v1

    #@1fb
    invoke-static {v0, v1}, Lcom/android/internal/telephony/DataConnectionManager;->getInstance(Landroid/content/Context;Lcom/android/internal/telephony/LGfeature;)Lcom/android/internal/telephony/DataConnectionManager;

    #@1fe
    move-result-object v0

    #@1ff
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@201
    .line 1101
    const-string v0, "com.kddi.android.cpa_CHANGED"

    #@203
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@206
    .line 1102
    const-string v0, "cpa_onSetupConnectionCompleted"

    #@208
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@20b
    .line 1105
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@20d
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@210
    move-result-object v0

    #@211
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@214
    move-result-object v0

    #@215
    const-string v1, "mobile_data"

    #@217
    invoke-static {v0, v1, v8}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@21a
    move-result v0

    #@21b
    if-ne v0, v8, :cond_2b4

    #@21d
    move v0, v8

    #@21e
    :goto_21e
    iput-boolean v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@220
    .line 1111
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@222
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@225
    move-result-object v0

    #@226
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@228
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@22a
    invoke-virtual {v0, v1, v6, v10, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@22d
    .line 1116
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@22f
    const-string v1, "net.def_data_on_boot"

    #@231
    invoke-static {v1, v8}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@234
    move-result v1

    #@235
    aput-boolean v1, v0, v9

    #@237
    .line 1118
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@239
    aget-boolean v0, v0, v9

    #@23b
    if-eqz v0, :cond_243

    #@23d
    .line 1119
    iget v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@23f
    add-int/lit8 v0, v0, 0x1

    #@241
    iput v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@243
    .line 1122
    :cond_243
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@245
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@248
    move-result-object v0

    #@249
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@24c
    move-result-object v7

    #@24d
    .line 1123
    .local v7, sp:Landroid/content/SharedPreferences;
    const-string v0, "disabled_on_boot_key"

    #@24f
    invoke-interface {v7, v0, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    #@252
    move-result v0

    #@253
    iput-boolean v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAutoAttachOnCreation:Z

    #@255
    .line 1126
    new-instance v0, Lcom/android/internal/telephony/DataConnectionTracker$DataRoamingSettingObserver;

    #@257
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@259
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker$DataRoamingSettingObserver;-><init>(Lcom/android/internal/telephony/DataConnectionTracker;Landroid/os/Handler;)V

    #@25c
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataRoamingSettingObserver:Lcom/android/internal/telephony/DataConnectionTracker$DataRoamingSettingObserver;

    #@25e
    .line 1127
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataRoamingSettingObserver:Lcom/android/internal/telephony/DataConnectionTracker$DataRoamingSettingObserver;

    #@260
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@262
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@265
    move-result-object v1

    #@266
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataConnectionTracker$DataRoamingSettingObserver;->register(Landroid/content/Context;)V

    #@269
    .line 1129
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@26b
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@26e
    move-result-object v0

    #@26f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@272
    move-result-object v0

    #@273
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mResolver:Landroid/content/ContentResolver;

    #@275
    .line 1130
    new-instance v0, Lcom/android/internal/telephony/DataConnectionTracker$ApnChangeObserver;

    #@277
    invoke-direct {v0, p0, p0}, Lcom/android/internal/telephony/DataConnectionTracker$ApnChangeObserver;-><init>(Lcom/android/internal/telephony/DataConnectionTracker;Landroid/os/Handler;)V

    #@27a
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnObserver:Lcom/android/internal/telephony/DataConnectionTracker$ApnChangeObserver;

    #@27c
    .line 1131
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@27e
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@281
    move-result-object v0

    #@282
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@285
    move-result-object v0

    #@286
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@288
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnObserver:Lcom/android/internal/telephony/DataConnectionTracker$ApnChangeObserver;

    #@28a
    invoke-virtual {v0, v1, v8, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@28d
    .line 1135
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@28f
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@291
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@294
    move-result-object v0

    #@295
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TOOL_DATA_BLOCK_HIDDEN_MENU:Z

    #@297
    if-eqz v0, :cond_2b3

    #@299
    .line 1136
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@29b
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@29e
    move-result-object v0

    #@29f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2a2
    move-result-object v0

    #@2a3
    const-string v1, "irat_test_mode"

    #@2a5
    invoke-static {v0, v1, v9}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2a8
    move-result v0

    #@2a9
    if-ne v0, v8, :cond_2b3

    #@2ab
    .line 1137
    const-string v0, "[DATA_AFW] It\'s IRAT Test Mode. We Will Data Block"

    #@2ad
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@2b0
    .line 1138
    invoke-direct {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->dataBlock()V

    #@2b3
    .line 1143
    :cond_2b3
    return-void

    #@2b4
    .end local v7           #sp:Landroid/content/SharedPreferences;
    :cond_2b4
    move v0, v9

    #@2b5
    .line 1105
    goto/16 :goto_21e
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/DataConnectionTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 133
    invoke-direct {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->handleDataOnRoamingChange()V

    #@3
    return-void
.end method

.method private dataBlock()V
    .registers 7

    #@0
    .prologue
    .line 1148
    const-string v4, "network_management"

    #@2
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v1

    #@6
    .line 1149
    .local v1, b:Landroid/os/IBinder;
    invoke-static {v1}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@9
    move-result-object v3

    #@a
    .line 1150
    .local v3, service:Landroid/os/INetworkManagementService;
    if-nez v3, :cond_12

    #@c
    .line 1151
    const-string v4, "[DATA_AFW] service is null, just return"

    #@e
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@11
    .line 1179
    :goto_11
    return-void

    #@12
    .line 1155
    :cond_12
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@14
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b
    move-result-object v4

    #@1c
    const-string v5, "data_accept_info"

    #@1e
    invoke-static {v4, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    .line 1157
    .local v0, acceptIpInfo:Ljava/lang/String;
    if-nez v0, :cond_26

    #@24
    .line 1158
    const-string v0, "192.168.0.70"

    #@26
    .line 1161
    :cond_26
    new-instance v4, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v5, "[DATA_AFW] accept IP Info is : "

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@3c
    .line 1164
    :try_start_3c
    const-string v4, "-F oem_out"

    #@3e
    invoke-interface {v3, v4}, Landroid/os/INetworkManagementService;->setMdmIptables(Ljava/lang/String;)V

    #@41
    .line 1165
    const-string v4, "-A oem_out -o usb0 -j ACCEPT"

    #@43
    invoke-interface {v3, v4}, Landroid/os/INetworkManagementService;->setMdmIptables(Ljava/lang/String;)V

    #@46
    .line 1167
    new-instance v4, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v5, "-A oem_out -d "

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    const-string v5, " -j ACCEPT"

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v4

    #@5f
    invoke-interface {v3, v4}, Landroid/os/INetworkManagementService;->setMdmIptables(Ljava/lang/String;)V

    #@62
    .line 1168
    const-string v4, "-A oem_out -p udp --dport 67:68 -j ACCEPT"

    #@64
    invoke-interface {v3, v4}, Landroid/os/INetworkManagementService;->setMdmIptables(Ljava/lang/String;)V

    #@67
    .line 1169
    const-string v4, "-A oem_out -j DROP"

    #@69
    invoke-interface {v3, v4}, Landroid/os/INetworkManagementService;->setMdmIptables(Ljava/lang/String;)V

    #@6c
    .line 1171
    const-string v4, "-F oem_fwd"

    #@6e
    invoke-interface {v3, v4}, Landroid/os/INetworkManagementService;->setMdmIptables(Ljava/lang/String;)V

    #@71
    .line 1173
    new-instance v4, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v5, "-A oem_fwd -i usb0 -d "

    #@78
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    const-string v5, " -j ACCEPT"

    #@82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    invoke-interface {v3, v4}, Landroid/os/INetworkManagementService;->setMdmIptables(Ljava/lang/String;)V

    #@8d
    .line 1174
    const-string v4, "-A oem_fwd -i usb0 -j DROP"

    #@8f
    invoke-interface {v3, v4}, Landroid/os/INetworkManagementService;->setMdmIptables(Ljava/lang/String;)V
    :try_end_92
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_92} :catch_94

    #@92
    goto/16 :goto_11

    #@94
    .line 1176
    :catch_94
    move-exception v2

    #@95
    .line 1177
    .local v2, e:Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v5, "[MdmInit] Fail to runDataCommand %s"

    #@9c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v4

    #@a0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v4

    #@a4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v4

    #@a8
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@ab
    goto/16 :goto_11
.end method

.method private handleDataOnRoamingChange()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1330
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_49

    #@e
    .line 1331
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_17

    #@14
    .line 1332
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->resetAllRetryCounts()V

    #@17
    .line 1336
    :cond_17
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@19
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1b
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1e
    move-result-object v0

    #@1f
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_KT:Z

    #@21
    if-eqz v0, :cond_3f

    #@23
    .line 1337
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_4a

    #@29
    .line 1339
    const-string v0, "getDataOnRoamingEnabled() = true , set mUserDataEnabled = true "

    #@2b
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@2e
    .line 1342
    iput-boolean v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@30
    .line 1343
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@32
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@35
    move-result-object v0

    #@36
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@39
    move-result-object v0

    #@3a
    const-string v1, "mobile_data"

    #@3c
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@3f
    .line 1356
    :cond_3f
    :goto_3f
    const v0, 0x4200b

    #@42
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@45
    move-result-object v0

    #@46
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@49
    .line 1358
    :cond_49
    return-void

    #@4a
    .line 1347
    :cond_4a
    const-string v0, "getDataOnRoamingEnabled() = false , set mUserDataEnabled = false"

    #@4c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@4f
    .line 1349
    iput-boolean v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@51
    .line 1350
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@53
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5a
    move-result-object v0

    #@5b
    const-string v1, "mobile_data"

    #@5d
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@60
    goto :goto_3f
.end method

.method private handleGetPreferredNetworkTypeAPNChangeResponse(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 2992
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/os/AsyncResult;

    #@4
    .line 2994
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@6
    if-nez v3, :cond_5c

    #@8
    .line 2996
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@a
    check-cast v3, [I

    #@c
    check-cast v3, [I

    #@e
    const/4 v4, 0x0

    #@f
    aget v2, v3, v4

    #@11
    .line 2998
    .local v2, modemNetworkMode:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "[APNCHANGE] : modemNetworkMode = "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@27
    .line 3000
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredNetworkMode()I

    #@2a
    move-result v1

    #@2b
    .line 3002
    .local v1, curPreferMode:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v4, "[APNCHANGE]: curPreferMode = "

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@41
    .line 3005
    if-eq v2, v1, :cond_5c

    #@43
    .line 3007
    new-instance v3, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v4, "[APNCHANGE] setPreferredNetworkMode set to = "

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@59
    .line 3009
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredNetworkMode(I)V

    #@5c
    .line 3012
    .end local v1           #curPreferMode:I
    .end local v2           #modemNetworkMode:I
    :cond_5c
    return-void
.end method

.method private isContainingNumericInDB(Ljava/lang/String;)Z
    .registers 10
    .parameter "numeric"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 4256
    const/4 v7, 0x0

    #@2
    .line 4257
    .local v7, exist:Z
    const-string v0, "content://telephony/dcm_settings"

    #@4
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v1

    #@8
    .line 4258
    .local v1, DCM_SETTINGS_URI:Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v5, "numeric = \'"

    #@f
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v5, "\'"

    #@19
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    .line 4259
    .local v3, selection:Ljava/lang/String;
    const/4 v0, 0x1

    #@22
    new-array v2, v0, [Ljava/lang/String;

    #@24
    const/4 v0, 0x0

    #@25
    const-string v5, "_id"

    #@27
    aput-object v5, v2, v0

    #@29
    .line 4260
    .local v2, columns:[Ljava/lang/String;
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2b
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@32
    move-result-object v0

    #@33
    move-object v5, v4

    #@34
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@37
    move-result-object v6

    #@38
    .line 4261
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_41

    #@3a
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@3d
    move-result v0

    #@3e
    if-lez v0, :cond_41

    #@40
    .line 4263
    const/4 v7, 0x1

    #@41
    .line 4265
    :cond_41
    if-eqz v6, :cond_46

    #@43
    .line 4266
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@46
    .line 4268
    :cond_46
    return v7
.end method

.method private isValidNetworkMode(I)Z
    .registers 5
    .parameter "nwMode"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2926
    if-eq p1, v0, :cond_28

    #@3
    const/4 v1, 0x2

    #@4
    if-eq p1, v1, :cond_28

    #@6
    const/16 v1, 0x9

    #@8
    if-eq p1, v1, :cond_28

    #@a
    const/16 v1, 0xb

    #@c
    if-eq p1, v1, :cond_28

    #@e
    const/4 v1, 0x3

    #@f
    if-eq p1, v1, :cond_28

    #@11
    const/16 v1, 0xc

    #@13
    if-eq p1, v1, :cond_28

    #@15
    const/4 v1, 0x4

    #@16
    if-eq p1, v1, :cond_28

    #@18
    const/4 v1, 0x5

    #@19
    if-eq p1, v1, :cond_28

    #@1b
    const/4 v1, 0x6

    #@1c
    if-eq p1, v1, :cond_28

    #@1e
    const/16 v1, 0x8

    #@20
    if-eq p1, v1, :cond_28

    #@22
    const/16 v1, 0xa

    #@24
    if-eq p1, v1, :cond_28

    #@26
    if-nez p1, :cond_3f

    #@28
    .line 2943
    :cond_28
    new-instance v1, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v2, "[LGE] isValidNetworkMode = "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@3e
    .line 2947
    :goto_3e
    return v0

    #@3f
    :cond_3f
    const/4 v0, 0x0

    #@40
    goto :goto_3e
.end method

.method public static networkModeToString(I)Ljava/lang/String;
    .registers 2
    .parameter "networkMode"

    #@0
    .prologue
    .line 2866
    packed-switch p0, :pswitch_data_30

    #@3
    .line 2910
    const-string v0, "Unexpected"

    #@5
    .line 2914
    .local v0, ntString:Ljava/lang/String;
    :goto_5
    return-object v0

    #@6
    .line 2868
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_6
    const-string v0, "WCDMA_PREF"

    #@8
    .line 2869
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@9
    .line 2871
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_9
    const-string v0, "GSM_ONLY"

    #@b
    .line 2872
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@c
    .line 2874
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_c
    const-string v0, "WCDMA_ONLY"

    #@e
    .line 2875
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@f
    .line 2877
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_f
    const-string v0, "GSM_UMTS"

    #@11
    .line 2878
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@12
    .line 2880
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_12
    const-string v0, "CDMA"

    #@14
    .line 2881
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@15
    .line 2883
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_15
    const-string v0, "CDMA_NO_EVDO"

    #@17
    .line 2884
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@18
    .line 2886
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_18
    const-string v0, "EVDO_NO_CDMA"

    #@1a
    .line 2887
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@1b
    .line 2889
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_1b
    const-string v0, "GLOBAL"

    #@1d
    .line 2890
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@1e
    .line 2892
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_1e
    const-string v0, "LTE_CDMA_EVDO"

    #@20
    .line 2893
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@21
    .line 2895
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_21
    const-string v0, "LTE_GSM_WCDMA"

    #@23
    .line 2896
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@24
    .line 2898
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_24
    const-string v0, "LTE_CMDA_EVDO_GSM_WCDMA"

    #@26
    .line 2899
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@27
    .line 2901
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_27
    const-string v0, "LTE_ONLY"

    #@29
    .line 2902
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@2a
    .line 2904
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_2a
    const-string v0, "LTE_WCDMA"

    #@2c
    .line 2905
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@2d
    .line 2907
    .end local v0           #ntString:Ljava/lang/String;
    :pswitch_2d
    const-string v0, "CDMA_WCDMA_GSM"

    #@2f
    .line 2908
    .restart local v0       #ntString:Ljava/lang/String;
    goto :goto_5

    #@30
    .line 2866
    :pswitch_data_30
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
        :pswitch_24
        :pswitch_27
        :pswitch_2a
        :pswitch_2d
    .end packed-switch
.end method

.method private notifyApnIdDisconnected(Ljava/lang/String;I)V
    .registers 6
    .parameter "reason"
    .parameter "apnId"

    #@0
    .prologue
    .line 1806
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@8
    invoke-virtual {v0, p1, v1, v2}, Lcom/android/internal/telephony/PhoneBase;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;)V

    #@b
    .line 1808
    return-void
.end method

.method private notifyApnIdUpToCurrent(Ljava/lang/String;I)V
    .registers 6
    .parameter "reason"
    .parameter "apnId"

    #@0
    .prologue
    .line 1785
    sget-object v0, Lcom/android/internal/telephony/DataConnectionTracker$5;->$SwitchMap$com$android$internal$telephony$DctConstants$State:[I

    #@2
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/telephony/DctConstants$State;->ordinal()I

    #@7
    move-result v1

    #@8
    aget v0, v0, v1

    #@a
    packed-switch v0, :pswitch_data_32

    #@d
    .line 1802
    :goto_d
    :pswitch_d
    return-void

    #@e
    .line 1791
    :pswitch_e
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@10
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTING:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@16
    invoke-virtual {v0, p1, v1, v2}, Lcom/android/internal/telephony/PhoneBase;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;)V

    #@19
    goto :goto_d

    #@1a
    .line 1796
    :pswitch_1a
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1c
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTING:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@22
    invoke-virtual {v0, p1, v1, v2}, Lcom/android/internal/telephony/PhoneBase;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;)V

    #@25
    .line 1798
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@27
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2d
    invoke-virtual {v0, p1, v1, v2}, Lcom/android/internal/telephony/PhoneBase;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;)V

    #@30
    goto :goto_d

    #@31
    .line 1785
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_d
        :pswitch_d
        :pswitch_e
        :pswitch_e
        :pswitch_1a
        :pswitch_1a
    .end packed-switch
.end method

.method private onTetheredModeStateChanged(Landroid/os/AsyncResult;)V
    .registers 8
    .parameter "ar"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2367
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4
    check-cast v2, [I

    #@6
    move-object v1, v2

    #@7
    check-cast v1, [I

    #@9
    .line 2369
    .local v1, ret:[I
    if-eqz v1, :cond_e

    #@b
    array-length v2, v1

    #@c
    if-eq v2, v5, :cond_14

    #@e
    .line 2371
    :cond_e
    const-string v2, "Error: Invalid Tethered mode received"

    #@10
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@13
    .line 2408
    :goto_13
    return-void

    #@14
    .line 2375
    :cond_14
    aget v0, v1, v4

    #@16
    .line 2377
    .local v0, mode:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "onTetheredModeStateChanged: mode:"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@2c
    .line 2379
    packed-switch v0, :pswitch_data_68

    #@2f
    .line 2406
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "Error: Invalid Tethered mode:"

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@45
    goto :goto_13

    #@46
    .line 2383
    :pswitch_46
    const-string v2, "Unsol Indication: RIL_TETHERED_MODE_ON"

    #@48
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@4b
    .line 2385
    iput-boolean v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->blockForDun:Z

    #@4d
    goto :goto_13

    #@4e
    .line 2390
    :pswitch_4e
    const-string v2, "Unsol Indication: RIL_TETHERED_MODE_OFF"

    #@50
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@53
    .line 2397
    iput-boolean v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->blockForDun:Z

    #@55
    .line 2399
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->resetAllRetryCounts()V

    #@58
    .line 2400
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->clearTetheredStateOnStatus()V

    #@5b
    .line 2401
    const v2, 0x42003

    #@5e
    const-string v3, "tetheredModeStateChanged"

    #@60
    invoke-virtual {p0, v2, v4, v4, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@63
    move-result-object v2

    #@64
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@67
    goto :goto_13

    #@68
    .line 2379
    :pswitch_data_68
    .packed-switch 0x0
        :pswitch_4e
        :pswitch_46
    .end packed-switch
.end method

.method private updateDataStallInfo()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const-wide/16 v9, 0x0

    #@3
    .line 2602
    new-instance v0, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

    #@5
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallTxRxSum:Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

    #@7
    invoke-direct {v0, p0, v5}, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;-><init>(Lcom/android/internal/telephony/DataConnectionTracker;Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;)V

    #@a
    .line 2603
    .local v0, preTxRxSum:Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallTxRxSum:Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

    #@c
    invoke-virtual {v5}, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->updateTxRxSum()V

    #@f
    .line 2606
    new-instance v5, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v6, "updateDataStallInfo: mDataStallTxRxSum="

    #@16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallTxRxSum:Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

    #@1c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v5

    #@20
    const-string v6, " preTxRxSum="

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@31
    .line 2610
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallTxRxSum:Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

    #@33
    iget-wide v5, v5, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->txPkts:J

    #@35
    iget-wide v7, v0, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->txPkts:J

    #@37
    sub-long v3, v5, v7

    #@39
    .line 2611
    .local v3, sent:J
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallTxRxSum:Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

    #@3b
    iget-wide v5, v5, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->rxPkts:J

    #@3d
    iget-wide v7, v0, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->rxPkts:J

    #@3f
    sub-long v1, v5, v7

    #@41
    .line 2619
    .local v1, received:J
    cmp-long v5, v3, v9

    #@43
    if-lez v5, :cond_54

    #@45
    cmp-long v5, v1, v9

    #@47
    if-lez v5, :cond_54

    #@49
    .line 2620
    const-string v5, "updateDataStallInfo: IN/OUT"

    #@4b
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@4e
    .line 2621
    iput-wide v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@50
    .line 2622
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/DataConnectionTracker;->putRecoveryAction(I)V

    #@53
    .line 2640
    :goto_53
    return-void

    #@54
    .line 2623
    :cond_54
    cmp-long v5, v3, v9

    #@56
    if-lez v5, :cond_91

    #@58
    cmp-long v5, v1, v9

    #@5a
    if-nez v5, :cond_91

    #@5c
    .line 2624
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5e
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@61
    move-result-object v5

    #@62
    sget-object v6, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@64
    if-ne v5, v6, :cond_8e

    #@66
    .line 2625
    iget-wide v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@68
    add-long/2addr v5, v3

    #@69
    iput-wide v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@6b
    .line 2630
    :goto_6b
    new-instance v5, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v6, "updateDataStallInfo: OUT sent="

    #@72
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v5

    #@76
    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    const-string v6, " mSentSinceLastRecv="

    #@7c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    iget-wide v6, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@82
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@85
    move-result-object v5

    #@86
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v5

    #@8a
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@8d
    goto :goto_53

    #@8e
    .line 2627
    :cond_8e
    iput-wide v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@90
    goto :goto_6b

    #@91
    .line 2633
    :cond_91
    cmp-long v5, v3, v9

    #@93
    if-nez v5, :cond_a4

    #@95
    cmp-long v5, v1, v9

    #@97
    if-lez v5, :cond_a4

    #@99
    .line 2634
    const-string v5, "updateDataStallInfo: IN"

    #@9b
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@9e
    .line 2635
    iput-wide v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@a0
    .line 2636
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/DataConnectionTracker;->putRecoveryAction(I)V

    #@a3
    goto :goto_53

    #@a4
    .line 2638
    :cond_a4
    const-string v5, "updateDataStallInfo: NONE"

    #@a6
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@a9
    goto :goto_53
.end method


# virtual methods
.method protected abstract CPAChanged()V
.end method

.method public DataOnRoamingEnabled_OnlySel(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 2859
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v1

    #@a
    const-string v2, "data_roaming"

    #@c
    if-eqz p1, :cond_13

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@12
    .line 2860
    return-void

    #@13
    .line 2859
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_f
.end method

.method protected apnIdToType(I)Ljava/lang/String;
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 1690
    packed-switch p1, :pswitch_data_58

    #@3
    .line 1744
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v1, "Unknown id ("

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, ") in apnIdToType"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1f
    .line 1745
    const-string v0, "default"

    #@21
    :goto_21
    return-object v0

    #@22
    .line 1692
    :pswitch_22
    const-string v0, "default"

    #@24
    goto :goto_21

    #@25
    .line 1694
    :pswitch_25
    const-string v0, "mms"

    #@27
    goto :goto_21

    #@28
    .line 1696
    :pswitch_28
    const-string v0, "supl"

    #@2a
    goto :goto_21

    #@2b
    .line 1698
    :pswitch_2b
    const-string v0, "dun"

    #@2d
    goto :goto_21

    #@2e
    .line 1700
    :pswitch_2e
    const-string v0, "hipri"

    #@30
    goto :goto_21

    #@31
    .line 1702
    :pswitch_31
    const-string v0, "ims"

    #@33
    goto :goto_21

    #@34
    .line 1704
    :pswitch_34
    const-string v0, "fota"

    #@36
    goto :goto_21

    #@37
    .line 1706
    :pswitch_37
    const-string v0, "cbs"

    #@39
    goto :goto_21

    #@3a
    .line 1709
    :pswitch_3a
    const-string v0, "entitlement"

    #@3c
    goto :goto_21

    #@3d
    .line 1713
    :pswitch_3d
    const-string v0, "admin"

    #@3f
    goto :goto_21

    #@40
    .line 1715
    :pswitch_40
    const-string v0, "vzwapp"

    #@42
    goto :goto_21

    #@43
    .line 1717
    :pswitch_43
    const-string v0, "vzw800"

    #@45
    goto :goto_21

    #@46
    .line 1721
    :pswitch_46
    const-string v0, "bip"

    #@48
    goto :goto_21

    #@49
    .line 1725
    :pswitch_49
    const-string v0, "tethering"

    #@4b
    goto :goto_21

    #@4c
    .line 1729
    :pswitch_4c
    const-string v0, "ktmultirab1"

    #@4e
    goto :goto_21

    #@4f
    .line 1732
    :pswitch_4f
    const-string v0, "ktmultirab2"

    #@51
    goto :goto_21

    #@52
    .line 1737
    :pswitch_52
    const-string v0, "emergency"

    #@54
    goto :goto_21

    #@55
    .line 1741
    :pswitch_55
    const-string v0, "rcs"

    #@57
    goto :goto_21

    #@58
    .line 1690
    :pswitch_data_58
    .packed-switch 0x0
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_2b
        :pswitch_2e
        :pswitch_31
        :pswitch_34
        :pswitch_37
        :pswitch_3d
        :pswitch_40
        :pswitch_3a
        :pswitch_43
        :pswitch_49
        :pswitch_4c
        :pswitch_4f
        :pswitch_46
        :pswitch_52
        :pswitch_55
    .end packed-switch
.end method

.method public apnTypeToId(Ljava/lang/String;)I
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 1624
    const-string v0, "default"

    #@2
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 1625
    const/4 v0, 0x0

    #@9
    .line 1685
    :goto_9
    return v0

    #@a
    .line 1626
    :cond_a
    const-string v0, "mms"

    #@c
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    .line 1627
    const/4 v0, 0x1

    #@13
    goto :goto_9

    #@14
    .line 1628
    :cond_14
    const-string v0, "supl"

    #@16
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_1e

    #@1c
    .line 1629
    const/4 v0, 0x2

    #@1d
    goto :goto_9

    #@1e
    .line 1630
    :cond_1e
    const-string v0, "dun"

    #@20
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_28

    #@26
    .line 1631
    const/4 v0, 0x3

    #@27
    goto :goto_9

    #@28
    .line 1632
    :cond_28
    const-string v0, "hipri"

    #@2a
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_32

    #@30
    .line 1633
    const/4 v0, 0x4

    #@31
    goto :goto_9

    #@32
    .line 1634
    :cond_32
    const-string v0, "ims"

    #@34
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_3c

    #@3a
    .line 1635
    const/4 v0, 0x5

    #@3b
    goto :goto_9

    #@3c
    .line 1636
    :cond_3c
    const-string v0, "fota"

    #@3e
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@41
    move-result v0

    #@42
    if-eqz v0, :cond_46

    #@44
    .line 1637
    const/4 v0, 0x6

    #@45
    goto :goto_9

    #@46
    .line 1638
    :cond_46
    const-string v0, "cbs"

    #@48
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@4b
    move-result v0

    #@4c
    if-eqz v0, :cond_50

    #@4e
    .line 1639
    const/4 v0, 0x7

    #@4f
    goto :goto_9

    #@50
    .line 1641
    :cond_50
    const-string v0, "entitlement"

    #@52
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@55
    move-result v0

    #@56
    if-eqz v0, :cond_5b

    #@58
    .line 1642
    const/16 v0, 0xa

    #@5a
    goto :goto_9

    #@5b
    .line 1645
    :cond_5b
    const-string v0, "admin"

    #@5d
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@60
    move-result v0

    #@61
    if-eqz v0, :cond_66

    #@63
    .line 1646
    const/16 v0, 0x8

    #@65
    goto :goto_9

    #@66
    .line 1648
    :cond_66
    const-string v0, "vzwapp"

    #@68
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@6b
    move-result v0

    #@6c
    if-eqz v0, :cond_71

    #@6e
    .line 1649
    const/16 v0, 0x9

    #@70
    goto :goto_9

    #@71
    .line 1651
    :cond_71
    const-string v0, "vzw800"

    #@73
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@76
    move-result v0

    #@77
    if-eqz v0, :cond_7c

    #@79
    .line 1652
    const/16 v0, 0xb

    #@7b
    goto :goto_9

    #@7c
    .line 1656
    :cond_7c
    const-string v0, "tethering"

    #@7e
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@81
    move-result v0

    #@82
    if-eqz v0, :cond_87

    #@84
    .line 1657
    const/16 v0, 0xc

    #@86
    goto :goto_9

    #@87
    .line 1662
    :cond_87
    const-string v0, "ktmultirab1"

    #@89
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@8c
    move-result v0

    #@8d
    if-eqz v0, :cond_93

    #@8f
    .line 1663
    const/16 v0, 0xd

    #@91
    goto/16 :goto_9

    #@93
    .line 1666
    :cond_93
    const-string v0, "ktmultirab2"

    #@95
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@98
    move-result v0

    #@99
    if-eqz v0, :cond_9f

    #@9b
    .line 1667
    const/16 v0, 0xe

    #@9d
    goto/16 :goto_9

    #@9f
    .line 1671
    :cond_9f
    const-string v0, "emergency"

    #@a1
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@a4
    move-result v0

    #@a5
    if-eqz v0, :cond_ab

    #@a7
    .line 1672
    const/16 v0, 0x10

    #@a9
    goto/16 :goto_9

    #@ab
    .line 1676
    :cond_ab
    const-string v0, "bip"

    #@ad
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@b0
    move-result v0

    #@b1
    if-eqz v0, :cond_b7

    #@b3
    .line 1677
    const/16 v0, 0xf

    #@b5
    goto/16 :goto_9

    #@b7
    .line 1681
    :cond_b7
    const-string v0, "rcs"

    #@b9
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@bc
    move-result v0

    #@bd
    if-eqz v0, :cond_c3

    #@bf
    .line 1682
    const/16 v0, 0x11

    #@c1
    goto/16 :goto_9

    #@c3
    .line 1685
    :cond_c3
    const/4 v0, -0x1

    #@c4
    goto/16 :goto_9
.end method

.method protected broadcastMessenger()V
    .registers 5

    #@0
    .prologue
    .line 1198
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@4
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@7
    move-result-object v2

    #@8
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@a
    if-eqz v2, :cond_23

    #@c
    .line 1199
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@e
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@11
    move-result-object v2

    #@12
    const-string v3, "connectivity"

    #@14
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Landroid/net/ConnectivityManager;

    #@1a
    .line 1200
    .local v1, mConnMgr:Landroid/net/ConnectivityManager;
    new-instance v2, Landroid/os/Messenger;

    #@1c
    invoke-direct {v2, p0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@1f
    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->setDataConnectionMessanger(Landroid/os/Messenger;)V

    #@22
    .line 1210
    .end local v1           #mConnMgr:Landroid/net/ConnectivityManager;
    :goto_22
    return-void

    #@23
    .line 1204
    :cond_23
    new-instance v0, Landroid/content/Intent;

    #@25
    sget-object v2, Lcom/android/internal/telephony/DctConstants;->ACTION_DATA_CONNECTION_TRACKER_MESSENGER:Ljava/lang/String;

    #@27
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2a
    .line 1205
    .local v0, intent:Landroid/content/Intent;
    sget-object v2, Lcom/android/internal/telephony/DctConstants;->EXTRA_MESSENGER:Ljava/lang/String;

    #@2c
    new-instance v3, Landroid/os/Messenger;

    #@2e
    invoke-direct {v3, p0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@31
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@34
    .line 1206
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@36
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@3d
    goto :goto_22
.end method

.method protected cancelReconnectAlarm(Lcom/android/internal/telephony/DataConnectionAc;)V
    .registers 2
    .parameter "dcac"

    #@0
    .prologue
    .line 2957
    return-void
.end method

.method public changePreferrredNetworkMode(Z)V
    .registers 13
    .parameter "enabled"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 3570
    const/4 v4, -0x1

    #@2
    .line 3571
    .local v4, newPreferMode:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredNetworkMode()I

    #@5
    move-result v0

    #@6
    .line 3572
    .local v0, curPreferMode:I
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@b
    move-result-object v7

    #@c
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@f
    move-result v1

    #@10
    .line 3574
    .local v1, curRadioTech:I
    new-instance v7, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v8, "[changePreferrredNetworkMode] enabled:"

    #@17
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v7

    #@1b
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v7

    #@1f
    const-string v8, ", curPreferMode:"

    #@21
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v7

    #@25
    invoke-static {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->networkModeToString(I)Ljava/lang/String;

    #@28
    move-result-object v8

    #@29
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    const-string v8, ", curRadioTech:"

    #@2f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@35
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@38
    invoke-static {v1}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@3b
    move-result-object v8

    #@3c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v7

    #@40
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v7

    #@44
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@47
    .line 3578
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@49
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@4c
    move-result-object v7

    #@4d
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@50
    move-result-object v7

    #@51
    const-string v8, "mobile_data"

    #@53
    invoke-static {v7, v8, v10}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@56
    move-result v2

    #@57
    .line 3579
    .local v2, dataNetwork:I
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@59
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5c
    move-result-object v7

    #@5d
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@60
    move-result-object v7

    #@61
    const-string v8, "data_roaming"

    #@63
    invoke-static {v7, v8, v10}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@66
    move-result v6

    #@67
    .line 3580
    .local v6, roamingData:I
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@69
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@6c
    move-result-object v7

    #@6d
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@70
    move-result-object v7

    #@71
    const-string v8, "lte_roaming"

    #@73
    const/4 v9, 0x0

    #@74
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@77
    move-result v3

    #@78
    .line 3581
    .local v3, lteRoaming:I
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7a
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@7d
    move-result-object v7

    #@7e
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@81
    move-result v5

    #@82
    .line 3583
    .local v5, roaming:Z
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@84
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@86
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@89
    move-result-object v7

    #@8a
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LTE_ROAMING_KT:Z

    #@8c
    if-ne v7, v10, :cond_bf

    #@8e
    if-eqz v5, :cond_bf

    #@90
    .line 3585
    new-instance v7, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v8, "[sehyun] dataNetwork = "

    #@97
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v7

    #@9b
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v7

    #@9f
    const-string v8, ", roamingData = "

    #@a1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v7

    #@a5
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v7

    #@a9
    const-string v8, ", lteRoaming = "

    #@ab
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v7

    #@af
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v7

    #@b3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v7

    #@b7
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@ba
    .line 3586
    if-eqz v3, :cond_be

    #@bc
    if-nez v6, :cond_bf

    #@be
    .line 3685
    :cond_be
    :goto_be
    return-void

    #@bf
    .line 3590
    :cond_bf
    if-eqz p1, :cond_15e

    #@c1
    .line 3591
    sparse-switch v0, :sswitch_data_1d8

    #@c4
    .line 3676
    :cond_c4
    :goto_c4
    new-instance v7, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v8, "[changePreferrredNetworkMode] newPreferMode : "

    #@cb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v7

    #@cf
    invoke-static {v4}, Lcom/android/internal/telephony/DataConnectionTracker;->networkModeToString(I)Ljava/lang/String;

    #@d2
    move-result-object v8

    #@d3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v7

    #@d7
    const-string v8, " / curPreferMode : "

    #@d9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v7

    #@dd
    invoke-static {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->networkModeToString(I)Ljava/lang/String;

    #@e0
    move-result-object v8

    #@e1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v7

    #@e5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v7

    #@e9
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@ec
    .line 3679
    const/4 v7, -0x1

    #@ed
    if-eq v4, v7, :cond_be

    #@ef
    if-eq v4, v0, :cond_be

    #@f1
    .line 3680
    new-instance v7, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v8, "[changePreferrredNetworkMode] change to newPreferMode:"

    #@f8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v7

    #@fc
    invoke-static {v4}, Lcom/android/internal/telephony/DataConnectionTracker;->networkModeToString(I)Ljava/lang/String;

    #@ff
    move-result-object v8

    #@100
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v7

    #@104
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@107
    move-result-object v7

    #@108
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@10b
    .line 3682
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@10d
    const v8, 0x42029

    #@110
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@113
    move-result-object v8

    #@114
    invoke-virtual {v7, v4, v8}, Lcom/android/internal/telephony/PhoneBase;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@117
    .line 3683
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredNetworkMode(I)V

    #@11a
    goto :goto_be

    #@11b
    .line 3595
    :sswitch_11b
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@11d
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@11f
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@122
    move-result-object v7

    #@123
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->bypassforuplus:Z

    #@125
    if-nez v7, :cond_12a

    #@127
    .line 3596
    const/16 v4, 0x9

    #@129
    .line 3597
    goto :goto_c4

    #@12a
    .line 3602
    :cond_12a
    :sswitch_12a
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@12c
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@12e
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@131
    move-result-object v7

    #@132
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODE_CHANGE_USER3G_KR:Z

    #@134
    if-eqz v7, :cond_14e

    #@136
    .line 3604
    new-instance v7, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v8, "[changePreferrredNetworkMode] User_3g_mode_maintain : "

    #@13d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v7

    #@141
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@144
    move-result-object v7

    #@145
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@148
    move-result-object v7

    #@149
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@14c
    goto/16 :goto_c4

    #@14e
    .line 3611
    :cond_14e
    :sswitch_14e
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@150
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@152
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@155
    move-result-object v7

    #@156
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->bypassforuplus:Z

    #@158
    if-nez v7, :cond_c4

    #@15a
    .line 3612
    const/16 v4, 0xc

    #@15c
    .line 3613
    goto/16 :goto_c4

    #@15e
    .line 3625
    :cond_15e
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@160
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@162
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@165
    move-result-object v7

    #@166
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DEREGISTRATION:Z

    #@168
    if-eqz v7, :cond_16d

    #@16a
    .line 3626
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->isCompleteIMSforDelayTime()Z

    #@16d
    .line 3630
    :cond_16d
    sparse-switch v0, :sswitch_data_1ee

    #@170
    .line 3666
    :cond_170
    :goto_170
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@172
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@174
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@177
    move-result-object v7

    #@178
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->bypassforuplus:Z

    #@17a
    if-eqz v7, :cond_c4

    #@17c
    const/4 v7, 0x6

    #@17d
    if-eq v0, v7, :cond_c4

    #@17f
    const/4 v7, 0x4

    #@180
    if-eq v0, v7, :cond_c4

    #@182
    const/16 v7, 0xa

    #@184
    if-ne v0, v7, :cond_c4

    #@186
    const/16 v7, 0xd

    #@188
    if-eq v1, v7, :cond_c4

    #@18a
    goto/16 :goto_c4

    #@18c
    .line 3634
    :sswitch_18c
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@18e
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@190
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@193
    move-result-object v7

    #@194
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->bypassforuplus:Z

    #@196
    if-nez v7, :cond_1a7

    #@198
    .line 3635
    const/4 v4, 0x3

    #@199
    .line 3638
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@19b
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@19d
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1a0
    move-result-object v7

    #@1a1
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PROFILE_SETTING_SINGLE_PDN:Z

    #@1a3
    if-eqz v7, :cond_170

    #@1a5
    .line 3639
    const/4 v4, 0x0

    #@1a6
    goto :goto_170

    #@1a7
    .line 3646
    :cond_1a7
    :sswitch_1a7
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1a9
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1ab
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1ae
    move-result-object v7

    #@1af
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODE_CHANGE_USER3G_KR:Z

    #@1b1
    if-eqz v7, :cond_1ca

    #@1b3
    .line 3648
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b5
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1b8
    const-string v8, "[changePreferrredNetworkMode] User_3g_mode_maintain : "

    #@1ba
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bd
    move-result-object v7

    #@1be
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v7

    #@1c2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c5
    move-result-object v7

    #@1c6
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1c9
    goto :goto_170

    #@1ca
    .line 3655
    :cond_1ca
    :sswitch_1ca
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1cc
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1ce
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1d1
    move-result-object v7

    #@1d2
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->bypassforuplus:Z

    #@1d4
    if-nez v7, :cond_170

    #@1d6
    .line 3656
    const/4 v4, 0x2

    #@1d7
    goto :goto_170

    #@1d8
    .line 3591
    :sswitch_data_1d8
    .sparse-switch
        0x0 -> :sswitch_11b
        0x2 -> :sswitch_12a
        0x3 -> :sswitch_11b
        0x9 -> :sswitch_11b
        0xc -> :sswitch_14e
    .end sparse-switch

    #@1ee
    .line 3630
    :sswitch_data_1ee
    .sparse-switch
        0x0 -> :sswitch_18c
        0x2 -> :sswitch_1a7
        0x3 -> :sswitch_18c
        0x9 -> :sswitch_18c
        0xc -> :sswitch_1ca
    .end sparse-switch
.end method

.method public checkDataProfileEx(II)Z
    .registers 10
    .parameter "type"
    .parameter "Q_IPv"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 4001
    const-string v1, "default"

    #@3
    .line 4003
    .local v1, CheckType:Ljava/lang/String;
    const-string v0, "IP"

    #@5
    .line 4005
    .local v0, CheckIP:Ljava/lang/String;
    if-eqz p2, :cond_9

    #@7
    .line 4007
    const-string v0, "IPV6"

    #@9
    .line 4011
    :cond_9
    packed-switch p1, :pswitch_data_a8

    #@c
    .line 4066
    :cond_c
    :goto_c
    :pswitch_c
    return v4

    #@d
    .line 4017
    :pswitch_d
    const-string v1, "mms"

    #@f
    .line 4047
    :goto_f
    :pswitch_f
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@11
    if-eqz v5, :cond_c

    #@13
    .line 4049
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@18
    move-result-object v3

    #@19
    .local v3, i$:Ljava/util/Iterator;
    :cond_19
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@1c
    move-result v5

    #@1d
    if-eqz v5, :cond_7f

    #@1f
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, Lcom/android/internal/telephony/DataProfile;

    #@25
    .line 4050
    .local v2, dp:Lcom/android/internal/telephony/DataProfile;
    invoke-virtual {v2}, Lcom/android/internal/telephony/DataProfile;->getDataProfileType()Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@28
    move-result-object v5

    #@29
    sget-object v6, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_APN:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@2b
    if-ne v5, v6, :cond_19

    #@2d
    .line 4054
    invoke-virtual {v2, v1}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@30
    move-result v5

    #@31
    if-eqz v5, :cond_19

    #@33
    iget-object v5, v2, Lcom/android/internal/telephony/DataProfile;->protocol:Ljava/lang/String;

    #@35
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v5

    #@39
    if-nez v5, :cond_45

    #@3b
    iget-object v5, v2, Lcom/android/internal/telephony/DataProfile;->protocol:Ljava/lang/String;

    #@3d
    const-string v6, "IPV4V6"

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@42
    move-result v5

    #@43
    if-eqz v5, :cond_19

    #@45
    .line 4057
    :cond_45
    new-instance v4, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v5, "Now check Profile Ex :"

    #@4c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    const-string v5, ", "

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    const-string v5, " : true "

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@6b
    .line 4058
    const/4 v4, 0x1

    #@6c
    goto :goto_c

    #@6d
    .line 4020
    .end local v2           #dp:Lcom/android/internal/telephony/DataProfile;
    .end local v3           #i$:Ljava/util/Iterator;
    :pswitch_6d
    const-string v1, "supl"

    #@6f
    .line 4021
    goto :goto_f

    #@70
    .line 4023
    :pswitch_70
    const-string v1, "dun"

    #@72
    .line 4024
    goto :goto_f

    #@73
    .line 4026
    :pswitch_73
    const-string v1, "fota"

    #@75
    .line 4027
    goto :goto_f

    #@76
    .line 4029
    :pswitch_76
    const-string v1, "ims"

    #@78
    .line 4030
    goto :goto_f

    #@79
    .line 4032
    :pswitch_79
    const-string v1, "admin"

    #@7b
    .line 4033
    goto :goto_f

    #@7c
    .line 4035
    :pswitch_7c
    const-string v1, "vzwapp"

    #@7e
    .line 4036
    goto :goto_f

    #@7f
    .line 4062
    .restart local v3       #i$:Ljava/util/Iterator;
    :cond_7f
    new-instance v5, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v6, "Now check Profile Ex :"

    #@86
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v5

    #@8a
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v5

    #@8e
    const-string v6, ", "

    #@90
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v5

    #@94
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v5

    #@98
    const-string v6, " : false "

    #@9a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v5

    #@9e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v5

    #@a2
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@a5
    goto/16 :goto_c

    #@a7
    .line 4011
    nop

    #@a8
    :pswitch_data_a8
    .packed-switch 0x0
        :pswitch_f
        :pswitch_c
        :pswitch_d
        :pswitch_6d
        :pswitch_70
        :pswitch_f
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_73
        :pswitch_76
        :pswitch_c
        :pswitch_c
        :pswitch_79
        :pswitch_7c
    .end packed-switch
.end method

.method public checkDefaultEx(I)V
    .registers 6
    .parameter "Newdefaultcount"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 3329
    iget v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->OldisDefaultCount:I

    #@3
    if-eq v0, p1, :cond_18

    #@5
    .line 3331
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    const-string v1, "changed"

    #@b
    invoke-interface {v0, v3, v3, v1}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@e
    .line 3332
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@10
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@12
    const/4 v1, 0x2

    #@13
    const-string v2, "changed"

    #@15
    invoke-interface {v0, v3, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@18
    .line 3335
    :cond_18
    iput p1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->OldisDefaultCount:I

    #@1a
    .line 3336
    return-void
.end method

.method public cleanUpAllConnections(Ljava/lang/String;)V
    .registers 4
    .parameter "cause"

    #@0
    .prologue
    .line 2048
    const v1, 0x4201d

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 2049
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 2050
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 2051
    return-void
.end method

.method protected abstract cleanUpConnection(Ljava/lang/String;Z)V
.end method

.method protected abstract clearTetheredStateOnStatus()V
.end method

.method protected abstract closeImsPdn(I)V
.end method

.method public declared-synchronized disableApnType(Ljava/lang/String;)I
    .registers 8
    .parameter "type"

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 1894
    monitor-enter p0

    #@4
    :try_start_4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "disableApnType("

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    const-string v5, ")"

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@20
    .line 1895
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I
    :try_end_23
    .catchall {:try_start_4 .. :try_end_23} :catchall_48

    #@23
    move-result v0

    #@24
    .line 1896
    .local v0, id:I
    const/4 v4, -0x1

    #@25
    if-ne v0, v4, :cond_29

    #@27
    .line 1911
    :cond_27
    :goto_27
    monitor-exit p0

    #@28
    return v1

    #@29
    .line 1899
    :cond_29
    :try_start_29
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnIdEnabled(I)Z

    #@2c
    move-result v4

    #@2d
    if-eqz v4, :cond_27

    #@2f
    .line 1900
    const/4 v1, 0x0

    #@30
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->setEnabled(IZ)V

    #@33
    .line 1901
    const-string v1, "default"

    #@35
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@38
    move-result v1

    #@39
    if-eqz v1, :cond_46

    #@3b
    .line 1902
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@3d
    const/4 v4, 0x0

    #@3e
    aget-boolean v1, v1, v4
    :try_end_40
    .catchall {:try_start_29 .. :try_end_40} :catchall_48

    #@40
    if-eqz v1, :cond_44

    #@42
    move v1, v2

    #@43
    .line 1903
    goto :goto_27

    #@44
    :cond_44
    move v1, v3

    #@45
    .line 1905
    goto :goto_27

    #@46
    :cond_46
    move v1, v3

    #@47
    .line 1908
    goto :goto_27

    #@48
    .line 1894
    .end local v0           #id:I
    :catchall_48
    move-exception v1

    #@49
    monitor-exit p0

    #@4a
    throw v1
.end method

.method protected abstract disable_ehrpd_internet_ipv6()V
.end method

.method protected abstract disconnectOneLowerPriorityCall(Ljava/lang/String;)Z
.end method

.method public disconnectonlyChangedPDN(Ljava/lang/String;)V
    .registers 2
    .parameter "pdntype"

    #@0
    .prologue
    .line 2964
    return-void
.end method

.method public dispose()V
    .registers 5

    #@0
    .prologue
    .line 1183
    const-string v2, "DCT.dispose"

    #@2
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@5
    .line 1184
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionAsyncChannels:Ljava/util/HashMap;

    #@7
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@a
    move-result-object v2

    #@b
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v1

    #@f
    .local v1, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_1f

    #@15
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Lcom/android/internal/telephony/DataConnectionAc;

    #@1b
    .line 1185
    .local v0, dcac:Lcom/android/internal/telephony/DataConnectionAc;
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionAc;->disconnect()V

    #@1e
    goto :goto_f

    #@1f
    .line 1187
    .end local v0           #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    :cond_1f
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionAsyncChannels:Ljava/util/HashMap;

    #@21
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    #@24
    .line 1188
    const/4 v2, 0x1

    #@25
    iput-boolean v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsDisposed:Z

    #@27
    .line 1189
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@29
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@2c
    move-result-object v2

    #@2d
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@2f
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@32
    .line 1190
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataRoamingSettingObserver:Lcom/android/internal/telephony/DataConnectionTracker$DataRoamingSettingObserver;

    #@34
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@36
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnectionTracker$DataRoamingSettingObserver;->unregister(Landroid/content/Context;)V

    #@3d
    .line 1191
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3f
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/uicc/UiccController;->unregisterForIccChanged(Landroid/os/Handler;)V

    #@42
    .line 1192
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@44
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@46
    invoke-interface {v2, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForTetheredModeStateChanged(Landroid/os/Handler;)V

    #@49
    .line 1193
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4b
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@52
    move-result-object v2

    #@53
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnObserver:Lcom/android/internal/telephony/DataConnectionTracker$ApnChangeObserver;

    #@55
    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@58
    .line 1194
    return-void
.end method

.method protected doRecovery()V
    .registers 5

    #@0
    .prologue
    .line 2544
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getOverallState()Lcom/android/internal/telephony/DctConstants$State;

    #@3
    move-result-object v1

    #@4
    sget-object v2, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@6
    if-ne v1, v2, :cond_47

    #@8
    .line 2546
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getRecoveryAction()I

    #@b
    move-result v0

    #@c
    .line 2547
    .local v0, recoveryAction:I
    packed-switch v0, :pswitch_data_b2

    #@f
    .line 2593
    new-instance v1, Ljava/lang/RuntimeException;

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "doRecovery: Invalid recoveryAction="

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 2549
    :pswitch_28
    const v1, 0xc3c6

    #@2b
    iget-wide v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@2d
    invoke-static {v1, v2, v3}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@30
    .line 2551
    const-string v1, "doRecovery() get data call list"

    #@32
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@35
    .line 2552
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@37
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@39
    const v2, 0x42004

    #@3c
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@3f
    move-result-object v2

    #@40
    invoke-interface {v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->getDataCallList(Landroid/os/Message;)V

    #@43
    .line 2553
    const/4 v1, 0x1

    #@44
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->putRecoveryAction(I)V

    #@47
    .line 2597
    .end local v0           #recoveryAction:I
    :cond_47
    :goto_47
    return-void

    #@48
    .line 2556
    .restart local v0       #recoveryAction:I
    :pswitch_48
    const v1, 0xc3c7

    #@4b
    iget-wide v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@4d
    invoke-static {v1, v2, v3}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@50
    .line 2557
    const-string v1, "doRecovery() cleanup all connections"

    #@52
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@55
    .line 2558
    const-string v1, "pdpReset"

    #@57
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@5a
    .line 2559
    const/4 v1, 0x2

    #@5b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->putRecoveryAction(I)V

    #@5e
    goto :goto_47

    #@5f
    .line 2562
    :pswitch_5f
    const v1, 0xc3c8

    #@62
    iget-wide v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@64
    invoke-static {v1, v2, v3}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@67
    .line 2564
    const-string v1, "doRecovery() re-register"

    #@69
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@6c
    .line 2565
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@6e
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@71
    move-result-object v1

    #@72
    const/4 v2, 0x0

    #@73
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/ServiceStateTracker;->reRegisterNetwork(Landroid/os/Message;)V

    #@76
    .line 2566
    const/4 v1, 0x3

    #@77
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->putRecoveryAction(I)V

    #@7a
    goto :goto_47

    #@7b
    .line 2569
    :pswitch_7b
    const v1, 0xc3c9

    #@7e
    iget-wide v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@80
    invoke-static {v1, v2, v3}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@83
    .line 2571
    const-string v1, "restarting radio"

    #@85
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@88
    .line 2572
    const/4 v1, 0x4

    #@89
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->putRecoveryAction(I)V

    #@8c
    .line 2573
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->restartRadio()V

    #@8f
    goto :goto_47

    #@90
    .line 2582
    :pswitch_90
    const v1, 0xc3ca

    #@93
    const/4 v2, -0x1

    #@94
    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(II)I

    #@97
    .line 2583
    const-string v1, "restarting radio with gsm.radioreset to true"

    #@99
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@9c
    .line 2584
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->RADIO_RESET_PROPERTY:Ljava/lang/String;

    #@9e
    const-string v2, "true"

    #@a0
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@a3
    .line 2587
    const-wide/16 v1, 0x3e8

    #@a5
    :try_start_a5
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_a8
    .catch Ljava/lang/InterruptedException; {:try_start_a5 .. :try_end_a8} :catch_b0

    #@a8
    .line 2589
    :goto_a8
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->restartRadio()V

    #@ab
    .line 2590
    const/4 v1, 0x0

    #@ac
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->putRecoveryAction(I)V

    #@af
    goto :goto_47

    #@b0
    .line 2588
    :catch_b0
    move-exception v1

    #@b1
    goto :goto_a8

    #@b2
    .line 2547
    :pswitch_data_b2
    .packed-switch 0x0
        :pswitch_28
        :pswitch_48
        :pswitch_5f
        :pswitch_7b
        :pswitch_90
    .end packed-switch
.end method

.method public dprilmsg(Lcom/android/internal/telephony/DataProfile;I)I
    .registers 37
    .parameter "dp"
    .parameter "nodbID"

    #@0
    .prologue
    .line 3169
    if-nez p1, :cond_9e

    #@2
    .line 3176
    const-string v5, "noneAPN"

    #@4
    .line 3177
    .local v5, temp:Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@7
    move-result v4

    #@8
    .line 3178
    .local v4, templen:I
    move-object/from16 v0, p0

    #@a
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@c
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    const/4 v3, 0x0

    #@f
    const/4 v7, 0x0

    #@10
    move/from16 v0, p2

    #@12
    invoke-interface {v2, v3, v0, v7}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    const-string v3, "none"

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-nez v2, :cond_83

    #@1e
    .line 3182
    move-object/from16 v0, p0

    #@20
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@22
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@24
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@27
    move-result-object v2

    #@28
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_DATA_CALL_WHEN_ADMIN_PDN_DSIABLED_VZW:Z

    #@2a
    if-eqz v2, :cond_48

    #@2c
    .line 3184
    const-string v2, "you use vzwsim so send nondb info"

    #@2e
    move-object/from16 v0, p0

    #@30
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@33
    .line 3185
    move-object/from16 v0, p0

    #@35
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@37
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@39
    const/4 v6, 0x0

    #@3a
    const/4 v7, 0x0

    #@3b
    const/4 v8, 0x0

    #@3c
    const/4 v9, 0x0

    #@3d
    const/4 v10, 0x0

    #@3e
    const-string v11, ""

    #@40
    const-string v12, ""

    #@42
    const/4 v13, 0x0

    #@43
    move/from16 v3, p2

    #@45
    invoke-interface/range {v2 .. v13}, Lcom/android/internal/telephony/CommandsInterface;->sendPdnTable(IILjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@48
    .line 3189
    :cond_48
    move-object/from16 v0, p0

    #@4a
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4c
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@4e
    const/4 v3, 0x1

    #@4f
    const/4 v7, 0x0

    #@50
    move/from16 v0, p2

    #@52
    invoke-interface {v2, v3, v0, v7}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@55
    .line 3191
    const/16 v24, 0x0

    #@57
    .line 3194
    .local v24, PDNType:Ljava/lang/String;
    move-object/from16 v0, p0

    #@59
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5b
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5d
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@60
    move-result-object v2

    #@61
    iget v2, v2, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@63
    move-object/from16 v0, p0

    #@65
    move/from16 v1, p2

    #@67
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->getPDNtypeByID(II)Ljava/lang/String;

    #@6a
    move-result-object v24

    #@6b
    .line 3198
    move-object/from16 v0, p0

    #@6d
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@6f
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@71
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@74
    move-result-object v2

    #@75
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC:Z

    #@77
    if-eqz v2, :cond_82

    #@79
    if-eqz v24, :cond_82

    #@7b
    .line 3200
    move-object/from16 v0, p0

    #@7d
    move-object/from16 v1, v24

    #@7f
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->disconnectonlyChangedPDN(Ljava/lang/String;)V

    #@82
    .line 3321
    .end local v4           #templen:I
    .end local v5           #temp:Ljava/lang/String;
    .end local v24           #PDNType:Ljava/lang/String;
    .end local p2
    :cond_82
    :goto_82
    return p2

    #@83
    .line 3212
    .restart local v4       #templen:I
    .restart local v5       #temp:Ljava/lang/String;
    .restart local p2
    :cond_83
    new-instance v2, Ljava/lang/StringBuilder;

    #@85
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@88
    const-string v3, "sending apn is same with old one id= "

    #@8a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v2

    #@8e
    move/from16 v0, p2

    #@90
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v2

    #@94
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v2

    #@98
    move-object/from16 v0, p0

    #@9a
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@9d
    goto :goto_82

    #@9e
    .end local v4           #templen:I
    .end local v5           #temp:Ljava/lang/String;
    :cond_9e
    move-object/from16 v27, p1

    #@a0
    .line 3217
    check-cast v27, Lcom/android/internal/telephony/ApnSetting;

    #@a2
    .line 3219
    .local v27, apnSetting:Lcom/android/internal/telephony/ApnSetting;
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/DataConnectionTracker;->getPdnIdFromApnType(Lcom/android/internal/telephony/DataProfile;)I

    #@a5
    move-result v29

    #@a6
    .line 3221
    .local v29, pdnId:I
    const/4 v2, -0x1

    #@a7
    move/from16 v0, v29

    #@a9
    if-ne v0, v2, :cond_b3

    #@ab
    .line 3222
    const-string v2, "APN_INVALID_ID"

    #@ad
    move-object/from16 v0, p0

    #@af
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@b2
    goto :goto_82

    #@b3
    .line 3229
    :cond_b3
    const/16 v26, 0x0

    #@b5
    .line 3231
    .local v26, apnLength:I
    move-object/from16 v0, v27

    #@b7
    iget-object v0, v0, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@b9
    move-object/from16 v25, v0

    #@bb
    .line 3232
    .local v25, apn:Ljava/lang/String;
    const-string v2, ""

    #@bd
    move-object/from16 v0, v25

    #@bf
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c2
    move-result v2

    #@c3
    if-eqz v2, :cond_cd

    #@c5
    .line 3233
    const-string v2, "apn is null"

    #@c7
    move-object/from16 v0, p0

    #@c9
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@cc
    goto :goto_82

    #@cd
    .line 3236
    :cond_cd
    new-instance v2, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v3, "apn is "

    #@d4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v2

    #@d8
    move-object/from16 v0, v25

    #@da
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v2

    #@de
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v2

    #@e2
    move-object/from16 v0, p0

    #@e4
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@e7
    .line 3237
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    #@ea
    move-result v26

    #@eb
    move-object/from16 v2, p1

    #@ed
    .line 3240
    check-cast v2, Lcom/android/internal/telephony/ApnSetting;

    #@ef
    move-object/from16 v0, p0

    #@f1
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->getIpTypeFromDataProfile(Lcom/android/internal/telephony/ApnSetting;)I

    #@f4
    move-result v11

    #@f5
    .line 3241
    .local v11, ipType:I
    const/4 v2, -0x1

    #@f6
    if-ne v11, v2, :cond_100

    #@f8
    .line 3242
    const-string v2, "IP_VERSION_SUPPORT_TYPE_NOT_AVAILABLE"

    #@fa
    move-object/from16 v0, p0

    #@fc
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@ff
    goto :goto_82

    #@100
    .line 3246
    :cond_100
    move-object/from16 v0, p0

    #@102
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@104
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@107
    move-result-object v2

    #@108
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@10b
    move-result v32

    #@10c
    .line 3247
    .local v32, roaming:Z
    move-object/from16 v0, p0

    #@10e
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@110
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@113
    move-result-object v2

    #@114
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@117
    move-result v31

    #@118
    .line 3248
    .local v31, radioTech:I
    invoke-static/range {v31 .. v31}, Landroid/telephony/ServiceState;->isGsm(I)Z

    #@11b
    move-result v2

    #@11c
    if-eqz v2, :cond_2d4

    #@11e
    const/16 v2, 0xe

    #@120
    move/from16 v0, v31

    #@122
    if-eq v0, v2, :cond_2d4

    #@124
    const/16 v2, 0xd

    #@126
    move/from16 v0, v31

    #@128
    if-eq v0, v2, :cond_2d4

    #@12a
    const/16 v28, 0x1

    #@12c
    .line 3252
    .local v28, isGsmRoaming:Z
    :goto_12c
    move-object/from16 v0, p0

    #@12e
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@130
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@132
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@135
    move-result-object v2

    #@136
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_BLOCK_TETHERINGPDN_ON_GSMROAMING_KDDI:Z

    #@138
    if-eqz v2, :cond_2d8

    #@13a
    .line 3253
    new-instance v2, Ljava/lang/StringBuilder;

    #@13c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13f
    const-string v3, "dprilmsg: radioTech is "

    #@141
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v2

    #@145
    move-object/from16 v0, p0

    #@147
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@149
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@14c
    move-result-object v3

    #@14d
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@150
    move-result v3

    #@151
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@154
    move-result-object v2

    #@155
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@158
    move-result-object v2

    #@159
    move-object/from16 v0, p0

    #@15b
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@15e
    .line 3254
    new-instance v2, Ljava/lang/StringBuilder;

    #@160
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@163
    const-string v3, "dprilmsg: getRoaming > "

    #@165
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v2

    #@169
    move/from16 v0, v32

    #@16b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v2

    #@16f
    const-string v3, " isGsmRoaming > "

    #@171
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v2

    #@175
    move/from16 v0, v28

    #@177
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v2

    #@17b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17e
    move-result-object v2

    #@17f
    move-object/from16 v0, p0

    #@181
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@184
    .line 3257
    if-eqz v32, :cond_2d8

    #@186
    if-eqz v28, :cond_2d8

    #@188
    .line 3258
    const-string v2, "dprilmsg: KDDI getRoaming, isGsmRoaming is true, so sending APN IP Type is IPv4 only"

    #@18a
    move-object/from16 v0, p0

    #@18c
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@18f
    .line 3261
    new-instance v6, Lcom/android/internal/telephony/ApnSetting;

    #@191
    move-object/from16 v0, v27

    #@193
    iget v7, v0, Lcom/android/internal/telephony/DataProfile;->id:I

    #@195
    move-object/from16 v0, v27

    #@197
    iget-object v8, v0, Lcom/android/internal/telephony/DataProfile;->numeric:Ljava/lang/String;

    #@199
    move-object/from16 v0, v27

    #@19b
    iget-object v9, v0, Lcom/android/internal/telephony/ApnSetting;->carrier:Ljava/lang/String;

    #@19d
    move-object/from16 v0, v27

    #@19f
    iget-object v10, v0, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@1a1
    move-object/from16 v0, v27

    #@1a3
    iget-object v11, v0, Lcom/android/internal/telephony/ApnSetting;->proxy:Ljava/lang/String;

    #@1a5
    .end local v11           #ipType:I
    move-object/from16 v0, v27

    #@1a7
    iget-object v12, v0, Lcom/android/internal/telephony/ApnSetting;->port:Ljava/lang/String;

    #@1a9
    move-object/from16 v0, v27

    #@1ab
    iget-object v13, v0, Lcom/android/internal/telephony/ApnSetting;->mmsc:Ljava/lang/String;

    #@1ad
    move-object/from16 v0, v27

    #@1af
    iget-object v14, v0, Lcom/android/internal/telephony/ApnSetting;->mmsProxy:Ljava/lang/String;

    #@1b1
    move-object/from16 v0, v27

    #@1b3
    iget-object v15, v0, Lcom/android/internal/telephony/ApnSetting;->mmsPort:Ljava/lang/String;

    #@1b5
    move-object/from16 v0, v27

    #@1b7
    iget-object v0, v0, Lcom/android/internal/telephony/DataProfile;->user:Ljava/lang/String;

    #@1b9
    move-object/from16 v16, v0

    #@1bb
    move-object/from16 v0, v27

    #@1bd
    iget-object v0, v0, Lcom/android/internal/telephony/DataProfile;->password:Ljava/lang/String;

    #@1bf
    move-object/from16 v17, v0

    #@1c1
    move-object/from16 v0, v27

    #@1c3
    iget v0, v0, Lcom/android/internal/telephony/DataProfile;->authType:I

    #@1c5
    move/from16 v18, v0

    #@1c7
    move-object/from16 v0, v27

    #@1c9
    iget-object v0, v0, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@1cb
    move-object/from16 v19, v0

    #@1cd
    const-string v20, "IP"

    #@1cf
    const-string v21, "IP"

    #@1d1
    move-object/from16 v0, v27

    #@1d3
    iget-boolean v0, v0, Lcom/android/internal/telephony/ApnSetting;->carrierEnabled:Z

    #@1d5
    move/from16 v22, v0

    #@1d7
    move-object/from16 v0, v27

    #@1d9
    iget v0, v0, Lcom/android/internal/telephony/DataProfile;->bearer:I

    #@1db
    move/from16 v23, v0

    #@1dd
    invoke-direct/range {v6 .. v23}, Lcom/android/internal/telephony/ApnSetting;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    #@1e0
    .line 3269
    .local v6, roamingPdn:Lcom/android/internal/telephony/ApnSetting;
    move-object/from16 v33, v6

    #@1e2
    .line 3271
    .local v33, roamingDp:Lcom/android/internal/telephony/DataProfile;
    new-instance v2, Ljava/lang/StringBuilder;

    #@1e4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e7
    const-string v3, "dprilmsg: KDDI roamingDp = "

    #@1e9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ec
    move-result-object v2

    #@1ed
    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/ApnSetting;->toString()Ljava/lang/String;

    #@1f0
    move-result-object v3

    #@1f1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v2

    #@1f5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f8
    move-result-object v2

    #@1f9
    move-object/from16 v0, p0

    #@1fb
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1fe
    .line 3272
    new-instance v2, Ljava/lang/StringBuilder;

    #@200
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@203
    const-string v3, "dprilmsg: before mPhone.mCM.getOrSetDB(false,pdnId,null) = "

    #@205
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@208
    move-result-object v2

    #@209
    move-object/from16 v0, p0

    #@20b
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@20d
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@20f
    const/4 v7, 0x0

    #@210
    const/4 v8, 0x0

    #@211
    move/from16 v0, v29

    #@213
    invoke-interface {v3, v7, v0, v8}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@216
    move-result-object v3

    #@217
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v2

    #@21b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21e
    move-result-object v2

    #@21f
    move-object/from16 v0, p0

    #@221
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@224
    .line 3273
    new-instance v2, Ljava/lang/StringBuilder;

    #@226
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@229
    const-string v3, "dprilmsg: dp.toHash = "

    #@22b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v2

    #@22f
    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/ApnSetting;->toHash()Ljava/lang/String;

    #@232
    move-result-object v3

    #@233
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@236
    move-result-object v2

    #@237
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23a
    move-result-object v2

    #@23b
    move-object/from16 v0, p0

    #@23d
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@240
    .line 3275
    move-object/from16 v0, p0

    #@242
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@244
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@246
    const/4 v3, 0x0

    #@247
    const/4 v7, 0x0

    #@248
    move/from16 v0, v29

    #@24a
    invoke-interface {v2, v3, v0, v7}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@24d
    move-result-object v2

    #@24e
    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/ApnSetting;->toHash()Ljava/lang/String;

    #@251
    move-result-object v3

    #@252
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@255
    move-result v2

    #@256
    if-nez v2, :cond_2d0

    #@258
    .line 3277
    move-object/from16 v0, p0

    #@25a
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@25c
    iget-object v7, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@25e
    const/4 v11, 0x0

    #@25f
    const/4 v12, 0x0

    #@260
    const/4 v13, 0x1

    #@261
    move-object/from16 v0, v27

    #@263
    iget v14, v0, Lcom/android/internal/telephony/DataProfile;->authType:I

    #@265
    const/4 v15, 0x0

    #@266
    move-object/from16 v0, v27

    #@268
    iget-object v0, v0, Lcom/android/internal/telephony/DataProfile;->user:Ljava/lang/String;

    #@26a
    move-object/from16 v16, v0

    #@26c
    move-object/from16 v0, v27

    #@26e
    iget-object v0, v0, Lcom/android/internal/telephony/DataProfile;->password:Ljava/lang/String;

    #@270
    move-object/from16 v17, v0

    #@272
    const/16 v18, 0x0

    #@274
    move/from16 v8, v29

    #@276
    move/from16 v9, v26

    #@278
    move-object/from16 v10, v25

    #@27a
    invoke-interface/range {v7 .. v18}, Lcom/android/internal/telephony/CommandsInterface;->sendPdnTable(IILjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@27d
    .line 3280
    move-object/from16 v0, p0

    #@27f
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@281
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@283
    const/4 v3, 0x1

    #@284
    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/ApnSetting;->toHash()Ljava/lang/String;

    #@287
    move-result-object v7

    #@288
    move/from16 v0, v29

    #@28a
    invoke-interface {v2, v3, v0, v7}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@28d
    .line 3281
    new-instance v2, Ljava/lang/StringBuilder;

    #@28f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@292
    const-string v3, "dprilmsg: after mPhone.mCM.getOrSetDB(false,pdnId,null) = "

    #@294
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@297
    move-result-object v2

    #@298
    move-object/from16 v0, p0

    #@29a
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@29c
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@29e
    const/4 v7, 0x0

    #@29f
    const/4 v8, 0x0

    #@2a0
    move/from16 v0, v29

    #@2a2
    invoke-interface {v3, v7, v0, v8}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@2a5
    move-result-object v3

    #@2a6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a9
    move-result-object v2

    #@2aa
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ad
    move-result-object v2

    #@2ae
    move-object/from16 v0, p0

    #@2b0
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@2b3
    .line 3283
    move-object/from16 v0, p0

    #@2b5
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2b7
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2b9
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2bc
    move-result-object v2

    #@2bd
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC:Z

    #@2bf
    if-eqz v2, :cond_2d0

    #@2c1
    .line 3285
    move-object/from16 v0, p0

    #@2c3
    move-object/from16 v1, v33

    #@2c5
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->getMainApnType(Lcom/android/internal/telephony/DataProfile;)Ljava/lang/String;

    #@2c8
    move-result-object v30

    #@2c9
    .line 3286
    .local v30, pdntype:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2cb
    move-object/from16 v1, v30

    #@2cd
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->disconnectonlyChangedPDN(Ljava/lang/String;)V

    #@2d0
    .end local v30           #pdntype:Ljava/lang/String;
    :cond_2d0
    move/from16 p2, v29

    #@2d2
    .line 3289
    goto/16 :goto_82

    #@2d4
    .line 3248
    .end local v6           #roamingPdn:Lcom/android/internal/telephony/ApnSetting;
    .end local v28           #isGsmRoaming:Z
    .end local v33           #roamingDp:Lcom/android/internal/telephony/DataProfile;
    .restart local v11       #ipType:I
    :cond_2d4
    const/16 v28, 0x0

    #@2d6
    goto/16 :goto_12c

    #@2d8
    .line 3294
    .restart local v28       #isGsmRoaming:Z
    :cond_2d8
    const/4 v12, 0x0

    #@2d9
    .line 3295
    .local v12, inactivityTime:I
    const/4 v15, 0x0

    #@2da
    .line 3297
    .local v15, esminfo:I
    move-object/from16 v0, p0

    #@2dc
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2de
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2e0
    const/4 v3, 0x0

    #@2e1
    const/4 v7, 0x0

    #@2e2
    move/from16 v0, v29

    #@2e4
    invoke-interface {v2, v3, v0, v7}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@2e7
    move-result-object v2

    #@2e8
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/DataProfile;->toHash()Ljava/lang/String;

    #@2eb
    move-result-object v3

    #@2ec
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2ef
    move-result v2

    #@2f0
    if-nez v2, :cond_341

    #@2f2
    .line 3299
    move-object/from16 v0, p0

    #@2f4
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2f6
    iget-object v7, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2f8
    const/4 v13, 0x1

    #@2f9
    move-object/from16 v0, v27

    #@2fb
    iget v14, v0, Lcom/android/internal/telephony/DataProfile;->authType:I

    #@2fd
    move-object/from16 v0, v27

    #@2ff
    iget-object v0, v0, Lcom/android/internal/telephony/DataProfile;->user:Ljava/lang/String;

    #@301
    move-object/from16 v16, v0

    #@303
    move-object/from16 v0, v27

    #@305
    iget-object v0, v0, Lcom/android/internal/telephony/DataProfile;->password:Ljava/lang/String;

    #@307
    move-object/from16 v17, v0

    #@309
    const/16 v18, 0x0

    #@30b
    move/from16 v8, v29

    #@30d
    move/from16 v9, v26

    #@30f
    move-object/from16 v10, v25

    #@311
    invoke-interface/range {v7 .. v18}, Lcom/android/internal/telephony/CommandsInterface;->sendPdnTable(IILjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@314
    .line 3302
    move-object/from16 v0, p0

    #@316
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@318
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@31a
    const/4 v3, 0x1

    #@31b
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/DataProfile;->toHash()Ljava/lang/String;

    #@31e
    move-result-object v7

    #@31f
    move/from16 v0, v29

    #@321
    invoke-interface {v2, v3, v0, v7}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@324
    .line 3308
    move-object/from16 v0, p0

    #@326
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@328
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@32a
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@32d
    move-result-object v2

    #@32e
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC:Z

    #@330
    if-eqz v2, :cond_33d

    #@332
    .line 3310
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/DataConnectionTracker;->getMainApnType(Lcom/android/internal/telephony/DataProfile;)Ljava/lang/String;

    #@335
    move-result-object v30

    #@336
    .line 3311
    .restart local v30       #pdntype:Ljava/lang/String;
    move-object/from16 v0, p0

    #@338
    move-object/from16 v1, v30

    #@33a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->disconnectonlyChangedPDN(Ljava/lang/String;)V

    #@33d
    .end local v30           #pdntype:Ljava/lang/String;
    :cond_33d
    :goto_33d
    move/from16 p2, v29

    #@33f
    .line 3321
    goto/16 :goto_82

    #@341
    .line 3318
    :cond_341
    new-instance v2, Ljava/lang/StringBuilder;

    #@343
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@346
    const-string v3, "sending apn is same with old one id= "

    #@348
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34b
    move-result-object v2

    #@34c
    move/from16 v0, v29

    #@34e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@351
    move-result-object v2

    #@352
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@355
    move-result-object v2

    #@356
    move-object/from16 v0, p0

    #@358
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@35b
    goto :goto_33d
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 16
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2777
    const-string v8, "DataConnectionTracker:"

    #@2
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 2778
    const-string v8, " RADIO_TESTS=false"

    #@7
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a
    .line 2779
    new-instance v8, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v9, " mInternalDataEnabled="

    #@11
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v8

    #@15
    iget-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mInternalDataEnabled:Z

    #@17
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v8

    #@1b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v8

    #@1f
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@22
    .line 2780
    new-instance v8, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v9, " mUserDataEnabled="

    #@29
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    iget-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@2f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3a
    .line 2781
    new-instance v8, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v9, " sPolicyDataEnabed="

    #@41
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v8

    #@45
    sget-boolean v9, Lcom/android/internal/telephony/DataConnectionTracker;->sPolicyDataEnabled:Z

    #@47
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v8

    #@4b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v8

    #@4f
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@52
    .line 2782
    const-string v8, " dataEnabled:"

    #@54
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@57
    .line 2783
    const/4 v3, 0x0

    #@58
    .local v3, i:I
    :goto_58
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@5a
    array-length v8, v8

    #@5b
    if-ge v3, v8, :cond_7a

    #@5d
    .line 2784
    const-string v8, "  dataEnabled[%d]=%b\n"

    #@5f
    const/4 v9, 0x2

    #@60
    new-array v9, v9, [Ljava/lang/Object;

    #@62
    const/4 v10, 0x0

    #@63
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@66
    move-result-object v11

    #@67
    aput-object v11, v9, v10

    #@69
    const/4 v10, 0x1

    #@6a
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@6c
    aget-boolean v11, v11, v3

    #@6e
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@71
    move-result-object v11

    #@72
    aput-object v11, v9, v10

    #@74
    invoke-virtual {p2, v8, v9}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@77
    .line 2783
    add-int/lit8 v3, v3, 0x1

    #@79
    goto :goto_58

    #@7a
    .line 2786
    :cond_7a
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@7d
    .line 2787
    new-instance v8, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v9, " enabledCount="

    #@84
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v8

    #@88
    iget v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@8a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v8

    #@8e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v8

    #@92
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@95
    .line 2788
    new-instance v8, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v9, " mRequestedApnType="

    #@9c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v8

    #@a0
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRequestedApnType:Ljava/lang/String;

    #@a2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v8

    #@a6
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v8

    #@aa
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ad
    .line 2789
    new-instance v8, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v9, " mPhone="

    #@b4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v8

    #@b8
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ba
    invoke-virtual {v9}, Lcom/android/internal/telephony/PhoneBase;->getPhoneName()Ljava/lang/String;

    #@bd
    move-result-object v9

    #@be
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v8

    #@c2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v8

    #@c6
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c9
    .line 2790
    new-instance v8, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    const-string v9, " mActivity="

    #@d0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v8

    #@d4
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActivity:Lcom/android/internal/telephony/DctConstants$Activity;

    #@d6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v8

    #@da
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v8

    #@de
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e1
    .line 2791
    new-instance v8, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v9, " mState="

    #@e8
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v8

    #@ec
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@ee
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v8

    #@f2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f5
    move-result-object v8

    #@f6
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f9
    .line 2792
    new-instance v8, Ljava/lang/StringBuilder;

    #@fb
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@fe
    const-string v9, " mTxPkts="

    #@100
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v8

    #@104
    iget-wide v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mTxPkts:J

    #@106
    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@109
    move-result-object v8

    #@10a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10d
    move-result-object v8

    #@10e
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@111
    .line 2793
    new-instance v8, Ljava/lang/StringBuilder;

    #@113
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@116
    const-string v9, " mRxPkts="

    #@118
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v8

    #@11c
    iget-wide v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRxPkts:J

    #@11e
    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@121
    move-result-object v8

    #@122
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@125
    move-result-object v8

    #@126
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@129
    .line 2794
    new-instance v8, Ljava/lang/StringBuilder;

    #@12b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@12e
    const-string v9, " mNetStatPollPeriod="

    #@130
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v8

    #@134
    iget v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollPeriod:I

    #@136
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@139
    move-result-object v8

    #@13a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13d
    move-result-object v8

    #@13e
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@141
    .line 2795
    new-instance v8, Ljava/lang/StringBuilder;

    #@143
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@146
    const-string v9, " mNetStatPollEnabled="

    #@148
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v8

    #@14c
    iget-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollEnabled:Z

    #@14e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@151
    move-result-object v8

    #@152
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@155
    move-result-object v8

    #@156
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@159
    .line 2796
    new-instance v8, Ljava/lang/StringBuilder;

    #@15b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@15e
    const-string v9, " mDataStallTxRxSum="

    #@160
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v8

    #@164
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallTxRxSum:Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

    #@166
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@169
    move-result-object v8

    #@16a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16d
    move-result-object v8

    #@16e
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@171
    .line 2797
    new-instance v8, Ljava/lang/StringBuilder;

    #@173
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@176
    const-string v9, " mDataStallAlarmTag="

    #@178
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v8

    #@17c
    iget v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmTag:I

    #@17e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@181
    move-result-object v8

    #@182
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@185
    move-result-object v8

    #@186
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@189
    .line 2798
    new-instance v8, Ljava/lang/StringBuilder;

    #@18b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@18e
    const-string v9, " mSentSinceLastRecv="

    #@190
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@193
    move-result-object v8

    #@194
    iget-wide v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@196
    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@199
    move-result-object v8

    #@19a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19d
    move-result-object v8

    #@19e
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a1
    .line 2799
    new-instance v8, Ljava/lang/StringBuilder;

    #@1a3
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1a6
    const-string v9, " mNoRecvPollCount="

    #@1a8
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ab
    move-result-object v8

    #@1ac
    iget v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mNoRecvPollCount:I

    #@1ae
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b1
    move-result-object v8

    #@1b2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b5
    move-result-object v8

    #@1b6
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b9
    .line 2800
    new-instance v8, Ljava/lang/StringBuilder;

    #@1bb
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1be
    const-string v9, " mResolver="

    #@1c0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c3
    move-result-object v8

    #@1c4
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mResolver:Landroid/content/ContentResolver;

    #@1c6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v8

    #@1ca
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cd
    move-result-object v8

    #@1ce
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d1
    .line 2801
    new-instance v8, Ljava/lang/StringBuilder;

    #@1d3
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1d6
    const-string v9, " mIsWifiConnected="

    #@1d8
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v8

    #@1dc
    iget-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@1de
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e1
    move-result-object v8

    #@1e2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e5
    move-result-object v8

    #@1e6
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1e9
    .line 2802
    new-instance v8, Ljava/lang/StringBuilder;

    #@1eb
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1ee
    const-string v9, " mReconnectIntent="

    #@1f0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v8

    #@1f4
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntent:Landroid/app/PendingIntent;

    #@1f6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f9
    move-result-object v8

    #@1fa
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fd
    move-result-object v8

    #@1fe
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@201
    .line 2803
    new-instance v8, Ljava/lang/StringBuilder;

    #@203
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@206
    const-string v9, " mCidActive="

    #@208
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20b
    move-result-object v8

    #@20c
    iget v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mCidActive:I

    #@20e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@211
    move-result-object v8

    #@212
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@215
    move-result-object v8

    #@216
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@219
    .line 2804
    new-instance v8, Ljava/lang/StringBuilder;

    #@21b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@21e
    const-string v9, " mAutoAttachOnCreation="

    #@220
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v8

    #@224
    iget-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAutoAttachOnCreation:Z

    #@226
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@229
    move-result-object v8

    #@22a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22d
    move-result-object v8

    #@22e
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@231
    .line 2805
    new-instance v8, Ljava/lang/StringBuilder;

    #@233
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@236
    const-string v9, " mIsScreenOn="

    #@238
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23b
    move-result-object v8

    #@23c
    iget-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsScreenOn:Z

    #@23e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@241
    move-result-object v8

    #@242
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@245
    move-result-object v8

    #@246
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@249
    .line 2806
    new-instance v8, Ljava/lang/StringBuilder;

    #@24b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@24e
    const-string v9, " mUniqueIdGenerator="

    #@250
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@253
    move-result-object v8

    #@254
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUniqueIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

    #@256
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@259
    move-result-object v8

    #@25a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25d
    move-result-object v8

    #@25e
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@261
    .line 2807
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@264
    .line 2808
    const-string v8, " ***************************************"

    #@266
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@269
    .line 2809
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@26b
    invoke-virtual {v8}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@26e
    move-result-object v7

    #@26f
    .line 2810
    .local v7, mDcSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/internal/telephony/DataConnection;>;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    #@271
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@274
    const-string v9, " mDataConnections: count="

    #@276
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@279
    move-result-object v8

    #@27a
    invoke-interface {v7}, Ljava/util/Set;->size()I

    #@27d
    move-result v9

    #@27e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@281
    move-result-object v8

    #@282
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@285
    move-result-object v8

    #@286
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@289
    .line 2811
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@28c
    move-result-object v4

    #@28d
    .local v4, i$:Ljava/util/Iterator;
    :goto_28d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@290
    move-result v8

    #@291
    if-eqz v8, :cond_2b2

    #@293
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@296
    move-result-object v0

    #@297
    check-cast v0, Ljava/util/Map$Entry;

    #@299
    .line 2812
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/internal/telephony/DataConnection;>;"
    const-string v8, " *** mDataConnection[%d] \n"

    #@29b
    const/4 v9, 0x1

    #@29c
    new-array v9, v9, [Ljava/lang/Object;

    #@29e
    const/4 v10, 0x0

    #@29f
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2a2
    move-result-object v11

    #@2a3
    aput-object v11, v9, v10

    #@2a5
    invoke-virtual {p2, v8, v9}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@2a8
    .line 2813
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2ab
    move-result-object v8

    #@2ac
    check-cast v8, Lcom/android/internal/telephony/DataConnection;

    #@2ae
    invoke-virtual {v8, p1, p2, p3}, Lcom/android/internal/telephony/DataConnection;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@2b1
    goto :goto_28d

    #@2b2
    .line 2815
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/internal/telephony/DataConnection;>;"
    :cond_2b2
    const-string v8, " ***************************************"

    #@2b4
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2b7
    .line 2816
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@2ba
    .line 2817
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnToDataConnectionId:Ljava/util/HashMap;

    #@2bc
    invoke-virtual {v8}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@2bf
    move-result-object v6

    #@2c0
    .line 2818
    .local v6, mApnToDcIdSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    #@2c2
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2c5
    const-string v9, " mApnToDataConnectonId size="

    #@2c7
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ca
    move-result-object v8

    #@2cb
    invoke-interface {v6}, Ljava/util/Set;->size()I

    #@2ce
    move-result v9

    #@2cf
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d2
    move-result-object v8

    #@2d3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d6
    move-result-object v8

    #@2d7
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2da
    .line 2819
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2dd
    move-result-object v4

    #@2de
    :goto_2de
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@2e1
    move-result v8

    #@2e2
    if-eqz v8, :cond_301

    #@2e4
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2e7
    move-result-object v2

    #@2e8
    check-cast v2, Ljava/util/Map$Entry;

    #@2ea
    .line 2820
    .local v2, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v8, " mApnToDataConnectonId[%s]=%d\n"

    #@2ec
    const/4 v9, 0x2

    #@2ed
    new-array v9, v9, [Ljava/lang/Object;

    #@2ef
    const/4 v10, 0x0

    #@2f0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2f3
    move-result-object v11

    #@2f4
    aput-object v11, v9, v10

    #@2f6
    const/4 v10, 0x1

    #@2f7
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2fa
    move-result-object v11

    #@2fb
    aput-object v11, v9, v10

    #@2fd
    invoke-virtual {p2, v8, v9}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@300
    goto :goto_2de

    #@301
    .line 2822
    .end local v2           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_301
    const-string v8, " ***************************************"

    #@303
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@306
    .line 2823
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@309
    .line 2824
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@30b
    if-eqz v8, :cond_3ab

    #@30d
    .line 2825
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@30f
    invoke-virtual {v8}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@312
    move-result-object v5

    #@313
    .line 2826
    .local v5, mApnContextsSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/telephony/ApnContext;>;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    #@315
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@318
    const-string v9, " mApnContexts size="

    #@31a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31d
    move-result-object v8

    #@31e
    invoke-interface {v5}, Ljava/util/Set;->size()I

    #@321
    move-result v9

    #@322
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@325
    move-result-object v8

    #@326
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@329
    move-result-object v8

    #@32a
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32d
    .line 2827
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@330
    move-result-object v4

    #@331
    :goto_331
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@334
    move-result v8

    #@335
    if-eqz v8, :cond_347

    #@337
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@33a
    move-result-object v1

    #@33b
    check-cast v1, Ljava/util/Map$Entry;

    #@33d
    .line 2828
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/telephony/ApnContext;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@340
    move-result-object v8

    #@341
    check-cast v8, Lcom/android/internal/telephony/ApnContext;

    #@343
    invoke-virtual {v8, p1, p2, p3}, Lcom/android/internal/telephony/ApnContext;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@346
    goto :goto_331

    #@347
    .line 2830
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/telephony/ApnContext;>;"
    :cond_347
    const-string v8, " ***************************************"

    #@349
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@34c
    .line 2834
    .end local v5           #mApnContextsSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/telephony/ApnContext;>;>;"
    :goto_34c
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@34f
    .line 2835
    new-instance v8, Ljava/lang/StringBuilder;

    #@351
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@354
    const-string v9, " mActiveApn="

    #@356
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@359
    move-result-object v8

    #@35a
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@35c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35f
    move-result-object v8

    #@360
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@363
    move-result-object v8

    #@364
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@367
    .line 2836
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@369
    if-eqz v8, :cond_448

    #@36b
    .line 2837
    new-instance v8, Ljava/lang/StringBuilder;

    #@36d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@370
    const-string v9, " mAllApns size="

    #@372
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@375
    move-result-object v8

    #@376
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@378
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@37b
    move-result v9

    #@37c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37f
    move-result-object v8

    #@380
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@383
    move-result-object v8

    #@384
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@387
    .line 2838
    const/4 v3, 0x0

    #@388
    :goto_388
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@38a
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@38d
    move-result v8

    #@38e
    if-ge v3, v8, :cond_3b1

    #@390
    .line 2839
    const-string v8, " mAllApns[%d]: %s\n"

    #@392
    const/4 v9, 0x2

    #@393
    new-array v9, v9, [Ljava/lang/Object;

    #@395
    const/4 v10, 0x0

    #@396
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@399
    move-result-object v11

    #@39a
    aput-object v11, v9, v10

    #@39c
    const/4 v10, 0x1

    #@39d
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@39f
    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3a2
    move-result-object v11

    #@3a3
    aput-object v11, v9, v10

    #@3a5
    invoke-virtual {p2, v8, v9}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@3a8
    .line 2838
    add-int/lit8 v3, v3, 0x1

    #@3aa
    goto :goto_388

    #@3ab
    .line 2832
    :cond_3ab
    const-string v8, " mApnContexts=null"

    #@3ad
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3b0
    goto :goto_34c

    #@3b1
    .line 2841
    :cond_3b1
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@3b4
    .line 2845
    :goto_3b4
    new-instance v8, Ljava/lang/StringBuilder;

    #@3b6
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3b9
    const-string v9, " mPreferredApn="

    #@3bb
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3be
    move-result-object v8

    #@3bf
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@3c1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c4
    move-result-object v8

    #@3c5
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c8
    move-result-object v8

    #@3c9
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3cc
    .line 2846
    new-instance v8, Ljava/lang/StringBuilder;

    #@3ce
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3d1
    const-string v9, " mIsPsRestricted="

    #@3d3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d6
    move-result-object v8

    #@3d7
    iget-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsPsRestricted:Z

    #@3d9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3dc
    move-result-object v8

    #@3dd
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e0
    move-result-object v8

    #@3e1
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3e4
    .line 2847
    new-instance v8, Ljava/lang/StringBuilder;

    #@3e6
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3e9
    const-string v9, " mIsDisposed="

    #@3eb
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ee
    move-result-object v8

    #@3ef
    iget-boolean v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsDisposed:Z

    #@3f1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3f4
    move-result-object v8

    #@3f5
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f8
    move-result-object v8

    #@3f9
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3fc
    .line 2848
    new-instance v8, Ljava/lang/StringBuilder;

    #@3fe
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@401
    const-string v9, " mIntentReceiver="

    #@403
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@406
    move-result-object v8

    #@407
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@409
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40c
    move-result-object v8

    #@40d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@410
    move-result-object v8

    #@411
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@414
    .line 2849
    new-instance v8, Ljava/lang/StringBuilder;

    #@416
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@419
    const-string v9, " mDataRoamingSettingObserver="

    #@41b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41e
    move-result-object v8

    #@41f
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataRoamingSettingObserver:Lcom/android/internal/telephony/DataConnectionTracker$DataRoamingSettingObserver;

    #@421
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@424
    move-result-object v8

    #@425
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@428
    move-result-object v8

    #@429
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@42c
    .line 2850
    new-instance v8, Ljava/lang/StringBuilder;

    #@42e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@431
    const-string v9, " mApnObserver="

    #@433
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@436
    move-result-object v8

    #@437
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnObserver:Lcom/android/internal/telephony/DataConnectionTracker$ApnChangeObserver;

    #@439
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43c
    move-result-object v8

    #@43d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@440
    move-result-object v8

    #@441
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@444
    .line 2851
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@447
    .line 2852
    return-void

    #@448
    .line 2843
    :cond_448
    const-string v8, " mAllApns=null"

    #@44a
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@44d
    goto/16 :goto_3b4
.end method

.method public declared-synchronized enableApnType(Ljava/lang/String;)I
    .registers 7
    .parameter "type"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 1846
    monitor-enter p0

    #@3
    :try_start_3
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_72

    #@6
    move-result v0

    #@7
    .line 1847
    .local v0, id:I
    const/4 v3, -0x1

    #@8
    if-ne v0, v3, :cond_d

    #@a
    .line 1848
    const/4 v1, 0x3

    #@b
    .line 1876
    :cond_b
    :goto_b
    monitor-exit p0

    #@c
    return v1

    #@d
    .line 1852
    :cond_d
    :try_start_d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v4, "enableApnType("

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    const-string v4, "), isApnTypeActive = "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@25
    move-result v4

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    const-string v4, ", isApnIdEnabled ="

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnIdEnabled(I)Z

    #@33
    move-result v4

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    const-string v4, " and state = "

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@40
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@4b
    .line 1856
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeAvailable(Ljava/lang/String;)Z

    #@4e
    move-result v3

    #@4f
    if-nez v3, :cond_58

    #@51
    .line 1857
    const-string v1, "type not available"

    #@53
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@56
    .line 1858
    const/4 v1, 0x2

    #@57
    goto :goto_b

    #@58
    .line 1861
    :cond_58
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnIdEnabled(I)Z

    #@5b
    move-result v3

    #@5c
    if-eqz v3, :cond_66

    #@5e
    .line 1863
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@60
    sget-object v4, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@62
    if-ne v3, v4, :cond_b

    #@64
    move v1, v2

    #@65
    .line 1867
    goto :goto_b

    #@66
    .line 1869
    :cond_66
    const/4 v3, 0x1

    #@67
    invoke-virtual {p0, v0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->setEnabled(IZ)V

    #@6a
    .line 1871
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@6c
    sget-object v4, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;
    :try_end_6e
    .catchall {:try_start_d .. :try_end_6e} :catchall_72

    #@6e
    if-ne v3, v4, :cond_b

    #@70
    move v1, v2

    #@71
    .line 1873
    goto :goto_b

    #@72
    .line 1846
    .end local v0           #id:I
    :catchall_72
    move-exception v1

    #@73
    monitor-exit p0

    #@74
    throw v1
.end method

.method protected abstract enable_ehrpd_internet_ipv6()V
.end method

.method protected fetchDunApn()Lcom/android/internal/telephony/DataProfile;
    .registers 6

    #@0
    .prologue
    .line 1228
    const-string v3, "net.tethering.noprovisioning"

    #@2
    const/4 v4, 0x0

    #@3
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_10

    #@9
    .line 1229
    const-string v3, "fetchDunApn: net.tethering.noprovisioning=true ret: null"

    #@b
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@e
    .line 1230
    const/4 v2, 0x0

    #@f
    .line 1244
    :goto_f
    return-object v2

    #@10
    .line 1232
    :cond_10
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@12
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@15
    move-result-object v1

    #@16
    .line 1233
    .local v1, c:Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@19
    move-result-object v3

    #@1a
    const-string v4, "tether_dun_apn"

    #@1c
    invoke-static {v3, v4}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 1235
    .local v0, apnData:Ljava/lang/String;
    invoke-static {v0}, Lcom/android/internal/telephony/ApnSetting;->fromString(Ljava/lang/String;)Lcom/android/internal/telephony/ApnSetting;

    #@23
    move-result-object v2

    #@24
    .line 1236
    .local v2, dunSetting:Lcom/android/internal/telephony/ApnSetting;
    if-eqz v2, :cond_3d

    #@26
    .line 1237
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v4, "fetchDunApn: global TETHER_DUN_APN dunSetting="

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@3c
    goto :goto_f

    #@3d
    .line 1241
    :cond_3d
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@40
    move-result-object v3

    #@41
    const v4, 0x1040034

    #@44
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@47
    move-result-object v0

    #@48
    .line 1242
    invoke-static {v0}, Lcom/android/internal/telephony/ApnSetting;->fromString(Ljava/lang/String;)Lcom/android/internal/telephony/ApnSetting;

    #@4b
    move-result-object v2

    #@4c
    .line 1243
    new-instance v3, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v4, "fetchDunApn: config_tether_apndata dunSetting="

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@62
    goto :goto_f
.end method

.method public getAPNList()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4075
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected abstract getActionIntentDataStallAlarm()Ljava/lang/String;
.end method

.method protected abstract getActionIntentReconnectAlarm()Ljava/lang/String;
.end method

.method public getActiveApnString(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "apnType"

    #@0
    .prologue
    .line 1260
    const/4 v0, 0x0

    #@1
    .line 1261
    .local v0, result:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@3
    if-eqz v1, :cond_9

    #@5
    .line 1262
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@7
    iget-object v0, v1, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@9
    .line 1264
    :cond_9
    return-object v0
.end method

.method public getActiveApnTypes()[Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1249
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 1250
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@6
    iget-object v0, v1, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@8
    .line 1255
    .local v0, result:[Ljava/lang/String;
    :goto_8
    return-object v0

    #@9
    .line 1252
    .end local v0           #result:[Ljava/lang/String;
    :cond_9
    const/4 v1, 0x1

    #@a
    new-array v0, v1, [Ljava/lang/String;

    #@c
    .line 1253
    .restart local v0       #result:[Ljava/lang/String;
    const/4 v1, 0x0

    #@d
    const-string v2, "default"

    #@f
    aput-object v2, v0, v1

    #@11
    goto :goto_8
.end method

.method public getActivity()Lcom/android/internal/telephony/DctConstants$Activity;
    .registers 2

    #@0
    .prologue
    .line 1213
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActivity:Lcom/android/internal/telephony/DctConstants$Activity;

    #@2
    return-object v0
.end method

.method public getAnyDataEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 1590
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataEnabledLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1591
    :try_start_3
    iget-boolean v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mInternalDataEnabled:Z

    #@5
    if-eqz v1, :cond_2e

    #@7
    iget-boolean v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@9
    if-eqz v1, :cond_2e

    #@b
    sget-boolean v1, Lcom/android/internal/telephony/DataConnectionTracker;->sPolicyDataEnabled:Z

    #@d
    if-eqz v1, :cond_2e

    #@f
    iget v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@11
    if-eqz v1, :cond_2e

    #@13
    const/4 v0, 0x1

    #@14
    .line 1593
    .local v0, result:Z
    :goto_14
    monitor-exit v2
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_30

    #@15
    .line 1594
    if-nez v0, :cond_2d

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "getAnyDataEnabled "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@2d
    .line 1595
    :cond_2d
    return v0

    #@2e
    .line 1591
    .end local v0           #result:Z
    :cond_2e
    const/4 v0, 0x0

    #@2f
    goto :goto_14

    #@30
    .line 1593
    :catchall_30
    move-exception v1

    #@31
    :try_start_31
    monitor-exit v2
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_30

    #@32
    throw v1
.end method

.method protected abstract getCdmaInfo()[Ljava/lang/String;
.end method

.method public getDataOnRoamingEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1322
    :try_start_1
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    .line 1323
    .local v0, resolver:Landroid/content/ContentResolver;
    const-string v3, "data_roaming"

    #@d
    invoke-static {v0, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_10
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_10} :catch_15

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_14

    #@13
    const/4 v2, 0x1

    #@14
    .line 1325
    .end local v0           #resolver:Landroid/content/ContentResolver;
    :cond_14
    :goto_14
    return v2

    #@15
    .line 1324
    :catch_15
    move-exception v1

    #@16
    .line 1325
    .local v1, snfe:Landroid/provider/Settings$SettingNotFoundException;
    goto :goto_14
.end method

.method public getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;
    .registers 2

    #@0
    .prologue
    .line 2855
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    return-object v0
.end method

.method public getIpTypeFromDataProfile(Lcom/android/internal/telephony/ApnSetting;)I
    .registers 5
    .parameter "apnSetting"

    #@0
    .prologue
    .line 3727
    const/4 v0, 0x0

    #@1
    .line 3729
    .local v0, pdp_type:I
    if-eqz p1, :cond_26

    #@3
    .line 3730
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "getIpTypeFromDataProfile : "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    iget-object v2, p1, Lcom/android/internal/telephony/DataProfile;->protocol:Ljava/lang/String;

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1b
    .line 3731
    iget-object v1, p1, Lcom/android/internal/telephony/DataProfile;->protocol:Ljava/lang/String;

    #@1d
    const-string v2, "IP"

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_27

    #@25
    .line 3732
    const/4 v0, 0x0

    #@26
    .line 3742
    :cond_26
    :goto_26
    return v0

    #@27
    .line 3734
    :cond_27
    iget-object v1, p1, Lcom/android/internal/telephony/DataProfile;->protocol:Ljava/lang/String;

    #@29
    const-string v2, "IPV6"

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_33

    #@31
    .line 3735
    const/4 v0, 0x2

    #@32
    goto :goto_26

    #@33
    .line 3737
    :cond_33
    iget-object v1, p1, Lcom/android/internal/telephony/DataProfile;->protocol:Ljava/lang/String;

    #@35
    const-string v2, "IPV4V6"

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v1

    #@3b
    if-eqz v1, :cond_26

    #@3d
    .line 3738
    const/4 v0, 0x3

    #@3e
    goto :goto_26
.end method

.method protected getLinkCapabilities(Ljava/lang/String;)Landroid/net/LinkCapabilities;
    .registers 6
    .parameter "apnType"

    #@0
    .prologue
    .line 1762
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@3
    move-result v1

    #@4
    .line 1763
    .local v1, id:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnIdEnabled(I)Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_1c

    #@a
    .line 1765
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionAsyncChannels:Ljava/util/HashMap;

    #@c
    const/4 v3, 0x0

    #@d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/internal/telephony/DataConnectionAc;

    #@17
    .line 1766
    .local v0, dcac:Lcom/android/internal/telephony/DataConnectionAc;
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionAc;->getLinkCapabilitiesSync()Landroid/net/LinkCapabilities;

    #@1a
    move-result-object v2

    #@1b
    .line 1768
    .end local v0           #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    :goto_1b
    return-object v2

    #@1c
    :cond_1c
    new-instance v2, Landroid/net/LinkCapabilities;

    #@1e
    invoke-direct {v2}, Landroid/net/LinkCapabilities;-><init>()V

    #@21
    goto :goto_1b
.end method

.method protected getLinkProperties(Ljava/lang/String;)Landroid/net/LinkProperties;
    .registers 6
    .parameter "apnType"

    #@0
    .prologue
    .line 1750
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@3
    move-result v1

    #@4
    .line 1752
    .local v1, id:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnIdEnabled(I)Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_1c

    #@a
    .line 1754
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionAsyncChannels:Ljava/util/HashMap;

    #@c
    const/4 v3, 0x0

    #@d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/internal/telephony/DataConnectionAc;

    #@17
    .line 1755
    .local v0, dcac:Lcom/android/internal/telephony/DataConnectionAc;
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionAc;->getLinkPropertiesSync()Landroid/net/LinkProperties;

    #@1a
    move-result-object v2

    #@1b
    .line 1757
    .end local v0           #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    :goto_1b
    return-object v2

    #@1c
    :cond_1c
    new-instance v2, Landroid/net/LinkProperties;

    #@1e
    invoke-direct {v2}, Landroid/net/LinkProperties;-><init>()V

    #@21
    goto :goto_1b
.end method

.method public abstract getLinkProperties_defaultAPN()Landroid/net/LinkProperties;
.end method

.method protected abstract getLteInfo()[Ljava/lang/String;
.end method

.method public getMainApnType(Lcom/android/internal/telephony/DataProfile;)Ljava/lang/String;
    .registers 3
    .parameter "dp"

    #@0
    .prologue
    .line 3939
    if-nez p1, :cond_a

    #@2
    .line 3941
    const-string v0, "something woring to get Main Apn type "

    #@4
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@7
    .line 3942
    const-string v0, "none"

    #@9
    .line 3978
    :goto_9
    return-object v0

    #@a
    .line 3946
    :cond_a
    const-string v0, "default"

    #@c
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_15

    #@12
    .line 3947
    const-string v0, "default"

    #@14
    goto :goto_9

    #@15
    .line 3948
    :cond_15
    const-string v0, "vzwapp"

    #@17
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_20

    #@1d
    .line 3949
    const-string v0, "vzwapp"

    #@1f
    goto :goto_9

    #@20
    .line 3950
    :cond_20
    const-string v0, "mms"

    #@22
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_2b

    #@28
    .line 3951
    const-string v0, "mms"

    #@2a
    goto :goto_9

    #@2b
    .line 3952
    :cond_2b
    const-string v0, "admin"

    #@2d
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_36

    #@33
    .line 3953
    const-string v0, "admin"

    #@35
    goto :goto_9

    #@36
    .line 3954
    :cond_36
    const-string v0, "ims"

    #@38
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_41

    #@3e
    .line 3955
    const-string v0, "ims"

    #@40
    goto :goto_9

    #@41
    .line 3956
    :cond_41
    const-string v0, "entitlement"

    #@43
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@46
    move-result v0

    #@47
    if-eqz v0, :cond_4c

    #@49
    .line 3957
    const-string v0, "entitlement"

    #@4b
    goto :goto_9

    #@4c
    .line 3958
    :cond_4c
    const-string v0, "vzw800"

    #@4e
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@51
    move-result v0

    #@52
    if-eqz v0, :cond_57

    #@54
    .line 3959
    const-string v0, "vzw800"

    #@56
    goto :goto_9

    #@57
    .line 3961
    :cond_57
    const-string v0, "fota"

    #@59
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@5c
    move-result v0

    #@5d
    if-eqz v0, :cond_62

    #@5f
    .line 3962
    const-string v0, "fota"

    #@61
    goto :goto_9

    #@62
    .line 3963
    :cond_62
    const-string v0, "dun"

    #@64
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@67
    move-result v0

    #@68
    if-eqz v0, :cond_6d

    #@6a
    .line 3964
    const-string v0, "dun"

    #@6c
    goto :goto_9

    #@6d
    .line 3966
    :cond_6d
    const-string v0, "tethering"

    #@6f
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@72
    move-result v0

    #@73
    if-eqz v0, :cond_78

    #@75
    .line 3967
    const-string v0, "tethering"

    #@77
    goto :goto_9

    #@78
    .line 3970
    :cond_78
    const-string v0, "emergency"

    #@7a
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@7d
    move-result v0

    #@7e
    if-eqz v0, :cond_83

    #@80
    .line 3971
    const-string v0, "emergency"

    #@82
    goto :goto_9

    #@83
    .line 3974
    :cond_83
    const-string v0, "supl"

    #@85
    invoke-virtual {p1, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@88
    move-result v0

    #@89
    if-eqz v0, :cond_8f

    #@8b
    .line 3975
    const-string v0, "supl"

    #@8d
    goto/16 :goto_9

    #@8f
    .line 3978
    :cond_8f
    const-string v0, "none"

    #@91
    goto/16 :goto_9
.end method

.method public getOperatorNumeric()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2954
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected abstract getOverallState()Lcom/android/internal/telephony/DctConstants$State;
.end method

.method protected getPDNtypeByID(II)Ljava/lang/String;
    .registers 8
    .parameter "id"
    .parameter "MPDNset"

    #@0
    .prologue
    const/4 v4, 0x4

    #@1
    const/4 v3, 0x3

    #@2
    const/4 v0, 0x0

    #@3
    const/4 v2, 0x2

    #@4
    const/4 v1, 0x1

    #@5
    .line 3022
    packed-switch p2, :pswitch_data_ba

    #@8
    .line 3157
    :pswitch_8
    const-string v0, "default"

    #@a
    :cond_a
    :goto_a
    return-object v0

    #@b
    .line 3026
    :pswitch_b
    if-ne p1, v1, :cond_10

    #@d
    .line 3027
    const-string v0, "ims"

    #@f
    goto :goto_a

    #@10
    .line 3028
    :cond_10
    if-ne p1, v2, :cond_15

    #@12
    .line 3029
    const-string v0, "admin"

    #@14
    goto :goto_a

    #@15
    .line 3030
    :cond_15
    if-ne p1, v3, :cond_1a

    #@17
    .line 3031
    const-string v0, "default"

    #@19
    goto :goto_a

    #@1a
    .line 3032
    :cond_1a
    if-ne p1, v4, :cond_1f

    #@1c
    .line 3033
    const-string v0, "vzwapp"

    #@1e
    goto :goto_a

    #@1f
    .line 3034
    :cond_1f
    const/4 v1, 0x5

    #@20
    if-ne p1, v1, :cond_a

    #@22
    .line 3035
    const-string v0, "vzw800"

    #@24
    goto :goto_a

    #@25
    .line 3042
    :pswitch_25
    if-ne p1, v1, :cond_2a

    #@27
    .line 3043
    const-string v0, "default"

    #@29
    goto :goto_a

    #@2a
    .line 3044
    :cond_2a
    if-ne p1, v2, :cond_2f

    #@2c
    .line 3045
    const-string v0, "admin"

    #@2e
    goto :goto_a

    #@2f
    .line 3046
    :cond_2f
    if-ne p1, v3, :cond_34

    #@31
    .line 3047
    const-string v0, "ims"

    #@33
    goto :goto_a

    #@34
    .line 3048
    :cond_34
    if-ne p1, v4, :cond_a

    #@36
    .line 3049
    const-string v0, "dun"

    #@38
    goto :goto_a

    #@39
    .line 3059
    :pswitch_39
    if-ne p1, v1, :cond_3e

    #@3b
    .line 3060
    const-string v0, "default"

    #@3d
    goto :goto_a

    #@3e
    .line 3061
    :cond_3e
    if-ne p1, v2, :cond_43

    #@40
    .line 3062
    const-string v0, "fota"

    #@42
    goto :goto_a

    #@43
    .line 3063
    :cond_43
    if-ne p1, v3, :cond_48

    #@45
    .line 3064
    const-string v0, "dun"

    #@47
    goto :goto_a

    #@48
    .line 3066
    :cond_48
    if-ne p1, v4, :cond_a

    #@4a
    .line 3067
    const-string v0, "supl"

    #@4c
    goto :goto_a

    #@4d
    .line 3079
    :pswitch_4d
    if-ne p1, v1, :cond_52

    #@4f
    .line 3080
    const-string v0, "default"

    #@51
    goto :goto_a

    #@52
    .line 3081
    :cond_52
    if-ne p1, v2, :cond_57

    #@54
    .line 3082
    const-string v0, "ims"

    #@56
    goto :goto_a

    #@57
    .line 3083
    :cond_57
    if-ne p1, v3, :cond_5c

    #@59
    .line 3084
    const-string v0, "entitlement"

    #@5b
    goto :goto_a

    #@5c
    .line 3085
    :cond_5c
    if-ne p1, v4, :cond_a

    #@5e
    .line 3086
    const-string v0, "emergency"

    #@60
    goto :goto_a

    #@61
    .line 3094
    :pswitch_61
    if-ne p1, v1, :cond_66

    #@63
    .line 3095
    const-string v0, "default"

    #@65
    goto :goto_a

    #@66
    .line 3096
    :cond_66
    if-ne p1, v2, :cond_6b

    #@68
    .line 3097
    const-string v0, "ims"

    #@6a
    goto :goto_a

    #@6b
    .line 3098
    :cond_6b
    if-ne p1, v3, :cond_a

    #@6d
    .line 3099
    const-string v0, "dun"

    #@6f
    goto :goto_a

    #@70
    .line 3106
    :pswitch_70
    if-ne p1, v1, :cond_75

    #@72
    .line 3107
    const-string v0, "ims"

    #@74
    goto :goto_a

    #@75
    .line 3108
    :cond_75
    if-ne p1, v2, :cond_7a

    #@77
    .line 3109
    const-string v0, "default"

    #@79
    goto :goto_a

    #@7a
    .line 3110
    :cond_7a
    if-ne p1, v3, :cond_7f

    #@7c
    .line 3111
    const-string v0, "tethering"

    #@7e
    goto :goto_a

    #@7f
    .line 3113
    :cond_7f
    if-ne p1, v4, :cond_84

    #@81
    .line 3114
    const-string v0, "admin"

    #@83
    goto :goto_a

    #@84
    .line 3115
    :cond_84
    const/4 v1, 0x5

    #@85
    if-ne p1, v1, :cond_a

    #@87
    .line 3116
    const-string v0, "emergency"

    #@89
    goto :goto_a

    #@8a
    .line 3123
    :pswitch_8a
    if-ne p1, v1, :cond_90

    #@8c
    .line 3124
    const-string v0, "ims"

    #@8e
    goto/16 :goto_a

    #@90
    .line 3125
    :cond_90
    if-ne p1, v2, :cond_a

    #@92
    .line 3126
    const-string v0, "default"

    #@94
    goto/16 :goto_a

    #@96
    .line 3132
    :pswitch_96
    if-ne p1, v1, :cond_9c

    #@98
    .line 3133
    const-string v0, "ims"

    #@9a
    goto/16 :goto_a

    #@9c
    .line 3134
    :cond_9c
    if-ne p1, v2, :cond_a

    #@9e
    .line 3135
    const-string v0, "default"

    #@a0
    goto/16 :goto_a

    #@a2
    .line 3143
    :pswitch_a2
    if-ne p1, v1, :cond_a8

    #@a4
    .line 3144
    const-string v0, "default"

    #@a6
    goto/16 :goto_a

    #@a8
    .line 3145
    :cond_a8
    if-ne p1, v2, :cond_ae

    #@aa
    .line 3146
    const-string v0, "ims"

    #@ac
    goto/16 :goto_a

    #@ae
    .line 3147
    :cond_ae
    if-ne p1, v3, :cond_b4

    #@b0
    .line 3148
    const-string v0, "admin"

    #@b2
    goto/16 :goto_a

    #@b4
    .line 3149
    :cond_b4
    if-ne p1, v4, :cond_a

    #@b6
    .line 3150
    const-string v0, "tethering"

    #@b8
    goto/16 :goto_a

    #@ba
    .line 3022
    :pswitch_data_ba
    .packed-switch 0x1
        :pswitch_b
        :pswitch_70
        :pswitch_8
        :pswitch_4d
        :pswitch_8a
        :pswitch_96
        :pswitch_a2
        :pswitch_25
        :pswitch_39
        :pswitch_8
        :pswitch_61
    .end packed-switch
.end method

.method protected abstract getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method protected abstract getPcscfAddress(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
.end method

.method public getPdnIdFromApnType(Lcom/android/internal/telephony/DataProfile;)I
    .registers 5
    .parameter "dp"

    #@0
    .prologue
    .line 3749
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->getMainApnType(Lcom/android/internal/telephony/DataProfile;)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 3750
    .local v1, maintype:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    .line 3751
    .local v0, AFWID:I
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@c
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@f
    move-result-object v2

    #@10
    iget v2, v2, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@12
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->switchingFixedtype(II)I

    #@15
    move-result v2

    #@16
    return v2
.end method

.method public getPreferredApn()Lcom/android/internal/telephony/DataProfile;
    .registers 2

    #@0
    .prologue
    .line 2977
    const-string v0, "DataConnectionTracker getPreferredApn() is null....."

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@5
    .line 2978
    const/4 v0, 0x0

    #@6
    return-object v0
.end method

.method protected getPreferredDunApn()Lcom/android/internal/telephony/DataProfile;
    .registers 2

    #@0
    .prologue
    .line 2985
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected getPreferredNetworkMode()I
    .registers 5

    #@0
    .prologue
    .line 3552
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v1

    #@a
    const-string v2, "preferred_network_mode"

    #@c
    const/4 v3, 0x0

    #@d
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10
    move-result v0

    #@11
    .line 3557
    .local v0, nwMode:I
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->isValidNetworkMode(I)Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_2f

    #@17
    .line 3559
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "[LGE] Not valid isValidNetworkMode = "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@2d
    .line 3561
    sget v0, Lcom/android/internal/telephony/Phone;->PREFERRED_NT_MODE:I

    #@2f
    .line 3566
    :cond_2f
    return v0
.end method

.method protected getPrioritySortedApnContextList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/ApnContext;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2319
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->getPrioritySortedApnContextList(I)Ljava/util/List;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method protected getPrioritySortedApnContextList(I)Ljava/util/List;
    .registers 7
    .parameter "policy"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/ApnContext;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2324
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 2330
    .local v2, sortedList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/ApnContext;>;"
    sget-object v3, Lcom/android/internal/telephony/DataConnectionTracker;->mApnPriorities:Ljava/util/LinkedHashMap;

    #@7
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    #@a
    move-result-object v3

    #@b
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v1

    #@f
    .line 2331
    .local v1, apnTypes:Ljava/util/Iterator;
    :cond_f
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_27

    #@15
    .line 2332
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@17
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Lcom/android/internal/telephony/ApnContext;

    #@21
    .line 2333
    .local v0, apnContext:Lcom/android/internal/telephony/ApnContext;
    if-eqz v0, :cond_f

    #@23
    .line 2334
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@26
    goto :goto_f

    #@27
    .line 2337
    .end local v0           #apnContext:Lcom/android/internal/telephony/ApnContext;
    :cond_27
    sget-object v3, Lcom/android/internal/telephony/DataConnectionTracker;->mApnPrioComparator:Ljava/util/Comparator;

    #@29
    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@2c
    .line 2338
    if-nez p1, :cond_31

    #@2e
    .line 2339
    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    #@31
    .line 2341
    :cond_31
    return-object v2
.end method

.method public getRecoveryAction()I
    .registers 5

    #@0
    .prologue
    .line 2528
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v1

    #@a
    const-string v2, "radio.data.stall.recovery.action"

    #@c
    const/4 v3, 0x0

    #@d
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10
    move-result v0

    #@11
    .line 2530
    .local v0, action:I
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "getRecoveryAction: "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@27
    .line 2531
    return v0
.end method

.method protected getReryConfig(Z)Ljava/lang/String;
    .registers 4
    .parameter "forDefault"

    #@0
    .prologue
    .line 2346
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getNetworkType()I

    #@9
    move-result v0

    #@a
    .line 2348
    .local v0, nt:I
    const/4 v1, 0x4

    #@b
    if-eq v0, v1, :cond_1e

    #@d
    const/4 v1, 0x7

    #@e
    if-eq v0, v1, :cond_1e

    #@10
    const/4 v1, 0x5

    #@11
    if-eq v0, v1, :cond_1e

    #@13
    const/4 v1, 0x6

    #@14
    if-eq v0, v1, :cond_1e

    #@16
    const/16 v1, 0xc

    #@18
    if-eq v0, v1, :cond_1e

    #@1a
    const/16 v1, 0xe

    #@1c
    if-ne v0, v1, :cond_25

    #@1e
    .line 2355
    :cond_1e
    const-string v1, "ro.cdma.data_retry_config"

    #@20
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    .line 2361
    :goto_24
    return-object v1

    #@25
    .line 2358
    :cond_25
    if-eqz p1, :cond_2e

    #@27
    .line 2359
    const-string v1, "ro.gsm.data_retry_config"

    #@29
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    goto :goto_24

    #@2e
    .line 2361
    :cond_2e
    const-string v1, "ro.gsm.2nd_data_retry_config"

    #@30
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    goto :goto_24
.end method

.method public abstract getState(Ljava/lang/String;)Lcom/android/internal/telephony/DctConstants$State;
.end method

.method public getpdnnumpereachoper()I
    .registers 8

    #@0
    .prologue
    const/4 v0, 0x5

    #@1
    const/4 v2, 0x3

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x2

    #@4
    const/4 v1, 0x4

    #@5
    .line 3691
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@c
    move-result-object v5

    #@d
    iget v5, v5, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@f
    if-ne v5, v4, :cond_12

    #@11
    .line 3722
    :goto_11
    return v0

    #@12
    .line 3693
    :cond_12
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@14
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@16
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@19
    move-result-object v5

    #@1a
    iget v5, v5, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@1c
    if-ne v5, v1, :cond_20

    #@1e
    move v0, v1

    #@1f
    .line 3694
    goto :goto_11

    #@20
    .line 3696
    :cond_20
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@22
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@24
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@27
    move-result-object v5

    #@28
    iget v5, v5, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@2a
    const/16 v6, 0x8

    #@2c
    if-ne v5, v6, :cond_30

    #@2e
    move v0, v1

    #@2f
    .line 3697
    goto :goto_11

    #@30
    .line 3699
    :cond_30
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@32
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@34
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@37
    move-result-object v5

    #@38
    iget v5, v5, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@3a
    const/16 v6, 0xb

    #@3c
    if-ne v5, v6, :cond_40

    #@3e
    move v0, v2

    #@3f
    .line 3700
    goto :goto_11

    #@40
    .line 3703
    :cond_40
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@42
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@44
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@47
    move-result-object v5

    #@48
    iget v5, v5, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@4a
    const/4 v6, 0x7

    #@4b
    if-ne v5, v6, :cond_4f

    #@4d
    move v0, v1

    #@4e
    .line 3704
    goto :goto_11

    #@4f
    .line 3708
    :cond_4f
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@51
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@53
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@56
    move-result-object v5

    #@57
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SUPPORT_MPDN_SPRINT:Z

    #@59
    if-eqz v5, :cond_5d

    #@5b
    move v0, v1

    #@5c
    .line 3709
    goto :goto_11

    #@5d
    .line 3713
    :cond_5d
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5f
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@61
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@64
    move-result-object v1

    #@65
    iget v1, v1, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@67
    if-ne v1, v3, :cond_6b

    #@69
    move v0, v2

    #@6a
    .line 3714
    goto :goto_11

    #@6b
    .line 3715
    :cond_6b
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@6d
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@6f
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@72
    move-result-object v1

    #@73
    iget v1, v1, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@75
    if-ne v1, v0, :cond_79

    #@77
    move v0, v3

    #@78
    .line 3716
    goto :goto_11

    #@79
    .line 3717
    :cond_79
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7b
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7d
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@80
    move-result-object v0

    #@81
    iget v0, v0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@83
    const/4 v1, 0x6

    #@84
    if-ne v0, v1, :cond_88

    #@86
    move v0, v3

    #@87
    .line 3718
    goto :goto_11

    #@88
    :cond_88
    move v0, v4

    #@89
    .line 3722
    goto :goto_11
.end method

.method protected abstract gotoIdleAndNotifyDataConnection(Ljava/lang/String;)V
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 13
    .parameter "msg"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v8, 0x1

    #@3
    .line 1425
    iget v9, p1, Landroid/os/Message;->what:I

    #@5
    sparse-switch v9, :sswitch_data_1ce

    #@8
    .line 1577
    const-string v8, "DATA"

    #@a
    new-instance v9, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v10, "Unidentified event msg="

    #@11
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v9

    #@15
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v9

    #@19
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v9

    #@1d
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1580
    :cond_20
    :goto_20
    return-void

    #@21
    .line 1427
    :sswitch_21
    new-instance v8, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v9, "DISCONNECTED_CONNECTED: msg="

    #@28
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v8

    #@2c
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v8

    #@34
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@37
    .line 1428
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@39
    check-cast v3, Lcom/android/internal/telephony/DataConnectionAc;

    #@3b
    .line 1429
    .local v3, dcac:Lcom/android/internal/telephony/DataConnectionAc;
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionAsyncChannels:Ljava/util/HashMap;

    #@3d
    iget-object v9, v3, Lcom/android/internal/telephony/DataConnectionAc;->dataConnection:Lcom/android/internal/telephony/DataConnection;

    #@3f
    invoke-virtual {v9}, Lcom/android/internal/telephony/DataConnection;->getDataConnectionId()I

    #@42
    move-result v9

    #@43
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@46
    move-result-object v9

    #@47
    invoke-virtual {v8, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@4a
    .line 1430
    invoke-virtual {v3}, Lcom/android/internal/telephony/DataConnectionAc;->disconnected()V

    #@4d
    goto :goto_20

    #@4e
    .line 1434
    .end local v3           #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    :sswitch_4e
    iget v8, p1, Landroid/os/Message;->arg1:I

    #@50
    iget v9, p1, Landroid/os/Message;->arg2:I

    #@52
    invoke-virtual {p0, v8, v9}, Lcom/android/internal/telephony/DataConnectionTracker;->onEnableApn(II)V

    #@55
    goto :goto_20

    #@56
    .line 1438
    :sswitch_56
    const/4 v6, 0x0

    #@57
    .line 1439
    .local v6, reason:Ljava/lang/String;
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@59
    instance-of v8, v8, Ljava/lang/String;

    #@5b
    if-eqz v8, :cond_61

    #@5d
    .line 1440
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5f
    .end local v6           #reason:Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    #@61
    .line 1442
    .restart local v6       #reason:Ljava/lang/String;
    :cond_61
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/DataConnectionTracker;->onTrySetupData(Ljava/lang/String;)Z

    #@64
    goto :goto_20

    #@65
    .line 1446
    .end local v6           #reason:Ljava/lang/String;
    :sswitch_65
    iget v8, p1, Landroid/os/Message;->arg1:I

    #@67
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->onDataStallAlarm(I)V

    #@6a
    goto :goto_20

    #@6b
    .line 1450
    :sswitch_6b
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@6e
    move-result v8

    #@6f
    if-nez v8, :cond_74

    #@71
    .line 1451
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->resetAllRetryCounts()V

    #@74
    .line 1453
    :cond_74
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->onRoamingOff()V

    #@77
    .line 1455
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@7a
    move-result-object v8

    #@7b
    if-eqz v8, :cond_20

    #@7d
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@80
    move-result-object v8

    #@81
    invoke-interface {v8}, Lcom/lge/cappuccino/IMdm;->getManualSyncWhenRoaming()Z

    #@84
    move-result v8

    #@85
    if-eqz v8, :cond_20

    #@87
    .line 1457
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@8a
    move-result-object v8

    #@8b
    invoke-interface {v8, v10}, Lcom/lge/cappuccino/IMdm;->handleManualSync(Landroid/content/ComponentName;)V

    #@8e
    goto :goto_20

    #@8f
    .line 1463
    :sswitch_8f
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->onRoamingOn()V

    #@92
    .line 1465
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@95
    move-result-object v8

    #@96
    if-eqz v8, :cond_20

    #@98
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@9b
    move-result-object v8

    #@9c
    invoke-interface {v8}, Lcom/lge/cappuccino/IMdm;->getManualSyncWhenRoaming()Z

    #@9f
    move-result v8

    #@a0
    if-eqz v8, :cond_20

    #@a2
    .line 1467
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@a5
    move-result-object v8

    #@a6
    invoke-interface {v8, v10}, Lcom/lge/cappuccino/IMdm;->handleManualSync(Landroid/content/ComponentName;)V

    #@a9
    goto/16 :goto_20

    #@ab
    .line 1473
    :sswitch_ab
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->onRadioAvailable()V

    #@ae
    goto/16 :goto_20

    #@b0
    .line 1479
    :sswitch_b0
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b2
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@b4
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@b7
    move-result-object v8

    #@b8
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DISPLAY_DATAERROR_ICON_SPRINT:Z

    #@ba
    if-eqz v8, :cond_d6

    #@bc
    .line 1481
    const-string v8, "com.lge.android.data.DisplayDataErrorIcon : No Display (AirPlan on)"

    #@be
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@c1
    .line 1482
    new-instance v0, Landroid/content/Intent;

    #@c3
    const-string v8, "com.lge.android.data.DisplayDataErrorIcon"

    #@c5
    invoke-direct {v0, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c8
    .line 1483
    .local v0, DisplayDataErrorIcon:Landroid/content/Intent;
    const-string v8, "Display"

    #@ca
    invoke-virtual {v0, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@cd
    .line 1484
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@cf
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@d2
    move-result-object v8

    #@d3
    invoke-virtual {v8, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@d6
    .line 1488
    .end local v0           #DisplayDataErrorIcon:Landroid/content/Intent;
    :cond_d6
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->onRadioOffOrNotAvailable()V

    #@d9
    goto/16 :goto_20

    #@db
    .line 1492
    :sswitch_db
    iget v8, p1, Landroid/os/Message;->arg1:I

    #@dd
    iput v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mCidActive:I

    #@df
    .line 1493
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e1
    check-cast v8, Landroid/os/AsyncResult;

    #@e3
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->onDataSetupComplete(Landroid/os/AsyncResult;)V

    #@e6
    goto/16 :goto_20

    #@e8
    .line 1497
    :sswitch_e8
    new-instance v8, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string v9, "DataConnectoinTracker.handleMessage: EVENT_DISCONNECT_DONE msg="

    #@ef
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v8

    #@f3
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v8

    #@f7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fa
    move-result-object v8

    #@fb
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@fe
    .line 1498
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@100
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@102
    check-cast v8, Landroid/os/AsyncResult;

    #@104
    invoke-virtual {p0, v9, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->onDisconnectDone(ILandroid/os/AsyncResult;)V

    #@107
    goto/16 :goto_20

    #@109
    .line 1502
    :sswitch_109
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->onVoiceCallStarted()V

    #@10c
    goto/16 :goto_20

    #@10e
    .line 1506
    :sswitch_10e
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->onVoiceCallEnded()V

    #@111
    goto/16 :goto_20

    #@113
    .line 1510
    :sswitch_113
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@115
    check-cast v8, Ljava/lang/String;

    #@117
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->onCleanUpAllConnections(Ljava/lang/String;)V

    #@11a
    goto/16 :goto_20

    #@11c
    .line 1514
    :sswitch_11c
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@11e
    if-nez v9, :cond_12b

    #@120
    .line 1515
    .local v7, tearDown:Z
    :goto_120
    iget v9, p1, Landroid/os/Message;->arg2:I

    #@122
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@124
    check-cast v8, Ljava/lang/String;

    #@126
    invoke-virtual {p0, v7, v9, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->onCleanUpConnection(ZILjava/lang/String;)V

    #@129
    goto/16 :goto_20

    #@12b
    .end local v7           #tearDown:Z
    :cond_12b
    move v7, v8

    #@12c
    .line 1514
    goto :goto_120

    #@12d
    .line 1519
    :sswitch_12d
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@12f
    if-ne v9, v8, :cond_137

    #@131
    move v4, v8

    #@132
    .line 1520
    .local v4, enabled:Z
    :goto_132
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->onSetInternalDataEnabled(Z)V

    #@135
    goto/16 :goto_20

    #@137
    .end local v4           #enabled:Z
    :cond_137
    move v4, v7

    #@138
    .line 1519
    goto :goto_132

    #@139
    .line 1524
    :sswitch_139
    const-string v8, "EVENT_RESET_DONE"

    #@13b
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@13e
    .line 1525
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@140
    check-cast v8, Landroid/os/AsyncResult;

    #@142
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->onResetDone(Landroid/os/AsyncResult;)V

    #@145
    goto/16 :goto_20

    #@147
    .line 1529
    :sswitch_147
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@149
    if-ne v9, v8, :cond_167

    #@14b
    move v4, v8

    #@14c
    .line 1530
    .restart local v4       #enabled:Z
    :goto_14c
    new-instance v8, Ljava/lang/StringBuilder;

    #@14e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@151
    const-string v9, "CMD_SET_USER_DATA_ENABLE enabled="

    #@153
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v8

    #@157
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v8

    #@15b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15e
    move-result-object v8

    #@15f
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@162
    .line 1531
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->onSetUserDataEnabled(Z)V

    #@165
    goto/16 :goto_20

    #@167
    .end local v4           #enabled:Z
    :cond_167
    move v4, v7

    #@168
    .line 1529
    goto :goto_14c

    #@169
    .line 1535
    :sswitch_169
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@16b
    if-ne v9, v8, :cond_199

    #@16d
    move v5, v8

    #@16e
    .line 1536
    .local v5, met:Z
    :goto_16e
    new-instance v8, Ljava/lang/StringBuilder;

    #@170
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@173
    const-string v9, "CMD_SET_DEPENDENCY_MET met="

    #@175
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@178
    move-result-object v8

    #@179
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v8

    #@17d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@180
    move-result-object v8

    #@181
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@184
    .line 1537
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@187
    move-result-object v2

    #@188
    .line 1538
    .local v2, bundle:Landroid/os/Bundle;
    if-eqz v2, :cond_20

    #@18a
    .line 1539
    const-string v8, "apnType"

    #@18c
    invoke-virtual {v2, v8}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@18f
    move-result-object v1

    #@190
    check-cast v1, Ljava/lang/String;

    #@192
    .line 1540
    .local v1, apnType:Ljava/lang/String;
    if-eqz v1, :cond_20

    #@194
    .line 1541
    invoke-virtual {p0, v1, v5}, Lcom/android/internal/telephony/DataConnectionTracker;->onSetDependencyMet(Ljava/lang/String;Z)V

    #@197
    goto/16 :goto_20

    #@199
    .end local v1           #apnType:Ljava/lang/String;
    .end local v2           #bundle:Landroid/os/Bundle;
    .end local v5           #met:Z
    :cond_199
    move v5, v7

    #@19a
    .line 1535
    goto :goto_16e

    #@19b
    .line 1547
    :sswitch_19b
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@19d
    if-ne v9, v8, :cond_1a5

    #@19f
    move v4, v8

    #@1a0
    .line 1548
    .restart local v4       #enabled:Z
    :goto_1a0
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->onSetPolicyDataEnabled(Z)V

    #@1a3
    goto/16 :goto_20

    #@1a5
    .end local v4           #enabled:Z
    :cond_1a5
    move v4, v7

    #@1a6
    .line 1547
    goto :goto_1a0

    #@1a7
    .line 1552
    :sswitch_1a7
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->onUpdateIcc()V

    #@1aa
    goto/16 :goto_20

    #@1ac
    .line 1555
    :sswitch_1ac
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1ae
    check-cast v8, Landroid/os/AsyncResult;

    #@1b0
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->onTetheredModeStateChanged(Landroid/os/AsyncResult;)V

    #@1b3
    goto/16 :goto_20

    #@1b5
    .line 1558
    :sswitch_1b5
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->onApnChanged()V

    #@1b8
    goto/16 :goto_20

    #@1ba
    .line 1562
    :sswitch_1ba
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->handleGetPreferredNetworkTypeAPNChangeResponse(Landroid/os/Message;)V

    #@1bd
    goto/16 :goto_20

    #@1bf
    .line 1567
    :sswitch_1bf
    const-string v8, "[EHRPD_IPV6] Got event EVENT_PPP_RESYNC_TO_BLOCK_EHRPD_INTERNET_IPV6"

    #@1c1
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1c4
    .line 1568
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->on_PPP_RESYNC_Ehrpd_Internet_Ipv6_block_requested()V

    #@1c7
    goto/16 :goto_20

    #@1c9
    .line 1573
    :sswitch_1c9
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->onCpaPackageCheck()V

    #@1cc
    goto/16 :goto_20

    #@1ce
    .line 1425
    :sswitch_data_1ce
    .sparse-switch
        0x400 -> :sswitch_1c9
        0x11004 -> :sswitch_21
        0x42000 -> :sswitch_db
        0x42001 -> :sswitch_ab
        0x42003 -> :sswitch_56
        0x42006 -> :sswitch_b0
        0x42007 -> :sswitch_109
        0x42008 -> :sswitch_10e
        0x4200b -> :sswitch_8f
        0x4200c -> :sswitch_6b
        0x4200d -> :sswitch_4e
        0x4200f -> :sswitch_e8
        0x42011 -> :sswitch_65
        0x42013 -> :sswitch_1b5
        0x42018 -> :sswitch_11c
        0x4201b -> :sswitch_12d
        0x4201c -> :sswitch_139
        0x4201d -> :sswitch_113
        0x4201e -> :sswitch_147
        0x4201f -> :sswitch_169
        0x42020 -> :sswitch_19b
        0x42021 -> :sswitch_1a7
        0x42022 -> :sswitch_1ac
        0x42030 -> :sswitch_1bf
        0x42033 -> :sswitch_1ba
    .end sparse-switch
.end method

.method protected declared-synchronized isApnIdEnabled(I)Z
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 1829
    monitor-enter p0

    #@1
    const/4 v0, -0x1

    #@2
    if-eq p1, v0, :cond_a

    #@4
    .line 1830
    :try_start_4
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@6
    aget-boolean v0, v0, p1
    :try_end_8
    .catchall {:try_start_4 .. :try_end_8} :catchall_c

    #@8
    .line 1832
    :goto_8
    monitor-exit p0

    #@9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_8

    #@c
    .line 1829
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public isApnTypeActive(Ljava/lang/String;)Z
    .registers 7
    .parameter "type"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1218
    const-string v3, "dun"

    #@4
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_27

    #@a
    .line 1219
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->fetchDunApn()Lcom/android/internal/telephony/DataProfile;

    #@d
    move-result-object v0

    #@e
    .line 1220
    .local v0, dunApn:Lcom/android/internal/telephony/DataProfile;
    if-eqz v0, :cond_27

    #@10
    .line 1221
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@12
    if-eqz v3, :cond_25

    #@14
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataProfile;->toHash()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@1a
    invoke-virtual {v4}, Lcom/android/internal/telephony/DataProfile;->toHash()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_25

    #@24
    .line 1224
    .end local v0           #dunApn:Lcom/android/internal/telephony/DataProfile;
    :cond_24
    :goto_24
    return v1

    #@25
    .restart local v0       #dunApn:Lcom/android/internal/telephony/DataProfile;
    :cond_25
    move v1, v2

    #@26
    .line 1221
    goto :goto_24

    #@27
    .line 1224
    .end local v0           #dunApn:Lcom/android/internal/telephony/DataProfile;
    :cond_27
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@29
    if-eqz v3, :cond_33

    #@2b
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@2d
    invoke-virtual {v3, p1}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@30
    move-result v3

    #@31
    if-nez v3, :cond_24

    #@33
    :cond_33
    move v1, v2

    #@34
    goto :goto_24
.end method

.method protected abstract isApnTypeAvailable(Ljava/lang/String;)Z
.end method

.method public isApnTypeEnabled(Ljava/lang/String;)Z
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 1821
    if-nez p1, :cond_4

    #@2
    .line 1822
    const/4 v0, 0x0

    #@3
    .line 1824
    :goto_3
    return v0

    #@4
    :cond_4
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnIdEnabled(I)Z

    #@b
    move-result v0

    #@c
    goto :goto_3
.end method

.method public isCompleteIMSforDelayTime()Z
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 2057
    const/4 v2, 0x0

    #@2
    .line 2059
    .local v2, isOk:Z
    new-instance v0, Landroid/content/Intent;

    #@4
    const-string v3, "android.intent.action.DATA_DISABLE"

    #@6
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9
    .line 2060
    .local v0, DataDisabledIntent:Landroid/content/Intent;
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@12
    .line 2062
    const-string v3, "[LGE_DATA] broadcasting [DATA_DISABLED] intent.. IMS module may receive this intent, on closeNetwork case"

    #@14
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@17
    .line 2063
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@19
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getDataState()I

    #@20
    move-result v3

    #@21
    if-ne v3, v6, :cond_29

    #@23
    .line 2065
    const-string v3, "[LGE_DATA] In OOS! don\'t start IMS de-reg timer"

    #@25
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@28
    .line 2081
    :goto_28
    return v6

    #@29
    .line 2069
    :cond_29
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "[LGE_DATA] setting value is "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@36
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3d
    move-result-object v4

    #@3e
    const-string v5, "mobile_data"

    #@40
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@43
    move-result v4

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@4f
    .line 2070
    const/4 v1, 0x4

    #@50
    .line 2072
    .local v1, count:I
    :goto_50
    iget-boolean v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->imsRegiState:Z

    #@52
    if-eqz v3, :cond_5e

    #@54
    if-lez v1, :cond_5e

    #@56
    .line 2074
    const-wide/16 v3, 0x3e8

    #@58
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    #@5b
    .line 2075
    add-int/lit8 v1, v1, -0x1

    #@5d
    goto :goto_50

    #@5e
    .line 2078
    :cond_5e
    const/4 v2, 0x1

    #@5f
    .line 2079
    new-instance v3, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v4, "[IMS] ["

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    rsub-int/lit8 v4, v1, 0x4

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    const-string v4, "] sec is delayed for SIP de-reg >> isOK :"

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@79
    move-result-object v3

    #@7a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v3

    #@7e
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@81
    goto :goto_28
.end method

.method protected isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 2540
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected abstract isDataAllowed()Z
.end method

.method protected abstract isDataPossible(Ljava/lang/String;)Z
.end method

.method protected isDataSetupCompleteOk(Landroid/os/AsyncResult;)Z
    .registers 5
    .parameter "ar"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 984
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3
    if-eqz v1, :cond_1e

    #@5
    .line 985
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "isDataSetupCompleteOk return false, ar.result="

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1d
    .line 999
    :goto_1d
    return v0

    #@1e
    .line 988
    :cond_1e
    iget v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupCounter:I

    #@20
    if-gtz v1, :cond_29

    #@22
    .line 989
    const-string v0, "isDataSetupCompleteOk return true"

    #@24
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@27
    .line 990
    const/4 v0, 0x1

    #@28
    goto :goto_1d

    #@29
    .line 992
    :cond_29
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2b
    iput-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2d
    .line 994
    new-instance v1, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v2, "isDataSetupCompleteOk return false mFailDataSetupCounter="

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    iget v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupCounter:I

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    const-string v2, " mFailDataSetupFailCause="

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@51
    .line 998
    iget v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupCounter:I

    #@53
    add-int/lit8 v1, v1, -0x1

    #@55
    iput v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupCounter:I

    #@57
    goto :goto_1d
.end method

.method public abstract isDisconnected()Z
.end method

.method protected isEmergency()Z
    .registers 4

    #@0
    .prologue
    .line 1616
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataEnabledLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1617
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->isInEcm()Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_13

    #@b
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->isInEmergencyCall()Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_2c

    #@13
    :cond_13
    const/4 v0, 0x1

    #@14
    .line 1618
    .local v0, result:Z
    :goto_14
    monitor-exit v2
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_2e

    #@15
    .line 1619
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "isEmergency: result="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@2b
    .line 1620
    return v0

    #@2c
    .line 1617
    .end local v0           #result:Z
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_14

    #@2e
    .line 1618
    :catchall_2e
    move-exception v1

    #@2f
    :try_start_2f
    monitor-exit v2
    :try_end_30
    .catchall {:try_start_2f .. :try_end_30} :catchall_2e

    #@30
    throw v1
.end method

.method public isEmergencyAttachSupportedOnLte()Z
    .registers 3

    #@0
    .prologue
    .line 4095
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mEmerAttachSupport:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@2
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->EMER_ATTACH_SUPPORT:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@4
    if-ne v0, v1, :cond_8

    #@6
    .line 4096
    const/4 v0, 0x1

    #@7
    .line 4098
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isEmergencyCallSupportedOnLte()Z
    .registers 3

    #@0
    .prologue
    .line 4088
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mEPDNSupport:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@2
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;->EPDN_SUPPORT:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@4
    if-ne v0, v1, :cond_8

    #@6
    .line 4089
    const/4 v0, 0x1

    #@7
    .line 4091
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isInternetPDNconnected()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 4118
    iget-boolean v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->internetPDNconnected:Z

    #@3
    if-ne v1, v0, :cond_6

    #@5
    .line 4121
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isOtaAttachedOnLte()Z
    .registers 5

    #@0
    .prologue
    .line 4103
    const/4 v0, 0x0

    #@1
    .line 4105
    .local v0, usimIsEmpty:I
    new-instance v1, Lcom/android/internal/telephony/uicc/UsimService;

    #@3
    invoke-direct {v1}, Lcom/android/internal/telephony/uicc/UsimService;-><init>()V

    #@6
    .line 4106
    .local v1, usimService:Lcom/android/internal/telephony/uicc/UsimService;
    if-eqz v1, :cond_22

    #@8
    .line 4107
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UsimService;->getUsimIsEmpty()I

    #@b
    move-result v0

    #@c
    .line 4108
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "[LGE_DATA] isOtaAttachedOnLte check UiccIsEmpty =  "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@22
    .line 4110
    :cond_22
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@24
    sget-object v3, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->NORMAL_ATTACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@26
    if-ne v2, v3, :cond_2d

    #@28
    const/4 v2, 0x2

    #@29
    if-ne v0, v2, :cond_2d

    #@2b
    .line 4111
    const/4 v2, 0x1

    #@2c
    .line 4113
    :goto_2c
    return v2

    #@2d
    :cond_2d
    const/4 v2, 0x0

    #@2e
    goto :goto_2c
.end method

.method public isVoiceCallSupprotedOnLte()Z
    .registers 3

    #@0
    .prologue
    .line 4081
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mVolteSupport:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@2
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;->VOLTE_SUPPORT:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@4
    if-ne v0, v1, :cond_8

    #@6
    .line 4082
    const/4 v0, 0x1

    #@7
    .line 4084
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method protected loadKeyFromDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 14
    .parameter "numeric"
    .parameter "autoprofileKey"
    .parameter "key"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 4272
    const/4 v6, 0x0

    #@3
    .line 4273
    .local v6, DCMSettings:Ljava/lang/String;
    const-string v0, "content://telephony/dcm_settings"

    #@5
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@8
    move-result-object v1

    #@9
    .line 4275
    .local v1, DCM_SETTINGS_URI:Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v5, "numeric = \'"

    #@10
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    const-string v5, "\'"

    #@1a
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v5, " and "

    #@20
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    const-string v5, "extraid = \'"

    #@26
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    const-string v5, "\'"

    #@30
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    .line 4276
    .local v3, selection:Ljava/lang/String;
    const/4 v0, 0x2

    #@39
    new-array v2, v0, [Ljava/lang/String;

    #@3b
    const/4 v0, 0x0

    #@3c
    const-string v5, "_id"

    #@3e
    aput-object v5, v2, v0

    #@40
    aput-object p3, v2, v9

    #@42
    .line 4277
    .local v2, columns:[Ljava/lang/String;
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@44
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@47
    move-result-object v0

    #@48
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4b
    move-result-object v0

    #@4c
    move-object v5, v4

    #@4d
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@50
    move-result-object v7

    #@51
    .line 4278
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_59

    #@53
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@56
    move-result v0

    #@57
    if-nez v0, :cond_da

    #@59
    .line 4280
    :cond_59
    new-instance v0, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v5, "[loadKeyFromDB] connot find the "

    #@60
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v0

    #@64
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v0

    #@68
    const-string v5, " setting with ("

    #@6a
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    const-string v5, ")"

    #@74
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v0

    #@78
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v0

    #@7c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V

    #@7f
    .line 4281
    if-eqz v7, :cond_84

    #@81
    .line 4282
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@84
    .line 4285
    :cond_84
    new-instance v0, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v5, "numeric = \'"

    #@8b
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v0

    #@8f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v0

    #@93
    const-string v5, "\'"

    #@95
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v0

    #@99
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v3

    #@9d
    .line 4286
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@9f
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@a2
    move-result-object v0

    #@a3
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a6
    move-result-object v0

    #@a7
    move-object v5, v4

    #@a8
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@ab
    move-result-object v7

    #@ac
    .line 4287
    if-eqz v7, :cond_b4

    #@ae
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@b1
    move-result v0

    #@b2
    if-nez v0, :cond_da

    #@b4
    .line 4288
    :cond_b4
    new-instance v0, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v4, "[loadKeyFromDB] connot find the "

    #@bb
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v0

    #@bf
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v0

    #@c3
    const-string v4, " setting with ("

    #@c5
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v0

    #@c9
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v0

    #@cd
    const-string v4, "), too"

    #@cf
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v0

    #@d3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v0

    #@d7
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V

    #@da
    .line 4291
    :cond_da
    if-eqz v7, :cond_ea

    #@dc
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@df
    move-result v0

    #@e0
    if-lez v0, :cond_ea

    #@e2
    .line 4295
    :try_start_e2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@e5
    .line 4296
    const/4 v0, 0x1

    #@e6
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_e9
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_e2 .. :try_end_e9} :catch_f0

    #@e9
    move-result-object v6

    #@ea
    .line 4304
    :cond_ea
    :goto_ea
    if-eqz v7, :cond_ef

    #@ec
    .line 4305
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@ef
    .line 4307
    :cond_ef
    return-object v6

    #@f0
    .line 4298
    :catch_f0
    move-exception v8

    #@f1
    .line 4300
    .local v8, e:Ljava/lang/IndexOutOfBoundsException;
    const/4 v6, 0x0

    #@f2
    .line 4301
    new-instance v0, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v4, "[loadKeyFromDB] not find index for "

    #@f9
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v0

    #@fd
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v0

    #@101
    const-string v4, " with name because of CursorIndexOutOfBoundsException"

    #@103
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v0

    #@107
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v0

    #@10b
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V

    #@10e
    goto :goto_ea
.end method

.method protected loadPreferAPN()V
    .registers 1

    #@0
    .prologue
    .line 896
    return-void
.end method

.method protected abstract log(Ljava/lang/String;)V
.end method

.method protected abstract loge(Ljava/lang/String;)V
.end method

.method protected notifyDataConnection(Ljava/lang/String;)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    .line 1774
    const/4 v0, 0x0

    #@1
    .local v0, id:I
    :goto_1
    const/16 v1, 0x12

    #@3
    if-ge v0, v1, :cond_17

    #@5
    .line 1775
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@7
    aget-boolean v1, v1, v0

    #@9
    if-eqz v1, :cond_14

    #@b
    .line 1776
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, p1, v2}, Lcom/android/internal/telephony/PhoneBase;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 1774
    :cond_14
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_1

    #@17
    .line 1779
    :cond_17
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@1a
    .line 1780
    return-void
.end method

.method protected notifyOffApnsOfAvailability(Ljava/lang/String;)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    .line 1812
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "notifyOffApnsOfAvailability - reason= "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@16
    .line 1813
    const/4 v0, 0x0

    #@17
    .local v0, id:I
    :goto_17
    const/16 v1, 0x12

    #@19
    if-ge v0, v1, :cond_27

    #@1b
    .line 1814
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnIdEnabled(I)Z

    #@1e
    move-result v1

    #@1f
    if-nez v1, :cond_24

    #@21
    .line 1815
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->notifyApnIdDisconnected(Ljava/lang/String;I)V

    #@24
    .line 1813
    :cond_24
    add-int/lit8 v0, v0, 0x1

    #@26
    goto :goto_17

    #@27
    .line 1818
    :cond_27
    return-void
.end method

.method protected onActionIntentDataStallAlarm(Landroid/content/Intent;)V
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 1015
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "onActionIntentDataStallAlarm: action="

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1a
    .line 1016
    const v1, 0x42011

    #@1d
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@24
    move-result-object v0

    #@25
    .line 1018
    .local v0, msg:Landroid/os/Message;
    const-string v1, "data.stall.alram.tag"

    #@27
    const/4 v2, 0x0

    #@28
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2b
    move-result v1

    #@2c
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@2e
    .line 1019
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@31
    .line 1020
    return-void
.end method

.method protected onActionIntentReconnectAlarm(Landroid/content/Intent;)V
    .registers 7
    .parameter "intent"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1003
    const-string v2, "reconnect_alarm_extra_reason"

    #@3
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    .line 1004
    .local v1, reason:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@9
    sget-object v3, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@b
    if-ne v2, v3, :cond_1d

    #@d
    .line 1005
    const v2, 0x42018

    #@10
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    .line 1006
    .local v0, msg:Landroid/os/Message;
    iput v4, v0, Landroid/os/Message;->arg1:I

    #@16
    .line 1007
    iput v4, v0, Landroid/os/Message;->arg2:I

    #@18
    .line 1008
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1a
    .line 1009
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@1d
    .line 1011
    .end local v0           #msg:Landroid/os/Message;
    :cond_1d
    const v2, 0x42003

    #@20
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@27
    .line 1012
    return-void
.end method

.method protected abstract onApnChanged()V
.end method

.method protected abstract onCleanUpAllConnections(Ljava/lang/String;)V
.end method

.method protected abstract onCleanUpConnection(ZILjava/lang/String;)V
.end method

.method protected onCpaPackageCheck()V
    .registers 11

    #@0
    .prologue
    .line 4144
    iget-boolean v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@2
    if-eqz v8, :cond_68

    #@4
    .line 4146
    const/4 v1, 0x0

    #@5
    .line 4147
    .local v1, bResult:Z
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@a
    move-result-object v2

    #@b
    .line 4148
    .local v2, context:Landroid/content/Context;
    const-string v8, "activity"

    #@d
    invoke-virtual {v2, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/app/ActivityManager;

    #@13
    .line 4149
    .local v0, am:Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    #@16
    move-result-object v5

    #@17
    .line 4150
    .local v5, proceses:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-eqz v5, :cond_34

    #@19
    .line 4151
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1c
    move-result-object v3

    #@1d
    .local v3, i$:Ljava/util/Iterator;
    :cond_1d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@20
    move-result v8

    #@21
    if-eqz v8, :cond_34

    #@23
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@26
    move-result-object v6

    #@27
    check-cast v6, Landroid/app/ActivityManager$RunningAppProcessInfo;

    #@29
    .line 4152
    .local v6, process:Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v8, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    #@2b
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_PackageName:Ljava/lang/String;

    #@2d
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v8

    #@31
    if-eqz v8, :cond_1d

    #@33
    .line 4153
    const/4 v1, 0x1

    #@34
    .line 4158
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v6           #process:Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_34
    new-instance v8, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v9, "************** onCpaPackageCheck cpa_PackageName:"

    #@3b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_PackageName:Ljava/lang/String;

    #@41
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v8

    #@45
    const-string v9, " is available? "

    #@47
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v8

    #@4b
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v8

    #@4f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v8

    #@53
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@56
    .line 4159
    if-nez v1, :cond_69

    #@58
    .line 4161
    new-instance v7, Landroid/content/Intent;

    #@5a
    const-string v8, "com.kddi.android.cpa_CHANGED"

    #@5c
    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5f
    .line 4162
    .local v7, sintent:Landroid/content/Intent;
    const-string v8, "cpa_enable"

    #@61
    const/4 v9, 0x0

    #@62
    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@65
    .line 4163
    invoke-virtual {v2, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@68
    .line 4173
    .end local v0           #am:Landroid/app/ActivityManager;
    .end local v1           #bResult:Z
    .end local v2           #context:Landroid/content/Context;
    .end local v5           #proceses:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    .end local v7           #sintent:Landroid/content/Intent;
    :cond_68
    :goto_68
    return-void

    #@69
    .line 4168
    .restart local v0       #am:Landroid/app/ActivityManager;
    .restart local v1       #bResult:Z
    .restart local v2       #context:Landroid/content/Context;
    .restart local v5       #proceses:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :cond_69
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage()Landroid/os/Message;

    #@6c
    move-result-object v4

    #@6d
    .line 4169
    .local v4, msg:Landroid/os/Message;
    const/16 v8, 0x400

    #@6f
    iput v8, v4, Landroid/os/Message;->what:I

    #@71
    .line 4170
    const-wide/16 v8, 0xbb8

    #@73
    invoke-virtual {p0, v4, v8, v9}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@76
    goto :goto_68
.end method

.method protected abstract onDataConnectionAttached()V
.end method

.method protected abstract onDataSetupComplete(Landroid/os/AsyncResult;)V
.end method

.method protected onDataStallAlarm(I)V
    .registers 8
    .parameter "tag"

    #@0
    .prologue
    .line 2643
    iget v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmTag:I

    #@2
    if-eq v2, p1, :cond_27

    #@4
    .line 2645
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "onDataStallAlarm: ignore, tag="

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, " expecting "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    iget v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmTag:I

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@26
    .line 2669
    :goto_26
    return-void

    #@27
    .line 2649
    :cond_27
    invoke-direct {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->updateDataStallInfo()V

    #@2a
    .line 2651
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mResolver:Landroid/content/ContentResolver;

    #@2c
    const-string v3, "pdp_watchdog_trigger_packet_count"

    #@2e
    const/16 v4, 0xa

    #@30
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@33
    move-result v0

    #@34
    .line 2655
    .local v0, hangWatchdogTrigger:I
    const/4 v1, 0x0

    #@35
    .line 2656
    .local v1, suspectedStall:Z
    iget-wide v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@37
    int-to-long v4, v0

    #@38
    cmp-long v2, v2, v4

    #@3a
    if-ltz v2, :cond_6f

    #@3c
    .line 2658
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "onDataStallAlarm: tag="

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    const-string v3, " do recovery action="

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getRecoveryAction()I

    #@54
    move-result v3

    #@55
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@60
    .line 2660
    const/4 v1, 0x1

    #@61
    .line 2661
    const v2, 0x42012

    #@64
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@67
    move-result-object v2

    #@68
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@6b
    .line 2668
    :goto_6b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->startDataStallAlarm(Z)V

    #@6e
    goto :goto_26

    #@6f
    .line 2664
    :cond_6f
    new-instance v2, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v3, "onDataStallAlarm: tag="

    #@76
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v2

    #@7a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    const-string v3, " Sent "

    #@80
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v2

    #@84
    iget-wide v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mSentSinceLastRecv:J

    #@86
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v2

    #@8e
    const-string v3, " pkts since last received, < watchdogTrigger="

    #@90
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v2

    #@94
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@97
    move-result-object v2

    #@98
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v2

    #@9c
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@9f
    goto :goto_6b
.end method

.method protected abstract onDisconnectDone(ILandroid/os/AsyncResult;)V
.end method

.method protected onEnableApn(II)V
    .registers 9
    .parameter "apnId"
    .parameter "enabled"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 1928
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "EVENT_APN_ENABLE_REQUEST apnId="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, ", apnType="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, ", enabled="

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, ", dataEnabled = "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@31
    aget-boolean v3, v3, p1

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, ", enabledCount = "

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    iget v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    const-string v3, ", isApnTypeActive = "

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@50
    move-result v3

    #@51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v2

    #@59
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@5c
    .line 1933
    if-ne p2, v4, :cond_8a

    #@5e
    .line 1934
    monitor-enter p0

    #@5f
    .line 1935
    :try_start_5f
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@61
    aget-boolean v2, v2, p1

    #@63
    if-nez v2, :cond_70

    #@65
    .line 1936
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@67
    const/4 v3, 0x1

    #@68
    aput-boolean v3, v2, p1

    #@6a
    .line 1937
    iget v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@6c
    add-int/lit8 v2, v2, 0x1

    #@6e
    iput v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@70
    .line 1939
    :cond_70
    monitor-exit p0
    :try_end_71
    .catchall {:try_start_5f .. :try_end_71} :catchall_81

    #@71
    .line 1940
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@74
    move-result-object v1

    #@75
    .line 1941
    .local v1, type:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@78
    move-result v2

    #@79
    if-nez v2, :cond_84

    #@7b
    .line 1942
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRequestedApnType:Ljava/lang/String;

    #@7d
    .line 1943
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->onEnableNewApn()V

    #@80
    .line 1977
    .end local v1           #type:Ljava/lang/String;
    :cond_80
    :goto_80
    return-void

    #@81
    .line 1939
    :catchall_81
    move-exception v2

    #@82
    :try_start_82
    monitor-exit p0
    :try_end_83
    .catchall {:try_start_82 .. :try_end_83} :catchall_81

    #@83
    throw v2

    #@84
    .line 1945
    .restart local v1       #type:Ljava/lang/String;
    :cond_84
    const-string v2, "apnSwitched"

    #@86
    invoke-direct {p0, v2, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->notifyApnIdUpToCurrent(Ljava/lang/String;I)V

    #@89
    goto :goto_80

    #@8a
    .line 1949
    .end local v1           #type:Ljava/lang/String;
    :cond_8a
    const/4 v0, 0x0

    #@8b
    .line 1950
    .local v0, didDisable:Z
    monitor-enter p0

    #@8c
    .line 1951
    :try_start_8c
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@8e
    aget-boolean v2, v2, p1

    #@90
    if-eqz v2, :cond_9e

    #@92
    .line 1952
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@94
    const/4 v3, 0x0

    #@95
    aput-boolean v3, v2, p1

    #@97
    .line 1953
    iget v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@99
    add-int/lit8 v2, v2, -0x1

    #@9b
    iput v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@9d
    .line 1954
    const/4 v0, 0x1

    #@9e
    .line 1956
    :cond_9e
    monitor-exit p0
    :try_end_9f
    .catchall {:try_start_8c .. :try_end_9f} :catchall_cc

    #@9f
    .line 1957
    if-eqz v0, :cond_80

    #@a1
    .line 1958
    iget v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@a3
    if-eqz v2, :cond_a8

    #@a5
    const/4 v2, 0x3

    #@a6
    if-ne p1, v2, :cond_b1

    #@a8
    .line 1959
    :cond_a8
    const-string v2, "default"

    #@aa
    iput-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRequestedApnType:Ljava/lang/String;

    #@ac
    .line 1960
    const-string v2, "dataDisabled"

    #@ae
    invoke-virtual {p0, v4, p1, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->onCleanUpConnection(ZILjava/lang/String;)V

    #@b1
    .line 1965
    :cond_b1
    const-string v2, "dataDisabled"

    #@b3
    invoke-direct {p0, v2, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->notifyApnIdDisconnected(Ljava/lang/String;I)V

    #@b6
    .line 1966
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@b8
    aget-boolean v2, v2, v5

    #@ba
    if-ne v2, v4, :cond_80

    #@bc
    const-string v2, "default"

    #@be
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@c1
    move-result v2

    #@c2
    if-nez v2, :cond_80

    #@c4
    .line 1972
    const-string v2, "default"

    #@c6
    iput-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRequestedApnType:Ljava/lang/String;

    #@c8
    .line 1973
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->onEnableNewApn()V

    #@cb
    goto :goto_80

    #@cc
    .line 1956
    :catchall_cc
    move-exception v2

    #@cd
    :try_start_cd
    monitor-exit p0
    :try_end_ce
    .catchall {:try_start_cd .. :try_end_ce} :catchall_cc

    #@ce
    throw v2
.end method

.method protected onEnableNewApn()V
    .registers 1

    #@0
    .prologue
    .line 1986
    return-void
.end method

.method public onLockStateChanged(Landroid/os/AsyncResult;)V
    .registers 6
    .parameter "ar"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 2282
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3
    if-eqz v1, :cond_1e

    #@5
    .line 2283
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "[DS]EVENT_DATA_LOCK_ORDER Err "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1d
    .line 2297
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 2285
    :cond_1e
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@20
    check-cast v1, [I

    #@22
    move-object v0, v1

    #@23
    check-cast v0, [I

    #@25
    .line 2287
    .local v0, ints:[I
    new-instance v1, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v2, "[DS]LockStateChanged ints[0] "

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    const/4 v2, 0x0

    #@31
    aget v2, v0, v2

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    const-string v2, " ints[1] "

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    aget v2, v0, v3

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@4a
    .line 2288
    aget v1, v0, v3

    #@4c
    if-ne v1, v3, :cond_1d

    #@4e
    .line 2290
    const-string v1, "[DS]LGT_AUTH_LOCK! cleanUpConnection()"

    #@50
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V

    #@53
    .line 2293
    const-string v1, "radioTurnedOff"

    #@55
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@58
    goto :goto_1d
.end method

.method protected abstract onRadioAvailable()V
.end method

.method protected abstract onRadioOffOrNotAvailable()V
.end method

.method protected onResetDone(Landroid/os/AsyncResult;)V
    .registers 4
    .parameter "ar"

    #@0
    .prologue
    .line 1996
    const-string v1, "EVENT_RESET_DONE"

    #@2
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@5
    .line 1997
    const/4 v0, 0x0

    #@6
    .line 1998
    .local v0, reason:Ljava/lang/String;
    iget-object v1, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@8
    instance-of v1, v1, Ljava/lang/String;

    #@a
    if-eqz v1, :cond_10

    #@c
    .line 1999
    iget-object v0, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@e
    .end local v0           #reason:Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    #@10
    .line 2001
    .restart local v0       #reason:Ljava/lang/String;
    :cond_10
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->gotoIdleAndNotifyDataConnection(Ljava/lang/String;)V

    #@13
    .line 2002
    return-void
.end method

.method protected abstract onRoamingOff()V
.end method

.method protected abstract onRoamingOn()V
.end method

.method protected onSetDependencyMet(Ljava/lang/String;Z)V
    .registers 3
    .parameter "apnType"
    .parameter "met"

    #@0
    .prologue
    .line 2247
    return-void
.end method

.method protected onSetInternalDataEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 2034
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataEnabledLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2035
    :try_start_3
    iput-boolean p1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mInternalDataEnabled:Z

    #@5
    .line 2036
    if-eqz p1, :cond_16

    #@7
    .line 2037
    const-string v0, "onSetInternalDataEnabled: changed to enabled, try to setup data call"

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@c
    .line 2038
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->resetAllRetryCounts()V

    #@f
    .line 2039
    const-string v0, "dataEnabled"

    #@11
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->onTrySetupData(Ljava/lang/String;)Z

    #@14
    .line 2044
    :goto_14
    monitor-exit v1

    #@15
    .line 2045
    return-void

    #@16
    .line 2041
    :cond_16
    const-string v0, "onSetInternalDataEnabled: changed to disabled, cleanUpAllConnections"

    #@18
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1b
    .line 2042
    const/4 v0, 0x0

    #@1c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@1f
    goto :goto_14

    #@20
    .line 2044
    :catchall_20
    move-exception v0

    #@21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_20

    #@22
    throw v0
.end method

.method protected onSetPolicyDataEnabled(Z)V
    .registers 6
    .parameter "enabled"

    #@0
    .prologue
    .line 2250
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataEnabledLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 2251
    :try_start_3
    sget-boolean v1, Lcom/android/internal/telephony/DataConnectionTracker;->sPolicyDataEnabled:Z

    #@5
    if-eq v1, p1, :cond_13

    #@7
    .line 2252
    sput-boolean p1, Lcom/android/internal/telephony/DataConnectionTracker;->sPolicyDataEnabled:Z

    #@9
    .line 2253
    if-eqz p1, :cond_15

    #@b
    .line 2254
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->resetAllRetryCounts()V

    #@e
    .line 2255
    const-string v1, "dataEnabled"

    #@10
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->onTrySetupData(Ljava/lang/String;)Z

    #@13
    .line 2277
    :cond_13
    :goto_13
    monitor-exit v2

    #@14
    .line 2278
    return-void

    #@15
    .line 2258
    :cond_15
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@17
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@19
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1c
    move-result-object v1

    #@1d
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DATA_MENU_NOT_CONRTOL:Z

    #@1f
    if-eqz v1, :cond_2a

    #@21
    .line 2259
    const-string v1, "specificDisabled"

    #@23
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->onCleanUpAllConnections(Ljava/lang/String;)V

    #@26
    goto :goto_13

    #@27
    .line 2277
    :catchall_27
    move-exception v1

    #@28
    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    #@29
    throw v1

    #@2a
    .line 2261
    :cond_2a
    :try_start_2a
    const-string v1, "dataDisabled"

    #@2c
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->onCleanUpAllConnections(Ljava/lang/String;)V

    #@2f
    .line 2264
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@31
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@33
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@36
    move-result-object v1

    #@37
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DISPLAY_DATAERROR_ICON_SPRINT:Z

    #@39
    if-eqz v1, :cond_13

    #@3b
    .line 2266
    const-string v1, "com.lge.android.data.DisplayDataErrorIcon datadisable: No Display(data disable 2)"

    #@3d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@40
    .line 2267
    new-instance v0, Landroid/content/Intent;

    #@42
    const-string v1, "com.lge.android.data.DisplayDataErrorIcon"

    #@44
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@47
    .line 2268
    .local v0, DisplayDataErrorIcon:Landroid/content/Intent;
    const-string v1, "Display"

    #@49
    const/4 v3, 0x0

    #@4a
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@4d
    .line 2269
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4f
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_56
    .catchall {:try_start_2a .. :try_end_56} :catchall_27

    #@56
    goto :goto_13
.end method

.method protected onSetUserDataEnabled(Z)V
    .registers 20
    .parameter "enabled"

    #@0
    .prologue
    .line 2088
    move-object/from16 v0, p0

    #@2
    iget-object v13, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataEnabledLock:Ljava/lang/Object;

    #@4
    monitor-enter v13

    #@5
    .line 2089
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getAnyDataEnabled()Z

    #@8
    move-result v9

    #@9
    .line 2090
    .local v9, prevEnabled:Z
    new-instance v12, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v14, "[LGE_DATA] onSetUserDataEnabled "

    #@10
    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v12

    #@14
    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17
    move-result-object v12

    #@18
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v12

    #@1c
    move-object/from16 v0, p0

    #@1e
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@21
    .line 2093
    move-object/from16 v0, p0

    #@23
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@25
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@27
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2a
    move-result-object v12

    #@2b
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MMS_ON_DATA_DISABLED_SKT:Z

    #@2d
    if-eqz v12, :cond_98

    #@2f
    .line 2095
    move-object/from16 v0, p0

    #@31
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@33
    const-string v14, "mms"

    #@35
    invoke-virtual {v12, v14}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    move-result-object v6

    #@39
    check-cast v6, Lcom/android/internal/telephony/ApnContext;

    #@3b
    .line 2097
    .local v6, apnContext:Lcom/android/internal/telephony/ApnContext;
    if-eqz v6, :cond_98

    #@3d
    .line 2099
    move-object/from16 v0, p0

    #@3f
    iget-boolean v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@41
    move/from16 v0, p1

    #@43
    if-eq v12, v0, :cond_98

    #@45
    const/4 v12, 0x1

    #@46
    move/from16 v0, p1

    #@48
    if-ne v0, v12, :cond_98

    #@4a
    invoke-virtual {v6}, Lcom/android/internal/telephony/ApnContext;->isEnabled()Z

    #@4d
    move-result v12

    #@4e
    if-eqz v12, :cond_98

    #@50
    move-object/from16 v0, p0

    #@52
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@54
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@57
    move-result-object v12

    #@58
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5b
    move-result-object v12

    #@5c
    const-string v14, "mobile_data"

    #@5e
    const/4 v15, 0x1

    #@5f
    invoke-static {v12, v14, v15}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@62
    move-result v12

    #@63
    if-nez v12, :cond_98

    #@65
    .line 2104
    const-string v12, "[LGE_DATA] It is NOT allowed to turn on Mobile Data while MMS is on with mobile off."

    #@67
    move-object/from16 v0, p0

    #@69
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@6c
    .line 2106
    new-instance v8, Landroid/content/Intent;

    #@6e
    const-string v12, "lge.intent.action.toast"

    #@70
    invoke-direct {v8, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@73
    .line 2107
    .local v8, intent:Landroid/content/Intent;
    const-string v12, "text"

    #@75
    move-object/from16 v0, p0

    #@77
    iget-object v14, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@79
    invoke-virtual {v14}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@7c
    move-result-object v14

    #@7d
    const v15, 0x1040598

    #@80
    invoke-virtual {v14, v15}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@83
    move-result-object v14

    #@84
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@87
    move-result-object v14

    #@88
    invoke-virtual {v8, v12, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@8b
    .line 2108
    move-object/from16 v0, p0

    #@8d
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8f
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@92
    move-result-object v12

    #@93
    invoke-virtual {v12, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@96
    .line 2110
    monitor-exit v13

    #@97
    .line 2244
    .end local v6           #apnContext:Lcom/android/internal/telephony/ApnContext;
    .end local v8           #intent:Landroid/content/Intent;
    :goto_97
    return-void

    #@98
    .line 2116
    :cond_98
    move-object/from16 v0, p0

    #@9a
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@9c
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9e
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@a1
    move-result-object v12

    #@a2
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_KR:Z

    #@a4
    if-eqz v12, :cond_12e

    #@a6
    .line 2117
    move-object/from16 v0, p0

    #@a8
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@aa
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@ad
    move-result-object v12

    #@ae
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b1
    move-result-object v12

    #@b2
    const-string v14, "airplane_mode_on"

    #@b4
    const/4 v15, 0x0

    #@b5
    invoke-static {v12, v14, v15}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@b8
    move-result v4

    #@b9
    .line 2118
    .local v4, airplaneMode:I
    move-object/from16 v0, p0

    #@bb
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@bd
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@c0
    move-result-object v12

    #@c1
    invoke-virtual {v12}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@c4
    move-result-object v12

    #@c5
    invoke-virtual {v12}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@c8
    move-result v11

    #@c9
    .line 2119
    .local v11, voice_call:Z
    move-object/from16 v0, p0

    #@cb
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@cd
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@d0
    move-result-object v12

    #@d1
    invoke-virtual {v12}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@d4
    move-result v10

    #@d5
    .line 2121
    .local v10, roaming:Z
    new-instance v12, Ljava/lang/StringBuilder;

    #@d7
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@da
    const-string v14, "[LGE_DATA] airplaneMode : "

    #@dc
    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v12

    #@e0
    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v12

    #@e4
    const-string v14, " / voice_call : "

    #@e6
    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v12

    #@ea
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v12

    #@ee
    const-string v14, " / roaming :"

    #@f0
    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v12

    #@f4
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v12

    #@f8
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v12

    #@fc
    move-object/from16 v0, p0

    #@fe
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@101
    .line 2123
    const/4 v12, 0x1

    #@102
    if-ne v10, v12, :cond_121

    #@104
    const-string v12, "ro.afwdata.LGfeatureset"

    #@106
    const-string v14, "none"

    #@108
    invoke-static {v12, v14}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@10b
    move-result-object v12

    #@10c
    const-string v14, "KTBASE"

    #@10e
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@111
    move-result v12

    #@112
    if-nez v12, :cond_121

    #@114
    .line 2124
    const-string v12, "[LGE_DATA] In Roaming, Setting Mobile_Data is not allowed."

    #@116
    move-object/from16 v0, p0

    #@118
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@11b
    .line 2125
    monitor-exit v13

    #@11c
    goto/16 :goto_97

    #@11e
    .line 2243
    .end local v4           #airplaneMode:I
    .end local v9           #prevEnabled:Z
    .end local v10           #roaming:Z
    .end local v11           #voice_call:Z
    :catchall_11e
    move-exception v12

    #@11f
    monitor-exit v13
    :try_end_120
    .catchall {:try_start_5 .. :try_end_120} :catchall_11e

    #@120
    throw v12

    #@121
    .line 2128
    .restart local v4       #airplaneMode:I
    .restart local v9       #prevEnabled:Z
    .restart local v10       #roaming:Z
    .restart local v11       #voice_call:Z
    :cond_121
    const/4 v12, 0x1

    #@122
    if-ne v4, v12, :cond_12e

    #@124
    .line 2129
    :try_start_124
    const-string v12, "[LGE_DATA] In Airplane Mode, Setting Mobile_Data is not allowed."

    #@126
    move-object/from16 v0, p0

    #@128
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@12b
    .line 2130
    monitor-exit v13

    #@12c
    goto/16 :goto_97

    #@12e
    .line 2137
    .end local v4           #airplaneMode:I
    .end local v10           #roaming:Z
    .end local v11           #voice_call:Z
    :cond_12e
    move-object/from16 v0, p0

    #@130
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@132
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@134
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@137
    move-result-object v12

    #@138
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODECHANGE:Z

    #@13a
    if-eqz v12, :cond_1e2

    #@13c
    .line 2138
    const/4 v7, 0x0

    #@13d
    .line 2140
    .local v7, bRunModeChange:Z
    const/4 v7, 0x0

    #@13e
    .line 2143
    move-object/from16 v0, p0

    #@140
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@142
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@144
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@147
    move-result-object v12

    #@148
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DEREGISTRATION:Z

    #@14a
    if-eqz v12, :cond_29d

    #@14c
    .line 2144
    if-nez p1, :cond_29a

    #@14e
    move-object/from16 v0, p0

    #@150
    iget-boolean v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->imsRegiState:Z

    #@152
    if-eqz v12, :cond_29a

    #@154
    .line 2145
    move-object/from16 v0, p0

    #@156
    iget-boolean v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->modeChangeAlarmState:Z

    #@158
    if-nez v12, :cond_291

    #@15a
    .line 2146
    new-instance v2, Landroid/content/Intent;

    #@15c
    const-string v12, "android.intent.action.DATA_DISABLE"

    #@15e
    invoke-direct {v2, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@161
    .line 2147
    .local v2, DataDisabledIntent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@163
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@165
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@168
    move-result-object v12

    #@169
    invoke-virtual {v12, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@16c
    .line 2149
    const-string v12, "[onSetUserDataEnabled] broadcasting [DATA_DISABLED] intent.. IMS module may receive this intent, on closeNetwork case"

    #@16e
    move-object/from16 v0, p0

    #@170
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@173
    .line 2151
    const-string v12, "[onSetUserDataEnabled] IMS deregistration Timer Start, because IMS client is not ready to disconnect for Mode Change"

    #@175
    move-object/from16 v0, p0

    #@177
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@17a
    .line 2152
    const/4 v12, 0x1

    #@17b
    move-object/from16 v0, p0

    #@17d
    iput-boolean v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->modeChangeAlarmState:Z

    #@17f
    .line 2153
    move-object/from16 v0, p0

    #@181
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@183
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@186
    move-result-object v12

    #@187
    const-string v14, "alarm"

    #@189
    invoke-virtual {v12, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@18c
    move-result-object v5

    #@18d
    check-cast v5, Landroid/app/AlarmManager;

    #@18f
    .line 2154
    .local v5, am:Landroid/app/AlarmManager;
    new-instance v8, Landroid/content/Intent;

    #@191
    const-string v12, "android.intent.action.ACTION_DELAY_MODE_CHANGE_FOR_IMS"

    #@193
    invoke-direct {v8, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@196
    .line 2155
    .restart local v8       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@198
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@19a
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@19d
    move-result-object v12

    #@19e
    const/4 v14, 0x0

    #@19f
    const/4 v15, 0x0

    #@1a0
    invoke-static {v12, v14, v8, v15}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@1a3
    move-result-object v12

    #@1a4
    move-object/from16 v0, p0

    #@1a6
    iput-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mDelayModeChangeforIms:Landroid/app/PendingIntent;

    #@1a8
    .line 2156
    move-object/from16 v0, p0

    #@1aa
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mDelayModeChangeforIms:Landroid/app/PendingIntent;

    #@1ac
    invoke-virtual {v5, v12}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@1af
    .line 2157
    const/4 v12, 0x2

    #@1b0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1b3
    move-result-wide v14

    #@1b4
    move-object/from16 v0, p0

    #@1b6
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1b8
    move-object/from16 v16, v0

    #@1ba
    move-object/from16 v0, v16

    #@1bc
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1be
    move-object/from16 v16, v0

    #@1c0
    invoke-interface/range {v16 .. v16}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1c3
    move-result-object v16

    #@1c4
    move-object/from16 v0, v16

    #@1c6
    iget v0, v0, Lcom/android/internal/telephony/LGfeature;->IMSPowerOffdelaytime:I

    #@1c8
    move/from16 v16, v0

    #@1ca
    move/from16 v0, v16

    #@1cc
    int-to-long v0, v0

    #@1cd
    move-wide/from16 v16, v0

    #@1cf
    add-long v14, v14, v16

    #@1d1
    move-object/from16 v0, p0

    #@1d3
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mDelayModeChangeforIms:Landroid/app/PendingIntent;

    #@1d5
    move-object/from16 v16, v0

    #@1d7
    move-object/from16 v0, v16

    #@1d9
    invoke-virtual {v5, v12, v14, v15, v0}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@1dc
    .line 2169
    .end local v2           #DataDisabledIntent:Landroid/content/Intent;
    .end local v5           #am:Landroid/app/AlarmManager;
    .end local v8           #intent:Landroid/content/Intent;
    :goto_1dc
    const/4 v12, 0x1

    #@1dd
    if-ne v12, v7, :cond_1e2

    #@1df
    .line 2170
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/DataConnectionTracker;->changePreferrredNetworkMode(Z)V

    #@1e2
    .line 2175
    .end local v7           #bRunModeChange:Z
    :cond_1e2
    move-object/from16 v0, p0

    #@1e4
    iget-boolean v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@1e6
    move/from16 v0, p1

    #@1e8
    if-eq v12, v0, :cond_28e

    #@1ea
    .line 2176
    move/from16 v0, p1

    #@1ec
    move-object/from16 v1, p0

    #@1ee
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@1f0
    .line 2177
    move-object/from16 v0, p0

    #@1f2
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1f4
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1f7
    move-result-object v12

    #@1f8
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1fb
    move-result-object v14

    #@1fc
    const-string v15, "mobile_data"

    #@1fe
    if-eqz p1, :cond_2a0

    #@200
    const/4 v12, 0x1

    #@201
    :goto_201
    invoke-static {v14, v15, v12}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@204
    .line 2181
    move-object/from16 v0, p0

    #@206
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@208
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@20a
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@20d
    move-result-object v12

    #@20e
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DISPLAY_DATAERROR_ICON_SPRINT:Z

    #@210
    if-eqz v12, :cond_233

    #@212
    .line 2183
    if-nez p1, :cond_233

    #@214
    .line 2185
    const-string v12, "com.lge.android.data.DisplayDataErrorIcon :Not display (reson : Data Disable)"

    #@216
    move-object/from16 v0, p0

    #@218
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@21b
    .line 2186
    new-instance v3, Landroid/content/Intent;

    #@21d
    const-string v12, "com.lge.android.data.DisplayDataErrorIcon"

    #@21f
    invoke-direct {v3, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@222
    .line 2187
    .local v3, DisplayDataErrorIcon:Landroid/content/Intent;
    const-string v12, "Display"

    #@224
    const/4 v14, 0x0

    #@225
    invoke-virtual {v3, v12, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@228
    .line 2188
    move-object/from16 v0, p0

    #@22a
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@22c
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@22f
    move-result-object v12

    #@230
    invoke-virtual {v12, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@233
    .line 2195
    .end local v3           #DisplayDataErrorIcon:Landroid/content/Intent;
    :cond_233
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@236
    move-result v12

    #@237
    if-nez v12, :cond_251

    #@239
    move-object/from16 v0, p0

    #@23b
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@23d
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@240
    move-result-object v12

    #@241
    invoke-virtual {v12}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@244
    move-result v12

    #@245
    const/4 v14, 0x1

    #@246
    if-ne v12, v14, :cond_251

    #@248
    .line 2197
    if-eqz p1, :cond_2a3

    #@24a
    .line 2198
    const-string v12, "roamingOn"

    #@24c
    move-object/from16 v0, p0

    #@24e
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@251
    .line 2205
    :cond_251
    :goto_251
    move-object/from16 v0, p0

    #@253
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@255
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@257
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@25a
    move-result-object v12

    #@25b
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LTE_ROAMING_KT:Z

    #@25d
    const/4 v14, 0x1

    #@25e
    if-ne v12, v14, :cond_2c9

    #@260
    .line 2206
    if-eqz p1, :cond_2ab

    #@262
    .line 2207
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionTracker;->resetAllRetryCounts()V

    #@265
    .line 2208
    const-string v12, "dataEnabled"

    #@267
    move-object/from16 v0, p0

    #@269
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->onTrySetupData(Ljava/lang/String;)Z

    #@26c
    .line 2236
    :cond_26c
    :goto_26c
    move-object/from16 v0, p0

    #@26e
    iget-boolean v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@270
    if-nez v12, :cond_28e

    #@272
    move-object/from16 v0, p0

    #@274
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@276
    invoke-virtual {v12}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@279
    move-result-object v12

    #@27a
    invoke-virtual {v12}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@27d
    move-result v12

    #@27e
    const/4 v14, 0x1

    #@27f
    if-ne v12, v14, :cond_28e

    #@281
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@284
    move-result v12

    #@285
    if-nez v12, :cond_28e

    #@287
    .line 2239
    const-string v12, "dataDisabled"

    #@289
    move-object/from16 v0, p0

    #@28b
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@28e
    .line 2243
    :cond_28e
    monitor-exit v13

    #@28f
    goto/16 :goto_97

    #@291
    .line 2159
    .restart local v7       #bRunModeChange:Z
    :cond_291
    const-string v12, "[onSetUserDataEnabled] During IMS Deregistation. Do not change network mode now."

    #@293
    move-object/from16 v0, p0

    #@295
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@298
    goto/16 :goto_1dc

    #@29a
    .line 2162
    :cond_29a
    const/4 v7, 0x1

    #@29b
    goto/16 :goto_1dc

    #@29d
    .line 2166
    :cond_29d
    const/4 v7, 0x1

    #@29e
    goto/16 :goto_1dc

    #@2a0
    .line 2177
    .end local v7           #bRunModeChange:Z
    :cond_2a0
    const/4 v12, 0x0

    #@2a1
    goto/16 :goto_201

    #@2a3
    .line 2200
    :cond_2a3
    const-string v12, "dataDisabled"

    #@2a5
    move-object/from16 v0, p0

    #@2a7
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@2aa
    goto :goto_251

    #@2ab
    .line 2211
    :cond_2ab
    move-object/from16 v0, p0

    #@2ad
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2af
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2b1
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2b4
    move-result-object v12

    #@2b5
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DATA_MENU_NOT_CONRTOL:Z

    #@2b7
    if-eqz v12, :cond_2c1

    #@2b9
    .line 2212
    const-string v12, "specificDisabled"

    #@2bb
    move-object/from16 v0, p0

    #@2bd
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->onCleanUpAllConnections(Ljava/lang/String;)V

    #@2c0
    goto :goto_26c

    #@2c1
    .line 2214
    :cond_2c1
    const-string v12, "dataDisabled"

    #@2c3
    move-object/from16 v0, p0

    #@2c5
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->onCleanUpAllConnections(Ljava/lang/String;)V

    #@2c8
    goto :goto_26c

    #@2c9
    .line 2220
    :cond_2c9
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getAnyDataEnabled()Z

    #@2cc
    move-result v12

    #@2cd
    if-eq v9, v12, :cond_26c

    #@2cf
    .line 2221
    if-nez v9, :cond_2dc

    #@2d1
    .line 2222
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionTracker;->resetAllRetryCounts()V

    #@2d4
    .line 2223
    const-string v12, "dataEnabled"

    #@2d6
    move-object/from16 v0, p0

    #@2d8
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->onTrySetupData(Ljava/lang/String;)Z

    #@2db
    goto :goto_26c

    #@2dc
    .line 2226
    :cond_2dc
    move-object/from16 v0, p0

    #@2de
    iget-object v12, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2e0
    iget-object v12, v12, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2e2
    invoke-interface {v12}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2e5
    move-result-object v12

    #@2e6
    iget-boolean v12, v12, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DATA_MENU_NOT_CONRTOL:Z

    #@2e8
    if-eqz v12, :cond_2f3

    #@2ea
    .line 2227
    const-string v12, "specificDisabled"

    #@2ec
    move-object/from16 v0, p0

    #@2ee
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->onCleanUpAllConnections(Ljava/lang/String;)V

    #@2f1
    goto/16 :goto_26c

    #@2f3
    .line 2229
    :cond_2f3
    const-string v12, "dataDisabled"

    #@2f5
    move-object/from16 v0, p0

    #@2f7
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/DataConnectionTracker;->onCleanUpAllConnections(Ljava/lang/String;)V
    :try_end_2fa
    .catchall {:try_start_124 .. :try_end_2fa} :catchall_11e

    #@2fa
    goto/16 :goto_26c
.end method

.method protected abstract onTrySetupData(Lcom/android/internal/telephony/ApnContext;)Z
.end method

.method protected abstract onTrySetupData(Ljava/lang/String;)Z
.end method

.method protected abstract onUpdateIcc()V
.end method

.method protected abstract onVoiceCallEnded()V
.end method

.method protected abstract onVoiceCallStarted()V
.end method

.method protected abstract on_PPP_RESYNC_Ehrpd_Internet_Ipv6_block_requested()V
.end method

.method public putRecoveryAction(I)V
    .registers 4
    .parameter "action"

    #@0
    .prologue
    .line 2534
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    const-string v1, "radio.data.stall.recovery.action"

    #@c
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@f
    .line 2536
    new-instance v0, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v1, "putRecoveryAction: "

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@25
    .line 2537
    return-void
.end method

.method protected reAttachDefaultPDN()V
    .registers 1

    #@0
    .prologue
    .line 2969
    return-void
.end method

.method public registerForDataConnectEvent(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 7
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1602
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1603
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 1605
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataEnabledLock:Ljava/lang/Object;

    #@c
    monitor-enter v2

    #@d
    .line 1606
    :try_start_d
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@10
    .line 1607
    monitor-exit v2

    #@11
    .line 1608
    return-void

    #@12
    .line 1607
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_d .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method protected resetAllRetryCounts()V
    .registers 5

    #@0
    .prologue
    .line 2411
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@2
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v3

    #@6
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v2

    #@a
    .local v2, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_1b

    #@10
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Lcom/android/internal/telephony/ApnContext;

    #@16
    .line 2412
    .local v0, ac:Lcom/android/internal/telephony/ApnContext;
    const/4 v3, 0x0

    #@17
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/ApnContext;->setRetryCount(I)V

    #@1a
    goto :goto_a

    #@1b
    .line 2414
    .end local v0           #ac:Lcom/android/internal/telephony/ApnContext;
    :cond_1b
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@1d
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@20
    move-result-object v3

    #@21
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@24
    move-result-object v2

    #@25
    :goto_25
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_35

    #@2b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2e
    move-result-object v1

    #@2f
    check-cast v1, Lcom/android/internal/telephony/DataConnection;

    #@31
    .line 2415
    .local v1, dc:Lcom/android/internal/telephony/DataConnection;
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->resetRetryCount()V

    #@34
    goto :goto_25

    #@35
    .line 2417
    .end local v1           #dc:Lcom/android/internal/telephony/DataConnection;
    :cond_35
    return-void
.end method

.method public resetNetworkModeToDefault()V
    .registers 4

    #@0
    .prologue
    .line 2919
    sget v0, Lcom/android/internal/telephony/Phone;->PREFERRED_NT_MODE:I

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredNetworkMode(I)V

    #@5
    .line 2921
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7
    sget v1, Lcom/android/internal/telephony/Phone;->PREFERRED_NT_MODE:I

    #@9
    const v2, 0x42029

    #@c
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/PhoneBase;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@13
    .line 2922
    return-void
.end method

.method protected resetPollStats()V
    .registers 3

    #@0
    .prologue
    const-wide/16 v0, -0x1

    #@2
    .line 2420
    iput-wide v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mTxPkts:J

    #@4
    .line 2421
    iput-wide v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRxPkts:J

    #@6
    .line 2422
    const/16 v0, 0x3e8

    #@8
    iput v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollPeriod:I

    #@a
    .line 2423
    return-void
.end method

.method protected restartDataStallAlarm()V
    .registers 3

    #@0
    .prologue
    .line 2754
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->isConnected()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_b

    #@6
    iget-boolean v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@8
    if-nez v1, :cond_b

    #@a
    .line 2774
    :cond_a
    :goto_a
    return-void

    #@b
    .line 2757
    :cond_b
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getRecoveryAction()I

    #@e
    move-result v0

    #@f
    .line 2759
    .local v0, nextAction:I
    invoke-static {v0}, Lcom/android/internal/telephony/DataConnectionTracker$RecoveryAction;->access$100(I)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_1b

    #@15
    .line 2760
    const-string v1, "data stall recovery action is pending. not resetting the alarm."

    #@17
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1a
    goto :goto_a

    #@1b
    .line 2769
    :cond_1b
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->isConnected()Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_a

    #@21
    iget-boolean v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@23
    if-nez v1, :cond_a

    #@25
    .line 2770
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->stopDataStallAlarm()V

    #@28
    .line 2771
    const/4 v1, 0x0

    #@29
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->startDataStallAlarm(Z)V

    #@2c
    goto :goto_a
.end method

.method protected abstract restartRadio()V
.end method

.method public sendPdnTable()V
    .registers 19

    #@0
    .prologue
    .line 3347
    move-object/from16 v0, p0

    #@2
    iget-object v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4
    invoke-virtual {v15}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@7
    move-result-object v15

    #@8
    invoke-virtual {v15}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v15

    #@c
    const-string v16, "SENDPDNTABLE_ENABLE_SAVE"

    #@e
    const/16 v17, 0x0

    #@10
    invoke-static/range {v15 .. v17}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@13
    move-result v15

    #@14
    const/16 v16, 0x1

    #@16
    move/from16 v0, v16

    #@18
    if-ne v15, v0, :cond_22

    #@1a
    .line 3348
    const-string v15, "NoDBSync is Enable, Do not send PDN Table."

    #@1c
    move-object/from16 v0, p0

    #@1e
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@21
    .line 3541
    :cond_21
    :goto_21
    return-void

    #@22
    .line 3353
    :cond_22
    const/4 v3, 0x0

    #@23
    .line 3354
    .local v3, defaultcount:I
    const/4 v1, 0x0

    #@24
    .line 3356
    .local v1, aleadysenddefault:Z
    const/4 v6, 0x0

    #@25
    .line 3357
    .local v6, duncount:I
    const/4 v2, 0x0

    #@26
    .line 3361
    .local v2, aleadysenddun:Z
    const/4 v11, 0x0

    #@27
    .line 3363
    .local v11, isOTAAttach:Z
    const/4 v13, 0x0

    #@28
    .line 3366
    .local v13, pdnId:I
    move-object/from16 v0, p0

    #@2a
    iget-object v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2c
    iget-object v15, v15, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2e
    invoke-interface {v15}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@31
    move-result-object v15

    #@32
    iget-boolean v15, v15, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_KR:Z

    #@34
    if-eqz v15, :cond_3e

    #@36
    .line 3368
    const-string v15, "[LGE_DATA_KR] Move to LGDataconnection!!!(Return)"

    #@38
    move-object/from16 v0, p0

    #@3a
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@3d
    goto :goto_21

    #@3e
    .line 3374
    :cond_3e
    const/4 v15, 0x6

    #@3f
    new-array v10, v15, [Z

    #@41
    fill-array-data v10, :array_23c

    #@44
    .line 3377
    .local v10, isDBEx:[Z
    const/4 v14, 0x0

    #@45
    .line 3380
    .local v14, tempOTAdp:Lcom/android/internal/telephony/DataProfile;
    move-object/from16 v0, p0

    #@47
    iget-object v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@49
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@4c
    move-result-object v9

    #@4d
    .local v9, i$:Ljava/util/Iterator;
    :cond_4d
    :goto_4d
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@50
    move-result v15

    #@51
    if-eqz v15, :cond_a9

    #@53
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@56
    move-result-object v5

    #@57
    check-cast v5, Lcom/android/internal/telephony/DataProfile;

    #@59
    .line 3382
    .local v5, dp:Lcom/android/internal/telephony/DataProfile;
    const-string v15, "default"

    #@5b
    invoke-virtual {v5, v15}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@5e
    move-result v15

    #@5f
    if-eqz v15, :cond_63

    #@61
    .line 3386
    add-int/lit8 v3, v3, 0x1

    #@63
    .line 3390
    :cond_63
    move-object/from16 v0, p0

    #@65
    iget-object v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@67
    iget-object v15, v15, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@69
    invoke-interface {v15}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@6c
    move-result-object v15

    #@6d
    iget-boolean v15, v15, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_CONTROL_PDN_ON_POA:Z

    #@6f
    if-eqz v15, :cond_76

    #@71
    .line 3392
    move-object/from16 v0, p0

    #@73
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->checkDefaultEx(I)V

    #@76
    .line 3399
    :cond_76
    const-string v15, "KDDIBASE"

    #@78
    move-object/from16 v0, p0

    #@7a
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7c
    move-object/from16 v16, v0

    #@7e
    move-object/from16 v0, v16

    #@80
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@82
    move-object/from16 v16, v0

    #@84
    invoke-interface/range {v16 .. v16}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@87
    move-result-object v16

    #@88
    move-object/from16 v0, v16

    #@8a
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@8c
    move-object/from16 v16, v0

    #@8e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@91
    move-result v15

    #@92
    if-eqz v15, :cond_4d

    #@94
    .line 3400
    invoke-virtual {v5}, Lcom/android/internal/telephony/DataProfile;->getDataProfileType()Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@97
    move-result-object v15

    #@98
    sget-object v16, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_APN:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@9a
    move-object/from16 v0, v16

    #@9c
    if-ne v15, v0, :cond_4d

    #@9e
    const-string v15, "dun"

    #@a0
    invoke-virtual {v5, v15}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@a3
    move-result v15

    #@a4
    if-eqz v15, :cond_4d

    #@a6
    .line 3403
    add-int/lit8 v6, v6, 0x1

    #@a8
    goto :goto_4d

    #@a9
    .line 3411
    .end local v5           #dp:Lcom/android/internal/telephony/DataProfile;
    :cond_a9
    const/4 v15, 0x1

    #@aa
    if-le v3, v15, :cond_bd

    #@ac
    .line 3413
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredApn()Lcom/android/internal/telephony/DataProfile;

    #@af
    move-result-object v4

    #@b0
    .line 3414
    .local v4, defaultdp:Lcom/android/internal/telephony/DataProfile;
    if-eqz v4, :cond_bd

    #@b2
    .line 3416
    const/4 v15, 0x0

    #@b3
    move-object/from16 v0, p0

    #@b5
    invoke-virtual {v0, v4, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->dprilmsg(Lcom/android/internal/telephony/DataProfile;I)I

    #@b8
    move-result v13

    #@b9
    .line 3417
    const/4 v15, 0x1

    #@ba
    aput-boolean v15, v10, v13

    #@bc
    .line 3418
    const/4 v1, 0x1

    #@bd
    .line 3423
    .end local v4           #defaultdp:Lcom/android/internal/telephony/DataProfile;
    :cond_bd
    const-string v15, "KDDIBASE"

    #@bf
    move-object/from16 v0, p0

    #@c1
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@c3
    move-object/from16 v16, v0

    #@c5
    move-object/from16 v0, v16

    #@c7
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@c9
    move-object/from16 v16, v0

    #@cb
    invoke-interface/range {v16 .. v16}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@ce
    move-result-object v16

    #@cf
    move-object/from16 v0, v16

    #@d1
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@d3
    move-object/from16 v16, v0

    #@d5
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d8
    move-result v15

    #@d9
    if-eqz v15, :cond_f6

    #@db
    .line 3424
    const/4 v15, 0x1

    #@dc
    if-le v6, v15, :cond_f6

    #@de
    .line 3426
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredDunApn()Lcom/android/internal/telephony/DataProfile;

    #@e1
    move-result-object v7

    #@e2
    .line 3427
    .local v7, dundp:Lcom/android/internal/telephony/DataProfile;
    if-eqz v7, :cond_f6

    #@e4
    .line 3429
    const-string v15, "[kjyean]sendPdnTable : aleadysenddun is true"

    #@e6
    move-object/from16 v0, p0

    #@e8
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@eb
    .line 3430
    const/4 v15, 0x0

    #@ec
    move-object/from16 v0, p0

    #@ee
    invoke-virtual {v0, v7, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->dprilmsg(Lcom/android/internal/telephony/DataProfile;I)I

    #@f1
    move-result v13

    #@f2
    .line 3431
    const/4 v15, 0x1

    #@f3
    aput-boolean v15, v10, v13

    #@f5
    .line 3432
    const/4 v2, 0x1

    #@f6
    .line 3438
    .end local v7           #dundp:Lcom/android/internal/telephony/DataProfile;
    :cond_f6
    move-object/from16 v0, p0

    #@f8
    iget-object v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@fa
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@fd
    move-result-object v9

    #@fe
    :cond_fe
    :goto_fe
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@101
    move-result v15

    #@102
    if-eqz v15, :cond_16d

    #@104
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@107
    move-result-object v5

    #@108
    check-cast v5, Lcom/android/internal/telephony/DataProfile;

    #@10a
    .line 3444
    .restart local v5       #dp:Lcom/android/internal/telephony/DataProfile;
    const-string v15, "default"

    #@10c
    invoke-virtual {v5, v15}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@10f
    move-result v15

    #@110
    if-eqz v15, :cond_120

    #@112
    .line 3446
    if-nez v1, :cond_fe

    #@114
    .line 3448
    const/4 v15, 0x0

    #@115
    move-object/from16 v0, p0

    #@117
    invoke-virtual {v0, v5, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->dprilmsg(Lcom/android/internal/telephony/DataProfile;I)I

    #@11a
    move-result v13

    #@11b
    .line 3449
    const/4 v15, 0x1

    #@11c
    aput-boolean v15, v10, v13

    #@11e
    .line 3450
    const/4 v1, 0x1

    #@11f
    goto :goto_fe

    #@120
    .line 3456
    :cond_120
    const-string v15, "KDDIBASE"

    #@122
    move-object/from16 v0, p0

    #@124
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@126
    move-object/from16 v16, v0

    #@128
    move-object/from16 v0, v16

    #@12a
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@12c
    move-object/from16 v16, v0

    #@12e
    invoke-interface/range {v16 .. v16}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@131
    move-result-object v16

    #@132
    move-object/from16 v0, v16

    #@134
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@136
    move-object/from16 v16, v0

    #@138
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13b
    move-result v15

    #@13c
    if-eqz v15, :cond_162

    #@13e
    .line 3457
    const-string v15, "dun"

    #@140
    invoke-virtual {v5, v15}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@143
    move-result v15

    #@144
    if-eqz v15, :cond_162

    #@146
    .line 3459
    const-string v15, "[kjyean]sendPdnTable : Phone.APN_TYPE_DUN"

    #@148
    move-object/from16 v0, p0

    #@14a
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@14d
    .line 3460
    if-nez v2, :cond_fe

    #@14f
    .line 3462
    const-string v15, "[kjyean]sendPdnTable : aleadysenddun is false"

    #@151
    move-object/from16 v0, p0

    #@153
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@156
    .line 3463
    const/4 v15, 0x0

    #@157
    move-object/from16 v0, p0

    #@159
    invoke-virtual {v0, v5, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->dprilmsg(Lcom/android/internal/telephony/DataProfile;I)I

    #@15c
    move-result v13

    #@15d
    .line 3464
    const/4 v15, 0x1

    #@15e
    aput-boolean v15, v10, v13

    #@160
    .line 3465
    const/4 v2, 0x1

    #@161
    goto :goto_fe

    #@162
    .line 3471
    :cond_162
    const/4 v15, 0x0

    #@163
    move-object/from16 v0, p0

    #@165
    invoke-virtual {v0, v5, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->dprilmsg(Lcom/android/internal/telephony/DataProfile;I)I

    #@168
    move-result v13

    #@169
    .line 3472
    const/4 v15, 0x1

    #@16a
    aput-boolean v15, v10, v13

    #@16c
    goto :goto_fe

    #@16d
    .line 3476
    .end local v5           #dp:Lcom/android/internal/telephony/DataProfile;
    :cond_16d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getpdnnumpereachoper()I

    #@170
    move-result v12

    #@171
    .line 3480
    .local v12, numofpdn:I
    const/4 v8, 0x1

    #@172
    .local v8, i:I
    :goto_172
    add-int/lit8 v15, v12, 0x1

    #@174
    if-ge v8, v15, :cond_183

    #@176
    .line 3482
    aget-boolean v15, v10, v8

    #@178
    if-nez v15, :cond_180

    #@17a
    .line 3483
    const/4 v15, 0x0

    #@17b
    move-object/from16 v0, p0

    #@17d
    invoke-virtual {v0, v15, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->dprilmsg(Lcom/android/internal/telephony/DataProfile;I)I

    #@180
    .line 3480
    :cond_180
    add-int/lit8 v8, v8, 0x1

    #@182
    goto :goto_172

    #@183
    .line 3491
    :cond_183
    move-object/from16 v0, p0

    #@185
    iget-object v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@187
    iget-object v15, v15, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@189
    invoke-interface {v15}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@18c
    move-result-object v15

    #@18d
    iget-boolean v15, v15, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_DATA_CALL_WHEN_ADMIN_PDN_DSIABLED_VZW:Z

    #@18f
    if-eqz v15, :cond_20d

    #@191
    .line 3493
    const/4 v15, 0x2

    #@192
    aget-boolean v15, v10, v15

    #@194
    if-nez v15, :cond_1e3

    #@196
    .line 3495
    const/4 v15, 0x1

    #@197
    move-object/from16 v0, p0

    #@199
    iput-boolean v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->isAPNDataBlock:Z

    #@19b
    .line 3496
    const/4 v15, 0x1

    #@19c
    move-object/from16 v0, p0

    #@19e
    iput-boolean v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->datablockbyadmin:Z

    #@1a0
    .line 3510
    :goto_1a0
    move-object/from16 v0, p0

    #@1a2
    iget-object v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1a4
    iget-object v15, v15, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1a6
    invoke-interface {v15}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1a9
    move-result-object v15

    #@1aa
    iget-boolean v15, v15, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APN2_ENABLE_BACKUP_RESTORE_VZW:Z

    #@1ac
    if-eqz v15, :cond_1c6

    #@1ae
    .line 3512
    move-object/from16 v0, p0

    #@1b0
    iget-boolean v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->datablockbyadmin:Z

    #@1b2
    const/16 v16, 0x1

    #@1b4
    move/from16 v0, v16

    #@1b6
    if-ne v15, v0, :cond_1fe

    #@1b8
    .line 3513
    const-string v15, "[APN Backup] datablockbyadmin == true so set apn2-disable to 1 "

    #@1ba
    move-object/from16 v0, p0

    #@1bc
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1bf
    .line 3514
    const-string v15, "ril.current.apn2-disable"

    #@1c1
    const-string v16, "1"

    #@1c3
    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1c6
    .line 3537
    :cond_1c6
    :goto_1c6
    move-object/from16 v0, p0

    #@1c8
    iget-object v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1ca
    iget-object v15, v15, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1cc
    invoke-interface {v15}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1cf
    move-result-object v15

    #@1d0
    iget v15, v15, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@1d2
    if-eqz v15, :cond_21

    #@1d4
    .line 3539
    move-object/from16 v0, p0

    #@1d6
    iget-object v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1d8
    iget-object v15, v15, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1da
    const/16 v16, 0x0

    #@1dc
    move-object/from16 v0, v16

    #@1de
    invoke-interface {v15, v0, v11}, Lcom/android/internal/telephony/CommandsInterface;->sendMPDNTable(Landroid/os/Message;Z)V

    #@1e1
    goto/16 :goto_21

    #@1e3
    .line 3498
    :cond_1e3
    const/4 v15, 0x1

    #@1e4
    aget-boolean v15, v10, v15

    #@1e6
    if-nez v15, :cond_1f3

    #@1e8
    .line 3500
    const/4 v15, 0x1

    #@1e9
    move-object/from16 v0, p0

    #@1eb
    iput-boolean v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->isAPNDataBlock:Z

    #@1ed
    .line 3501
    const/4 v15, 0x0

    #@1ee
    move-object/from16 v0, p0

    #@1f0
    iput-boolean v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->datablockbyadmin:Z

    #@1f2
    goto :goto_1a0

    #@1f3
    .line 3505
    :cond_1f3
    const/4 v15, 0x0

    #@1f4
    move-object/from16 v0, p0

    #@1f6
    iput-boolean v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->isAPNDataBlock:Z

    #@1f8
    .line 3506
    const/4 v15, 0x0

    #@1f9
    move-object/from16 v0, p0

    #@1fb
    iput-boolean v15, v0, Lcom/android/internal/telephony/DataConnectionTracker;->datablockbyadmin:Z

    #@1fd
    goto :goto_1a0

    #@1fe
    .line 3516
    :cond_1fe
    const-string v15, "[APN Backup] datablockbyadmin == false so set apn2-disable to 0 "

    #@200
    move-object/from16 v0, p0

    #@202
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@205
    .line 3517
    const-string v15, "ril.current.apn2-disable"

    #@207
    const-string v16, "0"

    #@209
    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@20c
    goto :goto_1c6

    #@20d
    .line 3525
    :cond_20d
    const-string v15, "SPCSBASE"

    #@20f
    move-object/from16 v0, p0

    #@211
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@213
    move-object/from16 v16, v0

    #@215
    move-object/from16 v0, v16

    #@217
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@219
    move-object/from16 v16, v0

    #@21b
    invoke-interface/range {v16 .. v16}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@21e
    move-result-object v16

    #@21f
    move-object/from16 v0, v16

    #@221
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@223
    move-object/from16 v16, v0

    #@225
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@228
    move-result v15

    #@229
    if-eqz v15, :cond_1c6

    #@22b
    .line 3528
    const/4 v15, 0x1

    #@22c
    aget-boolean v15, v10, v15

    #@22e
    if-nez v15, :cond_1c6

    #@230
    const/4 v15, 0x2

    #@231
    aget-boolean v15, v10, v15

    #@233
    const/16 v16, 0x1

    #@235
    move/from16 v0, v16

    #@237
    if-ne v15, v0, :cond_1c6

    #@239
    .line 3530
    const/4 v11, 0x1

    #@23a
    goto :goto_1c6

    #@23b
    .line 3374
    nop

    #@23c
    :array_23c
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public send_ehrpd_ipv6_setting(I)V
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 3545
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "send_ehrpd_ipv6_setting GO GO~~!!!= "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@16
    .line 3546
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@18
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1a
    const/4 v1, 0x0

    #@1b
    invoke-interface {v0, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->setEhrpdIpv6ControlSetting(ILandroid/os/Message;)V

    #@1e
    .line 3547
    return-void
.end method

.method public setDataConnection(Z)V
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 4127
    const/4 v0, 0x0

    #@1
    .line 4129
    .local v0, msg:Landroid/os/Message;
    if-eqz p1, :cond_12

    #@3
    .line 4130
    const v1, 0x42003

    #@6
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    .line 4131
    const-string v1, "dataEnabled"

    #@c
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e
    .line 4139
    :goto_e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 4140
    return-void

    #@12
    .line 4133
    :cond_12
    const v1, 0x42018

    #@15
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@18
    move-result-object v0

    #@19
    .line 4134
    const/4 v1, 0x1

    #@1a
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@1c
    .line 4136
    const-string v1, "connectionManagerHandleData"

    #@1e
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20
    goto :goto_e
.end method

.method public setDataOnRoamingEnabled(Z)V
    .registers 12
    .parameter "enabled"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 1271
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@5
    move-result v7

    #@6
    if-eq v7, p1, :cond_77

    #@8
    .line 1273
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@c
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@f
    move-result-object v7

    #@10
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_SKT:Z

    #@12
    if-eqz v7, :cond_66

    #@14
    if-ne p1, v5, :cond_66

    #@16
    .line 1275
    const-string v7, "ril.gsm.reject_cause"

    #@18
    invoke-static {v7, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@1b
    move-result v1

    #@1c
    .line 1277
    .local v1, data_rejCode:I
    new-instance v7, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v8, "[LGE_DATA] setDataOnRoamingEnabled(), reject_cause= "

    #@23
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v7

    #@27
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v7

    #@2f
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@32
    .line 1279
    if-lez v1, :cond_66

    #@34
    .line 1281
    const/4 v3, 0x0

    #@35
    .line 1282
    .local v3, msg:Ljava/lang/String;
    const/4 v0, 0x0

    #@36
    .line 1284
    .local v0, IsRoaming:Z
    const-string v7, "DataConnectionTracker"

    #@38
    new-instance v8, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v9, "handleNetworkRejection : Rejection code :"

    #@3f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v8

    #@43
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v8

    #@4b
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 1286
    new-instance v2, Landroid/content/Intent;

    #@50
    const-string v7, "lge.intent.action.toast"

    #@52
    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@55
    .line 1288
    .local v2, intent:Landroid/content/Intent;
    sparse-switch v1, :sswitch_data_88

    #@58
    .line 1306
    :goto_58
    const-string v7, "text"

    #@5a
    invoke-virtual {v2, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@5d
    .line 1307
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5f
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@62
    move-result-object v7

    #@63
    invoke-virtual {v7, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@66
    .line 1311
    .end local v0           #IsRoaming:Z
    .end local v1           #data_rejCode:I
    .end local v2           #intent:Landroid/content/Intent;
    .end local v3           #msg:Ljava/lang/String;
    :cond_66
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@68
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@6b
    move-result-object v7

    #@6c
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6f
    move-result-object v4

    #@70
    .line 1312
    .local v4, resolver:Landroid/content/ContentResolver;
    const-string v7, "data_roaming"

    #@72
    if-eqz p1, :cond_86

    #@74
    :goto_74
    invoke-static {v4, v7, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@77
    .line 1315
    .end local v4           #resolver:Landroid/content/ContentResolver;
    :cond_77
    return-void

    #@78
    .line 1292
    .restart local v0       #IsRoaming:Z
    .restart local v1       #data_rejCode:I
    .restart local v2       #intent:Landroid/content/Intent;
    .restart local v3       #msg:Ljava/lang/String;
    :sswitch_78
    const-string v7, "SKT_NRC_07_GPRS_NOT_ALLOWED"

    #@7a
    invoke-static {v7}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@7d
    move-result-object v3

    #@7e
    .line 1294
    goto :goto_58

    #@7f
    .line 1298
    :sswitch_7f
    const-string v7, "SKT_NRC_14_GPRS_NOT_ALLOWED_IN_THIS_PLMN"

    #@81
    invoke-static {v7}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@84
    move-result-object v3

    #@85
    .line 1300
    goto :goto_58

    #@86
    .end local v0           #IsRoaming:Z
    .end local v1           #data_rejCode:I
    .end local v2           #intent:Landroid/content/Intent;
    .end local v3           #msg:Ljava/lang/String;
    .restart local v4       #resolver:Landroid/content/ContentResolver;
    :cond_86
    move v5, v6

    #@87
    .line 1312
    goto :goto_74

    #@88
    .line 1288
    :sswitch_data_88
    .sparse-switch
        0x7 -> :sswitch_78
        0xe -> :sswitch_7f
    .end sparse-switch
.end method

.method protected setEnabled(IZ)V
    .registers 6
    .parameter "id"
    .parameter "enable"

    #@0
    .prologue
    .line 1917
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "setEnabled("

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ", "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, ") with old state = "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataEnabled:[Z

    #@21
    aget-boolean v2, v2, p1

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, " and enabledCount = "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    iget v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->enabledCount:I

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@3a
    .line 1920
    const v1, 0x4200d

    #@3d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@40
    move-result-object v0

    #@41
    .line 1921
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@43
    .line 1922
    if-eqz p2, :cond_4c

    #@45
    const/4 v1, 0x1

    #@46
    :goto_46
    iput v1, v0, Landroid/os/Message;->arg2:I

    #@48
    .line 1923
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@4b
    .line 1924
    return-void

    #@4c
    .line 1922
    :cond_4c
    const/4 v1, 0x0

    #@4d
    goto :goto_46
.end method

.method protected abstract setIMSRegistate(Z)V
.end method

.method public setInternalDataEnabled(Z)Z
    .registers 6
    .parameter "enable"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2015
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "setInternalDataEnabled("

    #@8
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    const-string v3, ")"

    #@12
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1d
    .line 2018
    if-nez p1, :cond_36

    #@1f
    const-string v1, "VZWBASE"

    #@21
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@23
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@25
    invoke-interface {v3}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@28
    move-result-object v3

    #@29
    iget-object v3, v3, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@2b
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_36

    #@31
    .line 2020
    const/4 v1, 0x0

    #@32
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->onCleanUpAllConnections(Ljava/lang/String;)V

    #@35
    .line 2030
    :goto_35
    return v2

    #@36
    .line 2025
    :cond_36
    const v1, 0x4201b

    #@39
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@3c
    move-result-object v0

    #@3d
    .line 2026
    .local v0, msg:Landroid/os/Message;
    if-eqz p1, :cond_46

    #@3f
    move v1, v2

    #@40
    :goto_40
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@42
    .line 2027
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@45
    goto :goto_35

    #@46
    .line 2026
    :cond_46
    const/4 v1, 0x0

    #@47
    goto :goto_40
.end method

.method protected setNetworkMtu()Z
    .registers 12

    #@0
    .prologue
    const/16 v10, 0x5dc

    #@2
    .line 4178
    const/16 v0, 0x5dc

    #@4
    .line 4179
    .local v0, LG_DATA_MTU_MAX:I
    const/4 v7, 0x0

    #@5
    .line 4180
    .local v7, suitableMtu:Ljava/lang/String;
    const/4 v3, 0x0

    #@6
    .line 4181
    .local v3, homeMTU:Ljava/lang/String;
    const/4 v6, 0x0

    #@7
    .line 4183
    .local v6, roamingMTU:Ljava/lang/String;
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getNumeric()Ljava/lang/String;

    #@a
    move-result-object v4

    #@b
    .line 4184
    .local v4, numeric:Ljava/lang/String;
    invoke-static {v4}, Landroid/provider/Telephony$Carriers;->getAutoProfileKey(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 4186
    .local v1, autoprofileKey:Ljava/lang/String;
    :try_start_f
    const-string v8, "ipmtu"

    #@11
    invoke-virtual {p0, v4, v1, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->loadKeyFromDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    .line 4187
    if-eqz v3, :cond_d0

    #@17
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@1a
    move-result v8

    #@1b
    if-lez v8, :cond_d0

    #@1d
    const-string v8, "0"

    #@1f
    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v8

    #@23
    if-nez v8, :cond_d0

    #@25
    .line 4188
    move-object v7, v3

    #@26
    .line 4193
    :goto_26
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@28
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@2b
    move-result-object v8

    #@2c
    if-eqz v8, :cond_f6

    #@2e
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@30
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@33
    move-result-object v8

    #@34
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@37
    move-result v8

    #@38
    if-eqz v8, :cond_f6

    #@3a
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3c
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@3f
    move-result-object v8

    #@40
    if-eqz v8, :cond_f6

    #@42
    .line 4196
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@44
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@47
    move-result-object v8

    #@48
    iget-object v8, v8, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@4a
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    .line 4197
    .local v5, operatorNumeric:Ljava/lang/String;
    const-string v8, ""

    #@50
    const-string v9, "ipmtu"

    #@52
    invoke-virtual {p0, v5, v8, v9}, Lcom/android/internal/telephony/DataConnectionTracker;->loadKeyFromDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@55
    move-result-object v6

    #@56
    .line 4199
    if-eqz v6, :cond_ef

    #@58
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@5b
    move-result v8

    #@5c
    if-lez v8, :cond_ef

    #@5e
    const-string v8, "0"

    #@60
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v8

    #@64
    if-nez v8, :cond_ef

    #@66
    .line 4200
    if-eqz v7, :cond_72

    #@68
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@6b
    move-result v8

    #@6c
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_6f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_6f} :catch_d7

    #@6f
    move-result v9

    #@70
    if-le v8, v9, :cond_73

    #@72
    .line 4201
    :cond_72
    move-object v7, v6

    #@73
    .line 4212
    .end local v5           #operatorNumeric:Ljava/lang/String;
    :cond_73
    :goto_73
    new-instance v8, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v9, "[setNetworkMtu] Home MTU : "

    #@7a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v8

    #@7e
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v8

    #@82
    const-string v9, ",  Roaming MTU : "

    #@84
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v8

    #@88
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v8

    #@8c
    const-string v9, ", Suitable Mtu : "

    #@8e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v8

    #@92
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v8

    #@96
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v8

    #@9a
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@9d
    .line 4215
    if-eqz v7, :cond_fd

    #@9f
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@a2
    move-result v8

    #@a3
    if-gt v8, v10, :cond_fd

    #@a5
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@a8
    move-result v8

    #@a9
    add-int/lit8 v8, v8, -0x28

    #@ab
    if-lez v8, :cond_fd

    #@ad
    .line 4216
    const-string v8, "persist.data_netmgrd_mtu"

    #@af
    invoke-static {v8, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@b2
    .line 4217
    new-instance v8, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v9, "[setNetworkMtu] Update MTU is ("

    #@b9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v8

    #@bd
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v8

    #@c1
    const-string v9, ")"

    #@c3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v8

    #@c7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v8

    #@cb
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V

    #@ce
    .line 4218
    const/4 v8, 0x1

    #@cf
    .line 4222
    :goto_cf
    return v8

    #@d0
    .line 4190
    :cond_d0
    :try_start_d0
    const-string v8, "[setNetworkMtu] Fail to load ipmtu setting for home NW from Db"

    #@d2
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V
    :try_end_d5
    .catch Ljava/lang/Exception; {:try_start_d0 .. :try_end_d5} :catch_d7

    #@d5
    goto/16 :goto_26

    #@d7
    .line 4209
    :catch_d7
    move-exception v2

    #@d8
    .line 4210
    .local v2, e:Ljava/lang/Exception;
    new-instance v8, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v9, "[setNetworkMtu] Exception : "

    #@df
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v8

    #@e3
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v8

    #@e7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ea
    move-result-object v8

    #@eb
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V

    #@ee
    goto :goto_73

    #@ef
    .line 4204
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v5       #operatorNumeric:Ljava/lang/String;
    :cond_ef
    :try_start_ef
    const-string v8, "[setNetworkMtu] Fail to load ipmtu setting for roaming NW from Db"

    #@f1
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V

    #@f4
    goto/16 :goto_73

    #@f6
    .line 4207
    .end local v5           #operatorNumeric:Ljava/lang/String;
    :cond_f6
    const-string v8, "[setNetworkMtu] Not on Roaming NW"

    #@f8
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V
    :try_end_fb
    .catch Ljava/lang/Exception; {:try_start_ef .. :try_end_fb} :catch_d7

    #@fb
    goto/16 :goto_73

    #@fd
    .line 4220
    :cond_fd
    const-string v8, "persist.data_netmgrd_mtu"

    #@ff
    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@102
    move-result-object v9

    #@103
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@106
    .line 4221
    const-string v8, "[setNetworkMtu] Fail to set suitable mtu size, use default mtu"

    #@108
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V

    #@10b
    .line 4222
    const/4 v8, 0x0

    #@10c
    goto :goto_cf
.end method

.method public setPreferredApn(I)V
    .registers 2
    .parameter "pos"

    #@0
    .prologue
    .line 2952
    return-void
.end method

.method protected setPreferredNetworkMode(I)V
    .registers 4
    .parameter "nwMode"

    #@0
    .prologue
    .line 3016
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    const-string v1, "preferred_network_mode"

    #@c
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@f
    .line 3018
    return-void
.end method

.method protected abstract setState(Lcom/android/internal/telephony/DctConstants$State;)V
.end method

.method protected setWindowBuferSize()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x6

    #@1
    .line 4226
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getNumeric()Ljava/lang/String;

    #@4
    move-result-object v6

    #@5
    .line 4227
    .local v6, numeric:Ljava/lang/String;
    invoke-static {v6}, Landroid/provider/Telephony$Carriers;->getAutoProfileKey(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    .line 4229
    .local v2, autoprofileKey:Ljava/lang/String;
    const/16 v9, 0xd

    #@b
    new-array v0, v9, [Ljava/lang/String;

    #@d
    const/4 v9, 0x0

    #@e
    const-string v10, "windefault"

    #@10
    aput-object v10, v0, v9

    #@12
    const/4 v9, 0x1

    #@13
    const-string v10, "lte"

    #@15
    aput-object v10, v0, v9

    #@17
    const/4 v9, 0x2

    #@18
    const-string v10, "umts"

    #@1a
    aput-object v10, v0, v9

    #@1c
    const/4 v9, 0x3

    #@1d
    const-string v10, "hspa"

    #@1f
    aput-object v10, v0, v9

    #@21
    const/4 v9, 0x4

    #@22
    const-string v10, "hsupa"

    #@24
    aput-object v10, v0, v9

    #@26
    const/4 v9, 0x5

    #@27
    const-string v10, "hsdpa"

    #@29
    aput-object v10, v0, v9

    #@2b
    const-string v9, "hspap"

    #@2d
    aput-object v9, v0, v11

    #@2f
    const/4 v9, 0x7

    #@30
    const-string v10, "edge"

    #@32
    aput-object v10, v0, v9

    #@34
    const/16 v9, 0x8

    #@36
    const-string v10, "gprs"

    #@38
    aput-object v10, v0, v9

    #@3a
    const/16 v9, 0x9

    #@3c
    const-string v10, "evdo_b"

    #@3e
    aput-object v10, v0, v9

    #@40
    const/16 v9, 0xa

    #@42
    const-string v10, "ehrpd"

    #@44
    aput-object v10, v0, v9

    #@46
    const/16 v9, 0xb

    #@48
    const-string v10, "evdo"

    #@4a
    aput-object v10, v0, v9

    #@4c
    const/16 v9, 0xc

    #@4e
    const-string v10, "gpass"

    #@50
    aput-object v10, v0, v9

    #@52
    .line 4232
    .local v0, allRats:[Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/DataConnectionTracker;->isContainingNumericInDB(Ljava/lang/String;)Z

    #@55
    move-result v9

    #@56
    if-nez v9, :cond_61

    #@58
    .line 4233
    const-string v6, "00101"

    #@5a
    .line 4234
    const-string v2, ""

    #@5c
    .line 4235
    const-string v9, "[setWindowBuferSize] use default values "

    #@5e
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V

    #@61
    .line 4237
    :cond_61
    move-object v1, v0

    #@62
    .local v1, arr$:[Ljava/lang/String;
    array-length v5, v1

    #@63
    .local v5, len$:I
    const/4 v4, 0x0

    #@64
    .local v4, i$:I
    :goto_64
    if-ge v4, v5, :cond_fd

    #@66
    aget-object v7, v1, v4

    #@68
    .line 4239
    .local v7, s:Ljava/lang/String;
    :try_start_68
    invoke-virtual {p0, v6, v2, v7}, Lcom/android/internal/telephony/DataConnectionTracker;->loadKeyFromDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6b
    move-result-object v8

    #@6c
    .line 4240
    .local v8, val:Ljava/lang/String;
    if-eqz v8, :cond_be

    #@6e
    const-string v9, "0"

    #@70
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@73
    move-result v9

    #@74
    if-nez v9, :cond_be

    #@76
    const-string v9, ","

    #@78
    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@7b
    move-result-object v9

    #@7c
    array-length v9, v9

    #@7d
    if-ne v9, v11, :cond_be

    #@7f
    .line 4243
    new-instance v9, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v10, "net.tcp.buffersize."

    #@86
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v9

    #@8a
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v9

    #@8e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v9

    #@92
    invoke-static {v9, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@95
    .line 4244
    new-instance v9, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v10, "[setWindowBuferSize] Update "

    #@9c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v9

    #@a0
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v9

    #@a4
    const-string v10, " is ("

    #@a6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v9

    #@aa
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v9

    #@ae
    const-string v10, ")"

    #@b0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v9

    #@b4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v9

    #@b8
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@bb
    .line 4237
    .end local v8           #val:Ljava/lang/String;
    :goto_bb
    add-int/lit8 v4, v4, 0x1

    #@bd
    goto :goto_64

    #@be
    .line 4247
    .restart local v8       #val:Ljava/lang/String;
    :cond_be
    new-instance v9, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v10, "[setWindowBuferSize] Fail to set "

    #@c5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v9

    #@c9
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v9

    #@cd
    const-string v10, " as ("

    #@cf
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v9

    #@d3
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v9

    #@d7
    const-string v10, ")"

    #@d9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v9

    #@dd
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v9

    #@e1
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V
    :try_end_e4
    .catch Ljava/lang/Exception; {:try_start_68 .. :try_end_e4} :catch_e5

    #@e4
    goto :goto_bb

    #@e5
    .line 4249
    .end local v8           #val:Ljava/lang/String;
    :catch_e5
    move-exception v3

    #@e6
    .line 4250
    .local v3, e:Ljava/lang/Exception;
    new-instance v9, Ljava/lang/StringBuilder;

    #@e8
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@eb
    const-string v10, "[setWindowBuferSize] Exception : "

    #@ed
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v9

    #@f1
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v9

    #@f5
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f8
    move-result-object v9

    #@f9
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/DataConnectionTracker;->loge(Ljava/lang/String;)V

    #@fc
    goto :goto_bb

    #@fd
    .line 4253
    .end local v3           #e:Ljava/lang/Exception;
    .end local v7           #s:Ljava/lang/String;
    :cond_fd
    return-void
.end method

.method protected startAlarmForReconnect(ILcom/android/internal/telephony/ApnContext;)V
    .registers 3
    .parameter "delay"
    .parameter "apnContext"

    #@0
    .prologue
    .line 2959
    return-void
.end method

.method protected startDataStallAlarm(Z)V
    .registers 11
    .parameter "suspectedStall"

    #@0
    .prologue
    .line 2673
    iget-boolean v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->SUPPORT_LG_DATA_RECOVERY:Z

    #@2
    if-nez v4, :cond_34

    #@4
    .line 2675
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getRecoveryAction()I

    #@7
    move-result v3

    #@8
    .line 2680
    .local v3, nextAction:I
    iget-boolean v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsScreenOn:Z

    #@a
    if-nez v4, :cond_14

    #@c
    if-nez p1, :cond_14

    #@e
    invoke-static {v3}, Lcom/android/internal/telephony/DataConnectionTracker$RecoveryAction;->access$100(I)Z

    #@11
    move-result v4

    #@12
    if-eqz v4, :cond_35

    #@14
    .line 2681
    :cond_14
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mResolver:Landroid/content/ContentResolver;

    #@16
    const-string v5, "data_stall_alarm_aggressive_delay_in_ms"

    #@18
    const v6, 0xea60

    #@1b
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1e
    move-result v1

    #@1f
    .line 2691
    .local v1, delayInMs:I
    :goto_1f
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@21
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@23
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@26
    move-result-object v4

    #@27
    iget-boolean v4, v4, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

    #@29
    if-eqz v4, :cond_41

    #@2b
    iget-boolean v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@2d
    if-eqz v4, :cond_41

    #@2f
    .line 2693
    const-string v4, "startDataStallAlarm: ignore during Wifi "

    #@31
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@34
    .line 2724
    .end local v1           #delayInMs:I
    .end local v3           #nextAction:I
    :cond_34
    :goto_34
    return-void

    #@35
    .line 2685
    .restart local v3       #nextAction:I
    :cond_35
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mResolver:Landroid/content/ContentResolver;

    #@37
    const-string v5, "data_stall_alarm_non_aggressive_delay_in_ms"

    #@39
    const v6, 0x57e40

    #@3c
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3f
    move-result v1

    #@40
    .restart local v1       #delayInMs:I
    goto :goto_1f

    #@41
    .line 2698
    :cond_41
    iget v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmTag:I

    #@43
    add-int/lit8 v4, v4, 0x1

    #@45
    iput v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmTag:I

    #@47
    .line 2700
    new-instance v4, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v5, "startDataStallAlarm: tag="

    #@4e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    iget v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmTag:I

    #@54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    const-string v5, " delay="

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    div-int/lit16 v5, v1, 0x3e8

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    const-string v5, "s"

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@71
    .line 2703
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@73
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@76
    move-result-object v4

    #@77
    const-string v5, "alarm"

    #@79
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7c
    move-result-object v0

    #@7d
    check-cast v0, Landroid/app/AlarmManager;

    #@7f
    .line 2706
    .local v0, am:Landroid/app/AlarmManager;
    new-instance v2, Landroid/content/Intent;

    #@81
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getActionIntentDataStallAlarm()Ljava/lang/String;

    #@84
    move-result-object v4

    #@85
    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@88
    .line 2707
    .local v2, intent:Landroid/content/Intent;
    const-string v4, "data.stall.alram.tag"

    #@8a
    iget v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmTag:I

    #@8c
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@8f
    .line 2709
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@91
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@94
    move-result-object v4

    #@95
    const/4 v5, 0x0

    #@96
    const/high16 v6, 0x800

    #@98
    invoke-static {v4, v5, v2, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@9b
    move-result-object v4

    #@9c
    iput-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@9e
    .line 2717
    const/4 v4, 0x3

    #@9f
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@a2
    move-result-wide v5

    #@a3
    int-to-long v7, v1

    #@a4
    add-long/2addr v5, v7

    #@a5
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@a7
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@aa
    goto :goto_34
.end method

.method protected startNetStatPoll()V
    .registers 3

    #@0
    .prologue
    .line 2428
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->getOverallState()Lcom/android/internal/telephony/DctConstants$State;

    #@3
    move-result-object v0

    #@4
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@6
    if-ne v0, v1, :cond_1c

    #@8
    iget-boolean v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollEnabled:Z

    #@a
    if-nez v0, :cond_1c

    #@c
    .line 2429
    const-string v0, "startNetStatPoll"

    #@e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@11
    .line 2430
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->resetPollStats()V

    #@14
    .line 2431
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollEnabled:Z

    #@17
    .line 2432
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPollNetStat:Ljava/lang/Runnable;

    #@19
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@1c
    .line 2434
    :cond_1c
    return-void
.end method

.method protected stopDataStallAlarm()V
    .registers 5

    #@0
    .prologue
    const v3, 0x42011

    #@3
    .line 2728
    iget-boolean v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->SUPPORT_LG_DATA_RECOVERY:Z

    #@5
    if-nez v1, :cond_54

    #@7
    .line 2730
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@9
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@c
    move-result-object v1

    #@d
    const-string v2, "alarm"

    #@f
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/app/AlarmManager;

    #@15
    .line 2734
    .local v0, am:Landroid/app/AlarmManager;
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "stopDataStallAlarm: current tag="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    iget v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmTag:I

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, " mDataStallAlarmIntent="

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@39
    .line 2737
    iget v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmTag:I

    #@3b
    add-int/lit8 v1, v1, 0x1

    #@3d
    iput v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmTag:I

    #@3f
    .line 2738
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@41
    if-eqz v1, :cond_4b

    #@43
    .line 2739
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@45
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@48
    .line 2740
    const/4 v1, 0x0

    #@49
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataStallAlarmIntent:Landroid/app/PendingIntent;

    #@4b
    .line 2743
    :cond_4b
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->hasMessages(I)Z

    #@4e
    move-result v1

    #@4f
    if-eqz v1, :cond_54

    #@51
    .line 2744
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->removeMessages(I)V

    #@54
    .line 2751
    .end local v0           #am:Landroid/app/AlarmManager;
    :cond_54
    return-void
.end method

.method protected stopNetStatPoll()V
    .registers 2

    #@0
    .prologue
    .line 2437
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollEnabled:Z

    #@3
    .line 2438
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPollNetStat:Ljava/lang/Runnable;

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->removeCallbacks(Ljava/lang/Runnable;)V

    #@8
    .line 2439
    const-string v0, "stopNetStatPoll"

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@d
    .line 2440
    return-void
.end method

.method public switchingFixedtype(II)I
    .registers 8
    .parameter "id"
    .parameter "MPDNset"

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    const/4 v1, 0x2

    #@2
    const/4 v4, -0x1

    #@3
    const/4 v0, 0x1

    #@4
    const/4 v3, 0x5

    #@5
    .line 3758
    packed-switch p2, :pswitch_data_19a

    #@8
    .line 3914
    :pswitch_8
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@c
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@f
    move-result-object v1

    #@10
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_KR:Z

    #@12
    if-eqz v1, :cond_195

    #@14
    .line 3915
    if-eqz p1, :cond_18

    #@16
    if-ne p1, v3, :cond_197

    #@18
    .line 3923
    :cond_18
    :goto_18
    return v0

    #@19
    .line 3762
    :pswitch_19
    if-eq p1, v3, :cond_18

    #@1b
    .line 3765
    const/16 v0, 0x8

    #@1d
    if-ne p1, v0, :cond_21

    #@1f
    move v0, v1

    #@20
    .line 3766
    goto :goto_18

    #@21
    .line 3767
    :cond_21
    if-nez p1, :cond_25

    #@23
    move v0, v2

    #@24
    .line 3768
    goto :goto_18

    #@25
    .line 3769
    :cond_25
    const/16 v0, 0x9

    #@27
    if-ne p1, v0, :cond_2b

    #@29
    .line 3770
    const/4 v0, 0x4

    #@2a
    goto :goto_18

    #@2b
    .line 3771
    :cond_2b
    const/16 v0, 0xb

    #@2d
    if-ne p1, v0, :cond_31

    #@2f
    move v0, v3

    #@30
    .line 3772
    goto :goto_18

    #@31
    .line 3774
    :cond_31
    new-instance v0, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v1, "wrong type for vzwapp : "

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v0

    #@44
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@47
    move v0, v4

    #@48
    .line 3776
    goto :goto_18

    #@49
    .line 3781
    :pswitch_49
    if-eqz p1, :cond_18

    #@4b
    .line 3783
    const/16 v0, 0x8

    #@4d
    if-ne p1, v0, :cond_51

    #@4f
    move v0, v1

    #@50
    .line 3784
    goto :goto_18

    #@51
    .line 3785
    :cond_51
    if-ne p1, v3, :cond_55

    #@53
    move v0, v2

    #@54
    .line 3786
    goto :goto_18

    #@55
    .line 3787
    :cond_55
    if-ne p1, v2, :cond_59

    #@57
    .line 3788
    const/4 v0, 0x4

    #@58
    goto :goto_18

    #@59
    .line 3790
    :cond_59
    new-instance v0, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v1, "wrong type for KDDI : "

    #@60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v0

    #@64
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@67
    move-result-object v0

    #@68
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v0

    #@6c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@6f
    move v0, v4

    #@70
    .line 3792
    goto :goto_18

    #@71
    .line 3798
    :pswitch_71
    const/4 v3, 0x6

    #@72
    if-ne p1, v3, :cond_76

    #@74
    move v0, v1

    #@75
    .line 3799
    goto :goto_18

    #@76
    .line 3800
    :cond_76
    if-eqz p1, :cond_18

    #@78
    .line 3802
    if-ne p1, v2, :cond_7c

    #@7a
    move v0, v2

    #@7b
    .line 3803
    goto :goto_18

    #@7c
    .line 3805
    :cond_7c
    if-ne p1, v1, :cond_80

    #@7e
    .line 3806
    const/4 v0, 0x4

    #@7f
    goto :goto_18

    #@80
    .line 3809
    :cond_80
    new-instance v0, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v1, "wrong type for sprint : "

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v0

    #@8f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v0

    #@93
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@96
    move v0, v4

    #@97
    .line 3811
    goto :goto_18

    #@98
    .line 3818
    :pswitch_98
    if-eqz p1, :cond_18

    #@9a
    .line 3820
    if-ne p1, v3, :cond_9f

    #@9c
    move v0, v1

    #@9d
    .line 3821
    goto/16 :goto_18

    #@9f
    .line 3822
    :cond_9f
    const/16 v0, 0xa

    #@a1
    if-ne p1, v0, :cond_a6

    #@a3
    move v0, v2

    #@a4
    .line 3823
    goto/16 :goto_18

    #@a6
    .line 3824
    :cond_a6
    const/16 v0, 0x10

    #@a8
    if-ne p1, v0, :cond_ad

    #@aa
    .line 3825
    const/4 v0, 0x4

    #@ab
    goto/16 :goto_18

    #@ad
    .line 3827
    :cond_ad
    new-instance v0, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v1, "wrong type for ATANT : "

    #@b4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v0

    #@b8
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v0

    #@bc
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v0

    #@c0
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@c3
    move v0, v4

    #@c4
    .line 3829
    goto/16 :goto_18

    #@c6
    .line 3834
    :pswitch_c6
    if-eqz p1, :cond_18

    #@c8
    .line 3836
    if-ne p1, v3, :cond_cd

    #@ca
    move v0, v1

    #@cb
    .line 3837
    goto/16 :goto_18

    #@cd
    .line 3838
    :cond_cd
    if-ne p1, v2, :cond_d2

    #@cf
    move v0, v2

    #@d0
    .line 3839
    goto/16 :goto_18

    #@d2
    .line 3841
    :cond_d2
    new-instance v0, Ljava/lang/StringBuilder;

    #@d4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d7
    const-string v1, "wrong type for TMUS : "

    #@d9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v0

    #@dd
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v0

    #@e1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e4
    move-result-object v0

    #@e5
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@e8
    move v0, v4

    #@e9
    .line 3842
    goto/16 :goto_18

    #@eb
    .line 3847
    :pswitch_eb
    if-eq p1, v3, :cond_18

    #@ed
    .line 3849
    if-nez p1, :cond_f2

    #@ef
    move v0, v1

    #@f0
    .line 3850
    goto/16 :goto_18

    #@f2
    .line 3851
    :cond_f2
    const/16 v0, 0xc

    #@f4
    if-ne p1, v0, :cond_f9

    #@f6
    move v0, v2

    #@f7
    .line 3852
    goto/16 :goto_18

    #@f9
    .line 3854
    :cond_f9
    const/16 v0, 0x8

    #@fb
    if-ne p1, v0, :cond_100

    #@fd
    .line 3855
    const/4 v0, 0x4

    #@fe
    goto/16 :goto_18

    #@100
    .line 3856
    :cond_100
    const/16 v0, 0x10

    #@102
    if-ne p1, v0, :cond_107

    #@104
    move v0, v3

    #@105
    .line 3857
    goto/16 :goto_18

    #@107
    .line 3860
    :cond_107
    new-instance v0, Ljava/lang/StringBuilder;

    #@109
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10c
    const-string v1, "wrong type for LGU+ : "

    #@10e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v0

    #@112
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@115
    move-result-object v0

    #@116
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@119
    move-result-object v0

    #@11a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@11d
    move v0, v4

    #@11e
    .line 3862
    goto/16 :goto_18

    #@120
    .line 3866
    :pswitch_120
    if-eq p1, v3, :cond_18

    #@122
    .line 3868
    if-nez p1, :cond_127

    #@124
    move v0, v1

    #@125
    .line 3869
    goto/16 :goto_18

    #@127
    .line 3871
    :cond_127
    new-instance v0, Ljava/lang/StringBuilder;

    #@129
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12c
    const-string v1, "wrong type for KT : "

    #@12e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v0

    #@132
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@135
    move-result-object v0

    #@136
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v0

    #@13a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@13d
    move v0, v4

    #@13e
    .line 3873
    goto/16 :goto_18

    #@140
    .line 3877
    :pswitch_140
    if-eq p1, v3, :cond_18

    #@142
    .line 3879
    if-nez p1, :cond_147

    #@144
    move v0, v1

    #@145
    .line 3880
    goto/16 :goto_18

    #@147
    .line 3882
    :cond_147
    new-instance v0, Ljava/lang/StringBuilder;

    #@149
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@14c
    const-string v1, "wrong type for SKT : "

    #@14e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v0

    #@152
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@155
    move-result-object v0

    #@156
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@159
    move-result-object v0

    #@15a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@15d
    move v0, v4

    #@15e
    .line 3884
    goto/16 :goto_18

    #@160
    .line 3891
    :pswitch_160
    if-eqz p1, :cond_18

    #@162
    .line 3893
    if-ne p1, v3, :cond_167

    #@164
    move v0, v1

    #@165
    .line 3894
    goto/16 :goto_18

    #@167
    .line 3895
    :cond_167
    const/16 v0, 0x8

    #@169
    if-ne p1, v0, :cond_16e

    #@16b
    move v0, v2

    #@16c
    .line 3896
    goto/16 :goto_18

    #@16e
    .line 3898
    :cond_16e
    const/16 v0, 0xc

    #@170
    if-ne p1, v0, :cond_175

    #@172
    .line 3899
    const/4 v0, 0x4

    #@173
    goto/16 :goto_18

    #@175
    .line 3902
    :cond_175
    const/16 v0, 0x10

    #@177
    if-ne p1, v0, :cond_17c

    #@179
    move v0, v3

    #@17a
    .line 3903
    goto/16 :goto_18

    #@17c
    .line 3906
    :cond_17c
    new-instance v0, Ljava/lang/StringBuilder;

    #@17e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@181
    const-string v1, "wrong type for MPCS : "

    #@183
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@186
    move-result-object v0

    #@187
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v0

    #@18b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18e
    move-result-object v0

    #@18f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@192
    move v0, v4

    #@193
    .line 3908
    goto/16 :goto_18

    #@195
    .line 3920
    :cond_195
    if-eqz p1, :cond_18

    #@197
    :cond_197
    move v0, v4

    #@198
    .line 3923
    goto/16 :goto_18

    #@19a
    .line 3758
    :pswitch_data_19a
    .packed-switch 0x1
        :pswitch_19
        :pswitch_eb
        :pswitch_8
        :pswitch_98
        :pswitch_120
        :pswitch_140
        :pswitch_160
        :pswitch_49
        :pswitch_71
        :pswitch_8
        :pswitch_c6
    .end packed-switch
.end method

.method public unregisterForDataConnectEvent(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1611
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1612
    return-void
.end method

.method public updateDataActivity()V
    .registers 15

    #@0
    .prologue
    const-wide/16 v12, 0x0

    #@2
    .line 2447
    new-instance v0, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

    #@4
    iget-wide v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mTxPkts:J

    #@6
    iget-wide v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRxPkts:J

    #@8
    move-object v1, p0

    #@9
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;-><init>(Lcom/android/internal/telephony/DataConnectionTracker;JJ)V

    #@c
    .line 2448
    .local v0, preTxRxSum:Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;
    new-instance v6, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;

    #@e
    invoke-direct {v6, p0}, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;-><init>(Lcom/android/internal/telephony/DataConnectionTracker;)V

    #@11
    .line 2449
    .local v6, curTxRxSum:Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;
    invoke-virtual {v6}, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->updateTxRxSum()V

    #@14
    .line 2450
    iget-wide v1, v6, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->txPkts:J

    #@16
    iput-wide v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mTxPkts:J

    #@18
    .line 2451
    iget-wide v1, v6, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->rxPkts:J

    #@1a
    iput-wide v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRxPkts:J

    #@1c
    .line 2454
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v2, "updateDataActivity: curTxRxSum="

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    const-string v2, " preTxRxSum="

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@3c
    .line 2457
    iget-boolean v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mNetStatPollEnabled:Z

    #@3e
    if-eqz v1, :cond_f8

    #@40
    iget-wide v1, v0, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->txPkts:J

    #@42
    cmp-long v1, v1, v12

    #@44
    if-gtz v1, :cond_4c

    #@46
    iget-wide v1, v0, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->rxPkts:J

    #@48
    cmp-long v1, v1, v12

    #@4a
    if-lez v1, :cond_f8

    #@4c
    .line 2458
    :cond_4c
    iget-wide v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mTxPkts:J

    #@4e
    iget-wide v3, v0, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->txPkts:J

    #@50
    sub-long v10, v1, v3

    #@52
    .line 2459
    .local v10, sent:J
    iget-wide v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRxPkts:J

    #@54
    iget-wide v3, v0, Lcom/android/internal/telephony/DataConnectionTracker$TxRxSum;->rxPkts:J

    #@56
    sub-long v8, v1, v3

    #@58
    .line 2462
    .local v8, received:J
    new-instance v1, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v2, "updateDataActivity: sent="

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    const-string v2, " received="

    #@69
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v1

    #@6d
    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v1

    #@75
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@78
    .line 2463
    cmp-long v1, v10, v12

    #@7a
    if-lez v1, :cond_f9

    #@7c
    cmp-long v1, v8, v12

    #@7e
    if-lez v1, :cond_f9

    #@80
    .line 2464
    sget-object v7, Lcom/android/internal/telephony/DctConstants$Activity;->DATAINANDOUT:Lcom/android/internal/telephony/DctConstants$Activity;

    #@82
    .line 2474
    .local v7, newActivity:Lcom/android/internal/telephony/DctConstants$Activity;
    :goto_82
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActivity:Lcom/android/internal/telephony/DctConstants$Activity;

    #@84
    if-eq v1, v7, :cond_a7

    #@86
    iget-boolean v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsScreenOn:Z

    #@88
    if-eqz v1, :cond_a7

    #@8a
    .line 2476
    new-instance v1, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v2, "updateDataActivity: newActivity="

    #@91
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v1

    #@99
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v1

    #@9d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@a0
    .line 2477
    iput-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActivity:Lcom/android/internal/telephony/DctConstants$Activity;

    #@a2
    .line 2478
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a4
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->notifyDataActivity()V

    #@a7
    .line 2483
    :cond_a7
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a9
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@ab
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@ae
    move-result-object v1

    #@af
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_DATABLOCK:Z

    #@b1
    if-eqz v1, :cond_f8

    #@b3
    .line 2485
    cmp-long v1, v10, v12

    #@b5
    if-nez v1, :cond_11e

    #@b7
    cmp-long v1, v8, v12

    #@b9
    if-nez v1, :cond_11e

    #@bb
    .line 2487
    iget-wide v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->tx_onlycount:J

    #@bd
    const-wide/16 v3, 0x1

    #@bf
    add-long/2addr v1, v3

    #@c0
    iput-wide v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->tx_onlycount:J

    #@c2
    .line 2495
    :goto_c2
    iget-wide v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->tx_onlycount:J

    #@c4
    const-wide/16 v3, 0x3c

    #@c6
    cmp-long v1, v1, v3

    #@c8
    if-lez v1, :cond_f8

    #@ca
    .line 2497
    new-instance v1, Ljava/lang/StringBuilder;

    #@cc
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@cf
    const-string v2, "[LGE_DATA] updateDataActivity: tx_onlycount = "

    #@d1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v1

    #@d5
    iget-wide v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->tx_onlycount:J

    #@d7
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@da
    move-result-object v1

    #@db
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v1

    #@df
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@e2
    .line 2498
    iput-wide v12, p0, Lcom/android/internal/telephony/DataConnectionTracker;->tx_onlycount:J

    #@e4
    .line 2501
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@e6
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getRouteList_debug:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@e8
    const-string v3, ""

    #@ea
    const/4 v4, 0x1

    #@eb
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@ee
    .line 2503
    sget-boolean v1, Lcom/android/internal/telephony/DataConnectionTracker;->voice_call_ing:Z

    #@f0
    if-nez v1, :cond_f8

    #@f2
    .line 2504
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@f4
    const/4 v2, 0x0

    #@f5
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@f8
    .line 2509
    .end local v7           #newActivity:Lcom/android/internal/telephony/DctConstants$Activity;
    .end local v8           #received:J
    .end local v10           #sent:J
    :cond_f8
    return-void

    #@f9
    .line 2465
    .restart local v8       #received:J
    .restart local v10       #sent:J
    :cond_f9
    cmp-long v1, v10, v12

    #@fb
    if-lez v1, :cond_105

    #@fd
    cmp-long v1, v8, v12

    #@ff
    if-nez v1, :cond_105

    #@101
    .line 2466
    sget-object v7, Lcom/android/internal/telephony/DctConstants$Activity;->DATAOUT:Lcom/android/internal/telephony/DctConstants$Activity;

    #@103
    .restart local v7       #newActivity:Lcom/android/internal/telephony/DctConstants$Activity;
    goto/16 :goto_82

    #@105
    .line 2467
    .end local v7           #newActivity:Lcom/android/internal/telephony/DctConstants$Activity;
    :cond_105
    cmp-long v1, v10, v12

    #@107
    if-nez v1, :cond_111

    #@109
    cmp-long v1, v8, v12

    #@10b
    if-lez v1, :cond_111

    #@10d
    .line 2468
    sget-object v7, Lcom/android/internal/telephony/DctConstants$Activity;->DATAIN:Lcom/android/internal/telephony/DctConstants$Activity;

    #@10f
    .restart local v7       #newActivity:Lcom/android/internal/telephony/DctConstants$Activity;
    goto/16 :goto_82

    #@111
    .line 2470
    .end local v7           #newActivity:Lcom/android/internal/telephony/DctConstants$Activity;
    :cond_111
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActivity:Lcom/android/internal/telephony/DctConstants$Activity;

    #@113
    sget-object v2, Lcom/android/internal/telephony/DctConstants$Activity;->DORMANT:Lcom/android/internal/telephony/DctConstants$Activity;

    #@115
    if-ne v1, v2, :cond_11b

    #@117
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActivity:Lcom/android/internal/telephony/DctConstants$Activity;

    #@119
    .restart local v7       #newActivity:Lcom/android/internal/telephony/DctConstants$Activity;
    :goto_119
    goto/16 :goto_82

    #@11b
    .end local v7           #newActivity:Lcom/android/internal/telephony/DctConstants$Activity;
    :cond_11b
    sget-object v7, Lcom/android/internal/telephony/DctConstants$Activity;->NONE:Lcom/android/internal/telephony/DctConstants$Activity;

    #@11d
    goto :goto_119

    #@11e
    .line 2491
    .restart local v7       #newActivity:Lcom/android/internal/telephony/DctConstants$Activity;
    :cond_11e
    iput-wide v12, p0, Lcom/android/internal/telephony/DataConnectionTracker;->tx_onlycount:J

    #@120
    goto :goto_c2
.end method
