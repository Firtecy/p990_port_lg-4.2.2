.class Lcom/android/internal/telephony/uicc/SIMRecords$EfSktImsiPLoaded;
.super Ljava/lang/Object;
.source "SIMRecords.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/SIMRecords;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EfSktImsiPLoaded"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/uicc/SIMRecords;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/uicc/SIMRecords;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3595
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfSktImsiPLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/uicc/SIMRecords;Lcom/android/internal/telephony/uicc/SIMRecords$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3595
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/SIMRecords$EfSktImsiPLoaded;-><init>(Lcom/android/internal/telephony/uicc/SIMRecords;)V

    #@3
    return-void
.end method


# virtual methods
.method public getEfName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3597
    const-string v0, "EF_IMSI_P"

    #@2
    return-object v0
.end method

.method public onRecordLoaded(Landroid/os/AsyncResult;)V
    .registers 6
    .parameter "ar"

    #@0
    .prologue
    .line 3601
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2
    check-cast v1, [B

    #@4
    move-object v0, v1

    #@5
    check-cast v0, [B

    #@7
    .line 3602
    .local v0, data:[B
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfSktImsiPLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@9
    const/4 v2, 0x0

    #@a
    array-length v3, v0

    #@b
    invoke-static {v0, v2, v3}, Lcom/android/internal/telephony/uicc/IccUtils;->bcdToString([BII)Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    iput-object v2, v1, Lcom/android/internal/telephony/uicc/SIMRecords;->imsip:Ljava/lang/String;

    #@11
    .line 3603
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfSktImsiPLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@13
    const/4 v2, 0x0

    #@14
    const-string v3, "skt"

    #@16
    invoke-static {v1, v2, v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$300(Lcom/android/internal/telephony/uicc/SIMRecords;Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 3604
    const-string v1, "GSM"

    #@1b
    const-string v2, "[LGE_USIM] mUiccType : SKT"

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 3605
    const-string v1, "GSM"

    #@22
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v3, "IMSIP: "

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfSktImsiPLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@2f
    iget-object v3, v3, Lcom/android/internal/telephony/uicc/SIMRecords;->imsip:Ljava/lang/String;

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 3606
    return-void
.end method
