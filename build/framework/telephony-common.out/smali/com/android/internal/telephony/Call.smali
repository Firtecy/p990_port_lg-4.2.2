.class public abstract Lcom/android/internal/telephony/Call;
.super Ljava/lang/Object;
.source "Call.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/Call$State;
    }
.end annotation


# instance fields
.field protected final LOG_TAG:Ljava/lang/String;

.field protected isGeneric:Z

.field public state:Lcom/android/internal/telephony/Call$State;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 48
    sget-object v0, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@7
    .line 57
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/android/internal/telephony/Call;->isGeneric:Z

    #@a
    .line 59
    const-string v0, "Call"

    #@c
    iput-object v0, p0, Lcom/android/internal/telephony/Call;->LOG_TAG:Ljava/lang/String;

    #@e
    return-void
.end method


# virtual methods
.method public abstract getConnections()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Connection;",
            ">;"
        }
    .end annotation
.end method

.method public getEarliestConnectTime()J
    .registers 11

    #@0
    .prologue
    .line 194
    const-wide v6, 0x7fffffffffffffffL

    #@5
    .line 195
    .local v6, time:J
    invoke-virtual {p0}, Lcom/android/internal/telephony/Call;->getConnections()Ljava/util/List;

    #@8
    move-result-object v2

    #@9
    .line 197
    .local v2, l:Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@c
    move-result v8

    #@d
    if-nez v8, :cond_12

    #@f
    .line 198
    const-wide/16 v8, 0x0

    #@11
    .line 210
    :goto_11
    return-wide v8

    #@12
    .line 201
    :cond_12
    const/4 v1, 0x0

    #@13
    .local v1, i:I
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@16
    move-result v3

    #@17
    .local v3, s:I
    :goto_17
    if-ge v1, v3, :cond_2b

    #@19
    .line 202
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Lcom/android/internal/telephony/Connection;

    #@1f
    .line 205
    .local v0, c:Lcom/android/internal/telephony/Connection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getConnectTime()J

    #@22
    move-result-wide v4

    #@23
    .line 207
    .local v4, t:J
    cmp-long v8, v4, v6

    #@25
    if-gez v8, :cond_28

    #@27
    move-wide v6, v4

    #@28
    .line 201
    :cond_28
    add-int/lit8 v1, v1, 0x1

    #@2a
    goto :goto_17

    #@2b
    .end local v0           #c:Lcom/android/internal/telephony/Connection;
    .end local v4           #t:J
    :cond_2b
    move-wide v8, v6

    #@2c
    .line 210
    goto :goto_11
.end method

.method public getEarliestConnection()Lcom/android/internal/telephony/Connection;
    .registers 11

    #@0
    .prologue
    .line 144
    const-wide v7, 0x7fffffffffffffffL

    #@5
    .line 146
    .local v7, time:J
    const/4 v1, 0x0

    #@6
    .line 148
    .local v1, earliest:Lcom/android/internal/telephony/Connection;
    invoke-virtual {p0}, Lcom/android/internal/telephony/Call;->getConnections()Ljava/util/List;

    #@9
    move-result-object v3

    #@a
    .line 150
    .local v3, l:Ljava/util/List;
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@d
    move-result v9

    #@e
    if-nez v9, :cond_12

    #@10
    .line 151
    const/4 v9, 0x0

    #@11
    .line 166
    :goto_11
    return-object v9

    #@12
    .line 154
    :cond_12
    const/4 v2, 0x0

    #@13
    .local v2, i:I
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@16
    move-result v4

    #@17
    .local v4, s:I
    :goto_17
    if-ge v2, v4, :cond_2c

    #@19
    .line 155
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Lcom/android/internal/telephony/Connection;

    #@1f
    .line 158
    .local v0, c:Lcom/android/internal/telephony/Connection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCreateTime()J

    #@22
    move-result-wide v5

    #@23
    .line 160
    .local v5, t:J
    cmp-long v9, v5, v7

    #@25
    if-gez v9, :cond_29

    #@27
    .line 161
    move-object v1, v0

    #@28
    .line 162
    move-wide v7, v5

    #@29
    .line 154
    :cond_29
    add-int/lit8 v2, v2, 0x1

    #@2b
    goto :goto_17

    #@2c
    .end local v0           #c:Lcom/android/internal/telephony/Connection;
    .end local v5           #t:J
    :cond_2c
    move-object v9, v1

    #@2d
    .line 166
    goto :goto_11
.end method

.method public getEarliestCreateTime()J
    .registers 11

    #@0
    .prologue
    .line 172
    const-wide v6, 0x7fffffffffffffffL

    #@5
    .line 174
    .local v6, time:J
    invoke-virtual {p0}, Lcom/android/internal/telephony/Call;->getConnections()Ljava/util/List;

    #@8
    move-result-object v2

    #@9
    .line 176
    .local v2, l:Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@c
    move-result v8

    #@d
    if-nez v8, :cond_12

    #@f
    .line 177
    const-wide/16 v8, 0x0

    #@11
    .line 189
    :goto_11
    return-wide v8

    #@12
    .line 180
    :cond_12
    const/4 v1, 0x0

    #@13
    .local v1, i:I
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@16
    move-result v3

    #@17
    .local v3, s:I
    :goto_17
    if-ge v1, v3, :cond_2b

    #@19
    .line 181
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Lcom/android/internal/telephony/Connection;

    #@1f
    .line 184
    .local v0, c:Lcom/android/internal/telephony/Connection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCreateTime()J

    #@22
    move-result-wide v4

    #@23
    .line 186
    .local v4, t:J
    cmp-long v8, v4, v6

    #@25
    if-gez v8, :cond_28

    #@27
    move-wide v6, v4

    #@28
    .line 180
    :cond_28
    add-int/lit8 v1, v1, 0x1

    #@2a
    goto :goto_17

    #@2b
    .end local v0           #c:Lcom/android/internal/telephony/Connection;
    .end local v4           #t:J
    :cond_2b
    move-wide v8, v6

    #@2c
    .line 189
    goto :goto_11
.end method

.method public getLatestConnection()Lcom/android/internal/telephony/Connection;
    .registers 11

    #@0
    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/android/internal/telephony/Call;->getConnections()Ljava/util/List;

    #@3
    move-result-object v2

    #@4
    .line 231
    .local v2, l:Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@7
    move-result v9

    #@8
    if-nez v9, :cond_c

    #@a
    .line 232
    const/4 v3, 0x0

    #@b
    .line 247
    :cond_b
    return-object v3

    #@c
    .line 235
    :cond_c
    const-wide/16 v7, 0x0

    #@e
    .line 236
    .local v7, time:J
    const/4 v3, 0x0

    #@f
    .line 237
    .local v3, latest:Lcom/android/internal/telephony/Connection;
    const/4 v1, 0x0

    #@10
    .local v1, i:I
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@13
    move-result v4

    #@14
    .local v4, s:I
    :goto_14
    if-ge v1, v4, :cond_b

    #@16
    .line 238
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Lcom/android/internal/telephony/Connection;

    #@1c
    .line 239
    .local v0, c:Lcom/android/internal/telephony/Connection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCreateTime()J

    #@1f
    move-result-wide v5

    #@20
    .line 241
    .local v5, t:J
    cmp-long v9, v5, v7

    #@22
    if-lez v9, :cond_26

    #@24
    .line 242
    move-object v3, v0

    #@25
    .line 243
    move-wide v7, v5

    #@26
    .line 237
    :cond_26
    add-int/lit8 v1, v1, 0x1

    #@28
    goto :goto_14
.end method

.method public abstract getPhone()Lcom/android/internal/telephony/Phone;
.end method

.method public getState()Lcom/android/internal/telephony/Call$State;
    .registers 2

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@2
    return-object v0
.end method

.method public abstract hangup()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation
.end method

.method public abstract hangup(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation
.end method

.method public abstract hangupAllCalls()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation
.end method

.method public hangupForVoIP()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 91
    return-void
.end method

.method public hangupIfAlive()V
    .registers 5

    #@0
    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_d

    #@a
    .line 271
    :try_start_a
    invoke-virtual {p0}, Lcom/android/internal/telephony/Call;->hangup()V
    :try_end_d
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_a .. :try_end_d} :catch_e

    #@d
    .line 276
    :cond_d
    :goto_d
    return-void

    #@e
    .line 272
    :catch_e
    move-exception v0

    #@f
    .line 273
    .local v0, ex:Lcom/android/internal/telephony/CallStateException;
    const-string v1, "Call"

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, " hangupIfActive: caught "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    goto :goto_d
.end method

.method public hangupNotResume()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 76
    return-void
.end method

.method public hasConnection(Lcom/android/internal/telephony/Connection;)Z
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 102
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    #@3
    move-result-object v0

    #@4
    if-ne v0, p0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public hasConnections()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 110
    invoke-virtual {p0}, Lcom/android/internal/telephony/Call;->getConnections()Ljava/util/List;

    #@4
    move-result-object v0

    #@5
    .line 112
    .local v0, connections:Ljava/util/List;
    if-nez v0, :cond_8

    #@7
    .line 116
    :cond_7
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@b
    move-result v2

    #@c
    if-lez v2, :cond_7

    #@e
    const/4 v1, 0x1

    #@f
    goto :goto_7
.end method

.method public isDialingOrAlerting()Z
    .registers 2

    #@0
    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->isDialing()Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public isGeneric()Z
    .registers 2

    #@0
    .prologue
    .line 255
    iget-boolean v0, p0, Lcom/android/internal/telephony/Call;->isGeneric:Z

    #@2
    return v0
.end method

.method public isIdle()Z
    .registers 2

    #@0
    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public abstract isMultiparty()Z
.end method

.method public isRinging()Z
    .registers 2

    #@0
    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->isRinging()Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public setGeneric(Z)V
    .registers 2
    .parameter "generic"

    #@0
    .prologue
    .line 262
    iput-boolean p1, p0, Lcom/android/internal/telephony/Call;->isGeneric:Z

    #@2
    .line 263
    return-void
.end method
