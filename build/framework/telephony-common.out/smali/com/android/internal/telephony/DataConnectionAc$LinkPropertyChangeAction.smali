.class public final enum Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;
.super Ljava/lang/Enum;
.source "DataConnectionAc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnectionAc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LinkPropertyChangeAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

.field public static final enum CHANGED:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

.field public static final enum NONE:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

.field public static final enum RESET:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 148
    new-instance v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@5
    const-string v1, "NONE"

    #@7
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->NONE:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@c
    new-instance v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@e
    const-string v1, "CHANGED"

    #@10
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->CHANGED:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@15
    new-instance v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@17
    const-string v1, "RESET"

    #@19
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->RESET:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@1e
    .line 147
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@21
    sget-object v1, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->NONE:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->CHANGED:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->RESET:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->$VALUES:[Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static fromInt(I)Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 151
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->NONE:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->ordinal()I

    #@5
    move-result v0

    #@6
    if-ne p0, v0, :cond_b

    #@8
    .line 152
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->NONE:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@a
    .line 156
    :goto_a
    return-object v0

    #@b
    .line 153
    :cond_b
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->CHANGED:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@d
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->ordinal()I

    #@10
    move-result v0

    #@11
    if-ne p0, v0, :cond_16

    #@13
    .line 154
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->CHANGED:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@15
    goto :goto_a

    #@16
    .line 155
    :cond_16
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->RESET:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@18
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->ordinal()I

    #@1b
    move-result v0

    #@1c
    if-ne p0, v0, :cond_21

    #@1e
    .line 156
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->RESET:Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@20
    goto :goto_a

    #@21
    .line 158
    :cond_21
    new-instance v0, Ljava/lang/RuntimeException;

    #@23
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v2, "LinkPropertyChangeAction.fromInt: bad value="

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@39
    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 147
    const-class v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;
    .registers 1

    #@0
    .prologue
    .line 147
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->$VALUES:[Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;

    #@8
    return-object v0
.end method
