.class Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;
.super Landroid/content/BroadcastReceiver;
.source "CdmaServiceStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 258
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 262
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4
    iget-boolean v1, v1, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@6
    if-nez v1, :cond_27

    #@8
    .line 263
    const-string v1, "CDMA"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Received Intent "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    const-string v3, " while being destroyed. Ignoring."

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 286
    :cond_26
    :goto_26
    return-void

    #@27
    .line 267
    :cond_27
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    const-string v2, "android.intent.action.LOCALE_CHANGED"

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v1

    #@31
    if-eqz v1, :cond_73

    #@33
    .line 269
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@35
    iget-object v1, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@37
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getState()I

    #@3a
    move-result v1

    #@3b
    if-nez v1, :cond_52

    #@3d
    .line 270
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@3f
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@41
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getCdmaEriText()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    .line 277
    .local v0, eriText:Ljava/lang/String;
    :goto_45
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@47
    iget-object v1, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@49
    invoke-virtual {v1, v0}, Landroid/telephony/ServiceState;->setOperatorAlphaLong(Ljava/lang/String;)V

    #@4c
    .line 278
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@4e
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->updateSpnDisplay()V

    #@51
    goto :goto_26

    #@52
    .line 271
    .end local v0           #eriText:Ljava/lang/String;
    :cond_52
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@54
    iget-object v1, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@56
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getState()I

    #@59
    move-result v1

    #@5a
    const/4 v2, 0x3

    #@5b
    if-ne v1, v2, :cond_5f

    #@5d
    .line 272
    const/4 v0, 0x0

    #@5e
    .restart local v0       #eriText:Ljava/lang/String;
    goto :goto_45

    #@5f
    .line 274
    .end local v0           #eriText:Ljava/lang/String;
    :cond_5f
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@61
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@63
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@66
    move-result-object v1

    #@67
    const v2, 0x10400d6

    #@6a
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@6d
    move-result-object v1

    #@6e
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@71
    move-result-object v0

    #@72
    .restart local v0       #eriText:Ljava/lang/String;
    goto :goto_45

    #@73
    .line 281
    .end local v0           #eriText:Ljava/lang/String;
    :cond_73
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@76
    move-result-object v1

    #@77
    const-string v2, "android.intent.action.ACTION_SHUTDOWN"

    #@79
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7c
    move-result v1

    #@7d
    if-eqz v1, :cond_26

    #@7f
    .line 282
    const-string v1, "CDMA"

    #@81
    const-string v2, "get ACTION SHUTDOWN!!"

    #@83
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 283
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@88
    const/4 v2, 0x1

    #@89
    iput-boolean v2, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isInShutDown:Z

    #@8b
    goto :goto_26
.end method
