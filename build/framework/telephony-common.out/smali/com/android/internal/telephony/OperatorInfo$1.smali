.class final Lcom/android/internal/telephony/OperatorInfo$1;
.super Ljava/lang/Object;
.source "OperatorInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/OperatorInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/android/internal/telephony/OperatorInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 196
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/android/internal/telephony/OperatorInfo;
    .registers 9
    .parameter "in"

    #@0
    .prologue
    .line 199
    const-string v1, "radio.ratdisplay"

    #@2
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    const-string v2, "true"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_2b

    #@e
    .line 200
    new-instance v0, Lcom/android/internal/telephony/OperatorInfo;

    #@10
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    #@1f
    move-result-object v4

    #@20
    check-cast v4, Lcom/android/internal/telephony/OperatorInfo$State;

    #@22
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/OperatorInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/OperatorInfo$State;Ljava/lang/String;)V

    #@29
    .local v0, opInfo:Lcom/android/internal/telephony/OperatorInfo;
    move-object v6, v0

    #@2a
    .line 214
    .end local v0           #opInfo:Lcom/android/internal/telephony/OperatorInfo;
    .local v6, opInfo:Ljava/lang/Object;
    :goto_2a
    return-object v6

    #@2b
    .line 209
    .end local v6           #opInfo:Ljava/lang/Object;
    :cond_2b
    new-instance v0, Lcom/android/internal/telephony/OperatorInfo;

    #@2d
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    #@3c
    move-result-object v1

    #@3d
    check-cast v1, Lcom/android/internal/telephony/OperatorInfo$State;

    #@3f
    invoke-direct {v0, v2, v3, v4, v1}, Lcom/android/internal/telephony/OperatorInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/OperatorInfo$State;)V

    #@42
    .restart local v0       #opInfo:Lcom/android/internal/telephony/OperatorInfo;
    move-object v6, v0

    #@43
    .line 214
    .restart local v6       #opInfo:Ljava/lang/Object;
    goto :goto_2a
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/OperatorInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/android/internal/telephony/OperatorInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/android/internal/telephony/OperatorInfo;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 221
    new-array v0, p1, [Lcom/android/internal/telephony/OperatorInfo;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/OperatorInfo$1;->newArray(I)[Lcom/android/internal/telephony/OperatorInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
