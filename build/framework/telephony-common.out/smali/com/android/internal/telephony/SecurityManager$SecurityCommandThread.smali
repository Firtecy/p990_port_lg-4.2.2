.class Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;
.super Ljava/lang/Thread;
.source "SecurityManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/SecurityManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SecurityCommandThread"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;
    }
.end annotation


# instance fields
.field data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

.field private mDone:Z

.field private mHandler:Landroid/os/Handler;

.field private mPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@3
    .line 120
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->mDone:Z

    #@6
    .line 135
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->mPhone:Lcom/android/internal/telephony/Phone;

    #@c
    .line 136
    iget-object v0, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->mPhone:Lcom/android/internal/telephony/Phone;

    #@e
    if-nez v0, :cond_18

    #@10
    .line 137
    const-string v0, "SecurityManager"

    #@12
    const-string v1, "Phone Failed"

    #@14
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 141
    :goto_17
    return-void

    #@18
    .line 139
    :cond_18
    const-string v0, "Phone Successfully"

    #@1a
    invoke-static {v0}, Lcom/android/internal/telephony/SecurityManager;->access$000(Ljava/lang/String;)V

    #@1d
    goto :goto_17
.end method

.method private declared-synchronized SecurityCommand(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 8
    .parameter "command"
    .parameter "request"
    .parameter "response"

    #@0
    .prologue
    .line 236
    monitor-enter p0

    #@1
    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->mHandler:Landroid/os/Handler;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_12

    #@3
    if-nez v2, :cond_15

    #@5
    .line 238
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_12
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_8} :catch_9

    #@8
    goto :goto_1

    #@9
    .line 239
    :catch_9
    move-exception v1

    #@a
    .line 240
    .local v1, e:Ljava/lang/InterruptedException;
    :try_start_a
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_11
    .catchall {:try_start_a .. :try_end_11} :catchall_12

    #@11
    goto :goto_1

    #@12
    .line 236
    .end local v1           #e:Ljava/lang/InterruptedException;
    :catchall_12
    move-exception v2

    #@13
    monitor-exit p0

    #@14
    throw v2

    #@15
    .line 244
    :cond_15
    :try_start_15
    new-instance v2, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@17
    invoke-direct {v2, p2, p3}, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    #@1a
    iput-object v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@1c
    .line 245
    iget-object v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->mHandler:Landroid/os/Handler;

    #@1e
    invoke-static {v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@21
    move-result-object v0

    #@22
    .line 246
    .local v0, callback:Landroid/os/Message;
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v3, "callback: "

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v2}, Lcom/android/internal/telephony/SecurityManager;->access$000(Ljava/lang/String;)V

    #@38
    .line 248
    const-string v2, "SecurityCommand Start!"

    #@3a
    invoke-static {v2}, Lcom/android/internal/telephony/SecurityManager;->access$000(Ljava/lang/String;)V

    #@3d
    .line 250
    new-instance v2, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v3, "Request Data: "

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    iget-object v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@4a
    iget-object v2, v2, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->request:Ljava/lang/Object;

    #@4c
    check-cast v2, [B

    #@4e
    check-cast v2, [B

    #@50
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    invoke-static {v2}, Lcom/android/internal/telephony/SecurityManager;->access$000(Ljava/lang/String;)V

    #@5f
    .line 252
    iget-object v3, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->mPhone:Lcom/android/internal/telephony/Phone;

    #@61
    iget-object v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@63
    iget-object v2, v2, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->request:Ljava/lang/Object;

    #@65
    check-cast v2, [B

    #@67
    check-cast v2, [B

    #@69
    invoke-interface {v3, v2, v0}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    #@6c
    .line 255
    :goto_6c
    iget-boolean v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->mDone:Z
    :try_end_6e
    .catchall {:try_start_15 .. :try_end_6e} :catchall_12

    #@6e
    if-nez v2, :cond_7d

    #@70
    .line 257
    :try_start_70
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_73
    .catchall {:try_start_70 .. :try_end_73} :catchall_12
    .catch Ljava/lang/InterruptedException; {:try_start_70 .. :try_end_73} :catch_74

    #@73
    goto :goto_6c

    #@74
    .line 258
    :catch_74
    move-exception v1

    #@75
    .line 260
    .restart local v1       #e:Ljava/lang/InterruptedException;
    :try_start_75
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@78
    move-result-object v2

    #@79
    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    #@7c
    goto :goto_6c

    #@7d
    .line 265
    .end local v1           #e:Ljava/lang/InterruptedException;
    :cond_7d
    new-instance v2, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v3, "callback after invokeOemRilRequestRaw: "

    #@84
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v2

    #@88
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v2

    #@8c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v2

    #@90
    invoke-static {v2}, Lcom/android/internal/telephony/SecurityManager;->access$000(Ljava/lang/String;)V

    #@93
    .line 266
    new-instance v2, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v3, "Response Data: "

    #@9a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v3

    #@9e
    iget-object v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@a0
    iget-object v2, v2, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;

    #@a2
    check-cast v2, [B

    #@a4
    check-cast v2, [B

    #@a6
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@a9
    move-result-object v2

    #@aa
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v2

    #@ae
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v2

    #@b2
    invoke-static {v2}, Lcom/android/internal/telephony/SecurityManager;->access$000(Ljava/lang/String;)V

    #@b5
    .line 267
    const-string v2, "SecurityCommand End!"

    #@b7
    invoke-static {v2}, Lcom/android/internal/telephony/SecurityManager;->access$000(Ljava/lang/String;)V

    #@ba
    .line 269
    iget-object v2, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->data:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;

    #@bc
    iget-object v2, v2, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$SecurityIO;->response:Ljava/lang/Object;
    :try_end_be
    .catchall {:try_start_75 .. :try_end_be} :catchall_12

    #@be
    monitor-exit p0

    #@bf
    return-object v2
.end method

.method static synthetic access$102(Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 116
    iput-boolean p1, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->mDone:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 116
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->SecurityCommand(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method


# virtual methods
.method public run()V
    .registers 2

    #@0
    .prologue
    .line 145
    const-string v0, "SecurityCommandThread Start!"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/SecurityManager;->access$000(Ljava/lang/String;)V

    #@5
    .line 146
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@8
    .line 147
    monitor-enter p0

    #@9
    .line 148
    :try_start_9
    new-instance v0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;

    #@b
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread$1;-><init>(Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;)V

    #@e
    iput-object v0, p0, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->mHandler:Landroid/os/Handler;

    #@10
    .line 229
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@13
    .line 230
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_9 .. :try_end_14} :catchall_1d

    #@14
    .line 231
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@17
    .line 232
    const-string v0, "SecurityCommandThread End!"

    #@19
    invoke-static {v0}, Lcom/android/internal/telephony/SecurityManager;->access$000(Ljava/lang/String;)V

    #@1c
    .line 233
    return-void

    #@1d
    .line 230
    :catchall_1d
    move-exception v0

    #@1e
    :try_start_1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method
