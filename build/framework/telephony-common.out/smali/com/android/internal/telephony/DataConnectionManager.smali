.class public Lcom/android/internal/telephony/DataConnectionManager;
.super Ljava/lang/Object;
.source "DataConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/DataConnectionManager$1;,
        Lcom/android/internal/telephony/DataConnectionManager$FunctionName;
    }
.end annotation


# static fields
.field public static final DATA_DISCONNECTED_PERMANENT_FAIL_NOTIFICATION:I = 0x9

.field public static final DATA_NOTI_BACKGROUND_SETTING_NOTIFICATION:I = 0xa

.field private static final DBG:Z = true

.field public static final DCM_MOBILE_NETWORK_IS_ALLOWED:I = 0x1

.field public static final DCM_MOBILE_NETWORK_IS_DISALLOWED:I = 0x2

.field public static final DCM_MOBILE_NETWORK_IS_NEED_POPUP:I = 0x3

.field private static final TAG:Ljava/lang/String; = "[LGE_DATA][DCM] "

.field public static alreadyAppUsedPacket:Z

.field public static blockPacketMenuFlag:Z

.field public static blockPakcetProcessFlag:Z

.field private static sDataConnectionManager:Lcom/android/internal/telephony/DataConnectionManager;


# instance fields
.field public final TOAST_DISABLE_MMS_INOUT:I

.field featureset:Ljava/lang/String;

.field mConnMgr:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field protected mLGfeature:Lcom/android/internal/telephony/LGfeature;

.field mNotification:Landroid/app/Notification;

.field mNotificationMgr:Landroid/app/NotificationManager;

.field private mPhoneMgr:Lcom/android/internal/telephony/ITelephony;

.field private mPolicyService:Landroid/net/INetworkPolicyManager;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 140
    const/4 v0, 0x1

    #@2
    sput-boolean v0, Lcom/android/internal/telephony/DataConnectionManager;->blockPacketMenuFlag:Z

    #@4
    .line 141
    sput-boolean v1, Lcom/android/internal/telephony/DataConnectionManager;->blockPakcetProcessFlag:Z

    #@6
    .line 142
    sput-boolean v1, Lcom/android/internal/telephony/DataConnectionManager;->alreadyAppUsedPacket:Z

    #@8
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/LGfeature;)V
    .registers 5
    .parameter "c"
    .parameter "myfeature"

    #@0
    .prologue
    .line 148
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 90
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->TOAST_DISABLE_MMS_INOUT:I

    #@6
    .line 135
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->mNotification:Landroid/app/Notification;

    #@9
    .line 149
    const-string v0, "[LGE_DATA][DCM] "

    #@b
    const-string v1, "LgeDataManager() has created"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 151
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@12
    .line 153
    const-string v0, "phone"

    #@14
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@17
    move-result-object v0

    #@18
    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->mPhoneMgr:Lcom/android/internal/telephony/ITelephony;

    #@1e
    .line 154
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@20
    const-string v1, "connectivity"

    #@22
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@25
    move-result-object v0

    #@26
    check-cast v0, Landroid/net/ConnectivityManager;

    #@28
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->mConnMgr:Landroid/net/ConnectivityManager;

    #@2a
    .line 157
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@2c
    const-string v1, "notification"

    #@2e
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@31
    move-result-object v0

    #@32
    check-cast v0, Landroid/app/NotificationManager;

    #@34
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->mNotificationMgr:Landroid/app/NotificationManager;

    #@36
    .line 158
    const-string v0, "netpolicy"

    #@38
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@3b
    move-result-object v0

    #@3c
    invoke-static {v0}, Landroid/net/INetworkPolicyManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkPolicyManager;

    #@3f
    move-result-object v0

    #@40
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->mPolicyService:Landroid/net/INetworkPolicyManager;

    #@42
    .line 162
    const-string v0, "ro.afwdata.LGfeatureset"

    #@44
    const-string v1, "none"

    #@46
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->featureset:Ljava/lang/String;

    #@4c
    .line 165
    if-eqz p2, :cond_50

    #@4e
    .line 167
    iput-object p2, p0, Lcom/android/internal/telephony/DataConnectionManager;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@50
    .line 169
    :cond_50
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@52
    if-nez v0, :cond_5e

    #@54
    .line 171
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@56
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionManager;->featureset:Ljava/lang/String;

    #@58
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LGfeature;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/android/internal/telephony/LGfeature;

    #@5b
    move-result-object v0

    #@5c
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionManager;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@5e
    .line 173
    :cond_5e
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/internal/telephony/DataConnectionManager;
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 185
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager;->sDataConnectionManager:Lcom/android/internal/telephony/DataConnectionManager;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 186
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/DataConnectionManager;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/LGfeature;)V

    #@a
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager;->sDataConnectionManager:Lcom/android/internal/telephony/DataConnectionManager;

    #@c
    .line 188
    :cond_c
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager;->sDataConnectionManager:Lcom/android/internal/telephony/DataConnectionManager;

    #@e
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;Lcom/android/internal/telephony/LGfeature;)Lcom/android/internal/telephony/DataConnectionManager;
    .registers 3
    .parameter "c"
    .parameter "myfeature"

    #@0
    .prologue
    .line 177
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager;->sDataConnectionManager:Lcom/android/internal/telephony/DataConnectionManager;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 178
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager;

    #@6
    invoke-direct {v0, p0, p1}, Lcom/android/internal/telephony/DataConnectionManager;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/LGfeature;)V

    #@9
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager;->sDataConnectionManager:Lcom/android/internal/telephony/DataConnectionManager;

    #@b
    .line 180
    :cond_b
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager;->sDataConnectionManager:Lcom/android/internal/telephony/DataConnectionManager;

    #@d
    return-object v0
.end method


# virtual methods
.method public declared-synchronized IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I
    .registers 60
    .parameter "funcName"
    .parameter "strParam"
    .parameter "intParam"

    #@0
    .prologue
    .line 318
    monitor-enter p0

    #@1
    const/16 v53, 0x0

    #@3
    .line 320
    .local v53, voidReturn:I
    :try_start_3
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@5
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@8
    move-result v3

    #@9
    aget v2, v2, v3
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_f1

    #@b
    packed-switch v2, :pswitch_data_83a

    #@e
    .line 889
    .end local v53           #voidReturn:I
    :cond_e
    :goto_e
    monitor-exit p0

    #@f
    return v53

    #@10
    .line 326
    .restart local v53       #voidReturn:I
    :pswitch_10
    :try_start_10
    const-string v2, "[LGE_DATA][DCM] "

    #@12
    const-string v3, "CallingSetMobileDataEnabled"

    #@14
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 328
    move-object/from16 v48, p2

    #@19
    .line 329
    .local v48, sText:Ljava/lang/String;
    const-string v21, ""

    #@1b
    .line 331
    .local v21, enabled:Ljava/lang/String;
    if-nez p3, :cond_f4

    #@1d
    .line 332
    const-string v21, "[Mobile Off]"

    #@1f
    .line 337
    :goto_1f
    const/16 v30, 0x4

    #@21
    .line 338
    .local v30, inputFileSize:I
    const-string v24, "CallingSetMobileDataEnabled.txt"

    #@23
    .line 339
    .local v24, fileName:Ljava/lang/String;
    new-instance v22, Ljava/io/File;

    #@25
    move-object/from16 v0, v22

    #@27
    move-object/from16 v1, v24

    #@29
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@2c
    .line 343
    .local v22, file:Ljava/io/File;
    new-instance v41, Ljava/util/Date;

    #@2e
    invoke-direct/range {v41 .. v41}, Ljava/util/Date;-><init>()V

    #@31
    .line 345
    .local v41, now:Ljava/util/Date;
    new-instance v49, Ljava/text/SimpleDateFormat;

    #@33
    const-string v2, "yyyy.MM.dd HH:mm:ss"

    #@35
    move-object/from16 v0, v49

    #@37
    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@3a
    .line 346
    .local v49, simpleformat:Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    move-object/from16 v0, v49

    #@41
    move-object/from16 v1, v41

    #@43
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    const-string v3, " "

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v51

    #@55
    .line 348
    .local v51, time:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    move-object/from16 v0, v51

    #@5c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v2

    #@60
    move-object/from16 v0, v21

    #@62
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v2

    #@66
    const-string v3, " PackagesName : "

    #@68
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v2

    #@6c
    move-object/from16 v0, v48

    #@6e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v2

    #@72
    const-string v3, "\n"

    #@74
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v48

    #@7c
    .line 349
    const-string v2, "[LGE_DATA][DCM] "

    #@7e
    move-object/from16 v0, v48

    #@80
    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 352
    const-string v2, "persist.service.logging_count"

    #@85
    const/4 v3, 0x0

    #@86
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@89
    move-result v43

    #@8a
    .line 353
    .local v43, prop_count:I
    const-string v2, "[LGE_DATA][DCM] "

    #@8c
    new-instance v3, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v4, "1. CallingSetMobileDataEnabled"

    #@93
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v3

    #@97
    move/from16 v0, v43

    #@99
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v3

    #@9d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v3

    #@a1
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a4
    .catchall {:try_start_10 .. :try_end_a4} :catchall_f1

    #@a4
    .line 355
    const/16 v2, 0x1f4

    #@a6
    move/from16 v0, v43

    #@a8
    if-ge v0, v2, :cond_fd

    #@aa
    .line 358
    :try_start_aa
    move-object/from16 v0, p0

    #@ac
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@ae
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->toString()Ljava/lang/String;

    #@b1
    move-result-object v3

    #@b2
    const v4, 0x8000

    #@b5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    #@b8
    move-result-object v28

    #@b9
    .line 359
    .local v28, fos:Ljava/io/FileOutputStream;
    invoke-virtual/range {v48 .. v48}, Ljava/lang/String;->getBytes()[B

    #@bc
    move-result-object v14

    #@bd
    .line 360
    .local v14, bText:[B
    const/4 v2, 0x0

    #@be
    array-length v3, v14

    #@bf
    move-object/from16 v0, v28

    #@c1
    invoke-virtual {v0, v14, v2, v3}, Ljava/io/FileOutputStream;->write([BII)V

    #@c4
    .line 361
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileOutputStream;->flush()V

    #@c7
    .line 362
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileOutputStream;->close()V

    #@ca
    .line 363
    add-int/lit8 v43, v43, 0x1

    #@cc
    .line 364
    const-string v2, "persist.service.logging_count"

    #@ce
    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@d1
    move-result-object v3

    #@d2
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d5
    .catchall {:try_start_aa .. :try_end_d5} :catchall_f1
    .catch Ljava/lang/Exception; {:try_start_aa .. :try_end_d5} :catch_f8

    #@d5
    .line 385
    .end local v14           #bText:[B
    .end local v28           #fos:Ljava/io/FileOutputStream;
    :goto_d5
    :try_start_d5
    const-string v2, "[LGE_DATA][DCM] "

    #@d7
    new-instance v3, Ljava/lang/StringBuilder;

    #@d9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@dc
    const-string v4, "2. CallingSetMobileDataEnabled"

    #@de
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v3

    #@e2
    move/from16 v0, v43

    #@e4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v3

    #@e8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v3

    #@ec
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ef
    .catchall {:try_start_d5 .. :try_end_ef} :catchall_f1

    #@ef
    goto/16 :goto_e

    #@f1
    .line 318
    .end local v21           #enabled:Ljava/lang/String;
    .end local v22           #file:Ljava/io/File;
    .end local v24           #fileName:Ljava/lang/String;
    .end local v30           #inputFileSize:I
    .end local v41           #now:Ljava/util/Date;
    .end local v43           #prop_count:I
    .end local v48           #sText:Ljava/lang/String;
    .end local v49           #simpleformat:Ljava/text/SimpleDateFormat;
    .end local v51           #time:Ljava/lang/String;
    :catchall_f1
    move-exception v2

    #@f2
    monitor-exit p0

    #@f3
    throw v2

    #@f4
    .line 334
    .restart local v21       #enabled:Ljava/lang/String;
    .restart local v48       #sText:Ljava/lang/String;
    :cond_f4
    :try_start_f4
    const-string v21, "[Mobile On]"

    #@f6
    goto/16 :goto_1f

    #@f8
    .line 365
    .restart local v22       #file:Ljava/io/File;
    .restart local v24       #fileName:Ljava/lang/String;
    .restart local v30       #inputFileSize:I
    .restart local v41       #now:Ljava/util/Date;
    .restart local v43       #prop_count:I
    .restart local v49       #simpleformat:Ljava/text/SimpleDateFormat;
    .restart local v51       #time:Ljava/lang/String;
    :catch_f8
    move-exception v20

    #@f9
    .line 366
    .local v20, e:Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_fc
    .catchall {:try_start_f4 .. :try_end_fc} :catchall_f1

    #@fc
    goto :goto_d5

    #@fd
    .line 372
    .end local v20           #e:Ljava/lang/Exception;
    :cond_fd
    :try_start_fd
    move-object/from16 v0, p0

    #@ff
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@101
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->toString()Ljava/lang/String;

    #@104
    move-result-object v3

    #@105
    const/4 v4, 0x1

    #@106
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    #@109
    move-result-object v28

    #@10a
    .line 373
    .restart local v28       #fos:Ljava/io/FileOutputStream;
    invoke-virtual/range {v48 .. v48}, Ljava/lang/String;->getBytes()[B

    #@10d
    move-result-object v14

    #@10e
    .line 374
    .restart local v14       #bText:[B
    const/4 v2, 0x0

    #@10f
    array-length v3, v14

    #@110
    move-object/from16 v0, v28

    #@112
    invoke-virtual {v0, v14, v2, v3}, Ljava/io/FileOutputStream;->write([BII)V

    #@115
    .line 375
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileOutputStream;->flush()V

    #@118
    .line 376
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileOutputStream;->close()V

    #@11b
    .line 377
    const/16 v43, 0x0

    #@11d
    .line 378
    const-string v2, "persist.service.logging_count"

    #@11f
    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@122
    move-result-object v3

    #@123
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_126
    .catchall {:try_start_fd .. :try_end_126} :catchall_f1
    .catch Ljava/lang/Exception; {:try_start_fd .. :try_end_126} :catch_127

    #@126
    goto :goto_d5

    #@127
    .line 379
    .end local v14           #bText:[B
    .end local v28           #fos:Ljava/io/FileOutputStream;
    :catch_127
    move-exception v20

    #@128
    .line 380
    .restart local v20       #e:Ljava/lang/Exception;
    :try_start_128
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    #@12b
    goto :goto_d5

    #@12c
    .line 393
    .end local v20           #e:Ljava/lang/Exception;
    .end local v21           #enabled:Ljava/lang/String;
    .end local v22           #file:Ljava/io/File;
    .end local v24           #fileName:Ljava/lang/String;
    .end local v30           #inputFileSize:I
    .end local v41           #now:Ljava/util/Date;
    .end local v43           #prop_count:I
    .end local v48           #sText:Ljava/lang/String;
    .end local v49           #simpleformat:Ljava/text/SimpleDateFormat;
    .end local v51           #time:Ljava/lang/String;
    :pswitch_12c
    const-string v2, "[LGE_DATA][DCM] "

    #@12e
    const-string v3, "getBlockPacketMenuProcess()"

    #@130
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    .line 394
    const-string v2, "[LGE_DATA][DCM] "

    #@135
    new-instance v3, Ljava/lang/StringBuilder;

    #@137
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13a
    const-string v4, " <getBlockPacketMenuProcess()> blockPacketMenuFlag = "

    #@13c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v3

    #@140
    sget-boolean v4, Lcom/android/internal/telephony/DataConnectionManager;->blockPacketMenuFlag:Z

    #@142
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@145
    move-result-object v3

    #@146
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@149
    move-result-object v3

    #@14a
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@14d
    .line 398
    sget-boolean v2, Lcom/android/internal/telephony/DataConnectionManager;->blockPacketMenuFlag:Z

    #@14f
    if-eqz v2, :cond_156

    #@151
    const/4 v2, 0x1

    #@152
    :goto_152
    move/from16 v53, v2

    #@154
    goto/16 :goto_e

    #@156
    :cond_156
    const/4 v2, 0x0

    #@157
    goto :goto_152

    #@158
    .line 403
    :pswitch_158
    const-string v2, "[LGE_DATA][DCM] "

    #@15a
    const-string v3, "getAlreadyAppUsedPacket()"

    #@15c
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@15f
    .line 404
    const-string v2, "[LGE_DATA][DCM] "

    #@161
    new-instance v3, Ljava/lang/StringBuilder;

    #@163
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@166
    const-string v4, " <getAlreadyAppUsedPacket()> alreadyAppUsedPacket = "

    #@168
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16b
    move-result-object v3

    #@16c
    sget-boolean v4, Lcom/android/internal/telephony/DataConnectionManager;->alreadyAppUsedPacket:Z

    #@16e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@171
    move-result-object v3

    #@172
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@175
    move-result-object v3

    #@176
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@179
    .line 406
    sget-boolean v2, Lcom/android/internal/telephony/DataConnectionManager;->alreadyAppUsedPacket:Z

    #@17b
    if-eqz v2, :cond_182

    #@17d
    const/4 v2, 0x1

    #@17e
    :goto_17e
    move/from16 v53, v2

    #@180
    goto/16 :goto_e

    #@182
    :cond_182
    const/4 v2, 0x0

    #@183
    goto :goto_17e

    #@184
    .line 411
    :pswitch_184
    const-string v2, "[LGE_DATA][DCM] "

    #@186
    const-string v3, "getDataNetworkMode()"

    #@188
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18b
    .line 412
    const/4 v2, 0x1

    #@18c
    move/from16 v0, p3

    #@18e
    if-ne v0, v2, :cond_259

    #@190
    const/4 v13, 0x1

    #@191
    .line 414
    .local v13, addPopupResult:Z
    :goto_191
    move-object/from16 v0, p0

    #@193
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@195
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@198
    move-result-object v2

    #@199
    const-string v3, "preferred_data_network_mode"

    #@19b
    const/4 v4, 0x1

    #@19c
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@19f
    move-result v35

    #@1a0
    .line 415
    .local v35, mode:I
    const/16 v38, 0x0

    #@1a2
    .line 417
    .local v38, network_mode:I
    const-string v2, "gsm.operator.isroaming"

    #@1a4
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1a7
    move-result-object v2

    #@1a8
    const-string v3, "true"

    #@1aa
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1ad
    .catchall {:try_start_128 .. :try_end_1ad} :catchall_f1

    #@1ad
    move-result v33

    #@1ae
    .line 421
    .local v33, isRoaming:Z
    :try_start_1ae
    move-object/from16 v0, p0

    #@1b0
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@1b2
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b5
    move-result-object v2

    #@1b6
    const-string v3, "data_roaming"

    #@1b8
    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_1bb
    .catchall {:try_start_1ae .. :try_end_1bb} :catchall_f1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1ae .. :try_end_1bb} :catch_260

    #@1bb
    move-result v2

    #@1bc
    if-eqz v2, :cond_25c

    #@1be
    const/16 v31, 0x1

    #@1c0
    .line 425
    .local v31, isDataRoaming:Z
    :goto_1c0
    :try_start_1c0
    const-string v2, "DATA"

    #@1c2
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c7
    const-string v4, "For KT Roaming isRoaming = "

    #@1c9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v3

    #@1cd
    move/from16 v0, v33

    #@1cf
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v3

    #@1d3
    const-string v4, "isDataRoaming = "

    #@1d5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v3

    #@1d9
    move/from16 v0, v31

    #@1db
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1de
    move-result-object v3

    #@1df
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e2
    move-result-object v3

    #@1e3
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e6
    .line 428
    const-string v2, "[LGE_DATA][DCM] "

    #@1e8
    new-instance v3, Ljava/lang/StringBuilder;

    #@1ea
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1ed
    const-string v4, "getDataNetworkMode() = "

    #@1ef
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v3

    #@1f3
    move-object/from16 v0, p0

    #@1f5
    iget-object v4, v0, Lcom/android/internal/telephony/DataConnectionManager;->mConnMgr:Landroid/net/ConnectivityManager;

    #@1f7
    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@1fa
    move-result v4

    #@1fb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v3

    #@1ff
    const-string v4, " mode = "

    #@201
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@204
    move-result-object v3

    #@205
    move/from16 v0, v35

    #@207
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v3

    #@20b
    const-string v4, "boot = "

    #@20d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v3

    #@211
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@214
    move-result-object v3

    #@215
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@218
    move-result-object v3

    #@219
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21c
    .line 430
    move-object/from16 v0, p0

    #@21e
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->featureset:Ljava/lang/String;

    #@220
    const-string v3, "SKTBASE"

    #@222
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@225
    move-result v2

    #@226
    if-eqz v2, :cond_275

    #@228
    .line 434
    move-object/from16 v0, p0

    #@22a
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mConnMgr:Landroid/net/ConnectivityManager;

    #@22c
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@22f
    move-result v2

    #@230
    if-eqz v2, :cond_268

    #@232
    .line 435
    const/4 v2, 0x1

    #@233
    move/from16 v0, v35

    #@235
    if-ne v0, v2, :cond_265

    #@237
    if-eqz v13, :cond_265

    #@239
    .line 436
    const/16 v38, 0x3

    #@23b
    .line 482
    :cond_23b
    :goto_23b
    const-string v2, "[LGE_DATA][DCM] "

    #@23d
    new-instance v3, Ljava/lang/StringBuilder;

    #@23f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@242
    const-string v4, "getDataNetworkMode() = "

    #@244
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@247
    move-result-object v3

    #@248
    move/from16 v0, v38

    #@24a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24d
    move-result-object v3

    #@24e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@251
    move-result-object v3

    #@252
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@255
    move/from16 v53, v38

    #@257
    .line 484
    goto/16 :goto_e

    #@259
    .line 412
    .end local v13           #addPopupResult:Z
    .end local v31           #isDataRoaming:Z
    .end local v33           #isRoaming:Z
    .end local v35           #mode:I
    .end local v38           #network_mode:I
    :cond_259
    const/4 v13, 0x0

    #@25a
    goto/16 :goto_191

    #@25c
    .line 421
    .restart local v13       #addPopupResult:Z
    .restart local v33       #isRoaming:Z
    .restart local v35       #mode:I
    .restart local v38       #network_mode:I
    :cond_25c
    const/16 v31, 0x0

    #@25e
    goto/16 :goto_1c0

    #@260
    .line 422
    :catch_260
    move-exception v50

    #@261
    .line 423
    .local v50, snfe:Landroid/provider/Settings$SettingNotFoundException;
    const/16 v53, 0x0

    #@263
    goto/16 :goto_e

    #@265
    .line 439
    .end local v50           #snfe:Landroid/provider/Settings$SettingNotFoundException;
    .restart local v31       #isDataRoaming:Z
    :cond_265
    const/16 v38, 0x1

    #@267
    goto :goto_23b

    #@268
    .line 442
    :cond_268
    const/4 v2, 0x1

    #@269
    move/from16 v0, v35

    #@26b
    if-ne v0, v2, :cond_272

    #@26d
    if-eqz v13, :cond_272

    #@26f
    .line 443
    const/16 v38, 0x3

    #@271
    goto :goto_23b

    #@272
    .line 446
    :cond_272
    const/16 v38, 0x2

    #@274
    goto :goto_23b

    #@275
    .line 449
    :cond_275
    move-object/from16 v0, p0

    #@277
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->featureset:Ljava/lang/String;

    #@279
    const-string v3, "KTBASE"

    #@27b
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@27e
    move-result v2

    #@27f
    if-eqz v2, :cond_2a8

    #@281
    .line 450
    move-object/from16 v0, p0

    #@283
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mConnMgr:Landroid/net/ConnectivityManager;

    #@285
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@288
    move-result v2

    #@289
    const/4 v3, 0x1

    #@28a
    if-ne v2, v3, :cond_2a5

    #@28c
    .line 453
    if-eqz v33, :cond_29a

    #@28e
    if-nez v31, :cond_29a

    #@290
    .line 454
    const-string v2, "DATA"

    #@292
    const-string v3, "return DCM_MOBILE_NETWORK_IS_DISALLOWED for kt roaming network"

    #@294
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@297
    .line 455
    const/16 v38, 0x2

    #@299
    goto :goto_23b

    #@29a
    .line 457
    :cond_29a
    const/4 v2, 0x1

    #@29b
    move/from16 v0, v35

    #@29d
    if-ne v0, v2, :cond_2a2

    #@29f
    .line 458
    const/16 v38, 0x3

    #@2a1
    goto :goto_23b

    #@2a2
    .line 460
    :cond_2a2
    const/16 v38, 0x1

    #@2a4
    goto :goto_23b

    #@2a5
    .line 466
    :cond_2a5
    const/16 v38, 0x2

    #@2a7
    goto :goto_23b

    #@2a8
    .line 468
    :cond_2a8
    move-object/from16 v0, p0

    #@2aa
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->featureset:Ljava/lang/String;

    #@2ac
    const-string v3, "LGTBASE"

    #@2ae
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@2b1
    move-result v2

    #@2b2
    if-eqz v2, :cond_23b

    #@2b4
    .line 471
    const/4 v2, 0x1

    #@2b5
    move/from16 v0, v35

    #@2b7
    if-eq v0, v2, :cond_2c0

    #@2b9
    const v2, 0x10001

    #@2bc
    move/from16 v0, v35

    #@2be
    if-ne v0, v2, :cond_2c4

    #@2c0
    .line 472
    :cond_2c0
    const/16 v38, 0x3

    #@2c2
    goto/16 :goto_23b

    #@2c4
    .line 474
    :cond_2c4
    move-object/from16 v0, p0

    #@2c6
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mConnMgr:Landroid/net/ConnectivityManager;

    #@2c8
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@2cb
    move-result v2

    #@2cc
    const/4 v3, 0x1

    #@2cd
    if-ne v2, v3, :cond_2d3

    #@2cf
    .line 475
    const/16 v38, 0x1

    #@2d1
    goto/16 :goto_23b

    #@2d3
    .line 477
    :cond_2d3
    const/16 v38, 0x2

    #@2d5
    goto/16 :goto_23b

    #@2d7
    .line 492
    .end local v13           #addPopupResult:Z
    .end local v31           #isDataRoaming:Z
    .end local v33           #isRoaming:Z
    .end local v35           #mode:I
    .end local v38           #network_mode:I
    :pswitch_2d7
    const-string v2, "[LGE_DATA][DCM] "

    #@2d9
    const-string v3, "setBlockPacketMenuProcess()"

    #@2db
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2de
    .line 493
    const/4 v2, 0x1

    #@2df
    move/from16 v0, p3

    #@2e1
    if-ne v0, v2, :cond_303

    #@2e3
    const/16 v16, 0x1

    #@2e5
    .line 494
    .local v16, block:Z
    :goto_2e5
    sput-boolean v16, Lcom/android/internal/telephony/DataConnectionManager;->blockPacketMenuFlag:Z

    #@2e7
    .line 495
    const-string v2, "[LGE_DATA][DCM] "

    #@2e9
    new-instance v3, Ljava/lang/StringBuilder;

    #@2eb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2ee
    const-string v4, " <setBlockPacketMenuProcess()> blockPacketMenuFlag = "

    #@2f0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f3
    move-result-object v3

    #@2f4
    sget-boolean v4, Lcom/android/internal/telephony/DataConnectionManager;->blockPacketMenuFlag:Z

    #@2f6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2f9
    move-result-object v3

    #@2fa
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2fd
    move-result-object v3

    #@2fe
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@301
    goto/16 :goto_e

    #@303
    .line 493
    .end local v16           #block:Z
    :cond_303
    const/16 v16, 0x0

    #@305
    goto :goto_2e5

    #@306
    .line 501
    :pswitch_306
    const-string v2, "[LGE_DATA][DCM] "

    #@308
    const-string v3, "setAlreadyAppUsedPacket()"

    #@30a
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@30d
    .line 502
    const/4 v2, 0x1

    #@30e
    move/from16 v0, p3

    #@310
    if-ne v0, v2, :cond_332

    #@312
    const/16 v52, 0x1

    #@314
    .line 503
    .local v52, used:Z
    :goto_314
    sput-boolean v52, Lcom/android/internal/telephony/DataConnectionManager;->alreadyAppUsedPacket:Z

    #@316
    .line 504
    const-string v2, "[LGE_DATA][DCM] "

    #@318
    new-instance v3, Ljava/lang/StringBuilder;

    #@31a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31d
    const-string v4, " <setAlreadyAppUsedPacket()> alreadyAppUsedPacket = "

    #@31f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@322
    move-result-object v3

    #@323
    sget-boolean v4, Lcom/android/internal/telephony/DataConnectionManager;->alreadyAppUsedPacket:Z

    #@325
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@328
    move-result-object v3

    #@329
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32c
    move-result-object v3

    #@32d
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@330
    goto/16 :goto_e

    #@332
    .line 502
    .end local v52           #used:Z
    :cond_332
    const/16 v52, 0x0

    #@334
    goto :goto_314

    #@335
    .line 510
    :pswitch_335
    const-string v2, "[LGE_DATA][DCM] "

    #@337
    const-string v3, "functionForPacketList()"

    #@339
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@33c
    .line 511
    move-object/from16 v0, p0

    #@33e
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->featureset:Ljava/lang/String;

    #@340
    const-string v3, "SKTBASE"

    #@342
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@345
    move-result v2

    #@346
    if-nez v2, :cond_351

    #@348
    .line 513
    const-string v2, "[LGE_DATA][DCM] "

    #@34a
    const-string v3, "[LGE_DATA] return ~~~~ !!! for not SKT "

    #@34c
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@34f
    goto/16 :goto_e

    #@351
    .line 517
    :cond_351
    const/16 v47, 0x0

    #@353
    .line 519
    .local v47, ret_value:Z
    const-string v2, "network_management"

    #@355
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@358
    move-result-object v37

    #@359
    .line 520
    .local v37, network_b:Landroid/os/IBinder;
    invoke-static/range {v37 .. v37}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@35c
    move-result-object v39

    #@35d
    .line 522
    .local v39, network_service:Landroid/os/INetworkManagementService;
    if-eqz v39, :cond_e

    #@35f
    .line 525
    const-string v2, "[LGE_DATA][DCM] "

    #@361
    const-string v3, "functionForPacketList  :::: "

    #@363
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_366
    .catchall {:try_start_1c0 .. :try_end_366} :catchall_f1

    #@366
    .line 527
    :try_start_366
    invoke-interface/range {v39 .. v39}, Landroid/os/INetworkManagementService;->packetList_Indrop()Z
    :try_end_369
    .catchall {:try_start_366 .. :try_end_369} :catchall_f1
    .catch Ljava/lang/Exception; {:try_start_366 .. :try_end_369} :catch_386

    #@369
    move-result v47

    #@36a
    .line 532
    :goto_36a
    :try_start_36a
    const-string v2, "[LGE_DATA][DCM] "

    #@36c
    new-instance v3, Ljava/lang/StringBuilder;

    #@36e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@371
    const-string v4, "functionForPacketList  ret_value :: :"

    #@373
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@376
    move-result-object v3

    #@377
    move/from16 v0, v47

    #@379
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@37c
    move-result-object v3

    #@37d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@380
    move-result-object v3

    #@381
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@384
    goto/16 :goto_e

    #@386
    .line 528
    :catch_386
    move-exception v20

    #@387
    .line 529
    .restart local v20       #e:Ljava/lang/Exception;
    const-string v2, "[LGE_DATA][DCM] "

    #@389
    new-instance v3, Ljava/lang/StringBuilder;

    #@38b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@38e
    const-string v4, "network_service.resetPacketDrop exception = "

    #@390
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@393
    move-result-object v3

    #@394
    move-object/from16 v0, v20

    #@396
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@399
    move-result-object v3

    #@39a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39d
    move-result-object v3

    #@39e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a1
    goto :goto_36a

    #@3a2
    .line 539
    .end local v20           #e:Ljava/lang/Exception;
    .end local v37           #network_b:Landroid/os/IBinder;
    .end local v39           #network_service:Landroid/os/INetworkManagementService;
    .end local v47           #ret_value:Z
    :pswitch_3a2
    const-string v2, "[LGE_DATA][DCM] "

    #@3a4
    const-string v3, "getRouteList_debug()"

    #@3a6
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3a9
    .line 540
    move-object/from16 v0, p0

    #@3ab
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->featureset:Ljava/lang/String;

    #@3ad
    const-string v3, "SKTBASE"

    #@3af
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@3b2
    move-result v2

    #@3b3
    if-nez v2, :cond_3be

    #@3b5
    .line 542
    const-string v2, "[LGE_DATA][DCM] "

    #@3b7
    const-string v3, "[LGE_DATA] return ~~~~ !!! for not SKT "

    #@3b9
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3bc
    goto/16 :goto_e

    #@3be
    .line 545
    :cond_3be
    const/16 v47, 0x0

    #@3c0
    .line 548
    .restart local v47       #ret_value:Z
    const-string v2, "network_management"

    #@3c2
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@3c5
    move-result-object v37

    #@3c6
    .line 549
    .restart local v37       #network_b:Landroid/os/IBinder;
    invoke-static/range {v37 .. v37}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@3c9
    move-result-object v39

    #@3ca
    .line 550
    .restart local v39       #network_service:Landroid/os/INetworkManagementService;
    const-string v2, "connectivity"

    #@3cc
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@3cf
    move-result-object v15

    #@3d0
    .line 551
    .local v15, bc:Landroid/os/IBinder;
    invoke-static {v15}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;
    :try_end_3d3
    .catchall {:try_start_36a .. :try_end_3d3} :catchall_f1

    #@3d3
    move-result-object v18

    #@3d4
    .line 553
    .local v18, cm:Landroid/net/IConnectivityManager;
    if-eqz v39, :cond_e

    #@3d6
    if-eqz v18, :cond_e

    #@3d8
    .line 556
    const/16 v29, 0x0

    #@3da
    .line 557
    .local v29, ifacename:Ljava/lang/String;
    const/16 v44, 0x0

    #@3dc
    .line 560
    .local v44, props:Landroid/net/LinkProperties;
    const/4 v2, 0x0

    #@3dd
    :try_start_3dd
    move-object/from16 v0, v18

    #@3df
    invoke-interface {v0, v2}, Landroid/net/IConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@3e2
    move-result-object v44

    #@3e3
    .line 561
    invoke-virtual/range {v44 .. v44}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
    :try_end_3e6
    .catchall {:try_start_3dd .. :try_end_3e6} :catchall_f1
    .catch Landroid/os/RemoteException; {:try_start_3dd .. :try_end_3e6} :catch_837

    #@3e6
    move-result-object v29

    #@3e7
    .line 566
    :goto_3e7
    :try_start_3e7
    const-string v2, "[LGE_DATA][DCM] "

    #@3e9
    const-string v3, "getRouteList_debug\t:::: "

    #@3eb
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3ee
    .catchall {:try_start_3e7 .. :try_end_3ee} :catchall_f1

    #@3ee
    .line 568
    :try_start_3ee
    move-object/from16 v0, v39

    #@3f0
    move-object/from16 v1, v29

    #@3f2
    invoke-interface {v0, v1}, Landroid/os/INetworkManagementService;->getRouteList_debug(Ljava/lang/String;)V
    :try_end_3f5
    .catchall {:try_start_3ee .. :try_end_3f5} :catchall_f1
    .catch Ljava/lang/Exception; {:try_start_3ee .. :try_end_3f5} :catch_411

    #@3f5
    .line 574
    :goto_3f5
    :try_start_3f5
    const-string v2, "[LGE_DATA][DCM] "

    #@3f7
    new-instance v3, Ljava/lang/StringBuilder;

    #@3f9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3fc
    const-string v4, "getRouteList_debug\tret_value :: :"

    #@3fe
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@401
    move-result-object v3

    #@402
    move/from16 v0, v47

    #@404
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@407
    move-result-object v3

    #@408
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40b
    move-result-object v3

    #@40c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40f
    goto/16 :goto_e

    #@411
    .line 570
    :catch_411
    move-exception v20

    #@412
    .line 571
    .restart local v20       #e:Ljava/lang/Exception;
    const-string v2, "[LGE_DATA][DCM] "

    #@414
    new-instance v3, Ljava/lang/StringBuilder;

    #@416
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@419
    const-string v4, "getRouteList_debug exception = "

    #@41b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41e
    move-result-object v3

    #@41f
    move-object/from16 v0, v20

    #@421
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@424
    move-result-object v3

    #@425
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@428
    move-result-object v3

    #@429
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@42c
    goto :goto_3f5

    #@42d
    .line 580
    .end local v15           #bc:Landroid/os/IBinder;
    .end local v18           #cm:Landroid/net/IConnectivityManager;
    .end local v20           #e:Ljava/lang/Exception;
    .end local v29           #ifacename:Ljava/lang/String;
    .end local v37           #network_b:Landroid/os/IBinder;
    .end local v39           #network_service:Landroid/os/INetworkManagementService;
    .end local v44           #props:Landroid/net/LinkProperties;
    .end local v47           #ret_value:Z
    :pswitch_42d
    const-string v2, "[LGE_DATA][DCM] "

    #@42f
    const-string v3, "handleSKT_QA()"

    #@431
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@434
    .line 582
    move/from16 v46, p3

    #@436
    .line 584
    .local v46, releaseCause:I
    move-object/from16 v0, p0

    #@438
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@43a
    const-string v3, "connectivity"

    #@43c
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@43f
    move-result-object v18

    #@440
    check-cast v18, Landroid/net/ConnectivityManager;

    #@442
    .line 587
    .local v18, cm:Landroid/net/ConnectivityManager;
    const-string v2, "[LGE_DATA][DCM] "

    #@444
    new-instance v3, Ljava/lang/StringBuilder;

    #@446
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@449
    const-string v4, "handleSKT_QA :  releaseCause : "

    #@44b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44e
    move-result-object v3

    #@44f
    move/from16 v0, v46

    #@451
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@454
    move-result-object v3

    #@455
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@458
    move-result-object v3

    #@459
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@45c
    .line 604
    new-instance v11, Landroid/content/Intent;

    #@45e
    const-string v2, "android.skt.intent.action.USER_BACKG_SETTING"

    #@460
    invoke-direct {v11, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@463
    .line 606
    .local v11, DataDisabledIntent:Landroid/content/Intent;
    const/4 v2, 0x2

    #@464
    move/from16 v0, v46

    #@466
    if-ne v0, v2, :cond_4cd

    #@468
    .line 608
    const-string v2, "on_off"

    #@46a
    const/4 v3, 0x1

    #@46b
    invoke-virtual {v11, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@46e
    .line 609
    move-object/from16 v0, p0

    #@470
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@472
    invoke-virtual {v2, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@475
    .line 611
    const-string v2, "[LGE_DATA][DCM] "

    #@477
    const-string v3, "handleSKT_QA : Limit_Background_data_Notification."

    #@479
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@47c
    .line 613
    new-instance v10, Landroid/content/Intent;

    #@47e
    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    #@481
    .line 615
    .local v10, mintent:Landroid/content/Intent;
    new-instance v2, Landroid/app/Notification;

    #@483
    move-object/from16 v0, p0

    #@485
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@487
    const v4, 0x108008a

    #@48a
    const/4 v5, 0x0

    #@48b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@48e
    move-result-wide v6

    #@48f
    move-object/from16 v0, p0

    #@491
    iget-object v8, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@493
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@496
    move-result-object v8

    #@497
    const v9, 0x2090280

    #@49a
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@49d
    move-result-object v8

    #@49e
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4a1
    move-result-object v8

    #@4a2
    move-object/from16 v0, p0

    #@4a4
    iget-object v9, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@4a6
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4a9
    move-result-object v9

    #@4aa
    const v55, 0x2090281

    #@4ad
    move/from16 v0, v55

    #@4af
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@4b2
    move-result-object v9

    #@4b3
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4b6
    move-result-object v9

    #@4b7
    invoke-direct/range {v2 .. v10}, Landroid/app/Notification;-><init>(Landroid/content/Context;ILjava/lang/CharSequence;JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    #@4ba
    move-object/from16 v0, p0

    #@4bc
    iput-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mNotification:Landroid/app/Notification;

    #@4be
    .line 624
    move-object/from16 v0, p0

    #@4c0
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mNotificationMgr:Landroid/app/NotificationManager;

    #@4c2
    const/16 v3, 0xa

    #@4c4
    move-object/from16 v0, p0

    #@4c6
    iget-object v4, v0, Lcom/android/internal/telephony/DataConnectionManager;->mNotification:Landroid/app/Notification;

    #@4c8
    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@4cb
    goto/16 :goto_e

    #@4cd
    .line 628
    .end local v10           #mintent:Landroid/content/Intent;
    :cond_4cd
    const/16 v21, 0x0

    #@4cf
    .line 629
    .local v21, enabled:Z
    const/4 v12, 0x0

    #@4d0
    .line 643
    .local v12, IsBackgroundRestricted:Z
    move-object/from16 v0, p0

    #@4d2
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@4d4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4d7
    move-result-object v2

    #@4d8
    const-string v3, "data_network_user_background_setting_data"

    #@4da
    const/4 v4, 0x0

    #@4db
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@4de
    move-result v2

    #@4df
    const/4 v3, 0x1

    #@4e0
    if-lt v2, v3, :cond_518

    #@4e2
    .line 644
    const/16 v21, 0x1

    #@4e4
    .line 648
    :goto_4e4
    const-string v2, "[LGE_DATA][DCM] "

    #@4e6
    new-instance v3, Ljava/lang/StringBuilder;

    #@4e8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4eb
    const-string v4, "handleSKT_QA : enabled : "

    #@4ed
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f0
    move-result-object v3

    #@4f1
    move/from16 v0, v21

    #@4f3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4f6
    move-result-object v3

    #@4f7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4fa
    move-result-object v3

    #@4fb
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4fe
    .line 651
    const-string v3, "on_off"

    #@500
    if-nez v21, :cond_51b

    #@502
    const/4 v2, 0x1

    #@503
    :goto_503
    invoke-virtual {v11, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@506
    .line 652
    move-object/from16 v0, p0

    #@508
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@50a
    invoke-virtual {v2, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@50d
    .line 654
    move-object/from16 v0, p0

    #@50f
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mNotificationMgr:Landroid/app/NotificationManager;

    #@511
    const/16 v3, 0xa

    #@513
    invoke-virtual {v2, v3}, Landroid/app/NotificationManager;->cancel(I)V

    #@516
    goto/16 :goto_e

    #@518
    .line 646
    :cond_518
    const/16 v21, 0x0

    #@51a
    goto :goto_4e4

    #@51b
    .line 651
    :cond_51b
    const/4 v2, 0x0

    #@51c
    goto :goto_503

    #@51d
    .line 663
    .end local v11           #DataDisabledIntent:Landroid/content/Intent;
    .end local v12           #IsBackgroundRestricted:Z
    .end local v18           #cm:Landroid/net/ConnectivityManager;
    .end local v21           #enabled:Z
    .end local v46           #releaseCause:I
    :pswitch_51d
    const-string v2, "[LGE_DATA][DCM] "

    #@51f
    const-string v3, "debugFileWrite()"

    #@521
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@524
    .line 665
    const-string v24, "DebugFile.txt"

    #@526
    .line 666
    .restart local v24       #fileName:Ljava/lang/String;
    new-instance v48, Ljava/lang/StringBuffer;

    #@528
    const-string v2, ""

    #@52a
    move-object/from16 v0, v48

    #@52c
    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@52f
    .line 667
    .local v48, sText:Ljava/lang/StringBuffer;
    move/from16 v30, p3

    #@531
    .line 669
    .restart local v30       #inputFileSize:I
    new-instance v22, Ljava/io/File;

    #@533
    move-object/from16 v0, v22

    #@535
    move-object/from16 v1, v24

    #@537
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@53a
    .line 673
    .restart local v22       #file:Ljava/io/File;
    new-instance v41, Ljava/util/Date;

    #@53c
    invoke-direct/range {v41 .. v41}, Ljava/util/Date;-><init>()V

    #@53f
    .line 675
    .restart local v41       #now:Ljava/util/Date;
    new-instance v49, Ljava/text/SimpleDateFormat;

    #@541
    const-string v2, "yyyy.MM.dd HH:mm:ss"

    #@543
    move-object/from16 v0, v49

    #@545
    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@548
    .line 676
    .restart local v49       #simpleformat:Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/lang/StringBuilder;

    #@54a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@54d
    move-object/from16 v0, v49

    #@54f
    move-object/from16 v1, v41

    #@551
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@554
    move-result-object v3

    #@555
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@558
    move-result-object v2

    #@559
    const-string v3, " "

    #@55b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55e
    move-result-object v2

    #@55f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@562
    move-result-object v51

    #@563
    .line 678
    .restart local v51       #time:Ljava/lang/String;
    move-object/from16 v0, v48

    #@565
    move-object/from16 v1, v51

    #@567
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@56a
    .line 679
    new-instance v2, Ljava/lang/StringBuilder;

    #@56c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@56f
    move-object/from16 v0, p2

    #@571
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@574
    move-result-object v2

    #@575
    const-string v3, "\n"

    #@577
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57a
    move-result-object v2

    #@57b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57e
    move-result-object v2

    #@57f
    move-object/from16 v0, v48

    #@581
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_584
    .catchall {:try_start_3f5 .. :try_end_584} :catchall_f1

    #@584
    .line 682
    const-wide/16 v25, 0x0

    #@586
    .line 686
    .local v25, fileSize:J
    :try_start_586
    new-instance v45, Ljava/io/File;

    #@588
    const-string v2, "data/data/com.android.phone/files/DebugFile.txt"

    #@58a
    move-object/from16 v0, v45

    #@58c
    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@58f
    .line 687
    .local v45, readFile:Ljava/io/File;
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->length()J
    :try_end_592
    .catchall {:try_start_586 .. :try_end_592} :catchall_f1
    .catch Ljava/lang/Exception; {:try_start_586 .. :try_end_592} :catch_60d

    #@592
    move-result-wide v25

    #@593
    .line 697
    .end local v45           #readFile:Ljava/io/File;
    :goto_593
    :try_start_593
    new-instance v23, Ljava/lang/StringBuffer;

    #@595
    const-string v2, ""

    #@597
    move-object/from16 v0, v23

    #@599
    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@59c
    .line 698
    .local v23, fileContent:Ljava/lang/StringBuffer;
    new-instance v54, Ljava/lang/StringBuffer;

    #@59e
    const-string v2, ""

    #@5a0
    move-object/from16 v0, v54

    #@5a2
    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V
    :try_end_5a5
    .catchall {:try_start_593 .. :try_end_5a5} :catchall_f1

    #@5a5
    .line 701
    .local v54, writeString:Ljava/lang/StringBuffer;
    move/from16 v0, v30

    #@5a7
    mul-int/lit16 v2, v0, 0x400

    #@5a9
    int-to-long v2, v2

    #@5aa
    cmp-long v2, v25, v2

    #@5ac
    if-lez v2, :cond_616

    #@5ae
    .line 703
    :try_start_5ae
    move-object/from16 v0, p0

    #@5b0
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@5b2
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->toString()Ljava/lang/String;

    #@5b5
    move-result-object v3

    #@5b6
    invoke-virtual {v2, v3}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    #@5b9
    move-result-object v27

    #@5ba
    .line 705
    .local v27, fis:Ljava/io/FileInputStream;
    :cond_5ba
    invoke-virtual/range {v27 .. v27}, Ljava/io/FileInputStream;->read()I

    #@5bd
    move-result v42

    #@5be
    .local v42, ouputByte:I
    const/16 v2, 0xa

    #@5c0
    move/from16 v0, v42

    #@5c2
    if-ne v0, v2, :cond_5ba

    #@5c4
    .line 709
    :goto_5c4
    invoke-virtual/range {v27 .. v27}, Ljava/io/FileInputStream;->read()I

    #@5c7
    move-result v42

    #@5c8
    const/4 v2, -0x1

    #@5c9
    move/from16 v0, v42

    #@5cb
    if-eq v0, v2, :cond_612

    #@5cd
    .line 710
    move/from16 v0, v42

    #@5cf
    int-to-char v2, v0

    #@5d0
    move-object/from16 v0, v23

    #@5d2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_5d5
    .catchall {:try_start_5ae .. :try_end_5d5} :catchall_f1
    .catch Ljava/lang/Exception; {:try_start_5ae .. :try_end_5d5} :catch_5d6

    #@5d5
    goto :goto_5c4

    #@5d6
    .line 713
    .end local v27           #fis:Ljava/io/FileInputStream;
    .end local v42           #ouputByte:I
    :catch_5d6
    move-exception v20

    #@5d7
    .line 715
    .restart local v20       #e:Ljava/lang/Exception;
    :try_start_5d7
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5da
    .catchall {:try_start_5d7 .. :try_end_5da} :catchall_f1

    #@5da
    .line 717
    .end local v20           #e:Ljava/lang/Exception;
    :goto_5da
    move-object/from16 v54, v23

    #@5dc
    .line 739
    :goto_5dc
    :try_start_5dc
    move-object/from16 v0, p0

    #@5de
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@5e0
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->toString()Ljava/lang/String;

    #@5e3
    move-result-object v3

    #@5e4
    const/4 v4, 0x1

    #@5e5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    #@5e8
    move-result-object v28

    #@5e9
    .line 741
    .restart local v28       #fos:Ljava/io/FileOutputStream;
    move-object/from16 v0, v54

    #@5eb
    move-object/from16 v1, v48

    #@5ed
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    #@5f0
    .line 742
    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@5f3
    move-result-object v2

    #@5f4
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    #@5f7
    move-result-object v14

    #@5f8
    .line 743
    .restart local v14       #bText:[B
    const/4 v2, 0x0

    #@5f9
    array-length v3, v14

    #@5fa
    move-object/from16 v0, v28

    #@5fc
    invoke-virtual {v0, v14, v2, v3}, Ljava/io/FileOutputStream;->write([BII)V

    #@5ff
    .line 744
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileOutputStream;->flush()V

    #@602
    .line 745
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileOutputStream;->close()V
    :try_end_605
    .catchall {:try_start_5dc .. :try_end_605} :catchall_f1
    .catch Ljava/lang/Exception; {:try_start_5dc .. :try_end_605} :catch_607

    #@605
    goto/16 :goto_e

    #@607
    .line 746
    .end local v14           #bText:[B
    .end local v28           #fos:Ljava/io/FileOutputStream;
    :catch_607
    move-exception v20

    #@608
    .line 749
    .restart local v20       #e:Ljava/lang/Exception;
    :try_start_608
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    #@60b
    goto/16 :goto_e

    #@60d
    .line 690
    .end local v20           #e:Ljava/lang/Exception;
    .end local v23           #fileContent:Ljava/lang/StringBuffer;
    .end local v54           #writeString:Ljava/lang/StringBuffer;
    :catch_60d
    move-exception v20

    #@60e
    .line 691
    .restart local v20       #e:Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_611
    .catchall {:try_start_608 .. :try_end_611} :catchall_f1

    #@611
    goto :goto_593

    #@612
    .line 712
    .end local v20           #e:Ljava/lang/Exception;
    .restart local v23       #fileContent:Ljava/lang/StringBuffer;
    .restart local v27       #fis:Ljava/io/FileInputStream;
    .restart local v42       #ouputByte:I
    .restart local v54       #writeString:Ljava/lang/StringBuffer;
    :cond_612
    :try_start_612
    invoke-virtual/range {v27 .. v27}, Ljava/io/FileInputStream;->close()V
    :try_end_615
    .catchall {:try_start_612 .. :try_end_615} :catchall_f1
    .catch Ljava/lang/Exception; {:try_start_612 .. :try_end_615} :catch_5d6

    #@615
    goto :goto_5da

    #@616
    .line 723
    .end local v27           #fis:Ljava/io/FileInputStream;
    .end local v42           #ouputByte:I
    :cond_616
    :try_start_616
    move-object/from16 v0, p0

    #@618
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@61a
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->toString()Ljava/lang/String;

    #@61d
    move-result-object v3

    #@61e
    invoke-virtual {v2, v3}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    #@621
    move-result-object v27

    #@622
    .line 725
    .restart local v27       #fis:Ljava/io/FileInputStream;
    :goto_622
    invoke-virtual/range {v27 .. v27}, Ljava/io/FileInputStream;->read()I

    #@625
    move-result v42

    #@626
    .restart local v42       #ouputByte:I
    const/4 v2, -0x1

    #@627
    move/from16 v0, v42

    #@629
    if-eq v0, v2, :cond_63b

    #@62b
    .line 726
    move/from16 v0, v42

    #@62d
    int-to-char v2, v0

    #@62e
    move-object/from16 v0, v23

    #@630
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_633
    .catchall {:try_start_616 .. :try_end_633} :catchall_f1
    .catch Ljava/lang/Exception; {:try_start_616 .. :try_end_633} :catch_634

    #@633
    goto :goto_622

    #@634
    .line 729
    .end local v27           #fis:Ljava/io/FileInputStream;
    .end local v42           #ouputByte:I
    :catch_634
    move-exception v20

    #@635
    .line 731
    .restart local v20       #e:Ljava/lang/Exception;
    :try_start_635
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_638
    .catchall {:try_start_635 .. :try_end_638} :catchall_f1

    #@638
    .line 734
    .end local v20           #e:Ljava/lang/Exception;
    :goto_638
    move-object/from16 v54, v23

    #@63a
    goto :goto_5dc

    #@63b
    .line 728
    .restart local v27       #fis:Ljava/io/FileInputStream;
    .restart local v42       #ouputByte:I
    :cond_63b
    :try_start_63b
    invoke-virtual/range {v27 .. v27}, Ljava/io/FileInputStream;->close()V
    :try_end_63e
    .catchall {:try_start_63b .. :try_end_63e} :catchall_f1
    .catch Ljava/lang/Exception; {:try_start_63b .. :try_end_63e} :catch_634

    #@63e
    goto :goto_638

    #@63f
    .line 758
    .end local v22           #file:Ljava/io/File;
    .end local v23           #fileContent:Ljava/lang/StringBuffer;
    .end local v24           #fileName:Ljava/lang/String;
    .end local v25           #fileSize:J
    .end local v27           #fis:Ljava/io/FileInputStream;
    .end local v30           #inputFileSize:I
    .end local v41           #now:Ljava/util/Date;
    .end local v42           #ouputByte:I
    .end local v48           #sText:Ljava/lang/StringBuffer;
    .end local v49           #simpleformat:Ljava/text/SimpleDateFormat;
    .end local v51           #time:Ljava/lang/String;
    .end local v54           #writeString:Ljava/lang/StringBuffer;
    :pswitch_63f
    move/from16 v36, p3

    #@641
    .line 759
    .local v36, networkType:I
    :try_start_641
    const-string v2, "true"

    #@643
    move-object/from16 v0, p2

    #@645
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_648
    .catchall {:try_start_641 .. :try_end_648} :catchall_f1

    #@648
    move-result v34

    #@649
    .line 763
    .local v34, isTeardownRequested:Z
    :try_start_649
    move-object/from16 v0, p0

    #@64b
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@64d
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@650
    move-result-object v2

    #@651
    const-string v3, "data_roaming"

    #@653
    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_656
    .catchall {:try_start_649 .. :try_end_656} :catchall_f1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_649 .. :try_end_656} :catch_6ef

    #@656
    move-result v19

    #@657
    .line 768
    .local v19, data_roaming:I
    :try_start_657
    move-object/from16 v0, p0

    #@659
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mConnMgr:Landroid/net/ConnectivityManager;

    #@65b
    move/from16 v0, v36

    #@65d
    invoke-virtual {v2, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@660
    move-result-object v40

    #@661
    .line 770
    .local v40, ni:Landroid/net/NetworkInfo;
    const-string v2, "[LGE_DATA][DCM] "

    #@663
    new-instance v3, Ljava/lang/StringBuilder;

    #@665
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@668
    const-string v4, "[LGE_DATA-KAF] data_roaming ="

    #@66a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66d
    move-result-object v3

    #@66e
    move/from16 v0, v19

    #@670
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@673
    move-result-object v3

    #@674
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@677
    move-result-object v3

    #@678
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@67b
    .line 771
    const-string v2, "[LGE_DATA][DCM] "

    #@67d
    new-instance v3, Ljava/lang/StringBuilder;

    #@67f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@682
    const-string v4, "[LGE_DATA-KAF] gsm.operator.isroaming ="

    #@684
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@687
    move-result-object v3

    #@688
    const-string v4, "gsm.operator.isroaming"

    #@68a
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@68d
    move-result-object v4

    #@68e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@691
    move-result-object v3

    #@692
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@695
    move-result-object v3

    #@696
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@699
    .line 774
    const-string v2, "[LGE_DATA][DCM] "

    #@69b
    new-instance v3, Ljava/lang/StringBuilder;

    #@69d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6a0
    const-string v4, "[LGE_DATA-KAF] data_restrict_mode ="

    #@6a2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a5
    move-result-object v3

    #@6a6
    sget-object v4, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@6a8
    const-string v5, ""

    #@6aa
    const/4 v6, 0x0

    #@6ab
    move-object/from16 v0, p0

    #@6ad
    invoke-virtual {v0, v4, v5, v6}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@6b0
    move-result v4

    #@6b1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b4
    move-result-object v3

    #@6b5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b8
    move-result-object v3

    #@6b9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6bc
    .line 777
    if-eqz v40, :cond_6da

    #@6be
    const-string v2, "[LGE_DATA][DCM] "

    #@6c0
    new-instance v3, Ljava/lang/StringBuilder;

    #@6c2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6c5
    const-string v4, "[LGE_DATA-KAF] ni.isAvailable() = "

    #@6c7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6ca
    move-result-object v3

    #@6cb
    invoke-virtual/range {v40 .. v40}, Landroid/net/NetworkInfo;->isAvailable()Z

    #@6ce
    move-result v4

    #@6cf
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6d2
    move-result-object v3

    #@6d3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d6
    move-result-object v3

    #@6d7
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6da
    .line 780
    :cond_6da
    const-string v2, "true"

    #@6dc
    const-string v3, "gsm.operator.isroaming"

    #@6de
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6e1
    move-result-object v3

    #@6e2
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e5
    move-result v2

    #@6e6
    const/4 v3, 0x1

    #@6e7
    if-ne v2, v3, :cond_6f4

    #@6e9
    .line 781
    if-gtz v19, :cond_706

    #@6eb
    .line 782
    const/16 v53, -0x3

    #@6ed
    goto/16 :goto_e

    #@6ef
    .line 764
    .end local v19           #data_roaming:I
    .end local v40           #ni:Landroid/net/NetworkInfo;
    :catch_6ef
    move-exception v50

    #@6f0
    .line 766
    .restart local v50       #snfe:Landroid/provider/Settings$SettingNotFoundException;
    const/16 v53, -0x1

    #@6f2
    goto/16 :goto_e

    #@6f4
    .line 786
    .end local v50           #snfe:Landroid/provider/Settings$SettingNotFoundException;
    .restart local v19       #data_roaming:I
    .restart local v40       #ni:Landroid/net/NetworkInfo;
    :cond_6f4
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@6f6
    const-string v3, ""

    #@6f8
    const/4 v4, 0x0

    #@6f9
    move-object/from16 v0, p0

    #@6fb
    invoke-virtual {v0, v2, v3, v4}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@6fe
    move-result v2

    #@6ff
    const/4 v3, 0x2

    #@700
    if-ne v2, v3, :cond_706

    #@702
    .line 787
    const/16 v53, -0x2

    #@704
    goto/16 :goto_e

    #@706
    .line 791
    :cond_706
    if-eqz v40, :cond_73f

    #@708
    .line 792
    invoke-virtual/range {v40 .. v40}, Landroid/net/NetworkInfo;->isAvailable()Z

    #@70b
    move-result v2

    #@70c
    if-nez v2, :cond_719

    #@70e
    .line 793
    const-string v2, "[LGE_DATA][DCM] "

    #@710
    const-string v3, "special network not available"

    #@712
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@715
    .line 794
    const/16 v53, -0x1

    #@717
    goto/16 :goto_e

    #@719
    .line 797
    :cond_719
    invoke-virtual/range {v40 .. v40}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    #@71c
    move-result v2

    #@71d
    const/4 v3, 0x1

    #@71e
    if-ne v2, v3, :cond_73f

    #@720
    if-nez v34, :cond_73f

    #@722
    .line 798
    invoke-virtual/range {v40 .. v40}, Landroid/net/NetworkInfo;->isConnected()Z

    #@725
    move-result v2

    #@726
    const/4 v3, 0x1

    #@727
    if-ne v2, v3, :cond_734

    #@729
    .line 799
    const-string v2, "[LGE_DATA][DCM] "

    #@72b
    const-string v3, "special network already active"

    #@72d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@730
    .line 800
    const/16 v53, 0x0

    #@732
    goto/16 :goto_e

    #@734
    .line 803
    :cond_734
    const-string v2, "[LGE_DATA][DCM] "

    #@736
    const-string v3, "special network already connecting"

    #@738
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@73b
    .line 804
    const/16 v53, 0x1

    #@73d
    goto/16 :goto_e

    #@73f
    .line 808
    :cond_73f
    const-string v2, "[LGE_DATA][DCM] "

    #@741
    const-string v3, "special network is not connected"

    #@743
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@746
    .line 810
    const/16 v53, -0x1

    #@748
    goto/16 :goto_e

    #@74a
    .line 818
    .end local v19           #data_roaming:I
    .end local v34           #isTeardownRequested:Z
    .end local v36           #networkType:I
    .end local v40           #ni:Landroid/net/NetworkInfo;
    :pswitch_74a
    const/16 v32, 0x1

    #@74c
    .line 820
    .local v32, isMobileDataEnabled:Z
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@74e
    const-string v3, ""

    #@750
    const/4 v4, 0x0

    #@751
    move-object/from16 v0, p0

    #@753
    invoke-virtual {v0, v2, v3, v4}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@756
    move-result v2

    #@757
    const/4 v3, 0x2

    #@758
    if-ne v2, v3, :cond_75c

    #@75a
    .line 822
    const/16 v32, 0x0

    #@75c
    .line 824
    :cond_75c
    const-string v2, "[LGE_DATA][DCM] "

    #@75e
    new-instance v3, Ljava/lang/StringBuilder;

    #@760
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@763
    const-string v4, "[LGE_DATA] <startUsingNetworkFeatureId()> isMobileDataAllowed : "

    #@765
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@768
    move-result-object v3

    #@769
    move/from16 v0, v32

    #@76b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@76e
    move-result-object v3

    #@76f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@772
    move-result-object v3

    #@773
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@776
    .line 826
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionManager;->isNetworkRoaming()Z

    #@779
    move-result v2

    #@77a
    const/4 v3, 0x1

    #@77b
    if-ne v2, v3, :cond_7a9

    #@77d
    .line 828
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionManager;->isAllowRoaming()Z

    #@780
    move-result v2

    #@781
    if-nez v2, :cond_7a0

    #@783
    .line 830
    const-string v2, "[LGE_DATA][DCM] "

    #@785
    const-string v3, "[LGE_DATA] <startUsingNetworkFeatureId()> Roaming : return."

    #@787
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78a
    .line 832
    new-instance v17, Landroid/content/Intent;

    #@78c
    const-string v2, "android.net.conn.STARTING_IN_ROAM_SETTING_DISABLE"

    #@78e
    move-object/from16 v0, v17

    #@790
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@793
    .line 833
    .local v17, broadcast:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@795
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@797
    move-object/from16 v0, v17

    #@799
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@79c
    .line 835
    const/16 v53, 0x2

    #@79e
    goto/16 :goto_e

    #@7a0
    .line 838
    .end local v17           #broadcast:Landroid/content/Intent;
    :cond_7a0
    const/16 v32, 0x1

    #@7a2
    .line 839
    const-string v2, "[LGE_DATA][DCM] "

    #@7a4
    const-string v3, "[LGE_DATA] <startUsingNetworkFeatureId()> Roaming : pass."

    #@7a6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a9
    .line 842
    :cond_7a9
    if-nez v32, :cond_e

    #@7ab
    .line 844
    const-string v2, "[LGE_DATA][DCM] "

    #@7ad
    const-string v3, "[LGE_DATA] <startUsingNetworkFeatureId()> Mobile Data is NOT enabled."

    #@7af
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7b2
    .line 845
    const-string v2, "[LGE_DATA][DCM] "

    #@7b4
    new-instance v3, Ljava/lang/StringBuilder;

    #@7b6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7b9
    const-string v4, "[LGE_DATA] isNetworkSKT_formccmnc = "

    #@7bb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7be
    move-result-object v3

    #@7bf
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionManager;->isNetworkSKT_formccmnc()Z

    #@7c2
    move-result v4

    #@7c3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7c6
    move-result-object v3

    #@7c7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7ca
    move-result-object v3

    #@7cb
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7ce
    .line 847
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionManager;->isNetworkSKT_formccmnc()Z

    #@7d1
    move-result v2

    #@7d2
    if-eqz v2, :cond_7de

    #@7d4
    const-string v2, "enableMMS"

    #@7d6
    move-object/from16 v0, p2

    #@7d8
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7db
    move-result v2

    #@7dc
    if-nez v2, :cond_e

    #@7de
    .line 849
    :cond_7de
    new-instance v17, Landroid/content/Intent;

    #@7e0
    const-string v2, "android.net.conn.STARTING_IN_DATA_SETTING_DISABLE_3GONLY"

    #@7e2
    move-object/from16 v0, v17

    #@7e4
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7e7
    .line 850
    .restart local v17       #broadcast:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@7e9
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@7eb
    move-object/from16 v0, v17

    #@7ed
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@7f0
    .line 852
    const/16 v53, 0x2

    #@7f2
    goto/16 :goto_e

    #@7f4
    .line 862
    .end local v17           #broadcast:Landroid/content/Intent;
    .end local v32           #isMobileDataEnabled:Z
    :pswitch_7f4
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionManager;->isNetworkRoaming()Z

    #@7f7
    move-result v2

    #@7f8
    const/4 v3, 0x1

    #@7f9
    if-ne v2, v3, :cond_80c

    #@7fb
    .line 864
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/DataConnectionManager;->isAllowRoaming()Z

    #@7fe
    move-result v2

    #@7ff
    if-nez v2, :cond_e

    #@801
    .line 866
    const-string v2, "[LGE_DATA][DCM] "

    #@803
    const-string v3, "[LGE_DATA_U] <startUsingNetworkFeatureId()> Roaming : return."

    #@805
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@808
    .line 867
    const/16 v53, 0x2

    #@80a
    goto/16 :goto_e

    #@80c
    .line 873
    :cond_80c
    move-object/from16 v0, p0

    #@80e
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mConnMgr:Landroid/net/ConnectivityManager;

    #@810
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@813
    move-result v2

    #@814
    if-nez v2, :cond_e

    #@816
    const/16 v2, 0xb

    #@818
    move/from16 v0, p3

    #@81a
    if-eq v0, v2, :cond_e

    #@81c
    move-object/from16 v0, p0

    #@81e
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionManager;->mConnMgr:Landroid/net/ConnectivityManager;

    #@820
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@823
    move-result v2

    #@824
    if-nez v2, :cond_e

    #@826
    const/16 v2, 0x17

    #@828
    move/from16 v0, p3

    #@82a
    if-eq v0, v2, :cond_e

    #@82c
    .line 878
    const-string v2, "[LGE_DATA][DCM] "

    #@82e
    const-string v3, "[LGE_DATA_U] <startUsingNetworkFeatureId()> KOR : return."

    #@830
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_833
    .catchall {:try_start_657 .. :try_end_833} :catchall_f1

    #@833
    .line 879
    const/16 v53, 0x2

    #@835
    goto/16 :goto_e

    #@837
    .line 563
    .restart local v15       #bc:Landroid/os/IBinder;
    .local v18, cm:Landroid/net/IConnectivityManager;
    .restart local v29       #ifacename:Ljava/lang/String;
    .restart local v37       #network_b:Landroid/os/IBinder;
    .restart local v39       #network_service:Landroid/os/INetworkManagementService;
    .restart local v44       #props:Landroid/net/LinkProperties;
    .restart local v47       #ret_value:Z
    :catch_837
    move-exception v2

    #@838
    goto/16 :goto_3e7

    #@83a
    .line 320
    :pswitch_data_83a
    .packed-switch 0x1
        :pswitch_10
        :pswitch_12c
        :pswitch_158
        :pswitch_184
        :pswitch_2d7
        :pswitch_306
        :pswitch_335
        :pswitch_3a2
        :pswitch_42d
        :pswitch_51d
        :pswitch_63f
        :pswitch_74a
        :pswitch_7f4
    .end packed-switch
.end method

.method public SendBroadcastPdpRejectCause(I)V
    .registers 6
    .parameter "cause"

    #@0
    .prologue
    .line 281
    const-string v2, "[LGE_DATA][DCM] "

    #@2
    const-string v3, "[LGE_DATA][PDP_reject] SendBroadcastPdpRejectCause, intent = android.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 284
    new-instance v0, Landroid/content/Intent;

    #@9
    const-string v2, "android.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@b
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e
    .line 285
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "cause"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@13
    .line 286
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@15
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@18
    .line 289
    new-instance v1, Landroid/content/Intent;

    #@1a
    const-string v2, "com.lge.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

    #@1c
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1f
    .line 290
    .local v1, intentLge:Landroid/content/Intent;
    const-string v2, "cause"

    #@21
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@24
    .line 291
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@26
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@29
    .line 292
    return-void
.end method

.method public declared-synchronized functionForPacketDrop(Z)V
    .registers 16
    .parameter "ok"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 193
    monitor-enter p0

    #@3
    :try_start_3
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnectionManager;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@5
    iget-boolean v11, v11, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@7
    if-nez v11, :cond_12

    #@9
    .line 194
    const-string v9, "[LGE_DATA][DCM] "

    #@b
    const-string v10, "[LGE_DATA] return ~~~~ !!! not defined feature "

    #@d
    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_44

    #@10
    .line 277
    :cond_10
    :goto_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 198
    :cond_12
    :try_start_12
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@14
    const-string v12, "phone"

    #@16
    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@19
    move-result-object v8

    #@1a
    check-cast v8, Landroid/telephony/TelephonyManager;

    #@1c
    .line 199
    .local v8, tm:Landroid/telephony/TelephonyManager;
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@1e
    const-string v11, "network_management"

    #@20
    invoke-static {v11}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@23
    move-result-object v0

    #@24
    .line 200
    .local v0, b:Landroid/os/IBinder;
    const-string v11, "connectivity"

    #@26
    invoke-static {v11}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@29
    move-result-object v1

    #@2a
    .line 201
    .local v1, bc:Landroid/os/IBinder;
    invoke-static {v1}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@2d
    move-result-object v2

    #@2e
    .line 203
    .local v2, cm:Landroid/net/IConnectivityManager;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@31
    move-result-object v7

    #@32
    .line 204
    .local v7, service:Landroid/os/INetworkManagementService;
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@35
    move-result-object v5

    #@36
    .line 205
    .local v5, networkOperator:Ljava/lang/String;
    const/4 v4, 0x0

    #@37
    .line 206
    .local v4, ifacename:Ljava/lang/String;
    const/4 v6, 0x0

    #@38
    .line 208
    .local v6, props:Landroid/net/LinkProperties;
    if-eqz v2, :cond_3c

    #@3a
    if-nez v7, :cond_47

    #@3c
    .line 209
    :cond_3c
    const-string v9, "[LGE_DATA][DCM] "

    #@3e
    const-string v10, " <functionForPacketDrop()> cm == null || service == null, so return!!"

    #@40
    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_43
    .catchall {:try_start_12 .. :try_end_43} :catchall_44

    #@43
    goto :goto_10

    #@44
    .line 193
    .end local v0           #b:Landroid/os/IBinder;
    .end local v1           #bc:Landroid/os/IBinder;
    .end local v2           #cm:Landroid/net/IConnectivityManager;
    .end local v4           #ifacename:Ljava/lang/String;
    .end local v5           #networkOperator:Ljava/lang/String;
    .end local v6           #props:Landroid/net/LinkProperties;
    .end local v7           #service:Landroid/os/INetworkManagementService;
    .end local v8           #tm:Landroid/telephony/TelephonyManager;
    :catchall_44
    move-exception v9

    #@45
    monitor-exit p0

    #@46
    throw v9

    #@47
    .line 214
    .restart local v0       #b:Landroid/os/IBinder;
    .restart local v1       #bc:Landroid/os/IBinder;
    .restart local v2       #cm:Landroid/net/IConnectivityManager;
    .restart local v4       #ifacename:Ljava/lang/String;
    .restart local v5       #networkOperator:Ljava/lang/String;
    .restart local v6       #props:Landroid/net/LinkProperties;
    .restart local v7       #service:Landroid/os/INetworkManagementService;
    .restart local v8       #tm:Landroid/telephony/TelephonyManager;
    :cond_47
    const/4 v11, 0x0

    #@48
    :try_start_48
    invoke-interface {v2, v11}, Landroid/net/IConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@4b
    move-result-object v6

    #@4c
    .line 215
    invoke-virtual {v6}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
    :try_end_4f
    .catchall {:try_start_48 .. :try_end_4f} :catchall_44
    .catch Landroid/os/RemoteException; {:try_start_48 .. :try_end_4f} :catch_1c3
    .catch Ljava/lang/Exception; {:try_start_48 .. :try_end_4f} :catch_e4

    #@4f
    move-result-object v4

    #@50
    .line 221
    :goto_50
    :try_start_50
    const-string v11, "[LGE_DATA][DCM] "

    #@52
    new-instance v12, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v13, " <functionForPacketDrop()> blockPacketMenuFlag : "

    #@59
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v12

    #@5d
    sget-boolean v13, Lcom/android/internal/telephony/DataConnectionManager;->blockPacketMenuFlag:Z

    #@5f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@62
    move-result-object v12

    #@63
    const-string v13, " / blockPakcetProcessFlag :  "

    #@65
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v12

    #@69
    sget-boolean v13, Lcom/android/internal/telephony/DataConnectionManager;->blockPakcetProcessFlag:Z

    #@6b
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v12

    #@6f
    const-string v13, " / ok : "

    #@71
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v12

    #@75
    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@78
    move-result-object v12

    #@79
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v12

    #@7d
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 225
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnectionManager;->featureset:Ljava/lang/String;

    #@82
    const-string v12, "KTBASE"

    #@84
    invoke-static {v11, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@87
    move-result v11

    #@88
    if-eqz v11, :cond_b5

    #@8a
    .line 226
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@8c
    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8f
    move-result-object v11

    #@90
    const-string v12, "multi_rab_setting"

    #@92
    const/4 v13, 0x0

    #@93
    invoke-static {v11, v12, v13}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@96
    move-result v11

    #@97
    if-ne v11, v9, :cond_ff

    #@99
    :goto_99
    sput-boolean v9, Lcom/android/internal/telephony/DataConnectionManager;->blockPacketMenuFlag:Z

    #@9b
    .line 227
    const-string v9, "[LGE_DATA][DCM] "

    #@9d
    new-instance v10, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    const-string v11, "functionForPacketDrop  blockPacketMenuFlag : "

    #@a4
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v10

    #@a8
    sget-boolean v11, Lcom/android/internal/telephony/DataConnectionManager;->blockPacketMenuFlag:Z

    #@aa
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v10

    #@ae
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v10

    #@b2
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 232
    :cond_b5
    sget-boolean v9, Lcom/android/internal/telephony/DataConnectionManager;->blockPacketMenuFlag:Z

    #@b7
    if-nez v9, :cond_11b

    #@b9
    .line 233
    sget-boolean v9, Lcom/android/internal/telephony/DataConnectionManager;->blockPakcetProcessFlag:Z

    #@bb
    if-eqz v9, :cond_db

    #@bd
    .line 234
    const/4 v9, 0x0

    #@be
    sput-boolean v9, Lcom/android/internal/telephony/DataConnectionManager;->blockPakcetProcessFlag:Z
    :try_end_c0
    .catchall {:try_start_50 .. :try_end_c0} :catchall_44

    #@c0
    .line 237
    :try_start_c0
    invoke-interface {v7, v4}, Landroid/os/INetworkManagementService;->acceptPacket(Ljava/lang/String;)V

    #@c3
    .line 239
    const-string v9, "[LGE_DATA][DCM] "

    #@c5
    new-instance v10, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v11, " <functionForPacketDrop()> acceptPacket_SKT ifacename = "

    #@cc
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v10

    #@d0
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v10

    #@d4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v10

    #@d8
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_db
    .catchall {:try_start_c0 .. :try_end_db} :catchall_44
    .catch Ljava/lang/Exception; {:try_start_c0 .. :try_end_db} :catch_101

    #@db
    .line 245
    :cond_db
    :goto_db
    :try_start_db
    const-string v9, "[LGE_DATA][DCM] "

    #@dd
    const-string v10, " <functionForPacketDrop()> return!!!"

    #@df
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    goto/16 :goto_10

    #@e4
    .line 218
    :catch_e4
    move-exception v3

    #@e5
    .line 219
    .local v3, e:Ljava/lang/Exception;
    const-string v11, "[LGE_DATA][DCM] "

    #@e7
    new-instance v12, Ljava/lang/StringBuilder;

    #@e9
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@ec
    const-string v13, "exception = "

    #@ee
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v12

    #@f2
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v12

    #@f6
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v12

    #@fa
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@fd
    goto/16 :goto_50

    #@ff
    .end local v3           #e:Ljava/lang/Exception;
    :cond_ff
    move v9, v10

    #@100
    .line 226
    goto :goto_99

    #@101
    .line 240
    :catch_101
    move-exception v3

    #@102
    .line 241
    .restart local v3       #e:Ljava/lang/Exception;
    const-string v9, "[LGE_DATA][DCM] "

    #@104
    new-instance v10, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v11, " <functionForPacketDrop()> service.acceptPacket exception = "

    #@10b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v10

    #@10f
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v10

    #@113
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v10

    #@117
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11a
    goto :goto_db

    #@11b
    .line 249
    .end local v3           #e:Ljava/lang/Exception;
    :cond_11b
    if-eqz p1, :cond_182

    #@11d
    sget-boolean v9, Lcom/android/internal/telephony/DataConnectionManager;->blockPakcetProcessFlag:Z

    #@11f
    if-nez v9, :cond_182

    #@121
    .line 250
    const/4 v9, 0x1

    #@122
    sput-boolean v9, Lcom/android/internal/telephony/DataConnectionManager;->blockPakcetProcessFlag:Z

    #@124
    .line 252
    if-eqz v5, :cond_10

    #@126
    const-string v9, "45005"

    #@128
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12b
    move-result v9

    #@12c
    if-eqz v9, :cond_138

    #@12e
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionManager;->featureset:Ljava/lang/String;

    #@130
    const-string v10, "SKTBASE"

    #@132
    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@135
    move-result v9

    #@136
    if-nez v9, :cond_14a

    #@138
    :cond_138
    const-string v9, "45008"

    #@13a
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13d
    move-result v9

    #@13e
    if-eqz v9, :cond_10

    #@140
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnectionManager;->featureset:Ljava/lang/String;

    #@142
    const-string v10, "KTBASE"

    #@144
    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_147
    .catchall {:try_start_db .. :try_end_147} :catchall_44

    #@147
    move-result v9

    #@148
    if-eqz v9, :cond_10

    #@14a
    .line 258
    :cond_14a
    :try_start_14a
    invoke-interface {v7, v4}, Landroid/os/INetworkManagementService;->dropPacket(Ljava/lang/String;)V

    #@14d
    .line 260
    const-string v9, "[LGE_DATA][DCM] "

    #@14f
    new-instance v10, Ljava/lang/StringBuilder;

    #@151
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@154
    const-string v11, " <functionForPacketDrop()> dropPacket_SKT  ifacename = "

    #@156
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@159
    move-result-object v10

    #@15a
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v10

    #@15e
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@161
    move-result-object v10

    #@162
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_165
    .catchall {:try_start_14a .. :try_end_165} :catchall_44
    .catch Ljava/lang/Exception; {:try_start_14a .. :try_end_165} :catch_167

    #@165
    goto/16 :goto_10

    #@167
    .line 261
    :catch_167
    move-exception v3

    #@168
    .line 262
    .restart local v3       #e:Ljava/lang/Exception;
    :try_start_168
    const-string v9, "[LGE_DATA][DCM] "

    #@16a
    new-instance v10, Ljava/lang/StringBuilder;

    #@16c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@16f
    const-string v11, " <functionForPacketDrop()> service.dropPacket exception = "

    #@171
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v10

    #@175
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@178
    move-result-object v10

    #@179
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17c
    move-result-object v10

    #@17d
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@180
    goto/16 :goto_10

    #@182
    .line 265
    .end local v3           #e:Ljava/lang/Exception;
    :cond_182
    if-nez p1, :cond_10

    #@184
    sget-boolean v9, Lcom/android/internal/telephony/DataConnectionManager;->blockPakcetProcessFlag:Z

    #@186
    if-eqz v9, :cond_10

    #@188
    .line 266
    const/4 v9, 0x0

    #@189
    sput-boolean v9, Lcom/android/internal/telephony/DataConnectionManager;->blockPakcetProcessFlag:Z
    :try_end_18b
    .catchall {:try_start_168 .. :try_end_18b} :catchall_44

    #@18b
    .line 270
    :try_start_18b
    invoke-interface {v7, v4}, Landroid/os/INetworkManagementService;->acceptPacket(Ljava/lang/String;)V

    #@18e
    .line 272
    const-string v9, "[LGE_DATA][DCM] "

    #@190
    new-instance v10, Ljava/lang/StringBuilder;

    #@192
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@195
    const-string v11, " <functionForPacketDrop()> acceptPacket_SKT ifacename = "

    #@197
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v10

    #@19b
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v10

    #@19f
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a2
    move-result-object v10

    #@1a3
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1a6
    .catchall {:try_start_18b .. :try_end_1a6} :catchall_44
    .catch Ljava/lang/Exception; {:try_start_18b .. :try_end_1a6} :catch_1a8

    #@1a6
    goto/16 :goto_10

    #@1a8
    .line 273
    :catch_1a8
    move-exception v3

    #@1a9
    .line 274
    .restart local v3       #e:Ljava/lang/Exception;
    :try_start_1a9
    const-string v9, "[LGE_DATA][DCM] "

    #@1ab
    new-instance v10, Ljava/lang/StringBuilder;

    #@1ad
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1b0
    const-string v11, " <functionForPacketDrop()> service.acceptPacket exception = "

    #@1b2
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b5
    move-result-object v10

    #@1b6
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v10

    #@1ba
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bd
    move-result-object v10

    #@1be
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1c1
    .catchall {:try_start_1a9 .. :try_end_1c1} :catchall_44

    #@1c1
    goto/16 :goto_10

    #@1c3
    .line 217
    .end local v3           #e:Ljava/lang/Exception;
    :catch_1c3
    move-exception v11

    #@1c4
    goto/16 :goto_50
.end method

.method public isAllowRoaming()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 309
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionManager;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v2

    #@7
    const-string v3, "data_roaming"

    #@9
    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_c
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_c} :catch_11

    #@c
    move-result v2

    #@d
    if-lez v2, :cond_10

    #@f
    const/4 v1, 0x1

    #@10
    .line 311
    :cond_10
    :goto_10
    return v1

    #@11
    .line 310
    :catch_11
    move-exception v0

    #@12
    .line 311
    .local v0, snfe:Landroid/provider/Settings$SettingNotFoundException;
    goto :goto_10
.end method

.method public isNetworkRoaming()Z
    .registers 3

    #@0
    .prologue
    .line 304
    const-string v0, "true"

    #@2
    const-string v1, "gsm.operator.isroaming"

    #@4
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public isNetworkSKT_formccmnc()Z
    .registers 5

    #@0
    .prologue
    .line 298
    const-string v1, "persist.radio.camped_mccmnc"

    #@2
    const-string v2, "false"

    #@4
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 299
    .local v0, prop_mccmnc:Ljava/lang/String;
    const-string v1, "[LGE_DATA][DCM] "

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "[LGE_DATA] USIM_MCC_MNC = "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 300
    const-string v1, "45005"

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v1

    #@26
    return v1
.end method
