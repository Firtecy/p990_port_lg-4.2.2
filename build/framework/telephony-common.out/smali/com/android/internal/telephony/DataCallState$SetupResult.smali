.class public final enum Lcom/android/internal/telephony/DataCallState$SetupResult;
.super Ljava/lang/Enum;
.source "DataCallState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataCallState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SetupResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/DataCallState$SetupResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/DataCallState$SetupResult;

.field public static final enum ERR_BadCommand:Lcom/android/internal/telephony/DataCallState$SetupResult;

.field public static final enum ERR_GetLastErrorFromRil:Lcom/android/internal/telephony/DataCallState$SetupResult;

.field public static final enum ERR_RilError:Lcom/android/internal/telephony/DataCallState$SetupResult;

.field public static final enum ERR_Stale:Lcom/android/internal/telephony/DataCallState$SetupResult;

.field public static final enum ERR_UnacceptableParameter:Lcom/android/internal/telephony/DataCallState$SetupResult;

.field public static final enum SUCCESS:Lcom/android/internal/telephony/DataCallState$SetupResult;


# instance fields
.field public mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 56
    new-instance v0, Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@7
    const-string v1, "SUCCESS"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/DataCallState$SetupResult;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/DataCallState$SetupResult;->SUCCESS:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@e
    .line 57
    new-instance v0, Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@10
    const-string v1, "ERR_BadCommand"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/DataCallState$SetupResult;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_BadCommand:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@17
    .line 58
    new-instance v0, Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@19
    const-string v1, "ERR_UnacceptableParameter"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/DataCallState$SetupResult;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_UnacceptableParameter:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@20
    .line 59
    new-instance v0, Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@22
    const-string v1, "ERR_GetLastErrorFromRil"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/DataCallState$SetupResult;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_GetLastErrorFromRil:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@29
    .line 60
    new-instance v0, Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@2b
    const-string v1, "ERR_Stale"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/internal/telephony/DataCallState$SetupResult;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_Stale:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@32
    .line 61
    new-instance v0, Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@34
    const-string v1, "ERR_RilError"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DataCallState$SetupResult;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_RilError:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@3c
    .line 55
    const/4 v0, 0x6

    #@3d
    new-array v0, v0, [Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@3f
    sget-object v1, Lcom/android/internal/telephony/DataCallState$SetupResult;->SUCCESS:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@41
    aput-object v1, v0, v3

    #@43
    sget-object v1, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_BadCommand:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@45
    aput-object v1, v0, v4

    #@47
    sget-object v1, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_UnacceptableParameter:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@49
    aput-object v1, v0, v5

    #@4b
    sget-object v1, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_GetLastErrorFromRil:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@4d
    aput-object v1, v0, v6

    #@4f
    sget-object v1, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_Stale:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@51
    aput-object v1, v0, v7

    #@53
    const/4 v1, 0x5

    #@54
    sget-object v2, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_RilError:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@56
    aput-object v2, v0, v1

    #@58
    sput-object v0, Lcom/android/internal/telephony/DataCallState$SetupResult;->$VALUES:[Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@5a
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 66
    const/4 v0, 0x0

    #@4
    invoke-static {v0}, Lcom/android/internal/telephony/DataConnection$FailCause;->fromInt(I)Lcom/android/internal/telephony/DataConnection$FailCause;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@a
    .line 67
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/DataCallState$SetupResult;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 55
    const-class v0, Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/DataCallState$SetupResult;
    .registers 1

    #@0
    .prologue
    .line 55
    sget-object v0, Lcom/android/internal/telephony/DataCallState$SetupResult;->$VALUES:[Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/DataCallState$SetupResult;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@8
    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataCallState$SetupResult;->name()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, "  SetupResult.mFailCause="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method
