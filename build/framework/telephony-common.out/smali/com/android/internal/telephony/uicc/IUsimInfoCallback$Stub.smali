.class public abstract Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;
.super Landroid/os/Binder;
.source "IUsimInfoCallback.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IUsimInfoCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/IUsimInfoCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.uicc.IUsimInfoCallback"

.field static final TRANSACTION_onPBMDeleteCB:I = 0x4

.field static final TRANSACTION_onPBMInfoCB:I = 0x1

.field static final TRANSACTION_onPBMReadCB:I = 0x2

.field static final TRANSACTION_onPBMWriteCB:I = 0x3


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/uicc/IUsimInfoCallback;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/uicc/IUsimInfoCallback;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/android/internal/telephony/uicc/IUsimInfoCallback;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_64

    #@4
    .line 92
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 42
    :sswitch_9
    const-string v3, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v3, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_26

    #@1a
    .line 50
    sget-object v3, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;

    #@22
    .line 55
    .local v0, _arg0:Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;
    :goto_22
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;->onPBMInfoCB(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V

    #@25
    goto :goto_8

    #@26
    .line 53
    .end local v0           #_arg0:Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;
    :cond_26
    const/4 v0, 0x0

    #@27
    .restart local v0       #_arg0:Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;
    goto :goto_22

    #@28
    .line 60
    .end local v0           #_arg0:Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;
    :sswitch_28
    const-string v3, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@2a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d
    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_3f

    #@33
    .line 63
    sget-object v3, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->CREATOR:Landroid/os/Parcelable$Creator;

    #@35
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@38
    move-result-object v0

    #@39
    check-cast v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;

    #@3b
    .line 68
    .local v0, _arg0:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    :goto_3b
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;->onPBMReadCB(Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)V

    #@3e
    goto :goto_8

    #@3f
    .line 66
    .end local v0           #_arg0:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    :cond_3f
    const/4 v0, 0x0

    #@40
    .restart local v0       #_arg0:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    goto :goto_3b

    #@41
    .line 73
    .end local v0           #_arg0:Lcom/android/internal/telephony/uicc/LGE_PBM_Records;
    :sswitch_41
    const-string v3, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@43
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@46
    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@49
    move-result v0

    #@4a
    .line 77
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v1

    #@4e
    .line 78
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;->onPBMWriteCB(II)V

    #@51
    goto :goto_8

    #@52
    .line 83
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_52
    const-string v3, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@54
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@57
    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5a
    move-result v0

    #@5b
    .line 87
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5e
    move-result v1

    #@5f
    .line 88
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;->onPBMDeleteCB(II)V

    #@62
    goto :goto_8

    #@63
    .line 38
    nop

    #@64
    :sswitch_data_64
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_28
        0x3 -> :sswitch_41
        0x4 -> :sswitch_52
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
