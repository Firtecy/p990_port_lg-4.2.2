.class Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;
.super Ljava/lang/Object;
.source "SIMRecords.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/SIMRecords;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EfKrMsisdnLoaded"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/uicc/SIMRecords;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/uicc/SIMRecords;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3609
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/uicc/SIMRecords;Lcom/android/internal/telephony/uicc/SIMRecords$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3609
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;-><init>(Lcom/android/internal/telephony/uicc/SIMRecords;)V

    #@3
    return-void
.end method


# virtual methods
.method public getEfName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3611
    const-string v0, "EF_MSISDN KR"

    #@2
    return-object v0
.end method

.method public onRecordLoaded(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    .line 3615
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2
    check-cast v2, [B

    #@4
    move-object v0, v2

    #@5
    check-cast v0, [B

    #@7
    .line 3617
    .local v0, data:[B
    invoke-static {}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$400()Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_34

    #@d
    .line 3619
    const-string v2, "GSM"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "[LGE_USIM] MSISDN-"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    array-length v4, v0

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, ": "

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 3622
    :cond_34
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@36
    array-length v3, v0

    #@37
    add-int/lit8 v3, v3, -0xe

    #@39
    add-int/lit8 v3, v3, 0x2

    #@3b
    array-length v4, v0

    #@3c
    add-int/lit8 v4, v4, -0x3

    #@3e
    invoke-static {v0, v3, v4}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->subarray([BII)[B

    #@41
    move-result-object v3

    #@42
    invoke-static {v2, v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$502(Lcom/android/internal/telephony/uicc/SIMRecords;[B)[B

    #@45
    .line 3624
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@47
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$500(Lcom/android/internal/telephony/uicc/SIMRecords;)[B

    #@4a
    move-result-object v2

    #@4b
    if-nez v2, :cond_55

    #@4d
    .line 3626
    const-string v2, "GSM"

    #@4f
    const-string v3, "[LGE_USIM] msisdnNumber is Invalid "

    #@51
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 3650
    :goto_54
    return-void

    #@55
    .line 3630
    :cond_55
    invoke-static {}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$400()Z

    #@58
    move-result v2

    #@59
    if-eqz v2, :cond_7d

    #@5b
    .line 3632
    const-string v2, "GSM"

    #@5d
    new-instance v3, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v4, "[LGE_USIM] MSISDN Number-"

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@6a
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$500(Lcom/android/internal/telephony/uicc/SIMRecords;)[B

    #@6d
    move-result-object v4

    #@6e
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@71
    move-result-object v4

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v3

    #@7a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 3635
    :cond_7d
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@7f
    const/4 v3, 0x0

    #@80
    invoke-static {v2, v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$602(Lcom/android/internal/telephony/uicc/SIMRecords;I)I

    #@83
    .line 3637
    const/4 v1, 0x0

    #@84
    .local v1, i:I
    :goto_84
    const/16 v2, 0xa

    #@86
    if-ge v1, v2, :cond_9a

    #@88
    .line 3639
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@8a
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@8c
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$500(Lcom/android/internal/telephony/uicc/SIMRecords;)[B

    #@8f
    move-result-object v3

    #@90
    aget-byte v3, v3, v1

    #@92
    and-int/lit16 v3, v3, 0xff

    #@94
    invoke-static {v2, v3}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$612(Lcom/android/internal/telephony/uicc/SIMRecords;I)I

    #@97
    .line 3637
    add-int/lit8 v1, v1, 0x1

    #@99
    goto :goto_84

    #@9a
    .line 3642
    :cond_9a
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@9c
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$600(Lcom/android/internal/telephony/uicc/SIMRecords;)I

    #@9f
    move-result v2

    #@a0
    if-eqz v2, :cond_ac

    #@a2
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/SIMRecords$EfKrMsisdnLoaded;->this$0:Lcom/android/internal/telephony/uicc/SIMRecords;

    #@a4
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->access$600(Lcom/android/internal/telephony/uicc/SIMRecords;)I

    #@a7
    move-result v2

    #@a8
    const/16 v3, 0x9f6

    #@aa
    if-ne v2, v3, :cond_d5

    #@ac
    .line 3643
    :cond_ac
    const/4 v2, 0x2

    #@ad
    sput v2, Lcom/android/internal/telephony/uicc/IccRecords;->mIsEmptyUsim:I

    #@af
    .line 3648
    :goto_af
    const-string v2, "gsm.sim.exception"

    #@b1
    sget v3, Lcom/android/internal/telephony/uicc/IccRecords;->mIsEmptyUsim:I

    #@b3
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@b6
    move-result-object v3

    #@b7
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@ba
    .line 3649
    const-string v2, "GSM"

    #@bc
    new-instance v3, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v4, "[LGE_USIM] mIsEmptyUsim: "

    #@c3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v3

    #@c7
    sget v4, Lcom/android/internal/telephony/uicc/IccRecords;->mIsEmptyUsim:I

    #@c9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v3

    #@cd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v3

    #@d1
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    goto :goto_54

    #@d5
    .line 3646
    :cond_d5
    const/4 v2, 0x1

    #@d6
    sput v2, Lcom/android/internal/telephony/uicc/IccRecords;->mIsEmptyUsim:I

    #@d8
    goto :goto_af
.end method
