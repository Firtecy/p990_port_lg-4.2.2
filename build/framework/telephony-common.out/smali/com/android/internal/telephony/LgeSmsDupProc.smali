.class public Lcom/android/internal/telephony/LgeSmsDupProc;
.super Ljava/lang/Object;
.source "LgeSmsDupProc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/LgeSmsDupProc$CompareMtSmsItem;
    }
.end annotation


# static fields
.field private static final COMPARE_LIST_NUM:I = 0x64

.field private static final TAG:Ljava/lang/String; = "DupCheck"

.field private static compareSmsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/LgeSmsDupProc$CompareMtSmsItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 27
    new-instance v0, Ljava/util/ArrayList;

    #@2
    const/16 v1, 0x64

    #@4
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@7
    sput-object v0, Lcom/android/internal/telephony/LgeSmsDupProc;->compareSmsList:Ljava/util/List;

    #@9
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    return-void
.end method


# virtual methods
.method public checkNetworkDuplicate(JLjava/lang/String;)Z
    .registers 10
    .parameter "stime"
    .parameter "addr"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 31
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    sget-object v2, Lcom/android/internal/telephony/LgeSmsDupProc;->compareSmsList:Ljava/util/List;

    #@4
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@7
    move-result v2

    #@8
    if-ge v0, v2, :cond_39

    #@a
    .line 32
    sget-object v2, Lcom/android/internal/telephony/LgeSmsDupProc;->compareSmsList:Ljava/util/List;

    #@c
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Lcom/android/internal/telephony/LgeSmsDupProc$CompareMtSmsItem;

    #@12
    iget-wide v4, v2, Lcom/android/internal/telephony/LgeSmsDupProc$CompareMtSmsItem;->timeStamp:J

    #@14
    cmp-long v2, v4, p1

    #@16
    if-nez v2, :cond_36

    #@18
    .line 33
    sget-object v2, Lcom/android/internal/telephony/LgeSmsDupProc;->compareSmsList:Ljava/util/List;

    #@1a
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/internal/telephony/LgeSmsDupProc$CompareMtSmsItem;

    #@20
    iget-object v2, v2, Lcom/android/internal/telephony/LgeSmsDupProc$CompareMtSmsItem;->address:Ljava/lang/String;

    #@22
    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_36

    #@28
    .line 34
    const-string v2, "DupCheck"

    #@2a
    const-string v3, "[TEL-MSG] Network Duplicated"

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 35
    sget-object v2, Lcom/android/internal/telephony/LgeSmsDupProc;->compareSmsList:Ljava/util/List;

    #@31
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@34
    .line 36
    const/4 v2, 0x1

    #@35
    .line 47
    :goto_35
    return v2

    #@36
    .line 31
    :cond_36
    add-int/lit8 v0, v0, 0x1

    #@38
    goto :goto_2

    #@39
    .line 40
    :cond_39
    new-instance v1, Lcom/android/internal/telephony/LgeSmsDupProc$CompareMtSmsItem;

    #@3b
    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/internal/telephony/LgeSmsDupProc$CompareMtSmsItem;-><init>(Lcom/android/internal/telephony/LgeSmsDupProc;JLjava/lang/String;)V

    #@3e
    .line 42
    .local v1, mtSms:Lcom/android/internal/telephony/LgeSmsDupProc$CompareMtSmsItem;
    sget-object v2, Lcom/android/internal/telephony/LgeSmsDupProc;->compareSmsList:Ljava/util/List;

    #@40
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@43
    move-result v2

    #@44
    const/16 v4, 0x64

    #@46
    if-lt v2, v4, :cond_4d

    #@48
    .line 43
    sget-object v2, Lcom/android/internal/telephony/LgeSmsDupProc;->compareSmsList:Ljava/util/List;

    #@4a
    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@4d
    .line 45
    :cond_4d
    sget-object v2, Lcom/android/internal/telephony/LgeSmsDupProc;->compareSmsList:Ljava/util/List;

    #@4f
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@52
    .line 46
    const-string v2, "DupCheck"

    #@54
    new-instance v4, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v5, "[TEL-MSG] compareSmsList.size()= "

    #@5b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    sget-object v5, Lcom/android/internal/telephony/LgeSmsDupProc;->compareSmsList:Ljava/util/List;

    #@61
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@64
    move-result v5

    #@65
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v4

    #@69
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v4

    #@6d
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    move v2, v3

    #@71
    .line 47
    goto :goto_35
.end method
