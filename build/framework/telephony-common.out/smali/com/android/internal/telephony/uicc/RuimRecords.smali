.class public final Lcom/android/internal/telephony/uicc/RuimRecords;
.super Lcom/android/internal/telephony/uicc/IccRecords;
.source "RuimRecords.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimEprlLoaded;,
        Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimCdmaHomeLoaded;,
        Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;,
        Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimMdnLoaded;,
        Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;,
        Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;,
        Lcom/android/internal/telephony/uicc/RuimRecords$EfPlLoaded;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final EVENT_APP_READY:I = 0x1

.field private static final EVENT_GET_ALL_SMS_DONE:I = 0x12

.field private static final EVENT_GET_CDMA_SUBSCRIPTION_DONE:I = 0xa

.field private static final EVENT_GET_DEVICE_IDENTITY_DONE:I = 0x4

.field private static final EVENT_GET_ICCID_DONE:I = 0x5

.field private static final EVENT_GET_IMSI_DONE:I = 0x3

.field private static final EVENT_GET_SMS_DONE:I = 0x16

.field private static final EVENT_GET_SST_DONE:I = 0x11

.field private static final EVENT_MARK_SMS_READ_DONE:I = 0x13

.field private static final EVENT_RUIM_REFRESH:I = 0x1f

.field private static final EVENT_SMS_ON_RUIM:I = 0x15

.field private static final EVENT_UPDATE_DONE:I = 0xe

.field public static final ICC_URI:Landroid/net/Uri; = null

.field public static final INDEX_ON_ICC:Ljava/lang/String; = "index_on_icc"

.field public static final IS_41_EMAIL_NETWORK_ADDRESS:Ljava/lang/String; = "6245"

.field public static final LGE_MSGTYPE_SIM:I = 0x7

.field public static final LGE_SERVICE_MSGTYPE:Ljava/lang/String; = "lgeMsgType"

.field static final LOG_TAG:Ljava/lang/String; = "CDMA"

.field private static final MISSING_TEXT:Ljava/lang/String; = "/*missing text*/"

.field public static final SMS_CONCAT_URI:Landroid/net/Uri; = null

.field private static final SMS_FORMAT_CSIM:I = 0x2

.field private static final SMS_FORMAT_USIM:I = 0x1

.field public static final SMS_INBOX_URI:Landroid/net/Uri; = null

.field public static final SMS_OUTBOX_URI:Landroid/net/Uri; = null

.field public static final SMS_SENT_URI:Landroid/net/Uri; = null

.field public static final SMS_URI:Landroid/net/Uri; = null

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final STATUS_PENDING:I = 0x20

.field public static final SUB_ID:Ljava/lang/String; = "sub_id"


# instance fields
.field mCsimSpnDisplayCondition:Z

.field private mEFli:[B

.field private mEFpl:[B

.field private mHomeNetworkId:Ljava/lang/String;

.field private mHomeSystemId:Ljava/lang/String;

.field mIndexOnIcc:I

.field private mMdn:Ljava/lang/String;

.field private mMin:Ljava/lang/String;

.field private mMin2Min1:Ljava/lang/String;

.field private mMyMobileNumber:Ljava/lang/String;

.field private mPrlVersion:Ljava/lang/String;

.field private m_ota_commited:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 118
    const-string v0, "content://sms/inbox"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_INBOX_URI:Landroid/net/Uri;

    #@8
    .line 119
    const-string v0, "content://sms/sent"

    #@a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_SENT_URI:Landroid/net/Uri;

    #@10
    .line 120
    const-string v0, "content://sms/outbox"

    #@12
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@15
    move-result-object v0

    #@16
    sput-object v0, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_OUTBOX_URI:Landroid/net/Uri;

    #@18
    .line 124
    const-string v0, "content://sms"

    #@1a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1d
    move-result-object v0

    #@1e
    sput-object v0, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_URI:Landroid/net/Uri;

    #@20
    .line 125
    const-string v0, "content://sms/icc"

    #@22
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@25
    move-result-object v0

    #@26
    sput-object v0, Lcom/android/internal/telephony/uicc/RuimRecords;->ICC_URI:Landroid/net/Uri;

    #@28
    .line 126
    const-string v0, "content://sms/concatpart"

    #@2a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@2d
    move-result-object v0

    #@2e
    sput-object v0, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@30
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 8
    .parameter "app"
    .parameter "c"
    .parameter "ci"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 156
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/uicc/IccRecords;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@5
    .line 96
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->m_ota_commited:Z

    #@7
    .line 99
    const/4 v0, -0x1

    #@8
    iput v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mIndexOnIcc:I

    #@a
    .line 109
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mEFpl:[B

    #@c
    .line 110
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mEFli:[B

    #@e
    .line 111
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mCsimSpnDisplayCondition:Z

    #@10
    .line 158
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@12
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@14
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecordCache;-><init>(Lcom/android/internal/telephony/uicc/IccFileHandler;)V

    #@17
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->adnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@19
    .line 160
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@1b
    .line 163
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@1d
    .line 166
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@1f
    const/16 v1, 0x1f

    #@21
    invoke-interface {v0, p0, v1, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForIccRefresh(Landroid/os/Handler;ILjava/lang/Object;)V

    #@24
    .line 169
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->resetRecords()V

    #@27
    .line 171
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@29
    const/4 v1, 0x1

    #@2a
    invoke-virtual {v0, p0, v1, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->registerForReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@2d
    .line 172
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/uicc/RuimRecords;)[B
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mEFpl:[B

    #@2
    return-object v0
.end method

.method static synthetic access$002(Lcom/android/internal/telephony/uicc/RuimRecords;[B)[B
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mEFpl:[B

    #@2
    return-object p1
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/uicc/RuimRecords;)[B
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mEFli:[B

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/android/internal/telephony/uicc/RuimRecords;[B)[B
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mEFli:[B

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/uicc/RuimRecords;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMdn:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/android/internal/telephony/uicc/RuimRecords;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMdn:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/uicc/RuimRecords;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/RuimRecords;->adjstMinDigits(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/uicc/RuimRecords;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMin:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/android/internal/telephony/uicc/RuimRecords;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMin:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$502(Lcom/android/internal/telephony/uicc/RuimRecords;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mHomeSystemId:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$602(Lcom/android/internal/telephony/uicc/RuimRecords;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mHomeNetworkId:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$700(Lcom/android/internal/telephony/uicc/RuimRecords;Landroid/os/AsyncResult;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/RuimRecords;->onGetCSimEprlDone(Landroid/os/AsyncResult;)V

    #@3
    return-void
.end method

.method private adjstMinDigits(I)I
    .registers 3
    .parameter "digits"

    #@0
    .prologue
    .line 260
    add-int/lit8 p1, p1, 0x6f

    #@2
    .line 261
    rem-int/lit8 v0, p1, 0xa

    #@4
    if-nez v0, :cond_8

    #@6
    add-int/lit8 p1, p1, -0xa

    #@8
    .line 262
    :cond_8
    div-int/lit8 v0, p1, 0xa

    #@a
    rem-int/lit8 v0, v0, 0xa

    #@c
    if-nez v0, :cond_10

    #@e
    add-int/lit8 p1, p1, -0x64

    #@10
    .line 263
    :cond_10
    div-int/lit8 v0, p1, 0x64

    #@12
    rem-int/lit8 v0, v0, 0xa

    #@14
    if-nez v0, :cond_18

    #@16
    add-int/lit16 p1, p1, -0x3e8

    #@18
    .line 264
    :cond_18
    return p1
.end method

.method private emptyCSimCache()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 741
    const-string v0, "emptyCSimCache(), start"

    #@3
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@6
    .line 742
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v1, "emptyCSimCache(), isSimSmsDeleteAll: "

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    sget-boolean v1, Lcom/android/internal/telephony/uicc/RuimRecords;->isSimSmsDeleteAll:Z

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1e
    .line 743
    sget-boolean v0, Lcom/android/internal/telephony/uicc/RuimRecords;->isSimSmsDeleteAll:Z

    #@20
    if-nez v0, :cond_5c

    #@22
    .line 744
    const-string v0, "emptyCSimCache(), Delete RuimRecords.ICC_URI"

    #@24
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@27
    .line 745
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@29
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@2b
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2e
    move-result-object v1

    #@2f
    sget-object v2, Lcom/android/internal/telephony/uicc/RuimRecords;->ICC_URI:Landroid/net/Uri;

    #@31
    invoke-static {v0, v1, v2, v3, v3}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@34
    .line 746
    const/4 v0, 0x1

    #@35
    sput-boolean v0, Lcom/android/internal/telephony/uicc/RuimRecords;->isSimSmsDeleteAll:Z

    #@37
    .line 747
    new-instance v0, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v1, "emptyCSimCache(), After isSimSmsDeleteAll: "

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    sget-boolean v1, Lcom/android/internal/telephony/uicc/RuimRecords;->isSimSmsDeleteAll:Z

    #@44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@47
    move-result-object v0

    #@48
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v0

    #@4c
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4f
    .line 748
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@51
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@53
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@56
    move-result-object v1

    #@57
    sget-object v2, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@59
    invoke-static {v0, v1, v2, v3, v3}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@5c
    .line 750
    :cond_5c
    return-void
.end method

.method private extractEmailAddressFromMessageBody(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .registers 15
    .parameter "originatingAddress"
    .parameter "messageBody"

    #@0
    .prologue
    .line 1080
    const-string v10, "( /)|( )"

    #@2
    const/4 v11, 0x2

    #@3
    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@6
    move-result-object v6

    #@7
    .line 1081
    .local v6, parts:[Ljava/lang/String;
    array-length v10, v6

    #@8
    const/4 v11, 0x2

    #@9
    if-ge v10, v11, :cond_d

    #@b
    const/4 v6, 0x0

    #@c
    .line 1134
    .end local v6           #parts:[Ljava/lang/String;
    :goto_c
    return-object v6

    #@d
    .line 1083
    .restart local v6       #parts:[Ljava/lang/String;
    :cond_d
    const/4 v10, 0x0

    #@e
    aget-object v1, v6, v10

    #@10
    .line 1084
    .local v1, emailFrom:Ljava/lang/String;
    const/4 v10, 0x1

    #@11
    aget-object v0, v6, v10

    #@13
    .line 1090
    .local v0, emailBody:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@16
    move-result v10

    #@17
    const/4 v11, 0x4

    #@18
    if-ne v10, v11, :cond_27

    #@1a
    const-string v10, "6245"

    #@1c
    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v10

    #@20
    if-eqz v10, :cond_27

    #@22
    .line 1092
    const/4 v2, 0x1

    #@23
    .line 1101
    .local v2, isEmail:Z
    :goto_23
    if-nez v2, :cond_2c

    #@25
    .line 1102
    const/4 v6, 0x0

    #@26
    goto :goto_c

    #@27
    .line 1094
    .end local v2           #isEmail:Z
    :cond_27
    invoke-static {v1}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@2a
    move-result v2

    #@2b
    .restart local v2       #isEmail:Z
    goto :goto_23

    #@2c
    .line 1105
    :cond_2c
    invoke-static {v1}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@2f
    move-result v10

    #@30
    if-nez v10, :cond_5d

    #@32
    const-string v10, "("

    #@34
    invoke-virtual {v1, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@37
    move-result v10

    #@38
    if-nez v10, :cond_5d

    #@3a
    .line 1106
    const-string v10, ")"

    #@3c
    invoke-virtual {p2, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@3f
    move-result v3

    #@40
    .line 1107
    .local v3, parenthesis:I
    const-string v10, ")"

    #@42
    invoke-virtual {v1, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@45
    move-result v10

    #@46
    const/4 v11, -0x1

    #@47
    if-ne v10, v11, :cond_5d

    #@49
    const/4 v10, -0x1

    #@4a
    if-eq v3, v10, :cond_5d

    #@4c
    .line 1108
    const/4 v10, 0x0

    #@4d
    add-int/lit8 v11, v3, 0x1

    #@4f
    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    .line 1109
    add-int/lit8 v10, v3, 0x2

    #@55
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@58
    move-result v11

    #@59
    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    .line 1113
    .end local v3           #parenthesis:I
    :cond_5d
    const-string v10, "( /)|( )"

    #@5f
    const/4 v11, 0x2

    #@60
    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@63
    move-result-object v7

    #@64
    .line 1114
    .local v7, parts2:[Ljava/lang/String;
    array-length v10, v7

    #@65
    const/4 v11, 0x1

    #@66
    if-le v10, v11, :cond_96

    #@68
    .line 1115
    const/4 v10, 0x0

    #@69
    aget-object v9, v7, v10

    #@6b
    .line 1116
    .local v9, tempFrom:Ljava/lang/String;
    const/4 v10, 0x1

    #@6c
    aget-object v8, v7, v10

    #@6e
    .line 1118
    .local v8, tempBody:Ljava/lang/String;
    const-string v10, "("

    #@70
    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@73
    move-result v5

    #@74
    .line 1119
    .local v5, parenthesisStart:I
    const/4 v10, -0x1

    #@75
    if-eq v5, v10, :cond_96

    #@77
    .line 1120
    const-string v10, ")"

    #@79
    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@7c
    move-result v4

    #@7d
    .line 1122
    .local v4, parenthesisEnd:I
    const/4 v10, -0x1

    #@7e
    if-eq v4, v10, :cond_96

    #@80
    if-le v4, v5, :cond_96

    #@82
    .line 1123
    add-int/lit8 v10, v5, 0x1

    #@84
    invoke-virtual {v9, v10, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@87
    move-result-object v9

    #@88
    .line 1124
    invoke-static {v9}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@8b
    move-result v10

    #@8c
    if-nez v10, :cond_94

    #@8e
    invoke-static {v9}, Landroid/provider/Telephony$Mms;->isPhoneNumber(Ljava/lang/String;)Z

    #@91
    move-result v10

    #@92
    if-eqz v10, :cond_96

    #@94
    .line 1125
    :cond_94
    move-object v1, v9

    #@95
    .line 1126
    move-object v0, v8

    #@96
    .line 1132
    .end local v4           #parenthesisEnd:I
    .end local v5           #parenthesisStart:I
    .end local v8           #tempBody:Ljava/lang/String;
    .end local v9           #tempFrom:Ljava/lang/String;
    :cond_96
    const/4 v10, 0x0

    #@97
    aput-object v1, v6, v10

    #@99
    .line 1133
    const/4 v10, 0x1

    #@9a
    aput-object v0, v6, v10

    #@9c
    goto/16 :goto_c
.end method

.method private fetchRuimRecords()V
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/16 v7, 0x64

    #@3
    const/4 v6, 0x0

    #@4
    .line 1257
    iput-boolean v5, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@6
    .line 1259
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "fetchRuimRecords "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    iget v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@1e
    .line 1268
    :try_start_1e
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@21
    move-result-object v2

    #@22
    const/4 v3, 0x1

    #@23
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@26
    move-result-object v0

    #@27
    .line 1272
    .local v0, cardApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eqz v0, :cond_131

    #@29
    .line 1273
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@2b
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getAid()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    const/4 v4, 0x3

    #@30
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/uicc/RuimRecords;->obtainMessage(I)Landroid/os/Message;

    #@33
    move-result-object v4

    #@34
    invoke-interface {v2, v3, v4}, Lcom/android/internal/telephony/CommandsInterface;->getIMSIForApp(Ljava/lang/String;Landroid/os/Message;)V

    #@37
    .line 1274
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@39
    add-int/lit8 v2, v2, 0x1

    #@3b
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I
    :try_end_3d
    .catch Ljava/lang/RuntimeException; {:try_start_1e .. :try_end_3d} :catch_138

    #@3d
    .line 1285
    .end local v0           #cardApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :goto_3d
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@3f
    const/16 v3, 0x2fe2

    #@41
    const/4 v4, 0x5

    #@42
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/uicc/RuimRecords;->obtainMessage(I)Landroid/os/Message;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@49
    .line 1286
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@4b
    add-int/lit8 v2, v2, 0x1

    #@4d
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@4f
    .line 1288
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@51
    const/16 v3, 0x2f05

    #@53
    new-instance v4, Lcom/android/internal/telephony/uicc/RuimRecords$EfPlLoaded;

    #@55
    invoke-direct {v4, p0, v6}, Lcom/android/internal/telephony/uicc/RuimRecords$EfPlLoaded;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;Lcom/android/internal/telephony/uicc/RuimRecords$1;)V

    #@58
    invoke-virtual {p0, v7, v4}, Lcom/android/internal/telephony/uicc/RuimRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@5b
    move-result-object v4

    #@5c
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@5f
    .line 1290
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@61
    add-int/lit8 v2, v2, 0x1

    #@63
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@65
    .line 1292
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@67
    const/16 v3, 0x6f3a

    #@69
    new-instance v4, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;

    #@6b
    invoke-direct {v4, p0, v6}, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimLiLoaded;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;Lcom/android/internal/telephony/uicc/RuimRecords$1;)V

    #@6e
    invoke-virtual {p0, v7, v4}, Lcom/android/internal/telephony/uicc/RuimRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@71
    move-result-object v4

    #@72
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@75
    .line 1294
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@77
    add-int/lit8 v2, v2, 0x1

    #@79
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@7b
    .line 1296
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@7d
    const/16 v3, 0x6f41

    #@7f
    new-instance v4, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;

    #@81
    invoke-direct {v4, p0, v6}, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;Lcom/android/internal/telephony/uicc/RuimRecords$1;)V

    #@84
    invoke-virtual {p0, v7, v4}, Lcom/android/internal/telephony/uicc/RuimRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@87
    move-result-object v4

    #@88
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@8b
    .line 1298
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@8d
    add-int/lit8 v2, v2, 0x1

    #@8f
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@91
    .line 1300
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@93
    const/16 v3, 0x6f44

    #@95
    new-instance v4, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimMdnLoaded;

    #@97
    invoke-direct {v4, p0, v6}, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimMdnLoaded;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;Lcom/android/internal/telephony/uicc/RuimRecords$1;)V

    #@9a
    invoke-virtual {p0, v7, v4}, Lcom/android/internal/telephony/uicc/RuimRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@9d
    move-result-object v4

    #@9e
    invoke-virtual {v2, v3, v5, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(IILandroid/os/Message;)V

    #@a1
    .line 1302
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@a3
    add-int/lit8 v2, v2, 0x1

    #@a5
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@a7
    .line 1304
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@a9
    const/16 v3, 0x6f22

    #@ab
    new-instance v4, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;

    #@ad
    invoke-direct {v4, p0, v6}, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;Lcom/android/internal/telephony/uicc/RuimRecords$1;)V

    #@b0
    invoke-virtual {p0, v7, v4}, Lcom/android/internal/telephony/uicc/RuimRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b3
    move-result-object v4

    #@b4
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    #@b7
    .line 1306
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@b9
    add-int/lit8 v2, v2, 0x1

    #@bb
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@bd
    .line 1308
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@bf
    const/16 v3, 0x6f28

    #@c1
    new-instance v4, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimCdmaHomeLoaded;

    #@c3
    invoke-direct {v4, p0, v6}, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimCdmaHomeLoaded;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;Lcom/android/internal/telephony/uicc/RuimRecords$1;)V

    #@c6
    invoke-virtual {p0, v7, v4}, Lcom/android/internal/telephony/uicc/RuimRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@c9
    move-result-object v4

    #@ca
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V

    #@cd
    .line 1310
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@cf
    add-int/lit8 v2, v2, 0x1

    #@d1
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@d3
    .line 1314
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@d5
    const/16 v3, 0x6f5a

    #@d7
    const/4 v4, 0x4

    #@d8
    new-instance v5, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimEprlLoaded;

    #@da
    invoke-direct {v5, p0, v6}, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimEprlLoaded;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;Lcom/android/internal/telephony/uicc/RuimRecords$1;)V

    #@dd
    invoke-virtual {p0, v7, v5}, Lcom/android/internal/telephony/uicc/RuimRecords;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@e0
    move-result-object v5

    #@e1
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(IILandroid/os/Message;)V

    #@e4
    .line 1316
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@e6
    add-int/lit8 v2, v2, 0x1

    #@e8
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@ea
    .line 1319
    const-string v2, "uicc_csim"

    #@ec
    invoke-static {v6, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@ef
    move-result v2

    #@f0
    if-eqz v2, :cond_10c

    #@f2
    .line 1321
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@f4
    const/16 v3, 0x6f3c

    #@f6
    const/16 v4, 0x12

    #@f8
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/uicc/RuimRecords;->obtainMessage(I)Landroid/os/Message;

    #@fb
    move-result-object v4

    #@fc
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V

    #@ff
    .line 1322
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@101
    add-int/lit8 v2, v2, 0x1

    #@103
    iput v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@105
    .line 1323
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@108
    move-result-wide v2

    #@109
    invoke-static {v2, v3}, Lcom/android/internal/telephony/cdma/SmsMessage;->setTimeforSMSonCSIM(J)V

    #@10c
    .line 1326
    :cond_10c
    new-instance v2, Ljava/lang/StringBuilder;

    #@10e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@111
    const-string v3, "fetchRuimRecords "

    #@113
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v2

    #@117
    iget v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@119
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v2

    #@11d
    const-string v3, " requested: "

    #@11f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v2

    #@123
    iget-boolean v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@125
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@128
    move-result-object v2

    #@129
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12c
    move-result-object v2

    #@12d
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@130
    .line 1328
    return-void

    #@131
    .line 1276
    .restart local v0       #cardApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :cond_131
    :try_start_131
    const-string v2, "Exception read IMSI : cardApp == null"

    #@133
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V
    :try_end_136
    .catch Ljava/lang/RuntimeException; {:try_start_131 .. :try_end_136} :catch_138

    #@136
    goto/16 :goto_3d

    #@138
    .line 1279
    .end local v0           #cardApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :catch_138
    move-exception v1

    #@139
    .line 1280
    .local v1, exc:Ljava/lang/RuntimeException;
    new-instance v2, Ljava/lang/StringBuilder;

    #@13b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13e
    const-string v3, "Exception read IMSI"

    #@140
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v2

    #@144
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v2

    #@148
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14b
    move-result-object v2

    #@14c
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@14f
    goto/16 :goto_3d
.end method

.method private findBestLanguage([B)Ljava/lang/String;
    .registers 13
    .parameter "languages"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v10, 0x2

    #@2
    .line 1139
    const/4 v0, 0x0

    #@3
    .line 1140
    .local v0, bestMatch:Ljava/lang/String;
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v7}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    #@8
    move-result-object v7

    #@9
    invoke-virtual {v7}, Landroid/content/res/AssetManager;->getLocales()[Ljava/lang/String;

    #@c
    move-result-object v5

    #@d
    .line 1142
    .local v5, locales:[Ljava/lang/String;
    if-eqz p1, :cond_11

    #@f
    if-nez v5, :cond_13

    #@11
    :cond_11
    move-object v4, v6

    #@12
    .line 1164
    :cond_12
    :goto_12
    return-object v4

    #@13
    .line 1145
    :cond_13
    const/4 v2, 0x0

    #@14
    .local v2, i:I
    :goto_14
    add-int/lit8 v7, v2, 0x1

    #@16
    array-length v8, p1

    #@17
    if-ge v7, v8, :cond_4e

    #@19
    .line 1147
    :try_start_19
    new-instance v4, Ljava/lang/String;

    #@1b
    const/4 v7, 0x2

    #@1c
    const-string v8, "ISO-8859-1"

    #@1e
    invoke-direct {v4, p1, v2, v7, v8}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@21
    .line 1150
    .local v4, lang:Ljava/lang/String;
    const-string v7, "kr"

    #@23
    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v7

    #@27
    if-eqz v7, :cond_2b

    #@29
    const-string v4, "ko"

    #@2b
    .line 1152
    :cond_2b
    const/4 v3, 0x0

    #@2c
    .local v3, j:I
    :goto_2c
    array-length v7, v5

    #@2d
    if-ge v3, v7, :cond_4c

    #@2f
    .line 1153
    aget-object v7, v5, v3

    #@31
    if-eqz v7, :cond_49

    #@33
    aget-object v7, v5, v3

    #@35
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@38
    move-result v7

    #@39
    if-lt v7, v10, :cond_49

    #@3b
    aget-object v7, v5, v3

    #@3d
    const/4 v8, 0x0

    #@3e
    const/4 v9, 0x2

    #@3f
    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@42
    move-result-object v7

    #@43
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_46
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_19 .. :try_end_46} :catch_50

    #@46
    move-result v7

    #@47
    if-nez v7, :cond_12

    #@49
    .line 1152
    :cond_49
    add-int/lit8 v3, v3, 0x1

    #@4b
    goto :goto_2c

    #@4c
    .line 1158
    :cond_4c
    if-eqz v0, :cond_56

    #@4e
    .end local v3           #j:I
    .end local v4           #lang:Ljava/lang/String;
    :cond_4e
    move-object v4, v6

    #@4f
    .line 1164
    goto :goto_12

    #@50
    .line 1159
    :catch_50
    move-exception v1

    #@51
    .line 1160
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    const-string v7, "Failed to parse SIM language records"

    #@53
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@56
    .line 1145
    .end local v1           #e:Ljava/io/UnsupportedEncodingException;
    :cond_56
    add-int/lit8 v2, v2, 0x2

    #@58
    goto :goto_14
.end method

.method private handleCSimSmses(Ljava/util/ArrayList;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 728
    .local p1, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    const-string v1, "handleCSimSmses(), start"

    #@2
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 729
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->emptyCSimCache()V

    #@8
    .line 731
    move-object v0, p1

    #@9
    .line 732
    .local v0, simSmsMessage:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    new-instance v1, Ljava/lang/Thread;

    #@b
    new-instance v2, Lcom/android/internal/telephony/uicc/RuimRecords$1;

    #@d
    invoke-direct {v2, p0, v0}, Lcom/android/internal/telephony/uicc/RuimRecords$1;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;Ljava/util/ArrayList;)V

    #@10
    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@13
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    #@16
    .line 739
    return-void
.end method

.method private handleRuimRefresh(Lcom/android/internal/telephony/uicc/IccRefreshResponse;)V
    .registers 5
    .parameter "refreshResponse"

    #@0
    .prologue
    .line 1380
    if-nez p1, :cond_8

    #@2
    .line 1381
    const-string v0, "handleRuimRefresh received without input"

    #@4
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@7
    .line 1430
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1396
    :cond_8
    iget v0, p1, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->refreshResult:I

    #@a
    packed-switch v0, :pswitch_data_52

    #@d
    .line 1427
    const-string v0, "handleRuimRefresh with unknown operation"

    #@f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@12
    goto :goto_7

    #@13
    .line 1398
    :pswitch_13
    const-string v0, "handleRuimRefresh with SIM_REFRESH_FILE_UPDATED"

    #@15
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@18
    .line 1399
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->adnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@1a
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->reset()V

    #@1d
    .line 1400
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->fetchRuimRecords()V

    #@20
    goto :goto_7

    #@21
    .line 1403
    :pswitch_21
    const-string v0, "handleRuimRefresh with SIM_REFRESH_INIT"

    #@23
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@26
    .line 1405
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->fetchRuimRecords()V

    #@29
    goto :goto_7

    #@2a
    .line 1408
    :pswitch_2a
    const-string v0, "handleRuimRefresh with SIM_REFRESH_RESET"

    #@2c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@2f
    .line 1409
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->powerOffOnSimReset()Z

    #@32
    move-result v0

    #@33
    if-eqz v0, :cond_3d

    #@35
    .line 1410
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@37
    const/4 v1, 0x0

    #@38
    const/4 v2, 0x0

    #@39
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setRadioPower(ZLandroid/os/Message;)V

    #@3c
    goto :goto_7

    #@3d
    .line 1419
    :cond_3d
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@3f
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@42
    move-result-object v0

    #@43
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@45
    if-ne v0, v1, :cond_7

    #@47
    .line 1420
    const-string v0, "CDMA"

    #@49
    const-string v1, "[RuimRecords] APPSTATE_READY"

    #@4b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 1421
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->fetchRuimRecords()V

    #@51
    goto :goto_7

    #@52
    .line 1396
    :pswitch_data_52
    .packed-switch 0x0
        :pswitch_13
        :pswitch_21
        :pswitch_2a
    .end packed-switch
.end method

.method private insertSmsDB(ILcom/android/internal/telephony/cdma/SmsMessage;)Landroid/net/Uri;
    .registers 24
    .parameter "statsOnSim"
    .parameter "sms"

    #@0
    .prologue
    .line 789
    const/4 v6, 0x0

    #@1
    .line 790
    .local v6, boxType:I
    const/4 v7, 0x0

    #@2
    .line 791
    .local v7, boxTypeKR:I
    const/4 v5, 0x0

    #@3
    .line 792
    .local v5, body:Ljava/lang/String;
    const/4 v4, 0x0

    #@4
    .line 793
    .local v4, address:Ljava/lang/String;
    const-wide/16 v8, -0x1

    #@6
    .line 794
    .local v8, date:J
    const/4 v15, 0x0

    #@7
    .line 795
    .local v15, smsBoxUri:Landroid/net/Uri;
    const/16 v16, 0x0

    #@9
    .line 796
    .local v16, smsInsertUri:Landroid/net/Uri;
    new-instance v18, Landroid/content/ContentValues;

    #@b
    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    #@e
    .line 802
    .local v18, values:Landroid/content/ContentValues;
    sparse-switch p1, :sswitch_data_224

    #@11
    .line 836
    const/4 v6, 0x2

    #@12
    .line 837
    const/4 v7, 0x2

    #@13
    .line 838
    :try_start_13
    sget-object v15, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_SENT_URI:Landroid/net/Uri;

    #@15
    .line 840
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    .line 841
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1c
    move-result-wide v8

    #@1d
    .line 842
    const-string v19, "read"

    #@1f
    const/16 v20, 0x1

    #@21
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v20

    #@25
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@28
    .line 843
    const-string v19, "seen"

    #@2a
    const/16 v20, 0x1

    #@2c
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f
    move-result-object v20

    #@30
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@33
    .line 848
    :cond_33
    :goto_33
    const-string v19, "type"

    #@35
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38
    move-result-object v20

    #@39
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3c
    .line 851
    const/16 v19, 0x0

    #@3e
    const-string v20, "parse_email_on_uicc"

    #@40
    invoke-static/range {v19 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@43
    move-result v19

    #@44
    if-eqz v19, :cond_6e

    #@46
    .line 852
    if-eqz v4, :cond_6e

    #@48
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@4b
    move-result v19

    #@4c
    const/16 v20, 0x4

    #@4e
    move/from16 v0, v19

    #@50
    move/from16 v1, v20

    #@52
    if-ne v0, v1, :cond_6e

    #@54
    const-string v19, "6245"

    #@56
    move-object/from16 v0, v19

    #@58
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v19

    #@5c
    if-eqz v19, :cond_6e

    #@5e
    .line 853
    move-object/from16 v0, p0

    #@60
    invoke-direct {v0, v4, v5}, Lcom/android/internal/telephony/uicc/RuimRecords;->extractEmailAddressFromMessageBody(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    #@63
    move-result-object v14

    #@64
    .line 854
    .local v14, parts:[Ljava/lang/String;
    if-eqz v14, :cond_6e

    #@66
    .line 855
    const/16 v19, 0x0

    #@68
    aget-object v4, v14, v19

    #@6a
    .line 856
    const/16 v19, 0x1

    #@6c
    aget-object v5, v14, v19

    #@6e
    .line 860
    .end local v14           #parts:[Ljava/lang/String;
    :cond_6e
    if-nez v4, :cond_72

    #@70
    .line 861
    const-string v4, "Unknown"

    #@72
    .line 864
    :cond_72
    const-string v19, "address"

    #@74
    move-object/from16 v0, v18

    #@76
    move-object/from16 v1, v19

    #@78
    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7b
    .line 865
    const-string v19, "date"

    #@7d
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@80
    move-result-object v20

    #@81
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@84
    .line 868
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    #@87
    move-result-object v5

    #@88
    .line 869
    if-nez v5, :cond_8c

    #@8a
    .line 870
    const-string v5, "empty contents"

    #@8c
    .line 872
    :cond_8c
    const-string v19, "body"

    #@8e
    move-object/from16 v0, v18

    #@90
    move-object/from16 v1, v19

    #@92
    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@95
    .line 875
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getPseudoSubject()Ljava/lang/String;

    #@98
    move-result-object v19

    #@99
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    #@9c
    move-result v19

    #@9d
    if-lez v19, :cond_a8

    #@9f
    .line 876
    const-string v19, "subject"

    #@a1
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getPseudoSubject()Ljava/lang/String;

    #@a4
    move-result-object v20

    #@a5
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a8
    .line 879
    :cond_a8
    const-string v20, "reply_path_present"

    #@aa
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->isReplyPathPresent()Z

    #@ad
    move-result v19

    #@ae
    if-eqz v19, :cond_1fc

    #@b0
    const/16 v19, 0x1

    #@b2
    :goto_b2
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b5
    move-result-object v19

    #@b6
    move-object/from16 v0, v18

    #@b8
    move-object/from16 v1, v20

    #@ba
    move-object/from16 v2, v19

    #@bc
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@bf
    .line 882
    const-string v19, "service_center"

    #@c1
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getServiceCenterAddress()Ljava/lang/String;

    #@c4
    move-result-object v20

    #@c5
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c8
    .line 885
    const-string v19, "lgeMsgType"

    #@ca
    const/16 v20, 0x7

    #@cc
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cf
    move-result-object v20

    #@d0
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@d3
    .line 888
    const-string v19, "index_on_icc"

    #@d5
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getIndexOnIcc()I

    #@d8
    move-result v20

    #@d9
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@dc
    move-result-object v20

    #@dd
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@e0
    .line 892
    const/16 v19, 0x0

    #@e2
    const-string v20, "uicc_csim"

    #@e4
    invoke-static/range {v19 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e7
    move-result v19

    #@e8
    if-eqz v19, :cond_fa

    #@ea
    .line 893
    const-string v19, "sms_format"

    #@ec
    const/16 v20, 0x2

    #@ee
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f1
    move-result-object v20

    #@f2
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@f5
    .line 894
    const-string v19, "insertSmsDB(), SMS_FORMAT : SMS_FORMAT_CSIM: 2"

    #@f7
    invoke-static/range {v19 .. v19}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@fa
    .line 899
    :cond_fa
    invoke-static {}, Landroid/telephony/MSimSmsManager;->getDefault()Landroid/telephony/MSimSmsManager;

    #@fd
    move-result-object v19

    #@fe
    invoke-virtual/range {v19 .. v19}, Landroid/telephony/MSimSmsManager;->getPreferredSmsSubscription()I

    #@101
    move-result v17

    #@102
    .line 900
    .local v17, subId:I
    const-string v19, "sub_id"

    #@104
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@107
    move-result-object v20

    #@108
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@10b
    .line 902
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@10e
    move-result-object v19

    #@10f
    invoke-virtual/range {v19 .. v19}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@112
    move-result v19

    #@113
    if-eqz v19, :cond_143

    #@115
    .line 903
    const-string v3, "sms_imsi_data"

    #@117
    .line 904
    .local v3, SMS_IMSI_DATA:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@11a
    move-result-object v19

    #@11b
    move-object/from16 v0, v19

    #@11d
    move/from16 v1, v17

    #@11f
    invoke-virtual {v0, v1}, Landroid/telephony/MSimTelephonyManager;->getSimSerialNumber(I)Ljava/lang/String;

    #@122
    move-result-object v11

    #@123
    .line 905
    .local v11, iccd:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@126
    move-result-object v19

    #@127
    move-object/from16 v0, v19

    #@129
    move/from16 v1, v17

    #@12b
    invoke-virtual {v0, v1}, Landroid/telephony/MSimTelephonyManager;->getSubscriberId(I)Ljava/lang/String;

    #@12e
    move-result-object v12

    #@12f
    .line 906
    .local v12, imsi:Ljava/lang/String;
    const/4 v13, 0x0

    #@130
    .line 907
    .local v13, mIccImsi:Ljava/lang/String;
    if-nez v11, :cond_204

    #@132
    if-nez v12, :cond_204

    #@134
    .line 908
    const/16 v19, 0x1

    #@136
    move/from16 v0, v17

    #@138
    move/from16 v1, v19

    #@13a
    if-ne v0, v1, :cond_200

    #@13c
    const-string v13, "sim2"

    #@13e
    .line 912
    :goto_13e
    move-object/from16 v0, v18

    #@140
    invoke-virtual {v0, v3, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_143
    .catch Ljava/lang/NullPointerException; {:try_start_13 .. :try_end_143} :catch_181

    #@143
    .line 917
    .end local v3           #SMS_IMSI_DATA:Ljava/lang/String;
    .end local v11           #iccd:Ljava/lang/String;
    .end local v12           #imsi:Ljava/lang/String;
    .end local v13           #mIccImsi:Ljava/lang/String;
    :cond_143
    :try_start_143
    move-object/from16 v0, p0

    #@145
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@147
    move-object/from16 v19, v0

    #@149
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@14c
    move-result-object v19

    #@14d
    move-object/from16 v0, v19

    #@14f
    move-object/from16 v1, v18

    #@151
    invoke-virtual {v0, v15, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@154
    move-result-object v16

    #@155
    .line 918
    if-nez v16, :cond_15c

    #@157
    .line 919
    const-string v19, "insertSmsDB(), smsInsertUri is null"

    #@159
    invoke-static/range {v19 .. v19}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_15c
    .catch Landroid/database/SQLException; {:try_start_143 .. :try_end_15c} :catch_21b
    .catch Ljava/lang/NullPointerException; {:try_start_143 .. :try_end_15c} :catch_181

    #@15c
    .line 927
    .end local v17           #subId:I
    :cond_15c
    :goto_15c
    return-object v16

    #@15d
    .line 804
    :sswitch_15d
    const/4 v6, 0x1

    #@15e
    .line 805
    const/4 v7, 0x1

    #@15f
    .line 806
    :try_start_15f
    sget-object v15, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_INBOX_URI:Landroid/net/Uri;

    #@161
    .line 807
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    #@164
    move-result-object v4

    #@165
    .line 808
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTimestampMillis()J

    #@168
    move-result-wide v8

    #@169
    .line 809
    const-string v19, "read"

    #@16b
    const/16 v20, 0x1

    #@16d
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@170
    move-result-object v20

    #@171
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@174
    .line 810
    const-string v19, "seen"

    #@176
    const/16 v20, 0x1

    #@178
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17b
    move-result-object v20

    #@17c
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_17f
    .catch Ljava/lang/NullPointerException; {:try_start_15f .. :try_end_17f} :catch_181

    #@17f
    goto/16 :goto_33

    #@181
    .line 924
    :catch_181
    move-exception v10

    #@182
    .line 925
    .local v10, e:Ljava/lang/NullPointerException;
    new-instance v19, Ljava/lang/StringBuilder;

    #@184
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@187
    const-string v20, "insertSmsDB(), "

    #@189
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18c
    move-result-object v19

    #@18d
    invoke-virtual {v10}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    #@190
    move-result-object v20

    #@191
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v19

    #@195
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v19

    #@199
    invoke-static/range {v19 .. v19}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@19c
    goto :goto_15c

    #@19d
    .line 813
    .end local v10           #e:Ljava/lang/NullPointerException;
    :sswitch_19d
    const/4 v6, 0x1

    #@19e
    .line 814
    const/4 v7, 0x1

    #@19f
    .line 815
    :try_start_19f
    sget-object v15, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_INBOX_URI:Landroid/net/Uri;

    #@1a1
    .line 816
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    #@1a4
    move-result-object v4

    #@1a5
    .line 817
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTimestampMillis()J

    #@1a8
    move-result-wide v8

    #@1a9
    .line 818
    const-string v19, "read"

    #@1ab
    const/16 v20, 0x0

    #@1ad
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b0
    move-result-object v20

    #@1b1
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1b4
    .line 819
    const-string v19, "seen"

    #@1b6
    const/16 v20, 0x0

    #@1b8
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1bb
    move-result-object v20

    #@1bc
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1bf
    goto/16 :goto_33

    #@1c1
    .line 822
    :sswitch_1c1
    const/4 v6, 0x5

    #@1c2
    .line 823
    const/4 v7, 0x0

    #@1c3
    .line 824
    sget-object v15, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_OUTBOX_URI:Landroid/net/Uri;

    #@1c5
    .line 826
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    #@1c8
    move-result-object v4

    #@1c9
    .line 827
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1cc
    move-result-wide v8

    #@1cd
    .line 828
    const-string v19, "read"

    #@1cf
    const/16 v20, 0x1

    #@1d1
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d4
    move-result-object v20

    #@1d5
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1d8
    .line 829
    const-string v19, "seen"

    #@1da
    const/16 v20, 0x1

    #@1dc
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1df
    move-result-object v20

    #@1e0
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1e3
    .line 830
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getStatusReportReq()I

    #@1e6
    move-result v19

    #@1e7
    const/16 v20, 0x1

    #@1e9
    move/from16 v0, v19

    #@1eb
    move/from16 v1, v20

    #@1ed
    if-ne v0, v1, :cond_33

    #@1ef
    .line 831
    const-string v19, "status"

    #@1f1
    const/16 v20, 0x20

    #@1f3
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f6
    move-result-object v20

    #@1f7
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1fa
    goto/16 :goto_33

    #@1fc
    .line 879
    :cond_1fc
    const/16 v19, 0x0

    #@1fe
    goto/16 :goto_b2

    #@200
    .line 908
    .restart local v3       #SMS_IMSI_DATA:Ljava/lang/String;
    .restart local v11       #iccd:Ljava/lang/String;
    .restart local v12       #imsi:Ljava/lang/String;
    .restart local v13       #mIccImsi:Ljava/lang/String;
    .restart local v17       #subId:I
    :cond_200
    const-string v13, "sim1"

    #@202
    goto/16 :goto_13e

    #@204
    .line 910
    :cond_204
    new-instance v19, Ljava/lang/StringBuilder;

    #@206
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@209
    move-object/from16 v0, v19

    #@20b
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20e
    move-result-object v19

    #@20f
    move-object/from16 v0, v19

    #@211
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@214
    move-result-object v19

    #@215
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@218
    move-result-object v13

    #@219
    goto/16 :goto_13e

    #@21b
    .line 921
    .end local v3           #SMS_IMSI_DATA:Ljava/lang/String;
    .end local v11           #iccd:Ljava/lang/String;
    .end local v12           #imsi:Ljava/lang/String;
    .end local v13           #mIccImsi:Ljava/lang/String;
    :catch_21b
    move-exception v10

    #@21c
    .line 922
    .local v10, e:Landroid/database/SQLException;
    const-string v19, "insertSmsDB(), SQL Insert error"

    #@21e
    invoke-static/range {v19 .. v19}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_221
    .catch Ljava/lang/NullPointerException; {:try_start_19f .. :try_end_221} :catch_181

    #@221
    goto/16 :goto_15c

    #@223
    .line 802
    nop

    #@224
    :sswitch_data_224
    .sparse-switch
        0x1 -> :sswitch_15d
        0x3 -> :sswitch_19d
        0x7 -> :sswitch_1c1
    .end sparse-switch
.end method

.method private insertSmsDBForConcat(ILcom/android/internal/telephony/cdma/SmsMessage;)V
    .registers 31
    .parameter "statusOnSim"
    .parameter "sms"

    #@0
    .prologue
    .line 931
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@3
    move-result-object v22

    #@4
    .line 932
    .local v22, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    move-object/from16 v0, v22

    #@6
    iget-object v2, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@8
    iget v0, v2, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@a
    move/from16 v16, v0

    #@c
    .line 933
    .local v16, msgCount:I
    move-object/from16 v0, v22

    #@e
    iget-object v2, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@10
    iget v0, v2, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@12
    move/from16 v21, v0

    #@14
    .line 934
    .local v21, seqNum:I
    move-object/from16 v0, v22

    #@16
    iget-object v2, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@18
    iget v0, v2, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@1a
    move/from16 v18, v0

    #@1c
    .line 936
    .local v18, refNum:I
    const/16 v17, 0x0

    #@1e
    .line 937
    .local v17, numOfQueriedItem:I
    const/16 v19, 0x0

    #@20
    .line 939
    .local v19, saparate_flag:Z
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "reference = "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    move/from16 v0, v18

    #@2d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    const-string v3, " and count = "

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    move/from16 v0, v16

    #@39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    .line 940
    .local v5, selection:Ljava/lang/String;
    move-object/from16 v0, p0

    #@43
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@45
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@48
    move-result-object v2

    #@49
    sget-object v3, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@4b
    const/4 v4, 0x0

    #@4c
    const/4 v6, 0x0

    #@4d
    const/4 v7, 0x0

    #@4e
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@51
    move-result-object v9

    #@52
    .line 942
    .local v9, c:Landroid/database/Cursor;
    if-eqz v9, :cond_208

    #@54
    .line 944
    :try_start_54
    new-instance v27, Landroid/content/ContentValues;

    #@56
    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    #@59
    .line 945
    .local v27, values:Landroid/content/ContentValues;
    const/16 v25, 0x0

    #@5b
    .line 946
    .local v25, smsUri:Landroid/net/Uri;
    const/4 v14, 0x0

    #@5c
    .line 947
    .local v14, iccSring:Ljava/lang/String;
    const/4 v15, 0x0

    #@5d
    .line 948
    .local v15, indexOnIcc:I
    const-wide/16 v23, 0x0

    #@5f
    .line 949
    .local v23, smsId:J
    const/16 v20, 0x0

    #@61
    .line 951
    .local v20, seq:I
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    #@64
    move-result v2

    #@65
    add-int/lit8 v17, v2, 0x1

    #@67
    .line 952
    new-instance v2, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v3, "insertSmsDBForConcat(), msgCount: "

    #@6e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v2

    #@72
    move/from16 v0, v16

    #@74
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    const-string v3, ", numOfQueriedItem: "

    #@7a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    move/from16 v0, v17

    #@80
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v2

    #@88
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@8b
    .line 953
    move/from16 v0, v16

    #@8d
    move/from16 v1, v17

    #@8f
    if-ge v0, v1, :cond_98

    #@91
    .line 954
    const-string v2, "insertSmsDBForConcat(),  Abnormal case!, separate following cocats"

    #@93
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@96
    .line 955
    const/16 v19, 0x1

    #@98
    .line 957
    :cond_98
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    #@9b
    move-result v2

    #@9c
    if-eqz v2, :cond_258

    #@9e
    if-nez v19, :cond_258

    #@a0
    .line 960
    new-instance v8, Ljava/util/ArrayList;

    #@a2
    move/from16 v0, v16

    #@a4
    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@a7
    .line 961
    .local v8, body_part:Ljava/util/ArrayList;
    const/4 v13, 0x0

    #@a8
    .local v13, i:I
    :goto_a8
    move/from16 v0, v16

    #@aa
    if-ge v13, v0, :cond_b4

    #@ac
    .line 962
    const-string v2, "/*missing text*/"

    #@ae
    invoke-virtual {v8, v13, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@b1
    .line 961
    add-int/lit8 v13, v13, 0x1

    #@b3
    goto :goto_a8

    #@b4
    .line 964
    :cond_b4
    new-instance v14, Ljava/lang/String;

    #@b6
    .end local v14           #iccSring:Ljava/lang/String;
    invoke-direct {v14}, Ljava/lang/String;-><init>()V

    #@b9
    .line 965
    .restart local v14       #iccSring:Ljava/lang/String;
    const-string v2, "sms_id"

    #@bb
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@be
    move-result v2

    #@bf
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    #@c2
    move-result-wide v23

    #@c3
    .line 967
    :cond_c3
    const-string v2, "sequence"

    #@c5
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@c8
    move-result v2

    #@c9
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    #@cc
    move-result v20

    #@cd
    .line 968
    add-int/lit8 v2, v20, -0x1

    #@cf
    const-string v3, "body"

    #@d1
    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@d4
    move-result v3

    #@d5
    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@d8
    move-result-object v3

    #@d9
    invoke-virtual {v8, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@dc
    .line 969
    const-string v2, "icc_index"

    #@de
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@e1
    move-result v2

    #@e2
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    #@e5
    move-result v15

    #@e6
    .line 970
    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@e9
    move-result-object v2

    #@ea
    invoke-virtual {v14, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@ed
    move-result-object v14

    #@ee
    .line 971
    const-string v2, ","

    #@f0
    invoke-virtual {v14, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@f3
    move-result-object v14

    #@f4
    .line 972
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@f7
    move-result v2

    #@f8
    if-nez v2, :cond_c3

    #@fa
    .line 974
    add-int/lit8 v2, v21, -0x1

    #@fc
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    #@ff
    move-result-object v3

    #@100
    invoke-virtual {v8, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@103
    .line 975
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getIndexOnIcc()I

    #@106
    move-result v2

    #@107
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@10a
    move-result-object v2

    #@10b
    invoke-virtual {v14, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@10e
    move-result-object v14

    #@10f
    .line 976
    const-string v10, ""

    #@111
    .line 977
    .local v10, concatBody:Ljava/lang/String;
    const/4 v13, 0x0

    #@112
    :goto_112
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@115
    move-result v2

    #@116
    if-ge v13, v2, :cond_130

    #@118
    .line 978
    new-instance v2, Ljava/lang/StringBuilder;

    #@11a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11d
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v2

    #@121
    invoke-virtual {v8, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@124
    move-result-object v3

    #@125
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v2

    #@129
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12c
    move-result-object v10

    #@12d
    .line 977
    add-int/lit8 v13, v13, 0x1

    #@12f
    goto :goto_112

    #@130
    .line 982
    :cond_130
    const-string v2, "sms_id"

    #@132
    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@135
    move-result-object v3

    #@136
    move-object/from16 v0, v27

    #@138
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@13b
    .line 983
    const-string v2, "body"

    #@13d
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    #@140
    move-result-object v3

    #@141
    move-object/from16 v0, v27

    #@143
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@146
    .line 984
    const-string v2, "reference"

    #@148
    move-object/from16 v0, v22

    #@14a
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@14c
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@14e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@151
    move-result-object v3

    #@152
    move-object/from16 v0, v27

    #@154
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@157
    .line 985
    const-string v2, "count"

    #@159
    move-object/from16 v0, v22

    #@15b
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@15d
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@15f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@162
    move-result-object v3

    #@163
    move-object/from16 v0, v27

    #@165
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@168
    .line 986
    const-string v2, "sequence"

    #@16a
    move-object/from16 v0, v22

    #@16c
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@16e
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@170
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@173
    move-result-object v3

    #@174
    move-object/from16 v0, v27

    #@176
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@179
    .line 987
    const-string v2, "icc_index"

    #@17b
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getIndexOnIcc()I

    #@17e
    move-result v3

    #@17f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@182
    move-result-object v3

    #@183
    move-object/from16 v0, v27

    #@185
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_188
    .catchall {:try_start_54 .. :try_end_188} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_54 .. :try_end_188} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_54 .. :try_end_188} :catch_235

    #@188
    .line 990
    :try_start_188
    move-object/from16 v0, p0

    #@18a
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@18c
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@18f
    move-result-object v2

    #@190
    sget-object v3, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@192
    move-object/from16 v0, v27

    #@194
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@197
    move-result-object v25

    #@198
    .line 991
    if-nez v25, :cond_19f

    #@19a
    .line 992
    const-string v2, "insertSmsDBForConcat(), smsUri of SMS_CONCAT_URI is null"

    #@19c
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_19f
    .catchall {:try_start_188 .. :try_end_19f} :catchall_2e4
    .catch Landroid/database/SQLException; {:try_start_188 .. :try_end_19f} :catch_209
    .catch Ljava/lang/NullPointerException; {:try_start_188 .. :try_end_19f} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_188 .. :try_end_19f} :catch_235

    #@19f
    .line 999
    :cond_19f
    :goto_19f
    :try_start_19f
    invoke-virtual/range {v27 .. v27}, Landroid/content/ContentValues;->clear()V

    #@1a2
    .line 1000
    const-string v2, "body"

    #@1a4
    move-object/from16 v0, v27

    #@1a6
    invoke-virtual {v0, v2, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1a9
    .line 1001
    const-string v2, "index_on_icc"

    #@1ab
    move-object/from16 v0, v27

    #@1ad
    invoke-virtual {v0, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1b0
    .line 1003
    const/4 v2, 0x0

    #@1b1
    const-string v3, "uicc_csim"

    #@1b3
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1b6
    move-result v2

    #@1b7
    if-eqz v2, :cond_1c5

    #@1b9
    .line 1004
    const-string v2, "sms_format"

    #@1bb
    const/4 v3, 0x2

    #@1bc
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1bf
    move-result-object v3

    #@1c0
    move-object/from16 v0, v27

    #@1c2
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_1c5
    .catchall {:try_start_19f .. :try_end_1c5} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_19f .. :try_end_1c5} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_19f .. :try_end_1c5} :catch_235

    #@1c5
    .line 1009
    :cond_1c5
    :try_start_1c5
    move-object/from16 v0, p0

    #@1c7
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@1c9
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1cc
    move-result-object v2

    #@1cd
    sget-object v3, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_URI:Landroid/net/Uri;

    #@1cf
    new-instance v4, Ljava/lang/StringBuilder;

    #@1d1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d4
    const-string v6, "_id="

    #@1d6
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v4

    #@1da
    move-wide/from16 v0, v23

    #@1dc
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v4

    #@1e0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e3
    move-result-object v4

    #@1e4
    const/4 v6, 0x0

    #@1e5
    move-object/from16 v0, v27

    #@1e7
    invoke-virtual {v2, v3, v0, v4, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@1ea
    move-result v26

    #@1eb
    .line 1010
    .local v26, updateResult:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@1ed
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f0
    const-string v3, "insertSmsDBForConcat(), updateResult = "

    #@1f2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v2

    #@1f6
    move/from16 v0, v26

    #@1f8
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fb
    move-result-object v2

    #@1fc
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ff
    move-result-object v2

    #@200
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_203
    .catchall {:try_start_1c5 .. :try_end_203} :catchall_2e4
    .catch Landroid/database/SQLException; {:try_start_1c5 .. :try_end_203} :catch_22e
    .catch Ljava/lang/NullPointerException; {:try_start_1c5 .. :try_end_203} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1c5 .. :try_end_203} :catch_235

    #@203
    .line 1042
    .end local v8           #body_part:Ljava/util/ArrayList;
    .end local v10           #concatBody:Ljava/lang/String;
    .end local v13           #i:I
    .end local v26           #updateResult:I
    :cond_203
    :goto_203
    if-eqz v9, :cond_208

    #@205
    .line 1043
    .end local v14           #iccSring:Ljava/lang/String;
    .end local v15           #indexOnIcc:I
    .end local v20           #seq:I
    .end local v23           #smsId:J
    .end local v25           #smsUri:Landroid/net/Uri;
    .end local v27           #values:Landroid/content/ContentValues;
    :goto_205
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@208
    .line 1047
    :cond_208
    return-void

    #@209
    .line 994
    .restart local v8       #body_part:Ljava/util/ArrayList;
    .restart local v10       #concatBody:Ljava/lang/String;
    .restart local v13       #i:I
    .restart local v14       #iccSring:Ljava/lang/String;
    .restart local v15       #indexOnIcc:I
    .restart local v20       #seq:I
    .restart local v23       #smsId:J
    .restart local v25       #smsUri:Landroid/net/Uri;
    .restart local v27       #values:Landroid/content/ContentValues;
    :catch_209
    move-exception v11

    #@20a
    .line 995
    .local v11, e:Landroid/database/SQLException;
    :try_start_20a
    const-string v2, "insertSmsDBForConcat(), SQL Insert error"

    #@20c
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_20f
    .catchall {:try_start_20a .. :try_end_20f} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_20a .. :try_end_20f} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_20a .. :try_end_20f} :catch_235

    #@20f
    goto :goto_19f

    #@210
    .line 1034
    .end local v8           #body_part:Ljava/util/ArrayList;
    .end local v10           #concatBody:Ljava/lang/String;
    .end local v11           #e:Landroid/database/SQLException;
    .end local v13           #i:I
    .end local v14           #iccSring:Ljava/lang/String;
    .end local v15           #indexOnIcc:I
    .end local v20           #seq:I
    .end local v23           #smsId:J
    .end local v25           #smsUri:Landroid/net/Uri;
    .end local v27           #values:Landroid/content/ContentValues;
    :catch_210
    move-exception v11

    #@211
    .line 1035
    .local v11, e:Ljava/lang/NullPointerException;
    :try_start_211
    new-instance v2, Ljava/lang/StringBuilder;

    #@213
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@216
    const-string v3, "insertSmsDBForConcat(), "

    #@218
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21b
    move-result-object v2

    #@21c
    invoke-virtual {v11}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    #@21f
    move-result-object v3

    #@220
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v2

    #@224
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@227
    move-result-object v2

    #@228
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_22b
    .catchall {:try_start_211 .. :try_end_22b} :catchall_2e4

    #@22b
    .line 1042
    if-eqz v9, :cond_208

    #@22d
    goto :goto_205

    #@22e
    .line 1011
    .end local v11           #e:Ljava/lang/NullPointerException;
    .restart local v8       #body_part:Ljava/util/ArrayList;
    .restart local v10       #concatBody:Ljava/lang/String;
    .restart local v13       #i:I
    .restart local v14       #iccSring:Ljava/lang/String;
    .restart local v15       #indexOnIcc:I
    .restart local v20       #seq:I
    .restart local v23       #smsId:J
    .restart local v25       #smsUri:Landroid/net/Uri;
    .restart local v27       #values:Landroid/content/ContentValues;
    :catch_22e
    move-exception v11

    #@22f
    .line 1012
    .local v11, e:Landroid/database/SQLException;
    :try_start_22f
    const-string v2, "insertSmsDBForConcat(), SQL update error"

    #@231
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_234
    .catchall {:try_start_22f .. :try_end_234} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_22f .. :try_end_234} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_22f .. :try_end_234} :catch_235

    #@234
    goto :goto_203

    #@235
    .line 1037
    .end local v8           #body_part:Ljava/util/ArrayList;
    .end local v10           #concatBody:Ljava/lang/String;
    .end local v11           #e:Landroid/database/SQLException;
    .end local v13           #i:I
    .end local v14           #iccSring:Ljava/lang/String;
    .end local v15           #indexOnIcc:I
    .end local v20           #seq:I
    .end local v23           #smsId:J
    .end local v25           #smsUri:Landroid/net/Uri;
    .end local v27           #values:Landroid/content/ContentValues;
    :catch_235
    move-exception v11

    #@236
    .line 1038
    .local v11, e:Ljava/lang/IndexOutOfBoundsException;
    :try_start_236
    const-string v2, "insertSmsDBForConcat(), IndexOutOfBoundsException"

    #@238
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@23b
    .line 1039
    new-instance v2, Ljava/lang/StringBuilder;

    #@23d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@240
    const-string v3, "insertSmsDBForConcat()"

    #@242
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@245
    move-result-object v2

    #@246
    invoke-virtual {v11}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    #@249
    move-result-object v3

    #@24a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24d
    move-result-object v2

    #@24e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@251
    move-result-object v2

    #@252
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_255
    .catchall {:try_start_236 .. :try_end_255} :catchall_2e4

    #@255
    .line 1042
    if-eqz v9, :cond_208

    #@257
    goto :goto_205

    #@258
    .line 1016
    .end local v11           #e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v14       #iccSring:Ljava/lang/String;
    .restart local v15       #indexOnIcc:I
    .restart local v20       #seq:I
    .restart local v23       #smsId:J
    .restart local v25       #smsUri:Landroid/net/Uri;
    .restart local v27       #values:Landroid/content/ContentValues;
    :cond_258
    :try_start_258
    invoke-direct/range {p0 .. p2}, Lcom/android/internal/telephony/uicc/RuimRecords;->insertSmsDB(ILcom/android/internal/telephony/cdma/SmsMessage;)Landroid/net/Uri;

    #@25b
    move-result-object v12

    #@25c
    .line 1019
    .local v12, firstInsertUri:Landroid/net/Uri;
    const-string v2, "reference"

    #@25e
    move-object/from16 v0, v22

    #@260
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@262
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@264
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@267
    move-result-object v3

    #@268
    move-object/from16 v0, v27

    #@26a
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@26d
    .line 1020
    const-string v2, "count"

    #@26f
    move-object/from16 v0, v22

    #@271
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@273
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@275
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@278
    move-result-object v3

    #@279
    move-object/from16 v0, v27

    #@27b
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@27e
    .line 1021
    const-string v2, "sequence"

    #@280
    move-object/from16 v0, v22

    #@282
    iget-object v3, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@284
    iget v3, v3, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@286
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@289
    move-result-object v3

    #@28a
    move-object/from16 v0, v27

    #@28c
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@28f
    .line 1022
    const-string v2, "icc_index"

    #@291
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getIndexOnIcc()I

    #@294
    move-result v3

    #@295
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@298
    move-result-object v3

    #@299
    move-object/from16 v0, v27

    #@29b
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@29e
    .line 1023
    const-string v3, "sms_id"

    #@2a0
    invoke-virtual {v12}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@2a3
    move-result-object v2

    #@2a4
    const/4 v4, 0x0

    #@2a5
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2a8
    move-result-object v2

    #@2a9
    check-cast v2, Ljava/lang/String;

    #@2ab
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@2ae
    move-result-wide v6

    #@2af
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2b2
    move-result-object v2

    #@2b3
    move-object/from16 v0, v27

    #@2b5
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@2b8
    .line 1024
    const-string v2, "body"

    #@2ba
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    #@2bd
    move-result-object v3

    #@2be
    move-object/from16 v0, v27

    #@2c0
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2c3
    .catchall {:try_start_258 .. :try_end_2c3} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_258 .. :try_end_2c3} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_258 .. :try_end_2c3} :catch_235

    #@2c3
    .line 1026
    :try_start_2c3
    move-object/from16 v0, p0

    #@2c5
    iget-object v2, v0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@2c7
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2ca
    move-result-object v2

    #@2cb
    sget-object v3, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@2cd
    move-object/from16 v0, v27

    #@2cf
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@2d2
    move-result-object v25

    #@2d3
    .line 1027
    if-nez v25, :cond_203

    #@2d5
    .line 1028
    const-string v2, "insertSmsDBForConcat(), smsUri of SMS_CONCAT_URI is null"

    #@2d7
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_2da
    .catchall {:try_start_2c3 .. :try_end_2da} :catchall_2e4
    .catch Landroid/database/SQLException; {:try_start_2c3 .. :try_end_2da} :catch_2dc
    .catch Ljava/lang/NullPointerException; {:try_start_2c3 .. :try_end_2da} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2c3 .. :try_end_2da} :catch_235

    #@2da
    goto/16 :goto_203

    #@2dc
    .line 1030
    :catch_2dc
    move-exception v11

    #@2dd
    .line 1031
    .local v11, e:Landroid/database/SQLException;
    :try_start_2dd
    const-string v2, "insertSmsDBForConcat(), SQL Insert error"

    #@2df
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_2e2
    .catchall {:try_start_2dd .. :try_end_2e2} :catchall_2e4
    .catch Ljava/lang/NullPointerException; {:try_start_2dd .. :try_end_2e2} :catch_210
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2dd .. :try_end_2e2} :catch_235

    #@2e2
    goto/16 :goto_203

    #@2e4
    .line 1042
    .end local v11           #e:Landroid/database/SQLException;
    .end local v12           #firstInsertUri:Landroid/net/Uri;
    .end local v14           #iccSring:Ljava/lang/String;
    .end local v15           #indexOnIcc:I
    .end local v20           #seq:I
    .end local v23           #smsId:J
    .end local v25           #smsUri:Landroid/net/Uri;
    .end local v27           #values:Landroid/content/ContentValues;
    :catchall_2e4
    move-exception v2

    #@2e5
    if-eqz v9, :cond_2ea

    #@2e7
    .line 1043
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@2ea
    .line 1042
    :cond_2ea
    throw v2
.end method

.method private onGetCSimEprlDone(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    .line 535
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3
    check-cast v2, [B

    #@5
    move-object v0, v2

    #@6
    check-cast v0, [B

    #@8
    .line 536
    .local v0, data:[B
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "CSIM_EPRL="

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@22
    .line 539
    array-length v2, v0

    #@23
    if-le v2, v4, :cond_38

    #@25
    .line 540
    const/4 v2, 0x2

    #@26
    aget-byte v2, v0, v2

    #@28
    and-int/lit16 v2, v2, 0xff

    #@2a
    shl-int/lit8 v2, v2, 0x8

    #@2c
    aget-byte v3, v0, v4

    #@2e
    and-int/lit16 v3, v3, 0xff

    #@30
    or-int v1, v2, v3

    #@32
    .line 541
    .local v1, prlId:I
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mPrlVersion:Ljava/lang/String;

    #@38
    .line 543
    .end local v1           #prlId:I
    :cond_38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "CSIM PRL version="

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mPrlVersion:Ljava/lang/String;

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@50
    .line 544
    return-void
.end method

.method private setLocaleFromCsim()V
    .registers 6

    #@0
    .prologue
    .line 1168
    const/4 v2, 0x0

    #@1
    .line 1170
    .local v2, prefLang:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mEFli:[B

    #@3
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/RuimRecords;->findBestLanguage([B)Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    .line 1172
    if-nez v2, :cond_f

    #@9
    .line 1173
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mEFpl:[B

    #@b
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/RuimRecords;->findBestLanguage([B)Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    .line 1176
    :cond_f
    if-eqz v2, :cond_4c

    #@11
    .line 1178
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->getIMSI()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 1179
    .local v1, imsi:Ljava/lang/String;
    const/4 v0, 0x0

    #@16
    .line 1180
    .local v0, country:Ljava/lang/String;
    if-eqz v1, :cond_26

    #@18
    .line 1181
    const/4 v3, 0x0

    #@19
    const/4 v4, 0x3

    #@1a
    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@21
    move-result v3

    #@22
    invoke-static {v3}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    .line 1184
    :cond_26
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v4, "Setting locale to "

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    const-string v4, "_"

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@46
    .line 1185
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@48
    invoke-static {v3, v2, v0}, Lcom/android/internal/telephony/MccTable;->setSystemLocale(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@4b
    .line 1189
    .end local v0           #country:Ljava/lang/String;
    .end local v1           #imsi:Ljava/lang/String;
    :goto_4b
    return-void

    #@4c
    .line 1187
    :cond_4c
    const-string v3, "No suitable CSIM selected locale"

    #@4e
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@51
    goto :goto_4b
.end method

.method private setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "key"
    .parameter "val"

    #@0
    .prologue
    .line 1464
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_d

    #@a
    .line 1465
    invoke-static {p1, p2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 1467
    :cond_d
    return-void
.end method

.method private updateMessagefromCSim(Lcom/android/internal/telephony/cdma/SmsMessage;)V
    .registers 4
    .parameter "sms"

    #@0
    .prologue
    .line 1050
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@3
    move-result-object v0

    #@4
    .line 1051
    .local v0, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    if-eqz v0, :cond_a

    #@6
    iget-object v1, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@8
    if-nez v1, :cond_12

    #@a
    .line 1052
    :cond_a
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getStatusOnIcc()I

    #@d
    move-result v1

    #@e
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/uicc/RuimRecords;->insertSmsDB(ILcom/android/internal/telephony/cdma/SmsMessage;)Landroid/net/Uri;

    #@11
    .line 1056
    :goto_11
    return-void

    #@12
    .line 1054
    :cond_12
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getStatusOnIcc()I

    #@15
    move-result v1

    #@16
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/uicc/RuimRecords;->insertSmsDBForConcat(ILcom/android/internal/telephony/cdma/SmsMessage;)V

    #@19
    goto :goto_11
.end method


# virtual methods
.method public dispose()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "Disposing RuimRecords "

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@17
    .line 179
    sput-boolean v2, Lcom/android/internal/telephony/uicc/RuimRecords;->iccidToLoad:Z

    #@19
    .line 180
    sput-boolean v2, Lcom/android/internal/telephony/uicc/RuimRecords;->isSimChanged:Z

    #@1b
    .line 182
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@1d
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForIccRefresh(Landroid/os/Handler;)V

    #@20
    .line 183
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@22
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForReady(Landroid/os/Handler;)V

    #@25
    .line 184
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->resetRecords()V

    #@28
    .line 185
    invoke-super {p0}, Lcom/android/internal/telephony/uicc/IccRecords;->dispose()V

    #@2b
    .line 186
    return-void
.end method

.method protected finalize()V
    .registers 2

    #@0
    .prologue
    .line 190
    const-string v0, "RuimRecords finalized"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@5
    .line 191
    return-void
.end method

.method public getCdmaMin()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 226
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMin2Min1:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getCsimSpnDisplayCondition()Z
    .registers 2

    #@0
    .prologue
    .line 1449
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mCsimSpnDisplayCondition:Z

    #@2
    return v0
.end method

.method public getDisplayRule(Ljava/lang/String;)I
    .registers 3
    .parameter "plmn"

    #@0
    .prologue
    .line 1338
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getIMSI()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 218
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMdn()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1433
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMdn:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMdnNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMyMobileNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMin()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1437
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMin:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getNid()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1445
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mHomeNetworkId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getOperatorNumeric()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 273
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@4
    if-nez v1, :cond_8

    #@6
    .line 274
    const/4 v1, 0x0

    #@7
    .line 302
    :goto_7
    return-object v1

    #@8
    .line 279
    :cond_8
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    const-string v2, "VZW"

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_20

    #@14
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    const-string v2, "SPR"

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_4a

    #@20
    .line 280
    :cond_20
    sget v1, Lcom/android/internal/telephony/uicc/RuimRecords;->mncLengthAd:I

    #@22
    if-eq v1, v4, :cond_4a

    #@24
    sget v1, Lcom/android/internal/telephony/uicc/RuimRecords;->mncLengthAd:I

    #@26
    if-eqz v1, :cond_4a

    #@28
    sget v1, Lcom/android/internal/telephony/uicc/RuimRecords;->mncLengthAd:I

    #@2a
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@2c
    if-eq v1, v2, :cond_4a

    #@2e
    .line 281
    sget v1, Lcom/android/internal/telephony/uicc/RuimRecords;->mncLengthAd:I

    #@30
    iput v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@32
    .line 282
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v2, "called getRUIMOperatorNumeric : mncLength = "

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@4a
    .line 287
    :cond_4a
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@4c
    if-eq v1, v4, :cond_64

    #@4e
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@50
    if-eqz v1, :cond_64

    #@52
    .line 291
    sget-object v1, Lcom/android/internal/telephony/uicc/RuimRecords;->mMccMnc:Ljava/lang/String;

    #@54
    if-eqz v1, :cond_59

    #@56
    sget-object v1, Lcom/android/internal/telephony/uicc/RuimRecords;->mMccMnc:Ljava/lang/String;

    #@58
    goto :goto_7

    #@59
    :cond_59
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@5b
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@5d
    add-int/lit8 v2, v2, 0x3

    #@5f
    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@62
    move-result-object v1

    #@63
    goto :goto_7

    #@64
    .line 299
    :cond_64
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@66
    const/4 v2, 0x3

    #@67
    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6a
    move-result-object v1

    #@6b
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@6e
    move-result v0

    #@6f
    .line 302
    .local v0, mcc:I
    sget-object v1, Lcom/android/internal/telephony/uicc/RuimRecords;->mMccMnc:Ljava/lang/String;

    #@71
    if-eqz v1, :cond_76

    #@73
    sget-object v1, Lcom/android/internal/telephony/uicc/RuimRecords;->mMccMnc:Ljava/lang/String;

    #@75
    goto :goto_7

    #@76
    :cond_76
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@78
    invoke-static {v0}, Lcom/android/internal/telephony/MccTable;->smallestDigitsMccForMnc(I)I

    #@7b
    move-result v2

    #@7c
    add-int/lit8 v2, v2, 0x3

    #@7e
    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@81
    move-result-object v1

    #@82
    goto :goto_7
.end method

.method public getPrlVersion()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mPrlVersion:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSid()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1441
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mHomeSystemId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getVoiceMessageCount()I
    .registers 3

    #@0
    .prologue
    .line 1375
    const-string v0, "CDMA"

    #@2
    const-string v1, "RuimRecords:getVoiceMessageCount - NOP for CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1376
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 15
    .parameter "msg"

    #@0
    .prologue
    const/4 v12, 0x6

    #@1
    const/4 v10, 0x1

    #@2
    .line 552
    const/4 v4, 0x0

    #@3
    .line 554
    .local v4, isRecordLoadResponse:Z
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@5
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@8
    move-result v8

    #@9
    if-eqz v8, :cond_34

    #@b
    .line 555
    new-instance v8, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v9, "Received message "

    #@12
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v8

    #@16
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v8

    #@1a
    const-string v9, "["

    #@1c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v8

    #@20
    iget v9, p1, Landroid/os/Message;->what:I

    #@22
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v8

    #@26
    const-string v9, "] while being destroyed. Ignoring."

    #@28
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v8

    #@2c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->loge(Ljava/lang/String;)V

    #@33
    .line 724
    :cond_33
    :goto_33
    return-void

    #@34
    .line 560
    :cond_34
    :try_start_34
    iget v8, p1, Landroid/os/Message;->what:I

    #@36
    sparse-switch v8, :sswitch_data_264

    #@39
    .line 713
    invoke-super {p0, p1}, Lcom/android/internal/telephony/uicc/IccRecords;->handleMessage(Landroid/os/Message;)V
    :try_end_3c
    .catchall {:try_start_34 .. :try_end_3c} :catchall_57
    .catch Ljava/lang/RuntimeException; {:try_start_34 .. :try_end_3c} :catch_46

    #@3c
    .line 720
    :cond_3c
    :goto_3c
    if-eqz v4, :cond_33

    #@3e
    .line 721
    :goto_3e
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->onRecordLoaded()V

    #@41
    goto :goto_33

    #@42
    .line 562
    :sswitch_42
    :try_start_42
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->onReady()V
    :try_end_45
    .catchall {:try_start_42 .. :try_end_45} :catchall_57
    .catch Ljava/lang/RuntimeException; {:try_start_42 .. :try_end_45} :catch_46

    #@45
    goto :goto_3c

    #@46
    .line 715
    :catch_46
    move-exception v3

    #@47
    .line 717
    .local v3, exc:Ljava/lang/RuntimeException;
    :try_start_47
    const-string v8, "CDMA"

    #@49
    const-string v9, "Exception parsing RUIM record"

    #@4b
    invoke-static {v8, v9, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4e
    .catchall {:try_start_47 .. :try_end_4e} :catchall_57

    #@4e
    .line 720
    if-eqz v4, :cond_33

    #@50
    goto :goto_3e

    #@51
    .line 566
    .end local v3           #exc:Ljava/lang/RuntimeException;
    :sswitch_51
    :try_start_51
    const-string v8, "Event EVENT_GET_DEVICE_IDENTITY_DONE Received"

    #@53
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V
    :try_end_56
    .catchall {:try_start_51 .. :try_end_56} :catchall_57
    .catch Ljava/lang/RuntimeException; {:try_start_51 .. :try_end_56} :catch_46

    #@56
    goto :goto_3c

    #@57
    .line 720
    :catchall_57
    move-exception v8

    #@58
    if-eqz v4, :cond_5d

    #@5a
    .line 721
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->onRecordLoaded()V

    #@5d
    .line 720
    :cond_5d
    throw v8

    #@5e
    .line 571
    :sswitch_5e
    const/4 v4, 0x1

    #@5f
    .line 573
    :try_start_5f
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@61
    check-cast v1, Landroid/os/AsyncResult;

    #@63
    .line 574
    .local v1, ar:Landroid/os/AsyncResult;
    iget-object v8, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@65
    if-eqz v8, :cond_80

    #@67
    .line 575
    new-instance v8, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v9, "Exception querying IMSI, Exception:"

    #@6e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v8

    #@72
    iget-object v9, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@74
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v8

    #@78
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v8

    #@7c
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->loge(Ljava/lang/String;)V

    #@7f
    goto :goto_3c

    #@80
    .line 579
    :cond_80
    iget-object v8, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@82
    check-cast v8, Ljava/lang/String;

    #@84
    iput-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@86
    .line 583
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@88
    if-eqz v8, :cond_b7

    #@8a
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@8c
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@8f
    move-result v8

    #@90
    if-lt v8, v12, :cond_9c

    #@92
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@94
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@97
    move-result v8

    #@98
    const/16 v9, 0xf

    #@9a
    if-le v8, v9, :cond_b7

    #@9c
    .line 584
    :cond_9c
    new-instance v8, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v9, "invalid IMSI "

    #@a3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v8

    #@a7
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@a9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v8

    #@ad
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v8

    #@b1
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->loge(Ljava/lang/String;)V

    #@b4
    .line 585
    const/4 v8, 0x0

    #@b5
    iput-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@b7
    .line 588
    :cond_b7
    new-instance v8, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v9, "IMSI: "

    #@be
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v8

    #@c2
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@c4
    const/4 v10, 0x0

    #@c5
    const/4 v11, 0x6

    #@c6
    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@c9
    move-result-object v9

    #@ca
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v8

    #@ce
    const-string v9, "xxxxxxxxx"

    #@d0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v8

    #@d4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v8

    #@d8
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@db
    .line 590
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->getOperatorNumeric()Ljava/lang/String;

    #@de
    move-result-object v6

    #@df
    .line 591
    .local v6, operatorNumeric:Ljava/lang/String;
    if-eqz v6, :cond_3c

    #@e1
    .line 592
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@e4
    move-result v8

    #@e5
    if-gt v8, v12, :cond_3c

    #@e7
    .line 593
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@e9
    invoke-static {v8, v6}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    #@ec
    goto/16 :goto_3c

    #@ee
    .line 599
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v6           #operatorNumeric:Ljava/lang/String;
    :sswitch_ee
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f0
    check-cast v1, Landroid/os/AsyncResult;

    #@f2
    .line 600
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v8, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@f4
    check-cast v8, [Ljava/lang/String;

    #@f6
    move-object v0, v8

    #@f7
    check-cast v0, [Ljava/lang/String;

    #@f9
    move-object v5, v0

    #@fa
    .line 601
    .local v5, localTemp:[Ljava/lang/String;
    iget-object v8, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@fc
    if-nez v8, :cond_3c

    #@fe
    .line 605
    const/4 v8, 0x0

    #@ff
    aget-object v8, v5, v8

    #@101
    iput-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMyMobileNumber:Ljava/lang/String;

    #@103
    .line 606
    const/4 v8, 0x3

    #@104
    aget-object v8, v5, v8

    #@106
    iput-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMin2Min1:Ljava/lang/String;

    #@108
    .line 607
    const/4 v8, 0x4

    #@109
    aget-object v8, v5, v8

    #@10b
    iput-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mPrlVersion:Ljava/lang/String;

    #@10d
    .line 609
    new-instance v8, Ljava/lang/StringBuilder;

    #@10f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@112
    const-string v9, "MDN: "

    #@114
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v8

    #@118
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMyMobileNumber:Ljava/lang/String;

    #@11a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v8

    #@11e
    const-string v9, " MIN: "

    #@120
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v8

    #@124
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMin2Min1:Ljava/lang/String;

    #@126
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v8

    #@12a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12d
    move-result-object v8

    #@12e
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@131
    goto/16 :goto_3c

    #@133
    .line 614
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v5           #localTemp:[Ljava/lang/String;
    :sswitch_133
    const/4 v4, 0x1

    #@134
    .line 616
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@136
    check-cast v1, Landroid/os/AsyncResult;

    #@138
    .line 617
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v8, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@13a
    check-cast v8, [B

    #@13c
    move-object v0, v8

    #@13d
    check-cast v0, [B

    #@13f
    move-object v2, v0

    #@140
    .line 619
    .local v2, data:[B
    iget-object v8, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@142
    if-nez v8, :cond_3c

    #@144
    .line 623
    const/4 v8, 0x0

    #@145
    array-length v9, v2

    #@146
    invoke-static {v2, v8, v9}, Lcom/android/internal/telephony/uicc/IccUtils;->bcdToString([BII)Ljava/lang/String;

    #@149
    move-result-object v8

    #@14a
    iput-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@14c
    .line 625
    new-instance v8, Ljava/lang/StringBuilder;

    #@14e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@151
    const-string v9, "iccid: "

    #@153
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v8

    #@157
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@159
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v8

    #@15d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v8

    #@161
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@164
    .line 627
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@166
    if-eqz v8, :cond_3c

    #@168
    sget-boolean v8, Lcom/android/internal/telephony/uicc/RuimRecords;->iccidToLoad:Z

    #@16a
    if-eq v8, v10, :cond_3c

    #@16c
    .line 629
    const/4 v8, 0x1

    #@16d
    sput-boolean v8, Lcom/android/internal/telephony/uicc/RuimRecords;->iccidToLoad:Z

    #@16f
    .line 631
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->getSimIccid()Ljava/lang/String;

    #@172
    move-result-object v7

    #@173
    .line 632
    .local v7, saved_iccid:Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    #@175
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@178
    const-string v9, "[LGE_UICC] saved ICCID: "

    #@17a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v8

    #@17e
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v8

    #@182
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@185
    move-result-object v8

    #@186
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@189
    .line 633
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@18b
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->setSimIccid(Ljava/lang/String;)V

    #@18e
    .line 640
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@190
    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@193
    move-result v8

    #@194
    if-eqz v8, :cond_1a4

    #@196
    .line 641
    const-string v8, "persist.radio.iccid-changed"

    #@198
    const-string v9, "0"

    #@19a
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@19d
    .line 642
    const-string v8, "[LGE_UICC] No SIM changed"

    #@19f
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@1a2
    goto/16 :goto_3c

    #@1a4
    .line 645
    :cond_1a4
    if-nez v7, :cond_1b7

    #@1a6
    .line 646
    const-string v8, "persist.radio.iccid-changed"

    #@1a8
    const-string v9, "2"

    #@1aa
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1ad
    .line 647
    const-string v8, "[LGE_UICC] SIM first insert"

    #@1af
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@1b2
    .line 653
    :goto_1b2
    const/4 v8, 0x1

    #@1b3
    sput-boolean v8, Lcom/android/internal/telephony/uicc/RuimRecords;->isSimChanged:Z

    #@1b5
    goto/16 :goto_3c

    #@1b7
    .line 650
    :cond_1b7
    const-string v8, "persist.radio.iccid-changed"

    #@1b9
    const-string v9, "1"

    #@1bb
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1be
    .line 651
    const-string v8, "[LGE_UICC] SIM Changed"

    #@1c0
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@1c3
    goto :goto_1b2

    #@1c4
    .line 662
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v2           #data:[B
    .end local v7           #saved_iccid:Ljava/lang/String;
    :sswitch_1c4
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1c6
    check-cast v1, Landroid/os/AsyncResult;

    #@1c8
    .line 663
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v8, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1ca
    if-eqz v8, :cond_3c

    #@1cc
    .line 664
    const-string v8, "CDMA"

    #@1ce
    const-string v9, "RuimRecords update failed"

    #@1d0
    iget-object v10, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d2
    invoke-static {v8, v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d5
    goto/16 :goto_3c

    #@1d7
    .line 670
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_1d7
    const-string v8, "handleMessage(), EVENT_GET_ALL_SMS_DONE is received"

    #@1d9
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1dc
    .line 671
    const/4 v4, 0x1

    #@1dd
    .line 672
    const/4 v8, 0x0

    #@1de
    const-string v9, "uicc_csim"

    #@1e0
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1e3
    move-result v8

    #@1e4
    if-eqz v8, :cond_3c

    #@1e6
    .line 673
    const-string v8, "handleMessage(), EVENT_GET_ALL_SMS_DONE is received"

    #@1e8
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1eb
    .line 674
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1ed
    check-cast v1, Landroid/os/AsyncResult;

    #@1ef
    .line 675
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v8, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1f1
    if-nez v8, :cond_3c

    #@1f3
    .line 678
    iget-object v8, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1f5
    check-cast v8, Ljava/util/ArrayList;

    #@1f7
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->handleCSimSmses(Ljava/util/ArrayList;)V

    #@1fa
    goto/16 :goto_3c

    #@1fc
    .line 686
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_1fc
    const-string v8, "CDMA"

    #@1fe
    new-instance v9, Ljava/lang/StringBuilder;

    #@200
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@203
    const-string v10, "Event not supported: "

    #@205
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@208
    move-result-object v9

    #@209
    iget v10, p1, Landroid/os/Message;->what:I

    #@20b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20e
    move-result-object v9

    #@20f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@212
    move-result-object v9

    #@213
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@216
    goto/16 :goto_3c

    #@218
    .line 691
    :sswitch_218
    const-string v8, "Event EVENT_GET_SST_DONE Received"

    #@21a
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@21d
    goto/16 :goto_3c

    #@21f
    .line 695
    :sswitch_21f
    const/4 v4, 0x0

    #@220
    .line 696
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@222
    check-cast v1, Landroid/os/AsyncResult;

    #@224
    .line 699
    .restart local v1       #ar:Landroid/os/AsyncResult;
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@227
    move-result-object v8

    #@228
    const-string v9, "VZW"

    #@22a
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22d
    move-result v8

    #@22e
    if-nez v8, :cond_23c

    #@230
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@233
    move-result-object v8

    #@234
    const-string v9, "USC"

    #@236
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@239
    move-result v8

    #@23a
    if-eqz v8, :cond_256

    #@23c
    .line 701
    :cond_23c
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@23e
    new-instance v9, Landroid/content/Intent;

    #@240
    const-string v10, "persist.radio.refresh_status"

    #@242
    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@245
    invoke-virtual {v8, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@248
    .line 702
    const-string v8, "persist.radio.refresh_status"

    #@24a
    const-string v9, "1"

    #@24c
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@24f
    .line 703
    const-string v8, "CDMA"

    #@251
    const-string v9, "Event EVENT_RUIM_REFRESH Received"

    #@253
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@256
    .line 707
    :cond_256
    iget-object v8, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@258
    if-nez v8, :cond_3c

    #@25a
    .line 708
    iget-object v8, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@25c
    check-cast v8, Lcom/android/internal/telephony/uicc/IccRefreshResponse;

    #@25e
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->handleRuimRefresh(Lcom/android/internal/telephony/uicc/IccRefreshResponse;)V
    :try_end_261
    .catchall {:try_start_5f .. :try_end_261} :catchall_57
    .catch Ljava/lang/RuntimeException; {:try_start_5f .. :try_end_261} :catch_46

    #@261
    goto/16 :goto_3c

    #@263
    .line 560
    nop

    #@264
    :sswitch_data_264
    .sparse-switch
        0x1 -> :sswitch_42
        0x3 -> :sswitch_5e
        0x4 -> :sswitch_51
        0x5 -> :sswitch_133
        0xa -> :sswitch_ee
        0xe -> :sswitch_1c4
        0x11 -> :sswitch_218
        0x12 -> :sswitch_1d7
        0x13 -> :sswitch_1fc
        0x15 -> :sswitch_1fc
        0x16 -> :sswitch_1fc
        0x1f -> :sswitch_21f
    .end sparse-switch
.end method

.method public isProvisioned()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1349
    const-string v2, "persist.radio.test-csim"

    #@4
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_b

    #@a
    .line 1361
    :cond_a
    :goto_a
    return v0

    #@b
    .line 1353
    :cond_b
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@d
    if-nez v2, :cond_11

    #@f
    move v0, v1

    #@10
    .line 1354
    goto :goto_a

    #@11
    .line 1357
    :cond_11
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@13
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@16
    move-result-object v2

    #@17
    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_CSIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@19
    if-ne v2, v3, :cond_a

    #@1b
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMdn:Ljava/lang/String;

    #@1d
    if-eqz v2, :cond_23

    #@1f
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/RuimRecords;->mMin:Ljava/lang/String;

    #@21
    if-nez v2, :cond_a

    #@23
    :cond_23
    move v0, v1

    #@24
    .line 1359
    goto :goto_a
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 1453
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[RuimRecords] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1454
    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 1458
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[RuimRecords] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1459
    return-void
.end method

.method protected onAllRecordsLoaded()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1224
    const-string v1, "record load complete"

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@6
    .line 1228
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->getOperatorNumeric()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 1230
    .local v0, operator:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "RuimRecords: onAllRecordsLoaded set \'gsm.sim.operator.numeric\' to operator=\'"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, "\'"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@26
    .line 1232
    if-eqz v0, :cond_2d

    #@28
    .line 1233
    const-string v1, "gsm.sim.operator.numeric"

    #@2a
    invoke-direct {p0, v1, v0}, Lcom/android/internal/telephony/uicc/RuimRecords;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 1236
    :cond_2d
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@2f
    if-eqz v1, :cond_52

    #@31
    .line 1237
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    const-string v2, "LGU"

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v1

    #@3b
    if-nez v1, :cond_52

    #@3d
    .line 1238
    const-string v1, "gsm.sim.operator.iso-country"

    #@3f
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mImsi:Ljava/lang/String;

    #@41
    const/4 v3, 0x0

    #@42
    const/4 v4, 0x3

    #@43
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4a
    move-result v2

    #@4b
    invoke-static {v2}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-direct {p0, v1, v2}, Lcom/android/internal/telephony/uicc/RuimRecords;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@52
    .line 1244
    :cond_52
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsLoadedRegistrants:Landroid/os/RegistrantList;

    #@54
    new-instance v2, Landroid/os/AsyncResult;

    #@56
    invoke-direct {v2, v5, v5, v5}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@59
    invoke-virtual {v1, v2}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@5c
    .line 1246
    return-void
.end method

.method public onReady()V
    .registers 3

    #@0
    .prologue
    .line 1250
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->fetchRuimRecords()V

    #@3
    .line 1252
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@5
    const/16 v1, 0xa

    #@7
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v1

    #@b
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getCDMASubscription(Landroid/os/Message;)V

    #@e
    .line 1253
    return-void
.end method

.method protected onRecordLoaded()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 1195
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@4
    add-int/lit8 v1, v1, -0x1

    #@6
    iput v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@8
    .line 1196
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "onRecordLoaded "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " requested: "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    iget-boolean v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@2c
    .line 1198
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@2e
    if-nez v1, :cond_79

    #@30
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@32
    if-ne v1, v3, :cond_79

    #@34
    .line 1199
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->onAllRecordsLoaded()V

    #@37
    .line 1201
    sget-boolean v1, Lcom/android/internal/telephony/uicc/RuimRecords;->iccidToLoad:Z

    #@39
    if-ne v1, v3, :cond_62

    #@3b
    sget-boolean v1, Lcom/android/internal/telephony/uicc/RuimRecords;->isSimChanged:Z

    #@3d
    if-ne v1, v3, :cond_62

    #@3f
    .line 1202
    new-instance v0, Landroid/content/Intent;

    #@41
    const-string v1, "android.intent.action.SIM_CHANGED_INFO"

    #@43
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@46
    .line 1204
    .local v0, intent_sim_changed:Landroid/content/Intent;
    const-string v1, "persist.radio.iccid-changed"

    #@48
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4b
    move-result-object v1

    #@4c
    const-string v2, "1"

    #@4e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v1

    #@52
    if-eqz v1, :cond_63

    #@54
    .line 1205
    const-string v1, "reason"

    #@56
    const-string v2, "actual"

    #@58
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@5b
    .line 1210
    :cond_5b
    :goto_5b
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@5d
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@60
    .line 1213
    sput-boolean v4, Lcom/android/internal/telephony/uicc/RuimRecords;->isSimChanged:Z

    #@62
    .line 1220
    .end local v0           #intent_sim_changed:Landroid/content/Intent;
    :cond_62
    :goto_62
    return-void

    #@63
    .line 1206
    .restart local v0       #intent_sim_changed:Landroid/content/Intent;
    :cond_63
    const-string v1, "persist.radio.iccid-changed"

    #@65
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    const-string v2, "2"

    #@6b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v1

    #@6f
    if-eqz v1, :cond_5b

    #@71
    .line 1207
    const-string v1, "reason"

    #@73
    const-string v2, "first"

    #@75
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@78
    goto :goto_5b

    #@79
    .line 1216
    .end local v0           #intent_sim_changed:Landroid/content/Intent;
    :cond_79
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@7b
    if-gez v1, :cond_62

    #@7d
    .line 1217
    const-string v1, "recordsToLoad <0, programmer error suspected"

    #@7f
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/RuimRecords;->loge(Ljava/lang/String;)V

    #@82
    .line 1218
    iput v4, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsToLoad:I

    #@84
    goto :goto_62
.end method

.method public onRefresh(Z[I)V
    .registers 3
    .parameter "fileChanged"
    .parameter "fileList"

    #@0
    .prologue
    .line 250
    if-eqz p1, :cond_5

    #@2
    .line 254
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/RuimRecords;->fetchRuimRecords()V

    #@5
    .line 256
    :cond_5
    return-void
.end method

.method protected resetRecords()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 194
    const/4 v0, -0x1

    #@2
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mncLength:I

    #@4
    .line 195
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccRecords;->iccid:Ljava/lang/String;

    #@6
    .line 197
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->adnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->reset()V

    #@b
    .line 208
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/IccRecords;->recordsRequested:Z

    #@e
    .line 210
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    const-string v1, "VZW"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_26

    #@1a
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, "USC"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_2b

    #@26
    .line 211
    :cond_26
    const-string v0, "persist.radio.refresh_status"

    #@28
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 214
    :cond_2b
    return-void
.end method

.method public setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "alphaTag"
    .parameter "voiceNumber"
    .parameter "onComplete"

    #@0
    .prologue
    .line 237
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@3
    move-result-object v0

    #@4
    new-instance v1, Lcom/android/internal/telephony/uicc/IccException;

    #@6
    const-string v2, "setVoiceMailNumber not implemented"

    #@8
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/uicc/IccException;-><init>(Ljava/lang/String;)V

    #@b
    iput-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@d
    .line 239
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@10
    .line 240
    const-string v0, "method setVoiceMailNumber is not implemented"

    #@12
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/RuimRecords;->loge(Ljava/lang/String;)V

    #@15
    .line 241
    return-void
.end method

.method public setVoiceMessageWaiting(IILandroid/os/Message;)V
    .registers 6
    .parameter "line"
    .parameter "countWaiting"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1368
    const-string v0, "CDMA"

    #@2
    const-string v1, "RuimRecords:setVoiceMessageWaiting - NOP for CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1369
    return-void
.end method

.method public updateCurrentIccCSimMessageProvider(Ljava/util/ArrayList;)Z
    .registers 14
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p1, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    const/4 v7, 0x0

    #@1
    .line 754
    :try_start_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@4
    move-result v1

    #@5
    .line 755
    .local v1, count:I
    new-instance v6, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v8, "updateCurrentIccCSimMessageProvider(), messages size = "

    #@c
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v6

    #@18
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1b
    .line 756
    const/4 v4, 0x0

    #@1c
    .local v4, i:I
    :goto_1c
    if-ge v4, v1, :cond_cf

    #@1e
    .line 757
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, [B

    #@24
    .line 759
    .local v0, ba:[B
    if-eqz v0, :cond_a8

    #@26
    .line 760
    new-instance v6, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v8, "updateCurrentIccCSimMessageProvider(), index: "

    #@2d
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    const-string v8, " :::::::::EF_SMS: "

    #@37
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@3e
    move-result-object v8

    #@3f
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4a
    .line 761
    new-instance v6, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v8, "updateCurrentIccCSimMessageProvider(), EF_SMS Total Length = "

    #@51
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v6

    #@55
    array-length v8, v0

    #@56
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@59
    move-result-object v6

    #@5a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v6

    #@5e
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@61
    .line 762
    const/4 v6, 0x0

    #@62
    aget-byte v6, v0, v6

    #@64
    if-eqz v6, :cond_a8

    #@66
    .line 763
    new-instance v2, Lcom/android/internal/telephony/SmsRawData;

    #@68
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6b
    move-result-object v6

    #@6c
    check-cast v6, [B

    #@6e
    invoke-direct {v2, v6}, Lcom/android/internal/telephony/SmsRawData;-><init>([B)V

    #@71
    .line 764
    .local v2, data:Lcom/android/internal/telephony/SmsRawData;
    new-instance v6, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v8, "updateCurrentIccCSimMessageProvider(), index: "

    #@78
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v6

    #@80
    const-string v8, " :::::::::data.getBytes: "

    #@82
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v6

    #@86
    invoke-virtual {v2}, Lcom/android/internal/telephony/SmsRawData;->getBytes()[B

    #@89
    move-result-object v8

    #@8a
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@8d
    move-result-object v8

    #@8e
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v6

    #@92
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v6

    #@96
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@99
    .line 765
    add-int/lit8 v6, v4, 0x1

    #@9b
    invoke-virtual {v2}, Lcom/android/internal/telephony/SmsRawData;->getBytes()[B

    #@9e
    move-result-object v8

    #@9f
    invoke-static {v6, v8}, Lcom/android/internal/telephony/cdma/SmsMessage;->createFromEfRecord(I[B)Lcom/android/internal/telephony/cdma/SmsMessage;

    #@a2
    move-result-object v5

    #@a3
    .line 766
    .local v5, sms:Lcom/android/internal/telephony/cdma/SmsMessage;
    if-eqz v5, :cond_ac

    #@a5
    .line 767
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/uicc/RuimRecords;->updateMessagefromCSim(Lcom/android/internal/telephony/cdma/SmsMessage;)V

    #@a8
    .line 756
    .end local v2           #data:Lcom/android/internal/telephony/SmsRawData;
    .end local v5           #sms:Lcom/android/internal/telephony/cdma/SmsMessage;
    :cond_a8
    :goto_a8
    add-int/lit8 v4, v4, 0x1

    #@aa
    goto/16 :goto_1c

    #@ac
    .line 769
    .restart local v2       #data:Lcom/android/internal/telephony/SmsRawData;
    .restart local v5       #sms:Lcom/android/internal/telephony/cdma/SmsMessage;
    :cond_ac
    const-string v6, "updateCurrentIccCSimMessageProvider(), SmsMessage is null. It is created from createFromCSimEfRecord()."

    #@ae
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_b1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_b1} :catch_b2
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_b1} :catch_e0

    #@b1
    goto :goto_a8

    #@b2
    .line 779
    .end local v0           #ba:[B
    .end local v1           #count:I
    .end local v2           #data:Lcom/android/internal/telephony/SmsRawData;
    .end local v4           #i:I
    .end local v5           #sms:Lcom/android/internal/telephony/cdma/SmsMessage;
    :catch_b2
    move-exception v3

    #@b3
    .line 780
    .local v3, e:Ljava/lang/IllegalArgumentException;
    new-instance v6, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v8, "updateCurrentIccCSimMessageProvider(), IllegalArgumentException"

    #@ba
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v6

    #@be
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    #@c1
    move-result-object v8

    #@c2
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v6

    #@c6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v6

    #@ca
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@cd
    .end local v3           #e:Ljava/lang/IllegalArgumentException;
    :goto_cd
    move v6, v7

    #@ce
    .line 785
    :goto_ce
    return v6

    #@cf
    .line 776
    .restart local v1       #count:I
    .restart local v4       #i:I
    :cond_cf
    :try_start_cf
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@d1
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/IccRecords;->mContext:Landroid/content/Context;

    #@d3
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d6
    move-result-object v8

    #@d7
    sget-object v9, Lcom/android/internal/telephony/uicc/RuimRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@d9
    const/4 v10, 0x0

    #@da
    const/4 v11, 0x0

    #@db
    invoke-static {v6, v8, v9, v10, v11}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_de
    .catch Ljava/lang/IllegalArgumentException; {:try_start_cf .. :try_end_de} :catch_b2
    .catch Landroid/database/SQLException; {:try_start_cf .. :try_end_de} :catch_e0

    #@de
    .line 777
    const/4 v6, 0x1

    #@df
    goto :goto_ce

    #@e0
    .line 782
    .end local v1           #count:I
    .end local v4           #i:I
    :catch_e0
    move-exception v3

    #@e1
    .line 783
    .local v3, e:Landroid/database/SQLException;
    const-string v6, "updateCurrentIccCSimMessageProvider(), Can\'t store current SMS in CSIM to SQLite DB"

    #@e3
    invoke-static {v6, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e6
    goto :goto_cd
.end method
