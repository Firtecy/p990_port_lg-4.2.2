.class public Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;
.super Lcom/android/internal/telephony/SMSDispatcher;
.source "GsmSMSDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;
    }
.end annotation


# static fields
.field private static final ACTION_KTLBS_DATA_SMS_RECEIVED:Ljava/lang/String; = "com.kt.location.action.KTLBS_DATA_SMS_RECEIVED"

.field public static final ADDRESS:Ljava/lang/String; = "address"

.field private static final ALLRECEIVE_MODE:B = 0x3t

.field private static final APP_DIRECTED_SMS_FORMATTED:I = 0x0

.field private static final APP_DIRECTED_SMS_NORMAL:I = -0x1

.field private static final APP_DIRECTED_SMS_PROCESSED:I = 0x1

.field private static final COMMERCIAL_MODE:B = 0x0t

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final DATE:Ljava/lang/String; = "date"

.field private static final DEBUG:Z = false

.field private static final EVENT_NEW_BROADCAST_SMS:I = 0x65

.field private static final EVENT_NEW_SMS_STATUS_REPORT:I = 0x64

.field private static final EVENT_WRITE_SMS_COMPLETE:I = 0x66

.field private static final KDDITEST_MODE:B = 0x2t

.field private static final KTLBS_SPECIALNUMBER_STRING:Ljava/lang/String; = "##30"

.field public static final LMS_MAX_COUNT:I = 0x32

.field public static final LMS_MAX_SEGMENT:I = 0x3

.field public static final LMS_URI:Landroid/net/Uri; = null

.field private static final MANUFACTURETEST_MODE:B = 0x1t

.field public static final PDU:Ljava/lang/String; = "pdu"

.field public static final REFERENCE_NUMBER:Ljava/lang/String; = "reference_number"

.field public static final SEQUENCE:Ljava/lang/String; = "sequence"

.field public static final SOURCE_MIN:Ljava/lang/String; = "source_min"

.field private static final TAG:Ljava/lang/String; = "GSM"

.field public static final TID:Ljava/lang/String; = "tid"

.field private static final prefixVZW:Ljava/lang/String; = "//VZW"


# instance fields
.field public final APPLICATION_PERMISSION:Ljava/lang/String;

.field public final METADATA_NAME:Ljava/lang/String;

.field private SIGNATURES:[Landroid/content/pm/Signature;

.field private VZWSignature:[Landroid/content/pm/Signature;

.field private final mDataDownloadHandler:Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;

.field private mGsmPhone:Lcom/android/internal/telephony/PhoneBase;

.field private mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/android/internal/telephony/uicc/IccRecords;",
            ">;"
        }
    .end annotation
.end field

.field private mImsSMSDispatcher:Lcom/android/internal/telephony/ImsSMSDispatcher;

.field private final mSmsCbPageMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;",
            "[[B>;"
        }
    .end annotation
.end field

.field private mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/android/internal/telephony/uicc/UiccCardApplication;",
            ">;"
        }
    .end annotation
.end field

.field protected mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

.field smsDupProc:Lcom/android/internal/telephony/LgeSmsDupProc;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 155
    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const-string v1, "raw"

    #@4
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@a
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;Lcom/android/internal/telephony/ImsSMSDispatcher;)V
    .registers 8
    .parameter "phone"
    .parameter "storageMonitor"
    .parameter "usageMonitor"
    .parameter "imsSMSDispatcher"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 184
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/SMSDispatcher;-><init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/SmsStorageMonitor;Lcom/android/internal/telephony/SmsUsageMonitor;)V

    #@4
    .line 127
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@6
    .line 128
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    #@8
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@d
    .line 129
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    #@f
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    #@12
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@14
    .line 147
    new-instance v0, Lcom/android/internal/telephony/LgeSmsDupProc;

    #@16
    invoke-direct {v0}, Lcom/android/internal/telephony/LgeSmsDupProc;-><init>()V

    #@19
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->smsDupProc:Lcom/android/internal/telephony/LgeSmsDupProc;

    #@1b
    .line 487
    const-string v0, "com.verizon.permissions.appdirectedsms"

    #@1d
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->APPLICATION_PERMISSION:Ljava/lang/String;

    #@1f
    .line 488
    const-string v0, "com.verizon.directedAppSMS"

    #@21
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->METADATA_NAME:Ljava/lang/String;

    #@23
    .line 1547
    new-instance v0, Ljava/util/HashMap;

    #@25
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@28
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mSmsCbPageMap:Ljava/util/HashMap;

    #@2a
    .line 185
    iput-object p4, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mImsSMSDispatcher:Lcom/android/internal/telephony/ImsSMSDispatcher;

    #@2c
    .line 186
    new-instance v0, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;

    #@2e
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@30
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;-><init>(Lcom/android/internal/telephony/CommandsInterface;)V

    #@33
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mDataDownloadHandler:Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;

    #@35
    .line 188
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mGsmPhone:Lcom/android/internal/telephony/PhoneBase;

    #@37
    .line 190
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@39
    const/4 v1, 0x1

    #@3a
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setOnNewGsmSms(Landroid/os/Handler;ILjava/lang/Object;)V

    #@3d
    .line 193
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@3f
    const/16 v1, 0x64

    #@41
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setOnSmsStatus(Landroid/os/Handler;ILjava/lang/Object;)V

    #@44
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@46
    const/16 v1, 0x65

    #@48
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setOnNewGsmBroadcastSms(Landroid/os/Handler;ILjava/lang/Object;)V

    #@4b
    .line 195
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@4e
    move-result-object v0

    #@4f
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@51
    .line 196
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@53
    const/16 v1, 0xe

    #@55
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccController;->registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@58
    .line 197
    const-string v0, "GsmSMSDispatcher(), GsmSMSDispatcher created"

    #@5a
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5d
    .line 198
    return-void
.end method

.method private completeOrInsertLms(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III[B[[BI)I
    .registers 11
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "currentSegment"
    .parameter "totalSegment"
    .parameter "pdu"
    .parameter "pdus"
    .parameter "sameMsgCount"

    #@0
    .prologue
    .line 2229
    add-int/lit8 v1, p8, 0x1

    #@2
    if-ne v1, p5, :cond_17

    #@4
    .line 2231
    add-int/lit8 v1, p4, -0x1

    #@6
    aput-object p6, p7, v1

    #@8
    .line 2232
    move v0, p5

    #@9
    .local v0, loop:I
    :goto_9
    const/4 v1, 0x3

    #@a
    if-ge v0, v1, :cond_12

    #@c
    .line 2233
    const/4 v1, 0x0

    #@d
    aput-object v1, p7, v0

    #@f
    .line 2232
    add-int/lit8 v0, v0, 0x1

    #@11
    goto :goto_9

    #@12
    .line 2235
    :cond_12
    invoke-virtual/range {p0 .. p5}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->deleteCompleteLmsMsg(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III)V

    #@15
    .line 2236
    const/4 v1, -0x1

    #@16
    .line 2239
    .end local v0           #loop:I
    :goto_16
    return v1

    #@17
    .line 2238
    :cond_17
    invoke-virtual/range {p0 .. p6}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->insertLmsMsg(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III[B)V

    #@1a
    .line 2239
    const/4 v1, 0x1

    #@1b
    goto :goto_16
.end method

.method private getVZWSignatures(Landroid/content/pm/PackageManager;)Z
    .registers 12
    .parameter "pm"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 496
    :try_start_1
    const-string v8, "com.verizon.permissions.appdirectedsms"

    #@3
    const/16 v9, 0x40

    #@5
    invoke-virtual {p1, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_8
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_8} :catch_19

    #@8
    move-result-object v5

    #@9
    .line 502
    .local v5, permissionPkgInfo:Landroid/content/pm/PackageInfo;
    if-eqz v5, :cond_64

    #@b
    .line 503
    iget-object v8, v5, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@d
    iput-object v8, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@f
    .line 504
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@11
    if-nez v8, :cond_20

    #@13
    .line 505
    const-string v8, "getVZWSignatures(), Can\'t find permission package signatures"

    #@15
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@18
    .line 515
    .end local v5           #permissionPkgInfo:Landroid/content/pm/PackageInfo;
    :goto_18
    return v7

    #@19
    .line 497
    :catch_19
    move-exception v1

    #@1a
    .line 498
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "getVZWSignatures(), Can\'t find permission package: com.verizon.permissions.appdirectedsms"

    #@1c
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@1f
    goto :goto_18

    #@20
    .line 508
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v5       #permissionPkgInfo:Landroid/content/pm/PackageInfo;
    :cond_20
    const/4 v3, 0x0

    #@21
    .line 509
    .local v3, index:I
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@23
    .local v0, arr$:[Landroid/content/pm/Signature;
    array-length v4, v0

    #@24
    .local v4, len$:I
    const/4 v2, 0x0

    #@25
    .local v2, i$:I
    :goto_25
    if-ge v2, v4, :cond_64

    #@27
    aget-object v6, v0, v2

    #@29
    .line 510
    .local v6, signature:Landroid/content/pm/Signature;
    new-instance v7, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v8, "getVZWSignatures(), VZWSignature: index = [ "

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v7

    #@38
    const-string v8, " ]"

    #@3a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v7

    #@3e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v7

    #@42
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@45
    .line 511
    new-instance v7, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v8, "getVZWSignatures(), VZWSignature : "

    #@4c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v7

    #@50
    invoke-virtual {v6}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    #@53
    move-result-object v8

    #@54
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v7

    #@58
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v7

    #@5c
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5f
    .line 512
    add-int/lit8 v3, v3, 0x1

    #@61
    .line 509
    add-int/lit8 v2, v2, 0x1

    #@63
    goto :goto_25

    #@64
    .line 515
    .end local v0           #arr$:[Landroid/content/pm/Signature;
    .end local v2           #i$:I
    .end local v3           #index:I
    .end local v4           #len$:I
    .end local v6           #signature:Landroid/content/pm/Signature;
    :cond_64
    const/4 v7, 0x1

    #@65
    goto :goto_18
.end method

.method private handleBroadcastSms(Landroid/os/AsyncResult;)V
    .registers 24
    .parameter "ar"

    #@0
    .prologue
    .line 1556
    :try_start_0
    move-object/from16 v0, p1

    #@2
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4
    move-object/from16 v20, v0

    #@6
    check-cast v20, [B

    #@8
    move-object/from16 v0, v20

    #@a
    check-cast v0, [B

    #@c
    move-object/from16 v19, v0

    #@e
    .line 1572
    .local v19, receivedPdu:[B
    new-instance v7, Lcom/android/internal/telephony/gsm/SmsCbHeader;

    #@10
    move-object/from16 v0, v19

    #@12
    invoke-direct {v7, v0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;-><init>([B)V

    #@15
    .line 1573
    .local v7, header:Lcom/android/internal/telephony/gsm/SmsCbHeader;
    const-string v20, "gsm.operator.numeric"

    #@17
    invoke-static/range {v20 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v18

    #@1b
    .line 1574
    .local v18, plmn:Ljava/lang/String;
    const/4 v12, -0x1

    #@1c
    .line 1575
    .local v12, lac:I
    const/4 v3, -0x1

    #@1d
    .line 1576
    .local v3, cid:I
    move-object/from16 v0, p0

    #@1f
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@21
    move-object/from16 v20, v0

    #@23
    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/telephony/PhoneBase;->getCellLocation()Landroid/telephony/CellLocation;

    #@26
    move-result-object v4

    #@27
    .line 1580
    .local v4, cl:Landroid/telephony/CellLocation;
    instance-of v0, v4, Landroid/telephony/gsm/GsmCellLocation;

    #@29
    move/from16 v20, v0

    #@2b
    if-eqz v20, :cond_39

    #@2d
    .line 1581
    move-object v0, v4

    #@2e
    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    #@30
    move-object v2, v0

    #@31
    .line 1582
    .local v2, cellLocation:Landroid/telephony/gsm/GsmCellLocation;
    invoke-virtual {v2}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    #@34
    move-result v12

    #@35
    .line 1583
    invoke-virtual {v2}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    #@38
    move-result v3

    #@39
    .line 1587
    .end local v2           #cellLocation:Landroid/telephony/gsm/GsmCellLocation;
    :cond_39
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getGeographicalScope()I

    #@3c
    move-result v20

    #@3d
    packed-switch v20, :pswitch_data_182

    #@40
    .line 1599
    :pswitch_40
    new-instance v13, Landroid/telephony/SmsCbLocation;

    #@42
    move-object/from16 v0, v18

    #@44
    invoke-direct {v13, v0}, Landroid/telephony/SmsCbLocation;-><init>(Ljava/lang/String;)V

    #@47
    .line 1604
    .local v13, location:Landroid/telephony/SmsCbLocation;
    :goto_47
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getNumberOfPages()I

    #@4a
    move-result v16

    #@4b
    .line 1605
    .local v16, pageCount:I
    const/16 v20, 0x1

    #@4d
    move/from16 v0, v16

    #@4f
    move/from16 v1, v20

    #@51
    if-le v0, v1, :cond_174

    #@53
    .line 1607
    new-instance v5, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;

    #@55
    invoke-direct {v5, v7, v13}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;-><init>(Lcom/android/internal/telephony/gsm/SmsCbHeader;Landroid/telephony/SmsCbLocation;)V

    #@58
    .line 1610
    .local v5, concatInfo:Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;
    move-object/from16 v0, p0

    #@5a
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mSmsCbPageMap:Ljava/util/HashMap;

    #@5c
    move-object/from16 v20, v0

    #@5e
    move-object/from16 v0, v20

    #@60
    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@63
    move-result-object v17

    #@64
    check-cast v17, [[B

    #@66
    .line 1612
    .local v17, pdus:[[B
    if-nez v17, :cond_7b

    #@68
    .line 1615
    move/from16 v0, v16

    #@6a
    new-array v0, v0, [[B

    #@6c
    move-object/from16 v17, v0

    #@6e
    .line 1617
    move-object/from16 v0, p0

    #@70
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mSmsCbPageMap:Ljava/util/HashMap;

    #@72
    move-object/from16 v20, v0

    #@74
    move-object/from16 v0, v20

    #@76
    move-object/from16 v1, v17

    #@78
    invoke-virtual {v0, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7b
    .line 1621
    :cond_7b
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getPageIndex()I

    #@7e
    move-result v20

    #@7f
    add-int/lit8 v20, v20, -0x1

    #@81
    aput-object v19, v17, v20

    #@83
    .line 1623
    const/4 v8, 0x0

    #@84
    .local v8, i:I
    :goto_84
    move-object/from16 v0, v17

    #@86
    array-length v0, v0

    #@87
    move/from16 v20, v0

    #@89
    move/from16 v0, v20

    #@8b
    if-ge v8, v0, :cond_a9

    #@8d
    .line 1624
    aget-object v20, v17, v8

    #@8f
    if-nez v20, :cond_a6

    #@91
    .line 1673
    .end local v3           #cid:I
    .end local v4           #cl:Landroid/telephony/CellLocation;
    .end local v5           #concatInfo:Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;
    .end local v7           #header:Lcom/android/internal/telephony/gsm/SmsCbHeader;
    .end local v8           #i:I
    .end local v12           #lac:I
    .end local v13           #location:Landroid/telephony/SmsCbLocation;
    .end local v16           #pageCount:I
    .end local v17           #pdus:[[B
    .end local v18           #plmn:Ljava/lang/String;
    .end local v19           #receivedPdu:[B
    :cond_91
    :goto_91
    return-void

    #@92
    .line 1589
    .restart local v3       #cid:I
    .restart local v4       #cl:Landroid/telephony/CellLocation;
    .restart local v7       #header:Lcom/android/internal/telephony/gsm/SmsCbHeader;
    .restart local v12       #lac:I
    .restart local v18       #plmn:Ljava/lang/String;
    .restart local v19       #receivedPdu:[B
    :pswitch_92
    new-instance v13, Landroid/telephony/SmsCbLocation;

    #@94
    const/16 v20, -0x1

    #@96
    move-object/from16 v0, v18

    #@98
    move/from16 v1, v20

    #@9a
    invoke-direct {v13, v0, v12, v1}, Landroid/telephony/SmsCbLocation;-><init>(Ljava/lang/String;II)V

    #@9d
    .line 1590
    .restart local v13       #location:Landroid/telephony/SmsCbLocation;
    goto :goto_47

    #@9e
    .line 1594
    .end local v13           #location:Landroid/telephony/SmsCbLocation;
    :pswitch_9e
    new-instance v13, Landroid/telephony/SmsCbLocation;

    #@a0
    move-object/from16 v0, v18

    #@a2
    invoke-direct {v13, v0, v12, v3}, Landroid/telephony/SmsCbLocation;-><init>(Ljava/lang/String;II)V

    #@a5
    .line 1595
    .restart local v13       #location:Landroid/telephony/SmsCbLocation;
    goto :goto_47

    #@a6
    .line 1623
    .restart local v5       #concatInfo:Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;
    .restart local v8       #i:I
    .restart local v16       #pageCount:I
    .restart local v17       #pdus:[[B
    :cond_a6
    add-int/lit8 v8, v8, 0x1

    #@a8
    goto :goto_84

    #@a9
    .line 1631
    :cond_a9
    move-object/from16 v0, p0

    #@ab
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mSmsCbPageMap:Ljava/util/HashMap;

    #@ad
    move-object/from16 v20, v0

    #@af
    move-object/from16 v0, v20

    #@b1
    invoke-virtual {v0, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@b4
    .line 1638
    .end local v5           #concatInfo:Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;
    .end local v8           #i:I
    :goto_b4
    move-object/from16 v0, v17

    #@b6
    invoke-static {v7, v13, v0}, Lcom/android/internal/telephony/gsm/GsmSmsCbMessage;->createSmsCbMessage(Lcom/android/internal/telephony/gsm/SmsCbHeader;Landroid/telephony/SmsCbLocation;[[B)Landroid/telephony/SmsCbMessage;

    #@b9
    move-result-object v14

    #@ba
    .line 1640
    .local v14, message:Landroid/telephony/SmsCbMessage;
    const/16 v20, 0x0

    #@bc
    const-string v21, "emergency_alert_filtering"

    #@be
    invoke-static/range {v20 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c1
    move-result v20

    #@c2
    const/16 v21, 0x1

    #@c4
    move/from16 v0, v20

    #@c6
    move/from16 v1, v21

    #@c8
    if-eq v0, v1, :cond_da

    #@ca
    const/16 v20, 0x0

    #@cc
    const-string v21, "wifi_off_emergency_received"

    #@ce
    invoke-static/range {v20 .. v21}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@d1
    move-result v20

    #@d2
    const/16 v21, 0x1

    #@d4
    move/from16 v0, v20

    #@d6
    move/from16 v1, v21

    #@d8
    if-ne v0, v1, :cond_13b

    #@da
    .line 1643
    :cond_da
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getServiceCategory()I

    #@dd
    move-result v15

    #@de
    .line 1644
    .local v15, msg_id:I
    new-instance v20, Ljava/lang/StringBuilder;

    #@e0
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@e3
    const-string v21, "handleBroadcastSms(), [KDDI] messgeIdentifier = "

    #@e5
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v20

    #@e9
    move-object/from16 v0, v20

    #@eb
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v20

    #@ef
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v20

    #@f3
    invoke-static/range {v20 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@f6
    .line 1645
    move-object/from16 v0, p0

    #@f8
    invoke-direct {v0, v15}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->kddiFilterMsgid(I)Z

    #@fb
    move-result v20

    #@fc
    if-eqz v20, :cond_91

    #@fe
    .line 1648
    const/16 v20, 0x1100

    #@100
    move/from16 v0, v20

    #@102
    if-eq v15, v0, :cond_10b

    #@104
    const v20, 0xa003

    #@107
    move/from16 v0, v20

    #@109
    if-ne v15, v0, :cond_13b

    #@10b
    .line 1650
    :cond_10b
    new-instance v20, Ljava/lang/StringBuilder;

    #@10d
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@110
    const-string v21, "handleBroadcastSms(), [KDDI] messageIdentifier = "

    #@112
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v20

    #@116
    move-object/from16 v0, v20

    #@118
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v20

    #@11c
    const-string v21, " broadcast to WIFI!! "

    #@11e
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v20

    #@122
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@125
    move-result-object v20

    #@126
    invoke-static/range {v20 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@129
    .line 1651
    new-instance v10, Landroid/content/Intent;

    #@12b
    const-string v20, "android.intent.action.SMS_WIFI_OFF"

    #@12d
    move-object/from16 v0, v20

    #@12f
    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@132
    .line 1652
    .local v10, intent:Landroid/content/Intent;
    const/16 v20, 0x0

    #@134
    move-object/from16 v0, p0

    #@136
    move-object/from16 v1, v20

    #@138
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@13b
    .line 1656
    .end local v10           #intent:Landroid/content/Intent;
    .end local v15           #msg_id:I
    :cond_13b
    move-object/from16 v0, p0

    #@13d
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->dispatchBroadcastMessage(Landroid/telephony/SmsCbMessage;)V

    #@140
    .line 1661
    move-object/from16 v0, p0

    #@142
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mSmsCbPageMap:Ljava/util/HashMap;

    #@144
    move-object/from16 v20, v0

    #@146
    invoke-virtual/range {v20 .. v20}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@149
    move-result-object v20

    #@14a
    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@14d
    move-result-object v11

    #@14e
    .line 1663
    .local v11, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;>;"
    :cond_14e
    :goto_14e
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@151
    move-result v20

    #@152
    if-eqz v20, :cond_91

    #@154
    .line 1664
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@157
    move-result-object v9

    #@158
    check-cast v9, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;

    #@15a
    .line 1666
    .local v9, info:Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;
    move-object/from16 v0, v18

    #@15c
    invoke-virtual {v9, v0, v12, v3}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;->matchesLocation(Ljava/lang/String;II)Z

    #@15f
    move-result v20

    #@160
    if-nez v20, :cond_14e

    #@162
    .line 1667
    invoke-interface {v11}, Ljava/util/Iterator;->remove()V
    :try_end_165
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_165} :catch_166

    #@165
    goto :goto_14e

    #@166
    .line 1670
    .end local v3           #cid:I
    .end local v4           #cl:Landroid/telephony/CellLocation;
    .end local v7           #header:Lcom/android/internal/telephony/gsm/SmsCbHeader;
    .end local v9           #info:Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;
    .end local v11           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/internal/telephony/gsm/GsmSMSDispatcher$SmsCbConcatInfo;>;"
    .end local v12           #lac:I
    .end local v13           #location:Landroid/telephony/SmsCbLocation;
    .end local v14           #message:Landroid/telephony/SmsCbMessage;
    .end local v16           #pageCount:I
    .end local v17           #pdus:[[B
    .end local v18           #plmn:Ljava/lang/String;
    .end local v19           #receivedPdu:[B
    :catch_166
    move-exception v6

    #@167
    .line 1671
    .local v6, e:Ljava/lang/RuntimeException;
    const-string v20, "GSM"

    #@169
    const-string v21, "Error in decoding SMS CB pdu"

    #@16b
    move-object/from16 v0, v20

    #@16d
    move-object/from16 v1, v21

    #@16f
    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@172
    goto/16 :goto_91

    #@174
    .line 1634
    .end local v6           #e:Ljava/lang/RuntimeException;
    .restart local v3       #cid:I
    .restart local v4       #cl:Landroid/telephony/CellLocation;
    .restart local v7       #header:Lcom/android/internal/telephony/gsm/SmsCbHeader;
    .restart local v12       #lac:I
    .restart local v13       #location:Landroid/telephony/SmsCbLocation;
    .restart local v16       #pageCount:I
    .restart local v18       #plmn:Ljava/lang/String;
    .restart local v19       #receivedPdu:[B
    :cond_174
    const/16 v20, 0x1

    #@176
    :try_start_176
    move/from16 v0, v20

    #@178
    new-array v0, v0, [[B

    #@17a
    move-object/from16 v17, v0

    #@17c
    .line 1635
    .restart local v17       #pdus:[[B
    const/16 v20, 0x0

    #@17e
    aput-object v19, v17, v20
    :try_end_180
    .catch Ljava/lang/RuntimeException; {:try_start_176 .. :try_end_180} :catch_166

    #@180
    goto/16 :goto_b4

    #@182
    .line 1587
    :pswitch_data_182
    .packed-switch 0x0
        :pswitch_9e
        :pswitch_40
        :pswitch_92
        :pswitch_9e
    .end packed-switch
.end method

.method private handleStatusReport(Landroid/os/AsyncResult;)V
    .registers 14
    .parameter "ar"

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    .line 277
    iget-object v5, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3
    check-cast v5, Ljava/lang/String;

    #@5
    .line 278
    .local v5, pduString:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/internal/telephony/gsm/SmsMessage;->newFromCDS(Ljava/lang/String;)Lcom/android/internal/telephony/gsm/SmsMessage;

    #@8
    move-result-object v6

    #@9
    .line 280
    .local v6, sms:Lcom/android/internal/telephony/gsm/SmsMessage;
    if-eqz v6, :cond_57

    #@b
    .line 281
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getStatus()I

    #@e
    move-result v7

    #@f
    .line 282
    .local v7, tpStatus:I
    iget v4, v6, Lcom/android/internal/telephony/SmsMessageBase;->messageRef:I

    #@11
    .line 283
    .local v4, messageRef:I
    const/4 v2, 0x0

    #@12
    .local v2, i:I
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->deliveryPendingList:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@17
    move-result v0

    #@18
    .local v0, count:I
    :goto_18
    if-ge v2, v0, :cond_57

    #@1a
    .line 284
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->deliveryPendingList:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v8

    #@20
    check-cast v8, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@22
    .line 285
    .local v8, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    iget v9, v8, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mMessageRef:I

    #@24
    if-ne v9, v4, :cond_5c

    #@26
    .line 287
    const/16 v9, 0x40

    #@28
    if-ge v7, v9, :cond_2e

    #@2a
    const/16 v9, 0x20

    #@2c
    if-ge v7, v9, :cond_33

    #@2e
    .line 288
    :cond_2e
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->deliveryPendingList:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@33
    .line 290
    :cond_33
    iget-object v3, v8, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mDeliveryIntent:Landroid/app/PendingIntent;

    #@35
    .line 291
    .local v3, intent:Landroid/app/PendingIntent;
    new-instance v1, Landroid/content/Intent;

    #@37
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@3a
    .line 292
    .local v1, fillIn:Landroid/content/Intent;
    const-string v9, "pdu"

    #@3c
    invoke-static {v5}, Lcom/android/internal/telephony/uicc/IccUtils;->hexStringToBytes(Ljava/lang/String;)[B

    #@3f
    move-result-object v10

    #@40
    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    #@43
    .line 293
    const-string v9, "format"

    #@45
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@48
    move-result-object v10

    #@49
    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4c
    .line 295
    :try_start_4c
    const-string v9, "handleStatusReport(), UI <-- GsmSMSDispatcher"

    #@4e
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@51
    .line 296
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@53
    const/4 v10, -0x1

    #@54
    invoke-virtual {v3, v9, v10, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_57
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_4c .. :try_end_57} :catch_5f

    #@57
    .line 304
    .end local v0           #count:I
    .end local v1           #fillIn:Landroid/content/Intent;
    .end local v2           #i:I
    .end local v3           #intent:Landroid/app/PendingIntent;
    .end local v4           #messageRef:I
    .end local v7           #tpStatus:I
    .end local v8           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :cond_57
    :goto_57
    const/4 v9, 0x0

    #@58
    invoke-virtual {p0, v11, v11, v9}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->acknowledgeLastIncomingSms(ZILandroid/os/Message;)V

    #@5b
    .line 305
    return-void

    #@5c
    .line 283
    .restart local v0       #count:I
    .restart local v2       #i:I
    .restart local v4       #messageRef:I
    .restart local v7       #tpStatus:I
    .restart local v8       #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :cond_5c
    add-int/lit8 v2, v2, 0x1

    #@5e
    goto :goto_18

    #@5f
    .line 297
    .restart local v1       #fillIn:Landroid/content/Intent;
    .restart local v3       #intent:Landroid/app/PendingIntent;
    :catch_5f
    move-exception v9

    #@60
    goto :goto_57
.end method

.method private isItSignedByVZW(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .registers 14
    .parameter "pm"
    .parameter "pkgName"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 521
    const-string v9, "isItSignedByVZW(), Non-system app"

    #@3
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@6
    .line 524
    const/16 v9, 0x40

    #@8
    :try_start_8
    invoke-virtual {p1, p2, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8 .. :try_end_b} :catch_f

    #@b
    move-result-object v7

    #@c
    .line 530
    .local v7, pkgInfo:Landroid/content/pm/PackageInfo;
    if-nez v7, :cond_27

    #@e
    .line 554
    .end local v7           #pkgInfo:Landroid/content/pm/PackageInfo;
    :goto_e
    return v8

    #@f
    .line 525
    :catch_f
    move-exception v3

    #@10
    .line 526
    .local v3, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v9, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v10, "isItSignedByVZW(), Can\'t find applicaiton: "

    #@17
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v9

    #@1b
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v9

    #@1f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v9

    #@23
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@26
    goto :goto_e

    #@27
    .line 535
    .end local v3           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v7       #pkgInfo:Landroid/content/pm/PackageInfo;
    :cond_27
    iget-object v1, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@29
    .line 537
    .local v1, appSignatures:[Landroid/content/pm/Signature;
    if-nez v1, :cond_31

    #@2b
    .line 538
    const-string v9, "isItSignedByVZW(), Can\'t find signatures"

    #@2d
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@30
    goto :goto_e

    #@31
    .line 542
    :cond_31
    move-object v2, v1

    #@32
    .local v2, arr$:[Landroid/content/pm/Signature;
    array-length v6, v2

    #@33
    .local v6, len$:I
    const/4 v5, 0x0

    #@34
    .local v5, i$:I
    :goto_34
    if-ge v5, v6, :cond_73

    #@36
    aget-object v0, v2, v5

    #@38
    .line 543
    .local v0, appSignature:Landroid/content/pm/Signature;
    new-instance v9, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v10, "isItSignedByVZW(), application Signature : "

    #@3f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v9

    #@43
    invoke-virtual {v0}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    #@46
    move-result-object v10

    #@47
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v9

    #@4b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v9

    #@4f
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@52
    .line 544
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@54
    if-eqz v9, :cond_70

    #@56
    .line 545
    const/4 v4, 0x0

    #@57
    .local v4, i:I
    :goto_57
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@59
    array-length v9, v9

    #@5a
    if-ge v4, v9, :cond_70

    #@5c
    .line 546
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->VZWSignature:[Landroid/content/pm/Signature;

    #@5e
    aget-object v9, v9, v4

    #@60
    invoke-virtual {v9, v0}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v9

    #@64
    if-eqz v9, :cond_6d

    #@66
    .line 547
    const-string v8, "isItSignedByVZW(), signature Match"

    #@68
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@6b
    .line 548
    const/4 v8, 0x1

    #@6c
    goto :goto_e

    #@6d
    .line 545
    :cond_6d
    add-int/lit8 v4, v4, 0x1

    #@6f
    goto :goto_57

    #@70
    .line 542
    .end local v4           #i:I
    :cond_70
    add-int/lit8 v5, v5, 0x1

    #@72
    goto :goto_34

    #@73
    .line 553
    .end local v0           #appSignature:Landroid/content/pm/Signature;
    :cond_73
    const-string v9, "isItSignedByVZW(), not signature Match"

    #@75
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@78
    goto :goto_e
.end method

.method private kddiFilterMsgid(I)Z
    .registers 10
    .parameter "msg_id"

    #@0
    .prologue
    .line 1678
    const-string v5, "kddiFilterMsgid(), [KDDI] kddiFilterMsgid !!"

    #@2
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 1679
    const/4 v3, 0x0

    #@6
    .line 1683
    .local v3, maintenanceMode:I
    :try_start_6
    iget-object v5, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@8
    const-string v6, "com.kddi.maintenanceMode"

    #@a
    iget-object v7, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@c
    const/4 v7, 0x2

    #@d
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@10
    move-result-object v0

    #@11
    .line 1684
    .local v0, context:Landroid/content/Context;
    const-string v5, "pref"

    #@13
    iget-object v6, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@15
    const/4 v6, 0x4

    #@16
    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@19
    move-result-object v4

    #@1a
    .line 1686
    .local v4, pref:Landroid/content/SharedPreferences;
    const-string v5, "maintenanceMode"

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_20
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_20} :catch_52

    #@20
    move-result v3

    #@21
    .line 1691
    .end local v0           #context:Landroid/content/Context;
    .end local v4           #pref:Landroid/content/SharedPreferences;
    :goto_21
    new-instance v5, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v6, "kddiFilterMsgid(, [KDDI] Maintainanace mode value = "

    #@28
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@37
    .line 1693
    const/4 v2, 0x0

    #@38
    .line 1694
    .local v2, isDelivery:Z
    packed-switch v3, :pswitch_data_86

    #@3b
    .line 1713
    :cond_3b
    :goto_3b
    new-instance v5, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v6, "kddiFilterMsgid(), [KDDI] isDelivery = "

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@51
    .line 1714
    return v2

    #@52
    .line 1687
    .end local v2           #isDelivery:Z
    :catch_52
    move-exception v1

    #@53
    .line 1688
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "kddiFilterMsgid(), [KDDI] maintenanceMode app not found"

    #@55
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@58
    goto :goto_21

    #@59
    .line 1696
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2       #isDelivery:Z
    :pswitch_59
    const-string v5, "kddiFilterMsgid(), [KDDI] COMMERCIAL_MODE "

    #@5b
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5e
    .line 1697
    const/16 v5, 0x1100

    #@60
    if-eq p1, v5, :cond_7d

    #@62
    const/16 v5, 0x1101

    #@64
    if-eq p1, v5, :cond_7d

    #@66
    const/16 v5, 0x1104

    #@68
    if-gt v5, p1, :cond_6e

    #@6a
    const/16 v5, 0x1107

    #@6c
    if-le p1, v5, :cond_7d

    #@6e
    :cond_6e
    const v5, 0xa003

    #@71
    if-eq p1, v5, :cond_7d

    #@73
    const v5, 0xa801

    #@76
    if-gt v5, p1, :cond_3b

    #@78
    const v5, 0xa8ff

    #@7b
    if-gt p1, v5, :cond_3b

    #@7d
    .line 1699
    :cond_7d
    const/4 v2, 0x1

    #@7e
    goto :goto_3b

    #@7f
    .line 1705
    :pswitch_7f
    const-string v5, "kddiFilterMsgid(), [KDDI]TEST_MODE "

    #@81
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@84
    .line 1706
    const/4 v2, 0x1

    #@85
    .line 1707
    goto :goto_3b

    #@86
    .line 1694
    :pswitch_data_86
    .packed-switch 0x0
        :pswitch_59
        :pswitch_7f
        :pswitch_7f
        :pswitch_7f
    .end packed-switch
.end method

.method private onUpdateIccAvailability()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1470
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1495
    :cond_5
    :goto_5
    return-void

    #@6
    .line 1474
    :cond_6
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getUiccCardApplication()Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    move-result-object v1

    #@a
    .line 1476
    .local v1, newUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@c
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@12
    .line 1477
    .local v0, app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eq v0, v1, :cond_5

    #@14
    .line 1478
    if-eqz v0, :cond_38

    #@16
    .line 1479
    const-string v2, "onUpdateIccAvailability(), Removing stale icc objects."

    #@18
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1b
    .line 1480
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@1d
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@20
    move-result-object v2

    #@21
    if-eqz v2, :cond_2e

    #@23
    .line 1481
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@25
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@28
    move-result-object v2

    #@29
    check-cast v2, Lcom/android/internal/telephony/uicc/IccRecords;

    #@2b
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForNewSms(Landroid/os/Handler;)V

    #@2e
    .line 1483
    :cond_2e
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@30
    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@33
    .line 1484
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@35
    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@38
    .line 1486
    :cond_38
    if-eqz v1, :cond_5

    #@3a
    .line 1487
    const-string v2, "onUpdateIccAvailability(), New Uicc application found"

    #@3c
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@3f
    .line 1488
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@41
    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@44
    .line 1489
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@46
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@4d
    .line 1490
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@4f
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@52
    move-result-object v2

    #@53
    if-eqz v2, :cond_5

    #@55
    .line 1491
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@57
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5a
    move-result-object v2

    #@5b
    check-cast v2, Lcom/android/internal/telephony/uicc/IccRecords;

    #@5d
    const/16 v3, 0xd

    #@5f
    invoke-virtual {v2, p0, v3, v4}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForNewSms(Landroid/os/Handler;ILjava/lang/Object;)V

    #@62
    goto :goto_5
.end method

.method private replyOptionDestnationNumber(Ljava/lang/String;I)Ljava/lang/String;
    .registers 6
    .parameter "number"
    .parameter "replyOption"

    #@0
    .prologue
    .line 1771
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "confirmRead"

    #@4
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_10

    #@a
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isReleaseOperator()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_11

    #@10
    .line 1791
    .end local p1
    :cond_10
    :goto_10
    return-object p1

    #@11
    .line 1776
    .restart local p1
    :cond_11
    new-instance v0, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    .line 1777
    .local v0, replyOptionDest:Ljava/lang/StringBuilder;
    packed-switch p2, :pswitch_data_34

    #@19
    .line 1790
    :goto_19
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 1791
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object p1

    #@20
    goto :goto_10

    #@21
    .line 1779
    :pswitch_21
    const-string v1, "##4323"

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    goto :goto_19

    #@27
    .line 1782
    :pswitch_27
    const-string v1, "##4325"

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    goto :goto_19

    #@2d
    .line 1785
    :pswitch_2d
    const-string v1, "##4324"

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    goto :goto_19

    #@33
    .line 1777
    nop

    #@34
    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_21
        :pswitch_27
        :pswitch_2d
    .end packed-switch
.end method

.method private static resultToCause(I)I
    .registers 2
    .parameter "rc"

    #@0
    .prologue
    .line 1452
    packed-switch p0, :pswitch_data_c

    #@3
    .line 1461
    :pswitch_3
    const/16 v0, 0xff

    #@5
    :goto_5
    return v0

    #@6
    .line 1456
    :pswitch_6
    const/4 v0, 0x0

    #@7
    goto :goto_5

    #@8
    .line 1458
    :pswitch_8
    const/16 v0, 0xd3

    #@a
    goto :goto_5

    #@b
    .line 1452
    nop

    #@c
    :pswitch_data_c
    .packed-switch -0x1
        :pswitch_6
        :pswitch_3
        :pswitch_6
        :pswitch_3
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method protected SendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 15
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 931
    if-eqz p5, :cond_1e

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    invoke-static {p2, p1, p3, v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@6
    move-result-object v4

    #@7
    .line 935
    .local v4, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz v4, :cond_20

    #@9
    move-object v0, p0

    #@a
    move-object v1, p1

    #@b
    move-object v2, p2

    #@c
    move-object v3, p3

    #@d
    move-object v5, p6

    #@e
    .line 936
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;Ljava/lang/String;)Ljava/util/HashMap;

    #@11
    move-result-object v6

    #@12
    .line 937
    .local v6, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {p0, v6, p4, p5, v0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@19
    move-result-object v7

    #@1a
    .line 938
    .local v7, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@1d
    .line 942
    .end local v6           #map:Ljava/util/HashMap;
    .end local v7           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_1d
    return-void

    #@1e
    .line 931
    .end local v4           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_3

    #@20
    .line 940
    .restart local v4       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_20
    const-string v0, "GSM"

    #@22
    const-string v1, "GsmSMSDispatcher.sendText(): getSubmitPdu() returned null"

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    goto :goto_1d
.end method

.method protected acknowledgeLastIncomingSms(ZILandroid/os/Message;)V
    .registers 6
    .parameter "success"
    .parameter "result"
    .parameter "response"

    #@0
    .prologue
    .line 1447
    const-string v0, "acknowledgeLastIncomingSms(), GsmSMSDispatcher --> RIL"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@5
    .line 1448
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-static {p2}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->resultToCause(I)I

    #@a
    move-result v1

    #@b
    invoke-interface {v0, p1, v1, p3}, Lcom/android/internal/telephony/CommandsInterface;->acknowledgeLastIncomingGsmSms(ZILandroid/os/Message;)V

    #@e
    .line 1449
    return-void
.end method

.method protected calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 4
    .parameter "messageBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 1251
    invoke-static {p1, p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 4
    .parameter "messageBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 1243
    invoke-static {p1, p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public checkCompleteMsg(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III[B[[B)I
    .registers 38
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "currentSegment"
    .parameter "totalSegment"
    .parameter "pdu"
    .parameter "pdus"

    #@0
    .prologue
    .line 2110
    const-string v2, "checkCompleteMsg()"

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@5
    .line 2111
    const/16 v19, 0x0

    #@7
    .line 2113
    .local v19, cursor:Landroid/database/Cursor;
    new-instance v29, Ljava/util/ArrayList;

    #@9
    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    #@c
    .line 2114
    .local v29, whereArgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v28, Ljava/lang/StringBuilder;

    #@e
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    .line 2116
    .local v28, where:Ljava/lang/StringBuilder;
    const-string v2, "source_min = ? "

    #@13
    move-object/from16 v0, v28

    #@15
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 2117
    move-object/from16 v0, v29

    #@1a
    move-object/from16 v1, p2

    #@1c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1f
    .line 2121
    const-string v2, " AND reference_number = ?"

    #@21
    move-object/from16 v0, v28

    #@23
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 2122
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    move-object/from16 v0, v29

    #@2c
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    .line 2123
    const-string v2, " AND count = ?"

    #@31
    move-object/from16 v0, v28

    #@33
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 2124
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    move-object/from16 v0, v29

    #@3c
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3f
    .line 2125
    const-string v2, " AND tid = ?"

    #@41
    move-object/from16 v0, v28

    #@43
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    .line 2126
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@49
    move-result-object v2

    #@4a
    iget-object v2, v2, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@4c
    iget v2, v2, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@4e
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    move-object/from16 v0, v29

    #@54
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@57
    .line 2129
    const-string v2, " AND address = ?"

    #@59
    move-object/from16 v0, v28

    #@5b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    .line 2130
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDisplayOriginatingAddressEx()Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    move-object/from16 v0, v29

    #@64
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@67
    .line 2135
    const/16 v2, 0x8

    #@69
    new-array v4, v2, [Ljava/lang/String;

    #@6b
    const/4 v2, 0x0

    #@6c
    const-string v3, "_id"

    #@6e
    aput-object v3, v4, v2

    #@70
    const/4 v2, 0x1

    #@71
    const-string v3, "source_min"

    #@73
    aput-object v3, v4, v2

    #@75
    const/4 v2, 0x2

    #@76
    const-string v3, "reference_number"

    #@78
    aput-object v3, v4, v2

    #@7a
    const/4 v2, 0x3

    #@7b
    const-string v3, "count"

    #@7d
    aput-object v3, v4, v2

    #@7f
    const/4 v2, 0x4

    #@80
    const-string v3, "sequence"

    #@82
    aput-object v3, v4, v2

    #@84
    const/4 v2, 0x5

    #@85
    const-string v3, "pdu"

    #@87
    aput-object v3, v4, v2

    #@89
    const/4 v2, 0x6

    #@8a
    const-string v3, "address"

    #@8c
    aput-object v3, v4, v2

    #@8e
    const/4 v2, 0x7

    #@8f
    const-string v3, "date"

    #@91
    aput-object v3, v4, v2

    #@93
    .line 2153
    .local v4, projection:[Ljava/lang/String;
    :try_start_93
    new-instance v25, Ljava/util/ArrayList;

    #@95
    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    #@98
    .line 2154
    .local v25, rowIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/16 v21, 0x0

    #@9a
    .line 2155
    .local v21, expiredMsgCount:I
    const/4 v13, 0x0

    #@9b
    .line 2157
    .local v13, sameMsgCount:I
    move-object/from16 v0, p0

    #@9d
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@9f
    sget-object v3, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@a1
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v5

    #@a5
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    #@a8
    move-result v6

    #@a9
    new-array v6, v6, [Ljava/lang/String;

    #@ab
    move-object/from16 v0, v29

    #@ad
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@b0
    move-result-object v6

    #@b1
    check-cast v6, [Ljava/lang/String;

    #@b3
    const-string v7, "date"

    #@b5
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@b8
    move-result-object v19

    #@b9
    .line 2163
    if-nez v19, :cond_c7

    #@bb
    .line 2164
    const-string v2, "checkCompleteMsg(), cursor is null"

    #@bd
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_c0
    .catchall {:try_start_93 .. :try_end_c0} :catchall_265
    .catch Landroid/database/SQLException; {:try_start_93 .. :try_end_c0} :catch_258

    #@c0
    .line 2165
    const/4 v2, 0x1

    #@c1
    .line 2220
    if-eqz v19, :cond_c6

    #@c3
    .line 2221
    .end local v13           #sameMsgCount:I
    .end local v21           #expiredMsgCount:I
    .end local v25           #rowIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :goto_c3
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@c6
    .line 2218
    :cond_c6
    return v2

    #@c7
    .line 2167
    .restart local v13       #sameMsgCount:I
    .restart local v21       #expiredMsgCount:I
    .restart local v25       #rowIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_c7
    :try_start_c7
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    #@ca
    move-result v13

    #@cb
    .line 2168
    new-instance v2, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v3, "checkCompleteMsg(), sameMsgCount = "

    #@d2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v2

    #@d6
    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v2

    #@da
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v2

    #@de
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@e1
    .line 2169
    if-gtz v13, :cond_fa

    #@e3
    move-object/from16 v5, p0

    #@e5
    move-object/from16 v6, p1

    #@e7
    move-object/from16 v7, p2

    #@e9
    move/from16 v8, p3

    #@eb
    move/from16 v9, p4

    #@ed
    move/from16 v10, p5

    #@ef
    move-object/from16 v11, p6

    #@f1
    move-object/from16 v12, p7

    #@f3
    .line 2170
    invoke-direct/range {v5 .. v13}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->completeOrInsertLms(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III[B[[BI)I

    #@f6
    move-result v2

    #@f7
    .line 2220
    if-eqz v19, :cond_c6

    #@f9
    goto :goto_c3

    #@fa
    .line 2174
    :cond_fa
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    #@fd
    move-result v2

    #@fe
    if-nez v2, :cond_109

    #@100
    .line 2175
    const-string v2, "checkCompleteMsg(), cursor moveToFirst error"

    #@102
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@105
    .line 2176
    const/4 v2, 0x1

    #@106
    .line 2220
    if-eqz v19, :cond_c6

    #@108
    goto :goto_c3

    #@109
    .line 2180
    :cond_109
    const-string v2, "_id"

    #@10b
    move-object/from16 v0, v19

    #@10d
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@110
    move-result v2

    #@111
    move-object/from16 v0, v19

    #@113
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    #@116
    move-result-wide v23

    #@117
    .line 2181
    .local v23, rowId:J
    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@11a
    move-result-object v2

    #@11b
    move-object/from16 v0, v25

    #@11d
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@120
    .line 2182
    const-string v2, "sequence"

    #@122
    move-object/from16 v0, v19

    #@124
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@127
    move-result v2

    #@128
    move-object/from16 v0, v19

    #@12a
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    #@12d
    move-result v16

    #@12e
    .line 2183
    .local v16, currSegment:I
    const-string v2, "date"

    #@130
    move-object/from16 v0, v19

    #@132
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@135
    move-result v2

    #@136
    move-object/from16 v0, v19

    #@138
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    #@13b
    move-result-wide v2

    #@13c
    const-wide/16 v5, 0x3e8

    #@13e
    div-long v26, v2, v5

    #@140
    .line 2184
    .local v26, time:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@143
    move-result-wide v2

    #@144
    const-wide/16 v5, 0x3e8

    #@146
    div-long v17, v2, v5

    #@148
    .line 2186
    .local v17, currentTime:J
    const-string v2, "address"

    #@14a
    move-object/from16 v0, v19

    #@14c
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@14f
    move-result v2

    #@150
    move-object/from16 v0, v19

    #@152
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@155
    move-result-object v14

    #@156
    .line 2187
    .local v14, callback:Ljava/lang/String;
    const-string v2, "pdu"

    #@158
    move-object/from16 v0, v19

    #@15a
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@15d
    move-result v2

    #@15e
    move-object/from16 v0, v19

    #@160
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@163
    move-result-object v2

    #@164
    invoke-static {v2}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@167
    move-result-object v15

    #@168
    .line 2190
    .local v15, currPdu:[B
    add-int/lit8 v2, v16, -0x1

    #@16a
    aput-object v15, p7, v2

    #@16c
    .line 2192
    new-instance v2, Ljava/lang/StringBuilder;

    #@16e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@171
    const-string v3, "checkCompleteMsg(), rowId = "

    #@173
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v2

    #@177
    move-wide/from16 v0, v23

    #@179
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v2

    #@17d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@180
    move-result-object v2

    #@181
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@184
    .line 2193
    new-instance v2, Ljava/lang/StringBuilder;

    #@186
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@189
    const-string v3, "checkCompleteMsg(), time = "

    #@18b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v2

    #@18f
    move-wide/from16 v0, v26

    #@191
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@194
    move-result-object v2

    #@195
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v2

    #@199
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@19c
    .line 2194
    new-instance v2, Ljava/lang/StringBuilder;

    #@19e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a1
    const-string v3, "checkCompleteMsg(), currentTime = "

    #@1a3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v2

    #@1a7
    move-wide/from16 v0, v17

    #@1a9
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v2

    #@1ad
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b0
    move-result-object v2

    #@1b1
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1b4
    .line 2195
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b9
    const-string v3, "checkCompleteMsg(), text = "

    #@1bb
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1be
    move-result-object v2

    #@1bf
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v2

    #@1c3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c6
    move-result-object v2

    #@1c7
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1ca
    .line 2196
    new-instance v2, Ljava/lang/StringBuilder;

    #@1cc
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1cf
    const-string v3, "checkCompleteMsg(), callback = "

    #@1d1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v2

    #@1d5
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v2

    #@1d9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dc
    move-result-object v2

    #@1dd
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1e0
    .line 2198
    sub-long v2, v17, v26

    #@1e2
    const-wide/16 v5, 0x258

    #@1e4
    cmp-long v2, v2, v5

    #@1e6
    if-lez v2, :cond_201

    #@1e8
    .line 2199
    add-int/lit8 v21, v21, 0x1

    #@1ea
    .line 2200
    move-object/from16 v0, p0

    #@1ec
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@1ee
    sget-object v3, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@1f0
    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1f3
    move-result-object v5

    #@1f4
    invoke-virtual {v5}, Ljava/lang/Long;->toString()Ljava/lang/String;

    #@1f7
    move-result-object v5

    #@1f8
    invoke-static {v3, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@1fb
    move-result-object v3

    #@1fc
    const/4 v5, 0x0

    #@1fd
    const/4 v6, 0x0

    #@1fe
    invoke-virtual {v2, v3, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@201
    .line 2203
    :cond_201
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    #@204
    move-result v2

    #@205
    if-nez v2, :cond_109

    #@207
    .line 2205
    if-lez v21, :cond_240

    #@209
    .line 2206
    const/16 v22, 0x0

    #@20b
    .local v22, loop:I
    :goto_20b
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    #@20e
    move-result v2

    #@20f
    move/from16 v0, v22

    #@211
    if-ge v0, v2, :cond_23b

    #@213
    .line 2207
    move-object/from16 v0, p0

    #@215
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@217
    sget-object v5, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@219
    move-object/from16 v0, v25

    #@21b
    move/from16 v1, v22

    #@21d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@220
    move-result-object v2

    #@221
    check-cast v2, Ljava/lang/Long;

    #@223
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@226
    move-result-wide v6

    #@227
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@22a
    move-result-object v2

    #@22b
    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    #@22e
    move-result-object v2

    #@22f
    invoke-static {v5, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@232
    move-result-object v2

    #@233
    const/4 v5, 0x0

    #@234
    const/4 v6, 0x0

    #@235
    invoke-virtual {v3, v2, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@238
    .line 2206
    add-int/lit8 v22, v22, 0x1

    #@23a
    goto :goto_20b

    #@23b
    .line 2211
    :cond_23b
    const/4 v2, 0x1

    #@23c
    .line 2220
    if-eqz v19, :cond_c6

    #@23e
    goto/16 :goto_c3

    #@240
    .end local v22           #loop:I
    :cond_240
    move-object/from16 v5, p0

    #@242
    move-object/from16 v6, p1

    #@244
    move-object/from16 v7, p2

    #@246
    move/from16 v8, p3

    #@248
    move/from16 v9, p4

    #@24a
    move/from16 v10, p5

    #@24c
    move-object/from16 v11, p6

    #@24e
    move-object/from16 v12, p7

    #@250
    .line 2213
    invoke-direct/range {v5 .. v13}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->completeOrInsertLms(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III[B[[BI)I
    :try_end_253
    .catchall {:try_start_c7 .. :try_end_253} :catchall_265
    .catch Landroid/database/SQLException; {:try_start_c7 .. :try_end_253} :catch_258

    #@253
    move-result v2

    #@254
    .line 2220
    if-eqz v19, :cond_c6

    #@256
    goto/16 :goto_c3

    #@258
    .line 2216
    .end local v13           #sameMsgCount:I
    .end local v14           #callback:Ljava/lang/String;
    .end local v15           #currPdu:[B
    .end local v16           #currSegment:I
    .end local v17           #currentTime:J
    .end local v21           #expiredMsgCount:I
    .end local v23           #rowId:J
    .end local v25           #rowIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v26           #time:J
    :catch_258
    move-exception v20

    #@259
    .line 2217
    .local v20, e:Landroid/database/SQLException;
    :try_start_259
    const-string v2, "checkCompleteMsg(), SQLException occurs"

    #@25b
    move-object/from16 v0, v20

    #@25d
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_260
    .catchall {:try_start_259 .. :try_end_260} :catchall_265

    #@260
    .line 2218
    const/4 v2, 0x1

    #@261
    .line 2220
    if-eqz v19, :cond_c6

    #@263
    goto/16 :goto_c3

    #@265
    .end local v20           #e:Landroid/database/SQLException;
    :catchall_265
    move-exception v2

    #@266
    if-eqz v19, :cond_26b

    #@268
    .line 2221
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@26b
    .line 2220
    :cond_26b
    throw v2
.end method

.method public checkLmsDuplicated(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III[B)Z
    .registers 29
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "totalSegment"
    .parameter "currentSegment"
    .parameter "pdu"

    #@0
    .prologue
    .line 1912
    const-string v2, "checkLmsDuplicated()"

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@5
    .line 1913
    invoke-static/range {p6 .. p6}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@8
    move-result-object v16

    #@9
    .line 1914
    .local v16, pduStr:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "checkLmsDuplicated(), pduStr = "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    move-object/from16 v0, v16

    #@16
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@21
    .line 1915
    const/4 v11, 0x0

    #@22
    .line 1916
    .local v11, cursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    #@23
    .line 1917
    .local v8, bRet:Z
    new-instance v21, Ljava/util/ArrayList;

    #@25
    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    #@28
    .line 1918
    .local v21, whereArgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v20, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    .line 1920
    .local v20, where:Ljava/lang/StringBuilder;
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@30
    move-result v2

    #@31
    if-nez v2, :cond_184

    #@33
    .line 1921
    const-string v2, "source_min = ? "

    #@35
    move-object/from16 v0, v20

    #@37
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    .line 1922
    move-object/from16 v0, v21

    #@3c
    move-object/from16 v1, p2

    #@3e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@41
    .line 1926
    :goto_41
    const-string v2, " AND reference_number = ?"

    #@43
    move-object/from16 v0, v20

    #@45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    .line 1927
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    move-object/from16 v0, v21

    #@4e
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@51
    .line 1928
    const-string v2, " AND count = ?"

    #@53
    move-object/from16 v0, v20

    #@55
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    .line 1929
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    move-object/from16 v0, v21

    #@5e
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@61
    .line 1930
    const-string v2, " AND sequence = ?"

    #@63
    move-object/from16 v0, v20

    #@65
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    .line 1931
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6b
    move-result-object v2

    #@6c
    move-object/from16 v0, v21

    #@6e
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@71
    .line 1940
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDisplayOriginatingAddressEx()Ljava/lang/String;

    #@74
    move-result-object v2

    #@75
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@78
    move-result v2

    #@79
    if-nez v2, :cond_18d

    #@7b
    .line 1941
    const-string v2, " AND address = ?"

    #@7d
    move-object/from16 v0, v20

    #@7f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    .line 1942
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDisplayOriginatingAddressEx()Ljava/lang/String;

    #@85
    move-result-object v2

    #@86
    move-object/from16 v0, v21

    #@88
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8b
    .line 1946
    :goto_8b
    const-string v2, " AND tid = ?"

    #@8d
    move-object/from16 v0, v20

    #@8f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    .line 1947
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@95
    move-result-object v2

    #@96
    iget-object v2, v2, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@98
    iget v2, v2, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@9a
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@9d
    move-result-object v2

    #@9e
    move-object/from16 v0, v21

    #@a0
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a3
    .line 1949
    new-instance v2, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v3, "checkLmsDuplicated(), where = "

    #@aa
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v2

    #@ae
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v3

    #@b2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v2

    #@b6
    const-string v3, ", whereArgs.toString() = "

    #@b8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v2

    #@bc
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    #@bf
    move-result-object v3

    #@c0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v2

    #@c4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v2

    #@c8
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@cb
    .line 1951
    const/4 v2, 0x2

    #@cc
    new-array v4, v2, [Ljava/lang/String;

    #@ce
    const/4 v2, 0x0

    #@cf
    const-string v3, "_id"

    #@d1
    aput-object v3, v4, v2

    #@d3
    const/4 v2, 0x1

    #@d4
    const-string v3, "pdu"

    #@d6
    aput-object v3, v4, v2

    #@d8
    .line 1956
    .local v4, projection:[Ljava/lang/String;
    :try_start_d8
    move-object/from16 v0, p0

    #@da
    iget-object v2, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@dc
    sget-object v3, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@de
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v5

    #@e2
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    #@e5
    move-result v6

    #@e6
    new-array v6, v6, [Ljava/lang/String;

    #@e8
    move-object/from16 v0, v21

    #@ea
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@ed
    move-result-object v6

    #@ee
    check-cast v6, [Ljava/lang/String;

    #@f0
    const/4 v7, 0x0

    #@f1
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@f4
    move-result-object v11

    #@f5
    .line 1962
    if-eqz v11, :cond_17e

    #@f7
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    #@fa
    move-result v2

    #@fb
    if-lez v2, :cond_17e

    #@fd
    .line 1963
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    #@100
    .line 1964
    const/4 v12, 0x0

    #@101
    .line 1966
    .local v12, different:Z
    :cond_101
    const/4 v2, 0x1

    #@102
    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@105
    move-result-object v9

    #@106
    .line 1967
    .local v9, body:Ljava/lang/String;
    invoke-static {v9}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@109
    move-result-object v10

    #@10a
    .line 1968
    .local v10, body_cursor:[B
    invoke-static {v10}, Lcom/android/internal/telephony/gsm/SmsMessage;->createFromPdu([B)Lcom/android/internal/telephony/gsm/SmsMessage;

    #@10d
    move-result-object v17

    #@10e
    .line 1970
    .local v17, sm:Lcom/android/internal/telephony/gsm/SmsMessage;
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserData()[B

    #@111
    move-result-object v18

    #@112
    .line 1971
    .local v18, ud:[B
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserData()[B

    #@115
    move-result-object v19

    #@116
    .line 1973
    .local v19, ud_curr:[B
    move-object/from16 v0, v18

    #@118
    array-length v15, v0

    #@119
    .line 1974
    .local v15, len:I
    const/4 v14, 0x0

    #@11a
    .local v14, i:I
    :goto_11a
    if-ge v14, v15, :cond_126

    #@11c
    .line 1975
    aget-byte v2, v19, v14

    #@11e
    aget-byte v3, v18, v14

    #@120
    if-eq v2, v3, :cond_196

    #@122
    .line 1976
    const/4 v12, 0x1

    #@123
    .line 1977
    invoke-interface {v11}, Landroid/database/Cursor;->moveToLast()Z

    #@126
    .line 1982
    :cond_126
    new-instance v2, Ljava/lang/StringBuilder;

    #@128
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12b
    const-string v3, "checkLmsDuplicated(), pduStr_cursor = "

    #@12d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v2

    #@131
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v2

    #@135
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@138
    move-result-object v2

    #@139
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@13c
    .line 1983
    new-instance v2, Ljava/lang/StringBuilder;

    #@13e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@141
    const-string v3, "checkLmsDuplicated(), userdata1 = "

    #@143
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v2

    #@147
    invoke-static/range {v19 .. v19}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@14a
    move-result-object v3

    #@14b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v2

    #@14f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@152
    move-result-object v2

    #@153
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@156
    .line 1984
    new-instance v2, Ljava/lang/StringBuilder;

    #@158
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15b
    const-string v3, "checkLmsDuplicated(), userdata2 = "

    #@15d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v2

    #@161
    invoke-static/range {v18 .. v18}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@164
    move-result-object v3

    #@165
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v2

    #@169
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16c
    move-result-object v2

    #@16d
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@170
    .line 1985
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    #@173
    move-result v2

    #@174
    if-nez v2, :cond_101

    #@176
    .line 1987
    if-eqz v12, :cond_199

    #@178
    .line 1988
    const-string v2, "checkLmsDuplicated(), pduStr not equals pduStr_cursor"

    #@17a
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I
    :try_end_17d
    .catchall {:try_start_d8 .. :try_end_17d} :catchall_1aa
    .catch Landroid/database/SQLException; {:try_start_d8 .. :try_end_17d} :catch_1a0

    #@17d
    .line 1989
    const/4 v8, 0x0

    #@17e
    .line 1999
    .end local v9           #body:Ljava/lang/String;
    .end local v10           #body_cursor:[B
    .end local v12           #different:Z
    .end local v14           #i:I
    .end local v15           #len:I
    .end local v17           #sm:Lcom/android/internal/telephony/gsm/SmsMessage;
    .end local v18           #ud:[B
    .end local v19           #ud_curr:[B
    :cond_17e
    :goto_17e
    if-eqz v11, :cond_183

    #@180
    .line 2000
    :goto_180
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@183
    .line 2003
    :cond_183
    return v8

    #@184
    .line 1924
    .end local v4           #projection:[Ljava/lang/String;
    :cond_184
    const-string v2, "source_min is null"

    #@186
    move-object/from16 v0, v20

    #@188
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18b
    goto/16 :goto_41

    #@18d
    .line 1944
    :cond_18d
    const-string v2, " AND address is null"

    #@18f
    move-object/from16 v0, v20

    #@191
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    goto/16 :goto_8b

    #@196
    .line 1974
    .restart local v4       #projection:[Ljava/lang/String;
    .restart local v9       #body:Ljava/lang/String;
    .restart local v10       #body_cursor:[B
    .restart local v12       #different:Z
    .restart local v14       #i:I
    .restart local v15       #len:I
    .restart local v17       #sm:Lcom/android/internal/telephony/gsm/SmsMessage;
    .restart local v18       #ud:[B
    .restart local v19       #ud_curr:[B
    :cond_196
    add-int/lit8 v14, v14, 0x1

    #@198
    goto :goto_11a

    #@199
    .line 1991
    :cond_199
    :try_start_199
    const-string v2, "checkLmsDuplicated(), pduStr equals pduStr_cursor "

    #@19b
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I
    :try_end_19e
    .catchall {:try_start_199 .. :try_end_19e} :catchall_1aa
    .catch Landroid/database/SQLException; {:try_start_199 .. :try_end_19e} :catch_1a0

    #@19e
    .line 1992
    const/4 v8, 0x1

    #@19f
    goto :goto_17e

    #@1a0
    .line 1995
    .end local v9           #body:Ljava/lang/String;
    .end local v10           #body_cursor:[B
    .end local v12           #different:Z
    .end local v14           #i:I
    .end local v15           #len:I
    .end local v17           #sm:Lcom/android/internal/telephony/gsm/SmsMessage;
    .end local v18           #ud:[B
    .end local v19           #ud_curr:[B
    :catch_1a0
    move-exception v13

    #@1a1
    .line 1996
    .local v13, e:Landroid/database/SQLException;
    :try_start_1a1
    const-string v2, "checkLmsDuplicated(), SQLException occurs"

    #@1a3
    invoke-static {v2, v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1a6
    .catchall {:try_start_1a1 .. :try_end_1a6} :catchall_1aa

    #@1a6
    .line 1997
    const/4 v8, 0x1

    #@1a7
    .line 1999
    if-eqz v11, :cond_183

    #@1a9
    goto :goto_180

    #@1aa
    .end local v13           #e:Landroid/database/SQLException;
    :catchall_1aa
    move-exception v2

    #@1ab
    if-eqz v11, :cond_1b0

    #@1ad
    .line 2000
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@1b0
    .line 1999
    :cond_1b0
    throw v2
.end method

.method public checkValidLmsMessage(I)Z
    .registers 3
    .parameter "teleService"

    #@0
    .prologue
    .line 1891
    const v0, 0xf6fe

    #@3
    if-eq v0, p1, :cond_23

    #@5
    const v0, 0xc256

    #@8
    if-eq v0, p1, :cond_23

    #@a
    const v0, 0xc264

    #@d
    if-eq v0, p1, :cond_23

    #@f
    const v0, 0xc266

    #@12
    if-eq v0, p1, :cond_23

    #@14
    const v0, 0xc268

    #@17
    if-eq v0, p1, :cond_23

    #@19
    const v0, 0xc006

    #@1c
    if-eq v0, p1, :cond_23

    #@1e
    const v0, 0xc00b

    #@21
    if-ne v0, p1, :cond_25

    #@23
    .line 1898
    :cond_23
    const/4 v0, 0x1

    #@24
    .line 1900
    :goto_24
    return v0

    #@25
    :cond_25
    const/4 v0, 0x0

    #@26
    goto :goto_24
.end method

.method public deleteAllExpiredMsg(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III)V
    .registers 20
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "currentSegment"
    .parameter "totalSegment"

    #@0
    .prologue
    .line 2049
    const-string v0, "deleteExpiredMsg()"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@5
    .line 2051
    const/4 v8, 0x0

    #@6
    .line 2053
    .local v8, cursor:Landroid/database/Cursor;
    const/4 v0, 0x2

    #@7
    new-array v2, v0, [Ljava/lang/String;

    #@9
    const/4 v0, 0x0

    #@a
    const-string v1, "_id"

    #@c
    aput-object v1, v2, v0

    #@e
    const/4 v0, 0x1

    #@f
    const-string v1, "date"

    #@11
    aput-object v1, v2, v0

    #@13
    .line 2063
    .local v2, projection:[Ljava/lang/String;
    :try_start_13
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@15
    sget-object v1, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x0

    #@19
    const-string v5, "date"

    #@1b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1e
    move-result-object v8

    #@1f
    .line 2070
    if-eqz v8, :cond_bb

    #@21
    .line 2071
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@24
    move-result v0

    #@25
    if-nez v0, :cond_32

    #@27
    .line 2072
    const-string v0, "deleteExpiredMsg(), cursor moveToFirst error"

    #@29
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_2c
    .catchall {:try_start_13 .. :try_end_2c} :catchall_cb
    .catch Landroid/database/SQLException; {:try_start_13 .. :try_end_2c} :catch_c1

    #@2c
    .line 2097
    if-eqz v8, :cond_31

    #@2e
    .line 2098
    :goto_2e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@31
    .line 2101
    :cond_31
    return-void

    #@32
    .line 2076
    :cond_32
    :try_start_32
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@35
    move-result-wide v0

    #@36
    const-wide/16 v3, 0x3e8

    #@38
    div-long v6, v0, v3

    #@3a
    .line 2079
    .local v6, currentTime:J
    :cond_3a
    const-string v0, "_id"

    #@3c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3f
    move-result v0

    #@40
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    #@43
    move-result-wide v10

    #@44
    .line 2080
    .local v10, rowId:J
    const-string v0, "date"

    #@46
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@49
    move-result v0

    #@4a
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    #@4d
    move-result-wide v0

    #@4e
    const-wide/16 v3, 0x3e8

    #@50
    div-long v12, v0, v3

    #@52
    .line 2082
    .local v12, time:J
    new-instance v0, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v1, "deleteExpiredMsg(), rowId = "

    #@59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@68
    .line 2083
    new-instance v0, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v1, "deleteExpiredMsg(), time = "

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@76
    move-result-object v0

    #@77
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v0

    #@7b
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@7e
    .line 2084
    new-instance v0, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v1, "deleteExpiredMsg(), currentTime = "

    #@85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v0

    #@89
    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v0

    #@8d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v0

    #@91
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@94
    .line 2086
    sub-long v0, v6, v12

    #@96
    const-wide/16 v3, 0x258

    #@98
    cmp-long v0, v0, v3

    #@9a
    if-lez v0, :cond_b1

    #@9c
    .line 2087
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@9e
    sget-object v1, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@a0
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@a3
    move-result-object v3

    #@a4
    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    #@a7
    move-result-object v3

    #@a8
    invoke-static {v1, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@ab
    move-result-object v1

    #@ac
    const/4 v3, 0x0

    #@ad
    const/4 v4, 0x0

    #@ae
    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@b1
    .line 2090
    :cond_b1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@b4
    move-result v0

    #@b5
    if-nez v0, :cond_3a

    #@b7
    .line 2097
    .end local v6           #currentTime:J
    .end local v10           #rowId:J
    .end local v12           #time:J
    :goto_b7
    if-eqz v8, :cond_31

    #@b9
    goto/16 :goto_2e

    #@bb
    .line 2092
    :cond_bb
    const-string v0, "deleteExpiredMsg(), mResolver.query() returned null"

    #@bd
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_c0
    .catchall {:try_start_32 .. :try_end_c0} :catchall_cb
    .catch Landroid/database/SQLException; {:try_start_32 .. :try_end_c0} :catch_c1

    #@c0
    goto :goto_b7

    #@c1
    .line 2094
    :catch_c1
    move-exception v9

    #@c2
    .line 2095
    .local v9, e:Landroid/database/SQLException;
    :try_start_c2
    const-string v0, "deleteExpiredMsg(), SQLException occurs"

    #@c4
    invoke-static {v0, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c7
    .catchall {:try_start_c2 .. :try_end_c7} :catchall_cb

    #@c7
    .line 2097
    if-eqz v8, :cond_31

    #@c9
    goto/16 :goto_2e

    #@cb
    .end local v9           #e:Landroid/database/SQLException;
    :catchall_cb
    move-exception v0

    #@cc
    if-eqz v8, :cond_d1

    #@ce
    .line 2098
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@d1
    .line 2097
    :cond_d1
    throw v0
.end method

.method public deleteCompleteLmsMsg(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III)V
    .registers 18
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "currentSegment"
    .parameter "totalSegment"

    #@0
    .prologue
    .line 2310
    const-string v0, "deleteCompleteLmsMsg()"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@5
    .line 2311
    const/4 v6, 0x0

    #@6
    .line 2314
    .local v6, cursor:Landroid/database/Cursor;
    new-instance v11, Ljava/util/ArrayList;

    #@8
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    #@b
    .line 2315
    .local v11, whereArgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v10, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    .line 2317
    .local v10, where:Ljava/lang/StringBuilder;
    const-string v0, "source_min = ? "

    #@12
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 2318
    invoke-virtual {v11, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@18
    .line 2322
    const-string v0, " AND reference_number = ?"

    #@1a
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 2323
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@24
    .line 2324
    const-string v0, " AND count = ?"

    #@26
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    .line 2325
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@30
    .line 2326
    const-string v0, " AND tid = ?"

    #@32
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    .line 2327
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@38
    move-result-object v0

    #@39
    iget-object v0, v0, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@3b
    iget v0, v0, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@3d
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@44
    .line 2330
    const-string v0, " AND address = ?"

    #@46
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    .line 2331
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDisplayOriginatingAddressEx()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@50
    .line 2336
    const/4 v0, 0x1

    #@51
    new-array v2, v0, [Ljava/lang/String;

    #@53
    const/4 v0, 0x0

    #@54
    const-string v1, "_id"

    #@56
    aput-object v1, v2, v0

    #@58
    .line 2341
    .local v2, projection:[Ljava/lang/String;
    :try_start_58
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@5a
    sget-object v1, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@5c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@63
    move-result v4

    #@64
    new-array v4, v4, [Ljava/lang/String;

    #@66
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@69
    move-result-object v4

    #@6a
    check-cast v4, [Ljava/lang/String;

    #@6c
    const-string v5, "date"

    #@6e
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@71
    move-result-object v6

    #@72
    .line 2348
    if-eqz v6, :cond_c4

    #@74
    .line 2350
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@77
    move-result v0

    #@78
    if-nez v0, :cond_85

    #@7a
    .line 2351
    const-string v0, "deleteCompleteLmsMsg(), cursor moveToFirst error"

    #@7c
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_7f
    .catchall {:try_start_58 .. :try_end_7f} :catchall_d3
    .catch Landroid/database/SQLException; {:try_start_58 .. :try_end_7f} :catch_ca

    #@7f
    .line 2369
    if-eqz v6, :cond_84

    #@81
    .line 2370
    :goto_81
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@84
    .line 2373
    :cond_84
    return-void

    #@85
    .line 2356
    :cond_85
    :try_start_85
    const-string v0, "_id"

    #@87
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@8a
    move-result v0

    #@8b
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    #@8e
    move-result-wide v8

    #@8f
    .line 2357
    .local v8, rowId:J
    new-instance v0, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string v1, "deleteCompleteLmsMsg(), rowId = "

    #@96
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v0

    #@9a
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v0

    #@9e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v0

    #@a2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@a5
    .line 2358
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@a7
    sget-object v1, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@a9
    new-instance v3, Ljava/lang/Long;

    #@ab
    invoke-direct {v3, v8, v9}, Ljava/lang/Long;-><init>(J)V

    #@ae
    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    #@b1
    move-result-object v3

    #@b2
    invoke-static {v1, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@b5
    move-result-object v1

    #@b6
    const/4 v3, 0x0

    #@b7
    const/4 v4, 0x0

    #@b8
    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@bb
    .line 2360
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@be
    move-result v0

    #@bf
    if-nez v0, :cond_85

    #@c1
    .line 2369
    .end local v8           #rowId:J
    :goto_c1
    if-eqz v6, :cond_84

    #@c3
    goto :goto_81

    #@c4
    .line 2363
    :cond_c4
    const-string v0, "deleteCompleteLmsMsg(), mResolver.query() returned null"

    #@c6
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_c9
    .catchall {:try_start_85 .. :try_end_c9} :catchall_d3
    .catch Landroid/database/SQLException; {:try_start_85 .. :try_end_c9} :catch_ca

    #@c9
    goto :goto_c1

    #@ca
    .line 2365
    :catch_ca
    move-exception v7

    #@cb
    .line 2366
    .local v7, e:Landroid/database/SQLException;
    :try_start_cb
    const-string v0, "deleteCompleteLmsMsg(), SQLException occurs"

    #@cd
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_d0
    .catchall {:try_start_cb .. :try_end_d0} :catchall_d3

    #@d0
    .line 2369
    if-eqz v6, :cond_84

    #@d2
    goto :goto_81

    #@d3
    .end local v7           #e:Landroid/database/SQLException;
    :catchall_d3
    move-exception v0

    #@d4
    if-eqz v6, :cond_d9

    #@d6
    .line 2370
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@d9
    .line 2369
    :cond_d9
    throw v0
.end method

.method protected dispatchMessage(Lcom/android/internal/telephony/SmsMessageBase;)I
    .registers 16
    .parameter "smsb"

    #@0
    .prologue
    .line 312
    if-nez p1, :cond_b

    #@2
    .line 313
    const-string v10, "GSM"

    #@4
    const-string v11, "dispatchMessage: message is null"

    #@6
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 314
    const/4 v10, 0x2

    #@a
    .line 483
    :goto_a
    return v10

    #@b
    :cond_b
    move-object v6, p1

    #@c
    .line 317
    check-cast v6, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@e
    .line 320
    .local v6, sms:Lcom/android/internal/telephony/gsm/SmsMessage;
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@10
    const-string v11, "SKTfindFriends"

    #@12
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@15
    move-result v10

    #@16
    if-nez v10, :cond_27

    #@18
    .line 322
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->isTypeZero()Z

    #@1b
    move-result v10

    #@1c
    if-eqz v10, :cond_27

    #@1e
    .line 325
    const-string v10, "GSM"

    #@20
    const-string v11, "Received short message type 0, Don\'t display or store it. Send Ack"

    #@22
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 326
    const/4 v10, 0x1

    #@26
    goto :goto_a

    #@27
    .line 332
    :cond_27
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->isUsimDataDownload()Z

    #@2a
    move-result v10

    #@2b
    if-eqz v10, :cond_74

    #@2d
    .line 333
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2f
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getUsimServiceTable()Lcom/android/internal/telephony/uicc/UsimServiceTable;

    #@32
    move-result-object v9

    #@33
    .line 338
    .local v9, ust:Lcom/android/internal/telephony/uicc/UsimServiceTable;
    if-eqz v9, :cond_4b

    #@35
    sget-object v10, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->DATA_DL_VIA_SMS_PP:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@37
    invoke-virtual {v9, v10}, Lcom/android/internal/telephony/uicc/UsimServiceTable;->isAvailable(Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;)Z

    #@3a
    move-result v10

    #@3b
    if-eqz v10, :cond_4b

    #@3d
    .line 340
    const-string v10, "GSM"

    #@3f
    const-string v11, "Received SMS-PP data download, sending to UICC."

    #@41
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 341
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mDataDownloadHandler:Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;

    #@46
    invoke-virtual {v10, v6}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->startDataDownload(Lcom/android/internal/telephony/gsm/SmsMessage;)I

    #@49
    move-result v10

    #@4a
    goto :goto_a

    #@4b
    .line 343
    :cond_4b
    const-string v10, "GSM"

    #@4d
    const-string v11, "DATA_DL_VIA_SMS_PP service not available, storing message to UICC."

    #@4f
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 344
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getServiceCenterAddress()Ljava/lang/String;

    #@55
    move-result-object v10

    #@56
    invoke-static {v10}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCDWithLength(Ljava/lang/String;)[B

    #@59
    move-result-object v10

    #@5a
    invoke-static {v10}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@5d
    move-result-object v8

    #@5e
    .line 347
    .local v8, smsc:Ljava/lang/String;
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@60
    const/4 v11, 0x3

    #@61
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getPdu()[B

    #@64
    move-result-object v12

    #@65
    invoke-static {v12}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@68
    move-result-object v12

    #@69
    const/16 v13, 0x66

    #@6b
    invoke-virtual {p0, v13}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->obtainMessage(I)Landroid/os/Message;

    #@6e
    move-result-object v13

    #@6f
    invoke-interface {v10, v11, v8, v12, v13}, Lcom/android/internal/telephony/CommandsInterface;->writeSmsToSim(ILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@72
    .line 350
    const/4 v10, -0x1

    #@73
    goto :goto_a

    #@74
    .line 354
    .end local v8           #smsc:Ljava/lang/String;
    .end local v9           #ust:Lcom/android/internal/telephony/uicc/UsimServiceTable;
    :cond_74
    iget-boolean v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mSmsReceiveDisabled:Z

    #@76
    if-eqz v10, :cond_81

    #@78
    .line 356
    const-string v10, "GSM"

    #@7a
    const-string v11, "Received short message on device which doesn\'t support SMS service. Ignored."

    #@7c
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 358
    const/4 v10, 0x1

    #@80
    goto :goto_a

    #@81
    .line 362
    :cond_81
    const/4 v1, 0x0

    #@82
    .line 363
    .local v1, handled:Z
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->isMWISetMessage()Z

    #@85
    move-result v10

    #@86
    if-eqz v10, :cond_d9

    #@88
    .line 364
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getNumOfVoicemails()I

    #@8b
    move-result v10

    #@8c
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->updateMessageWaitingIndicator(I)V

    #@8f
    .line 365
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->isMwiDontStore()Z

    #@92
    move-result v1

    #@93
    .line 369
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@95
    const-string v11, "mwi_only_notify"

    #@97
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9a
    move-result v10

    #@9b
    const/4 v11, 0x1

    #@9c
    if-ne v10, v11, :cond_d4

    #@9e
    .line 370
    const/4 v10, 0x1

    #@9f
    if-ne v1, v10, :cond_d4

    #@a1
    .line 371
    const/4 v10, 0x1

    #@a2
    new-array v5, v10, [[B

    #@a4
    .line 372
    .local v5, pdus:[[B
    const/4 v10, 0x0

    #@a5
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getPdu()[B

    #@a8
    move-result-object v11

    #@a9
    aput-object v11, v5, v10

    #@ab
    .line 374
    new-instance v3, Landroid/content/Intent;

    #@ad
    const-string v10, "android.provider.Telephony.SMS_RECEIVED"

    #@af
    invoke-direct {v3, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b2
    .line 375
    .local v3, intent:Landroid/content/Intent;
    const-string v10, "pdus"

    #@b4
    invoke-virtual {v3, v10, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@b7
    .line 376
    const-string v10, "encoding"

    #@b9
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getEncoding()I

    #@bc
    move-result v11

    #@bd
    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@c0
    .line 377
    const-string v10, "notionly"

    #@c2
    const/4 v11, 0x1

    #@c3
    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@c6
    .line 378
    const-string v10, "format"

    #@c8
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@cb
    move-result-object v11

    #@cc
    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@cf
    .line 380
    const-string v10, "android.permission.RECEIVE_SMS"

    #@d1
    invoke-virtual {p0, v3, v10}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@d4
    .line 415
    .end local v3           #intent:Landroid/content/Intent;
    .end local v5           #pdus:[[B
    :cond_d4
    :goto_d4
    if-eqz v1, :cond_129

    #@d6
    .line 416
    const/4 v10, 0x1

    #@d7
    goto/16 :goto_a

    #@d9
    .line 388
    :cond_d9
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->isMWIClearMessage()Z

    #@dc
    move-result v10

    #@dd
    if-eqz v10, :cond_d4

    #@df
    .line 389
    const/4 v10, 0x0

    #@e0
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->updateMessageWaitingIndicator(I)V

    #@e3
    .line 390
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->isMwiDontStore()Z

    #@e6
    move-result v1

    #@e7
    .line 394
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@e9
    const-string v11, "mwi_only_notify"

    #@eb
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@ee
    move-result v10

    #@ef
    const/4 v11, 0x1

    #@f0
    if-ne v10, v11, :cond_d4

    #@f2
    .line 395
    const/4 v10, 0x1

    #@f3
    if-ne v1, v10, :cond_d4

    #@f5
    .line 396
    const/4 v10, 0x1

    #@f6
    new-array v5, v10, [[B

    #@f8
    .line 397
    .restart local v5       #pdus:[[B
    const/4 v10, 0x0

    #@f9
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getPdu()[B

    #@fc
    move-result-object v11

    #@fd
    aput-object v11, v5, v10

    #@ff
    .line 399
    new-instance v3, Landroid/content/Intent;

    #@101
    const-string v10, "android.provider.Telephony.SMS_RECEIVED"

    #@103
    invoke-direct {v3, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@106
    .line 400
    .restart local v3       #intent:Landroid/content/Intent;
    const-string v10, "pdus"

    #@108
    invoke-virtual {v3, v10, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@10b
    .line 401
    const-string v10, "encoding"

    #@10d
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getEncoding()I

    #@110
    move-result v11

    #@111
    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@114
    .line 402
    const-string v10, "notionly"

    #@116
    const/4 v11, 0x1

    #@117
    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@11a
    .line 403
    const-string v10, "format"

    #@11c
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@11f
    move-result-object v11

    #@120
    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@123
    .line 405
    const-string v10, "android.permission.RECEIVE_SMS"

    #@125
    invoke-virtual {p0, v3, v10}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@128
    goto :goto_d4

    #@129
    .line 424
    .end local v3           #intent:Landroid/content/Intent;
    .end local v5           #pdus:[[B
    :cond_129
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@12b
    invoke-virtual {v10}, Lcom/android/internal/telephony/SmsStorageMonitor;->isStorageAvailable()Z

    #@12e
    move-result v10

    #@12f
    if-nez v10, :cond_15d

    #@131
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getMessageClass()Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@134
    move-result-object v10

    #@135
    sget-object v11, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_0:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@137
    if-eq v10, v11, :cond_15d

    #@139
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getMessageClass()Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@13c
    move-result-object v10

    #@13d
    sget-object v11, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_2:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@13f
    if-eq v10, v11, :cond_15d

    #@141
    .line 428
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@144
    move-result-object v7

    #@145
    .line 429
    .local v7, smsHeader2:Lcom/android/internal/telephony/SmsHeader;
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@147
    const-string v11, "DCM_MEMFULL_SMSPUSH"

    #@149
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@14c
    move-result v10

    #@14d
    if-eqz v10, :cond_176

    #@14f
    if-eqz v7, :cond_176

    #@151
    iget-object v10, v7, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@153
    if-eqz v10, :cond_176

    #@155
    iget-object v10, v7, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@157
    iget v10, v10, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@159
    const/16 v11, 0xb84

    #@15b
    if-ne v10, v11, :cond_176

    #@15d
    .line 458
    .end local v7           #smsHeader2:Lcom/android/internal/telephony/SmsHeader;
    :cond_15d
    :goto_15d
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@15f
    const-string v11, "app_directed_sms"

    #@161
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@164
    move-result v10

    #@165
    if-eqz v10, :cond_1d8

    #@167
    .line 459
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->parseDirectedSMS(Lcom/android/internal/telephony/gsm/SmsMessage;)I

    #@16a
    move-result v0

    #@16b
    .line 460
    .local v0, directedSmsStatus:I
    const/4 v10, 0x1

    #@16c
    if-ne v10, v0, :cond_1b8

    #@16e
    .line 461
    const-string v10, "dispatchMessage(), return parseDirectedSMS = true"

    #@170
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@173
    .line 462
    const/4 v10, 0x1

    #@174
    goto/16 :goto_a

    #@176
    .line 436
    .end local v0           #directedSmsStatus:I
    .restart local v7       #smsHeader2:Lcom/android/internal/telephony/SmsHeader;
    :cond_176
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mStorageMonitor:Lcom/android/internal/telephony/SmsStorageMonitor;

    #@178
    invoke-virtual {v10}, Lcom/android/internal/telephony/SmsStorageMonitor;->isStorageAvailable()Z

    #@17b
    move-result v10

    #@17c
    if-nez v10, :cond_19a

    #@17e
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->isMWISetMessage()Z

    #@181
    move-result v10

    #@182
    if-nez v10, :cond_18a

    #@184
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->isMWIClearMessage()Z

    #@187
    move-result v10

    #@188
    if-eqz v10, :cond_19a

    #@18a
    :cond_18a
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@18c
    const-string v11, "dcm_voicemail_receive_memoryfull"

    #@18e
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@191
    move-result v10

    #@192
    if-eqz v10, :cond_19a

    #@194
    .line 438
    const-string v10, "dispatchMessage(), KEY_SMS_DCM_VOICEMAIL_RECEIVE_MEMORYFULL"

    #@196
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@199
    goto :goto_15d

    #@19a
    .line 445
    :cond_19a
    iget-object v10, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@19c
    const-string v11, "lgu_dispatch"

    #@19e
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1a1
    move-result v10

    #@1a2
    if-eqz v10, :cond_1b5

    #@1a4
    .line 447
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@1a7
    move-result-object v10

    #@1a8
    iget-object v10, v10, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@1aa
    iget v10, v10, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@1ac
    invoke-static {v10}, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->isMessageProcessTid(I)Z

    #@1af
    move-result v10

    #@1b0
    if-eqz v10, :cond_15d

    #@1b2
    .line 448
    const/4 v10, 0x3

    #@1b3
    goto/16 :goto_a

    #@1b5
    .line 451
    :cond_1b5
    const/4 v10, 0x3

    #@1b6
    goto/16 :goto_a

    #@1b8
    .line 463
    .end local v7           #smsHeader2:Lcom/android/internal/telephony/SmsHeader;
    .restart local v0       #directedSmsStatus:I
    :cond_1b8
    if-nez v0, :cond_1c2

    #@1ba
    .line 464
    const-string v10, "dispatchMessage(), Discard!! there is no application for Application Directed SMS"

    #@1bc
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1bf
    .line 465
    const/4 v10, 0x1

    #@1c0
    goto/16 :goto_a

    #@1c2
    .line 467
    :cond_1c2
    new-instance v10, Ljava/lang/StringBuilder;

    #@1c4
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1c7
    const-string v11, "dispatchMessage(), directedSmsStatus = "

    #@1c9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v10

    #@1cd
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d0
    move-result-object v10

    #@1d1
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d4
    move-result-object v10

    #@1d5
    invoke-static {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1d8
    .line 473
    .end local v0           #directedSmsStatus:I
    :cond_1d8
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@1db
    move-result-object v4

    #@1dc
    .line 474
    .local v4, mSimInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    invoke-virtual {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@1df
    move-result-object v10

    #@1e0
    if-eqz v10, :cond_20b

    #@1e2
    invoke-virtual {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@1e5
    move-result-object v10

    #@1e6
    const-string v11, "208"

    #@1e8
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1eb
    move-result v10

    #@1ec
    if-eqz v10, :cond_20b

    #@1ee
    .line 475
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@1f1
    move-result-object v2

    #@1f2
    .line 476
    .local v2, headerForCheck:Lcom/android/internal/telephony/SmsHeader;
    if-eqz v2, :cond_1f8

    #@1f4
    iget-object v10, v2, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@1f6
    if-nez v10, :cond_20b

    #@1f8
    .line 477
    :cond_1f8
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->smsDupProc:Lcom/android/internal/telephony/LgeSmsDupProc;

    #@1fa
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getTimestampMillis()J

    #@1fd
    move-result-wide v11

    #@1fe
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@201
    move-result-object v13

    #@202
    invoke-virtual {v10, v11, v12, v13}, Lcom/android/internal/telephony/LgeSmsDupProc;->checkNetworkDuplicate(JLjava/lang/String;)Z

    #@205
    move-result v10

    #@206
    if-eqz v10, :cond_20b

    #@208
    .line 478
    const/4 v10, 0x1

    #@209
    goto/16 :goto_a

    #@20b
    .line 483
    .end local v2           #headerForCheck:Lcom/android/internal/telephony/SmsHeader;
    :cond_20b
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->dispatchNormalMessage(Lcom/android/internal/telephony/SmsMessageBase;)I

    #@20e
    move-result v10

    #@20f
    goto/16 :goto_a
.end method

.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 202
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnNewGsmSms(Landroid/os/Handler;)V

    #@5
    .line 205
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnSmsStatus(Landroid/os/Handler;)V

    #@a
    .line 206
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@c
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnNewGsmBroadcastSms(Landroid/os/Handler;)V

    #@f
    .line 213
    return-void
.end method

.method protected getEncoding()I
    .registers 2

    #@0
    .prologue
    .line 802
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected getFormat()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 217
    const-string v0, "3gpp"

    #@2
    return-object v0
.end method

.method public getImsSmsFormat()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1881
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mImsSMSDispatcher:Lcom/android/internal/telephony/ImsSMSDispatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->getImsSmsFormat()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected getUiccCardApplication()Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .registers 3

    #@0
    .prologue
    .line 1466
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 228
    iget v1, p1, Landroid/os/Message;->what:I

    #@4
    sparse-switch v1, :sswitch_data_6c

    #@7
    .line 265
    invoke-super {p0, p1}, Lcom/android/internal/telephony/SMSDispatcher;->handleMessage(Landroid/os/Message;)V

    #@a
    .line 267
    :cond_a
    :goto_a
    return-void

    #@b
    .line 230
    :sswitch_b
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d
    check-cast v1, Landroid/os/AsyncResult;

    #@f
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->handleStatusReport(Landroid/os/AsyncResult;)V

    #@12
    goto :goto_a

    #@13
    .line 234
    :sswitch_13
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@15
    check-cast v1, Landroid/os/AsyncResult;

    #@17
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->handleBroadcastSms(Landroid/os/AsyncResult;)V

    #@1a
    goto :goto_a

    #@1b
    .line 238
    :sswitch_1b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1d
    check-cast v0, Landroid/os/AsyncResult;

    #@1f
    .line 239
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@21
    if-nez v1, :cond_31

    #@23
    .line 240
    const-string v1, "GSM"

    #@25
    const-string v2, "Successfully wrote SMS-PP message to UICC"

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 241
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@2c
    const/4 v2, 0x1

    #@2d
    invoke-interface {v1, v2, v4, v5}, Lcom/android/internal/telephony/CommandsInterface;->acknowledgeLastIncomingGsmSms(ZILandroid/os/Message;)V

    #@30
    goto :goto_a

    #@31
    .line 243
    :cond_31
    const-string v1, "GSM"

    #@33
    const-string v2, "Failed to write SMS-PP message to UICC"

    #@35
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@37
    invoke-static {v1, v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3a
    .line 244
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@3c
    const/16 v2, 0xff

    #@3e
    invoke-interface {v1, v4, v2, v5}, Lcom/android/internal/telephony/CommandsInterface;->acknowledgeLastIncomingGsmSms(ZILandroid/os/Message;)V

    #@41
    goto :goto_a

    #@42
    .line 250
    .end local v0           #ar:Landroid/os/AsyncResult;
    :sswitch_42
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@44
    check-cast v0, Landroid/os/AsyncResult;

    #@46
    .line 251
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@48
    check-cast v1, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@4a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->dispatchMessage(Lcom/android/internal/telephony/SmsMessageBase;)I

    #@4d
    goto :goto_a

    #@4e
    .line 255
    .end local v0           #ar:Landroid/os/AsyncResult;
    :sswitch_4e
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->onUpdateIccAvailability()V

    #@51
    goto :goto_a

    #@52
    .line 259
    :sswitch_52
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@54
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@57
    move-result-object v1

    #@58
    if-eqz v1, :cond_a

    #@5a
    .line 260
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@5c
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5f
    move-result-object v1

    #@60
    check-cast v1, Lcom/android/internal/telephony/uicc/SIMRecords;

    #@62
    check-cast v1, Lcom/android/internal/telephony/uicc/SIMRecords;

    #@64
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@66
    check-cast v2, Landroid/os/AsyncResult;

    #@68
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/uicc/SIMRecords;->handleSmsOnIcc(Landroid/os/AsyncResult;)V

    #@6b
    goto :goto_a

    #@6c
    .line 228
    :sswitch_data_6c
    .sparse-switch
        0xd -> :sswitch_42
        0xe -> :sswitch_4e
        0xf -> :sswitch_52
        0x64 -> :sswitch_b
        0x65 -> :sswitch_13
        0x66 -> :sswitch_1b
    .end sparse-switch
.end method

.method public insertLmsMsg(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III[B)V
    .registers 19
    .parameter "sms"
    .parameter "sourceMin"
    .parameter "sessionId"
    .parameter "currentSegment"
    .parameter "totalSegment"
    .parameter "pdu"

    #@0
    .prologue
    .line 2247
    const-string v0, "insertLmsMsg()"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@5
    .line 2249
    const/4 v6, 0x0

    #@6
    .line 2251
    .local v6, cursor:Landroid/database/Cursor;
    const/4 v0, 0x1

    #@7
    new-array v2, v0, [Ljava/lang/String;

    #@9
    const/4 v0, 0x0

    #@a
    const-string v1, "_id"

    #@c
    aput-object v1, v2, v0

    #@e
    .line 2256
    .local v2, projection:[Ljava/lang/String;
    :try_start_e
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@10
    sget-object v1, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x0

    #@14
    const/4 v5, 0x0

    #@15
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@18
    move-result-object v6

    #@19
    .line 2259
    if-eqz v6, :cond_d7

    #@1b
    .line 2262
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@1e
    move-result v0

    #@1f
    const/16 v1, 0x32

    #@21
    if-lt v0, v1, :cond_6c

    #@23
    .line 2264
    const/4 v0, 0x1

    #@24
    new-array v8, v0, [Ljava/lang/String;

    #@26
    const/4 v0, 0x0

    #@27
    const-string v1, "_id"

    #@29
    aput-object v1, v8, v0
    :try_end_2b
    .catchall {:try_start_e .. :try_end_2b} :catchall_ea
    .catch Landroid/database/SQLException; {:try_start_e .. :try_end_2b} :catch_dd

    #@2b
    .line 2267
    .end local v2           #projection:[Ljava/lang/String;
    .local v8, projection:[Ljava/lang/String;
    :try_start_2b
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@2e
    move-result v0

    #@2f
    if-nez v0, :cond_3d

    #@31
    .line 2268
    const-string v0, "insertLmsMsg(), cursor moveToLast error"

    #@33
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_36
    .catchall {:try_start_2b .. :try_end_36} :catchall_f1
    .catch Landroid/database/SQLException; {:try_start_2b .. :try_end_36} :catch_f4

    #@36
    .line 2287
    if-eqz v6, :cond_3b

    #@38
    .line 2288
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3b
    :cond_3b
    move-object v2, v8

    #@3c
    .line 2305
    .end local v8           #projection:[Ljava/lang/String;
    .restart local v2       #projection:[Ljava/lang/String;
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .line 2272
    .end local v2           #projection:[Ljava/lang/String;
    .restart local v8       #projection:[Ljava/lang/String;
    :cond_3d
    :try_start_3d
    const-string v0, "_id"

    #@3f
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@42
    move-result v0

    #@43
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    #@46
    move-result-wide v9

    #@47
    .line 2273
    .local v9, rowId:J
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@49
    sget-object v1, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@4b
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    invoke-static {v1, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@56
    move-result-object v1

    #@57
    const/4 v3, 0x0

    #@58
    const/4 v4, 0x0

    #@59
    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@5c
    move-result v0

    #@5d
    if-gez v0, :cond_6b

    #@5f
    .line 2275
    const-string v0, "insertLmsMsg(), oldest lms delete fail"

    #@61
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_64
    .catchall {:try_start_3d .. :try_end_64} :catchall_f1
    .catch Landroid/database/SQLException; {:try_start_3d .. :try_end_64} :catch_f4

    #@64
    .line 2287
    if-eqz v6, :cond_69

    #@66
    .line 2288
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@69
    :cond_69
    move-object v2, v8

    #@6a
    .line 2276
    .end local v8           #projection:[Ljava/lang/String;
    .restart local v2       #projection:[Ljava/lang/String;
    goto :goto_3c

    #@6b
    .end local v2           #projection:[Ljava/lang/String;
    .restart local v8       #projection:[Ljava/lang/String;
    :cond_6b
    move-object v2, v8

    #@6c
    .line 2287
    .end local v8           #projection:[Ljava/lang/String;
    .end local v9           #rowId:J
    .restart local v2       #projection:[Ljava/lang/String;
    :cond_6c
    :goto_6c
    if-eqz v6, :cond_71

    #@6e
    .line 2288
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@71
    .line 2292
    :cond_71
    new-instance v11, Landroid/content/ContentValues;

    #@73
    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    #@76
    .line 2293
    .local v11, value:Landroid/content/ContentValues;
    const-string v0, "source_min"

    #@78
    invoke-virtual {v11, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7b
    .line 2294
    const-string v0, "reference_number"

    #@7d
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@80
    move-result-object v1

    #@81
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@84
    .line 2295
    const-string v0, "count"

    #@86
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@89
    move-result-object v1

    #@8a
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@8d
    .line 2296
    const-string v0, "sequence"

    #@8f
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@92
    move-result-object v1

    #@93
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@96
    .line 2297
    const-string v0, "pdu"

    #@98
    invoke-static/range {p6 .. p6}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@9b
    move-result-object v1

    #@9c
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9f
    .line 2298
    const-string v0, "address"

    #@a1
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDisplayOriginatingAddressEx()Ljava/lang/String;

    #@a4
    move-result-object v1

    #@a5
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a8
    .line 2299
    const-string v0, "date"

    #@aa
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getTimestampMillis()J

    #@ad
    move-result-wide v3

    #@ae
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@b1
    move-result-object v1

    #@b2
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@b5
    .line 2300
    const-string v0, "tid"

    #@b7
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@ba
    move-result-object v1

    #@bb
    iget-object v1, v1, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@bd
    iget v1, v1, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@bf
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c2
    move-result-object v1

    #@c3
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@c6
    .line 2301
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@c8
    sget-object v1, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->LMS_URI:Landroid/net/Uri;

    #@ca
    invoke-virtual {v0, v1, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@cd
    move-result-object v0

    #@ce
    if-nez v0, :cond_3c

    #@d0
    .line 2302
    const-string v0, "insertLmsMsg(), lms insert fail"

    #@d2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@d5
    goto/16 :goto_3c

    #@d7
    .line 2281
    .end local v11           #value:Landroid/content/ContentValues;
    :cond_d7
    :try_start_d7
    const-string v0, "insertLmsMsg(), mResolver.query() returned null"

    #@d9
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_dc
    .catchall {:try_start_d7 .. :try_end_dc} :catchall_ea
    .catch Landroid/database/SQLException; {:try_start_d7 .. :try_end_dc} :catch_dd

    #@dc
    goto :goto_6c

    #@dd
    .line 2283
    :catch_dd
    move-exception v7

    #@de
    .line 2284
    .local v7, e:Landroid/database/SQLException;
    :goto_de
    :try_start_de
    const-string v0, "insertLmsMsg(), SQLException occurs"

    #@e0
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_e3
    .catchall {:try_start_de .. :try_end_e3} :catchall_ea

    #@e3
    .line 2287
    if-eqz v6, :cond_3c

    #@e5
    .line 2288
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@e8
    goto/16 :goto_3c

    #@ea
    .line 2287
    .end local v7           #e:Landroid/database/SQLException;
    :catchall_ea
    move-exception v0

    #@eb
    :goto_eb
    if-eqz v6, :cond_f0

    #@ed
    .line 2288
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@f0
    .line 2287
    :cond_f0
    throw v0

    #@f1
    .end local v2           #projection:[Ljava/lang/String;
    .restart local v8       #projection:[Ljava/lang/String;
    :catchall_f1
    move-exception v0

    #@f2
    move-object v2, v8

    #@f3
    .end local v8           #projection:[Ljava/lang/String;
    .restart local v2       #projection:[Ljava/lang/String;
    goto :goto_eb

    #@f4
    .line 2283
    .end local v2           #projection:[Ljava/lang/String;
    .restart local v8       #projection:[Ljava/lang/String;
    :catch_f4
    move-exception v7

    #@f5
    move-object v2, v8

    #@f6
    .end local v8           #projection:[Ljava/lang/String;
    .restart local v2       #projection:[Ljava/lang/String;
    goto :goto_de
.end method

.method public isIms()Z
    .registers 2

    #@0
    .prologue
    .line 1766
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mImsSMSDispatcher:Lcom/android/internal/telephony/ImsSMSDispatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/ImsSMSDispatcher;->isIms()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method isSystemApplication(Landroid/content/pm/ApplicationInfo;)Z
    .registers 3
    .parameter "appInfo"

    #@0
    .prologue
    .line 558
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 559
    const-string v0, "isSystemApplication(), system app"

    #@8
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@b
    .line 560
    const/4 v0, 0x1

    #@c
    .line 562
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method protected parseDirectedSMS(Lcom/android/internal/telephony/gsm/SmsMessage;)I
    .registers 17
    .parameter "sms"

    #@0
    .prologue
    .line 566
    const/4 v10, 0x0

    #@1
    .line 567
    .local v10, processStatus:I
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@4
    move-result-object v13

    #@5
    if-nez v13, :cond_f

    #@7
    .line 568
    const-string v13, "parseDirectedSMS(), sms.getMessageBody() is NULL "

    #@9
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@c
    .line 569
    const/4 v10, -0x1

    #@d
    move v11, v10

    #@e
    .line 650
    .end local v10           #processStatus:I
    .local v11, processStatus:I
    :goto_e
    return v11

    #@f
    .line 571
    .end local v11           #processStatus:I
    .restart local v10       #processStatus:I
    :cond_f
    new-instance v13, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v14, "parseDirectedSMS(), sms.getMessageBody() = "

    #@16
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v13

    #@1a
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@1d
    move-result-object v14

    #@1e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v13

    #@22
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v13

    #@26
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->p(Ljava/lang/String;)I

    #@29
    .line 574
    new-instance v4, Ljava/util/ArrayList;

    #@2b
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@2e
    .line 575
    .local v4, applications:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    #@2f
    .line 579
    .local v7, packageIndex:I
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@32
    move-result-object v13

    #@33
    const-string v14, "//VZW"

    #@35
    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@38
    move-result v13

    #@39
    if-nez v13, :cond_43

    #@3b
    .line 580
    const-string v13, "parseDirectedSMS(), normal sms - not startsWith //VZW "

    #@3d
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@40
    .line 581
    const/4 v10, -0x1

    #@41
    move v11, v10

    #@42
    .end local v10           #processStatus:I
    .restart local v11       #processStatus:I
    goto :goto_e

    #@43
    .line 584
    .end local v11           #processStatus:I
    .restart local v10       #processStatus:I
    :cond_43
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getMessageBody()Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    .line 585
    .local v0, appDirectedSMS:Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v14, "parseDirectedSMS(), parseDirectedSMS / appDirectedSMS : "

    #@4e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v13

    #@52
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v13

    #@56
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v13

    #@5a
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5d
    .line 588
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@60
    move-result v13

    #@61
    const/16 v14, 0xe

    #@63
    if-le v13, v14, :cond_7c

    #@65
    const/4 v13, 0x0

    #@66
    const/16 v14, 0xd

    #@68
    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6b
    move-result-object v13

    #@6c
    const-string v14, "//VZWLBSROVER"

    #@6e
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v13

    #@72
    if-eqz v13, :cond_7c

    #@74
    .line 589
    const-string v13, "parseDirectedSMS(), //VZWLBSROVER"

    #@76
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@79
    .line 590
    const/4 v10, -0x1

    #@7a
    move v11, v10

    #@7b
    .end local v10           #processStatus:I
    .restart local v11       #processStatus:I
    goto :goto_e

    #@7c
    .line 595
    .end local v11           #processStatus:I
    .restart local v10       #processStatus:I
    :cond_7c
    const-string v13, "//VZW"

    #@7e
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@81
    move-result v13

    #@82
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@85
    move-result v14

    #@86
    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@89
    move-result-object v12

    #@8a
    .line 598
    .local v12, tempSMS:Ljava/lang/String;
    const-string v13, ":"

    #@8c
    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@8f
    move-result v13

    #@90
    const/4 v14, -0x1

    #@91
    if-ne v13, v14, :cond_9c

    #@93
    .line 599
    const-string v13, "parseDirectedSMS(), check separator"

    #@95
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@98
    .line 600
    const/4 v10, -0x1

    #@99
    move v11, v10

    #@9a
    .end local v10           #processStatus:I
    .restart local v11       #processStatus:I
    goto/16 :goto_e

    #@9c
    .line 604
    .end local v11           #processStatus:I
    .restart local v10       #processStatus:I
    :cond_9c
    iget-object v13, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@9e
    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@a1
    move-result-object v9

    #@a2
    .line 605
    .local v9, pkgManager:Landroid/content/pm/PackageManager;
    const/16 v13, 0x80

    #@a4
    invoke-virtual {v9, v13}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    #@a7
    move-result-object v6

    #@a8
    .line 608
    .local v6, installedAppList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getVZWSignatures(Landroid/content/pm/PackageManager;)Z

    #@ab
    .line 610
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@ae
    move-result-object v5

    #@af
    .local v5, i$:Ljava/util/Iterator;
    :goto_af
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@b2
    move-result v13

    #@b3
    if-eqz v13, :cond_18f

    #@b5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@b8
    move-result-object v1

    #@b9
    check-cast v1, Landroid/content/pm/ApplicationInfo;

    #@bb
    .line 611
    .local v1, appInfo:Landroid/content/pm/ApplicationInfo;
    new-instance v13, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v14, "parseDirectedSMS(), ("

    #@c2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v13

    #@c6
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v13

    #@ca
    const-string v14, ")"

    #@cc
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v13

    #@d0
    const-string v14, " appInfo.packageName: "

    #@d2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v13

    #@d6
    iget-object v14, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@d8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v13

    #@dc
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v13

    #@e0
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@e3
    .line 612
    iget-object v13, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@e5
    invoke-direct {p0, v9, v13}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->isItSignedByVZW(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    #@e8
    move-result v13

    #@e9
    if-nez v13, :cond_f1

    #@eb
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->isSystemApplication(Landroid/content/pm/ApplicationInfo;)Z

    #@ee
    move-result v13

    #@ef
    if-eqz v13, :cond_132

    #@f1
    .line 614
    :cond_f1
    iget-object v13, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@f3
    if-nez v13, :cond_fd

    #@f5
    .line 615
    const-string v13, "parseDirectedSMS(), appInfo.metaData == null"

    #@f7
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@fa
    .line 616
    add-int/lit8 v7, v7, 0x1

    #@fc
    .line 617
    goto :goto_af

    #@fd
    .line 621
    :cond_fd
    iget-object v13, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@ff
    const-string v14, "com.verizon.directedAppSMS"

    #@101
    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@104
    move-result-object v2

    #@105
    .line 622
    .local v2, applicationPrefix:Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    const-string v14, "parseDirectedSMS(), appInfo = "

    #@10c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v13

    #@110
    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v13

    #@114
    const-string v14, " appInfo.metaData = "

    #@116
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v13

    #@11a
    iget-object v14, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@11c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v13

    #@120
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@123
    move-result-object v13

    #@124
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@127
    .line 624
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@12a
    move-result v13

    #@12b
    if-eqz v13, :cond_136

    #@12d
    .line 626
    const-string v13, "parseDirectedSMS(), applicationPrefix is Empty"

    #@12f
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@132
    .line 645
    .end local v2           #applicationPrefix:Ljava/lang/String;
    :cond_132
    add-int/lit8 v7, v7, 0x1

    #@134
    goto/16 :goto_af

    #@136
    .line 628
    .restart local v2       #applicationPrefix:Ljava/lang/String;
    :cond_136
    new-instance v13, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v14, "parseDirectedSMS(), applicationPrefix: "

    #@13d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v13

    #@141
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v13

    #@145
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@148
    move-result-object v13

    #@149
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@14c
    .line 631
    const/4 v13, 0x0

    #@14d
    const-string v14, ":"

    #@14f
    invoke-virtual {v12, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@152
    move-result v14

    #@153
    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@156
    move-result-object v3

    #@157
    .line 632
    .local v3, applicationPrefixFromSMSBody:Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    #@159
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@15c
    const-string v14, "parseDirectedSMS(), applicationPrefixFromSMSBody: "

    #@15e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v13

    #@162
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v13

    #@166
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@169
    move-result-object v13

    #@16a
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16d
    .line 634
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@170
    move-result v13

    #@171
    if-eqz v13, :cond_132

    #@173
    .line 635
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@176
    move-result v13

    #@177
    add-int/lit8 v13, v13, 0x1

    #@179
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@17c
    move-result v14

    #@17d
    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@180
    move-result-object v8

    #@181
    .line 636
    .local v8, parameters:Ljava/lang/String;
    const-string v13, "parseDirectedSMS(), dispatchDirectedSms!!"

    #@183
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@186
    .line 638
    iget-object v13, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@188
    invoke-virtual {p0, v13, v8}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->dispatchDirectedSms(Ljava/lang/String;Ljava/lang/String;)V

    #@18b
    .line 640
    const/4 v10, 0x1

    #@18c
    move v11, v10

    #@18d
    .line 641
    .end local v10           #processStatus:I
    .restart local v11       #processStatus:I
    goto/16 :goto_e

    #@18f
    .line 647
    .end local v1           #appInfo:Landroid/content/pm/ApplicationInfo;
    .end local v2           #applicationPrefix:Ljava/lang/String;
    .end local v3           #applicationPrefixFromSMSBody:Ljava/lang/String;
    .end local v8           #parameters:Ljava/lang/String;
    .end local v11           #processStatus:I
    .restart local v10       #processStatus:I
    :cond_18f
    const/4 v10, 0x0

    #@190
    .line 649
    new-instance v13, Ljava/lang/StringBuilder;

    #@192
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@195
    const-string v14, "parseDirectedSMS(), processStatus=("

    #@197
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v13

    #@19b
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v13

    #@19f
    const-string v14, ")"

    #@1a1
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v13

    #@1a5
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a8
    move-result-object v13

    #@1a9
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1ac
    move v11, v10

    #@1ad
    .line 650
    .end local v10           #processStatus:I
    .restart local v11       #processStatus:I
    goto/16 :goto_e
.end method

.method public processLmsMessage(Lcom/android/internal/telephony/gsm/SmsMessage;[[B)I
    .registers 19
    .parameter "sms"
    .parameter "pdus"

    #@0
    .prologue
    .line 2010
    const-string v0, "processLmsMessage()"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@5
    .line 2012
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getPdu()[B

    #@8
    move-result-object v6

    #@9
    .line 2013
    .local v6, pdu:[B
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserData()[B

    #@c
    move-result-object v15

    #@d
    .line 2014
    .local v15, userData:[B
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    .line 2015
    .local v2, sourceMin:Ljava/lang/String;
    const/4 v0, 0x0

    #@12
    aget-byte v3, v15, v0

    #@14
    .line 2016
    .local v3, sessionId:I
    const/4 v0, 0x1

    #@15
    aget-byte v0, v15, v0

    #@17
    and-int/lit16 v0, v0, 0xf0

    #@19
    shr-int/lit8 v4, v0, 0x4

    #@1b
    .line 2017
    .local v4, totalSegment:I
    const/4 v0, 0x1

    #@1c
    aget-byte v0, v15, v0

    #@1e
    and-int/lit8 v5, v0, 0xf

    #@20
    .line 2019
    .local v5, currentSegment:I
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, "processLmsMessage(), sourceMin = "

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@36
    .line 2020
    new-instance v0, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v1, "processLmsMessage(), sessionId = "

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@4c
    .line 2021
    new-instance v0, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v1, "processLmsMessage(), totalSegment = "

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v0

    #@5f
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@62
    .line 2022
    new-instance v0, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v1, "processLmsMessage(), currentSegment = "

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v0

    #@6d
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v0

    #@71
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v0

    #@75
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@78
    .line 2023
    new-instance v0, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v1, "processLmsMessage(), tid = "

    #@7f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v0

    #@83
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@86
    move-result-object v1

    #@87
    iget-object v1, v1, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@89
    iget v1, v1, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@8b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v0

    #@8f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v0

    #@93
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@96
    .line 2026
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserData()[B

    #@99
    move-result-object v0

    #@9a
    array-length v0, v0

    #@9b
    const/4 v1, 0x3

    #@9c
    if-lt v0, v1, :cond_a4

    #@9e
    const/4 v0, 0x3

    #@9f
    if-gt v4, v0, :cond_a4

    #@a1
    const/4 v0, 0x3

    #@a2
    if-le v5, v0, :cond_ab

    #@a4
    .line 2030
    :cond_a4
    const-string v0, "processLmsMessage(), invalid LMS"

    #@a6
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@a9
    .line 2031
    const/4 v0, 0x1

    #@aa
    .line 2041
    :goto_aa
    return v0

    #@ab
    .line 2033
    :cond_ab
    const/4 v7, 0x1

    #@ac
    move-object/from16 v0, p0

    #@ae
    move-object/from16 v1, p1

    #@b0
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->checkLmsDuplicated(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III[B)Z

    #@b3
    move-result v0

    #@b4
    if-ne v7, v0, :cond_bd

    #@b6
    .line 2034
    const-string v0, "processLmsMessage(), LMS ???"

    #@b8
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@bb
    .line 2035
    const/4 v0, 0x1

    #@bc
    goto :goto_aa

    #@bd
    :cond_bd
    move-object/from16 v7, p0

    #@bf
    move-object/from16 v8, p1

    #@c1
    move-object v9, v2

    #@c2
    move v10, v3

    #@c3
    move v11, v5

    #@c4
    move v12, v4

    #@c5
    .line 2039
    invoke-virtual/range {v7 .. v12}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->deleteAllExpiredMsg(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III)V

    #@c8
    move-object/from16 v7, p0

    #@ca
    move-object/from16 v8, p1

    #@cc
    move-object v9, v2

    #@cd
    move v10, v3

    #@ce
    move v11, v5

    #@cf
    move v12, v4

    #@d0
    move-object v13, v6

    #@d1
    move-object/from16 v14, p2

    #@d3
    .line 2041
    invoke-virtual/range {v7 .. v14}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->checkCompleteMsg(Lcom/android/internal/telephony/gsm/SmsMessage;Ljava/lang/String;III[B[[B)I

    #@d6
    move-result v0

    #@d7
    goto :goto_aa
.end method

.method protected processOperatorMessage([[BLcom/android/internal/telephony/SmsMessageBase;ZZ)I
    .registers 16
    .parameter "pdus"
    .parameter "smsb"
    .parameter "bportAddrs"
    .parameter "bConcat"

    #@0
    .prologue
    .line 689
    const/4 v8, 0x1

    #@1
    if-eq p4, v8, :cond_5

    #@3
    if-nez p2, :cond_37

    #@5
    .line 690
    :cond_5
    const/4 v8, 0x0

    #@6
    aget-object v8, p1, v8

    #@8
    invoke-static {v8}, Lcom/android/internal/telephony/gsm/SmsMessage;->createFromPdu([B)Lcom/android/internal/telephony/gsm/SmsMessage;

    #@b
    move-result-object v7

    #@c
    .line 695
    .local v7, sms:Lcom/android/internal/telephony/gsm/SmsMessage;
    :goto_c
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@e
    const-string v9, "KT_LBS"

    #@10
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@13
    move-result v8

    #@14
    const/4 v9, 0x1

    #@15
    if-ne v8, v9, :cond_3b

    #@17
    .line 696
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getProtocolIdentifier()I

    #@1a
    move-result v8

    #@1b
    const/16 v9, 0x51

    #@1d
    if-ne v8, v9, :cond_3b

    #@1f
    .line 697
    new-instance v1, Landroid/content/Intent;

    #@21
    const-string v8, "com.kt.location.action.KTLBS_DATA_SMS_RECEIVED"

    #@23
    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@26
    .line 698
    .local v1, intent:Landroid/content/Intent;
    const-string v8, "pdus"

    #@28
    invoke-virtual {v1, v8, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@2b
    .line 699
    const/16 v8, 0x20

    #@2d
    invoke-virtual {v1, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@30
    .line 700
    const-string v8, "android.permission.RECEIVE_SMS"

    #@32
    invoke-virtual {p0, v1, v8}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@35
    .line 702
    const/4 v6, 0x1

    #@36
    .line 796
    .end local v1           #intent:Landroid/content/Intent;
    :cond_36
    :goto_36
    return v6

    #@37
    .end local v7           #sms:Lcom/android/internal/telephony/gsm/SmsMessage;
    :cond_37
    move-object v7, p2

    #@38
    .line 692
    check-cast v7, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@3a
    .restart local v7       #sms:Lcom/android/internal/telephony/gsm/SmsMessage;
    goto :goto_c

    #@3b
    .line 708
    :cond_3b
    new-instance v4, Ljava/util/ArrayList;

    #@3d
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@40
    .line 709
    .local v4, operatorMessageList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/SmsOperatorBasicMessage;>;"
    const/4 v8, 0x1

    #@41
    if-ne p3, v8, :cond_149

    #@43
    .line 711
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@45
    const-string v9, "KTPortMessage"

    #@47
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@4a
    move-result v8

    #@4b
    const/4 v9, 0x1

    #@4c
    if-ne v8, v9, :cond_59

    #@4e
    .line 712
    const-string v8, "GsmSmsKTPortMessage"

    #@50
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@52
    invoke-static {v8, p0, v9, v7, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@55
    move-result-object v8

    #@56
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@59
    .line 715
    :cond_59
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@5b
    const-string v9, "SKTCommonPush"

    #@5d
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@60
    move-result v8

    #@61
    const/4 v9, 0x1

    #@62
    if-ne v8, v9, :cond_6f

    #@64
    .line 716
    const-string v8, "GsmSmsSKTPortMessage"

    #@66
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@68
    invoke-static {v8, p0, v9, v7, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@6b
    move-result-object v8

    #@6c
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6f
    .line 719
    :cond_6f
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@71
    const-string v9, "SKTUrlCallback"

    #@73
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@76
    move-result v8

    #@77
    const/4 v9, 0x1

    #@78
    if-ne v8, v9, :cond_85

    #@7a
    .line 720
    const-string v8, "GsmSmsSKTUrlCallback"

    #@7c
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@7e
    invoke-static {v8, p0, v9, v7, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@81
    move-result-object v8

    #@82
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@85
    .line 723
    :cond_85
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@87
    const-string v9, "lgu_gsm_operator_message"

    #@89
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8c
    move-result v8

    #@8d
    if-eqz v8, :cond_113

    #@8f
    .line 724
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@92
    move-result-object v8

    #@93
    if-eqz v8, :cond_113

    #@95
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@98
    move-result-object v8

    #@99
    iget-object v8, v8, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@9b
    if-eqz v8, :cond_113

    #@9d
    .line 726
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@9f
    const-string v9, "LGU_OEMMMS"

    #@a1
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a4
    move-result v8

    #@a5
    if-nez v8, :cond_c2

    #@a7
    .line 727
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@aa
    move-result-object v8

    #@ab
    iget-object v8, v8, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@ad
    iget v8, v8, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@af
    const/16 v9, 0x1004

    #@b1
    if-ne v8, v9, :cond_c2

    #@b3
    .line 728
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserData()[B

    #@b6
    move-result-object v8

    #@b7
    const/4 v9, 0x0

    #@b8
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@bb
    move-result-object v10

    #@bc
    invoke-virtual {p0, v8, v9, v10}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->processWapPdu([BILjava/lang/String;)I

    #@bf
    move-result v6

    #@c0
    goto/16 :goto_36

    #@c2
    .line 732
    :cond_c2
    const/4 v8, 0x1

    #@c3
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@c6
    move-result-object v9

    #@c7
    iget-object v9, v9, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@c9
    iget v9, v9, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@cb
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->checkValidLmsMessage(I)Z

    #@ce
    move-result v9

    #@cf
    if-ne v8, v9, :cond_108

    #@d1
    .line 733
    const/4 v8, 0x3

    #@d2
    new-array p1, v8, [[B

    #@d4
    .line 734
    const/4 v8, 0x0

    #@d5
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getPdu()[B

    #@d8
    move-result-object v9

    #@d9
    aput-object v9, p1, v8

    #@db
    .line 735
    invoke-virtual {p0, v7, p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->processLmsMessage(Lcom/android/internal/telephony/gsm/SmsMessage;[[B)I

    #@de
    move-result v6

    #@df
    .line 736
    .local v6, ret:I
    const/4 v8, -0x1

    #@e0
    if-ne v6, v8, :cond_36

    #@e2
    .line 741
    const/4 v0, 0x0

    #@e3
    .line 742
    .local v0, count:I
    const/4 v2, 0x0

    #@e4
    .line 743
    .local v2, loop:I
    const/4 v2, 0x0

    #@e5
    :goto_e5
    const/4 v8, 0x3

    #@e6
    if-ge v2, v8, :cond_ec

    #@e8
    .line 744
    aget-object v8, p1, v2

    #@ea
    if-nez v8, :cond_f9

    #@ec
    .line 748
    :cond_ec
    move v0, v2

    #@ed
    .line 749
    new-array v3, v0, [[B

    #@ef
    .line 750
    .local v3, newPuds:[[B
    const/4 v2, 0x0

    #@f0
    :goto_f0
    if-ge v2, v0, :cond_fc

    #@f2
    .line 751
    aget-object v8, p1, v2

    #@f4
    aput-object v8, v3, v2

    #@f6
    .line 750
    add-int/lit8 v2, v2, 0x1

    #@f8
    goto :goto_f0

    #@f9
    .line 743
    .end local v3           #newPuds:[[B
    :cond_f9
    add-int/lit8 v2, v2, 0x1

    #@fb
    goto :goto_e5

    #@fc
    .line 753
    .restart local v3       #newPuds:[[B
    :cond_fc
    new-array p1, v0, [[B

    #@fe
    .line 754
    const/4 v2, 0x0

    #@ff
    :goto_ff
    if-ge v2, v0, :cond_108

    #@101
    .line 755
    aget-object v8, v3, v2

    #@103
    aput-object v8, p1, v2

    #@105
    .line 754
    add-int/lit8 v2, v2, 0x1

    #@107
    goto :goto_ff

    #@108
    .line 760
    .end local v0           #count:I
    .end local v2           #loop:I
    .end local v3           #newPuds:[[B
    .end local v6           #ret:I
    :cond_108
    const-string v8, "GsmSmsLgtMessage"

    #@10a
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@10c
    invoke-static {v8, p0, v9, v7, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@10f
    move-result-object v8

    #@110
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@113
    .line 765
    :cond_113
    if-nez p4, :cond_141

    #@115
    .line 767
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@117
    const-string v9, "specialMessage"

    #@119
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@11c
    move-result v8

    #@11d
    const/4 v9, 0x1

    #@11e
    if-ne v8, v9, :cond_12b

    #@120
    .line 768
    const-string v8, "GsmSmsKRSpecialMessage"

    #@122
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@124
    invoke-static {v8, p0, v9, v7, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@127
    move-result-object v8

    #@128
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@12b
    .line 771
    :cond_12b
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@12d
    const-string v9, "KTFotaMessage"

    #@12f
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@132
    move-result v8

    #@133
    const/4 v9, 0x1

    #@134
    if-ne v8, v9, :cond_141

    #@136
    .line 772
    const-string v8, "GsmSmsKTFotaMessage"

    #@138
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@13a
    invoke-static {v8, p0, v9, v7, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@13d
    move-result-object v8

    #@13e
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@141
    .line 794
    :cond_141
    :goto_141
    const/4 v5, 0x5

    #@142
    .line 795
    .local v5, result:I
    invoke-virtual {p0, v7, v4}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->dispatchOperatorMessage(Lcom/android/internal/telephony/SmsMessageBase;Ljava/util/ArrayList;)I

    #@145
    move-result v5

    #@146
    move v6, v5

    #@147
    .line 796
    goto/16 :goto_36

    #@149
    .line 777
    .end local v5           #result:I
    :cond_149
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@14b
    const-string v9, "KTUrlCallback"

    #@14d
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@150
    move-result v8

    #@151
    const/4 v9, 0x1

    #@152
    if-ne v8, v9, :cond_15f

    #@154
    .line 778
    const-string v8, "GsmSmsKTUrlCallback"

    #@156
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@158
    invoke-static {v8, p0, v9, v7, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@15b
    move-result-object v8

    #@15c
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@15f
    .line 781
    :cond_15f
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@161
    const-string v9, "spam"

    #@163
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@166
    move-result v8

    #@167
    const/4 v9, 0x1

    #@168
    if-ne v8, v9, :cond_173

    #@16a
    .line 782
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@16c
    invoke-static {p0, v8, v7, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorSpamMessage(Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@16f
    move-result-object v8

    #@170
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@173
    .line 784
    :cond_173
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@175
    const-string v9, "LGUspam"

    #@177
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@17a
    move-result v8

    #@17b
    const/4 v9, 0x1

    #@17c
    if-ne v8, v9, :cond_189

    #@17e
    .line 785
    const-string v8, "GsmSmsKTSpamMessage"

    #@180
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@182
    invoke-static {v8, p0, v9, v7, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@185
    move-result-object v8

    #@186
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@189
    .line 787
    :cond_189
    if-nez p4, :cond_141

    #@18b
    .line 789
    iget-object v8, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@18d
    const-string v9, "specialMessage"

    #@18f
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@192
    move-result v8

    #@193
    const/4 v9, 0x1

    #@194
    if-ne v8, v9, :cond_141

    #@196
    .line 790
    const-string v8, "GsmSmsKRSpecialMessage"

    #@198
    iget-object v9, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@19a
    invoke-static {v8, p0, v9, v7, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@19d
    move-result-object v8

    #@19e
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a1
    goto :goto_141
.end method

.method protected processWapPdu([BILjava/lang/String;)I
    .registers 33
    .parameter "pdu"
    .parameter "referenceNumber"
    .parameter "address"

    #@0
    .prologue
    .line 2388
    const/16 v17, 0x0

    #@2
    .line 2391
    .local v17, index:I
    const/16 v25, 0x0

    #@4
    .line 2392
    .local v25, sourcePort:I
    const/4 v13, 0x0

    #@5
    .line 2394
    .local v13, destinationPort:I
    add-int/lit8 v18, v17, 0x1

    #@7
    .end local v17           #index:I
    .local v18, index:I
    aget-byte v19, p1, v17

    #@9
    .line 2395
    .local v19, msgType:I
    if-eqz v19, :cond_14

    #@b
    .line 2396
    const-string v3, "processWapPdu(), Received a WAP SMS which is not WDP. Discard."

    #@d
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@10
    .line 2397
    const/4 v3, 0x1

    #@11
    move/from16 v17, v18

    #@13
    .line 2503
    .end local v18           #index:I
    .restart local v17       #index:I
    :cond_13
    :goto_13
    return v3

    #@14
    .line 2399
    .end local v17           #index:I
    .restart local v18       #index:I
    :cond_14
    add-int/lit8 v17, v18, 0x1

    #@16
    .end local v18           #index:I
    .restart local v17       #index:I
    aget-byte v26, p1, v18

    #@18
    .line 2400
    .local v26, totalSegments:I
    add-int/lit8 v18, v17, 0x1

    #@1a
    .end local v17           #index:I
    .restart local v18       #index:I
    aget-byte v23, p1, v17

    #@1c
    .line 2403
    .local v23, segment:I
    if-nez v23, :cond_22e

    #@1e
    .line 2405
    add-int/lit8 v17, v18, 0x1

    #@20
    .end local v18           #index:I
    .restart local v17       #index:I
    aget-byte v3, p1, v18

    #@22
    and-int/lit16 v3, v3, 0xff

    #@24
    shl-int/lit8 v25, v3, 0x8

    #@26
    .line 2406
    add-int/lit8 v18, v17, 0x1

    #@28
    .end local v17           #index:I
    .restart local v18       #index:I
    aget-byte v3, p1, v17

    #@2a
    and-int/lit16 v3, v3, 0xff

    #@2c
    or-int v25, v25, v3

    #@2e
    .line 2407
    add-int/lit8 v17, v18, 0x1

    #@30
    .end local v18           #index:I
    .restart local v17       #index:I
    aget-byte v3, p1, v18

    #@32
    and-int/lit16 v3, v3, 0xff

    #@34
    shl-int/lit8 v13, v3, 0x8

    #@36
    .line 2408
    add-int/lit8 v18, v17, 0x1

    #@38
    .end local v17           #index:I
    .restart local v18       #index:I
    aget-byte v3, p1, v17

    #@3a
    and-int/lit16 v3, v3, 0xff

    #@3c
    or-int/2addr v13, v3

    #@3d
    move/from16 v17, v18

    #@3f
    .line 2412
    .end local v18           #index:I
    .restart local v17       #index:I
    :goto_3f
    new-instance v28, Ljava/lang/StringBuilder;

    #@41
    const-string v3, "reference_number ="

    #@43
    move-object/from16 v0, v28

    #@45
    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@48
    .line 2413
    .local v28, where:Ljava/lang/StringBuilder;
    move-object/from16 v0, v28

    #@4a
    move/from16 v1, p2

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    .line 2414
    const-string v3, " AND address = ?"

    #@51
    move-object/from16 v0, v28

    #@53
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    .line 2415
    const/4 v3, 0x1

    #@57
    new-array v7, v3, [Ljava/lang/String;

    #@59
    const/4 v3, 0x0

    #@5a
    aput-object p3, v7, v3

    #@5c
    .line 2417
    .local v7, whereArgs:[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v4, "processWapPdu(), Received WAP PDU. Type = "

    #@63
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v3

    #@67
    move/from16 v0, v19

    #@69
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    const-string v4, ", originator = "

    #@6f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v3

    #@73
    move-object/from16 v0, p3

    #@75
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v3

    #@79
    const-string v4, ", src-port = "

    #@7b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v3

    #@7f
    move/from16 v0, v25

    #@81
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@84
    move-result-object v3

    #@85
    const-string v4, ", dst-port = "

    #@87
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v3

    #@8b
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v3

    #@8f
    const-string v4, ", ID = "

    #@91
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v3

    #@95
    move/from16 v0, p2

    #@97
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v3

    #@9b
    const-string v4, ", segment# = "

    #@9d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v3

    #@a1
    move/from16 v0, v23

    #@a3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v3

    #@a7
    const-string v4, "/"

    #@a9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v3

    #@ad
    move/from16 v0, v26

    #@af
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v3

    #@b3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v3

    #@b7
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@ba
    .line 2421
    const/16 v22, 0x0

    #@bc
    check-cast v22, [[B

    #@be
    .line 2422
    .local v22, pdus:[[B
    const/4 v9, 0x0

    #@bf
    .line 2424
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_bf
    move-object/from16 v0, p0

    #@c1
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@c3
    sget-object v4, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@c5
    const/4 v5, 0x3

    #@c6
    new-array v5, v5, [Ljava/lang/String;

    #@c8
    const/4 v6, 0x0

    #@c9
    const-string v8, "pdu"

    #@cb
    aput-object v8, v5, v6

    #@cd
    const/4 v6, 0x1

    #@ce
    const-string v8, "sequence"

    #@d0
    aput-object v8, v5, v6

    #@d2
    const/4 v6, 0x2

    #@d3
    const-string v8, "destination_port"

    #@d5
    aput-object v8, v5, v6

    #@d7
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v6

    #@db
    const/4 v8, 0x0

    #@dc
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@df
    move-result-object v9

    #@e0
    .line 2427
    if-eqz v9, :cond_1cd

    #@e2
    .line 2429
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    #@e5
    move-result v10

    #@e6
    .line 2430
    .local v10, cursorCount:I
    add-int/lit8 v3, v26, -0x1

    #@e8
    if-eq v10, v3, :cond_159

    #@ea
    .line 2432
    new-instance v27, Landroid/content/ContentValues;

    #@ec
    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    #@ef
    .line 2433
    .local v27, values:Landroid/content/ContentValues;
    const-string v3, "date"

    #@f1
    new-instance v4, Ljava/lang/Long;

    #@f3
    const-wide/16 v5, 0x0

    #@f5
    invoke-direct {v4, v5, v6}, Ljava/lang/Long;-><init>(J)V

    #@f8
    move-object/from16 v0, v27

    #@fa
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@fd
    .line 2434
    const-string v3, "pdu"

    #@ff
    move-object/from16 v0, p1

    #@101
    array-length v4, v0

    #@102
    sub-int v4, v4, v17

    #@104
    move-object/from16 v0, p1

    #@106
    move/from16 v1, v17

    #@108
    invoke-static {v0, v1, v4}, Lcom/android/internal/util/HexDump;->toHexString([BII)Ljava/lang/String;

    #@10b
    move-result-object v4

    #@10c
    move-object/from16 v0, v27

    #@10e
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@111
    .line 2435
    const-string v3, "address"

    #@113
    move-object/from16 v0, v27

    #@115
    move-object/from16 v1, p3

    #@117
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@11a
    .line 2436
    const-string v3, "reference_number"

    #@11c
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11f
    move-result-object v4

    #@120
    move-object/from16 v0, v27

    #@122
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@125
    .line 2437
    const-string v3, "count"

    #@127
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12a
    move-result-object v4

    #@12b
    move-object/from16 v0, v27

    #@12d
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@130
    .line 2438
    const-string v3, "sequence"

    #@132
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@135
    move-result-object v4

    #@136
    move-object/from16 v0, v27

    #@138
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@13b
    .line 2439
    const-string v3, "destination_port"

    #@13d
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@140
    move-result-object v4

    #@141
    move-object/from16 v0, v27

    #@143
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@146
    .line 2441
    move-object/from16 v0, p0

    #@148
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@14a
    sget-object v4, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@14c
    move-object/from16 v0, v27

    #@14e
    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_151
    .catchall {:try_start_bf .. :try_end_151} :catchall_1e2
    .catch Landroid/database/SQLException; {:try_start_bf .. :try_end_151} :catch_1d7

    #@151
    .line 2442
    const/4 v3, 0x1

    #@152
    .line 2474
    if-eqz v9, :cond_13

    #@154
    .end local v10           #cursorCount:I
    .end local v27           #values:Landroid/content/ContentValues;
    :goto_154
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@157
    goto/16 :goto_13

    #@159
    .line 2446
    .restart local v10       #cursorCount:I
    :cond_159
    :try_start_159
    const-string v3, "pdu"

    #@15b
    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@15e
    move-result v21

    #@15f
    .line 2447
    .local v21, pduColumn:I
    const-string v3, "sequence"

    #@161
    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@164
    move-result v24

    #@165
    .line 2449
    .local v24, sequenceColumn:I
    move/from16 v0, v26

    #@167
    new-array v0, v0, [[B

    #@169
    move-object/from16 v22, v0

    #@16b
    .line 2450
    const/16 v16, 0x0

    #@16d
    .local v16, i:I
    :goto_16d
    move/from16 v0, v16

    #@16f
    if-ge v0, v10, :cond_197

    #@171
    .line 2451
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@174
    .line 2452
    move/from16 v0, v24

    #@176
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    #@179
    move-result-wide v3

    #@17a
    long-to-int v11, v3

    #@17b
    .line 2454
    .local v11, cursorSequence:I
    if-nez v11, :cond_188

    #@17d
    .line 2455
    const-string v3, "destination_port"

    #@17f
    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@182
    move-result v14

    #@183
    .line 2456
    .local v14, destinationPortColumn:I
    invoke-interface {v9, v14}, Landroid/database/Cursor;->getLong(I)J

    #@186
    move-result-wide v3

    #@187
    long-to-int v13, v3

    #@188
    .line 2458
    .end local v14           #destinationPortColumn:I
    :cond_188
    move/from16 v0, v21

    #@18a
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@18d
    move-result-object v3

    #@18e
    invoke-static {v3}, Lcom/android/internal/util/HexDump;->hexStringToByteArray(Ljava/lang/String;)[B

    #@191
    move-result-object v3

    #@192
    aput-object v3, v22, v11

    #@194
    .line 2450
    add-int/lit8 v16, v16, 0x1

    #@196
    goto :goto_16d

    #@197
    .line 2464
    .end local v11           #cursorSequence:I
    :cond_197
    move-object/from16 v0, p0

    #@199
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mResolver:Landroid/content/ContentResolver;

    #@19b
    sget-object v4, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mRawUri:Landroid/net/Uri;

    #@19d
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a0
    move-result-object v5

    #@1a1
    invoke-virtual {v3, v4, v5, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1a4
    .catchall {:try_start_159 .. :try_end_1a4} :catchall_1e2
    .catch Landroid/database/SQLException; {:try_start_159 .. :try_end_1a4} :catch_1d7

    #@1a4
    .line 2474
    if-eqz v9, :cond_1a9

    #@1a6
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@1a9
    .line 2478
    :cond_1a9
    new-instance v20, Ljava/io/ByteArrayOutputStream;

    #@1ab
    invoke-direct/range {v20 .. v20}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@1ae
    .line 2479
    .local v20, output:Ljava/io/ByteArrayOutputStream;
    const/16 v16, 0x0

    #@1b0
    :goto_1b0
    move/from16 v0, v16

    #@1b2
    move/from16 v1, v26

    #@1b4
    if-ge v0, v1, :cond_1f5

    #@1b6
    .line 2481
    move/from16 v0, v16

    #@1b8
    move/from16 v1, v23

    #@1ba
    if-ne v0, v1, :cond_1e9

    #@1bc
    .line 2483
    move-object/from16 v0, p1

    #@1be
    array-length v3, v0

    #@1bf
    sub-int v3, v3, v17

    #@1c1
    move-object/from16 v0, v20

    #@1c3
    move-object/from16 v1, p1

    #@1c5
    move/from16 v2, v17

    #@1c7
    invoke-virtual {v0, v1, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@1ca
    .line 2479
    :goto_1ca
    add-int/lit8 v16, v16, 0x1

    #@1cc
    goto :goto_1b0

    #@1cd
    .line 2467
    .end local v10           #cursorCount:I
    .end local v16           #i:I
    .end local v20           #output:Ljava/io/ByteArrayOutputStream;
    .end local v21           #pduColumn:I
    .end local v24           #sequenceColumn:I
    :cond_1cd
    :try_start_1cd
    const-string v3, "processWapPdu(), mResolver.query() returned null"

    #@1cf
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_1d2
    .catchall {:try_start_1cd .. :try_end_1d2} :catchall_1e2
    .catch Landroid/database/SQLException; {:try_start_1cd .. :try_end_1d2} :catch_1d7

    #@1d2
    .line 2468
    const/4 v3, -0x1

    #@1d3
    .line 2474
    if-eqz v9, :cond_13

    #@1d5
    goto/16 :goto_154

    #@1d7
    .line 2470
    :catch_1d7
    move-exception v15

    #@1d8
    .line 2471
    .local v15, e:Landroid/database/SQLException;
    :try_start_1d8
    const-string v3, "processWapPdu(), Can\'t access multipart SMS database"

    #@1da
    invoke-static {v3, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1dd
    .catchall {:try_start_1d8 .. :try_end_1dd} :catchall_1e2

    #@1dd
    .line 2472
    const/4 v3, 0x2

    #@1de
    .line 2474
    if-eqz v9, :cond_13

    #@1e0
    goto/16 :goto_154

    #@1e2
    .end local v15           #e:Landroid/database/SQLException;
    :catchall_1e2
    move-exception v3

    #@1e3
    if-eqz v9, :cond_1e8

    #@1e5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@1e8
    :cond_1e8
    throw v3

    #@1e9
    .line 2485
    .restart local v10       #cursorCount:I
    .restart local v16       #i:I
    .restart local v20       #output:Ljava/io/ByteArrayOutputStream;
    .restart local v21       #pduColumn:I
    .restart local v24       #sequenceColumn:I
    :cond_1e9
    aget-object v3, v22, v16

    #@1eb
    const/4 v4, 0x0

    #@1ec
    aget-object v5, v22, v16

    #@1ee
    array-length v5, v5

    #@1ef
    move-object/from16 v0, v20

    #@1f1
    invoke-virtual {v0, v3, v4, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@1f4
    goto :goto_1ca

    #@1f5
    .line 2489
    :cond_1f5
    invoke-virtual/range {v20 .. v20}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@1f8
    move-result-object v12

    #@1f9
    .line 2492
    .local v12, datagram:[B
    new-instance v3, Ljava/lang/StringBuilder;

    #@1fb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1fe
    const-string v4, "processWapPdu(), destinationPort : "

    #@200
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@203
    move-result-object v3

    #@204
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@207
    move-result-object v3

    #@208
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20b
    move-result-object v3

    #@20c
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@20f
    .line 2493
    packed-switch v13, :pswitch_data_232

    #@212
    .line 2499
    const/4 v3, 0x1

    #@213
    new-array v0, v3, [[B

    #@215
    move-object/from16 v22, v0

    #@217
    .line 2500
    const/4 v3, 0x0

    #@218
    aput-object v12, v22, v3

    #@21a
    .line 2502
    move-object/from16 v0, p0

    #@21c
    move-object/from16 v1, v22

    #@21e
    invoke-virtual {v0, v1, v13}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->dispatchPortAddressedPdus([[BI)V

    #@221
    .line 2503
    const/4 v3, -0x1

    #@222
    goto/16 :goto_13

    #@224
    .line 2496
    :pswitch_224
    move-object/from16 v0, p0

    #@226
    iget-object v3, v0, Lcom/android/internal/telephony/SMSDispatcher;->mWapPush:Lcom/android/internal/telephony/WapPushOverSms;

    #@228
    invoke-virtual {v3, v12}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([B)I

    #@22b
    move-result v3

    #@22c
    goto/16 :goto_13

    #@22e
    .end local v7           #whereArgs:[Ljava/lang/String;
    .end local v9           #cursor:Landroid/database/Cursor;
    .end local v10           #cursorCount:I
    .end local v12           #datagram:[B
    .end local v16           #i:I
    .end local v17           #index:I
    .end local v20           #output:Ljava/io/ByteArrayOutputStream;
    .end local v21           #pduColumn:I
    .end local v22           #pdus:[[B
    .end local v24           #sequenceColumn:I
    .end local v28           #where:Ljava/lang/StringBuilder;
    .restart local v18       #index:I
    :cond_22e
    move/from16 v17, v18

    #@230
    .end local v18           #index:I
    .restart local v17       #index:I
    goto/16 :goto_3f

    #@232
    .line 2493
    :pswitch_data_232
    .packed-switch 0xb84
        :pswitch_224
    .end packed-switch
.end method

.method public readSmsPrefixWzone(Ljava/lang/String;)Ljava/lang/String;
    .registers 18
    .parameter "destinationAddress"

    #@0
    .prologue
    .line 1799
    const/4 v10, 0x0

    #@1
    .line 1800
    .local v10, smsPreFix:Ljava/lang/String;
    const/4 v11, 0x0

    #@2
    .line 1801
    .local v11, smsPreFixLine:Ljava/lang/String;
    const/4 v8, 0x0

    #@3
    .line 1802
    .local v8, low_smsPreFixLine:Ljava/lang/String;
    const/4 v1, 0x0

    #@4
    .line 1803
    .local v1, bisWzone:Z
    const/4 v6, 0x0

    #@5
    .line 1804
    .local v6, isWzoneLine:Ljava/lang/String;
    const/4 v7, 0x0

    #@6
    .line 1805
    .local v7, low_isWzoneLine:Ljava/lang/String;
    const/4 v4, 0x0

    #@7
    .line 1807
    .local v4, in:Ljava/io/BufferedReader;
    const-string v12, "readSmsPrefixWzone()"

    #@9
    invoke-static {v12}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@c
    .line 1810
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@f
    move-result-object v12

    #@10
    const-string v13, "mounted"

    #@12
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v12

    #@16
    if-eqz v12, :cond_9d

    #@18
    .line 1814
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@1b
    move-result-object v12

    #@1c
    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@1f
    move-result-object v9

    #@20
    .line 1818
    .local v9, mSdPath:Ljava/lang/String;
    :try_start_20
    new-instance v5, Ljava/io/BufferedReader;

    #@22
    new-instance v12, Ljava/io/InputStreamReader;

    #@24
    new-instance v13, Ljava/io/FileInputStream;

    #@26
    new-instance v14, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v14

    #@2f
    const-string v15, "/sktelecom/w-zone/smsenv.env"

    #@31
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v14

    #@35
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v14

    #@39
    invoke-direct {v13, v14}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    #@3c
    invoke-direct {v12, v13}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@3f
    invoke-direct {v5, v12}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_42
    .catchall {:try_start_20 .. :try_end_42} :catchall_f3
    .catch Ljava/io/FileNotFoundException; {:try_start_20 .. :try_end_42} :catch_109
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_42} :catch_107

    #@42
    .line 1820
    .end local v4           #in:Ljava/io/BufferedReader;
    .local v5, in:Ljava/io/BufferedReader;
    if-eqz v5, :cond_97

    #@44
    .line 1821
    :try_start_44
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@47
    move-result-object v6

    #@48
    if-eqz v6, :cond_94

    #@4a
    .line 1822
    const-string v12, " "

    #@4c
    const-string v13, ""

    #@4e
    invoke-virtual {v6, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@51
    move-result-object v12

    #@52
    invoke-virtual {v12}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@55
    move-result-object v7

    #@56
    .line 1823
    const-string v12, "iswzone:true"

    #@58
    invoke-virtual {v7, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@5b
    move-result v12

    #@5c
    const/4 v13, 0x1

    #@5d
    if-ne v12, v13, :cond_94

    #@5f
    .line 1824
    const/4 v1, 0x1

    #@60
    .line 1825
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@63
    move-result-object v11

    #@64
    if-eqz v11, :cond_dc

    #@66
    .line 1826
    const-string v12, " "

    #@68
    const-string v13, ""

    #@6a
    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@6d
    move-result-object v12

    #@6e
    invoke-virtual {v12}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@71
    move-result-object v8

    #@72
    .line 1827
    const-string v12, "smsprefix:"

    #@74
    invoke-virtual {v8, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@77
    move-result v12

    #@78
    const/4 v13, 0x1

    #@79
    if-ne v12, v13, :cond_c2

    #@7b
    .line 1828
    const-string v12, "smsprefix:"

    #@7d
    const-string v13, ""

    #@7f
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@82
    move-result-object v10

    #@83
    .line 1829
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@86
    move-result v12

    #@87
    if-lez v12, :cond_94

    #@89
    .line 1830
    move-object/from16 v0, p1

    #@8b
    invoke-virtual {v0, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@8e
    move-result v12

    #@8f
    const/4 v13, 0x1

    #@90
    if-ne v12, v13, :cond_94

    #@92
    .line 1831
    const-string v10, ""

    #@94
    .line 1843
    :cond_94
    :goto_94
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_97
    .catchall {:try_start_44 .. :try_end_97} :catchall_104
    .catch Ljava/io/FileNotFoundException; {:try_start_44 .. :try_end_97} :catch_c9
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_97} :catch_e3

    #@97
    .line 1852
    :cond_97
    if-eqz v5, :cond_9c

    #@99
    .line 1854
    :try_start_99
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_9c
    .catch Ljava/io/IOException; {:try_start_99 .. :try_end_9c} :catch_ff

    #@9c
    :cond_9c
    :goto_9c
    move-object v4, v5

    #@9d
    .line 1862
    .end local v5           #in:Ljava/io/BufferedReader;
    .end local v9           #mSdPath:Ljava/lang/String;
    .restart local v4       #in:Ljava/io/BufferedReader;
    :cond_9d
    :goto_9d
    if-nez v10, :cond_a1

    #@9f
    .line 1863
    const-string v10, ""

    #@a1
    .line 1864
    :cond_a1
    new-instance v12, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v13, "readSmsPrefixWzone(), END>>>bisWzone:"

    #@a8
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v12

    #@ac
    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@af
    move-result-object v12

    #@b0
    const-string v13, " smsPreFix:"

    #@b2
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v12

    #@b6
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v12

    #@ba
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v12

    #@be
    invoke-static {v12}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@c1
    .line 1865
    return-object v10

    #@c2
    .line 1834
    .end local v4           #in:Ljava/io/BufferedReader;
    .restart local v5       #in:Ljava/io/BufferedReader;
    .restart local v9       #mSdPath:Ljava/lang/String;
    :cond_c2
    const/4 v1, 0x0

    #@c3
    .line 1835
    :try_start_c3
    const-string v12, "readSmsPrefixWzone(), smspreFix::not contains"

    #@c5
    invoke-static {v12}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_c8
    .catchall {:try_start_c3 .. :try_end_c8} :catchall_104
    .catch Ljava/io/FileNotFoundException; {:try_start_c3 .. :try_end_c8} :catch_c9
    .catch Ljava/lang/Exception; {:try_start_c3 .. :try_end_c8} :catch_e3

    #@c8
    goto :goto_94

    #@c9
    .line 1845
    :catch_c9
    move-exception v2

    #@ca
    move-object v4, v5

    #@cb
    .line 1846
    .end local v5           #in:Ljava/io/BufferedReader;
    .local v2, e:Ljava/io/FileNotFoundException;
    .restart local v4       #in:Ljava/io/BufferedReader;
    :goto_cb
    const/4 v1, 0x0

    #@cc
    .line 1847
    :try_start_cc
    const-string v12, "readSmsPrefixWzone(), smsenv.env::not exist"

    #@ce
    invoke-static {v12}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_d1
    .catchall {:try_start_cc .. :try_end_d1} :catchall_f3

    #@d1
    .line 1852
    if-eqz v4, :cond_9d

    #@d3
    .line 1854
    :try_start_d3
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_d6
    .catch Ljava/io/IOException; {:try_start_d3 .. :try_end_d6} :catch_d7

    #@d6
    goto :goto_9d

    #@d7
    .line 1855
    :catch_d7
    move-exception v3

    #@d8
    .line 1857
    .end local v2           #e:Ljava/io/FileNotFoundException;
    .local v3, e1:Ljava/io/IOException;
    :goto_d8
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    #@db
    goto :goto_9d

    #@dc
    .line 1838
    .end local v3           #e1:Ljava/io/IOException;
    .end local v4           #in:Ljava/io/BufferedReader;
    .restart local v5       #in:Ljava/io/BufferedReader;
    :cond_dc
    const/4 v1, 0x0

    #@dd
    .line 1839
    :try_start_dd
    const-string v12, "readSmsPrefixWzone(), smspreFix::not contains"

    #@df
    invoke-static {v12}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_e2
    .catchall {:try_start_dd .. :try_end_e2} :catchall_104
    .catch Ljava/io/FileNotFoundException; {:try_start_dd .. :try_end_e2} :catch_c9
    .catch Ljava/lang/Exception; {:try_start_dd .. :try_end_e2} :catch_e3

    #@e2
    goto :goto_94

    #@e3
    .line 1848
    :catch_e3
    move-exception v2

    #@e4
    move-object v4, v5

    #@e5
    .line 1849
    .end local v5           #in:Ljava/io/BufferedReader;
    .local v2, e:Ljava/lang/Exception;
    .restart local v4       #in:Ljava/io/BufferedReader;
    :goto_e5
    const/4 v1, 0x0

    #@e6
    .line 1850
    :try_start_e6
    const-string v12, "readSmsPrefixWzone(), smsenv.env::Exception"

    #@e8
    invoke-static {v12}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_eb
    .catchall {:try_start_e6 .. :try_end_eb} :catchall_f3

    #@eb
    .line 1852
    if-eqz v4, :cond_9d

    #@ed
    .line 1854
    :try_start_ed
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_f0
    .catch Ljava/io/IOException; {:try_start_ed .. :try_end_f0} :catch_f1

    #@f0
    goto :goto_9d

    #@f1
    .line 1855
    :catch_f1
    move-exception v3

    #@f2
    goto :goto_d8

    #@f3
    .line 1852
    .end local v2           #e:Ljava/lang/Exception;
    :catchall_f3
    move-exception v12

    #@f4
    :goto_f4
    if-eqz v4, :cond_f9

    #@f6
    .line 1854
    :try_start_f6
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_f9
    .catch Ljava/io/IOException; {:try_start_f6 .. :try_end_f9} :catch_fa

    #@f9
    .line 1852
    :cond_f9
    :goto_f9
    throw v12

    #@fa
    .line 1855
    :catch_fa
    move-exception v3

    #@fb
    .line 1857
    .restart local v3       #e1:Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    #@fe
    goto :goto_f9

    #@ff
    .line 1855
    .end local v3           #e1:Ljava/io/IOException;
    .end local v4           #in:Ljava/io/BufferedReader;
    .restart local v5       #in:Ljava/io/BufferedReader;
    :catch_ff
    move-exception v3

    #@100
    .line 1857
    .restart local v3       #e1:Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    #@103
    goto :goto_9c

    #@104
    .line 1852
    .end local v3           #e1:Ljava/io/IOException;
    :catchall_104
    move-exception v12

    #@105
    move-object v4, v5

    #@106
    .end local v5           #in:Ljava/io/BufferedReader;
    .restart local v4       #in:Ljava/io/BufferedReader;
    goto :goto_f4

    #@107
    .line 1848
    :catch_107
    move-exception v2

    #@108
    goto :goto_e5

    #@109
    .line 1845
    :catch_109
    move-exception v2

    #@10a
    goto :goto_cb
.end method

.method protected sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 15
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "destPort"
    .parameter "data"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 811
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "SKTWzone"

    #@5
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8
    move-result v1

    #@9
    if-ne v1, v0, :cond_f

    #@b
    .line 812
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->wZoneDestnationNumber(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object p1

    #@f
    .line 815
    :cond_f
    if-eqz p6, :cond_2c

    #@11
    :goto_11
    invoke-static {p2, p1, p3, p4, v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;I[BZ)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@14
    move-result-object v5

    #@15
    .line 817
    .local v5, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz v5, :cond_2e

    #@17
    move-object v0, p0

    #@18
    move-object v1, p1

    #@19
    move-object v2, p2

    #@1a
    move v3, p3

    #@1b
    move-object v4, p4

    #@1c
    .line 818
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;I[BLcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@1f
    move-result-object v6

    #@20
    .line 819
    .local v6, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {p0, v6, p5, p6, v0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@27
    move-result-object v7

    #@28
    .line 821
    .local v7, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@2b
    .line 825
    .end local v6           #map:Ljava/util/HashMap;
    .end local v7           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_2b
    return-void

    #@2c
    .line 815
    .end local v5           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_11

    #@2e
    .line 823
    .restart local v5       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_2e
    const-string v0, "GSM"

    #@30
    const-string v1, "GsmSMSDispatcher.sendData(): getSubmitPdu() returned null"

    #@32
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_2b
.end method

.method protected sendEmailoverText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 15
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 950
    const-string v0, "sendEmailoverText()"

    #@3
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@6
    .line 951
    if-eqz p5, :cond_23

    #@8
    const/4 v3, 0x1

    #@9
    :goto_9
    const/4 v4, 0x0

    #@a
    move-object v0, p2

    #@b
    move-object v1, p1

    #@c
    move-object v2, p3

    #@d
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPduforEmailoverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BI)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@10
    move-result-object v7

    #@11
    .line 955
    .local v7, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz v7, :cond_25

    #@13
    .line 956
    invoke-virtual {p0, p1, p2, p3, v7}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@16
    move-result-object v6

    #@17
    .line 957
    .local v6, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {p0, v6, p4, p5, v0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@1e
    move-result-object v8

    #@1f
    .line 958
    .local v8, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@22
    .line 962
    .end local v6           #map:Ljava/util/HashMap;
    .end local v8           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_22
    return-void

    #@23
    .end local v7           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_23
    move v3, v5

    #@24
    .line 951
    goto :goto_9

    #@25
    .line 960
    .restart local v7       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_25
    const-string v0, "sendEmailoverText(), returned null"

    #@27
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@2a
    goto :goto_22
.end method

.method protected sendMultipartTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V
    .registers 36
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1012
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const-string v4, "sendMultipartTextLge()"

    #@2
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@5
    .line 1013
    invoke-static {}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getNextConcatenatedRef()I

    #@8
    move-result v4

    #@9
    and-int/lit16 v0, v4, 0xff

    #@b
    move/from16 v20, v0

    #@d
    .line 1014
    .local v20, refNumber:I
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v18

    #@11
    .line 1015
    .local v18, msgCount:I
    const/4 v9, 0x0

    #@12
    .line 1017
    .local v9, encoding:I
    move-object/from16 v0, p0

    #@14
    iget-object v4, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@16
    const-string v5, "SKTWzone"

    #@18
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1b
    move-result v4

    #@1c
    const/4 v5, 0x1

    #@1d
    if-ne v4, v5, :cond_23

    #@1f
    .line 1018
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->wZoneDestnationNumber(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object p1

    #@23
    .line 1022
    :cond_23
    move/from16 v0, v18

    #@25
    move-object/from16 v1, p0

    #@27
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@29
    .line 1023
    const/16 v16, 0x0

    #@2b
    .local v16, i:I
    :goto_2b
    move/from16 v0, v16

    #@2d
    move/from16 v1, v18

    #@2f
    if-ge v0, v1, :cond_4f

    #@31
    .line 1024
    move-object/from16 v0, p3

    #@33
    move/from16 v1, v16

    #@35
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@38
    move-result-object v4

    #@39
    check-cast v4, Ljava/lang/CharSequence;

    #@3b
    const/4 v5, 0x0

    #@3c
    const/4 v6, 0x0

    #@3d
    invoke-static {v4, v5, v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;ZZ)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@40
    move-result-object v15

    #@41
    .line 1025
    .local v15, details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    iget v4, v15, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@43
    if-eq v9, v4, :cond_4c

    #@45
    if-eqz v9, :cond_4a

    #@47
    const/4 v4, 0x1

    #@48
    if-ne v9, v4, :cond_4c

    #@4a
    .line 1028
    :cond_4a
    iget v9, v15, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@4c
    .line 1023
    :cond_4c
    add-int/lit8 v16, v16, 0x1

    #@4e
    goto :goto_2b

    #@4f
    .line 1032
    .end local v15           #details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_4f
    const/16 v16, 0x0

    #@51
    :goto_51
    move/from16 v0, v16

    #@53
    move/from16 v1, v18

    #@55
    if-ge v0, v1, :cond_150

    #@57
    .line 1033
    new-instance v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@59
    invoke-direct {v12}, Lcom/android/internal/telephony/SmsHeader$ConcatRef;-><init>()V

    #@5c
    .line 1034
    .local v12, concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    move/from16 v0, v20

    #@5e
    iput v0, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@60
    .line 1035
    add-int/lit8 v4, v16, 0x1

    #@62
    iput v4, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@64
    .line 1036
    move/from16 v0, v18

    #@66
    iput v0, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@68
    .line 1043
    const/4 v4, 0x1

    #@69
    iput-boolean v4, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->isEightBits:Z

    #@6b
    .line 1044
    new-instance v24, Lcom/android/internal/telephony/SmsHeader;

    #@6d
    invoke-direct/range {v24 .. v24}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@70
    .line 1045
    .local v24, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    move-object/from16 v0, v24

    #@72
    iput-object v12, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@74
    .line 1047
    if-nez v16, :cond_ef

    #@76
    .line 1049
    move-object/from16 v0, p0

    #@78
    iget-object v4, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@7a
    const-string v5, "replyAddress"

    #@7c
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7f
    move-result v4

    #@80
    const/4 v5, 0x1

    #@81
    if-ne v4, v5, :cond_bc

    #@83
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isReleaseOperator()Z

    #@86
    move-result v4

    #@87
    const/4 v5, 0x1

    #@88
    if-ne v4, v5, :cond_bc

    #@8a
    .line 1050
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8d
    move-result v4

    #@8e
    if-nez v4, :cond_bc

    #@90
    .line 1052
    invoke-static/range {p6 .. p6}, Landroid/telephony/PhoneNumberUtils;->KRsmsnetworkPortionToCalledPartyBCD(Ljava/lang/String;)[B

    #@93
    move-result-object v13

    #@94
    .line 1053
    .local v13, daBytes:[B
    new-instance v21, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@96
    invoke-direct/range {v21 .. v21}, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;-><init>()V

    #@99
    .line 1055
    .local v21, replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    if-eqz v13, :cond_b6

    #@9b
    .line 1057
    move-object/from16 v0, v21

    #@9d
    iput-object v13, v0, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->replyData:[B

    #@9f
    .line 1058
    array-length v4, v13

    #@a0
    add-int/lit8 v4, v4, -0x1

    #@a2
    mul-int/lit8 v5, v4, 0x2

    #@a4
    array-length v4, v13

    #@a5
    add-int/lit8 v4, v4, -0x1

    #@a7
    aget-byte v4, v13, v4

    #@a9
    and-int/lit16 v4, v4, 0xf0

    #@ab
    const/16 v6, 0xf0

    #@ad
    if-ne v4, v6, :cond_151

    #@af
    const/4 v4, 0x1

    #@b0
    :goto_b0
    sub-int v4, v5, v4

    #@b2
    move-object/from16 v0, v21

    #@b4
    iput v4, v0, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->addressLength:I

    #@b6
    .line 1063
    :cond_b6
    move-object/from16 v0, v21

    #@b8
    move-object/from16 v1, v24

    #@ba
    iput-object v0, v1, Lcom/android/internal/telephony/SmsHeader;->replyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@bc
    .line 1069
    .end local v13           #daBytes:[B
    .end local v21           #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    :cond_bc
    move-object/from16 v0, p0

    #@be
    iget-object v4, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@c0
    const-string v5, "confirmRead"

    #@c2
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c5
    move-result v4

    #@c6
    const/4 v5, 0x1

    #@c7
    if-ne v4, v5, :cond_ef

    #@c9
    .line 1070
    const/4 v4, -0x1

    #@ca
    move/from16 v0, p7

    #@cc
    if-le v0, v4, :cond_ef

    #@ce
    .line 1071
    new-instance v23, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@d0
    invoke-direct/range {v23 .. v23}, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;-><init>()V

    #@d3
    .line 1072
    .local v23, smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;
    const/4 v4, 0x1

    #@d4
    move-object/from16 v0, v23

    #@d6
    iput v4, v0, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->length:I

    #@d8
    .line 1073
    const/4 v4, 0x1

    #@d9
    new-array v4, v4, [B

    #@db
    move-object/from16 v0, v23

    #@dd
    iput-object v4, v0, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@df
    .line 1074
    move-object/from16 v0, v23

    #@e1
    iget-object v4, v0, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@e3
    const/4 v5, 0x0

    #@e4
    move/from16 v0, p7

    #@e6
    int-to-byte v6, v0

    #@e7
    aput-byte v6, v4, v5

    #@e9
    .line 1075
    move-object/from16 v0, v23

    #@eb
    move-object/from16 v1, v24

    #@ed
    iput-object v0, v1, Lcom/android/internal/telephony/SmsHeader;->smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@ef
    .line 1080
    .end local v23           #smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;
    :cond_ef
    const/16 v22, 0x0

    #@f1
    .line 1081
    .local v22, sentIntent:Landroid/app/PendingIntent;
    if-eqz p4, :cond_105

    #@f3
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    #@f6
    move-result v4

    #@f7
    move/from16 v0, v16

    #@f9
    if-le v4, v0, :cond_105

    #@fb
    .line 1082
    move-object/from16 v0, p4

    #@fd
    move/from16 v1, v16

    #@ff
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@102
    move-result-object v22

    #@103
    .end local v22           #sentIntent:Landroid/app/PendingIntent;
    check-cast v22, Landroid/app/PendingIntent;

    #@105
    .line 1085
    .restart local v22       #sentIntent:Landroid/app/PendingIntent;
    :cond_105
    const/4 v14, 0x0

    #@106
    .line 1086
    .local v14, deliveryIntent:Landroid/app/PendingIntent;
    if-eqz p5, :cond_11a

    #@108
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    #@10b
    move-result v4

    #@10c
    move/from16 v0, v16

    #@10e
    if-le v4, v0, :cond_11a

    #@110
    .line 1087
    move-object/from16 v0, p5

    #@112
    move/from16 v1, v16

    #@114
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@117
    move-result-object v14

    #@118
    .end local v14           #deliveryIntent:Landroid/app/PendingIntent;
    check-cast v14, Landroid/app/PendingIntent;

    #@11a
    .line 1090
    .restart local v14       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_11a
    move-object/from16 v0, p0

    #@11c
    move-object/from16 v1, p1

    #@11e
    move/from16 v2, p8

    #@120
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->replyOptionDestnationNumber(Ljava/lang/String;I)Ljava/lang/String;

    #@123
    move-result-object v5

    #@124
    move-object/from16 v0, p3

    #@126
    move/from16 v1, v16

    #@128
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12b
    move-result-object v6

    #@12c
    check-cast v6, Ljava/lang/String;

    #@12e
    if-eqz v14, :cond_154

    #@130
    const/4 v7, 0x1

    #@131
    :goto_131
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@134
    move-result-object v8

    #@135
    move-object/from16 v0, v24

    #@137
    iget v10, v0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@139
    move-object/from16 v0, v24

    #@13b
    iget v11, v0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@13d
    move-object/from16 v4, p2

    #@13f
    invoke-static/range {v4 .. v11}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@142
    move-result-object v19

    #@143
    .line 1094
    .local v19, pdus:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-nez v19, :cond_156

    #@145
    .line 1095
    const-string v4, "sendMultipartTextLge(), failed : pdu is null"

    #@147
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@14a
    .line 1097
    const/4 v4, 0x3

    #@14b
    move-object/from16 v0, p4

    #@14d
    invoke-static {v0, v4}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Ljava/util/ArrayList;I)V

    #@150
    .line 1106
    .end local v12           #concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    .end local v14           #deliveryIntent:Landroid/app/PendingIntent;
    .end local v19           #pdus:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .end local v22           #sentIntent:Landroid/app/PendingIntent;
    .end local v24           #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    :cond_150
    return-void

    #@151
    .line 1058
    .restart local v12       #concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    .restart local v13       #daBytes:[B
    .restart local v21       #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    .restart local v24       #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    :cond_151
    const/4 v4, 0x0

    #@152
    goto/16 :goto_b0

    #@154
    .line 1090
    .end local v13           #daBytes:[B
    .end local v21           #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    .restart local v14       #deliveryIntent:Landroid/app/PendingIntent;
    .restart local v22       #sentIntent:Landroid/app/PendingIntent;
    :cond_154
    const/4 v7, 0x0

    #@155
    goto :goto_131

    #@156
    .line 1101
    .restart local v19       #pdus:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_156
    const/4 v4, 0x0

    #@157
    move-object/from16 v0, p0

    #@159
    move-object/from16 v1, p1

    #@15b
    move-object/from16 v2, p2

    #@15d
    move-object/from16 v3, v19

    #@15f
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@162
    move-result-object v17

    #@163
    .line 1102
    .local v17, map:Ljava/util/HashMap;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@166
    move-result-object v4

    #@167
    move-object/from16 v0, p0

    #@169
    move-object/from16 v1, v17

    #@16b
    move-object/from16 v2, v22

    #@16d
    invoke-virtual {v0, v1, v2, v14, v4}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@170
    move-result-object v25

    #@171
    .line 1104
    .local v25, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    move-object/from16 v0, p0

    #@173
    move-object/from16 v1, v25

    #@175
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@178
    .line 1032
    add-int/lit8 v16, v16, 0x1

    #@17a
    goto/16 :goto_51
.end method

.method protected sendMultipartTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V
    .registers 36
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1113
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const-string v4, "sendMultipartTextMoreLge()"

    #@2
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@5
    .line 1114
    invoke-static {}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getNextConcatenatedRef()I

    #@8
    move-result v4

    #@9
    and-int/lit16 v0, v4, 0xff

    #@b
    move/from16 v20, v0

    #@d
    .line 1115
    .local v20, refNumber:I
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v18

    #@11
    .line 1116
    .local v18, msgCount:I
    const/4 v9, 0x0

    #@12
    .line 1118
    .local v9, encoding:I
    move-object/from16 v0, p0

    #@14
    iget-object v4, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@16
    const-string v5, "SKTWzone"

    #@18
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1b
    move-result v4

    #@1c
    const/4 v5, 0x1

    #@1d
    if-ne v4, v5, :cond_23

    #@1f
    .line 1119
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->wZoneDestnationNumber(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object p1

    #@23
    .line 1122
    :cond_23
    move/from16 v0, v18

    #@25
    move-object/from16 v1, p0

    #@27
    iput v0, v1, Lcom/android/internal/telephony/SMSDispatcher;->mRemainingMessages:I

    #@29
    .line 1123
    const/16 v16, 0x0

    #@2b
    .local v16, i:I
    :goto_2b
    move/from16 v0, v16

    #@2d
    move/from16 v1, v18

    #@2f
    if-ge v0, v1, :cond_4f

    #@31
    .line 1124
    move-object/from16 v0, p3

    #@33
    move/from16 v1, v16

    #@35
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@38
    move-result-object v4

    #@39
    check-cast v4, Ljava/lang/CharSequence;

    #@3b
    const/4 v5, 0x0

    #@3c
    const/4 v6, 0x0

    #@3d
    invoke-static {v4, v5, v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;ZZ)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@40
    move-result-object v15

    #@41
    .line 1125
    .local v15, details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    iget v4, v15, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@43
    if-eq v9, v4, :cond_4c

    #@45
    if-eqz v9, :cond_4a

    #@47
    const/4 v4, 0x1

    #@48
    if-ne v9, v4, :cond_4c

    #@4a
    .line 1128
    :cond_4a
    iget v9, v15, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@4c
    .line 1123
    :cond_4c
    add-int/lit8 v16, v16, 0x1

    #@4e
    goto :goto_2b

    #@4f
    .line 1132
    .end local v15           #details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_4f
    const/16 v16, 0x0

    #@51
    :goto_51
    move/from16 v0, v16

    #@53
    move/from16 v1, v18

    #@55
    if-ge v0, v1, :cond_17c

    #@57
    .line 1133
    new-instance v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@59
    invoke-direct {v12}, Lcom/android/internal/telephony/SmsHeader$ConcatRef;-><init>()V

    #@5c
    .line 1134
    .local v12, concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    move/from16 v0, v20

    #@5e
    iput v0, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->refNumber:I

    #@60
    .line 1135
    add-int/lit8 v4, v16, 0x1

    #@62
    iput v4, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->seqNumber:I

    #@64
    .line 1136
    move/from16 v0, v18

    #@66
    iput v0, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->msgCount:I

    #@68
    .line 1143
    const/4 v4, 0x1

    #@69
    iput-boolean v4, v12, Lcom/android/internal/telephony/SmsHeader$ConcatRef;->isEightBits:Z

    #@6b
    .line 1144
    new-instance v24, Lcom/android/internal/telephony/SmsHeader;

    #@6d
    invoke-direct/range {v24 .. v24}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@70
    .line 1145
    .local v24, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    move-object/from16 v0, v24

    #@72
    iput-object v12, v0, Lcom/android/internal/telephony/SmsHeader;->concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;

    #@74
    .line 1148
    if-nez v16, :cond_ef

    #@76
    .line 1150
    move-object/from16 v0, p0

    #@78
    iget-object v4, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@7a
    const-string v5, "replyAddress"

    #@7c
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7f
    move-result v4

    #@80
    const/4 v5, 0x1

    #@81
    if-ne v4, v5, :cond_bc

    #@83
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isReleaseOperator()Z

    #@86
    move-result v4

    #@87
    const/4 v5, 0x1

    #@88
    if-ne v4, v5, :cond_bc

    #@8a
    .line 1152
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8d
    move-result v4

    #@8e
    if-nez v4, :cond_bc

    #@90
    .line 1154
    invoke-static/range {p6 .. p6}, Landroid/telephony/PhoneNumberUtils;->KRsmsnetworkPortionToCalledPartyBCD(Ljava/lang/String;)[B

    #@93
    move-result-object v13

    #@94
    .line 1155
    .local v13, daBytes:[B
    new-instance v21, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@96
    invoke-direct/range {v21 .. v21}, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;-><init>()V

    #@99
    .line 1157
    .local v21, replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    if-eqz v13, :cond_b6

    #@9b
    .line 1158
    move-object/from16 v0, v21

    #@9d
    iput-object v13, v0, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->replyData:[B

    #@9f
    .line 1159
    array-length v4, v13

    #@a0
    add-int/lit8 v4, v4, -0x1

    #@a2
    mul-int/lit8 v5, v4, 0x2

    #@a4
    array-length v4, v13

    #@a5
    add-int/lit8 v4, v4, -0x1

    #@a7
    aget-byte v4, v13, v4

    #@a9
    and-int/lit16 v4, v4, 0xf0

    #@ab
    const/16 v6, 0xf0

    #@ad
    if-ne v4, v6, :cond_16b

    #@af
    const/4 v4, 0x1

    #@b0
    :goto_b0
    sub-int v4, v5, v4

    #@b2
    move-object/from16 v0, v21

    #@b4
    iput v4, v0, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->addressLength:I

    #@b6
    .line 1165
    :cond_b6
    move-object/from16 v0, v21

    #@b8
    move-object/from16 v1, v24

    #@ba
    iput-object v0, v1, Lcom/android/internal/telephony/SmsHeader;->replyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@bc
    .line 1171
    .end local v13           #daBytes:[B
    .end local v21           #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    :cond_bc
    move-object/from16 v0, p0

    #@be
    iget-object v4, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@c0
    const-string v5, "confirmRead"

    #@c2
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c5
    move-result v4

    #@c6
    const/4 v5, 0x1

    #@c7
    if-ne v4, v5, :cond_ef

    #@c9
    .line 1172
    const/4 v4, -0x1

    #@ca
    move/from16 v0, p7

    #@cc
    if-le v0, v4, :cond_ef

    #@ce
    .line 1173
    new-instance v23, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@d0
    invoke-direct/range {v23 .. v23}, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;-><init>()V

    #@d3
    .line 1174
    .local v23, smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;
    const/4 v4, 0x1

    #@d4
    move-object/from16 v0, v23

    #@d6
    iput v4, v0, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->length:I

    #@d8
    .line 1175
    const/4 v4, 0x1

    #@d9
    new-array v4, v4, [B

    #@db
    move-object/from16 v0, v23

    #@dd
    iput-object v4, v0, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@df
    .line 1176
    move-object/from16 v0, v23

    #@e1
    iget-object v4, v0, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@e3
    const/4 v5, 0x0

    #@e4
    move/from16 v0, p7

    #@e6
    int-to-byte v6, v0

    #@e7
    aput-byte v6, v4, v5

    #@e9
    .line 1177
    move-object/from16 v0, v23

    #@eb
    move-object/from16 v1, v24

    #@ed
    iput-object v0, v1, Lcom/android/internal/telephony/SmsHeader;->smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@ef
    .line 1183
    .end local v23           #smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;
    :cond_ef
    const/16 v22, 0x0

    #@f1
    .line 1184
    .local v22, sentIntent:Landroid/app/PendingIntent;
    if-eqz p4, :cond_105

    #@f3
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    #@f6
    move-result v4

    #@f7
    move/from16 v0, v16

    #@f9
    if-le v4, v0, :cond_105

    #@fb
    .line 1185
    move-object/from16 v0, p4

    #@fd
    move/from16 v1, v16

    #@ff
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@102
    move-result-object v22

    #@103
    .end local v22           #sentIntent:Landroid/app/PendingIntent;
    check-cast v22, Landroid/app/PendingIntent;

    #@105
    .line 1188
    .restart local v22       #sentIntent:Landroid/app/PendingIntent;
    :cond_105
    const/4 v14, 0x0

    #@106
    .line 1189
    .local v14, deliveryIntent:Landroid/app/PendingIntent;
    if-eqz p5, :cond_11a

    #@108
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    #@10b
    move-result v4

    #@10c
    move/from16 v0, v16

    #@10e
    if-le v4, v0, :cond_11a

    #@110
    .line 1190
    move-object/from16 v0, p5

    #@112
    move/from16 v1, v16

    #@114
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@117
    move-result-object v14

    #@118
    .end local v14           #deliveryIntent:Landroid/app/PendingIntent;
    check-cast v14, Landroid/app/PendingIntent;

    #@11a
    .line 1193
    .restart local v14       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_11a
    move-object/from16 v0, p0

    #@11c
    move-object/from16 v1, p1

    #@11e
    move/from16 v2, p8

    #@120
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->replyOptionDestnationNumber(Ljava/lang/String;I)Ljava/lang/String;

    #@123
    move-result-object v5

    #@124
    move-object/from16 v0, p3

    #@126
    move/from16 v1, v16

    #@128
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12b
    move-result-object v6

    #@12c
    check-cast v6, Ljava/lang/String;

    #@12e
    if-eqz v14, :cond_16e

    #@130
    const/4 v7, 0x1

    #@131
    :goto_131
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@134
    move-result-object v8

    #@135
    move-object/from16 v0, v24

    #@137
    iget v10, v0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@139
    move-object/from16 v0, v24

    #@13b
    iget v11, v0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@13d
    move-object/from16 v4, p2

    #@13f
    invoke-static/range {v4 .. v11}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@142
    move-result-object v19

    #@143
    .line 1197
    .local v19, pdus:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz v19, :cond_170

    #@145
    .line 1198
    const/4 v4, 0x0

    #@146
    move-object/from16 v0, p0

    #@148
    move-object/from16 v1, p1

    #@14a
    move-object/from16 v2, p2

    #@14c
    move-object/from16 v3, v19

    #@14e
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@151
    move-result-object v17

    #@152
    .line 1199
    .local v17, map:Ljava/util/HashMap;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@155
    move-result-object v4

    #@156
    move-object/from16 v0, p0

    #@158
    move-object/from16 v1, v17

    #@15a
    move-object/from16 v2, v22

    #@15c
    invoke-virtual {v0, v1, v2, v14, v4}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@15f
    move-result-object v25

    #@160
    .line 1201
    .local v25, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    move-object/from16 v0, p0

    #@162
    move-object/from16 v1, v25

    #@164
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPduMore(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@167
    .line 1132
    .end local v17           #map:Ljava/util/HashMap;
    .end local v25           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_167
    add-int/lit8 v16, v16, 0x1

    #@169
    goto/16 :goto_51

    #@16b
    .line 1159
    .end local v14           #deliveryIntent:Landroid/app/PendingIntent;
    .end local v19           #pdus:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .end local v22           #sentIntent:Landroid/app/PendingIntent;
    .restart local v13       #daBytes:[B
    .restart local v21       #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    :cond_16b
    const/4 v4, 0x0

    #@16c
    goto/16 :goto_b0

    #@16e
    .line 1193
    .end local v13           #daBytes:[B
    .end local v21           #replayaddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;
    .restart local v14       #deliveryIntent:Landroid/app/PendingIntent;
    .restart local v22       #sentIntent:Landroid/app/PendingIntent;
    :cond_16e
    const/4 v7, 0x0

    #@16f
    goto :goto_131

    #@170
    .line 1204
    .restart local v19       #pdus:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_170
    const-string v4, "sendMultipartTextMoreLge(), getSubmitPdu() returned null"

    #@172
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@175
    .line 1206
    const/4 v4, 0x3

    #@176
    move-object/from16 v0, p4

    #@178
    invoke-static {v0, v4}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Ljava/util/ArrayList;I)V

    #@17b
    goto :goto_167

    #@17c
    .line 1210
    .end local v12           #concatRef:Lcom/android/internal/telephony/SmsHeader$ConcatRef;
    .end local v14           #deliveryIntent:Landroid/app/PendingIntent;
    .end local v19           #pdus:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .end local v22           #sentIntent:Landroid/app/PendingIntent;
    .end local v24           #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    :cond_17c
    return-void
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V
    .registers 22
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"

    #@0
    .prologue
    .line 1261
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "SKTWzone"

    #@4
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v2

    #@8
    const/4 v3, 0x1

    #@9
    if-ne v2, v3, :cond_f

    #@b
    .line 1262
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->wZoneDestnationNumber(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object p1

    #@f
    .line 1265
    :cond_f
    if-eqz p7, :cond_40

    #@11
    const/4 v5, 0x1

    #@12
    :goto_12
    invoke-static/range {p4 .. p4}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@15
    move-result-object v6

    #@16
    move-object/from16 v0, p4

    #@18
    iget v8, v0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@1a
    move-object/from16 v0, p4

    #@1c
    iget v9, v0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@1e
    move-object v2, p2

    #@1f
    move-object v3, p1

    #@20
    move-object/from16 v4, p3

    #@22
    move/from16 v7, p5

    #@24
    invoke-static/range {v2 .. v9}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@27
    move-result-object v11

    #@28
    .line 1268
    .local v11, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz v11, :cond_42

    #@2a
    .line 1269
    move-object/from16 v0, p3

    #@2c
    invoke-virtual {p0, p1, p2, v0, v11}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@2f
    move-result-object v10

    #@30
    .line 1271
    .local v10, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    move-object/from16 v0, p6

    #@36
    move-object/from16 v1, p7

    #@38
    invoke-virtual {p0, v10, v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@3b
    move-result-object v12

    #@3c
    .line 1273
    .local v12, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v12}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@3f
    .line 1277
    .end local v10           #map:Ljava/util/HashMap;
    .end local v12           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_3f
    return-void

    #@40
    .line 1265
    .end local v11           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_40
    const/4 v5, 0x0

    #@41
    goto :goto_12

    #@42
    .line 1275
    .restart local v11       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_42
    const-string v2, "sendNewSubmitPdu(), getSubmitPdu() returned null"

    #@44
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@47
    goto :goto_3f
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;)V
    .registers 22
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 1306
    if-eqz p7, :cond_34

    #@2
    const/4 v5, 0x1

    #@3
    :goto_3
    invoke-static/range {p4 .. p4}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@6
    move-result-object v6

    #@7
    move-object/from16 v0, p4

    #@9
    iget v8, v0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@b
    move-object/from16 v0, p4

    #@d
    iget v9, v0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@f
    move-object v2, p2

    #@10
    move-object v3, p1

    #@11
    move-object v4, p3

    #@12
    move/from16 v7, p5

    #@14
    invoke-static/range {v2 .. v9}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@17
    move-result-object v6

    #@18
    .line 1310
    .local v6, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz v6, :cond_36

    #@1a
    move-object v2, p0

    #@1b
    move-object v3, p1

    #@1c
    move-object v4, p2

    #@1d
    move-object v5, p3

    #@1e
    move-object/from16 v7, p9

    #@20
    .line 1311
    invoke-virtual/range {v2 .. v7}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;Ljava/lang/String;)Ljava/util/HashMap;

    #@23
    move-result-object v10

    #@24
    .line 1313
    .local v10, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    move-object/from16 v0, p6

    #@2a
    move-object/from16 v1, p7

    #@2c
    invoke-virtual {p0, v10, v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@2f
    move-result-object v11

    #@30
    .line 1315
    .local v11, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@33
    .line 1320
    .end local v10           #map:Ljava/util/HashMap;
    .end local v11           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_33
    return-void

    #@34
    .line 1306
    .end local v6           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_34
    const/4 v5, 0x0

    #@35
    goto :goto_3

    #@36
    .line 1317
    .restart local v6       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_36
    const-string v2, "sendNewSubmitPdu(), getSubmitPdu() returned null"

    #@38
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@3b
    goto :goto_33
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZLjava/lang/String;Z)V
    .registers 23
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"
    .parameter "cbAddress"
    .parameter "isMultiPart"

    #@0
    .prologue
    .line 1329
    if-eqz p7, :cond_36

    #@2
    const/4 v5, 0x1

    #@3
    :goto_3
    invoke-static/range {p4 .. p4}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@6
    move-result-object v6

    #@7
    move-object/from16 v0, p4

    #@9
    iget v8, v0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@b
    move-object/from16 v0, p4

    #@d
    iget v9, v0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@f
    move-object v2, p2

    #@10
    move-object v3, p1

    #@11
    move-object v4, p3

    #@12
    move/from16 v7, p5

    #@14
    invoke-static/range {v2 .. v9}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@17
    move-result-object v6

    #@18
    .line 1333
    .local v6, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz v6, :cond_38

    #@1a
    move-object v2, p0

    #@1b
    move-object v3, p1

    #@1c
    move-object v4, p2

    #@1d
    move-object v5, p3

    #@1e
    move-object/from16 v7, p9

    #@20
    move/from16 v8, p10

    #@22
    .line 1334
    invoke-virtual/range {v2 .. v8}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;Ljava/lang/String;Z)Ljava/util/HashMap;

    #@25
    move-result-object v10

    #@26
    .line 1336
    .local v10, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    move-object/from16 v0, p6

    #@2c
    move-object/from16 v1, p7

    #@2e
    invoke-virtual {p0, v10, v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@31
    move-result-object v11

    #@32
    .line 1338
    .local v11, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@35
    .line 1343
    .end local v10           #map:Ljava/util/HashMap;
    .end local v11           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_35
    return-void

    #@36
    .line 1329
    .end local v6           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_36
    const/4 v5, 0x0

    #@37
    goto :goto_3

    #@38
    .line 1340
    .restart local v6       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_38
    const-string v2, "sendNewSubmitPdu(), getSubmitPdu() returned null"

    #@3a
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@3d
    goto :goto_35
.end method

.method protected sendNewSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;ZZ)V
    .registers 22
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"
    .parameter "isMultiPart"

    #@0
    .prologue
    .line 1285
    if-eqz p7, :cond_34

    #@2
    const/4 v5, 0x1

    #@3
    :goto_3
    invoke-static/range {p4 .. p4}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@6
    move-result-object v6

    #@7
    move-object/from16 v0, p4

    #@9
    iget v8, v0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@b
    move-object/from16 v0, p4

    #@d
    iget v9, v0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@f
    move-object v2, p2

    #@10
    move-object v3, p1

    #@11
    move-object v4, p3

    #@12
    move/from16 v7, p5

    #@14
    invoke-static/range {v2 .. v9}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@17
    move-result-object v6

    #@18
    .line 1288
    .local v6, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz v6, :cond_36

    #@1a
    move-object v2, p0

    #@1b
    move-object v3, p1

    #@1c
    move-object v4, p2

    #@1d
    move-object v5, p3

    #@1e
    move/from16 v7, p9

    #@20
    .line 1289
    invoke-virtual/range {v2 .. v7}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;Z)Ljava/util/HashMap;

    #@23
    move-result-object v10

    #@24
    .line 1291
    .local v10, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    move-object/from16 v0, p6

    #@2a
    move-object/from16 v1, p7

    #@2c
    invoke-virtual {p0, v10, v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@2f
    move-result-object v11

    #@30
    .line 1293
    .local v11, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@33
    .line 1297
    .end local v10           #map:Ljava/util/HashMap;
    .end local v11           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_33
    return-void

    #@34
    .line 1285
    .end local v6           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_34
    const/4 v5, 0x0

    #@35
    goto :goto_3

    #@36
    .line 1295
    .restart local v6       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_36
    const-string v2, "sendNewSubmitPdu(), getSubmitPdu() returned null"

    #@38
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@3b
    goto :goto_33
.end method

.method protected sendNewSubmitPduforEmailoverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsHeader;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V
    .registers 20
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "message"
    .parameter "smsHeader"
    .parameter "encoding"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "lastPart"

    #@0
    .prologue
    .line 1352
    if-eqz p7, :cond_26

    #@2
    const/4 v5, 0x1

    #@3
    :goto_3
    invoke-static {p4}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@6
    move-result-object v6

    #@7
    move-object v2, p2

    #@8
    move-object v3, p1

    #@9
    move-object v4, p3

    #@a
    move/from16 v7, p5

    #@c
    invoke-static/range {v2 .. v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPduforEmailoverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BI)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@f
    move-result-object v9

    #@10
    .line 1356
    .local v9, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz v9, :cond_28

    #@12
    .line 1357
    invoke-virtual {p0, p1, p2, p3, v9}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@15
    move-result-object v8

    #@16
    .line 1359
    .local v8, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    move-object/from16 v0, p6

    #@1c
    move-object/from16 v1, p7

    #@1e
    invoke-virtual {p0, v8, v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@21
    move-result-object v10

    #@22
    .line 1361
    .local v10, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@25
    .line 1366
    .end local v8           #map:Ljava/util/HashMap;
    .end local v10           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_25
    return-void

    #@26
    .line 1352
    .end local v9           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_26
    const/4 v5, 0x0

    #@27
    goto :goto_3

    #@28
    .line 1363
    .restart local v9       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_28
    const-string v2, "GSM"

    #@2a
    const-string v3, "GsmSMSDispatcher.sendNewSubmitPdu(): getSubmitPdu() returned null"

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_25
.end method

.method public sendRetrySms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 3
    .parameter "tracker"

    #@0
    .prologue
    .line 1426
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mImsSMSDispatcher:Lcom/android/internal/telephony/ImsSMSDispatcher;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/ImsSMSDispatcher;->sendRetrySms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@5
    .line 1427
    return-void
.end method

.method protected sendSms(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 11
    .parameter "tracker"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 1372
    iget-object v6, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mData:Ljava/util/HashMap;

    #@4
    .line 1374
    .local v6, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "smsc"

    #@6
    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, [B

    #@c
    move-object v8, v0

    #@d
    check-cast v8, [B

    #@f
    .line 1375
    .local v8, smsc:[B
    const-string v0, "pdu"

    #@11
    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, [B

    #@17
    move-object v7, v0

    #@18
    check-cast v7, [B

    #@1a
    .line 1377
    .local v7, pdu:[B
    const/4 v0, 0x2

    #@1b
    invoke-virtual {p0, v0, p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1e
    move-result-object v5

    #@1f
    .line 1379
    .local v5, reply:Landroid/os/Message;
    new-instance v0, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v1, "sendSms(),  isIms()="

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->isIms()Z

    #@2d
    move-result v1

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    const-string v1, " mRetryCount="

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    iget v1, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v0

    #@3e
    const-string v1, " mImsRetry="

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    iget v1, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v0

    #@4a
    const-string v1, " mMessageRef="

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v0

    #@50
    iget v1, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mMessageRef:I

    #@52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v0

    #@56
    const-string v1, " SS="

    #@58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v0

    #@5c
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5e
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getState()I

    #@65
    move-result v1

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v0

    #@6a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v0

    #@6e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@71
    .line 1390
    iget v0, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@73
    if-nez v0, :cond_bc

    #@75
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->isIms()Z

    #@78
    move-result v0

    #@79
    if-nez v0, :cond_bc

    #@7b
    .line 1391
    iget v0, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mRetryCount:I

    #@7d
    if-lez v0, :cond_91

    #@7f
    .line 1395
    aget-byte v0, v7, v3

    #@81
    and-int/lit8 v0, v0, 0x1

    #@83
    if-ne v0, v2, :cond_91

    #@85
    .line 1396
    aget-byte v0, v7, v3

    #@87
    or-int/lit8 v0, v0, 0x4

    #@89
    int-to-byte v0, v0

    #@8a
    aput-byte v0, v7, v3

    #@8c
    .line 1397
    iget v0, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mMessageRef:I

    #@8e
    int-to-byte v0, v0

    #@8f
    aput-byte v0, v7, v2

    #@91
    .line 1401
    :cond_91
    const-string v0, "sendSms(), GsmSMSDispatcher --> RIL"

    #@93
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@96
    .line 1403
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@98
    const-string v1, "enable_link_control"

    #@9a
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9d
    move-result v0

    #@9e
    if-ne v0, v2, :cond_ae

    #@a0
    .line 1404
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@a2
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@a5
    move-result-object v1

    #@a6
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@a9
    move-result-object v2

    #@aa
    invoke-interface {v0, v1, v2, v5}, Lcom/android/internal/telephony/CommandsInterface;->sendSmsMore(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@ad
    .line 1421
    :goto_ad
    return-void

    #@ae
    .line 1407
    :cond_ae
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@b0
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@b3
    move-result-object v1

    #@b4
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@b7
    move-result-object v2

    #@b8
    invoke-interface {v0, v1, v2, v5}, Lcom/android/internal/telephony/CommandsInterface;->sendSMS(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@bb
    goto :goto_ad

    #@bc
    .line 1413
    :cond_bc
    const-string v0, "sendSms(), GsmSMSDispatcher --> RIL"

    #@be
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@c1
    .line 1414
    iget-object v0, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@c3
    invoke-static {v8}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@c6
    move-result-object v1

    #@c7
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@ca
    move-result-object v2

    #@cb
    iget v3, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@cd
    iget v4, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mMessageRef:I

    #@cf
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/CommandsInterface;->sendImsGsmSms(Ljava/lang/String;Ljava/lang/String;IILandroid/os/Message;)V

    #@d2
    .line 1419
    iget v0, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@d4
    add-int/lit8 v0, v0, 0x1

    #@d6
    iput v0, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mImsRetry:I

    #@d8
    goto :goto_ad
.end method

.method protected sendSmsMore(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V
    .registers 9
    .parameter "tracker"

    #@0
    .prologue
    .line 1432
    iget-object v0, p1, Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;->mData:Ljava/util/HashMap;

    #@2
    .line 1434
    .local v0, map:Ljava/util/HashMap;
    const-string v4, "smsc"

    #@4
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v4

    #@8
    check-cast v4, [B

    #@a
    move-object v3, v4

    #@b
    check-cast v3, [B

    #@d
    .line 1435
    .local v3, smsc:[B
    const-string v4, "pdu"

    #@f
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v4

    #@13
    check-cast v4, [B

    #@15
    move-object v1, v4

    #@16
    check-cast v1, [B

    #@18
    .line 1437
    .local v1, pdu:[B
    const-string v4, "sendSmsMore()"

    #@1a
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@1d
    .line 1438
    const/4 v4, 0x2

    #@1e
    invoke-virtual {p0, v4, p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@21
    move-result-object v2

    #@22
    .line 1439
    .local v2, reply:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/internal/telephony/SMSDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@24
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@27
    move-result-object v5

    #@28
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@2b
    move-result-object v6

    #@2c
    invoke-interface {v4, v5, v6, v2}, Lcom/android/internal/telephony/CommandsInterface;->sendSmsMore(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@2f
    .line 1441
    return-void
.end method

.method protected sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 27
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    .line 837
    move-object/from16 v0, p0

    #@2
    iget-object v5, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@4
    const-string v6, "SKTWzone"

    #@6
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9
    move-result v5

    #@a
    const/4 v6, 0x1

    #@b
    if-ne v5, v6, :cond_11

    #@d
    .line 838
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->wZoneDestnationNumber(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object p1

    #@11
    .line 842
    :cond_11
    const/4 v10, 0x0

    #@12
    .line 844
    .local v10, encoding:I
    move-object/from16 v0, p0

    #@14
    iget-object v5, v0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@16
    const-string v6, "KREncodingScheme"

    #@18
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1b
    move-result v5

    #@1c
    const/4 v6, 0x1

    #@1d
    if-ne v5, v6, :cond_82

    #@1f
    .line 845
    sget v5, Lcom/android/internal/telephony/SMSDispatcher;->vp:I

    #@21
    if-lez v5, :cond_28

    #@23
    .line 846
    sget v5, Lcom/android/internal/telephony/SMSDispatcher;->vp:I

    #@25
    invoke-static {v5}, Lcom/android/internal/telephony/gsm/SmsMessage;->setValidityPeriod(I)V

    #@28
    .line 848
    :cond_28
    if-eqz p5, :cond_50

    #@2a
    const/4 v8, 0x1

    #@2b
    :goto_2b
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getLine1Number()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@32
    move-result v5

    #@33
    const/4 v6, 0x1

    #@34
    if-ne v5, v6, :cond_52

    #@36
    const/4 v9, 0x0

    #@37
    :goto_37
    const/4 v11, 0x0

    #@38
    move-object/from16 v5, p2

    #@3a
    move-object/from16 v6, p1

    #@3c
    move-object/from16 v7, p3

    #@3e
    invoke-static/range {v5 .. v11}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@41
    move-result-object v18

    #@42
    .line 853
    .local v18, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-nez v18, :cond_5c

    #@44
    .line 854
    const-string v5, "sendText(), sendText failed : pdu is null"

    #@46
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@49
    .line 856
    const/4 v5, 0x3

    #@4a
    move-object/from16 v0, p4

    #@4c
    invoke-static {v0, v5}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Landroid/app/PendingIntent;I)V

    #@4f
    .line 922
    :goto_4f
    return-void

    #@50
    .line 848
    .end local v18           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_50
    const/4 v8, 0x0

    #@51
    goto :goto_2b

    #@52
    :cond_52
    const/4 v5, -0x1

    #@53
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getLine1Number()Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    invoke-static {v5, v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->makeSmsHeader(ILjava/lang/String;)[B

    #@5a
    move-result-object v9

    #@5b
    goto :goto_37

    #@5c
    .line 861
    .restart local v18       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_5c
    move-object/from16 v0, p0

    #@5e
    move-object/from16 v1, p1

    #@60
    move-object/from16 v2, p2

    #@62
    move-object/from16 v3, p3

    #@64
    move-object/from16 v4, v18

    #@66
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@69
    move-result-object v17

    #@6a
    .line 862
    .local v17, map:Ljava/util/HashMap;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@6d
    move-result-object v5

    #@6e
    move-object/from16 v0, p0

    #@70
    move-object/from16 v1, v17

    #@72
    move-object/from16 v2, p4

    #@74
    move-object/from16 v3, p5

    #@76
    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@79
    move-result-object v20

    #@7a
    .line 864
    .local v20, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    move-object/from16 v0, p0

    #@7c
    move-object/from16 v1, v20

    #@7e
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@81
    goto :goto_4f

    #@82
    .line 869
    .end local v17           #map:Ljava/util/HashMap;
    .end local v18           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .end local v20           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :cond_82
    const-string v5, "persist.gsm.sms.forcegsm7"

    #@84
    const-string v6, "1"

    #@86
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@89
    move-result-object v15

    #@8a
    .line 870
    .local v15, encodingType:Ljava/lang/String;
    const-string v5, "0"

    #@8c
    invoke-virtual {v15, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v16

    #@90
    .line 872
    .local v16, force7bit:Z
    new-instance v14, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@92
    invoke-direct {v14}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@95
    .line 877
    .local v14, encodingForParts:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    move-object/from16 v0, p3

    #@97
    move/from16 v1, v16

    #@99
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@9c
    move-result-object v13

    #@9d
    .line 879
    .local v13, details:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    iget v5, v13, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@9f
    if-eq v10, v5, :cond_a8

    #@a1
    if-eqz v10, :cond_a6

    #@a3
    const/4 v5, 0x1

    #@a4
    if-ne v10, v5, :cond_a8

    #@a6
    .line 882
    :cond_a6
    iget v10, v13, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@a8
    .line 884
    :cond_a8
    move-object v14, v13

    #@a9
    .line 888
    new-instance v19, Lcom/android/internal/telephony/SmsHeader;

    #@ab
    invoke-direct/range {v19 .. v19}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@ae
    .line 889
    .local v19, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    const/4 v5, 0x1

    #@af
    if-ne v10, v5, :cond_c2

    #@b1
    .line 890
    iget v5, v14, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@b3
    const/4 v6, 0x1

    #@b4
    if-ne v5, v6, :cond_c2

    #@b6
    .line 891
    iget v5, v14, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@b8
    move-object/from16 v0, v19

    #@ba
    iput v5, v0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@bc
    .line 892
    iget v5, v14, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@be
    move-object/from16 v0, v19

    #@c0
    iput v5, v0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@c2
    .line 899
    :cond_c2
    sget v5, Lcom/android/internal/telephony/SMSDispatcher;->vp:I

    #@c4
    if-lez v5, :cond_cb

    #@c6
    .line 900
    sget v5, Lcom/android/internal/telephony/SMSDispatcher;->vp:I

    #@c8
    invoke-static {v5}, Lcom/android/internal/telephony/gsm/SmsMessage;->setValidityPeriod(I)V

    #@cb
    .line 903
    :cond_cb
    new-instance v18, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@cd
    invoke-direct/range {v18 .. v18}, Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;-><init>()V

    #@d0
    .line 904
    .restart local v18       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    iget v5, v14, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@d2
    const/4 v6, 0x1

    #@d3
    if-ne v5, v6, :cond_119

    #@d5
    .line 905
    if-eqz p5, :cond_117

    #@d7
    const/4 v8, 0x1

    #@d8
    :goto_d8
    invoke-static/range {v19 .. v19}, Lcom/android/internal/telephony/SmsHeader;->toByteArray(Lcom/android/internal/telephony/SmsHeader;)[B

    #@db
    move-result-object v9

    #@dc
    move-object/from16 v0, v19

    #@de
    iget v11, v0, Lcom/android/internal/telephony/SmsHeader;->languageTable:I

    #@e0
    move-object/from16 v0, v19

    #@e2
    iget v12, v0, Lcom/android/internal/telephony/SmsHeader;->languageShiftTable:I

    #@e4
    move-object/from16 v5, p2

    #@e6
    move-object/from16 v6, p1

    #@e8
    move-object/from16 v7, p3

    #@ea
    invoke-static/range {v5 .. v12}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@ed
    move-result-object v18

    #@ee
    .line 914
    :goto_ee
    if-eqz v18, :cond_129

    #@f0
    .line 915
    move-object/from16 v0, p0

    #@f2
    move-object/from16 v1, p1

    #@f4
    move-object/from16 v2, p2

    #@f6
    move-object/from16 v3, p3

    #@f8
    move-object/from16 v4, v18

    #@fa
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@fd
    move-result-object v17

    #@fe
    .line 916
    .restart local v17       #map:Ljava/util/HashMap;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@101
    move-result-object v5

    #@102
    move-object/from16 v0, p0

    #@104
    move-object/from16 v1, v17

    #@106
    move-object/from16 v2, p4

    #@108
    move-object/from16 v3, p5

    #@10a
    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@10d
    move-result-object v20

    #@10e
    .line 918
    .restart local v20       #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    move-object/from16 v0, p0

    #@110
    move-object/from16 v1, v20

    #@112
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@115
    goto/16 :goto_4f

    #@117
    .line 905
    .end local v17           #map:Ljava/util/HashMap;
    .end local v20           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :cond_117
    const/4 v8, 0x0

    #@118
    goto :goto_d8

    #@119
    .line 909
    :cond_119
    if-eqz p5, :cond_127

    #@11b
    const/4 v5, 0x1

    #@11c
    :goto_11c
    move-object/from16 v0, p2

    #@11e
    move-object/from16 v1, p1

    #@120
    move-object/from16 v2, p3

    #@122
    invoke-static {v0, v1, v2, v5}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@125
    move-result-object v18

    #@126
    goto :goto_ee

    #@127
    :cond_127
    const/4 v5, 0x0

    #@128
    goto :goto_11c

    #@129
    .line 920
    :cond_129
    const-string v5, "sendText(), getSubmitPdu() returned null"

    #@12b
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@12e
    goto/16 :goto_4f
.end method

.method protected sendTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V
    .registers 22
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"

    #@0
    .prologue
    .line 968
    const-string v2, "sendTextLge(), GsmSMSDispatcher > sendTextLge"

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@5
    .line 970
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@7
    const-string v3, "SKTWzone"

    #@9
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c
    move-result v2

    #@d
    const/4 v3, 0x1

    #@e
    if-ne v2, v3, :cond_14

    #@10
    .line 971
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->wZoneDestnationNumber(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object p1

    #@14
    .line 974
    :cond_14
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@16
    const-string v3, "KREncodingScheme"

    #@18
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1b
    move-result v2

    #@1c
    const/4 v3, 0x1

    #@1d
    if-ne v2, v3, :cond_6a

    #@1f
    .line 975
    const-string v2, "sendTextLge(), GsmSMSDispatcher > sendTextLge > ModelConfig.COUNTRY_KR"

    #@21
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@24
    .line 976
    move/from16 v0, p8

    #@26
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->replyOptionDestnationNumber(Ljava/lang/String;I)Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    if-eqz p5, :cond_52

    #@2c
    const/4 v5, 0x1

    #@2d
    :goto_2d
    move/from16 v0, p7

    #@2f
    move-object/from16 v1, p6

    #@31
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->makeSmsHeader(ILjava/lang/String;)[B

    #@34
    move-result-object v6

    #@35
    const/16 v2, 0x4f

    #@37
    move/from16 v0, p9

    #@39
    if-ne v0, v2, :cond_54

    #@3b
    const/4 v7, 0x2

    #@3c
    :goto_3c
    move-object v2, p2

    #@3d
    move-object v4, p3

    #@3e
    move/from16 v8, p9

    #@40
    invoke-static/range {v2 .. v8}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@43
    move-result-object v10

    #@44
    .line 981
    .local v10, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-nez v10, :cond_56

    #@46
    .line 982
    const-string v2, "sendTextLge(), failed : pdu is null"

    #@48
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4b
    .line 984
    const/4 v2, 0x3

    #@4c
    move-object/from16 v0, p4

    #@4e
    invoke-static {v0, v2}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Landroid/app/PendingIntent;I)V

    #@51
    .line 1006
    :goto_51
    return-void

    #@52
    .line 976
    .end local v10           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_52
    const/4 v5, 0x0

    #@53
    goto :goto_2d

    #@54
    :cond_54
    const/4 v7, 0x0

    #@55
    goto :goto_3c

    #@56
    .line 989
    .restart local v10       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_56
    invoke-virtual {p0, p1, p2, p3, v10}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@59
    move-result-object v9

    #@5a
    .line 990
    .local v9, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    move-object/from16 v0, p4

    #@60
    move-object/from16 v1, p5

    #@62
    invoke-virtual {p0, v9, v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@65
    move-result-object v11

    #@66
    .line 992
    .local v11, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@69
    goto :goto_51

    #@6a
    .line 995
    .end local v9           #map:Ljava/util/HashMap;
    .end local v10           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .end local v11           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :cond_6a
    if-eqz p5, :cond_8c

    #@6c
    const/4 v2, 0x1

    #@6d
    :goto_6d
    invoke-static {p2, p1, p3, v2}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@70
    move-result-object v10

    #@71
    .line 997
    .restart local v10       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz v10, :cond_8e

    #@73
    .line 998
    invoke-virtual {p0, p1, p2, p3, v10}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@76
    move-result-object v9

    #@77
    .line 999
    .restart local v9       #map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@7a
    move-result-object v2

    #@7b
    move-object/from16 v0, p4

    #@7d
    move-object/from16 v1, p5

    #@7f
    invoke-virtual {p0, v9, v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@82
    move-result-object v11

    #@83
    .line 1001
    .restart local v11       #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    const-string v2, "sendTextLge(), GsmSMSDispatcher > sendTextLge > ModelConfig.COUNTRY_KR"

    #@85
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@88
    .line 1002
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPdu(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@8b
    goto :goto_51

    #@8c
    .line 995
    .end local v9           #map:Ljava/util/HashMap;
    .end local v10           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    .end local v11           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :cond_8c
    const/4 v2, 0x0

    #@8d
    goto :goto_6d

    #@8e
    .line 1004
    .restart local v10       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_8e
    const-string v2, "sendTextLge(), returned null"

    #@90
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@93
    goto :goto_51
.end method

.method protected sendTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V
    .registers 22
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"

    #@0
    .prologue
    .line 1214
    const-string v2, "sendTextMoreLge()"

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@5
    .line 1216
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@7
    const-string v3, "SKTWzone"

    #@9
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c
    move-result v2

    #@d
    const/4 v3, 0x1

    #@e
    if-ne v2, v3, :cond_14

    #@10
    .line 1217
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->wZoneDestnationNumber(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object p1

    #@14
    .line 1220
    :cond_14
    move/from16 v0, p8

    #@16
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->replyOptionDestnationNumber(Ljava/lang/String;I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    if-eqz p5, :cond_4e

    #@1c
    const/4 v5, 0x1

    #@1d
    :goto_1d
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getLine1Number()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@24
    move-result v2

    #@25
    const/4 v4, 0x1

    #@26
    if-ne v2, v4, :cond_50

    #@28
    const/4 v6, 0x0

    #@29
    :goto_29
    const/16 v2, 0x4f

    #@2b
    move/from16 v0, p9

    #@2d
    if-ne v0, v2, :cond_59

    #@2f
    const/4 v7, 0x2

    #@30
    :goto_30
    move-object v2, p2

    #@31
    move-object v4, p3

    #@32
    move/from16 v8, p9

    #@34
    invoke-static/range {v2 .. v8}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@37
    move-result-object v10

    #@38
    .line 1226
    .local v10, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    if-eqz v10, :cond_5b

    #@3a
    .line 1227
    invoke-virtual {p0, p1, p2, p3, v10}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerMapFactory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)Ljava/util/HashMap;

    #@3d
    move-result-object v9

    #@3e
    .line 1228
    .local v9, map:Ljava/util/HashMap;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->getFormat()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    move-object/from16 v0, p4

    #@44
    move-object/from16 v1, p5

    #@46
    invoke-virtual {p0, v9, v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->SmsTrackerFactory(Ljava/util/HashMap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;

    #@49
    move-result-object v11

    #@4a
    .line 1229
    .local v11, tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->sendRawPduMore(Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;)V

    #@4d
    .line 1236
    .end local v9           #map:Ljava/util/HashMap;
    .end local v11           #tracker:Lcom/android/internal/telephony/SMSDispatcher$SmsTracker;
    :goto_4d
    return-void

    #@4e
    .line 1220
    .end local v10           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_4e
    const/4 v5, 0x0

    #@4f
    goto :goto_1d

    #@50
    :cond_50
    move/from16 v0, p7

    #@52
    move-object/from16 v1, p6

    #@54
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->makeSmsHeader(ILjava/lang/String;)[B

    #@57
    move-result-object v6

    #@58
    goto :goto_29

    #@59
    :cond_59
    const/4 v7, 0x0

    #@5a
    goto :goto_30

    #@5b
    .line 1231
    .restart local v10       #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;
    :cond_5b
    const-string v2, "sendTextMoreLge(), getSubmitPdu() returned null"

    #@5d
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@60
    .line 1233
    const/4 v2, 0x3

    #@61
    move-object/from16 v0, p4

    #@63
    invoke-static {v0, v2}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Landroid/app/PendingIntent;I)V

    #@66
    goto :goto_4d
.end method

.method updateMessageWaitingIndicator(I)V
    .registers 6
    .parameter "mwi"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 656
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "KRVMSType"

    #@5
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8
    move-result v1

    #@9
    if-ne v1, v3, :cond_c

    #@b
    .line 681
    :goto_b
    return-void

    #@c
    .line 664
    :cond_c
    if-gez p1, :cond_33

    #@e
    .line 665
    const/4 p1, -0x1

    #@f
    .line 671
    :cond_f
    :goto_f
    const-string v1, "updateMessageWaitingIndicator(), GsmSMSDispatcher --> PhoneBase"

    #@11
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@14
    .line 673
    iget-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@16
    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/PhoneBase;->setVoiceMessageCount(I)V

    #@19
    .line 675
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@1b
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@1e
    move-result-object v1

    #@1f
    if-eqz v1, :cond_3a

    #@21
    .line 676
    const/16 v1, 0x14

    #@23
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->obtainMessage(I)Landroid/os/Message;

    #@26
    move-result-object v0

    #@27
    .line 677
    .local v0, onComplete:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@29
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@2c
    move-result-object v1

    #@2d
    check-cast v1, Lcom/android/internal/telephony/uicc/IccRecords;

    #@2f
    invoke-virtual {v1, v3, p1, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->setVoiceMessageWaiting(IILandroid/os/Message;)V

    #@32
    goto :goto_b

    #@33
    .line 666
    .end local v0           #onComplete:Landroid/os/Message;
    :cond_33
    const/16 v1, 0xff

    #@35
    if-le p1, v1, :cond_f

    #@37
    .line 669
    const/16 p1, 0xff

    #@39
    goto :goto_f

    #@3a
    .line 679
    :cond_3a
    const-string v1, "updateMessageWaitingIndicator(), SIM Records not found, MWI not updated"

    #@3c
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@3f
    goto :goto_b
.end method

.method public wZoneDestnationNumber(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "number"

    #@0
    .prologue
    .line 1872
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1873
    .local v0, wZoneDest:Ljava/lang/StringBuilder;
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmSMSDispatcher;->readSmsPrefixWzone(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 1874
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 1875
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    return-object v1
.end method
