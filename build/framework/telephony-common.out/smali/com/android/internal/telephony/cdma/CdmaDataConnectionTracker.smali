.class public Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;
.super Lcom/android/internal/telephony/DataConnectionTracker;
.source "CdmaDataConnectionTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker$CDMAApnObserver;
    }
.end annotation


# static fields
.field private static final DATA_CONNECTION_POOL_SIZE:I = 0x1

.field private static final INTENT_DATA_STALL_ALARM:Ljava/lang/String; = "com.android.internal.telephony.cdma-data-stall"

.field private static final INTENT_RECONNECT_ALARM:Ljava/lang/String; = "com.android.internal.telephony.cdma-reconnect"

.field private static final TIME_DELAYED_TO_RESTART_RADIO:I


# instance fields
.field protected final LOG_TAG:Ljava/lang/String;

.field private mApnObserver:Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker$CDMAApnObserver;

.field private mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

.field private mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

.field protected mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

.field private mPendingDataConnection:Lcom/android/internal/telephony/cdma/CdmaDataConnection;

.field private mPendingRestartRadio:Z

.field private mPrevPending:Z

.field private mPrevPendingcount:I

.field private myfeatureset:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 104
    const-string v0, "ro.cdma.timetoradiorestart"

    #@2
    const v1, 0xea60

    #@5
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@8
    move-result v0

    #@9
    sput v0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->TIME_DELAYED_TO_RESTART_RADIO:I

    #@b
    return-void
.end method

.method protected constructor <init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V
    .registers 8
    .parameter "p"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 146
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;-><init>(Lcom/android/internal/telephony/PhoneBase;)V

    #@6
    .line 95
    const-string v1, "CDMA"

    #@8
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->LOG_TAG:Ljava/lang/String;

    #@a
    .line 103
    iput-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingRestartRadio:Z

    #@c
    .line 107
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@e
    .line 121
    iput v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPrevPendingcount:I

    #@10
    .line 122
    iput-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPrevPending:Z

    #@12
    .line 147
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@14
    .line 150
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@16
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@18
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1b
    move-result-object v1

    #@1c
    iget-object v1, v1, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@1e
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->myfeatureset:Ljava/lang/String;

    #@20
    .line 152
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@22
    const v2, 0x42001

    #@25
    invoke-interface {v1, p0, v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@28
    .line 153
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2a
    const v2, 0x42006

    #@2d
    invoke-interface {v1, p0, v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForOffOrNotAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@30
    .line 154
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@32
    const v2, 0x42004

    #@35
    invoke-interface {v1, p0, v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForDataNetworkStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@38
    .line 155
    iget-object v1, p1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@3a
    const v2, 0x42008

    #@3d
    invoke-virtual {v1, p0, v2, v4}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->registerForVoiceCallEnded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@40
    .line 156
    iget-object v1, p1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@42
    const v2, 0x42007

    #@45
    invoke-virtual {v1, p0, v2, v4}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->registerForVoiceCallStarted(Landroid/os/Handler;ILjava/lang/Object;)V

    #@48
    .line 157
    iget-object v1, p1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@4a
    const v2, 0x42014

    #@4d
    invoke-virtual {v1, p0, v2, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->registerForDataConnectionDetached(Landroid/os/Handler;ILjava/lang/Object;)V

    #@50
    .line 158
    iget-object v1, p1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@52
    const v2, 0x4200b

    #@55
    invoke-virtual {v1, p0, v2, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->registerForRoamingOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@58
    .line 159
    iget-object v1, p1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@5a
    const v2, 0x4200c

    #@5d
    invoke-virtual {v1, p0, v2, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->registerForRoamingOff(Landroid/os/Handler;ILjava/lang/Object;)V

    #@60
    .line 160
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@62
    const v2, 0x42019

    #@65
    invoke-interface {v1, p0, v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForCdmaOtaProvision(Landroid/os/Handler;ILjava/lang/Object;)V

    #@68
    .line 161
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@6b
    move-result-object v1

    #@6c
    iget-object v2, p1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@6e
    const v3, 0x42015

    #@71
    invoke-static {v1, v2, p0, v3, v4}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getInstance(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Landroid/os/Handler;ILjava/lang/Object;)Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@74
    move-result-object v1

    #@75
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@77
    .line 165
    iput-object p0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionTracker:Landroid/os/Handler;

    #@79
    .line 168
    new-instance v1, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@7b
    invoke-direct {v1, p1}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V

    #@7e
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@80
    .line 169
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@82
    const v2, 0x42025

    #@85
    invoke-virtual {v1, p0, v2, v4}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->registerForModemProfileReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@88
    .line 173
    iget-object v1, p1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@8a
    const v2, 0x42003

    #@8d
    invoke-virtual {v1, p0, v2, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->registerForDataConnectionAttached(Landroid/os/Handler;ILjava/lang/Object;)V

    #@90
    .line 177
    new-instance v1, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker$CDMAApnObserver;

    #@92
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker$CDMAApnObserver;-><init>(Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;)V

    #@95
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mApnObserver:Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker$CDMAApnObserver;

    #@97
    .line 178
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@9a
    move-result-object v1

    #@9b
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9e
    move-result-object v1

    #@9f
    sget-object v2, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@a1
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mApnObserver:Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker$CDMAApnObserver;

    #@a3
    invoke-virtual {v1, v2, v5, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@a6
    .line 183
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a8
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@aa
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@ad
    move-result-object v1

    #@ae
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@b0
    if-eqz v1, :cond_d1

    #@b2
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b4
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@b6
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getPendinginfo()Z

    #@b9
    move-result v1

    #@ba
    if-eqz v1, :cond_d1

    #@bc
    .line 185
    const-string v1, "setupdatacall_pending"

    #@be
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@c1
    .line 186
    iput-boolean v5, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPrevPending:Z

    #@c3
    .line 189
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->obtainMessage()Landroid/os/Message;

    #@c6
    move-result-object v0

    #@c7
    .line 190
    .local v0, msg:Landroid/os/Message;
    const v1, 0x42034

    #@ca
    iput v1, v0, Landroid/os/Message;->what:I

    #@cc
    .line 191
    const-wide/16 v1, 0x7d0

    #@ce
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@d1
    .line 194
    .end local v0           #msg:Landroid/os/Message;
    :cond_d1
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->createAllDataConnectionList()V

    #@d4
    .line 195
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->broadcastMessenger()V

    #@d7
    .line 197
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionTracker:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method private createAllDataConnectionList()V
    .registers 13

    #@0
    .prologue
    const/16 v11, 0x7d0

    #@2
    const/16 v10, 0x3e8

    #@4
    const/16 v9, 0x14

    #@6
    .line 1160
    const-string v7, "ro.cdma.data_retry_config"

    #@8
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v4

    #@c
    .line 1161
    .local v4, retryConfig:Ljava/lang/String;
    const/4 v2, 0x0

    #@d
    .local v2, i:I
    :goto_d
    const/4 v7, 0x1

    #@e
    if-ge v2, v7, :cond_ae

    #@10
    .line 1162
    new-instance v5, Lcom/android/internal/telephony/RetryManager;

    #@12
    invoke-direct {v5}, Lcom/android/internal/telephony/RetryManager;-><init>()V

    #@15
    .line 1163
    .local v5, rm:Lcom/android/internal/telephony/RetryManager;
    invoke-virtual {v5, v4}, Lcom/android/internal/telephony/RetryManager;->configure(Ljava/lang/String;)Z

    #@18
    move-result v7

    #@19
    if-nez v7, :cond_37

    #@1b
    .line 1166
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1d
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1f
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@22
    move-result-object v7

    #@23
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_RETRY_CONFIG_VZW:Z

    #@25
    if-eqz v7, :cond_7a

    #@27
    .line 1167
    const-string v7, "default_randomization=0,max_retries=infinite,5000,10000,20000:100,40000:100,80000:100,120000:100,180000:100,240000:100"

    #@29
    invoke-virtual {v5, v7}, Lcom/android/internal/telephony/RetryManager;->configure(Ljava/lang/String;)Z

    #@2c
    move-result v7

    #@2d
    if-nez v7, :cond_37

    #@2f
    .line 1169
    const-string v7, "Could not configure using VZW_DATA_RETRY_CONFIG=default_randomization=0,max_retries=infinite,5000,10000,20000:100,40000:100,80000:100,120000:100,180000:100,240000:100"

    #@31
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@34
    .line 1171
    invoke-virtual {v5, v9, v11, v10}, Lcom/android/internal/telephony/RetryManager;->configure(III)Z

    #@37
    .line 1185
    :cond_37
    :goto_37
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUniqueIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

    #@39
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    #@3c
    move-result v3

    #@3d
    .line 1186
    .local v3, id:I
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@3f
    invoke-static {v7, v3, v5, p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnection;->makeDataConnection(Lcom/android/internal/telephony/cdma/CDMAPhone;ILcom/android/internal/telephony/RetryManager;Lcom/android/internal/telephony/DataConnectionTracker;)Lcom/android/internal/telephony/cdma/CdmaDataConnection;

    #@42
    move-result-object v0

    #@43
    .line 1187
    .local v0, dataConn:Lcom/android/internal/telephony/cdma/CdmaDataConnection;
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@45
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@48
    move-result-object v8

    #@49
    invoke-virtual {v7, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4c
    .line 1188
    new-instance v1, Lcom/android/internal/telephony/DataConnectionAc;

    #@4e
    const-string v7, "CDMA"

    #@50
    invoke-direct {v1, v0, v7}, Lcom/android/internal/telephony/DataConnectionAc;-><init>(Lcom/android/internal/telephony/DataConnection;Ljava/lang/String;)V

    #@53
    .line 1189
    .local v1, dcac:Lcom/android/internal/telephony/DataConnectionAc;
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@55
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@58
    move-result-object v7

    #@59
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnection;->getHandler()Landroid/os/Handler;

    #@5c
    move-result-object v8

    #@5d
    invoke-virtual {v1, v7, p0, v8}, Lcom/android/internal/telephony/DataConnectionAc;->fullyConnectSync(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Handler;)I

    #@60
    move-result v6

    #@61
    .line 1190
    .local v6, status:I
    if-nez v6, :cond_8b

    #@63
    .line 1191
    const-string v7, "Fully connected"

    #@65
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@68
    .line 1192
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionAsyncChannels:Ljava/util/HashMap;

    #@6a
    iget-object v8, v1, Lcom/android/internal/telephony/DataConnectionAc;->dataConnection:Lcom/android/internal/telephony/DataConnection;

    #@6c
    invoke-virtual {v8}, Lcom/android/internal/telephony/DataConnection;->getDataConnectionId()I

    #@6f
    move-result v8

    #@70
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@73
    move-result-object v8

    #@74
    invoke-virtual {v7, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@77
    .line 1161
    :goto_77
    add-int/lit8 v2, v2, 0x1

    #@79
    goto :goto_d

    #@7a
    .line 1175
    .end local v0           #dataConn:Lcom/android/internal/telephony/cdma/CdmaDataConnection;
    .end local v1           #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    .end local v3           #id:I
    .end local v6           #status:I
    :cond_7a
    const-string v7, "default_randomization=2000,5000,10000,20000,40000,80000:5000,160000:5000,320000:5000,640000:5000,1280000:5000,1800000:5000"

    #@7c
    invoke-virtual {v5, v7}, Lcom/android/internal/telephony/RetryManager;->configure(Ljava/lang/String;)Z

    #@7f
    move-result v7

    #@80
    if-nez v7, :cond_37

    #@82
    .line 1177
    const-string v7, "Could not configure using DEFAULT_DATA_RETRY_CONFIG=default_randomization=2000,5000,10000,20000,40000,80000:5000,160000:5000,320000:5000,640000:5000,1280000:5000,1800000:5000"

    #@84
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@87
    .line 1179
    invoke-virtual {v5, v9, v11, v10}, Lcom/android/internal/telephony/RetryManager;->configure(III)Z

    #@8a
    goto :goto_37

    #@8b
    .line 1194
    .restart local v0       #dataConn:Lcom/android/internal/telephony/cdma/CdmaDataConnection;
    .restart local v1       #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    .restart local v3       #id:I
    .restart local v6       #status:I
    :cond_8b
    new-instance v7, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v8, "Could not connect to dcac.dataConnection="

    #@92
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v7

    #@96
    iget-object v8, v1, Lcom/android/internal/telephony/DataConnectionAc;->dataConnection:Lcom/android/internal/telephony/DataConnection;

    #@98
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v7

    #@9c
    const-string v8, " status="

    #@9e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v7

    #@a2
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v7

    #@a6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v7

    #@aa
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@ad
    goto :goto_77

    #@ae
    .line 1199
    .end local v0           #dataConn:Lcom/android/internal/telephony/cdma/CdmaDataConnection;
    .end local v1           #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    .end local v3           #id:I
    .end local v5           #rm:Lcom/android/internal/telephony/RetryManager;
    .end local v6           #status:I
    :cond_ae
    return-void
.end method

.method private destroyAllDataConnectionList()V
    .registers 2

    #@0
    .prologue
    .line 1202
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1203
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@6
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@9
    .line 1205
    :cond_9
    return-void
.end method

.method private findDataConnectionAcByCid(I)Lcom/android/internal/telephony/DataConnectionAc;
    .registers 5
    .parameter "cid"

    #@0
    .prologue
    .line 1278
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionAsyncChannels:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .local v1, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_1d

    #@10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Lcom/android/internal/telephony/DataConnectionAc;

    #@16
    .line 1279
    .local v0, dcac:Lcom/android/internal/telephony/DataConnectionAc;
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionAc;->getCidSync()I

    #@19
    move-result v2

    #@1a
    if-ne v2, p1, :cond_a

    #@1c
    .line 1283
    .end local v0           #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_1c
.end method

.method private findFreeDataConnection()Lcom/android/internal/telephony/cdma/CdmaDataConnection;
    .registers 4

    #@0
    .prologue
    .line 564
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionAsyncChannels:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .local v1, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_26

    #@10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Lcom/android/internal/telephony/DataConnectionAc;

    #@16
    .line 565
    .local v0, dcac:Lcom/android/internal/telephony/DataConnectionAc;
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionAc;->isInactiveSync()Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_a

    #@1c
    .line 566
    const-string v2, "found free CdmaDataConnection"

    #@1e
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@21
    .line 567
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionAc;->dataConnection:Lcom/android/internal/telephony/DataConnection;

    #@23
    check-cast v2, Lcom/android/internal/telephony/cdma/CdmaDataConnection;

    #@25
    .line 571
    .end local v0           #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    :goto_25
    return-object v2

    #@26
    .line 570
    :cond_26
    const-string v2, "NO free CdmaDataConnection"

    #@28
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@2b
    .line 571
    const/4 v2, 0x0

    #@2c
    goto :goto_25
.end method

.method private getSelectedApnKey()Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 576
    const-string v0, "content://telephony/carriers/preferapn"

    #@4
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v1

    #@8
    .line 577
    .local v1, PREFERAPN_URI:Landroid/net/Uri;
    const/4 v8, 0x0

    #@9
    .line 579
    .local v8, key:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v0

    #@13
    const/4 v2, 0x1

    #@14
    new-array v2, v2, [Ljava/lang/String;

    #@16
    const-string v4, "_id"

    #@18
    aput-object v4, v2, v5

    #@1a
    const-string v5, "name ASC"

    #@1c
    move-object v4, v3

    #@1d
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@20
    move-result-object v6

    #@21
    .line 583
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_34

    #@23
    .line 585
    :try_start_23
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@26
    move-result v0

    #@27
    if-lez v0, :cond_31

    #@29
    .line 586
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@2c
    .line 587
    const/4 v0, 0x0

    #@2d
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_30
    .catchall {:try_start_23 .. :try_end_30} :catchall_51
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_30} :catch_35

    #@30
    move-result-object v8

    #@31
    .line 593
    :cond_31
    :goto_31
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@34
    .line 597
    :cond_34
    return-object v8

    #@35
    .line 589
    :catch_35
    move-exception v7

    #@36
    .line 591
    .local v7, e:Ljava/lang/Exception;
    :try_start_36
    new-instance v0, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v2, " An error occurred  on getSelectedApnKey : "

    #@3d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V
    :try_end_50
    .catchall {:try_start_36 .. :try_end_50} :catchall_51

    #@50
    goto :goto_31

    #@51
    .line 593
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_51
    move-exception v0

    #@52
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@55
    throw v0
.end method

.method private notifyDefaultData(Ljava/lang/String;)V
    .registers 4
    .parameter "reason"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 713
    sget-object v0, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->setState(Lcom/android/internal/telephony/DctConstants$State;)V

    #@6
    .line 714
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@9
    .line 715
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->startNetStatPoll()V

    #@c
    .line 716
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->startDataStallAlarm(Z)V

    #@f
    .line 717
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@11
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Lcom/android/internal/telephony/DataConnection;

    #@1b
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnection;->resetRetryCount()V

    #@1e
    .line 718
    return-void
.end method

.method private notifyNoData(Lcom/android/internal/telephony/DataConnection$FailCause;)V
    .registers 3
    .parameter "lastFailCauseCode"

    #@0
    .prologue
    .line 796
    sget-object v0, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->setState(Lcom/android/internal/telephony/DctConstants$State;)V

    #@5
    .line 797
    const/4 v0, 0x0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@9
    .line 798
    return-void
.end method

.method private onCdmaDataDetached()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1208
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@3
    sget-object v2, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@5
    if-ne v1, v2, :cond_13

    #@7
    .line 1209
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->startNetStatPoll()V

    #@a
    .line 1210
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->startDataStallAlarm(Z)V

    #@d
    .line 1211
    const-string v1, "cdmaDataDetached"

    #@f
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@12
    .line 1224
    :goto_12
    return-void

    #@13
    .line 1213
    :cond_13
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@15
    sget-object v2, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@17
    if-ne v1, v2, :cond_5c

    #@19
    .line 1214
    const-string v1, "cdmaDataDetached"

    #@1b
    invoke-virtual {p0, v4, v1, v4}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpConnection(ZLjava/lang/String;Z)V

    #@1e
    .line 1215
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@20
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    move-result-object v1

    #@28
    check-cast v1, Lcom/android/internal/telephony/DataConnection;

    #@2a
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->resetRetryCount()V

    #@2d
    .line 1217
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2f
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getCellLocation()Landroid/telephony/CellLocation;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Landroid/telephony/cdma/CdmaCellLocation;

    #@35
    move-object v0, v1

    #@36
    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    #@38
    .line 1218
    .local v0, loc:Landroid/telephony/cdma/CdmaCellLocation;
    const v2, 0xc3be

    #@3b
    const/4 v1, 0x2

    #@3c
    new-array v3, v1, [Ljava/lang/Object;

    #@3e
    if-eqz v0, :cond_62

    #@40
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    #@43
    move-result v1

    #@44
    :goto_44
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@47
    move-result-object v1

    #@48
    aput-object v1, v3, v4

    #@4a
    const/4 v1, 0x1

    #@4b
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@52
    move-result v4

    #@53
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@56
    move-result-object v4

    #@57
    aput-object v4, v3, v1

    #@59
    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@5c
    .line 1222
    .end local v0           #loc:Landroid/telephony/cdma/CdmaCellLocation;
    :cond_5c
    const-string v1, "cdmaDataDetached"

    #@5e
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->trySetupData(Ljava/lang/String;)Z

    #@61
    goto :goto_12

    #@62
    .line 1218
    .restart local v0       #loc:Landroid/telephony/cdma/CdmaCellLocation;
    :cond_62
    const/4 v1, -0x1

    #@63
    goto :goto_44
.end method

.method private onCdmaOtaProvision(Landroid/os/AsyncResult;)V
    .registers 6
    .parameter "ar"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1227
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3
    if-eqz v1, :cond_17

    #@5
    .line 1228
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@7
    check-cast v1, [I

    #@9
    move-object v0, v1

    #@a
    check-cast v0, [I

    #@c
    .line 1229
    .local v0, otaPrivision:[I
    if-eqz v0, :cond_17

    #@e
    array-length v1, v0

    #@f
    const/4 v2, 0x1

    #@10
    if-le v1, v2, :cond_17

    #@12
    .line 1230
    aget v1, v0, v3

    #@14
    packed-switch v1, :pswitch_data_28

    #@17
    .line 1240
    .end local v0           #otaPrivision:[I
    :cond_17
    :goto_17
    :pswitch_17
    return-void

    #@18
    .line 1233
    .restart local v0       #otaPrivision:[I
    :pswitch_18
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@1a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Lcom/android/internal/telephony/DataConnection;

    #@24
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->resetRetryCount()V

    #@27
    goto :goto_17

    #@28
    .line 1230
    :pswitch_data_28
    .packed-switch 0x8
        :pswitch_18
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method private onModemDataProfileReady()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1258
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@3
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@5
    if-ne v0, v1, :cond_b

    #@7
    .line 1259
    const/4 v0, 0x0

    #@8
    invoke-virtual {p0, v2, v0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpConnection(ZLjava/lang/String;Z)V

    #@b
    .line 1262
    :cond_b
    const-string v0, "CDMA"

    #@d
    const-string v1, "OMH: onModemDataProfileReady(): Setting up data call"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 1263
    const v0, 0x42003

    #@15
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@1c
    .line 1264
    return-void
.end method

.method private onPendingTimeout()V
    .registers 4

    #@0
    .prologue
    .line 1467
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@4
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@7
    move-result-object v1

    #@8
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@a
    if-eqz v1, :cond_48

    #@c
    .line 1470
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@e
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getPendinginfo()Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_49

    #@16
    iget v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPrevPendingcount:I

    #@18
    const/16 v2, 0x3c

    #@1a
    if-ge v1, v2, :cond_49

    #@1c
    .line 1472
    iget v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPrevPendingcount:I

    #@1e
    add-int/lit8 v1, v1, 0x1

    #@20
    iput v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPrevPendingcount:I

    #@22
    .line 1473
    new-instance v1, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v2, "Still Data pending!!! "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    iget v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPrevPendingcount:I

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@3a
    .line 1475
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->obtainMessage()Landroid/os/Message;

    #@3d
    move-result-object v0

    #@3e
    .line 1476
    .local v0, msg:Landroid/os/Message;
    const v1, 0x42034

    #@41
    iput v1, v0, Landroid/os/Message;->what:I

    #@43
    .line 1477
    const-wide/16 v1, 0x7d0

    #@45
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@48
    .line 1485
    .end local v0           #msg:Landroid/os/Message;
    :cond_48
    :goto_48
    return-void

    #@49
    .line 1481
    :cond_49
    const/4 v1, 0x0

    #@4a
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPrevPending:Z

    #@4c
    .line 1482
    const-string v1, "pending off"

    #@4e
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->trySetupData(Ljava/lang/String;)Z

    #@51
    goto :goto_48
.end method

.method private onRestartRadio()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1243
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingRestartRadio:Z

    #@3
    if-eqz v0, :cond_14

    #@5
    .line 1244
    const-string v0, "************TURN OFF RADIO**************"

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@a
    .line 1245
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@c
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    const/4 v1, 0x0

    #@f
    invoke-interface {v0, v2, v1}, Lcom/android/internal/telephony/CommandsInterface;->setRadioPower(ZLandroid/os/Message;)V

    #@12
    .line 1253
    iput-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingRestartRadio:Z

    #@14
    .line 1255
    :cond_14
    return-void
.end method

.method private reconnectAfterFail(Lcom/android/internal/telephony/DataConnection$FailCause;Ljava/lang/String;I)V
    .registers 8
    .parameter "lastFailCauseCode"
    .parameter "reason"
    .parameter "retryOverride"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 756
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@3
    sget-object v2, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@5
    if-ne v1, v2, :cond_37

    #@7
    .line 763
    move v0, p3

    #@8
    .line 764
    .local v0, nextReconnectDelay:I
    if-gez v0, :cond_29

    #@a
    .line 765
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Lcom/android/internal/telephony/DataConnection;

    #@16
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->getRetryTimer()I

    #@19
    move-result v0

    #@1a
    .line 766
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@1c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Lcom/android/internal/telephony/DataConnection;

    #@26
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->increaseRetryCount()V

    #@29
    .line 768
    :cond_29
    invoke-direct {p0, v0, p2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->startAlarmForReconnect(ILjava/lang/String;)V

    #@2c
    .line 770
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->shouldPostNotification(Lcom/android/internal/telephony/DataConnection$FailCause;)Z

    #@2f
    move-result v1

    #@30
    if-nez v1, :cond_38

    #@32
    .line 771
    const-string v1, "NOT Posting Data Connection Unavailable notification -- likely transient error"

    #@34
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@37
    .line 777
    .end local v0           #nextReconnectDelay:I
    :cond_37
    :goto_37
    return-void

    #@38
    .line 774
    .restart local v0       #nextReconnectDelay:I
    :cond_38
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyNoData(Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@3b
    goto :goto_37
.end method

.method private retryAfterDisconnected(Ljava/lang/String;)Z
    .registers 4
    .parameter "reason"

    #@0
    .prologue
    .line 747
    const/4 v0, 0x1

    #@1
    .line 749
    .local v0, retry:Z
    const-string v1, "radioTurnedOff"

    #@3
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_a

    #@9
    .line 750
    const/4 v0, 0x0

    #@a
    .line 752
    :cond_a
    return v0
.end method

.method private setupData(Ljava/lang/String;)Z
    .registers 24
    .parameter "reason"

    #@0
    .prologue
    .line 602
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->findFreeDataConnection()Lcom/android/internal/telephony/cdma/CdmaDataConnection;

    #@3
    move-result-object v15

    #@4
    .line 604
    .local v15, conn:Lcom/android/internal/telephony/cdma/CdmaDataConnection;
    if-nez v15, :cond_f

    #@6
    .line 605
    const-string v2, "setupData: No free CdmaDataConnection found!"

    #@8
    move-object/from16 v0, p0

    #@a
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@d
    .line 606
    const/4 v2, 0x0

    #@e
    .line 709
    :goto_e
    return v2

    #@f
    .line 610
    :cond_f
    move-object/from16 v0, p0

    #@11
    iput-object v15, v0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingDataConnection:Lcom/android/internal/telephony/cdma/CdmaDataConnection;

    #@13
    .line 612
    const-string v2, "persist.telephony.cdma.protocol"

    #@15
    const-string v3, "IP"

    #@17
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v17

    #@1b
    .line 613
    .local v17, ipProto:Ljava/lang/String;
    const-string v2, "persist.telephony.cdma.rproto"

    #@1d
    const-string v3, "IP"

    #@1f
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v21

    #@23
    .line 615
    .local v21, roamingIpProto:Ljava/lang/String;
    move-object/from16 v0, p0

    #@25
    iget-boolean v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@27
    if-eqz v2, :cond_74

    #@29
    .line 616
    const/4 v2, 0x5

    #@2a
    new-array v9, v2, [Ljava/lang/String;

    #@2c
    const/4 v2, 0x0

    #@2d
    const-string v3, "default"

    #@2f
    aput-object v3, v9, v2

    #@31
    const/4 v2, 0x1

    #@32
    const-string v3, "mms"

    #@34
    aput-object v3, v9, v2

    #@36
    const/4 v2, 0x2

    #@37
    const-string v3, "supl"

    #@39
    aput-object v3, v9, v2

    #@3b
    const/4 v2, 0x3

    #@3c
    const-string v3, "hipri"

    #@3e
    aput-object v3, v9, v2

    #@40
    const/4 v2, 0x4

    #@41
    const-string v3, "dun"

    #@43
    aput-object v3, v9, v2

    #@45
    .line 617
    .local v9, types:[Ljava/lang/String;
    new-instance v2, Lcom/android/internal/telephony/cdma/DataProfileCdma;

    #@47
    const/4 v3, 0x0

    #@48
    const/4 v4, 0x0

    #@49
    move-object/from16 v0, p0

    #@4b
    iget-object v5, v0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_apn:Ljava/lang/String;

    #@4d
    move-object/from16 v0, p0

    #@4f
    iget-object v6, v0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_user:Ljava/lang/String;

    #@51
    move-object/from16 v0, p0

    #@53
    iget-object v7, v0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_password:Ljava/lang/String;

    #@55
    move-object/from16 v0, p0

    #@57
    iget v8, v0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_authType:I

    #@59
    const-string v10, "IP"

    #@5b
    const-string v11, "IP"

    #@5d
    const/4 v12, 0x0

    #@5e
    invoke-direct/range {v2 .. v12}, Lcom/android/internal/telephony/cdma/DataProfileCdma;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    #@61
    move-object/from16 v0, p0

    #@63
    iput-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@65
    .line 686
    .end local v9           #types:[Ljava/lang/String;
    :goto_65
    move-object/from16 v0, p0

    #@67
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@69
    if-nez v2, :cond_1db

    #@6b
    .line 687
    const-string v2, "mActiveApn is null, unable to initiate data call"

    #@6d
    move-object/from16 v0, p0

    #@6f
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@72
    .line 688
    const/4 v2, 0x0

    #@73
    goto :goto_e

    #@74
    .line 620
    :cond_74
    move-object/from16 v0, p0

    #@76
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@78
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7a
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@7d
    sget-boolean v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_KDDI_SUPPORT_DYNAMIC_APN_NAI_SETTING_FOR_CPA:Z

    #@7f
    const/4 v3, 0x1

    #@80
    if-ne v2, v3, :cond_1c9

    #@82
    .line 621
    const/16 v19, 0x0

    #@84
    .line 622
    .local v19, mSelectedKey:Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->getSelectedApnKey()Ljava/lang/String;

    #@87
    move-result-object v19

    #@88
    .line 624
    if-nez v19, :cond_b7

    #@8a
    .line 626
    move-object/from16 v0, p0

    #@8c
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@8e
    move-object/from16 v0, p0

    #@90
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mRequestedApnType:Ljava/lang/String;

    #@92
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->getDataProfile(Ljava/lang/String;)Lcom/android/internal/telephony/DataProfile;

    #@95
    move-result-object v2

    #@96
    move-object/from16 v0, p0

    #@98
    iput-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@9a
    .line 627
    new-instance v2, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v3, "mSelectedKey==null so Cause mActiveApn is "

    #@a1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v2

    #@a5
    move-object/from16 v0, p0

    #@a7
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@a9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v2

    #@ad
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v2

    #@b1
    move-object/from16 v0, p0

    #@b3
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@b6
    goto :goto_65

    #@b7
    .line 631
    :cond_b7
    move-object/from16 v0, p0

    #@b9
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@bb
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@be
    move-result-object v2

    #@bf
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c2
    move-result-object v2

    #@c3
    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@c5
    const/4 v4, 0x5

    #@c6
    new-array v4, v4, [Ljava/lang/String;

    #@c8
    const/4 v10, 0x0

    #@c9
    const-string v11, "_id"

    #@cb
    aput-object v11, v4, v10

    #@cd
    const/4 v10, 0x1

    #@ce
    const-string v11, "apn"

    #@d0
    aput-object v11, v4, v10

    #@d2
    const/4 v10, 0x2

    #@d3
    const-string v11, "user"

    #@d5
    aput-object v11, v4, v10

    #@d7
    const/4 v10, 0x3

    #@d8
    const-string v11, "password"

    #@da
    aput-object v11, v4, v10

    #@dc
    const/4 v10, 0x4

    #@dd
    const-string v11, "authtype"

    #@df
    aput-object v11, v4, v10

    #@e1
    const/4 v5, 0x0

    #@e2
    const/4 v6, 0x0

    #@e3
    const-string v7, "name ASC"

    #@e5
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e8
    move-result-object v16

    #@e9
    .line 634
    .local v16, cursor:Landroid/database/Cursor;
    const/4 v14, 0x0

    #@ea
    .line 635
    .local v14, canSetPreferApn:Z
    const/4 v5, 0x0

    #@eb
    .line 636
    .local v5, apn:Ljava/lang/String;
    const/4 v6, 0x0

    #@ec
    .line 637
    .local v6, user:Ljava/lang/String;
    const/4 v7, 0x0

    #@ed
    .line 638
    .local v7, password:Ljava/lang/String;
    const/4 v13, 0x0

    #@ee
    .line 639
    .local v13, authtype:Ljava/lang/String;
    if-eqz v16, :cond_166

    #@f0
    .line 640
    const/4 v14, 0x1

    #@f1
    .line 645
    :goto_f1
    if-eqz v14, :cond_131

    #@f3
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    #@f6
    move-result v2

    #@f7
    if-lez v2, :cond_131

    #@f9
    .line 647
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    #@fc
    .line 648
    :goto_fc
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->isAfterLast()Z

    #@ff
    move-result v2

    #@100
    if-nez v2, :cond_131

    #@102
    .line 649
    const/4 v2, 0x0

    #@103
    move-object/from16 v0, v16

    #@105
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@108
    move-result-object v18

    #@109
    .line 650
    .local v18, key:Ljava/lang/String;
    if-eqz v19, :cond_168

    #@10b
    move-object/from16 v0, v19

    #@10d
    move-object/from16 v1, v18

    #@10f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@112
    move-result v2

    #@113
    if-eqz v2, :cond_168

    #@115
    .line 651
    const/4 v2, 0x1

    #@116
    move-object/from16 v0, v16

    #@118
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@11b
    move-result-object v5

    #@11c
    .line 652
    const/4 v2, 0x2

    #@11d
    move-object/from16 v0, v16

    #@11f
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@122
    move-result-object v6

    #@123
    .line 653
    const/4 v2, 0x3

    #@124
    move-object/from16 v0, v16

    #@126
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@129
    move-result-object v7

    #@12a
    .line 654
    const/4 v2, 0x4

    #@12b
    move-object/from16 v0, v16

    #@12d
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@130
    move-result-object v13

    #@131
    .line 661
    .end local v18           #key:Ljava/lang/String;
    :cond_131
    if-eqz v16, :cond_136

    #@133
    .line 662
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    #@136
    .line 665
    :cond_136
    if-nez v14, :cond_16c

    #@138
    .line 666
    move-object/from16 v0, p0

    #@13a
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@13c
    move-object/from16 v0, p0

    #@13e
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mRequestedApnType:Ljava/lang/String;

    #@140
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->getDataProfile(Ljava/lang/String;)Lcom/android/internal/telephony/DataProfile;

    #@143
    move-result-object v2

    #@144
    move-object/from16 v0, p0

    #@146
    iput-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@148
    .line 667
    new-instance v2, Ljava/lang/StringBuilder;

    #@14a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14d
    const-string v3, "canSetPreferApn==false so Cause mActiveApn is "

    #@14f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v2

    #@153
    move-object/from16 v0, p0

    #@155
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@157
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v2

    #@15b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15e
    move-result-object v2

    #@15f
    move-object/from16 v0, p0

    #@161
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@164
    goto/16 :goto_65

    #@166
    .line 642
    :cond_166
    const/4 v14, 0x0

    #@167
    goto :goto_f1

    #@168
    .line 657
    .restart local v18       #key:Ljava/lang/String;
    :cond_168
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    #@16b
    goto :goto_fc

    #@16c
    .line 671
    .end local v18           #key:Ljava/lang/String;
    :cond_16c
    const/4 v8, 0x0

    #@16d
    .line 672
    .local v8, authType:I
    move-object/from16 v0, p0

    #@16f
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@171
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->getActiveApnTypes()[Ljava/lang/String;

    #@174
    move-result-object v9

    #@175
    .line 673
    .restart local v9       #types:[Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@178
    move-result v2

    #@179
    if-nez v2, :cond_17f

    #@17b
    .line 674
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@17e
    move-result v8

    #@17f
    .line 675
    :cond_17f
    new-instance v2, Lcom/android/internal/telephony/cdma/DataProfileCdma;

    #@181
    const/4 v3, 0x0

    #@182
    const/4 v4, 0x0

    #@183
    const-string v10, "IP"

    #@185
    const-string v11, "IP"

    #@187
    const/4 v12, 0x0

    #@188
    invoke-direct/range {v2 .. v12}, Lcom/android/internal/telephony/cdma/DataProfileCdma;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    #@18b
    move-object/from16 v0, p0

    #@18d
    iput-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@18f
    .line 677
    new-instance v2, Ljava/lang/StringBuilder;

    #@191
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@194
    const-string v3, "##### cpa  user="

    #@196
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@199
    move-result-object v2

    #@19a
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v2

    #@19e
    const-string v3, " password="

    #@1a0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v2

    #@1a4
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v2

    #@1a8
    const-string v3, " apn=["

    #@1aa
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ad
    move-result-object v2

    #@1ae
    move-object/from16 v0, p0

    #@1b0
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@1b2
    iget-object v3, v3, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@1b4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v2

    #@1b8
    const-string v3, "]"

    #@1ba
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bd
    move-result-object v2

    #@1be
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c1
    move-result-object v2

    #@1c2
    move-object/from16 v0, p0

    #@1c4
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@1c7
    goto/16 :goto_65

    #@1c9
    .line 684
    .end local v5           #apn:Ljava/lang/String;
    .end local v6           #user:Ljava/lang/String;
    .end local v7           #password:Ljava/lang/String;
    .end local v8           #authType:I
    .end local v9           #types:[Ljava/lang/String;
    .end local v13           #authtype:Ljava/lang/String;
    .end local v14           #canSetPreferApn:Z
    .end local v16           #cursor:Landroid/database/Cursor;
    .end local v19           #mSelectedKey:Ljava/lang/String;
    :cond_1c9
    move-object/from16 v0, p0

    #@1cb
    iget-object v2, v0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@1cd
    move-object/from16 v0, p0

    #@1cf
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mRequestedApnType:Ljava/lang/String;

    #@1d1
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->getDataProfile(Ljava/lang/String;)Lcom/android/internal/telephony/DataProfile;

    #@1d4
    move-result-object v2

    #@1d5
    move-object/from16 v0, p0

    #@1d7
    iput-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@1d9
    goto/16 :goto_65

    #@1db
    .line 691
    :cond_1db
    new-instance v2, Ljava/lang/StringBuilder;

    #@1dd
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e0
    const-string v3, "call conn.bringUp mActiveApn="

    #@1e2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e5
    move-result-object v2

    #@1e6
    move-object/from16 v0, p0

    #@1e8
    iget-object v3, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@1ea
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1ed
    move-result-object v2

    #@1ee
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f1
    move-result-object v2

    #@1f2
    move-object/from16 v0, p0

    #@1f4
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@1f7
    .line 694
    move-object/from16 v0, p0

    #@1f9
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1fb
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1fd
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@200
    move-result-object v2

    #@201
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

    #@203
    if-eqz v2, :cond_21d

    #@205
    .line 696
    move-object/from16 v0, p0

    #@207
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@209
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@20c
    move-result-object v2

    #@20d
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@210
    move-result v2

    #@211
    if-nez v2, :cond_21d

    #@213
    .line 697
    const-string v2, "RADIO_TECHNOLOGY_UNKNOWN setupData is false"

    #@215
    move-object/from16 v0, p0

    #@217
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@21a
    .line 698
    const/4 v2, 0x0

    #@21b
    goto/16 :goto_e

    #@21d
    .line 702
    :cond_21d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->obtainMessage()Landroid/os/Message;

    #@220
    move-result-object v20

    #@221
    .line 703
    .local v20, msg:Landroid/os/Message;
    const v2, 0x42000

    #@224
    move-object/from16 v0, v20

    #@226
    iput v2, v0, Landroid/os/Message;->what:I

    #@228
    .line 704
    move-object/from16 v0, p1

    #@22a
    move-object/from16 v1, v20

    #@22c
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@22e
    .line 705
    move-object/from16 v0, p0

    #@230
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@232
    move-object/from16 v0, v20

    #@234
    invoke-virtual {v15, v0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnection;->bringUp(Landroid/os/Message;Lcom/android/internal/telephony/DataProfile;)V

    #@237
    .line 707
    sget-object v2, Lcom/android/internal/telephony/DctConstants$State;->INITING:Lcom/android/internal/telephony/DctConstants$State;

    #@239
    move-object/from16 v0, p0

    #@23b
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->setState(Lcom/android/internal/telephony/DctConstants$State;)V

    #@23e
    .line 708
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@241
    .line 709
    const/4 v2, 0x1

    #@242
    goto/16 :goto_e
.end method

.method private shouldPostNotification(Lcom/android/internal/telephony/DataConnection$FailCause;)Z
    .registers 3
    .parameter "cause"

    #@0
    .prologue
    .line 736
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2
    if-eq p1, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private startAlarmForReconnect(ILjava/lang/String;)V
    .registers 10
    .parameter "delay"
    .parameter "reason"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 781
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "Data Connection activate failed. Scheduling next attempt for "

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    div-int/lit16 v3, p1, 0x3e8

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    const-string v3, "s"

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@1f
    .line 784
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@21
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@24
    move-result-object v2

    #@25
    const-string v3, "alarm"

    #@27
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, Landroid/app/AlarmManager;

    #@2d
    .line 786
    .local v0, am:Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    #@2f
    const-string v2, "com.android.internal.telephony.cdma-reconnect"

    #@31
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@34
    .line 787
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "reconnect_alarm_extra_reason"

    #@36
    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@39
    .line 788
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3b
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@3e
    move-result-object v2

    #@3f
    invoke-static {v2, v4, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@42
    move-result-object v2

    #@43
    iput-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntent:Landroid/app/PendingIntent;

    #@45
    .line 790
    const/4 v2, 0x2

    #@46
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@49
    move-result-wide v3

    #@4a
    int-to-long v5, p1

    #@4b
    add-long/2addr v3, v5

    #@4c
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntent:Landroid/app/PendingIntent;

    #@4e
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@51
    .line 793
    return-void
.end method

.method private startDelayedRetry(Lcom/android/internal/telephony/DataConnection$FailCause;Ljava/lang/String;I)V
    .registers 4
    .parameter "cause"
    .parameter "reason"
    .parameter "retryOverride"

    #@0
    .prologue
    .line 1379
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyNoData(Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@3
    .line 1380
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->reconnectAfterFail(Lcom/android/internal/telephony/DataConnection$FailCause;Ljava/lang/String;I)V

    #@6
    .line 1381
    return-void
.end method

.method private triggerDataSetup(Ljava/lang/String;)V
    .registers 4
    .parameter "reason"

    #@0
    .prologue
    .line 865
    const-string v0, "onRecordsLoaded(): calling readDataProfilesFromModem()"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@5
    .line 867
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@7
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->loadProfiles()V

    #@a
    .line 869
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@c
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@e
    if-ne v0, v1, :cond_14

    #@10
    .line 870
    const/4 v0, 0x0

    #@11
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@14
    .line 873
    :cond_14
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@16
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->isOmhEnabled()Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_3c

    #@1c
    .line 874
    new-instance v0, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v1, "onRecordsLoaded(): Profiles are ready - trigger setup_data with reason: "

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@32
    .line 876
    const v0, 0x42003

    #@35
    invoke-virtual {p0, v0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@3c
    .line 878
    :cond_3c
    return-void
.end method

.method private trySetupData(Ljava/lang/String;)Z
    .registers 8
    .parameter "reason"

    #@0
    .prologue
    .line 474
    new-instance v4, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v5, "***trySetupData due to "

    #@7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v5

    #@b
    if-nez p1, :cond_34

    #@d
    const-string v4, "(unspecified)"

    #@f
    :goto_f
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@1a
    .line 476
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1c
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getSimulatedRadioControl()Lcom/android/internal/telephony/test/SimulatedRadioControl;

    #@1f
    move-result-object v4

    #@20
    if-eqz v4, :cond_36

    #@22
    .line 479
    sget-object v4, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@24
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->setState(Lcom/android/internal/telephony/DctConstants$State;)V

    #@27
    .line 480
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@2a
    .line 481
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@2d
    .line 483
    const-string v4, "(fix?) We\'re on the simulator; assuming data is connected"

    #@2f
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@32
    .line 484
    const/4 v2, 0x1

    #@33
    .line 502
    :goto_33
    return v2

    #@34
    :cond_34
    move-object v4, p1

    #@35
    .line 474
    goto :goto_f

    #@36
    .line 487
    :cond_36
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@38
    iget-object v4, v4, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@3a
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getCurrentDataConnectionState()I

    #@3d
    move-result v1

    #@3e
    .line 488
    .local v1, psState:I
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@40
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@47
    move-result v3

    #@48
    .line 489
    .local v3, roaming:Z
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4a
    iget-object v4, v4, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@4c
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getDesiredPowerState()Z

    #@4f
    move-result v0

    #@50
    .line 490
    .local v0, desiredPowerState:Z
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@52
    sget-object v5, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@54
    if-eq v4, v5, :cond_5c

    #@56
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@58
    sget-object v5, Lcom/android/internal/telephony/DctConstants$State;->SCANNING:Lcom/android/internal/telephony/DctConstants$State;

    #@5a
    if-ne v4, v5, :cond_76

    #@5c
    :cond_5c
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->isDataAllowed()Z

    #@5f
    move-result v4

    #@60
    if-eqz v4, :cond_76

    #@62
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->getAnyDataEnabled()Z

    #@65
    move-result v4

    #@66
    if-eqz v4, :cond_76

    #@68
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->isEmergency()Z

    #@6b
    move-result v4

    #@6c
    if-nez v4, :cond_76

    #@6e
    .line 492
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->setupData(Ljava/lang/String;)Z

    #@71
    move-result v2

    #@72
    .line 493
    .local v2, retValue:Z
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@75
    goto :goto_33

    #@76
    .line 496
    .end local v2           #retValue:Z
    :cond_76
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRequestedApnType:Ljava/lang/String;

    #@78
    const-string v5, "default"

    #@7a
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7d
    move-result v4

    #@7e
    if-nez v4, :cond_93

    #@80
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@82
    sget-object v5, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@84
    if-eq v4, v5, :cond_8c

    #@86
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@88
    sget-object v5, Lcom/android/internal/telephony/DctConstants$State;->SCANNING:Lcom/android/internal/telephony/DctConstants$State;

    #@8a
    if-ne v4, v5, :cond_93

    #@8c
    .line 499
    :cond_8c
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8e
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRequestedApnType:Ljava/lang/String;

    #@90
    invoke-virtual {v4, p1, v5}, Lcom/android/internal/telephony/PhoneBase;->notifyDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V

    #@93
    .line 501
    :cond_93
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@96
    .line 502
    const/4 v2, 0x0

    #@97
    goto :goto_33
.end method

.method private writeEventLogCdmaDataDrop()V
    .registers 6

    #@0
    .prologue
    .line 1267
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getCellLocation()Landroid/telephony/CellLocation;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/telephony/cdma/CdmaCellLocation;

    #@8
    move-object v0, v1

    #@9
    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    #@b
    .line 1268
    .local v0, loc:Landroid/telephony/cdma/CdmaCellLocation;
    const v2, 0xc3bf

    #@e
    const/4 v1, 0x2

    #@f
    new-array v3, v1, [Ljava/lang/Object;

    #@11
    const/4 v4, 0x0

    #@12
    if-eqz v0, :cond_31

    #@14
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    #@17
    move-result v1

    #@18
    :goto_18
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v1

    #@1c
    aput-object v1, v3, v4

    #@1e
    const/4 v1, 0x1

    #@1f
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@26
    move-result v4

    #@27
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v4

    #@2b
    aput-object v4, v3, v1

    #@2d
    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@30
    .line 1271
    return-void

    #@31
    .line 1268
    :cond_31
    const/4 v1, -0x1

    #@32
    goto :goto_18
.end method


# virtual methods
.method public CPAChanged()V
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1744
    iget-boolean v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@3
    if-eqz v2, :cond_57

    #@5
    .line 1748
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@7
    sget-object v3, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@9
    if-eq v2, v3, :cond_55

    #@b
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@d
    sget-object v3, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@f
    if-eq v2, v3, :cond_55

    #@11
    const/4 v0, 0x1

    #@12
    .line 1749
    .local v0, isConnected:Z
    :goto_12
    const-string v2, "CDMA"

    #@14
    new-instance v3, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v4, "******* [CDMADCT] CPAChanged called enable:"

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    iget-boolean v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, " isConnected:"

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v4, " mState:"

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 1750
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@44
    sget-object v3, Lcom/android/internal/telephony/DctConstants$State;->DISCONNECTING:Lcom/android/internal/telephony/DctConstants$State;

    #@46
    if-eq v2, v3, :cond_54

    #@48
    .line 1752
    const-string v2, "apnChanged"

    #@4a
    invoke-virtual {p0, v0, v2, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpConnection(ZLjava/lang/String;Z)V

    #@4d
    .line 1753
    if-nez v0, :cond_54

    #@4f
    .line 1755
    const-string v1, "apnChanged"

    #@51
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onTrySetupData(Ljava/lang/String;)Z

    #@54
    .line 1763
    .end local v0           #isConnected:Z
    :cond_54
    :goto_54
    return-void

    #@55
    :cond_55
    move v0, v1

    #@56
    .line 1748
    goto :goto_12

    #@57
    .line 1761
    :cond_57
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onApnChanged()V

    #@5a
    goto :goto_54
.end method

.method public apnTypeToId(Ljava/lang/String;)I
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 254
    invoke-super {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected cleanUpConnection(Ljava/lang/String;Z)V
    .registers 3
    .parameter "requestApn"
    .parameter "doAll"

    #@0
    .prologue
    .line 1739
    return-void
.end method

.method protected cleanUpConnection(ZLjava/lang/String;Z)V
    .registers 13
    .parameter "tearDown"
    .parameter "reason"
    .parameter "doAll"

    #@0
    .prologue
    const v8, 0x4200f

    #@3
    const/4 v7, 0x0

    #@4
    .line 515
    new-instance v5, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v6, "cleanUpConnection: reason: "

    #@b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v5

    #@f
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@1a
    .line 518
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntent:Landroid/app/PendingIntent;

    #@1c
    if-eqz v5, :cond_34

    #@1e
    .line 519
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@20
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@23
    move-result-object v5

    #@24
    const-string v6, "alarm"

    #@26
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@29
    move-result-object v0

    #@2a
    check-cast v0, Landroid/app/AlarmManager;

    #@2c
    .line 521
    .local v0, am:Landroid/app/AlarmManager;
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntent:Landroid/app/PendingIntent;

    #@2e
    invoke-virtual {v0, v5}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@31
    .line 522
    const/4 v5, 0x0

    #@32
    iput-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntent:Landroid/app/PendingIntent;

    #@34
    .line 525
    .end local v0           #am:Landroid/app/AlarmManager;
    :cond_34
    sget-object v5, Lcom/android/internal/telephony/DctConstants$State;->DISCONNECTING:Lcom/android/internal/telephony/DctConstants$State;

    #@36
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->setState(Lcom/android/internal/telephony/DctConstants$State;)V

    #@39
    .line 526
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@3c
    .line 528
    const/4 v4, 0x0

    #@3d
    .line 529
    .local v4, notificationDeferred:Z
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@3f
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@42
    move-result-object v5

    #@43
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@46
    move-result-object v3

    #@47
    .local v3, i$:Ljava/util/Iterator;
    :cond_47
    :goto_47
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@4a
    move-result v5

    #@4b
    if-eqz v5, :cond_98

    #@4d
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@50
    move-result-object v1

    #@51
    check-cast v1, Lcom/android/internal/telephony/DataConnection;

    #@53
    .line 530
    .local v1, conn:Lcom/android/internal/telephony/DataConnection;
    if-eqz v1, :cond_47

    #@55
    .line 531
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnectionAsyncChannels:Ljava/util/HashMap;

    #@57
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->getDataConnectionId()I

    #@5a
    move-result v6

    #@5b
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5e
    move-result-object v6

    #@5f
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@62
    move-result-object v2

    #@63
    check-cast v2, Lcom/android/internal/telephony/DataConnectionAc;

    #@65
    .line 533
    .local v2, dcac:Lcom/android/internal/telephony/DataConnectionAc;
    if-eqz p1, :cond_8c

    #@67
    .line 534
    if-eqz p3, :cond_7b

    #@69
    .line 535
    const-string v5, "cleanUpConnection: teardown, conn.tearDownAll"

    #@6b
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@6e
    .line 536
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->getDataConnectionId()I

    #@71
    move-result v5

    #@72
    invoke-virtual {p0, v8, v5, v7, p2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@75
    move-result-object v5

    #@76
    invoke-virtual {v1, p2, v5}, Lcom/android/internal/telephony/DataConnection;->tearDownAll(Ljava/lang/String;Landroid/os/Message;)V

    #@79
    .line 543
    :goto_79
    const/4 v4, 0x1

    #@7a
    goto :goto_47

    #@7b
    .line 539
    :cond_7b
    const-string v5, "cleanUpConnection: teardown, conn.tearDown"

    #@7d
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@80
    .line 540
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->getDataConnectionId()I

    #@83
    move-result v5

    #@84
    invoke-virtual {p0, v8, v5, v7, p2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@87
    move-result-object v5

    #@88
    invoke-virtual {v1, p2, v5}, Lcom/android/internal/telephony/DataConnection;->tearDown(Ljava/lang/String;Landroid/os/Message;)V

    #@8b
    goto :goto_79

    #@8c
    .line 545
    :cond_8c
    const-string v5, "cleanUpConnection: !tearDown, call conn.resetSynchronously"

    #@8e
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@91
    .line 546
    if-eqz v2, :cond_96

    #@93
    .line 547
    invoke-virtual {v2}, Lcom/android/internal/telephony/DataConnectionAc;->resetSync()V

    #@96
    .line 549
    :cond_96
    const/4 v4, 0x0

    #@97
    goto :goto_47

    #@98
    .line 554
    .end local v1           #conn:Lcom/android/internal/telephony/DataConnection;
    .end local v2           #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    :cond_98
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->stopNetStatPoll()V

    #@9b
    .line 555
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->stopDataStallAlarm()V

    #@9e
    .line 557
    if-nez v4, :cond_a8

    #@a0
    .line 558
    const-string v5, "cleanupConnection: !notificationDeferred"

    #@a2
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@a5
    .line 559
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->gotoIdleAndNotifyDataConnection(Ljava/lang/String;)V

    #@a8
    .line 561
    :cond_a8
    return-void
.end method

.method protected clearTetheredStateOnStatus()V
    .registers 3

    #@0
    .prologue
    .line 1645
    const-string v0, "clearTetheredStateOnStatus()"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@5
    .line 1646
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@7
    if-eqz v0, :cond_e

    #@9
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@b
    const/4 v1, 0x0

    #@c
    iput-boolean v1, v0, Lcom/android/internal/telephony/DataProfile;->mTetheredCallOn:Z

    #@e
    .line 1647
    :cond_e
    return-void
.end method

.method public closeImsPdn(I)V
    .registers 4
    .parameter "reason"

    #@0
    .prologue
    .line 1571
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IMS_AFW] closeImsPdn - reason : "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@16
    .line 1572
    return-void
.end method

.method public disable_ehrpd_internet_ipv6()V
    .registers 1

    #@0
    .prologue
    .line 1654
    return-void
.end method

.method protected disconnectOneLowerPriorityCall(Ljava/lang/String;)Z
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 1538
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dispose()V
    .registers 5

    #@0
    .prologue
    .line 201
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpConnection(ZLjava/lang/String;Z)V

    #@6
    .line 203
    invoke-super {p0}, Lcom/android/internal/telephony/DataConnectionTracker;->dispose()V

    #@9
    .line 206
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@d
    invoke-interface {v1, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForAvailable(Landroid/os/Handler;)V

    #@10
    .line 207
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@12
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@14
    invoke-interface {v1, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOffOrNotAvailable(Landroid/os/Handler;)V

    #@17
    .line 208
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@19
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@1f
    .line 209
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_24

    #@21
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@24
    .line 210
    :cond_24
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@26
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@28
    invoke-interface {v1, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForDataNetworkStateChanged(Landroid/os/Handler;)V

    #@2b
    .line 211
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2d
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2f
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->unregisterForVoiceCallEnded(Landroid/os/Handler;)V

    #@32
    .line 212
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@34
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@36
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->unregisterForVoiceCallStarted(Landroid/os/Handler;)V

    #@39
    .line 213
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@3b
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@3d
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->unregisterForDataConnectionAttached(Landroid/os/Handler;)V

    #@40
    .line 214
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@42
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@44
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->unregisterForDataConnectionDetached(Landroid/os/Handler;)V

    #@47
    .line 215
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@49
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@4b
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->unregisterForRoamingOn(Landroid/os/Handler;)V

    #@4e
    .line 216
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@50
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@52
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->unregisterForRoamingOff(Landroid/os/Handler;)V

    #@55
    .line 217
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@57
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->dispose(Landroid/os/Handler;)V

    #@5a
    .line 218
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5c
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5e
    invoke-interface {v1, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForCdmaOtaProvision(Landroid/os/Handler;)V

    #@61
    .line 219
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@63
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->unregisterForModemProfileReady(Landroid/os/Handler;)V

    #@66
    .line 221
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->destroyAllDataConnectionList()V

    #@69
    .line 222
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 1678
    const-string v0, "CdmaDataConnectionTracker extends:"

    #@2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 1679
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/telephony/DataConnectionTracker;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@8
    .line 1680
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, " mCdmaPhone="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20
    .line 1681
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, " mCdmaSSM="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 1682
    new-instance v0, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v1, " mPendingDataConnection="

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingDataConnection:Lcom/android/internal/telephony/cdma/CdmaDataConnection;

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@50
    .line 1683
    new-instance v0, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v1, " mPendingRestartRadio="

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingRestartRadio:Z

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@68
    .line 1684
    return-void
.end method

.method public enable_ehrpd_internet_ipv6()V
    .registers 1

    #@0
    .prologue
    .line 1658
    return-void
.end method

.method protected fetchDunApn()Lcom/android/internal/telephony/DataProfile;
    .registers 2

    #@0
    .prologue
    .line 1533
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected finalize()V
    .registers 2

    #@0
    .prologue
    .line 226
    const-string v0, "CdmaDataConnectionTracker finalized"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@5
    .line 227
    return-void
.end method

.method public getAPNList()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1689
    new-instance v3, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    .line 1690
    .local v3, sb:Ljava/lang/StringBuilder;
    const-string v5, "CDMA"

    #@8
    const-string v6, "[CdmaDCT] agetAPNList()"

    #@a
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 1692
    const/4 v1, 0x0

    #@e
    .line 1693
    .local v1, isactiveEx:Z
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@10
    sget-object v6, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@12
    if-ne v5, v6, :cond_4f

    #@14
    .line 1694
    const-string v5, "CDMA"

    #@16
    const-string v6, "[CdmaDCT] agetAPNList() is CONNECTED "

    #@18
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 1695
    const/4 v1, 0x1

    #@1c
    .line 1696
    const-string v5, "default"

    #@1e
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->getLinkProperties(Ljava/lang/String;)Landroid/net/LinkProperties;

    #@21
    move-result-object v0

    #@22
    .line 1698
    .local v0, apnprop:Landroid/net/LinkProperties;
    if-nez v0, :cond_2c

    #@24
    .line 1699
    const-string v5, "CDMA"

    #@26
    const-string v6, "[CdmaDCT] apnprop is null. return null."

    #@28
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1709
    .end local v0           #apnprop:Landroid/net/LinkProperties;
    :cond_2b
    :goto_2b
    return-object v4

    #@2c
    .line 1702
    .restart local v0       #apnprop:Landroid/net/LinkProperties;
    :cond_2c
    invoke-virtual {v0}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/Collection;

    #@2f
    move-result-object v2

    #@30
    .line 1703
    .local v2, linkAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    const-string v5, "CDMA_NAI"

    #@32
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    .line 1704
    new-instance v5, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v6, ","

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v5

    #@4c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    .line 1706
    .end local v0           #apnprop:Landroid/net/LinkProperties;
    .end local v2           #linkAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    :cond_4f
    if-eqz v1, :cond_2b

    #@51
    .line 1707
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    goto :goto_2b
.end method

.method protected getActionIntentDataStallAlarm()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 236
    const-string v0, "com.android.internal.telephony.cdma-data-stall"

    #@2
    return-object v0
.end method

.method protected getActionIntentReconnectAlarm()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 231
    const-string v0, "com.android.internal.telephony.cdma-reconnect"

    #@2
    return-object v0
.end method

.method public getCdmaInfo()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1587
    const-string v0, "[IMS_AFW] getCdmaInfo()"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@5
    .line 1588
    const/4 v0, 0x0

    #@6
    return-object v0
.end method

.method public getLinkProperties_defaultAPN()Landroid/net/LinkProperties;
    .registers 2

    #@0
    .prologue
    .line 1662
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getLteInfo()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1581
    const-string v0, "[IMS_AFW] getLteInfo()"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@5
    .line 1582
    const/4 v0, 0x0

    #@6
    return-object v0
.end method

.method public getOverallState()Lcom/android/internal/telephony/DctConstants$State;
    .registers 2

    #@0
    .prologue
    .line 263
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@2
    return-object v0
.end method

.method public getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;
    .registers 3
    .parameter "ipv"

    #@0
    .prologue
    .line 1566
    const-string v0, "[IMS_AFW] getPcscfAddress()"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@5
    .line 1567
    const/4 v0, 0x0

    #@6
    return-object v0
.end method

.method public getPcscfAddress(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .registers 5
    .parameter "ipv"
    .parameter "apnType"

    #@0
    .prologue
    .line 1559
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IMS_AFW] getPcscfAddress() : apnTyp="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@16
    .line 1560
    const/4 v0, 0x0

    #@17
    return-object v0
.end method

.method public getPreferredApn()Lcom/android/internal/telephony/DataProfile;
    .registers 12

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1600
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@3
    if-eqz v0, :cond_d

    #@5
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_e

    #@d
    .line 1635
    :cond_d
    :goto_d
    return-object v3

    #@e
    .line 1604
    :cond_e
    const/4 v6, 0x0

    #@f
    .line 1605
    .local v6, canSetPreferApn:Z
    const-string v0, "content://telephony/carriers/preferapn"

    #@11
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@14
    move-result-object v1

    #@15
    .line 1607
    .local v1, PREFERAPN_URI:Landroid/net/Uri;
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@17
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1e
    move-result-object v0

    #@1f
    const/4 v2, 0x3

    #@20
    new-array v2, v2, [Ljava/lang/String;

    #@22
    const/4 v4, 0x0

    #@23
    const-string v5, "_id"

    #@25
    aput-object v5, v2, v4

    #@27
    const/4 v4, 0x1

    #@28
    const-string v5, "name"

    #@2a
    aput-object v5, v2, v4

    #@2c
    const/4 v4, 0x2

    #@2d
    const-string v5, "apn"

    #@2f
    aput-object v5, v2, v4

    #@31
    const-string v5, "name ASC"

    #@33
    move-object v4, v3

    #@34
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@37
    move-result-object v7

    #@38
    .line 1611
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_89

    #@3a
    .line 1612
    const/4 v6, 0x1

    #@3b
    .line 1617
    :goto_3b
    if-eqz v6, :cond_8b

    #@3d
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@40
    move-result v0

    #@41
    if-lez v0, :cond_8b

    #@43
    .line 1619
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@46
    .line 1620
    const-string v0, "_id"

    #@48
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@4b
    move-result v0

    #@4c
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    #@4f
    move-result v10

    #@50
    .line 1621
    .local v10, pos:I
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@52
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@55
    move-result-object v8

    #@56
    .local v8, i$:Ljava/util/Iterator;
    :cond_56
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@59
    move-result v0

    #@5a
    if-eqz v0, :cond_8b

    #@5c
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5f
    move-result-object v9

    #@60
    check-cast v9, Lcom/android/internal/telephony/DataProfile;

    #@62
    .line 1622
    .local v9, p:Lcom/android/internal/telephony/DataProfile;
    iget v0, v9, Lcom/android/internal/telephony/DataProfile;->id:I

    #@64
    if-ne v0, v10, :cond_56

    #@66
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mRequestedApnType:Ljava/lang/String;

    #@68
    invoke-virtual {v9, v0}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@6b
    move-result v0

    #@6c
    if-eqz v0, :cond_56

    #@6e
    .line 1623
    new-instance v0, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v2, "getPreferredApn: X found apnSetting"

    #@75
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v0

    #@7d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v0

    #@81
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@84
    .line 1624
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@87
    move-object v3, v9

    #@88
    .line 1625
    goto :goto_d

    #@89
    .line 1614
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v9           #p:Lcom/android/internal/telephony/DataProfile;
    .end local v10           #pos:I
    :cond_89
    const/4 v6, 0x0

    #@8a
    goto :goto_3b

    #@8b
    .line 1630
    :cond_8b
    if-eqz v7, :cond_90

    #@8d
    .line 1631
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@90
    .line 1634
    :cond_90
    const-string v0, "getPreferredApn: X not found"

    #@92
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@95
    goto/16 :goto_d
.end method

.method public declared-synchronized getState(Ljava/lang/String;)Lcom/android/internal/telephony/DctConstants$State;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 258
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method protected getUiccCardApplication()Lcom/android/internal/telephony/uicc/IccRecords;
    .registers 3

    #@0
    .prologue
    .line 1489
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->getIccRecords(I)Lcom/android/internal/telephony/uicc/IccRecords;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method protected gotoIdleAndNotifyDataConnection(Ljava/lang/String;)V
    .registers 4
    .parameter "reason"

    #@0
    .prologue
    .line 801
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "gotoIdleAndNotifyDataConnection: reason="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@16
    .line 802
    sget-object v0, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@18
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->setState(Lcom/android/internal/telephony/DctConstants$State;)V

    #@1b
    .line 803
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@1e
    .line 804
    const/4 v0, 0x0

    #@1f
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@21
    .line 805
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@23
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->clearActiveDataProfile()V

    #@26
    .line 806
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1385
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "CdmaDCT handleMessage msg="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@18
    .line 1387
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1a
    iget-boolean v2, v2, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@1c
    if-eqz v2, :cond_22

    #@1e
    iget-boolean v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsDisposed:Z

    #@20
    if-eqz v2, :cond_28

    #@22
    .line 1388
    :cond_22
    const-string v2, "Ignore CDMA msgs since CDMA phone is inactive"

    #@24
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@27
    .line 1463
    :cond_27
    :goto_27
    return-void

    #@28
    .line 1392
    :cond_28
    iget v2, p1, Landroid/os/Message;->what:I

    #@2a
    sparse-switch v2, :sswitch_data_ea

    #@2d
    .line 1460
    invoke-super {p0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->handleMessage(Landroid/os/Message;)V

    #@30
    goto :goto_27

    #@31
    .line 1394
    :sswitch_31
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onRecordsLoaded()V

    #@34
    goto :goto_27

    #@35
    .line 1398
    :sswitch_35
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@37
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getCdmaSubscriptionSource()I

    #@3a
    move-result v2

    #@3b
    if-ne v2, v5, :cond_27

    #@3d
    .line 1400
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onNVReady()V

    #@40
    goto :goto_27

    #@41
    .line 1405
    :sswitch_41
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onCdmaDataDetached()V

    #@44
    goto :goto_27

    #@45
    .line 1409
    :sswitch_45
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@47
    check-cast v2, Landroid/os/AsyncResult;

    #@49
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onDataStateChanged(Landroid/os/AsyncResult;)V

    #@4c
    goto :goto_27

    #@4d
    .line 1413
    :sswitch_4d
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4f
    check-cast v2, Landroid/os/AsyncResult;

    #@51
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onCdmaOtaProvision(Landroid/os/AsyncResult;)V

    #@54
    goto :goto_27

    #@55
    .line 1417
    :sswitch_55
    const-string v2, "EVENT_RESTART_RADIO"

    #@57
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@5a
    .line 1418
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onRestartRadio()V

    #@5d
    goto :goto_27

    #@5e
    .line 1422
    :sswitch_5e
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onModemDataProfileReady()V

    #@61
    goto :goto_27

    #@62
    .line 1426
    :sswitch_62
    const-string v2, "EVENT_APN_CHANGED_FOR_CDMA"

    #@64
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@67
    .line 1427
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@69
    if-eqz v2, :cond_83

    #@6b
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@6d
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@6f
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@72
    move-result-object v2

    #@73
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_ON_CDMA:Z

    #@75
    if-eqz v2, :cond_83

    #@77
    .line 1428
    const-string v2, "CDMA"

    #@79
    const-string v3, "onApnChanged(): calling readDataProfileFromDb"

    #@7b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 1429
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@80
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->readDataProfileFromDb()V

    #@83
    .line 1431
    :cond_83
    iget-boolean v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->datablockbyadmin:Z

    #@85
    if-eqz v2, :cond_27

    #@87
    const-string v2, "VZWBASE"

    #@89
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8b
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@8d
    invoke-interface {v3}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@90
    move-result-object v3

    #@91
    iget-object v3, v3, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@93
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@96
    move-result v2

    #@97
    if-eqz v2, :cond_27

    #@99
    .line 1432
    const-string v2, "AdminDisabled"

    #@9b
    invoke-virtual {p0, v5, v2, v4}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpConnection(ZLjava/lang/String;Z)V

    #@9e
    goto :goto_27

    #@9f
    .line 1439
    :sswitch_9f
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a1
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@a3
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@a6
    move-result-object v2

    #@a7
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@a9
    if-eqz v2, :cond_27

    #@ab
    .line 1441
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onPendingTimeout()V

    #@ae
    goto/16 :goto_27

    #@b0
    .line 1448
    :sswitch_b0
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b2
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@b4
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@b7
    sget-boolean v2, Lcom/android/internal/telephony/LGfeature;->mipErrorPopup:Z

    #@b9
    if-eqz v2, :cond_27

    #@bb
    .line 1450
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@bd
    check-cast v1, Landroid/os/AsyncResult;

    #@bf
    .line 1451
    .local v1, ar:Landroid/os/AsyncResult;
    iget-object v2, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c1
    check-cast v2, [I

    #@c3
    check-cast v2, [I

    #@c5
    aget v0, v2, v4

    #@c7
    .line 1452
    .local v0, LTE_EMM_ERRROR_CODE:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v3, "EVENT_LTE_EMM_ERRROR_CODE ::"

    #@ce
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v2

    #@d2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v2

    #@d6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v2

    #@da
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@dd
    .line 1453
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@df
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e1
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getMyDebugger()Lcom/android/internal/telephony/MMdebuger;

    #@e4
    move-result-object v2

    #@e5
    invoke-virtual {v2, v0}, Lcom/android/internal/telephony/MMdebuger;->SetLteEmmErrorCode(I)V

    #@e8
    goto/16 :goto_27

    #@ea
    .line 1392
    :sswitch_data_ea
    .sparse-switch
        0x42002 -> :sswitch_31
        0x42004 -> :sswitch_45
        0x42014 -> :sswitch_41
        0x42015 -> :sswitch_35
        0x42019 -> :sswitch_4d
        0x4201a -> :sswitch_55
        0x42025 -> :sswitch_5e
        0x42034 -> :sswitch_9f
        0x4203a -> :sswitch_62
        0x4203b -> :sswitch_b0
    .end sparse-switch
.end method

.method protected isApnTypeAvailable(Ljava/lang/String;)Z
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 268
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->isApnTypeAvailable(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected isConnected()Z
    .registers 3

    #@0
    .prologue
    .line 1641
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@2
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method protected isDataAllowed()Z
    .registers 14

    #@0
    .prologue
    .line 274
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataEnabledLock:Ljava/lang/Object;

    #@2
    monitor-enter v11

    #@3
    .line 275
    :try_start_3
    iget-boolean v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mInternalDataEnabled:Z

    #@5
    .line 276
    .local v3, internalDataEnabled:Z
    monitor-exit v11
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_4a

    #@6
    .line 278
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@8
    iget-object v10, v10, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@a
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getCurrentDataConnectionState()I

    #@d
    move-result v4

    #@e
    .line 279
    .local v4, psState:I
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@10
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@13
    move-result-object v10

    #@14
    invoke-virtual {v10}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@17
    move-result v10

    #@18
    if-eqz v10, :cond_4d

    #@1a
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@1d
    move-result v10

    #@1e
    if-nez v10, :cond_4d

    #@20
    const/4 v7, 0x1

    #@21
    .line 280
    .local v7, roaming:Z
    :goto_21
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@23
    iget-object v10, v10, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@25
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getDesiredPowerState()Z

    #@28
    move-result v1

    #@29
    .line 281
    .local v1, desiredPowerState:Z
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@2b
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getCdmaSubscriptionSource()I

    #@2e
    move-result v10

    #@2f
    const/4 v11, 0x1

    #@30
    if-ne v10, v11, :cond_4f

    #@32
    const/4 v9, 0x1

    #@33
    .line 286
    .local v9, subscriptionFromNv:Z
    :goto_33
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@35
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@37
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@3a
    move-result-object v10

    #@3b
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_DATA_CALL_WHEN_ADMIN_PDN_DSIABLED_VZW:Z

    #@3d
    if-eqz v10, :cond_51

    #@3f
    .line 288
    iget-boolean v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->datablockbyadmin:Z

    #@41
    if-eqz v10, :cond_51

    #@43
    .line 290
    const-string v10, "there are no admin pdn so block"

    #@45
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@48
    .line 291
    const/4 v0, 0x0

    #@49
    .line 457
    :cond_49
    :goto_49
    return v0

    #@4a
    .line 276
    .end local v1           #desiredPowerState:Z
    .end local v3           #internalDataEnabled:Z
    .end local v4           #psState:I
    .end local v7           #roaming:Z
    .end local v9           #subscriptionFromNv:Z
    :catchall_4a
    move-exception v10

    #@4b
    :try_start_4b
    monitor-exit v11
    :try_end_4c
    .catchall {:try_start_4b .. :try_end_4c} :catchall_4a

    #@4c
    throw v10

    #@4d
    .line 279
    .restart local v3       #internalDataEnabled:Z
    .restart local v4       #psState:I
    :cond_4d
    const/4 v7, 0x0

    #@4e
    goto :goto_21

    #@4f
    .line 281
    .restart local v1       #desiredPowerState:Z
    .restart local v7       #roaming:Z
    :cond_4f
    const/4 v9, 0x0

    #@50
    goto :goto_33

    #@51
    .line 297
    .restart local v9       #subscriptionFromNv:Z
    :cond_51
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@53
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@55
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@58
    move-result-object v10

    #@59
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@5b
    if-eqz v10, :cond_68

    #@5d
    iget-boolean v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPrevPending:Z

    #@5f
    if-eqz v10, :cond_68

    #@61
    .line 299
    const-string v10, "pre tracker is pending so wait"

    #@63
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@66
    .line 300
    const/4 v0, 0x0

    #@67
    goto :goto_49

    #@68
    .line 304
    :cond_68
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@6a
    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@6d
    move-result-object v5

    #@6e
    check-cast v5, Lcom/android/internal/telephony/uicc/IccRecords;

    #@70
    .line 306
    .local v5, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v5, :cond_367

    #@72
    invoke-virtual {v5}, Lcom/android/internal/telephony/uicc/IccRecords;->getIMSI_M_RecordsLoaded()Z

    #@75
    move-result v2

    #@76
    .line 308
    .local v2, imsi_m_recordsLoaded:Z
    :goto_76
    if-eqz v4, :cond_7c

    #@78
    iget-boolean v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAutoAttachOnCreation:Z

    #@7a
    if-eqz v10, :cond_36a

    #@7c
    :cond_7c
    if-nez v9, :cond_86

    #@7e
    if-eqz v5, :cond_36a

    #@80
    invoke-virtual {v5}, Lcom/android/internal/telephony/uicc/IccRecords;->getRecordsLoaded()Z

    #@83
    move-result v10

    #@84
    if-eqz v10, :cond_36a

    #@86
    :cond_86
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@88
    iget-object v10, v10, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@8a
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isConcurrentVoiceAndDataAllowed()Z

    #@8d
    move-result v10

    #@8e
    if-nez v10, :cond_9a

    #@90
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@92
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@95
    move-result-object v10

    #@96
    sget-object v11, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@98
    if-ne v10, v11, :cond_36a

    #@9a
    :cond_9a
    if-nez v7, :cond_36a

    #@9c
    if-eqz v3, :cond_36a

    #@9e
    const/4 v0, 0x1

    #@9f
    .line 328
    .local v0, allowed:Z
    :goto_9f
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a1
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@a3
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@a6
    sget-boolean v10, Lcom/android/internal/telephony/LGfeature;->omadmDataBlock:Z

    #@a8
    const/4 v11, 0x1

    #@a9
    if-ne v10, v11, :cond_d7

    #@ab
    iget v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmSession:I

    #@ad
    const/4 v11, 0x1

    #@ae
    if-ne v10, v11, :cond_d7

    #@b0
    .line 329
    if-eqz v4, :cond_b6

    #@b2
    iget-boolean v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAutoAttachOnCreation:Z

    #@b4
    if-eqz v10, :cond_36d

    #@b6
    :cond_b6
    if-nez v9, :cond_c0

    #@b8
    if-eqz v5, :cond_36d

    #@ba
    invoke-virtual {v5}, Lcom/android/internal/telephony/uicc/IccRecords;->getRecordsLoaded()Z

    #@bd
    move-result v10

    #@be
    if-eqz v10, :cond_36d

    #@c0
    :cond_c0
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@c2
    iget-object v10, v10, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@c4
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isConcurrentVoiceAndDataAllowed()Z

    #@c7
    move-result v10

    #@c8
    if-nez v10, :cond_d4

    #@ca
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@cc
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@cf
    move-result-object v10

    #@d0
    sget-object v11, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@d2
    if-ne v10, v11, :cond_36d

    #@d4
    :cond_d4
    if-eqz v3, :cond_36d

    #@d6
    const/4 v0, 0x1

    #@d7
    .line 341
    :cond_d7
    :goto_d7
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d9
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@db
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@de
    move-result-object v10

    #@df
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->lg_data_sprint_oamdm_provisioning:Z

    #@e1
    if-eqz v10, :cond_373

    #@e3
    .line 342
    if-eqz v0, :cond_370

    #@e5
    if-eqz v1, :cond_370

    #@e7
    iget-boolean v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingRestartRadio:Z

    #@e9
    if-nez v10, :cond_370

    #@eb
    const/4 v0, 0x1

    #@ec
    .line 357
    :goto_ec
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ee
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@f1
    move-result-object v10

    #@f2
    invoke-virtual {v10}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@f5
    move-result v8

    #@f6
    .line 359
    .local v8, rt:I
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f8
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@fa
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@fd
    move-result-object v10

    #@fe
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_1XEVDO_UPLUS:Z

    #@100
    if-eqz v10, :cond_14f

    #@102
    .line 360
    const/4 v10, 0x4

    #@103
    if-eq v8, v10, :cond_118

    #@105
    const/4 v10, 0x5

    #@106
    if-eq v8, v10, :cond_118

    #@108
    const/4 v10, 0x6

    #@109
    if-eq v8, v10, :cond_118

    #@10b
    const/4 v10, 0x7

    #@10c
    if-eq v8, v10, :cond_118

    #@10e
    const/16 v10, 0x8

    #@110
    if-eq v8, v10, :cond_118

    #@112
    const/16 v10, 0xc

    #@114
    if-eq v8, v10, :cond_118

    #@116
    if-nez v8, :cond_14f

    #@118
    .line 368
    :cond_118
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@11a
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@11c
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@11f
    move-result-object v10

    #@120
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_1XEVDO_UPLUS:Z

    #@122
    if-eqz v10, :cond_391

    #@124
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@126
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@129
    move-result-object v10

    #@12a
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12d
    move-result-object v10

    #@12e
    const-string v11, "revsetting_hidden"

    #@130
    const/4 v12, 0x0

    #@131
    invoke-static {v10, v11, v12}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@134
    move-result v10

    #@135
    const/4 v11, 0x1

    #@136
    if-ne v10, v11, :cond_391

    #@138
    .line 369
    const/4 v0, 0x1

    #@139
    .line 373
    :goto_139
    new-instance v10, Ljava/lang/StringBuilder;

    #@13b
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@13e
    const-string v11, "disallowed1xDataCall =  "

    #@140
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v10

    #@144
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@147
    move-result-object v10

    #@148
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14b
    move-result-object v10

    #@14c
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@14f
    .line 379
    :cond_14f
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@151
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@153
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@156
    move-result-object v10

    #@157
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_DUN_NV_ONOFF_UPLUS:Z

    #@159
    if-eqz v10, :cond_160

    #@15b
    iget-boolean v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->blockForDun:Z

    #@15d
    if-eqz v10, :cond_160

    #@15f
    .line 380
    const/4 v0, 0x0

    #@160
    .line 386
    :cond_160
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@162
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@164
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@167
    sget-boolean v10, Lcom/android/internal/telephony/LGfeature;->omadmDataBlock:Z

    #@169
    const/4 v11, 0x1

    #@16a
    if-ne v10, v11, :cond_1cc

    #@16c
    .line 388
    const-string v10, "CDMA"

    #@16e
    new-instance v11, Ljava/lang/StringBuilder;

    #@170
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@173
    const-string v12, "[LG_DATA_SPRINT_OMADM_DATA_BLOCK] : isDataAllowed = "

    #@175
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@178
    move-result-object v11

    #@179
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v11

    #@17d
    const-string v12, ", mDataProfile = "

    #@17f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v11

    #@183
    iget v12, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataProfile:I

    #@185
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@188
    move-result-object v11

    #@189
    const-string v12, ", mIsOmaDmLock = "

    #@18b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v11

    #@18f
    iget v12, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmLock:I

    #@191
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@194
    move-result-object v11

    #@195
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v11

    #@199
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19c
    .line 391
    iget v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmSession:I

    #@19e
    const/4 v11, 0x1

    #@19f
    if-eq v10, v11, :cond_1ac

    #@1a1
    .line 392
    if-eqz v0, :cond_394

    #@1a3
    if-eqz v7, :cond_1ab

    #@1a5
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@1a8
    move-result v10

    #@1a9
    if-eqz v10, :cond_394

    #@1ab
    :cond_1ab
    const/4 v0, 0x1

    #@1ac
    .line 396
    :cond_1ac
    :goto_1ac
    :try_start_1ac
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1ae
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1b1
    move-result-object v10

    #@1b2
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b5
    move-result-object v10

    #@1b6
    const-string v11, "lg_omadm_lwmo_lock_state"

    #@1b8
    const/4 v12, 0x0

    #@1b9
    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1bc
    move-result v10

    #@1bd
    iput v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmLock:I
    :try_end_1bf
    .catch Ljava/lang/Exception; {:try_start_1ac .. :try_end_1bf} :catch_39a

    #@1bf
    .line 400
    :goto_1bf
    iget v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmSession:I

    #@1c1
    const/4 v11, 0x1

    #@1c2
    if-eq v10, v11, :cond_1cc

    #@1c4
    .line 401
    if-eqz v0, :cond_1cb

    #@1c6
    iget v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmLock:I

    #@1c8
    const/4 v11, 0x1

    #@1c9
    if-ne v10, v11, :cond_397

    #@1cb
    :cond_1cb
    const/4 v0, 0x0

    #@1cc
    .line 405
    :cond_1cc
    :goto_1cc
    if-nez v0, :cond_49

    #@1ce
    .line 406
    const-string v6, ""

    #@1d0
    .line 407
    .local v6, reason:Ljava/lang/String;
    if-eqz v4, :cond_1ed

    #@1d2
    iget-boolean v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mAutoAttachOnCreation:Z

    #@1d4
    if-nez v10, :cond_1ed

    #@1d6
    .line 408
    new-instance v10, Ljava/lang/StringBuilder;

    #@1d8
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1db
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1de
    move-result-object v10

    #@1df
    const-string v11, " - psState= "

    #@1e1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e4
    move-result-object v10

    #@1e5
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v10

    #@1e9
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ec
    move-result-object v6

    #@1ed
    .line 410
    :cond_1ed
    if-nez v9, :cond_20a

    #@1ef
    if-eqz v5, :cond_1f7

    #@1f1
    invoke-virtual {v5}, Lcom/android/internal/telephony/uicc/IccRecords;->getRecordsLoaded()Z

    #@1f4
    move-result v10

    #@1f5
    if-nez v10, :cond_20a

    #@1f7
    .line 412
    :cond_1f7
    new-instance v10, Ljava/lang/StringBuilder;

    #@1f9
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1fc
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ff
    move-result-object v10

    #@200
    const-string v11, " - RUIM not loaded"

    #@202
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@205
    move-result-object v10

    #@206
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@209
    move-result-object v6

    #@20a
    .line 414
    :cond_20a
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@20c
    iget-object v10, v10, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@20e
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isConcurrentVoiceAndDataAllowed()Z

    #@211
    move-result v10

    #@212
    if-nez v10, :cond_23b

    #@214
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@216
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@219
    move-result-object v10

    #@21a
    sget-object v11, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@21c
    if-eq v10, v11, :cond_23b

    #@21e
    .line 416
    new-instance v10, Ljava/lang/StringBuilder;

    #@220
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@223
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v10

    #@227
    const-string v11, " - concurrentVoiceAndData not allowed and state= "

    #@229
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22c
    move-result-object v10

    #@22d
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@22f
    invoke-virtual {v11}, Lcom/android/internal/telephony/PhoneBase;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@232
    move-result-object v11

    #@233
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@236
    move-result-object v10

    #@237
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23a
    move-result-object v6

    #@23b
    .line 418
    :cond_23b
    if-eqz v7, :cond_250

    #@23d
    new-instance v10, Ljava/lang/StringBuilder;

    #@23f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@242
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@245
    move-result-object v10

    #@246
    const-string v11, " - Roaming"

    #@248
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24b
    move-result-object v10

    #@24c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24f
    move-result-object v6

    #@250
    .line 419
    :cond_250
    if-nez v3, :cond_265

    #@252
    new-instance v10, Ljava/lang/StringBuilder;

    #@254
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@257
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25a
    move-result-object v10

    #@25b
    const-string v11, " - mInternalDataEnabled= false"

    #@25d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@260
    move-result-object v10

    #@261
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@264
    move-result-object v6

    #@265
    .line 420
    :cond_265
    if-nez v1, :cond_27a

    #@267
    new-instance v10, Ljava/lang/StringBuilder;

    #@269
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@26c
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26f
    move-result-object v10

    #@270
    const-string v11, " - desiredPowerState= false"

    #@272
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@275
    move-result-object v10

    #@276
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@279
    move-result-object v6

    #@27a
    .line 421
    :cond_27a
    iget-boolean v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingRestartRadio:Z

    #@27c
    if-eqz v10, :cond_291

    #@27e
    new-instance v10, Ljava/lang/StringBuilder;

    #@280
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@283
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@286
    move-result-object v10

    #@287
    const-string v11, " - mPendingRestartRadio= true"

    #@289
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28c
    move-result-object v10

    #@28d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@290
    move-result-object v6

    #@291
    .line 422
    :cond_291
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@293
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@295
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@298
    move-result-object v10

    #@299
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->lg_data_sprint_oamdm_provisioning:Z

    #@29b
    if-nez v10, :cond_2b8

    #@29d
    .line 423
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@29f
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CDMAPhone;->needsOtaServiceProvisioning()Z

    #@2a2
    move-result v10

    #@2a3
    if-eqz v10, :cond_2b8

    #@2a5
    new-instance v10, Ljava/lang/StringBuilder;

    #@2a7
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@2aa
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ad
    move-result-object v10

    #@2ae
    const-string v11, " - needs Provisioning"

    #@2b0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b3
    move-result-object v10

    #@2b4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b7
    move-result-object v6

    #@2b8
    .line 426
    :cond_2b8
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2ba
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2bc
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2bf
    sget-boolean v10, Lcom/android/internal/telephony/LGfeature;->omadmDataBlock:Z

    #@2c1
    const/4 v11, 0x1

    #@2c2
    if-ne v10, v11, :cond_2f7

    #@2c4
    .line 427
    if-eqz v7, :cond_2df

    #@2c6
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@2c9
    move-result v10

    #@2ca
    if-nez v10, :cond_2df

    #@2cc
    .line 428
    new-instance v10, Ljava/lang/StringBuilder;

    #@2ce
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@2d1
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d4
    move-result-object v10

    #@2d5
    const-string v11, " - omadm_data_block mDataProfile & roaming"

    #@2d7
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2da
    move-result-object v10

    #@2db
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2de
    move-result-object v6

    #@2df
    .line 430
    :cond_2df
    iget v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmLock:I

    #@2e1
    const/4 v11, 0x1

    #@2e2
    if-ne v10, v11, :cond_2f7

    #@2e4
    .line 431
    new-instance v10, Ljava/lang/StringBuilder;

    #@2e6
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@2e9
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ec
    move-result-object v10

    #@2ed
    const-string v11, " - omadm_data_block mIsOmaDmLock is true"

    #@2ef
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f2
    move-result-object v10

    #@2f3
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f6
    move-result-object v6

    #@2f7
    .line 437
    :cond_2f7
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2f9
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2fb
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2fe
    move-result-object v10

    #@2ff
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_1XEVDO_UPLUS:Z

    #@301
    if-eqz v10, :cond_330

    #@303
    .line 438
    const/4 v10, 0x4

    #@304
    if-eq v8, v10, :cond_319

    #@306
    const/4 v10, 0x5

    #@307
    if-eq v8, v10, :cond_319

    #@309
    const/4 v10, 0x6

    #@30a
    if-eq v8, v10, :cond_319

    #@30c
    const/4 v10, 0x7

    #@30d
    if-eq v8, v10, :cond_319

    #@30f
    const/16 v10, 0x8

    #@311
    if-eq v8, v10, :cond_319

    #@313
    const/16 v10, 0xc

    #@315
    if-eq v8, v10, :cond_319

    #@317
    if-nez v8, :cond_330

    #@319
    .line 445
    :cond_319
    new-instance v10, Ljava/lang/StringBuilder;

    #@31b
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@31e
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@321
    move-result-object v10

    #@322
    const-string v11, " - Radio Tech is "

    #@324
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@327
    move-result-object v10

    #@328
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32b
    move-result-object v10

    #@32c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32f
    move-result-object v6

    #@330
    .line 451
    :cond_330
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@332
    iget-object v10, v10, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@334
    invoke-interface {v10}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@337
    move-result-object v10

    #@338
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_DUN_NV_ONOFF_UPLUS:Z

    #@33a
    if-eqz v10, :cond_34f

    #@33c
    .line 452
    new-instance v10, Ljava/lang/StringBuilder;

    #@33e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@341
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@344
    move-result-object v10

    #@345
    const-string v11, " - block for DUN call"

    #@347
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34a
    move-result-object v10

    #@34b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34e
    move-result-object v6

    #@34f
    .line 455
    :cond_34f
    new-instance v10, Ljava/lang/StringBuilder;

    #@351
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@354
    const-string v11, "Data not allowed due to"

    #@356
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@359
    move-result-object v10

    #@35a
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35d
    move-result-object v10

    #@35e
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@361
    move-result-object v10

    #@362
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@365
    goto/16 :goto_49

    #@367
    .line 306
    .end local v0           #allowed:Z
    .end local v2           #imsi_m_recordsLoaded:Z
    .end local v6           #reason:Ljava/lang/String;
    .end local v8           #rt:I
    :cond_367
    const/4 v2, 0x0

    #@368
    goto/16 :goto_76

    #@36a
    .line 308
    .restart local v2       #imsi_m_recordsLoaded:Z
    :cond_36a
    const/4 v0, 0x0

    #@36b
    goto/16 :goto_9f

    #@36d
    .line 329
    .restart local v0       #allowed:Z
    :cond_36d
    const/4 v0, 0x0

    #@36e
    goto/16 :goto_d7

    #@370
    .line 342
    :cond_370
    const/4 v0, 0x0

    #@371
    goto/16 :goto_ec

    #@373
    .line 347
    :cond_373
    if-eqz v0, :cond_38f

    #@375
    if-eqz v1, :cond_38f

    #@377
    iget-boolean v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingRestartRadio:Z

    #@379
    if-nez v10, :cond_38f

    #@37b
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@37d
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getLteOnCdmaMode()I

    #@380
    move-result v10

    #@381
    const/4 v11, 0x1

    #@382
    if-eq v10, v11, :cond_38c

    #@384
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@386
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CDMAPhone;->needsOtaServiceProvisioning()Z

    #@389
    move-result v10

    #@38a
    if-nez v10, :cond_38f

    #@38c
    :cond_38c
    const/4 v0, 0x1

    #@38d
    :goto_38d
    goto/16 :goto_ec

    #@38f
    :cond_38f
    const/4 v0, 0x0

    #@390
    goto :goto_38d

    #@391
    .line 371
    .restart local v8       #rt:I
    :cond_391
    const/4 v0, 0x0

    #@392
    goto/16 :goto_139

    #@394
    .line 392
    :cond_394
    const/4 v0, 0x0

    #@395
    goto/16 :goto_1ac

    #@397
    .line 401
    :cond_397
    const/4 v0, 0x1

    #@398
    goto/16 :goto_1cc

    #@39a
    .line 397
    :catch_39a
    move-exception v10

    #@39b
    goto/16 :goto_1bf
.end method

.method protected isDataPossible(Ljava/lang/String;)Z
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->isDataAllowed()Z

    #@3
    move-result v0

    #@4
    .line 470
    .local v0, possible:Z
    return v0
.end method

.method public isDisconnected()Z
    .registers 3

    #@0
    .prologue
    .line 1553
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@2
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@4
    if-eq v0, v1, :cond_c

    #@6
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@8
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@a
    if-ne v0, v1, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 1668
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[CdmaDCT] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1669
    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 1673
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[CdmaDCT] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1674
    return-void
.end method

.method protected onApnChanged()V
    .registers 6

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 848
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@6
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@9
    sget-boolean v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_KDDI_SUPPORT_DYNAMIC_APN_NAI_SETTING_FOR_CPA:Z

    #@b
    if-ne v2, v0, :cond_54

    #@d
    .line 852
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@f
    sget-object v3, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@11
    if-eq v2, v3, :cond_5a

    #@13
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@15
    sget-object v3, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@17
    if-eq v2, v3, :cond_5a

    #@19
    .line 853
    .local v0, isConnected:Z
    :goto_19
    const-string v2, "CDMA"

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v4, "******* [CDMADCT] 1x RTT CPAChanged called enable:"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    iget-boolean v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    const-string v4, " isConnected:"

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    const-string v4, " mState:"

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 854
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@4b
    sget-object v3, Lcom/android/internal/telephony/DctConstants$State;->DISCONNECTING:Lcom/android/internal/telephony/DctConstants$State;

    #@4d
    if-eq v2, v3, :cond_54

    #@4f
    .line 856
    const-string v2, "apnChanged"

    #@51
    invoke-virtual {p0, v0, v2, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpConnection(ZLjava/lang/String;Z)V

    #@54
    .line 860
    .end local v0           #isConnected:Z
    :cond_54
    const-string v1, "apnChanged"

    #@56
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onTrySetupData(Ljava/lang/String;)Z

    #@59
    .line 861
    return-void

    #@5a
    :cond_5a
    move v0, v1

    #@5b
    .line 852
    goto :goto_19
.end method

.method protected onCleanUpAllConnections(Ljava/lang/String;)V
    .registers 4
    .parameter "cause"

    #@0
    .prologue
    .line 1154
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v0, p1, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpConnection(ZLjava/lang/String;Z)V

    #@5
    .line 1155
    return-void
.end method

.method protected onCleanUpConnection(ZILjava/lang/String;)V
    .registers 8
    .parameter "tearDown"
    .parameter "apnId"
    .parameter "reason"

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    const/4 v0, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 1136
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@a
    sget-boolean v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DONOT_DEACTIVATE_DUN_TYPE_KDDI:Z

    #@c
    if-ne v2, v0, :cond_2a

    #@e
    .line 1137
    const-string v0, "KDDI don\'t used 1x/EVDO all clean up "

    #@10
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@13
    .line 1138
    if-ne p2, v3, :cond_26

    #@15
    .line 1140
    const-string v0, "onCleanUpConnection: notify APN_DUN_ID of DISCONNECTED, do not cleanUpConnection"

    #@17
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@1a
    .line 1141
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1c
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->apnIdToType(I)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@22
    invoke-virtual {v0, p3, v1, v2}, Lcom/android/internal/telephony/PhoneBase;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/telephony/PhoneConstants$DataState;)V

    #@25
    .line 1149
    :goto_25
    return-void

    #@26
    .line 1144
    :cond_26
    invoke-virtual {p0, p1, p3, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpConnection(ZLjava/lang/String;Z)V

    #@29
    goto :goto_25

    #@2a
    .line 1148
    :cond_2a
    if-ne p2, v3, :cond_30

    #@2c
    :goto_2c
    invoke-virtual {p0, p1, p3, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpConnection(ZLjava/lang/String;Z)V

    #@2f
    goto :goto_25

    #@30
    :cond_30
    move v0, v1

    #@31
    goto :goto_2c
.end method

.method protected onDataConnectionAttached()V
    .registers 1

    #@0
    .prologue
    .line 1544
    return-void
.end method

.method protected onDataSetupComplete(Landroid/os/AsyncResult;)V
    .registers 15
    .parameter "ar"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v12, 0x1

    #@2
    .line 972
    const/4 v10, 0x0

    #@3
    .line 973
    .local v10, reason:Ljava/lang/String;
    iget-object v0, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@5
    instance-of v0, v0, Ljava/lang/String;

    #@7
    if-eqz v0, :cond_d

    #@9
    .line 974
    iget-object v10, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@b
    .end local v10           #reason:Ljava/lang/String;
    check-cast v10, Ljava/lang/String;

    #@d
    .line 977
    .restart local v10       #reason:Ljava/lang/String;
    :cond_d
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->isDataSetupCompleteOk(Landroid/os/AsyncResult;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_5d

    #@13
    .line 980
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@15
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1a
    move-result-object v0

    #@1b
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DISPLAY_DATAERROR_ICON_SPRINT:Z

    #@1d
    if-eqz v0, :cond_39

    #@1f
    .line 982
    const-string v0, "com.lge.android.data.DisplayDataErrorIcon: No Display (Dataconnection OK)"

    #@21
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@24
    .line 983
    new-instance v6, Landroid/content/Intent;

    #@26
    const-string v0, "com.lge.android.data.DisplayDataErrorIcon"

    #@28
    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2b
    .line 984
    .local v6, DisplayDataErrorIcon:Landroid/content/Intent;
    const-string v0, "Display"

    #@2d
    invoke-virtual {v6, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@30
    .line 985
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@32
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@35
    move-result-object v0

    #@36
    invoke-virtual {v0, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@39
    .line 991
    .end local v6           #DisplayDataErrorIcon:Landroid/content/Intent;
    :cond_39
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3b
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3d
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@40
    sget-boolean v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_INTENT_ON_CONNECTED_KDDI:Z

    #@42
    if-ne v0, v12, :cond_59

    #@44
    .line 992
    const-string v0, "1x/EVDO data call connected "

    #@46
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@49
    .line 993
    new-instance v9, Landroid/content/Intent;

    #@4b
    const-string v0, "lge.intent.action.1X_DATA_CONNECT"

    #@4d
    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@50
    .line 994
    .local v9, intent1xdata:Landroid/content/Intent;
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@52
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@55
    move-result-object v0

    #@56
    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@59
    .line 999
    .end local v9           #intent1xdata:Landroid/content/Intent;
    :cond_59
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDefaultData(Ljava/lang/String;)V

    #@5c
    .line 1059
    :goto_5c
    return-void

    #@5d
    .line 1001
    :cond_5d
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@5f
    check-cast v0, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@61
    move-object v8, v0

    #@62
    check-cast v8, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@64
    .line 1002
    .local v8, cause:Lcom/android/internal/telephony/DataConnection$FailCause;
    new-instance v0, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v1, "Data Connection setup failed "

    #@6b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v0

    #@6f
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v0

    #@77
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@7a
    .line 1004
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7c
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7e
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@81
    move-result-object v0

    #@82
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_RIL_CONN_HISTORY:Z

    #@84
    if-eqz v0, :cond_a3

    #@86
    .line 1006
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@88
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@8a
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getMyDebugger()Lcom/android/internal/telephony/MMdebuger;

    #@8d
    move-result-object v0

    #@8e
    const-string v1, "default"

    #@90
    const-string v2, "IP"

    #@92
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@94
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@97
    move-result-object v3

    #@98
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@9b
    move-result v3

    #@9c
    invoke-virtual {v8}, Lcom/android/internal/telephony/DataConnection$FailCause;->ordinal()I

    #@9f
    move-result v4

    #@a0
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/MMdebuger;->setFailHistory(Ljava/lang/String;Ljava/lang/String;III)V

    #@a3
    .line 1015
    :cond_a3
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a5
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@a7
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@aa
    sget-boolean v0, Lcom/android/internal/telephony/LGfeature;->mipErrorPopup:Z

    #@ac
    if-ne v0, v12, :cond_cd

    #@ae
    .line 1016
    const-string v0, "Data Setup Failed - Popup will appear"

    #@b0
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@b3
    .line 1018
    const v0, 0x4203b

    #@b6
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@b9
    move-result-object v7

    #@ba
    .line 1019
    .local v7, LteErrorCode:Landroid/os/Message;
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@bc
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@be
    invoke-interface {v0, v7}, Lcom/android/internal/telephony/CommandsInterface;->getLteEmmErrorCode(Landroid/os/Message;)V

    #@c1
    .line 1023
    sget-object v0, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@c3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->setState(Lcom/android/internal/telephony/DctConstants$State;)V

    #@c6
    .line 1024
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@c8
    const-string v1, "connectionMipErrorCheck"

    #@ca
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/PhoneBase;->notifyDataConnection(Ljava/lang/String;)V

    #@cd
    .line 1029
    .end local v7           #LteErrorCode:Landroid/os/Message;
    :cond_cd
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@cf
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@d1
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@d4
    move-result-object v0

    #@d5
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DISPLAY_DATAERROR_ICON_SPRINT:Z

    #@d7
    if-eqz v0, :cond_f7

    #@d9
    .line 1031
    iget-boolean v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@db
    if-nez v0, :cond_f7

    #@dd
    .line 1033
    const-string v0, "com.lge.android.data.DisplayDataErrorIcon: Display (Dataconnection fail)"

    #@df
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@e2
    .line 1034
    new-instance v6, Landroid/content/Intent;

    #@e4
    const-string v0, "com.lge.android.data.DisplayDataErrorIcon"

    #@e6
    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e9
    .line 1035
    .restart local v6       #DisplayDataErrorIcon:Landroid/content/Intent;
    const-string v0, "Display"

    #@eb
    invoke-virtual {v6, v0, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@ee
    .line 1036
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f0
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@f3
    move-result-object v0

    #@f4
    invoke-virtual {v0, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@f7
    .line 1042
    .end local v6           #DisplayDataErrorIcon:Landroid/content/Intent;
    :cond_f7
    invoke-virtual {v8}, Lcom/android/internal/telephony/DataConnection$FailCause;->isPermanentFail()Z

    #@fa
    move-result v0

    #@fb
    if-eqz v0, :cond_102

    #@fd
    .line 1043
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyNoData(Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@100
    goto/16 :goto_5c

    #@102
    .line 1047
    :cond_102
    const/4 v11, -0x1

    #@103
    .line 1048
    .local v11, retryOverride:I
    iget-object v0, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@105
    instance-of v0, v0, Lcom/android/internal/telephony/DataConnection$CallSetupException;

    #@107
    if-eqz v0, :cond_111

    #@109
    .line 1049
    iget-object v0, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@10b
    check-cast v0, Lcom/android/internal/telephony/DataConnection$CallSetupException;

    #@10d
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnection$CallSetupException;->getRetryOverride()I

    #@110
    move-result v11

    #@111
    .line 1052
    :cond_111
    const v0, 0x7fffffff

    #@114
    if-ne v11, v0, :cond_11d

    #@116
    .line 1053
    const-string v0, "No retry is suggested."

    #@118
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@11b
    goto/16 :goto_5c

    #@11d
    .line 1055
    :cond_11d
    invoke-direct {p0, v8, v10, v11}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->startDelayedRetry(Lcom/android/internal/telephony/DataConnection$FailCause;Ljava/lang/String;I)V

    #@120
    .line 1056
    const-string v0, "startDelayedRetry"

    #@122
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@125
    goto/16 :goto_5c
.end method

.method protected onDataStateChanged(Landroid/os/AsyncResult;)V
    .registers 13
    .parameter "ar"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 1287
    iget-object v7, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4
    check-cast v7, Ljava/util/ArrayList;

    #@6
    move-object v1, v7

    #@7
    check-cast v1, Ljava/util/ArrayList;

    #@9
    .line 1289
    .local v1, dataCallStates:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataCallState;>;"
    iget-object v7, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@b
    if-eqz v7, :cond_e

    #@d
    .line 1376
    :goto_d
    return-void

    #@e
    .line 1296
    :cond_e
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@10
    sget-object v8, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@12
    if-ne v7, v8, :cond_112

    #@14
    .line 1297
    const/4 v5, 0x0

    #@15
    .line 1298
    .local v5, isActiveOrDormantConnectionPresent:Z
    const/4 v0, 0x0

    #@16
    .line 1302
    .local v0, connectionState:I
    const/4 v4, 0x0

    #@17
    .local v4, index:I
    :goto_17
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@1a
    move-result v7

    #@1b
    if-ge v4, v7, :cond_3f

    #@1d
    .line 1303
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v2

    #@21
    check-cast v2, Lcom/android/internal/telephony/DataCallState;

    #@23
    .line 1304
    .local v2, dcState:Lcom/android/internal/telephony/DataCallState;
    iget v0, v2, Lcom/android/internal/telephony/DataCallState;->active:I

    #@25
    .line 1305
    if-eqz v0, :cond_9d

    #@27
    .line 1306
    const/4 v5, 0x1

    #@28
    .line 1308
    iget v7, v2, Lcom/android/internal/telephony/DataCallState;->cid:I

    #@2a
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->findDataConnectionAcByCid(I)Lcom/android/internal/telephony/DataConnectionAc;

    #@2d
    move-result-object v3

    #@2e
    .line 1309
    .local v3, dcac:Lcom/android/internal/telephony/DataConnectionAc;
    if-eqz v3, :cond_3f

    #@30
    .line 1311
    invoke-virtual {v3, v2}, Lcom/android/internal/telephony/DataConnectionAc;->updateLinkPropertiesDataCallStateSync(Lcom/android/internal/telephony/DataCallState;)Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;

    #@33
    move-result-object v6

    #@34
    .line 1313
    .local v6, result:Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    iget-object v7, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->setupResult:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@36
    sget-object v8, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_Stale:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@38
    if-ne v7, v8, :cond_4e

    #@3a
    .line 1314
    const-string v7, "onDataStateChanged(ar): state is Inactive no changes"

    #@3c
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@3f
    .line 1342
    .end local v2           #dcState:Lcom/android/internal/telephony/DataCallState;
    .end local v3           #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    .end local v6           #result:Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    :cond_3f
    :goto_3f
    if-nez v5, :cond_cb

    #@41
    .line 1344
    const-string v7, "onDataStateChanged: No active connectionstate is CONNECTED, disconnecting/cleanup"

    #@43
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@46
    .line 1346
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->writeEventLogCdmaDataDrop()V

    #@49
    .line 1347
    const/4 v7, 0x0

    #@4a
    invoke-virtual {p0, v10, v7, v9}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpConnection(ZLjava/lang/String;Z)V

    #@4d
    goto :goto_d

    #@4e
    .line 1315
    .restart local v2       #dcState:Lcom/android/internal/telephony/DataCallState;
    .restart local v3       #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    .restart local v6       #result:Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    :cond_4e
    iget-object v7, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->oldLp:Landroid/net/LinkProperties;

    #@50
    iget-object v8, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@52
    invoke-virtual {v7, v8}, Landroid/net/LinkProperties;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v7

    #@56
    if-eqz v7, :cond_5e

    #@58
    .line 1316
    const-string v7, "onDataStateChanged(ar): no change"

    #@5a
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@5d
    goto :goto_3f

    #@5e
    .line 1318
    :cond_5e
    const-string v7, "onDataStateChanged(ar): there is a change"

    #@60
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@63
    .line 1319
    iget-object v7, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->oldLp:Landroid/net/LinkProperties;

    #@65
    iget-object v8, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@67
    invoke-virtual {v7, v8}, Landroid/net/LinkProperties;->isIdenticalInterfaceName(Landroid/net/LinkProperties;)Z

    #@6a
    move-result v7

    #@6b
    if-eqz v7, :cond_3f

    #@6d
    .line 1320
    iget-object v7, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->oldLp:Landroid/net/LinkProperties;

    #@6f
    iget-object v8, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@71
    invoke-virtual {v7, v8}, Landroid/net/LinkProperties;->isIdenticalDnses(Landroid/net/LinkProperties;)Z

    #@74
    move-result v7

    #@75
    if-eqz v7, :cond_95

    #@77
    iget-object v7, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->oldLp:Landroid/net/LinkProperties;

    #@79
    iget-object v8, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@7b
    invoke-virtual {v7, v8}, Landroid/net/LinkProperties;->isIdenticalRoutes(Landroid/net/LinkProperties;)Z

    #@7e
    move-result v7

    #@7f
    if-eqz v7, :cond_95

    #@81
    iget-object v7, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->oldLp:Landroid/net/LinkProperties;

    #@83
    iget-object v8, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@85
    invoke-virtual {v7, v8}, Landroid/net/LinkProperties;->isIdenticalHttpProxy(Landroid/net/LinkProperties;)Z

    #@88
    move-result v7

    #@89
    if-eqz v7, :cond_95

    #@8b
    iget-object v7, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->oldLp:Landroid/net/LinkProperties;

    #@8d
    iget-object v8, v6, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@8f
    invoke-virtual {v7, v8}, Landroid/net/LinkProperties;->isIdenticalAddresses(Landroid/net/LinkProperties;)Z

    #@92
    move-result v7

    #@93
    if-nez v7, :cond_3f

    #@95
    .line 1324
    :cond_95
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@97
    const-string v8, "linkPropertiesChanged"

    #@99
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/PhoneBase;->notifyDataConnection(Ljava/lang/String;)V

    #@9c
    goto :goto_3f

    #@9d
    .line 1334
    .end local v3           #dcac:Lcom/android/internal/telephony/DataConnectionAc;
    .end local v6           #result:Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    :cond_9d
    iget v7, v2, Lcom/android/internal/telephony/DataCallState;->status:I

    #@9f
    invoke-static {v7}, Lcom/android/internal/telephony/DataConnection$FailCause;->fromInt(I)Lcom/android/internal/telephony/DataConnection$FailCause;

    #@a2
    move-result-object v7

    #@a3
    sget-object v8, Lcom/android/internal/telephony/DataConnection$FailCause;->TETHERED_CALL_ACTIVE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@a5
    if-ne v7, v8, :cond_c7

    #@a7
    .line 1336
    new-instance v7, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v8, "setTetheredCallOn for apn:"

    #@ae
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v7

    #@b2
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@b4
    invoke-virtual {v8}, Lcom/android/internal/telephony/DataProfile;->toString()Ljava/lang/String;

    #@b7
    move-result-object v8

    #@b8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v7

    #@bc
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v7

    #@c0
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@c3
    .line 1337
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@c5
    iput-boolean v10, v7, Lcom/android/internal/telephony/DataProfile;->mTetheredCallOn:Z

    #@c7
    .line 1302
    :cond_c7
    add-int/lit8 v4, v4, 0x1

    #@c9
    goto/16 :goto_17

    #@cb
    .line 1351
    .end local v2           #dcState:Lcom/android/internal/telephony/DataCallState;
    :cond_cb
    packed-switch v0, :pswitch_data_132

    #@ce
    .line 1369
    new-instance v7, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v8, "onDataStateChanged: IGNORE unexpected DataCallState.active="

    #@d5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v7

    #@d9
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v7

    #@dd
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v7

    #@e1
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@e4
    goto/16 :goto_d

    #@e6
    .line 1353
    :pswitch_e6
    const-string v7, "onDataStateChanged: active=LINK_ACTIVE && CONNECTED, ignore"

    #@e8
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@eb
    .line 1354
    sget-object v7, Lcom/android/internal/telephony/DctConstants$Activity;->NONE:Lcom/android/internal/telephony/DctConstants$Activity;

    #@ed
    iput-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActivity:Lcom/android/internal/telephony/DctConstants$Activity;

    #@ef
    .line 1355
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f1
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneBase;->notifyDataActivity()V

    #@f4
    .line 1356
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->startNetStatPoll()V

    #@f7
    .line 1357
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->startDataStallAlarm(Z)V

    #@fa
    goto/16 :goto_d

    #@fc
    .line 1361
    :pswitch_fc
    const-string v7, "onDataStateChanged active=LINK_DOWN && CONNECTED, dormant"

    #@fe
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@101
    .line 1362
    sget-object v7, Lcom/android/internal/telephony/DctConstants$Activity;->DORMANT:Lcom/android/internal/telephony/DctConstants$Activity;

    #@103
    iput-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActivity:Lcom/android/internal/telephony/DctConstants$Activity;

    #@105
    .line 1363
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@107
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneBase;->notifyDataActivity()V

    #@10a
    .line 1364
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->stopNetStatPoll()V

    #@10d
    .line 1365
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->stopDataStallAlarm()V

    #@110
    goto/16 :goto_d

    #@112
    .line 1374
    .end local v0           #connectionState:I
    .end local v4           #index:I
    .end local v5           #isActiveOrDormantConnectionPresent:Z
    :cond_112
    new-instance v7, Ljava/lang/StringBuilder;

    #@114
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@117
    const-string v8, "onDataStateChanged: not connected, state="

    #@119
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v7

    #@11d
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@11f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v7

    #@123
    const-string v8, " ignoring"

    #@125
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v7

    #@129
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12c
    move-result-object v7

    #@12d
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@130
    goto/16 :goto_d

    #@132
    .line 1351
    :pswitch_data_132
    .packed-switch 0x1
        :pswitch_fc
        :pswitch_e6
    .end packed-switch
.end method

.method protected onDisconnectDone(ILandroid/os/AsyncResult;)V
    .registers 7
    .parameter "connId"
    .parameter "ar"

    #@0
    .prologue
    .line 1066
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "EVENT_DISCONNECT_DONE connId="

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@16
    .line 1067
    const/4 v0, 0x0

    #@17
    .line 1068
    .local v0, reason:Ljava/lang/String;
    iget-object v2, p2, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@19
    instance-of v2, v2, Ljava/lang/String;

    #@1b
    if-eqz v2, :cond_21

    #@1d
    .line 1069
    iget-object v0, p2, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@1f
    .end local v0           #reason:Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    #@21
    .line 1071
    .restart local v0       #reason:Ljava/lang/String;
    :cond_21
    sget-object v2, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@23
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->setState(Lcom/android/internal/telephony/DctConstants$State;)V

    #@26
    .line 1075
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingRestartRadio:Z

    #@28
    if-eqz v2, :cond_30

    #@2a
    const v2, 0x4201a

    #@2d
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->removeMessages(I)V

    #@30
    .line 1079
    :cond_30
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@32
    iget-object v1, v2, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@34
    .line 1080
    .local v1, ssTracker:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->processPendingRadioPowerOffAfterDataOff()Z

    #@37
    move-result v2

    #@38
    if-eqz v2, :cond_54

    #@3a
    .line 1081
    const/4 v2, 0x0

    #@3b
    iput-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingRestartRadio:Z

    #@3d
    .line 1086
    :goto_3d
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@40
    .line 1087
    const/4 v2, 0x0

    #@41
    iput-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mActiveApn:Lcom/android/internal/telephony/DataProfile;

    #@43
    .line 1088
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@45
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->clearActiveDataProfile()V

    #@48
    .line 1089
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->retryAfterDisconnected(Ljava/lang/String;)Z

    #@4b
    move-result v2

    #@4c
    if-eqz v2, :cond_53

    #@4e
    .line 1091
    sget v2, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->APN_DELAY_MILLIS:I

    #@50
    invoke-direct {p0, v2, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->startAlarmForReconnect(ILjava/lang/String;)V

    #@53
    .line 1093
    :cond_53
    return-void

    #@54
    .line 1083
    :cond_54
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->onRestartRadio()V

    #@57
    goto :goto_3d
.end method

.method protected onEnableNewApn()V
    .registers 4

    #@0
    .prologue
    .line 886
    const/4 v0, 0x1

    #@1
    const-string v1, "apnSwitched"

    #@3
    const/4 v2, 0x0

    #@4
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpConnection(ZLjava/lang/String;Z)V

    #@7
    .line 887
    return-void
.end method

.method protected onNVReady()V
    .registers 2

    #@0
    .prologue
    .line 839
    const-string v0, "NvReady"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->triggerDataSetup(Ljava/lang/String;)V

    #@5
    .line 840
    return-void
.end method

.method protected onRadioAvailable()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 934
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getSimulatedRadioControl()Lcom/android/internal/telephony/test/SimulatedRadioControl;

    #@6
    move-result-object v0

    #@7
    if-eqz v0, :cond_16

    #@9
    .line 937
    sget-object v0, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@b
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->setState(Lcom/android/internal/telephony/DctConstants$State;)V

    #@e
    .line 938
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@11
    .line 940
    const-string v0, "We\'re on the simulator; assuming data is connected"

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@16
    .line 943
    :cond_16
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@19
    .line 945
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@1b
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@1d
    if-eq v0, v1, :cond_22

    #@1f
    .line 946
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@22
    .line 948
    :cond_22
    return-void
.end method

.method protected onRadioOffOrNotAvailable()V
    .registers 3

    #@0
    .prologue
    .line 955
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Lcom/android/internal/telephony/DataConnection;

    #@d
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnection;->resetRetryCount()V

    #@10
    .line 957
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@12
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getSimulatedRadioControl()Lcom/android/internal/telephony/test/SimulatedRadioControl;

    #@15
    move-result-object v0

    #@16
    if-eqz v0, :cond_1e

    #@18
    .line 960
    const-string v0, "We\'re on the simulator; assuming radio off is meaningless"

    #@1a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@1d
    .line 965
    :goto_1d
    return-void

    #@1e
    .line 962
    :cond_1e
    const-string v0, "Radio is off and clean up all connection"

    #@20
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@23
    .line 963
    const/4 v0, 0x0

    #@24
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@27
    goto :goto_1d
.end method

.method protected onRecordsLoaded()V
    .registers 4

    #@0
    .prologue
    .line 810
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->setWindowBuferSize()V

    #@3
    .line 812
    const-string v1, "CDMA"

    #@5
    const-string v2, "OMH: onRecordsLoaded(): calling readDataProfilesFromModem()"

    #@7
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 815
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@c
    if-eqz v1, :cond_26

    #@e
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@10
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@12
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@15
    move-result-object v1

    #@16
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_ON_CDMA:Z

    #@18
    if-eqz v1, :cond_26

    #@1a
    .line 816
    const-string v1, "CDMA"

    #@1c
    const-string v2, "onRecordsLoaded(): calling readDataProfileFromDb"

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 817
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@23
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->readDataProfileFromDb()V

    #@26
    .line 823
    :cond_26
    const/4 v0, 0x0

    #@27
    .line 824
    .local v0, needModemProfiles:Z
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@29
    if-eqz v1, :cond_31

    #@2b
    .line 826
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mDpt:Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;

    #@2d
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->readDataProfilesFromModem()Z

    #@30
    move-result v0

    #@31
    .line 829
    :cond_31
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@33
    sget-object v2, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@35
    if-ne v1, v2, :cond_3b

    #@37
    .line 830
    const/4 v1, 0x0

    #@38
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@3b
    .line 833
    :cond_3b
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3d
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3f
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@42
    move-result-object v1

    #@43
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_TRYSETUP_AFTER_SIM_LOADED_ON_CDMA:Z

    #@45
    if-eqz v1, :cond_4c

    #@47
    .line 834
    const-string v1, "simLoaded"

    #@49
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->triggerDataSetup(Ljava/lang/String;)V

    #@4c
    .line 837
    :cond_4c
    return-void
.end method

.method protected onRoamingOff()V
    .registers 2

    #@0
    .prologue
    .line 902
    iget-boolean v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 910
    :goto_4
    return-void

    #@5
    .line 904
    :cond_5
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_16

    #@b
    .line 905
    const-string v0, "roamingOff"

    #@d
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@10
    .line 906
    const-string v0, "roamingOff"

    #@12
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->trySetupData(Ljava/lang/String;)Z

    #@15
    goto :goto_4

    #@16
    .line 908
    :cond_16
    const-string v0, "roamingOff"

    #@18
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@1b
    goto :goto_4
.end method

.method protected onRoamingOn()V
    .registers 2

    #@0
    .prologue
    .line 917
    iget-boolean v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUserDataEnabled:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 927
    :goto_4
    return-void

    #@5
    .line 919
    :cond_5
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_16

    #@b
    .line 920
    const-string v0, "roamingOn"

    #@d
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->trySetupData(Ljava/lang/String;)Z

    #@10
    .line 921
    const-string v0, "roamingOn"

    #@12
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@15
    goto :goto_4

    #@16
    .line 923
    :cond_16
    const-string v0, "Tear down data connection on roaming."

    #@18
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@1b
    .line 924
    const/4 v0, 0x0

    #@1c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@1f
    .line 925
    const-string v0, "roamingOn"

    #@21
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@24
    goto :goto_4
.end method

.method protected onTrySetupData(Lcom/android/internal/telephony/ApnContext;)Z
    .registers 3
    .parameter "apnContext"

    #@0
    .prologue
    .line 1548
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onTrySetupData(Ljava/lang/String;)Z
    .registers 3
    .parameter "reason"

    #@0
    .prologue
    .line 894
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->trySetupData(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected onUpdateIcc()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1494
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1529
    :cond_5
    :goto_5
    return-void

    #@6
    .line 1498
    :cond_6
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->getUiccCardApplication()Lcom/android/internal/telephony/uicc/IccRecords;

    #@9
    move-result-object v0

    #@a
    .line 1500
    .local v0, newIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;
    if-nez v0, :cond_1a

    #@c
    .line 1503
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@e
    const/4 v3, 0x1

    #@f
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/uicc/UiccController;->getIccRecords(I)Lcom/android/internal/telephony/uicc/IccRecords;

    #@12
    move-result-object v0

    #@13
    .line 1504
    if-eqz v0, :cond_5

    #@15
    .line 1509
    const-string v2, "newIccRecords are assigned to 3GPP records."

    #@17
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@1a
    .line 1515
    :cond_1a
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@1c
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Lcom/android/internal/telephony/uicc/IccRecords;

    #@22
    .line 1516
    .local v1, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eq v1, v0, :cond_5

    #@24
    .line 1517
    if-eqz v1, :cond_33

    #@26
    .line 1518
    const-string v2, "Removing stale icc objects."

    #@28
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@2b
    .line 1519
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@2e
    .line 1520
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@30
    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@33
    .line 1522
    :cond_33
    if-eqz v0, :cond_5

    #@35
    .line 1523
    const-string v2, "New records found"

    #@37
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@3a
    .line 1524
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@3c
    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@3f
    .line 1525
    const v2, 0x42002

    #@42
    invoke-virtual {v0, p0, v2, v4}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@45
    goto :goto_5
.end method

.method protected onVoiceCallEnded()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1114
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@3
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@5
    if-ne v0, v1, :cond_26

    #@7
    .line 1115
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@9
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@b
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isConcurrentVoiceAndDataAllowed()Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_22

    #@11
    .line 1116
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->startNetStatPoll()V

    #@14
    .line 1117
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->startDataStallAlarm(Z)V

    #@17
    .line 1118
    const-string v0, "2GVoiceCallEnded"

    #@19
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@1c
    .line 1123
    :goto_1c
    const-string v0, "2GVoiceCallEnded"

    #@1e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@21
    .line 1129
    :goto_21
    return-void

    #@22
    .line 1121
    :cond_22
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->resetPollStats()V

    #@25
    goto :goto_1c

    #@26
    .line 1125
    :cond_26
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mDataConnections:Ljava/util/HashMap;

    #@28
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v0

    #@30
    check-cast v0, Lcom/android/internal/telephony/DataConnection;

    #@32
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnection;->resetRetryCount()V

    #@35
    .line 1127
    const-string v0, "2GVoiceCallEnded"

    #@37
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->trySetupData(Ljava/lang/String;)Z

    #@3a
    goto :goto_21
.end method

.method protected onVoiceCallStarted()V
    .registers 3

    #@0
    .prologue
    .line 1100
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@2
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@4
    if-ne v0, v1, :cond_20

    #@6
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mCdmaPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@8
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isConcurrentVoiceAndDataAllowed()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_20

    #@10
    .line 1102
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->stopNetStatPoll()V

    #@13
    .line 1103
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->stopDataStallAlarm()V

    #@16
    .line 1104
    const-string v0, "2GVoiceCallStarted"

    #@18
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyDataConnection(Ljava/lang/String;)V

    #@1b
    .line 1105
    const-string v0, "2GVoiceCallStarted"

    #@1d
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->notifyOffApnsOfAvailability(Ljava/lang/String;)V

    #@20
    .line 1107
    :cond_20
    return-void
.end method

.method public on_PPP_RESYNC_Ehrpd_Internet_Ipv6_block_requested()V
    .registers 1

    #@0
    .prologue
    .line 1650
    return-void
.end method

.method protected restartDataStallAlarm()V
    .registers 1

    #@0
    .prologue
    .line 240
    return-void
.end method

.method protected restartRadio()V
    .registers 4

    #@0
    .prologue
    .line 722
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Cleanup connection and wait "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget v1, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->TIME_DELAYED_TO_RESTART_RADIO:I

    #@d
    div-int/lit16 v1, v1, 0x3e8

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, "s to restart radio"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@20
    .line 724
    const/4 v0, 0x0

    #@21
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@24
    .line 725
    const v0, 0x4201a

    #@27
    sget v1, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->TIME_DELAYED_TO_RESTART_RADIO:I

    #@29
    int-to-long v1, v1

    #@2a
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->sendEmptyMessageDelayed(IJ)Z

    #@2d
    .line 726
    const/4 v0, 0x1

    #@2e
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->mPendingRestartRadio:Z

    #@30
    .line 727
    return-void
.end method

.method public setDataConnection(Z)V
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 1718
    const/4 v0, 0x0

    #@1
    .line 1720
    .local v0, msg:Landroid/os/Message;
    if-eqz p1, :cond_12

    #@3
    .line 1721
    const v1, 0x42003

    #@6
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    .line 1722
    const-string v1, "dataEnabled"

    #@c
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e
    .line 1730
    :goto_e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 1731
    return-void

    #@12
    .line 1724
    :cond_12
    const v1, 0x42018

    #@15
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@18
    move-result-object v0

    #@19
    .line 1725
    const/4 v1, 0x1

    #@1a
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@1c
    .line 1727
    const-string v1, "connectionManagerHandleData"

    #@1e
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20
    goto :goto_e
.end method

.method public setIMSRegistate(Z)V
    .registers 4
    .parameter "Registate"

    #@0
    .prologue
    .line 1576
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IMS_AFW] setIMSRegistate - Registate : "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@16
    .line 1577
    return-void
.end method

.method protected setState(Lcom/android/internal/telephony/DctConstants$State;)V
    .registers 6
    .parameter "s"

    #@0
    .prologue
    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "setState: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;->log(Ljava/lang/String;)V

    #@16
    .line 245
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@18
    if-eq v0, p1, :cond_35

    #@1a
    .line 246
    const v0, 0xc3c3

    #@1d
    const/4 v1, 0x2

    #@1e
    new-array v1, v1, [Ljava/lang/Object;

    #@20
    const/4 v2, 0x0

    #@21
    iget-object v3, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@23
    invoke-virtual {v3}, Lcom/android/internal/telephony/DctConstants$State;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    aput-object v3, v1, v2

    #@29
    const/4 v2, 0x1

    #@2a
    invoke-virtual {p1}, Lcom/android/internal/telephony/DctConstants$State;->toString()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    aput-object v3, v1, v2

    #@30
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@33
    .line 248
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@35
    .line 250
    :cond_35
    return-void
.end method
