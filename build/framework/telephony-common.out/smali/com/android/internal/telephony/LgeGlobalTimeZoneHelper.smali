.class public final Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;
.super Ljava/lang/Object;
.source "LgeGlobalTimeZoneHelper.java"


# static fields
.field private static final AREA_CODE_AFRI:I = 0x1

.field private static final AREA_CODE_ANTA:I = 0x2

.field private static final AREA_CODE_ASIA:I = 0x3

.field private static final AREA_CODE_AUST:I = 0x4

.field private static final AREA_CODE_EURO:I = 0x5

.field private static final AREA_CODE_GLOBAL:I = 0x8

.field private static final AREA_CODE_NORTH:I = 0x6

.field private static final AREA_CODE_SOUTH:I = 0x7

.field private static final AREA_CODE_UNDEFINED:I = 0x0

.field private static final BUILTIN_OEMTZ_PATH:[Ljava/lang/String; = null

.field private static final IND_CODES:[I = null

.field private static final LOG_TAG:Ljava/lang/String; = "CALL_FRW"

.field private static final MCC_CODES:[S = null

.field private static final TIMEZONE_PROPERTY:Ljava/lang/String; = "persist.sys.timezone"

.field private static final TIMEZONE_TZFOCUS:Ljava/lang/String; = "ril.gsm.tzfocus"

.field private static final UPDATED_USERTZ_PATH:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 29
    const/16 v0, 0xec

    #@7
    new-array v0, v0, [S

    #@9
    fill-array-data v0, :array_76

    #@c
    sput-object v0, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->MCC_CODES:[S

    #@e
    .line 56
    const/16 v0, 0xec

    #@10
    new-array v0, v0, [I

    #@12
    fill-array-data v0, :array_166

    #@15
    sput-object v0, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->IND_CODES:[I

    #@17
    .line 83
    const/16 v0, 0x9

    #@19
    new-array v0, v0, [Ljava/lang/String;

    #@1b
    const-string v1, "etc/tz/tz_undefined.xml"

    #@1d
    aput-object v1, v0, v3

    #@1f
    const-string v1, "etc/tz/tz_af.xml"

    #@21
    aput-object v1, v0, v4

    #@23
    const-string v1, "etc/tz/tz_an.xml"

    #@25
    aput-object v1, v0, v5

    #@27
    const-string v1, "etc/tz/tz_as.xml"

    #@29
    aput-object v1, v0, v6

    #@2b
    const-string v1, "etc/tz/tz_au.xml"

    #@2d
    aput-object v1, v0, v7

    #@2f
    const/4 v1, 0x5

    #@30
    const-string v2, "etc/tz/tz_eu.xml"

    #@32
    aput-object v2, v0, v1

    #@34
    const/4 v1, 0x6

    #@35
    const-string v2, "etc/tz/tz_no.xml"

    #@37
    aput-object v2, v0, v1

    #@39
    const/4 v1, 0x7

    #@3a
    const-string v2, "etc/tz/tz_so.xml"

    #@3c
    aput-object v2, v0, v1

    #@3e
    const/16 v1, 0x8

    #@40
    const-string v2, "etc/tz/tz_gl.xml"

    #@42
    aput-object v2, v0, v1

    #@44
    sput-object v0, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->BUILTIN_OEMTZ_PATH:[Ljava/lang/String;

    #@46
    .line 95
    const/16 v0, 0x9

    #@48
    new-array v0, v0, [Ljava/lang/String;

    #@4a
    const-string v1, "shared/tz/tz_undefined.xml"

    #@4c
    aput-object v1, v0, v3

    #@4e
    const-string v1, "shared/tz/tz_af.xml"

    #@50
    aput-object v1, v0, v4

    #@52
    const-string v1, "shared/tz/tz_an.xml"

    #@54
    aput-object v1, v0, v5

    #@56
    const-string v1, "shared/tz/tz_as.xml"

    #@58
    aput-object v1, v0, v6

    #@5a
    const-string v1, "shared/tz/tz_au.xml"

    #@5c
    aput-object v1, v0, v7

    #@5e
    const/4 v1, 0x5

    #@5f
    const-string v2, "shared/tz/tz_eu.xml"

    #@61
    aput-object v2, v0, v1

    #@63
    const/4 v1, 0x6

    #@64
    const-string v2, "shared/tz/tz_no.xml"

    #@66
    aput-object v2, v0, v1

    #@68
    const/4 v1, 0x7

    #@69
    const-string v2, "shared/tz/tz_so.xml"

    #@6b
    aput-object v2, v0, v1

    #@6d
    const/16 v1, 0x8

    #@6f
    const-string v2, "shared/tz/tz_gl.xml"

    #@71
    aput-object v2, v0, v1

    #@73
    sput-object v0, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->UPDATED_USERTZ_PATH:[Ljava/lang/String;

    #@75
    return-void

    #@76
    .line 29
    :array_76
    .array-data 0x2
        0xcat 0x0t
        0xcct 0x0t
        0xcet 0x0t
        0xd0t 0x0t
        0xd4t 0x0t
        0xd5t 0x0t
        0xd6t 0x0t
        0xd8t 0x0t
        0xdat 0x0t
        0xdbt 0x0t
        0xdct 0x0t
        0xdet 0x0t
        0xe1t 0x0t
        0xe2t 0x0t
        0xe4t 0x0t
        0xe6t 0x0t
        0xe7t 0x0t
        0xe8t 0x0t
        0xeat 0x0t
        0xebt 0x0t
        0xeet 0x0t
        0xf0t 0x0t
        0xf2t 0x0t
        0xf4t 0x0t
        0xf6t 0x0t
        0xf7t 0x0t
        0xf8t 0x0t
        0xfat 0x0t
        0xfft 0x0t
        0x1t 0x1t
        0x3t 0x1t
        0x4t 0x1t
        0x6t 0x1t
        0xat 0x1t
        0xct 0x1t
        0xet 0x1t
        0x10t 0x1t
        0x12t 0x1t
        0x14t 0x1t
        0x16t 0x1t
        0x18t 0x1t
        0x1at 0x1t
        0x1bt 0x1t
        0x1ct 0x1t
        0x1et 0x1t
        0x20t 0x1t
        0x21t 0x1t
        0x22t 0x1t
        0x24t 0x1t
        0x25t 0x1t
        0x26t 0x1t
        0x27t 0x1t
        0x29t 0x1t
        0x2et 0x1t
        0x34t 0x1t
        0x36t 0x1t
        0x37t 0x1t
        0x38t 0x1t
        0x39t 0x1t
        0x3at 0x1t
        0x3bt 0x1t
        0x3ct 0x1t
        0x4at 0x1t
        0x4ct 0x1t
        0x4et 0x1t
        0x52t 0x1t
        0x54t 0x1t
        0x56t 0x1t
        0x58t 0x1t
        0x5at 0x1t
        0x5ct 0x1t
        0x5et 0x1t
        0x60t 0x1t
        0x62t 0x1t
        0x64t 0x1t
        0x66t 0x1t
        0x68t 0x1t
        0x6at 0x1t
        0x6bt 0x1t
        0x6ct 0x1t
        0x6dt 0x1t
        0x6et 0x1t
        0x70t 0x1t
        0x72t 0x1t
        0x74t 0x1t
        0x76t 0x1t
        0x78t 0x1t
        0x90t 0x1t
        0x91t 0x1t
        0x92t 0x1t
        0x94t 0x1t
        0x95t 0x1t
        0x9at 0x1t
        0x9ct 0x1t
        0x9dt 0x1t
        0x9et 0x1t
        0x9ft 0x1t
        0xa0t 0x1t
        0xa1t 0x1t
        0xa2t 0x1t
        0xa3t 0x1t
        0xa4t 0x1t
        0xa5t 0x1t
        0xa6t 0x1t
        0xa7t 0x1t
        0xa8t 0x1t
        0xa9t 0x1t
        0xaat 0x1t
        0xabt 0x1t
        0xact 0x1t
        0xadt 0x1t
        0xaet 0x1t
        0xaft 0x1t
        0xb0t 0x1t
        0xb2t 0x1t
        0xb4t 0x1t
        0xb5t 0x1t
        0xb6t 0x1t
        0xb8t 0x1t
        0xb9t 0x1t
        0xc2t 0x1t
        0xc4t 0x1t
        0xc6t 0x1t
        0xc7t 0x1t
        0xc8t 0x1t
        0xc9t 0x1t
        0xcct 0x1t
        0xcdt 0x1t
        0xd2t 0x1t
        0xd3t 0x1t
        0xd6t 0x1t
        0xd8t 0x1t
        0xf6t 0x1t
        0xf9t 0x1t
        0xfet 0x1t
        0x2t 0x2t
        0x3t 0x2t
        0x8t 0x2t
        0xdt 0x2t
        0x10t 0x2t
        0x12t 0x2t
        0x16t 0x2t
        0x17t 0x2t
        0x18t 0x2t
        0x19t 0x2t
        0x1bt 0x2t
        0x1ct 0x2t
        0x1dt 0x2t
        0x1et 0x2t
        0x1ft 0x2t
        0x20t 0x2t
        0x21t 0x2t
        0x22t 0x2t
        0x23t 0x2t
        0x24t 0x2t
        0x25t 0x2t
        0x26t 0x2t
        0x27t 0x2t
        0x28t 0x2t
        0x29t 0x2t
        0x2bt 0x2t
        0x5at 0x2t
        0x5bt 0x2t
        0x5ct 0x2t
        0x5dt 0x2t
        0x5et 0x2t
        0x5ft 0x2t
        0x60t 0x2t
        0x61t 0x2t
        0x62t 0x2t
        0x63t 0x2t
        0x64t 0x2t
        0x65t 0x2t
        0x66t 0x2t
        0x67t 0x2t
        0x68t 0x2t
        0x69t 0x2t
        0x6at 0x2t
        0x6bt 0x2t
        0x6ct 0x2t
        0x6dt 0x2t
        0x6et 0x2t
        0x6ft 0x2t
        0x70t 0x2t
        0x71t 0x2t
        0x72t 0x2t
        0x73t 0x2t
        0x74t 0x2t
        0x75t 0x2t
        0x76t 0x2t
        0x77t 0x2t
        0x78t 0x2t
        0x79t 0x2t
        0x7at 0x2t
        0x7bt 0x2t
        0x7ct 0x2t
        0x7dt 0x2t
        0x7et 0x2t
        0x7ft 0x2t
        0x80t 0x2t
        0x81t 0x2t
        0x82t 0x2t
        0x83t 0x2t
        0x85t 0x2t
        0x86t 0x2t
        0x87t 0x2t
        0x88t 0x2t
        0x89t 0x2t
        0x8at 0x2t
        0x8bt 0x2t
        0x8ct 0x2t
        0x8dt 0x2t
        0x8et 0x2t
        0x8ft 0x2t
        0x91t 0x2t
        0xbet 0x2t
        0xc0t 0x2t
        0xc2t 0x2t
        0xc4t 0x2t
        0xc6t 0x2t
        0xc8t 0x2t
        0xcat 0x2t
        0xcct 0x2t
        0xd2t 0x2t
        0xd4t 0x2t
        0xdat 0x2t
        0xdct 0x2t
        0xdet 0x2t
        0xe0t 0x2t
        0xe2t 0x2t
        0xe4t 0x2t
        0xe6t 0x2t
        0xe8t 0x2t
        0xeat 0x2t
        0xect 0x2t
        0xeet 0x2t
    .end array-data

    #@166
    .line 56
    :array_166
    .array-data 0x4
        0x5t 0x4t 0x72t 0x67t
        0x5t 0x4t 0x6ct 0x6et
        0x5t 0x4t 0x65t 0x62t
        0x5t 0x4t 0x72t 0x66t
        0x5t 0x4t 0x63t 0x6dt
        0x5t 0x4t 0x64t 0x61t
        0x5t 0x4t 0x73t 0x65t
        0x5t 0x4t 0x75t 0x68t
        0x5t 0x4t 0x61t 0x62t
        0x5t 0x4t 0x72t 0x68t
        0x5t 0x4t 0x73t 0x72t
        0x5t 0x4t 0x74t 0x69t
        0x5t 0x4t 0x61t 0x76t
        0x5t 0x4t 0x6ft 0x72t
        0x5t 0x4t 0x68t 0x63t
        0x5t 0x4t 0x7at 0x63t
        0x5t 0x4t 0x6bt 0x73t
        0x5t 0x4t 0x74t 0x61t
        0x5t 0x4t 0x62t 0x67t
        0x5t 0x4t 0x62t 0x67t
        0x5t 0x4t 0x6bt 0x64t
        0x5t 0x4t 0x65t 0x73t
        0x5t 0x4t 0x6ft 0x6et
        0x5t 0x4t 0x69t 0x66t
        0x5t 0x4t 0x74t 0x6ct
        0x5t 0x4t 0x76t 0x6ct
        0x5t 0x4t 0x65t 0x65t
        0x5t 0x4t 0x75t 0x72t
        0x5t 0x4t 0x61t 0x75t
        0x5t 0x4t 0x79t 0x62t
        0x5t 0x4t 0x64t 0x6dt
        0x5t 0x4t 0x6ct 0x70t
        0x5t 0x4t 0x65t 0x64t
        0x5t 0x4t 0x69t 0x67t
        0x5t 0x4t 0x74t 0x70t
        0x5t 0x4t 0x75t 0x6ct
        0x5t 0x4t 0x65t 0x69t
        0x5t 0x4t 0x73t 0x69t
        0x5t 0x4t 0x6ct 0x61t
        0x5t 0x4t 0x74t 0x6dt
        0x3t 0x4t 0x79t 0x63t
        0x3t 0x4t 0x65t 0x67t
        0x3t 0x4t 0x6dt 0x61t
        0x5t 0x4t 0x67t 0x62t
        0x5t 0x4t 0x72t 0x74t
        0x5t 0x4t 0x6ft 0x66t
        0x3t 0x4t 0x65t 0x67t
        0x5t 0x4t 0x6ct 0x67t
        0x5t 0x4t 0x6dt 0x73t
        0x5t 0x4t 0x69t 0x73t
        0x5t 0x4t 0x6bt 0x6dt
        0x5t 0x4t 0x69t 0x6ct
        0x5t 0x4t 0x65t 0x6dt
        0x6t 0x4t 0x61t 0x63t
        0x6t 0x4t 0x6dt 0x70t
        0x6t 0x4t 0x73t 0x75t
        0x6t 0x4t 0x73t 0x75t
        0x6t 0x4t 0x73t 0x75t
        0x6t 0x4t 0x73t 0x75t
        0x6t 0x4t 0x73t 0x75t
        0x6t 0x4t 0x73t 0x75t
        0x3t 0x4t 0x73t 0x75t
        0x6t 0x4t 0x72t 0x70t
        0x6t 0x4t 0x69t 0x76t
        0x6t 0x4t 0x78t 0x6dt
        0x6t 0x4t 0x6dt 0x6at
        0x6t 0x4t 0x70t 0x67t
        0x6t 0x4t 0x62t 0x62t
        0x6t 0x4t 0x67t 0x61t
        0x6t 0x4t 0x79t 0x6bt
        0x6t 0x4t 0x67t 0x76t
        0x6t 0x4t 0x6dt 0x62t
        0x6t 0x4t 0x64t 0x67t
        0x6t 0x4t 0x73t 0x6dt
        0x6t 0x4t 0x6et 0x6bt
        0x6t 0x4t 0x63t 0x6ct
        0x6t 0x4t 0x63t 0x76t
        0x7t 0x4t 0x77t 0x63t
        0x7t 0x4t 0x77t 0x61t
        0x6t 0x4t 0x73t 0x62t
        0x6t 0x4t 0x69t 0x61t
        0x6t 0x4t 0x6dt 0x64t
        0x6t 0x4t 0x75t 0x63t
        0x6t 0x4t 0x6ft 0x64t
        0x6t 0x4t 0x74t 0x68t
        0x7t 0x4t 0x74t 0x74t
        0x6t 0x4t 0x63t 0x74t
        0x3t 0x4t 0x7at 0x61t
        0x3t 0x4t 0x7at 0x6bt
        0x3t 0x4t 0x74t 0x62t
        0x3t 0x4t 0x6et 0x69t
        0x3t 0x4t 0x6et 0x69t
        0x3t 0x4t 0x6bt 0x70t
        0x3t 0x4t 0x66t 0x61t
        0x3t 0x4t 0x6bt 0x6ct
        0x3t 0x4t 0x6dt 0x6dt
        0x3t 0x4t 0x62t 0x6ct
        0x3t 0x4t 0x6ft 0x6at
        0x3t 0x4t 0x79t 0x73t
        0x3t 0x4t 0x71t 0x69t
        0x3t 0x4t 0x77t 0x6bt
        0x3t 0x4t 0x61t 0x73t
        0x3t 0x4t 0x65t 0x79t
        0x3t 0x4t 0x6dt 0x6ft
        0x3t 0x4t 0x73t 0x70t
        0x3t 0x4t 0x65t 0x61t
        0x3t 0x4t 0x6ct 0x69t
        0x3t 0x4t 0x68t 0x62t
        0x3t 0x4t 0x61t 0x71t
        0x3t 0x4t 0x6et 0x6dt
        0x3t 0x4t 0x70t 0x6et
        0x3t 0x4t 0x65t 0x61t
        0x3t 0x4t 0x65t 0x61t
        0x3t 0x4t 0x72t 0x69t
        0x3t 0x4t 0x7at 0x75t
        0x3t 0x4t 0x6at 0x74t
        0x3t 0x4t 0x67t 0x6bt
        0x3t 0x4t 0x6dt 0x74t
        0x3t 0x4t 0x70t 0x6at
        0x3t 0x4t 0x70t 0x6at
        0x3t 0x4t 0x72t 0x6bt
        0x3t 0x4t 0x6et 0x76t
        0x3t 0x4t 0x6bt 0x68t
        0x3t 0x4t 0x6ft 0x6dt
        0x3t 0x4t 0x68t 0x6bt
        0x3t 0x4t 0x61t 0x6ct
        0x3t 0x4t 0x6et 0x63t
        0x3t 0x4t 0x6et 0x63t
        0x3t 0x4t 0x77t 0x74t
        0x3t 0x4t 0x70t 0x6bt
        0x3t 0x4t 0x64t 0x62t
        0x3t 0x4t 0x76t 0x6dt
        0x3t 0x4t 0x79t 0x6dt
        0x4t 0x4t 0x75t 0x61t
        0x3t 0x4t 0x64t 0x69t
        0x3t 0x4t 0x6ct 0x74t
        0x3t 0x4t 0x68t 0x70t
        0x3t 0x4t 0x68t 0x74t
        0x3t 0x4t 0x67t 0x73t
        0x3t 0x4t 0x6et 0x62t
        0x4t 0x4t 0x7at 0x6et
        0x4t 0x4t 0x70t 0x6dt
        0x4t 0x4t 0x75t 0x67t
        0x4t 0x4t 0x72t 0x6et
        0x4t 0x4t 0x67t 0x70t
        0x4t 0x4t 0x6ft 0x74t
        0x4t 0x4t 0x62t 0x73t
        0x4t 0x4t 0x75t 0x76t
        0x4t 0x4t 0x6at 0x66t
        0x4t 0x4t 0x66t 0x77t
        0x4t 0x4t 0x73t 0x61t
        0x4t 0x4t 0x69t 0x6bt
        0x4t 0x4t 0x63t 0x6et
        0x4t 0x4t 0x66t 0x70t
        0x4t 0x4t 0x6bt 0x63t
        0x4t 0x4t 0x73t 0x77t
        0x4t 0x4t 0x6dt 0x66t
        0x4t 0x4t 0x68t 0x6dt
        0x4t 0x4t 0x77t 0x70t
        0x4t 0x4t 0x76t 0x74t
        0x4t 0x4t 0x75t 0x6et
        0x1t 0x4t 0x67t 0x65t
        0x1t 0x4t 0x7at 0x64t
        0x1t 0x4t 0x61t 0x6dt
        0x1t 0x4t 0x6et 0x74t
        0x1t 0x4t 0x79t 0x6ct
        0x1t 0x4t 0x6dt 0x67t
        0x1t 0x4t 0x6et 0x73t
        0x1t 0x4t 0x72t 0x6dt
        0x1t 0x4t 0x6ct 0x6dt
        0x1t 0x4t 0x6et 0x67t
        0x1t 0x4t 0x69t 0x63t
        0x1t 0x4t 0x66t 0x62t
        0x1t 0x4t 0x65t 0x6et
        0x1t 0x4t 0x67t 0x74t
        0x1t 0x4t 0x6at 0x62t
        0x1t 0x4t 0x75t 0x6dt
        0x1t 0x4t 0x72t 0x6ct
        0x1t 0x4t 0x6ct 0x73t
        0x1t 0x4t 0x68t 0x67t
        0x1t 0x4t 0x67t 0x6et
        0x1t 0x4t 0x64t 0x74t
        0x1t 0x4t 0x66t 0x63t
        0x1t 0x4t 0x6dt 0x63t
        0x1t 0x4t 0x76t 0x63t
        0x1t 0x4t 0x74t 0x73t
        0x1t 0x4t 0x71t 0x67t
        0x1t 0x4t 0x61t 0x67t
        0x1t 0x4t 0x67t 0x63t
        0x1t 0x4t 0x64t 0x63t
        0x1t 0x4t 0x6ft 0x61t
        0x1t 0x4t 0x77t 0x67t
        0x1t 0x4t 0x63t 0x73t
        0x1t 0x4t 0x64t 0x73t
        0x1t 0x4t 0x77t 0x72t
        0x1t 0x4t 0x74t 0x65t
        0x1t 0x4t 0x6ft 0x73t
        0x1t 0x4t 0x6at 0x64t
        0x1t 0x4t 0x65t 0x6bt
        0x1t 0x4t 0x7at 0x74t
        0x1t 0x4t 0x67t 0x75t
        0x1t 0x4t 0x69t 0x62t
        0x1t 0x4t 0x7at 0x6dt
        0x1t 0x4t 0x6dt 0x7at
        0x1t 0x4t 0x67t 0x6dt
        0x1t 0x4t 0x65t 0x72t
        0x1t 0x4t 0x77t 0x7at
        0x1t 0x4t 0x61t 0x6et
        0x1t 0x4t 0x77t 0x6dt
        0x1t 0x4t 0x73t 0x6ct
        0x1t 0x4t 0x77t 0x62t
        0x1t 0x4t 0x7at 0x73t
        0x1t 0x4t 0x6dt 0x6bt
        0x1t 0x4t 0x61t 0x7at
        0x1t 0x4t 0x72t 0x65t
        0x6t 0x4t 0x7at 0x62t
        0x6t 0x4t 0x74t 0x67t
        0x6t 0x4t 0x76t 0x73t
        0x6t 0x4t 0x6et 0x68t
        0x6t 0x4t 0x69t 0x6et
        0x6t 0x4t 0x72t 0x63t
        0x6t 0x4t 0x61t 0x70t
        0x7t 0x4t 0x65t 0x70t
        0x7t 0x4t 0x72t 0x61t
        0x7t 0x4t 0x72t 0x62t
        0x7t 0x4t 0x6ct 0x63t
        0x7t 0x6t 0x6ft 0x63t
        0x7t 0x4t 0x65t 0x76t
        0x7t 0x4t 0x6ft 0x62t
        0x7t 0x4t 0x79t 0x67t
        0x7t 0x4t 0x63t 0x65t
        0x7t 0x4t 0x66t 0x67t
        0x7t 0x4t 0x79t 0x70t
        0x7t 0x4t 0x72t 0x73t
        0x7t 0x4t 0x79t 0x75t
        0x7t 0x4t 0x6bt 0x66t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static areaCodeForMcc(I)I
    .registers 5
    .parameter "mcc"

    #@0
    .prologue
    .line 143
    sget-object v2, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->MCC_CODES:[S

    #@2
    int-to-short v3, p0

    #@3
    invoke-static {v2, v3}, Ljava/util/Arrays;->binarySearch([SS)I

    #@6
    move-result v1

    #@7
    .line 144
    .local v1, index:I
    if-gez v1, :cond_b

    #@9
    .line 145
    const/4 v2, 0x0

    #@a
    .line 148
    :goto_a
    return v2

    #@b
    .line 147
    :cond_b
    sget-object v2, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->IND_CODES:[I

    #@d
    aget v0, v2, v1

    #@f
    .line 148
    .local v0, indCode:I
    and-int/lit8 v2, v0, 0xf

    #@11
    goto :goto_a
.end method

.method public static changeLocalTimeForMcc(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 10
    .parameter "context"
    .parameter "strmcc"

    #@0
    .prologue
    .line 161
    new-instance v6, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v7, "changeLocalTimeForMcc "

    #@7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v6

    #@b
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v6

    #@13
    invoke-static {v6}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->log(Ljava/lang/String;)V

    #@16
    .line 163
    const/4 v4, 0x0

    #@17
    .line 165
    .local v4, result:Z
    if-eqz p1, :cond_21

    #@19
    const-string v6, ""

    #@1b
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v6

    #@1f
    if-eqz v6, :cond_23

    #@21
    :cond_21
    const/4 v6, 0x0

    #@22
    .line 187
    :goto_22
    return v6

    #@23
    .line 167
    :cond_23
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@26
    move-result v2

    #@27
    .line 168
    .local v2, mcc:I
    invoke-static {v2}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->areaCodeForMcc(I)I

    #@2a
    move-result v0

    #@2b
    .line 169
    .local v0, area:I
    invoke-static {v2}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->countryCodeForMcc(I)Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    .line 171
    .local v1, iso:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->getTzInfo(ILjava/lang/String;)Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    .line 172
    .local v5, timezoneId:Ljava/lang/String;
    const-string v6, "persist.sys.timezone"

    #@35
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    .line 174
    .local v3, prev_zoneId:Ljava/lang/String;
    if-eqz v5, :cond_7c

    #@3b
    .line 175
    new-instance v6, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v7, "changeLocalTimeForMcc timezoneId="

    #@42
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v6

    #@4a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v6

    #@4e
    invoke-static {v6}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->log(Ljava/lang/String;)V

    #@51
    .line 176
    if-eqz v3, :cond_60

    #@53
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v6

    #@57
    if-eqz v6, :cond_60

    #@59
    .line 177
    const-string v6, "changeLocalTimeForMcc already changed"

    #@5b
    invoke-static {v6}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->log(Ljava/lang/String;)V

    #@5e
    .line 178
    const/4 v6, 0x1

    #@5f
    goto :goto_22

    #@60
    .line 180
    :cond_60
    invoke-static {p0, v5}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->setTimezone(Landroid/content/Context;Ljava/lang/String;)V

    #@63
    .line 181
    const/4 v4, 0x1

    #@64
    .line 186
    :goto_64
    new-instance v6, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v7, "changeLocalTimeForMcc returns "

    #@6b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v6

    #@6f
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v6

    #@77
    invoke-static {v6}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->log(Ljava/lang/String;)V

    #@7a
    move v6, v4

    #@7b
    .line 187
    goto :goto_22

    #@7c
    .line 183
    :cond_7c
    const-string v6, "changeLocalTimeForMcc timezoneId=(null)"

    #@7e
    invoke-static {v6}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->log(Ljava/lang/String;)V

    #@81
    goto :goto_64
.end method

.method public static countryCodeForMcc(I)Ljava/lang/String;
    .registers 6
    .parameter "mcc"

    #@0
    .prologue
    .line 133
    sget-object v3, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->MCC_CODES:[S

    #@2
    int-to-short v4, p0

    #@3
    invoke-static {v3, v4}, Ljava/util/Arrays;->binarySearch([SS)I

    #@6
    move-result v1

    #@7
    .line 134
    .local v1, index:I
    if-gez v1, :cond_c

    #@9
    .line 135
    const-string v3, ""

    #@b
    .line 139
    :goto_b
    return-object v3

    #@c
    .line 137
    :cond_c
    sget-object v3, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->IND_CODES:[I

    #@e
    aget v0, v3, v1

    #@10
    .line 138
    .local v0, indCode:I
    const/4 v3, 0x2

    #@11
    new-array v2, v3, [B

    #@13
    const/4 v3, 0x0

    #@14
    ushr-int/lit8 v4, v0, 0x18

    #@16
    and-int/lit16 v4, v4, 0xff

    #@18
    int-to-byte v4, v4

    #@19
    aput-byte v4, v2, v3

    #@1b
    const/4 v3, 0x1

    #@1c
    ushr-int/lit8 v4, v0, 0x10

    #@1e
    and-int/lit16 v4, v4, 0xff

    #@20
    int-to-byte v4, v4

    #@21
    aput-byte v4, v2, v3

    #@23
    .line 139
    .local v2, iso:[B
    new-instance v3, Ljava/lang/String;

    #@25
    invoke-direct {v3, v2}, Ljava/lang/String;-><init>([B)V

    #@28
    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    goto :goto_b
.end method

.method public static defaultTimeZoneForMcc(I)Ljava/lang/String;
    .registers 2
    .parameter "mcc"

    #@0
    .prologue
    .line 129
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method private static getTzInfo(ILjava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "area"
    .parameter "iso"

    #@0
    .prologue
    .line 191
    new-instance v6, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v7, "getTzInfo area="

    #@7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v6

    #@b
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    const-string v7, ", isi="

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v6

    #@19
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v6

    #@1d
    invoke-static {v6}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->log(Ljava/lang/String;)V

    #@20
    .line 193
    const/4 v5, 0x0

    #@21
    .line 194
    .local v5, timezoneId:Ljava/lang/String;
    const/4 v2, 0x0

    #@22
    .line 196
    .local v2, infoparser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v6, 0x1

    #@23
    if-lt p0, v6, :cond_29

    #@25
    const/16 v6, 0x8

    #@27
    if-le p0, v6, :cond_2b

    #@29
    :cond_29
    const/4 v6, 0x0

    #@2a
    .line 221
    :goto_2a
    return-object v6

    #@2b
    .line 198
    :cond_2b
    new-instance v1, Ljava/io/File;

    #@2d
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@30
    move-result-object v6

    #@31
    sget-object v7, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->UPDATED_USERTZ_PATH:[Ljava/lang/String;

    #@33
    aget-object v7, v7, p0

    #@35
    invoke-direct {v1, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@38
    .line 199
    .local v1, infoFile:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@3b
    move-result v6

    #@3c
    if-nez v6, :cond_4b

    #@3e
    .line 200
    new-instance v1, Ljava/io/File;

    #@40
    .end local v1           #infoFile:Ljava/io/File;
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    #@43
    move-result-object v6

    #@44
    sget-object v7, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->BUILTIN_OEMTZ_PATH:[Ljava/lang/String;

    #@46
    aget-object v7, v7, p0

    #@48
    invoke-direct {v1, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@4b
    .line 203
    .restart local v1       #infoFile:Ljava/io/File;
    :cond_4b
    const/4 v3, 0x0

    #@4c
    .line 205
    .local v3, inforeader:Ljava/io/FileReader;
    :try_start_4c
    new-instance v4, Ljava/io/FileReader;

    #@4e
    invoke-direct {v4, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_51
    .catchall {:try_start_4c .. :try_end_51} :catchall_bf
    .catch Ljava/io/FileNotFoundException; {:try_start_4c .. :try_end_51} :catch_69
    .catch Ljava/lang/Exception; {:try_start_4c .. :try_end_51} :catch_95

    #@51
    .line 206
    .end local v3           #inforeader:Ljava/io/FileReader;
    .local v4, inforeader:Ljava/io/FileReader;
    :try_start_51
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@54
    move-result-object v2

    #@55
    .line 207
    invoke-interface {v2, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    #@58
    .line 208
    const-string v6, "area"

    #@5a
    invoke-static {v2, v6}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@5d
    .line 210
    invoke-static {v2, p0, p1}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->loadTzFile(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Ljava/lang/String;
    :try_end_60
    .catchall {:try_start_51 .. :try_end_60} :catchall_ca
    .catch Ljava/io/FileNotFoundException; {:try_start_51 .. :try_end_60} :catch_d0
    .catch Ljava/lang/Exception; {:try_start_51 .. :try_end_60} :catch_cd

    #@60
    move-result-object v5

    #@61
    .line 218
    if-eqz v4, :cond_66

    #@63
    :try_start_63
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_66
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_66} :catch_c8

    #@66
    :cond_66
    :goto_66
    move-object v3, v4

    #@67
    .end local v4           #inforeader:Ljava/io/FileReader;
    .restart local v3       #inforeader:Ljava/io/FileReader;
    :cond_67
    :goto_67
    move-object v6, v5

    #@68
    .line 221
    goto :goto_2a

    #@69
    .line 211
    :catch_69
    move-exception v0

    #@6a
    .line 212
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_6a
    :try_start_6a
    const-string v6, "CALL_FRW"

    #@6c
    new-instance v7, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v8, "File not found: \'"

    #@73
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v7

    #@77
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@7a
    move-result-object v8

    #@7b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v7

    #@7f
    const-string v8, "\'"

    #@81
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v7

    #@85
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v7

    #@89
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8c
    .catchall {:try_start_6a .. :try_end_8c} :catchall_bf

    #@8c
    .line 213
    const/4 v5, 0x0

    #@8d
    .line 218
    if-eqz v3, :cond_67

    #@8f
    :try_start_8f
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_92
    .catch Ljava/io/IOException; {:try_start_8f .. :try_end_92} :catch_93

    #@92
    goto :goto_67

    #@93
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_93
    move-exception v6

    #@94
    goto :goto_67

    #@95
    .line 214
    :catch_95
    move-exception v0

    #@96
    .line 215
    .local v0, e:Ljava/lang/Exception;
    :goto_96
    :try_start_96
    const-string v6, "CALL_FRW"

    #@98
    new-instance v7, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v8, "Exception while parsing \'"

    #@9f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v7

    #@a3
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@a6
    move-result-object v8

    #@a7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v7

    #@ab
    const-string v8, "\'"

    #@ad
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v7

    #@b1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v7

    #@b5
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b8
    .catchall {:try_start_96 .. :try_end_b8} :catchall_bf

    #@b8
    .line 216
    const/4 v5, 0x0

    #@b9
    .line 218
    if-eqz v3, :cond_67

    #@bb
    :try_start_bb
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_be
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_be} :catch_93

    #@be
    goto :goto_67

    #@bf
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_bf
    move-exception v6

    #@c0
    :goto_c0
    if-eqz v3, :cond_c5

    #@c2
    :try_start_c2
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_c5
    .catch Ljava/io/IOException; {:try_start_c2 .. :try_end_c5} :catch_c6

    #@c5
    :cond_c5
    :goto_c5
    throw v6

    #@c6
    :catch_c6
    move-exception v7

    #@c7
    goto :goto_c5

    #@c8
    .end local v3           #inforeader:Ljava/io/FileReader;
    .restart local v4       #inforeader:Ljava/io/FileReader;
    :catch_c8
    move-exception v6

    #@c9
    goto :goto_66

    #@ca
    :catchall_ca
    move-exception v6

    #@cb
    move-object v3, v4

    #@cc
    .end local v4           #inforeader:Ljava/io/FileReader;
    .restart local v3       #inforeader:Ljava/io/FileReader;
    goto :goto_c0

    #@cd
    .line 214
    .end local v3           #inforeader:Ljava/io/FileReader;
    .restart local v4       #inforeader:Ljava/io/FileReader;
    :catch_cd
    move-exception v0

    #@ce
    move-object v3, v4

    #@cf
    .end local v4           #inforeader:Ljava/io/FileReader;
    .restart local v3       #inforeader:Ljava/io/FileReader;
    goto :goto_96

    #@d0
    .line 211
    .end local v3           #inforeader:Ljava/io/FileReader;
    .restart local v4       #inforeader:Ljava/io/FileReader;
    :catch_d0
    move-exception v0

    #@d1
    move-object v3, v4

    #@d2
    .end local v4           #inforeader:Ljava/io/FileReader;
    .restart local v3       #inforeader:Ljava/io/FileReader;
    goto :goto_6a
.end method

.method private static loadTzFile(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "parser"
    .parameter "area"
    .parameter "target"

    #@0
    .prologue
    .line 225
    new-instance v7, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v8, "loadTzFile area="

    #@7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v7

    #@b
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v7

    #@f
    const-string v8, ", target="

    #@11
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v7

    #@15
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v7

    #@19
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v7

    #@1d
    invoke-static {v7}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->log(Ljava/lang/String;)V

    #@20
    .line 227
    const/4 v6, 0x0

    #@21
    .line 228
    .local v6, timezoneId:Ljava/lang/String;
    const/4 v0, 0x0

    #@22
    .line 230
    .local v0, doExit:Z
    if-eqz p0, :cond_79

    #@24
    .line 232
    :try_start_24
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@27
    move-result v2

    #@28
    .line 233
    .local v2, eventType:I
    :goto_28
    if-nez v0, :cond_79

    #@2a
    const/4 v7, 0x1

    #@2b
    if-eq v2, v7, :cond_79

    #@2d
    .line 234
    invoke-static {p0}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@30
    .line 235
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    .line 236
    .local v5, recType:Ljava/lang/String;
    const-string v7, "iso"

    #@36
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v7

    #@3a
    if-eqz v7, :cond_6a

    #@3c
    .line 237
    const/4 v7, 0x0

    #@3d
    const-string v8, "name"

    #@3f
    invoke-interface {p0, v7, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@42
    move-result-object v4

    #@43
    .line 238
    .local v4, name:Ljava/lang/String;
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v7

    #@47
    if-eqz v7, :cond_6a

    #@49
    .line 239
    const/4 v7, 0x0

    #@4a
    const-string v8, "zoneid"

    #@4c
    invoke-interface {p0, v7, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4f
    move-result-object v6

    #@50
    .line 240
    const/4 v7, 0x0

    #@51
    const-string v8, "multiple"

    #@53
    invoke-interface {p0, v7, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    .line 241
    .local v3, multiple:Ljava/lang/String;
    if-eqz v6, :cond_6f

    #@59
    if-eqz v3, :cond_6f

    #@5b
    .line 242
    const-string v7, "true"

    #@5d
    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@60
    move-result v7

    #@61
    if-eqz v7, :cond_69

    #@63
    .line 243
    const-string v7, "ril.gsm.tzfocus"

    #@65
    invoke-static {v7, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 244
    const/4 v6, 0x0

    #@69
    .line 249
    :cond_69
    :goto_69
    const/4 v0, 0x1

    #@6a
    .line 252
    .end local v3           #multiple:Ljava/lang/String;
    .end local v4           #name:Ljava/lang/String;
    :cond_6a
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_6d
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_24 .. :try_end_6d} :catch_71
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_6d} :catch_7a

    #@6d
    move-result v2

    #@6e
    .line 253
    goto :goto_28

    #@6f
    .line 247
    .restart local v3       #multiple:Ljava/lang/String;
    .restart local v4       #name:Ljava/lang/String;
    :cond_6f
    const/4 v6, 0x0

    #@70
    goto :goto_69

    #@71
    .line 254
    .end local v2           #eventType:I
    .end local v3           #multiple:Ljava/lang/String;
    .end local v4           #name:Ljava/lang/String;
    .end local v5           #recType:Ljava/lang/String;
    :catch_71
    move-exception v1

    #@72
    .line 255
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v7, "CALL_FRW"

    #@74
    const-string v8, "Got execption while getting perferred time zone."

    #@76
    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@79
    .line 261
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :cond_79
    :goto_79
    return-object v6

    #@7a
    .line 256
    :catch_7a
    move-exception v1

    #@7b
    .line 257
    .local v1, e:Ljava/io/IOException;
    const-string v7, "CALL_FRW"

    #@7d
    const-string v8, "Got execption while getting perferred time zone."

    #@7f
    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@82
    goto :goto_79
.end method

.method protected static log(Ljava/lang/String;)V
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 265
    const-string v0, "CALL_FRW"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[LgeGlobalTimeZoneHelper] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 266
    return-void
.end method

.method private static setTimezone(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "zoneId"

    #@0
    .prologue
    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "setTimezone zoneId="

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->log(Ljava/lang/String;)V

    #@16
    .line 153
    if-eqz p1, :cond_3f

    #@18
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@1b
    move-result v1

    #@1c
    if-lez v1, :cond_3f

    #@1e
    .line 154
    const-string v1, "alarm"

    #@20
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@23
    move-result-object v0

    #@24
    check-cast v0, Landroid/app/AlarmManager;

    #@26
    .line 155
    .local v0, alarm:Landroid/app/AlarmManager;
    invoke-virtual {v0, p1}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    #@29
    .line 156
    new-instance v1, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v2, "timezone set to "

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-static {v1}, Lcom/android/internal/telephony/LgeGlobalTimeZoneHelper;->log(Ljava/lang/String;)V

    #@3f
    .line 158
    .end local v0           #alarm:Landroid/app/AlarmManager;
    :cond_3f
    return-void
.end method
