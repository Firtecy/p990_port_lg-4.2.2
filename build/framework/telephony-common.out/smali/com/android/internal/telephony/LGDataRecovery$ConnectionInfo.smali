.class public Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
.super Ljava/lang/Object;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGDataRecovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ConnectionInfo"
.end annotation


# instance fields
.field public APN:Ljava/lang/String;

.field public CurrStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

.field public CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

.field public FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

.field public LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

.field public PrevStatAP:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

.field public PrevStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

.field public iface:Ljava/lang/String;

.field mConnCheckAlarmIntent:Landroid/app/PendingIntent;

.field public recoverySign:I

.field public sentSinceLastRecv:I

.field public sentSinceLastRecvModem:I

.field final synthetic this$0:Lcom/android/internal/telephony/LGDataRecovery;

.field public types:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/LGDataRecovery;)V
    .registers 5
    .parameter

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 965
    iput-object p1, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 966
    sget-object v0, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKDOWN:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@b
    .line 967
    sget-object v0, Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;->DISCONNECTED:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@d
    iput-object v0, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@f
    .line 968
    new-instance v0, Ljava/util/ArrayList;

    #@11
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@14
    iput-object v0, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@16
    .line 969
    iput-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@18
    .line 970
    iput-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@1a
    .line 971
    new-instance v0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@1c
    invoke-direct {v0, p1}, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@1f
    iput-object v0, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->PrevStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@21
    .line 972
    new-instance v0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@23
    invoke-direct {v0, p1}, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;-><init>(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@26
    iput-object v0, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->CurrStatModem:Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;

    #@28
    .line 973
    iput v2, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->sentSinceLastRecv:I

    #@2a
    .line 974
    iput v2, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->sentSinceLastRecvModem:I

    #@2c
    .line 975
    const/4 v0, 0x1

    #@2d
    iput v0, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->recoverySign:I

    #@2f
    .line 976
    iput-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->mConnCheckAlarmIntent:Landroid/app/PendingIntent;

    #@31
    .line 977
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 981
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 982
    .local v2, sb:Ljava/lang/StringBuilder;
    iget-object v4, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@7
    if-eqz v4, :cond_3f

    #@9
    iget-object v4, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v4

    #@f
    if-lez v4, :cond_3f

    #@11
    .line 983
    const-string v4, "["

    #@13
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 984
    iget-object v4, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1b
    move-result-object v1

    #@1c
    .local v1, i$:Ljava/util/Iterator;
    :goto_1c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_3a

    #@22
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v0

    #@26
    check-cast v0, Ljava/lang/String;

    #@28
    .line 985
    .local v0, a:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->types:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@2d
    move-result v4

    #@2e
    const/4 v5, 0x1

    #@2f
    if-le v4, v5, :cond_36

    #@31
    .line 986
    const-string v4, ", "

    #@33
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 988
    :cond_36
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    goto :goto_1c

    #@3a
    .line 990
    .end local v0           #a:Ljava/lang/String;
    :cond_3a
    const-string v4, "]"

    #@3c
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    .line 992
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    .line 993
    .local v3, type:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v5, " iface: "

    #@4a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v4

    #@4e
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    const-string v5, "   APN: "

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->APN:Ljava/lang/String;

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    const-string v5, "   type: "

    #@62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    const-string v5, "   LinkState: "

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->LState:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@72
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v4

    #@76
    const-string v5, "   FrameworkState: "

    #@78
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v4

    #@7c
    iget-object v5, p0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->FState:Lcom/android/internal/telephony/LGDataRecovery$FrameworkState;

    #@7e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v4

    #@86
    return-object v4
.end method
