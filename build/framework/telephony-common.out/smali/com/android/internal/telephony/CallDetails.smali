.class public Lcom/android/internal/telephony/CallDetails;
.super Ljava/lang/Object;
.source "CallDetails.java"


# static fields
.field public static final CALL_DOMAIN_AUTOMATIC:I = 0x3

.field public static final CALL_DOMAIN_CS:I = 0x1

.field public static final CALL_DOMAIN_NOT_SET:I = 0x4

.field public static final CALL_DOMAIN_PS:I = 0x2

.field public static final CALL_DOMAIN_UNKNOWN:I = 0x0

.field public static final CALL_TYPE_UNKNOWN:I = 0xa

.field public static final CALL_TYPE_VOICE:I = 0x0

.field public static final CALL_TYPE_VT:I = 0x3

.field public static final CALL_TYPE_VT_NODIR:I = 0x4

.field public static final CALL_TYPE_VT_RX:I = 0x2

.field public static final CALL_TYPE_VT_TX:I = 0x1

.field public static final EXTRAS_IS_CONFERENCE_URI:Ljava/lang/String; = "isConferenceUri"

.field public static final EXTRAS_PARENT_CALL_ID:Ljava/lang/String; = "parentCallId"


# instance fields
.field public call_domain:I

.field public call_type:I

.field public extras:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 113
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 114
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/internal/telephony/CallDetails;->call_type:I

    #@6
    .line 115
    const/4 v0, 0x4

    #@7
    iput v0, p0, Lcom/android/internal/telephony/CallDetails;->call_domain:I

    #@9
    .line 116
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/CallDetails;->extras:[Ljava/lang/String;

    #@c
    .line 117
    return-void
.end method

.method public constructor <init>(II[Ljava/lang/String;)V
    .registers 4
    .parameter "callType"
    .parameter "callDomain"
    .parameter "extraparams"

    #@0
    .prologue
    .line 119
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 120
    iput p1, p0, Lcom/android/internal/telephony/CallDetails;->call_type:I

    #@5
    .line 121
    iput p2, p0, Lcom/android/internal/telephony/CallDetails;->call_domain:I

    #@7
    .line 122
    iput-object p3, p0, Lcom/android/internal/telephony/CallDetails;->extras:[Ljava/lang/String;

    #@9
    .line 123
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/CallDetails;)V
    .registers 3
    .parameter "srcCall"

    #@0
    .prologue
    .line 125
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 126
    if-eqz p1, :cond_11

    #@5
    .line 127
    iget v0, p1, Lcom/android/internal/telephony/CallDetails;->call_type:I

    #@7
    iput v0, p0, Lcom/android/internal/telephony/CallDetails;->call_type:I

    #@9
    .line 128
    iget v0, p1, Lcom/android/internal/telephony/CallDetails;->call_domain:I

    #@b
    iput v0, p0, Lcom/android/internal/telephony/CallDetails;->call_domain:I

    #@d
    .line 129
    iget-object v0, p1, Lcom/android/internal/telephony/CallDetails;->extras:[Ljava/lang/String;

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/CallDetails;->extras:[Ljava/lang/String;

    #@11
    .line 131
    :cond_11
    return-void
.end method

.method public static getExtrasFromMap(Ljava/util/Map;)[Ljava/lang/String;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 138
    .local p0, newExtras:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    #@1
    .line 140
    .local v1, extras:[Ljava/lang/String;
    if-nez p0, :cond_5

    #@3
    .line 141
    const/4 v4, 0x0

    #@4
    .line 153
    :goto_4
    return-object v4

    #@5
    .line 145
    :cond_5
    invoke-interface {p0}, Ljava/util/Map;->size()I

    #@8
    move-result v4

    #@9
    new-array v1, v4, [Ljava/lang/String;

    #@b
    .line 147
    if-eqz v1, :cond_4e

    #@d
    .line 148
    const/4 v2, 0x0

    #@e
    .line 149
    .local v2, i:I
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@11
    move-result-object v4

    #@12
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v3

    #@16
    .local v3, i$:Ljava/util/Iterator;
    :goto_16
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_4e

    #@1c
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Ljava/util/Map$Entry;

    #@22
    .line 150
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v5, ""

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@30
    move-result-object v4

    #@31
    check-cast v4, Ljava/lang/String;

    #@33
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    const-string v5, "="

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@40
    move-result-object v4

    #@41
    check-cast v4, Ljava/lang/String;

    #@43
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    aput-object v4, v1, v2

    #@4d
    goto :goto_16

    #@4e
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2           #i:I
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_4e
    move-object v4, v1

    #@4f
    .line 153
    goto :goto_4
.end method


# virtual methods
.method public getValueForKeyFromExtras([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "extras"
    .parameter "key"

    #@0
    .prologue
    .line 161
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    if-eqz p1, :cond_26

    #@3
    array-length v2, p1

    #@4
    if-ge v1, v2, :cond_26

    #@6
    .line 162
    aget-object v2, p1, v1

    #@8
    if-eqz v2, :cond_23

    #@a
    .line 163
    aget-object v2, p1, v1

    #@c
    const-string v3, "="

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 164
    .local v0, currKey:[Ljava/lang/String;
    array-length v2, v0

    #@13
    const/4 v3, 0x2

    #@14
    if-ne v2, v3, :cond_23

    #@16
    const/4 v2, 0x0

    #@17
    aget-object v2, v0, v2

    #@19
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_23

    #@1f
    .line 165
    const/4 v2, 0x1

    #@20
    aget-object v2, v0, v2

    #@22
    .line 169
    .end local v0           #currKey:[Ljava/lang/String;
    :goto_22
    return-object v2

    #@23
    .line 161
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_1

    #@26
    .line 169
    :cond_26
    const/4 v2, 0x0

    #@27
    goto :goto_22
.end method

.method public setExtras([Ljava/lang/String;)V
    .registers 2
    .parameter "extraparams"

    #@0
    .prologue
    .line 134
    iput-object p1, p0, Lcom/android/internal/telephony/CallDetails;->extras:[Ljava/lang/String;

    #@2
    .line 135
    return-void
.end method

.method public setExtrasFromMap(Ljava/util/Map;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 157
    .local p1, newExtras:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/android/internal/telephony/CallDetails;->getExtrasFromMap(Ljava/util/Map;)[Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/android/internal/telephony/CallDetails;->extras:[Ljava/lang/String;

    #@6
    .line 158
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 177
    const-string v1, ""

    #@2
    .line 178
    .local v1, extrasResult:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/internal/telephony/CallDetails;->extras:[Ljava/lang/String;

    #@4
    if-eqz v5, :cond_22

    #@6
    .line 179
    iget-object v0, p0, Lcom/android/internal/telephony/CallDetails;->extras:[Ljava/lang/String;

    #@8
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@9
    .local v3, len$:I
    const/4 v2, 0x0

    #@a
    .local v2, i$:I
    :goto_a
    if-ge v2, v3, :cond_22

    #@c
    aget-object v4, v0, v2

    #@e
    .line 180
    .local v4, s:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    .line 179
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_a

    #@22
    .line 183
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v4           #s:Ljava/lang/String;
    :cond_22
    new-instance v5, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v6, " "

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    iget v6, p0, Lcom/android/internal/telephony/CallDetails;->call_type:I

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    const-string v6, " "

    #@35
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    iget v6, p0, Lcom/android/internal/telephony/CallDetails;->call_domain:I

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    const-string v6, " "

    #@41
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v5

    #@4d
    return-object v5
.end method
