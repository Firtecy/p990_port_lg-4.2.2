.class Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;
.super Lcom/android/internal/telephony/cat/ResponseData;
.source "ResponseData.java"


# static fields
.field protected static final GET_INKEY_NO:B = 0x0t

.field protected static final GET_INKEY_YES:B = 0x1t


# instance fields
.field public mInData:Ljava/lang/String;

.field private mIsPacked:Z

.field private mIsUcs2:Z

.field private mIsYesNo:Z

.field private mYesNoResponse:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZZ)V
    .registers 5
    .parameter "inData"
    .parameter "ucs2"
    .parameter "packed"

    #@0
    .prologue
    .line 81
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/ResponseData;-><init>()V

    #@3
    .line 82
    iput-boolean p2, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mIsUcs2:Z

    #@5
    .line 83
    iput-boolean p3, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mIsPacked:Z

    #@7
    .line 84
    iput-object p1, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mInData:Ljava/lang/String;

    #@9
    .line 85
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mIsYesNo:Z

    #@c
    .line 86
    return-void
.end method

.method public constructor <init>(Z)V
    .registers 3
    .parameter "yesNoResponse"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 89
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/ResponseData;-><init>()V

    #@4
    .line 90
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mIsUcs2:Z

    #@6
    .line 91
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mIsPacked:Z

    #@8
    .line 92
    const-string v0, ""

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mInData:Ljava/lang/String;

    #@c
    .line 93
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mIsYesNo:Z

    #@f
    .line 94
    iput-boolean p1, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mYesNoResponse:Z

    #@11
    .line 95
    return-void
.end method


# virtual methods
.method public format(Ljava/io/ByteArrayOutputStream;)V
    .registers 15
    .parameter "buf"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 99
    if-nez p1, :cond_5

    #@4
    .line 158
    :cond_4
    return-void

    #@5
    .line 104
    :cond_5
    sget-object v11, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->TEXT_STRING:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@7
    invoke-virtual {v11}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@a
    move-result v11

    #@b
    or-int/lit16 v7, v11, 0x80

    #@d
    .line 105
    .local v7, tag:I
    invoke-virtual {p1, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@10
    .line 109
    iget-boolean v11, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mIsYesNo:Z

    #@12
    if-eqz v11, :cond_3c

    #@14
    .line 110
    new-array v2, v9, [B

    #@16
    .line 111
    .local v2, data:[B
    iget-boolean v11, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mYesNoResponse:Z

    #@18
    if-eqz v11, :cond_3a

    #@1a
    :goto_1a
    aput-byte v9, v2, v10

    #@1c
    .line 142
    :goto_1c
    array-length v9, v2

    #@1d
    add-int/lit8 v9, v9, 0x1

    #@1f
    invoke-static {p1, v9}, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->writeLength(Ljava/io/ByteArrayOutputStream;I)V

    #@22
    .line 145
    iget-boolean v9, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mIsUcs2:Z

    #@24
    if-eqz v9, :cond_81

    #@26
    .line 146
    const/16 v9, 0x8

    #@28
    invoke-virtual {p1, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2b
    .line 153
    :goto_2b
    if-eqz v2, :cond_4

    #@2d
    .line 154
    move-object v0, v2

    #@2e
    .local v0, arr$:[B
    array-length v5, v0

    #@2f
    .local v5, len$:I
    const/4 v4, 0x0

    #@30
    .local v4, i$:I
    :goto_30
    if-ge v4, v5, :cond_4

    #@32
    aget-byte v1, v0, v4

    #@34
    .line 155
    .local v1, b:B
    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@37
    .line 154
    add-int/lit8 v4, v4, 0x1

    #@39
    goto :goto_30

    #@3a
    .end local v0           #arr$:[B
    .end local v1           #b:B
    .end local v4           #i$:I
    .end local v5           #len$:I
    :cond_3a
    move v9, v10

    #@3b
    .line 111
    goto :goto_1a

    #@3c
    .line 112
    .end local v2           #data:[B
    :cond_3c
    iget-object v9, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mInData:Ljava/lang/String;

    #@3e
    if-eqz v9, :cond_7e

    #@40
    iget-object v9, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mInData:Ljava/lang/String;

    #@42
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@45
    move-result v9

    #@46
    if-lez v9, :cond_7e

    #@48
    .line 116
    :try_start_48
    iget-boolean v9, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mIsUcs2:Z

    #@4a
    if-eqz v9, :cond_55

    #@4c
    .line 118
    iget-object v9, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mInData:Ljava/lang/String;

    #@4e
    const-string v11, "UTF-16BE"

    #@50
    invoke-virtual {v9, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@53
    move-result-object v2

    #@54
    .restart local v2       #data:[B
    goto :goto_1c

    #@55
    .line 119
    .end local v2           #data:[B
    :cond_55
    iget-boolean v9, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mIsPacked:Z

    #@57
    if-eqz v9, :cond_73

    #@59
    .line 120
    iget-object v9, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mInData:Ljava/lang/String;

    #@5b
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@5e
    move-result v6

    #@5f
    .line 122
    .local v6, size:I
    iget-object v9, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mInData:Ljava/lang/String;

    #@61
    const/4 v11, 0x0

    #@62
    const/4 v12, 0x0

    #@63
    invoke-static {v9, v11, v12}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPacked(Ljava/lang/String;II)[B

    #@66
    move-result-object v8

    #@67
    .line 124
    .local v8, tempData:[B
    new-array v2, v6, [B

    #@69
    .line 128
    .restart local v2       #data:[B
    const/4 v9, 0x1

    #@6a
    const/4 v11, 0x0

    #@6b
    invoke-static {v8, v9, v2, v11, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_6e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_48 .. :try_end_6e} :catch_6f
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_48 .. :try_end_6e} :catch_7a

    #@6e
    goto :goto_1c

    #@6f
    .line 132
    .end local v2           #data:[B
    .end local v6           #size:I
    .end local v8           #tempData:[B
    :catch_6f
    move-exception v3

    #@70
    .line 133
    .local v3, e:Ljava/io/UnsupportedEncodingException;
    new-array v2, v10, [B

    #@72
    .line 136
    .restart local v2       #data:[B
    goto :goto_1c

    #@73
    .line 130
    .end local v2           #data:[B
    .end local v3           #e:Ljava/io/UnsupportedEncodingException;
    :cond_73
    :try_start_73
    iget-object v9, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mInData:Ljava/lang/String;

    #@75
    invoke-static {v9}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm8BitPacked(Ljava/lang/String;)[B
    :try_end_78
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_73 .. :try_end_78} :catch_6f
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_73 .. :try_end_78} :catch_7a

    #@78
    move-result-object v2

    #@79
    .restart local v2       #data:[B
    goto :goto_1c

    #@7a
    .line 134
    .end local v2           #data:[B
    :catch_7a
    move-exception v3

    #@7b
    .line 135
    .local v3, e:Lcom/android/internal/telephony/EncodeException;
    new-array v2, v10, [B

    #@7d
    .line 136
    .restart local v2       #data:[B
    goto :goto_1c

    #@7e
    .line 138
    .end local v2           #data:[B
    .end local v3           #e:Lcom/android/internal/telephony/EncodeException;
    :cond_7e
    new-array v2, v10, [B

    #@80
    .restart local v2       #data:[B
    goto :goto_1c

    #@81
    .line 147
    :cond_81
    iget-boolean v9, p0, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;->mIsPacked:Z

    #@83
    if-eqz v9, :cond_89

    #@85
    .line 148
    invoke-virtual {p1, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@88
    goto :goto_2b

    #@89
    .line 150
    :cond_89
    const/4 v9, 0x4

    #@8a
    invoke-virtual {p1, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@8d
    goto :goto_2b
.end method
