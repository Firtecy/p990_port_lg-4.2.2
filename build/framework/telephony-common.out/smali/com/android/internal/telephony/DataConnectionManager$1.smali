.class synthetic Lcom/android/internal/telephony/DataConnectionManager$1;
.super Ljava/lang/Object;
.source "DataConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 320
    invoke-static {}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->values()[Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    new-array v0, v0, [I

    #@7
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@9
    :try_start_9
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@b
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->callingSetMobileDataEnabled:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@d
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x1

    #@12
    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_b9

    #@14
    :goto_14
    :try_start_14
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@16
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@18
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@1b
    move-result v1

    #@1c
    const/4 v2, 0x2

    #@1d
    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_b6

    #@1f
    :goto_1f
    :try_start_1f
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@21
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@23
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x3

    #@28
    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_b3

    #@2a
    :goto_2a
    :try_start_2a
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@2c
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@2e
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@31
    move-result v1

    #@32
    const/4 v2, 0x4

    #@33
    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_35} :catch_b1

    #@35
    :goto_35
    :try_start_35
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@37
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@39
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@3c
    move-result v1

    #@3d
    const/4 v2, 0x5

    #@3e
    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_40} :catch_af

    #@40
    :goto_40
    :try_start_40
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@42
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@44
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@47
    move-result v1

    #@48
    const/4 v2, 0x6

    #@49
    aput v2, v0, v1
    :try_end_4b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_4b} :catch_ad

    #@4b
    :goto_4b
    :try_start_4b
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@4d
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->functionForPacketList:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@4f
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@52
    move-result v1

    #@53
    const/4 v2, 0x7

    #@54
    aput v2, v0, v1
    :try_end_56
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4b .. :try_end_56} :catch_ab

    #@56
    :goto_56
    :try_start_56
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@58
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getRouteList_debug:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@5a
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@5d
    move-result v1

    #@5e
    const/16 v2, 0x8

    #@60
    aput v2, v0, v1
    :try_end_62
    .catch Ljava/lang/NoSuchFieldError; {:try_start_56 .. :try_end_62} :catch_a9

    #@62
    :goto_62
    :try_start_62
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@64
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->handleSKT_QA:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@66
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@69
    move-result v1

    #@6a
    const/16 v2, 0x9

    #@6c
    aput v2, v0, v1
    :try_end_6e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_62 .. :try_end_6e} :catch_a7

    #@6e
    :goto_6e
    :try_start_6e
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@70
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->debugFileWrite:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@72
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@75
    move-result v1

    #@76
    const/16 v2, 0xa

    #@78
    aput v2, v0, v1
    :try_end_7a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6e .. :try_end_7a} :catch_a5

    #@7a
    :goto_7a
    :try_start_7a
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@7c
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->KT_KAF:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@7e
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@81
    move-result v1

    #@82
    const/16 v2, 0xb

    #@84
    aput v2, v0, v1
    :try_end_86
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7a .. :try_end_86} :catch_a3

    #@86
    :goto_86
    :try_start_86
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@88
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->MMS_MOBILE_OFF:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@8a
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@8d
    move-result v1

    #@8e
    const/16 v2, 0xc

    #@90
    aput v2, v0, v1
    :try_end_92
    .catch Ljava/lang/NoSuchFieldError; {:try_start_86 .. :try_end_92} :catch_a1

    #@92
    :goto_92
    :try_start_92
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$1;->$SwitchMap$com$android$internal$telephony$DataConnectionManager$FunctionName:[I

    #@94
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->WHEN_DATA_DISALLOWED:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@96
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->ordinal()I

    #@99
    move-result v1

    #@9a
    const/16 v2, 0xd

    #@9c
    aput v2, v0, v1
    :try_end_9e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_92 .. :try_end_9e} :catch_9f

    #@9e
    :goto_9e
    return-void

    #@9f
    :catch_9f
    move-exception v0

    #@a0
    goto :goto_9e

    #@a1
    :catch_a1
    move-exception v0

    #@a2
    goto :goto_92

    #@a3
    :catch_a3
    move-exception v0

    #@a4
    goto :goto_86

    #@a5
    :catch_a5
    move-exception v0

    #@a6
    goto :goto_7a

    #@a7
    :catch_a7
    move-exception v0

    #@a8
    goto :goto_6e

    #@a9
    :catch_a9
    move-exception v0

    #@aa
    goto :goto_62

    #@ab
    :catch_ab
    move-exception v0

    #@ac
    goto :goto_56

    #@ad
    :catch_ad
    move-exception v0

    #@ae
    goto :goto_4b

    #@af
    :catch_af
    move-exception v0

    #@b0
    goto :goto_40

    #@b1
    :catch_b1
    move-exception v0

    #@b2
    goto :goto_35

    #@b3
    :catch_b3
    move-exception v0

    #@b4
    goto/16 :goto_2a

    #@b6
    :catch_b6
    move-exception v0

    #@b7
    goto/16 :goto_1f

    #@b9
    :catch_b9
    move-exception v0

    #@ba
    goto/16 :goto_14
.end method
