.class Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub$Proxy;
.super Ljava/lang/Object;
.source "IUsimInfoCallback.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IUsimInfoCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 98
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 99
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 100
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 107
    const-string v0, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@2
    return-object v0
.end method

.method public onPBMDeleteCB(II)V
    .registers 8
    .parameter "retPbmResult"
    .parameter "DeleteIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 160
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 162
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 163
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 164
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 165
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x4

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    #@17
    .line 168
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 170
    return-void

    #@1b
    .line 168
    :catchall_1b
    move-exception v1

    #@1c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    throw v1
.end method

.method public onPBMInfoCB(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V
    .registers 7
    .parameter "pbm_info"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 111
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 113
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 114
    if-eqz p1, :cond_1f

    #@b
    .line 115
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 116
    const/4 v1, 0x0

    #@10
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 121
    :goto_13
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v2, 0x1

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_24

    #@1b
    .line 124
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 126
    return-void

    #@1f
    .line 119
    :cond_1f
    const/4 v1, 0x0

    #@20
    :try_start_20
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_24

    #@23
    goto :goto_13

    #@24
    .line 124
    :catchall_24
    move-exception v1

    #@25
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v1
.end method

.method public onPBMReadCB(Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)V
    .registers 7
    .parameter "pbm_read"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 129
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 131
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 132
    if-eqz p1, :cond_1f

    #@b
    .line 133
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 134
    const/4 v1, 0x0

    #@10
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/telephony/uicc/LGE_PBM_Records;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 139
    :goto_13
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v2, 0x2

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_24

    #@1b
    .line 142
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 144
    return-void

    #@1f
    .line 137
    :cond_1f
    const/4 v1, 0x0

    #@20
    :try_start_20
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_24

    #@23
    goto :goto_13

    #@24
    .line 142
    :catchall_24
    move-exception v1

    #@25
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v1
.end method

.method public onPBMWriteCB(II)V
    .registers 8
    .parameter "retPbmResult"
    .parameter "WriteIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 147
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 149
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.uicc.IUsimInfoCallback"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 150
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 151
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 152
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x3

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    #@17
    .line 155
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 157
    return-void

    #@1b
    .line 155
    :catchall_1b
    move-exception v1

    #@1c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    throw v1
.end method
