.class public Lcom/android/internal/telephony/PDPContextStateBroadcaster;
.super Ljava/lang/Object;
.source "PDPContextStateBroadcaster.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/PDPContextStateBroadcaster$InstanceLock;
    }
.end annotation


# static fields
.field private static final ACTION_PDP_CONTEXT_STATE:Ljava/lang/String; = "diagandroid.data.PDPContextState"

.field private static final CONTEXT_STATE_CONNECTED:Ljava/lang/String; = "CONNECTED"

.field private static final CONTEXT_STATE_DISCONNECTED:Ljava/lang/String; = "DISCONNECTED"

.field private static final CONTEXT_STATE_REQUESTED:Ljava/lang/String; = "REQUEST"

.field private static final CONTEXT_TYPE_PRIMARY:Ljava/lang/String; = "PRIMARY"

.field private static final CONTEXT_TYPE_SECONDARY:Ljava/lang/String; = "SECONDARY"

.field private static final DNS_EXTRA_COUNT:I = 0x2

.field private static final EXTRA_CONTEXT_APN:Ljava/lang/String; = "ContextAPN"

.field private static final EXTRA_CONTEXT_DNS:[Ljava/lang/String; = null

.field private static final EXTRA_CONTEXT_ERROR_CODE:Ljava/lang/String; = "ContextErrorCode"

.field private static final EXTRA_CONTEXT_ID:Ljava/lang/String; = "ContextID"

.field private static final EXTRA_CONTEXT_INITIATOR:Ljava/lang/String; = "ContextInitiator"

.field private static final EXTRA_CONTEXT_IPV4_ADDR:Ljava/lang/String; = "ContextIPV4Addr"

.field private static final EXTRA_CONTEXT_IPV6_ADDR:Ljava/lang/String; = "ContextIPV6Addr"

.field private static final EXTRA_CONTEXT_NSAPI:Ljava/lang/String; = "ContextNSAPI"

.field private static final EXTRA_CONTEXT_SAPI:Ljava/lang/String; = "ContextSAPI"

.field private static final EXTRA_CONTEXT_STATE:Ljava/lang/String; = "ContextState"

.field private static final EXTRA_CONTEXT_TERM_CODE:Ljava/lang/String; = "ContextTermCode"

.field private static final EXTRA_CONTEXT_TYPE:Ljava/lang/String; = "ContextType"

.field private static final EXTRA_CONTEXT_V6DNS:[Ljava/lang/String; = null

.field private static final INITIATOR_NETWORK:Ljava/lang/String; = "NETWORK"

.field private static final INITIATOR_UE:Ljava/lang/String; = "USER"

.field private static final IP_ADDRESS_TYPE_COUNT:I = 0x2

.field private static final IP_ADDRESS_V4_INDEX:I = 0x0

.field private static final IP_ADDRESS_V6_INDEX:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String; = "PDPContextStateBroadcaster"

.field private static final PERMISSION_RECEIVE_PDP_CONTEXT_STATE:Ljava/lang/String; = "diagandroid.data.receivePDPContextState"

.field private static sInstance:Lcom/android/internal/telephony/PDPContextStateBroadcaster;

.field private static final sTermCodeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mContextIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNextContextId:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 124
    new-array v0, v4, [Ljava/lang/String;

    #@5
    const-string v1, "ContextDNS1"

    #@7
    aput-object v1, v0, v2

    #@9
    const-string v1, "ContextDNS2"

    #@b
    aput-object v1, v0, v3

    #@d
    sput-object v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->EXTRA_CONTEXT_DNS:[Ljava/lang/String;

    #@f
    .line 125
    new-array v0, v4, [Ljava/lang/String;

    #@11
    const-string v1, "ContextV6DNS1"

    #@13
    aput-object v1, v0, v2

    #@15
    const-string v1, "ContextV6DNS2"

    #@17
    aput-object v1, v0, v3

    #@19
    sput-object v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->EXTRA_CONTEXT_V6DNS:[Ljava/lang/String;

    #@1b
    .line 142
    new-instance v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;

    #@1d
    invoke-direct {v0}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;-><init>()V

    #@20
    sput-object v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sTermCodeMap:Ljava/util/HashMap;

    #@22
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 196
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 193
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mContextIdMap:Ljava/util/HashMap;

    #@a
    .line 194
    const/4 v0, 0x1

    #@b
    iput v0, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mNextContextId:I

    #@d
    .line 197
    iput-object p1, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mContext:Landroid/content/Context;

    #@f
    .line 198
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;-><init>(Landroid/content/Context;)V

    #@3
    return-void
.end method

.method static synthetic access$002(Lcom/android/internal/telephony/PDPContextStateBroadcaster;)Lcom/android/internal/telephony/PDPContextStateBroadcaster;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 23
    sput-object p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sInstance:Lcom/android/internal/telephony/PDPContextStateBroadcaster;

    #@2
    return-object p0
.end method

.method private static assignToArrayElementIfEmpty(Ljava/lang/String;[Ljava/lang/String;I)Z
    .registers 5
    .parameter "value"
    .parameter "targetArray"
    .parameter "targetIndex"

    #@0
    .prologue
    .line 335
    aget-object v1, p1, p2

    #@2
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    #@5
    move-result v0

    #@6
    .line 336
    .local v0, empty:Z
    if-eqz v0, :cond_a

    #@8
    .line 337
    aput-object p0, p1, p2

    #@a
    .line 339
    :cond_a
    return v0
.end method

.method private static assignToEmptyElement(Ljava/lang/String;[Ljava/lang/String;)V
    .registers 5
    .parameter "value"
    .parameter "targetArray"

    #@0
    .prologue
    .line 324
    const/4 v0, 0x0

    #@1
    .line 325
    .local v0, index:I
    :goto_1
    add-int/lit8 v1, v0, 0x1

    #@3
    .end local v0           #index:I
    .local v1, index:I
    invoke-static {p0, p1, v0}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->assignToArrayElementIfEmpty(Ljava/lang/String;[Ljava/lang/String;I)Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_c

    #@9
    .line 326
    array-length v2, p1

    #@a
    if-ne v1, v2, :cond_d

    #@c
    .line 330
    :cond_c
    return-void

    #@d
    :cond_d
    move v0, v1

    #@e
    .end local v1           #index:I
    .restart local v0       #index:I
    goto :goto_1
.end method

.method private broadcast(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 281
    iget-object v0, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "diagandroid.data.receivePDPContextState"

    #@4
    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@7
    .line 282
    return-void
.end method

.method private static createIntent(Ljava/lang/String;Ljava/lang/Integer;)Landroid/content/Intent;
    .registers 5
    .parameter "contextState"
    .parameter "contextId"

    #@0
    .prologue
    .line 274
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "diagandroid.data.PDPContextState"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 275
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "ContextState"

    #@9
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 276
    const-string v1, "ContextID"

    #@e
    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@15
    .line 277
    return-object v0
.end method

.method private declared-synchronized getNextContextId()I
    .registers 4

    #@0
    .prologue
    .line 205
    monitor-enter p0

    #@1
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mNextContextId:I

    #@3
    add-int/lit8 v1, v0, 0x1

    #@5
    iput v1, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mNextContextId:I

    #@7
    .line 207
    .local v0, nextId:I
    iget v1, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mNextContextId:I

    #@9
    const v2, 0xffff

    #@c
    if-le v1, v2, :cond_11

    #@e
    .line 208
    const/4 v1, 0x1

    #@f
    iput v1, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mNextContextId:I

    #@11
    .line 210
    :cond_11
    iget-object v1, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mContextIdMap:Ljava/util/HashMap;

    #@13
    new-instance v2, Ljava/lang/Integer;

    #@15
    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    #@18
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_20

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_1

    #@1e
    .line 211
    monitor-exit p0

    #@1f
    return v0

    #@20
    .line 205
    .end local v0           #nextId:I
    :catchall_20
    move-exception v1

    #@21
    monitor-exit p0

    #@22
    throw v1
.end method

.method private static processDNSAddresses(Ljava/util/Collection;[Ljava/lang/String;[Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter "ipv4DNSes"
    .parameter "ipv6DNSes"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/net/InetAddress;",
            ">;[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 308
    .local p0, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@3
    move-result-object v1

    #@4
    .local v1, i$:Ljava/util/Iterator;
    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_26

    #@a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Ljava/net/InetAddress;

    #@10
    .line 309
    .local v0, address:Ljava/net/InetAddress;
    const/4 v2, 0x0

    #@11
    .line 310
    .local v2, targetArray:[Ljava/lang/String;
    instance-of v3, v0, Ljava/net/Inet4Address;

    #@13
    if-eqz v3, :cond_20

    #@15
    .line 311
    move-object v2, p1

    #@16
    .line 316
    :cond_16
    :goto_16
    if-eqz v2, :cond_4

    #@18
    .line 317
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v3, v2}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->assignToEmptyElement(Ljava/lang/String;[Ljava/lang/String;)V

    #@1f
    goto :goto_4

    #@20
    .line 312
    :cond_20
    instance-of v3, v0, Ljava/net/Inet6Address;

    #@22
    if-eqz v3, :cond_16

    #@24
    .line 313
    move-object v2, p2

    #@25
    goto :goto_16

    #@26
    .line 320
    .end local v0           #address:Ljava/net/InetAddress;
    .end local v2           #targetArray:[Ljava/lang/String;
    :cond_26
    return-void
.end method

.method private static processIPAddresses(Ljava/util/Collection;[Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter "sortedAddresses"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/net/InetAddress;",
            ">;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 293
    .local p0, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@3
    move-result-object v1

    #@4
    .local v1, i$:Ljava/util/Iterator;
    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_2a

    #@a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Ljava/net/InetAddress;

    #@10
    .line 294
    .local v0, address:Ljava/net/InetAddress;
    instance-of v2, v0, Ljava/net/Inet4Address;

    #@12
    if-eqz v2, :cond_1d

    #@14
    .line 295
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    const/4 v3, 0x0

    #@19
    invoke-static {v2, p1, v3}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->assignToArrayElementIfEmpty(Ljava/lang/String;[Ljava/lang/String;I)Z

    #@1c
    goto :goto_4

    #@1d
    .line 296
    :cond_1d
    instance-of v2, v0, Ljava/net/Inet6Address;

    #@1f
    if-eqz v2, :cond_4

    #@21
    .line 297
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    const/4 v3, 0x1

    #@26
    invoke-static {v2, p1, v3}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->assignToArrayElementIfEmpty(Ljava/lang/String;[Ljava/lang/String;I)Z

    #@29
    goto :goto_4

    #@2a
    .line 300
    .end local v0           #address:Ljava/net/InetAddress;
    :cond_2a
    return-void
.end method

.method public static sendConnected(ILandroid/net/LinkProperties;)V
    .registers 9
    .parameter "contextId"
    .parameter "linkProperties"

    #@0
    .prologue
    .line 35
    sget-object v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sInstance:Lcom/android/internal/telephony/PDPContextStateBroadcaster;

    #@2
    if-eqz v0, :cond_47

    #@4
    if-eqz p1, :cond_47

    #@6
    .line 37
    const/4 v0, 0x2

    #@7
    :try_start_7
    new-array v6, v0, [Ljava/lang/String;

    #@9
    const/4 v0, 0x0

    #@a
    const-string v1, ""

    #@c
    aput-object v1, v6, v0

    #@e
    const/4 v0, 0x1

    #@f
    const-string v1, ""

    #@11
    aput-object v1, v6, v0

    #@13
    .line 38
    .local v6, ipAddresses:[Ljava/lang/String;
    const/4 v0, 0x2

    #@14
    new-array v4, v0, [Ljava/lang/String;

    #@16
    const/4 v0, 0x0

    #@17
    const-string v1, ""

    #@19
    aput-object v1, v4, v0

    #@1b
    const/4 v0, 0x1

    #@1c
    const-string v1, ""

    #@1e
    aput-object v1, v4, v0

    #@20
    .line 39
    .local v4, ipv4DNS:[Ljava/lang/String;
    const/4 v0, 0x2

    #@21
    new-array v5, v0, [Ljava/lang/String;

    #@23
    const/4 v0, 0x0

    #@24
    const-string v1, ""

    #@26
    aput-object v1, v5, v0

    #@28
    const/4 v0, 0x1

    #@29
    const-string v1, ""

    #@2b
    aput-object v1, v5, v0

    #@2d
    .line 42
    .local v5, ipv6DNS:[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/Collection;

    #@30
    move-result-object v0

    #@31
    invoke-static {v0, v6}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->processIPAddresses(Ljava/util/Collection;[Ljava/lang/String;)V

    #@34
    .line 45
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@37
    move-result-object v0

    #@38
    invoke-static {v0, v4, v5}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->processDNSAddresses(Ljava/util/Collection;[Ljava/lang/String;[Ljava/lang/String;)V

    #@3b
    .line 47
    sget-object v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sInstance:Lcom/android/internal/telephony/PDPContextStateBroadcaster;

    #@3d
    const/4 v1, 0x0

    #@3e
    aget-object v2, v6, v1

    #@40
    const/4 v1, 0x1

    #@41
    aget-object v3, v6, v1

    #@43
    move v1, p0

    #@44
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sendPDPContextConnected(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_47} :catch_48

    #@47
    .line 51
    .end local v4           #ipv4DNS:[Ljava/lang/String;
    .end local v5           #ipv6DNS:[Ljava/lang/String;
    .end local v6           #ipAddresses:[Ljava/lang/String;
    :cond_47
    :goto_47
    return-void

    #@48
    .line 48
    :catch_48
    move-exception v0

    #@49
    goto :goto_47
.end method

.method public static sendDisconnected(ILjava/lang/String;)V
    .registers 5
    .parameter "contextId"
    .parameter "reason"

    #@0
    .prologue
    .line 54
    sget-object v1, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sInstance:Lcom/android/internal/telephony/PDPContextStateBroadcaster;

    #@2
    if-eqz v1, :cond_1c

    #@4
    .line 56
    :try_start_4
    sget-object v1, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sTermCodeMap:Ljava/util/HashMap;

    #@6
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/Integer;

    #@c
    .line 58
    .local v0, termCode:Ljava/lang/Integer;
    if-nez v0, :cond_17

    #@e
    sget-object v1, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sTermCodeMap:Ljava/util/HashMap;

    #@10
    const/4 v2, 0x0

    #@11
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    .end local v0           #termCode:Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    #@17
    .line 59
    .restart local v0       #termCode:Ljava/lang/Integer;
    :cond_17
    sget-object v1, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sInstance:Lcom/android/internal/telephony/PDPContextStateBroadcaster;

    #@19
    invoke-direct {v1, p0, v0}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sendPDPContextDisconnected(ILjava/lang/Integer;)V
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_1c} :catch_1d

    #@1c
    .line 63
    .end local v0           #termCode:Ljava/lang/Integer;
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 60
    :catch_1d
    move-exception v1

    #@1e
    goto :goto_1c
.end method

.method private sendPDPContextConnected(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .registers 12
    .parameter "apnId"
    .parameter "ipv4Address"
    .parameter "ipv6Address"
    .parameter "ipv4DNS"
    .parameter "ipv6DNS"

    #@0
    .prologue
    .line 238
    iget-object v3, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mContextIdMap:Ljava/util/HashMap;

    #@2
    new-instance v4, Ljava/lang/Integer;

    #@4
    invoke-direct {v4, p1}, Ljava/lang/Integer;-><init>(I)V

    #@7
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Ljava/lang/Integer;

    #@d
    .line 239
    .local v0, contextId:Ljava/lang/Integer;
    if-eqz v0, :cond_9b

    #@f
    .line 240
    const-string v3, "CONNECTED"

    #@11
    invoke-static {v3, v0}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->createIntent(Ljava/lang/String;Ljava/lang/Integer;)Landroid/content/Intent;

    #@14
    move-result-object v2

    #@15
    .line 241
    .local v2, intent:Landroid/content/Intent;
    const-string v3, "ContextIPV4Addr"

    #@17
    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1a
    .line 242
    const-string v3, "ContextIPV6Addr"

    #@1c
    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1f
    .line 243
    const-string v3, "PDPContextStateBroadcaster"

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "sendPDPContextConnected - ID : "

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 244
    const-string v3, "PDPContextStateBroadcaster"

    #@39
    new-instance v4, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v5, "sendPDPContextConnected - IPv4 Address : "

    #@40
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    const-string v5, " IPv6 Address : "

    #@4a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v4

    #@4e
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v4

    #@56
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 245
    const/4 v1, 0x0

    #@5a
    .local v1, dnsExtraIndex:I
    :goto_5a
    const/4 v3, 0x2

    #@5b
    if-ge v1, v3, :cond_98

    #@5d
    .line 246
    sget-object v3, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->EXTRA_CONTEXT_DNS:[Ljava/lang/String;

    #@5f
    aget-object v3, v3, v1

    #@61
    aget-object v4, p4, v1

    #@63
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@66
    .line 247
    sget-object v3, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->EXTRA_CONTEXT_V6DNS:[Ljava/lang/String;

    #@68
    aget-object v3, v3, v1

    #@6a
    aget-object v4, p5, v1

    #@6c
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@6f
    .line 248
    const-string v3, "PDPContextStateBroadcaster"

    #@71
    new-instance v4, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v5, "sendPDPContextConnected - V4 Dns : "

    #@78
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v4

    #@7c
    aget-object v5, p4, v1

    #@7e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    const-string v5, " v6 Dns : "

    #@84
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v4

    #@88
    aget-object v5, p5, v1

    #@8a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v4

    #@8e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v4

    #@92
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 245
    add-int/lit8 v1, v1, 0x1

    #@97
    goto :goto_5a

    #@98
    .line 250
    :cond_98
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->broadcast(Landroid/content/Intent;)V

    #@9b
    .line 252
    .end local v1           #dnsExtraIndex:I
    .end local v2           #intent:Landroid/content/Intent;
    :cond_9b
    return-void
.end method

.method private sendPDPContextDisconnected(ILjava/lang/Integer;)V
    .registers 10
    .parameter "apnId"
    .parameter "termCode"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 256
    new-instance v0, Ljava/lang/Integer;

    #@3
    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(I)V

    #@6
    .line 257
    .local v0, apnIdObject:Ljava/lang/Integer;
    iget-object v3, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mContextIdMap:Ljava/util/HashMap;

    #@8
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Ljava/lang/Integer;

    #@e
    .line 258
    .local v1, contextId:Ljava/lang/Integer;
    if-eqz v1, :cond_8e

    #@10
    .line 260
    iget-object v3, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mContextIdMap:Ljava/util/HashMap;

    #@12
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    .line 261
    const-string v3, "DISCONNECTED"

    #@17
    invoke-static {v3, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->createIntent(Ljava/lang/String;Ljava/lang/Integer;)Landroid/content/Intent;

    #@1a
    move-result-object v2

    #@1b
    .line 262
    .local v2, intent:Landroid/content/Intent;
    const-string v3, "ContextInitiator"

    #@1d
    const-string v4, "USER"

    #@1f
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@22
    .line 263
    const-string v3, "ContextTermCode"

    #@24
    invoke-virtual {p2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2b
    .line 264
    const-string v3, "ContextErrorCode"

    #@2d
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@34
    .line 265
    const-string v3, "PDPContextStateBroadcaster"

    #@36
    new-instance v4, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v5, "sendPDPContextDisconnected - ID : "

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 266
    const-string v3, "PDPContextStateBroadcaster"

    #@4e
    const-string v4, "sendPDPContextDisconnected - Initiator : USER"

    #@50
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 267
    const-string v3, "PDPContextStateBroadcaster"

    #@55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "sendPDPContextDisconnected - Term Code : "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {p2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 268
    const-string v3, "PDPContextStateBroadcaster"

    #@71
    new-instance v4, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v5, "sendPDPContextDisconnected - Error Code : "

    #@78
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v4

    #@7c
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@7f
    move-result-object v5

    #@80
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v4

    #@84
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v4

    #@88
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 269
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->broadcast(Landroid/content/Intent;)V

    #@8e
    .line 271
    .end local v2           #intent:Landroid/content/Intent;
    :cond_8e
    return-void
.end method

.method private sendPDPContextRequested(ILjava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "apnId"
    .parameter "contextType"
    .parameter "contextAPN"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 216
    invoke-direct {p0}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->getNextContextId()I

    #@4
    move-result v1

    #@5
    .line 217
    .local v1, contextId:I
    new-instance v0, Ljava/lang/Integer;

    #@7
    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(I)V

    #@a
    .line 218
    .local v0, apnIdObject:Ljava/lang/Integer;
    new-instance v2, Ljava/lang/Integer;

    #@c
    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    #@f
    .line 219
    .local v2, contextIdObject:Ljava/lang/Integer;
    iget-object v4, p0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->mContextIdMap:Ljava/util/HashMap;

    #@11
    invoke-virtual {v4, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 221
    const-string v4, "REQUEST"

    #@16
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19
    move-result-object v5

    #@1a
    invoke-static {v4, v5}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->createIntent(Ljava/lang/String;Ljava/lang/Integer;)Landroid/content/Intent;

    #@1d
    move-result-object v3

    #@1e
    .line 222
    .local v3, intent:Landroid/content/Intent;
    const-string v4, "ContextInitiator"

    #@20
    const-string v5, "USER"

    #@22
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@25
    .line 223
    const-string v4, "ContextType"

    #@27
    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2a
    .line 224
    const-string v4, "ContextNSAPI"

    #@2c
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@33
    .line 225
    const-string v4, "ContextSAPI"

    #@35
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3c
    .line 226
    const-string v4, "ContextAPN"

    #@3e
    invoke-virtual {v3, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@41
    .line 227
    const-string v4, "PDPContextStateBroadcaster"

    #@43
    new-instance v5, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v6, "sendPDPContextRequested - ID : "

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v5

    #@56
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 228
    const-string v4, "PDPContextStateBroadcaster"

    #@5b
    const-string v5, "sendPDPContextRequested - Initiator : USER"

    #@5d
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 229
    const-string v4, "PDPContextStateBroadcaster"

    #@62
    new-instance v5, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v6, "sendPDPContextRequested - Type : "

    #@69
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v5

    #@75
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 230
    const-string v4, "PDPContextStateBroadcaster"

    #@7a
    new-instance v5, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v6, "sendPDPContextRequested - NSAPI : "

    #@81
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v5

    #@85
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@88
    move-result-object v6

    #@89
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v5

    #@8d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v5

    #@91
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 231
    const-string v4, "PDPContextStateBroadcaster"

    #@96
    new-instance v5, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v6, "sendPDPContextRequested - SAPI : "

    #@9d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v5

    #@a1
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@a4
    move-result-object v6

    #@a5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v5

    #@a9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v5

    #@ad
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 232
    const-string v4, "PDPContextStateBroadcaster"

    #@b2
    new-instance v5, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v6, "sendPDPContextRequested - APN : "

    #@b9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v5

    #@bd
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v5

    #@c1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v5

    #@c5
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c8
    .line 233
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->broadcast(Landroid/content/Intent;)V

    #@cb
    .line 234
    return-void
.end method

.method public static sendRequested(ILjava/lang/String;)V
    .registers 4
    .parameter "contextId"
    .parameter "apnName"

    #@0
    .prologue
    .line 26
    sget-object v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sInstance:Lcom/android/internal/telephony/PDPContextStateBroadcaster;

    #@2
    if-eqz v0, :cond_d

    #@4
    if-eqz p1, :cond_d

    #@6
    .line 28
    :try_start_6
    sget-object v0, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sInstance:Lcom/android/internal/telephony/PDPContextStateBroadcaster;

    #@8
    const-string v1, "PRIMARY"

    #@a
    invoke-direct {v0, p0, v1, p1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sendPDPContextRequested(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_d} :catch_e

    #@d
    .line 32
    :cond_d
    :goto_d
    return-void

    #@e
    .line 29
    :catch_e
    move-exception v0

    #@f
    goto :goto_d
.end method
