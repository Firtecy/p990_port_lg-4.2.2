.class Lcom/android/internal/telephony/LgeTimeZoneMonitor$1;
.super Landroid/content/BroadcastReceiver;
.source "LgeTimeZoneMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LgeTimeZoneMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/LgeTimeZoneMonitor;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 77
    iput-object p1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$1;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 80
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 81
    .local v0, action:Ljava/lang/String;
    const-string v1, "android.intent.action.NETWORK_SET_TIMEZONE"

    #@6
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_2a

    #@c
    .line 82
    iget-object v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$1;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "intent received : "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@24
    .line 83
    iget-object v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$1;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@26
    invoke-static {v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->access$000(Lcom/android/internal/telephony/LgeTimeZoneMonitor;)V

    #@29
    .line 88
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 84
    :cond_2a
    const-string v1, "android.intent.action.NETWORK_SET_TIME"

    #@2c
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v1

    #@30
    if-eqz v1, :cond_29

    #@32
    .line 85
    iget-object v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$1;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@34
    new-instance v2, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v3, "intent received : "

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@4a
    .line 86
    iget-object v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$1;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@4c
    invoke-static {v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->access$000(Lcom/android/internal/telephony/LgeTimeZoneMonitor;)V

    #@4f
    goto :goto_29
.end method
