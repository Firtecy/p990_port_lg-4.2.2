.class Lcom/android/internal/telephony/gfit/GfitUtils$1;
.super Landroid/content/BroadcastReceiver;
.source "GfitUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gfit/GfitUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/gfit/GfitUtils;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/gfit/GfitUtils;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 243
    iput-object p1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$1;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/16 v6, 0xc9

    #@2
    const/4 v5, 0x0

    #@3
    .line 246
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    const-string v3, "android.intent.action.SIM_STATE_CHANGED"

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_42

    #@f
    .line 247
    const-string v2, "ss"

    #@11
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 248
    .local v1, stateExtra:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils$1;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@17
    new-instance v3, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v4, "receive ACTION_SIM_STATE_CHANGED stateExtra = "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$000(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V

    #@2d
    .line 249
    const/4 v0, 0x0

    #@2e
    .line 251
    .local v0, isUiccInserted:I
    const-string v2, "ABSENT"

    #@30
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v2

    #@34
    if-eqz v2, :cond_43

    #@36
    .line 252
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils$1;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@38
    const-string v3, "Display VZW_GFIT_ICC_ABSENT after 3000 mSec"

    #@3a
    invoke-static {v2, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$000(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V

    #@3d
    .line 253
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils$1;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@3f
    invoke-static {v2, v6, v5, v5, v5}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$100(Lcom/android/internal/telephony/gfit/GfitUtils;IIII)V

    #@42
    .line 274
    .end local v0           #isUiccInserted:I
    .end local v1           #stateExtra:Ljava/lang/String;
    :cond_42
    :goto_42
    return-void

    #@43
    .line 258
    .restart local v0       #isUiccInserted:I
    .restart local v1       #stateExtra:Ljava/lang/String;
    :cond_43
    const-string v2, "LOADED"

    #@45
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v2

    #@49
    if-eqz v2, :cond_42

    #@4b
    .line 259
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils$1;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@4d
    const-string v3, "Display VZW_GFIT_ICC_READY after 3000 mSec"

    #@4f
    invoke-static {v2, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$000(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V

    #@52
    .line 261
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils$1;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@54
    invoke-virtual {v2, v6}, Lcom/android/internal/telephony/gfit/GfitUtils;->hasMessages(I)Z

    #@57
    move-result v2

    #@58
    if-eqz v2, :cond_66

    #@5a
    .line 262
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils$1;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@5c
    const-string v3, "remove EVENT_GFIT_HANDLE_NETWORK_MODE_AFTER_DELAY from MSG queue"

    #@5e
    invoke-static {v2, v3}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$000(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V

    #@61
    .line 263
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils$1;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@63
    invoke-virtual {v2, v6}, Lcom/android/internal/telephony/gfit/GfitUtils;->removeMessages(I)V

    #@66
    .line 266
    :cond_66
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils$1;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@68
    const/4 v3, 0x1

    #@69
    const/16 v4, 0xbb8

    #@6b
    invoke-static {v2, v6, v3, v5, v4}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$100(Lcom/android/internal/telephony/gfit/GfitUtils;IIII)V

    #@6e
    .line 270
    iget-object v2, p0, Lcom/android/internal/telephony/gfit/GfitUtils$1;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@70
    invoke-static {v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$200(Lcom/android/internal/telephony/gfit/GfitUtils;)V

    #@73
    goto :goto_42
.end method
