.class public final enum Lcom/android/internal/telephony/cat/Tone;
.super Ljava/lang/Enum;
.source "Tone.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/cat/Tone;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/cat/Tone;

.field public static final enum BUSY:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum CALL_WAITING:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum CONGESTION:Lcom/android/internal/telephony/cat/Tone;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/telephony/cat/Tone;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CRITICAL_ALERT:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum DIAL:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum ERROR_SPECIAL_INFO:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum GENERAL_BEEP:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum HAPPY:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum INCOMING_SMS:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum INCOMING_SPEECH_CALL:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum MELODY_1:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum MELODY_2:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum MELODY_3:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum MELODY_4:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum MELODY_5:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum MELODY_6:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum MELODY_7:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum MELODY_8:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum MESSAGE_RECEIVED:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum NEGATIVE_ACK:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum POSITIVE_ACK:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum QUESTION:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum RADIO_PATH_ACK:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum RADIO_PATH_NOT_AVAILABLE:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum RINGING:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum SAD:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum URGENT:Lcom/android/internal/telephony/cat/Tone;

.field public static final enum VIBRATE_ONLY:Lcom/android/internal/telephony/cat/Tone;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x5

    #@1
    const/4 v7, 0x4

    #@2
    const/4 v6, 0x3

    #@3
    const/4 v5, 0x2

    #@4
    const/4 v4, 0x1

    #@5
    .line 34
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@7
    const-string v1, "DIAL"

    #@9
    const/4 v2, 0x0

    #@a
    invoke-direct {v0, v1, v2, v4}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@d
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->DIAL:Lcom/android/internal/telephony/cat/Tone;

    #@f
    .line 39
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@11
    const-string v1, "BUSY"

    #@13
    invoke-direct {v0, v1, v4, v5}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@16
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->BUSY:Lcom/android/internal/telephony/cat/Tone;

    #@18
    .line 44
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@1a
    const-string v1, "CONGESTION"

    #@1c
    invoke-direct {v0, v1, v5, v6}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@1f
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->CONGESTION:Lcom/android/internal/telephony/cat/Tone;

    #@21
    .line 49
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@23
    const-string v1, "RADIO_PATH_ACK"

    #@25
    invoke-direct {v0, v1, v6, v7}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@28
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->RADIO_PATH_ACK:Lcom/android/internal/telephony/cat/Tone;

    #@2a
    .line 54
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@2c
    const-string v1, "RADIO_PATH_NOT_AVAILABLE"

    #@2e
    invoke-direct {v0, v1, v7, v8}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@31
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->RADIO_PATH_NOT_AVAILABLE:Lcom/android/internal/telephony/cat/Tone;

    #@33
    .line 59
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@35
    const-string v1, "ERROR_SPECIAL_INFO"

    #@37
    const/4 v2, 0x6

    #@38
    invoke-direct {v0, v1, v8, v2}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@3b
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->ERROR_SPECIAL_INFO:Lcom/android/internal/telephony/cat/Tone;

    #@3d
    .line 64
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@3f
    const-string v1, "CALL_WAITING"

    #@41
    const/4 v2, 0x6

    #@42
    const/4 v3, 0x7

    #@43
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@46
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->CALL_WAITING:Lcom/android/internal/telephony/cat/Tone;

    #@48
    .line 69
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@4a
    const-string v1, "RINGING"

    #@4c
    const/4 v2, 0x7

    #@4d
    const/16 v3, 0x8

    #@4f
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@52
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->RINGING:Lcom/android/internal/telephony/cat/Tone;

    #@54
    .line 76
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@56
    const-string v1, "GENERAL_BEEP"

    #@58
    const/16 v2, 0x8

    #@5a
    const/16 v3, 0x10

    #@5c
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@5f
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->GENERAL_BEEP:Lcom/android/internal/telephony/cat/Tone;

    #@61
    .line 81
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@63
    const-string v1, "POSITIVE_ACK"

    #@65
    const/16 v2, 0x9

    #@67
    const/16 v3, 0x11

    #@69
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@6c
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->POSITIVE_ACK:Lcom/android/internal/telephony/cat/Tone;

    #@6e
    .line 86
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@70
    const-string v1, "NEGATIVE_ACK"

    #@72
    const/16 v2, 0xa

    #@74
    const/16 v3, 0x12

    #@76
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@79
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->NEGATIVE_ACK:Lcom/android/internal/telephony/cat/Tone;

    #@7b
    .line 91
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@7d
    const-string v1, "INCOMING_SPEECH_CALL"

    #@7f
    const/16 v2, 0xb

    #@81
    const/16 v3, 0x13

    #@83
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@86
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->INCOMING_SPEECH_CALL:Lcom/android/internal/telephony/cat/Tone;

    #@88
    .line 96
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@8a
    const-string v1, "INCOMING_SMS"

    #@8c
    const/16 v2, 0xc

    #@8e
    const/16 v3, 0x14

    #@90
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@93
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->INCOMING_SMS:Lcom/android/internal/telephony/cat/Tone;

    #@95
    .line 104
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@97
    const-string v1, "CRITICAL_ALERT"

    #@99
    const/16 v2, 0xd

    #@9b
    const/16 v3, 0x15

    #@9d
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@a0
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->CRITICAL_ALERT:Lcom/android/internal/telephony/cat/Tone;

    #@a2
    .line 109
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@a4
    const-string v1, "VIBRATE_ONLY"

    #@a6
    const/16 v2, 0xe

    #@a8
    const/16 v3, 0x20

    #@aa
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@ad
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->VIBRATE_ONLY:Lcom/android/internal/telephony/cat/Tone;

    #@af
    .line 116
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@b1
    const-string v1, "HAPPY"

    #@b3
    const/16 v2, 0xf

    #@b5
    const/16 v3, 0x30

    #@b7
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@ba
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->HAPPY:Lcom/android/internal/telephony/cat/Tone;

    #@bc
    .line 121
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@be
    const-string v1, "SAD"

    #@c0
    const/16 v2, 0x10

    #@c2
    const/16 v3, 0x31

    #@c4
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@c7
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->SAD:Lcom/android/internal/telephony/cat/Tone;

    #@c9
    .line 126
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@cb
    const-string v1, "URGENT"

    #@cd
    const/16 v2, 0x11

    #@cf
    const/16 v3, 0x32

    #@d1
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@d4
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->URGENT:Lcom/android/internal/telephony/cat/Tone;

    #@d6
    .line 131
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@d8
    const-string v1, "QUESTION"

    #@da
    const/16 v2, 0x12

    #@dc
    const/16 v3, 0x33

    #@de
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@e1
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->QUESTION:Lcom/android/internal/telephony/cat/Tone;

    #@e3
    .line 136
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@e5
    const-string v1, "MESSAGE_RECEIVED"

    #@e7
    const/16 v2, 0x13

    #@e9
    const/16 v3, 0x34

    #@eb
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@ee
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->MESSAGE_RECEIVED:Lcom/android/internal/telephony/cat/Tone;

    #@f0
    .line 139
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@f2
    const-string v1, "MELODY_1"

    #@f4
    const/16 v2, 0x14

    #@f6
    const/16 v3, 0x40

    #@f8
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@fb
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->MELODY_1:Lcom/android/internal/telephony/cat/Tone;

    #@fd
    .line 140
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@ff
    const-string v1, "MELODY_2"

    #@101
    const/16 v2, 0x15

    #@103
    const/16 v3, 0x41

    #@105
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@108
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->MELODY_2:Lcom/android/internal/telephony/cat/Tone;

    #@10a
    .line 141
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@10c
    const-string v1, "MELODY_3"

    #@10e
    const/16 v2, 0x16

    #@110
    const/16 v3, 0x42

    #@112
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@115
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->MELODY_3:Lcom/android/internal/telephony/cat/Tone;

    #@117
    .line 142
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@119
    const-string v1, "MELODY_4"

    #@11b
    const/16 v2, 0x17

    #@11d
    const/16 v3, 0x43

    #@11f
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@122
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->MELODY_4:Lcom/android/internal/telephony/cat/Tone;

    #@124
    .line 143
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@126
    const-string v1, "MELODY_5"

    #@128
    const/16 v2, 0x18

    #@12a
    const/16 v3, 0x44

    #@12c
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@12f
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->MELODY_5:Lcom/android/internal/telephony/cat/Tone;

    #@131
    .line 144
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@133
    const-string v1, "MELODY_6"

    #@135
    const/16 v2, 0x19

    #@137
    const/16 v3, 0x45

    #@139
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@13c
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->MELODY_6:Lcom/android/internal/telephony/cat/Tone;

    #@13e
    .line 145
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@140
    const-string v1, "MELODY_7"

    #@142
    const/16 v2, 0x1a

    #@144
    const/16 v3, 0x46

    #@146
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@149
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->MELODY_7:Lcom/android/internal/telephony/cat/Tone;

    #@14b
    .line 146
    new-instance v0, Lcom/android/internal/telephony/cat/Tone;

    #@14d
    const-string v1, "MELODY_8"

    #@14f
    const/16 v2, 0x1b

    #@151
    const/16 v3, 0x47

    #@153
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/Tone;-><init>(Ljava/lang/String;II)V

    #@156
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->MELODY_8:Lcom/android/internal/telephony/cat/Tone;

    #@158
    .line 28
    const/16 v0, 0x1c

    #@15a
    new-array v0, v0, [Lcom/android/internal/telephony/cat/Tone;

    #@15c
    const/4 v1, 0x0

    #@15d
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->DIAL:Lcom/android/internal/telephony/cat/Tone;

    #@15f
    aput-object v2, v0, v1

    #@161
    sget-object v1, Lcom/android/internal/telephony/cat/Tone;->BUSY:Lcom/android/internal/telephony/cat/Tone;

    #@163
    aput-object v1, v0, v4

    #@165
    sget-object v1, Lcom/android/internal/telephony/cat/Tone;->CONGESTION:Lcom/android/internal/telephony/cat/Tone;

    #@167
    aput-object v1, v0, v5

    #@169
    sget-object v1, Lcom/android/internal/telephony/cat/Tone;->RADIO_PATH_ACK:Lcom/android/internal/telephony/cat/Tone;

    #@16b
    aput-object v1, v0, v6

    #@16d
    sget-object v1, Lcom/android/internal/telephony/cat/Tone;->RADIO_PATH_NOT_AVAILABLE:Lcom/android/internal/telephony/cat/Tone;

    #@16f
    aput-object v1, v0, v7

    #@171
    sget-object v1, Lcom/android/internal/telephony/cat/Tone;->ERROR_SPECIAL_INFO:Lcom/android/internal/telephony/cat/Tone;

    #@173
    aput-object v1, v0, v8

    #@175
    const/4 v1, 0x6

    #@176
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->CALL_WAITING:Lcom/android/internal/telephony/cat/Tone;

    #@178
    aput-object v2, v0, v1

    #@17a
    const/4 v1, 0x7

    #@17b
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->RINGING:Lcom/android/internal/telephony/cat/Tone;

    #@17d
    aput-object v2, v0, v1

    #@17f
    const/16 v1, 0x8

    #@181
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->GENERAL_BEEP:Lcom/android/internal/telephony/cat/Tone;

    #@183
    aput-object v2, v0, v1

    #@185
    const/16 v1, 0x9

    #@187
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->POSITIVE_ACK:Lcom/android/internal/telephony/cat/Tone;

    #@189
    aput-object v2, v0, v1

    #@18b
    const/16 v1, 0xa

    #@18d
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->NEGATIVE_ACK:Lcom/android/internal/telephony/cat/Tone;

    #@18f
    aput-object v2, v0, v1

    #@191
    const/16 v1, 0xb

    #@193
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->INCOMING_SPEECH_CALL:Lcom/android/internal/telephony/cat/Tone;

    #@195
    aput-object v2, v0, v1

    #@197
    const/16 v1, 0xc

    #@199
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->INCOMING_SMS:Lcom/android/internal/telephony/cat/Tone;

    #@19b
    aput-object v2, v0, v1

    #@19d
    const/16 v1, 0xd

    #@19f
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->CRITICAL_ALERT:Lcom/android/internal/telephony/cat/Tone;

    #@1a1
    aput-object v2, v0, v1

    #@1a3
    const/16 v1, 0xe

    #@1a5
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->VIBRATE_ONLY:Lcom/android/internal/telephony/cat/Tone;

    #@1a7
    aput-object v2, v0, v1

    #@1a9
    const/16 v1, 0xf

    #@1ab
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->HAPPY:Lcom/android/internal/telephony/cat/Tone;

    #@1ad
    aput-object v2, v0, v1

    #@1af
    const/16 v1, 0x10

    #@1b1
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->SAD:Lcom/android/internal/telephony/cat/Tone;

    #@1b3
    aput-object v2, v0, v1

    #@1b5
    const/16 v1, 0x11

    #@1b7
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->URGENT:Lcom/android/internal/telephony/cat/Tone;

    #@1b9
    aput-object v2, v0, v1

    #@1bb
    const/16 v1, 0x12

    #@1bd
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->QUESTION:Lcom/android/internal/telephony/cat/Tone;

    #@1bf
    aput-object v2, v0, v1

    #@1c1
    const/16 v1, 0x13

    #@1c3
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->MESSAGE_RECEIVED:Lcom/android/internal/telephony/cat/Tone;

    #@1c5
    aput-object v2, v0, v1

    #@1c7
    const/16 v1, 0x14

    #@1c9
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->MELODY_1:Lcom/android/internal/telephony/cat/Tone;

    #@1cb
    aput-object v2, v0, v1

    #@1cd
    const/16 v1, 0x15

    #@1cf
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->MELODY_2:Lcom/android/internal/telephony/cat/Tone;

    #@1d1
    aput-object v2, v0, v1

    #@1d3
    const/16 v1, 0x16

    #@1d5
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->MELODY_3:Lcom/android/internal/telephony/cat/Tone;

    #@1d7
    aput-object v2, v0, v1

    #@1d9
    const/16 v1, 0x17

    #@1db
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->MELODY_4:Lcom/android/internal/telephony/cat/Tone;

    #@1dd
    aput-object v2, v0, v1

    #@1df
    const/16 v1, 0x18

    #@1e1
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->MELODY_5:Lcom/android/internal/telephony/cat/Tone;

    #@1e3
    aput-object v2, v0, v1

    #@1e5
    const/16 v1, 0x19

    #@1e7
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->MELODY_6:Lcom/android/internal/telephony/cat/Tone;

    #@1e9
    aput-object v2, v0, v1

    #@1eb
    const/16 v1, 0x1a

    #@1ed
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->MELODY_7:Lcom/android/internal/telephony/cat/Tone;

    #@1ef
    aput-object v2, v0, v1

    #@1f1
    const/16 v1, 0x1b

    #@1f3
    sget-object v2, Lcom/android/internal/telephony/cat/Tone;->MELODY_8:Lcom/android/internal/telephony/cat/Tone;

    #@1f5
    aput-object v2, v0, v1

    #@1f7
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->$VALUES:[Lcom/android/internal/telephony/cat/Tone;

    #@1f9
    .line 181
    new-instance v0, Lcom/android/internal/telephony/cat/Tone$1;

    #@1fb
    invoke-direct {v0}, Lcom/android/internal/telephony/cat/Tone$1;-><init>()V

    #@1fe
    sput-object v0, Lcom/android/internal/telephony/cat/Tone;->CREATOR:Landroid/os/Parcelable$Creator;

    #@200
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 150
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 151
    iput p3, p0, Lcom/android/internal/telephony/cat/Tone;->mValue:I

    #@5
    .line 152
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILandroid/os/Parcel;)V
    .registers 5
    .parameter
    .parameter
    .parameter "in"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 170
    invoke-virtual {p3}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Lcom/android/internal/telephony/cat/Tone;->mValue:I

    #@9
    .line 171
    return-void
.end method

.method public static fromInt(I)Lcom/android/internal/telephony/cat/Tone;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 161
    invoke-static {}, Lcom/android/internal/telephony/cat/Tone;->values()[Lcom/android/internal/telephony/cat/Tone;

    #@3
    move-result-object v0

    #@4
    .local v0, arr$:[Lcom/android/internal/telephony/cat/Tone;
    array-length v3, v0

    #@5
    .local v3, len$:I
    const/4 v2, 0x0

    #@6
    .local v2, i$:I
    :goto_6
    if-ge v2, v3, :cond_12

    #@8
    aget-object v1, v0, v2

    #@a
    .line 162
    .local v1, e:Lcom/android/internal/telephony/cat/Tone;
    iget v4, v1, Lcom/android/internal/telephony/cat/Tone;->mValue:I

    #@c
    if-ne v4, p0, :cond_f

    #@e
    .line 166
    .end local v1           #e:Lcom/android/internal/telephony/cat/Tone;
    :goto_e
    return-object v1

    #@f
    .line 161
    .restart local v1       #e:Lcom/android/internal/telephony/cat/Tone;
    :cond_f
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_6

    #@12
    .line 166
    .end local v1           #e:Lcom/android/internal/telephony/cat/Tone;
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_e
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/cat/Tone;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 28
    const-class v0, Lcom/android/internal/telephony/cat/Tone;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/cat/Tone;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/cat/Tone;
    .registers 1

    #@0
    .prologue
    .line 28
    sget-object v0, Lcom/android/internal/telephony/cat/Tone;->$VALUES:[Lcom/android/internal/telephony/cat/Tone;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/cat/Tone;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/cat/Tone;

    #@8
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 178
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/Tone;->ordinal()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 175
    return-void
.end method
