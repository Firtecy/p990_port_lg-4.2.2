.class Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;
.super Ljava/lang/Object;
.source "RuimRecords.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/RuimRecords;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EfCsimImsimLoaded"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/uicc/RuimRecords;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/uicc/RuimRecords;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 440
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/uicc/RuimRecords;Lcom/android/internal/telephony/uicc/RuimRecords$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 440
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;)V

    #@3
    return-void
.end method


# virtual methods
.method public getEfName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 442
    const-string v0, "EF_CSIM_IMSIM"

    #@2
    return-object v0
.end method

.method public onRecordLoaded(Landroid/os/AsyncResult;)V
    .registers 15
    .parameter "ar"

    #@0
    .prologue
    .line 446
    iget-object v8, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2
    check-cast v8, [B

    #@4
    move-object v1, v8

    #@5
    check-cast v1, [B

    #@7
    .line 447
    .local v1, data:[B
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@9
    new-instance v9, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v10, "CSIM_IMSIM="

    #@10
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v9

    #@14
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@17
    move-result-object v10

    #@18
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v9

    #@1c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v9

    #@20
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@23
    .line 451
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@25
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@28
    move-result-object v9

    #@29
    iput-object v9, v8, Lcom/android/internal/telephony/uicc/IccRecords;->imsi_m:Ljava/lang/String;

    #@2b
    .line 452
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@2d
    new-instance v9, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v10, "[LGE UICC] imsi_m present="

    #@34
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v9

    #@38
    iget-object v10, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@3a
    iget-object v10, v10, Lcom/android/internal/telephony/uicc/IccRecords;->imsi_m:Ljava/lang/String;

    #@3c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v9

    #@40
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v9

    #@44
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@47
    .line 455
    const/4 v8, 0x7

    #@48
    aget-byte v8, v1, v8

    #@4a
    and-int/lit16 v8, v8, 0x80

    #@4c
    const/16 v9, 0x80

    #@4e
    if-ne v8, v9, :cond_169

    #@50
    const/4 v6, 0x1

    #@51
    .line 457
    .local v6, provisioned:Z
    :goto_51
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@53
    const/4 v8, 0x7

    #@54
    aget-byte v8, v1, v8

    #@56
    and-int/lit16 v8, v8, 0x80

    #@58
    const/16 v10, 0x80

    #@5a
    if-ne v8, v10, :cond_16c

    #@5c
    const/4 v8, 0x1

    #@5d
    :goto_5d
    iput-boolean v8, v9, Lcom/android/internal/telephony/uicc/IccRecords;->imsi_m_provisioned:Z

    #@5f
    .line 458
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@61
    new-instance v9, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v10, "[LGE UICC] imsi_m provisioned ="

    #@68
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v9

    #@6c
    iget-object v10, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@6e
    iget-boolean v10, v10, Lcom/android/internal/telephony/uicc/IccRecords;->imsi_m_provisioned:Z

    #@70
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@73
    move-result-object v9

    #@74
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v9

    #@78
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@7b
    .line 461
    if-eqz v6, :cond_16f

    #@7d
    .line 462
    const/4 v8, 0x2

    #@7e
    aget-byte v8, v1, v8

    #@80
    and-int/lit8 v8, v8, 0x3

    #@82
    shl-int/lit8 v8, v8, 0x8

    #@84
    const/4 v9, 0x1

    #@85
    aget-byte v9, v1, v9

    #@87
    and-int/lit16 v9, v9, 0xff

    #@89
    add-int v3, v8, v9

    #@8b
    .line 463
    .local v3, first3digits:I
    const/4 v8, 0x5

    #@8c
    aget-byte v8, v1, v8

    #@8e
    and-int/lit16 v8, v8, 0xff

    #@90
    shl-int/lit8 v8, v8, 0x8

    #@92
    const/4 v9, 0x4

    #@93
    aget-byte v9, v1, v9

    #@95
    and-int/lit16 v9, v9, 0xff

    #@97
    or-int/2addr v8, v9

    #@98
    shr-int/lit8 v7, v8, 0x6

    #@9a
    .line 464
    .local v7, second3digits:I
    const/4 v8, 0x4

    #@9b
    aget-byte v8, v1, v8

    #@9d
    shr-int/lit8 v8, v8, 0x2

    #@9f
    and-int/lit8 v2, v8, 0xf

    #@a1
    .line 465
    .local v2, digit7:I
    const/16 v8, 0x9

    #@a3
    if-le v2, v8, :cond_a6

    #@a5
    const/4 v2, 0x0

    #@a6
    .line 466
    :cond_a6
    const/4 v8, 0x4

    #@a7
    aget-byte v8, v1, v8

    #@a9
    and-int/lit8 v8, v8, 0x3

    #@ab
    shl-int/lit8 v8, v8, 0x8

    #@ad
    const/4 v9, 0x3

    #@ae
    aget-byte v9, v1, v9

    #@b0
    and-int/lit16 v9, v9, 0xff

    #@b2
    or-int v5, v8, v9

    #@b4
    .line 467
    .local v5, last3digits:I
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@b6
    invoke-static {v8, v3}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$300(Lcom/android/internal/telephony/uicc/RuimRecords;I)I

    #@b9
    move-result v3

    #@ba
    .line 468
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@bc
    invoke-static {v8, v7}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$300(Lcom/android/internal/telephony/uicc/RuimRecords;I)I

    #@bf
    move-result v7

    #@c0
    .line 469
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@c2
    invoke-static {v8, v5}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$300(Lcom/android/internal/telephony/uicc/RuimRecords;I)I

    #@c5
    move-result v5

    #@c6
    .line 471
    new-instance v0, Ljava/lang/StringBuilder;

    #@c8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@cb
    .line 472
    .local v0, builder:Ljava/lang/StringBuilder;
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@cd
    const-string v9, "%03d"

    #@cf
    const/4 v10, 0x1

    #@d0
    new-array v10, v10, [Ljava/lang/Object;

    #@d2
    const/4 v11, 0x0

    #@d3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d6
    move-result-object v12

    #@d7
    aput-object v12, v10, v11

    #@d9
    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@dc
    move-result-object v8

    #@dd
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    .line 473
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@e2
    const-string v9, "%03d"

    #@e4
    const/4 v10, 0x1

    #@e5
    new-array v10, v10, [Ljava/lang/Object;

    #@e7
    const/4 v11, 0x0

    #@e8
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@eb
    move-result-object v12

    #@ec
    aput-object v12, v10, v11

    #@ee
    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@f1
    move-result-object v8

    #@f2
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    .line 474
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@f7
    const-string v9, "%d"

    #@f9
    const/4 v10, 0x1

    #@fa
    new-array v10, v10, [Ljava/lang/Object;

    #@fc
    const/4 v11, 0x0

    #@fd
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@100
    move-result-object v12

    #@101
    aput-object v12, v10, v11

    #@103
    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@106
    move-result-object v8

    #@107
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    .line 475
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@10c
    const-string v9, "%03d"

    #@10e
    const/4 v10, 0x1

    #@10f
    new-array v10, v10, [Ljava/lang/Object;

    #@111
    const/4 v11, 0x0

    #@112
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@115
    move-result-object v12

    #@116
    aput-object v12, v10, v11

    #@118
    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@11b
    move-result-object v8

    #@11c
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    .line 476
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@121
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v9

    #@125
    invoke-static {v8, v9}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$402(Lcom/android/internal/telephony/uicc/RuimRecords;Ljava/lang/String;)Ljava/lang/String;

    #@128
    .line 477
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@12a
    new-instance v9, Ljava/lang/StringBuilder;

    #@12c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@12f
    const-string v10, "min present="

    #@131
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v9

    #@135
    iget-object v10, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@137
    invoke-static {v10}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$400(Lcom/android/internal/telephony/uicc/RuimRecords;)Ljava/lang/String;

    #@13a
    move-result-object v10

    #@13b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v9

    #@13f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@142
    move-result-object v9

    #@143
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@146
    .line 482
    .end local v0           #builder:Ljava/lang/StringBuilder;
    .end local v2           #digit7:I
    .end local v3           #first3digits:I
    .end local v5           #last3digits:I
    .end local v7           #second3digits:I
    :goto_146
    const-string v8, "CDMA"

    #@148
    const-string v9, "Broadcast Received EVENT_IMSI_M "

    #@14a
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14d
    .line 483
    new-instance v4, Landroid/content/Intent;

    #@14f
    const-string v8, "android.intent.action.SIM_STATE_CHANGED"

    #@151
    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@154
    .line 484
    .local v4, intent:Landroid/content/Intent;
    const-string v8, "phoneName"

    #@156
    const-string v9, "Phone"

    #@158
    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@15b
    .line 485
    const-string v8, "ss"

    #@15d
    const-string v9, "IMSI_M"

    #@15f
    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@162
    .line 486
    const-string v8, "android.permission.READ_PHONE_STATE"

    #@164
    const/4 v9, -0x1

    #@165
    invoke-static {v4, v8, v9}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    #@168
    .line 488
    return-void

    #@169
    .line 455
    .end local v4           #intent:Landroid/content/Intent;
    .end local v6           #provisioned:Z
    :cond_169
    const/4 v6, 0x0

    #@16a
    goto/16 :goto_51

    #@16c
    .line 457
    .restart local v6       #provisioned:Z
    :cond_16c
    const/4 v8, 0x0

    #@16d
    goto/16 :goto_5d

    #@16f
    .line 479
    :cond_16f
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimImsimLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@171
    const-string v9, "min not present"

    #@173
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@176
    goto :goto_146
.end method
