.class Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;
.super Ljava/lang/Object;
.source "IccPhonebookProvider.java"

# interfaces
.implements Lcom/android/internal/telephony/IccPhonebookProvider$IBackend;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccPhonebookProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IccPhonebookProviderBackendImp"
.end annotation


# static fields
.field private static final EMPTY_ENTRY:[Ljava/lang/Object; = null

.field private static final EMPTY_GROUP:[Ljava/lang/Object; = null

.field private static final EMPTY_INFO:[Ljava/lang/Object; = null

.field private static final TAG:Ljava/lang/String; = "IccPhonebookProvider"


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 493
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@7
    array-length v0, v0

    #@8
    new-array v0, v0, [Ljava/lang/Object;

    #@a
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@c
    .line 494
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v1

    #@12
    aput-object v1, v0, v3

    #@14
    .line 495
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@16
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19
    move-result-object v1

    #@1a
    aput-object v1, v0, v6

    #@1c
    .line 496
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@1e
    const-string v1, ""

    #@20
    aput-object v1, v0, v4

    #@22
    .line 497
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@24
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v1

    #@28
    aput-object v1, v0, v5

    #@2a
    .line 498
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@2c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f
    move-result-object v1

    #@30
    aput-object v1, v0, v7

    #@32
    .line 499
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@34
    const/4 v1, 0x5

    #@35
    const-string v2, ""

    #@37
    aput-object v2, v0, v1

    #@39
    .line 500
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@3b
    const/4 v1, 0x6

    #@3c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v2

    #@40
    aput-object v2, v0, v1

    #@42
    .line 501
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@44
    const/4 v1, 0x7

    #@45
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@48
    move-result-object v2

    #@49
    aput-object v2, v0, v1

    #@4b
    .line 502
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@4d
    const/16 v1, 0x8

    #@4f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@52
    move-result-object v2

    #@53
    aput-object v2, v0, v1

    #@55
    .line 503
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@57
    const/16 v1, 0x9

    #@59
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5c
    move-result-object v2

    #@5d
    aput-object v2, v0, v1

    #@5f
    .line 504
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@61
    const/16 v1, 0xa

    #@63
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@66
    move-result-object v2

    #@67
    aput-object v2, v0, v1

    #@69
    .line 505
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@6b
    const/16 v1, 0xb

    #@6d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@70
    move-result-object v2

    #@71
    aput-object v2, v0, v1

    #@73
    .line 506
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@75
    const/16 v1, 0xc

    #@77
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7a
    move-result-object v2

    #@7b
    aput-object v2, v0, v1

    #@7d
    .line 507
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@7f
    const/16 v1, 0xd

    #@81
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@84
    move-result-object v2

    #@85
    aput-object v2, v0, v1

    #@87
    .line 508
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@89
    const/16 v1, 0xe

    #@8b
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8e
    move-result-object v2

    #@8f
    aput-object v2, v0, v1

    #@91
    .line 509
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@93
    const/16 v1, 0xf

    #@95
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@98
    move-result-object v2

    #@99
    aput-object v2, v0, v1

    #@9b
    .line 514
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@9d
    array-length v0, v0

    #@9e
    new-array v0, v0, [Ljava/lang/Object;

    #@a0
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@a2
    .line 515
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@a4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a7
    move-result-object v1

    #@a8
    aput-object v1, v0, v3

    #@aa
    .line 516
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@ac
    const-string v1, ""

    #@ae
    aput-object v1, v0, v4

    #@b0
    .line 517
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@b2
    const-string v1, ""

    #@b4
    aput-object v1, v0, v5

    #@b6
    .line 518
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@b8
    const-string v1, ""

    #@ba
    aput-object v1, v0, v6

    #@bc
    .line 519
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@be
    const-string v1, ""

    #@c0
    aput-object v1, v0, v7

    #@c2
    .line 520
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@c4
    const/4 v1, 0x5

    #@c5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c8
    move-result-object v2

    #@c9
    aput-object v2, v0, v1

    #@cb
    .line 525
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->GROUP_PROJECTION:[Ljava/lang/String;

    #@cd
    array-length v0, v0

    #@ce
    new-array v0, v0, [Ljava/lang/Object;

    #@d0
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_GROUP:[Ljava/lang/Object;

    #@d2
    .line 526
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_GROUP:[Ljava/lang/Object;

    #@d4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d7
    move-result-object v1

    #@d8
    aput-object v1, v0, v3

    #@da
    .line 527
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_GROUP:[Ljava/lang/Object;

    #@dc
    const-string v1, ""

    #@de
    aput-object v1, v0, v4

    #@e0
    .line 528
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 532
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 533
    const-string v0, "IccPhonebookProvider"

    #@5
    const-string v1, "*** NoSim IccPhonebookProvider ***"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 534
    return-void
.end method


# virtual methods
.method public deleteEntry(Landroid/content/Context;I)I
    .registers 4
    .parameter "context"
    .parameter "simIndex"

    #@0
    .prologue
    .line 574
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public deleteGroup(Landroid/content/Context;I)I
    .registers 4
    .parameter "context"
    .parameter "groupIndex"

    #@0
    .prologue
    .line 605
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public insertEntry(Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 5
    .parameter "context"
    .parameter "simIndex"
    .parameter "values"

    #@0
    .prologue
    .line 560
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public insertGroup(Landroid/content/Context;Landroid/content/ContentValues;)I
    .registers 4
    .parameter "context"
    .parameter "values"

    #@0
    .prologue
    .line 591
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public readEntry(Landroid/content/Context;I)Landroid/database/Cursor;
    .registers 5
    .parameter "context"
    .parameter "simIndex"

    #@0
    .prologue
    .line 550
    new-instance v0, Landroid/database/MatrixCursor;

    #@2
    sget-object v1, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@4
    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    #@7
    .line 551
    .local v0, cursor:Landroid/database/MatrixCursor;
    sget-object v1, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_ENTRY:[Ljava/lang/Object;

    #@9
    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    #@c
    .line 553
    return-object v0
.end method

.method public readGroup(Landroid/content/Context;I)Landroid/database/Cursor;
    .registers 5
    .parameter "context"
    .parameter "groupIndex"

    #@0
    .prologue
    .line 581
    new-instance v0, Landroid/database/MatrixCursor;

    #@2
    sget-object v1, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@4
    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    #@7
    .line 582
    .local v0, cursor:Landroid/database/MatrixCursor;
    sget-object v1, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_GROUP:[Ljava/lang/Object;

    #@9
    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    #@c
    .line 584
    return-object v0
.end method

.method public readInfo(Landroid/content/Context;)Landroid/database/Cursor;
    .registers 4
    .parameter "Context"

    #@0
    .prologue
    .line 540
    new-instance v0, Landroid/database/MatrixCursor;

    #@2
    sget-object v1, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@4
    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    #@7
    .line 541
    .local v0, cursor:Landroid/database/MatrixCursor;
    sget-object v1, Lcom/android/internal/telephony/IccPhonebookProvider$IccPhonebookProviderBackendImp;->EMPTY_INFO:[Ljava/lang/Object;

    #@9
    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    #@c
    .line 543
    return-object v0
.end method

.method public updateEntry(Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 5
    .parameter "context"
    .parameter "simIndex"
    .parameter "values"

    #@0
    .prologue
    .line 567
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public updateGroup(Landroid/content/Context;ILandroid/content/ContentValues;)I
    .registers 5
    .parameter "context"
    .parameter "groupIndex"
    .parameter "values"

    #@0
    .prologue
    .line 598
    const/4 v0, 0x0

    #@1
    return v0
.end method
