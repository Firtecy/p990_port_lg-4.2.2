.class public final enum Lcom/android/internal/telephony/IPVersion;
.super Ljava/lang/Enum;
.source "IPVersion.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/IPVersion;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/IPVersion;

.field public static final enum INET:Lcom/android/internal/telephony/IPVersion;

.field public static final enum INET6:Lcom/android/internal/telephony/IPVersion;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 33
    new-instance v0, Lcom/android/internal/telephony/IPVersion;

    #@4
    const-string v1, "INET"

    #@6
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IPVersion;-><init>(Ljava/lang/String;I)V

    #@9
    sput-object v0, Lcom/android/internal/telephony/IPVersion;->INET:Lcom/android/internal/telephony/IPVersion;

    #@b
    new-instance v0, Lcom/android/internal/telephony/IPVersion;

    #@d
    const-string v1, "INET6"

    #@f
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/IPVersion;-><init>(Ljava/lang/String;I)V

    #@12
    sput-object v0, Lcom/android/internal/telephony/IPVersion;->INET6:Lcom/android/internal/telephony/IPVersion;

    #@14
    .line 32
    const/4 v0, 0x2

    #@15
    new-array v0, v0, [Lcom/android/internal/telephony/IPVersion;

    #@17
    sget-object v1, Lcom/android/internal/telephony/IPVersion;->INET:Lcom/android/internal/telephony/IPVersion;

    #@19
    aput-object v1, v0, v2

    #@1b
    sget-object v1, Lcom/android/internal/telephony/IPVersion;->INET6:Lcom/android/internal/telephony/IPVersion;

    #@1d
    aput-object v1, v0, v3

    #@1f
    sput-object v0, Lcom/android/internal/telephony/IPVersion;->$VALUES:[Lcom/android/internal/telephony/IPVersion;

    #@21
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/IPVersion;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 32
    const-class v0, Lcom/android/internal/telephony/IPVersion;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/IPVersion;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/IPVersion;
    .registers 1

    #@0
    .prologue
    .line 32
    sget-object v0, Lcom/android/internal/telephony/IPVersion;->$VALUES:[Lcom/android/internal/telephony/IPVersion;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/IPVersion;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/IPVersion;

    #@8
    return-object v0
.end method
