.class Lcom/android/internal/telephony/LGDataRecovery$DataLogFormatter;
.super Ljava/util/logging/Formatter;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGDataRecovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DataLogFormatter"
.end annotation


# instance fields
.field timestamp:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 1798
    invoke-direct {p0}, Ljava/util/logging/Formatter;-><init>()V

    #@3
    .line 1796
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Lcom/android/internal/telephony/LGDataRecovery$DataLogFormatter;->timestamp:Z

    #@6
    .line 1799
    return-void
.end method

.method public constructor <init>(Z)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 1801
    invoke-direct {p0}, Ljava/util/logging/Formatter;-><init>()V

    #@3
    .line 1796
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Lcom/android/internal/telephony/LGDataRecovery$DataLogFormatter;->timestamp:Z

    #@6
    .line 1802
    iput-boolean p1, p0, Lcom/android/internal/telephony/LGDataRecovery$DataLogFormatter;->timestamp:Z

    #@8
    .line 1803
    return-void
.end method


# virtual methods
.method public format(Ljava/util/logging/LogRecord;)Ljava/lang/String;
    .registers 11
    .parameter "r"

    #@0
    .prologue
    .line 1807
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1808
    .local v2, sb:Ljava/lang/StringBuilder;
    iget-boolean v5, p0, Lcom/android/internal/telephony/LGDataRecovery$DataLogFormatter;->timestamp:Z

    #@7
    if-eqz v5, :cond_24

    #@9
    .line 1809
    const-string v5, "yyyy-MM-dd kk:mm:ss"

    #@b
    new-instance v6, Ljava/util/Date;

    #@d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@10
    move-result-wide v7

    #@11
    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    #@14
    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    #@17
    move-result-wide v6

    #@18
    invoke-static {v5, v6, v7}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@1f
    .line 1811
    const-string v5, " [LGDataRecovery] "

    #@21
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    .line 1813
    :cond_24
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/LGDataRecovery$DataLogFormatter;->formatMessage(Ljava/util/logging/LogRecord;)Ljava/lang/String;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    invoke-static {}, Ljava/lang/System;->lineSeparator()Ljava/lang/String;

    #@2f
    move-result-object v6

    #@30
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    .line 1815
    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getThrown()Ljava/lang/Throwable;

    #@36
    move-result-object v5

    #@37
    if-eqz v5, :cond_5a

    #@39
    .line 1816
    const-string v5, "Throwable occurred: "

    #@3b
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    .line 1817
    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getThrown()Ljava/lang/Throwable;

    #@41
    move-result-object v4

    #@42
    .line 1818
    .local v4, t:Ljava/lang/Throwable;
    const/4 v0, 0x0

    #@43
    .line 1820
    .local v0, pw:Ljava/io/PrintWriter;
    :try_start_43
    new-instance v3, Ljava/io/StringWriter;

    #@45
    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    #@48
    .line 1821
    .local v3, sw:Ljava/io/StringWriter;
    new-instance v1, Ljava/io/PrintWriter;

    #@4a
    invoke-direct {v1, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_4d
    .catchall {:try_start_43 .. :try_end_4d} :catchall_5f

    #@4d
    .line 1822
    .end local v0           #pw:Ljava/io/PrintWriter;
    .local v1, pw:Ljava/io/PrintWriter;
    :try_start_4d
    invoke-virtual {v4, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    #@50
    .line 1823
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    #@53
    move-result-object v5

    #@54
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_57
    .catchall {:try_start_4d .. :try_end_57} :catchall_64

    #@57
    .line 1825
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@5a
    .line 1828
    .end local v1           #pw:Ljava/io/PrintWriter;
    .end local v3           #sw:Ljava/io/StringWriter;
    .end local v4           #t:Ljava/lang/Throwable;
    :cond_5a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v5

    #@5e
    return-object v5

    #@5f
    .line 1825
    .restart local v0       #pw:Ljava/io/PrintWriter;
    .restart local v4       #t:Ljava/lang/Throwable;
    :catchall_5f
    move-exception v5

    #@60
    :goto_60
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@63
    throw v5

    #@64
    .end local v0           #pw:Ljava/io/PrintWriter;
    .restart local v1       #pw:Ljava/io/PrintWriter;
    .restart local v3       #sw:Ljava/io/StringWriter;
    :catchall_64
    move-exception v5

    #@65
    move-object v0, v1

    #@66
    .end local v1           #pw:Ljava/io/PrintWriter;
    .restart local v0       #pw:Ljava/io/PrintWriter;
    goto :goto_60
.end method
