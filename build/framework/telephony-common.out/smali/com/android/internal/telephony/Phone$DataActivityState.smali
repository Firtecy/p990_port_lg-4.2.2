.class public final enum Lcom/android/internal/telephony/Phone$DataActivityState;
.super Ljava/lang/Enum;
.source "Phone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/Phone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataActivityState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/Phone$DataActivityState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/Phone$DataActivityState;

.field public static final enum DATAIN:Lcom/android/internal/telephony/Phone$DataActivityState;

.field public static final enum DATAINANDOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

.field public static final enum DATAOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

.field public static final enum DORMANT:Lcom/android/internal/telephony/Phone$DataActivityState;

.field public static final enum NONE:Lcom/android/internal/telephony/Phone$DataActivityState;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 85
    new-instance v0, Lcom/android/internal/telephony/Phone$DataActivityState;

    #@7
    const-string v1, "NONE"

    #@9
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Phone$DataActivityState;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->NONE:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@e
    new-instance v0, Lcom/android/internal/telephony/Phone$DataActivityState;

    #@10
    const-string v1, "DATAIN"

    #@12
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/Phone$DataActivityState;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAIN:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@17
    new-instance v0, Lcom/android/internal/telephony/Phone$DataActivityState;

    #@19
    const-string v1, "DATAOUT"

    #@1b
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/Phone$DataActivityState;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@20
    new-instance v0, Lcom/android/internal/telephony/Phone$DataActivityState;

    #@22
    const-string v1, "DATAINANDOUT"

    #@24
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/Phone$DataActivityState;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAINANDOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@29
    new-instance v0, Lcom/android/internal/telephony/Phone$DataActivityState;

    #@2b
    const-string v1, "DORMANT"

    #@2d
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/Phone$DataActivityState;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DORMANT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@32
    .line 73
    const/4 v0, 0x5

    #@33
    new-array v0, v0, [Lcom/android/internal/telephony/Phone$DataActivityState;

    #@35
    sget-object v1, Lcom/android/internal/telephony/Phone$DataActivityState;->NONE:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@37
    aput-object v1, v0, v2

    #@39
    sget-object v1, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAIN:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@3b
    aput-object v1, v0, v3

    #@3d
    sget-object v1, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@3f
    aput-object v1, v0, v4

    #@41
    sget-object v1, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAINANDOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@43
    aput-object v1, v0, v5

    #@45
    sget-object v1, Lcom/android/internal/telephony/Phone$DataActivityState;->DORMANT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@47
    aput-object v1, v0, v6

    #@49
    sput-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->$VALUES:[Lcom/android/internal/telephony/Phone$DataActivityState;

    #@4b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/Phone$DataActivityState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 73
    const-class v0, Lcom/android/internal/telephony/Phone$DataActivityState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/Phone$DataActivityState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/Phone$DataActivityState;
    .registers 1

    #@0
    .prologue
    .line 73
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->$VALUES:[Lcom/android/internal/telephony/Phone$DataActivityState;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/Phone$DataActivityState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/Phone$DataActivityState;

    #@8
    return-object v0
.end method
