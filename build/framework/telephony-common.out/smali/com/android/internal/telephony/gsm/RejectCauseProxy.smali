.class public Lcom/android/internal/telephony/gsm/RejectCauseProxy;
.super Ljava/lang/Object;
.source "RejectCauseProxy.java"

# interfaces
.implements Lcom/android/internal/telephony/gsm/RejectCause;


# instance fields
.field private mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/gsm/RejectCause;)V
    .registers 3
    .parameter "rc"

    #@0
    .prologue
    .line 16
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 14
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/RejectCauseProxy;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@6
    .line 17
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/RejectCauseProxy;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@8
    .line 18
    return-void
.end method


# virtual methods
.method public bManualSelectionAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/RejectCauseProxy;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 34
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/RejectCauseProxy;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@6
    invoke-interface {v0}, Lcom/android/internal/telephony/gsm/RejectCause;->bManualSelectionAvailable()Z

    #@9
    move-result v0

    #@a
    .line 35
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x1

    #@c
    goto :goto_a
.end method

.method public clearRejectCause(II)Z
    .registers 5
    .parameter "clear_mm"
    .parameter "clear_gmm"

    #@0
    .prologue
    .line 41
    const/4 v0, 0x0

    #@1
    .line 42
    .local v0, result:Z
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/RejectCauseProxy;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@3
    if-eqz v1, :cond_b

    #@5
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/RejectCauseProxy;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@7
    invoke-interface {v1, p1, p2}, Lcom/android/internal/telephony/gsm/RejectCause;->clearRejectCause(II)Z

    #@a
    move-result v0

    #@b
    .line 43
    :cond_b
    return v0
.end method

.method public handleServiceStatusResult(Landroid/os/AsyncResult;)I
    .registers 4
    .parameter "ar"

    #@0
    .prologue
    .line 21
    const/4 v0, 0x0

    #@1
    .line 22
    .local v0, result:I
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/RejectCauseProxy;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@3
    if-eqz v1, :cond_b

    #@5
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/RejectCauseProxy;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@7
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/gsm/RejectCause;->handleServiceStatusResult(Landroid/os/AsyncResult;)I

    #@a
    move-result v0

    #@b
    .line 23
    :cond_b
    return v0
.end method

.method public initialize()V
    .registers 2

    #@0
    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/RejectCauseProxy;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/RejectCauseProxy;->mRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@6
    invoke-interface {v0}, Lcom/android/internal/telephony/gsm/RejectCause;->initialize()V

    #@9
    .line 28
    :cond_9
    return-void
.end method
