.class public Lcom/android/internal/telephony/SmsRawData;
.super Ljava/lang/Object;
.source "SmsRawData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/telephony/SmsRawData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field data:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 30
    new-instance v0, Lcom/android/internal/telephony/SmsRawData$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/SmsRawData$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/telephony/SmsRawData;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>([B)V
    .registers 2
    .parameter "data"

    #@0
    .prologue
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 47
    iput-object p1, p0, Lcom/android/internal/telephony/SmsRawData;->data:[B

    #@5
    .line 48
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 55
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getBytes()[B
    .registers 2

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/internal/telephony/SmsRawData;->data:[B

    #@2
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/internal/telephony/SmsRawData;->data:[B

    #@2
    array-length v0, v0

    #@3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 60
    iget-object v0, p0, Lcom/android/internal/telephony/SmsRawData;->data:[B

    #@8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@b
    .line 61
    return-void
.end method
