.class Lcom/android/internal/telephony/gsm/GSMPhone$PendingNaf;
.super Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;
.source "GSMPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gsm/GSMPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PendingNaf"
.end annotation


# instance fields
.field private mNafId:[B

.field final synthetic this$0:Lcom/android/internal/telephony/gsm/GSMPhone;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/gsm/GSMPhone;[BLandroid/os/Message;)V
    .registers 4
    .parameter
    .parameter "nafId"
    .parameter "onComplete"

    #@0
    .prologue
    .line 3253
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingNaf;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2
    .line 3254
    invoke-direct {p0, p1, p3}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Landroid/os/Message;)V

    #@5
    .line 3255
    iput-object p2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingNaf;->mNafId:[B

    #@7
    .line 3256
    return-void
.end method


# virtual methods
.method public onSessionStarted(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .registers 9
    .parameter "res"
    .parameter "e"

    #@0
    .prologue
    .line 3260
    if-eqz p2, :cond_6

    #@2
    .line 3261
    invoke-super {p0, p1, p2}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;->onSessionStarted(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@5
    .line 3266
    :goto_5
    return-void

    #@6
    .line 3263
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingNaf;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@8
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@a
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingNaf;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@c
    invoke-static {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$1600(Lcom/android/internal/telephony/gsm/GSMPhone;)I

    #@f
    move-result v1

    #@10
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingNaf;->mNafId:[B

    #@12
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingNaf;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@14
    const/16 v4, 0x2a

    #@16
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;->mOnComplete:Landroid/os/Message;

    #@18
    invoke-virtual {v3, v4, v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1b
    move-result-object v3

    #@1c
    invoke-interface {v0, v1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->uiccGbaAuthenticateNaf(I[BLandroid/os/Message;)V

    #@1f
    goto :goto_5
.end method
