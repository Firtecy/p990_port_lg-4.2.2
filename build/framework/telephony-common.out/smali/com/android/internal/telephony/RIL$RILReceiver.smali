.class Lcom/android/internal/telephony/RIL$RILReceiver;
.super Ljava/lang/Object;
.source "RIL.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/RIL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RILReceiver"
.end annotation


# instance fields
.field buffer:[B

.field final synthetic this$0:Lcom/android/internal/telephony/RIL;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/RIL;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 666
    iput-object p1, p0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 667
    const/16 v0, 0x2000

    #@7
    new-array v0, v0, [B

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/RIL$RILReceiver;->buffer:[B

    #@b
    .line 668
    return-void
.end method


# virtual methods
.method public run()V
    .registers 19

    #@0
    .prologue
    .line 672
    const/4 v9, 0x0

    #@1
    .line 673
    .local v9, retryCount:I
    const-string v10, "rild"

    #@3
    .line 676
    .local v10, rilSocket:Ljava/lang/String;
    :goto_3
    const/4 v11, 0x0

    #@4
    .line 679
    .local v11, s:Landroid/net/LocalSocket;
    :try_start_4
    const-string v15, "ro.multi.rild"

    #@6
    const/16 v16, 0x0

    #@8
    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@b
    move-result v7

    #@c
    .line 687
    .local v7, multiRild:Z
    move-object/from16 v0, p0

    #@e
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@10
    invoke-static {v15}, Lcom/android/internal/telephony/RIL;->access$500(Lcom/android/internal/telephony/RIL;)Ljava/lang/Integer;

    #@13
    move-result-object v15

    #@14
    if-eqz v15, :cond_26

    #@16
    move-object/from16 v0, p0

    #@18
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@1a
    invoke-static {v15}, Lcom/android/internal/telephony/RIL;->access$500(Lcom/android/internal/telephony/RIL;)Ljava/lang/Integer;

    #@1d
    move-result-object v15

    #@1e
    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    #@21
    move-result v15

    #@22
    if-eqz v15, :cond_26

    #@24
    if-nez v7, :cond_135

    #@26
    .line 688
    :cond_26
    const-string v10, "rild"
    :try_end_28
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_28} :catch_19f

    #@28
    .line 694
    :goto_28
    :try_start_28
    new-instance v12, Landroid/net/LocalSocket;

    #@2a
    invoke-direct {v12}, Landroid/net/LocalSocket;-><init>()V
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_2d} :catch_139
    .catch Ljava/lang/Throwable; {:try_start_28 .. :try_end_2d} :catch_19f

    #@2d
    .line 695
    .end local v11           #s:Landroid/net/LocalSocket;
    .local v12, s:Landroid/net/LocalSocket;
    :try_start_2d
    new-instance v5, Landroid/net/LocalSocketAddress;

    #@2f
    sget-object v15, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    #@31
    invoke-direct {v5, v10, v15}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    #@34
    .line 697
    .local v5, l:Landroid/net/LocalSocketAddress;
    invoke-virtual {v12, v5}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V
    :try_end_37
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_37} :catch_235
    .catch Ljava/lang/Throwable; {:try_start_2d .. :try_end_37} :catch_120

    #@37
    .line 730
    const/4 v9, 0x0

    #@38
    .line 732
    :try_start_38
    move-object/from16 v0, p0

    #@3a
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@3c
    iput-object v12, v15, Lcom/android/internal/telephony/RIL;->mSocket:Landroid/net/LocalSocket;

    #@3e
    .line 733
    const-string v15, "RILJ"

    #@40
    new-instance v16, Ljava/lang/StringBuilder;

    #@42
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v17, "Connected to \'"

    #@47
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v16

    #@4b
    move-object/from16 v0, v16

    #@4d
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v16

    #@51
    const-string v17, "\' socket"

    #@53
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v16

    #@57
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v16

    #@5b
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 735
    move-object/from16 v0, p0

    #@60
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@62
    invoke-static {v15}, Lcom/android/internal/telephony/RIL;->access$500(Lcom/android/internal/telephony/RIL;)Ljava/lang/Integer;

    #@65
    move-result-object v15

    #@66
    if-eqz v15, :cond_79

    #@68
    move-object/from16 v0, p0

    #@6a
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@6c
    invoke-static {v15}, Lcom/android/internal/telephony/RIL;->access$500(Lcom/android/internal/telephony/RIL;)Ljava/lang/Integer;

    #@6f
    move-result-object v15

    #@70
    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    #@73
    move-result v15

    #@74
    if-eqz v15, :cond_79

    #@76
    const/4 v15, 0x1

    #@77
    if-ne v7, v15, :cond_1a1

    #@79
    :cond_79
    const-string v13, "SUB1"

    #@7b
    .line 744
    .local v13, str:Ljava/lang/String;
    :goto_7b
    const-string v15, "RILJ"

    #@7d
    new-instance v16, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v17, "Sending  SUB data : "

    #@84
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v16

    #@88
    move-object/from16 v0, v16

    #@8a
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v16

    #@8e
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v16

    #@92
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 745
    invoke-virtual {v13}, Ljava/lang/String;->getBytes()[B
    :try_end_98
    .catch Ljava/lang/Throwable; {:try_start_38 .. :try_end_98} :catch_120

    #@98
    move-result-object v1

    #@99
    .line 747
    .local v1, data:[B
    :try_start_99
    move-object/from16 v0, p0

    #@9b
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@9d
    iget-object v15, v15, Lcom/android/internal/telephony/RIL;->mSocket:Landroid/net/LocalSocket;

    #@9f
    invoke-virtual {v15}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@a2
    move-result-object v15

    #@a3
    invoke-virtual {v15, v1}, Ljava/io/OutputStream;->write([B)V

    #@a6
    .line 748
    const-string v15, "RILJ"

    #@a8
    const-string v16, "Data sent!!"

    #@aa
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ad
    .catch Ljava/io/IOException; {:try_start_99 .. :try_end_ad} :catch_1a5
    .catch Ljava/lang/RuntimeException; {:try_start_99 .. :try_end_ad} :catch_1b1
    .catch Ljava/lang/Throwable; {:try_start_99 .. :try_end_ad} :catch_120

    #@ad
    .line 754
    :goto_ad
    const/4 v6, 0x0

    #@ae
    .line 756
    .local v6, length:I
    :try_start_ae
    move-object/from16 v0, p0

    #@b0
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@b2
    iget-object v15, v15, Lcom/android/internal/telephony/RIL;->mSocket:Landroid/net/LocalSocket;

    #@b4
    invoke-virtual {v15}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    #@b7
    move-result-object v4

    #@b8
    .line 761
    .local v4, is:Ljava/io/InputStream;
    :goto_b8
    move-object/from16 v0, p0

    #@ba
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->buffer:[B

    #@bc
    invoke-static {v4, v15}, Lcom/android/internal/telephony/RIL;->access$600(Ljava/io/InputStream;[B)I
    :try_end_bf
    .catch Ljava/io/IOException; {:try_start_ae .. :try_end_bf} :catch_1dc
    .catch Ljava/lang/Throwable; {:try_start_ae .. :try_end_bf} :catch_201

    #@bf
    move-result v6

    #@c0
    .line 763
    if-gez v6, :cond_1bd

    #@c2
    .line 785
    .end local v4           #is:Ljava/io/InputStream;
    :goto_c2
    :try_start_c2
    const-string v15, "RILJ"

    #@c4
    new-instance v16, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v17, "Disconnected from \'"

    #@cb
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v16

    #@cf
    move-object/from16 v0, v16

    #@d1
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v16

    #@d5
    const-string v17, "\' socket"

    #@d7
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v16

    #@db
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v16

    #@df
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    .line 788
    move-object/from16 v0, p0

    #@e4
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@e6
    sget-object v16, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_UNAVAILABLE:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@e8
    invoke-virtual/range {v15 .. v16}, Lcom/android/internal/telephony/RIL;->setRadioState(Lcom/android/internal/telephony/CommandsInterface$RadioState;)V
    :try_end_eb
    .catch Ljava/lang/Throwable; {:try_start_c2 .. :try_end_eb} :catch_120

    #@eb
    .line 791
    :try_start_eb
    move-object/from16 v0, p0

    #@ed
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@ef
    iget-object v15, v15, Lcom/android/internal/telephony/RIL;->mSocket:Landroid/net/LocalSocket;

    #@f1
    invoke-virtual {v15}, Landroid/net/LocalSocket;->close()V
    :try_end_f4
    .catch Ljava/io/IOException; {:try_start_eb .. :try_end_f4} :catch_232
    .catch Ljava/lang/Throwable; {:try_start_eb .. :try_end_f4} :catch_120

    #@f4
    .line 795
    :goto_f4
    :try_start_f4
    move-object/from16 v0, p0

    #@f6
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@f8
    const/16 v16, 0x0

    #@fa
    move-object/from16 v0, v16

    #@fc
    iput-object v0, v15, Lcom/android/internal/telephony/RIL;->mSocket:Landroid/net/LocalSocket;

    #@fe
    .line 796
    invoke-static {}, Lcom/android/internal/telephony/RILRequest;->resetSerial()V

    #@101
    .line 800
    move-object/from16 v0, p0

    #@103
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@105
    const/16 v16, 0x0

    #@107
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/RIL;->access$802(Lcom/android/internal/telephony/RIL;I)I

    #@10a
    .line 801
    move-object/from16 v0, p0

    #@10c
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@10e
    const/16 v16, 0x0

    #@110
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/RIL;->access$902(Lcom/android/internal/telephony/RIL;I)I

    #@113
    .line 805
    move-object/from16 v0, p0

    #@115
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@117
    const/16 v16, 0x1

    #@119
    const/16 v17, 0x0

    #@11b
    invoke-static/range {v15 .. v17}, Lcom/android/internal/telephony/RIL;->access$1000(Lcom/android/internal/telephony/RIL;IZ)V
    :try_end_11e
    .catch Ljava/lang/Throwable; {:try_start_f4 .. :try_end_11e} :catch_120

    #@11e
    goto/16 :goto_3

    #@120
    .line 806
    .end local v1           #data:[B
    .end local v5           #l:Landroid/net/LocalSocketAddress;
    .end local v6           #length:I
    .end local v13           #str:Ljava/lang/String;
    :catch_120
    move-exception v14

    #@121
    move-object v11, v12

    #@122
    .line 807
    .end local v7           #multiRild:Z
    .end local v12           #s:Landroid/net/LocalSocket;
    .restart local v11       #s:Landroid/net/LocalSocket;
    .local v14, tr:Ljava/lang/Throwable;
    :goto_122
    const-string v15, "RILJ"

    #@124
    const-string v16, "Uncaught exception"

    #@126
    move-object/from16 v0, v16

    #@128
    invoke-static {v15, v0, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12b
    .line 811
    move-object/from16 v0, p0

    #@12d
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@12f
    const/16 v16, -0x1

    #@131
    invoke-static/range {v15 .. v16}, Lcom/android/internal/telephony/RIL;->access$1100(Lcom/android/internal/telephony/RIL;I)V

    #@134
    .line 812
    return-void

    #@135
    .line 690
    .end local v14           #tr:Ljava/lang/Throwable;
    .restart local v7       #multiRild:Z
    :cond_135
    :try_start_135
    const-string v10, "rild1"
    :try_end_137
    .catch Ljava/lang/Throwable; {:try_start_135 .. :try_end_137} :catch_19f

    #@137
    goto/16 :goto_28

    #@139
    .line 698
    :catch_139
    move-exception v2

    #@13a
    .line 700
    .local v2, ex:Ljava/io/IOException;
    :goto_13a
    if-eqz v11, :cond_13f

    #@13c
    .line 701
    :try_start_13c
    invoke-virtual {v11}, Landroid/net/LocalSocket;->close()V
    :try_end_13f
    .catch Ljava/io/IOException; {:try_start_13c .. :try_end_13f} :catch_22c
    .catch Ljava/lang/Throwable; {:try_start_13c .. :try_end_13f} :catch_19f

    #@13f
    .line 710
    :cond_13f
    :goto_13f
    const/16 v15, 0x8

    #@141
    if-ne v9, v15, :cond_178

    #@143
    .line 711
    :try_start_143
    const-string v15, "RILJ"

    #@145
    new-instance v16, Ljava/lang/StringBuilder;

    #@147
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@14a
    const-string v17, "Couldn\'t find \'"

    #@14c
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v16

    #@150
    move-object/from16 v0, v16

    #@152
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v16

    #@156
    const-string v17, "\' socket after "

    #@158
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v16

    #@15c
    move-object/from16 v0, v16

    #@15e
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@161
    move-result-object v16

    #@162
    const-string v17, " times, continuing to retry silently"

    #@164
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v16

    #@168
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v16

    #@16c
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_16f
    .catch Ljava/lang/Throwable; {:try_start_143 .. :try_end_16f} :catch_19f

    #@16f
    .line 722
    :cond_16f
    :goto_16f
    const-wide/16 v15, 0xfa0

    #@171
    :try_start_171
    invoke-static/range {v15 .. v16}, Ljava/lang/Thread;->sleep(J)V
    :try_end_174
    .catch Ljava/lang/InterruptedException; {:try_start_171 .. :try_end_174} :catch_22f
    .catch Ljava/lang/Throwable; {:try_start_171 .. :try_end_174} :catch_19f

    #@174
    .line 726
    :goto_174
    add-int/lit8 v9, v9, 0x1

    #@176
    .line 727
    goto/16 :goto_3

    #@178
    .line 715
    :cond_178
    if-lez v9, :cond_16f

    #@17a
    const/16 v15, 0x8

    #@17c
    if-ge v9, v15, :cond_16f

    #@17e
    .line 716
    :try_start_17e
    const-string v15, "RILJ"

    #@180
    new-instance v16, Ljava/lang/StringBuilder;

    #@182
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@185
    const-string v17, "Couldn\'t find \'"

    #@187
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v16

    #@18b
    move-object/from16 v0, v16

    #@18d
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@190
    move-result-object v16

    #@191
    const-string v17, "\' socket; retrying after timeout"

    #@193
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@196
    move-result-object v16

    #@197
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19a
    move-result-object v16

    #@19b
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_19e
    .catch Ljava/lang/Throwable; {:try_start_17e .. :try_end_19e} :catch_19f

    #@19e
    goto :goto_16f

    #@19f
    .line 806
    .end local v2           #ex:Ljava/io/IOException;
    .end local v7           #multiRild:Z
    :catch_19f
    move-exception v14

    #@1a0
    goto :goto_122

    #@1a1
    .line 735
    .end local v11           #s:Landroid/net/LocalSocket;
    .restart local v5       #l:Landroid/net/LocalSocketAddress;
    .restart local v7       #multiRild:Z
    .restart local v12       #s:Landroid/net/LocalSocket;
    :cond_1a1
    :try_start_1a1
    const-string v13, "SUB2"

    #@1a3
    goto/16 :goto_7b

    #@1a5
    .line 749
    .restart local v1       #data:[B
    .restart local v13       #str:Ljava/lang/String;
    :catch_1a5
    move-exception v2

    #@1a6
    .line 750
    .restart local v2       #ex:Ljava/io/IOException;
    const-string v15, "RILJ"

    #@1a8
    const-string v16, "IOException"

    #@1aa
    move-object/from16 v0, v16

    #@1ac
    invoke-static {v15, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1af
    goto/16 :goto_ad

    #@1b1
    .line 751
    .end local v2           #ex:Ljava/io/IOException;
    :catch_1b1
    move-exception v3

    #@1b2
    .line 752
    .local v3, exc:Ljava/lang/RuntimeException;
    const-string v15, "RILJ"

    #@1b4
    const-string v16, "Uncaught exception "

    #@1b6
    move-object/from16 v0, v16

    #@1b8
    invoke-static {v15, v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1bb
    .catch Ljava/lang/Throwable; {:try_start_1a1 .. :try_end_1bb} :catch_120

    #@1bb
    goto/16 :goto_ad

    #@1bd
    .line 768
    .end local v3           #exc:Ljava/lang/RuntimeException;
    .restart local v4       #is:Ljava/io/InputStream;
    .restart local v6       #length:I
    :cond_1bd
    :try_start_1bd
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1c0
    move-result-object v8

    #@1c1
    .line 769
    .local v8, p:Landroid/os/Parcel;
    move-object/from16 v0, p0

    #@1c3
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->buffer:[B

    #@1c5
    const/16 v16, 0x0

    #@1c7
    move/from16 v0, v16

    #@1c9
    invoke-virtual {v8, v15, v0, v6}, Landroid/os/Parcel;->unmarshall([BII)V

    #@1cc
    .line 770
    const/4 v15, 0x0

    #@1cd
    invoke-virtual {v8, v15}, Landroid/os/Parcel;->setDataPosition(I)V

    #@1d0
    .line 774
    move-object/from16 v0, p0

    #@1d2
    iget-object v15, v0, Lcom/android/internal/telephony/RIL$RILReceiver;->this$0:Lcom/android/internal/telephony/RIL;

    #@1d4
    invoke-static {v15, v8}, Lcom/android/internal/telephony/RIL;->access$700(Lcom/android/internal/telephony/RIL;Landroid/os/Parcel;)V

    #@1d7
    .line 775
    invoke-virtual {v8}, Landroid/os/Parcel;->recycle()V
    :try_end_1da
    .catch Ljava/io/IOException; {:try_start_1bd .. :try_end_1da} :catch_1dc
    .catch Ljava/lang/Throwable; {:try_start_1bd .. :try_end_1da} :catch_201

    #@1da
    goto/16 :goto_b8

    #@1dc
    .line 777
    .end local v4           #is:Ljava/io/InputStream;
    .end local v8           #p:Landroid/os/Parcel;
    :catch_1dc
    move-exception v2

    #@1dd
    .line 778
    .restart local v2       #ex:Ljava/io/IOException;
    :try_start_1dd
    const-string v15, "RILJ"

    #@1df
    new-instance v16, Ljava/lang/StringBuilder;

    #@1e1
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@1e4
    const-string v17, "\'"

    #@1e6
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v16

    #@1ea
    move-object/from16 v0, v16

    #@1ec
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ef
    move-result-object v16

    #@1f0
    const-string v17, "\' socket closed"

    #@1f2
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v16

    #@1f6
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f9
    move-result-object v16

    #@1fa
    move-object/from16 v0, v16

    #@1fc
    invoke-static {v15, v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1ff
    goto/16 :goto_c2

    #@201
    .line 780
    .end local v2           #ex:Ljava/io/IOException;
    :catch_201
    move-exception v14

    #@202
    .line 781
    .restart local v14       #tr:Ljava/lang/Throwable;
    const-string v15, "RILJ"

    #@204
    new-instance v16, Ljava/lang/StringBuilder;

    #@206
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@209
    const-string v17, "Uncaught exception read length="

    #@20b
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20e
    move-result-object v16

    #@20f
    move-object/from16 v0, v16

    #@211
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@214
    move-result-object v16

    #@215
    const-string v17, "Exception:"

    #@217
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v16

    #@21b
    invoke-virtual {v14}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    #@21e
    move-result-object v17

    #@21f
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@222
    move-result-object v16

    #@223
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@226
    move-result-object v16

    #@227
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_22a
    .catch Ljava/lang/Throwable; {:try_start_1dd .. :try_end_22a} :catch_120

    #@22a
    goto/16 :goto_c2

    #@22c
    .line 703
    .end local v1           #data:[B
    .end local v5           #l:Landroid/net/LocalSocketAddress;
    .end local v6           #length:I
    .end local v12           #s:Landroid/net/LocalSocket;
    .end local v13           #str:Ljava/lang/String;
    .end local v14           #tr:Ljava/lang/Throwable;
    .restart local v2       #ex:Ljava/io/IOException;
    .restart local v11       #s:Landroid/net/LocalSocket;
    :catch_22c
    move-exception v15

    #@22d
    goto/16 :goto_13f

    #@22f
    .line 723
    :catch_22f
    move-exception v15

    #@230
    goto/16 :goto_174

    #@232
    .line 792
    .end local v2           #ex:Ljava/io/IOException;
    .end local v11           #s:Landroid/net/LocalSocket;
    .restart local v1       #data:[B
    .restart local v5       #l:Landroid/net/LocalSocketAddress;
    .restart local v6       #length:I
    .restart local v12       #s:Landroid/net/LocalSocket;
    .restart local v13       #str:Ljava/lang/String;
    :catch_232
    move-exception v15

    #@233
    goto/16 :goto_f4

    #@235
    .line 698
    .end local v1           #data:[B
    .end local v5           #l:Landroid/net/LocalSocketAddress;
    .end local v6           #length:I
    .end local v13           #str:Ljava/lang/String;
    :catch_235
    move-exception v2

    #@236
    move-object v11, v12

    #@237
    .end local v12           #s:Landroid/net/LocalSocket;
    .restart local v11       #s:Landroid/net/LocalSocket;
    goto/16 :goto_13a
.end method
